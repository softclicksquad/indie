<style type="text/css">
.min-block-time {
    display: block;
    text-align: center;
    vertical-align: middle;
}
.time-remaining-head {
    display: block;
    text-align: center;
    vertical-align: middle;
    font-size: 16px;
    color: #223750;
    font-family: 'ralewaysemibold';
    margin-bottom: 5px;
}
.count-min-block {
    font-size: 26px;
    color: #228B22;
    font-family: 'ralewaysemibold';
    line-height: 26px;
}
.min-label-block {
    font-size: 12px;
    color: #5f5f5f;
    text-transform: uppercase;
}
.not-approved-btn {
  
      border: 1px solid #2d2d2d;
      background: #2d2d2d;
    padding: 0 20px;
      border-radius: 3px;
      color: #fff;
      height: 32px;

    border: 1px solid #2d2d2d;
  }
                        
td{border-top: 0px !important;}
</style>
<?php
  $user     = Sentinel::check();
  if ($user->inRole('project_manager')){
    $folder = "project_manager";  
  }elseif ($user->inRole('client')){
    $folder = "client"; 
  } elseif ($user->inRole('expert')){
    $folder = "expert"; 
  }
?>
@extends($folder.'.layout.master')  
@section('main_content') 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" integrity="sha256-Z8TW+REiUm9zSQMGZH4bfZi52VJgMqETCbPFlGRB1P8=" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.js" integrity="sha256-ZvMf9li0M5GGriGUEKn1g6lLwnj5u+ENqCbLM5ItjQ0=" crossorigin="anonymous"></script>

      <div class="col-sm-7 col-md-8 col-lg-9">
         @include('front.layout._operation_status')
            <div class="milestone-listing">
             
            @if(isset($arr_milestone['data']) && count($arr_milestone['data']) > 0)
              <div class="head_grn">{{trans('milestones/project_milestones.text_title')}}</div>

              <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;"> 
                 <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                    <div class="search-grey-bx">
                    <div class="head_grn">
                      <a href="{{$module_url_path or ''}}/details/{{isset($arr_milestone['data'][0]['project_details']['id'])?base64_encode($arr_milestone['data'][0]['project_details']['id']):''}}">
                        {{$arr_milestone['data'][0]['project_details']['project_name'] or 'N/A'}}    
                      </a>
                    </div>
                     <div class="clearfix"></div>
                     <div class="row">
                      <!-- Wallet Details -->
                      @if(isset($mangopay_wallet_details) && $mangopay_wallet_details !="" && !empty($mangopay_wallet_details))
                      <div class="col-sm-12 col-md-6 col-lg-6 col-padding-setting" style="padding-left:0px; margin-bottom: 15px;">
                        <div class="dash-user-details">
                            <div class="title"><i class="fa fa-money" aria-hidden="true"></i> {{trans('common/wallet/text.text_project_wallet')}} </div>
                            <div class="user-details-section">
                                <table class="table" style="margin-bottom:15px">
                                  <tbody>     
                                    <tr>
                                      <td>
                                        <span class="card-id"> {{ isset($mangopay_wallet_details->Id)? $mangopay_wallet_details->Id:''}} </span>
                                      </td>
                                      <td>
                                        <div class="description" style="font-size: 11px;">{{isset($mangopay_wallet_details->Description)? $mangopay_wallet_details->Description:''}}</div>
                                      </td>
                                      <td style="font-size: 17px;width: 120px;">
                                        <div class="high-text-up">
                                          <span>
                                            {{ isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:''}}
                                          </span>
                                          <span class="mp-amount">
                                            {{ isset($mangopay_wallet_details->Balance->Amount)? $mangopay_wallet_details->Balance->Amount/100:'0'}}
                                          </span>
                                         
                                        </div>
                                        <div class="small-text-down" style="font-size: 10px;">
                                          {{trans('common/wallet/text.text_money_balance')}}
                                        </div>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                            </div>
                        </div>
                      </div>
                      @endif 
                      @if(isset($user) && $user->inRole('client'))
                        <?php
                          $arr_profile_data = sidebar_information($arr_bid_details['expert_user_id']);
                        ?>

                      <div class="col-sm-12 col-md-6 col-lg-6 col-padding-setting" style="padding-right:0px; margin-bottom: 15px;">
                      <div class="dash-user-details">
                          <div class="title">Bid Proposal </div>
                          <div class="user-details-section">
                              <div class="mck-row">      
                                  <div class="listin-pro-image">
                                    @if(isset($arr_bid_details['expert_details']['is_online']) && $arr_bid_details['expert_details']['is_online']!='' && $arr_bid_details['expert_details']['is_online']=='1')
                                      <div class="user-online-round"></div>
                                    @else
                                      <div class="user-offline-round"></div>
                                    @endif

                                    @if(isset($arr_bid_details['expert_details']['role_info']['profile_image']) && $arr_bid_details['expert_details']['role_info']['profile_image'] != ""  && file_exists('public/uploads/front/profile/'.$arr_bid_details['expert_details']['role_info']['profile_image']) ) 
                                      <div class="going-pro">
                                      <img src="{{ url('/public/uploads/front/profile/').'/'.$arr_bid_details['expert_details']['role_info']['profile_image']}}" alt="pic"/>
                                      </div>
                                    @else
                                      <div class="going-pro">
                                      <img src="{{  url('/public/uploads/front/profile/default_profile_image.png')}}" alt="pic"/>

                                      </div>
                                    @endif
                                  </div> 

                                  <div class="user-discription-content-main"> 
                                      <div class="user-discription-content-section">
                                          <a href="javascript:void(0)" class="profile-name-section-block">
                                              {{isset($arr_bid_details['expert_details']['role_info']['first_name'])?$arr_bid_details['expert_details']['role_info']['first_name']:'Unknown user'}} 
                                              @if(isset($arr_bid_details['expert_details']['role_info']['last_name']) && $arr_bid_details['expert_details']['role_info']['last_name'] !="") @php echo substr($arr_bid_details['expert_details']['role_info']['last_name'],0,1).'.'; @endphp @endif
                                          </a>
                                          <div class="place-time-section">
                                            @if( isset($arr_profile_data['last_login_duration']) && $arr_profile_data['last_login_duration'] == 'Active')
                                            <span class="pull-left" title="{{isset($arr_profile_data['expert_timezone'])?$arr_profile_data['expert_timezone']:'Not Specify'}}">
                                              <span style="color:#4D4D4D;font-size: 14px;text-transform:none;cursor:text;">
                                                <span class="flag-image"> 
                                                  @if(isset($arr_profile_data['user_country_flag'])) 
                                                  @php 
                                                  echo $arr_profile_data['user_country_flag']; 
                                                  @endphp 
                                                  @elseif(isset($arr_profile_data['user_country'])) 
                                                  @php 
                                                  echo $arr_profile_data['user_country']; 
                                                  @endphp 
                                                  @else 
                                                  @php 
                                                  echo 'Not Specify'; 
                                                  @endphp 
                                                  @endif 
                                                </span>
                                              {{-- trans('common/_expert_details.text_local_time') --}}  {{isset($arr_profile_data['expert_timezone_without_date'])?$arr_profile_data['expert_timezone_without_date']:'Not Specify'}}</span>
                                            </span>
                                            @else
                                            <span class="flag-image"> 
                                              @if(isset($arr_profile_data['user_country_flag'])) 
                                              @php 
                                              echo $arr_profile_data['user_country_flag']; 
                                              @endphp 
                                              @elseif(isset($arr_profile_data['user_country'])) 
                                              @php 
                                              echo $arr_profile_data['user_country']; 
                                              @endphp 
                                              @else 
                                              @php 
                                              echo 'Not Specify'; 
                                              @endphp 
                                              @endif 
                                            </span>   
                                              <span class="date-info">
                                             {{ isset($arr_profile_data['last_login_duration'])?$arr_profile_data['last_login_duration']:'Not Specify' }}  
                                                  </span>
                                            @endif
                                          </div>
                                      </div>

                                    <a href="#" class="applozic-launcher" data-mck-id="{{env('applozicUserIdPrefix')}}{{$arr_bid_details['expert_user_id']}}" data-mck-name="{{isset($arr_bid_details['expert_details']['role_info']['first_name'])?$arr_bid_details['expert_details']['role_info']['first_name']:'Unknown user'}} 
                                    @if(isset($arr_bid_details['expert_details']['role_info']['last_name']) && $arr_bid_details['expert_details']['role_info']['last_name'] !="") @php echo substr($arr_bid_details['expert_details']['role_info']['last_name'],0,1).'.'; @endphp @endif" ><img style="width: 25px;" src="{{url('/public')}}/front/images/comment-icon.png" alt="" /></a>


                                  </div>
                                  <div class="clearfix"></div>
                              </div>

                              <div class="projrct-prce1">
                                   @if(isset($arr_bid_details['project_currency_code']) && $arr_bid_details['project_currency_code']=="$")
                                    <span>
                                      <img src="{{url('/public')}}/front/images/doller-img.png" alt="img"/> 
                                    </span> <span>{{$arr_bid_details['project_currency_code']}}
                                    {{isset($arr_bid_details['project_bid_info']['bid_cost'])?number_format($arr_bid_details['project_bid_info']['bid_cost']):0}}</span>
                                    @else
                                    <span>
                                      <img src="{{url('/public')}}/front/images/doller-img-new-1.png" alt="img"/> 
                                    </span> <span>{{$arr_bid_details['project_currency_code']}}
                                    {{isset($arr_bid_details['project_bid_info']['bid_cost'])?number_format($arr_bid_details['project_bid_info']['bid_cost']):0}}</span>
                                    @endif
                              </div>
                              <div class="projrct-prce1">
                                  <span>
                                      <i class="fa fa-clock-o"></i>
                                  </span> 
                                  <span>
                                     {{isset($arr_bid_details['project_bid_info']['bid_estimated_duration'])?$arr_bid_details['project_bid_info']['bid_estimated_duration']:0}} Days
                                  </span>
                              </div>
                          </div>
                      </div>
                      </div>
                      @endif

                    </div>




                      <!-- End Wallet Details -->
                    </div>
                  </td>
               </tr>
               @foreach($arr_milestone['data'] as $key=>$milestone)
                 <div class="search-grey-bx white-wrapper">
                    <div class="milestone-details">
                          @if(empty($milestone['milestone_release_details']['status']) || isset($milestone['milestone_release_details']['status']) && $milestone['milestone_release_details']['status'] != '2' && $milestone['status'] != '0')
                            <div class="min-block-time">
                                <div id='mins{{$milestone["id"]}}' class="count-min-block">
                                    <span id='timer{{$milestone["id"]}}'></span>
                                </div>
                            </div>
                          @endif
                          <h3><span class="milestone-batch">Milestone {{ isset($milestone['milestone_no']) ?  $milestone['milestone_no']:"0" }}</span> &nbsp;&nbsp; {{ isset($milestone['title']) ? $milestone['title']:"" }}</h3>
                          <div class="project-list mile-list">
                             <ul>
                                <li><span class="hidden-xs hidden-sm"><i class="fa fa-calendar" aria-hidden="true"></i></span>{{ date('d M Y' , strtotime($milestone['created_at']))}}</li>
                                @if(empty($milestone['milestone_release_details']['status']) || isset($milestone['milestone_release_details']['status']) && count($milestone['milestone_refund_details']) <= 0 || $milestone['milestone_refund_details'] == 'null')
                                  @if(empty($milestone['milestone_release_details']['status']) || isset($milestone['milestone_release_details']['status']) &&  $milestone['milestone_release_details']['status'] != '2' || $milestone['milestone_release_details']['status'] == 'null')
                                    @if(isset($milestone['milestone_due_date']) && $milestone['milestone_due_date'] != "" && $milestone['milestone_due_date'] != '0000-00-00 00:00:00' && $milestone['status'] != '0')
                                    <?php 
                                      $timeFirst             = strtotime(date('Y-m-d H:i:s'));
                                      $timeSecond            = strtotime($milestone['milestone_due_date']);
                                      $differenceInSeconds   = $timeSecond - $timeFirst;
                                      if($differenceInSeconds <= 0){
                                        $differenceInSeconds = '0';
                                      }
                                    ?>
                                    <script type="text/javascript">
                                        var time        = '<?php echo $differenceInSeconds; ?>';
                                        var milestoneid = '<?php echo $milestone["id"]; ?>';
                                        if(time < 0) {  time = 0; }
                                        var c           = time; // in seconds
                                        var t;
                                        var days    = parseInt( c / 86400 ) % 365;
                                        var hours   = parseInt( c / 3600 ) % 24;
                                        var minutes = parseInt( c / 60 ) % 60;
                                        var seconds = c % 60;
                                        //var result  = (days < 10 ? "0" + days : days) + ":"+ (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
                                        var d = ""; 
                                        var h = ""; 
                                        var m = "";
                                        var s = ""; 

                                        if(days     !=  ""  &&  days     !="0"){  d  =  (days+'d')     +  " ";  }
                                        if(hours    !=  ""  &&  hours    !="0"){  h  =  (hours+'h')    +  " ";  }
                                        if(minutes  !=  ""  &&  minutes  !="0"){  m  =  (minutes+'m')  +  " ";  }
                                        if(seconds  !=  ""  &&  seconds  !="0"){  s  =  (seconds+'s')  +  " ";  }
                                        
                                        if(d != "" && h !=""){
                                        var result    = d + h;
                                        }else if(h != "" && m !=""){
                                        var result    = h + m;
                                        }else if(m !=""){
                                        var result    = m;
                                        }else if(s !=""){
                                        var result    = s;
                                        }
                                        if(result == "NaN:NaN:NaN:NaN"){ 
                                          $('#timer'+milestoneid).html('{{trans('client/projects/ongoing_projects.text_next_expire')}}');
                                          $('#mins'+milestoneid).addClass('expired');
                                          $('#mins'+milestoneid).removeClass('timer');
                                        } else {
                                          $('#timer'+milestoneid).html(result);
                                          $('#mins'+milestoneid).addClass('timer');
                                          $('#mins'+milestoneid).removeClass('expired');
                                        }
                                        if(c == 0 ){
                                          $('#timer'+milestoneid).html('{{trans('client/projects/ongoing_projects.text_next_expire')}}');
                                          $('#mins'+milestoneid).addClass('expired');
                                          $('#mins'+milestoneid).removeClass('timer');
                                        }
                                        c = c - 1;
                                    </script>
                                  @endif
                                @endif
                                <li><span class="hidden-xs hidden-sm"><i class="fa fa-calendar" aria-hidden="true"></i></span>{{trans('client/projects/project_details.entry_milestone_due_date')}} : {{ date('d M Y' , strtotime($milestone['milestone_due_date']))}}</li>
                                @endif
                                
                                <li style="background:none;">
                                <span class="projrct-prce">
                                <i class="hidden-xs hidden-sm fa fa-money" aria-hidden="true"></i>                 
                                  @if(isset($milestone_project_currency_code))
                                    {{ $milestone_project_currency_code }}
                                  @elseif(isset($milestone['project_details']['project_currency_code']))
                                    {{$milestone['project_details']['project_currency_code']}}
                                  @endif
                                   {{ isset($milestone['cost'])? number_format($milestone['cost']): '0' }}
                                </span>
                                </li>
                             </ul>
                          </div>
                          <div class="more-project-dec" style="margin-bottom: 10px;">{{ str_limit(isset($milestone['description'])? $milestone['description']: "", 350) }}</div>
                          @if(isset($milestone['milestone_refund_details']) && count($milestone['milestone_refund_details']) > 0)
                              @if($user->inRole('expert'))
                                <a href="javascript:void(0)">
                                   <button class="awd_btn new-btn-class black-border-new-btn">Aborted by client</button>
                                </a>
                                <div class="clerfix"></div>
                                <small><i>Client requested for refund to ArchExperts</i></small>
                              @elseif($user->inRole('client'))
                                <a href="javascript:void(0)">
                                   @if($milestone['milestone_refund_details']['is_refunded'] == 'no')
                                   <button class="awd_btn new-btn-class green-new-btn">Refund requested</button>
                                   <button class="awd_btn new-btn-class request-refund-btn" 
                                           href="#refund_request" 
                                           data-keyboard="false" 
                                           data-backdrop="static" 
                                           data-toggle="modal" 
                                           id="refund-request-model" 
                                           data-milestone="{{isset($milestone['milestone_no']) ?  $milestone['milestone_no']:'0'}}" 
                                           data-expert_id="{{isset($milestone['expert_user_id']) ?  $milestone['expert_user_id']:'0'}}" 
                                           data-milestone-cost="{{isset($milestone['cost']) ?  $milestone['cost']:'0'}}" 
                                           data-project-id="{{isset($milestone['project_id'])?$milestone['project_id']:'0'}}" 
                                           data-currency-code="{{ isset($milestone['project_details']['project_currency_code']) ? $milestone['project_details']['project_currency_code'] : 'USD' }}"
                                           data-milestone-id="{{isset($milestone['id'])?$milestone['id']:'0'}}"><i class="fa fa-repeat" aria-hidden="true"></i> Resend</button>
                                   @else
                                   <button class="awd_btn new-btn-class green-new-btn"><i class="fa fa-check-circle" aria-hidden="true"></i> Refunded</button>
                                   @endif`
                                </a>
                              @endif  
                          @else

                               @if(isset($milestone['transaction_details']) && isset($milestone['transaction_details']['payment_status']) && ($milestone['transaction_details']['payment_status']==1 || $milestone['transaction_details']['payment_status']==2) )
                                    @if($user->inRole('expert'))
                                      @if(isset($milestone['milestone_release_details']['status']) && $milestone['milestone_release_details']['status'] == '0')
                                        <a href="javascript:void(0)">
                                          <button class="awd_btn new-btn-class green-new-btn"> {{trans('milestones/project_milestones.text_requested')}} </button>
                                        </a>
                                      @elseif(isset($milestone['milestone_release_details']['status']) && $milestone['milestone_release_details']['status'] == '1')
                                        <a href="javascript:void(0)">
                                          <button class="black-border-btn" style="cursor: default;">{{trans('milestones/project_milestones.text_approved')}} </button>
                                        </a>
                                      @elseif(isset($milestone['milestone_release_details']['status']) && $milestone['milestone_release_details']['status'] == '2')
                                        <a href="javascript:void(0);">
                                          <button class="black-btn active-green"><i class="fa fa-check-circle" aria-hidden="true"></i> {{trans('milestones/project_milestones.text_paid')}} </button>
                                        </a>
                                        <a href="{{$module_url_path}}/invoice/{{base64_encode($milestone['id'])}}">
                                          <button class="black-border-btn">{{trans('milestones/project_milestones.text_invoice')}} </button>
                                        </a>
                                      @elseif(isset($milestone['project_details']['project_status']) && $milestone['project_details']['project_status'] != '5')
                                        <a onclick="confirmRelease(this)" data-currency="{{$milestone['project_details']['project_currency_code']}}" data-release-id="{{base64_encode($milestone['id'])}}">
                                          <button class="black-btn">{{trans('milestones/project_milestones.text_release_request')}}</button>
                                        </a>
                                      @endif
                                    @endif 
                                @else
                                    @if($user->inRole('client') && isset($milestone['project_details']['project_status']) && $milestone['project_details']['project_status']!='5')
                                    <span><b>{{trans('milestones/project_milestones.text_unsuccessful_payment')}}</b></span>
                                    <br><br>
                                    <a href="{{ $module_url_path }}/milestones/delete/{{base64_encode($milestone['id'])}}">
                                      <button class="release-btn-red">{{trans('milestones/project_milestones.text_delete')}} </button>
                                    </a>
                                    @endif  
                                @endif
                                @if(isset($milestone['milestone_release_details']) && $milestone['milestone_release_details'] != NULL)    
                                    @if($user->inRole('client'))
                                      @if(isset($milestone['milestone_release_details']['status']) && $milestone['milestone_release_details']['status'] == '0')  
                                        <span >{{trans('milestones/project_milestones.text_approve_request_received_by_expert')}}</span>
                                        <br/>
                                        <br/>
                                        <a onclick="confirmApprove(this)" data-release-details-id="{{base64_encode($milestone['milestone_release_details']['id'])}}">
                                          <button class="awd_btn new-btn-class in-progress-btn"><font color="white">{{trans('milestones/project_milestones.text_approve')}}</font></button>
                                        </a>  

                                        <a onclick="confirmNotApprove(this)" data-release-details-id="{{base64_encode($milestone['milestone_release_details']['id'])}}">
                                           <button class="not-approved-btn">{{trans('milestones/project_milestones.text_reject_request')}}</button>
                                        </a> 

                                      @elseif(isset($milestone['milestone_release_details']['status']) && $milestone['milestone_release_details']['status'] == '1')  
                                        <a href="javascript:void(0)">
                                           <button class="black-btn new-btn-class" style="cursor: default;">{{trans('milestones/project_milestones.text_approved')}} </button>
                                        </a>
                                      @elseif(isset($milestone['milestone_release_details']['status']) && $milestone['milestone_release_details']['status'] == '2')  
                                        <a href="javascript:void(0)">
                                           <button class="black-btn new-btn-class active-green"><font color="white"> <i class="fa fa-check-circle" aria-hidden="true"></i> {{trans('milestones/project_milestones.text_paid')}}</font> </button>
                                        </a>
                                      @endif
                                    @endif
                                @elseif($user->inRole('client') && (isset($milestone['transaction_details']) && isset($milestone['transaction_details']['payment_status']) && ($milestone['transaction_details']['payment_status']==1 || $milestone['transaction_details']['payment_status']==2) ))
                                  <a href="javascript:void(0);">
                                    <button class="awd_btn new-btn-class in-progress-btn ">
                                    <font color="white">{{trans('milestones/project_milestones.text_inprogress')}} </font></button>
                                  </a>
                                  <a href="#refund_request" 
                                     data-keyboard="false" 
                                     data-backdrop="static" 
                                     data-toggle="modal" 
                                     id="refund-request-model" 
                                     data-milestone="{{isset($milestone['milestone_no']) ?  $milestone['milestone_no']:'0'}}" 
                                     data-expert_id="{{isset($milestone['expert_user_id']) ?  $milestone['expert_user_id']:'0'}}"
                                     data-milestone-cost="{{isset($milestone['cost']) ?  $milestone['cost']:'0'}}" 
                                     data-project-id="{{isset($milestone['project_id'])?$milestone['project_id']:'0'}}" 
                                     data-currency-code="{{ isset($milestone['project_details']['project_currency_code']) ? $milestone['project_details']['project_currency_code'] : 'USD' }}"
                                     data-milestone-id="{{isset($milestone['id'])?$milestone['id']:'0'}}">
                                    <button class="awd_btn new-btn-class  request-refund-btn" >
                                    <font color="white"> {{trans('milestones/project_milestones.text_refund_request')}}</font></button>
                                  </a>
                                @endif
                          @endif      
                    </div>
                 </div>
              @endforeach
             </div> 
            @else
             <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;"> 
               <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                  <div class="head_grn">{{trans('milestones/project_milestones.text_title')}}</div>
                  <div class="search-grey-bx">
                    <div class="milestone-title">
                      <a href="{{ $module_url_path }}/details/{{ $enc_project_id }}"><span>{{isset($milestone_project_name)?''.$milestone_project_name.'':''}}</span></a>
                    </div>
                    <br/>
                    <div class="clearfix"></div> 
                    <!-- Wallet Details -->
                    @if(isset($mangopay_wallet_details) && $mangopay_wallet_details !="" && !empty($mangopay_wallet_details))
                    <div class="col-sm-12 col-md-6 col-lg-6 col-padding-setting" style="padding-left:0px; margin-bottom: 15px;">
                      <div class="dash-user-details milestone-wallet">
                          <div class="title"><i class="fa fa-money" aria-hidden="true"></i> Wallet </div>
                          <div class="user-details-section">
                              <table class="table" style="margin-bottom:15px">
                                <tbody>     
                                  <tr>
                                    <td>
                                      <span class="card-id"> {{ isset($mangopay_wallet_details->Id)? $mangopay_wallet_details->Id:''}} </span>
                                    </td>
                                    <td>
                                      <div class="description" style="font-size: 11px;">{{isset($mangopay_wallet_details->Description)? $mangopay_wallet_details->Description:''}}</div>
                                    </td>
                                    <td style="font-size: 17px;width: 120px;">
                                      <div class="high-text-up">
                                        <span>
                                          {{ isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:''}}
                                        </span>
                                        <span class="mp-amount">
                                          {{ isset($mangopay_wallet_details->Balance->Amount)? $mangopay_wallet_details->Balance->Amount/100:'0'}}
                                        </span>
                                       
                                      </div>
                                      <div class="small-text-down" style="font-size: 10px;">
                                        money balance
                                      </div>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                          </div>
                      </div>
                    </div>
                    @endif  

                    @if(isset($user) && $user->inRole('client'))
                    <?php
                        $arr_profile_data = sidebar_information($arr_bid_details['expert_user_id']);
                    ?>

                    <div class="col-sm-12 col-md-6 col-lg-6 col-padding-setting" style="padding-right:0px; margin-bottom: 15px;">
                      <div class="dash-user-details">
                          <div class="title">Bid Proposal </div>
                          <div class="user-details-section">
                              <div class="mck-row">      
                                  <div class="listin-pro-image">
                                    @if(isset($arr_bid_details['expert_details']['is_online']) && $arr_bid_details['expert_details']['is_online']!='' && $arr_bid_details['expert_details']['is_online']=='1')
                                      <div class="user-online-round"></div>
                                    @else
                                      <div class="user-offline-round"></div>
                                    @endif

                                    @if(isset($arr_bid_details['expert_details']['role_info']['profile_image']) && $arr_bid_details['expert_details']['role_info']['profile_image'] != ""  && file_exists('public/uploads/front/profile/'.$arr_bid_details['expert_details']['role_info']['profile_image']) ) 
                                      <div class="going-pro">
                                      <img src="{{ url('/public/uploads/front/profile/').'/'.$arr_bid_details['expert_details']['role_info']['profile_image']}}" alt="pic"/>
                                      </div>
                                    @else
                                      <div class="going-pro">
                                      <img src="{{  url('/public/uploads/front/profile/default_profile_image.png')}}" alt="pic"/>

                                      </div>
                                    @endif
                                  </div> 

                                  <div class="user-discription-content-main"> 
                                      <div class="user-discription-content-section">
                                          <a href="javascript:void(0)" class="profile-name-section-block">
                                              {{isset($arr_bid_details['expert_details']['role_info']['first_name'])?$arr_bid_details['expert_details']['role_info']['first_name']:'Unknown user'}} 
                                              @if(isset($arr_bid_details['expert_details']['role_info']['last_name']) && $arr_bid_details['expert_details']['role_info']['last_name'] !="") @php echo substr($arr_bid_details['expert_details']['role_info']['last_name'],0,1).'.'; @endphp @endif
                                          </a>
                                          <div class="place-time-section">
                                            @if(isset($arr_profile_data['last_login_duration']) && $arr_profile_data['last_login_duration'] == 'Active')
                                            <span class="pull-left" title="{{isset($arr_profile_data['expert_timezone'])?$arr_profile_data['expert_timezone']:'Not Specify'}}">
                                              <span style="color:#4D4D4D;font-size: 14px;text-transform:none;cursor:text;">
                                                <span class="flag-image"> 
                                                  @if(isset($arr_profile_data['user_country_flag'])) 
                                                  @php 
                                                  echo $arr_profile_data['user_country_flag']; 
                                                  @endphp 
                                                  @elseif(isset($arr_profile_data['user_country'])) 
                                                  @php 
                                                  echo $arr_profile_data['user_country']; 
                                                  @endphp 
                                                  @else 
                                                  @php 
                                                  echo 'Not Specify'; 
                                                  @endphp 
                                                  @endif 
                                                </span>
                                              {{-- trans('common/_expert_details.text_local_time') --}}  {{isset($arr_profile_data['expert_timezone_without_date'])?$arr_profile_data['expert_timezone_without_date']:'Not Specify'}}</span>
                                            </span>
                                            @else
                                            <span class="flag-image"> 
                                              @if(isset($arr_profile_data['user_country_flag'])) 
                                              @php 
                                              echo $arr_profile_data['user_country_flag']; 
                                              @endphp 
                                              @elseif(isset($arr_profile_data['user_country'])) 
                                              @php 
                                              echo $arr_profile_data['user_country']; 
                                              @endphp 
                                              @else 
                                              @php 
                                              echo 'Not Specify'; 
                                              @endphp 
                                              @endif 
                                            </span>   
                                              <span class="date-info">
                                             {{ isset($arr_profile_data['last_login_duration'])?$arr_profile_data['last_login_duration']:'Not Specify' }}  
                                                  </span>
                                            @endif
                                          </div>
                                      </div>

                                    <a href="#" class="applozic-launcher" data-mck-id="{{env('applozicUserIdPrefix')}}{{$arr_bid_details['expert_user_id']}}" data-mck-name="{{isset($arr_bid_details['expert_details']['role_info']['first_name'])?$arr_bid_details['expert_details']['role_info']['first_name']:'Unknown user'}} 
                                    @if(isset($arr_bid_details['expert_details']['role_info']['last_name']) && $arr_bid_details['expert_details']['role_info']['last_name'] !="") @php echo substr($arr_bid_details['expert_details']['role_info']['last_name'],0,1).'.'; @endphp @endif" ><img style="width: 25px;" src="{{url('/public')}}/front/images/comment-icon.png" alt="" /></a>


                                  </div>
                                  <div class="clearfix"></div>
                              </div>

                              <div class="projrct-prce1">
                                   @if(isset($arr_bid_details['project_currency']) && $arr_bid_details['project_currency']=="$")
                                    <span>
                                      <img src="{{url('/public')}}/front/images/doller-img.png" alt="img"/> 
                                    </span> <span>{{$arr_bid_details['project_currency_code']}}
                                    {{isset($arr_bid_details['project_bid_info']['bid_cost'])?$arr_bid_details['project_bid_info']['bid_cost']:0}}</span>
                                    @else
                                    <span>
                                      <img src="{{url('/public')}}/front/images/doller-img-new-1.png" alt="img"/> 
                                    </span> <span>{{$arr_bid_details['project_currency_code']}}
                                    {{isset($arr_bid_details['project_bid_info']['bid_cost'])?$arr_bid_details['project_bid_info']['bid_cost']:0}}</span>
                                    @endif
                              </div>
                              <div class="projrct-prce1">
                                  <span>
                                      <i class="fa fa-clock-o"></i>
                                  </span> 
                                  <span>
                                     {{isset($arr_bid_details['project_bid_info']['bid_estimated_duration'])?$arr_bid_details['project_bid_info']['bid_estimated_duration']:0}} Days
                                  </span>
                              </div>
                          </div>
                      </div>
                    </div>
                    @endif
                    <!-- End Wallet Details -->
                    <br/>
                    <div class="clearfix"></div>
                    <div class="alert alert-info">
                      <img style="height:21px; margin-top:-4px;" src="{{url('/public')}}/front/images/information-icon-th.png"> 

                      {{trans('milestones/project_milestones.text_no_record_found_1')}} {{isset($arr_bid_details['project_currency'])?$arr_bid_details['project_currency']:'$'}}  {{isset($arr_bid_details['project_bid_info']['bid_cost'])?number_format($arr_bid_details['project_bid_info']['bid_cost']):0}} {{trans('milestones/project_milestones.text_no_record_found_2')}}
                    </div> 
                  </div>
                </td>
             </tr>
            @endif   
            <!-- Pagination Links -->
            @include('front.common.pagination_view', ['paginator' => $arr_pagination])
            <!-- Pagination Links -->
           <?php 
            //dd($arr_bid_details);
            $project_id       = $client_user_id = $project_status = "";
            $project_id       = \Request::segment(4);
            $client_user_id   = base64_encode($user->id);
            $obj_project      = \App\Models\ProjectpostModel::where('id','=',base64_decode($project_id))->first(['id','project_name','project_status','expert_user_id']);
            if($obj_project){ 
              $project_status = isset($obj_project->project_status) ? $obj_project->project_status:'';
            } 
           ?>
           @if( isset($user) && $user != FALSE && $user->inRole('client') && $project_status == "4" ) 
            <div class="search-grey-bx create-milestone">
              <form id="frm_add_milestone" method="post"  action="{{url('/')}}/client/projects/store_milestone">
                   {{ csrf_field() }}
                   <div class="subm-text" style="color: #2d2d2d;">{{trans('client/projects/project_details.text_ceate_milestone')}}</div>
                   <div class="row">
                      <input type="hidden" readonly="" name="project_id" value="{{$project_id}}" ></input> 
                      <input type="hidden" readonly="" name="client_user_id" value="{{$client_user_id}}" ></input>     
                      <input type="hidden" readonly="" name="expert_user_id" value="{{isset($obj_project->expert_user_id)?$obj_project->expert_user_id:''}}" ></input>     
                      <input type="hidden" readonly="" name="project_currency" value="{{isset($arr_bid_details['project_currency_code'])?$arr_bid_details['project_currency_code']:''}}" ></input>     
                      <div class="col-sm-12 col-md-12 col-lg-9">
                         <div class="user-bx">
                            <div class="frm-nm">{{trans('client/projects/project_details.title')}}<i style="color: red;">*</i></div>
                            <input type="text" name="title" style="max-width:100%;" data-rule-required="true" placeholder="{{trans('client/projects/project_details.entry_milestone_title')}}" class="clint-input valid" />
                            <span class="error">{{ $errors->first('title') }}</span>
                         </div>
                      </div>
                      <div class="col-sm-12 col-md-12 col-lg-3">
                         <div class="user-bx">
                            <div class="frm-nm">{{trans('client/projects/project_details.amount')}} ({{ isset($milestone_project_currency_code) ? $milestone_project_currency_code : '' }}) <i style="color: red;">*</i></div>
                            <input type="text" id="project_milestone_amount" name="amount" style="max-width:100%;" class="clint-input valid char_restrict space_restrict" data-rule-required="true" placeholder="{{trans('client/projects/project_details.entry_milestone_amount')}}" data-rule-min="1" data-rule-max="25000"/>
                            <span class="error">{{ $errors->first('amount') }}</span>
                         </div>
                      </div>
                      <div class="clr"></div>
                      <div class="col-sm-12 col-md-12 col-lg-12">
                         <div class="user-bx">
                            <div class="frm-nm">{{trans('client/projects/project_details.description')}}<i style="color: red;">*</i></div>
                            <textarea style="max-width:100%;height: 125px;"  name="description" data-rule-required="true" class="clint-input valid" placeholder="{{trans('client/projects/project_details.entry_milestone_description')}}"/></textarea>
                            <span class="error">{{ $errors->first('description') }}</span>
                         </div>
                      </div>
                      <div class="clr"></div>
                      <div class="col-sm-12 col-md-12 col-lg-12">
                         <div class="user-bx">
                            <div class="frm-nm">{{trans('client/projects/project_details.entry_milestone_due_date')}}<i style="color: red;">*</i></div>
                              <input data-rule-required="true"  class="datepicker-start box-de" placeholder="{{trans('client/projects/project_details.entry_milestone_due_date')}}" id="due_date" name="due_date" type="text" data-show-preview="false" value="{{old('due_date')}}">
                            <span class="error">{{ $errors->first('due_date') }}</span>
                         </div>
                      </div>
                      <div class="clr"></div>
                      
                      <div id="payment_info_section" style="display: none;">
                        
                        <div class="col-sm-12 col-md-12 col-lg-12">
                           <div class="user-bx">
                              <div class="frm-nm">Milestone Amount: <span id="project_milestone_amount_span"></span></div>
                           </div>
                        </div>
                        <div class="clr"></div>

                        <div class="col-sm-12 col-md-12 col-lg-12">
                           <div class="user-bx">
                              <div class="frm-nm">Wallet Balance: <span id="project_wallet_balance_span"></span></div>
                           </div>
                        </div>
                        <div class="clr"></div>

                        <div class="col-sm-12 col-md-12 col-lg-12">
                           <div class="user-bx">
                              <div class="frm-nm">Sub Total: <span id="project_sub_total_span"></span></div>
                           </div>
                        </div>
                        <div class="clr"></div>

                        <div class="col-sm-12 col-md-12 col-lg-12">
                           <div class="user-bx">
                              <div class="frm-nm">Service Charges: <span id="project_service_charges_span"></span></div>
                           </div>
                        </div>
                        <div class="clr"></div>

                        <div class="col-sm-12 col-md-12 col-lg-12">
                           <div class="user-bx">
                              <div class="frm-nm">Total: <span id="project_total_amount_span"></span></div>
                           </div>
                        </div>
                        <div class="clr"></div>


                      </div>
                      <br/>
                      <div class="col-sm-3 col-md-3 col-lg-3">
                         <button type="submit" class="black-btn" style="float:none;">{{trans('client/projects/project_details.add')}}</button>
                      </div>

                   </div>
                </form>
            </div>
           @endif
      </div>
    </div>


<!-- refund model -->
<div class="modal fade invite-member-modal" id="refund_request" role="dialog">
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <h4 style="margin-left: 130px;">{{trans('common/wallet/text.text_send_refund_request')}}</h4>
             </div>
             <form action="{{ $module_url_path }}/refund_requests" method="post" id="refund-request">
               {{csrf_field()}}
               <div class="invite-form">
                    <span>{{trans('common/wallet/text.text_milestone')}}  : <span class="ref_milestone">  </span> </span>
                    <div class="clearfix"></div>
                    <span>{{trans('common/wallet/text.text_refund_amount')}}  : <span class="ref_milestone_cost">0</span> <span class="ref_milestone_currency">{{config('app.project_currency.$')}}</span></span>
                    <div class="clearfix"></div>
                    <hr>
                    <input type="hidden" name="ref_milestone_id" id="ref_milestone_id" value="0" placeholder="Milestone id">
                    <input type="hidden" name="ref_project_id" id="ref_project_id" value="0" Placeholder="Project id">
                    <input type="hidden" name="ref_expert_id" id="ref_expert_id" value="0" Placeholder="Expert id">
                    <div class="user-box">
                       <div class="input-name">
                          <textarea type="text" class="clint-input" data-rule-required="true" name="refunc_request_reasn" id="refunc_request_reasn" placeholder="Enter refund reason"></textarea>
                       </div>
                    </div>
                    <button type="submit" id="send-refund-request" class="black-btn">{{trans('common/wallet/text.text_send')}}</button>
               </div>
             </form>
          </div>
       </div>
    </div>
</div>

<!-- end refund model -->
<script type="text/javascript">
  $('#frm_add_milestone').validate();  
  $('#refund-request').validate();  
</script>
<link rel="stylesheet" type="text/css"  href="{{url('/public')}}/front/css/jquery-ui.css"/>
<script src="{{url('/public')}}/front/js/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript">

  var milestone_project_currency_code = '{{ isset($milestone_project_currency_code) ? $milestone_project_currency_code : '' }}';
  var user_wallet_balance_amount      = '{{ isset($user_wallet_balance_amount) ? floatval($user_wallet_balance_amount) : 0 }}';
  var min_amount                      = '{{ isset($arr_currency_data['deposite']['min_amount'])?floatval($arr_currency_data['deposite']['min_amount']):0 }}';
  var max_amount                      = '{{ isset($arr_currency_data['deposite']['max_amount'])?floatval($arr_currency_data['deposite']['max_amount']):0 }}';
  var min_charge                      = '{{ isset($arr_currency_data['deposite']['min_amount_charge'])?str_replace(',', '.', $arr_currency_data['deposite']['min_amount_charge']):0 }}';
  var max_charge                      = '{{ isset($arr_currency_data['deposite']['max_amount_charge'])?str_replace(',', '.', $arr_currency_data['deposite']['max_amount_charge']):0 }}';

  $('#project_milestone_amount').keyup(function() {
    $('#project_milestone_amount_span').html('');
    $('#project_wallet_balance_span').html('');
    $('#project_sub_total_span').html('');
    $('#project_service_charges_span').html('');
    $('#project_total_amount_span').html('');

    var project_milestone_amount = 0
    var project_service_charges  = 0;
    var project_sub_total_amount = 0;
    var project_total_amount     = 0;
    user_wallet_balance_amount = parseFloat(user_wallet_balance_amount);
    
    var project_milestone_amount = $(this).val();
    if(project_milestone_amount !='' && parseFloat(project_milestone_amount) > parseFloat(user_wallet_balance_amount) ) {
      
      project_milestone_amount = parseFloat(project_milestone_amount);
      
      project_sub_total_amount  = project_milestone_amount - user_wallet_balance_amount;
      project_sub_total_amount = parseFloat(project_sub_total_amount);

      if(project_sub_total_amount>min_amount) {
          project_service_charges = project_sub_total_amount * parseFloat(max_charge)/100;
      } else {
          project_service_charges = parseFloat(min_charge);
      }

      project_total_amount = project_sub_total_amount + project_service_charges;

      $('#project_milestone_amount_span').html( milestone_project_currency_code+ ' ' + project_milestone_amount.toFixed(2) );
      $('#project_wallet_balance_span').html( milestone_project_currency_code+ ' ' + user_wallet_balance_amount.toFixed(2) );
      $('#project_sub_total_span').html( milestone_project_currency_code+ ' ' + project_sub_total_amount.toFixed(2) );
      $('#project_service_charges_span').html( milestone_project_currency_code+ ' ' + project_service_charges.toFixed(2) );
      $('#project_total_amount_span').html( milestone_project_currency_code+ ' ' + project_total_amount.toFixed(2) );

      $('#payment_info_section').show();
      return true;
    }
    $('#payment_info_section').hide();
    return true;
  });

$(document).ready(function(){
  $("#due_date" ).datepicker({
    dateFormat: 'yy-mm-dd',
    'startDate': new Date(),
    minDate: 'today',
  });
  $(document).on('click', '.add-btn', function (e) {
      var check_active = $(this).next('.money-drop').attr('class');
      if (check_active.indexOf("active") >= 0){
        $(this).next('.money-drop').removeClass('active');
      } else {
        $('.money-drop').removeClass('active');
        $(this).next('.money-drop').addClass('active');
      }
  });
  $(document).on('click','#refund-request-model',function(){
     var project_id      = $(this).data('project-id');
     var milestone_id    = $(this).data('milestone-id');
     var expert_id       = $(this).data('expert_id');
     var milestone       = $(this).data('milestone');
     var milestone_cost  = $(this).data('milestone-cost');
     var currency_code  = $(this).data('currency-code') || 'USD';
     $('#ref_project_id').val(project_id);
     $('#ref_milestone_id').val(milestone_id);
     $('.ref_milestone').html(milestone);
     $('.ref_milestone_cost').html(milestone_cost);
     $('.ref_milestone_currency').html(currency_code);
     $('#ref_expert_id').val(expert_id);
  }); 
});
</script>
<script type="text/javascript">
    function confirmRelease(ref) {
      swal({
              title: "Are you sure? You want to send release request ?",
              text: "",
              icon: "warning",
              
              dangerMode: true,
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
             },
             function(isConfirm){

               if (isConfirm){
                  var release_id = $(ref).attr('data-release-id');
                  var currency = $(ref).attr('data-currency');
                 window.location.href = '{{ $module_url_path }}/milestones/release/'+release_id+'/'+currency;
                    return true;

                } else {
                    swal.close();
                  return false;
                }
             });
    }

    function confirmApprove(ref) {
      swal({
              title: "Are you sure? You want to approve and release milestone ?",
              text: "",
              icon: "warning",
              
              dangerMode: true,
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
             },
             function(isConfirm){

               if (isConfirm){
                  var release_details_id = $(ref).attr('data-release-details-id');
                  var currency = $(ref).attr('data-currency');
                 window.location.href = "{{ $module_url_path }}/milestones/approve/"+release_details_id+"/{{isset($arr_bid_details['project_currency_code'])?$arr_bid_details['project_currency_code']:''}}";
                    return true;
                } else {
                    swal.close();
                  return false;
                }
             });
    }

    function confirmNotApprove(ref) {
      swal({
              title: "Are you sure? You want to Not approved milestone request?",
              text: "",
              icon: "warning",
              
              dangerMode: true,
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
             },
             function(isConfirm){

               if (isConfirm){
                  var release_details_id = $(ref).attr('data-release-details-id');
                  
                 window.location.href = "{{ $module_url_path }}/milestones/not_approve/"+release_details_id;
                    return true;
                } else {
                    swal.close();
                  return false;
                }
             });
    }
</script>   
@stop
