@extends('client.layout.master')                
@section('main_content')

<!-- <div class="middle-container"> -->
      <div class="container">
         <!-- <br/> -->
         <div class="row">

         <div class="col-sm-7 col-md-8 col-lg-9">
         @include('front.layout._operation_status')
         
         @if( isset($arr_milestone) && count($arr_milestone)>0 )
         
            <div class="search-grey-bx">
            
               <div class="ongonig-project-section">
                  <div class="dispute-head">{{ isset($arr_milestone['project_details']['project_name']) ? $arr_milestone['project_details']['project_name']:''  }} 
                  </div>
               </div>

               <form id="frm_add_milestone" method="post"  action="{{ $module_url_path }}/update_milestone">
                  {{ csrf_field() }}
                  <div class="subm-text">{{ trans('milestones/update_project_milestone.text_milestones') }}</div>
                  <div class="row">
                     <input type="hidden" readonly="" name="milestone_id"  value="{{isset($arr_milestone['id'])?base64_encode($arr_milestone['id']):''}}" ></input> 
                     
                     <div class="col-sm-12 col-md-12 col-lg-9">
                        <div class="user-bx">
                           <div class="frm-nm">{{ trans('milestones/update_project_milestone.text_title') }}<i style="color: red;">*</i></div>
                           <input type="text" name="title"  data-rule-required="true"  value="{{ $arr_milestone['title'] or '' }}" style="max-width:100%;" placeholder="{{ trans('milestones/update_project_milestone.text_milestones') }}" class="det-in"/>
                           <span class="error">{{ $errors->first('title') }}</span>
                        </div>
                     </div>
                     
                     <div class="col-sm-12 col-md-12 col-lg-3">
                        <div class="user-bx">
                           <div class="frm-nm">{{ trans('milestones/update_project_milestone.text_amount') }}<i style="color: red;">*</i></div>
                           <input type="text" name="amount"  data-rule-required="true" data-rule-number="true" data-rule-max="25000"  data-rule-min="1" value="{{ $arr_milestone['cost'] }}" style="max-width:100%;" class="det-in" placeholder="{{ trans('milestones/update_project_milestone.text_amount') }}" />
                           <span class="error">{{ $errors->first('amount') }}</span>
                        </div>   
                     </div>
                     
                     <div class="clr"></div>
                     
                     <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="user-bx">
                           <div class="frm-nm">{{ trans('milestones/update_project_milestone.text_description') }}<i style="color: red;">*</i></div>
                           <textarea style="max-width:100%;height: 150px;" data-rule-required="true" name="description" class="det-in" placeholder="{{ trans('milestones/update_project_milestone.text_description') }}" />{{ $arr_milestone['description'] }}</textarea>
                           <span class="error">{{ $errors->first('description') }}</span>
                        </div>   
                     </div>

                     <div class="clr"></div>
                     <br/>
                     
                     <div class="col-sm-2 col-md-2 col-lg-2">
                        <button type="submit" class="det-sub-btn" style="float:none;">{{ trans('milestones/update_project_milestone.text_update') }}</button>
                     </div>

                     <div class="col-sm-2 col-md-2 col-lg-2" style="margin-left: -35px;">
                          <a href="{{$module_url_path}}/milestones/{{base64_encode($arr_milestone['project_id'])}}" class="msg_btn det-sub-btn" style="text-align: center;">{{ trans('milestones/update_project_milestone.text_back') }}</a>
                     </div>

                  </div>
               </form>
            </div>

            @endif
             
      </div>
   </div>
</div>



<script type="text/javascript">

   $('#frm_add_milestone').validate();

</script>
@stop