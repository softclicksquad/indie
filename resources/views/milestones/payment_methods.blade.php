@extends('client.layout.master')                
@section('main_content')


<style type="text/css">
  .year option {
    width: 75px;
  }
  .month option {
    width: 70px;
  } 
</style>
<div class="col-sm-7 col-md-8 col-lg-9">
   <div class="right_side_section payment-section">
      <div class="head_grn">{{isset($page_title)?$page_title:''}}</div>
      @include('front.layout._operation_status')
      {{--dd($arr_milestone)--}}
      <div class="ongonig-project-section">
         <div class="dispute-head" style="font-size: 20px;">
            {{isset($arr_milestone['title'])?$arr_milestone['title']:''}}
         </div>
         <div class="det-divider"></div>
         <div class="project-title">
         </div>
         <div class="clr"></div>
         <br>
         <div class="project-list pro-list-ul">
            <ul>
               <li>
    				   <span>
    					   <i class="fa fa-calendar" aria-hidden="true"></i>
    				   </span>{{isset($arr_milestone['created_at'])?date('d M Y',strtotime($arr_milestone['created_at'])):''}} </li>
               <li style="background:none;">
               @if(isset($arr_milestone['project_details']['project_currency']) && $arr_milestone['project_details']['project_currency']=="$")
                <span class="projrct-prce"> <i class="fa fa-money" aria-hidden="true"></i>  
               {{$arr_milestone['project_details']['project_currency']}}
               @else
                <span class="projrct-prce"> <i class="fa fa-money" aria-hidden="true"></i>
               {{$arr_milestone['project_details']['project_currency']}}
               @endif {{isset($arr_milestone['cost_from_client'])?number_format($arr_milestone['cost_from_client'],2):''}}

               @if(isset($arr_milestone['cost_project_manager_commission']) && $arr_milestone['cost_project_manager_commission']!=0)
                  ( @if(isset($arr_milestone['project_details']['project_currency']) && $arr_milestone['project_details']['project_currency']=="$")
                  {{$arr_milestone['project_details']['project_currency']}}
                  @else
                  {{$arr_milestone['project_details']['project_currency']}}
                  @endif {{isset($arr_milestone['cost'])?number_format($arr_milestone['cost'],2):'0'}} +
                  @if(isset($arr_milestone['project_details']['project_currency']) && $arr_milestone['project_details']['project_currency']=="$")
                  {{$arr_milestone['project_details']['project_currency']}}
                  @else
                  {{$arr_milestone['project_details']['project_currency']}}
                  @endif{{isset($arr_milestone['cost_project_manager_commission'])?number_format($arr_milestone['cost_project_manager_commission'],2):''}} ({{trans('milestones/payment_methods.text_project_manager_commision')}})
                  )
               @endif
               </span>
               </li>
            </ul>
         </div>
         <div class="clr"></div>
         <br/>
         <form id="validate-form-payment" name="validate-form-payment" method="POST" action="{{url('/payment/paynow')}}">
            {{ csrf_field() }}
            <!-- payment_obj_id is id for which user is going to pay like subscription pack or milestone -->
            <input type="hidden" name="payment_obj_id" id="payment_obj_id" value=" {{isset($arr_milestone['id'])?$arr_milestone['id']:'0'}}">
            <!-- transaction_type is type for which user making this transaction like subscription pack or milestone -->
            <input type="hidden" name="transaction_type" id="transaction_type" value="2">
            <div class="paypa_radio">
               <div class="radio_area regi" onclick="javascript: return setOption('paypal');">
                  <input type="radio" name="payment_method" id="radio_paypal" class="css-checkbox" checked="" value="1">
                  <label for="radio_paypal" class="css-label radGroup1"><img src="{{url('/public')}}/front/images/paypal-payment.png" alt="paypal payment method"/></label>
               </div>
               <div class="radio_area regi" onclick="javascript: return setOption('stripe');">
                  <input type="radio" name="payment_method" id="radio_stripe" class="css-checkbox" value="2" 
                  @if(old('payment_method')==2)
                  checked="" 
                  @endif
                  >
                  <label for="radio_stripe" class="css-label radGroup1"><img src="{{url('/public')}}/front/images/stripe2.png" alt="Stripe payment method" width="148"/></label>
               </div>
               <span class='error'>{{ $errors->first('payment_method') }}</span>
            </div>

             <br/>
            <div class="clr"></div>

            <div class="right_side_section" style="display:none;" id="section-card-details">
               <div class="head_grn">{{trans('milestones/payment_methods.text_card_details')}}</div>
               <div class="row">
                  <div class="change-pwd-form">
                     <div class="col-sm-12  col-md-12 col-lg-12">
                     <div class="col-sm-12  col-md-8 col-lg-8">
                        <div class="user-box">
                           <div class="p-control-label">{{trans('milestones/payment_methods.text_card_number')}}<span>*</span></div>
                           <div class="input-name"><input type="text" class="clint-input" placeholder="{{trans('milestones/payment_methods.entry_card_number')}}" name="cardNumber" id="cardNumber" required autofocus value="{{old('cardNumber')}}"/></div>
                           <span class='error'>{{ $errors->first('cardNumber') }}</span>
                        </div>
                     </div>
                     </div>
                     <div class="col-sm-12  col-md-12 col-lg-12">
                        <div class="col-sm-12  col-md-6 col-lg-5">
                        <div class="user-box">
                           <div class="p-control-label">{{trans('milestones/payment_methods.text_card_expiration_month_year')}}<span>*</span></div>
                           <div class="input-name">
                              <div class="row">
                                 <div class="col-xs-3 col-sm-3 col-md-3 col-lg-4 droup-select month" style="margin-left:14px;margin-right:14px;width: 85px;padding-right: 0px;padding-left: 0px;" >
                                  <select class="droup" name="cardExpiryMonth" id="cardExpiryMonth" data-rule-required="true">
                                     <option value="">{{trans('milestones/payment_methods.entry_mm')}}</option>
                                     @for($i = 1;$i <= 12;$i++)
                                      <option value="@if($i < 10){{0}}@endif{{$i}}" @if(old('cardExpiryMonth') == $i) selected="selected" @endif >@if($i < 10){{0}}@endif{{$i}}</option>
                                     @endfor
                                  </select>
                                 </div>
                                 <span class='error'>{{ $errors->first('cardExpiryMonth') }}</span>
                                 <div class="col-xs-3 col-sm-3 col-md-3 col-lg-5 p-l-r droup-select year" style="width: 105px;padding-right: 0px;padding-left: 0px;">
                                   <?php $year = date('Y'); ?>
                                    <select class="droup" name="cardExpiryYear" id="cardExpiryYear" data-rule-required="true" >
                                     <option value="">{{trans('milestones/payment_methods.entry_yyyy')}}</option>
                                     @for($j = $year ;$j <= $year + 25 ;$j++)
                                        <option value="{{$j}}" @if(old('cardExpiryYear') == $j) selected="selected" @endif >{{$j}}</option>
                                     @endfor
                                   </select>
                                   </div>
                                  <span class='error'>{{ $errors->first('cardExpiryYear') }}</span>
                              </div>
                           </div>
                        </div>
                        </div>
                        <div class="col-sm-12  col-md-6 col-lg-3">
                        <div class="user-box">
                           <div class="p-control-label">{{trans('milestones/payment_methods.text_card_cv_code')}}<span>*</span></div>
                           <div class="input-name"><input type="text" class="clint-input" placeholder="{{trans('milestones/payment_methods.entry_cv_code')}}" name="cardCVC" id="cardCVC" required value="{{old('cardCVC')}}" /></div>
                            <span class='error'>{{ $errors->first('cardCVC') }}</span>
                        </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <br/>
            <div class="clr"></div>
            <div class="right-side">
               <button class="normal-btn pull-right" onclick="showLoader()" value="submit" id="btn-form-payment-submit">{{trans('milestones/payment_methods.text_processed')}}</button>
            </div>
         </form>
         <div class="clr"></div>
      </div>
   </div>
</div>
<script type="text/javascript" src="{{url('/public')}}/assets/payment/jquery.payment.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript">
   $( document ).ready(function(){
      var is_stripe = jQuery('#radio_stripe').prop('checked');
       if (is_stripe==true){
         jQuery('#section-card-details').show();
       } else {
         jQuery('#section-card-details').hide();
       }
   });
   $('input[name=cardNumber]').payment('formatCardNumber');
   $('input[name=cardCVC]').payment('formatCardCVC');
   jQuery("#validate-form-payment").validate({
      errorElement: 'span',   
   });
   
   $('#validate-form-payment').submit(function (){
     /*jQuery('#btn-form-payment-submit').prop('disabled', true);*/
   });
   function setOption(opt) {
       if (opt=='stripe'){
         jQuery('#section-card-details').show();
         jQuery('#radio_stripe').prop('checked',true);
       } else {
         jQuery('#section-card-details').hide();
         jQuery('#radio_paypal').prop('checked',true);
       }
   }
   $('#cardExpiryYear').keypress(function(eve){
      if(eve.which == 8 || eve.which == 190) 
      {
        return true;
      } else if (eve.which != 8 && eve.which != 0 && (eve.which < 48 || eve.which > 57)) {
       eve.preventDefault();
      }
      if($('#cardExpiryYear').val().length>3) 
      {
         eve.preventDefault();
      }
   });

   $('#cardExpiryMonth').keypress(function(eve){
      if(eve.which == 8 || eve.which == 190){
        return true;
      }
      else if (eve.which != 8 && eve.which != 0 && (eve.which < 48 || eve.which > 57)) {
       eve.preventDefault();
      }

      if($('#cardExpiryMonth').val().length>1) 
      {
         eve.preventDefault();
      }
   });
   function showLoader()
   {
    if($('#radio_stripe').is(':checked'))
    {
      if($('#validate-form-payment').valid() == true)
      {
        showProcessingOverlay();
      }
    }
    else if($('#radio_paypal').is(':checked'))
    {
      showProcessingOverlay();
    }
   }
</script>
 <script type="text/javascript">
  (function (global, $) {
    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";
        global.setTimeout(function () 
        {
            global.location.href += "!";
        }, 50);
    };

    global.onhashchange = function () {
        if (global.location.hash != _hash) {
            global.location.hash = _hash;
        }
    };

    global.onload = function () 
    {
        noBackPlease();
        // disables backspace on page except on input fields and textarea..
        $(document.body).keydown(function (e) {
            var elm = e.target.nodeName.toLowerCase();
            if (e.which == 8 && (elm !== 'input' && elm  !== 'textarea')) 
            {
                e.preventDefault();
            }
            // stopping event bubbling up the DOM tree..
            e.stopPropagation();
        });
    };
  })(window, jQuery || window.jQuery);
 </script>
@stop