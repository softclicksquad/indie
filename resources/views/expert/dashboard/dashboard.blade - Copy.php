@extends('expert.layout.master')                
@section('main_content')

<div class="col-sm-7 col-md-8 col-lg-9">
  @include('front.layout._operation_status')
  <div class="dashboard-box">
   <div class="head_grn">{{ trans('dashboard/dashboard.welcome_to_dashboard') }}</div>
   <div class="">
     <div class="row">
       <div class="col-sm-12 col-md-6 col-lg-6">
         <a href="{{ $module_url_path }}/projects/ongoing" class="pink-bx ">
           <span class="dash-top-block">
             <div class="dash-icon">
              <img src="{{url('/public')}}/front/images/dash-icon1.png" class="img-responsive" alt="Notpad icon"/>
            </div>
            <div class="dash-content">
              <span class="dash-count">{{ isset($count_ongoing_projects)? $count_ongoing_projects:'0'}}</span>
              <p>{{ trans('dashboard/dashboard.ongoing_projects') }}</p>
            </div>
            <span class="clearfix"></span>
          </span>
        </a>
      </div>
      <div class="col-sm-12 col-md-6 col-lg-6">
        <a href="{{ $module_url_path }}/projects/completed" class="pink-bx">
          <span class="dash-top-block">
           <div class="dash-icon">
            <img src="{{url('/public')}}/front/images/dash-icon2.png" class="img-responsive" alt="msgs icon"/>
          </div>
          <div class="dash-content" >
            <span class="dash-count">{{ isset($count_completed_projects)? $count_completed_projects:'0'}}</span>
            <p>{{ trans('dashboard/dashboard.completed_projects') }}</p>
          </div>
          <span class="clearfix"></span>
        </span>
      </a>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-6">
     <a href="{{ $module_url_path }}/projects/awarded" class="pink-bx">
      <span class="dash-top-block">
       <div class="dash-icon">
        <img src="{{url('/public')}}/front/images/dash-icon3.png" class="img-responsive" alt="Email icon"/>
      </div>
      <div class="dash-content">
        <span class="dash-count">{{ isset($count_awarded_projects)? $count_awarded_projects:'0'}}</span>
        <p>{{ trans('dashboard/dashboard.awarded_projects') }}</p>
      </div>
      <span class="clearfix"></span>
    </span>
  </a>  
</div>
<div class="col-sm-12 col-md-6 col-lg-6">
 <a href="{{ $module_url_path }}/projects/applied" class="pink-bx">
  <span class="dash-top-block">
    <div class="dash-icon">
      <img src="{{url('/public')}}/front/images/dash-icon7.png" class="img-responsive" alt="star icon"/>
    </div>
    <div class="dash-content">
      <span class="dash-count">{{ isset($count_applied_projects)? $count_applied_projects:'0'}}</span>
      <p>{{ trans('dashboard/dashboard.applied_projects') }}</p>
    </div>
    <span class="clearfix"></span>
  </span>
</a>
</div>
@php
$user = Sentinel::check();
$sidebar_information = sidebar_information($user->id);
@endphp
<div class="col-sm-12 col-md-6 col-lg-6">
  <a href="{{ $module_url_path }}/contest/applied-contest" class="pink-bx">
    <span class="dash-top-block">
     <div class="dash-icon">
      <img src="{{url('/public')}}/front/images/contest.png" class="img-responsive" alt="email icon"/>
    </div>
    <div class="dash-content">
      <span class="dash-count">{{$sidebar_information['contest_applied_count'] or '0'}}</span>
      <p>{{ trans('dashboard/dashboard.text_my_contests') }}</p>
    </div>
    <span class="clearfix"></span>
  </span>
</a>
</div>

{{-- <div class="col-sm-12 col-md-6 col-lg-6">
 <a href="{{ $module_url_path }}/inbox" class="pink-bx">
   <span class="dash-top-block">
    <div class="dash-icon">
      <img src="{{url('/public')}}/front/images/dash-icon5.png" class="img-responsive" alt="email icon"/>
    </div>
    <div class="dash-content">
      <span class="dash-count">{{ $arr_notification['unread_messages'] or '0' }}</span>
      <p>{{ trans('dashboard/dashboard.new_messages') }}</p>   
    </div>
    <span class="clearfix"></span>
  </span>
</a>
</div> --}}
<?php 
$user = Sentinel::check();
if($user)
{
  $user_id = $user->id;
  $arr_profile_data = [];
  $arr_profile_data = sidebar_information($user_id);
}
?>
<div class="col-sm-12 col-md-6 col-lg-6">
  <a href="{{ $module_url_path }}/projects/completed" class="pink-bx">
    <span class="dash-top-block">
     <div class="dash-icon">
      <img src="{{url('/public')}}/front/images/dash-icon6.png" class="img-responsive" alt="msgs icon"/>
    </div>
    <div class="dash-content">
      <span class="dash-count">@if(isset($arr_profile_data['average_rating']) && $arr_profile_data['average_rating'] != "" ) {{ $arr_profile_data['average_rating'] }}&nbsp;{{ trans('dashboard/dashboard.of_5') }} @else {{ trans('dashboard/dashboard.no_reviews_yet') }} @endif </span>
      <p>{{ trans('dashboard/dashboard.review_ratings') }}</p>
    </div>
    <span class="clearfix"></span>
  </span>
</a>
</div>
  @if(isset($arr_data_bal['balance']) && count($arr_data_bal['balance'])>0)
  @foreach($arr_data_bal['balance'] as $key=>$value)
  <div class="col-sm-12 col-md-6 col-lg-6">    
  <a href="{{ $module_url_path }}/wallet/dashboard" class="pink-bx marg-rgt">
    <span class="dash-top-block">
     <div class="dash-icon">
      <img src="{{url('/public')}}/front/images/archexpertdefault/wallet.png" class="img-responsive" alt="transactions-icns"/>
    </div>
    <div class="dash-content">
      <span class="dash-count"> {{isset($value)?number_format($value,2):''}} {{isset($arr_data_bal['currency'][$key])?$arr_data_bal['currency'][$key]:''}} </span>
      <p>{{trans('common/wallet/text.text_my_wallet_balance')}}</p>
    </div>
    <span class="clearfix"></span>
  </span>
</a>
</div>
@endforeach
@endif

</div>
</div>
</div>
</div>

@stop