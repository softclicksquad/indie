@extends('expert.layout.master')                
@section('main_content')

   <div class="col-sm-7 col-md-8 col-lg-9">
      <div class="right_side_section min-height">
       @include('front.layout._operation_status')

		  <div class="row">
		   @if(isset($arr_portfolio['data']) && sizeof($arr_portfolio['data'])>0)
		   @foreach($arr_portfolio['data'] as $portfolio)
			  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 res-po-class">
				  <div class="profile-imagae-container view view-first nwimg">
				      @if(isset($portfolio_resize_path) && $portfolio['file_name']!="")
				      <img src="{{ get_resized_image_path($portfolio['file_name'],$portfolio_resize_path.'/',250,370) }}"  class="img-responsive" alt="">
					  @endif
					  <div class="mask max-wth-top">
						  <a href="javascript:void(0)" class="info-edit info-delete"  onclick="javascript:return confirm_delete('{{url('/expert/delete/'.base64_encode($portfolio['id']))}}');" ><img src="{{url('/public')}}/front/images/cross-btn.png" alt="" /></a>
					  </div>
				  </div>
			  </div>
			@endforeach
			@else
			<br/>
           <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;" align="center">
              <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                 <div class="search-content-block">
                    <div class="search-head" style="color:rgba(45, 45, 45, 0.8);" align="center">
                       <!-- {{ trans('expert/portfolio/portfolio.text_no_images_found') }} -->
                    </div>
                 </div>
              </td>
           </tr>  
			@endif
			  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 res-po-class">
				  <div class="profile-imagae-container view view-first nwimg">
					  <img src="{{url('/public')}}/front/images/portfolio-bg.jpg" alt="" class="img-responsive"/>
					   <form action="{{url('/expert/update_portfolio')}}" method="POST" id="form-expert_portfolio" name="form-expert_portfolio" enctype="multipart/form-data" files ="true">
                        {{ csrf_field() }}
                        <div class="max-wdth-btn">
					      <div class="fileUpload btn btn-primary">
						  <span>{{ trans('expert/portfolio/portfolio.text_upload') }}</span>
						  <input type="file" id="portfolio_image" name="portfolio_image" class="upload" />
					      </div>
					      <div class="clr"></div>
					      </div>
					    </form>  
				  </div>
			  </div>
		  </div>
      
      <div style="max-width:815px; width: 100%; display: block;" id="myself_note" class="alert alert-info">
	 	  <strong>{{ trans('new_translations.info') }}!</strong> 
		 	{{ trans('new_translations.upload_correct_size_image_for_portfoilo')}}
	      </div>
      </div>
      <!--pagigation start here-->
        @include('front.common.pagination_view', ['paginator' => $arr_portfolio])
      <!--pagigation end here-->
   </div>
<script type="text/javascript">
    function confirm_delete(url) {
      alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
          if (e) {
              if(url != "" && url != undefined){
                showProcessingOverlay();
                window.location.href=url;
              }
              return true;
          } else {
              return false;
          }
      });
    }
	$('#portfolio_image').change(function(){
	   $("#form-expert_portfolio").submit();
	});
</script>
@stop
  