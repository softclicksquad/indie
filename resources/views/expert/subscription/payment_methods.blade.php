@extends('expert.layout.master')                
@section('main_content')
<style type="text/css">
  .droup-select option { width: auto; }
</style>

<div class="col-sm-7 col-md-8 col-lg-9">
   <div class="right_side_section payment-section">
      <div class="head_grn">{{ trans('expert/subscription/packs.text_payment_method') }}</div>
      @include('front.layout._operation_status')
      <div class="ongonig-project-section">
         <div class="dispute-head" style="font-size: 32px;">
            {{isset($arr_pack_details['pack_name'])?$arr_pack_details['pack_name']:''}}
         </div>

         @if($pre_pack_remaining_days > 0)
           <div class="det-divider"></div>
           <div class="pro-title">
              Previous subscription details :
           </div>
           <div class="row">
              <div class="col-sm-7 col-md-7 col-lg-7">
                 <div class="pro-title"><span>Previous pack remaining days :</span>{{ isset($pre_pack_remaining_days) ? intval($pre_pack_remaining_days) : 0 }} </div>
                 <div class="pro-title" title="${{$pre_pack_remaining_price}}"><span>Previous pack remaining price :
                 </span>{{$currency}} {{number_format($pre_pack_remaining_price,2)}}</div>
              </div>
           </div>
         @endif
         <!-- <div class="row" style="display:none">
            <div class="col-sm-7 col-md-7 col-lg-7">
               <div class="pro-title"> <span style="width:165px;">{{ trans('expert/subscription/packs.text_bids') }} :</span>{{isset($arr_pack_details['number_of_bids'])?$arr_pack_details['number_of_bids']:''}} </div>
               <div class="pro-title"><span style="width:165px;">{{ trans('expert/subscription/packs.text_topup_bid') }} :</span>{{isset($arr_pack_details['topup_bid_price'])?'$'.$arr_pack_details['topup_bid_price']:''}}</div>
            </div>
            <div class="col-sm-5 col-md-5 col-lg-5">
               <div class="pro-title"><span style="width:165px;">{{ trans('expert/subscription/packs.text_categories') }}: </span>{{isset($arr_pack_details['number_of_categories'])?$arr_pack_details['number_of_categories']:''}} </div>
               <div class="pro-title"><span style="width:165px;">{{ trans('expert/subscription/packs.text_skills') }}: </span>{{isset($arr_pack_details['number_of_skills'])?$arr_pack_details['number_of_skills']:''}}</div>
            </div>
            <div class="col-sm-7 col-md-7 col-lg-7">
               <div class="pro-title"> <span style="width:165px;">{{ trans('expert/subscription/packs.text_favorites_projects') }} :</span>{{isset($arr_pack_details['number_of_favorites_projects'])?$arr_pack_details['number_of_favorites_projects']:''}}  </div>
            </div>
         </div> -->
         <!-- <div class="row" style="display:none">
            <div class="col-sm-6 col-md-6 col-lg-6">
               <div class="pro-title"><span style="width:220px;">{{ trans('expert/subscription/packs.text_email') }} :</span>
                  @if(isset($arr_pack_details['allow_email']) && $arr_pack_details['allow_email']==1)
                  YES
                  @elseif(isset($arr_pack_details['allow_email']) && $arr_pack_details['allow_email']==0)
                  NO
                  @endif
               </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6">
               <div class="pro-title"><span style="width:220px;">{{ trans('expert/subscription/packs.text_chat') }}: </span>
                  @if(isset($arr_pack_details['allow_chat']) && $arr_pack_details['allow_chat']==1)
                  YES
                  @elseif(isset($arr_pack_details['allow_chat']) && $arr_pack_details['allow_chat']==0)
                  NO
                  @endif
               </div>
            </div>
         </div> -->
         <div class="det-divider"></div>
         <div class="pro-title">
            Current subscription details :
         </div>
         <div class="clr"></div>
         <br>
        
         @php $total_cost = '0'; @endphp
         <div class="project-list pro-list-ul">
            <ul>
               <li><span><i class="fa fa-calendar" aria-hidden="true"></i></span>{{date('d M Y')}} </li>
                
                @php
                  $prev_plan_type = $curr_plan_type = '';
                  if(isset($arr_pre_pack_details['type']) && $arr_pre_pack_details['type'] == 'MONTHLY') {
                    $prev_plan_type = '(for 1 month)';
                  } else if(isset($arr_pre_pack_details['type']) && $arr_pre_pack_details['type'] == 'YEARLY') {
                    $prev_plan_type = '(for 1 year)';
                  }

                  if(isset($arr_pack_details['type']) && $arr_pack_details['type'] == 'MONTHLY') {
                    $curr_plan_type = '(for 1 month)';
                  } else if(isset($arr_pack_details['type']) && $arr_pack_details['type'] == 'YEARLY') {
                    $curr_plan_type = '(for 1 year)';
                  }
                @endphp
                @if(isset($pre_pack_remaining_price) && $pre_pack_remaining_price > 0)

                  @php
                    if(isset($currency) && $currency!='')
                    {
                      $arr_currency = config('app.project_converted_currency');

                      if(isset($arr_currency) && count($arr_currency)>0)
                      {
                        $currency_symbol = $arr_currency["$currency"];
                      }
                      else
                      {
                        $currency_symbol = '$';
                      }

                      if($currency=='USD')
                      {
                        $pack_price = $arr_pack_details['pack_price'];
                        $new_price  = $arr_pack_details['pack_price']-$pre_pack_remaining_price;
                        $new_price  = isset($new_price)&&$new_price>0?$new_price:'0';

                      }
                      else
                      {
                        $currency = strtolower($currency);
                        $pack_price = $arr_pack_details["pack_price_$currency"];
                        $new_price  = $arr_pack_details["pack_price_$currency"]- $pre_pack_remaining_price; 
                        $new_price  = isset($new_price)&&$new_price>0?$new_price:'0';
                      }
                    }
                    else
                    {
                      $pack_price = '0';
                      $new_price = '0';
                      $currency_symbol = $arr_currency["$currency"];
                    }

                  @endphp

                  <li style="background:none;">
                    <span class="projrct-prce" style="text-decoration: line-through;"> <i class="fa fa-money" aria-hidden="true"></i> {{ isset($currency) ? strtoupper($currency) : 'USD' }} {{isset($pack_price)?$pack_price:''}}</span>
                    <span class="projrct-prce" title="{{isset($new_price)?$new_price:''}}"><b style="color:#2d2d2d;"> {{ isset($currency) ? strtoupper($currency) : 'USD' }} {{isset($new_price)?bcdiv($new_price, 1, 2):''}}</b> <small><i> {{ isset($prev_plan_type) ? $prev_plan_type : '' }} ( Decreased price, previous credit is being taken into account )</i></small></span>
                  </li>

                  @php
                    $total_cost = isset($new_price)?$new_price:'0';
                  @endphp

                @else

                  @php 
                    if(isset($currency) && $currency!='')
                    {
                      $arr_currency = config('app.project_converted_currency');

                      if(isset($arr_currency) && count($arr_currency)>0)
                      {
                        $currency_symbol = $arr_currency["$currency"];
                      }
                      else
                      {
                        $currency_symbol = '$';
                      }

                      if($currency=='USD')
                      {
                        $total_cost = isset($arr_pack_details['pack_price'])?$arr_pack_details['pack_price']:'0';
                      }
                      else
                      {
                        $currency   = strtolower($currency);
                        $total_cost = isset($arr_pack_details["pack_price_$currency"])?$arr_pack_details["pack_price_$currency"]:'0';
                      }
                    }
                    else
                    {
                      $total_cost = '0';
                      $currency_symbol = '$';
                    }
                  @endphp

                  <li style="background:none;"><span class="projrct-prce"> <i class="fa fa-money" aria-hidden="true"></i> {{ isset($currency) ? strtoupper($currency) : '$' }} {{isset($total_cost)?$total_cost:''}}</span></li>
               
                @endif
            </ul>
         </div>
         <div class="clr"></div>

         <br/>
         
         <form id="validate-form-payment" name="validate-form-payment" method="POST" action="{{url('/payment/paynow')}}">
            {{ csrf_field() }}
            <!-- payment_obj_id is id for which user is going to pay like subscription pack or milestone -->
            <input type="hidden" name="payment_obj_id" id="payment_obj_id" value=" {{isset($arr_pack_details['id'])?$arr_pack_details['id']:'0'}}">
            <!-- transaction_type is type for which user making this transaction like subscription pack or milestone -->
            <input type="hidden" name="transaction_type" id="transaction_type" value="1">
            <input type="hidden" name="total_cost" id="total_cost" value="{{isset($total_cost)?number_format($total_cost,2):'0'}}">
            <input type="hidden" name="type" value="{{isset($arr_pack_details['type'])?$arr_pack_details['type']:'0'}}">
            <div class="paypa_radio">
               <!-- Paypal -->
                   <?php /*
                     <div class="radio_area regi" onclick="javascript: return setOption('paypal');">
                        <input type="radio" name="payment_method" id="radio_paypal" class="css-checkbox" checked="" value="1">
                        <label for="radio_paypal" class="css-label radGroup1"><img src="{{url('/public')}}/front/images/paypal-payment.png" alt="paypal payment method"/></label>
                     </div>
                   */ ?>
               <!-- End Paypal -->
               <!-- Stripe -->
                   <?php /*    
                     <div class="radio_area regi" onclick="javascript: return setOption('stripe');">
                        <input type="radio" name="payment_method" id="radio_stripe" class="css-checkbox" value="2" 
                        @if(old('payment_method')==2)
                        checked="" 
                        @endif
                        >
                        <label for="radio_stripe" class="css-label radGroup1"><img src="{{url('/public')}}/front/images/stripe2.png" alt="Stripe payment method" width="148"/></label>
                     </div>
                    */ ?>
               <!-- End Stripe -->
               <!-- Wallet -->     
               {{-- @php $wallet_amount= 0; @endphp
               @if(isset($mangopay_wallet_details->Balance->Amount) && $mangopay_wallet_details->Balance->Amount!='')
                 @php $wallet_amount= $mangopay_wallet_details->Balance->Amount/100; @endphp
               @endif
               <div class="radio_area regi" onclick="javascript: return setOption('wallet');">
                  <input type="radio" name="payment_method" id="radio_wallet" class="css-checkbox" checked="" value="3">
                  <label for="radio_wallet" class="css-label radGroup1">
                    <img style="width: 292px;height: 57px;margin-top:-20px;" src="{{url('/public')}}/front/images/archexpertdefault/arc-hexpert-wallet-logo.png" alt="wallet payment method"/> 
                    <i> ( {{trans('common/wallet/text.text_money_balance')}} : {{ isset($mangopay_wallet_details->Balance->Amount)? $mangopay_wallet_details->Balance->Amount/100:'0'}}{{ isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:''}}) </i>
                    <a href=""><i class="fa fa-refresh" aria-hidden="true"></i></a>
                  </label>
               </div>  --}}
              <input type="hidden" name="currency_code" id="currency_code" value="">
              <input type="hidden" name="card_fingerprint" id="card_fingerprint" value="">
              <input type="hidden" name="card_id" id="card_id" value="">

              @if(isset($mangopay_wallet_details) && count($mangopay_wallet_details)>0)
                  @foreach($mangopay_wallet_details as $key=>$value)
                   <!-- Wallet -->
                       <div class="radio_area regi" onclick="javascript: return setOption('wallet');">
                          <input type="radio" name="payment_method" id="radio_wallet{{$key}}" class="css-checkbox" checked="" data-amount={{$value->Balance->Amount/100}} value="3" data-currency="{{ isset($value->Balance->Currency)? $value->Balance->Currency:''}}" data-payment-type="wallet">
                          <label for="radio_wallet{{$key}}" class="css-label radGroup1">
                            <img style="width: 292px;height: 57px;margin-top:-20px;" src="{{url('/public')}}/front/images/archexpertdefault/arc-hexpert-wallet-logo.png" alt="wallet payment method"/> 
                            <i> ( {{trans('common/wallet/text.text_money_balance')}} : {{ isset($value->Balance->Currency)? $value->Balance->Currency:''}} {{ isset($value->Balance->Amount)? $value->Balance->Amount/100:'0'}} ) </i>
                          </label>
                       </div> 
                       <div class="clearfix"><hr style="width: 100%;margin-left: 2px;"></div>
                   <!-- End Wallet -->
                   @endforeach
              @endif
                
               <!-- End Wallet -->

              @if(isset($mangopay_cards_details) && count($mangopay_cards_details)>0 && !empty($mangopay_cards_details))
                    @foreach($mangopay_cards_details as $card_key => $card_value)
                     <!-- card details -->
                     <?php
                        $card_alias = $expiration_date = '';
                        if(isset($card_value->Alias) && $card_value->Alias!='') {
                            $arr_card_alias = explode('XXXXXX',$card_value->Alias);
                            $card_alias = isset($arr_card_alias[1])  ? $arr_card_alias[1] : '';
                        }
                        if(isset($card_value->ExpirationDate) && $card_value->ExpirationDate!='') {
                            $expiration_date = implode('/', str_split($card_value->ExpirationDate, 2));
                        }
                     ?>
                         <div class="radio_area regi" onclick="javascript: return setOption('card');">
                            <input type="radio" 
                                   name="payment_method" 
                                   id="radio_wallet{{$card_value->Id}}" 
                                   class="css-checkbox" 
                                   value="4" 
                                   data-currency="{{ isset($card_value->Currency)? $card_value->Currency:''}}" 
                                   data-payment-type="card" 
                                   data-card-fingerprint="{{ isset($card_value->Fingerprint)? $card_value->Fingerprint:''}}" 
                                   data-card-id="{{ isset($card_value->Id)? $card_value->Id:''}}"
                                   >
                            <label for="radio_wallet{{$card_value->Id}}" class="css-label radGroup1">
                              <i> {{ isset($card_value->CardProvider)? $card_value->CardProvider: '' }} Card, ends with {{ isset($card_alias)? $card_alias: '' }} , expiry date {{ isset($expiration_date)? $expiration_date: '' }} </i>
                            </label>
                         </div> 
                         <div class="clearfix"><hr style="width: 100%;margin-left: 2px;"></div>
                     <!-- End Wallet -->
                     @endforeach
              @endif

              {{-- <div class="clearfix">
                <hr style="width: 100%;margin-left: 2px;">
              </div> --}}

              <span class='error' id="payment_method_err">{{$errors->first('payment_method')}}</span>
              <span class='error'>{{ $errors->first('payment_method') }}</span>

            </div>
            <br/>
            <div class="clr"></div>

            <div class="pay-amt-details-main">
            @if( isset($mangopay_wallet_details[0]) && ($total_cost > $mangopay_wallet_details[0]->Balance->Amount/100))
            <div class="pay-amt-details">
                <div class="total-amt-txt-section">
                    Plan Price: 
                </div>
                <div class="total-amt-value-section" style="text-align: left;" >
                    {{ isset($currency) ? strtoupper($currency) : 'USD' }} {{isset($total_cost)?number_format($total_cost,2):'0'}} {{ isset($curr_plan_type) ? $curr_plan_type : '' }}
                </div>
            </div>
            <div class="pay-amt-details">
                <div class="total-amt-txt-section">
                    Wallet Balance: 
                </div>
                <div class="total-amt-value-section" style="text-align: left;">
                    {{ isset($mangopay_wallet_details[0]->Balance->Currency)? $mangopay_wallet_details[0]->Balance->Currency:''}}  {{ isset($mangopay_wallet_details[0]->Balance->Amount) ? number_format($mangopay_wallet_details[0]->Balance->Amount/100,2) : '0.0'}}
                </div>
            </div>
            <div class="pay-amt-details net-total-section">
                <div class="total-amt-txt-section">
                    Sub Total: 
                </div>
                <div class="total-amt-value-section" style="text-align: left;">
                  @php
                    $sub_total_amount = 0;
                    if(isset($total_cost) && isset($mangopay_wallet_details[0]->Balance->Amount)){
                      $sub_total_amount = $total_cost - $mangopay_wallet_details[0]->Balance->Amount/100;
                    }
                  @endphp

                    {{ isset($currency) ? strtoupper($currency) : 'USD' }} {{number_format($sub_total_amount,2)}}
                </div>
            </div>

            <div class="pay-amt-details net-total-section">
                <div class="total-amt-txt-section">
                    Total: 
                </div>
                <div class="total-amt-value-section" style="text-align: left;">
                    {{ isset($currency) ? strtoupper($currency) : 'USD' }} {{ number_format($sub_total_amount,2)  }} 
                </div>
            </div>
            @else
              <div class="pay-amt-details">
                <div class="total-amt-txt-section">
                    Total: 
                </div>
                <div class="total-amt-value-section" style="text-align: left;">
                    {{ isset($currency) ? strtoupper($currency) : 'USD' }} {{isset($total_cost)?number_format($total_cost,2):'0'}} (Amount will be charged from the wallet)
                </div>
            </div>
            @endif
         </div>

            <div class="right_side_section" style="display:none;" id="section-card-details">
               <div class="head_grn">{{ trans('expert/subscription/packs.text_card_details') }}</div>
               <div class="row">
                  <div class="change-pwd-form">
                     <div class="col-sm-12  col-md-12 col-lg-12">
                     <div class="col-sm-12  col-md-8 col-lg-8">
                        <div class="user-box">
                           <div class="p-control-label">{{ trans('expert/subscription/packs.text_card_number') }}<span>*</span></div>
                           <div class="input-name"><input type="text" class="clint-input" placeholder="{{ trans('expert/subscription/packs.text_enter_card_number') }}" name="cardNumber" id="cardNumber" required autofocus value="{{old('cardNumber')}}"/></div>
                           <span class='error'>{{ $errors->first('cardNumber') }}</span>
                        </div>
                     </div>
                     </div>
                     <div class="col-sm-12  col-md-12 col-lg-12">
                        <div class="col-sm-12  col-md-6 col-lg-5">
                        <div class="user-box">
                           <div class="p-control-label">{{ trans('expert/subscription/packs.text_card_expiration_month_year') }}<span>*</span></div>

                           <div class="input-name">
                              <div class="row">
                                 <div class="col-xs-3 col-sm-3 col-md-3 col-lg-4 droup-select month" style="margin-left:14px;margin-right:14px;width: 85px;padding-right: 0px;padding-left: 0px;" >
                                  <select class="droup" name="cardExpiryMonth" id="cardExpiryMonth" data-rule-required="true">
                                     <option value="">{{trans('milestones/payment_methods.entry_mm')}}</option>
                                     @for($i = 1;$i <= 12;$i++)
                                      <option value="@if($i < 10){{0}}@endif{{$i}}"
                                      @if(old('cardExpiryMonth') == $i) 
                                        selected="selected" 
                                        @endif >@if($i < 10){{0}}@endif{{$i}}</option>
                                     @endfor
                                 </select>
                                 </div>
                                 <span class='error'>{{ $errors->first('cardExpiryMonth') }}</span>
                                   <?php $year = date('Y'); ?>
                                    <select class="droup" name="cardExpiryYear" id="cardExpiryYear" data-rule-required="true" >
                                     <option value="">{{trans('milestones/payment_methods.entry_yyyy')}}</option>
                                     @for($j = $year ;$j <= $year + 25 ;$j++)
                                        <option value="{{$j}}" @if(old('cardExpiryYear') == $j) selected="selected" @endif >{{$j}}</option>
                                     @endfor
                                   </select>
                                   </div>
                                  <span class='error'>{{ $errors->first('cardExpiryYear') }}</span>
                              </div>
                           </div>
                        </div>
                        </div>
                        <div class="col-sm-12  col-md-6 col-lg-3">
                        <div class="user-box">
                           <div class="p-control-label">{{ trans('expert/subscription/packs.text_card_cv_code') }}<span>*</span></div>
                           <div class="input-name"><input type="text" class="clint-input" placeholder="{{ trans('expert/subscription/packs.text_enter_cv_code') }}" name="cardCVC" id="cardCVC" required value="{{old('cardCVC')}}" /></div>
                            <span class='error'>{{ $errors->first('cardCVC') }}</span>
                        </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>

            <br/>
            <div class="clr"></div>
            <div class="right-side">
               <button class="normal-btn pull-right" onclick="showLoader()" value="submit" id="btn-form-payment-submit">{{ trans('expert/subscription/packs.text_proceed') }}</button>
            </div>
         </form>
         <div class="clr"></div>
      </div>
   </div>
</div>
<script type="text/javascript" src="{{url('/public')}}/assets/payment/jquery.payment.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript">
   $( document ).ready(function(){
      var is_stripe = jQuery('#radio_stripe').prop('checked');
       if (is_stripe==true){
         jQuery('#section-card-details').show();
       } else {
         jQuery('#section-card-details').hide();
       }
   });
   $('input[name=cardNumber]').payment('formatCardNumber');
   $('input[name=cardCVC]').payment('formatCardCVC');
   jQuery("#validate-form-payment").validate({
      errorElement: 'span',   
   });
   $('#validate-form-payment').submit(function () {
     // jQuery('#btn-form-payment-submit').prop('disabled', true);
   });
   $('#btn-form-payment-submit').click(function (){
      var total_cost     = '{{$total_cost}}';
      var wallet_amount  = $("input[name='payment_method']:checked").attr('data-amount');
      var currency_code  = $("input[name='payment_method']:checked").attr('data-currency');
      var payment_type   = $("input[name='payment_method']:checked").attr('payment-type');
      
      var card_fingerprint = $("input[name='payment_method']:checked").attr('data-card-fingerprint');
      var card_id          = $("input[name='payment_method']:checked").attr('data-card-id');

      $('#card_fingerprint').val(card_fingerprint);
      $('#card_id').val(card_id);

      $('#currency_code').val(currency_code);
      $('#payment_method_err').html('');

      var check_currency = $('#currency_code').val();
      if(check_currency=='' || check_currency==null)
      {
        $("#payment_method_err").html('Sorry, Please select payment method.');
        return false;
      }
      
      if(payment_type == 'wallet') {
        if($('#radio_wallet').is(':checked')) {
         if(total_cost=='0' || total_cost=='0.0' || total_cost=='0.00'){
           $("#payment_method_err").html('Sorry, total cost should not 0.');
           return false;
         }
         if(parseFloat(total_cost)>parseFloat(wallet_amount)){
           $("#payment_method_err").html('Sorry, Your wallet amount is not suffeciant to make transaction.');
           return false;
         } 
        }
      }

      if(payment_type == 'card') {
        if(total_cost=='0' || total_cost=='0.0' || total_cost=='0.00'){
           $("#payment_method_err").html('Sorry, total cost should not 0.');
           return false;
        }
      }

      /*jQuery('#btn-form-payment-submit').prop('disabled', true);*/
   });
   function setOption(opt) {
       if (opt=='stripe'){
         jQuery('#section-card-details').show();
         jQuery('#radio_stripe').prop('checked',true);
       } else if (opt=='paypal'){
         jQuery('#section-card-details').hide();
         jQuery('#radio_paypal').prop('checked',true);
       } else if (opt=='wallet'){
         jQuery('#section-card-details').hide();
         jQuery('#radio_wallet').prop('checked',true);
       }
   }
   $('#cardExpiryYear').keypress(function(eve){
      if(eve.which == 8 || eve.which == 190){
        return true;
      } else if (eve.which != 8 && eve.which != 0 && (eve.which < 48 || eve.which > 57)) {
       eve.preventDefault();
      }
      if($('#cardExpiryYear').val().length>3){
         eve.preventDefault();
      }
   });
   $('#cardExpiryMonth').keypress(function(eve){
      if(eve.which == 8 || eve.which == 190){
        return true;
      } else if (eve.which != 8 && eve.which != 0 && (eve.which < 48 || eve.which > 57)) {
       eve.preventDefault();
      } 
      if($('#cardExpiryMonth').val().length>1){
         eve.preventDefault();
      }
   });
   function showLoader(){
    if($('#radio_stripe').is(':checked')){
      if($('#validate-form-payment').valid() == true){
        showProcessingOverlay();
      }
    } else if($('#radio_paypal').is(':checked')) {
      showProcessingOverlay();
    }
  }
</script>
@stop