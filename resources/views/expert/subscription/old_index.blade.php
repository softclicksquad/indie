@extends('expert.layout.master_withoutsidebar')                
@section('main_content')
<div class="top-title-pattern">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="title-box-top">
               <h3>{{isset($page_title)?$page_title:''}}</h3>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="middle-container">
   <div class="container">
   @include('front.layout._operation_status')
      <div class="pkg-subsc search-grey-bx">{{ trans('expert/subscription/packs.text_heading') }}</div>
      @if (!isset($arr_subscription) || sizeof($arr_subscription)<1) 
         <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            {{ trans('expert/subscription/packs.text_valid_subscription_upgrade_now') }}
         </div>
      @endif

      <div class="pricing pricing-5 bottommargin clearfix">
         @if(isset($arr_packs) && sizeof($arr_packs)>0)
         @foreach($arr_packs as $key => $pack)
            <div class="pricing-box pricing-minimal
               @if(isset($arr_subscription['subscription_pack_id']) && $arr_subscription['subscription_pack_id']==$pack['id'])
                best-price
               @endif
               ">
               <div class="pricing-title">
                  <h3>{{isset($pack['pack_name'])?$pack['pack_name']:''}}</h3>
                  <span>
                  @if(isset($arr_subscription['subscription_pack_id']) && $arr_subscription['subscription_pack_id']==$pack['id'])
                     @if($arr_subscription['expiry_at']==null && $arr_subscription['subscription_pack_id']==1)
                     {{ trans('expert/subscription/packs.text_lifetime') }}
                     @else
                     {{ trans('expert/subscription/packs.text_valid_till') }} {{isset($arr_subscription['expiry_at'])?date('d-M-Y',strtotime($arr_subscription['expiry_at'])):''}}
                     @endif
                     <?php
                        $remaining_bids = 0;
                        if(isset($arr_subscription['number_of_bids']) && $arr_subscription['number_of_bids'] > 0 && isset($bids_applied) ) {  
                           if ($arr_subscription['number_of_bids']>=$bids_applied) {
                              $remaining_bids = $arr_subscription['number_of_bids'] - $bids_applied ;
                           }
                        }
                     ?>
                     <span style="margin-bottom: 0px;">
                        <b> {{ trans('expert/subscription/packs.text_remaining_bids') }} &nbsp;{{ $remaining_bids }}</b>
                     </span>
                     <span style="margin-bottom: -20px;">
                        {{ trans('expert/subscription/packs.text_top_up_bids') }} &nbsp;{{ $topup_bids }}
                     </span>                  
                  @endif
                  &nbsp;</span>
               </div>
               <div class="pricing-price">
                  <span class="price-unit">&#36;</span>{{isset($pack['pack_price'])?$pack['pack_price']:''}}
                   @if(isset($pack['id']) && $pack['id']!=1)
                  <span class="price-tenure">
                 
                  {{ trans('expert/subscription/packs.text_mo') }}
                  
                  </span>
                  <div class="billed-month">{{ trans('expert/subscription/packs.text_billed_monthly') }}</div>
                  @endif
               </div>
               <div class="pricing-features">
                  <ul>
                     <li>
                        {{isset($pack['number_of_bids'])?$pack['number_of_bids']:''}} {{ trans('expert/subscription/packs.text_bids') }}
                     </li>
                     <li>
                        {{isset($pack['topup_bid_price'])?$pack['topup_bid_price']:''}} {{ trans('expert/subscription/packs.text_topup_bid') }}
                     </li>
                     <li>
                        {{isset($pack['number_of_categories'])?$pack['number_of_categories']:''}} {{ trans('expert/subscription/packs.text_categories') }}
                     </li>
                     <li>
                        {{isset($pack['number_of_subcategories'])?$pack['number_of_subcategories']:''}} {{ trans('expert/subscription/packs.text_subcategories') }}
                     </li>
                     <li>
                        {{isset($pack['number_of_skills'])?$pack['number_of_skills']:''}} {{ trans('expert/subscription/packs.text_skills') }}
                     </li>
                     <li>
                        {{isset($pack['number_of_favorites_projects'])?$pack['number_of_favorites_projects']:''}} {{ trans('expert/subscription/packs.text_favorites_projects') }}
                     </li>

                     <li>
                        {{isset($pack['website_commision'])?$pack['website_commision']:''}} % {{ trans('expert/subscription/packs.text_website_commission') }}
                     </li>
                  </ul>
               </div>
               <div class="clr"></div>
               <div class="pricing-action">
   
                  @if($key != 0)
                     {{--not free--}}
                     @if(isset($arr_subscription['subscription_pack_id']) && isset($pack['id']) && ($arr_subscription['subscription_pack_id'] == $pack['id']) )
                        <a href="javascript: void(0);">
                           <button class="btn-paynow"><span class="btn1" style="cursor: no-drop;">
                           @if(isset($arr_subscription['subscription_pack_id']) && $arr_subscription['subscription_pack_id']==$pack['id'])
                           {{ trans('expert/subscription/packs.text_activated') }}
                           @else
                           {{ trans('expert/subscription/packs.text_pay_now') }}
                           @endif
                           </span>
                           <span style="margin-left:3px;" class="btn1 narr"><i aria-hidden="true" class="fa fa-angle-right"></i></span>
                           </button>
                        </a>
                     @else
                        @if(isset($arr_subscription['pack_name']) && $arr_subscription['pack_name'] == 'Plus')
                           @if($pack['pack_name'] == 'Premium' || $pack['pack_name'] == 'Professional')
                                 <a href="{{isset($module_url_path)?$module_url_path.'/payment/'.base64_encode($pack['id']).'/'.base64_encode($arr_subscription['subscription_pack_id']):''}}">
                                    <button class="btn-paynow"><span class="btn1">
                                       @if(isset($arr_subscription['subscription_pack_id']) && $arr_subscription['subscription_pack_id']==$pack['id'])
                                       {{ trans('expert/subscription/packs.text_activated') }}
                                       @else
                                       {{ trans('expert/subscription/packs.text_pay_now') }}
                                       @endif
                                       </span>
                                       <span style="margin-left:3px;" class="btn1 narr"><i aria-hidden="true" class="fa fa-angle-right"></i></span>
                                    </button>
                                 </a>
                           @else
                              <a href="javascript: void(0);" style="cursor: no-drop;" class="btn-paynow"><span class="btn1">Deactivated</span><span style="margin-left:3px;" class="btn1 narr"><i aria-hidden="true" class="fa fa-angle-right"></i></span></a>
                           @endif  
                        @elseif(isset($arr_subscription['pack_name']) && $arr_subscription['pack_name'] == 'Premium')
                           @if($pack['pack_name'] == 'Professional')
                                 <a href="{{isset($module_url_path)?$module_url_path.'/payment/'.base64_encode($pack['id']).'/'.base64_encode($arr_subscription['subscription_pack_id']):''}}">
                                    <button class="btn-paynow"><span class="btn1">
                                       @if(isset($arr_subscription['subscription_pack_id']) && $arr_subscription['subscription_pack_id']==$pack['id'])
                                       {{ trans('expert/subscription/packs.text_activated') }}
                                       @else
                                       {{ trans('expert/subscription/packs.text_pay_now') }}
                                       @endif
                                       </span>
                                       <span style="margin-left:3px;" class="btn1 narr"><i aria-hidden="true" class="fa fa-angle-right"></i></span>
                                    </button>
                                 </a>
                           @else
                              <a href="javascript: void(0);" style="cursor: no-drop;" class="btn-paynow"><span class="btn1">Deactivated</span><span style="margin-left:3px;" class="btn1 narr"><i aria-hidden="true" class="fa fa-angle-right"></i></span></a>
                           @endif     
                        @elseif(isset($arr_subscription['pack_name']) && $arr_subscription['pack_name'] == 'Professional')
                              <a href="javascript: void(0);" style="cursor: no-drop;" class="btn-paynow"><span class="btn1">Deactivated</span><span style="margin-left:3px;" class="btn1 narr"><i aria-hidden="true" class="fa fa-angle-right"></i></span></a>
                        @elseif((isset($arr_subscription['pack_name']) && $arr_subscription['pack_name'] == 'Starter') || (isset($arr_subscription['pack_name']) && $arr_subscription['pack_name'] == 'Basic'))
                              <a href="{{isset($module_url_path)?$module_url_path.'/payment/'.base64_encode($pack['id']).'/'.base64_encode($arr_subscription['subscription_pack_id']):''}}">
                                 <button class="btn-paynow"><span class="btn1">
                                    @if(isset($arr_subscription['subscription_pack_id']) && $arr_subscription['subscription_pack_id']==$pack['id'])
                                    {{ trans('expert/subscription/packs.text_activated') }}
                                    @else
                                    {{ trans('expert/subscription/packs.text_pay_now') }}
                                    @endif
                                    </span>
                                    <span style="margin-left:3px;" class="btn1 narr"><i aria-hidden="true" class="fa fa-angle-right"></i></span>
                                 </button>
                              </a>
                        @endif   

                     @endif
                  @else
                     {{-- free --}}
                     @if(isset($arr_subscription['subscription_pack_id']) && $arr_subscription['subscription_pack_id'] == 1)
                       <a href="javascript: void(0);" style="cursor: no-drop;" class="btn-paynow"><span class="btn1">{{ trans('expert/subscription/packs.text_free') }}</span><span style="margin-left:3px;" class="btn1 narr"><i aria-hidden="true" class="fa fa-angle-right"></i></span></a>
                     @else 
                       <a href="javascript: void(0);" style="cursor: no-drop;" class="btn-paynow"><span class="btn1">{{ trans('expert/subscription/packs.text_free') }}</span><span style="margin-left:3px;" class="btn1 narr"><i aria-hidden="true" class="fa fa-angle-right"></i></span></a>
                     @endif
                  @endif
               </div>
            </div>
         @endforeach
         @endif
      </div>
   </div>
</div>
@stop