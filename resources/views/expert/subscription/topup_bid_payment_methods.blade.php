@extends('expert.layout.master')                
@section('main_content')
<div class="col-sm-7 col-md-8 col-lg-9">
   <div class="right_side_section payment-section">
      <?php $user = Sentinel::check(); 
          
          $arr_data = get_subscription_plan_details($user->id);
          $expiry_date = isset($arr_data['subscription_pack_details']['subscription_user_details']['expiry_at'])?
                $arr_data['subscription_pack_details']['subscription_user_details']['expiry_at']:'';
      ?>
      @include('front.layout._operation_status')
      <div class="head_grn">{{ trans('expert/subscription/packs.text_topup_bid_payment') }}</div>
      <div class="ongonig-project-section">
          <div class="project-title">
            <div style="height:auto;" class="more-project-dec">
            <h5>
                You can now buy a package with additional bids or upgrade your <a href="{{url('/')}}/expert/subscription">subscription</a> plan. Remanining bids will expire at end of the subscription plan on a monthly basic. 
                {{-- {{trans('expert/subscription/packs.text_bid_limit_purchase_extra_1') }}<br><br>{{ trans('expert/subscription/packs.text_bid_limit_purchase_extra_2') }} 
                <a href="{{url('/public')}}/expert/subscription">{{ trans('expert/subscription/packs.text_bid_limit_purchase_extra_3') }}</a> 
                {{ trans('expert/subscription/packs.text_bid_limit_purchase_extra_4') }} --}}
            </h5>
            @if(isset($expiry_date) && ($expiry_date!=null || $expiry_date!=''))
            {{-- <h5> Your Current Plan will expire at {{isset($expiry_date)?$expiry_date:'N/A'}}</h5> --}}
            {{-- @else
            <h5> Your Current plan is expire on <b> 2020-12-12</b></h5> --}}
            @endif
            </div>
         </div>
         <div class="det-divider"></div>
         {{-- <div class="clr"></div>
         <br> --}}

          <div class="radio_area regi">
            <input type="radio" 
                   id="radio_top_up_bids" 
                   class="css-checkbox" 
                   checked="" 
                   >
            <label for="radio_top_up_bids" class="css-label radGroup1">
              <i> 5 Top up Bids </i>
            </label>
          </div>
          <div class="clr"></div>
          <br/>
         {{-- <div class="project-list pro-list-ul">
          <ul>
             <li style="background:none;">
                  <span class="projrct-prce">Bids need to purchase :  <span style="color:#737373;"></span>
                  5
                  </span> 
             </li>
          </ul>
       </div>
       <div class="clr"></div>
       <br/> --}}

         {{-- <div class="project-list pro-list-ul">
              <ul>
                 <li style="background:none;">
                      <span class="projrct-prce">Per bid prize :  <span style="color:#737373;"></span>
                      {{$currency_code}} {{isset($arr_subscription_details['topup_bid_price'])?$arr_subscription_details['topup_bid_price']:'0'}}
                      </span> 
                 </li>
              </ul>
           </div>
           <div class="clr"></div>
           <br/> --}}


         <div class="project-list pro-list-ul">
            <ul>
                <li><span><i class="fa fa-calendar" aria-hidden="true"></i> </span>{{date('d M Y')}} </li>
               <li style="background:none;"><span class="projrct-prce"> Total prize:  {{$currency_code}} {{isset($arr_subscription_details['topup_bid_price'])? number_format($arr_subscription_details['topup_bid_price'],2) :'0'}}</span></li>
            </ul>
         </div>
         <div class="clr"></div>

         <br/>
         <?php
            
            $total_cost = isset($arr_subscription_details['topup_bid_price'])?$arr_subscription_details['topup_bid_price']:'0'; 
            // $total_cost = $total_cost*5;
           
         ?>
         <form id="validate-form-payment" name="validate-form-payment" method="POST" action="{{url('/payment/paynow')}}">
            {{ csrf_field() }}
            <!-- payment_obj_id is id for which user is going to pay like subscription pack or milestone -->
            <input type="hidden" name="payment_obj_id" id="payment_obj_id" value=" {{isset($arr_subscription_details['id'])?$arr_subscription_details['id']:'0'}}">
            <!-- transaction_type is type for which user making this transaction like subscription pack or milestone -->
            <input type="hidden" name="transaction_type" id="transaction_type" value="4">
            <input type="hidden" name="total_cost" id="total_cost" value="{{isset($total_cost)?$total_cost:'0'}}">
            <div class="paypa_radio">
               
               <!-- Paypal -->
                    <?php /*
                    <div class="radio_area regi" onclick="javascript: return setOption('paypal');">
                        <input type="radio" name="payment_method" id="radio_paypal" class="css-checkbox" checked="" value="1">
                        <label for="radio_paypal" class="css-label radGroup1"><img src="{{url('/public')}}/front/images/paypal-payment.png" alt="paypal payment method"/></label>
                    </div>
                    <div class="clearfix"><hr style="width: 100%;margin-left: 2px;"></div>
                    */ ?>
               <!-- End Paypal -->
               <!-- Stripe -->
                   <?php /*
                   <div class="radio_area regi" onclick="javascript: return setOption('stripe');">
                      <input type="radio" name="payment_method" id="radio_stripe" class="css-checkbox" value="2" 
                      @if(old('payment_method')==2)
                      checked="" 
                      @endif
                      >
                      <label for="radio_stripe" class="css-label radGroup1"><img src="{{url('/public')}}/front/images/stripe2.png" alt="Stripe payment method" width="148"/></label>
                   </div>
                   <div class="clearfix"><hr style="width: 100%;margin-left: 2px;"></div>
                   */ ?>
               <!-- End Stripe -->
               <!-- Wallet -->
               @php $wallet_amount= 0; @endphp
               @if(isset($mangopay_wallet_details->Balance->Amount) && $mangopay_wallet_details->Balance->Amount!='')
                 @php $wallet_amount= $mangopay_wallet_details->Balance->Amount/100; @endphp
               @endif
               <div class="radio_area regi" onclick="javascript: return setOption('wallet');">
                  <input type="radio" name="payment_method" id="radio_wallet" class="css-checkbox" checked="" value="3">
                  <label for="radio_wallet" class="css-label radGroup1">
                    <img style="width: 292px;height: 57px;margin-top:-20px;" src="{{url('/public')}}/front/images/archexpertdefault/arc-hexpert-wallet-logo.png" alt="wallet payment method"/> 
                    <i> ( {{trans('common/wallet/text.text_money_balance')}} : {{ isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:''}} {{ isset($mangopay_wallet_details->Balance->Amount)? $mangopay_wallet_details->Balance->Amount/100:'0'}}) </i>
                    <a href=""><i class="fa fa-refresh" aria-hidden="true"></i></a>
                  </label>
               </div> 
               <input type="hidden" name="currency_code" value="{{ isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:''}}">
               <div class="clearfix"><hr style="width: 100%;margin-left: 2px;"></div>
               <!-- End Wallet -->
                <span class='error' id="payment_method_err">{{$errors->first('payment_method')}}</span>
                <span class='error'>{{ $errors->first('payment_method') }}</span>
              
              <div class="project-list pro-list-ul">
                  <ul>
                    <li style="background:none;"><span style="text-decoration: line-through;" class="projrct-prce"> Processing fee:  {{$currency_code}} {{isset($service_charge_payin)? number_format($service_charge_payin,2) :'0'}}</span></li>
                  </ul>
               </div>
               <div class="clr"></div>

               {{-- <br/> --}}

            </div>
            <br/>
            <div class="clr"></div>
            <div class="right_side_section" style="display:none;" id="section-card-details">
               <div class="head_grn">{{ trans('expert/subscription/packs.text_card_details') }}</div>
               <div class="row">
                  <div class="change-pwd-form">
                     <div class="col-sm-12  col-md-12 col-lg-12">
                     <div class="col-sm-12  col-md-8 col-lg-8">
                        <div class="user-box">
                           <div class="p-control-label">{{ trans('expert/subscription/packs.text_card_number') }}<span>*</span></div>
                           <div class="input-name"><input type="text" class="clint-input" placeholder="Enter Card Number" name="cardNumber" id="cardNumber" required autofocus value="{{old('cardNumber')}}"/></div>
                           <span class='error'>{{ $errors->first('cardNumber') }}</span>
                        </div>
                     </div>
                     </div>
                     <div class="col-sm-12  col-md-12 col-lg-12">
                        <div class="col-sm-12  col-md-6 col-lg-5">
                        <div class="user-box">
                           <div class="p-control-label">{{ trans('expert/subscription/packs.text_card_expiration_month_year') }}<span>*</span></div>
                           <div class="input-name">
                              <div class="row">
                                 <div class="col-xs-3 col-sm-3 col-md-3 col-lg-4 droup-select month" style="margin-left:14px;margin-right:14px;width: 85px;padding-right: 0px;padding-left: 0px;" >
                                  <select class="droup" name="cardExpiryMonth" id="cardExpiryMonth" data-rule-required="true">
                                     <option value="">{{trans('milestones/payment_methods.entry_mm')}}</option>
                                     @for($i = 1;$i <= 12;$i++)
                                      <option value="@if($i < 10){{0}}@endif{{$i}}" @if(old('cardExpiryMonth') == $i) selected="selected" @endif >@if($i < 10){{0}}@endif{{$i}}</option>
                                     @endfor
                                 </select>
                                 </div>
                                 <span class='error'>{{ $errors->first('cardExpiryMonth') }}</span>
                                 <div class="col-xs-3 col-sm-3 col-md-3 col-lg-5 p-l-r droup-select year" style="width: 105px;padding-right: 0px;padding-left: 0px;">
                                   <?php $year = date('Y'); ?>
                                    <select class="droup" name="cardExpiryYear" id="cardExpiryYear" data-rule-required="true" >
                                     <option value="">{{trans('milestones/payment_methods.entry_yyyy')}}</option>
                                     @for($j = $year ;$j <= $year + 25 ;$j++)
                                        <option value="{{$j}}" @if(old('cardExpiryYear') == $j) selected="selected" @endif >{{$j}}</option>
                                     @endfor
                                   </select>
                                   </div>
                                  <span class='error'>{{ $errors->first('cardExpiryYear') }}</span>
                              </div>
                           </div>
                        </div>
                        </div>
                        <div class="col-sm-12  col-md-6 col-lg-3">
                        <div class="user-box">
                           <div class="p-control-label">{{ trans('expert/subscription/packs.text_card_cv_code') }}<span>*</span></div>
                           <div class="input-name"><input type="text" class="clint-input" placeholder="{{ trans('expert/subscription/packs.text_enter_cv_code') }}" name="cardCVC" id="cardCVC" required value="{{old('cardCVC')}}" /></div>
                            <span class='error'>{{ $errors->first('cardCVC') }}</span>
                        </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <br/>
            <div class="clr"></div>
            <div class="right-side">
               <button class="normal-btn pull-right" onclick="showLoader()" value="submit" id="btn-form-payment-submit">{{ trans('expert/subscription/packs.text_proceed') }}</button>
            </div>
         </form>

         <div class="clr"></div>
      </div>
   </div>
</div>
<script type="text/javascript" src="{{url('/public')}}/assets/payment/jquery.payment.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript">
   $( document ).ready(function() {
      var is_stripe = jQuery('#radio_stripe').prop('checked');
       if (is_stripe==true) {
         jQuery('#section-card-details').show();
       } else {
         jQuery('#section-card-details').hide();
       }
   });
   $('input[name=cardNumber]').payment('formatCardNumber');
   $('input[name=cardCVC]').payment('formatCardCVC');
   jQuery("#validate-form-payment").validate({
      errorElement: 'span',   
   });
   $('#validate-form-payment').submit(function () 
   {
     // jQuery('#btn-form-payment-submit').prop('disabled', true);
   });
   // $('#btn-form-payment-submit').click(function (){
   //    var total_cost     = '{{$total_cost}}';
   //    var wallet_amount  = '{{$wallet_amount}}';
   //    $('#payment_method_err').html('');
   //    if($('#radio_wallet').is(':checked')) {
   //     if(total_cost=='0' || total_cost=='0.0' || total_cost=='0.00'){
   //       $("#payment_method_err").html('Sorry, total cost should not 0USD.');
   //       return false;
   //     }
   //     if(parseFloat(total_cost)>parseFloat(wallet_amount)){
   //       $("#payment_method_err").html('Sorry, Your wallet amount is not suffeciant to make transaction.');
   //       return false;
   //     } 
   //    }
   //    /*jQuery('#btn-form-payment-submit').prop('disabled', true);*/
   // });
   function setOption(opt) {
       if (opt=='stripe'){
         jQuery('#section-card-details').show();
         jQuery('#radio_stripe').prop('checked',true);
       } else if (opt=='paypal'){
         jQuery('#section-card-details').hide();
         jQuery('#radio_paypal').prop('checked',true);
       } else if (opt=='wallet'){
         jQuery('#section-card-details').hide();
         jQuery('#radio_wallet').prop('checked',true);
       }
   }
   $('#cardExpiryYear').keypress(function(eve) 
   {
      if(eve.which == 8 || eve.which == 190){
        return true;
      } else if (eve.which != 8 && eve.which != 0 && (eve.which < 48 || eve.which > 57)) {
       eve.preventDefault();
      }
      if($('#cardExpiryYear').val().length>3){
         eve.preventDefault();
      }
   });
   $('#cardExpiryMonth').keypress(function(eve) 
   {
      if(eve.which == 8 || eve.which == 190) {
        return true;
      } else if (eve.which != 8 && eve.which != 0 && (eve.which < 48 || eve.which > 57)) {
       eve.preventDefault();
      }
      if($('#cardExpiryMonth').val().length>1){
         eve.preventDefault();
      }
   });
   function showLoader() {
    if($('#radio_stripe').is(':checked')) {
      if($('#validate-form-payment').valid() == true) {
        showProcessingOverlay();
      }
    } else if($('#radio_paypal').is(':checked')) {
      showProcessingOverlay();
    }
   }
 </script>
@stop