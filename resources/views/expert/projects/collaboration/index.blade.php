@extends('expert.layout.master')                
@section('main_content')
<?php $user = Sentinel::check();?>
<div class="col-sm-7 col-md-8 col-lg-9">
    <div class="coll-listing-section project-coll">
        <div class="contest-titile">
            <div class="sm-logo d-inlin"><img src="{{url('/public/front/images/small-logo.png')}}" class="img-responsive"></div>
            <span class="d-inlin">Project</span>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <h2><a href="{{url('/expert/projects/details/')}}/{{base64_encode($project_id)}}" style="color:#8e8e8e;">{{isset($project_main_attachment['project_name'])?$project_main_attachment['project_name']:'-'}}</a></h2>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4">
                <div style="margin: 4px 0 0;" class="upload-btn p-relative">
                    <a href="#colla-upload-file" data-keyboard="false" data-backdrop="static"  data-toggle="modal"  class="black-border-btn"><i class="fa fa-upload"></i> Upload Image/ Document</a>
                </div>
            </div>
            <div class="col-sm-12 col-md-2 col-lg-2">
                 <a href="{{url('/expert/projects/details/')}}/{{base64_encode($project_id)}}" style="margin: 23px 0 0;" class="back-btn pull-right"><i class="fa fa-reply-all"></i> {{trans('client/contest/common.text_back')}}</a>
            </div>
        </div>
        <div class="row">
            <!-- project main attachment from project table -->
            @if(isset($project_main_attachment['project_attachment']) && $project_main_attachment['project_attachment'] != "")
                <?php 
                    $attachment             = isset($project_main_attachment['project_attachment'])?$project_main_attachment['project_attachment']:"";
                    $get_attatchment_format = get_attatchment_format($attachment);
                ?>
                @if(isset($get_attatchment_format) &&
                    $get_attatchment_format == 'doc'  ||
                    $get_attatchment_format == 'docx' ||
                    $get_attatchment_format == 'odt'  ||
                    $get_attatchment_format == 'pdf'  ||
                    $get_attatchment_format == 'txt'  ||
                    $get_attatchment_format == 'xlsx')
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 coll-list">
                        <div class="coll-list-block">
                            <div class="coll-img p-relative">
                                @if(file_exists('public/uploads/front/postprojects/'.$project_main_attachment['project_attachment']))
                                <img src="{{url('/public')}}/front/images/file_formats/{{$get_attatchment_format}}.png" class="img-responsive">
                                @else
                                <img src="{{url('/public')}}/front/images/file_formats/image.png"  class="img-responsive" alt=""/>
                                @endif
                                <div class="coll-details">
                                    <div class="coll-person">
                                        <div class="coll-name"><!-- File #1 --></div>
                                    </div>
                                    <!-- <div class="avg-rating">
                                        <ul>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>
                                    </div> -->
                                </div>
                                <div class="list-hover">
                                    <a style="font-size:2em;" download href="{{url('/public')}}{{config('app.project.img_path.project_attachment')}}{{$project_main_attachment['project_attachment']}}" class="view-button"><i class="fa fa-cloud-download"></i></a>
                                </div>
                            </div>
                            <div class="design-title">
                                <p>Main attachment</p>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 coll-list">
                            <div class="coll-list-block">
                                <div class="coll-img p-relative">
                                    @if(file_exists('public/uploads/front/postprojects/'.$project_main_attachment['project_attachment']))
                                    <img src="{{url('/public')}}{{config('app.project.img_path.project_attachment')}}{{$project_main_attachment['project_attachment']}}" class="img-responsive">
                                    @else
                                    <img src="{{url('/public')}}/front/images/archexpertdefault/default-arch.jpg"  class="img-responsive" alt=""/>
                                    @endif
                                    <div class="coll-details">
                                        <div class="coll-person">
                                            <div class="coll-name"><!-- File #2 --></div>
                                        </div>
                                        <!-- <div class="avg-rating">
                                            <ul>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                            </ul>
                                        </div> -->
                                    </div>
                                    <div class="list-hover">
                                        <a style="font-size:2em;" download href="{{url('/public')}}{{config('app.project.img_path.project_attachment')}}{{$project_main_attachment['project_attachment']}}" class="view-button"><i class="fa fa-cloud-download"></i></a>
                                    </div>
                                </div>
                                <div class="design-title">
                                    <p>Main attachment</p>
                                </div>
                            </div>
                    </div>
                @endif
                <div class="clearfix"></div>
            @endif
            <!-- end project main attachment from project table -->

            @if(isset($project_attachment['data']) && sizeof($project_attachment['data']) >0)
                @foreach($project_attachment['data'] as $key => $attch)
                    @php 
                        $attachment             = isset($attch['attachment'])?$attch['attachment']:"";
                        $get_attatchment_format = get_attatchment_format($attachment);
                    @endphp
                    @if(isset($get_attatchment_format) &&
                        $get_attatchment_format == 'doc'  ||
                        $get_attatchment_format == 'docx' ||
                        $get_attatchment_format == 'odt'  ||
                        $get_attatchment_format == 'pdf'  ||
                        $get_attatchment_format == 'txt'  ||
                        $get_attatchment_format == 'xlsx')
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 coll-list">
                            <div class="coll-list-block">
                                <div class="coll-img p-relative">
                                    @if(file_exists('public/uploads/front/project_attachments/'.$attch['attachment']))
                                    <img src="{{url('/public')}}/front/images/file_formats/{{$get_attatchment_format}}.png" class="img-responsive">
                                    @else
                                    <img src="{{url('/public')}}/front/images/file_formats/image.png"  class="img-responsive" alt=""/>
                                    @endif
                                    <div class="coll-details">
                                        <div class="coll-person">
                                            <div class="coll-name">File #{{isset($attch['attchment_no'])?$attch['attchment_no']:'-'}}</div>
                                        </div>
                                        <!-- <div class="avg-rating">
                                            <ul>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                            </ul>
                                        </div> -->
                                    </div>
                                    <div class="list-hover">
                                        <a href="{{url('/expert/projects/collaboration/details')}}/{{isset($attch['id'])?base64_encode($attch['id']):'0'}}/{{base64_encode($project_id)}}" class="view-button">View</a>
                                    </div>
                                </div>
                                <div class="design-title">
                                    <p style="display: inline;" title="{{isset($attch['attachment'])?$attch['attachment']:'Unknown'}}">{{isset($attch['attachment'])?str_limit($attch['attachment'],14):'Unknown'}}</p>
                                    <a  style="font-size:1.5em;color:#2d2d2d"  style="display: inline;" class="pull-right" download href="{{url('/public/uploads/front/project_attachments/')}}/{{$attch['attachment']}}"><i class="fa fa-cloud-download"></i></a>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 coll-list">
                                <div class="coll-list-block">
                                    <div class="coll-img p-relative">
                                        @if(file_exists('public/uploads/front/project_attachments/'.$attch['attachment']))
                                        <img src="{{url('/public/uploads/front/project_attachments/')}}/{{$attch['attachment']}}" class="img-responsive">
                                        @else
                                        <img src="{{url('/public')}}/front/images/archexpertdefault/default-arch.jpg"  class="img-responsive" alt=""/>
                                        @endif
                                        <div class="coll-details">
                                            <div class="coll-person">
                                                <div class="coll-name">File #{{isset($attch['attchment_no'])?$attch['attchment_no']:'-'}}</div>
                                            </div>
                                            <!-- <div class="avg-rating">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </div> -->
                                        </div>
                                        <div class="list-hover">
                                            <a href="{{url('/expert/projects/collaboration/details')}}/{{isset($attch['id'])?base64_encode($attch['id']):'0'}}/{{base64_encode($project_id)}}" class="view-button">View</a>
                                        </div>
                                    </div>
                                    <div class="design-title" >
                                        <p style="display: inline;" title="{{isset($attch['attachment'])?$attch['attachment']:'Unknown'}}">{{isset($attch['attachment'])?str_limit($attch['attachment'],14):'Unknown'}}</p>
                                        <a  style="font-size:1.5em;color:#2d2d2d;"  style="display: inline;" class="pull-right" download href="{{url('/public/uploads/front/project_attachments/')}}/{{$attch['attachment']}}"><i class="fa fa-cloud-download"></i></a>
                                    </div>
                                </div>
                            </div>
                    @endif
                @endforeach
            @endif
        </div>
        @if(isset($project_main_attachment['project_attachment']) && $project_main_attachment['project_attachment'] == "" && empty($project_attachment['data']) && sizeof($project_attachment['data']) <= 0)
            <div class="search-grey-bx">
              <div class="no-record" >
                 {{trans('expert/projects/awarded_projects.text_sorry_no_record_found')}}
              </div>
            </div> 
        @endif
        <!-- Pagination -->
        @include('front.common.pagination_view', ['paginator' => $arr_pagination])
        <!-- ends -->
    </div>
</div>

@if($user != null) 
    @if($user->inRole('expert'))
      <div class="modal fade invite-member-modal" id="colla-upload-file" role="dialog">
          <div class="modal-dialog modal-lg">
             <button type="button" id="btn_close_popup" class="close"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
             <div class="modal-content">
                <div class="modal-body">
                 <h2>Upload File</h2>
                     <div class="invite-form project-found-div">
                          <div class="user-box">
                               <div class="txt-edit"> <b>Enter your file details</b> </div>
                               <span class="txt-edit" id="colla_desc_msg">
                                {{trans('client/projects/post.text_project_limit_description')}}
                               </span>
                               <input type="hidden" id="project_id" id="project_id" value="{{base64_encode($project_id)}}">
                               <div class="input-name">
                                  <textarea class="client-taxtarea beginningSpace_restrict" rows="5" data-rule-required="true" data-rule-maxlength="1000" data-txt_gramm_id="21e619fb-536d-f8c4-4166-76a46ac5edce" onkeyup="javascript: return textCounter(this,1000);" id="colla_file_desc" name="colla_file_desc" placeholder="{{ trans('contets_listing/listing.text_Provide_general_info_about_your_entry_e_g_what_you_can_deliver_and_when_why_you_think_you_can_do_this_contest_etc') }}"></textarea><span class="error err" id="err_colla_file_desc"></span>
                               </div>
                               <div class="input-name">
                                  <div class="txt-edit"> <b>{{ trans('contets_listing/listing.text_attachment_instructions') }} :</b> </div>
                                  <!--image upload start-->
                                  <div class="fallback dfvdfvdfvdf dropzone-section" id="dropzone_image_div">
                                     <form action="{{url('/')}}/expert/projects/collaboration/upload/files" class="dropzone" id="dropzonewidget">
                                      {{csrf_field()}}
                                      <div id="dropzone_append_id"></div>
                                    </form>
                                      <div class="post-img-note-block">
                                        <div class="error" id="err_image_file"></div>
                                        <label id="basic-error" class="validation-error-label" for="basic"></label>
                                        <div class="note-block">
                                            <b>{{ trans('contets_listing/listing.text_note') }} : </b> 
                                            {{ trans('contets_listing/listing.text_select_atleast_one_image_to_upload_Only_jpg_png_jpeg_images_allowed_with_max_size_1mb_You_can_upload_upto_5_images_at_a_time') }}
                                        </div>
                                       </div>
                                  </div>
                              </div>
                              <div class="clearfix"></div>
                              <hr>
                              <div class="input-name">
                                  <div class="terms">
                                      <div class="check-box">{{-- wrk-done-by-me --}}
                                         <p>
                                            {{--<input class="filled-in" type="checkbox" name="wrk-done-by-me" id="wrk-done-by-me">
                                           <label for="wrk-done-by-me"></label>--}}                         
                                            <div class="radio-buttons">
                                              <input class="filled-in " type="radio" value="0" name="wrk-done-by-me" >
                                              <font class="txt-edit">{{ trans('contets_listing/listing.text_all_work_was_done_by_me_or_our_company') }}</font>
                                            </div>
                                            <div class="radio-buttons">
                                              <input class="filled-in" type="radio" value="1" name="wrk-done-by-me" >
                                              <font class="txt-edit">{{ trans('contets_listing/listing.text_the_work_was_partially_done_by_me_or_our_company') }}</font >
                                            </div>
                                            {{-- <div class="error" id="wrk-done-by-me-err"></div>
                                            <label class="error" for="wrk-done-by-me"></label> --}} 
                                            <div id="wrk-done-by-me-err" class="error"></div> 
                                        </p>
                                      </div>
                                        {{--<div class="temsp-d">
                                        <span class="txt-edit"><b>All work is done by me</b></span> <small>
                                      </div> --}}
                                      {{-- <div id="wrk-done-by-me-err" class="error"></div>  --}}
                                  </div>    
                               </div>
                          </div>
                          <div class="clearfix"></div>
                          <button type="button" id="cancel_btn" style="width: 185px;" class="black-border-btn inline-btn">{{ trans('contets_listing/listing.text_cancel') }}</button>
                          <button type="button" id="btn_post_colla_images" class="black-btn inline-btn" style="height: auto;">Upload</button>
                     </div>
                </div>
             </div>
          </div>
      </div>
    @endif
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn_post_colla_images').click(function(){
            $("#wrk-done-by-me-err").html('');
            if($("input[name='wrk-done-by-me']:checked").val()) {}
            else{    
                $("#wrk-done-by-me-err").html('This field is required.');
                return false;
            } 
        });
    });
    function textCounter(field,maxlimit){
      var countfield = 0;
      if( field.value.length > maxlimit ){
        field.value = field.value.substring( 0, maxlimit );
        return false;
      } else {
        countfield = maxlimit - field.value.length;
        var message = '{{trans('client/projects/invite_experts.text_you_have_left')}} <b>'+countfield+'</b> {{trans('client/projects/invite_experts.text_characters_for_description')}}';
        jQuery('#colla_desc_msg').html(message);
        return true;
      }
    }
    Dropzone.options.dropzonewidget = {
      addRemoveLinks: true,
      maxFiles: 5,
      parallelUploads: 5,
      uploadMultiple: true,
      maxFilesize: 5,
      autoProcessQueue: false,
      acceptedFiles: '.jpg, .JPG, .png, .PNG, .jpeg, .JPEG,.doc,.docx,.odt,.pdf,.txt,.xlsx',
      dictMaxFilesExceeded: "{{ trans('contets_listing/listing.text_you_can_upload_only_5_images_at_a_time') }}",
      dictFileTooBig: "{{ trans('contets_listing/listing.text_image_is_too_large_please_upload_upto_1mb_image') }}",
      dictDefaultMessage: "{{ trans('contets_listing/listing.text_drop_your_images_here_to_upload') }}",
      success: function( file, response )  {
        hideProcessingOverlay(); 
        if(response.status=='error') {
          swal("",response.message,'error');
          this.removeFile(file); 
          $('#colla_file_desc').val();
        } else {   
            var msg = response.message;
            swal("",msg,'success');
            this.removeFile(file); 
            $('#colla_file_desc').val('');
            $("#colla-upload-file").modal('hide');  
            setTimeout(function(){
            location.reload();
            },500);  
        }
      },
      init: function() {
        var myDropzone = this;
        this.on("error", function(file, message) { 
          swal("",message,'error');
          this.removeFile(file); 
        });
        $('#btn_post_colla_images').on("click", function(){
          
          if($("#colla_file_desc").val()=="") {
            $("#err_colla_file_desc").html('{{ trans('contets_listing/listing.text_this_field_is_required') }}');
          }else if(!$("input[name='wrk-done-by-me']:checked").val()){    
                $("#wrk-done-by-me-err").html('This field is required.');
                return false;
            } else {
              if (!myDropzone.files || !myDropzone.files.length) 
              {
                  $('#err_image_file').html('{{ trans('contets_listing/listing.text_this_field_is_required') }}');
                  $("#err_colla_file_desc").html('');
                  return false;
              } else {
                  myDropzone.processQueue();  
                  $('#err_colla_file_desc').val('');
                  $('#colla_file_desc').html(''); 
              }
          }

          $('#basic-error').html('');    
        });
        myDropzone.on("sending", function(file, xhr, data) {
        showProcessingOverlay();
          data.append("_token", $('meta[name="token"]').attr('content'));
          data.append("project_id",$("#project_id").val());
          data.append("colla_file_desc", $('#colla_file_desc').val());
          if($("input[name='wrk-done-by-me']:checked").val())
          {
          data.append("wrk-done-by-me", $("input[name='wrk-done-by-me']:checked").val());
          }
        });
      }
    };
    function makeStatusMessageHtml(status, message){
        str = '<div class="alert alert-'+status+' alert-dismissable"><a class="close" data-dismiss="alert" aria-label="close">×</a><strong>'+status+' :</strong>'+message+'</div>';
        return str;
    }
    $("#btn_close_popup,#cancel_btn").on('click',function(e)
    {
        $('#colla_file_desc').val('');
        $("#err_colla_file_desc").html(''); 
        $('#err_image_file').html('');
        var myDropzone = Dropzone.forElement("#dropzonewidget");
        myDropzone.removeAllFiles(true); 
        $("#colla-upload-file").modal('hide');
    });
</script>  
@endif  
@stop