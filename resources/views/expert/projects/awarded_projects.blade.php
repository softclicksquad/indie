@extends('expert.layout.master')
@section('main_content')
@php 
    $user = Sentinel::check(); 
@endphp
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" integrity="sha256-Z8TW+REiUm9zSQMGZH4bfZi52VJgMqETCbPFlGRB1P8=" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.js" integrity="sha256-ZvMf9li0M5GGriGUEKn1g6lLwnj5u+ENqCbLM5ItjQ0=" crossorigin="anonymous"></script>
<div class="col-sm-8 col-md-8 col-lg-9">
    <div class="right_side_section proect-listing">
        @include('front.layout._operation_status')
        
        <div class="head_grn">{{trans('expert/projects/awarded_projects.text_awarded_project_title')}}</div>
        @if(isset($arr_awarded_projects['data']) && sizeof($arr_awarded_projects['data'])>0)
        @foreach($arr_awarded_projects['data'] as $key=>$proRec)
        <div class="white-block-bg-no-padding">
            @if(isset($proRec['project_details']))
            <div class="white-block-bg hover awarded-project">
                <div class="project-left-side">
                   
                        <div class="search-freelancer-user-head">
                            <a href="{{ $module_url_path }}/details/{{ base64_encode($proRec['project_details']['id'])}}">
                                {{$proRec['project_details']['project_name']}} <span style="font-size: 13px;color: #53423B;">(&nbsp;{{ trans('new_translations.bid_proposal')}}&nbsp;)</span>
                            </a>
                        </div>
                        <?php
                            $bid_estimated_duration   =  isset($proRec['project_details']['project_bid_info']['bid_estimated_duration']) ? $proRec['project_details']['project_bid_info']['bid_estimated_duration']:'---';
                            $bid_cost           =  isset($proRec['project_details']['project_bid_info']['bid_cost']) ? $proRec['project_details']['project_bid_info']['bid_cost'] : '---';
                            $bid_description    =  isset($proRec['project_details']['project_bid_info']['bid_description']) ? $proRec['project_details']['project_bid_info']['bid_description']:'---';
                            $project_currency  = isset($proRec['project_details']['project_currency']) ? $proRec['project_details']['project_currency']:'---';
                          ?>

                        <div class="search-freelancer-user-location">
                            <span class="gavel-icon"><img src="{{url('/public')}}/front/images/bid.png" alt="" /></span>
                            <span class="dur-txt"> {{trans('expert/projects/awarded_projects.text_awarded_on')}} : @if($proRec['created_at'] != NULL ) {{date('d M Y',strtotime($proRec['created_at']))}} @else --- @endif</span>
                        </div>
                        
                         <div class="search-freelancer-user-location">
                            <span class="gavel-icon"><img src="{{url('/public')}}/front/images/clock.png" alt="" /></span>
                            <span class="dur-txt"> {{ trans('new_translations.estimated_duration')}} : {{$bid_estimated_duration}} {{ trans('new_translations.days')}}</span>
                        </div>

                    <div class="sub-project-dec">
                        <span class="sub-pro-span">{{ trans('new_translations.bid_description')}} :</span> {{ $bid_description or '' }}
                    </div>

           
               
                    <div class="sub-project-dec">
                        <span class="sub-pro-span">
                            {{ trans('new_translations.project_description')}} :
                        </span>
                        {{ isset($proRec['project_details']['project_description']) ? str_limit($proRec['project_details']['project_description'],340):'' }}
                        @if(isset($proRec['project_details']['project_description']) && strlen( $proRec['project_details']['project_description']) > 340 )<a href="{{ $module_url_path }}/details/{{ base64_encode($proRec['project_details']['id'])}}">{{ trans('new_translations.more')}}</a> @endif
                    </div>

                    <?php
                      $projects_cost = explode('-',$proRec['project_details']['project_cost']);
                      if( (isset($projects_cost[0]) && $projects_cost[0]!='') && (isset($projects_cost[1]) && $projects_cost[1]!=''))
                      {
                          $projects_cost = number_format(floor($projects_cost[0])).'-'.number_format(floor($projects_cost[1]));
                      }
                      elseif($projects_cost[0]!='')
                      {
                          $projects_cost = number_format(floor($projects_cost[0]));
                      }

                      $text_hour = '';
                      if(isset($proRec['project_details']['project_pricing_method']) && $proRec['project_details']['project_pricing_method'] == '2'){
                        $text_hour = trans('common/_top_project_details.text_hour');
                      }
                    ?>
                    <div class="project-list timing-details">
                        <ul>
                            <li title="Expected Duration">
                                <span class="" title="Expected Days">
                                   <img src="{{url('/public')}}/front/images/clock.png" alt="" />
                                </span>Expected Duration: {{ $proRec['project_details']['project_expected_duration'] }}
                            </li>
                            <li style="background:none;" title="Project Cost">
                                <span class="projrct-prce">
                                    @if(isset($proRec['project_details']['project_currency']) && $proRec['project_details']['project_currency']=="$")
                                        <img src="{{url('/public')}}/front/images/money.png" alt="" />
                                        
                                        {{ isset($proRec['project_details']['project_currency'])?$proRec['project_details']['project_currency']:"" }} {{$projects_cost }}{{$text_hour}}

                                        @if($proRec['project_details']['project_currency_code'] != $user->currency_code)
                                            @if(isset($proRec['project_details']['is_custom_price']) && $proRec['project_details']['is_custom_price']==0)
                                                ({{ $user->currency_code ?? '' }} {{ number_format(floor(currencyConverterAPI($proRec['project_details']['project_currency_code'],$user->currency_code,$proRec['project_details']['min_project_cost']))) }}-{{ number_format(floor(currencyConverterAPI($proRec['project_details']['project_currency_code'],$user->currency_code,$proRec['project_details']['max_project_cost']))) }}{{$text_hour}})
                                            @else
                                                ({{ $user->currency_code ?? '' }} {{ number_format(floor(currencyConverterAPI($proRec['project_details']['project_currency_code'],$user->currency_code,$proRec['project_details']['min_project_cost']))) }}{{$text_hour}})
                                            @endif
                                        @endif

                                    @else
                                        <img src="{{url('/public')}}/front/images/money.png" alt="" />
                                        {{ isset($proRec['project_details']['project_currency'])?$proRec['project_details']['project_currency']:"" }} {{$projects_cost }}{{$text_hour}}
                                        @if($proRec['project_details']['project_currency_code'] != $user->currency_code)
                                            @if(isset($proRec['project_details']['is_custom_price']) && $proRec['project_details']['is_custom_price']==0)
                                                ({{ $user->currency_code ?? '' }} {{ number_format(floor(currencyConverterAPI($proRec['project_details']['project_currency_code'],$user->currency_code,$proRec['project_details']['min_project_cost']))) }}-{{ number_format(floor(currencyConverterAPI($proRec['project_details']['project_currency_code'],$user->currency_code,$proRec['project_details']['max_project_cost']))) }}{{$text_hour}})
                                            @else
                                                ({{ $user->currency_code ?? '' }} {{ number_format(floor(currencyConverterAPI($proRec['project_details']['project_currency_code'],$user->currency_code,$proRec['project_details']['max_project_cost']))) }}{{$text_hour}})
                                            @endif
                                        @endif
                                                
                                    @endif
                                </span>
                            </li>
                        </ul>
                    </div>
                    <div class="category-new">
                        <img src="{{url('/public')}}/front/images/tag-2.png" alt="" /> {{$proRec['project_details']['category_details']['category_title'] or 'NA'}}
                        &nbsp;&nbsp;
                        <img src="{{url('/public')}}/front/images/tag-2.png" alt="" /> {{isset($proRec['project_details']['sub_category_details']['subcategory_title'])?$proRec['project_details']['sub_category_details']['subcategory_title']:'NA'}}
                    </div>
                    <div class="clearfix"></div>
                    @if(isset($proRec['project_details']['project_skills']) && count($proRec['project_details']['project_skills']) > 0)
                        <div class="skils-project">
                              <img src="{{url('/public')}}/front/images/tag.png" alt="" />
                            <ul>
                                @if(isset($proRec['project_details']['project_skills']) && count($proRec['project_details']['project_skills']) > 0)
                                @foreach($proRec['project_details']['project_skills'] as $key=>$skills)

                                @if(isset($skills['skill_data']['skill_name']) && $skills['skill_data']['skill_name'] != "")
                                <li>{{ str_limit($skills['skill_data']['skill_name'],25) }}</li>
                                @endif
                                @endforeach
                                @endif
                            </ul>
                        </div>
                        @endif
                  
             
            </div>
                
           <div class="search-project-listing-right contest-right">
                <div class="search-project-right-price">
                    {{$project_currency}} {{ isset($bid_cost)?number_format($bid_cost):'' }}{{$text_hour}}
                    <span>{{ get_currency_description($proRec['project_details']['project_currency_code']) ?? '' }}</span>
                </div>
                @if($proRec['project_details']['project_currency_code'] != $user->currency_code)
                    <div class="default-currency-price">
                      ({{ $user->currency_code ?? '' }} {{ number_format(floor(currencyConverterAPI($proRec['project_details']['project_currency_code'],$user->currency_code,$bid_cost))) }}{{$text_hour}})
                    </div>
                @endif
            </div>
                
            <div class="invite-expert-btns">
                <a class="black-border-btn" href="{{ $module_url_path }}/details/{{ base64_encode($proRec['project_details']['id'])}}">
                    <span class="">{{ trans('new_translations.view')}}</span>

                </a>
                <a href="javascript:void(0)" class="applozic-launcher black-border-btn" data-mck-id="{{env('applozicUserIdPrefix')}}{{$proRec['client_user_id']}}" data-mck-name="{{isset($proRec['client_details']['role_info']['first_name'])?$proRec['client_details']['role_info']['first_name']:'Unknown user'}} 
                                            @if(isset($proRec['client_details']['role_info']['last_name']) && $proRec['client_details']['role_info']['last_name'] !="") @php echo substr($proRec['client_details']['role_info']['last_name'],0,1).'.'; @endphp @endif">
                    {{trans('expert/projects/ongoing_projects.text_message_chat')}}
                </a>
                <a class="black-border-btn blue_btn" onclick="confirmaccept(this)" data-project-id="{{ isset($proRec['project_details']['id'])?base64_encode($proRec['project_details']['id']):'' }}">
                    <span class=""> {{trans('expert/projects/awarded_projects.text_accept')}} </span>

                </a>
                <a class="black-btn" onclick="rejectProject(this)" data-project-id="{{ isset($proRec['project_details']['id'])?base64_encode($proRec['project_details']['id']):'' }}">
                    <span class=""> {{trans('expert/projects/awarded_projects.text_reject')}} </span>

                </a>
            </div>
                
              
                
            <div class="clearfix"></div>
            <?php 
                    $arr_bid_data               = [];
                    $arr_bid_data               = $proRec['project_details']['project_bid_info'];
                    $bid_attachment_public_path = '';
                  ?>
        </div>
        @endif
    </div>
    @endforeach
    @else
    <div class="search-grey-bx">
        <div class="no-record">
            {{trans('expert/projects/awarded_projects.text_sorry_no_record_found')}}
        </div>
    </div>
    @endif
    <div class="clearfix"></div>
</div>
</div>
</div>
</div>
@include('front.common.pagination_view', ['paginator' => $arr_pagination])
{{-- tag to open modal --}}
<a data-toggle="modal" style="display: none;" data-target="#myModal" id="open_modal"></a>
{{-- ends --}}
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 style="color: #0E99E5;" class="modal-title"><b>
                        {{ trans('new_translations.reject_project_bid_title')}}
                    </b></h4>
            </div>
            <div class="modal-body">
                <form method="post" id="frm_reject_modal" action="{{url('/public')}}/expert/projects/awarded/reject">
                    <div class="row">
                        {{csrf_field()}}
                        <input type="hidden" readonly="" name="project_id" id="modal_project_id" value="" />

                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="user-bx">
                                <textarea cols="" rows="" name="rejection_reason" id="rejection_reason" class="det-in1" style="font-size: 14px;font-style:normal;color: #3B3B3D;" placeholder="{{ trans('new_translations.enter_comments')}}.."></textarea>
                                <div id="chk" style="display: none;" class="error">{{ trans('new_translations.this_field_is_required')}}</div>
                            </div>
                            <button type="button" id="submit-btn" class="sb-det-btn">{{ trans('new_translations.submit')}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Pagination -->
<!-- ends -->
<script type="text/javascript">
    /* Accept project code */
    function confirmaccept(ref) {
        var confirm_message = "{!! trans('expert/projects/awarded_projects.confirm_accept_project') !!}";
        swal({
                    title: confirm_message,
                    text: "",
                    icon: "warning",
                    dangerMode: true,
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No",
                    closeOnConfirm: false,
                    closeOnCancel: false
                 },
                 function(isConfirm){

                   if (isConfirm){
                    var project_id = $(ref).attr('data-project-id');
                    showProcessingOverlay();
                    window.location.href = "{{ $module_url_path }}/awarded/accept/" + project_id;
                    return true;

                    } else {
                        swal.close();
                      return false;
                    }
                 });
    }
    /* reject project code */
    $('#submit-btn').click(function() {
        if ($('#rejection_reason').val() == '') {
            $('#chk').show();
            return false;
        } else if ($('#rejection_reason').val() != '') {
            swal({
                    title: "Are you sure? You want to reject project ?",
                    text: "",
                    icon: "warning",
                  
                    dangerMode: true,
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No",
                    closeOnConfirm: false,
                    closeOnCancel: false
                 },
                 function(isConfirm){

                   if (isConfirm){
                     showProcessingOverlay();
                      $('#frm_reject_modal').submit();
                        return true;

                    } else {
                        swal.close();
                      return false;
                    }
                 });
        }
    });

    function rejectProject(ref) {
        var project_id = $(ref).attr('data-project-id');
        if (project_id.trim() != "") {
            $('#open_modal').click();
            $('#modal_project_id').prop('value', project_id.trim());
        } else {
            $('#chk').html('Error while rejecting project. Please try after some time.');
            return false;
        }
    }
</script>
@stop