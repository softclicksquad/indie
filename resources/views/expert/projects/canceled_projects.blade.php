@extends('expert.layout.master')                
@section('main_content')
<div class="col-sm-8 col-md-8 col-lg-9">
      <div class="proect-listing">
         @include('front.layout._operation_status')
         <div class="head_grn">{{trans('expert/projects/canceled_projects.text_canceled_project_title')}}</div>
         @if(isset($arr_canceled_projects['data']) && sizeof($arr_canceled_projects['data'])>0)
         @foreach($arr_canceled_projects['data'] as $proRec)
         <div class="white-block-bg big-space hover">
            <div class="search-freelancer-user-head">
               <a href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}">
                 {{$proRec['project_name']}}
               </a>
                </div>

            
                
                 <div class="search-freelancer-user-location">
                            <span class="gavel-icon"><img src="{{url('/public')}}/front/images/clock.png" alt="" /></span>
                            <span class="dur-txt"> {{trans('expert/projects/canceled_projects.text_est_time')}} : {{$proRec['project_expected_duration']}}</span></div>
                

               <div class="more-project-dec">{{str_limit($proRec['project_description'],340)}}</div>
             <div class="category-new">
              <img src="{{url('/public')}}/front/images/tag-2.png" alt="" />{{$proRec['category_details']['category_title'] or 'NA'}}
                &nbsp;&nbsp;
              <img src="{{url('/public')}}/front/images/tag-2.png" alt="" /> {{isset($proRec['sub_category_details']['subcategory_title'])?$proRec['sub_category_details']['subcategory_title']:'NA'}}
              </div>
                    <div class="clearfix"></div>
            <div class="row">
               <div class="col-sm-12 col-md-11 col-lg-11 skill-de-content">
                  <div class="skils-project">
                     <ul>
                        @if(isset($proRec['project_skills'])  && count($proRec['project_skills']) > 0)
                        @foreach($proRec['project_skills'] as $key=>$skills) 
                        @if(isset($skills['skill_data']['skill_name']) &&  $skills['skill_data']['skill_name'] != "")
                        <li style="font-size:12px; " >{{ str_limit($skills['skill_data']['skill_name'],25) }}</li>
                        @endif
                        @endforeach
                        @endif    
                     </ul>
                  </div>
               </div>
               <div class="search-project-listing-right contest-right">
                  <div class="search-project-right-price">
                     {{ isset($proRec['project_currency']) && $proRec['project_currency']!=''?$proRec['project_currency']:'$'}}
                      {{isset($proRec['project_cost'])?$proRec['project_cost']:'0'}} 
                  </div>
               </div>
               
               <div class="col-md-12">
                 <div class="mrg-btm">
                  <a class="black-border-btn"href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}">{{--  class="view_btn hidden-xs hidden-sm hidden-md"> --}}
                
                 {{trans('expert/projects/canceled_projects.text_view')}} 
                 
                  </a>
                  {{-- <a class="black-btn" href="{{ $module_url_path }}/milestones/{{ base64_encode($proRec['id'])}}"  > --}}
                  
                  {{-- <i class="fa fa-money" aria-hidden="true"></i> --}}{{trans('expert/projects/canceled_projects.text_milestones')}} 
                 {{-- {{trans('expert/projects/canceled_projects.text_view')}} --}} 
                  {{-- </a> --}}
                  </div>
               </div>
            </div>
         </div>
         <hr/>
         @endforeach
         @else
         <div class="search-grey-bx">
              <div class="no-record" >
                 {{trans('expert/projects/awarded_projects.text_sorry_no_record_found')}}
              </div>
         </div>
         @endif
      </div>
   <!-- Paination Links -->
   @include('front.common.pagination_view', ['paginator' => $arr_canceled_projects])
   <!-- Paination Links -->
</div>
@stop