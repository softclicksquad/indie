@extends('expert.layout.master')
@section('main_content')
@php 
    $user = Sentinel::check(); 
@endphp
<div class="col-sm-8 col-md-8 col-lg-9">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="right_side_section proect-listing">
                @include('front.layout._operation_status')
                <div class="head_grn">{{trans('expert/projects/ongoing_projects.text_ongoing_project_title')}}</div>
                @if(isset($arr_ongoing_projects['data']) && sizeof($arr_ongoing_projects['data'])>0)
                @foreach($arr_ongoing_projects['data'] as $proRec)

                <div class="white-block-bg big-space hover">
                    <div class="project-left-side">
                        <div class="search-freelancer-user-head">
                            <div class="min-block-time pull-right">
                                <?php 
                          $next_milestone       = '0000-00-00 00:00:00';
                          $expired_milestone    = 0;
                          $chk_released         = 0;
                          foreach ($proRec['project_milestones'] as $key => $project_next_milestones) {
                            $current_datetime   = date('Y-m-d H:i:s'); 
                            $chk_released       = \DB::table('milestone_release')->where('status','2')->where('milestone_id',$project_next_milestones['id'])->count();
                            $chk_refund         = \DB::table('milestone_refund_requests')->where('milestone_id',$project_next_milestones['id'])->count();
                            if($chk_released != '1' && $chk_refund == 0){
                              if($current_datetime < $project_next_milestones['milestone_due_date']) {
                                $next_milestone = $project_next_milestones['milestone_due_date'];
                              } else if($current_datetime > $project_next_milestones['milestone_due_date']) {
                                $expired_milestone +=1; 
                              }
                            }                         
                          }

                            $text_hour = '';
                              if(isset($proRec['project_pricing_method']) && $proRec['project_pricing_method'] == '2'){
                                $text_hour = trans('common/_top_project_details.text_hour');
                              }

                        ?>
                                @if($expired_milestone > 0)
                                <div id="mins" class="count-min-block">
                                    {{trans('client/projects/ongoing_projects.text_next_expiremilestone')}} : <span style="color:#FF0000">{{$expired_milestone}}</span>/{{count($proRec['project_milestones'])}}</span>
                                </div>
                                @endif
                                @if(isset($next_milestone) && $next_milestone != "" && $next_milestone != '0000-00-00 00:00:00')
                                <div id="mins" class="count-min-block">
                                    {{trans('expert/projects/ongoing_projects.text_next_milestone')}} : <span id='timer{{$proRec["id"]}}' style="color:#FF0000">0 s</span>
                                </div>
                                <?php

                                    $arr_diff_between_two_date = get_array_diff_between_two_date(date('Y-m-d H:i:s'),$next_milestone);
                                    $str_days_diff = isset($arr_diff_between_two_date['str_days_diff']) ? $arr_diff_between_two_date['str_days_diff'] : '';

                                ?>
                                <script type="text/javascript">
                                    
                                    var milestoneid = '<?php echo $proRec["id"]; ?>';
                                    var str_days_diff = '{{ isset($str_days_diff) ? $str_days_diff : '' }}';

                                    if (str_days_diff == "") {
                                        $('#timer' + milestoneid).html('0 s');
                                        $('#timer' + milestoneid).css('color', '#FF0000');
                                    } else {
                                        $('#timer' + milestoneid).html(str_days_diff);
                                        $('#timer' + milestoneid).css('color', '#228B22');
                                    }
                                </script>
                                @endif
                            </div>
                            <a href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}">
                                {{$proRec['project_name']}}
                            </a>
                        </div>


                        <div class="search-freelancer-user-location">
                            <span class="gavel-icon"><img src="{{url('/public')}}/front/images/clock.png" alt="" /></span>
                            <span class="dur-txt"> {{trans('expert/projects/ongoing_projects.text_est_time')}} : {{$proRec['project_expected_duration']}}</span> </div>

                        <div class="search-freelancer-user-location">
                            <span class="gavel-icon"><img src="{{url('/public')}}/front/images/bid.png" alt="" /></span>
                            <span class="dur-txt"> {{trans('expert/projects/ongoing_projects.text_accepted_on')}} : @if($proRec['project_accepted_time'] != NULL ) {{date('d M Y',strtotime($proRec['project_accepted_time']))}} @else --- @endif</span></div>



                        <div class="more-project-dec">{{str_limit($proRec['project_description'],350)}}</div>

                        <!-- <div class="col-sm-12 col-md-1 col-lg-1  skills-de-new"><span class="colrs">{{trans('expert/projects/ongoing_projects.text_skills')}}:</span></div>-->

                        <div class="category-new">
                            <img src="{{url('/public')}}/front/images/tag-2.png" alt="" /> {{$proRec['category_details']['category_title'] or 'NA'}}

                            &nbsp;&nbsp;
                            <img src="{{url('/public')}}/front/images/tag-2.png" alt="" /> {{isset($proRec['sub_category_details']['subcategory_title'])?$proRec['sub_category_details']['subcategory_title']:'NA'}}

                        </div>
                        <div class="clearfix"></div>


                        <div class="skils-project">
                            @if(isset($proRec['project_skills']) && count($proRec['project_skills']) > 0)
                            <img src="{{url('/public')}}/front/images/tag.png" alt="" />
                            @endif
                            <ul>
                                @if(isset($proRec['project_skills']) && count($proRec['project_skills']) > 0)
                                @foreach($proRec['project_skills'] as $key=>$skills)
                                @if(isset($skills['skill_data']['skill_name']) && $skills['skill_data']['skill_name'] != "")
                                <li>{{ str_limit($skills['skill_data']['skill_name'],25) }}</li>
                                @endif
                                @endforeach
                                @endif
                            </ul>
                        </div>

                    </div>

                    <div class="search-project-listing-right contest-right">
                        <div class="search-project-right-price">
                            {{ isset($proRec['project_currency']) && $proRec['project_currency']!=''?$proRec['project_currency']:'$'}} {{isset($proRec['project_bid_info']['bid_cost'])?number_format($proRec['project_bid_info']['bid_cost']):'0'}}{{$text_hour}} 
                            <span>{{ get_currency_description($proRec['project_currency_code']) ?? '' }}</span>
                        </div>
                        @if($proRec['project_currency_code'] != $user->currency_code)
                            <div class="default-currency-price">
                              ({{ $user->currency_code ?? '' }} {{ number_format(floor(currencyConverterAPI($proRec['project_currency_code'],$user->currency_code,$proRec['project_bid_info']['bid_cost']))) }}{{$text_hour}})
                            </div>
                        @endif
                    </div>
                    <div class="invite-expert-btns">
                        <a class="black-border-btn" href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}">{{-- class="view_btn hidden-xs hidden-sm hidden-md"> --}} {{trans('expert/projects/ongoing_projects.text_view')}} </a>
                        <a href="javascript:void(0)" class="applozic-launcher black-border-btn" data-mck-id="{{env('applozicUserIdPrefix')}}{{$proRec['client_user_id']}}" data-mck-name="{{isset($proRec['client_details']['role_info']['first_name'])?$proRec['client_details']['role_info']['first_name']:'Unknown user'}} 
                                    @if(isset($proRec['client_details']['role_info']['last_name']) && $proRec['client_details']['role_info']['last_name'] !="") @php echo substr($proRec['client_details']['role_info']['last_name'],0,1).'.'; @endphp @endif">
                            {{trans('expert/projects/ongoing_projects.text_message_chat')}}
                        </a>

                        @if(isset($proRec['project_milestones']) && count($proRec['project_milestones']) > 0 )
                        <a class="black-btn" href="{{ $module_url_path }}/milestones/{{ base64_encode($proRec['id'])}}">
                            {{trans('expert/projects/ongoing_projects.text_milestones')}}
                        </a>
                        @else
                        <a href="javascript:void(0)" class="black-btn">
                            {{trans('expert/projects/ongoing_projects.text_no_milestones')}}
                        </a>
                        @endif
                        <div class="clearfix"></div>
                    </div>



                    <div class="clearfix"></div>
                </div>

                @endforeach
                @else
                <div class="search-grey-bx">
                    <div class="no-record">
                        {{trans('expert/projects/awarded_projects.text_sorry_no_record_found')}}
                    </div>
                </div>
                @endif
            </div>
            <!-- Paination Links -->
            @include('front.common.pagination_view', ['paginator' => $arr_ongoing_projects])
            <!-- Paination Links -->
        </div>
    </div>
</div>
@stop