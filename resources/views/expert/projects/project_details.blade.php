  @extends('expert.layout.master')                
  @section('main_content')
  
  <link rel="stylesheet" href="{{url('/public')}}/assets/rating-master/fontawesome-stars.css">
  <style type="text/css">
    table { border-collapse: collapse; }
    td, th {padding: 0.5rem; text-align: left;  }
    td{border-top: 0px !important;}
  </style>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" integrity="sha256-Z8TW+REiUm9zSQMGZH4bfZi52VJgMqETCbPFlGRB1P8=" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.js" integrity="sha256-ZvMf9li0M5GGriGUEKn1g6lLwnj5u+ENqCbLM5ItjQ0=" crossorigin="anonymous"></script>
  <?php $user = Sentinel::check();?>  
  <script type="text/javascript" src="{{url('/public')}}/assets/rating-master/jquery.barrating.min.js"></script>
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">

                @php
                  $diffInSeconds = '0';

                  if(isset($request_time) && $request_time!='')
                  {      
                      $expired_time = strtotime(date('Y-m-d H:i:s',strtotime('+2day', strtotime($request_time))));
                      $current_time = strtotime(date('Y-m-d H:i:s'));
                      $diffInSeconds = $expired_time - $current_time;

                      if($diffInSeconds <= 0){
                          $diffInSeconds = '0';
                      }
                  }
                @endphp


                <?php 
                  if(isset($arr_bid_data))
                  {
                    if(isset($arr_project_info['project_status']) && $arr_project_info['project_status'] == '2' &&  isset($arr_bid_data['bid_status']) && $arr_bid_data['bid_status'] == '4')
                    {
                ?>
                    <div class="alert alert-info alert-dismissible">
                        <img style="height:21px; margin-top:-4px;" src="{{url('/public/front/images/information-icon-th.png')}}"> Congratulations {{$arr_bid_data['expert_details']['first_name'] or ''}}, 
                        {{trans('controller_translations.project_request_to_expert_sent_message')}}   Remaining time: <span id="days_left_timer"></span>
                    </div>
                <?php
                    }
                }
                ?> 

                <script type="text/javascript">
                    var time        = '<?php echo $diffInSeconds; ?>';
                    if(time < 0) {  time = 0; }

                    var c           = time;
                    var t;
                    var days    = parseInt( c / 86400 ) % 365;
                    var hours   = parseInt( c / 3600 ) % 24;
                    var minutes = parseInt( c / 60 ) % 60;
                    var seconds = c % 60;

                    var d = ""; 
                    var h = ""; 
                    var m = "";
                    var s = ""; 

                    if(days     !=  ""  &&  days     !="0"){  d  =  (days+'d')     +  " ";  }
                    if(hours    !=  ""  &&  hours    !="0"){  h  =  (hours+'h')    +  " ";  }
                    if(minutes  !=  ""  &&  minutes  !="0"){  m  =  (minutes+'m')  +  " ";  }
                    if(seconds  !=  ""  &&  seconds  !="0"){  s  =  (seconds+'s')  +  " ";  }

                    if(d >= '2d' && (h>'0h' || m>'0m' || s>'0s')){
                        var result = "EXPIRED"
                    }
                    else if(d != "" && h == ""){
                        var result    = d;
                    }
                    else if(d != "" && h !=""){
                        var result    = d + h;
                    }
                    else if(h != "" && m !=""){
                        var result    = h + m;
                    }
                    else if(m !=""){
                        var result    = m;
                    }
                    else if(s !=""){
                        var result    = s;
                    }

                    if(result == "NaN:NaN:NaN:NaN"){ 
                        $('#days_left_timer').html('EXPIRED');
                        $('#days_left_timer').css('color','red');
                    }
                    else if(result == 'EXPIRED'){
                        $('#days_left_timer').html(result);
                        $('#days_left_timer').css('color','red');
                    }
                    else{
                        $('#days_left_timer').html(result);
                        $('#days_left_timer').css('font-weight','bold');
                    }

                    if(c == 0){
                        $('#days_left_timer').html('EXPIRED');
                        $('#days_left_timer').css('color','red');
                    }
                    c = c - 1;
                </script>




   
                 <!-- Including top view of project details - Just pass project information array to the view -->
                  @include('front.common._top_project_details',['projectInfo'=>$arr_project_info,'arr_milestone'=>$count_milestone])
                 <!-- Ends -->  
                 {{-- Bid Information --}}
                  @if(isset($arr_bid_data) && count($arr_bid_data) > 0 )
                      <div class="search-grey-bx projt-detls">
                        <div class="row">
                           <div class="col-sm-12 col-md-9 col-lg-9">
                              <div class="going-profile-detail">
                                 <div class="going-pro-content" style="margin-left: 0px;">
                                    <div class="profile-name">{{trans('expert/projects/project_details.text_your_bid_proposal')}}</div>
                                    <div style="margin-bottom: 12px;"></div>
                                    <div class="more-project-dec" style="height:auto;">
                                      {{isset($arr_bid_data['bid_description'])?$arr_bid_data['bid_description']:''}}
                                    <div class="clr"></div>
                                    <br/>
                                    </div>
                                 </div>
                              </div>
                              @if(isset($arr_bid_data['bid_attachment']) && trim($arr_bid_data['bid_attachment']) != "" && file_exists('public/uploads/front/bids/'.$arr_bid_data['bid_attachment']))
                                  <div class="user-box" style="max-width: 300px; width: 100%;">
                                  <div class="input-name">
                                      <div class="upload-block">
                                        <div class="">
                                            <!-- <input type="text" class="form-control file-caption  kv-fileinput-caption" id="profile_image_name" disabled="disabled" value="{{$arr_bid_data['bid_attachment'] or ''}}" > -->
                                            <a title="Download Previous Proposal" download href="{{$bid_attachment_public_path}}{{$arr_bid_data['bid_attachment']}}" >
                                              <div class="p-control-label"><i class="fa fa-paperclip"></i> {{ trans('expert/projects/add_project_bid.text_attachment') }} : <span style="color: #0E81C2;"><i class="fa fa-download"></i></span></div>
                                            </a>    
                                         </div>
                                      </div>
                                      <div id="msg_bid_attach" style="color: red;font-size: 12px;font-weight:600;display: none;"></div>
                                      <div class="user-box hidden-lg">&nbsp;</div>
                                      <div class="note_browse"></div>
                                   </div>
                                  </div>  
                               @endif  
                               @if(isset($arr_bid_data['nda_aggreement_pdf']) && trim($arr_bid_data['nda_aggreement_pdf']) != "" && file_exists('public/uploads/front/nda-agreement/'.$arr_bid_data['nda_aggreement_pdf']))
                               <div class="user-box" style="max-width: 300px; width: 100%;">
                                  <div class="input-name">
                                      <div class="upload-block">
                                        <div class="">
                                            <a title="Download Previous Proposal" download href="{{url('/public/uploads/front/nda-agreement/')}}/{{$arr_bid_data['nda_aggreement_pdf']}}" >
                                              <div class="p-control-label"><i class="fa fa-paperclip"></i> NDA agreement : <span style="color: #0E81C2;"><i class="fa fa-download"></i></span></div>
                                            </a>    
                                         </div>
                                      </div>
                                      <div id="msg_bid_attach" style="color: red;font-size: 12px;font-weight:600;display: none;"></div>
                                      <div class="user-box hidden-lg">&nbsp;</div>
                                      <div class="note_browse"></div>
                                   </div>
                               </div> 
                               @endif  
                           </div>
                           <?php
                              $text_hour = '';
                              if(isset($arr_project_info['project_pricing_method']) && $arr_project_info['project_pricing_method'] == '2'){
                                $text_hour = trans('common/_top_project_details.text_hour');
                              }
                           ?>
                           <div class="col-sm-12 col-md-4 col-lg-3 ">
                              <div class="rating-profile1 br-left" style="margin-top: -10px; padding-left: 10px;">
                                <div title="Bid Cost" class="projrct-prce1">
                                  @if(isset($arr_project_info['project_currency']) && $arr_project_info['project_currency']=="$")
                                    <span>
                                    <img src="{{url('/public')}}/front/images/doller-img.png" alt="img"/> 
                                  </span><span>{{$arr_project_info['project_currency_code']}} {{isset($arr_bid_data['bid_cost'])?number_format($arr_bid_data['bid_cost']):0}}{{$text_hour}}</span>
                                  @else
                                    <span>
                                    <img src="{{url('/public')}}/front/images/doller-img-new-1.png" alt="img"/> 
                                  </span><span>{{$arr_project_info['project_currency_code']}} {{isset($arr_bid_data['bid_cost'])?number_format($arr_bid_data['bid_cost']):0}}{{$text_hour}}</span>
                                  @endif
                                </div>

                                @if($arr_project_info['project_currency_code'] != $user->currency_code)
                                <div class="default-currency-price">
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;({{ $user->currency_code ?? '' }} {{ number_format(floor(currencyConverterAPI($arr_project_info['project_currency_code'],$user->currency_code,$arr_bid_data['bid_cost']))) }}{{$text_hour}})
                                </div>
                                @endif

                                <div title="Project Duration" class="projrct-prce1"><span><img src="{{url('/public')}}/front/images/bid-time.png" alt="img"/> </span>
                                  {{isset($arr_bid_data['bid_estimated_duration'])?$arr_bid_data['bid_estimated_duration']:0}}&nbsp;{{ trans('new_translations.days')}}
                                </div>
                              </div>
                           </div>
                        </div>
                      </div>
                  @endif
                {{-- Bid Information Ends --}}
                {{-- Review Display--}}
                @if(isset($arr_review_details) && sizeof($arr_review_details)>0 && $arr_project_info['project_status'] == '3' ) 
                   @foreach($arr_review_details as $review)
                   <?php
                      if($review['from_user_id'] == $user->id)
                      {
                        $profile_image  = isset($review['from_user_info']['profile_image']) ?$review['from_user_info']['profile_image']:"default_profile_image.png";
                        $first_name     = isset($review['from_user_info']['first_name']) ?$review['from_user_info']['first_name']:"";
                        $last_name      = isset($review['from_user_info']['last_name']) ?substr($review['from_user_info']['last_name'], 0,1).'.':""; 
                        $user_name      = $first_name.' '.$last_name;
                        $title    = trans('expert/projects/completed_projects.text_your_rating');
                        $chk_user = Sentinel::findById($review['from_user_id'] );
                        /*if(isset($chk_user) && $chk_user != FALSE) {
                            if(isset($chk_user->user_name)) {  
                               $user_name = $chk_user->user_name;
                            }
                        }*/
                      }
                      else
                      {
                        if($user->inRole('client'))
                        {
                          $title = trans('client/projects/completed_projects.text_expert_rating');
                        }
                        else if($user->inRole('expert'))
                        {
                          $title = trans('client/projects/completed_projects.text_client_rating');
                        }
                        $profile_image  = isset($review['from_user_info']['profile_image']) ? $review['from_user_info']['profile_image']:"default_profile_image.png";
                        $first_name     = isset($review['from_user_info']['first_name']) ?$review['from_user_info']['first_name']:"";
                        $last_name      = isset($review['from_user_info']['last_name']) ?substr($review['from_user_info']['last_name'], 0,1).'.':""; 
                        $user_name      = $first_name.' '.$last_name; 
                        /*$chk_user = Sentinel::findById($review['from_user_id'] ); 
                        if(isset($chk_user) && $chk_user != FALSE){
                            if(isset($chk_user->user_name)) {  
                               $user_name = $chk_user->user_name;
                            }
                        }*/
                      }
                      ?>

                    @if(isset($is_review_exists) && $is_review_exists!=0 )
                    <div class="search-grey-bx com-review-bx">
                      <div class="row">
                         <div class="col-sm-12 col-md-8 col-lg-8">
                            <div class="going-profile-detail">
                               <div class="going-pro">
                                 @if(isset($profile_image) && $profile_image != ""  && file_exists('public/uploads/front/profile/'.$profile_image)) 
                                 <div class="going-pro">
                                   <img src="{{url('/public')}}/uploads/front/profile/{{$profile_image or ''}}" alt="pic"/>
                                 </div>
                                 @else
                                 <div class="going-pro">
                                    <img src="{{  url('/uploads/front/profile/default_profile_image.png')}}" alt="pic"/>
                                 </div>
                                 @endif
                               </div>
                               <div class="going-pro-content">
                                  <div class="profile-name">{{ $title }}
                                    <span style="color:#494949;font-size: 14px;" class="sub-project-dec" > (&nbsp;{{ $user_name or ''}}&nbsp;)</span></div>
                                  <div class="more-project-dec" style="height:auto;">
                                     {{ $review['review'] }}
                                     <div class="clr"></div>
                                     <br/>
                                  </div>
                               </div>
                            </div>
                         </div>
                         
                         <div class="col-sm-12 col-md-4 col-lg-4">
                            <div class="rating-profile1 br-left client-rating">
                               <div class="rating_profile">
                                  <div class="rating-title1">{{trans('expert/projects/project_reviews.text_rating')}} <span>:</span> </div>
                                  <div class="rate-t">
                                     &nbsp;<div class="rating-list">
                                      <span class="stars">{{isset($review['rating'])?$review['rating']:'0'}}</span>
                                     </div>
                                     @if(isset($review['rating']) && $review['rating'] != "" )   
                                     ( {{ number_format(floatval($review['rating']), 1, '.', '')  }} )
                                     @else
                                     ( '0.0' )
                                     @endif
                                  </div>
                               </div>
                               <div class="rating_profile">
                                  <div class="rating-title1">{{trans('expert/projects/project_reviews.text_date')}} <span>:</span> </div>
                                  <div class="rating-list"> {{ date('d M Y' ,strtotime($review['created_at']))}}</div>
                               </div>
                            </div>
                         </div>
                      </div>
                    </div>
                    @endif

                   @endforeach                       
                @endif
               {{--  Review Display Ends --}}
                @if(isset($is_review_exists) && $is_review_exists==0 && isset($arr_project_info['project_status']) && $arr_project_info['project_status']==3 )
                  <div class="search-grey-bx add-review-bx">
                    <form id="frm_add_review" method="post"  action="{{url('/') }}/expert/review/add">
                       {{ csrf_field() }}
                       <div class="subm-text">{{trans('expert/projects/project_reviews.text_title')}}</div>
                       <div class="row">
                          <input type="hidden" readonly="" name="project_id" value="{{isset($arr_project_info['id'])?base64_encode($arr_project_info['id']):''}}" ></input> 
                          <input type="hidden" readonly="" name="client_user_id" value="{{isset($arr_project_info['client_user_id'])?base64_encode($arr_project_info['client_user_id']):''}}" ></input>   
                          <div class="col-sm-12 col-md-12 col-lg-12">
                             <div class="user-bx">
                                <div class="frm-nm">{{trans('expert/projects/project_reviews.text_rating_type_one')}}<i style="color: red;">*</i></div>
                                <select  class="review-rating-starts" name="rating_type_one">
                                   <option value="1">1</option>
                                   <option value="2">2</option>
                                   <option value="3">3</option>
                                   <option value="4">4</option>
                                   <option value="5">5</option>
                                </select>
                                <span class="error">{{ $errors->first('rating_type_one') }}</span>
                                 <div class="frm-nm">{{trans('expert/projects/project_reviews.text_rating_type_two')}}<i style="color: red;">*</i></div>
                                <select  class="review-rating-starts" name="rating_type_two">
                                   <option value="1">1</option>
                                   <option value="2">2</option>
                                   <option value="3">3</option>
                                   <option value="4">4</option>
                                   <option value="5">5</option>
                                </select>
                                <span class="error">{{ $errors->first('rating_type_two') }}</span>
                                <div class="frm-nm">{{trans('expert/projects/project_reviews.text_rating_type_three')}}<i style="color: red;">*</i></div>
                                <select  class="review-rating-starts" name="rating_type_three">
                                   <option value="1">1</option>
                                   <option value="2">2</option>
                                   <option value="3">3</option>
                                   <option value="4">4</option>
                                   <option value="5">5</option>
                                </select>
                                <span class="error">{{ $errors->first('rating_type_three') }}</span>
                                <div class="frm-nm">{{trans('expert/projects/project_reviews.text_rating_type_four')}}<i style="color: red;">*</i></div>
                                <select  class="review-rating-starts" name="rating_type_four">
                                   <option value="1">1</option>
                                   <option value="2">2</option>
                                   <option value="3">3</option>
                                   <option value="4">4</option>
                                   <option value="5">5</option>
                                </select>
                                <span class="error">{{ $errors->first('rating_type_four') }}</span>
                                <div class="frm-nm">{{trans('expert/projects/project_reviews.text_rating_type_five')}}<i style="color: red;">*</i></div>
                                <select  class="review-rating-starts" name="rating_type_five">
                                   <option value="1">1</option>
                                   <option value="2">2</option>
                                   <option value="3">3</option>
                                   <option value="4">4</option>
                                   <option value="5">5</option>
                                </select>
                                <span class="error">{{ $errors->first('rating_type_five') }}</span>
                             </div>
                          </div>
                          <div class="col-sm-12 col-md-12 col-lg-12">
                             <div class="user-bx user-frandlys">
                                <div class="frm-nm">{{trans('expert/projects/project_reviews.text_comment')}}<i style="color: red;">*</i></div>
                                <textarea style="max-width:100%;height:125px" name="review" id="comment" data-rule-required="true" class="det-in" placeholder="{{trans('expert/projects/project_reviews.placeholder_comment')}}" /></textarea>
                                <span class="error">{{ $errors->first('review') }}</span>
                                 <div class="error" id="err_comment"></div>
                             </div>
                          </div>
                          <div class="clr"></div>
                          <br/>
                          <div class="col-sm-12 col-md-12 col-lg-12">
                             <button type="submit" class="black-btn w-150 pull-right" onclick="showLoader()" style="float:none;">{{trans('expert/projects/project_reviews.text_add')}}</button>
                              <div class="clr"></div>
                          </div>
                       </div>
                    </form>
                  </div>
                @endif


            {{-- Milestone section --}}
            @if(!empty($arr_milestone_data['data']))
              <div class="milestone-listing">
              @if(isset($arr_milestone_data['data']) && count($arr_milestone_data['data']) > 0)
                <div class="head_grn">{{trans('milestones/project_milestones.text_title')}}</div>

                <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;"> 
                   <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                     
                      
                       
                        <!-- Wallet Details -->
                        
                        @if(isset($mangopay_wallet_details) && $mangopay_wallet_details !="" && !empty($mangopay_wallet_details))
                        <div style="padding:0px; margin-bottom: 15px;">
                          <div class="dash-user-details">
                              <div class="title"><i class="fa fa-money" aria-hidden="true"></i> {{trans('common/wallet/text.text_project_wallet')}} </div>
                              <div class="user-details-section">
                                  <table class="table" style="margin-bottom:15px">
                                    <tbody>     
                                      <tr>
                                        <td>
                                          <span class="card-id"> {{ isset($mangopay_wallet_details->Id)? $mangopay_wallet_details->Id:''}} </span>
                                        </td>
                                        <td>
                                          <div class="description" style="font-size: 11px;">{{isset($mangopay_wallet_details->Description)? $mangopay_wallet_details->Description:''}}</div>
                                        </td>
                                        <td style="font-size: 17px;width: 120px;">
                                          <div class="high-text-up">
                                            <span>
                                              {{ isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:''}}
                                            </span>
                                            <span class="mp-amount">
                                              {{ isset($mangopay_wallet_details->Balance->Amount)? $mangopay_wallet_details->Balance->Amount/100:'0'}}
                                            </span>
                                          </div>
                                          <div class="small-text-down" style="font-size: 10px;">
                                            {{trans('common/wallet/text.text_money_balance')}}
                                          </div>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                              </div>
                          </div>
                        </div>
                        @endif  
                        <!-- End Wallet Details -->
                    
                    </td>
                 </tr>
                @foreach($arr_milestone_data['data'] as $key=>$milestone)
                   <div class="search-grey-bx white-wrapper">
                      <div class="milestone-details">
                            @if(empty($milestone['milestone_release_details']['status']) || isset($milestone['milestone_release_details']['status']) && $milestone['milestone_release_details']['status'] != '2' && $milestone['status'] != '0')
                              <div class="min-block-time">
                                  <div id='mins{{$milestone["id"]}}' class="count-min-block">
                                      <span id='timer{{$milestone["id"]}}'></span>
                                  </div>
                              </div>
                            @endif
                            <h3><span class="milestone-batch">Milestone {{ isset($milestone['milestone_no']) ?  $milestone['milestone_no']:"0" }}</span> &nbsp;&nbsp; {{ isset($milestone['title']) ? $milestone['title']:"" }}</h3>
                            <div class="project-list mile-list">
                               <ul>
                                  <li><span class="hidden-xs hidden-sm"><i class="fa fa-calendar" aria-hidden="true"></i></span>{{ date('d M Y' , strtotime($milestone['created_at']))}}</li>
                                  @if(empty($milestone['milestone_release_details']['status']) || isset($milestone['milestone_release_details']['status']) && count($milestone['milestone_refund_details']) <= 0 || $milestone['milestone_refund_details'] == 'null')
                                    @if(empty($milestone['milestone_release_details']['status']) || isset($milestone['milestone_release_details']['status']) &&  $milestone['milestone_release_details']['status'] != '2' || $milestone['milestone_release_details']['status'] == 'null')
                                      @if(isset($milestone['milestone_due_date']) && $milestone['milestone_due_date'] != "" && $milestone['milestone_due_date'] != '0000-00-00 00:00:00' && $milestone['status'] != '0')
                                      <?php 
                                        $timeFirst             = strtotime(date('Y-m-d H:i:s'));
                                        $timeSecond            = strtotime($milestone['milestone_due_date']);
                                        $differenceInSeconds   = $timeSecond - $timeFirst;
                                        if($differenceInSeconds <= 0){
                                          $differenceInSeconds = '0';
                                        }
                                      ?>
                                      <script type="text/javascript">
                                          var time        = '<?php echo $differenceInSeconds; ?>';
                                          var milestoneid = '<?php echo $milestone["id"]; ?>';
                                          if(time < 0) {  time = 0; }
                                          var c           = time; // in seconds
                                          var t;
                                          var days    = parseInt( c / 86400 ) % 365;
                                          var hours   = parseInt( c / 3600 ) % 24;
                                          var minutes = parseInt( c / 60 ) % 60;
                                          var seconds = c % 60;
                                          //var result  = (days < 10 ? "0" + days : days) + ":"+ (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
                                          var d = ""; 
                                          var h = ""; 
                                          var m = "";
                                          var s = ""; 

                                          if(days     !=  ""  &&  days     !="0"){  d  =  (days+'d')     +  " ";  }
                                          if(hours    !=  ""  &&  hours    !="0"){  h  =  (hours+'h')    +  " ";  }
                                          if(minutes  !=  ""  &&  minutes  !="0"){  m  =  (minutes+'m')  +  " ";  }
                                          if(seconds  !=  ""  &&  seconds  !="0"){  s  =  (seconds+'s')  +  " ";  }
                                          
                                          if(d != "" && h !=""){
                                          var result    = d + h;
                                          }else if(h != "" && m !=""){
                                          var result    = h + m;
                                          }else if(m !=""){
                                          var result    = m;
                                          }else if(s !=""){
                                          var result    = s;
                                          }
                                          if(result == "NaN:NaN:NaN:NaN"){ 
                                            $('#timer'+milestoneid).html('{{trans('client/projects/ongoing_projects.text_next_expire')}}');
                                            $('#mins'+milestoneid).addClass('expired');
                                            $('#mins'+milestoneid).removeClass('timer');
                                          } else {
                                            $('#timer'+milestoneid).html(result);
                                            $('#mins'+milestoneid).addClass('timer');
                                            $('#mins'+milestoneid).removeClass('expired');
                                          }
                                          if(c == 0 ){
                                            $('#timer'+milestoneid).html('{{trans('client/projects/ongoing_projects.text_next_expire')}}');
                                            $('#mins'+milestoneid).addClass('expired');
                                            $('#mins'+milestoneid).removeClass('timer');
                                          }
                                          c = c - 1;
                                      </script>
                                    @endif
                                  @endif
                                  <li><span class="hidden-xs hidden-sm"><i class="fa fa-calendar" aria-hidden="true"></i></span>{{trans('client/projects/project_details.entry_milestone_due_date')}} : {{ date('d M Y' , strtotime($milestone['milestone_due_date']))}}</li>
                                  @endif
                                  
                                  <li style="background:none;">
                                  <span class="projrct-prce">
                                  @if(isset($milestone_project_currency) && $milestone_project_currency=="$")
                                  <i class="hidden-xs hidden-sm fa fa-money" aria-hidden="true"></i>  {{$milestone_project_currency}}&nbsp; {{ isset($milestone['cost'])? $milestone['cost']: '0' }}
                                  @else
                                   <i class="hidden-xs hidden-sm fa fa-money" aria-hidden="true"></i> {{'$'}}&nbsp; {{ isset($milestone['cost'])? $milestone['cost']: '0' }}
                                  @endif
                                  </span>
                                  </li>
                               </ul>
                            </div>
                            <div class="more-project-dec" style="margin-bottom: 10px;">{{ str_limit(isset($milestone['description'])? $milestone['description']: "", 350) }}</div>
                            @if(isset($milestone['milestone_refund_details']) && count($milestone['milestone_refund_details']) > 0)
                                @if($user->inRole('expert'))
                                  <a href="javascript:void(0)">
                                     <button class="awd_btn new-btn-class black-border-new-btn">Aborted by client</button>
                                  </a>
                                  <div class="clerfix"></div>
                                  <small><i>Client requested for refund to ArchExperts</i></small>
                                @elseif($user->inRole('client'))
                                  <a href="javascript:void(0)">
                                     @if($milestone['milestone_refund_details']['is_refunded'] == 'no')
                                     <button class="awd_btn new-btn-class" style="cursor: default; background-color: #D4EFDF;"><font color="#636363">Refund requested</font></button>
                                     <button class="awd_btn new-btn-class" 
                                             href="#refund_request" 
                                             data-keyboard="false" 
                                             data-backdrop="static" 
                                             data-toggle="modal" 
                                             id="refund-request-model" 
                                             data-milestone="{{isset($milestone['milestone_no']) ?  $milestone['milestone_no']:'0'}}" 
                                             data-expert_id="{{isset($milestone['expert_user_id']) ?  $milestone['expert_user_id']:'0'}}" 
                                             data-milestone-cost="{{isset($milestone['cost']) ?  $milestone['cost']:'0'}}" 
                                             data-project-id="{{isset($milestone['project_id'])?$milestone['project_id']:'0'}}" 
                                             data-currency-code="{{ isset($milestone['project_details']['project_currency_code']) ? $milestone['project_details']['project_currency_code'] : 'USD' }}"
                                             data-milestone-id="{{isset($milestone['id'])?$milestone['id']:'0'}}"><i class="fa fa-repeat" aria-hidden="true"></i>  Resend</button>
                                     @else
                                     <button class="awd_btn new-btn-class" style="cursor: default;background-color: #85C1E9;"><i class="fa fa-check-circle" aria-hidden="true"></i>Refunded</button>
                                     @endif`
                                  </a>
                                @endif  
                            @else
                                 @if(isset($milestone['transaction_details']) && isset($milestone['transaction_details']['payment_status']) && ($milestone['transaction_details']['payment_status']==1 || $milestone['transaction_details']['payment_status']==2) )
                                      @if($user->inRole('expert'))
                                        @if(isset($milestone['milestone_release_details']['status']) && $milestone['milestone_release_details']['status'] == '0')
                                          <a href="javascript:void(0)">
                                            <button class="awd_btn new-btn-class green-new-btn">{{trans('milestones/project_milestones.text_requested')}} </button>
                                          </a>
                                        @elseif(isset($milestone['milestone_release_details']['status']) && $milestone['milestone_release_details']['status'] == '1')
                                          <a href="javascript:void(0)">
                                            <button class="black-border-btn" style="cursor: default;">{{trans('milestones/project_milestones.text_approved')}} </button>
                                          </a>
                                        @elseif(isset($milestone['milestone_release_details']['status']) && $milestone['milestone_release_details']['status'] == '2')
                                          <a href="javascript:void(0);">
                                            <button class="black-btn new-btn-class active-green"><font color="white"> <i class="fa fa-check-circle" aria-hidden="true"></i> {{trans('milestones/project_milestones.text_paid')}}</font> </button>
                                          </a>

                                          <a href="{{$module_url_path}}/invoice/{{base64_encode($milestone['id'])}}">
                                            <button class="black-border-btn">{{trans('milestones/project_milestones.text_invoice')}} </button>
                                          </a>
                                        @elseif(isset($milestone['project_details']['project_status']) && $milestone['project_details']['project_status'] != '5')
                                          @if(isset($project_attachment['data']) && count($project_attachment['data'])>0)
                                            <a onclick="return confirmRelease(this)" data-currency="{{$project_main_attachment['project_currency_code']}}" data-release-id="{{base64_encode($milestone['id'])}}">
                                              <button class="black-btn">{{trans('milestones/project_milestones.text_release_request')}}</button>
                                            </a>
                                          @else
                                            <button class="blue_btn">{{trans('milestones/project_milestones.text_work_in_progress')}}</button>
                                          @endif  
                                        @endif
                                      @endif 
                                  @else
                                      @if($user->inRole('client') && isset($milestone['project_details']['project_status']) && $milestone['project_details']['project_status']!='5')
                                      <span><b>{{trans('milestones/project_milestones.text_unsuccessful_payment')}}</b></span>
                                      <br><br>
                                      <a href="{{ $module_url_path }}/milestones/delete/{{base64_encode($milestone['id'])}}">
                                        <button class="release-btn-red">{{trans('milestones/project_milestones.text_delete')}} </button>
                                      </a>
                                      @endif  
                                  @endif
                                  @if(isset($milestone['milestone_release_details']) && $milestone['milestone_release_details'] != NULL)    
                                      @if($user->inRole('client'))
                                        @if(isset($milestone['milestone_release_details']['status']) && $milestone['milestone_release_details']['status'] == '0')  
                                          <span >{{trans('milestones/project_milestones.text_approve_request_received_by_expert')}}</span>
                                          <br/>
                                          <br/>
                                          <a onclick="confirmApprove(this)" data-release-details-id="{{base64_encode($milestone['milestone_release_details']['id'])}}">
                                             <button class="release-btn">{{trans('milestones/project_milestones.text_approve')}}</button>
                                          </a>  

                                          <a onclick="confirmNotApprove(this)" data-release-details-id="{{base64_encode($milestone['milestone_release_details']['id'])}}">
                                             <button class="not-approved-btn">Not Approved</button>
                                          </a> 

                                        @elseif(isset($milestone['milestone_release_details']['status']) && $milestone['milestone_release_details']['status'] == '1')  
                                          <a href="javascript:void(0)">
                                             <button class="awd_btn new-btn-class" style="cursor: default;">{{trans('milestones/project_milestones.text_approved')}} </button>
                                          </a>
                                        @elseif(isset($milestone['milestone_release_details']['status']) && $milestone['milestone_release_details']['status'] == '2')  
                                          <a href="javascript:void(0)">
                                             <button class="awd_btn new-btn-class" style="cursor: default;background-color: #28B463;border-color: black;"><font color="white"><i class="fa fa-check-circle" aria-hidden="true"></i> {{trans('milestones/project_milestones.text_paid')}}</font> </button>
                                          </a>
                                        @endif
                                      @endif
                                  @elseif($user->inRole('client') && (isset($milestone['transaction_details']) && isset($milestone['transaction_details']['payment_status']) && ($milestone['transaction_details']['payment_status']==1 || $milestone['transaction_details']['payment_status']==2) ))
                                    <a href="javascript:void(0);">
                                      <button class="awd_btn new-btn-class"  style="cursor: default; background-color:#149ec3;">
                                      <font color="white">{{trans('milestones/project_milestones.text_inprogress')}} </font></button>
                                    </a>
                                    <a href="#refund_request" 
                                       data-keyboard="false" 
                                       data-backdrop="static" 
                                       data-toggle="modal" 
                                       id="refund-request-model" 
                                       data-milestone="{{isset($milestone['milestone_no']) ?  $milestone['milestone_no']:'0'}}" 
                                       data-expert_id="{{isset($milestone['expert_user_id']) ?  $milestone['expert_user_id']:'0'}}"
                                       data-milestone-cost="{{isset($milestone['cost']) ?  $milestone['cost']:'0'}}" 
                                       data-project-id="{{isset($milestone['project_id'])?$milestone['project_id']:'0'}}" 
                                       data-currency-code="{{ isset($milestone['project_details']['project_currency_code']) ? $milestone['project_details']['project_currency_code'] : 'USD' }}"
                                       data-milestone-id="{{isset($milestone['id'])?$milestone['id']:'0'}}">
                                      <button class="awd_btn new-btn-class" style="background-color:#2d2d2d;">
                                      <font color="white"> {{trans('milestones/project_milestones.text_refund_request')}}</font></button>
                                    </a>
                                  @endif
                            @endif      
                      </div>
                   </div>
                @endforeach
               </div> 
              @else
               <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;"> 
                 <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                    <div class="head_grn">{{trans('milestones/project_milestones.text_title')}}</div>
                    <div class="search-grey-bx">
                      <div class="milestone-title">
                        <a href="{{ $module_url_path }}/details/{{ $enc_project_id }}"><span>{{isset($milestone_project_name)?''.$milestone_project_name.'':''}}</span></a>
                      </div>
                      <br/>
                      <div class="clearfix"></div> 
                      <!-- Wallet Details -->
                      @if(isset($mangopay_wallet_details) && $mangopay_wallet_details !="" && !empty($mangopay_wallet_details))
                      <div class="col-sm-12 col-md-6 col-lg-6" style="padding:0px; margin-bottom: 15px;">
                        <div class="dash-user-details">
                            <div class="title"><i class="fa fa-money" aria-hidden="true"></i> Wallet </div>
                            <div class="user-details-section">
                                <table class="table" style="margin-bottom:15px">
                                  <tbody>     
                                    <tr>
                                      <td>
                                        <span class="card-id"> {{ isset($mangopay_wallet_details->Id)? $mangopay_wallet_details->Id:''}} </span>
                                      </td>
                                      <td>
                                        <div class="description" style="font-size: 11px;">{{isset($mangopay_wallet_details->Description)? $mangopay_wallet_details->Description:''}}</div>
                                      </td>
                                      <td style="font-size: 17px;width: 120px;">
                                        <div class="high-text-up">
                                          <span>
                                            {{ isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:''}}
                                          </span>
                                          <span class="mp-amount">
                                            {{ isset($mangopay_wallet_details->Balance->Amount)? $mangopay_wallet_details->Balance->Amount/100:'0'}}
                                          </span>
                                          
                                        </div>
                                        <div class="small-text-down" style="font-size: 10px;">
                                          money balance
                                        </div>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                            </div>
                        </div>
                      </div>
                      @endif  
                      <!-- End Wallet Details -->
                      <br/>
                      <div class="clearfix"></div>
                      <div class="alert alert-info">
                        <img style="height:21px; margin-top:-4px;" src="{{url('/public')}}/front/images/information-icon-th.png"> 
                        {{trans('milestones/project_milestones.text_no_record_found')}}
                      </div> 
                    </div>
                  </td>
               </tr>
              @endif

              <!-- Pagination Links -->
              @include('front.common.pagination_view', ['paginator' => $arr_milestone_pagination])
              <!-- Pagination Links -->
            @endif
              {{-- End of Milestone section --}}

                {{-- Collaboration Section --}}
                <div class="search-grey-bx projt-detls">
                  <div class="coll-listing-section project-coll">
                      
                      <div class="row">
                          <div class="col-sm-12 col-md-6 col-lg-6">
                              <h2><i class="fa fa-object-group"></i>Collaboration Space</h2>
                          </div>
                          
                          @if(isset($arr_project_info['expert_user_id']) && $arr_project_info['expert_user_id']!='' && $arr_project_info['expert_user_id']!='0' && $arr_project_info['expert_user_id'] == $user->id)
                            @if(isset($arr_project_info['is_awarded']) && $arr_project_info['is_awarded']!='' && $arr_project_info['is_awarded'] != '0')
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                 <div style="margin: 4px 0 0;" class="upload-btn p-relative pull-right">
                                    <a href="#colla-upload-file" data-keyboard="false" data-backdrop="static"  data-toggle="modal"  class="black-border-btn"><i class="fa fa-upload"></i> Upload Image/ Document</a>
                                </div>
                            </div>
                            @endif
                          @endif

                      </div>
                      <div class="row">
                          <!-- project main attachment from project table -->
                          @if(isset($project_main_attachment['project_attachment']) && $project_main_attachment['project_attachment'] != "")
                              <?php 
                                  $attachment             = isset($project_main_attachment['project_attachment'])?$project_main_attachment['project_attachment']:"";
                                  $get_attatchment_format = get_attatchment_format($attachment);
                              ?>
                              @if(isset($get_attatchment_format) &&
                                  $get_attatchment_format == 'doc'  ||
                                  $get_attatchment_format == 'docx' ||
                                  $get_attatchment_format == 'odt'  ||
                                  $get_attatchment_format == 'pdf'  ||
                                  $get_attatchment_format == 'txt'  ||
                                  $get_attatchment_format == 'xlsx')
                                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 coll-list">
                                      <div class="coll-list-block">
                                          <div class="coll-img p-relative">
                                              @if(file_exists('public/uploads/front/postprojects/'.$project_main_attachment['project_attachment']))
                                              <img src="{{url('/public')}}/front/images/file_formats/{{$get_attatchment_format}}.png" class="img-responsive">
                                              @else
                                              <img src="{{url('/public')}}/front/images/file_formats/image.png"  class="img-responsive" alt=""/>
                                              @endif
                                              <div class="coll-details">
                                                  <div class="coll-person">
                                                      <div class="coll-name"><!-- File #1 --></div>
                                                  </div>
                                                  <!-- <div class="avg-rating">
                                                      <ul>
                                                          <li><i class="fa fa-star"></i></li>
                                                          <li><i class="fa fa-star"></i></li>
                                                          <li><i class="fa fa-star"></i></li>
                                                          <li><i class="fa fa-star"></i></li>
                                                          <li><i class="fa fa-star"></i></li>
                                                      </ul>
                                                  </div> -->
                                              </div>
                                              <div class="list-hover">
                                                  <a style="font-size:2em;" download href="{{url('/public')}}{{config('app.project.img_path.project_attachment')}}{{$project_main_attachment['project_attachment']}}" class="view-button"><i class="fa fa-cloud-download"></i></a>
                                              </div>
                                          </div>
                                          <div class="design-title">
                                              <p>Main attachment</p>
                                          </div>
                                      </div>
                                  </div>
                              @else
                                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 coll-list">
                                          <div class="coll-list-block">
                                              <div class="coll-img p-relative">
                                                  @if(file_exists('public/uploads/front/postprojects/'.$project_main_attachment['project_attachment']))
                                                  <img src="{{url('/public')}}{{config('app.project.img_path.project_attachment')}}{{$project_main_attachment['project_attachment']}}" class="img-responsive">
                                                  @else
                                                  <img src="{{url('/public')}}/front/images/archexpertdefault/default-arch.jpg"  class="img-responsive" alt=""/>
                                                  @endif
                                                  <div class="coll-details">
                                                      <div class="coll-person">
                                                          <div class="coll-name"><!-- File #2 --></div>
                                                      </div>
                                                      <!-- <div class="avg-rating">
                                                          <ul>
                                                              <li><i class="fa fa-star"></i></li>
                                                              <li><i class="fa fa-star"></i></li>
                                                              <li><i class="fa fa-star"></i></li>
                                                              <li><i class="fa fa-star"></i></li>
                                                              <li><i class="fa fa-star"></i></li>
                                                          </ul>
                                                      </div> -->
                                                  </div>
                                                  <div class="list-hover">
                                                      <a style="font-size:2em;" download href="{{url('/public')}}{{config('app.project.img_path.project_attachment')}}{{$project_main_attachment['project_attachment']}}" class="view-button"><i class="fa fa-cloud-download"></i></a>
                                                  </div>
                                              </div>
                                              <div class="design-title">
                                                  <p>Main attachment</p>
                                              </div>
                                          </div>
                                  </div>
                              @endif
                              <div class="clearfix"></div>
                          @endif
                          <!-- end project main attachment from project table -->

                          @if(isset($project_attachment['data']) && sizeof($project_attachment['data']) >0)
                              @foreach($project_attachment['data'] as $key => $attch)
                                @if(isset($attch['attachment']) && $attch['attachment']!= '' && file_exists('public/uploads/front/project_attachments/'.$attch['attachment']))
                                  @php 
                                      $attachment             = isset($attch['attachment'])?$attch['attachment']:"";
                                      $get_attatchment_format = get_attatchment_format($attachment);
                                  @endphp
                                  @if(isset($get_attatchment_format) &&
                                      $get_attatchment_format == 'doc'  ||
                                      $get_attatchment_format == 'docx' ||
                                      $get_attatchment_format == 'odt'  ||
                                      $get_attatchment_format == 'pdf'  ||
                                      $get_attatchment_format == 'txt'  ||
                                      $get_attatchment_format == 'xlsx')
                                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 coll-list">
                                          <div class="coll-list-block">
                                              <div class="coll-img p-relative">
                                                  @if(file_exists('public/uploads/front/project_attachments/'.$attch['attachment']))
                                                  <img src="{{url('/public')}}/front/images/file_formats/{{$get_attatchment_format}}.png" class="img-responsive">
                                                  @else
                                                  <img src="{{url('/public')}}/front/images/file_formats/image.png"  class="img-responsive" alt=""/>
                                                  @endif
                                                  <div class="coll-details">
                                                      <div class="coll-person">
                                                          <div class="coll-name">File #{{isset($attch['attchment_no'])?$attch['attchment_no']:'-'}}</div>
                                                      </div>
                                                      <!-- <div class="avg-rating">
                                                          <ul>
                                                              <li><i class="fa fa-star"></i></li>
                                                              <li><i class="fa fa-star"></i></li>
                                                              <li><i class="fa fa-star"></i></li>
                                                              <li><i class="fa fa-star"></i></li>
                                                              <li><i class="fa fa-star"></i></li>
                                                          </ul>
                                                      </div> -->
                                                  </div>
                                                  <div class="list-hover">
                                                      <a href="{{url('/expert/projects/collaboration/details')}}/{{isset($attch['id'])?base64_encode($attch['id']):'0'}}/{{base64_encode($project_id)}}" class="view-button">View</a>
                                                  </div>
                                              </div>
                                              <div class="design-title">
                                                  <p style="display: inline;" title="{{isset($attch['attachment'])?$attch['attachment']:'Unknown'}}">{{isset($attch['attachment'])?str_limit($attch['attachment'],14):'Unknown'}}</p>
                                                  <a  style="font-size:1.5em;color:#2d2d2d"  style="display: inline;" class="pull-right" download href="{{url('/public/uploads/front/project_attachments/')}}/{{$attch['attachment']}}"><i class="fa fa-cloud-download"></i></a>
                                              </div>
                                          </div>
                                      </div>
                                  @else
                                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 coll-list">
                                              <div class="coll-list-block">
                                                  <div class="coll-img p-relative">
                                                      @if(file_exists('public/uploads/front/project_attachments/'.$attch['attachment']))
                                                      <img src="{{url('/public/uploads/front/project_attachments/')}}/{{$attch['attachment']}}" class="img-responsive">
                                                      @else
                                                      <img src="{{url('/public')}}/front/images/archexpertdefault/default-arch.jpg"  class="img-responsive" alt=""/>
                                                      @endif
                                                      <div class="coll-details">
                                                          <div class="coll-person">
                                                              <div class="coll-name">File #{{isset($attch['attchment_no'])?$attch['attchment_no']:'-'}}</div>
                                                          </div>
                                                          <!-- <div class="avg-rating">
                                                              <ul>
                                                                  <li><i class="fa fa-star"></i></li>
                                                                  <li><i class="fa fa-star"></i></li>
                                                                  <li><i class="fa fa-star"></i></li>
                                                                  <li><i class="fa fa-star"></i></li>
                                                                  <li><i class="fa fa-star"></i></li>
                                                              </ul>
                                                          </div> -->
                                                      </div>
                                                      <div class="list-hover">
                                                          <a href="{{url('/expert/projects/collaboration/details')}}/{{isset($attch['id'])?base64_encode($attch['id']):'0'}}/{{base64_encode($project_id)}}" class="view-button">View</a>
                                                      </div>
                                                  </div>
                                                  <div class="design-title" >
                                                      <p style="display: inline;" title="{{isset($attch['attachment'])?$attch['attachment']:'Unknown'}}">{{isset($attch['attachment'])?str_limit($attch['attachment'],14):'Unknown'}}</p>
                                                      <a  style="font-size:1.5em;color:#2d2d2d;"  style="display: inline;" class="pull-right" download href="{{url('/public/uploads/front/project_attachments/')}}/{{$attch['attachment']}}"><i class="fa fa-cloud-download"></i></a>
                                                  </div>
                                              </div>
                                          </div>
                                  @endif
                                @endif
                              @endforeach
                          @endif
                      </div>

                      @if(isset($project_main_attachment['project_attachment']) && $project_main_attachment['project_attachment'] == "" && empty($project_attachment['data']) && sizeof($project_attachment['data']) <= 0)
                          <div class="search-grey-bx">
                            <div class="no-record" >
                               {{trans('expert/projects/awarded_projects.text_sorry_no_record_found')}}
                            </div>
                          </div> 
                      @endif
                      <!-- Pagination -->
                      @include('front.common.pagination_view', ['paginator' => $arr_collaboration_pagination])
                      <!-- ends -->
                  </div>
                </div>
                {{-- End of collaboration section --}}






            </div>
          </div>
        </div>
  </div>





@if($user != null) 
    @if($user->inRole('expert'))
      <div class="modal fade invite-member-modal" id="colla-upload-file" role="dialog">
          <div class="modal-dialog modal-lg">
             <button type="button" id="btn_close_popup" class="close"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
             <div class="modal-content">
                <div class="modal-body">
                 <h2>Upload File</h2>
                     <div class="invite-form project-found-div">
                          <div class="user-box">
                               <div class="txt-edit"> <b>Enter your file details</b> </div>
                               <span class="txt-edit" id="colla_desc_msg">
                                {{trans('client/projects/post.text_project_limit_description')}}
                               </span>
                               <input type="hidden" id="project_id" id="project_id" value="{{base64_encode($project_id)}}">
                               <div class="input-name">
                                  <textarea class="client-taxtarea beginningSpace_restrict" rows="5" data-rule-required="true" data-rule-maxlength="1000" data-txt_gramm_id="21e619fb-536d-f8c4-4166-76a46ac5edce" onkeyup="javascript: return textCounter(this,1000);" id="colla_file_desc" name="colla_file_desc" placeholder="{{ trans('project_listing/listing.text_Provide_general_info_about_your_project_file') }}"></textarea><span class="error err" id="err_colla_file_desc"></span>
                               </div>
                               <div class="input-name">
                                  <div class="txt-edit"> <b>{{ trans('contets_listing/listing.text_attachment_instructions') }} :</b> </div>
                                  <!--image upload start-->
                                  <div class="fallback dfvdfvdfvdf dropzone-section" id="dropzone_image_div">
                                     <form action="{{url('/')}}/expert/projects/collaboration/upload/files" class="dropzone" id="dropzonewidget">
                                      {{csrf_field()}}
                                      <div id="dropzone_append_id"></div>
                                    </form>
                                      <div class="post-img-note-block">
                                        <div class="error" id="err_image_file"></div>
                                        <label id="basic-error" class="validation-error-label" for="basic"></label>
                                        <div class="note-block">
                                            <b>{{ trans('contets_listing/listing.text_note') }} : </b> 
                                            {{ trans('project_listing/listing.text_select_atleast_one_image_to_upload_Only_jpg_png_jpeg_images_allowed_with_max_size_1mb_You_can_upload_upto_5_images_at_a_time') }}
                                        </div>
                                       </div>
                                  </div>
                              </div>
                              <div class="clearfix"></div>
                              <hr>
                              <div class="input-name">
                                  <div class="terms">
                                      <div class="check-box">{{-- wrk-done-by-me --}}
                                         <p>
                                            {{--<input class="filled-in" type="checkbox" name="wrk-done-by-me" id="wrk-done-by-me">
                                           <label for="wrk-done-by-me"></label>--}}                         
                                            <div class="radio-buttons">
                                              <input class="filled-in " type="radio" value="0" name="wrk-done-by-me" >
                                              <font class="txt-edit">{{ trans('contets_listing/listing.text_all_work_was_done_by_me_or_our_company') }}</font>
                                            </div>
                                            <div class="radio-buttons">
                                              <input class="filled-in" type="radio" value="1" name="wrk-done-by-me" >
                                              <font class="txt-edit">{{ trans('contets_listing/listing.text_the_work_was_partially_done_by_me_or_our_company') }}</font >
                                            </div>
                                            {{-- <div class="error" id="wrk-done-by-me-err"></div>
                                            <label class="error" for="wrk-done-by-me"></label> --}} 
                                            <div id="wrk-done-by-me-err" class="error"></div> 
                                        </p>
                                      </div>
                                        {{--<div class="temsp-d">
                                        <span class="txt-edit"><b>All work is done by me</b></span> <small>
                                      </div> --}}
                                      {{-- <div id="wrk-done-by-me-err" class="error"></div>  --}}
                                  </div>    
                               </div>
                          </div>
                          <div class="clearfix"></div>
                          <button type="button" id="cancel_btn" style="width: 185px;" class="black-border-btn inline-btn">{{ trans('contets_listing/listing.text_cancel') }}</button>
                          <button type="button" id="btn_post_colla_images" class="black-btn inline-btn" style="height: auto;">Upload</button>
                     </div>
                </div>
             </div>
          </div>
      </div>
    @endif
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn_post_colla_images').click(function(){
            $("#wrk-done-by-me-err").html('');
            if($("input[name='wrk-done-by-me']:checked").val()) {}
            else{    
                $("#wrk-done-by-me-err").html('This field is required.');
                return false;
            } 
        });
    });
    function textCounter(field,maxlimit){
      var countfield = 0;
      if( field.value.length > maxlimit ){
        field.value = field.value.substring( 0, maxlimit );
        return false;
      } else {
        countfield = maxlimit - field.value.length;
        var message = '{{trans('client/projects/invite_experts.text_you_have_left')}} <b>'+countfield+'</b> {{trans('client/projects/invite_experts.text_characters_for_description')}}';
        jQuery('#colla_desc_msg').html(message);
        return true;
      }
    }
    Dropzone.options.dropzonewidget = {
      addRemoveLinks: true,
      maxFiles: 5,
      parallelUploads: 5,
      uploadMultiple: true,
      maxFilesize: 5,
      autoProcessQueue: false,
      acceptedFiles: '.jpg, .JPG, .png, .PNG, .jpeg, .JPEG,.doc,.docx,.odt,.pdf,.txt,.xlsx',
      dictMaxFilesExceeded: "{{ trans('contets_listing/listing.text_you_can_upload_only_5_images_at_a_time') }}",
      dictFileTooBig: "{{ trans('contets_listing/listing.text_image_is_too_large_please_upload_upto_1mb_image') }}",
      dictDefaultMessage: "{{ trans('project_listing/listing.text_drop_your_images_here_to_upload') }}",
      success: function( file, response )  {
        hideProcessingOverlay(); 
        if(response.status=='error') {
          swal("",response.message,'error');
          this.removeFile(file); 
          $('#colla_file_desc').val();
        } else {   
            var msg = response.message;
            swal("",msg,'success');
            this.removeFile(file); 
            $('#colla_file_desc').val('');
            $("#colla-upload-file").modal('hide');  
            setTimeout(function(){
            location.reload();
            },500);  
        }
      },
      init: function() {
        var myDropzone = this;
        this.on("error", function(file, message) { 
          swal("",message,'error');
          this.removeFile(file); 
        });
        $('#btn_post_colla_images').on("click", function(){
          
          if($("#colla_file_desc").val()=="") {
            $("#err_colla_file_desc").html('{{ trans('contets_listing/listing.text_this_field_is_required') }}');
          }else if(!$("input[name='wrk-done-by-me']:checked").val()){    
                $("#wrk-done-by-me-err").html('This field is required.');
                return false;
            } else {
              if (!myDropzone.files || !myDropzone.files.length) 
              {
                  $('#err_image_file').html('{{ trans('contets_listing/listing.text_this_field_is_required') }}');
                  $("#err_colla_file_desc").html('');
                  return false;
              } else {
                  myDropzone.processQueue();  
                  $('#err_colla_file_desc').val('');
                  $('#colla_file_desc').html(''); 
              }
          }

          $('#basic-error').html('');    
        });
        myDropzone.on("sending", function(file, xhr, data) {
        showProcessingOverlay();
          data.append("_token", $('meta[name="token"]').attr('content'));
          data.append("project_id",$("#project_id").val());
          data.append("colla_file_desc", $('#colla_file_desc').val());
          if($("input[name='wrk-done-by-me']:checked").val())
          {
          data.append("wrk-done-by-me", $("input[name='wrk-done-by-me']:checked").val());
          }
        });
      }
    };
    function makeStatusMessageHtml(status, message){
        str = '<div class="alert alert-'+status+' alert-dismissable"><a class="close" data-dismiss="alert" aria-label="close">×</a><strong>'+status+' :</strong>'+message+'</div>';
        return str;
    }
    $("#btn_close_popup,#cancel_btn").on('click',function(e)
    {
        $('#colla_file_desc').val('');
        $("#err_colla_file_desc").html(''); 
        $('#err_image_file').html('');
        var myDropzone = Dropzone.forElement("#dropzonewidget");
        myDropzone.removeAllFiles(true); 
        $("#colla-upload-file").modal('hide');
    });
</script>  
@endif 



  <!-- </div> -->
  <script type="text/javascript">
      $('#frm-bid-details').validate(); 
      $('#frm_add_review').validate(); 
      $('#frm_project_attchment').validate(); 
      $(function(){
        $('.review-rating-starts').barrating({
           theme: 'fontawesome-stars'
        });
      }); 
      /* This function deletes bid attchment */
      function deleteProjectAttachment(attchment_id){
        var confirm_msg = ' {!! trans('expert/projects/project_reviews.send_delete_bid_attachment_request') !!} ';
        alertify.confirm(confirm_message, function (e) {
            if (e) {
                return false;
            } else {
                return false;
            }
        });
        $.ajax({
        url:"{{url('/public')}}"+"/expert/delete_project_attachment",
        data:{ id: attchment_id },
        success:function (response)
          {
             showProcessingOverlay();
             window.location.reload();
             if(typeof response.status != "undefined" && response.status == "SUCCESS_PROJECT_ATTACHMENT") {
               /*$('#msg_bid_attach').html(response.msg).show().fadeOut(5000);
                $('#attch_'+attchment_id).hide();*/            
             }
          }
        });
      }
      /* Code for project attchment upload */
      $(document).ready(function() { 
        $('#project_attachment').change(function() {
          $("#btn_remove_image").show();
          $('#project_attachment_name').val($(this).val());
        });
        removeBrowsedImage();
      });
      function browseImage(){
          $("#project_attachment").trigger('click');
      }
      function removeBrowsedImage(){
          $('#project_attachment_name').val("");
          $("#btn_remove_image").hide();
          $("#project_attachment").val("");
      }
      function showLoader(){  
        var comment = $('#comment').val();
        if($.trim(comment)!==""){
          showProcessingOverlay();
        }
      }
  </script>
  <script type="text/javascript">
    function confirmRelease(ref) {
      swal({
              title: "Are you sure? You want to send release request ?",
              text: "",
              icon: "warning",
              
              dangerMode: true,
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
             },
             function(isConfirm){

               if (isConfirm){
                  var release_id    = $(ref).attr('data-release-id');
                  var currency_code = $(ref).attr('data-currency');
                 window.location.href = "{{ $module_url_path }}/milestones/release/"+release_id+"/"+currency_code;
                    return true;

                } else {
                    swal.close();
                  return false;
                }
             });
    }
  </script>
  @stop