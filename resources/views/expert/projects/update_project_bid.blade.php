@extends('expert.layout.master')                
@section('main_content')

<style type="text/css">
  .error {
    position: relative;
  }

    table {
    border-collapse: collapse;
  }

  td, th {
    border: 1px solid #999;
    padding: 0.5rem;
    text-align: left; 
  }

</style>  


<div class="container">
   <div class="row">
      <div class="col-sm-8 col-md-8 col-lg-9">
         {{--@include('front.layout._operation_status')--}}
         <!-- Including top view of project details - Just pass project information array to the view -->
         @include('front.common._top_project_details',['projectInfo'=>$arr_project_info])
         <!-- Ends -->  

         <div class="search-grey-bx">
            <form action="{{ $module_url_path }}/update_bid_details" method="post" id="frm-bid-details" enctype="multipart/form-data">
               {{ csrf_field() }}
               <input type="hidden"  name="bid_id" readonly="" value="{{ isset($arr_bid_data['id']) ? base64_encode($arr_bid_data['id']):''}}"/>  
               <div class="subm-text">{{ trans('expert/projects/update_project_bid.text_update_proposal') }}</div>
               <div class="row">
                <?php
                   if($arr_project_info['project_pricing_method'] == '1')
                    {
                      $rate_title = "Price";
                      $currency_rate_title = $arr_project_info['project_currency'].'-'. get_currency_description($arr_project_info['project_currency_code']);
                      $rate_placeholder = 'Price';
                    }
                    elseif($arr_project_info['project_pricing_method'] == '2')
                    {
                      $rate_title = "Hourly Rate";
                      $currency_rate_title = $arr_project_info['project_currency_code'].'/h';
                      $rate_placeholder = 'My hourly rate for this job';
                    }

                    $arr_project_duration = config('app.project_duration');
                    $arr_hours_per_week = config('app.hours_per_week');
                ?>

                 {{--  if($arr_project_info['project_pricing_method'] == '2')
                  {
                     $rate_title = trans('common/_top_project_details.text_hourly_rate') ;
                  }
                  elseif($arr_project_info['project_pricing_method'] == '1')
                  {
                     $rate_title = trans('common/_top_project_details.text_budget') ;
                  } --}}

                  <div class="col-sm-12 col-md-6 col-lg-4">
                     <div class="user-bx">
                        <div class="frm-nm">{{$rate_title}}<i style="color:red">&nbsp;*</i>&nbsp;&nbsp;({{ $currency_rate_title }})</div>
                        <input type="text" class="clint-input valid" value="{{isset($arr_bid_data['bid_cost'])?$arr_bid_data['bid_cost']:0}}" data-rule-required="true" name="bid_cost"  data-rule-number="true" data-rule-max="25000" data-rule-min="1" onkeyup="return chk_validation(this);"  placeholder="{{$rate_placeholder}}" />
                        <span class="error">{{ $errors->first('bid_cost') }}</span>
                     </div>
                  </div>

                  @if($arr_project_info['project_pricing_method'] == '2')
                    <div class="col-sm-4 col-md-4 col-lg-4" id="section_hour_per_week">
                      <div class="input-bx d-se">
                            <div class="p-control-label">Hours/Week<span class="star-col-block">*</span></div>
                            <div class="droup-select input-bx">
                                <select name="hour_per_week" id="hour_per_week" class="droup mrns tp-margn form-control" onchange="set_custom_hour_per_week()" data-rule-required="true">
                                    <option value="">My availability for this job</option>
                                    @if(isset($arr_hours_per_week) && sizeof($arr_hours_per_week)>0)
                                      @foreach($arr_hours_per_week as $hour_key => $hour_value)
                                      <option value="{{$hour_key}}" data-text="{{$hour_value}}" @if(isset($arr_bid_data['hour_per_week']) && $arr_bid_data['hour_per_week']!="" && $arr_bid_data['hour_per_week'] == $hour_key) selected @endif>{{$hour_value}}
                                      </option>
                                      @endforeach
                                    @endif
                                    <option value="HOUR_CUSTOM_RATE" @if($arr_bid_data['is_hour_custom_rate'] == '1') selected @endif>Custom</option>
                                </select>
                                <span class='error'>{{ $errors->first('hour_per_week') }}</span>
                                <span id="err_hour_per_week" class='error'></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4" id="hour_custom_rate" style="display: none">
                        <div class="input-bx d-se">
                            <div class="p-control-label hour_custom_rate_description"><span class="star-col-block">*</span></div>
                            <div class="cust-budget input-bx">
                                <input name="hour_custom_rate" data-rule-required="true" data-rule-number="true" value="{{ isset($arr_bid_data['hour_per_week'])?$arr_bid_data['hour_per_week']:'' }}" data-rule-min="1" type="text" class="input-lsit"></input>
                                <span class='error'>{{ $errors->first('hour_custom_rate') }}</span>
                            </div>
                        </div>
                    </div>
                  @endif


                 {{--  <div class="col-sm-12 col-md-6 col-lg-6">
                     <div class="user-bx">
                        <div class="frm-nm">{{ trans('expert/projects/update_project_bid.text_estimated_duration') }}<i style="color:red">&nbsp;*</i>&nbsp;<i style="font-size: 10px;">{{ trans('expert/projects/update_project_bid.text_in_number_of_days') }}</i></div>
                        <input type="text" name="expected_duration" value="{{isset($arr_bid_data['bid_estimated_duration'])?$arr_bid_data['bid_estimated_duration']:0}}" data-rule-required="true" placeholder="{{ trans('expert/projects/update_project_bid.text_enter_number_of_days') }}" data-rule-number="true" data-rule-min="1" data-rule-max="365" onkeyup="return chk_validation(this);" class="clint-input valid"/>
                        <span class="error">{{ $errors->first('expected_duration') }}</span>
                     </div>
                  </div>
                  <div class="clr"></div> --}}

                  <div class="col-sm-4 col-md-4 col-lg-4">
                      <div class="user-box">
                          <div class="p-control-label dr_label">Duration<span class="star-col-block">*</span></div>
                          <div class="calender-input droup-select">
                              <select name="expected_duration" id="expected_duration" class="droup mrns tp-margn" data-rule-required="true" >
                                   <option value="">I can support for</option>
                                   @if(isset($arr_project_duration) && sizeof($arr_project_duration)>0)
                                      @foreach($arr_project_duration as $dr_key => $dr_value)
                                          <option value="{{ $dr_value }}" @if(isset($arr_bid_data['bid_estimated_duration']) && $arr_bid_data['bid_estimated_duration']!='' && $arr_bid_data['bid_estimated_duration'] == $dr_value) selected @endif>{{ $dr_value }}</option>
                                      @endforeach
                                   @endif
                              </select>
                              <span class="error">{{ $errors->first('expected_duration') }}</span>
                          </div>
                      </div>
                  </div>

                  <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="user-bx">
                      <div class="frm-nm">{{ trans('expert/projects/update_project_bid.text_enter_your_proposal_details') }}<i style="color:red">&nbsp;*</i></div>
                      <textarea cols="" rows="" value="" name="proposal_details" style="height:100px; padding:10px;"  data-rule-required="true" class="clint-input valid"  placeholder="{{ trans('expert/projects/update_project_bid.text_proposal_placeholder') }}">{{isset($arr_bid_data['bid_description'])?$arr_bid_data['bid_description']:''}}</textarea>
                      <span class="error">{{ $errors->first('proposal_details') }}</span>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12">
                      {{-- <div class="attach-text txt-attach-block"> --}}
                        <div class="user-box" style="max-width: 100%; width: 100%;">
                          <div class="p-control-label"><i class="fa fa-paperclip"></i> {{ trans('expert/projects/add_project_bid.text_file_attachment') }}</div>
                            <div class="input-name">
                              <div class="user-box">
                                  {{-- <div class="p-control-label">{{trans('common/_top_project_details.text_project_upload')}}
                                  </div> --}}
                                  <div class="picture-div new-picture" style="" id="pictures_div">
                                      <div class="update-pro-img-main"> 
                                          <div class="lab_img" id="lab_1">
                                            {{-- <div class="col-sm-12 col-lg-12 col-lg-12" style="float:right;">
                                              <span>
                                                <a href="javascript:void(0);" id='remove_project' class="remove_project" style="display:none;" >
                                                  <span class="glyphicon glyphicon-minus-sign" style="font-size: 20px;"></span>
                                                </a>
                                              </span>
                                            </div> --}}
                                            <div class="" id="add_lab_div">
                                              <div class="add_pht upload-pic loc_add_pht" id="div_blank" onclick="return addpictures(this)"  style="height: 120px;width: 120px; float: left;"> 
                                                <img src="{{url('/public')}}/front/images/plus-img.png" alt="user pic"  style="width:100%;height:100%;" />
                                              </div>
                                              <div class="show_photos" id="show_photos" style="width: auto; display: initial;float: none;"></div>
                                              <div id="div_hidden_photo_list" class="div_hidden_photo_list">
                                                 <input type="file" name="contest_attachments[]" id="contest_attachments" class="contest_attachments" style="display:none" />
                                              </div>
                                            </div>
                                          </div>
                                       </div>                       
                                          <input type="hidden" name="file_name_lab" id="file_name_lab"  >
                                           
                                      <div class="clearfix"></div>                     
                                      <div class="error-red" id="err_other_image"></div>                                          
                                  </div>
                                  <br>
                                  <span class="label label-important" style="background-color:red; margin-top:-15px; color: white;">Note!</span> Only this Extension File can be Uploaded: png,docx,xls,xlsx,gif,png,jpeg,jpg,cad,pdf,odt,doc,txt.
                              </div>
                            </div>
                          

                            <div id="msg_bid_attach" style="color: red;font-size: 12px;font-weight:600;display: none;"></div>

                           @if(isset($arr_bid_data['bid_attachment']) && trim($arr_bid_data['bid_attachment']) != "")
                            <div class="attach-text" style="float: left;margin-top:15px;margin-right:-55px; " id="download_attach">
                               <a title="Download Previous Proposal" download href="{{$bid_attachment_public_path}}/{{$arr_bid_data['bid_attachment']}}" >
                               <span style="color: #c42881;" ><i class="fa fa-download"></i></span>&nbsp;{{ trans('expert/projects/update_project_bid.text_download_proposal') }} 
                               </a>
                            </div>
                            @endif
                            @if(isset($arr_bid_data['nda_aggreement_pdf']) && trim($arr_bid_data['nda_aggreement_pdf']) != "" && file_exists('public/uploads/front/nda-agreement/'.$arr_bid_data['nda_aggreement_pdf']))
                           <div class="user-box" style="max-width: 300px; width: 100%;">
                              <div class="input-name">
                                  <div class="upload-block">
                                    <div class="">
                                        <a title="Download Previous Proposal" download href="{{url('/public/uploads/front/nda-agreement/')}}/{{$arr_bid_data['nda_aggreement_pdf']}}" >
                                          <div class="p-control-label"><i class="fa fa-paperclip"></i> NDA agreement : <span style="color: #0E81C2;"><i class="fa fa-download"></i></span></div>
                                        </a>    
                                     </div>
                                  </div>
                                  <div id="msg_bid_attach" style="color: red;font-size: 12px;font-weight:600;display: none;"></div>
                                  <div class="user-box hidden-lg">&nbsp;</div>
                                  <div class="note_browse"></div>
                               </div>
                           </div> 
                           @endif 
                          <div class="user-box hidden-lg">&nbsp;</div>
                          <div class="note_browse"></div>
                        </div>
                      {{-- </div>  --}}

                      <div class="clr"></div>

                      <button type="submit" value="" class="det-sub-btn">{{ trans('expert/projects/update_project_bid.text_update') }}</button>
                    </div>
                  </div>

                   {{-- bid attachmenys section --}}
                      {{--  @if(isset($arr_bid_data['bid_attachments']) && count($arr_bid_data['bid_attachments']) > 0)
                        <div class="col-sm-12 col-md-12 col-lg-12" > 
                          <span id="msg_bid_attach" style="color: red;" ></span>
                              <table style="margin-top:10px; ">
                                  <thead>
                                    <tr>
                                      <th>Sr. No.</th>
                                      <th>Document Name</th>
                                      <th>Download</th>
                                    </tr>
                                  </thead>

                                  <tbody >
                                  @foreach($arr_bid_data['bid_attachments'] as $key => $bid_data )
                                    @if($bid_data['attachment'] != "")
                                    <tr>
                                      <td>{{$key + 1}}</td>
                                      <td>{{ $bid_data['attachment'] or '' }}</td>
                                      <td><a title="Download Bid Attchment" download href="{{$bid_attachment_public_path}}{{$bid_data['attachment']}}" >
                                        <span style="color: #0E81C2;"><i class="fa fa-download"></i></span>
                                     </a>&nbsp;<span style="color: #C32880;" ><i style="cursor: pointer;" onclick="deleteBidAttachment('{{base64_encode($bid_data['id'])}}')" class="fa fa-trash"></i></span></td>
                                    </tr>
                                    @endif
                                   @endforeach 
                                  </tbody>
                                </table>

                            </div> 
                        @endif     --}}
                       {{-- ends  --}}

               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<!-- </div> -->
<script type="text/javascript">

   $(document).ready(function()
   {
        set_custom_hour_per_week();

       $('#frm-bid-details').validate();   

       $('#bid_attachment').change(function()
       {
         if($(this).val().length>0)
         {
           $("#btn_remove_image").show();
         }

         $('#profile_image_name').val($(this).val());
       });
    });
   
   function set_custom_hour_per_week() {
        if ($('#hour_per_week').val() == 'HOUR_CUSTOM_RATE') {
            $('.hour_custom_rate_description').html('Custom'+'<span class="star-col-block">*</span>');
            $('#hour_custom_rate').show();
        } else {
            $('.hour_custom_rate_description').html('');
            $('#hour_custom_rate').hide();
        }
    }

   function browseImage()
   {
     $("#bid_attachment").trigger('click');
   }
   
   function removeBrowsedImage()
   {
     
     if(!confirm('Are you sure ? Do you want to delete bid attachment'))
     {
        return false;
     }
     
    var bid_id = {{$arr_bid_data['id']}};
     
     $('#profile_image_name').val("");
     $("#btn_remove_image").hide();
     $("#bid_attachment").val("");
     
      $.ajax({
      url:"{{url('/public')}}"+"/expert/delete_attachment",
      data:{ bid_id: btoa(bid_id) },
      success:function (response)
        {
           if(typeof response.status != "undefined" && response.status == "SUCCESS_BID_ATTACHMENT")
           {
              $("#download_attach").hide();
              $('#msg_bid_attach').html(response.msg).show();
           }
        }
      });     
   }

   /* This function deletes bid attchment */

    function deleteBidAttachment(attchment_id)
   {
      if(!confirm('Are you sure ? Do you want to delete bid attachment'))
      {
        return false;
      }

      $.ajax({
      url:"{{url('/public')}}"+"/expert/delete_attachment",
      data:{ id: attchment_id },
      success:function (response) 
        {
           showProcessingOverlay();
           window.location.reload();
           if(typeof response.status != "undefined" && response.status == "SUCCESS_BID_ATTACHMENT")
           {
             /*$('#msg_bid_attach').html(response.msg).show().fadeOut(5000);
              $('#attch_'+attchment_id).hide();*/            
           }
        }
      });
   }

  function chk_validation(ref)
  {
    var yourInput = $(ref).val();
    re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
    var isSplChar = re.test(yourInput);
    if(isSplChar)
    {
      var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
      $(ref).val(no_spl_char);
    }
  }

</script>

<script type="text/javascript">
  
  var arr_job_document_formats = ['png','docx','xls','xlsx','gif','png','jpeg','jpg','cad','pdf','odt','doc','txt'];

  function addpictures(ref)
  { 
      var new_images = $("input[name='contest_attachments[]']").map(function(){
             return $(this).val();
         }).get();

      var count = new_images.length;
      //alert(count);

      if(count > 1)
      {
          swal('U can upload only 1 file');
          return false;
      }            

      $('#err_other_image').html('');
      var image_id = $(ref).closest('.lab_img').attr('id');
      var length = $('.lab_img').length;
      var view_photo_cnt = jQuery('#'+image_id).find('.photo_view').length                                
      jQuery('#'+image_id).last().find( ".div_hidden_photo_list" ).last().find( "input[name='contest_attachments[]']:last" ).click(); 

      jQuery('#'+image_id).last().find( ".div_hidden_photo_list" ).last().find( "input[name='contest_attachments[]']:last" ).change(function()
      { 
          var files      = this.files;
          var exist_file = $('#file_name_lab').val();
          
          if(exist_file == files[0]['name']) 
          { 
              return false; 
          }
          else 
          {
              $('#file_name_lab').val(files[0]['name']);
              for (var i=0, l=files.length; i<l; i++) 
              {
                  var max_size = 5000000;
                  var current_size = files[i].size;
                  if (max_size>=current_size) 
                  {
                      var file = files[i];

                      var prjct_id = image_id.split('_');
                      jQuery('#'+image_id).find('#image'+prjct_id[1]+'_'+(view_photo_cnt+1)).attr('value',files[i]['name']);
                      var img, reader, xhr;
                      img = document.createElement("img");
                      reader = new FileReader();
                      img = new Image();      

                      var ext      =   files[i]['name'].split('.').pop();  

                      if ($.inArray(ext, arr_job_document_formats) == -1)
                      {                                          
                          swal('File type not allowed');
                          return false;
                      }
                      else
                      {
                          img.onload = function()
                          {
                                               
                          }                            
                      }

                      reader.onload = (function (theImg) 
                      {      
                          if(ext == 'docx' || ext == 'doc' || ext == 'pdf' || ext == 'zip' || ext == 'mp3' || ext == 'odt' || ext == 'txt' || ext == 'xlsx')
                          {                          
                              var image_src = '{{url('/public')}}/front/images/file_formats/'+ext+'.png';      
                              return function (evt){                                       
                                  theImg.src = evt.target.result;                                
                                  var html = "<div class='photo_view2' onclick='remove_this(this);' style='width:120px;height:120%;position:relative;display: inline-block;'><img src="+ image_src +" class='add_pht' id='add_pht upload-pic' style='float: left; padding: 0px ! important; margin:0' width='120' height='120'><div class='overlay2'><span class='plus2'><i class='fa fa-trash-o' aria-hidden='true'></i></span></div></div>";
                                  jQuery('#'+image_id).last().find('.show_photos').append(html);
                                  jQuery('#'+image_id).last().find('.div_hidden_photo_list').append('<input type="file" name="contest_attachments[]" id="contest_attachments" class="contest_attachments" style="display:none" />');   
                                  //countImages();          
                                  $('#file_name_lab').val('');
                                  };
                          }
                          else if(ext == 'jpg' || ext == 'png' || ext == 'jpeg' || ext == 'gif' || ext == 'JPG' || ext == 'PNG' || ext == 'JPEG' || ext == 'GIF')
                          { 
                              return function (evt){                                       
                                  theImg.src = evt.target.result;                                
                                  var html = "<div class='photo_view2' onclick='remove_this(this);' style='width:120px;height:120%;position:relative;display: inline-block;'><img src="+ evt.target.result +" class='add_pht' id='add_pht upload-pic' style='float: left; padding: 0px ! important; margin:0' width='120' height='120'><div class='overlay2'><span class='plus2'><i class='fa fa-trash-o' aria-hidden='true'></i></span></div></div>";
                                  jQuery('#'+image_id).last().find('.show_photos').append(html);
                                  jQuery('#'+image_id).last().find('.div_hidden_photo_list').append('<input type="file" name="contest_attachments[]" id="contest_attachments" class="contest_attachments" style="display:none" />');   
                                  //countImages();          
                                  $('#file_name_lab').val('');
                                  };
                          }
                          else
                          {
                              var image_src = '{{url('/public')}}/uploads/front/default/default.jpg';  
                              return function (evt){   
                                  theImg.src = evt.target.result;                                
                                  var html = "<div class='photo_view2' onclick='remove_this(this);' style='width:120px;height:120%;position:relative;display: inline-block;'><img src="+ image_src +" class='add_pht' id='add_pht upload-pic' style='float: left; padding: 0px ! important; margin:0' width='120' height='120'><div class='overlay2'><span class='plus2'><i class='fa fa-trash-o' aria-hidden='true'></i></span></div></div>";
                                  jQuery('#'+image_id).last().find('.show_photos').append(html);
                                  jQuery('#'+image_id).last().find('.div_hidden_photo_list').append('<input type="file" name="contest_attachments[]" id="contest_attachments" class="contest_attachments" style="display:none" />');   
                                  //countImages();          
                                  $('#file_name_lab').val('');
                                  };
                          } 

                      }(img));
                      reader.readAsDataURL(file);                        
                  }
                  else
                  {
                      swal('File size should be less than 5MB');
                      return false;
                  }                    
              };
          }        
      });          
  } 

  function remove_this(elm)
  {
     var this_index = jQuery(elm).index();
     jQuery('.lab_img').find(".div_hidden_photo_list").find("input").eq(this_index).remove();
     jQuery(elm).remove();
     //countImages();
  }  

</script>
@stop