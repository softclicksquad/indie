<?php $user = Sentinel::check();
if($user != false) { 
$user_data = $user->toArray(); ?>
<!-- Wrapper/Container Table: Use a wrapper table to control the width and the background color consistently of your email. Use this approach instead of setting attributes on the body tag. -->
<table cellpadding="0" style="100%" cellspacing="0" border="1">
   <tr>
      <td>
         <table cellpadding="0"  align="center" cellspacing="0" border="0">
            <tr>
               <td>
                  <!-- Tables are the most common way to format your email consistently. Set your table widths inside cells and in most cases reset cellpadding, cellspacing, and border to zero. Use nested tables as a way to space effectively in your message. -->
                  <table cellpadding="0" cellspacing="0" border="0" align="center" >
                     <tr>
                        <td class='movableContentContainer bgItem'>                           
                           <table cellpadding="0" cellspacing="0" border="0" align="center" >    
                              <tr colspan="3" height="40">
                                 <td>&nbsp;</td>                                 
                              </tr>                                      
                              <tr>                                             
                                 <td colspan="3" valign="top" align="center">                                    
                                    <img src="{{url('/public')}}/front/images/virtual-logo.png"   alt='{{config('app.project.name')}}' data-default="placeholder" />                                    
                                 </td>                                             
                              </tr>                                          
                           </table>                                                      
                           <table >
                              <tr>
                                 <td width="100%" colspan="3" align="center">                                    
                                    <h3><i>NDA Project Aggreement</i></h3>                                       
                                 </td>
                              </tr>
                              <tr>                                             
                                 <td colspan="3" align="left">                                    
                                    <p style="padding-right: 30px;">Hello {{$user_data['role_info']['first_name']}} {{substr($user_data['role_info']['last_name'],0,1).'.'}},<br>
                                        <br/><br/>
                                        <span>
                                          The Parties. Ths is an agreement between {{config('app.project.name')}} concerning the confidentiality of information relating to the Company. References in this agreement to "The client" mean <Company Name > and all subsidiaries, affiliated companies, associated companies and holding companies together with all and any successors in title and assignees of any of the above. Proposed Association Of The Parties. The Company wishes to employ or to contract with, or to enter into discussions in anticipation of employing or contracting with, Dextrous Info Solutions. The compensation for his/her employment or contract will be not only for his/her services but also for the confidential manner in which his/her services will be performed. Recognition of a Compelling Need For Confidentiality. Dextrous Info Solutions realizes that the Company has a compelling need to maintain confidentiality, and further recognizes that his/her employment or contract with the Company, or his/her discussions with the Company for such employment or contract, will place him in a position of special trust and confidence with access to confidential information concerning the Company and its operations. Consideration. For the reasons explained above, Dextrous Info Solutions, as a precondition to his/her employment or contract with the Company, and in partial consideration, agrees and covenants with the Company as follows.
                                        </span>
                                        <br/> 
                                    </p>                                    
                                 </td>                                             
                              </tr>
                           </table>
                           <br/>
                           <table cellpadding="0" cellspacing="0" border="0" align="center" >
                              <tr>                                 
                                 <td colspan="3" align="left">
                                    <div class="contentEditableContainer contentTextEditable">
                                       <div class="contentEditable" align='left' >
                                          <p >Thank you,
                                             <br/>
                                             Team {{config('app.project.name')}}.
                                          </p>
                                       </div>
                                    </div>
                                 </td>                                 
                              </tr>
                              <tr>                                 
                                 <td colspan="3" align="left">    
                                    <i>{{$user_data['email'] or ''}}</i><br>                                
                                    <label for="nda-agrrement-proposal">Accepted</label>                                    
                                 </td>                                 
                              </tr>
                           </table>                                                      
                           <table cellpadding="0" cellspacing="0" border="0" align="center" >
                              <tr>
                                 <td width="100%" colspan="2">
                                    <hr style="height:1px;border:none;color:#333;background-color:#ddd;" />
                                 </td>
                              </tr>
                              <tr>
                                 <td width="100%" valign="middle" style="padding-bottom:20px;">
                                    <div class="contentEditableContainer contentTextEditable">
                                       <div class="contentEditable" align='center' >
                                          <span style="font-size:13px;color:#181818;font-family:Helvetica, Arial, sans-serif;line-height:200%;">Copyright &#169; {{date("Y")}} <a href="{{url('/')}}" target="_blank" style="text-decoration:none;">{{config('app.project.name')}}</a>. All&nbsp;rights&nbsp;reserved.</span>
                                          <br/>
                                       </div>
                                    </div>
                                 </td>
                              </tr>
                           </table>                           
                        </td>
                     </tr>
                  </table>
               </td>
            </tr>
         </table>
      </td>
   </tr>
  </table>         
<?php } ?>  