<?php $user = Sentinel::check();
if($user != false) { 
   $user_data = $user->toArray(); ?>
<style type="text/css">
.accept-checkbox-main [type="checkbox"] + label{vertical-align: middle;margin-bottom: 0;float: left;}
    .employer-details-section-block{float: left}
    .contractor-details-section-block{float: right}
    .details-section-block-main{padding-bottom: 20px;margin-bottom: 20px;border-bottom: 1px solid #dddddd;}
    .agreement-content-section{position: relative;padding-left: 17px;display: block;padding-bottom: 10px;}
    .content-count-section{position: absolute;left: 0;top: 0;display: block}
    .archexperts-link-section a{color: #000000;text-decoration: underline;}
    .concluded-text-section{padding-top: 10px;text-align: center}
</style>
<!-- Wrapper/Container Table: Use a wrapper table to control the width and the background color consistently of your email. Use this approach instead of setting attributes on the body tag. -->
<table cellpadding="0" style="100%" cellspacing="0" border="1" id="backgroundTable" class='bgBody'>
   <tr>
      <td>
         <table cellpadding="0"  align="center" cellspacing="0" border="0">
            <tr>
               <td>
                  <!-- Tables are the most common way to format your email consistently. Set your table widths inside cells and in most cases reset cellpadding, cellspacing, and border to zero. Use nested tables as a way to space effectively in your message. -->
                  <table cellpadding="0" cellspacing="0" border="0" align="center" >
                     <tr>
                        <td class='movableContentContainer bgItem'>
                           <div class='movableContent'>
                              <table cellpadding="0" cellspacing="0" border="0" align="center" >
                                 <tr height="40">
                                    <td width="200">&nbsp;</td>
                                    <td width="200">&nbsp;</td>
                                    <td width="200">&nbsp;</td>
                                 </tr>
                                 <tr>
                                    <td width="200" valign="top">&nbsp;</td>
                                    <td width="200" valign="top" align="center">
                                       <div class="contentEditableContainer contentImageEditable">
                                          <div class="contentEditable" align='center' >
                                             <img src="{{url('/public')}}/front/images/virtual-logo.png"   alt='{{config('app.project.name')}}' style="height:30px;" data-default="placeholder" />
                                          </div>
                                       </div>
                                    </td>
                                    <td width="200" valign="top">&nbsp;</td>
                                 </tr>
                                 <tr height="25">
                                    <td width="200">&nbsp;</td>
                                    <td width="200">&nbsp;</td>
                                    <td width="200">&nbsp;</td>
                                 </tr>
                              </table>
                           </div>
                           <div class='movableContent'>
                              <table cellpadding="0" cellspacing="0" border="0" align="center" >
                                 <tr>
                                    <td width="100%" colspan="3" align="center" style="padding-bottom:10px;padding-top:0px;">
                                       <div class="contentEditableContainer contentTextEditable">
                                          <div class="contentEditable" align='center' >
                                             <h3><i>NDA Project Aggreement</i></h3>
                                          </div>
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td width="100%" colspan="3" align="center" style="padding-bottom:10px;padding-top:0px;">
                                       <div class="contentEditableContainer contentTextEditable">
                                          <div class="contentEditable archexperts-link-section" align='center' >
                                             arranged through the portal <a target="_blank" href="{{url('/')}}">Archexperts</a>
                                          </div>
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td width="100">&nbsp;</td>
                                    <td width="400" align="center">
                                       <div class="contentEditableContainer contentTextEditable">
                                          <div class="contentEditable" align='left' >
                                             <div class="details-section-block-main">
                                                 <div class="employer-details-section-block">
                                                     Employer : <br/>
                                                     Sudarshan Client D<br/> 
                                                     Nashik Railway Track Rd, <br/>Nawle Colony,  <br/>Government Colony,  <br/>Nashik,  <br/>Maharashtra 422214, India
                                                 </div>
                                                 <div class="contractor-details-section-block">
                                                     Contractor :<br/>
                                                     Shankar J.<br/> 
                                                     Nashik Railway Track Rd, <br/>Nawle Colony,  <br/>Government Colony,  <br/>Nashik,  <br/>Maharashtra 422214, India
                                                 </div>
                                                 <div class="clearfix"></div>
                                                 <div class="concluded-text-section">
                                                     The following Non Disclosure Agreement is concluded.
                                                 </div>
                                              </div>
                                             <p style="text-align: justify !important;">Hello {{$user_data['role_info']['first_name']}} {{substr($user_data['role_info']['last_name'],0,1).'.'}},<br><i>{{$user_data['email'] or ''}}</i>
                                                 <br/><br/>
                                                 <span class="agreement-content-section">
                                                     <span class="content-count-section">1</span> The contractor undertakes to keep the secret knowledge and information communicated to him for the preparation of the assigned work as well as the work itself, its various stages of development and forms, as well as in connection with the creation of modifications and all information on the identity and purpose of the client to keep secret. The contractor undertakes to take all necessary measures to prevent the third parties from gaining knowledge and information. Employees and subcontractors are required to maintain secrecy unless they are already detained on the basis of their employment contract.
                                                 </span>
                                                 <span class="agreement-content-section">
                                                     <span class="content-count-section">2</span> The obligation of secrecy does not apply to developments that are already obvious and thus no longer secret or protectable. If disclosure of a development occurs later, then the obligation expires on that date.
                                                 </span>
                                                 <span class="agreement-content-section">
                                                     <span class="content-count-section">3</span> This obligation of confidentiality shall continue to apply if the cooperation agreement is not concluded or terminated, unless the development is now obvious, for which the client bears the burden of proof.
                                                 </span>
                                                 <span class="agreement-content-section">
                                                     <span class="content-count-section">4</span> Irrespective of any claim for damages, the Contractor undertakes to pay a contractual penalty of EUR 1,000.00 for each case of culpable violation of this agreement.
                                                 </span>
                                                 <span class="agreement-content-section">
                                                     <span class="content-count-section">5</span> German law applies to the contract and this confidentiality declaration. For disputes arising from this declaration, the court at the registered office of the contractor has local jurisdiction, insofar as the client is a merchant.
                                                 </span>
                                                 <span class="agreement-content-section">
                                                     <span class="content-count-section">6</span> Should one or more provisions of this declaration be or become invalid, this shall not affect the validity of the remaining provisions. The parties undertake to replace the ineffective provision with a provision which comes closest to the intended economic purpose.
                                                 </span>                                                 
                                                 <br/> 
                                             </p>
                                             <div class="contractor-details-section-block">
                                                 Nashik, 27/11/2019
                                             </div>                                             
                                             <div class="clearfix"></div>
                                          </div>
                                       </div>
                                    </td>
                                    <td width="100">&nbsp;</td>
                                 </tr>
                              </table>
                              <br/>
                              <table cellpadding="0" cellspacing="0" border="0" align="center" >
                                 <tr>
                                    <td width="100">&nbsp;</td>
                                    <td width="400" align="center">
                                       <div class="contentEditableContainer contentTextEditable">
                                          <div class="contentEditable" align='left' >
                                             <p >Thank you,
                                                <br/>
                                                Team {{config('app.project.name')}}.
                                             </p>
                                          </div>
                                       </div>
                                    </td>
                                    <td width="100">&nbsp;</td>
                                 </tr>
                                 <tr>
                                    <td width="100">&nbsp;</td>
                                    <td width="400" align="center">
                                       <div class="terms accept-checkbox-main">
                                            <div class="check-box">
                                                <p>
                                                 <input id="nda-agrrement-proposal" class="filled-in" @if(isset($project_details['id']) && $project_details['id']!="") checked=""  @endif type="checkbox" name="nda-agrrement-proposal" data-rule-required="true"> 
                                                 Accept By Shankar J.
                                                 <label for="nda-agrrement-proposal"></label>
                                                </p>
                                            </div>
                                        </div>
                                    </td>
                                    <td width="100">&nbsp;</td>
                                 </tr>
                              </table>
                           </div>
                           <div class='movableContent'>
                              <table cellpadding="0" cellspacing="0" border="0" align="center" >
                                 <tr>
                                    <td width="100%" colspan="2" style="padding-top:10px;">
                                       <hr style="height:1px;border:none;color:#333;background-color:#ddd;" />
                                    </td>
                                 </tr>
                                 <tr>
                                    <td width="100%" height="70" valign="middle" style="padding-bottom:20px;">
                                       <div class="contentEditableContainer contentTextEditable">
                                          <div class="contentEditable" align='center' >
                                             <span style="font-size:13px;color:#181818;font-family:Helvetica, Arial, sans-serif;line-height:200%;">Copyright &#169; {{date("Y")}} <a href="{{url('/')}}" target="_blank" style="text-decoration:none;">{{config('app.project.name')}}</a>. All&nbsp;rights&nbsp;reserved.</span>
                                             <br/>
                                          </div>
                                       </div>
                                    </td>
                                 </tr>
                              </table>
                           </div>
                        </td>
                     </tr>
                  </table>
               </td>
            </tr>
         </table>
      </td>
   </tr>
</table>
<?php } ?>  