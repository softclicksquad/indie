@extends('expert.layout.master')
@section('main_content')

@php 
    $user = Sentinel::check(); 
@endphp
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" integrity="sha256-Z8TW+REiUm9zSQMGZH4bfZi52VJgMqETCbPFlGRB1P8=" crossorigin="anonymous" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.js" integrity="sha256-ZvMf9li0M5GGriGUEKn1g6lLwnj5u+ENqCbLM5ItjQ0=" crossorigin="anonymous"></script>

<div class="col-sm-8 col-md-8 col-lg-9">
    @if(isset($arr_applied_projects['data']) && sizeof($arr_applied_projects['data'])>0)
        <div class="proect-listing">
            @include('front.layout._operation_status')
            <div class="head_grn">{{trans('expert/projects/applied_projects.text_applied_project_title')}}</div> 
            @foreach($arr_applied_projects['data'] as $key=>$proRec)
                @if(isset($proRec['project_details']))
                    <div class="white-block-bg big-space hover">
                        <div class="project-left-side">
                            <div class="search-freelancer-user-head">
                                <a href="{{ $module_url_path }}/bid/update/{{ base64_encode($proRec['project_details']['id'])}}">
                                 {{$proRec['project_details']['project_name']}}
                                </a>
                            </div>
                                  
                            <div class="search-freelancer-user-location">
                                <span class="gavel-icon"><img src="{{url('/public')}}/front/images/bid.png" alt="" /></span> 
                                <span class="dur-txt"> {{trans('expert/projects/applied_projects.text_bid_date')}}: {{date('d M Y',strtotime($proRec['created_at']))}}</span> 
                            </div>

                            <div class="more-project-dec">{{ str_limit($proRec['project_details']['project_description'],340) }}
                            </div>
                            <?php
                              $projects_cost = explode('-',$proRec['project_details']['project_cost']);
                              if( (isset($projects_cost[0]) && $projects_cost[0]!='') && (isset($projects_cost[1]) && $projects_cost[1]!=''))
                              {
                                  $projects_cost = number_format(floor($projects_cost[0])).'-'.number_format(floor($projects_cost[1]));
                              }
                              elseif($projects_cost[0]!='')
                              {
                                  $projects_cost = number_format(floor($projects_cost[0]));
                              }

                              $text_hour = '';
                              if(isset($proRec['project_details']['project_pricing_method']) && $proRec['project_details']['project_pricing_method'] == '2'){
                                $text_hour = trans('common/_top_project_details.text_hour');
                              }
                            ?>
                            <div class="project-list new-lid imgsize">
                                <ul>
                                    <li title="Expected Duration" class="expented-cls" >
                                        <span class="hidden-xs hidden-sm" title="Expected Days">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        </span>{{ $proRec['project_details']['project_expected_duration'] }} 
                                    </li>
                                    <li style="background:none;" title="Project Cost" >
                                        <span class="projrct-prce aplid-projts">
                                            @if(isset($proRec['project_details']['project_currency']) && $proRec['project_details']['project_currency']=="$")
                                                <i class="fa fa-money hidden-xs hidden-sm" aria-hidden="true"></i>

                                                {{$proRec['project_details']['project_currency']}}  {{$projects_cost }}{{$text_hour}} 

                                                @if($proRec['project_details']['project_currency_code'] != $user->currency_code)
                                                  @if(isset($proRec['project_details']['is_custom_price']) && $proRec['project_details']['is_custom_price']==0)
                                                      ({{ $user->currency_code ?? '' }} {{ number_format(floor(currencyConverterAPI($proRec['project_details']['project_currency_code'],$user->currency_code,$proRec['project_details']['min_project_cost']))) }}-{{ number_format(floor(currencyConverterAPI($proRec['project_details']['project_currency_code'],$user->currency_code,$proRec['project_details']['max_project_cost']))) }}{{ $text_hour }})
                                                  @else
                                                      ({{ $user->currency_code ?? '' }} {{ number_format(floor(currencyConverterAPI($proRec['project_details']['project_currency_code'],$user->currency_code,$proRec['project_details']['min_project_cost']))) }}{{ $text_hour }})
                                                  @endif
                                                @endif

                                            @else
                                                <i class="fa fa-money hidden-xs hidden-sm" aria-hidden="true"></i>
                                                {{$proRec['project_details']['project_currency']}} {{$projects_cost }}{{$text_hour}} 
                                                @if($proRec['project_details']['project_currency_code'] != $user->currency_code)
                                                  @if(isset($proRec['project_details']['is_custom_price']) && $proRec['project_details']['is_custom_price']==0)
                                                      ({{ $user->currency_code ?? '' }} {{ number_format(floor(currencyConverterAPI($proRec['project_details']['project_currency_code'],$user->currency_code,$proRec['project_details']['min_project_cost']))) }}-{{ number_format(floor(currencyConverterAPI($proRec['project_details']['project_currency_code'],$user->currency_code,$proRec['project_details']['max_project_cost']))) }}{{$text_hour}})
                                                  @else
                                                      ({{ $user->currency_code ?? '' }} {{ number_format(floor(currencyConverterAPI($proRec['project_details']['project_currency_code'],$user->currency_code,$proRec['project_details']['max_project_cost']))) }}{{$text_hour}})
                                                  @endif
                                                @endif

                                            @endif 
                                        </span>
                                    </li>
                                </ul>
                            </div>  

                            <div class="category-new">
                              <img src="{{url('/public')}}/front/images/tag-2.png" alt="" /> {{$proRec['project_details']['category_details']['category_title'] or 'NA'}}

                              &nbsp;&nbsp;
                              <img src="{{url('/public')}}/front/images/tag-2.png" alt="" /> {{isset($proRec['project_details']['sub_category_details']['subcategory_title'])?$proRec['project_details']['sub_category_details']['subcategory_title']:'NA'}}
                            </div>

                            <div class="clearfix"></div>

                            <div class="skils-project">
                                @if(isset($proRec['project_details']['project_skills'])  && count($proRec['project_details']['project_skills']) > 0)
                                    <img src="{{url('/public')}}/front/images/tag.png" alt="" />
                                @endif
                                <ul>
                                    @if(isset($proRec['project_details']['project_skills'])  && count($proRec['project_details']['project_skills']) > 0)
                                        @foreach($proRec['project_details']['project_skills'] as $key=>$skills)
                                            @if(isset($skills['skill_data']['skill_name']) &&  $skills['skill_data']['skill_name'] != "")
                                                <li>{{ str_limit($skills['skill_data']['skill_name'],25) }}
                                                </li>
                                            @endif
                                        @endforeach
                                    @endif    
                                </ul>
                            </div>
                        </div>

                        <div class="search-project-listing-right contest-right">
                            <div class="search-project-right-price">
                                {{isset($proRec['project_details']['project_currency']) && $proRec['project_details']['project_currency']!=''?$proRec['project_details']['project_currency']:'$'}} {{isset($proRec['bid_cost'])?number_format($proRec['bid_cost']):''}}{{ $text_hour }}<br>
                                <span>{{ get_currency_description($proRec['project_details']['project_currency_code']) ?? '' }}</span>
                            </div>
                            @if($proRec['project_details']['project_currency_code'] != $user->currency_code)
                              <div class="default-currency-price">
                                ({{ $user->currency_code ?? '' }} {{ number_format(floor(currencyConverterAPI($proRec['project_details']['project_currency_code'],$user->currency_code,$proRec['bid_cost']))) }}{{ $text_hour }})
                              </div>
                            @endif
                        </div>

                        <div class="invite-expert-btns">
                            <a class="black-border-btn" href="{{ $module_url_path }}/details/{{ base64_encode($proRec['project_details']['id'])}}">
                            <span class="">  {{trans('expert/projects/applied_projects.text_view')}}  </span> 
                            </a> 
                            @if(isset($proRec['bid_status']) &&  $proRec['bid_status'] == "3")
                                <a class="black-border-btn"  href="javascript:void(0);">
                                    <span class="">{{trans('expert/projects/applied_projects.text_rejected')}}  </span> 
                                </a>   
                            @else
                                @if(isset($proRec['bid_status']) &&  $proRec['bid_status'] != "4")
                                    <a class="black-btn" href="{{ $module_url_path }}/bid/update/{{ base64_encode($proRec['project_details']['id'])}}">
                                        <span class="">  {{trans('expert/projects/applied_projects.text_update')}} </span>
                                  </a>   
                                @else
                                    <a class="awd_btn" href="javascript:void(0)">
                                        <span class="">  {{trans('expert/projects/applied_projects.text_bid_awarded')}}</span>
                                    </a> 
                                @endif
                            @endif

                            @php
                                $is_award_request_received = project_award_request($user->id,$proRec['project_id']);
                            @endphp

                            @if(isset($is_award_request_received) && $is_award_request_received=='1')
                                <a class="blue_btn" onclick="confirmaccept(this)" data-project-id="{{ isset($proRec['project_id'])?base64_encode($proRec['project_id']):'' }}"><span class=""> {{trans('expert/projects/awarded_projects.text_accept')}} </span>
                                </a>

                                <a class="black-btn" onclick="rejectProject(this)" data-project-id="{{ isset($proRec['project_id'])?base64_encode($proRec['project_id']):'' }}"><span class=""> {{trans('expert/projects/awarded_projects.text_reject')}} </span>
                                </a>
                            @endif

                            @if(isset($proRec['is_chat_initiated']) &&  $proRec['is_chat_initiated'] == "1")
                              <a href="{{url('/expert/twilio-chat/projectbid')}}/{{base64_encode($proRec['id'])}}" class="applozic-launcher">
                                  <button class="black-btn" >{{trans('client/projects/project_details.message')}}</button>
                              </a>
                            @endif
                        </div>

                        <div class="clearfix"></div>
                    </div>
                @endif
            @endforeach 
        </div>
    @else
        <div class="search-grey-bx">
            <div class="no-record" >
               {{trans('expert/projects/awarded_projects.text_sorry_no_record_found')}}
            </div>
        </div>
    @endif
</div>

<!-- Pagination -->
@include('front.common.pagination_view', ['paginator' => $arr_pagination])
<!-- ends -->
</div>

  {{-- tag to open modal --}}
  <a data-toggle="modal" style="display: none;" data-target="#myModal" id="open_modal"></a>
  {{-- ends --}}
  <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog modal-lg">

          <!-- Modal content-->
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 style="color: #0E99E5;" class="modal-title"><b>
                          {{ trans('new_translations.reject_project_bid_title')}}
                      </b></h4>
              </div>
              <div class="modal-body">
                  <form method="post" id="frm_reject_modal" action="{{url('/')}}/expert/projects/awarded/reject">
                      <div class="row">
                          {{csrf_field()}}
                          <input type="hidden" readonly="" name="project_id" id="modal_project_id" value="" />

                          <div class="col-sm-12 col-md-12 col-lg-12">
                              <div class="user-bx">
                                  <textarea cols="" rows="" name="rejection_reason" id="rejection_reason" class="det-in1" style="font-size: 14px;font-style:normal;color: #3B3B3D;" placeholder="{{ trans('new_translations.enter_comments')}}.."></textarea>
                                  <div id="chk" style="display: none;" class="error">{{ trans('new_translations.this_field_is_required')}}</div>
                              </div>
                              <button type="button" id="submit-btn" class="sb-det-btn">{{ trans('new_translations.submit')}}</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
      </div>
  </div>

<script type="text/javascript">
  
  function confirmaccept(ref){
    var confirm_message = "{!! trans('expert/projects/awarded_projects.confirm_accept_project') !!}";
    // alertify.confirm(confirm_message, function(e) {
    //     if (e) {
    //         var project_id = $(ref).attr('data-project-id');
    //         showProcessingOverlay();
    //         window.location.href = "{{ $module_url_path }}/awarded/accept/" + project_id;
    //         return true;
    //     } else {
    //         return false;
    //     }
    // });
    swal({
              title: confirm_message,
              text: "",
              icon: "warning",
              
              dangerMode: true,
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
             },
             function(isConfirm){

               if (isConfirm){
                  var project_id = $(ref).attr('data-project-id');
                  showProcessingOverlay();
                 window.location.href = "{{ $module_url_path }}/awarded/accept/" + project_id;
                    return true;

                } else {
                    swal.close();
                  return false;
                }
             });
  }

  function rejectProject(ref) {
      var project_id = $(ref).attr('data-project-id');
      if (project_id.trim() != "") {
          $('#open_modal').click();
          $('#modal_project_id').prop('value', project_id.trim());
      } else {
          $('#chk').html('Error while rejecting project. Please try after some time.');
          return false;
      }
  }

  $('#submit-btn').click(function() {
    if ($('#rejection_reason').val() == '') {
        $('#chk').show();
        return false;
    } else if ($('#rejection_reason').val() != '') {
        // alertify.confirm("Are you sure? You want to reject project ?", function(e) {
        //     if (e) {
        //         showProcessingOverlay();
        //         $('#frm_reject_modal').submit();
        //         // return true;
        //     } else {
        //         return false;
        //     }
        // });
        swal({
              title: "Are you sure? You want to reject project ?",
              text: "",
              icon: "warning",
              
              dangerMode: true,
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
             },
             function(isConfirm){

               if (isConfirm){
                  var project_id = $(ref).attr('data-project-id');
                 showProcessingOverlay();
                 $('#frm_reject_modal').submit();
                    return true;

                } else {
                    swal.close();
                  return false;
                }
             });
    }
  });
</script>
@stop