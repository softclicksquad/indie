@extends('expert.layout.master')                
@section('main_content')
<div class="col-sm-8 col-md-8 col-lg-9">
   <form action="{{ url($module_url_path) }}/manage" method="POST" id="form-manage-projects" name="form-manage-projects" enctype="multipart/form-data" files ="true">
      {{ csrf_field() }}
         <div class="right_side_section">
            @include('front.layout._operation_status')
            <div class="head_grn">{{trans('expert/projects/open_projects.text_open_project_title')}}</div>
            @if(isset($arr_open_projects['data']) && sizeof($arr_open_projects['data'])>0)
            @foreach($arr_open_projects['data'] as $proRec)
            <div class="ongonig-project-section client-dash">
               <div class="project-title">
                  <a href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}">
                     <h3>{{$proRec['project_name']}}</h3>
                  </a>
                  <div class="sub-project-dec">{{trans('expert/projects/open_projects.text_est_time')}}:{{$proRec['project_expected_duration']}}</div>
                  <div class="more-project-dec">{{str_limit($proRec['project_description'],350)}}</div>
               </div>
               <div class="row">
                  <div class="col-sm-12 col-md-1 col-lg-1"><span class="colrs">{{trans('expert/projects/open_projects.text_skills')}}:</span></div>
                  <div class="col-sm-12 col-md-11 col-lg-11">
                     <div class="skils-project">
                        <ul>
                           @if(isset($proRec['project_skills'])  && count($proRec['project_skills']) > 0)
                           @foreach($proRec['project_skills'] as $key=>$skills) 
                           @if(isset($skills['skill_data']['skill_name']) &&  $skills['skill_data']['skill_name'] != "")
                           <li style="font-size:12px; " >{{ str_limit($skills['skill_data']['skill_name'],18) }}</li>
                           @endif
                           @endforeach
                           @endif    
                        </ul>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <a href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}" class="view_btn hidden-xs hidden-sm hidden-md">
                     <i class="fa fa-eye" aria-hidden="true"></i> {{trans('expert/projects/open_projects.text_view')}} 
                     </a>
                  </div>
               </div>
            </div>
            <div class="clr"></div>
            <hr/>
            @endforeach
            @else
            <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;">
               <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                  <div class="search-content-block">
                     <div class="no-record">
                        {{trans('expert/projects/open_projects.text_sorry_no_record_found')}}
                     </div>
                  </div>
               </td>
            </tr>
            @endif
         </div>
         <!-- Paination Links -->
         @include('front.common.pagination_view', ['paginator' => $arr_pagination])
         <!-- Paination Links -->
      
   </form>
</div>
@stop