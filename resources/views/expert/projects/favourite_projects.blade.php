@extends('expert.layout.master')                
@section('main_content')


<div class="col-sm-8 col-md-8 col-lg-9">
      @include('front.layout._operation_status')
      @if(isset($arr_favourite_projects['data']) && sizeof($arr_favourite_projects['data'])>0) 
        @foreach($arr_favourite_projects['data'] as $key=>$proRec)
          @if(isset($proRec['project_details']))
            <div class="search-grey-bx">
    
               @if($key == 0)
                 <div class="head_grn">{{trans('expert/projects/favourite_projects.text_favourite_project_title')}}</div>
               @endif

               <div class="row">
                  <div class="col-sm-9 col-md-9 col-lg-12">

                    <div class="ongonig-project-section client-dash">
                          <div class="project-title">
                             <a href="{{ $module_url_path }}/details/{{ base64_encode($proRec['project_details']['id'])}}">
                                <h3>{{$proRec['project_details']['project_name']}}</h3>
                             </a>
                             
                              <span title="Remove Favourite ?" onclick="addOrRemoveFavourite(this)" data-action="{{ base64_encode('remove') }}" style="cursor: pointer;" data-project-id="{{isset($proRec['project_details']['id'])?base64_encode($proRec['project_details']['id']):''}}" class="favourite pull-right" ><i class="fa fa-heart"></i></span>

                             <div class="sub-project-dec">{{trans('expert/projects/favourite_projects.text_project_duration')}}:&nbsp;{{$proRec['project_details']['project_expected_duration']}}</div>
                             
                             <div class="more-project-dec">{{str_limit($proRec['project_details']['project_description'],350)}}</div>
                          </div>
                          <div class="row">
                             <div class="col-sm-12 col-md-1 col-lg-1  skills-de-new"><span class="colrs">{{trans('expert/projects/favourite_projects.text_skills')}}:</span></div>
                             <div class="col-sm-12 col-md-11 col-lg-11  skill-de-content">
                                <div class="skils-project">
                                   <ul>
                                      @if(isset($proRec['project_details']['project_skills'])  && count($proRec['project_details']['project_skills']) > 0)
                                      @foreach($proRec['project_details']['project_skills'] as $key=>$skills) 
                                      @if(isset($skills['skill_data']['skill_name']) &&  $skills['skill_data']['skill_name'] != "")
                                      <li style="font-size:12px; " >{{ str_limit($skills['skill_data']['skill_name'],18) }}</li>
                                      @endif
                                      @endforeach
                                      @endif    
                                   </ul>
                                </div>
                             </div>
                             <div class="col-md-12">
                                <a href="{{ $module_url_path }}/details/{{ base64_encode($proRec['project_details']['id'])}}">{{--  class="view_btn hidden-xs hidden-sm hidden-md"> --}}
                                <button class="view_btn" >
                                <i class="fa fa-eye" aria-hidden="true"></i>&nbsp;{{trans('expert/projects/favourite_projects.text_view')}} 
                                </button>
                                </a>
                             </div>
                          </div>
                       </div>

                  </div>
               
      </div>
   </div>
   @endif
   @endforeach 
   @else

   <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;">
      <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
         <div class="search-grey-bx">
            <div class="head_grn">{{trans('expert/projects/favourite_projects.text_favourite_project_title')}}</div>
            <br/>
            <div class="no-record">
               {{trans('expert/projects/favourite_projects.text_sorry_no_record_found')}}
            </div>
         </div>
      </td>
   </tr>
   @endif

<!-- Pagination -->
@include('front.common.pagination_view', ['paginator' => $arr_pagination])
</div>
<!-- ends -->
<script type="text/javascript">
    function addOrRemoveFavourite(ref)
    {
      var project_id = $(ref).attr('data-project-id');
      var action     = $(ref).attr('data-action');

      var confirm_message = '{!! trans('expert/projects/favourite_projects.confirm_remove_to_favourite') !!}';

      if(!confirm(confirm_message))
      {
        return false;
      }
      
      if(project_id != "")
      {
        var data  = { project_id:project_id,_token:"{{csrf_token()}}",action:action };
          
        $.ajax({
        url:"{{url('/public')}}"+'/projects/add_or_remove_favourite',
        type:'POST',
        dataType:'json',
        data: data,
        beforeSend: function()
        {
          showProcessingOverlay();                  
        },
        success:function(response)
        {
          if(response.status == "FAVOURITE")
          {
            console.log('Project added to favourites successfully.');
          }

          if(response.status=="UNFAVOURITE")
          {
            console.log('Project added to favourites successfully.');
          }

          window.location.reload();

        }
        });

      }
    }
   
   
</script>
@stop