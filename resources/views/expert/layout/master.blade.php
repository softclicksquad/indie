<!-- HEader -->        
@include('front.layout.header')    
        
<!-- BEGIN Sidebar -->

<!-- END Sidebar -->

<!-- BEGIN Content -->
@include('expert.layout.sidebar')

    @yield('main_content')

    <!-- END Main Content -->

<!-- Footer -->    

@include('expert.layout.dynamic_footer') 

@include('front.layout.footer')    
                
              