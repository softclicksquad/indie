<!-- HEader -->        
@include('front.layout.header')    

<!-- BEGIN Content -->
    @yield('main_content')
<!-- END Main Content -->

<!-- Footers -->    

@include('expert.layout.dynamic_footer') 

@include('front.layout.footer')    
                
              