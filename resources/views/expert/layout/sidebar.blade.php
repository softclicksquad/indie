<?php
   $currentUrl  = Route::getCurrentRoute()->getPath();
   $user = Sentinel::check(); 
   $sidebar_information = array();  
   if($user){
    $profile_img_public_path = url('/').config('app.project.img_path.profile_image');
    $sidebar_information = sidebar_information($user->id);
   }

  //$wallet_bal = 0;
  //$mangopay_wallet_details = get_logged_user_wallet_details();
  //$wallet_bal = isset($mangopay_wallet_details->Balance->Amount)? $mangopay_wallet_details->Balance->Amount/100:'0';
  //$wallet_currency = isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:'';
  
  $project_type = "";   
  $url = \URL::previous();
  $arr_url = [];
  $check_current_url = ['expert/projects/awarded',
                        'expert/projects/ongoing',
                        'expert/projects/applied',
                        'expert/projects/completed',
                        'expert/projects/canceled',
                        'expert/projects/invitations',
                        'expert/contest/applied-contest'
                        ];
  if($url != "" && !in_array($currentUrl, $check_current_url) )
  {  
    $arr_url =  explode('/', $url);
    if(isset($arr_url[7]) && $arr_url[7] == 'awarded'){
      $project_type = 'awarded';   
    }
    else if(isset($arr_url[7]) && $arr_url[7] == 'ongoing'){
      $project_type = 'ongoing';   
    }
    else if(isset($arr_url[7]) && $arr_url[7] == 'applied'){
      $project_type = 'applied';   
    }
    else if(isset($arr_url[7]) && $arr_url[7] == 'completed'){
      $project_type = 'completed';   
    }
    else if(isset($arr_url[7]) && $arr_url[7] == 'canceled'){
      $project_type = 'canceled';   
    }
    else if(isset($arr_url[7]) && $arr_url[7] == 'invitations'){
      $project_type = 'invitations';   
    }
    else if(isset($arr_url[7]) && $arr_url[7] == 'applied-contest'){
      $project_type = 'applied-contest';   
    }
  }
?>
 {{-- Top Navbar --}}
   <div class="top-title-pattern pattern-title-top top-links project-status-strip">
      <div class="container">
         <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
               <div class="title-box-top">  
                   <!--<div id="region_id" class="meshows hidden-lg hidden-md">{{ trans('new_translations.projects')}}</div>-->
                   <h4 class="visible-xs">{{ trans('new_translations.projects')}} <span class="arrow"><i class="fa fa-angle-down"></i></span></h4>       
                 <ul>
                    <li @if($currentUrl=='expert/projects/applied') class="top-menu-active"   @elseif($project_type == 'applied') class="top-menu-active" @endif>
                       <a href="{{url('expert/projects/applied')}}" @if($currentUrl=='expert/projects/applied') style="color: #2d2d2d;"  @elseif($project_type == 'applied') style="color: #2d2d2d;"  @endif >{{ trans('common/expert_sidebar.text_bids_on_projects') }}
                          <button class="btn-prj-count" type="button">
                            @if(isset($sidebar_information['applied_project_count']) && $sidebar_information['applied_project_count'])
                               @if($sidebar_information['applied_project_count'] >99)
                                 @php $applied_project_count = '99+'; @endphp
                               @else 
                                 @php $applied_project_count = $sidebar_information['applied_project_count']; @endphp 
                               @endif
                            @else
                               @php $applied_project_count = 0; @endphp
                            @endif
                            {{$applied_project_count}}
                          </button>
                       </a>
                    </li>
                    <li @if($currentUrl=='expert/projects/awarded') class="top-menu-active"   @elseif($project_type == 'awarded') class="top-menu-active" @endif>
                       <a href="{{url('expert/projects/awarded')}}" @if($currentUrl=='expert/projects/awarded') style="color: #2d2d2d;"  @elseif($project_type == 'awarded') style="color: #2d2d2d;"  @endif >{{ trans('common/expert_sidebar.text_awarded') }}
                       <button class="btn-prj-count" type="button">
                          @if(isset($sidebar_information['awarded_project_count']) && $sidebar_information['awarded_project_count'])
                             @if($sidebar_information['awarded_project_count'] >99)
                               @php $awarded_project_count = '99+'; @endphp
                             @else 
                               @php $awarded_project_count = $sidebar_information['awarded_project_count']; @endphp 
                             @endif
                          @else
                             @php $awarded_project_count = 0; @endphp
                          @endif
                         {{$awarded_project_count}}
                       </button>
                       </a>
                    </li>
                    <li @if($currentUrl=='expert/projects/ongoing') class="top-menu-active" @elseif($project_type == 'ongoing') class="top-menu-active" @endif>
                       <a href="{{url('expert/projects/ongoing')}}" @if($currentUrl=='expert/projects/ongoing') style="color: #2d2d2d;" @elseif($project_type == 'ongoing') style="color: #2d2d2d;" @endif >{{ trans('common/expert_sidebar.text_ongoing') }}
                       <button class="btn-prj-count" type="button">
                        @if(isset($sidebar_information['ongoing_project_count']) && $sidebar_information['ongoing_project_count'])
                           @if($sidebar_information['ongoing_project_count'] >99)
                             @php $ongoing_project_count = '99+'; @endphp
                           @else 
                             @php $ongoing_project_count = $sidebar_information['ongoing_project_count']; @endphp 
                           @endif
                        @else
                           @php $ongoing_project_count = 0; @endphp
                        @endif
                        {{$ongoing_project_count}}
                       </button>
                       </a>
                    </li>
                    <li @if($currentUrl=='expert/projects/completed') class="top-menu-active" @elseif($project_type == 'completed') class="top-menu-active" @endif>
                     <a href="{{url('expert/projects/completed')}}" @if($currentUrl=='expert/projects/completed') style="color: #2d2d2d;" @elseif($project_type == 'completed') style="color: #2d2d2d;" @endif >{{ trans('common/expert_sidebar.text_completed') }}
                       <button class="btn-prj-count" type="button">
                        @if(isset($sidebar_information['completed_project_count']) && $sidebar_information['completed_project_count'])
                           @if($sidebar_information['completed_project_count'] >99)
                             @php $completed_project_count = '99+'; @endphp
                           @else 
                             @php $completed_project_count = $sidebar_information['completed_project_count']; @endphp 
                           @endif
                        @else
                           @php $completed_project_count = 0; @endphp
                        @endif
                        {{$completed_project_count}}
                       </button>
                     </a>
                    </li>
                    <li @if($currentUrl=='expert/projects/canceled') class="top-menu-active" @elseif($project_type == 'canceled') class="top-menu-active" @endif>
                     <a href="{{url('expert/projects/canceled')}}"  @if($currentUrl=='expert/projects/canceled') style="color: #2d2d2d;" @elseif($project_type == 'canceled') style="color: #2d2d2d;"  @endif >{{ trans('common/expert_sidebar.text_canceled') }}
                      <button class="btn-prj-count" type="button">
                        @if(isset($sidebar_information['canceled_project_count']) && $sidebar_information['canceled_project_count'])
                           @if($sidebar_information['canceled_project_count'] >99)
                             @php $canceled_project_count = '99+'; @endphp
                           @else 
                             @php $canceled_project_count = $sidebar_information['canceled_project_count']; @endphp 
                           @endif
                        @else
                           @php $canceled_project_count = 0; @endphp
                        @endif
                        {{$canceled_project_count}}
                      </button> 
                     </a>
                    </li>
                    <li @if($currentUrl=='expert/projects/invitations') class="top-menu-active" @elseif($project_type == 'invitations') class="top-menu-active" @endif>
                     <a href="{{url('expert/projects/invitations')}}"  @if($currentUrl=='expert/projects/invitations') style="color: #2d2d2d;" @else style="color: #7b7b7b;"  @endif >{{ trans('common/expert_sidebar.text_invitations') }}
                      <button class="btn-prj-count" type="button">
                        @if(isset($sidebar_information['invitation_project_count']) && $sidebar_information['invitation_project_count'])
                           @if($sidebar_information['invitation_project_count'] >99)
                             @php $invitation_project_count = '99+'; @endphp
                           @else 
                             @php $invitation_project_count = $sidebar_information['invitation_project_count']; @endphp 
                           @endif
                        @else
                           @php $invitation_project_count = 0; @endphp
                        @endif
                        {{$invitation_project_count}}
                      </button> 
                     </a>
                    </li>
                    <li @if($currentUrl=='expert/contest/applied-contest' || Request::segment(2) == 'contest') class="top-menu-active"  @endif>
                        <a  @if($currentUrl=='expert/contest/applied-contest' || Request::segment(2) == 'contest') style="color: #000;"  @endif href="{{url('/expert/contest/applied-contest')}}" >{{trans('client/contest/post.text_contests')}}
                          <button class=" btn-prj-count" type="button">
                            @if(isset($sidebar_information['contest_applied_count']) && $sidebar_information['contest_applied_count'])
                               @if($sidebar_information['contest_applied_count'] >99)
                                 @php $contest_applied_count = '99+'; @endphp
                               @else 
                                 @php $contest_applied_count = $sidebar_information['contest_applied_count']; @endphp 
                               @endif
                            @else
                               @php $contest_applied_count = 0; @endphp
                            @endif
                            {{$contest_applied_count}}
                          </button> 
                        </a>
                    </li>
                 </ul>                 
               </div>
            </div>
         </div>
      </div>
   </div>
{{-- Top Ends --}}
<div class="middle-container">
<div class="container">
<div class="row">
<div class="col-sm-4 col-md-4 col-lg-3">
   <div class="left-sidebar user-left-bar-main">      
      <div class="left-side-tabs">
        <div class="dashboard-menu-head">Dashboard Menu <span><i class="fa fa-angle-down"></i></span></div>
        <ul class="responsive-menu-hide-section">
            <li @if($currentUrl=='expert/dashboard') class="active-p" @endif><a href="{{url('/')}}/expert/dashboard" class="dashboards-p">{{ trans('common/client_sidebar.text_dashboard') }}</a></li>

            <li >
               <a class="acc-setting">{{-- {{ trans('common/expert_sidebar.text_account_settings') }} --}} User Setting <span class="arrow"><i class="fa fa-angle-down"></i></span></a>
                <ul class="sub-menu" @if($currentUrl=='expert/profile' || 
                                         $currentUrl=='expert/change_password' ||
                                         $currentUrl=='expert/availability' ||
                                         $currentUrl=='expert/portfolio') style="display:block;" @else style="display:none" @endif >
                    <li @if($currentUrl=='expert/profile') class="active-p" @endif><a href="{{url('/expert/profile')}}" class="edit-profile3">{{ trans('common/expert_sidebar.text_edit_profile') }}</a></li>
                    <li @if($currentUrl=='expert/change_password') class="active-p" @endif><a href="{{url('/expert/change_password')}}" class="change-p-3">{{ trans('common/expert_sidebar.text_change_password') }}</a></li>
                    <li @if($currentUrl=='expert/availability') class="active-p" @endif><a href="{{url('/expert/availability')}}" class="availability-p">{{ trans('common/expert_sidebar.text_availability') }}</a></li>
                    <li @if($currentUrl=='expert/portfolio') class="active-p" @endif><a href="{{url('/expert/portfolio')}}" class="review-rating-icn">{{ trans('common/expert_sidebar.text_portfolio') }}</a></li>

                    @if(isset($user->mp_wallet_created) && $user->mp_wallet_created == 'Yes')
                      <li @if($currentUrl=='expert/wallet/dashboard' ) class="active-p" @endif><a href="{{url('/expert/wallet/dashboard')}}" class="mangopay-p"><img style="width: 27px;height: 25px;" src="{{url('/public/front/images/archexpertdefault/wallet.png')}}" alt="mangopay"> Payment and financials</a></li>
                    @endif

                    <li @if($currentUrl=='expert/wallet/transactions' ) class="active-p" @endif><a href="{{url('/expert/wallet/transactions')}}" class="mangopay-p"><img style="width: 27px;height: 25px;" src="{{url('/public/front/images/archexpertdefault/wallet.png')}}" alt="mangopay"> Mangopay Transactions</a></li>

                </ul>
            </li>

            <li >
               <a class="ongoing-projects-icon">{{ trans('common/expert_sidebar.text_my_projects') }} <span class="arrow"><i class="fa fa-angle-down"></i></span></a>
                <ul class="sub-menu" @if($currentUrl=='expert/projects/applied'   || 
                                         $currentUrl=='expert/projects/awarded'   ||
                                         $currentUrl=='expert/projects/ongoing'   ||
                                         $currentUrl=='expert/projects/completed' ||
                                         $currentUrl=='expert/projects/canceled') style="display:block;" @else style="display:none" @endif >
                    <li @if($currentUrl=='expert/projects/applied') class="active-p" @endif>
                       <a href="{{url('expert/projects/applied')}}" class="applied-p">{{ trans('common/expert_sidebar.text_bids_on_projects') }}</a>
                    </li>
                    <li @if($currentUrl=='expert/projects/awarded') class="active-p" @endif>
                       <a href="{{url('expert/projects/awarded')}}" class="review-rating-icn">{{ trans('common/expert_sidebar.text_awarded_projects') }}</a>
                    </li>
                    <li @if($currentUrl=='expert/projects/ongoing') class="active-p" @endif>
                       <a href="{{url('expert/projects/ongoing')}}" class="ongoing-pd-3">{{ trans('common/expert_sidebar.text_ongoing_projects') }}</a>
                    </li>
                    <li @if($currentUrl=='expert/projects/completed') class="active-p" @endif>
                     <a href="{{url('expert/projects/completed')}}"  class="completed-p-4">{{ trans('common/expert_sidebar.text_completed_projects') }}</a>
                    </li>
                    <li @if($currentUrl=='expert/projects/canceled') class="active-p" @endif>
                     <a href="{{url('expert/projects/canceled')}}"   class="cancell-p">{{ trans('common/expert_sidebar.text_canceled_projects') }}</a>
                    </li>
                </ul>
            </li>
            <li> 
               <a class="contest-icon">{{trans('client/contest/post.text_contests')}} <span class="arrow"><i @if($currentUrl=='expert/contest/applied-contest' || Request::segment(2) == 'contest') class="fa fa-angle-up" @else class="fa fa-angle-down" @endif></i></span></a>
                <ul class="sub-menu"  @if($currentUrl=='expert/contest/applied-contest' || Request::segment(2) == 'contest') style="display:block;" @else style="display:none" @endif>
                    <li @if($currentUrl=='expert/contest/applied-contest' || Request::segment(3) == 'show_contest_entry_details'){ class="active-p" @endif><a href="{{url('/expert/contest/applied-contest')}}" class="applied-p">Applied Contest</a></li>
                </ul>
            </li>

            <li>
               <a class="transactions-icns">{{ trans('common/expert_sidebar.text_payment') }} <span class="arrow"><i class="fa fa-angle-down"></i></span></a>
                <ul class="sub-menu" @if($currentUrl=='expert/subscription' || 
                                         $currentUrl=='expert/settings/payment'|| 
                                         $currentUrl=='expert/transactions'|| 
                                         $currentUrl=='expert/sealedentrieshistory'||
                                         $currentUrl=='expert/subscriptionhistory'||
                                         $currentUrl=='expert/wallet/dashboard') style="display:block;" @else style="display:none" @endif >
                    {{-- <li @if($currentUrl=='expert/wallet/dashboard') class="active-p" @endif><a href="{{url('/expert/wallet/dashboard')}}" class="mangopay-p"><img style="width: 27px;height: 25px;" src="{{url('/public/front/images/archexpertdefault/wallet.png')}}" alt="mangopay">&nbsp;&nbsp; My Wallet</a></li> --}}
                     
                     @if(isset($user->mp_wallet_created) && $user->mp_wallet_created == 'Yes')
                     <li @if($currentUrl=='expert/wallet/withdraw' ) class="active-p" @endif><a href="{{url('/expert/wallet/withdraw')}}" class="mangopay-p"><img style="width: 27px;height: 25px;" src="{{url('/public/front/images/archexpertdefault/wallet.png')}}" alt="mangopay">&nbsp; Withdraw</a></li>
                     @endif

                    <li @if($currentUrl=='expert/subscription') class="active-p" @endif><a href="{{url('/expert/subscription')}}" class="subscription-icon">{{ trans('common/expert_sidebar.text_subscription') }}</a></li>
                    <!-- <li @if($currentUrl=='expert/settings/payment') class="active-p" @endif><a href="{{url('expert/settings/payment')}}" class="payment-settings1-icns">{{ trans('common/expert_sidebar.text_payment_settings') }}</a></li> -->
                    <li @if($currentUrl=='expert/transactions') class="active-p" @endif><a href="{{url('/expert/transactions')}}" class="transactions-icns">{{ trans('common/expert_sidebar.text_transactions') }}</a></li>
                    <li @if($currentUrl=='expert/sealedentrieshistory') class="active-p" @endif><a href="{{url('/expert/sealedentrieshistory')}}" class="applied-p">Sealed Entries History</a></li>
                    <li @if($currentUrl=='expert/subscriptionhistory') class="active-p" @endif><a href="{{url('/expert/subscriptionhistory')}}" class="applied-p">{{ trans('common/expert_sidebar.text_subscription_history') }}</a></li>

                </ul>
            </li>

            <!-- <li @if($currentUrl=='expert/projects/favourites') class="active-p" @endif><a href="{{url('/expert/projects/favourites')}}" class="favort-2">{{ trans('common/expert_sidebar.text_favourites') }}</a></li> -->
            <li @if($currentUrl=='expert/projects/dispute') class="active-p" @endif><a href="{{url('/expert/projects/dispute')}}" class="dispute-icn">{{ trans('common/expert_sidebar.text_dispute') }}</a></li>

            <!-- Support Ticket -->
            <li @if($currentUrl=='expert/tickets' ) class="active-p" @endif><a href="{{url('/expert/tickets')}}" class="support-ticket-icn">
                <!-- {{ trans('common/client_sidebar.tickets') }} -->
                Support Ticket
            </a></li>
            <!-- Support Ticket -->

            <li @if($currentUrl=='expert/twilio-chat/chat_list' ) class="active-p" @endif>
                <a href="{{url('expert/twilio-chat/chat_list')}}" class="inbox-p-8">{{ trans('common/client_sidebar.text_messages') }}</a>
                {{-- <a href="javascript:void(0)" class="applozic-launcher inbox-p-8">{{ trans('common/client_sidebar.text_messages') }}</a> --}}
            </li>

            <li @if($currentUrl=='expert/subscription') class="active-p" @endif>
              
              <a href="{{url('expert/subscription')}}" class="subscription dashboards-p">Subscription</a>
            </li>

         </ul>
      </div>
   </div>
</div>
<script>
    $('.title-box-top h4').click(function(){
       $(this).next('ul').slideToggle();
        $(this).find('.arrow i').toggleClass('fa-angle-down fa-angle-up');
    });
</script>
<script>
    $('.left-side-tabs li').click(function(){
       $(this).children('.sub-menu').slideToggle();
       $(this).children('a').find('.arrow i').toggleClass('fa-angle-down fa-angle-up');
    });
</script>
<script type="text/javascript">
   $('#image').on('click', function() { 
         $('#profile_image').click(); 
   });
   $('#profile_image').change(function(){
      var formData = new FormData();
      formData.append('file', $('input[type=file]')[0].files[0]);
        $.ajax({
           url: '{{url('/')}}/expert/store_profile_image?_token='+"{{ csrf_token()}}",
           type: 'POST',
           data: formData,
           cache: false,
           contentType: false,
           processData: false,
           success:function(response)
           {
              if(response.status=="SUCCESS")
              {   
                 $('#set_image').attr("src",response.image_name);
                 console.log(response.msg); 
              }
              else if(response.status=="ERROR")
              { 
                 console.log(response.msg);
              }
           }
        });
      });
</script>
    
      <script>
                $(document).ready(function() {
                    $('[data-toggle="tooltip"]').tooltip();
                });
            </script>
    
     <script>
                $(".dashboard-menu-head").on("click", function() {
                    $(".responsive-menu-hide-section").slideToggle("slow");
                    $(this).toggleClass("active");
                })
            </script>