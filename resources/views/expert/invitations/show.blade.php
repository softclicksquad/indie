@extends('expert.layout.master')                
@section('main_content')
<div class="col-sm-7 col-md-8 col-lg-9">
  @include('front.layout._operation_status')
  <div class="search-grey-bx">
    <div class="head_grn">{{ trans('expert/invitations/packs.text_heading_details') }}</div>
    @if(isset($invitation) && sizeof($invitation)>0)
    <div class="row">
       <div class="col-sm-12 col-md-6 col-lg-12 mrgt">
        <div class="row">
           <div class="form_group">
            <div class="col-sm-12 col-md-8 col-lg-3">
             <div class="card_holder">{{ trans('expert/invitations/packs.text_unique_id') }} : </div>
           </div>
           <div class="col-sm-12 col-md-4 col-lg-9">
             <div class="fill_input">  
              {{isset($invitation['unique_id'])?$invitation['unique_id']:''}}
            </div>
          </div>
          <div class="clr"></div>
        </div>
      </div>
            <!-- <div class="row">
               <div class="form_group">
                  <div class="col-sm-12 col-md-8 col-lg-3">
                     <div class="card_holder">{{ trans('expert/invitations/packs.text_project_name') }} : </div>
                  </div>
                  <div class="col-sm-12 col-md-4 col-lg-9">
                     <div class="fill_input">  
                        {{isset($invitation['project_details']['project_name'])?$invitation['project_details']['project_name']:''}}
                     </div>
                  </div>
                  <div class="clr"></div>
               </div>
            </div>
            <div class="row">
               <div class="form_group">
                  <div class="col-sm-12 col-md-8 col-lg-3">
                     <div class="card_holder">{{ trans('expert/invitations/packs.text_client') }} : </div>
                  </div>
                  <div class="col-sm-12 col-md-4 col-lg-9">
                     <div class="fill_input">
                     {{isset($invitation['client_details']['first_name'])?$invitation['client_details']['first_name']:''}}
                     {{isset($invitation['client_details']['last_name'])?substr($invitation['client_details']['last_name'],0,1).'.':''}}
                     </div>
                  </div>
                  <div class="clr"></div>
               </div>
             </div> -->
             <div class="row">
               <div class="form_group">
                <div class="col-sm-12 col-md-8 col-lg-3">
                 <div class="card_holder">{{ trans('expert/invitations/packs.text_notification') }} : </div>
               </div>
               <div class="col-sm-12 col-md-4 col-lg-9">
                 <div class="fill_input">
                   @if(App::isLocale('en'))
                   <td>{{isset($invitation['notification_details']['notification_text_en'])?$invitation['notification_details']['notification_text_en']:''}}</td>
                   @else
                   <td>{{isset($invitation['notification_details']['notification_text_de'])?$invitation['notification_details']['notification_text_de']:''}}</td>
                   @endif
                 </div>
               </div>
               <div class="clr"></div>
             </div>
           </div>
           <div class="row">
             <div class="form_group">
              <div class="col-sm-12 col-md-8 col-lg-3">
               <div class="card_holder">{{ trans('expert/invitations/packs.text_invite_date') }} : </div>
             </div>
             <div class="col-sm-12 col-md-4 col-lg-9">
               <div class="fill_input"> 
                <?php echo date('j M  Y', strtotime($invitation['create_at']))/*." at ".date("g:i a", strtotime($invitation['create_at']))*/; ?> 
              </div>
            </div>
            <div class="clr"></div>
          </div>
        </div>
        <div class="row">
         <div class="form_group">
          <div class="col-sm-12 col-md-8 col-lg-3">
           <div class="card_holder">{{ trans('expert/invitations/packs.text_expired_on') }} : </div>
         </div>
         <div class="col-sm-12 col-md-4 col-lg-9">
           <div class="fill_input" id="timer<?php echo $invitation["id"]; ?>">{{trans('client/projects/ongoing_projects.text_next_expire')}}</div>
           <?php 
           $invitation_time    = strtotime(date('Y-m-d H:i:s'));
           $invitation_endtime = strtotime($invitation['expire_on']);
           $diffInSeconds = $invitation_endtime - $invitation_time;
           if($diffInSeconds <= 0){
             $diffInSeconds = '0';
           }?>
           <script type="text/javascript">
             var time         = '<?php echo $diffInSeconds; ?>';
             var invitationid = '<?php echo $invitation["id"]; ?>';
             if(time < 0) {  time = 0; }

                       var c           = time; // in seconds
                       var t;
                       var days    = parseInt( c / 86400 ) % 365;
                       var hours   = parseInt( c / 3600 ) % 24;
                       var minutes = parseInt( c / 60 ) % 60;
                       var seconds = c % 60;

                       //var result  = (days < 10 ? "0" + days : days) + ":"+ (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
                       
                       var d = ""; 
                       var h = ""; 
                       var m = "";
                       var s = ""; 

                       if(days     !=  ""  &&  days     !="0"){  d  =  (days+'d')     +  " ";  }
                       if(hours    !=  ""  &&  hours    !="0"){  h  =  (hours+'h')    +  " ";  }
                       if(minutes  !=  ""  &&  minutes  !="0"){  m  =  (minutes+'m')  +  " ";  }
                       if(seconds  !=  ""  &&  seconds  !="0"){  s  =  (seconds+'s')  +  " ";  }
                       
                       if(d != "" && h !=""){
                         var result    = d + h;
                       }else if(h != "" && m !=""){
                         var result    = h + m;
                       }else if(m !=""){
                         var result    = m;
                       }else if(s !=""){
                         var result    = s;
                       }
                       //var result    = d + h + m + s;
                       if(result == "NaN:NaN:NaN:NaN"){ 
                         $('#timer'+invitationid).html('{{trans('client/projects/ongoing_projects.text_next_expire')}}');
                         $('#timer'+invitationid).css('color','red');
                       } else {
                         $('#timer'+invitationid).html(result);
                         $('#timer'+invitationid).css('color','green');
                       }
                       if(c == 0 ){
                         $('#timer'+invitationid).html('{{trans('client/projects/ongoing_projects.text_next_expire')}}');
                         $('#timer'+invitationid).css('color','red');
                       }
                       c = c - 1;
                     </script>
                   </div>
                   <div class="clr"></div>
                 </div>
               </div>
               <div class="row">
                 <div class="form_group">
                  <div class="col-sm-12 col-md-8 col-lg-3">
                   <div class="card_holder">{{ trans('expert/invitations/packs.text_message') }} : </div>
                 </div>
                 <div class="col-sm-12 col-md-4 col-lg-9">
                   <div class="fill_input">   
                    {{isset($invitation['message'])?$invitation['message']:''}}
                  </div>
                </div>
                <div class="clr"></div>
              </div>
            </div>
            <div class="row">
             <div class="form_group">
              <div class="col-sm-12 col-md-8 col-lg-3">
               <div class="card_holder">{{ trans('expert/invitations/packs.text_Url') }} : </div>
             </div>
             <div class="col-sm-12 col-md-4 col-lg-9">
               <div class="fill_input"> 
                @if(isset($invitation['project_details']['project_status']) && $invitation['project_details']['project_status'] == 2)
                @if(isset($invitation['notification_details']['url']) && $invitation['notification_details']['url'] != "")
                @if($invitation['expire_on'] < date('Y-m-d H:i:s') ||  $invitation['expire_on'] == null)
                <a href="javascript:void(0);" style="cursor:no-drop;">
                 <span class="msg_btn-lnew ht-tp msg-btn-auto-height" style="padding-left: 21px;padding-top: 4px;"><i class="fa fa-eye"></i>
                 {{ trans('expert/invitations/packs.text_view')}}</span>
               </a>
               @else
               <a title="View" href="{{url('/expert/projects/details/')}}/{{isset($invitation['project_id'])?base64_encode($invitation['project_id']):base64_encode(0)}}">
                <span class="msg_btn-lnew ht-tp msg-btn-auto-height" style="padding-left: 21px;padding-top: 4px;"><i class="fa fa-eye"></i>
                {{ trans('expert/invitations/packs.text_view')}}</span>
              </a>
                <a title="Delete" onclick="javascript:return confirm_delete('{{url('/expert/projects/invitations/delete/'.base64_encode($invitation['id'])) }}')" href="javascript:void(0)" >
                <span class="msg_btn-lnew ht-tp msg-btn-auto-height" style="padding-left: 21px;padding-top: 4px;"><i class="fa fa-trash"></i> Delete
                </span>
              </a>
              @endif
              @endif
              @else
              <span style="color:red;">{{trans('client/projects/ongoing_projects.text_next_expire')}}
                <!-- Tool Tip section-->
                <div class="tooltip-block">
                 <div class="info-icon"><img src="{{url('/public')}}/front/images/info-icon.png" class="img-responsive" alt=""/></div>
                 <span class="tooltip-content"> 
                  <li><b>{{ trans('expert/invitations/packs.text_expected_reasons') }}</b></li>
                  <li>1.{{ trans('expert/invitations/packs.text_may_be_project_have_awarded_to_someone_else') }}</li>
                  <li>2.{{ trans('expert/invitations/packs.text_may_be_project_has_canceled') }}</li>
                </span>
              </div>
              <!-- Tool Tip section-->
            </span> 
            @endif  
          </div>
        </div>
        <div class="clr"></div>
      </div>
    </div>
  </div>
</div>
@endif
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
  function confirm_delete(url) {
      alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
          if (e) {
              if(url != "" && url != undefined){
                showProcessingOverlay();
                window.location.href=url;
              }
              return true;
          } else {
              return false;
          }
      });
   }
</script>
@stop

