@extends('expert.layout.master')                
@section('main_content')
<style type="text/css">
.msg-btn-auto-height{  
   padding: 4px 5px; font-size:13px;
}
.table-expert-invitation{
   font-size:11px;   
}
</style>
<div class="col-sm-7 col-md-8 col-lg-9">
  @include('front.layout._operation_status')
   <div class="search-grey-bx white-wrapper">
      <div class="head_grn">{{ trans('expert/invitations/packs.text_heading') }}</div>
      <div class="table-responsive">
         <form id="frm_show_cnt" class="form-horizontal show-entry-form" method="POST" action="{{url('expert/invitations/show_cnt')}}">
             {{ csrf_field() }}
               <div class="col-sm-2 col-md-2 col-lg-2">
                  <div class="sort-by">
                       @php $show_cnt ='show_invitations_cnt';  @endphp
                       @include('front.common.show-cnt-selectbox')
                     </div>
               </div>
         </form>
         <table id="TaBle" class="theme-table invoice-table-s table table-expert-invitation" style="border: 1px solid rgb(239, 239, 239); margin-bottom:0;">
            <thead>
               <tr>
                  <th>{{ trans('expert/invitations/packs.text_heading') }}</th>
                  <th>{{ trans('expert/invitations/packs.text_invite_date') }}</th>
                  <th>{{ trans('expert/invitations/packs.text_expired_on') }}</th>
                  <th>{{ trans('expert/invitations/packs.text_action') }}</th>
               </tr>
            </thead>
            <tbody>

               @if(isset($arr_invitations['data']) && sizeof($arr_invitations['data'])>0)
               @foreach($arr_invitations['data'] as $invitation)

               <tr 
                  @if($invitation['is_read'] == 'NO' && $invitation['expire_on'] < date('Y-m-d H:i:s') ||  $invitation['expire_on'] == null) 
                    style="color:red;" 
                  @elseif($invitation['is_read'] == 'NO')  
                    style="color:#3361a9;" 
                  @endif
                  >
                  
                  @if(App::isLocale('en'))
                  <td style="width: 300px;">{{isset($invitation['notification_details']['notification_text_en'])?$invitation['notification_details']['notification_text_en']:''}}</td>
                  @else
                  <td style="width: 300px;">{{isset($invitation['notification_details']['notification_text_de'])?$invitation['notification_details']['notification_text_de']:''}}</td>
                  @endif

                  <td><?php echo date('j M  Y', strtotime($invitation['create_at']))/*." at ".date("g:i a", strtotime($invitation['create_at']))*/; ?></td>
                  @if(isset($invitation['project_details']['project_status']) && $invitation['project_details']['project_status'] == 2)
                    <td id="timer<?php echo $invitation["id"]; ?>">{{trans('client/projects/ongoing_projects.text_next_expire')}}</td>
                  @else
                    <td>
                       <span style="color:red;">{{trans('client/projects/ongoing_projects.text_next_expire')}}
                          <!-- Tool Tip section-->
                           <div class="tooltip-block top-tooltip">
                             <div class="info-icon"><img src="{{url('/public')}}/front/images/info-icon.png" class="img-responsive" alt=""/></div>
                             <span class="tooltip-content pull-left"> 
                                  <li><b>{{ trans('expert/invitations/packs.text_expected_reasons') }}</b></li>
                                  <li>1.{{ trans('expert/invitations/packs.text_may_be_project_have_awarded_to_someone_else') }}</li>
                                  <li>2.{{ trans('expert/invitations/packs.text_may_be_project_has_canceled') }}</li>
                             </span>
                           </div>
                          <!-- Tool Tip section-->
                         </span>  
                    </td>
                  @endif
                  <?php 
                  $invitation_time    = strtotime(date('Y-m-d H:i:s'));
                  $invitation_endtime = strtotime($invitation['expire_on']);
                  $diffInSeconds = $invitation_endtime - $invitation_time;
                  if($diffInSeconds <= 0){
                    $diffInSeconds = '0';
                  }?>
                  <script type="text/javascript">
                       var time         = '<?php echo $diffInSeconds; ?>';
                       var invitationid = '<?php echo $invitation["id"]; ?>';
                       if(time < 0) {  time = 0; }

                       var c           = time; // in seconds
                       var t;
                       var days    = parseInt( c / 86400 ) % 365;
                       var hours   = parseInt( c / 3600 ) % 24;
                       var minutes = parseInt( c / 60 ) % 60;
                       var seconds = c % 60;
                       var d = ""; 
                       var h = ""; 
                       var m = "";
                       var s = ""; 

                       if(days     !=  ""  &&  days     !="0"){  d  =  (days+'d')     +  " ";  }
                       if(hours    !=  ""  &&  hours    !="0"){  h  =  (hours+'h')    +  " ";  }
                       if(minutes  !=  ""  &&  minutes  !="0"){  m  =  (minutes+'m')  +  " ";  }
                       if(seconds  !=  ""  &&  seconds  !="0"){  s  =  (seconds+'s')  +  " ";  }
                       
                       if(d != "" && h !=""){
                       var result    = d + h;
                       }else if(h != "" && m !=""){
                       var result    = h + m;
                       }else if(m !=""){
                       var result    = m;
                       }else if(s !=""){
                       var result    = s;
                       }
                       //var result    = d + h + m + s;
                       if(result == "NaN:NaN:NaN:NaN"){ 
                         $('#timer'+invitationid).html('{{trans('client/projects/ongoing_projects.text_next_expire')}}');
                         $('#timer'+invitationid).css('color','red');
                       } else {
                         $('#timer'+invitationid).html(result);
                         $('#timer'+invitationid).css('color','green');
                       }
                       if(c == 0 ){
                         $('#timer'+invitationid).html('{{trans('client/projects/ongoing_projects.text_next_expire')}}');
                         $('#timer'+invitationid).css('color','red');
                       }
                       c = c - 1;
                  </script>
                  <td style="white-space: nowrap;">
                    @if(isset($invitation['project_details']['project_status']) && $invitation['project_details']['project_status'] == 2)
                       @if($invitation['expire_on'] < date('Y-m-d H:i:s') ||  $invitation['expire_on'] == null )
                       <a href="javascript:void(0);" style="cursor:no-drop;">
                          <span class="msg_btn-lnew ht-tp msg-btn-auto-height" style="padding-top: 4px; text-align: center;">
                          {{ trans('expert/invitations/packs.text_view')}}</span>
                       </a>
                       <a class="dlt-btn-table" title="Delete" onclick="javascript:return confirm_delete('{{url('/expert/projects/invitations/delete/'.base64_encode($invitation['id'])) }}')" href="javascript:void(0)">
                          <span class="msg_btn-lnew ht-tp msg-btn-auto-height" style="max-width: 30px;padding-top: 7px; text-align: center;"><i class="fa fa-trash-o" ></i>
                          </span>
                       </a>
                       @else
                       <a title="View" href="{{url('/expert/projects/invitations/show/'.base64_encode($invitation['id'])) }}">
                          <span class="msg_btn-lnew ht-tp msg-btn-auto-height" style="padding-top: 4px; text-align: center;">
                          {{ trans('expert/invitations/packs.text_view')}}</span>
                       </a>
                       <a title="Delete" onclick="javascript:return confirm_delete('{{url('/expert/projects/invitations/delete/'.base64_encode($invitation['id'])) }}')" href="javascript:void(0)">
                          <span class="msg_btn-lnew ht-tp msg-btn-auto-height" style="max-width: 30px;padding-top: 7px; text-align: center;"><i class="fa fa-trash-o"></i>
                          </span>
                       </a>
                       @endif
                    @else
                       <a href="javascript:void(0);" style="cursor:no-drop;">
                          <span class="msg_btn-lnew ht-tp msg-btn-auto-height" style="padding-top: 4px; text-align: center;">
                          {{ trans('expert/invitations/packs.text_view')}}</span>
                       </a>
                        <a title="Delete" onclick="javascript:return confirm_delete('{{url('/expert/projects/invitations/delete/'.base64_encode($invitation['id'])) }}')" href="javascript:void(0)">
                          <span class="msg_btn-lnew ht-tp msg-btn-auto-height" style="max-width: 30px;padding-top: 7px; text-align: center;"><i class="fa fa-trash-o"></i>
                          </span>
                       </a>
                    @endif   
                  </td>
               </tr>
               @endforeach
               @else
               <tr align="center">
                  <td colspan="3">
                           {{ trans('expert/invitations/packs.text_sorry_no_records_found')}}                           
                  </td>
               </tr>
               @endif
            </tbody>
         </table>
      </div>
   </div>
   <!--pagigation start here-->
   @include('front.common.pagination_view', ['paginator' => $arr_invitations])
   <!--pagigation end here-->
</div>
</div>
</div>
</div>
<script type="text/javascript">
  function confirm_delete(url) {
      alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
          if (e) {
              if(url != "" && url != undefined){
                showProcessingOverlay();
                window.location.href=url;
              }
              return true;
          } else {
              return false;
          }
      });
   }
</script>
@stop

