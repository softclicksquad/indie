@extends('expert.layout.master')                
@section('main_content')
<style type="text/css">
.msg-btn-auto-height
{   
    height: auto;
    margin-right: 5px;
}
</style>
<div class="col-sm-7 col-md-8 col-lg-9">
   <div class="search-grey-bx white-wrapper">
      <div class="head_grn">{{ trans('expert/subscription_history/packs.text_heading') }}</div>
      <div class="table-responsive">
         <table class="theme-table invoice-table-s table" style="border: 1px solid rgb(239, 239, 239); margin-bottom:0;">
            <thead>
               <tr>
                  <th>{{ trans('expert/subscription_history/packs.text_pack_name') }}</th>
                  <th>{{ trans('expert/subscription_history/packs.text_pack_price') }}</th>
                  <th>{{ trans('expert/subscription_history/packs.text_pack_validity') }}</th>
                  <th>{{ trans('expert/subscription_history/packs.text_no_of_bids') }}</th>
                  <th>{{ trans('expert/subscription_history/packs.text_payment_status') }}</th>
                  <th>{{ trans('expert/subscription_history/packs.text_start_date') }}</th>
                  <th>{{ trans('expert/subscription_history/packs.text_expiry_date') }}</th>
                  <th>{{ trans('expert/subscription_history/packs.text_action') }}</th>
               </tr>
            </thead>
            <tbody>

               @if(isset($arr_subscription['data']) && sizeof($arr_subscription['data'])>0)
               @foreach($arr_subscription['data'] as $subscription)
               <tr>
                  <td>{{isset($subscription['pack_name'])?$subscription['pack_name']:''}}</td>
                  <td>$&nbsp;{{isset($subscription['pack_price'])?number_format($subscription['pack_price'],2):'0' }}
                  </td>
                  <td>{{isset($subscription['validity'])?$subscription['validity']:''}}</td>
                  <td>{{isset($subscription['number_of_bids'])?$subscription['number_of_bids']:'0' }}
                  </td>

                  <td >
                     @if(isset($subscription['transaction_details']['payment_status']))

                        @if($subscription['transaction_details']['payment_status']==1 || $subscription['transaction_details']['payment_status']==2)
                           {{ trans('expert/subscription_history/packs.text_paid') }}
                        @else
                           {{ trans('expert/subscription_history/packs.text_fail') }}
                        @endif   
                     @else
                        {{ trans('expert/subscription_history/packs.text_fail') }}
                     @endif
                  </td>                  


                  <td>{{isset($subscription['created_at'])?date("d-M-Y", strtotime($subscription['created_at'])):'--'}}</td>

                  @if(isset($subscription['subscription_pack_id']) && $subscription['subscription_pack_id']==1)
                  <td>{{ trans('new_translations.lifetime')}}</td>
                  @else
                  <td>{{isset($subscription['expiry_at'])?date("d-M-Y", strtotime($subscription['expiry_at'])):'--'}}</td>
                  @endif

                  <td><a href="{{url('/expert/subscriptionhistory/show/'.base64_encode($subscription['id'])) }}"><button class="msg_btn-lnew ht-tp msg-btn-auto-height trans-view-btn" type="button"><i class="fa fa-eye"></i>
                     {{ trans('new_translations.view')}}</button></a>
                  </td>
               </tr>
               @endforeach
               @else
               <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;" align="center">
                    <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                       <div class="search-content-block">
                          <div class="no-record">
                           {{ trans('expert/subscription_history/packs.text_sorry_no_records_found') }}
                        </div>
                     </div>
                  </td>
               </tr>
               @endif
            </tbody>
         </table>
      </div>
   </div>
   <!--pagigation start here-->
   @include('front.common.pagination_view', ['paginator' => $arr_subscription])
   <!--pagigation end here-->
</div>
</div>
</div>
</div>
@stop

