@extends('expert.layout.master')                
@section('main_content')
<div class="col-sm-7 col-md-8 col-lg-9">
   <div class="search-grey-bx">
      <div class="head_grn">{{ trans('expert/subscription_history/packs.text_heading_details') }}</div>
      @if(isset($subscription) && sizeof($subscription)>0)
      <div class="row">
         <div class="col-sm-12 col-md-6 col-lg-6 mrgt">
            <div class="row">
               <div class="form_group">
                  <div class="col-xs-6 col-sm-12 col-md-7 col-lg-7">
                     <div class="card_holder">{{ trans('expert/subscription_history/packs.text_pack_name') }} : </div>
                  </div>
                  <div class="col-xs-6 col-sm-12 col-md-5 col-lg-5">
                     <div class="fill_input">  
                        {{isset($subscription['pack_name'])?$subscription['pack_name']:''}}
                     </div>
                  </div>
                  <div class="clr"></div>
               </div>
            </div>
            <div class="row mrgt">
               <div class="form_group">
                  <div class="col-xs-6 col-sm-12 col-md-7 col-lg-7">
                     <div class="card_holder">{{ trans('expert/subscription_history/packs.text_pack_price') }} : </div>
                  </div>
                  <div class="col-xs-6 col-sm-12 col-md-5 col-lg-5">
                     <div class="fill_input">$&nbsp;{{isset($subscription['pack_price'])?number_format($subscription['pack_price'],2):'0' }}</div>
                  </div>
                  <div class="clr"></div>
               </div>
            </div>
            <div class="row mrgt">
               <div class="form_group">
                  <div class="col-xs-6 col-sm-12 col-md-7 col-lg-7">
                     <div class="card_holder">{{ trans('expert/subscription_history/packs.text_pack_validity') }} : </div>
                  </div>
                  <div class="col-xs-6 col-sm-12 col-md-5 col-lg-5">
                     <div class="fill_input"> 
                        {{isset($subscription['validity'])?$subscription['validity']:'' }}
                     </div>
                  </div>
                  <div class="clr"></div>
               </div>
            </div>
            <div class="row mrgt">
               <div class="form_group">
                  <div class="col-xs-6 col-sm-12 col-md-7 col-lg-7">
                     <div class="card_holder">{{ trans('expert/subscription_history/packs.text_no_of_bids') }} : </div>
                  </div>
                  <div class="col-xs-6 col-sm-12 col-md-5 col-lg-5">
                     <div class="fill_input">   
                        {{isset($subscription['number_of_bids'])?$subscription['number_of_bids']:'0' }}
                     </div>
                  </div>
                  <div class="clr"></div>
               </div>
            </div>
           
           
           
           
           
           
            <div class="row mrgt">
               <div class="form_group">
                  <div class="col-xs-6 col-sm-12 col-md-7 col-lg-7">
                     <div class="card_holder">{{trans('expert/subscription_history/packs.text_payment_status') }} : </div>
                  </div>
                  <div class="col-xs-6 col-sm-12 col-md-5 col-lg-5">
                     <div class="fill_input">       
                        @if(isset($subscription['transaction_details']['payment_status']))
                           @if($subscription['transaction_details']['payment_status']==1 || $subscription['transaction_details']['payment_status']==2)
                              {{ trans('expert/subscription_history/packs.text_paid') }}
                           @else
                              {{ trans('expert/subscription_history/packs.text_fail') }}
                           @endif   
                        @else
                           {{ trans('expert/subscription_history/packs.text_fail') }}
                        @endif
                     </div>
                  </div>
                  <div class="clr"></div>
               </div>
            </div>
           
         </div>
         <div class="col-sm-12 col-md-6 col-lg-6 mrgt">
            <div class="row">
               <div class="form_group">
                  <div class="col-xs-6 col-sm-12 col-md-7 col-lg-7">
                     <div class="card_holder">{{trans('expert/subscription_history/packs.text_top_up_bid_price') }} : </div>
                  </div>
                  <div class="col-xs-6 col-sm-12 col-md-5 col-lg-5">
                     <div class="fill_input">       
                        {{isset($subscription['topup_bid_price'])?number_format($subscription['topup_bid_price'],2):'0' }}
                     </div>
                  </div>
                  <div class="clr"></div>
               </div>
            </div>
         </div>
         <div class="col-sm-12 col-md-6 col-lg-6 mrgt">
            <div class="row">
               <div class="form_group">
                  <div class="col-xs-6 col-sm-12 col-md-7 col-lg-7">
                     <div class="card_holder">{{trans('expert/subscription_history/packs.text_no_of_cats') }} : </div>
                  </div>
                  <div class="col-xs-6 col-sm-12 col-md-5 col-lg-5">
                     <div class="fill_input">       
                        {{isset($subscription['number_of_categories'])?$subscription['number_of_categories']:'0' }}
                     </div>
                  </div>
                  <div class="clr"></div>
               </div>
            </div>
         </div>
         <div class="col-sm-12 col-md-6 col-lg-6 mrgt">
            <div class="row">
               <div class="form_group">
                  <div class="col-xs-6 col-sm-12 col-md-7 col-lg-7">
                     <div class="card_holder">{{trans('expert/subscription_history/packs.text_no_of_skills') }} : </div>
                  </div>
                  <div class="col-xs-6 col-sm-12 col-md-5 col-lg-5">
                     <div class="fill_input">       
                        {{isset($subscription['number_of_skills'])?$subscription['number_of_skills']:'0' }}
                     </div>
                  </div>
                  <div class="clr"></div>
               </div>
            </div>
         </div>
         <div class="col-sm-12 col-md-6 col-lg-6 mrgt">
            <div class="row">
               <div class="form_group">
                  <div class="col-xs-6 col-sm-12 col-md-7 col-lg-7">
                     <div class="card_holder">{{trans('expert/subscription_history/packs.text_favorites_projects') }} : </div>
                  </div>
                  <div class="col-xs-6 col-sm-12 col-md-5 col-lg-5">
                     <div class="fill_input">       
                        {{isset($subscription['number_of_favorites_projects'])?$subscription['number_of_favorites_projects']:'0' }}
                     </div>
                  </div>
                  <div class="clr"></div>
               </div>
            </div>
         </div>
         <div class="col-sm-12 col-md-6 col-lg-6 mrgt">
            <div class="row">
               <div class="form_group">
                  <div class="col-xs-6 col-sm-12 col-md-7 col-lg-7">
                     <div class="card_holder">{{trans('expert/subscription_history/packs.text_email') }} : </div>
                  </div>
                  <div class="col-xs-6 col-sm-12 col-md-5 col-lg-5">
                     <div class="fill_input">       
                        @if(isset($subscription['allow_email']) && $subscription['allow_email']=='0') 
                        {{trans('expert/subscription_history/packs.text_no') }}
                        @endif
                        @if(isset($subscription['allow_email']) && $subscription['allow_email']=='1') 
                        {{trans('expert/subscription_history/packs.text_yes') }}
                        @endif
                     </div>
                  </div>
                  <div class="clr"></div>
               </div>
            </div>
         </div>
         <div class="col-sm-12 col-md-6 col-lg-6 mrgt">
            <div class="row">
               <div class="form_group">
                  <div class="col-xs-6 col-sm-12 col-md-7 col-lg-7">
                     <div class="card_holder">{{trans('expert/subscription_history/packs.text_chat') }} : </div>
                  </div>
                  <div class="col-xs-6 col-sm-12 col-md-5 col-lg-5">
                     <div class="fill_input">       
                        @if(isset($subscription['allow_chat']) && $subscription['allow_chat']=='0') {{trans('expert/subscription_history/packs.text_no') }} @endif
                        @if(isset($subscription['allow_chat']) && $subscription['allow_chat']=='1') {{trans('expert/subscription_history/packs.text_yes') }} @endif
                     </div>
                  </div>
                  <div class="clr"></div>
               </div>
            </div>
         </div>
         <div class="col-sm-12 col-md-6 col-lg-6 mrgt">
            <div class="row">
               <div class="form_group">
                  <div class="col-xs-6 col-sm-12 col-md-7 col-lg-7">
                     <div class="card_holder">{{trans('expert/subscription_history/packs.text_start_date') }} : </div>
                  </div>
                  <div class="col-xs-6 col-sm-12 col-md-5 col-lg-5">
                     <div class="fill_input">       
                        {{isset($subscription['created_at'])?date("d-M-Y", strtotime($subscription['created_at'])):''}}
                     </div>
                  </div>
                  <div class="clr"></div>
               </div>
            </div>
         </div>
         <div class="col-sm-12 col-md-6 col-lg-6 mrgt">
            <div class="row">
               <div class="form_group">
                  <div class="col-xs-6 col-sm-12 col-md-7 col-lg-7">
                     <div class="card_holder">{{ trans('expert/subscription_history/packs.text_invoice_id') }} : </div>
                  </div>
                  <div class="col-xs-6 col-sm-12 col-md-5 col-lg-5">
                     <div class="fill_input">{{isset($subscription['invoice_id'])?$subscription['invoice_id']:''}}</div>
                  </div>
                  <div class="clr"></div>
               </div>
            </div>
           
          
         </div>
         <div class="col-sm-12 col-md-6 col-lg-6 mrgt">
             <div class="row">
               <div class="form_group">
                  <div class="col-xs-6 col-sm-12 col-md-7 col-lg-7">
                     <div class="card_holder">{{trans('expert/subscription_history/packs.text_expiry_date') }} : </div>
                  </div>
                  <div class="col-xs-6 col-sm-12 col-md-5 col-lg-5">
                     <div class="fill_input">       
                     @if(isset($subscription['subscription_pack_id']) && $subscription['subscription_pack_id']==1)
                        {{ trans('new_translations.lifetime')}}
                     @else
                        {{isset($subscription['expiry_at'])?date("d-M-Y", strtotime($subscription['expiry_at'])):''}}
                     @endif
                     </div>
                  </div>
                  <div class="clr"></div>
               </div>
            </div>
         </div>
      </div>
      @endif
   </div>
</div>
</div>
</div>
</div>
@stop

