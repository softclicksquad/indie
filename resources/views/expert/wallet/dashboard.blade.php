@extends('expert.layout.master')                
@section('main_content')
<style type="text/css">
    td{border-top: 0px !important;}
    
</style>
<?php for ($i = 0; $i < 10; $i++) {
                $arr_years[] = date('Y', strtotime("+$i year"));
            }
            
 ?>
@if(isset($mp_wallet_created) && $mp_wallet_created == 'Yes')
    <div class="col-sm-7 col-md-8 col-lg-9">
      @include('front.layout._operation_status')
      <div class="dashboard-box mangopay-dash section-1-open">
         <div class="head_grn"><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"></div>
         <div class="row">
            <!--Section 1: User Details -->
            <div class="col-sm-12 col-md-6 col-lg-6" style="padding: 15px;">
                <div class="dash-user-details">
                    <div class="title">
                       <i class="fa fa-user" aria-hidden="true"></i> {{trans('common/wallet/text.text_user_details')}}
                      <span class="pull-right" ><a href="#edit_user_details" class="mp-add-btn-text" data-keyboard="false" data-toggle="modal" data-backdrop="static"><i class="fa fa-edit"></i></a></span>
                    </div>
                    <div class="user-details-section">
                        <p><b>{{trans('common/wallet/text.text_user_id')}}     </b> <span>{{ isset($mango_user_detail->Id)? $mango_user_detail->Id:''}}</span></p>
                        <!-- <p><b>{{trans('common/wallet/text.text_full_name')}}   </b> <span>{{ isset($mango_user_detail->FirstName)? $mango_user_detail->FirstName:''}}  {{ isset($mango_user_detail->LastName)? $mango_user_detail->LastName:''}}</span></p> -->
                        <p><b>{{trans('common/wallet/text.text_email')}}       </b> <span>{{ isset($mango_user_detail->Email)? $mango_user_detail->Email:''}}</span></p>
                        @if(isset($mango_user_detail->LegalPersonType) && $mango_user_detail->LegalPersonType!='' && $mango_user_detail->LegalPersonType =='BUSINESS')
                          <p><b>{{trans('common/wallet/text.text_full_nationality')}} </b> <span>{{ isset($mango_user_detail->LegalRepresentativeNationality)? $mango_user_detail->LegalRepresentativeNationality:''}}</span></p>
                          <p><b>{{trans('common/wallet/text.text_full_residence')}}   </b> <span>{{ isset($mango_user_detail->LegalRepresentativeCountryOfResidence)? $mango_user_detail->LegalRepresentativeCountryOfResidence:''}}</span></p>
                        @elseif(isset($mango_user_detail->PersonType) && $mango_user_detail->PersonType!='' && $mango_user_detail->PersonType =='NATURAL')
                          <p><b>{{trans('common/wallet/text.text_full_nationality')}} </b> <span>{{ isset($mango_user_detail->Nationality)? $mango_user_detail->Nationality:''}}</span></p>
                          <p><b>{{trans('common/wallet/text.text_full_residence')}}   </b> <span>{{ isset($mango_user_detail->CountryOfResidence)? $mango_user_detail->CountryOfResidence:''}}</span></p>
                        @endif
                    </div>
                </div>
                <!--Section 2: Kyc Details -->
                <div class="dash-user-details">
                  <div class="title">
                    <i class="fa fa-file-text" aria-hidden="true"></i> {{trans('common/wallet/text.text_kyc_documents')}}
                    {{-- @if(isset($mangopay_kyc_details) && $mangopay_kyc_details =="" && empty($mangopay_kyc_details)) --}}
                    <span class="add-btn pull-right" ><i class="fa fa-ellipsis-h"></i></span>
                    <div class="money-drop pull-right">
                        <ul>
                          <a href="#upload_kyc_docs" class="mp-add-btn-text" data-keyboard="false" data-backdrop="static" data-toggle="modal"> <li> <i class="fa fa-plus-circle" aria-hidden="true"></i> {{trans('common/wallet/text.text_upload_kyc_documents')}} </li> </a>
                        </ul>
                    </div>
                    {{-- @endif --}}
                  </div>
                  <div class="user-details-section">
                    @if(isset($mangopay_kyc_details) && $mangopay_kyc_details !="" && !empty($mangopay_kyc_details) && !empty($mangopay_kyc_details))
                      @foreach($mangopay_kyc_details as $kyckey => $documents)
                        <span>{{ isset($documents->Id)? $documents->Id:''}}-{{ isset($documents->Type)? $documents->Type:''}} 
                            @if(isset($documents->Status) && $documents->Status == "VALIDATED") 
                            <small class="pull-right" style="color:#16864A;" >{{ isset($documents->Status)? $documents->Status:''}} </small>
                            @elseif(isset($documents->Status) && $documents->Status == "VALIDATION_ASKED")
                            <small class="pull-right" style="color:#FFC300;" >{{ isset($documents->Status)? $documents->Status:''}} </small>
                            @elseif(isset($documents->Status) && $documents->Status == "REFUSED")
                            <small class="pull-right" style="color:#CD1D35;" >{{ isset($documents->Status)? $documents->Status:''}} 
                              <br>
                              <small style="color:grey;"><i> {{ isset($documents->RefusedReasonType)? $documents->RefusedReasonType:''}} </i> </small>
                            </small>
                            @endif 
                        </span>
                        <div class="clearfix"></div>
                        <!-- Type / Status / UserId / RefusedReasonType / RefusedReasonMessage / ProcessedDate / Id / Tag / CreationDate -->
                      @endforeach
                    @else
                        
                        @if(isset($is_blocked_country) && $is_blocked_country!='' && $is_blocked_country != '1')
                           <span><i>-- {{trans('common/wallet/text.text_kyc_documents_note')}} --</i></span>  
                        @else
                            <span style="color:#CD1D35;"><i>-- Due to anti-money laundering policy, we are not allowed to accept the creation of user residing in: Afghanistan, Bahamas, Bosnia and Herzegovina, Botswana, Cambodia, Democratic People's Republic of Korea, Ethiopia, Ghana, Guyana, Iran, Irak, Laos, Uganda, Pakistan, Serbia, Sri Lanka, Syria, Trinidad and Tobago, Tunisia, Vanuatu, Yemen. We are very sorry. --</i></span>
                        @endif  

                    @endif
                  </div>
                </div>
                <!--End Kyc Details -->

                <!--section 3: Bank Details -->
              <div class="dash-user-details">
                  <div class="title">
                    <i class="fa fa-university" aria-hidden="true"></i> {{trans('common/wallet/text.text_bank_accounts')}} 
                    <span class="add-btn pull-right" ><i class="fa fa-ellipsis-h"></i></span>
                    <div class="money-drop pull-right">
                        <ul>
                            <a href="#add_bank" class="mp-add-btn-text" data-keyboard="false" data-backdrop="static" data-toggle="modal"> <li><i class="fa fa-plus-circle" aria-hidden="true"></i> {{trans('common/wallet/text.text_create_new_account')}} </li> </a>
                        </ul>
                    </div>  
                  </div>

                  <div class="user-details-section">
                      @if(isset($mangopay_bank_details) && $mangopay_bank_details !="" && !empty($mangopay_bank_details))
                        @foreach($mangopay_bank_details as $bankkey => $bank)
                          <div class="bnk">
                              <span class="pull-right" ><a href="#cashout" data-keyboard="false" class="cashout-model" cashout-bank-id="{{ isset($bank->Id)? $bank->Id:''}}" data-backdrop="static" data-toggle="modal"><i class="fa fa-money" aria-hidden="true"> {{trans('common/wallet/text.text_payout')}} </i></a></span>
                              <span>{{ isset($bank->Id)? $bank->Id:''}}</span> 
                              <div class="clearfix"></div>
                              <span> <small> {{trans('common/wallet/text.text_bank_type')}} </small> <small class="pull-right">{{ isset($bank->Type)? $bank->Type:''}}</small></span>
                              <div class="clearfix"></div>
                              <span> <small> {{trans('common/wallet/text.text_owner')}} </small>  <small class="pull-right">{{ isset($bank->OwnerName)? $bank->OwnerName:''}}</small></span>
                              @if($bank->Type == 'IBAN') 
                                <div class="clearfix"></div>
                                <span> <small> IBAN </small> <small class="pull-right">{{ isset($bank->Details->IBAN)? $bank->Details->IBAN:''}}</small></span>
                                <div class="clearfix"></div>
                                <span> <small> BIC </small> <small class="pull-right">{{ isset($bank->Details->BIC)? $bank->Details->BIC:''}}</small></span>
                              @endif
                              @if($bank->Type == 'GB') 
                                <div class="clearfix"></div>
                                <span> <small> {{trans('common/wallet/text.text_account_number')}} </small> <small class="pull-right">{{ isset($bank->Details->AccountNumber)? $bank->Details->AccountNumber:''}}</small></span>
                                <div class="clearfix"></div>
                                <span> <small> {{trans('common/wallet/text.text_sort_code')}} </small> <small class="pull-right">{{ isset($bank->Details->SortCode)? $bank->Details->SortCode:''}}</small></span>
                              @endif
                              @if($bank->Type == 'US') 
                                <div class="clearfix"></div>
                                <span> <small> {{trans('common/wallet/text.text_account_number')}} </small> <small class="pull-right">{{ isset($bank->Details->AccountNumber)? $bank->Details->AccountNumber:''}}</small></span>
                                <div class="clearfix"></div>
                                <span> <small> ABA </small> <small class="pull-right">{{ isset($bank->Details->ABA)? $bank->Details->ABA:''}}</small></span>
                                <div class="clearfix"></div>
                                <span> <small> {{trans('common/wallet/text.text_deposit_coount_type')}} </small>  <small class="pull-right">{{ isset($bank->Details->DepositAccountType)? $bank->Details->DepositAccountType:''}}</small></span>
                              @endif
                              @if($bank->Type == 'CA') 
                                <div class="clearfix"></div>
                                <span> BANK NAME  <small class="pull-right">{{ isset($bank->Details->BankName)? $bank->Details->BankName:''}}</small></span>
                                <div class="clearfix"></div>
                                <span> INSTITUTION NUMBER  <small class="pull-right">{{ isset($bank->Details->InstitutionNumber)? $bank->Details->InstitutionNumber:''}}</small></span>
                                <div class="clearfix"></div>
                                <span> {{trans('common/wallet/text.text_branch_code')}}  <small class="pull-right">{{ isset($bank->Details->BranchCode)? $bank->Details->BranchCode:''}}</small></span>
                                <div class="clearfix"></div>
                                <span> {{trans('common/wallet/text.text_account_number')}}  <small class="pull-right">{{ isset($bank->Details->AccountNumber)? $bank->Details->AccountNumber:''}}</small></span>
                              @endif
                              @if($bank->Type == 'OTHER') 
                                <div class="clearfix"></div>
                                <span> {{trans('common/wallet/text.text_account_number')}}  <small class="pull-right">{{ isset($bank->Details->AccountNumber)? $bank->Details->AccountNumber:''}}</small></span>
                                <div class="clearfix"></div>
                                <span> BIC  <small class="pull-right">{{ isset($bank->Details->BIC)? $bank->Details->BIC:''}}</small></span>
                              @endif
                          </div>
                          
                        @endforeach
                      @else
                       <span><i>-- {{trans('common/wallet/text.text_bank_account_note')}} --</span></i>
                      @endif
                  </div>
                </div>
                <!--End bank Details -->
            </div>
            <!--User Details -->
            <!--Section 2: Wallet Details -->
            <div class="col-sm-12 col-md-6 col-lg-6" style="padding: 15px;">
              @if(isset($mangopay_wallet_details) && $mangopay_wallet_details !="" && !empty($mangopay_wallet_details))
                <div class="dash-user-details">
                    <div class="title">
                        <i class="fa fa-money" aria-hidden="true"></i> {{trans('common/wallet/text.text_wallet')}} 
                        <div class="wallet-dropdwon-section">
                            <div class="wallet-dropdwon-head">
                                Wallet Currency <i class="fa fa-angle-down"></i>
                            </div>
                            <div class="wallet-dropdwon-content-main">
                                <div class="arrow-up-mn"> <img src="http://192.168.1.65/archexpert/public/front/images/arrows-tp.png" alt=""></div>
                                <div class="wallet-dropdwon-content" style="width:auto;">
                                <ul>
                                    @if(isset($mangopay_wallet_details) && count($mangopay_wallet_details)>0)
                                    @foreach($mangopay_wallet_details as $key=>$value)
                                      <li class="currency-li currency{{$key+1}}"> {{ isset($value->Balance->Currency)? $value->Balance->Currency:''}}</li>
                                    @endforeach
                                    @endif
                                </ul>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="user-details-section">
                        @if(isset($mangopay_wallet_details) && count($mangopay_wallet_details)>0)
                        @foreach($mangopay_wallet_details as $key=>$value)
                        <div class="wallet-content-section wallet-{{$key+1}}">
                        <input type="hidden" name="wallet_amout" id="wallet_amout_{{ isset($value->Balance->Currency)? $value->Balance->Currency:''}}" value="{{ isset($value->Balance->Amount)? $value->Balance->Amount/100:'0'}}">
                        <table class="table" style="margin-bottom:15px">
                          <tbody>     
                            <tr>
                              <td>
                                <span class="card-id">{{ isset($value->Id)? $value->Id:''}} </span>
                              </td>
                              <td>
                                <div class="description" style="font-size: 11px;">{{isset($value->Description)? $value->Description:''}}</div>
                              </td>
                              <td style="font-size: 17px;width: 120px;">
                                <div class="high-text-up">
                                  <span>
                                    {{ isset($value->Balance->Currency)? $value->Balance->Currency:''}}
                                  </span>
                                  <span class="mp-amount">
                                    {{ isset($value->Balance->Amount)? $value->Balance->Amount/100:'0'}}
                                  </span>
                                  
                                </div>
                                <div class="small-text-down" style="font-size: 10px;">
                                  {{trans('common/wallet/text.text_money_balance')}} 
                                </div>
                              </td>
                              <td class="text-right">
                                <span class="add-btn" ><i class="fa fa-ellipsis-h"></i></span>
                                  <div class="money-drop">
                                    <ul>
                                      <a href="#add_money" class="mp-add-btn-text"  data-keyboard="false" data-backdrop="static" data-toggle="modal"> <li><i class="fa fa-plus-circle" aria-hidden="true"></i> {{trans('common/wallet/text.text_payin')}} </li> </a>
                                      <!-- <li>Create pay-out</li>
                                      <li>Create transfer</li> -->
                                    </ul>
                                  </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                        @endforeach
                        @endif
                    </div>
                </div>
              @else
            </div>
            <div class="dashboard-box mangopay-dash" style="padding: 15px;">
                <div class="row">
                  <div class="col-sm-6 col-md-3 col-lg-3">
                    <div class="pink-bx">
                      <span class="dash-top-block">
                        <div class="dash-icon">
                          <a href="{{url('/expert/wallet/create_wallet_using_mp_owner_id')}}/{{ isset($mango_user_detail->Id)?base64_encode($mango_user_detail->Id):''}}">
                            <img src="{{url('/public/front/images/add-btn.png')}}" class="img-responsive" alt="wallet"/>
                          </a> 
                        </div>
                        <span class="clearfix"></span>
                      </span>
                      <span class="dash-bottom-block">
                        {{trans('common/wallet/text.text_create_wallet')}}
                      </span>
                    </div>
                  </div>
                </div>
            @endif  
            <!--section 4: Card Details -->
            <div class="dash-user-details">
                  <div class="title">
                    <i class="fa fa-file-text" aria-hidden="true"></i> Card registration for recurring subscription payments
                    {{-- @if(isset($mangopay_cards_details) && $mangopay_cards_details =="" && empty($mangopay_cards_details)) --}}
                    <span class="add-btn pull-right" ><i class="fa fa-ellipsis-h"></i></span>
                    <div class="money-drop pull-right">
                        <ul>
                          <a href="#add_new_card" class="mp-add-btn-text" data-keyboard="false" data-backdrop="static" data-toggle="modal"> <li> <i class="fa fa-plus-circle" aria-hidden="true"></i> Add Card </li> </a>
                        </ul>
                    </div>
                    {{-- @endif --}}
                  </div>
                  <div class="user-details-section">
                    @if(isset($mangopay_cards_details) && $mangopay_cards_details !="" && !empty($mangopay_cards_details) && !empty($mangopay_cards_details))
                      @foreach($mangopay_cards_details as $kyckey => $card)
                      <div class="bnk">
                          <p><b>Card No. </b> <span>{{ isset($card->Alias)? $card->Alias:''}}</span></p>
                          <p><b>Provider </b> <span>{{ isset($card->CardProvider)? $card->CardProvider:''}}</span></p>
                          <p><b>USer Id </b> <span>{{ isset($card->UserId)? $card->UserId:''}}</span></p>
                          <p><b>Country </b> <span>{{ isset($card->Country)? $card->Country:''}}</span></p>
                          <p><b>ID </b> <span>{{ isset($card->Id)? $card->Id:''}}</span></p>
                          <p><b>Expiration Date </b> <span>{{ isset($card->ExpirationDate)? $card->ExpirationDate:''}}</span></p>
                      </div>
                          
                      @endforeach
                    @endif
                  </div>
              </div>
              <!-- End Card Details -->
            </div>
            <!-- End Wallet Details-->

            <div class="clearfix"></div>            
            
            
            
            @if(isset($user_type) && $user_type!='' && $user_type == 'business')
            <!--Section 6: UBO DECLARATION -->
            <div class="col-sm-12 col-md-6 col-lg-6" style="padding: 15px;">
              <div class="dash-user-details">
                  <div class="title">
                    <i class="fa fa-file-text" aria-hidden="true"></i> UBO Declarations
                    <span class="pull-right" ><a href="{{url('/expert/wallet/create_ubo')}}"><i class="fa fa-plus-circle"></i></a></span>
                  </div>

                  <div class="user-details-section">
                    <div class="create-an-ubo-main">
                        <span class="badge" class="pull-right">1</span> Create an UBO declaration <i class="fa fa-hand-o-up" aria-hidden="true"></i><br><br>
                      </div>
                    <div class="clearfix"></div>

                     @if(isset($mangopay_ubo_declaration) && $mangopay_ubo_declaration !="" && !empty($mangopay_ubo_declaration) && !empty($mangopay_ubo_declaration))
                      @foreach($mangopay_ubo_declaration as $Ubokey => $ubo_value)
                        <span>{{ isset($ubo_value->Id)? $ubo_value->Id:''}}-{{date('Y-m-d',$ubo_value->CreationDate)}} {{date('h:i:s',$ubo_value->CreationDate)}} {{date('a',$ubo_value->CreationDate)}}
                            @if(isset($ubo_value->Status) && $ubo_value->Status !='') 
                            <small class="pull-right" style="color:#FFC300;" >{{ isset($ubo_value->Status)? $ubo_value->Status:''}} </small>
                            @endif   
                        </span>
                        <div class="clearfix"></div>
                      @endforeach
                    @else
                      -- No Ubo Declaration yet --
                    @endif
                  </div>
                </div>
            </div>
            <!--End UBO DECLARATION -->
             <!--Section 7: UBO DECLARATION DETAILS -->
           
            <div class="clearfix"></div>
            @endif
            <div class="clearfix"></div>            
            
            <!--End Transactions Details -->
        </div>
      </div>
    </div>
@else
<div class="col-sm-7 col-md-8 col-lg-9">
  @include('front.layout._operation_status')
  <div class="dashboard-box mangopay-dash">
     <div class="head_grn"><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"></div>
     <div class="row">
        <div class="col-sm-6 col-md-3 col-lg-3">
              <div class="pink-bx">
                  <span class="dash-top-block">
                     <div class="dash-icon">
                      <a href="{{url('/expert/wallet/create_user')}}">
                       <img src="{{url('/public/front/images/add-btn.png')}}" class="img-responsive" alt="wallet"/>
                      </a> 
                     </div>
                     <span class="clearfix"></span>
                  </span>
                <span class="dash-bottom-block">
                    {{trans('common/wallet/text.text_archexpert_wallet')}}  
                </span>
              </div>
         </div>
     </div>
  </div>
</div>
@endif

<!-- add money model -->
{{-- <div class="modal fade invite-member-modal" id="add_money" role="dialog">
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <h4>{{trans('common/wallet/text.text_payin_in_to_wallet')}}</h4>
             </div>
             <form action="{{url('/expert/wallet/add_money')}}" method="post" id="mp-add-money-form">
               {{csrf_field()}}
               <div class="invite-form">
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input char_restrict " data-rule-required="true" name="amount" id="amount" placeholder="{{trans('common/wallet/text.text_enter_amount_in')}} {{$currency_code}}">
                       </div>
                    </div>
                    <button type="submit" id="mp-add-money" class="black-btn">{{trans('common/wallet/text.text_payin')}}</button>
               </div>
             </form>
          </div>
       </div>
    </div>
</div> --}}
<!-- end add money model -->

{{-- {{dd($arr_currency_backend)}} --}}

<!-- add card model -->

<div class="modal fade invite-member-modal" id="add_card" role="dialog">
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <h4 style="margin-left: 160px;">Add new card</h4>
             </div>
             <form action="#{{-- {{url('/expert/wallet/add_bank')}} --}}" method="post" id="mp-add-bank-acc-form">
               {{csrf_field()}}
               <div class="invite-form">
                    
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input " data-rule-required="true" name="Card_holder_name" id="Card_holder_name" placeholder="Card holder name">
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input " data-rule-required="true" name="card_number" id="card_number" placeholder="Card number" maxlength="19" minlength="12">
                       </div>
                    </div>
                    <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input " data-rule-required="true" name="expire_month" id="expire_month" placeholder="Expire month">
                           </div>
                        </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input " data-rule-required="true" name="expire_year" id="expire_year" placeholder="Expire year">
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input " data-rule-required="true" name="cvv_no" id="cvv_no" placeholder="Cvv" maxlength="4">
                          <img src="{{ url ('/') }}/front/images/cvv-img.png" alt="" />
                       </div>
                    </div>
                <button type="submit" id="mp-add-bank" class="black-btn">Add Card</button>
               </div>
             </form>
          </div>
       </div>
    </div>
</div>
<!-- end add card model -->






<!-- add bank model -->
<div class="modal fade invite-member-modal" id="add_bank" role="dialog">
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <h4 style="margin-left: 30px;">{{trans('common/wallet/text.text_new_bank_account')}}</h4>
             </div>
             <form action="{{url('/expert/wallet/add_bank')}}" method="post" id="mp-add-bank-acc-form">
               {{csrf_field()}}
               <div class="invite-form">
                    <div class="user-box">
                      <select class="clint-input" id="bank_type" name="bank_type" data-rule-required="true">
                        <option value="IBAN">IBAN</option>
                        <option value="GB">GB</option>
                        <option value="US">US</option>
                        <option value="CA">CA</option>
                        <option value="OTHER">OTHER</option>
                      </select>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input " data-rule-required="true" name="FirstName" id="FirstName" placeholder="{{trans('common/wallet/text.text_first_name')}}">
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input " data-rule-required="true" name="LastName" id="LastName" placeholder="{{trans('common/wallet/text.text_last_name')}}">
                       </div>
                    </div>
                    <div class="user-box">
                           <div class="input-name">
                              <textarea type="text" class="clint-input" data-rule-required="true" name="Address" id="Address" placeholder="{{trans('common/wallet/text.text_enter_address')}}"></textarea>
                           </div>
                        </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input " data-rule-required="true" name="City" id="City" placeholder="{{trans('common/wallet/text.text_city')}}">
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input " data-rule-required="true" name="PostalCode" id="PostalCode" placeholder="{{trans('common/wallet/text.text_PostalCode')}}">
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input " data-rule-required="true" name="Region" id="Region" placeholder="{{trans('common/wallet/text.text_region')}}">
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <select class="clint-input " id="Country" name="Country" data-rule-required="true">
                            <option value="">-- {{trans('common/wallet/text.text_please_select_country')}} --</option>
                            @foreach($arr_country as $country_val)
                              <option value="{{$country_val}}">{{$country_val}}</option>
                            @endforeach
                          </select>
                       </div>
                    </div>
                    
                    <!-- for-iban-account -->
                    <div id="for-iban-account">
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input " data-rule-required="true" name="IBAN" id="IBAN" placeholder="{{trans('common/wallet/text.text_enter_iban')}}">
                           </div>
                        </div>
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input " data-rule-required="true" name="BIC" id="BIC" placeholder="{{trans('common/wallet/text.text_enter_bic')}}">
                           </div>
                        </div>
                    </div>  
                    
                    <!-- for-gb-account -->
                    <div id="for-gb-account" style="display:none;">
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input " data-rule-required="true" name="gbSortCode" id="gbSortCode" placeholder="{{trans('common/wallet/text.text_enter_sort_code')}}">
                           </div>
                        </div>
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input " data-rule-required="true" name="gbAccountNumber" id="gbAccountNumber" placeholder="{{trans('common/wallet/text.text_enter_account_number')}}">
                           </div>
                        </div>
                    </div>  

                    <!-- for-us-account -->
                    <div id="for-us-account" style="display:none;">
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input " data-rule-required="true" name="usAccountNumber" id="usAccountNumber" placeholder="{{trans('common/wallet/text.text_enter_account_number')}}">
                           </div>
                        </div>
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input " data-rule-required="true" name="usABA" id="usABA" placeholder="{{trans('common/wallet/text.text_enter_aba')}}">
                           </div>
                        </div>
                        <div class="user-box">
                          <div class="input-name">
                            <select class="clint-input" id="usDepositAccountType" name="usDepositAccountType">
                              <option value="CHECKING">CHECKING</option>
                              <option value="SAVINGS">SAVINGS</option>
                            </select>
                          </div>
                        </div>
                    </div>

                    <!-- for-ca-account -->
                    <div id="for-ca-account" style="display:none;"> 
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input " data-rule-required="true" name="caAccountNumber" id="caAccountNumber" placeholder="{{trans('common/wallet/text.text_enter_account_number')}}">
                           </div>
                        </div> 
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input " data-rule-required="true" name="caBranchCode" id="caBranchCode" placeholder="{{trans('common/wallet/text.text_enter_branch_code')}}">
                           </div>
                        </div>
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input" data-rule-required="true" name="caBankName" id="caBankName" placeholder="{{trans('common/wallet/text.text_enter_bank_name')}}">
                           </div>
                        </div>
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input " data-rule-required="true" name="caInstitutionNumber" id="caInstitutionNumber" placeholder="{{trans('common/wallet/text.text_enter_institution_number')}}">
                           </div>
                        </div>
                    </div>

                    <!-- for-other-account -->
                    <div id="for-other-account" style="display:none;"> 
                      <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input " data-rule-required="true" name="otherAccountNumber" id="otherAccountNumber" placeholder="{{trans('common/wallet/text.text_enter_account_number')}}">
                           </div>
                      </div>
                      <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input " data-rule-required="true" name="otherBIC" id="otherBIC" placeholder="{{trans('common/wallet/text.text_enter_bic')}}">
                           </div>
                      </div>
                    </div>  
                <button type="submit" id="mp-add-bank" class="black-btn">{{trans('common/wallet/text.text_add_bank')}}</button>
               </div>
             </form>
          </div>
       </div>
    </div>
</div>
<!-- end add bank model -->



<!-- Update user details model -->
<div class="modal fade invite-member-modal" id="edit_user_details" role="dialog">
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <h4>Edit {{trans('common/wallet/text.text_user_details')}}</h4>
             </div>
             <form action="{{url('/expert/wallet/update_user_details')}}" method="post" id="mp-add-bank-acc-form">
               {{csrf_field()}}
               <div class="invite-form">
                  <div class="user-box">
                    <b>Nationality</b>
                    <div class="input-name">
                      <select class="clint-input " id="nationality" name="nationality" data-rule-required="true">
                        <option value="">- Please select your nationality -</option>
                        @foreach($arr_country as $country_val)
                          <option value="{{$country_val}}" @if(isset($mango_user_detail->Nationality) && $mango_user_detail->Nationality!='' && $mango_user_detail->Nationality == $country_val) selected @endif>{{$country_val}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="user-box">
                    <b>Residence</b>
                    <div class="input-name">
                      <select class="clint-input " id="residence" name="residence" data-rule-required="true">
                        <option value="">- Please select your country -</option>
                        @foreach($arr_country as $country_val)
                          <option value="{{$country_val}}" @if(isset($mango_user_detail->CountryOfResidence) && $mango_user_detail->CountryOfResidence!='' && $mango_user_detail->CountryOfResidence == $country_val) selected @endif>{{$country_val}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                <button type="submit" id="mp-update-user" class="black-btn">Update</button>
               </div>
             </form>
          </div>
       </div>
    </div>
</div>
<!-- end update user details model -->

<!-- add kyc model -->
<div class="modal fade invite-member-modal" id="upload_kyc_docs" role="dialog" >
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <h4>{{trans('common/wallet/text.text_kyc_documents_upload')}}</h4>
             </div>
             <form action="{{url('/expert/wallet/upload_kyc_docs')}}" method="post" id="mp-upload-document-form" enctype="multipart/form-data">
               {{csrf_field()}}
               <input type="hidden" name="user_type" value="{{$user_type}}">
               <div class="invite-form">
                    {{-- <div class="user-box">
                       <div class="input-name">
                          <select class="clint-input " id="kyc_upload_documents_natural_types" name="kyc_upload_documents_natural_types" data-rule-required="true">
                            <option value="IDENTITY_PROOF">{{trans('common/wallet/text.text_identity_proof')}}</option>
                          </select>
                       </div>
                    </div>
                    {{trans('common/wallet/text.text_accepted_for_the_documents')}} --}}

                   <div class="user-box">
                      <b>Identity Proof</b>
                      <div class="id-proof-main-section">
                        <div class="input-name">
                          <input type="file" class="clint-input" name="id_proof" id="Kyc_doc" placeholder="Select document" data-rule-required="true">
                          <span class="error kyc_doc_file_err"></span>
                        </div>
                        <div class="id-proof-img-section">
                                <div id="example1" class="webwing-gallery">
                                    <div class="prod-carousel">
                                        <img src="{{url('/public')}}/front/images/identity.jpg" data-medium-img="{{url('/public')}}/front/images/identity.jpg" data-big-img="{{url('/public')}}/front/images/identity.jpg" data-title="" alt="">
                                        <img src="{{url('/public')}}/front/images/passport.jpg" data-medium-img="{{url('/public')}}/front/images/passport.jpg" data-big-img="{{url('/public')}}/front/images/passport.jpg" alt="">
                                    </div>
                                </div>                               
                          </div>
                          <div class="id-proof-img-section">
                               <div id="example2" class="webwing-gallery">
                                    <div class="prod-carousel">
                                        <img src="{{url('/public')}}/front/images/passport.jpg" data-medium-img="{{url('/public')}}/front/images/passport.jpg" data-big-img="{{url('/public')}}/front/images/passport.jpg" alt="">
                                        <img src="{{url('/public')}}/front/images/identity.jpg" data-medium-img="{{url('/public')}}/front/images/identity.jpg" data-big-img="{{url('/public')}}/front/images/identity.jpg" data-title="" alt="">                                        
                                    </div>
                                </div>                               
                          </div>
                      </div>
                      Eg. <span id="id_proof_note" style="color:#ff9d00">Passport, national ID card, driving license</span>
                    </div>
                    
                    @if(isset($user_type) && $user_type!='' && $user_type=='business')
                    <div class="user-box">
                      <b>Registration Proof</b>
                       <div class="input-name">
                          <input type="file" class="clint-input" name="reg_proof" id="Kyc_doc1" placeholder="Select document" data-rule-required="true">
                           <span class="error kyc_doc_file_err1"></span>
                       </div>
                      Eg. <span id="reg_proof_note" style="color:#ff9d00">The company registration document is a written statement from the Government in your country which confirms that the company legally exists.</span>
                    </div>
                    @endif
                    <div class="user-box">
                       <div class="input-name">
                          <textarea type="text" class="clint-input" name="Kyc_Tag" id="Kyc_Tag" placeholder="{{trans('common/wallet/text.text_enter_tag')}}"></textarea>
                       </div>
                    </div>

                    <button type="submit" id="mp-upload-document" class="black-btn">UPLOAD</button>
               </div>
             </form>
          </div>
       </div>
    </div>
</div>
<!-- end add kyc model -->


<!-- add Card model -->
<div class="modal fade invite-member-modal" id="add_new_card" role="dialog" >
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <h4 style="margin-left: 160px;">Add New Card</h4>
             </div>
             <form action="{{url('/expert/wallet/add_new_card')}}" method="post">
               {{csrf_field()}}
               <input type="hidden" name="user_type" value="{{$user_type}}">
               <div class="invite-form">
                    <div class="user-box">
                       <div class="input-name">
                          <select class="clint-input " id="card_type" name="card_type" data-rule-required="true">
                            <option value="">Select Card Type</option>
                            <option value="CB_VISA_MASTERCARD">CB</option>
                            <option value="CB_VISA_MASTERCARD">VISA</option>
                            <option value="CB_VISA_MASTERCARD">MASTERCARD</option>
                            <option value="AMEX">AMEX</option>
                            <option value="MAESTRO">MAESTRO</option>
                            <option value="BCMC">BCMC</option>
                            
                          </select>
                       </div>

                       <input type="hidden" name="user_id" value="{{$mp_user_id}}">
                    </div>

                     <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input " data-rule-required="true" name="Card_holder_name" id="Card_holder_name" placeholder="Card holder name">
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input " data-rule-required="true" name="card_number" id="card_number" placeholder="Card number" maxlength="19" minlength="12">
                       </div>
                    </div>
                    <div class="user-box">
                           <div class="input-name">
                            <select class="clint-input " id="expire_month" name="expire_month" data-rule-required="true">
                            <option value="">Select Expire Month</option>
                            <option value="01" >January</option>
                            <option value="02">February</option>
                            <option value="03">March</option>
                            <option value="04">April</option>
                            <option value="05">May</option>
                            <option value="06">June</option>
                            <option value="07">July</option>
                            <option value="08">August</option>
                            <option value="09">Septmber</option>
                            <option value="10">Octumber</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                          </select>
                              
                           </div>
                        </div>
                    <div class="user-box">
                       <div class="input-name">
                          
                          <select class="clint-input " id="expire_year" name="expire_year" data-rule-required="true">
                            <option value="">Select Expire Year</option>
                            @if(isset($arr_years) && count($arr_years)>0)
                            @foreach($arr_years as $key=>$values)
                            <option value="{{substr($arr_years[$key],2,2)}}" >{{$values}}</option>
                            @endforeach
                            @endif
                          </select>

                          {{-- <input type="text" class="clint-input " data-rule-required="true" name="expire_year" id="expire_year" placeholder="Expire year"> --}}
                       </div>
                    </div>

                        <input type="hidden" name="currency_code" value="EUR">
                     {{-- <div class="user-box">
                       <div class="input-name">
                          <select class="clint-input " id="currency_code" name="currency_code" data-rule-required="true">
                            <option value="">Select Currency</option>
                            @if(isset($arr_currency_backend) && count($arr_currency_backend)>0)
                            @foreach($arr_currency_backend as $key=>$values)
                            <option value="{{isset($values['currency_code'])?$values['currency_code']:''}}" >{{isset($values['currency_code'])?$values['currency_code']:''}}</option>
                            @endforeach
                            @endif
                          </select>
                       </div>
                    </div> --}}

                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input " data-rule-required="true" name="cvv_no" id="cvv_no" placeholder="CVV/CVC" maxlength="4">
                          <img src="{{ url ('/') }}/front/images/cvv-img.png" alt="" class="cvv-card-number-img" />
                       </div>
                    </div>
                    <button type="submit" class="black-btn">Submit</button>
               </div>
             </form>
          </div>
       </div>
    </div>
</div>
<!-- end add Card model -->





<!-- cashout model -->
<div class="modal fade invite-member-modal" id="cashout" role="dialog">
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <h4 style="margin-left: 172px;">{{trans('common/wallet/text.text_payout')}}</h4>
             </div>
             <form action="{{url('/expert/wallet/cashout_in_bank')}}" method="post" id="mp-cashout-form">
                {{-- {{csrf_field()}}
                <input type="hidden" value="0" class="clint-input char_restrict " data-rule-required="true" name="act_cashout_amount" id="act_cashout_amount" placeholder="act cashout amout">
                <input type="hidden" value="{{ isset($mangopay_wallet_details->Id)? $mangopay_wallet_details->Id:''}}" class="clint-input char_restrict " data-rule-required="true" name="walletId" id="walletId" placeholder="walletId">
                <input type="hidden" value="{{ isset($bank->Id)? $bank->Id:''}}" class="clint-input char_restrict " data-rule-required="true" name="bankId" id="bankId" placeholder="bankId">
                
                <div class="invite-form">
                    <span>{{trans('common/wallet/text.text_bank')}}              : <span class="cashout-bank-id"> {{ isset($bank->Id)? $bank->Id:''}} </span> </span>
                    <div class="clearfix"></div>
                    <span>{{trans('common/wallet/text.text_available_balance')}} : {{ isset($mangopay_wallet_details->Balance->Amount)? $mangopay_wallet_details->Balance->Amount/100:'0'}} {{ isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:''}}</span>
                    <div class="clearfix"></div>
                    <hr>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input char_restrict " data-rule-required="true" name="cashout_amount" id="cashout_amount" placeholder="{{trans('common/wallet/text.text_enter_amount_in')}} {{config('app.project_currency.$')}}">
                          <span class="error" id="cashout_amount_err"></span>
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <textarea type="text" class="clint-input" name="Tag" id="Tag" placeholder="{{trans('common/wallet/text.text_enter_tag')}}"></textarea>
                       </div>
                    </div>
                    <button type="submit" id="mp-cashout" class="black-btn">{{trans('common/wallet/text.text_make_payout')}}</button>
                </div> --}}
                {{csrf_field()}}
                <div class="invite-form inter-amt">

                  <span>{{trans('common/wallet/text.text_bank')}}: <span class="cashout-bank-id"> {{ isset($bank->Id)? $bank->Id:''}} </span> </span>
                    <div class="clearfix"></div>
                    <span>{{trans('common/wallet/text.text_available_balance')}} : <span class="mangopay-wallet-balance"> </span> <span class="amount-currency-code"></span> 
                    </span>


                <div class="user-box">
                      <div class="p-control-label">Currency <span class="star-col-block">*</span></div>
                      <div class="droup-select">
                        <select name="currency_code" id="project_currency_code_dashboard" onchange="set_currency_dashboard(this)" class="droup mrns tp-margn" data-rule-required="true">
                          @if(isset($arr_currency_backend) && sizeof($arr_currency_backend)>0)
                          <option value="">{{trans('expert/projects/post.text_select_currency')}}</option>
                          @foreach($arr_currency_backend as $key => $currency)
                          <option value="{{isset($currency['currency_code'])?$currency['currency_code']:''}}" data-code="{{isset($currency['currency'])?$currency['currency']:''}}" >
                              {{isset($currency['currency_code'])?$currency['currency_code']:''}}
                          </option>
                          @endforeach
                          @endif
                      </select>
                      <span class='error'>{{ $errors->first('project_currency') }}</span>
                      </div>
                  </div>
                    <div class="user-box">
                       <div class="input-name">
                        <span class="amt-code">$</span>
                       <input type="text" class="clint-input char_restrict " data-rule-required="true" name="cashout_amount" id="cashout_amount" >
                       </div>
                          <span class="error" id="cashout_amount_err"></span>
                    </div>
                <div class="clearfix"></div>
                <div class="user-box">
                       <div class="input-name">
                          <textarea type="text" class="clint-input" name="Tag" id="Tag" placeholder="{{trans('common/wallet/text.text_enter_tag')}}"></textarea>
                       </div>
                    </div>
                    

                <input type="hidden" name="currency_symbol" id="currency_symbol_code" >
                <input type="hidden" value="0" class="clint-input char_restrict " data-rule-required="true" name="act_cashout_amount" id="act_cashout_amount" placeholder="act cashout amout">
                <input type="hidden" value="" class="clint-input char_restrict " data-rule-required="true" name="walletId" id="walletId" placeholder="walletId">
                <input type="hidden" value="{{ isset($bank->Id)? $bank->Id:''}}" class="clint-input char_restrict " data-rule-required="true" name="bankId" id="bankId" placeholder="bankId">
                
                <div class="invite-form">
                    <button type="submit" id="mp-cashout" class="black-btn">{{trans('common/wallet/text.text_make_payout')}}</button>
                </div>
              </div>
             </form>
          </div>
       </div>
    </div>
</div>
<!-- end cashout model -->

<!-- refund model -->
<div class="modal fade invite-member-modal" id="refund" role="dialog">
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <h4>{{trans('common/wallet/text.text_payin_refund')}}</h4>
             </div>
             <form action="{{url('/expert/wallet/payin_refund')}}" method="post" id="mp-refund-form">
                {{csrf_field()}}
                <input type="hidden" value="0" class="clint-input char_restrict " data-rule-required="true" name="act_refund_amount" id="act_refund_amount" placeholder="act refund amout">
                <input type="hidden" value="{{ isset($mangopay_wallet_details->Id)? $mangopay_wallet_details->Id:''}}" class="clint-input char_restrict " data-rule-required="true" name="walletId" id="walletId" placeholder="walletId">
                <input type="hidden" value="0" class="clint-input char_restrict " data-rule-required="true" name="authorId" id="authorId" placeholder="authorId">
                <input type="hidden" value="0" class="clint-input char_restrict " data-rule-required="true" name="currency" id="currency" placeholder="currency">
                <input type="hidden" value="0" class="clint-input char_restrict " data-rule-required="true" name="payinId" id="payinId" placeholder="payinId">
                <div class="invite-form">
                    <span>{{trans('common/wallet/text.text_available_balance')}} : {{ isset($mangopay_wallet_details->Balance->Amount)? $mangopay_wallet_details->Balance->Amount/100:'0'}} {{ isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:''}}</span>
                    <div class="clearfix"></div>
                    <span>Author Id : <span id="author_id_text">0</span></span>
                    <div class="clearfix"></div>
                    <hr>
                    <span>{{trans('common/wallet/text.text_requested_refund_amount')}} : <span id="refund_amount">0</span> <span id="refund_amount_currency">{{ isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:''}}</span></span>
                    <div class="clearfix"></div>
                    <div class="user-box">
                       <div class="input-name">
                          <textarea type="text" class="clint-input" name="Tag" id="Tag" placeholder="{{trans('common/wallet/text.text_enter_tag')}}"></textarea>
                       </div>
                    </div>
                    <button type="submit" id="mp-refund" class="black-btn">{{trans('common/wallet/text.text_refund')}}</button>
                </div>
             </form>
          </div>
       </div>
    </div>
</div>
<!-- end refund model -->

<link rel="stylesheet" href="{{url('/public')}}/front/css/gallery.css" type="text/css" />
<script type="text/javascript" src="{{url('/public')}}/front/js/gallery.min.js"></script>
<script>
  function set_currency_dashboard()
  {
    var token     = "<?php echo csrf_token(); ?>";
    var currency = $("#project_currency_code_dashboard option:selected").attr('data-code');
    var currency_code = $("#project_currency_code_dashboard option:selected").val();
    
    $('.amt-code').text(currency);
    
    var url="{{url('/get_wallet_balance')}}";
    $.ajax({
        type: 'GET',
        contentType: 'application/x-www-form-urlencoded',
        data: $('#mp-cashout-form').serialize(),
        url:url,
        headers: {
        'X-CSRF-TOKEN': token
        },
        success: function(response) {
          console.log(response);
          $('.mangopay-wallet-balance').html(response.balance);
          $('.amount-currency-code').text(currency_code);
          $('#act_cashout_amount').val(response.balance);
          $('#walletId').val(response.walletId);
          $('#bankId').val(response.bankId);
          }
    });

  }

    $(document).ready(function() {
        $('#example1, #example2,#example3, #example4').webwingGallery({
            openGalleryStyle: 'transform',
            changeMediumStyle: true
        });
    });
</script>

<script type="text/javascript">
 $(document).ready(function(){
   $('.cashout-model').click(function(){
      var bank_id = $(this).attr('cashout-bank-id');
      $('#bankId').val(bank_id);
      $('.cashout-bank-id').html(bank_id);
   });

   $('#cashout_amount').keyup(function(){
      var currency = $("#project_currency_code_dashboard option:selected").attr('data-code');

      if(currency=='undefined' || currency==null || currency=='')
      {
        $('#cashout_amount_err').html('Please select currency first');
        return false;
      }
         $('#cashout_amount_err').html('');
      var wallet_amout       = $('#act_cashout_amount').val();
      var act_cashout_amount = $(this).val();
      if(act_cashout_amount == ""){
        act_cashout_amount = 0;
      }
      if(parseFloat(wallet_amout) <= parseFloat(act_cashout_amount)){
        $('#cashout_amount_err').html('{{trans('common/wallet/text.text_please_enter_cashout_amount_less_than_wallet_amount')}}');
        //$('#act_cashout_amount').val(wallet_amout);
      } else {
        //$('#act_cashout_amount').val(act_cashout_amount);
      } 
   });
   $('#cashout_amount').blur(function(){
      $('#cashout_amount_err').html('');
      var wallet_amout       = $('#wallet_amout').val();
      var act_cashout_amount = $('#cashout_amount').val();
      if(act_cashout_amount == ""){
        act_cashout_amount = 0;
      }
      if(parseFloat(wallet_amout) <= parseFloat(act_cashout_amount)){
        $('#cashout_amount_err').html('{{trans('common/wallet/text.text_please_enter_cashout_amount_less_than_wallet_amount')}}');
        //$('#act_cashout_amount').val(wallet_amout);
      } else {
        //$('#act_cashout_amount').val(act_cashout_amount);
      } 
   });
   $('#mp-cashout').click(function(){
      $('#cashout_amount_err').html('');
      var wallet_amout       = $('#act_cashout_amount').val();
      var act_cashout_amount = $('#cashout_amount').val();
      if(act_cashout_amount == ""){
        act_cashout_amount = 0;
      }
      if(parseFloat(wallet_amout) <= parseFloat(act_cashout_amount)){
        $('#cashout_amount_err').html('{{trans('common/wallet/text.text_please_enter_cashout_amount_less_than_wallet_amount')}}');
        //$('#act_cashout_amount').val(wallet_amout);
        return false;
      } else {
        //$('#act_cashout_amount').val(act_cashout_amount);
        return true;
      }
   });
 }); 
</script>
{{-- <script type="text/javascript">
 $(document).ready(function(){
  $('.cashout-model').click(function(){
      var bank_id = $(this).attr('cashout-bank-id');
      $('#bankId').val(bank_id);
      $('.cashout-bank-id').html(bank_id);
   });
   $('#cashout_amount').keyup(function(){
      $('#cashout_amount_err').html('');
      var wallet_amout       = $('#wallet_amout').val();
      var act_cashout_amount = $(this).val();
      if(act_cashout_amount == ""){
        act_cashout_amount = 0;
      }
      if(parseFloat(wallet_amout) <= parseFloat(act_cashout_amount)){
        $('#cashout_amount_err').html('{{trans('common/wallet/text.text_please_enter_cashout_amount_less_than_wallet_amount')}}');
        $('#act_cashout_amount').val(wallet_amout);
      } else {
        $('#act_cashout_amount').val(act_cashout_amount);
      } 
   });
   $('#cashout_amount').blur(function(){
      $('#cashout_amount_err').html('');
      var wallet_amout       = $('#wallet_amout').val();
      var act_cashout_amount = $('#cashout_amount').val();
      if(act_cashout_amount == ""){
        act_cashout_amount = 0;
      }
      if(parseFloat(wallet_amout) <= parseFloat(act_cashout_amount)){
        $('#cashout_amount_err').html('{{trans('common/wallet/text.text_please_enter_cashout_amount_less_than_wallet_amount')}}');
        $('#act_cashout_amount').val(wallet_amout);
      } else {
        $('#act_cashout_amount').val(act_cashout_amount);
      } 
   });
   $('#mp-cashout').click(function(){
      $('#cashout_amount_err').html('');
      var wallet_amout       = $('#wallet_amout').val();
      var act_cashout_amount = $('#cashout_amount').val();
      if(act_cashout_amount == ""){
        act_cashout_amount = 0;
      }
      if(parseFloat(wallet_amout) <= parseFloat(act_cashout_amount)){
        $('#cashout_amount_err').html('{{trans('common/wallet/text.text_please_enter_cashout_amount_less_than_wallet_amount')}}');
        $('#act_cashout_amount').val(wallet_amout);
        return false;
      } else {
        $('#act_cashout_amount').val(act_cashout_amount);
        return true;
      }
   });
   
   // for refund 
    $(document).on('click','#refund-module',function(){
      var AuthorId       = $(this).data('authorid');
      var RefundAmount   = $(this).data('refundamount');
      var RefundCurrency = $(this).data('refundcurrency');
      var payinId        = $(this).data('payinid');
      $('#act_refund_amount').val(RefundAmount);
      $('#refund_amount').html(RefundAmount);
      $('#authorId').val(AuthorId);
      $('#currency').val(RefundCurrency);
      $('#refund_amount_currency').html(RefundCurrency);
      $('#author_id_text').html(AuthorId);
      $('#payinId').val(payinId);
    });
 }); 
</script> --}}

<script>
 $(document).ready(function(){
    $("#mp-add-money-form").validate();
    $("#mp-add-bank-acc-form").validate();
    $("#mp-upload-document-form").validate();
    $("#mp-cashout-form").validate();
    $("#mp-refund-form").validate();
    $(document).on('click', '.add-btn', function (e) {
        var check_active = $(this).next('.money-drop').attr('class');
        if (check_active.indexOf("active") >= 0){
          $(this).next('.money-drop').removeClass('active');
        } else {
          $('.money-drop').removeClass('active');
          $(this).next('.money-drop').addClass('active');
        }
    }); 
    $(document).on('click','.close',function(){
      $("#mp-add-money-form")[0].reset();
      $("#mp-add-bank-acc-form")[0].reset();
      $("#mp-upload-document-form")[0].reset();
      $("#mp-cashout-form")[0].reset();
      $("#mp-refund-form")[0].reset();
      $('label.error,.error').html('');
    });
    $(document).on('change','#page_cnt',function(){
      $('#mp-transaction-link-page').submit();
    });
    $("#Kyc_doc").on("change", function () {
       showProcessingOverlay();
       $('.kyc_doc_file_err,.error').html('');
       var file_name       = this.files[0].name;
       var explode         = file_name.split('.');
       var file_extension  = explode['1'];
       var flag = 0;
       if(file_extension != 'pdf'  &&
          file_extension != 'jpeg' &&
          file_extension != 'jpg'  &&
          file_extension != 'gif'  &&
          file_extension != 'png'){
          $('.kyc_doc_file_err').css('color','red');
          $('.kyc_doc_file_err').html('{{trans('common/wallet/text.text_image_should_be')}}');
          flag = 1;
       } 
       if(this.files[0].size > 7000000) {
          $('.kyc_doc_file_err').css('color','red');
          $('.kyc_doc_file_err').html('{{trans('common/wallet/text.text_please_upload_file')}}');
          flag = 1;
       }
       if(flag == 1){
         hideProcessingOverlay(); 
         $(this).val('');
         return false;
       } else {
        hideProcessingOverlay(); 
        return true; 
       }
    });
     
    $("#bank_type").on("change", function () {
      var bank_type = $(this).val();
      if(bank_type == 'IBAN'){
        $('#for-iban-account').show();
        $('#for-gb-account').hide();
        $('#for-us-account').hide();
        $('#for-ca-account').hide();
        $('#for-other-account').hide();
        $("#mp-add-bank-acc-form")[0].reset();
        $('#bank_type option[value=IBAN]').attr('selected','selected');
      } else if(bank_type == 'GB'){
        $('#for-iban-account').hide();
        $('#for-gb-account').show();
        $('#for-us-account').hide();
        $('#for-ca-account').hide();
        $('#for-other-account').hide(); 
        $("#mp-add-bank-acc-form")[0].reset();
        $('#bank_type option[value=GB]').attr('selected','selected');
      } else if(bank_type == 'US'){
        $('#for-iban-account').hide();
        $('#for-gb-account').hide();
        $('#for-us-account').show();
        $('#for-ca-account').hide();
        $('#for-other-account').hide();
        $("#mp-add-bank-acc-form")[0].reset();
        $('#bank_type option[value=US]').attr('selected','selected');
      } else if(bank_type == 'CA'){
        $('#for-iban-account').hide();
        $('#for-gb-account').hide();
        $('#for-us-account').hide();
        $('#for-ca-account').show();
        $('#for-other-account').hide();
        $("#mp-add-bank-acc-form")[0].reset();
        $('#bank_type option[value=CA]').attr('selected','selected');
      } else if(bank_type == 'OTHER'){
        $('#for-iban-account').hide();
        $('#for-gb-account').hide();
        $('#for-us-account').hide();
        $('#for-ca-account').hide();
        $('#for-other-account').show();
        $("#mp-add-bank-acc-form")[0].reset();
        $('#bank_type option[value=OTHER]').attr('selected','selected');
      } 
    });
 });    
</script> 

<script>
    $(".wallet-dropdwon-head").on("click", function(){
        $(this).parent(".wallet-dropdwon-section").toggleClass("dropdown-open-main")
    });
    $(".currency-li").on("click", function(){
        $(this).parent().parent().parent().parent(".wallet-dropdwon-section").removeClass("dropdown-open-main")
    });    
    $(".currency1").on("click", function(){
        $(".dashboard-box").addClass("section-1-open").removeClass("section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency2").on("click", function(){
        $(".dashboard-box").addClass("section-2-open").removeClass("section-1-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency3").on("click", function(){
        $(".dashboard-box").addClass("section-3-open").removeClass("section-1-open section-2-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency4").on("click", function(){
        $(".dashboard-box").addClass("section-4-open").removeClass("section-1-open section-2-open section-3-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency5").on("click", function(){
        $(".dashboard-box").addClass("section-5-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency6").on("click", function(){
        $(".dashboard-box").addClass("section-6-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency7").on("click", function(){
        $(".dashboard-box").addClass("section-7-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency8").on("click", function(){
        $(".dashboard-box").addClass("section-8-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency9").on("click", function(){
        $(".dashboard-box").addClass("section-9-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency10").on("click", function(){
        $(".dashboard-box").addClass("section-10-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency11").on("click", function(){
        $(".dashboard-box").addClass("section-11-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-12-open section-13-open section-14-open")
    });    
    $(".currency12").on("click", function(){
        $(".dashboard-box").addClass("section-12-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-13-open section-14-open")
    });    
    $(".currency13").on("click", function(){
        $(".dashboard-box").addClass("section-13-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-14-open")
    });    
    $(".currency14").on("click", function(){
        $(".dashboard-box").addClass("section-14-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open")
    });
</script>
      
@stop