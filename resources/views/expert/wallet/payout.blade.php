@extends('client.layout.master')                
@section('main_content')

<style>
.payout-table-main {border-top: 1px solid #dddddd;padding: 5px 10px;}
.payout-table-head {width: 33.333%;float: left;}
.payout-main-section{border: 1px solid #dddddd}    
.payout-table-head-main{background: #eeeeee;padding: 8px 10px;}
.payout-table-head-main .payout-table-head{color: #2d2d2d}
.payout-table-head input{border: 1px solid #dddddd;padding: 0 15px;height: 35px}
.payout-table-head select{height: 35px;border: none;background: none;padding: 0 5px;width: 90px;}
.payout-table-head-select{position: relative;display: inline-block;}
.table-select-drop{position: absolute;right: 10px;top: 8px;font-size: 20px;line-height: 0}
.payment-btn-section .btn-create-btn{position: relative;left: auto;right: auto;top: auto;margin-top: 20px;float: right}
</style>

<div class="col-sm-7 col-md-8 col-lg-9">

<div class="row">
<div class="col-sm-7 col-md-9 col-lg-9">
    <div class="payout-main-section">
        <div class="payout-table-main payout-table-head-main">
            <div class="payout-table-head">
                Currency
            </div>
            <div class="payout-table-head">
                Balance
            </div>
            <div class="payout-table-head text-right">
                Withdrawal Breakdown
            </div>
            <div class="clearfix"></div>
        </div>
        <form id="mp-cashout-form" action="{{url('/client/wallet/cashout_in_bank')}}" method="post" >
              {{csrf_field()}}
        <div class="payout-table-main">
            <div class="payout-table-head">
                <div class="payout-table-head-select">                
                    <select name="currency_code" id="project_currency_code_dashboard" onchange="set_currency_dashboard(this)" class="droup mrns tp-margn" data-rule-required="true">
                          @if(isset($arr_currency_backend) && sizeof($arr_currency_backend)>0)
                          @foreach($arr_currency_backend as $key => $currency)
                          <option value="{{isset($currency['currency_code'])?$currency['currency_code']:''}}" data-code="{{isset($currency['currency'])?$currency['currency']:''}}" >
                              {{isset($currency['currency_code'])?$currency['currency_code']:''}}
                          </option>
                          @endforeach
                          @endif
                      </select>
                    <span class="table-select-drop"><i class="fa fa-angle-down"></i></span>
                </div>
            </div>
            <div class="payout-table-head mangopay-wallet-balance" style="padding-top: 6px;">
                <span class="amount-currency-code"> </span> 0.00
            </div>
            <div class="payout-table-head text-right">
                <input type="text" name="act_cashout_amount" id="act_cashout_amount" placeholder="0.00" />
            </div>
            <div class="clearfix"></div>
        </div>

         <input type="hidden" value="" name="walletId" id="walletId" >
         <input type="hidden" value="{{ isset($bank_id)? $bank_id:''}}" name="bankId" id="bankId">

    
    <div class="payment-btn-section">
        <button type="submit" class="btn-create-btn">
            Continue
        </button>
      
    </div>
</form>
</div>
</div>
 

   <div class="col-sm-5 col-md-4 col-lg-3">            
            <div class="currencies-section-main-block">
                <div class="currencies-section-th">
                    <div class="currencies-section-title">
                        Currency
                    </div>
                    <div class="currencies-amount-title">
                        Total
                    </div>
                    <div class="clearfix"></div>
                </div>
                @if(isset($arr_data['balance']) && count($arr_data['balance'])>0)
                @foreach($arr_data['balance'] as $key=>$value)
                <div class="currencies-section-block">
                    <div class="currencies-section-title">
                        {{isset($arr_data['currency'][$key])?$arr_data['currency'][$key]:''}}
                    </div>
                    <div class="currencies-amount-title">
                        {{isset($value)?number_format($value,2):''}}
                    </div>
                    <div class="clearfix"></div>
                </div>
                @endforeach
                @endif
            </div>
        </div>
        
        </div>
        </div>


<script type="text/javascript">
  function set_currency_dashboard()
  {
    var token     = "<?php echo csrf_token(); ?>";
    var currency  = $("#project_currency_code_dashboard option:selected").attr('data-code');
    var currency_code = $("#project_currency_code_dashboard option:selected").val();
  
    $('.amt-code').text(currency);
    
    var url="{{url('/get_wallet_balance')}}";
    $.ajax({
        type: 'GET',
        contentType: 'application/x-www-form-urlencoded',
        data: $('#mp-cashout-form').serialize(),
        url:url,
        headers: {
        'X-CSRF-TOKEN': token
        },
        success: function(response) {
          console.log(response);
          $('.mangopay-wallet-balance').html(response.balance);
          $('.amount-currency-code').html(currency_code);
          $('#act_cashout_amount').val(response.balance);
          $('#walletId').val(response.walletId);
          $('#bankId').val(response.bankId);
          }
    });

  }

</script>
@stop