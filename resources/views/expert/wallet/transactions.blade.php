@extends('client.layout.master')                
@section('main_content')

<div class="col-sm-8 col-md-8 col-lg-8 recent-transactions" style="padding: 15px;">
              <div class="dash-user-details">
                  <div class="title">
                    <i class="fa fa-credit-card-alt" aria-hidden="true"></i> {{trans('common/wallet/text.text_recent_transactions')}}
                    <div class="col-sm-2 col-md-2 col-lg-2 pull-right">
                      <form id="mp-transaction-link-page" action="{{url('/expert/wallet/transactions')}}" method="get">
                        <!-- {{csrf_field()}} -->
                        <select  name="page_cnt" class="select-box" id="page_cnt" style="font-size: smaller;">
                            @if(isset($_REQUEST['page_cnt']) && $_REQUEST['page_cnt'] != "")
                              @php $active_page = $_REQUEST['page_cnt']; @endphp
                            @else
                              @php $active_page = $mangopay_transaction_pagi_links_count; @endphp
                            @endif
                            @for($i=1;$i <= $active_page;$i++)
                            @php $nxt = ($i*config('app.project.pagi_cnt')); @endphp
                            <option value="{{$i}}" @if($i == $active_page) selected="selected" @endif>{{$nxt-config('app.project.pagi_cnt')}} - {{$nxt}}</option>
                            @endfor
                        </select>
                      </form>
                    </div>
                  </div>
                  
                  <div class="user-details-section">
                    @if(isset($mangopay_transaction_details) && $mangopay_transaction_details !="" && !empty($mangopay_transaction_details))
                    @foreach($mangopay_transaction_details as $index=>$data)
                    <div class="table-section-main table-{{$index+1}}">
                      <table id="TaBle" class="theme-table invoice-table-s table" style="border: 1px solid rgb(239, 239, 239); margin-bottom:0;">
                         <thead class="tras-client-tbl">
                            <tr>
                               <th>{{trans('common/wallet/text.text_creation')}}</th>
                               <th>{{trans('common/wallet/text.text_id')}}</th>
                               <th>{{trans('common/wallet/text.text_type')}}</th>
                               <th>Debited Amount</th>
                               <th>Credited Amount</th>
                               <th>{{trans('common/wallet/text.text_status')}}</th>
                               <th>{{trans('common/wallet/text.text_result')}}</th>
                            </tr>
                         </thead>
                         <tbody>
                            @if(isset($mangopay_transaction_details[$index]) && count($mangopay_transaction_details[$index])>0)
                              @if($mangopay_transaction_details[$index]!=false)
                              @foreach(array_reverse($mangopay_transaction_details[$index]) as $transaction)
                                <tr>
                                   <td>
                                       {{date('Y-m-d',$transaction->CreationDate)}} <br> 
                                       {{date('h:i:s',$transaction->CreationDate)}} <br>
                                       {{date('a',$transaction->CreationDate)}}
                                   </td>
                                   <td>{{$transaction->Id or '-'}}</td>
                                   <td width="15%">
                                    {{$transaction->Type or '-'}}
                                    @if(isset($transaction->Type) && $transaction->Type == 'TRANSFER' && isset($transaction->CreditedWalletId) && $transaction->CreditedWalletId == $mangopay_wallet_details[$index]->Id)
                                    <br><i class="fa fa-sign-in" style="color:green;" aria-hidden="true"> Credited</i> 
                                    @elseif(isset($transaction->Type) && $transaction->Type == 'TRANSFER' && isset($transaction->DebitedWalletId) && $transaction->DebitedWalletId == $mangopay_wallet_details[$index]->Id)
                                    <br><i class="fa fa-sign-out"style="color:red;" aria-hidden="true"> Debited</i> 
                                    @endif
                                   </td>
                                   <td><span @if($transaction->Status == 'SUCCEEDED') style="color:green" @else style="color:red" @endif>{{ isset($transaction->DebitedFunds->Amount)? $transaction->DebitedFunds->Amount/100:'0'}}</span> <b>{{$transaction->DebitedFunds->Currency or 'USD'}}</b> <br>incl. {{ isset($transaction->Fees->Amount)? $transaction->Fees->Amount/100:'0'}} fees </td>
                                   <td><span @if($transaction->Status == 'SUCCEEDED') style="color:green" @else style="color:red" @endif>{{ isset($transaction->CreditedFunds->Amount)? $transaction->CreditedFunds->Amount/100:'0'}}</span> <b>{{$transaction->DebitedFunds->Currency or 'USD'}}</b></td>
                                   <td @if($transaction->Status == 'SUCCEEDED') style="color:green" @else style="color:red" @endif>{{$transaction->Status or '-'}}</td>
                                   <td width="30%">{{$transaction->ResultCode or '-'}} :{{$transaction->ResultMessage or '-'}}  <br><i><b>Tag: {{$transaction->Tag or '-'}}<b></i></td>
                                </tr>
                              @endforeach
                              @endif
                            @else
                            <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;" align="center">
                              <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                                 <div class="search-content-block">
                                    <div class="no-record">
                                        {{ trans('expert/transactions/packs.text_sorry_no_records_found')}}                           
                                      </div>
                                    </div>
                                 </td>
                             </tr>
                            @endif
                         </tbody>
                      </table>
                      </div>
                      @endforeach
                    @else
                      -- {{trans('common/wallet/text.text_transactions_note')}} --
                    @endif  
                  </div>
              </div>
            </div>
     
<script type="text/javascript">

$(document).on('change','#page_cnt',function(){
   $('#mp-transaction-link-page').submit();
 });

</script> 

@stop