@extends('expert.layout.master')                
@section('main_content')

<div class="col-sm-7 col-md-8 col-lg-9">
   <div class="right_side_section min-height">
      <div class="head_grn">{{ trans('expert/settings/payment.text_heading') }}</div>
      @include('front.layout._operation_status')
      <form action="{{$module_url_path.'/update'}}" method="POST" id="form-payment-settings" name="form-payment-settings">
         {{ csrf_field() }}
         <div class="row">

            <div class="col-lg-3">&nbsp;</div>
            <div class="col-lg-6">

            <div style="margin-top:5px;" id="myself_note" class="alert alert-info">
                     <strong>{{ trans('new_translations.info')}}!</strong> {{ trans('new_translations.fill_your_payment')}}.
            </div>
               <div class="change-pwd-form">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('expert/settings/payment.text_paypal_client_id') }}:</div>
                     <div class="input-name">
                     
                     <textarea placeholder="{{ trans('expert/settings/payment.entry_paypal_client_id') }}" class="clint-input" name="paypal_client_id" id="paypal_client_id" data-rule-required='true'>{{ isset($payment_details['paypal_client_id'])?$payment_details['paypal_client_id']:''}}</textarea>

                     </div>
                     <span class='error'>{{ $errors->first('paypal_client_id') }}</span>
                  </div>

                  <div class="user-box">
                     <div class="p-control-label">{{ trans('expert/settings/payment.text_paypal_secret_key') }}:</div>
                     <div class="input-name">
                     <textarea type="text" placeholder="{{ trans('expert/settings/payment.entry_paypal_secret_key') }}" class="clint-input" name="paypal_secret_key" id="paypal_secret_key" data-rule-required='true'>{{isset($payment_details['paypal_secret_key'])?$payment_details['paypal_secret_key']:''}}</textarea>

                     <!--<input type="text" placeholder="{{ trans('expert/settings/payment.entry_paypal_secret_key') }}" class="clint-input" name="paypal_secret_key" id="paypal_secret_key" data-rule-required='true' value="{{isset($payment_details['paypal_secret_key'])?$payment_details['paypal_secret_key']:''}}"> -->

                     </div>
                     <span class='error'>{{ $errors->first('paypal_secret_key') }}</span>
                  </div>

                 <!-- <div class="user-box">
                     <div class="p-control-label">{{ trans('expert/settings/payment.text_stripe_secret_key') }}:</div>
                     <div class="input-name"><input type="text" placeholder="{{ trans('expert/settings/payment.entry_stripe_secret_key') }}" class="clint-input" name="stripe_secret_key" id="stripe_secret_key" data-rule-required='true' value="{{isset($payment_details['stripe_secret_key'])?$payment_details['stripe_secret_key']:''}}"></div>
                     <span class='error'>{{ $errors->first('stripe_secret_key') }}</span>
                  </div>

                  <div class="user-box">
                     <div class="p-control-label">{{ trans('expert/settings/payment.text_stripe_publishable_key') }}:</div>
                     <div class="input-name"><input type="text" placeholder="{{ trans('expert/settings/payment.entry_stripe_publishable_key') }}" class="clint-input" name="stripe_publishable_key" id="stripe_publishable_key" data-rule-required='true' value="{{isset($payment_details['stripe_publishable_key'])?$payment_details['stripe_publishable_key']:''}}"></div>
                     <span class='error'>{{ $errors->first('stripe_publishable_key') }}</span>
                  </div>
                  -->

                  <button class="normal-btn pull-right" type="submit">{{ trans('expert/settings/payment.text_update') }}</button>
               </div>
            </div>
            <div class="col-lg-3">&nbsp;</div>
         </div>
      </form>
   </div>
</div>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript">
  $("#form-payment-settings").validate({
    errorElement: 'span',
  });
</script>
@stop