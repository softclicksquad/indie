@extends('expert.layout.master')                
@section('main_content')

<div class="col-sm-7 col-md-8 col-lg-9">
   <div class="right_side_section min-height">
      <div class="head_grn">{{ trans('expert/settings/payment.text_heading') }}</div>
      @include('front.layout._operation_status')
      <form action="{{$module_url_path.'/update'}}" method="POST" id="form-payment-settings" name="form-payment-settings">
         {{ csrf_field() }}
         <div style="margin-top:5px;" id="myself_note" class="alert alert-info">
              <strong>{{ trans('new_translations.info')}}!</strong>
              {{ trans('expert/settings/payment.fill_your_payment_details') }}
         </div>
        <div class="change-pwd-form paypal-setting">
         <div class="row">
             <div class="col-sm-8 col-md-9 col-lg-10">
                 <div class="user-box">
                     <div class="p-control-label">{{ trans('expert/settings/payment.text_paypal_email_id') }}:</div>
                     <div class="input-name">

                     <input type="text" placeholder="{{ trans('expert/settings/payment.entry_paypal_emial_id') }}" class="clint-input" name="paypal_email" id="paypal_email" data-rule-required='true' data-rule-email='true' value="{{isset($payment_details['paypal_email'])?$payment_details['paypal_email']:''}}">
                     </div>
                     <span class='error'>{{ $errors->first('paypal_email') }}</span>
                </div>
             </div>
             <div class="col-sm-4 col-md-3 col-lg-2">
                 <button class="normal-btn pull-right" type="submit">{{ trans('expert/settings/payment.text_update') }}</button>
             </div>
         </div>
        </div>
    </form>
   </div>
</div>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript">
  $("#form-payment-settings").validate({
    errorElement: 'span',
  });
</script>
@stop