<div class="right_side_section category-edit-bx" id="all_category_replace_div">
        <div class="row">
         @if(isset($arr_experts_categories) && (sizeof($arr_experts_categories) < $number_of_categories) && $check_flag== '0' ) 
               {{-- start of category --}}
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">Add {{ trans('expert/profile/profile.text_categories') }}<span>*</span>
                        @if(isset($number_of_categories))
                           <span class="label label-important" style="background-color: red; margin-top: -15px; color: antiquewhite; font-size: 9px;">Note!</span> <span class="subcategory-note">{{ trans('expert/profile/profile.text_categories_note',array('cat'=>$number_of_categories-$count_of_categories)) }}&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        @else
                           <span class="label label-important" style="background-color: red; margin-top: -15px; color: antiquewhite; font-size: 9px;">Note!</span> <span class="subcategory-note">{{ trans('expert/profile/profile.text_categories_note',array('cat'=>0)) }}&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        @endif
                     </div>
                   
            

                    <div class="input-name err_categories" id="step1Content">
                     <div class="droup-select">
                       <select class="droup" data-rule-required="false" name="category" onchange="return getSubCategory(this);" id="category"  >
                           <option value="">Select Category</option>
                           @if(isset($arr_categories) && sizeof($arr_categories)>0)
                              @foreach($arr_categories as $key => $category)
                              <option value="{{isset($category['id'])?$category['id']:""}}" >
                                 {{isset($category['category_title'])?$category['category_title']:"" }}
                              </option>
                              @endforeach
                           @endif
                     </select>
                  </div>
                   </div>
                   <br>
                  </div>
               </div>
               {{-- End of category --}}
               {{-- start of subcategory --}}                
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label"><span>*</span>
                     
                     <span class="label label-important" style="background-color: red; margin-top: -15px; color: antiquewhite; font-size: 9px;">Note!</span> <span class="subcategory-note">You can add upto 3 subcategories under each category</span>

                     </div>                      
                     
                        <div class="input-name err_categories">
                           <div class="droup-select multiselect-drop">
                              <div id="subcategory_div">
                                 <select class="droup" name="subcategory[]" id="subcategory"  value=""  data-rule-required="false" >
                                 </select>
                              </div>
                           </div>
                        </div>
                        <br />
                  </div>
               </div>
            
               {{-- End of subcategory --}}                
               @endif

             <div id="edit_subcategory_replace">
               
              </div>
               
               <div id="replace_category">
               @if(isset($arr_experts_categories) && (sizeof($arr_experts_categories) > 0) && isset($arr_all_categories)) 
               {{-- start of EDIT category --}}
               @foreach($arr_experts_categories as $upper_key=>$values)
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">Edit {{ trans('expert/profile/profile.text_categories') }}<span>*</span>
                     </div>
                   <div class="input-name err_categories">
                        <div class="droup-select multiselect-drop">
                          <select class="droup" data-rule-required="false"  name="edit_category{{$upper_key}}" data-expert-id="{{$values['id']}}" upper-key="{{$upper_key}}" onchange="return getSubCategory2(this);" id="edit_category{{$upper_key}}">
                              <option value="">Select Category</option>
                              @if(isset($arr_all_categories) && sizeof($arr_all_categories)>0)
                                 @foreach($arr_all_categories as $key => $category)

                                 <option value="{{isset($category['id'])?$category['id']:""}}" 
                                    @if($values['category_id'] == $category['id']) selected  @endif >
                                    {{isset($category['category_title'])?$category['category_title']:"" }}
                                 </option>
                                 @endforeach
                              @endif
                        </select>
                        {{-- <a class="get-sub-cate" data-category-id="{{$edit_arr_experts_categories['category_id']}}" onclick="return getSubCategory3(this);">Get subcategories</a> --}}
                         
                        </div>
                     </div>
                       {{-- <a class="cancel-sub-cate" href="{{url('/public')}}/expert/profile">Cancel</a> --}}  
                  
                  <br />
                  </div>
               </div>
               {{-- End of EDIT category --}}
               {{-- start of EDIT subcategory --}}
               
                  <div class="col-sm-12  col-md-6 col-lg-6">
                     <div class="user-box">
                        <div class="p-control-label">Edit Subcategories<span>*</span>
                        </div>

                           <?php
                                // dd($values['experts_categories_subcategories_details']);
                                 $arr_edit_sub_categories_id = [];
                                 if(isset($values['experts_categories_subcategories_details']) && count($values['experts_categories_subcategories_details'])>0){
                                    foreach ($values['experts_categories_subcategories_details'] as $key => $value) 
                                    {
                                       if(isset($value['subcategory_id'])){
                                          $arr_edit_sub_categories_id[] =$value['subcategory_id'];
                                       }
                                    }
                                 }

                           ?>

                           <div class="input-name err_categories">
                              <div class="droup-select multiselect-drop">
                                 <div id="edit_subcategory_div">
                                    <select  class="droup" name="edit_subcategory[]" id="edit_subcategory{{$upper_key}}"  value="" data-key="{{$upper_key}}"  data-rule-required="false"  multiple="multiple" onchange="saveCategory2(this);">
                                       @if(isset($values['experts_categories_subcategories_details']) && count($values['experts_categories_subcategories_details'])>=$number_of_subcategories)
                                       @foreach ($values['experts_categories_subcategories_details'] as $sub_key => $sub_val)
                                          <option value="{{isset($sub_val['subcategory_id'])?$sub_val['subcategory_id']:''}}" selected="selected">
                                             {{isset($sub_val['subcategory_traslation'][0]['subcategory_title'])?
                                                     $sub_val['subcategory_traslation'][0]['subcategory_title']:''}}
                                          </option>
                                       @endforeach 
                                          @else
                                       @foreach($values['is_sub_category_exist'] as $row => $col)
                                          <option 
                                                value="{{isset($col['id'])?$col['id']:''}}"
                                                @if(isset($col['id']) && in_array($col['id'],$arr_edit_sub_categories_id))
                                                   selected="selected"
                                                @endif
                                             >
                                             {{isset($col['subcategory_title'])?$col['subcategory_title']:''}}
                                          </option>
                                       @endforeach
                                       @endif
                                    </select>
                                  
                                 </div>
                                  
                              </div> 
                                </div>
                                 <a id="btn_delete" onclick="confirmDelete(this)" data-categorey-id="{{$values['category_id']}}" style="cursor: pointer;float: right;">Delete</a>

                         
                           <br />
                     </div>
                  </div>
               @endforeach
               {{-- End of EDIT subcategory --}}
               @endif
               
               <div>
               <input type="hidden" name="room_no" id="room_no" value="2">
               <input type="hidden" name="certification_room_no" id="certification_room_no" value="2">               
         </div>
      </div>
      </div>
   </div>
<script type="text/javascript">
   $( document ).ready(function() {
   
   var i ='';
   for(i=0;i<=10;i++)
   {
      jQuery("#subcategory"+i).select2();
      jQuery("#edit_subcategory"+i).select2(); 
   }
   
 });
</script>