@extends('expert.layout.master')                
@section('main_content')
<div class="col-sm-7 col-md-8 col-lg-9">
  
   <form action="{{ url($module_url_path) }}/update" method="POST" id="form-client_profile" name="form-client_profile" enctype="multipart/form-data" files ="true" autocomplete="off">
      {{ csrf_field() }}
      <div class="right_side_section"> 
         @include('front.layout._operation_status')
         <div class="head_grn">{{ trans('expert/profile/profile.text_heading') }}</div>
         <div class="row">
            <div class="change-pwd-form edit-profile-section">
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('expert/profile/profile.text_first_name') }}<span>*</span></div>
                     <div class="input-name">
                        <input type="text" class="clint-input" placeholder="{{ trans('expert/profile/profile.entry_first_name') }}" name="first_name" id="first_name" value="{{isset($arr_expert_details['first_name'])?$arr_expert_details['first_name']:''}}" data-rule-required="true" data-rule-maxlength="250" onkeyup="return chk_validation(this);" />
                        <span class='error'>{{ $errors->first('first_name') }}</span>
                     </div>
                  </div>
               </div>
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('expert/profile/profile.text_last_name') }}<span>*</span></div>
                     <div class="input-name"><input type="text" class="clint-input" placeholder="{{ trans('expert/profile/profile.entry_last_name') }}" name="last_name" id="last_name" value="{{isset($arr_expert_details['last_name'])?$arr_expert_details['last_name']:''}}" data-rule-required="true" data-rule-maxlength="250" onkeyup="return chk_validation(this);" />
                        <span class='error'>{{ $errors->first('last_name') }}</span>
                     </div>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('expert/profile/profile.text_email') }}<span>*</span></div>
                     <div class="input-name"><input type="email" class="clint-input" placeholder="{{ trans('expert/profile/profile.entry_email') }}" name="email"  id="email" value="{{isset($arr_expert_details['user_details']['email'])?$arr_expert_details['user_details']['email']:''}}" data-rule-required="true" /></div>
                  </div>
               </div>
               <input type="hidden" name="old_email" value="{{isset($arr_expert_details['user_details']['email'])?$arr_expert_details['user_details']['email']:''}}">
                <?php 
                  $phone_code = '';
                  if( isset($arr_expert_details['phone_code']) && $arr_expert_details['phone_code'] != "" )
                  {
                     $phone_code = $arr_expert_details['phone_code'];                     
                  }
                  else if (old('phone_code') != "")
                  {
                     $phone_code = old('phone_code');                     
                  }

                  $phone_number = '';
                  if( isset($arr_expert_details['phone_number']) && $arr_expert_details['phone_number'] != "" )
                  {
                     $phone_number = $arr_expert_details['phone_number'];                     
                  }
                  else if (old('phone_number') != "")
                  {
                     $phone_number = old('phone_number');                     
                  }
               ?>
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('expert/profile/profile.text_phone_number') }}<span>*</span></div>
                     <div class="input-name">
                        <div class="row">
                            <!-- <div class="col-xs-3 col-sm-2 col-md-3 col-lg-2"><input type="text" class="clint-input" value="+" readonly></div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 p-l-r">
                              <input type="text" class="clint-input" placeholder="{{ trans('expert/profile/profile.entry_phone_code') }}" name="phone_code" id="phone_code" value="{{ $phone_code}}" data-rule-required="true" data-rule-maxlength="6" data-rule-minlength="1" data-rule-number="true" onkeyup="return chk_validation(this);">
                            </div> -->
                            <span class='error'>{{ $errors->first('phone_code') }}</span>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pad-right-0">
                                <div class="input-name">
                                    <div class="droup-select code-select">
                                          <select class="selectpicker droup" data-live-search="true" name="phone_code" id="phone_code" data-rule-required="true">
                                             <option data-countryCode="DZ" value="213"  @if(isset($phone_code) && $phone_code == '213') selected="selected" @endif>Algeria (+213)</option>
                                             <option data-countryCode="AD" value="376"  @if(isset($phone_code) && $phone_code == '376') selected="selected" @endif>Andorra (+376)</option>
                                             <option data-countryCode="AO" value="244"  @if(isset($phone_code) && $phone_code == '244') selected="selected" @endif>Angola (+244)</option>
                                             <option data-countryCode="AI" value="1264"  @if(isset($phone_code) && $phone_code == '1264') selected="selected" @endif>Anguilla (+1264)</option>
                                             <option data-countryCode="AG" value="1268"  @if(isset($phone_code) && $phone_code == '1268') selected="selected" @endif>Antigua &amp; Barbuda (+1268)</option>
                                             <option data-countryCode="AR" value="54"  @if(isset($phone_code) && $phone_code == '54') selected="selected" @endif>Argentina (+54)</option>
                                             <option data-countryCode="AM" value="374"  @if(isset($phone_code) && $phone_code == '374') selected="selected" @endif>Armenia (+374)</option>
                                             <option data-countryCode="AW" value="297"  @if(isset($phone_code) && $phone_code == '297') selected="selected" @endif>Aruba (+297)</option>
                                             <option data-countryCode="AU" value="61"  @if(isset($phone_code) && $phone_code == '61') selected="selected" @endif>Australia (+61)</option>
                                             <option data-countryCode="AT" value="43"  @if(isset($phone_code) && $phone_code == '43') selected="selected" @endif>Austria (+43)</option>
                                             <option data-countryCode="AZ" value="994"  @if(isset($phone_code) && $phone_code == '994') selected="selected" @endif>Azerbaijan (+994)</option>
                                             <option data-countryCode="BS" value="1242"  @if(isset($phone_code) && $phone_code == '1242') selected="selected" @endif>Bahamas (+1242)</option>
                                             <option data-countryCode="BH" value="973"  @if(isset($phone_code) && $phone_code == '973') selected="selected" @endif>Bahrain (+973)</option>
                                             <option data-countryCode="BD" value="880"  @if(isset($phone_code) && $phone_code == '880') selected="selected" @endif>Bangladesh (+880)</option>
                                             <option data-countryCode="BB" value="1246"  @if(isset($phone_code) && $phone_code == '1246') selected="selected" @endif>Barbados (+1246)</option>
                                             <option data-countryCode="BY" value="375"  @if(isset($phone_code) && $phone_code == '375') selected="selected" @endif>Belarus (+375)</option>
                                             <option data-countryCode="BE" value="32"  @if(isset($phone_code) && $phone_code == '32') selected="selected" @endif>Belgium (+32)</option>
                                             <option data-countryCode="BZ" value="501"  @if(isset($phone_code) && $phone_code == '501') selected="selected" @endif>Belize (+501)</option>
                                             <option data-countryCode="BJ" value="229"  @if(isset($phone_code) && $phone_code == '229') selected="selected" @endif>Benin (+229)</option>
                                             <option data-countryCode="BM" value="1441"  @if(isset($phone_code) && $phone_code == '1441') selected="selected" @endif>Bermuda (+1441)</option>
                                             <option data-countryCode="BT" value="975"  @if(isset($phone_code) && $phone_code == '975') selected="selected" @endif>Bhutan (+975)</option>
                                             <option data-countryCode="BO" value="591"  @if(isset($phone_code) && $phone_code == '591') selected="selected" @endif>Bolivia (+591)</option>
                                             <option data-countryCode="BA" value="387"  @if(isset($phone_code) && $phone_code == '387') selected="selected" @endif>Bosnia Herzegovina (+387)</option>
                                             <option data-countryCode="BW" value="267"  @if(isset($phone_code) && $phone_code == '267') selected="selected" @endif>Botswana (+267)</option>
                                             <option data-countryCode="BR" value="55"  @if(isset($phone_code) && $phone_code == '55') selected="selected" @endif>Brazil (+55)</option>
                                             <option data-countryCode="BN" value="673"  @if(isset($phone_code) && $phone_code == '673') selected="selected" @endif>Brunei (+673)</option>
                                             <option data-countryCode="BG" value="359"  @if(isset($phone_code) && $phone_code == '359') selected="selected" @endif>Bulgaria (+359)</option>
                                             <option data-countryCode="BF" value="226"  @if(isset($phone_code) && $phone_code == '226') selected="selected" @endif>Burkina Faso (+226)</option>
                                             <option data-countryCode="BI" value="257"  @if(isset($phone_code) && $phone_code == '257') selected="selected" @endif>Burundi (+257)</option>
                                             <option data-countryCode="KH" value="855"  @if(isset($phone_code) && $phone_code == '855') selected="selected" @endif>Cambodia (+855)</option>
                                             <option data-countryCode="CM" value="237"  @if(isset($phone_code) && $phone_code == '237') selected="selected" @endif>Cameroon (+237)</option>
                                             <option data-countryCode="CA" value="1"  @if(isset($phone_code) && $phone_code == '1') selected="selected" @endif>Canada (+1)</option>
                                             <option data-countryCode="CV" value="238"  @if(isset($phone_code) && $phone_code == '238') selected="selected" @endif>Cape Verde Islands (+238)</option>
                                             <option data-countryCode="KY" value="1345"  @if(isset($phone_code) && $phone_code == '1345') selected="selected" @endif>Cayman Islands (+1345)</option>
                                             <option data-countryCode="CF" value="236"  @if(isset($phone_code) && $phone_code == '236') selected="selected" @endif>Central African Republic (+236)</option>
                                             <option data-countryCode="CL" value="56"  @if(isset($phone_code) && $phone_code == '56') selected="selected" @endif>Chile (+56)</option>
                                             <option data-countryCode="CN" value="86"  @if(isset($phone_code) && $phone_code == '86') selected="selected" @endif>China (+86)</option>
                                             <option data-countryCode="CO" value="57"  @if(isset($phone_code) && $phone_code == '57') selected="selected" @endif>Colombia (+57)</option>
                                             <option data-countryCode="KM" value="269"  @if(isset($phone_code) && $phone_code == '269') selected="selected" @endif>Comoros (+269)</option>
                                             <option data-countryCode="CG" value="242"  @if(isset($phone_code) && $phone_code == '242') selected="selected" @endif>Congo (+242)</option>
                                             <option data-countryCode="CK" value="682"  @if(isset($phone_code) && $phone_code == '682') selected="selected" @endif>Cook Islands (+682)</option>
                                             <option data-countryCode="CR" value="506"  @if(isset($phone_code) && $phone_code == '506') selected="selected" @endif>Costa Rica (+506)</option>
                                             <option data-countryCode="HR" value="385"  @if(isset($phone_code) && $phone_code == '385') selected="selected" @endif>Croatia (+385)</option>
                                             <option data-countryCode="CU" value="53"  @if(isset($phone_code) && $phone_code == '53') selected="selected" @endif>Cuba (+53)</option>
                                             <option data-countryCode="CY" value="90392"  @if(isset($phone_code) && $phone_code == '90392') selected="selected" @endif>Cyprus North (+90392)</option>
                                             <option data-countryCode="CY" value="357"  @if(isset($phone_code) && $phone_code == '357') selected="selected" @endif>Cyprus South (+357)</option>
                                             <option data-countryCode="CZ" value="42"  @if(isset($phone_code) && $phone_code == '42') selected="selected" @endif>Czech Republic (+42)</option>
                                             <option data-countryCode="DK" value="45"  @if(isset($phone_code) && $phone_code == '45') selected="selected" @endif>Denmark (+45)</option>
                                             <option data-countryCode="DJ" value="253"  @if(isset($phone_code) && $phone_code == '253') selected="selected" @endif>Djibouti (+253)</option>
                                             <option data-countryCode="DM" value="1809"  @if(isset($phone_code) && $phone_code == '1809') selected="selected" @endif>Dominica (+1809)</option>
                                             <option data-countryCode="DO" value="1809"  @if(isset($phone_code) && $phone_code == '1809') selected="selected" @endif>Dominican Republic (+1809)</option>
                                             <option data-countryCode="EC" value="593"  @if(isset($phone_code) && $phone_code == '593') selected="selected" @endif>Ecuador (+593)</option>
                                             <option data-countryCode="EG" value="20"  @if(isset($phone_code) && $phone_code == '20') selected="selected" @endif>Egypt (+20)</option>
                                             <option data-countryCode="SV" value="503"  @if(isset($phone_code) && $phone_code == '503') selected="selected" @endif>El Salvador (+503)</option>
                                             <option data-countryCode="GQ" value="240"  @if(isset($phone_code) && $phone_code == '240') selected="selected" @endif>Equatorial Guinea (+240)</option>
                                             <option data-countryCode="ER" value="291"  @if(isset($phone_code) && $phone_code == '291') selected="selected" @endif>Eritrea (+291)</option>
                                             <option data-countryCode="EE" value="372"  @if(isset($phone_code) && $phone_code == '372') selected="selected" @endif>Estonia (+372)</option>
                                             <option data-countryCode="ET" value="251"  @if(isset($phone_code) && $phone_code == '251') selected="selected" @endif>Ethiopia (+251)</option>
                                             <option data-countryCode="FK" value="500"  @if(isset($phone_code) && $phone_code == '500') selected="selected" @endif>Falkland Islands (+500)</option>
                                             <option data-countryCode="FO" value="298"  @if(isset($phone_code) && $phone_code == '298') selected="selected" @endif>Faroe Islands (+298)</option>
                                             <option data-countryCode="FJ" value="679"  @if(isset($phone_code) && $phone_code == '679') selected="selected" @endif>Fiji (+679)</option>
                                             <option data-countryCode="FI" value="358"  @if(isset($phone_code) && $phone_code == '358') selected="selected" @endif>Finland (+358)</option>
                                             <option data-countryCode="FR" value="33"  @if(isset($phone_code) && $phone_code == '33') selected="selected" @endif>France (+33)</option>
                                             <option data-countryCode="GF" value="594"  @if(isset($phone_code) && $phone_code == '594') selected="selected" @endif>French Guiana (+594)</option>
                                             <option data-countryCode="PF" value="689"  @if(isset($phone_code) && $phone_code == '689') selected="selected" @endif>French Polynesia (+689)</option>
                                             <option data-countryCode="GA" value="241"  @if(isset($phone_code) && $phone_code == '241') selected="selected" @endif>Gabon (+241)</option>
                                             <option data-countryCode="GM" value="220"  @if(isset($phone_code) && $phone_code == '220') selected="selected" @endif>Gambia (+220)</option>
                                             <option data-countryCode="GE" value="7880"  @if(isset($phone_code) && $phone_code == '7880') selected="selected" @endif>Georgia (+7880)</option>
                                             <option data-countryCode="DE" value="49"  @if(isset($phone_code) && $phone_code == '49') selected="selected" @endif>Germany (+49)</option>
                                             <option data-countryCode="GH" value="233"  @if(isset($phone_code) && $phone_code == '233') selected="selected" @endif>Ghana (+233)</option>
                                             <option data-countryCode="GI" value="350"  @if(isset($phone_code) && $phone_code == '350') selected="selected" @endif>Gibraltar (+350)</option>
                                             <option data-countryCode="GR" value="30"  @if(isset($phone_code) && $phone_code == '30') selected="selected" @endif>Greece (+30)</option>
                                             <option data-countryCode="GL" value="299"  @if(isset($phone_code) && $phone_code == '299') selected="selected" @endif>Greenland (+299)</option>
                                             <option data-countryCode="GD" value="1473"  @if(isset($phone_code) && $phone_code == '1473') selected="selected" @endif>Grenada (+1473)</option>
                                             <option data-countryCode="GP" value="590"  @if(isset($phone_code) && $phone_code == '590') selected="selected" @endif>Guadeloupe (+590)</option>
                                             <option data-countryCode="GU" value="671"  @if(isset($phone_code) && $phone_code == '671') selected="selected" @endif>Guam (+671)</option>
                                             <option data-countryCode="GT" value="502"  @if(isset($phone_code) && $phone_code == '502') selected="selected" @endif>Guatemala (+502)</option>
                                             <option data-countryCode="GN" value="224"  @if(isset($phone_code) && $phone_code == '224') selected="selected" @endif>Guinea (+224)</option>
                                             <option data-countryCode="GW" value="245"  @if(isset($phone_code) && $phone_code == '245') selected="selected" @endif>Guinea - Bissau (+245)</option>
                                             <option data-countryCode="GY" value="592"  @if(isset($phone_code) && $phone_code == '592') selected="selected" @endif>Guyana (+592)</option>
                                             <option data-countryCode="HT" value="509"  @if(isset($phone_code) && $phone_code == '509') selected="selected" @endif>Haiti (+509)</option>
                                             <option data-countryCode="HN" value="504"  @if(isset($phone_code) && $phone_code == '504') selected="selected" @endif>Honduras (+504)</option>
                                             <option data-countryCode="HK" value="852"  @if(isset($phone_code) && $phone_code == '852') selected="selected" @endif>Hong Kong (+852)</option>
                                             <option data-countryCode="HU" value="36"  @if(isset($phone_code) && $phone_code == '36') selected="selected" @endif>Hungary (+36)</option>
                                             <option data-countryCode="IS" value="354"  @if(isset($phone_code) && $phone_code == '354') selected="selected" @endif>Iceland (+354)</option>
                                             <option data-countryCode="IN" value="91"  @if(isset($phone_code) && $phone_code == '91') selected="selected" @endif >India (+91)</option>
                                             <option data-countryCode="ID" value="62"  @if(isset($phone_code) && $phone_code == '62') selected="selected" @endif>Indonesia (+62)</option>
                                             <option data-countryCode="IR" value="98"  @if(isset($phone_code) && $phone_code == '98') selected="selected" @endif>Iran (+98)</option>
                                             <option data-countryCode="IQ" value="964"  @if(isset($phone_code) && $phone_code == '964') selected="selected" @endif>Iraq (+964)</option>
                                             <option data-countryCode="IE" value="353"  @if(isset($phone_code) && $phone_code == '353') selected="selected" @endif>Ireland (+353)</option>
                                             <option data-countryCode="IL" value="972"  @if(isset($phone_code) && $phone_code == '972') selected="selected" @endif>Israel (+972)</option>
                                             <option data-countryCode="IT" value="39"  @if(isset($phone_code) && $phone_code == '39') selected="selected" @endif>Italy (+39)</option>
                                             <option data-countryCode="JM" value="1876"  @if(isset($phone_code) && $phone_code == '1876') selected="selected" @endif>Jamaica (+1876)</option>
                                             <option data-countryCode="JP" value="81"  @if(isset($phone_code) && $phone_code == '81') selected="selected" @endif>Japan (+81)</option>
                                             <option data-countryCode="JO" value="962"  @if(isset($phone_code) && $phone_code == '962') selected="selected" @endif>Jordan (+962)</option>
                                             <option data-countryCode="KZ" value="7"  @if(isset($phone_code) && $phone_code == '7') selected="selected" @endif>Kazakhstan (+7)</option>
                                             <option data-countryCode="KE" value="254"  @if(isset($phone_code) && $phone_code == '254') selected="selected" @endif>Kenya (+254)</option>
                                             <option data-countryCode="KI" value="686"  @if(isset($phone_code) && $phone_code == '686') selected="selected" @endif>Kiribati (+686)</option>
                                             <option data-countryCode="KP" value="850"  @if(isset($phone_code) && $phone_code == '850') selected="selected" @endif>Korea North (+850)</option>
                                             <option data-countryCode="KR" value="82"  @if(isset($phone_code) && $phone_code == '82') selected="selected" @endif>Korea South (+82)</option>
                                             <option data-countryCode="KW" value="965"  @if(isset($phone_code) && $phone_code == '965') selected="selected" @endif>Kuwait (+965)</option>
                                             <option data-countryCode="KG" value="996"  @if(isset($phone_code) && $phone_code == '996') selected="selected" @endif>Kyrgyzstan (+996)</option>
                                             <option data-countryCode="LA" value="856"  @if(isset($phone_code) && $phone_code == '856') selected="selected" @endif>Laos (+856)</option>
                                             <option data-countryCode="LV" value="371"  @if(isset($phone_code) && $phone_code == '371') selected="selected" @endif>Latvia (+371)</option>
                                             <option data-countryCode="LB" value="961"  @if(isset($phone_code) && $phone_code == '961') selected="selected" @endif>Lebanon (+961)</option>
                                             <option data-countryCode="LS" value="266"  @if(isset($phone_code) && $phone_code == '266') selected="selected" @endif>Lesotho (+266)</option>
                                             <option data-countryCode="LR" value="231"  @if(isset($phone_code) && $phone_code == '231') selected="selected" @endif>Liberia (+231)</option>
                                             <option data-countryCode="LY" value="218"  @if(isset($phone_code) && $phone_code == '218') selected="selected" @endif>Libya (+218)</option>
                                             <option data-countryCode="LI" value="417"  @if(isset($phone_code) && $phone_code == '417') selected="selected" @endif>Liechtenstein (+417)</option>
                                             <option data-countryCode="LT" value="370"  @if(isset($phone_code) && $phone_code == '370') selected="selected" @endif>Lithuania (+370)</option>
                                             <option data-countryCode="LU" value="352"  @if(isset($phone_code) && $phone_code == '352') selected="selected" @endif>Luxembourg (+352)</option>
                                             <option data-countryCode="MO" value="853"  @if(isset($phone_code) && $phone_code == '853') selected="selected" @endif>Macao (+853)</option>
                                             <option data-countryCode="MK" value="389"  @if(isset($phone_code) && $phone_code == '389') selected="selected" @endif>Macedonia (+389)</option>
                                             <option data-countryCode="MG" value="261"  @if(isset($phone_code) && $phone_code == '261') selected="selected" @endif>Madagascar (+261)</option>
                                             <option data-countryCode="MW" value="265"  @if(isset($phone_code) && $phone_code == '265') selected="selected" @endif>Malawi (+265)</option>
                                             <option data-countryCode="MY" value="60"  @if(isset($phone_code) && $phone_code == '60') selected="selected" @endif>Malaysia (+60)</option>
                                             <option data-countryCode="MV" value="960"  @if(isset($phone_code) && $phone_code == '960') selected="selected" @endif>Maldives (+960)</option>
                                             <option data-countryCode="ML" value="223"  @if(isset($phone_code) && $phone_code == '223') selected="selected" @endif>Mali (+223)</option>
                                             <option data-countryCode="MT" value="356"  @if(isset($phone_code) && $phone_code == '356') selected="selected" @endif>Malta (+356)</option>
                                             <option data-countryCode="MH" value="692"  @if(isset($phone_code) && $phone_code == '692') selected="selected" @endif>Marshall Islands (+692)</option>
                                             <option data-countryCode="MQ" value="596"  @if(isset($phone_code) && $phone_code == '596') selected="selected" @endif>Martinique (+596)</option>
                                             <option data-countryCode="MR" value="222"  @if(isset($phone_code) && $phone_code == '222') selected="selected" @endif>Mauritania (+222)</option>
                                             <option data-countryCode="YT" value="269"  @if(isset($phone_code) && $phone_code == '269') selected="selected" @endif>Mayotte (+269)</option>
                                             <option data-countryCode="MX" value="52"  @if(isset($phone_code) && $phone_code == '52') selected="selected" @endif>Mexico (+52)</option>
                                             <option data-countryCode="FM" value="691"  @if(isset($phone_code) && $phone_code == '691') selected="selected" @endif>Micronesia (+691)</option>
                                             <option data-countryCode="MD" value="373"  @if(isset($phone_code) && $phone_code == '373') selected="selected" @endif>Moldova (+373)</option>
                                             <option data-countryCode="MC" value="377"  @if(isset($phone_code) && $phone_code == '377') selected="selected" @endif>Monaco (+377)</option>
                                             <option data-countryCode="MN" value="976"  @if(isset($phone_code) && $phone_code == '976') selected="selected" @endif>Mongolia (+976)</option>
                                             <option data-countryCode="MS" value="1664"  @if(isset($phone_code) && $phone_code == '1664') selected="selected" @endif>Montserrat (+1664)</option>
                                             <option data-countryCode="MA" value="212"  @if(isset($phone_code) && $phone_code == '212') selected="selected" @endif>Morocco (+212)</option>
                                             <option data-countryCode="MZ" value="258"  @if(isset($phone_code) && $phone_code == '258') selected="selected" @endif>Mozambique (+258)</option>
                                             <option data-countryCode="MN" value="95"  @if(isset($phone_code) && $phone_code == '95') selected="selected" @endif>Myanmar (+95)</option>
                                             <option data-countryCode="NA" value="264"  @if(isset($phone_code) && $phone_code == '264') selected="selected" @endif>Namibia (+264)</option>
                                             <option data-countryCode="NR" value="674"  @if(isset($phone_code) && $phone_code == '674') selected="selected" @endif>Nauru (+674)</option>
                                             <option data-countryCode="NP" value="977"  @if(isset($phone_code) && $phone_code == '977') selected="selected" @endif>Nepal (+977)</option>
                                             <option data-countryCode="NL" value="31"  @if(isset($phone_code) && $phone_code == '31') selected="selected" @endif>Netherlands (+31)</option>
                                             <option data-countryCode="NC" value="687"  @if(isset($phone_code) && $phone_code == '687') selected="selected" @endif>New Caledonia (+687)</option>
                                             <option data-countryCode="NZ" value="64"  @if(isset($phone_code) && $phone_code == '64') selected="selected" @endif>New Zealand (+64)</option>
                                             <option data-countryCode="NI" value="505"  @if(isset($phone_code) && $phone_code == '505') selected="selected" @endif>Nicaragua (+505)</option>
                                             <option data-countryCode="NE" value="227"  @if(isset($phone_code) && $phone_code == '227') selected="selected" @endif>Niger (+227)</option>
                                             <option data-countryCode="NG" value="234"  @if(isset($phone_code) && $phone_code == '234') selected="selected" @endif>Nigeria (+234)</option>
                                             <option data-countryCode="NU" value="683"  @if(isset($phone_code) && $phone_code == '683') selected="selected" @endif>Niue (+683)</option>
                                             <option data-countryCode="NF" value="672"  @if(isset($phone_code) && $phone_code == '672') selected="selected" @endif>Norfolk Islands (+672)</option>
                                             <option data-countryCode="NP" value="670"  @if(isset($phone_code) && $phone_code == '670') selected="selected" @endif>Northern Marianas (+670)</option>
                                             <option data-countryCode="NO" value="47"  @if(isset($phone_code) && $phone_code == '47') selected="selected" @endif>Norway (+47)</option>
                                             <option data-countryCode="OM" value="968"  @if(isset($phone_code) && $phone_code == '968') selected="selected" @endif>Oman (+968)</option>
                                             <option data-countryCode="PW" value="680"  @if(isset($phone_code) && $phone_code == '680') selected="selected" @endif>Palau (+680)</option>
                                             <option data-countryCode="PA" value="507"  @if(isset($phone_code) && $phone_code == '507') selected="selected" @endif>Panama (+507)</option>
                                             <option data-countryCode="PG" value="675"  @if(isset($phone_code) && $phone_code == '675') selected="selected" @endif>Papua New Guinea (+675)</option>
                                             <option data-countryCode="PY" value="595"  @if(isset($phone_code) && $phone_code == '595') selected="selected" @endif>Paraguay (+595)</option>
                                             <option data-countryCode="PE" value="51"  @if(isset($phone_code) && $phone_code == '51') selected="selected" @endif>Peru (+51)</option>
                                             <option data-countryCode="PH" value="63"  @if(isset($phone_code) && $phone_code == '63') selected="selected" @endif>Philippines (+63)</option>
                                             <option data-countryCode="PL" value="48"  @if(isset($phone_code) && $phone_code == '48') selected="selected" @endif>Poland (+48)</option>
                                             <option data-countryCode="PT" value="351"  @if(isset($phone_code) && $phone_code == '351') selected="selected" @endif>Portugal (+351)</option>
                                             <option data-countryCode="PR" value="1787"  @if(isset($phone_code) && $phone_code == '1787') selected="selected" @endif>Puerto Rico (+1787)</option>
                                             <option data-countryCode="QA" value="974"  @if(isset($phone_code) && $phone_code == '974') selected="selected" @endif>Qatar (+974)</option>
                                             <option data-countryCode="RE" value="262"  @if(isset($phone_code) && $phone_code == '262') selected="selected" @endif>Reunion (+262)</option>
                                             <option data-countryCode="RO" value="40"  @if(isset($phone_code) && $phone_code == '40') selected="selected" @endif>Romania (+40)</option>
                                             <option data-countryCode="RU" value="7"  @if(isset($phone_code) && $phone_code == '7') selected="selected" @endif>Russia (+7)</option>
                                             <option data-countryCode="RW" value="250"  @if(isset($phone_code) && $phone_code == '250') selected="selected" @endif>Rwanda (+250)</option>
                                             <option data-countryCode="SM" value="378"  @if(isset($phone_code) && $phone_code == '378') selected="selected" @endif>San Marino (+378)</option>
                                             <option data-countryCode="ST" value="239"  @if(isset($phone_code) && $phone_code == '239') selected="selected" @endif>Sao Tome &amp; Principe (+239)</option>
                                             <option data-countryCode="SA" value="966"  @if(isset($phone_code) && $phone_code == '966') selected="selected" @endif>Saudi Arabia (+966)</option>
                                             <option data-countryCode="SN" value="221"  @if(isset($phone_code) && $phone_code == '221') selected="selected" @endif>Senegal (+221)</option>
                                             <option data-countryCode="CS" value="381"  @if(isset($phone_code) && $phone_code == '381') selected="selected" @endif>Serbia (+381)</option>
                                             <option data-countryCode="SC" value="248"  @if(isset($phone_code) && $phone_code == '248') selected="selected" @endif>Seychelles (+248)</option>
                                             <option data-countryCode="SL" value="232"  @if(isset($phone_code) && $phone_code == '232') selected="selected" @endif>Sierra Leone (+232)</option>
                                             <option data-countryCode="SG" value="65"  @if(isset($phone_code) && $phone_code == '65') selected="selected" @endif>Singapore (+65)</option>
                                             <option data-countryCode="SK" value="421"  @if(isset($phone_code) && $phone_code == '421') selected="selected" @endif>Slovak Republic (+421)</option>
                                             <option data-countryCode="SI" value="386"  @if(isset($phone_code) && $phone_code == '386') selected="selected" @endif>Slovenia (+386)</option>
                                             <option data-countryCode="SB" value="677"  @if(isset($phone_code) && $phone_code == '677') selected="selected" @endif>Solomon Islands (+677)</option>
                                             <option data-countryCode="SO" value="252"  @if(isset($phone_code) && $phone_code == '252') selected="selected" @endif>Somalia (+252)</option>
                                             <option data-countryCode="ZA" value="27"  @if(isset($phone_code) && $phone_code == '27') selected="selected" @endif>South Africa (+27)</option>
                                             <option data-countryCode="ES" value="34"  @if(isset($phone_code) && $phone_code == '34') selected="selected" @endif>Spain (+34)</option>
                                             <option data-countryCode="LK" value="94"  @if(isset($phone_code) && $phone_code == '94') selected="selected" @endif>Sri Lanka (+94)</option>
                                             <option data-countryCode="SH" value="290"  @if(isset($phone_code) && $phone_code == '290') selected="selected" @endif>St. Helena (+290)</option>
                                             <option data-countryCode="KN" value="1869"  @if(isset($phone_code) && $phone_code == '1869') selected="selected" @endif>St. Kitts (+1869)</option>
                                             <option data-countryCode="SC" value="1758"  @if(isset($phone_code) && $phone_code == '1758') selected="selected" @endif>St. Lucia (+1758)</option>
                                             <option data-countryCode="SD" value="249"  @if(isset($phone_code) && $phone_code == '249') selected="selected" @endif>Sudan (+249)</option>
                                             <option data-countryCode="SR" value="597"  @if(isset($phone_code) && $phone_code == '597') selected="selected" @endif>Suriname (+597)</option>
                                             <option data-countryCode="SZ" value="268"  @if(isset($phone_code) && $phone_code == '268') selected="selected" @endif>Swaziland (+268)</option>
                                             <option data-countryCode="SE" value="46"  @if(isset($phone_code) && $phone_code == '46') selected="selected" @endif>Sweden (+46)</option>
                                             <option data-countryCode="CH" value="41"  @if(isset($phone_code) && $phone_code == '41') selected="selected" @endif>Switzerland (+41)</option>
                                             <option data-countryCode="SI" value="963"  @if(isset($phone_code) && $phone_code == '963') selected="selected" @endif>Syria (+963)</option>
                                             <option data-countryCode="TW" value="886"  @if(isset($phone_code) && $phone_code == '886') selected="selected" @endif>Taiwan (+886)</option>
                                             <option data-countryCode="TJ" value="7"  @if(isset($phone_code) && $phone_code == '7') selected="selected" @endif>Tajikstan (+7)</option>
                                             <option data-countryCode="TH" value="66"  @if(isset($phone_code) && $phone_code == '66') selected="selected" @endif>Thailand (+66)</option>
                                             <option data-countryCode="TG" value="228"  @if(isset($phone_code) && $phone_code == '228') selected="selected" @endif>Togo (+228)</option>
                                             <option data-countryCode="TO" value="676"  @if(isset($phone_code) && $phone_code == '676') selected="selected" @endif>Tonga (+676)</option>
                                             <option data-countryCode="TT" value="1868"  @if(isset($phone_code) && $phone_code == '1868') selected="selected" @endif>Trinidad &amp; Tobago (+1868)</option>
                                             <option data-countryCode="TN" value="216"  @if(isset($phone_code) && $phone_code == '216') selected="selected" @endif>Tunisia (+216)</option>
                                             <option data-countryCode="TR" value="90"  @if(isset($phone_code) && $phone_code == '90') selected="selected" @endif>Turkey (+90)</option>
                                             <option data-countryCode="TM" value="7"  @if(isset($phone_code) && $phone_code == '7') selected="selected" @endif>Turkmenistan (+7)</option>
                                             <option data-countryCode="TM" value="993"  @if(isset($phone_code) && $phone_code == '993') selected="selected" @endif>Turkmenistan (+993)</option>
                                             <option data-countryCode="TC" value="1649"  @if(isset($phone_code) && $phone_code == '1649') selected="selected" @endif>Turks &amp; Caicos Islands (+1649)</option>
                                             <option data-countryCode="TV" value="688"  @if(isset($phone_code) && $phone_code == '688') selected="selected" @endif>Tuvalu (+688)</option>
                                             <option data-countryCode="UG" value="256"  @if(isset($phone_code) && $phone_code == '256') selected="selected" @endif>Uganda (+256)</option>
                                             <!-- <option data-countryCode="GB" value="44"  @if(isset($phone_code) && $phone_code == '44') selected="selected" @endif>UK (+44)</option> -->
                                             <option data-countryCode="UA" value="380"  @if(isset($phone_code) && $phone_code == '380') selected="selected" @endif>Ukraine (+380)</option>
                                             <option data-countryCode="AE" value="971"  @if(isset($phone_code) && $phone_code == '971') selected="selected" @endif>United Arab Emirates (+971)</option>
                                             <option data-countryCode="UY" value="598"  @if(isset($phone_code) && $phone_code == '598') selected="selected" @endif>Uruguay (+598)</option>
                                             <!-- <option data-countryCode="US" value="1"  @if(isset($phone_code) && $phone_code == '1') selected="selected" @endif>USA (+1)</option> -->
                                             <option data-countryCode="UZ" value="7"  @if(isset($phone_code) && $phone_code == '7') selected="selected" @endif>Uzbekistan (+7)</option>
                                             <option data-countryCode="VU" value="678"  @if(isset($phone_code) && $phone_code == '678') selected="selected" @endif>Vanuatu (+678)</option>
                                             <option data-countryCode="VA" value="379"  @if(isset($phone_code) && $phone_code == '379') selected="selected" @endif>Vatican City (+379)</option>
                                             <option data-countryCode="VE" value="58"  @if(isset($phone_code) && $phone_code == '58') selected="selected" @endif>Venezuela (+58)</option>
                                             <option data-countryCode="VN" value="84"  @if(isset($phone_code) && $phone_code == '84') selected="selected" @endif>Vietnam (+84)</option>
                                             <option data-countryCode="VG" value="84"  @if(isset($phone_code) && $phone_code == '84') selected="selected" @endif>Virgin Islands - British (+1284)</option>
                                             <option data-countryCode="VI" value="84"  @if(isset($phone_code) && $phone_code == '84') selected="selected" @endif>Virgin Islands - US (+1340)</option>
                                             <option data-countryCode="WF" value="681"  @if(isset($phone_code) && $phone_code == '681') selected="selected" @endif>Wallis &amp; Futuna (+681)</option>
                                             <option data-countryCode="YE" value="969"  @if(isset($phone_code) && $phone_code == '969') selected="selected" @endif>Yemen (North)(+969)</option>
                                             <option data-countryCode="YE" value="967"  @if(isset($phone_code) && $phone_code == '967') selected="selected" @endif>Yemen (South)(+967)</option>
                                             <option data-countryCode="ZM" value="260"  @if(isset($phone_code) && $phone_code == '260') selected="selected" @endif>Zambia (+260)</option>
                                             <option data-countryCode="ZW" value="263"  @if(isset($phone_code) && $phone_code == '263') selected="selected" @endif>Zimbabwe (+263)</option>
                                          </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"><input type="text" class="clint-input" placeholder="{{ trans('expert/profile/profile.entry_phone_number') }}" name="phone_number" id="phone_number" value="{{$phone_number}}" data-rule-required="true" data-rule-maxlength="17" data-rule-minlength="7" data-rule-number="true">
                            <span class='error'>{{ $errors->first('phone_number') }}</span>   
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="clearfix"></div>
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('expert/profile/profile.text_user_name') }}<span>*</span></div>
                        <div class="input-name"><input autocomplete="off" @if(isset($arr_expert_details['user_details']['user_name']) && $arr_expert_details['user_details']['user_name']) != "") disabled @endif type="text" onkeyup="return chk_user_name_availablity(this.value);" class="clint-input symbols_restrict" name="user_name" placeholder="{{ trans('client/profile/profile.text_user_name') }}" data-rule-minlength="3" data-rule-maxlength="32" id="user_name" value="{{isset($arr_expert_details['user_details']['user_name'])?$arr_expert_details['user_details']['user_name']:''}}" data-rule-required="true" /></div>
                        <span id="err_user_name" class='error'></span>
                     </div>
               </div>

               <?php 
                  $address = '';
                  if( isset($arr_expert_details['address']) && $arr_expert_details['address'] != "" )
                  {
                     $address = $arr_expert_details['address'];                     
                  }
                  else if (old('address') != "")
                  {
                     $address = old('address');                     
                  }
               ?>

               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('expert/profile/profile.text_address') }}<span>*</span></div>
                     <div class="input-name"><textarea rows="" cols="" class="clint-input" autocomplete="off" placeholder="{{ trans('expert/profile/profile.entry_address') }}" name="address" id="address"  data-rule-required="true" data-rule-maxlength="450">{{$address}}</textarea>
                        <span class='error'>{{ $errors->first('address') }}</span>                                                            
                     </div>   
                  </div>
               </div>
               <div class="geo-details">
                           <div class="col-sm-12  col-md-6 col-lg-6">
                              <div class="user-box">
                                 <div class="p-control-label">{{ trans('expert/profile/profile.text_country') }}<span>*</span></div>
                                 <div class="input-name">
                                    <div class="droup-select">
                                       <select class="droup" data-rule-required="true" name="country" id="country_client" onchange="javascript: return loadStates(this);">
                                          <option value="">{{ trans('expert/profile/profile.entry_country') }}</option>
                                          @if(isset($arr_countries) && sizeof($arr_countries)>0)
                                          @foreach($arr_countries as $countries)
                                          <option value="{{isset($countries['id'])?$countries['id']:""}}" 
                                          @if(isset($arr_expert_details['country']) && $countries['id']==$arr_expert_details['country'])
                                          selected="true"
                                          @endif
                                          >
                                          {{isset($countries['country_name'])?$countries['country_name']:"" }}
                                          </option>
                                          @endforeach
                                          @endif
                                       </select>
                                       <span class='error'>{{ $errors->first('country') }}</span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-12  col-md-6 col-lg-6">
                              <div class="user-box">
                                 <div class="p-control-label">{{ trans('expert/profile/profile.text_state') }}</div>
                                 <div class="input-name">
                                    <div class="droup-select">
                                       <select class="droup" name="state" id="state_client" onchange="javascript: return loadCities(this);">
                                          <option value="">{{ trans('expert/profile/profile.entry_state') }}</option>
                                          @if(isset($arr_expert_details['country_details']['states']) && sizeof($arr_expert_details['country_details']['states'])>0)
                                          @foreach($arr_expert_details['country_details']['states'] as $states)
                                          <option value="{{isset($states['id'])?$states['id']:""}}"
                                          @if($states['id']==$arr_expert_details['state'])
                                          selected="true" 
                                          @endif
                                          >
                                          {{isset($states['state_name'])?$states['state_name']:"" }}
                                          </option>
                                          @endforeach
                                          @endif
                                       </select>
                                       <span class='error'>{{ $errors->first('state') }}</span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clearfix"></div>
                           <div class="col-sm-12  col-md-6 col-lg-6">
                              <div class="user-box">
                                 <div class="p-control-label">{{ trans('expert/profile/profile.text_city') }}</div>
                                 <div class="input-name">
                                   <div class="droup-select">
                                    <select class="droup" name="city" id="city_client">
                                       <option value="">{{ trans('expert/profile/profile.entry_city') }}</option>
                                       @if(isset($arr_expert_details['state_details']['cities']) && sizeof($arr_expert_details['state_details']['cities'])>0)
                                       @foreach($arr_expert_details['state_details']['cities'] as $cities)
                                       <option value="{{isset($cities['id'])?$cities['id']:""}}" 
                                       @if($cities['id']==$arr_expert_details['city'])
                                       selected="true" 
                                       @endif
                                       >
                                       {{isset($cities['city_name'])?$cities['city_name']:"" }}
                                       </option>
                                       @endforeach
                                       @endif
                                    </select>
                                    <span class='error'>{{ $errors->first('city') }}</span>
                                 </div>
                                  </div>
                              </div>
                           </div>
                           <?php 
                              $zip = '';
                              if( isset($arr_expert_details['zip']) && $arr_expert_details['zip'] != "" )
                              {
                                 $zip = $arr_expert_details['zip'];                     
                              }
                              else if (old('zip') != "")
                              {
                                 $zip = old('zip');                     
                              }
                           ?>
                           <div class="col-sm-12  col-md-6 col-lg-6">
                              <div class="user-box">
                                 <div class="p-control-label">{{ trans('expert/profile/profile.text_postal_code') }}<span>*</span></div>
                                 <div class="input-name"><input type="text" class="clint-input" placeholder="{{ trans('expert/profile/profile.entry_postal_code') }}" name="zip" id="zip" value="{{$zip}}" data-rule-required="true" data-rule-maxlength="17" data-rule-minlength="4" onkeyup="return chk_validation(this);"/>
                                    <span class='error'>{{ $errors->first('zip') }}</span>                                                            
                                 </div>
                              </div>
                           </div>

                     <!-- <input data-geo="administrative_area_level_2"  class="con-input" name="city"    id="city"     placeholder="city"    type="text" /><br>
                     <input data-geo="administrative_area_level_1"  class="con-input" name="state"   id="state"    placeholder="state"   type="text" /><br>
                     <input data-geo="country"                      class="con-input" name="country" id="country"  placeholder="country" type="text" /><br> -->
                     <!-- <input data-geo="postal_code" class="con-input" name="zipcode" id="zipcode"  placeholder="zipcode" type="text" /><br> -->      
                     <input data-geo="lat"         class="con-input" name="lat"     id="lat"      placeholder="lat"     type="hidden" /><br>
                     <input data-geo="lng"         class="con-input" name="long"    id="long"     placeholder="lat"     type="hidden" /><br>       
               </div>
               <div class="clearfix"></div>
               <?php 
                  $vat_number = '';
                  if( isset($arr_expert_details['vat_number']) && $arr_expert_details['vat_number'] != "" )
                  {
                     $vat_number = $arr_expert_details['vat_number'];                     
                  }
                  else if (old('vat_number') != "")
                  {
                     $vat_number = old('vat_number');                     
                  }
               ?>
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('expert/profile/profile.text_vat_number') }}<span>*</span></div>
                     <div class="input-name"><input type="text" class="clint-input" placeholder="{{ trans('expert/profile/profile.entry_vat_number') }}" name="vat_number" id="vat_number" value="{{$vat_number}}"  data-rule-required="true" data-rule-maxlength="20" data-fv-vat="true" onkeyup="return chk_validation(this);"/>
                        <span class='error'>{{ $errors->first('vat_number') }}</span>                                                            
                     </div>
                  </div>
               </div>
               <?php 
                  $company_name = '';
                  if( isset($arr_expert_details['company_name']) && $arr_expert_details['company_name'] != "" )
                  {
                     $company_name = $arr_expert_details['company_name'];                     
                  }
                  else if (old('company_name') != "")
                  {
                     $company_name = old('company_name');                     
                  }
               ?>

               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('expert/profile/profile.text_compny_name') }}<span>*</span></div>
                     <div class="input-name"><input type="text" class="clint-input" placeholder="{{ trans('expert/profile/profile.entry_compny_name') }}" data-rule-required="true" name="company_name" id="company_name" value="{{$company_name}}" />
                     </div>
                  </div>
               </div>

               <div class="clearfix"></div>

               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('expert/profile/profile.text_time_zone') }}<span>*</span></div>
                     <div class="input-name">
                        <div class="droup-select">
                           <select class="droup" name="timezone" id="timezone" data-rule-required="true">
                              <option value="">{{ trans('expert/profile/profile.text_time_zone') }}</option>
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
               <?php 
                  $oldprofession = '';
                  if( isset($arr_expert_details['profession']) && $arr_expert_details['profession'] != "" )
                  {
                     $oldprofession = $arr_expert_details['profession'];                     
                  }
                  else if (old('profession') != "")
                  {
                     $oldprofession = old('profession');                     
                  }
               ?>
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('client/profile/profile.text_occupation') }}<span>*</span></div>
                     <div class="input-name">
                        <div class="droup-select">
                           <select class="droup" data-rule-required="true" name="profession" id="profession">
                              <option value="">{{ trans('client/profile/profile.entry_occupation') }}</option>
                              @if(isset($arr_professions) && sizeof($arr_professions)>0)
                                 @foreach($arr_professions as $profession)
                                    <option value="{{isset($profession['id'])?$profession['id']:""}}" 
                                    @if($profession['id']==$oldprofession)
                                    selected=""
                                    @endif>
                                    {{isset($profession['profession_title'])?$profession['profession_title']:"" }}
                                    </option>
                                 @endforeach
                              @endif
                           </select>
                           <span class=''><i><span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span> If not available <a href="{{url('/contact-us')}}" target="_blank">Contact to admin</a></i></span>
                           <span class='error'>{{ $errors->first('profession') }}</span>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="clearfix"></div>

               <?php 
                  $about_me = '';
                  if( isset($arr_expert_details['about_me']) && $arr_expert_details['about_me'] != "" )
                  {
                     $about_me = $arr_expert_details['about_me'];                     
                  }
                  else if (old('about_me') != "")
                  {
                     $about_me = old('about_me');                     
                  }
               ?>

               <div class="col-sm-12  col-md-12 col-lg-12">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('expert/profile/profile.text_spoken_languages') }}<span>*</span></div>
                     <div class="input-name err_spoken_languages">
                     <div class="droup-select multiselect-drop">
                        <select class="droup" name="spoken_languages[]" id="spoken_languages" multiple="multiple" data-rule-required="true">
                        </select>
                     </div>
                     </div>
                  </div>
               </div>
               <div class="clearfix"></div>
              
               {{-- Sub cat section here --}}

               <div class="col-sm-12  col-md-12 col-lg-12">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('expert/profile/profile.text_skills') }}<span>*</span>
                     @if(isset($number_of_skills))
                     <span class="label label-important" style="background-color: red; margin-top: -15px; color: antiquewhite; font-size: 9px;">Note!</span> {{ trans('expert/profile/profile.text_skill_note',array('skill'=>$number_of_skills)) }} 
                     @else
                     <span class="label label-important" style="background-color: red; margin-top: -15px; color: antiquewhite; font-size: 9px;">Note!</span> {{ trans('expert/profile/profile.text_skill_note',array('skill'=>0)) }} 
                     @endif
                     </div>
                     <div class="input-name err_skills">
                        <div class="droup-select multiselect-drop">
                           
                           <select class="droup" name="skills[]" id="skills" data-rule-required="true" multiple="multiple">
                           @if(isset($arr_skills) && sizeof($arr_skills)>0)
                              @foreach($arr_skills as $skill)

                              <option  value="{{isset($skill['id'])?$skill['id']:''}}"
                                          @if(isset($arr_expert_details['expert_skills']) && sizeof($arr_expert_details['expert_skills']))
                                             @foreach($arr_expert_details['expert_skills'] as $expt_skill)
                                                @if(isset($expt_skill['skill_id']) && $expt_skill['skill_id']==$skill['id'])
                                                   selected='selected'
                                                @endif
                                             @endforeach
                                          @endif
                                           >{{isset($skill['skill_name'])?$skill['skill_name']:''}}</option>
                              @endforeach
                           @endif
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="clearfix"></div>
              
               <div class="col-sm-12  col-md-12 col-lg-12">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('expert/profile/profile.text_about_you') }}</div>
                     <div class="input-name"><textarea rows="" cols="" class="client-taxtarea" placeholder="{{ trans('expert/profile/profile.short_introduce_of_yourself') }}" name="about_me" id="about_me"  data-rule-maxlength="450">{{$about_me}}</textarea>
                        <span class='error'>{{ $errors->first('about_me') }}</span>                                                            
                     </div>
                  </div>
               </div>

                 <?php 
                  $hourly_rate = '';
                  if( isset($arr_expert_details['hourly_rate']) && $arr_expert_details['hourly_rate'] != "" )
                  {
                     $hourly_rate = $arr_expert_details['hourly_rate'];                     
                  }
                  else if (old('hourly_rate') != "")
                  {
                     $hourly_rate = old('hourly_rate');                     
                  }
               ?>
               
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">Hourly Rate ( {{config('app.currency_symbol')}} )<span>*</span></div>
                     <div class="input-name">
                        <input type="text" class="clint-input char_restrict" placeholder="Hourly Rate" data-rule-required="true" name="hourly_rate" id="hourly_rate" value="{{$hourly_rate}}" />                            
                     </div>
                  </div>
               </div>
               


               <!-- Start Add education, work experience and certifications field -->

               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('expert/profile/profile.text_education') }}</div>
                     <div class="input-name">
                       <div class="droup-select">
                        <select class="droup" name="education">
                           <option value="">{{ trans('expert/profile/profile.enter_education') }}</option>
                           @if(isset($arr_education) && sizeof($arr_education)>0)
                           @foreach($arr_education as $education)
                           <option value="{{isset($education['id'])?$education['id']:""}}" 
                           @if($education['id']==$arr_expert_details['education_id'])
                           selected="true" 
                           @endif
                           >
                           {{isset($education['title'])?$education['title']:"" }}
                           </option>
                           @endforeach
                           @endif
                        </select>
                        <span class='error'>{{ $errors->first('qualification') }}</span>
                     </div>
                      </div>
                  </div>
               </div>

               <div class="clearfix"></div>

               <div class="work_experience">
                  <div class="col-sm-12  col-md-12 col-lg-12">
                     <div class="p-control-label section-title-heading">{{ trans('expert/profile/profile.text_work_experience') }}</div>
                  </div>

                  <div class="col-sm-12  col-md-3 col-lg-4">
                     <div class="p-control-label">{{ trans('expert/profile/profile.text_post') }}</div>
                  </div>

                  <div class="col-sm-12  col-md-3 col-lg-3">
                     <div class="p-control-label">{{ trans('expert/profile/profile.text_company_name') }}</div>
                  </div>

                  <div class="col-sm-12  col-md-2 col-lg-2">
                     <div class="p-control-label">{{ trans('expert/profile/profile.text_from_year') }}</div>
                  </div>

                  <div class="col-sm-12  col-md-2 col-lg-2">
                     <div class="p-control-label">{{ trans('expert/profile/profile.text_to_year') }}</div>
                  </div>

                  <div>
                     <button class="add-button-section-main" type="button" title="Add" onclick="education_fields();"><i class="fa fa-plus"></i></button>
                  </div>                  
               </div> 

               <div class="clearfix"></div>

               @if(isset($arr_expert_details['expert_work_experience']) && count($arr_expert_details['expert_work_experience'])>0)
                  @foreach($arr_expert_details['expert_work_experience'] as $key=>$work_experience)
                     <div class="work_details_{{$work_experience['id']}}">
                        <div class="col-sm-12  col-md-3 col-lg-4">
                           <div class="user-box">
                              <div class="input-name">
                                 <input type="text" class="clint-input" id="designation_{{$work_experience['id']}}" placeholder="{{ trans('expert/profile/profile.enter_post') }}" name="designation[]" value="{{$work_experience['designation'] or 'NA'}}" onkeyup="return validate({{$work_experience['id']}});"/>
                              </div>
                              <span class="error" id="error_designation_{{$work_experience['id']}}"></span>
                           </div>
                        </div>

                        <div class="col-sm-12  col-md-3 col-lg-3">
                           <div class="user-box">
                              <div class="input-name">
                                 <input type="text" class="clint-input" id="work_company_name_{{$work_experience['id']}}" placeholder="{{ trans('expert/profile/profile.enter_company_name') }}" name="work_company_name[]" value="{{$work_experience['company_name'] or 'NA'}}" onkeyup="return validate({{$work_experience['id']}});"/>
                              </div>
                              <span class="error" id="error_work_company_name_{{$work_experience['id']}}"></span>
                           </div>
                        </div>

                        <div class="col-sm-12  col-md-2 col-lg-2">
                           <div class="user-box">
                              <div class="input-name">
                                 <input type="text" class="clint-input" id="from_year_{{$work_experience['id']}}" name="from_year[]" placeholder="{{ trans('expert/profile/profile.enter_year') }}" value="{{$work_experience['from_year'] or 'NA'}}" onkeyup="return validate({{$work_experience['id']}});"/>
                              </div>
                              <span class="error" id="error_from_year_{{$work_experience['id']}}"></span>
                           </div>
                        </div>

                        <div class="col-sm-12  col-md-2 col-lg-2">
                           <div class="user-box">
                              <div class="input-name">
                                 <input type="text" class="clint-input" id="to_year_{{$work_experience['id']}}" name="to_year[]" placeholder="{{ trans('expert/profile/profile.enter_year') }}" value="{{$work_experience['to_year'] or 'NA'}}" onkeyup="return validate({{$work_experience['id']}});" />
                              </div>
                              <span class="error" id="error_to_year_{{$work_experience['id']}}"></span>
                           </div>
                        </div>

                        <div>
                           <button class="add-button-section-main" type="button" id="remove_button_{{$work_experience['id']}}" data-id-{{$work_experience['id']}}="{{$key}}" title="Remove" onclick="remove_exist_work_details({{$work_experience['id']}});"><i class="fa fa-minus"></i></button>
                        </div> 
                        <div class="clearfix"></div>
                     </div>
                     <div class="clearfix"></div>
                  @endforeach 
               @else
                  <div>
                     <div class="col-sm-12  col-md-3 col-lg-3">
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" id="new_designation1" class="clint-input" placeholder="{{ trans('expert/profile/profile.enter_post') }}" name="designation[]" onkeyup="return new_validate(1);"/>
                           </div>
                           <span class="error" id="error_new_designation1"></span>
                        </div>
                     </div>

                     <div class="col-sm-12  col-md-3 col-lg-3">
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input" id="new_work_company_name1" placeholder="{{ trans('expert/profile/profile.enter_company_name') }}" name="work_company_name[]" onkeyup="return new_validate(1);"/>
                           </div>
                           <span class="error" id="error_new_work_company_name1"></span>
                        </div>
                     </div>

                     <div class="col-sm-12  col-md-2 col-lg-2">
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input" id="new_from_year1" name="from_year[]" placeholder="{{ trans('expert/profile/profile.enter_year') }}" onkeyup="return new_validate(1);"/>
                           </div>
                           <span class="error" id="error_new_from_year1"></span>                           
                        </div>
                     </div>

                     <div class="col-sm-12  col-md-2 col-lg-2">
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input" id="new_to_year1" name="to_year[]" placeholder="{{ trans('expert/profile/profile.enter_year') }}" onkeyup="return new_validate(1);" />
                           </div>
                           <span class="error" id="error_new_to_year1"></span>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                  </div>
                  <div class="clearfix"></div>
               @endif           

               <div class="clearfix"></div>

               <div id="education_fields">
               </div>

               <div class="certification_details">
                  <div class="col-sm-12  col-md-12 col-lg-12">
                     <div class="p-control-label section-title-heading">{{ trans('expert/profile/profile.text_certifications') }}</div> 
                  </div>

                  <div class="col-sm-12  col-md-4 col-lg-4">
                     <div class="p-control-label">{{ trans('expert/profile/profile.text_certification_name') }}</div>
                  </div>

                  <div class="col-sm-12  col-md-4 col-lg-4">
                     <div class="p-control-label">{{ trans('expert/profile/profile.text_completion_year') }}</div>
                  </div>

                  <div class="col-sm-12  col-md-3 col-lg-3">
                     <div class="p-control-label">{{ trans('expert/profile/profile.text_expire_year') }}</div>
                  </div>

                  <div>
                     <button id="btn_certificate_plus" class="add-button-section-main" type="button" title="Add" onclick="add_certification();"><i class="fa fa-plus"></i></button>
                  </div>
               </div>

               <div class="clearfix"></div>

               @if(isset($arr_expert_details['expert_certification_details']) && count($arr_expert_details['expert_certification_details'])>0)
                  @foreach($arr_expert_details['expert_certification_details'] as $key=>$certification)
                     <div class="certification_details_{{$certification['id']}}">
                        <div class="col-sm-12  col-md-4 col-lg-4">
                           <div class="user-box">
                              <div class="input-name">
                                 <input type="text" id="certification_{{$certification['id']}}" class="clint-input" placeholder="{{ trans('expert/profile/profile.text_enter_certification_name') }}" name="certification_name[]" onkeyup="return validate_old_certification({{$certification['id']}});" value="{{$certification['certification_name'] or 'NA'}}" />
                              </div>
                              <span class="error" id="error_certification_{{$certification['id']}}"></span>
                           </div>
                        </div>

                        <div class="col-sm-12  col-md-4 col-lg-4">
                           <div class="user-box">
                              <div class="input-name">
                                    <!-- <input type="text" class="clint-input" id="completion_year_{{$certification['id']}}" placeholder="{{ trans('expert/profile/profile.text_enter_completion_year') }}" name="completion_year[]" onkeyup="return validate_old_certification({{$certification['id']}});" value="{{date('d-M-y',strtotime($certification['completion_year'])) or 'NA'}}" />-->

                                    <input class="onclick-calendar clint-input" type="text" id="completion_year_{{$certification['id']}}" placeholder="{{ trans('expert/profile/profile.text_enter_completion_year') }}" name="completion_year[]" onchange="return validate_old_certification({{$certification['id']}});" value="{{isset($certification['completion_year'])?date('d-M-y',strtotime($certification['completion_year'])):''}}">
                                    <i class="fa fa-calendar" id="last_date"></i>
                              </div>
                              <span class="error" id="error_completion_year_{{$certification['id']}}"></span>
                           </div>
                        </div>                        

                         <div class="col-sm-12  col-md-3 col-lg-3">
                           <div class="user-box">
                              <div class="input-name">
                                 <!-- <input type="text" class="clint-input" id="expire_year_{{$certification['id']}}" placeholder="{{ trans('expert/profile/profile.text_enter_completion_year') }}" name="expire_year[]" onkeyup="return validate_old_certification({{$certification['id']}});" value="{{$certification['expire_year'] or 'NA'}}" />-->

                                    <input class="onclick-calendar clint-input" type="text" id="expire_year_{{$certification['id']}}" placeholder="{{ trans('expert/profile/profile.text_enter_completion_year') }}" name="expire_year[]" onchange="return validate_old_certification({{$certification['id']}});" value="{{isset($certification['expire_year'])?date('d-M-y',strtotime($certification['expire_year'])):''}}">
                                    <i class="fa fa-calendar" id="last_date"></i>
                              </div>
                              <span class="error" id="error_expire_year_{{$certification['id']}}"></span>
                           </div>
                        </div>

                        <div>
                           <button class="add-button-section-main" type="button" title="Remove" onclick="remove_exist_certification_details('{{$certification['id']}}','{{$key}}');"><i class="fa fa-minus"></i></button>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                  @endforeach
               @else
                  <div>
                     <div class="col-sm-12  col-md-4 col-lg-4">
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" id="new_certification_1" class="clint-input" placeholder="{{ trans('expert/profile/profile.text_enter_certification_name') }}" name="certification_name[]" onkeyup="return validate_new_certification(1);"/>
                           </div>
                           <span class="error" id="error_new_certification_1"></span>
                        </div>
                     </div>

                     <div class="col-sm-12  col-md-4 col-lg-4">
                        <div class="user-box">
                           <div class="input-name">
                             {{--  <input type="text" class="clint-input" id="new_completion_year_1" placeholder="{{ trans('expert/profile/profile.text_enter_completion_year') }}" name="completion_year[]" onkeyup="return validate_new_certification(1);"/> --}}
                             <input class="onclick-calendar clint-input" type="text" id="new_completion_year_1" placeholder="{{ trans('expert/profile/profile.text_enter_completion_year') }}" name="completion_year[]" onchange="return validate_new_certification(1);">
                              <i class="fa fa-calendar" id="last_date"></i>
                           </div>
                           <span class="error" id="error_new_completion_year_1"></span>
                        </div>
                     </div>

                     <div class="col-sm-12  col-md-3 col-lg-3">
                        <div class="user-box">
                           <div class="input-name">
                             {{--  <input type="text" class="clint-input" id="new_expire_year_1" placeholder="{{ trans('expert/profile/profile.text_enter_expire_year') }}" name="expire_year[]" onkeyup="return validate_new_certification(1);"/> --}}
                             <input class="onclick-calendar clint-input" type="text" id="new_expire_year_1" placeholder="{{ trans('expert/profile/profile.text_enter_completion_year') }}" name="expire_year[]" 
                             onchange="return validate_new_certification(1);">
                              <i class="fa fa-calendar" id="last_date"></i>
                           </div>
                           <span class="error" id="error_new_expire_year_1"></span>
                        </div>
                     </div>

                  </div>
               @endif

               <div class="clearfix"></div>

               <div id="add_certification">
               </div>

               <!-- End Add education, work experience and certifications field -->

               <div class="col-sm-12 col-md-12 col-lg-12"><button id="eupdate" class="normal-btn pull-right" type="submit">{{ trans('expert/profile/profile.text_update') }}</button></div>
            </div>
         </div>
      </div>
   </form>
    <div class="clearfix"></div>

   <div class="right_side_section category-edit-bx">
        <div class="row">
         @if(isset($arr_experts_categories) && (sizeof($arr_experts_categories) < $number_of_categories) && $check_flag== '0' ) 
               {{-- start of category --}}
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">Add {{ trans('expert/profile/profile.text_categories') }}<span>*</span>
                     
                      @if(isset($number_of_categories))
                     <span class="label label-important" style="background-color: red; margin-top: -15px; color: antiquewhite; font-size: 9px;">Note!</span> {{ trans('expert/profile/profile.text_categories_note',array('cat'=>$number_of_categories)) }}&nbsp;&nbsp;&nbsp;&nbsp;
                     @else
                     <span class="label label-important" style="background-color: red; margin-top: -15px; color: antiquewhite; font-size: 9px;">Note!</span> {{ trans('expert/profile/profile.text_categories_note',array('cat'=>0)) }}&nbsp;&nbsp;&nbsp;&nbsp;
                     @endif
                     </div>
                   
            

                   <div class="input-name err_categories">
                     <div class="droup-select">
                       <select class="droup" data-rule-required="false" name="category" onchange="return getSubCategory(this);" id="category"  >
                           <option value="">Select Category</option>
                           @if(isset($arr_categories) && sizeof($arr_categories)>0)
                              @foreach($arr_categories as $key => $category)
                              <option value="{{isset($category['id'])?$category['id']:""}}" >
                                 {{isset($category['category_title'])?$category['category_title']:"" }}
                              </option>
                              @endforeach
                           @endif
                     </select>
                  </div>
                   </div>
                   <br>
                  </div>
               </div>
               {{-- End of category --}}
               {{-- start of subcategory --}}                
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">Add Subcategories<span>*</span>
                     
                     @if(isset($number_of_subcategories))
                     <span class="label label-important"  style="background-color: red; margin-top: -15px; color: antiquewhite; font-size: 9px;">Note!</span> {{ trans('expert/profile/profile.text_sub_categories_note',array('cat'=>$number_of_subcategories)) }}
                     @else
                     <span class="label label-important" style="background-color: red; margin-top: -15px; color: antiquewhite; font-size: 9px;">Note!</span> {{ trans('expert/profile/profile.text_sub_categories_note',array('cat'=>0)) }}
                     @endif
                     </div>                      
                     
                        <div class="input-name err_categories">
                           <div class="droup-select multiselect-drop">
                              <div id="subcategory_div">
                                 <select class="droup" name="subcategory[]" id="subcategory"  value=""  data-rule-required="false" >
                                 </select>
                              </div>
                           </div>
                        </div>
                        <br />
                  </div>
               </div>
            
               {{-- End of subcategory --}}                
               @endif


               @if(isset($arr_experts_categories) && (sizeof($arr_experts_categories) > 0) && isset($edit_arr_experts_categories) && isset($arr_subcategories) && $check_flag == '1') 
               {{-- start of EDIT category --}}
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">Edit {{ trans('expert/profile/profile.text_categories') }}<span>*</span>
                     </div>
                   
            
                   <div class="input-name err_categories">
                        <div class="droup-select multiselect-drop">
                          <select class="droup" data-rule-required="false" name="edit_category" data-expert-id="{{$edit_arr_experts_categories['id']}}" onchange="return getSubCategory2(this);" id="edit_category"  >
                              <option value="">Select Category</option>
                              @if(isset($arr_categories) && sizeof($arr_categories)>0)
                                 @foreach($arr_categories as $key => $category)
                                 <option value="{{isset($category['id'])?$category['id']:""}}" 
                                    @if($edit_arr_experts_categories['category_id'] == $category['id']) selected  @endif >
                                    {{isset($category['category_title'])?$category['category_title']:"" }}
                                 </option>
                                 @endforeach
                              @endif
                        </select>
                        {{-- <a class="get-sub-cate" data-category-id="{{$edit_arr_experts_categories['category_id']}}" onclick="return getSubCategory3(this);">Get subcategories</a> --}}
                         
                        </div>
                     </div>
                       <a class="cancel-sub-cate" href="{{url('/public')}}/expert/profile">Cancel</a>  
                  
                  <br />
                  </div>
               </div>
               {{-- End of EDIT category --}}
               {{-- start of EDIT subcategory --}}
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">Edit Subcategories<span>*</span>
                     </div>

                        <?php
                              $arr_edit_sub_categories_id = [];
                              if(isset($edit_arr_experts_categories['experts_categories_subcategories_details']) && count($edit_arr_experts_categories['experts_categories_subcategories_details'])>0){
                                 foreach ($edit_arr_experts_categories['experts_categories_subcategories_details'] as $key => $value) 
                                 {
                                    if(isset($value['subcategory_id'])){
                                       $arr_edit_sub_categories_id[] =$value['subcategory_id'];
                                    }
                                 }
                              }

                        ?>
                        <div class="input-name err_categories">
                           <div class="droup-select multiselect-drop">
                              <div id="edit_subcategory_div">
                                 <select class="droup" name="edit_subcategory[]" id="edit_subcategory"  value=""  data-rule-required="false"  multiple="multiple" >
                                    @foreach( $arr_subcategories as $row => $col )
                                       <option 
                                             value="{{isset($col['id'])?$col['id']:''}}"
                                             @if(isset($col['id']) && in_array($col['id'],$arr_edit_sub_categories_id))
                                                selected="selected"
                                             @endif
                                          >
                                          {{isset($col['subcategory_title'])?$col['subcategory_title']:''}}
                                       </option>
                                    @endforeach
                                    {{-- @foreach($edit_arr_experts_categories['experts_categories_subcategories_details'] as $key1 => $subcat)
                                    
                                       <option  selected="selected" value="{{isset($subcat['subcategory_traslation'][0]['subcategories_id'])?$subcat['subcategory_traslation'][0]['subcategories_id']:''}}" >
                                       {{isset($subcat['subcategory_traslation'][0]['subcategory_title'])?$subcat['subcategory_traslation'][0]['subcategory_title']:''}}
                                       </option>
                                    @endforeach --}}
                                 </select>
                               
                              </div>
                               
                           </div> 
                             </div>
                              <a onclick="saveCategory2(this);" class="cancel-sub-cate">Update</a>

                      
                        <br />
                  </div>
               </div>
               {{-- End of EDIT subcategory --}}
               @endif

               @if(isset($arr_experts_categories) && sizeof($arr_experts_categories)>0)
               @foreach($arr_experts_categories as $key => $data)
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">Category {{$key+1}}</div>
                     <div class="input-name"><input type="text" class="clint-input valid" name="vat_number" id="vat_number" value="{{$data['categories']['category_title']}}"  data-fv-vat="true" autocomplete="off" disabled="true" >
                        <span class="error"></span>                                                            
                     </div>
                  </div>
               </div>
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">Subcategories</div>
                       <div class="read-only-category">
                     <div class="input-name err_categories">
                           <div class="droup-select multiselect-drop">
                              <div id="subcategory_div">
                                 <select class="droup" name="subcategory[]" id="subcategory{{$key}}" value="" data-rule-required="false" multiple="multiple" >
                                 @foreach($data['experts_categories_subcategories_details'] as $key1=>$subcat)
                                    <option  selected="selected" value="{{isset($subcat['subcategory_traslation'][0]['subcategories_id'])?$subcat['subcategory_traslation'][0]['subcategories_id']:''}}">
                                    {{isset($subcat['subcategory_traslation'][0]['subcategory_title'])?$subcat['subcategory_traslation'][0]['subcategory_title']:''}}
                                    </option>
                                 @endforeach
                                 </select>
                              </div>
                              <a id="btn_edit" href="{{url('/public')}}/expert/profile/{{base64_encode($data['id'])}}/{{base64_encode($data['category_id'])}}" >Edit</a>
                           </div>
                        </div>
                             </div>
                  </div>
               </div>  
               @endforeach
               @endif

               <input type="hidden" name="room_no" id="room_no" value="2">
               <input type="hidden" name="certification_room_no" id="certification_room_no" value="2">               
         </div>
   </div>


<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/timezones/dist/timezones.full.js"></script>
<link href="{{url('/public')}}/assets/select2/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
{{-- <script type="text/javascript" src="{{url('/public')}}/assets/select2/dist/js/select2.full.js"></script>
 --}}
<script type="text/javascript" src="{{url('/public')}}/assets/languages/all_langs.js"></script>
<script type="text/javascript" src="{{url('/public')}}/front/js/bootstrap-select.min.js"></script>
<link href="{{url('/public')}}/front/css/bootstrap-select.min.css" type="text/css" rel="stylesheet" />

<!--date picker start here-->
<link rel="stylesheet" type="text/css" href="{{url('/public')}}/front/css/jquery-ui.css" />
<script src="{{url('/public')}}/front/js/jquery-ui.js" type="text/javascript"></script>                         
 <script>
    $(function() {
      
      $(".onclick-calendar").datepicker({
         dateFormat: 'dd-M-yy',
         changeYear: true,
         changeMonth: true,
      })
      
    });
/*
    $('#btn_certificate_plus').load(function() {
      $(".onclick-calendar" ).datepicker();
    });*/

    $("#btn_certificate_plus").click(function(){
     $(".onclick-calendar" ).datepicker({
      dateFormat: 'dd-M-yy',
      changeYear: true,
      changeMonth: true,

     });
   });

 </script>

<script>
   var room = 1;
   var certification_room = 1;
   
   function education_fields(id) {
   
       var room_no = $('#room_no').val();
       room_no++;
       var room = room_no;
   
       var objTo = document.getElementById('education_fields')
       var divtest = document.createElement("div");
       divtest.setAttribute("class", "removeclass" + room);
       var rdiv = 'removeclass' + room;
               
       divtest.innerHTML ='<div><div class="col-sm-12  col-md-3 col-lg-4"><div class="user-box"><div class="input-name"><input type="text" class="clint-input" id="new_designation'+room+'" placeholder="{{ trans('expert/profile/profile.enter_post') }}" name="designation[]" onkeyup="return new_validate('+room+');"/></div><span class="error" id="error_new_designation'+room+'"></span></div></div><div class="col-sm-12  col-md-3 col-lg-3"><div class="user-box"><div class="input-name"><input type="text" class="clint-input" id="new_work_company_name'+room+'" name="work_company_name[]" placeholder="{{ trans('expert/profile/profile.enter_company_name') }}" onkeyup="return new_validate('+room+');"/></div><span class="error" id="error_new_work_company_name'+room+'"></span></div></div><div class="col-sm-12  col-md-2 col-lg-2"><div class="user-box"><div class="input-name"><input type="text" class="clint-input" id="new_from_year'+room+'" name="from_year[]" placeholder="{{ trans('expert/profile/profile.enter_year') }}" onkeyup="return new_validate('+room+');"/></div><span class="error" id="error_new_from_year'+room+'"></span></div></div><div class="col-sm-12  col-md-2 col-lg-2"><div class="user-box"><div class="input-name"><input type="text" class="clint-input" id="new_to_year'+room+'" name="to_year[]" placeholder="{{ trans('expert/profile/profile.enter_year') }}" onkeyup="return new_validate('+room+');" /></div><span class="error" id="error_new_to_year'+room+'"></span></div></div><div><button class="add-button-section-main" type="button" title="Remove" onclick="remove_education_fields(' + room + ');"><i class="fa fa-minus"></i></button></div></div><div class="clearfix"></div>';
   
       objTo.appendChild(divtest);
   
       $('#room_no').val(room_no);
   }
   
   function remove_education_fields(rid)
   {
       $('.removeclass' + rid).remove();
       room = room - 1;
       $('#eupdate').removeAttr('disabled');
   }

   function remove_exist_work_details(ref)
   {
      var id = $('#remove_button_'+ref).attr('data-id-'+ref);

      if(id==0)
      {
         $('#designation_'+ref).val('');
         $('#work_company_name_'+ref).val('');
         $('#from_year_'+ref).val('');
         $('#to_year_'+ref).val('');
         $('#eupdate').removeAttr('disabled');
      }
      else
      {         
         $('.work_details_' + ref).remove();
         $('#eupdate').removeAttr('disabled');
      }
   }

   function validate(ref)
   {
      var flag = 0;
      var designation = $('#designation_'+ref).val();
      var company_name = $('#work_company_name_'+ref).val();
      var from_year = $('#from_year_'+ref).val();
      var to_year = $('#to_year_'+ref).val();
      var date = new Date();
      var year = date.getFullYear();

      $('.error').html('');

      if(designation=='')
      {
         $('#error_designation_'+ref).html('This field is required');
         flag = 1;
      }
      
      if(company_name=='')
      {
         $('#error_work_company_name_'+ref).html('This field is required');
         flag = 1;
      }
      
      if(from_year=='')
      {
         $('#error_from_year_'+ref).html('This field is required');
         flag = 1;
      }
      else if(from_year.length != '4')
      {
         $('#error_from_year_'+ref).html('Enter valid data');
         flag = 1;
      }
      else if(from_year > year)
      {
         $('#error_from_year_'+ref).html('Enter valid data');
         flag = 1;
      }
      
      if(to_year=='')
      {
         $('#error_to_year_'+ref).html('This field is required');
         flag = 1;
      }      
      else if(to_year.length != '4')
      {
         $('#error_to_year_'+ref).html('Enter valid data');
         flag = 1;
      }      
      else if(to_year > year)
      {
         $('#error_to_year_'+ref).html('Enter valid data');
         flag = 1;
      }
      else if(from_year > to_year)
      {
         $('#error_to_year_'+ref).html('Enter valid data');
         flag = 1;
      }

      if(flag == 1)
      {
         $('#eupdate').attr('disabled','true');
         return false;
      }
      else
      {
         $('#eupdate').removeAttr('disabled');
         return true;
      }
      return false;
   }

   function new_validate(ref)
   {
      var flag = 0;
      var designation = $('#new_designation'+ref).val();
      var company_name = $('#new_work_company_name'+ref).val();
      var from_year = $('#new_from_year'+ref).val();
      var to_year = $('#new_to_year'+ref).val();
      var date = new Date();
      var year = date.getFullYear();

      $('.error').html('');

      if(designation=='')
      {
         $('#error_new_designation'+ref).html('This field is required');
         flag = 1;
      }

      if(company_name=='')
      {
         $('#error_new_work_company_name'+ref).html('This field is required');
         flag = 1;
      }

      if(from_year=='')
      {
         $('#error_new_from_year'+ref).html('This field is required');
         flag = 1;
      }
      else if(from_year.length != '4')
      {
         $('#error_new_from_year'+ref).html('Enter valid data');
         flag = 1;
      }
      else if(from_year > year)
      {
         $('#error_new_from_year'+ref).html('Enter valid data');
         flag = 1;
      }

      if(to_year=='')
      {
         $('#error_new_to_year'+ref).html('This field is required');
         flag = 1;
      }      
      else if(to_year.length != '4')
      {
         $('#error_new_to_year'+ref).html('Enter valid data');
         flag = 1;
      }      
      else if(to_year > year)
      {
         $('#error_new_to_year'+ref).html('Enter valid data');
         flag = 1;
      }
      else if(from_year > to_year)
      {
         $('#error_new_to_year'+ref).html('Enter valid data');
         flag = 1;
      }

      if(flag == 1)
      {
         $('#eupdate').attr('disabled','true');
         return false;
      }
      else
      {
         $('#eupdate').removeAttr('disabled');
         return true;
      }
      return false;
   }

   function add_certification(id)
   {   
      var certification_room_no = $('#certification_room_no').val();
      certification_room_no++;
      var certification_room = certification_room_no;
   
      var objTo = document.getElementById('add_certification')
      var divtest = document.createElement("div");
      divtest.setAttribute("class", "remove_certification" + certification_room);

      divtest.innerHTML = '<div><div class="col-sm-12  col-md-4 col-lg-4"><div class="user-box"><div class="input-name"><input type="text" id="new_certification_'+certification_room+'" class="clint-input" placeholder="{{ trans('expert/profile/profile.text_enter_certification_name') }}" name="certification_name[]" onkeyup="return validate_new_certification('+certification_room+');"/></div><span class="error" id="error_new_certification_'+certification_room+'"></span></div></div><div class="col-sm-12  col-md-4 col-lg-4"><div class="user-box"><div class="input-name"><input type="text" class="onclick-calendar clint-input" id="new_completion_year_'+certification_room+'" placeholder="{{ trans('expert/profile/profile.text_enter_completion_year') }}" name="completion_year[]" onchange="return validate_new_certification('+certification_room+');"/><i class="fa fa-calendar" id="last_date"></i></div><span class="error" id="error_new_completion_year_'+certification_room+'"></span></div></div><div class="col-sm-12  col-md-3 col-lg-3"><div class="user-box"><div class="input-name"><input type="text" class="onclick-calendar clint-input" id="new_expire_year_'+certification_room+'" placeholder="{{ trans('expert/profile/profile.text_enter_expire_year') }}" name="expire_year[]" onchange="return validate_new_certification('+certification_room+');"/><i class="fa fa-calendar" id="last_date"></i></div><span class="error" id="error_new_expire_year_'+certification_room+'"></span></div></div><div><button class="add-button-section-main" type="button" title="Remove" onclick="remove_certification('+certification_room+');"><i class="fa fa-minus"></i></button></div></div><div class="clearfix"></div>';
   
       objTo.appendChild(divtest);
   
       $('#certification_room_no').val(certification_room_no);
   }

   function remove_certification(rid)
   {
       $('.remove_certification'+rid).remove();
       certification_room = certification_room - 1;
       $('#eupdate').removeAttr('disabled');
   }

   function remove_exist_certification_details(ref,id)
   {
      if(id==0)
      {
         $('#certification_'+ref).val('');
         $('#completion_year_'+ref).val('');
         $('#expire_year_'+ref).val('');
         $('#eupdate').removeAttr('disabled');
      }
      else
      {
         $('.certification_details_' + ref).remove();
         $('#eupdate').removeAttr('disabled');
      }
   }

   function validate_old_certification(ref)
   {
      var flag = 0;
      var certification_name = $('#certification_'+ref).val();
      var completion_year = $('#completion_year_'+ref).val();
      var expire_year = $('#expire_year_'+ref).val();
      var date = new Date();
      var year = date.getFullYear();

      $('.error').html('');

      if(certification_name=='')
      {
         $('#error_certification_'+ref).html('This field is required');
         flag = 1;
      }

      if(completion_year=='')
      {
         $('#error_completion_year_'+ref).html('This field is required');
         flag = 1;
      }      

      if(expire_year=='')
      {
         $('#error_expire_year_'+ref).html('This field is required');
         flag = 1;
      }
      else if(completion_year>expire_year)
      {
         $('#error_expire_year_'+ref).html('Enter valid data');
         flag = 1;
      }      

      if(flag == 1)
      {
         $('#eupdate').attr('disabled','true');
         return false;
      }
      else
      {
         $('#eupdate').removeAttr('disabled');
         return true;
      }
      return false;
   }

   function validate_new_certification(ref)
   {
      var flag = 0;
      var certification_name = $('#new_certification_'+ref).val();
      var completion_year = $('#new_completion_year_'+ref).val();
      var expire_year = $('#new_expire_year_'+ref).val();
      var date = new Date();
      var year = date.getFullYear();

      $('.error').html('');

      if(certification_name=='')
      {
         $('#error_new_certification_'+ref).html('This field is required');
         flag = 1;
      }

      if(completion_year=='')
      {
         $('#error_new_completion_year_'+ref).html('This field is required');
         flag = 1;
      }      

       if(expire_year=='')
      {
         $('#error_new_expire_year_'+ref).html('This field is required');
         flag = 1;
      }
      else if(completion_year>expire_year)
      {
         $('#error_new_expire_year_'+ref).html('Enter valid data');
         flag = 1;
      }      

      if(flag == 1)
      {
         $('#eupdate').attr('disabled','true');
         return false;
      }
      else
      {
         $('#eupdate').removeAttr('disabled');
         return true;
      }
      return false;
   }

</script>

<script type="text/javascript">
 $("#form-client_profile").validate({
      errorElement: 'span',
      errorPlacement: function (error, element) 
      {
         if(element.attr("name") == 'phone_code')
         {
            error.insertAfter($("#phone_number"));
         }
         else if(element.attr("id") == 'spoken_languages')
         {
            error.insertAfter($(".err_spoken_languages"));
         }
         else if(element.attr("id") == 'skills')
         {
            error.insertAfter($(".err_skills"));  
         }
         else if(element.attr("id") == 'categories')
         {
            error.insertAfter($(".err_categories"));  
         }
         else
         {
            error.insertAfter(element);
         }
      },
       groups: {
                    phone_number: "phone_code phone_number"
                },
      
   });
$( document ).ready(function() {
   jQuery("#skills").select2();
   jQuery("#categories").select2();
   var i ='';
   for(i=0;i<=10;i++)
   {
      jQuery("#subcategory"+i).select2();
   }
   jQuery("#edit_subcategory").select2(); 
   
   // This is the simple bit of jquery to duplicate the hidden field to subfile
   $('#profile_image').change(function()
   {
     if($(this).val().length>0)
     {
       $("#btn_remove_image").show();
     }
     $('#profile_image_name').val($(this).val());
   });

  
   //times js init
   $('#timezone').timezones();
   setTimezone();
   //set spoken languages pass id of select box
   set_languages('spoken_languages');
   <?php
      $spk_lang = array();
      if (isset($arr_expert_details['expert_spoken_languages']) && count($arr_expert_details['expert_spoken_languages']) > 0) 
      {
         $spk_lang = [];
         foreach ($arr_expert_details['expert_spoken_languages'] as $_key => $value) 
         {
            $spk_lang[] = $value['language'];     
         }
      }
   ?>
   jQuery("#spoken_languages").select2();
 });

function set_languages(control_id){
    if (control_id){
        var all_lang = get_languages();
        if(all_lang){
            if(typeof(all_lang) == "object"){  
                var option = '<option value="">Select languages</option>'; 
                  <?php
                     if (isset($spk_lang) && sizeof($spk_lang)>0){
                        foreach ($spk_lang as $spklang){
                        ?>
                           var currnt_lang = '<?php echo $spklang;?>';
                           for(lang in all_lang){
                            if (currnt_lang==all_lang[lang].name) {
                                 option+='<option selected value="'+all_lang[lang].name+'">'+all_lang[lang].name+'</option>';
                              }
                              else{
                                 option+='<option value="'+all_lang[lang].name+'">'+all_lang[lang].name+'</option>';
                              }
                           }
               <?php    }
                     }
                     else { 
               ?>
                  for(lang in all_lang){
                     option+='<option value="'+all_lang[lang].name+'">'+all_lang[lang].name+'</option>';
                  }
               <?php  
                  }
               ?>
               jQuery('select[id="'+control_id+'"]').html(option);
            }
        }
    }
}
function setTimezone() 
{
   var selected_timezone = "{{isset($arr_expert_details['timezone'])?$arr_expert_details['timezone']:''}}";
   if (selected_timezone && selected_timezone!="") 
   {
         $("#timezone").val(selected_timezone);
   }
}
function browseImage()
{
   $("#profile_image").trigger('click');
}

function removeBrowsedImage(ref){
   var image_type = $(ref).attr('data-image-type');
   if(image_type == 'profile' ){
      if(!confirm('Are you sure ? Do you want to delete profile image')){
         return false;
      }
   }
   else if( image_type == 'cover' ){
      if(!confirm('Are you sure ? Do you want to delete cover image')){
         return false;
      }  
   }
   if(image_type == 'profile'){
      $('#profile_image_name').val("");
      $("#btn_remove_image").hide();
      $("#profile_image").val("");
   }
   $.ajax({
      url:"{{url('/public')}}"+"/expert/delete_image",
      data:{image_type:image_type},
      success:function (response)
      {
         if(typeof response.status != "undefined" && response.status == "SUCCESS_PROFILE")
         {
            $('#msg_profile_image').html(response.msg).show();
         }
         
      }
   });   
}
</script>
<script type="text/javascript">
   function loadStates(ref){
        var selected_country = jQuery(ref).val();
        if(selected_country && selected_country!="" && selected_country!=0){
         jQuery('select[id="state_client"]').find('option').remove().end().append('<option value="">Select State</option>').val('');
         jQuery('select[id="city_client"]').find('option').remove().end().append('<option value="">Select City</option>').val('');
              jQuery.ajax({
               url:'{{url("/")}}/locations/get_states/'+btoa(selected_country),
               type:'GET',
               data:'flag=true',
               dataType:'json',
               beforeSend:function()
               {
                   jQuery('select[id="state_client"]').attr('readonly','readonly');
               },
               success:function(response)
               {
                   if(response.status=="success")
                   {
                       jQuery('select[id="state_client"]').removeAttr('readonly');

                       if(typeof(response.states) == "object")
                       {
                          var option = '<option value="">Select State</option>'; 
                          jQuery(response.states).each(function(index,state)
                          {
                               option+='<option value="'+state.id+'">'+state.state_name+'</option>';
                          });

                          jQuery('select[id="state_client"]').html(option);
                       }

                   }
                   return false;
               }    
              });
         }
   }
   function loadCities(ref)   
    {
        var selected_state = jQuery(ref).val();
   
        if(selected_state && selected_state!="" && selected_state!=0)
        {
   
         jQuery('select[id="city_client"]').find('option').remove().end().append('<option value="">Select City</option>').val('');
   
              jQuery.ajax({
                              url:'{{url("/")}}/locations/get_cities/'+btoa(selected_state),
                              type:'GET',
                              data:'flag=true',
                              dataType:'json',
                             beforeSend:function()
                              {
                                  jQuery('select[id="city_client"]').attr('readonly','readonly');
                              },
                              success:function(response)
                              {
                                  if(response.status=="success")
                                  {
                                      jQuery('select[id="city_client"]').removeAttr('readonly');
   
                                      if(typeof(response.cities) == "object")
                                      {
                                         var option = '<option value="">Select City</option>'; 
                                         jQuery(response.cities).each(function(index,city)
                                         {
                                              option+='<option value="'+city.id+'">'+city.city_name+'</option>';
                                         });
   
                                         jQuery('select[id="city_client"]').html(option);
                                      }
   
                                  }
                                  return false;
                              }    
                        });
         }
    }
   $('#phone_number').keypress(function(eve) 
   {

    if(eve.which == 8 || eve.which == 190) 
      {
        return true;
      }
    else if ((eve.which != 46 || $(this).val().indexOf('.') != -1) && (eve.which < 48 || eve.which > 57) || (eve.which == 46 && $(this).caret().start == 0) ) {
     eve.preventDefault();
  }
     
// this part is when left part of number is deleted and leaves a . in the leftmost position. For example, 33.25, then 33 is deleted
 $('#phone_number').keyup(function(eve) {
  if($(this).val().indexOf('.') == 0) {    $(this).val($(this).val().substring(1));
  }
 });
});
// Function for special characters not accepts.
function chk_validation(ref)
{
   var yourInput = $(ref).val();
    re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
    var isSplChar = re.test(yourInput);
    if(isSplChar)
    {
      var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
      $(ref).val(no_spl_char);
    }
}
  
$('#eupdate').click(function(){
   var user_name = $('#user_name').val();
   if(user_name != ""){
     var chk_username_availablity =  chk_user_name_availablity(user_name); //"Taclient"
     if(chk_username_availablity != 'Available' ){
        return false;
     }
   }
});
function chk_user_name_availablity(ref)
{
  $( "#form-client_profile" ).validate().element( "#user_name" );
  $('#err_user_name').hide();
  $('#err_user_name').html('');
  $('for[user_name]').html('');
  $('#user_name').css('border','1px solid red');
  var yourInput = ref;
  var site_url  = "<?php echo url('/'); ?>";
  var token     = "<?php echo csrf_token(); ?>";
  var return_val = "";
  if(yourInput != "" && yourInput.length >= '3' && yourInput.length <= '32'){
     $.ajax({
      type:"post",
      async:false,
      url:site_url+'/chk_username_availabilty',
      data:{username:yourInput, _token:token},
      success:function(result){
        return_val = result;
        $('#err_user_name').show();
        if(result != 'Available'){
            $('#user_name').css('border','1px solid red');
            $('#err_user_name').html(result);
            $('for[user_name]').html(result);
            $('#err_user_name').css('color','red');
        } else {
            $('#user_name').css('border','1px solid green');
            $('#err_user_name').css('color','green');
        }
      }
    });  
    return return_val;
  }
}
</script>
<!-- location -autocomplete -->
<script type="text/javascript">
   $(document).ready(function(){
      var location = $("#address").val();
      $("#address").geocomplete({
         details: ".geo-details",
         detailsAttribute: "data-geo",
         location:location,
         types:[]
      }).bind("geocode:result", function(event, result){
         //console.log(result['address_components'][0]['long_name']); // city
         $.each( result['address_components'], function( key, value ) {
           var long_name = result['address_components'][key]['long_name'];
         });
      });
   });
</script>

{{-- Here code for get subcategory by ajax --}}

<script type="text/javascript">


   function getSubCategory(ref)
   {    
        var categoryId = $(ref).children("option:selected").val();
        var i = $(ref).attr("data-cat-id");
         

        if(categoryId!='')
        {
            $.ajax({
                url : '{{url('/public')}}/expert/get_subcategory/'+categoryId,
                data: {   _token : "{{ csrf_token() }}", },
                success : function(resp) 
                {
                    if(resp.status == 'success')
                    {
                        $('body').on('DOMNodeInserted', 'select', function () {
                            $(this).select2();
                         });

                        if(resp.html != undefined && resp.html != '')
                        {
                            options = '<select class="droup" multiple="multiple" id="subcategory" name="subcategory[]" data-rule-required="false"><option value=""> -- Select Subcategory -- </option>'+resp.html+'</select><a class="save-sub-cate" onclick="saveCategory(this);">Save</a>';
                            $("#subcategory_div").html('');
                            $("#subcategory_div").html(options); 
                        }
                    }
                    else
                    {
                        options = '<select class="droup" multiple="multiple"><option value=""> -- Select Subcategory -- </option></select>';
                        $("#subcategory_div").html('');
                        $("#subcategory_div").html(options);
                    }
                }
            })
        }
        else
        {
            options = '<select class="droup" multiple="multiple"><option value=""> -- Select Subcategory -- </option></select>';
            $("#subcategory_div").html('');
            $("#subcategory_div").html(options);
        }
    }

</script>


{{-- Here code for add category & subcategory by ajax --}}
<script type="text/javascript">
  function saveCategory(ref)
  {
   
      var arr_subcategory = $("#subcategory").val();
      var category_id     = $("#category").val();
     
      //if(arr_subcategory.length > )
      if(arr_subcategory==null)
      {
         alert('Please select subcategory');
         return false;
      }
      else
      {
          $.ajax({
                type : 'POST',
                url : '{{url('/public')}}/expert/save_category',
                data: {   _token : "{{ csrf_token() }}", category_id:category_id, subcategory:arr_subcategory },
                success : function(resp) 
                {
                    if(resp.status == 'success')
                    {
                       setTimeout(function(){
                           window.location.href = "{{url('/public')}}/expert/profile";
                       }, 1000); 
                    }
                    else
                    {
                         setTimeout(function(){
                            window.location.href = "{{url('/public')}}/expert/profile";
                         }, 1000); 
                       
                    }
                }
            })
      }

  }
</script>


{{-- Here code for reinitialize multiselect demo --}}

<!--</script>-->

<script type="text/javascript">
   function getSubCategory2(ref)
   {
      var categoryId = $(ref).children("option:selected").val();
      var i = $(ref).attr("data-cat-id");
   
        if(categoryId!='')
        {
            $.ajax({
                url : '{{url('/public')}}/expert/get_subcategory/'+categoryId,
                data: {   _token : "{{ csrf_token() }}", },
                success : function(resp) 
                {
                    if(resp.status == 'success')
                    {
                        $('body').on('DOMNodeInserted', 'select', function () {
                            $(this).select2();
                        });

                        if(resp.html != undefined && resp.html != '')
                        {
                            options = '<select class="droup" multiple="multiple" id="edit_subcategory" name="edit_subcategory[]" data-rule-required="false"><option value=""> -- Select Subcategory -- </option>'+resp.html+'</select>';
                            $("#edit_subcategory_div").html('');
                            $("#edit_subcategory_div").html(options); 
                        }
                    }
                    else
                    {
                        options = '<select class="droup" multiple="multiple"><option value=""> -- Select Subcategory -- </option></select>';
                        $("#edit_subcategory_div").html('');
                        $("#edit_subcategory_div").html(options);
                    }
                }
            })
        }
        else
        {
            options = '<select class="droup" multiple="multiple"><option value=""> -- Select Subcategory -- </option></select>';
            $("#edit_subcategory_div").html('');
            $("#edit_subcategory_div").html(options);
        }
    }
   
</script>
<script type="text/javascript">     

  function saveCategory2(ref)
  {

      var arr_subcategory = $("#edit_subcategory").val();
      var category_id     = $("#edit_category").val();
      var expert_id = $('#edit_category').attr("data-expert-id");
      var id = btoa(expert_id);

      if(arr_subcategory==null)
      {
         alert('Please select subcategory');
         return false;
      }
      else
      {
          $.ajax({
                type : 'POST',
                url : '{{url('/public')}}/expert/update_category/'+id,
                data: {   _token : "{{ csrf_token() }}", category_id:category_id, subcategory:arr_subcategory },
                success : function(resp) 
                {
                    if(resp.status == 'success')
                    {
                       setTimeout(function(){
                           window.location.href = "{{url('/public')}}/expert/profile";
                       },1000); 
                    }
                    else
                    {
                         setTimeout(function(){
                            window.location.href = "{{url('/public')}}/expert/profile";
                         }, 1000); 
                       
                    }
                }
            })
      }

  }
</script>
<script type="text/javascript">
   function getSubCategory3(ref)
    {    
        var categoryId = $(ref).attr("data-category-id");
        //var categoryId = $('').val();

        if(categoryId!='')
        {
            $.ajax({
                url : '{{url('/public')}}/expert/get_subcategory/'+categoryId,
                data: {   _token : "{{ csrf_token() }}", },
                success : function(resp) 
                {
                    if(resp.status == 'success')
                    {
                        if(resp.html != undefined && resp.html != '')
                        {
                            options = '<select class="droup" multiple="multiple" id="edit_subcategory" name="edit_subcategory[]" data-rule-required="false"><option value=""> -- Select   Subcategory -- </option>'+resp.html+'</select><a onclick="saveCategory2(this);" class="cancel-sub-cate">Update</a>';
                            $("#edit_subcategory_div").html('');
                            $("#edit_subcategory_div").html(options); 
                        }
                    }
                    else
                    {
                        options = '<select class="droup" multiple="multiple"><option value=""> -- Select Subcategory -- </option></select>';
                        $("#edit_subcategory_div").html('');
                        $("#edit_subcategory_div").html(options);
                    }
                }
            })
        }
        else
        {
            options = '<select class="droup" multiple="multiple"><option value=""> -- Select Subcategory -- </option></select>';
            $("#edit_subcategory_div").html('');
            $("#edit_subcategory_div").html(options);
        }
    }

</script>
<script type="text/javascript" src="{{url('/public')}}/assets/select2/dist/js/select2.full.js"> 
@stop