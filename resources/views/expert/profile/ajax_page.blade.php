@if(isset($arr_experts_categories) && sizeof($arr_experts_categories)>0)
               @foreach($arr_experts_categories as $key => $data)
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">Category {{$key+1}}</div>
                     <div class="input-name"><input type="text" class="clint-input valid" name="vat_number" id="vat_number" value="{{$data['categories']['category_title']}}"  data-fv-vat="true" autocomplete="off" disabled="true" >
                        <span class="error"></span>                                                            
                     </div>
                  </div>
               </div>
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">Subcategories</div>
                        <div class="read-only-category">
                        <div class="input-name err_categories">
                              <div class="droup-select multiselect-drop">
                                 <div id="subcategory_div">
                                    <select class="droup" name="subcategory[]" id="subcategory{{$key}}" value="" data-rule-required="false" multiple="multiple" >
                                    @foreach($data['experts_categories_subcategories_details'] as $key1=>$subcat)
                                       <option  selected="selected" value="{{isset($subcat['subcategory_traslation'][0]['subcategories_id'])?$subcat['subcategory_traslation'][0]['subcategories_id']:''}}">
                                       {{isset($subcat['subcategory_traslation'][0]['subcategory_title'])?$subcat['subcategory_traslation'][0]['subcategory_title']:''}}
                                       </option>
                                    @endforeach
                                    </select>
                                 </div>
                                 <a id="btn_edit" onclick="edit_category(this)" data-id="{{$data['id']}}" data-category-id="{{$data['category_id']}}" style="cursor: pointer;">Edit</a>
                                 
                                 <a id="btn_delete" onclick="confirmDelete(this)" data-categorey-id="{{$data['category_id']}}" style="cursor: pointer;">Delete</a>

                              </div>
                        </div>
                        </div>
                  </div>
               </div>  
               @endforeach
               @endif
<script type="text/javascript">
   $( document ).ready(function() {
   jQuery("#skills").select2();
   jQuery("#categories").select2();
   var i ='';
   for(i=0;i<=10;i++)
   {
      jQuery("#subcategory"+i).select2();
   }
   jQuery("#edit_subcategory").select2(); 
   
   // This is the simple bit of jquery to duplicate the hidden field to subfile
   $('#profile_image').change(function()
   {
     if($(this).val().length>0)
     {
       $("#btn_remove_image").show();
     }
     $('#profile_image_name').val($(this).val());
   });

  
   //times js init
   $('#timezone').timezones();
   setTimezone();
   //set spoken languages pass id of select box
   set_languages('spoken_languages');
   <?php
      $spk_lang = array();
      if (isset($arr_expert_details['expert_spoken_languages']) && count($arr_expert_details['expert_spoken_languages']) > 0) 
      {
         $spk_lang = [];
         foreach ($arr_expert_details['expert_spoken_languages'] as $_key => $value) 
         {
            $spk_lang[] = $value['language'];     
         }
      }
   ?>
   jQuery("#spoken_languages").select2();
 });
</script>