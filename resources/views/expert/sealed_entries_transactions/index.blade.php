@extends('expert.layout.master')                
@section('main_content')
<style type="text/css">
.msg-btn-auto-height
{  
   padding: 4px 5px; font-size:13px;
}
.table-expert-transaction
{
   font-size:11px;   
}

</style>

<div class="col-sm-7 col-md-8 col-lg-9">
    @include('front.layout._operation_status')
    <div class="pink-bx">
        <div class="project-awarded-section remve-mrg-awd">
            <h2>Sealed Entries Summary</h2>
            <div class="project-award-bx">
                <div class="awarded-count">{{ isset($arr_sealed_entries_counts['total_sealed_entries']) ? $arr_sealed_entries_counts['total_sealed_entries'] : 0 }}</div>
                <div class="awarded-days">Total Credits</div>
            </div>
            <div class="project-award-bx awad-line">
                <div class="awarded-count">{{ isset($arr_sealed_entries_counts['used_sealed_entries']) ? $arr_sealed_entries_counts['used_sealed_entries'] : 0 }}</div>
                <div class="awarded-days">Used Credits</div>
            </div>
            <div class="project-award-bx awad-line">
                <div class="awarded-count">{{ isset($arr_sealed_entries_counts['remaning_sealed_entries']) ? $arr_sealed_entries_counts['remaning_sealed_entries'] : 0 }}</div>
                <div class="awarded-days">Remaning Credits</div>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-7 col-md-8 col-lg-9">
   <div class="search-grey-bx white-wrapper">
      <div class="head_grn">Sealed Entries Transactions</div>
      <div class="invite-expert-btns" style="padding-top: 21px;">
           <a class="black-border-btn btn-right-mrg" href="#sealed_entry_topup" class="mp-add-btn-text add-money-but" data-keyboard="false" data-backdrop="static" data-toggle="modal">TopUp Sealed Entry</a>
     </div>
      <div class="table-responsive">
         <table id="TaBle" class="theme-table invoice-table-s table table-expert-transaction" style="border: 1px solid rgb(239, 239, 239); margin-bottom:0;">
            <thead>
               <tr>
                  <th>{{ trans('expert/transactions/packs.text_invoice_id') }}</th>
                  <th>{{ trans('expert/transactions/packs.text_transaction_type') }}</th>
                  <th>{{ trans('expert/transactions/packs.text_payment_amount') }}</th>
                  <th>Quantity</th>
                  <th>{{ trans('expert/transactions/packs.text_payment_method') }}</th>
                  <th>{{ trans('expert/transactions/packs.text_Payment_status') }}</th>
                  <th>{{ trans('expert/transactions/packs.text_Payment_date') }}</th>
                  <th>{{ trans('expert/transactions/packs.text_action') }}</th>
               </tr>
            </thead>
            <tbody>
               @if(isset($arr_transactions['data']) && sizeof($arr_transactions['data'])>0)
               @foreach($arr_transactions['data'] as $transaction)
               <tr>
                  <td>{{isset($transaction['invoice_id'])?$transaction['invoice_id']:''}}</td>
                  <td>
                     @if(isset($transaction['transaction_type']) && $transaction['transaction_type']=='1')
                      Credit
                     @elseif(isset($transaction['transaction_type']) && $transaction['transaction_type']=='2')
                      Debit
                     @endif
                  </td>
                  <td>
                      @if(isset($transaction['transaction_type']) && $transaction['transaction_type']=='1')
                        {{isset($transaction['currency_code'])?$transaction['currency_code']:''}}&nbsp;{{isset($transaction['payment_amount'])?number_format($transaction['payment_amount'],2):'0' }}
                      @elseif(isset($transaction['transaction_type']) && $transaction['transaction_type']=='2')
                        -
                      @endif
                  </td>
                  <td>
                      {{isset($transaction['sealed_entries_quantity'])?$transaction['sealed_entries_quantity']:'0' }}
                  </td>
                  <td>
                     @if(isset($transaction['payment_method']) && $transaction['payment_method']=='1')
                        Wallet
                     @elseif(isset($transaction['payment_method']) && $transaction['payment_method']=='2') 
                       Online
                     @endif
                  </td>
                  <td>
                     @if(isset($transaction['payment_status']) && $transaction['payment_status']=='0')Pending @endif
                     @if(isset($transaction['payment_status']) && $transaction['payment_status']=='1')Success @endif
                     @if(isset($transaction['payment_status']) && $transaction['payment_status']=='2')Failed @endif
                  </td>
                  <td>{{isset($transaction['payment_date'])&&$transaction['payment_date']!='0000-00-00 00:00:00'?date("d-M-Y", strtotime($transaction['payment_date'])):'--'}}</td>
                  <td>
                  <a href="{{ $module_url_path.'/show/'.base64_encode($transaction['id']) }}">

                  <span class="msg_btn-lnew ht-tp msg-btn-auto-height"><i class="fa fa-eye"></i>
                     {{ trans('expert/transactions/packs.text_view')}}</span>
                     </a>
                  </td>
               </tr>
               @endforeach
               @endif
            </tbody>
         </table>
      </div>
   </div>
   <!--pagigation start here-->
   @include('front.common.pagination_view', ['paginator' => $arr_transactions])
   <!--pagigation end here-->
</div>
</div>
</div>
</div>

<!-- add money model -->
<div class="modal fade invite-member-modal" id="sealed_entry_topup" role="dialog">
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <center><h4>Top Up Credits for Seal Entry</h4></center>
             </div>
             <form action="{{url('/expert/sealedentrieshistory/payment')}}" method="post" id="sealed_entry_form">
                {{csrf_field()}}
                <input type="hidden" name="currency_code" id="currency_code" value="{{ isset($arr_data_bal['currency'][0]) ? $arr_data_bal['currency'][0] : 'USD' }}">
                <input type="hidden" name="currency_symbol" id="currency_symbol" value="{{ isset($arr_data_bal['currency'][0]) ? $arr_data_bal['currency'][0] : 'USD' }}">
                <input type="hidden" name="contest_id" id="contest_id" value="{{ isset($arr_contest['id']) ? base64_encode($arr_contest['id']) : '' }}">
                <input type="hidden" name="is_json_data" id="is_json_data" value="0">
                
                <div class="invite-form inter-amt">
                  <div class="user-box">
                      <div class="p-control-label">Sealed Entry Quantity <span class="star-col-block">*</span></div>
                      <div class="droup-select">
                        <select name="sealed_entry_rates_id" id="sealed_entry_rates_id" class="droup mrns tp-margn" data-rule-required="true">
                          <option value="">How many credits would you like to get?</option>
                          @if(isset($arr_sealed_entry_rates) && sizeof($arr_sealed_entry_rates)>0)
                            @foreach($arr_sealed_entry_rates as $key => $rates)
                              <option 
                                value="{{isset($rates['id'])?$rates['id']:''}}"
                                data-amount="{{isset($rates['converted_price']) ? number_format($rates['converted_price'],2) :'0.0'}}">
                                {{isset($rates['quantity'])?$rates['quantity']:''}} - {{isset($rates['currency_code'])?$rates['currency_code']:''}} {{isset($rates['converted_price']) ? number_format($rates['converted_price'],2) :'0.0'}}
                              </option>
                            @endforeach
                          @endif
                        </select>
                        <span class='error'>{{ $errors->first('sealed_entry_rates_id') }}</span>
                        <span class='error_sealed_entry_quantity' style="color:red;"></span>
                      </div>
                  </div>

                  <div class="clearfix"></div>
                  
                  {{-- <div class="amt-info">
                    <span>Total Funds</span><span> <span class="amt-code">{{ isset($user_auth_details['currency_code']) ? $user_auth_details['currency_code'] : 'USD' }}</span> <span id="sealed_entry_total_amount"> 0.00 </span> </span>
                  </div> --}}

                    <div class="amt-info">
                      <span>Wallet Amount</span><span> <span class="amt-code">{{ isset($user_auth_details['currency_code']) ? $user_auth_details['currency_code'] : 'USD' }}</span> <span id="sealed_entry_wallet_amount"> 0.00 </span> </span>
                    </div>

                  <div class="amt-info no-border">
                    <span><b>Total Amount</b></span><span><span class="amt-code">{{ isset($user_auth_details['currency_code']) ? $user_auth_details['currency_code'] : 'USD' }}</span> <span id="sealed_entry_paid_amount"> 0.00 </span> </span>
                  </div>

                  <button type="submit" id="btn_topup_sealed_entry" class="black-btn">Pay Now</button>
               </div>
             </form>
          </div>
       </div>
    </div>
</div>

<script type="text/javascript">
  var currency_code = '{{ isset($user_auth_details['currency_code']) ? $user_auth_details['currency_code'] : 'USD' }}';
  var convert_rates_url = '{{$module_url_path}}'+ '/convert_rates';

  $('#btn_topup_sealed_entry').click(function() {
      if($('#sealed_entry_form').valid()){
        $('#currency_code').val(currency_code);
        $('#sealed_entry_form').submit();
        return true;
      }
  });

  $('#sealed_entry_rates_id').on('change',function(){
      if($(this).val()!=''){
        var sealed_entry_rates_id = $(this).val();  
        $.ajax({
          type: 'GET',
          data: { 'sealed_entry_rates_id' : sealed_entry_rates_id,'currency_code':currency_code },
          url:convert_rates_url,
          beforeSend:function(){
            $('#sealed_entry_total_amount').text('0.0');
            $('#sealed_entry_wallet_amount').text('0.0');
            $('#sealed_entry_paid_amount').text('0.0');
            showProcessingOverlay('');
          },
          success: function(response) {
              hideProcessingOverlay();
              if(response.status!='error'){
                if(response.is_higt_wallet_amount == '1') {
                  $('#sealed_entry_total_amount').parent().parent().hide();
                  $('#sealed_entry_wallet_amount').parent().parent().hide();
                } else {
                  $('#sealed_entry_total_amount').parent().parent().show();
                  $('#sealed_entry_wallet_amount').parent().parent().show();
                }
                $('#sealed_entry_total_amount').text(response.price);
                $('#sealed_entry_wallet_amount').text(response.wallet_amount);
                $('#sealed_entry_paid_amount').text(response.paid_amount);
              } else{
                $('#sealed_entry_total_amount').text('0.0');
                $('#sealed_entry_wallet_amount').text('0.0');
                $('#sealed_entry_paid_amount').text('0.0');
              }
            }
        });
      }
  });
</script>
@stop

