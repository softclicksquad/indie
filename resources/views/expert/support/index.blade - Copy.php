@extends('client.layout.master')
@section('main_content')
<style type="text/css">
.msg-btn-auto-height {   
   padding: 4px 5px; font-size:13px;
}
</style>
<!-- <div class="middle-container"> -->
  <div class="container">
  <!-- <br/> -->
     <div class="row">
         <div class="col-sm-7 col-md-8 col-lg-9">
            <div class="search-grey-bx white-wrapper">
               <div class="head_grn">{{ trans('client/transactions/packs.text_heading') }}</div>
               <div class="table-responsive">
                  <form id="frm_show_cnt" class="form-horizontal show-entry-form" method="POST" action="{{url('client/transactions/show_cnt')}}">
                        {{ csrf_field() }}
                        <div class="col-sm-2 col-md-2 col-lg-2">
                            <div class="sort-by">
                                @php $show_cnt ='show_transaction_cnt';  @endphp
                                @include('front.common.show-cnt-selectbox')
                            </div>
                        </div>
                  </form> 
                  <table id="TaBle" class="theme-table invoice-table-s table" style="border: 1px solid rgb(239, 239, 239); margin-bottom:0;">
                    <thead class="tras-client-tbl">
                        <tr>
                            <th>Reference</th>
                            <th>Subject</th>
                            <th>Department</th>
                            <th>Date Created</th>
                            <th>Last Action</th>                           
                        </tr>
                    </thead>
                    <tbody>
                        <tr role="row" class="odd">
                            <td>INV201910074F89BE</td>
                            <td>Project fee adjust request for <br>Project ID: 17478418</td>
                            <td>General</td>
                            <td>Wed, 8th May 2019 <br>12:16am</td>
                            <td style="text-align: right"><span style="font-family: 'robotobold';color: #444">Robyn G.</span> <br>Mon, 13th May 2019 <br>11:29am</td>
                        </tr>
                        <tr role="row" class="even">
                            <td>INV201910074F89BE</td>
                            <td>Project fee adjust request for <br>Project ID: 17478418</td>
                            <td>General</td>
                            <td>Wed, 8th May 2019 <br>12:16am</td>
                            <td style="text-align: right"><span style="font-family: 'robotobold';color: #444">Robyn G.</span> <br>Mon, 13th May 2019 <br>11:29am</td>
                        </tr>
                        <tr role="row" class="odd">
                            <td>INV201910074F89BE</td>
                            <td>Project fee adjust request for <br>Project ID: 17478418</td>
                            <td>General</td>
                            <td>Wed, 8th May 2019 <br>12:16am</td>
                            <td style="text-align: right"><span style="font-family: 'robotobold';color: #444">Robyn G.</span> <br>Mon, 13th May 2019 <br>11:29am</td>
                        </tr>
                        <tr role="row" class="even">
                            <td>INV201910074F89BE</td>
                            <td>Project fee adjust request for <br>Project ID: 17478418</td>
                            <td>General</td>
                            <td>Wed, 8th May 2019 <br>12:16am</td>
                            <td style="text-align: right"><span style="font-family: 'robotobold';color: #444">Robyn G.</span> <br>Mon, 13th May 2019 <br>11:29am</td>
                        </tr>
                        <tr role="row" class="odd">
                            <td>INV201910074F89BE</td>
                            <td>Project fee adjust request for <br>Project ID: 17478418</td>
                            <td>General</td>
                            <td>Wed, 8th May 2019 <br>12:16am</td>
                            <td style="text-align: right"><span style="font-family: 'robotobold';color: #444">Robyn G.</span> <br>Mon, 13th May 2019 <br>11:29am</td>
                        </tr>
                        <tr role="row" class="even">
                            <td>INV201910074F89BE</td>
                            <td>Project fee adjust request for <br>Project ID: 17478418</td>
                            <td>General</td>
                            <td>Wed, 8th May 2019 <br>12:16am</td>
                            <td style="text-align: right"><span style="font-family: 'robotobold';color: #444">Robyn G.</span> <br>Mon, 13th May 2019 <br>11:29am</td>
                        </tr>
                        <tr role="row" class="odd">
                            <td>INV201910074F89BE</td>
                            <td>Project fee adjust request for <br>Project ID: 17478418</td>
                            <td>General</td>
                            <td>Wed, 8th May 2019 <br>12:16am</td>
                            <td style="text-align: right"><span style="font-family: 'robotobold';color: #444">Robyn G.</span> <br>Mon, 13th May 2019 <br>11:29am</td>
                        </tr>
                        <tr role="row" class="even">
                            <td>INV201910074F89BE</td>
                            <td>Project fee adjust request for <br>Project ID: 17478418</td>
                            <td>General</td>
                            <td>Wed, 8th May 2019 <br>12:16am</td>
                            <td style="text-align: right"><span style="font-family: 'robotobold';color: #444">Robyn G.</span> <br>Mon, 13th May 2019 <br>11:29am</td>
                        </tr>
                    </tbody>
                  </table>
               </div>
            </div>
          <!-- Paination Links -->
         @include('front.common.pagination_view', ['paginator' => $arr_transactions])
         <!-- Paination Links -->
         </div>
      </div>
   </div>
<!-- </div> -->
@stop

