@extends('client.layout.master')
@section('main_content')
<style type="text/css">
.msg-btn-auto-height {padding: 4px 5px; font-size:13px;}
/*file input css start here*/
.btn-file input[type=file] {position: absolute;top: 0;right: 0;min-width: 100%;min-height: 100%;text-align: right;opacity: 0;background: none repeat scroll 0 0 transparent;cursor: inherit;display: block;}
.btn.btn-primary.btn-file {background: #2d2d2d;border-color: #2d2d2d;border-radius: 0 ;color: #fff;display: block;height: 40px;float: right;padding-top: 7px;max-width: 92px; width: 100%;position: absolute;right: 0;top: 0;border-radius: 0 3px 3px 0;z-index: 1}
.user-box .input-group {width: 100%;border-radius: 3px;position: relative;border: none}
.user-box .file { color: #fff;opacity: 1;}
/*file input css end here*/
    .document-row-seciton{font-family: 'robotolight'; color:#686868;line-height: 21px;font-size: 13px;margin-top: 5px;}
    .document-row-seciton a{color:#686868;}
</style>
<?php $arr_threads = $obj_threads->toArray(); ?>
<div class="col-sm-7 col-md-8 col-lg-9">
    <div class="search-grey-bx white-wrapper">
        <div class="head_grn">Ticket reference No: {{ $obj_ticket->ticket_number or '' }}</div>
        <div class="subject-section-page">
            Subject: {{ $obj_ticket->title or '' }}
        </div>
        <div class="department-name-section">
            <span>Department:</span> {{ isset($obj_ticket->get_category) && isset($obj_ticket->get_category->title) ? $obj_ticket->get_category->title : '' }}
        </div>

        @if(isset($obj_ticket->get_files) && !empty($obj_ticket->get_files))
            <div class="download-document-section">
                @foreach($obj_ticket->get_files as $file)
                <div class="document-row-seciton">
                    <a href="{{ $support_ticket_path.$file['file'] }}" download=""><i class="fa fa-download"></i></a>
                    {{ $file['file'] or '' }}
                </div>
                @endforeach
            </div>
        @endif
        
        @if(isset($obj_ticket->status) && ($obj_ticket->status == 'in_progress' || $obj_ticket->status == 're_opened'))
        <div class="rating-white-block">
            <div class="add-review-block">
                Add Replay
            </div>
            <form class="profile-page-form" method="post" action="{{ $module_url_path.'/submit_reply/'.base64_encode($obj_ticket->id) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="user-box">
                    <div class="input-name">
                        <textarea class="client-taxtarea valid wysihtml5" name="reply_msg" style="padding: 5px;" rows="5" cols=""></textarea>
                    </div>
                </div>
                <div class="user-box" style="display: inline-block;max-width: 400px;width: 100%;">                        
                    <div class="input-name">
                        <div class="upload-block">
                            <label>Attachment</label>
                            <input type="file" id="pdffile" style="visibility:hidden; height: 0;" name="upload_file[]" multiple="">
                            <div class="input-group ">
                                <input type="text" class="clint-input file-caption kv-fileinput-caption" placeholder="Add Attachment" id="subfile" disabled style="cursor: not-allowed" />
                                <div class="btn btn-primary btn-file"><a class="file" onclick="$('#pdffile').click();">Browse...</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="user-box" style="display: inline-block;float: right;margin-top: 32px;width: 130px;">
                    <button id="cupdate" class="normal-btn pull-left" type="submit">{{ trans('common/common.submit') }}</button>
                </div>
                <div class="clr"></div>
            </form>
        </div>
        @endif
        <div class="messages-main-section">
            @if(isset($arr_threads) && isset($arr_threads['data']) && !empty($arr_threads['data']))
            @foreach($arr_threads['data'] as $row)
            @if(!empty($row['get_user_details']))
            <div class="rating-white-block gray-color">
                {{-- <div class="review-profile-image">
                    @if($row['get_user_details']['role_info']['profile_image'] != '')
                    <img src="{{ $user_img_path.$row['get_user_details']['role_info']['profile_image'] }}" alt="" />
                    @else
                    <img src="{{ $user_img_path.'default_profile_image.png' }}" alt="" />
                    @endif
                </div> --}}
                    <b style="color: black;">{{ isset($row['get_user_details']['role_info']['first_name'])?$row['get_user_details']['role_info']['first_name']:'' }}  {{ substr($row['get_user_details']['role_info']['last_name'], 0,1) }}</b>
                <div class="review-content-block">
                    {{-- <div class="review-send-head">
                        Phasellus quis lectus metus
                    </div> --}}
                    <div class="agent-name-section pull-left">
                        {{ time_elapsed_string($row['created_at']) }}
                    </div>
                    <div class="agent-name-section pull-right">
                        {{-- Agent Name: {{ $row['get_user_details']['role_info']['first_name'] }} 
                                    {{ $row['get_user_details']['role_info']['last_name'] }} --}}
                    </div>
                    <div class="clearfix"></div>
                    <div class="review-rating-message">
                        {!! $row['description'] !!} 
                    </div>
                    @if(isset($row['get_files']) && !empty($row['get_files']))
                    <div class="download-document-section">
                        @foreach($row['get_files'] as $file)
                        <div class="document-row-seciton">
                            <a href="{{ $support_ticket_path.$file['file'] }}" download=""><i class="fa fa-download"></i></a>
                            {{ $file['file'] or '' }}
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
            @elseif(!empty($row['get_admin_details']))
            <div class="rating-white-block">
                {{-- <div class="review-profile-image">
                    @if($row['get_admin_details']['role_info']['image'] != '')
                    <img src="{{$admin_img_path.$row['get_admin_details']['role_info']['image']}}" alt="" />
                    @else
                    <img src="{{ $user_img_path.'default_profile_image.png' }}" alt="" />
                    @endif
                    {{ substr($row['get_admin_details']['role_info']['name'], 0,1) }}
                </div> --}}
                    <b style="color: black;"> {{ isset($row['get_admin_details']['role_info']['name'])?$row['get_admin_details']['role_info']['name']:'' }}</b>
                <div class="review-content-block">
                    {{-- <div class="review-send-head">
                        Phasellus quis lectus metus
                    </div> --}}
                    <div class="agent-name-section pull-left">
                        {{ time_elapsed_string($row['created_at']) }}
                    </div>
                    <div class="agent-name-section pull-right">
                        Agent Name: {{ $row['get_admin_details']['role_info']['name'] }}
                    </div>
                    <div class="clearfix"></div>
                    <div class="review-rating-message">
                        {!! $row['description'] !!}
                    </div>
                    @if(isset($row['get_files']) && !empty($row['get_files']))
                    <div class="download-document-section">
                        @foreach($row['get_files'] as $file)
                        <div class="document-row-seciton">
                            <a href="{{ $support_ticket_path.$file['file'] }}" download=""><i class="fa fa-download"></i></a>
                            {{ $file['file'] or '' }}
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
            @endif
            @endforeach
            @endif

            {{-- Paginate --}}
            @if(isset($obj_threads))
            <div class="product-pagi-block">
                {{ $obj_threads->links() }}
            </div>
            @endif
            {{-- Paginate --}}

            <div class="rating-white-block">
                {{-- <div class="review-profile-image">
                    <img src="{{url('/public')}}/front/images/admin.png" alt="" />
                </div> --}}
                <div class="review-content-block">
                    {{-- <div class="review-send-head">
                        Agent Robyn G. marked this ticket: <span>Resolved</span>
                    </div> --}}
                    @if(isset($obj_ticket->status) && $obj_ticket->status == 'closed')
                    <div class="reopen-ticket-box-section">
                        <div class="ticket-closed-head">
                            This ticket is now closed
                        </div>
                        <div class="closeing-time-section">
                            This ticket was closed {{ time_elapsed_string($obj_ticket->updated_at) }} ( {{ date('M d Y', strtotime($obj_ticket->updated_at)) }} ).
                        </div>
                        <div class="open-new-ticket-link-section">
                            You can <a href="{{ $module_url_path }}">return to the knowledge base</a> or <a href="{{ $module_url_path.'/create' }}">open a new ticket.</a>
                        </div>
                        <div class="reopen-ticket-btn-section">
                            <a class="btn-reopen-ticket" href="{{ $module_url_path.'/reopen_ticket/'.base64_encode($obj_ticket->id) }}">Re-open Ticket</a>
                        </div>
                    </div>
                    @endif
                    {{-- @if(isset($obj_ticket->status) && ($obj_ticket->status == 'in_progress' || $obj_ticket->status == 're_opened'))
                    <div class="reopen-ticket-box-section">
                        <div class="ticket-closed-head">
                            Do you want to resolve this ticket? 
                        </div>
                        <div class="closeing-time-section">
                            This ticket was opened {{ time_elapsed_string($obj_ticket->created_at) }} ( {{ date('M d Y', strtotime($obj_ticket->created_at)) }} ).
                        </div>
                        <div class="open-new-ticket-link-section">
                            You can <a href="{{ $module_url_path }}">return to the knowledge base</a> or <a href="{{ $module_url_path.'/create' }}">open a new ticket.</a>
                        </div>
                        <div class="reopen-ticket-btn-section">
                            <a class="btn-reopen-ticket" href="{{ $module_url_path.'/resolve_ticket/'.base64_encode($obj_ticket->id) }}">Resolve Ticket</a>
                        </div>
                    </div>
                    @endif --}}
                </div>
            </div>

        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
<script type="text/javascript" src="{{url('/public')}}/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
<script type="text/javascript" src="{{url('/public')}}/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        /*$('.wysihtml5').wysihtml5({
            "image": false, //Button to insert an image.
        });*/

        $("#form_ticket").validate({
        });
    });
</script>
@stop