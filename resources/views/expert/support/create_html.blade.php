@extends('client.layout.master')
@section('main_content')
<style type="text/css">
.msg-btn-auto-height {padding: 4px 5px; font-size:13px;}    
/*file input css start here*/
.btn-file input[type=file] {position: absolute;top: 0;right: 0;min-width: 100%;min-height: 100%;text-align: right;opacity: 0;background: none repeat scroll 0 0 transparent;cursor: inherit;display: block;}
.btn.btn-primary.btn-file {background: #2d2d2d;border-color: #2d2d2d;border-radius: 0 ;color: #fff;display: block;height: 40px;float: right;padding-top: 7px;max-width: 92px; width: 100%;position: absolute;right: 0;top: 0;border-radius: 0 3px 3px 0;z-index: 1}
.user-box .input-group {width: 100%;border-radius: 3px;position: relative;border: none}
.user-box .file { color: #fff;opacity: 1}
/*file input css end here*/
</style>
<!-- <div class="middle-container"> -->
  <div class="container">
  <!-- <br/> -->
     <div class="row">
         <div class="col-sm-7 col-md-8 col-lg-9">
            <div class="search-grey-bx white-wrapper">
                <div class="head_grn">Add Support Ticket</div>                    
                <div class="user-box">
                    <div class="p-control-label">Department <span>*</span></div>
                    <div class="input-name">
                        <div class="droup-select">
                            <select class="droup" data-rule-required="true" name="country">
                                <option value="">General</option>
                                <option value="">General</option>
                           </select>
                           <span class='error'></span>
                        </div>
                     </div>
                </div>                
                <div class="user-box">
                    <div class="p-control-label">Subject <span>*</span></div>
                    <div class="input-name">
                        <input type="text" class="clint-input" placeholder="" name="first_name" value="" data-rule-required="true" data-rule-maxlength="250"/>
                    </div>
                </div>                   
                <div class="user-box">
                    <div class="p-control-label">Message <span>*</span></div>
                    <div class="input-name"><textarea style="padding: 5px;" rows="" cols="" type="text" class="client-taxtarea" placeholder="" name="profile_summary" data-rule-maxlength="500"></textarea> </div>
                </div>   
                <div class="user-box">
                    <div class="p-control-label">Subject <span>*</span></div>
                    <div class="input-name">
                        <div class="upload-block">
                            <input type="file" id="pdffile" style="visibility:hidden; height: 0;" name="file">
                            <div class="input-group ">
                                <input type="text" class="clint-input file-caption  kv-fileinput-caption" placeholder="" id="subfile" />
                                <div class="btn btn-primary btn-file"><a class="file" onclick="$('#pdffile').click();">Browse...</a></div>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
                      <!-- Paination Links -->
         @include('front.common.pagination_view', ['paginator' => $arr_transactions])
         <!-- Paination Links -->
         </div>
      </div>
   </div>
<!-- </div> -->
@stop

