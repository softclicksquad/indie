@extends('client.layout.master')
@section('main_content')
<style type="text/css">
.msg-btn-auto-height {padding: 4px 5px; font-size:13px;}    
/*file input css start here*/
.btn-file input[type=file] {position: absolute;top: 0;right: 0;min-width: 100%;min-height: 100%;text-align: right;opacity: 0;background: none repeat scroll 0 0 transparent;cursor: inherit;display: block;}
.btn.btn-primary.btn-file {background: #2d2d2d;border-color: #2d2d2d;border-radius: 0 ;color: #fff;display: block;height: 40px;float: right;padding-top: 7px;max-width: 92px; width: 100%;position: absolute;right: 0;top: 0;border-radius: 0 3px 3px 0;z-index: 1}
.user-box .input-group {width: 100%;border-radius: 3px;position: relative;border: none}
.user-box .file { color: #fff;opacity: 1}
/*file input css end here*/
</style>
<link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
<!-- <div class="middle-container"> -->
<div class="container">
    <div class="row">
        <div class="col-sm-7 col-md-8 col-lg-9">
            <div class="search-grey-bx white-wrapper">
                <form method="POST" enctype="multipart/form-data" action="{{ $module_url_path.'/store' }}" id="form_ticket">
                    {{ csrf_field() }}
                    <div class="head_grn">{{ trans('common/support.add_support_ticket') }}</div>                    
                    <div class="user-box">
                        <div class="p-control-label">{{ trans('common/support.department') }}<span>*</span></div>
                        <div class="input-name">
                            <div class="droup-select">
                                <select class="droup" data-rule-required="true" name="category">
                                    <option value=""> -- {{ trans('common/support.select_department_category') }} -- </option>
                                    @if(isset($arr_categories) && !empty($arr_categories))
                                    @foreach($arr_categories as $row)
                                    <option value="{{ $row['id'] or '' }}"> {{ $row['title'] or '' }} </option>
                                    @endforeach
                                    @endif
                                </select>
                                <span class='error'></span>
                            </div>
                        </div>
                    </div>
                    <div class="user-box">
                        <div class="p-control-label">{{ trans('common/support.priority') }}<span>*</span></div>
                        <div class="input-name">
                            <div class="droup-select">
                                <select class="droup" data-rule-required="true" name="priority">
                                    <option value=""> -- {{ trans('common/support.select_priority') }} -- </option>
                                    <option value="urgent"> {{ trans('common/support.urgent') }} </option>
                                    <option value="normal"> {{ trans('common/support.normal') }} </option>
                                </select>
                                <span class='error'>{{ $errors->first('priority') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="user-box">
                        <div class="p-control-label">{{ trans('common/common.subject') }} <span>*</span></div>
                        <div class="input-name">
                            <input type="text" class="clint-input" name="subject" value="" data-rule-required="true" data-rule-maxlength="250"/>
                        </div>
                    </div>
                    <div class="user-box">
                        <div class="p-control-label">{{ trans('common/common.message') }} <span>*</span></div>
                        <div class="input-name">
                            <textarea class="client-taxtarea wysihtml5" name="message" data-rule-required="true" data-rule-maxlength="500" style="padding: 5px;" rows="5" cols="" ></textarea>
                        </div>
                    </div>
                    <div class="user-box">
                        <div class="p-control-label">{{ trans('common/common.attachment') }} </div>
                        <div class="input-name">
                            <div class="upload-block">
                                <input type="file" id="pdffile" style="visibility:hidden; height: 0;" name="attachment[]" multiple="">
                                <div class="input-group ">
                                    <input type="text" class="clint-input file-caption kv-fileinput-caption" id="subfile" />
                                    <div class="btn btn-primary btn-file"><a class="file" onclick="$('#pdffile').click();">{{ trans('common/common.browse') }}...</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="user-box">
                        <button id="cupdate" class="normal-btn pull-right" type="submit">{{ trans('common/common.submit') }}</button>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
            <!-- Paination Links -->
            @include('front.common.pagination_view', ['paginator' => $arr_transactions])
            <!-- Paination Links -->
        </div>
    </div>
</div>
<!-- </div> -->

<script type="text/javascript" src="{{url('/public')}}/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
<script type="text/javascript" src="{{url('/public')}}/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        /*$('.wysihtml5').wysihtml5({
            "image": false, //Button to insert an image.
        });*/

        $("#form_ticket").validate({
        });
    });
</script>
@stop