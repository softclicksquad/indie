@extends('expert.layout.master')
@section('main_content')
<style type="text/css">
.moretext{
    color: rgb(0, 0, 0);
    text-decoration: none;
    text-transform: capitalize;
}
</style>
<div class="col-sm-7 col-md-8 col-lg-9">
  <div class="row">
    <div class="col-lg-12">
       @include('front.layout._operation_status')
       <div class="head_grn">{{trans('client/contest/common.text_applied_contests')}}</div>
    </div>
    @if(isset($arr_open_contests['data']) && sizeof($arr_open_contests['data'])>0)
    @foreach($arr_open_contests['data'] as $contestRec)
    <div class="col-lg-12">
        <div class="white-block-bg-no-padding">
            <div class="white-block-bg big-space hover">
                <div class="project-left-side">
                    <div class="search-freelancer-user-head">
                        <a href="{{ $module_url_path }}/show_contest_entry_details/{{ base64_encode($contestRec['id'])}}"> {{$contestRec['contest_details']['contest_title'] or '-'}} </a>
                    </div>
                    @php $postes_time_ago = time_ago($contestRec['contest_details']['created_at']); @endphp
                    <div class="search-freelancer-user-location">
                        <span class="gavel-icon"><img src="{{url('/public')}}/front/images/clock.png" alt="" /></span>
                        <span class="dur-txt">
                        {{ trans('contets_listing/listing.text_posted') }} : {{$postes_time_ago}}</span></div>
                    <div class="clearfix"></div>
                    <div class="more-project-dec">{{str_limit($contestRec['contest_details']['contest_description'],100)}} 
                         @if(strlen(trim($contestRec['contest_details']['contest_description'])) > 100)
                         <a class="moretext" href="#">
                         {{trans('project_manager/projects/postedprojects.text_more')}}
                         </a>
                         @endif
                    </div>
                    
                      <div class="skils-project">
                          @if(isset($proRec['project_details']['project_skills'])  && count($proRec['project_details']['project_skills']) > 0)
                          <img src="{{url('/public')}}/front/images/tag.png" alt="" />
                          @endif
                        <ul>
                           @if(isset($proRec['project_details']['project_skills'])  && count($proRec['project_details']['project_skills']) > 0)
                           @foreach($proRec['project_details']['project_skills'] as $key=>$skills)
                           @if(isset($skills['skill_data']['skill_name']) &&  $skills['skill_data']['skill_name'] != "")
                           <li>{{ str_limit($skills['skill_data']['skill_name'],25) }}
                           </li>
                           @endif
                           @endforeach
                           @endif    
                        </ul>
                     </div>
                    
                    <div class="skils-project">
                                @if(isset($contestRec['contest_details']['contest_skills'])  && count($contestRec['contest_details']['contest_skills']) > 0)
                                 <img src="{{url('/public')}}/front/images/tag.png" alt="" />
                                 @endif
                                <ul>
                                    @if(isset($contestRec['contest_details']['contest_skills'])  && count($contestRec['contest_details']['contest_skills']) > 0)
                                    @foreach($contestRec['contest_details']['contest_skills'] as $key=> $skills)
                                    @if(isset($skills['skill_data']['skill_name']) &&  $skills['skill_data']['skill_name'] != "")
                                    <li>{{ str_limit($skills['skill_data']['skill_name'],18) }}</li>
                                    @endif
                                    @endforeach
                                    @endif
                                </ul>
                            </div>

                    
                </div>
                <div class="search-project-listing-right contest-right">
                    <div class="search-project-right-price">
                        {{-- {{ dd($contestRec['contest_details']['is_active']) }} --}}
                        {{isset($contestRec['contest_details']['contest_currency'])?$contestRec['contest_details']['contest_currency']:'$'}} {{isset($contestRec['contest_details']['contest_price'])?$contestRec['contest_details']['contest_price']:'0'}}
                        {{-- <span>{{trans('client/contest/post.text_prize')}}</span> --}}
                    </div>
                    <div class="search-project-right-price">
                        @if($contestRec['contest_details']['winner_choose'] == 'YES')
                            <span><img style="cursor: default;height: 40px;" src="{{url('/public')}}/front/images/archexpertdefault/contest-winner.png"></span>   
                        @else
                            @if($contestRec['contest_details']['is_active'] == '2')
                                <span style="font-size: 16px;font-weight: bold; color: red;">{{trans('common/common.closed')}}</span>
                            @else
                            <span id='days_left{{$contestRec['contest_details']["id"]}}' style="margin-top:0px;">{{trans('client/projects/ongoing_projects.text_next_expire')}} </span><span>{{trans('client/contest/posted.text_time_left')}}</span>
                            @endif
                            <?php 
                                $arr_diff_between_two_date = get_array_diff_between_two_date(date('Y-m-d H:i:s'),$contestRec['contest_details']['contest_end_date']);
                                $str_days_diff = isset($arr_diff_between_two_date['str_days_diff']) ? $arr_diff_between_two_date['str_days_diff'] : '';
                            ?>
                            <script type="text/javascript">

                                var status      = "{{ $contestRec['contest_details']['is_active'] or '' }}";
                                var contestid   = '<?php echo $contestRec['contest_details']["id"]; ?>';
                                var str_days_diff = '{{ isset($str_days_diff) ? $str_days_diff : '' }}';
                                

                                if(str_days_diff == ""){
                                  $('#days_left'+contestid).html('{{trans('client/projects/ongoing_projects.text_next_expire')}}');
                                  $('#days_left'+contestid).css('color','red');
                                } else {
                                  $('#days_left'+contestid).html(str_days_diff);
                                  $('#days_left'+contestid).css('font-size','16px');
                                  $('#days_left'+contestid).css('color','green');
                                  $('#days_left'+contestid).css('font-weight','bold');
                                }
                            </script> 
                        @endif    
                    </div>
                    <div class="clearfix"></div>
                    <div class="invite-expert-btns" style="padding-top: 21px;">
                          <a class="black-border-btn btn-right-mrg"href="{{url('/expert/contest/show_contest_entry_details/'.base64_encode($contestRec['id']))}}">{{ trans('contets_listing/listing.text_entry_details') }}</a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    @endforeach
    @include('front.common.pagination_view', ['paginator' => $arr_pagination]) 
    @else
    <div class="col-lg-12">
        <div class="search-grey-bx">
            <div class="no-record" >
               {{trans('expert/projects/awarded_projects.text_sorry_no_record_found')}}
            </div>
        </div>
    </div>
    @endif
  </div>
</div>  
@stop