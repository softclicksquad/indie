@extends('expert.layout.master') @section('main_content')
<style type="text/css">
.moretext{
    color: rgb(0, 0, 0);
    text-decoration: none;
    text-transform: capitalize;
}
</style>

<div class="">
    <div class="coll-listing-section">
        <div class="contest-titile">
            <div class="sm-logo d-inlin"><img src="{{url('/public/front/images/small-logo.png')}}" class="img-responsive"></div>
            <span class="d-inlin">Contest</span>
        </div>
        <h2>{{isset($arr_contest_details['contest_title'])?$arr_contest_details['contest_title']:""}}</h2>
       @if(isset($arr_contest_entry_data['data']) && sizeof($arr_contest_entry_data['data'])>0)
   <div class="contest-bottom-list">
   <div class="row">  
          @php  $i = 1; @endphp
          @foreach($arr_contest_entry_data['data'] as $key=>$entry)
          @php
          $sidebar_information      = sidebar_information($entry['expert_id']);
          $profile_img_public_path  = url('/public').config('app.project.img_path.profile_image');
          $profile_img_base_path    = base_path().'/public'.config('app.project.img_path.profile_image');
          $timeline_img_public_path = url('/public').config('app.project.img_path.cover_image');
          $timeline_img_base_path   = base_path().'/public'.config('app.project.img_path.cover_image');
          @endphp

          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 coll-list">
         
              <div class="coll-list-block">
                  <div class="coll-img p-relative">
                        @if(isset($sidebar_information['user_details']['cover_image']) && $sidebar_information['user_details']['cover_image']!=null && file_exists($timeline_img_base_path.$sidebar_information['user_details']['cover_image']))
                         <img src="{{$timeline_img_public_path.$sidebar_information['user_details']['cover_image']}}" class="img-responsive" alt=""/>
                        @else
                          <img src="{{$timeline_img_public_path.'/resize_cache/default-371x250.jpg'}}" class="img-responsive" alt=""/>
                        @endif
                     
                       <div class="design-title">
                          <!--<p>{{isset($entry['title'])?str_limit($entry['title'],'30','...'):""}}</p>-->
                          <div class="id-block">ID {{isset($entry['entry_number'])?'#'.$entry['entry_number']:"-"}}</div>
                          <div class="avg-rating">
                                <span>{{$sidebar_information['average_rating']}}</span>
                                <div class="avg" style="margin-left:0px;"><i class="fa fa-star"></i></div>
                                <!--<ul>
                                    <span class="stars">{{$sidebar_information['average_rating']}}</span>
                                </ul>-->
                           </div>
                      </div>
                  <div class="list-hover">
                    {{-- @if(isset($user) && $user != false && $user->inRole('user'))  --}}
                      <a href="{{$module_url_path}}/show_contest_entry_details/{{base64_encode($entry['id'])}}" class="view-button">View</a>
                    {{-- @else
                      <a href="javascript:void(0)" class="view-button">View</a>  
                    @endif     --}}
                  </div>
                  </div>
                  <div class="coll-details">
                          <div class="coll-person">
                              <div class="collaboration-img">
                                  @if(isset($sidebar_information['user_details']['profile_image']) && $sidebar_information['user_details']['profile_image']!=null && file_exists($profile_img_base_path.$sidebar_information['user_details']['profile_image']))
                                   <img src="{{$profile_img_public_path.$sidebar_information['user_details']['profile_image']}}" class="img-responsive" alt=""/>
                                  @else
                                    <img src="{{$profile_img_public_path.'default_profile_image.png'}}" class="img-responsive" alt=""/>
                                  @endif

                                  @if(isset($sidebar_information['last_login_duration']) && $sidebar_information['last_login_duration']=='Active')
                                    <div class="online-status"> <img src="{{url('/public/front/images/active.png')}}"   class=""></div>
                                  @else
                                    <div class="online-status"> <img src="{{url('/public/front/images/deactive.png')}}" class=""></div>
                                  @endif
                                  
                              </div>
                              <div class="coll-name">
                                  <h5>{{isset($sidebar_information['user_details']['first_name'])?$sidebar_information['user_details']['first_name'].'  '.str_limit($sidebar_information['user_details']['last_name'],1,'.'):""}}</h5>
                                  @if($sidebar_information['last_login_duration'] == 'Active')
                                         <span class="flag" title="{{isset($sidebar_information['expert_timezone'])?$sidebar_information['expert_timezone']:'-'}}">
                                            <span class="flag-image"> 
                                                @if(isset($sidebar_information['user_country_flag'])) 
                                                  @php 
                                                     echo $sidebar_information['user_country_flag']; 
                                                  @endphp 
                                                @elseif(isset($sidebar_information['user_country'])) 
                                                  @php 
                                                    echo $sidebar_information['user_country']; 
                                                  @endphp 
                                                @else 
                                                  @php 
                                                    echo '-'; 
                                                  @endphp 
                                                @endif 
                                            </span>
                                            {{-- trans('common/_expert_details.text_local_time') --}}  {{isset($sidebar_information['expert_timezone_without_date'])?$sidebar_information['expert_timezone_without_date']:'-'}}
                                         </span>
                                      @else
                                         <span class="" title="{{$sidebar_information['last_login_full_duration']}}">
                                            <span class="flag-image"> 
                                                @if(isset($sidebar_information['user_country_flag'])) 
                                                  @php 
                                                     echo $sidebar_information['user_country_flag']; 
                                                  @endphp 
                                                @elseif(isset($sidebar_information['user_country'])) 
                                                  @php 
                                                    echo $sidebar_information['user_country']; 
                                                  @endphp 
                                                @else 
                                                  @php 
                                                    echo '-'; 
                                                  @endphp 
                                                @endif 
                                            </span>
                                            <!-- <i  class="fa fa-clock-o" aria-hidden="true"></i> -->
                                            <a style="color:#4D4D4D;font-size: 14px;text-transform:none;cursor:text;">
                                               {{$sidebar_information['last_login_duration']}}
                                            </a>
                                         </span>
                                      @endif
                              </div>
                          </div>
                          <div class="clearfix"></div>
                          <!--<h6>Entry #{{$i++}}</h6>-->
                          <!--<div class="like-block">
                              <span>(23)</span>
                              <span class="like-icon"><i class="fa fa-thumbs-up"></i></span>
                          </div>-->
                      </div>
                 </div>
            </div>

            <!-- Sealed entry block -->
            <!-- <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 coll-list">
              <div class="coll-list-block sealed-block">
                  <div class="coll-img p-relative">
                      <img src="{{url('/public/front/images/listing-img_14.jpg')}}" class="img-responsive" alt=""/>
                      <div class="design-title">
                           <div class="id-block">ID #110587</div>
                       <div class="avg-rating">
                            <span>0.0</span>
                            <div class="avg" style="margin-left:0px;"><i class="fa fa-star"></i></div>
                       </div>
                      </div>
                      <div class="list-hover sealed-block">
                           <a href="javascript:void(0)" class="view-button">Sealed</a>  
                      </div>
                  </div>
                  <div class="coll-details">
                      <div class="coll-person">
                          <div class="collaboration-img">
                              <img src="{{$profile_img_public_path.'default_profile_image.png'}}" class="img-responsive" alt=""/>
                          </div>
                          <div class="coll-name">
                              <h5>Tushar Ahire</h5>
                              <div class="flag-block">
                                  <img src="{{url('/public/front/images/us-flag.png')}}"><span>Paris</span>
                              </div>
                          </div>
                      </div>
                      <div class="clearfix"></div>
                </div>
                </div>
            </div> -->
          @endforeach
      </div>
   </div>
  </div>
  @endif
   
    </div>
</div>
<script type="text/javascript">
    function confirmDelete(ref) {
      alertify.confirm("Are you sure? You want to cancel contest ?", function (e) {
          if (e) {
              var contest_id = $(ref).attr('data-contest-id');
              window.location.href="{{ $module_url_path }}"+"/delete_contest/"+contest_id; 
              return true;
          } else {
              return false;
          }
      });
    }
</script>   
@stop