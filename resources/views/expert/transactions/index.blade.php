@extends('expert.layout.master')                
@section('main_content')
<style type="text/css">
.msg-btn-auto-height
{  
   padding: 4px 5px; font-size:13px;
}
.table-expert-transaction
{
   font-size:11px;   
}

</style>
<div class="col-sm-7 col-md-8 col-lg-9">
   <div class="search-grey-bx white-wrapper">
      <div class="head_grn">{{ trans('expert/transactions/packs.text_heading') }}</div>
      <div class="table-responsive">
         <form id="frm_show_cnt" class="form-horizontal show-entry-form" method="POST" action="{{url('expert/transactions/show_cnt')}}">
             {{ csrf_field() }}
               <div class="col-sm-2 col-md-2 col-lg-2">
                  <div class="sort-by">
                       @php $show_cnt ='show_transaction_cnt';  @endphp
                       @include('front.common.show-cnt-selectbox')
                     </div>
               </div>
         </form>
         <table id="TaBle" class="theme-table invoice-table-s table table-expert-transaction" style="border: 1px solid rgb(239, 239, 239); margin-bottom:0;">
            <thead>
               <tr>
                  <th>{{ trans('expert/transactions/packs.text_invoice_id') }}</th>
                  <th>{{ trans('expert/transactions/packs.text_transaction_type') }}</th>
                  <th>{{ trans('expert/transactions/packs.text_payment_amount') }}</th>
                  <th>{{ trans('expert/transactions/packs.text_payment_method') }}</th>
                  <th>{{ trans('expert/transactions/packs.text_Payment_status') }}</th>
                  <th>{{ trans('expert/transactions/packs.text_Payment_date') }}</th>
                  <th>{{ trans('expert/transactions/packs.text_action') }}</th>
               </tr>
            </thead>
            <tbody>
               @if(isset($arr_transactions['data']) && sizeof($arr_transactions['data'])>0)
               @foreach($arr_transactions['data'] as $transaction)
               <tr>
                  <td>{{isset($transaction['invoice_id'])?$transaction['invoice_id']:''}}</td>
                  <td>
                     @if(isset($transaction['transaction_type']) && $transaction['transaction_type']=='1')
                     {{ trans('new_translations.subscription')}}
                     @elseif(isset($transaction['transaction_type']) && $transaction['transaction_type']=='2')
                     {{ trans('new_translations.milestone')}}
                     @elseif(isset($transaction['transaction_type']) && $transaction['transaction_type']=='3')
                     {{ trans('new_translations.release_milestones')}}
                     @elseif(isset($transaction['transaction_type']) && $transaction['transaction_type']=='4')
                     {{ trans('new_translations.topup_bid')}}
                     @endif
                  </td>
                  <td>{{isset($transaction['currency_code'])?$transaction['currency_code']:''}}&nbsp;{{isset($transaction['paymen_amount'])?number_format($transaction['paymen_amount'],2):'0' }}
                  </td>
                  <td>
                     @if(isset($transaction['payment_method']) && $transaction['payment_method']=='1')
                        {{ trans('new_translations.paypal')}}
                     @endif
                     @if(isset($transaction['payment_method']) && $transaction['payment_method']=='2') 
                       {{ trans('new_translations.stripe')}}
                     @endif
                     @if(isset($transaction['payment_method'])&& $transaction['payment_method']=='0') 
                        {{ trans('new_translations.free')}}
                     @endif
                  </td>
                  <td>
                     @if(isset($transaction['payment_status']) && $transaction['payment_status']=='0'){{ trans('new_translations.fail')}} @endif
                     @if(isset($transaction['payment_status']) && $transaction['payment_status']=='1'){{ trans('new_translations.paid')}} @endif
                     @if(isset($transaction['payment_status']) && $transaction['payment_status']=='2'){{ trans('new_translations.paid')}} @endif
                     @if(isset($transaction['payment_status']) && $transaction['payment_status']=='3'){{ trans('new_translations.fail')}} @endif
                  </td>
                  <td>{{isset($transaction['payment_date'])&&$transaction['payment_date']!='0000-00-00 00:00:00'?date("d-M-Y", strtotime($transaction['payment_date'])):'--'}}</td>
                  <td>
                  <a href="{{url('/expert/transactions/show/'.base64_encode($transaction['id'])) }}">

                  <span class="msg_btn-lnew ht-tp msg-btn-auto-height"><i class="fa fa-eye"></i>
                     {{ trans('expert/transactions/packs.text_view')}}</span>
                     </a>
                  </td>
               </tr>
               @endforeach
               @else
               <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;" align="center">
                    <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                       <div class="search-content-block">
                          <div class="no-record">
                           {{ trans('expert/transactions/packs.text_sorry_no_records_found')}}                           
                        </div>
                     </div>
                  </td>
               </tr>
               @endif
            </tbody>
         </table>
      </div>
   </div>
   <!--pagigation start here-->
   @include('front.common.pagination_view', ['paginator' => $arr_transactions])
   <!--pagigation end here-->
</div>
</div>
</div>
</div>
@stop

