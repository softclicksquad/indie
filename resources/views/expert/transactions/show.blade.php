@extends('expert.layout.master')                
@section('main_content')
<div class="col-sm-7 col-md-8 col-lg-9">
   <div class="search-grey-bx">
      <div class="head_grn">{{ trans('expert/transactions/packs.text_heading_details') }}</div>
      @if(isset($transaction) && sizeof($transaction)>0)
      <div class="row">
         <div class="col-sm-12 col-md-6 col-lg-6 mrgt">
            <div class="row">
               <div class="form_group">
                  <div class="col-sm-12 col-md-8 col-lg-5">
                     <div class="card_holder">{{ trans('expert/transactions/packs.text_transaction_type') }} : </div>
                  </div>
                  <div class="col-sm-12 col-md-4 col-lg-7">
                     <div class="fill_input">  
                        @if(isset($transaction['transaction_type']) && $transaction['transaction_type']=='1'){{ trans('new_translations.subscription')}}
                        @elseif(isset($transaction['transaction_type']) && $transaction['transaction_type']=='2'){{ trans('new_translations.milestone')}}
                        @elseif(isset($transaction['transaction_type']) && $transaction['transaction_type']=='3'){{ trans('new_translations.release_milestones')}}
                        @elseif(isset($transaction['transaction_type']) && $transaction['transaction_type']=='4')
                         {{ trans('new_translations.topup_bid')}}
                        @endif
                     </div>
                  </div>
                  <div class="clr"></div>
               </div>
            </div>
            <div class="row">
               <div class="form_group">
                  <div class="col-sm-12 col-md-8 col-lg-5">
                     <div class="card_holder">{{ trans('expert/transactions/packs.text_invoice_id') }} : </div>
                  </div>
                  <div class="col-sm-12 col-md-4 col-lg-7">
                     <div class="fill_input">{{isset($transaction['invoice_id'])?$transaction['invoice_id']:''}}</div>
                  </div>
                  <div class="clr"></div>
               </div>
            </div>
            <div class="row">
               <div class="form_group">
                  <div class="col-sm-12 col-md-8 col-lg-5">
                     <div class="card_holder">{{ trans('expert/transactions/packs.text_payment_amount') }} : </div>
                  </div>
                  <div class="col-sm-12 col-md-4 col-lg-7">
                     <div class="fill_input">{{isset($transaction['currency'])?$transaction['currency']:''}}&nbsp;{{isset($transaction['paymen_amount'])?number_format($transaction['paymen_amount'],2):'0' }}</div>
                  </div>
                  <div class="clr"></div>
               </div>
            </div>
            <div class="row">
               <div class="form_group">
                  <div class="col-sm-12 col-md-8 col-lg-5">
                     <div class="card_holder">{{ trans('expert/transactions/packs.text_payment_method') }} : </div>
                  </div>
                  <div class="col-sm-12 col-md-4 col-lg-7">
                     <div class="fill_input"> 
                        @if(isset($transaction['payment_method']) && $transaction['payment_method']=='1') {{ trans('new_translations.paypal')}} @endif
                        @if(isset($transaction['payment_method']) && $transaction['payment_method']=='2') {{ trans('new_translations.stripe')}} @endif
                        @if(isset($transaction['payment_method'])&& $transaction['payment_method']=='0')  {{ trans('new_translations.free')}} @endif
                     </div>
                  </div>
                  <div class="clr"></div>
               </div>
            </div>
            <div class="row">
               <div class="form_group">
                  <div class="col-sm-12 col-md-8 col-lg-5">
                     <div class="card_holder">{{ trans('expert/transactions/packs.text_Payment_status') }} : </div>
                  </div>
                  <div class="col-sm-12 col-md-4 col-lg-7">
                     <div class="fill_input">   
                        @if(isset($transaction['payment_status']) && $transaction['payment_status']=='0'){{ trans('new_translations.fail')}} @endif
                        @if(isset($transaction['payment_status']) && $transaction['payment_status']=='1'){{ trans('new_translations.paid')}} @endif
                        @if(isset($transaction['payment_status']) && $transaction['payment_status']=='2'){{ trans('new_translations.paid')}} @endif
                        @if(isset($transaction['payment_status']) && $transaction['payment_status']=='3'){{ trans('new_translations.fail')}} @endif 
                     </div>
                  </div>
                  <div class="clr"></div>
               </div>
            </div>
         </div>
         <div class="col-sm-12 col-md-6 col-lg-6 mrgt">
            <div class="row">
               <div class="form_group">
                  <div class="col-sm-12 col-md-5 col-lg-5">
                     <div class="card_holder">{{trans('expert/transactions/packs.text_Payment_date') }} : </div>
                  </div>
                  <div class="col-sm-12 col-md-7 col-lg-7">
                     <div class="fill_input">{{isset($transaction['payment_date'])&&$transaction['payment_date']!='0000-00-00 00:00:00'?date("d-M-Y", strtotime($transaction['payment_date'])):'-'}}</div>
                  </div>
                  <div class="clr"></div>
               </div>
            </div>
         </div>
      </div>
      @endif
   </div>
</div>
</div>
</div>
</div>
@stop

