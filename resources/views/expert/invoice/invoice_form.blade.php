@extends('expert.layout.master')                
@section('main_content')
<div class="container">
   <div class="row">
      <div class="col-sm-12 col-md-8 col-lg-9">
         
        <div class="search-grey-bx">
           <div class="ongonig-project-section">
              <div class="dispute-head"><b>{{ trans('new_translations.invoice') }}: </b>#{{isset($arr_milestone['invoice_id'])?$arr_milestone['invoice_id']:""}}</div>

              @include('front.layout._operation_status')
              
              <div class="row">
                
              <div class="change-pwd-form">
                        <div class="col-sm-12 col-md-12 col-lg-8">
                           <div class="row">
                              <div class="form_group">
                                 <div class="col-sm-5 col-md-4 col-lg-4">
                                    <div class="card_holder">{{ trans('new_translations.from')}} : </div>
                                 </div>
                                 <div class="col-sm-7 col-md-8 col-lg-8">
                                    <div class="fill_input">{{isset($arr_milestone['expert_details']['role_info']['first_name'])?$arr_milestone['expert_details']['role_info']['first_name']:""}} {{isset($arr_milestone['expert_details']['role_info']['last_name'])?$arr_milestone['expert_details']['role_info']['last_name']:""}}
                                    </div> <div class="fill_input"> {{isset($arr_milestone['expert_details']['role_info']['address'])?$arr_milestone['expert_details']['role_info']['address']:""}} 
                                   {{isset($arr_milestone['expert_details']['role_info']['city_details']['city_name'])?$arr_milestone['expert_details']['role_info']['city_details']['city_name']:""}},
                                   {{isset($arr_milestone['expert_details']['role_info']['state_details']['state_name'])?$arr_milestone['expert_details']['role_info']['state_details']['state_name']:""}},
                                     {{isset($arr_milestone['expert_details']['role_info']['country_details']['country_name'])?$arr_milestone['expert_details']['role_info']['country_details']['country_name']:""}}</div>
                                 </div>
                                 <div class="clr"></div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="form_group">
                                 <div class="col-sm-5 col-md-4 col-lg-4">
                                    <div class="card_holder">{{ trans('new_translations.to')}} : </div>
                                 </div>
                                 <div class="col-sm-7 col-md-8 col-lg-8">
                                    <div class="fill_input">{{isset($arr_milestone['client_details']['role_info']['first_name'])?$arr_milestone['client_details']['role_info']['first_name']:""}} {{isset($arr_milestone['client_details']['role_info']['last_name'])?$arr_milestone['client_details']['role_info']['last_name']:""}}</div><div class="fill_input">
                                {{isset($arr_milestone['client_details']['role_info']['address'])?$arr_milestone['client_details']['role_info']['address']:""}}
                        {{isset($arr_milestone['client_details']['role_info']['city_details']['city_name'])?$arr_milestone['client_details']['role_info']['city_details']['city_name']:""}},
                        {{isset($arr_milestone['client_details']['role_info']['state_details']['state_name'])?$arr_milestone['client_details']['role_info']['state_details']['state_name']:""}},
                                     {{isset($arr_milestone['client_details']['role_info']['country_details']['country_name'])?$arr_milestone['client_details']['role_info']['country_details']['country_name']:""}}</div>
                                  </div>
                                 <div class="clr"></div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="form_group">
                                 <div class="col-sm-5 col-md-4 col-lg-4">
                                    <div class="card_holder">{{ trans('new_translations.project_name')}} : </div>
                                 </div>
                                 <div class="col-sm-7 col-md-8 col-lg-8">
                                    <div class="fill_input"> {{isset($arr_milestone['project_details']['project_name'])?$arr_milestone['project_details']['project_name']:""}}</div>
                                 </div>
                                 <div class="clr"></div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="form_group">
                                 <div class="col-sm-5 col-md-4 col-lg-4">
                                    <div class="card_holder">{{ trans('new_translations.date')}} : </div>
                                 </div>
                                 <div class="col-sm-7 col-md-8 col-lg-8">
                                    <div class="fill_input">{{date('d M Y')}}</div>
                                 </div>
                                 <div class="clr"></div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="form_group">
                                 <div class="col-sm-5 col-md-4 col-lg-4">
                                    <div class="card_holder">{{ trans('new_translations.milestone_name')}} :</div>
                                 </div>
                                 <div class="col-sm-7 col-md-8 col-lg-8">
                                    <div class="fill_input">{{isset($arr_milestone['milestone_details']['title'])?$arr_milestone['milestone_details']['title']:""}}</div>
                                 </div>
                                 <div class="clr"></div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="form_group">
                                 <div class="col-sm-5 col-md-4 col-lg-4">
                                    <div class="card_holder">{{ trans('new_translations.milestone_cost')}} :</div>
                                 </div>
                                 <div class="col-sm-7 col-md-8 col-lg-8">
                                    <div class="fill_input"> {{isset($arr_milestone['project_details']['project_currency'])?$arr_milestone['project_details']['project_currency']:""}}{{isset($arr_milestone['milestone_details']['cost'])?number_format($arr_milestone['milestone_details']['cost'],2):""}}
                                   ({{trans('new_translations.including_vat')}})</div>
                                 </div>
                                 <div class="clr"></div>
                              </div>
                           </div>
                        </div>
                     </div> 
                
                
                
                
                
                
                
                 <div class="col-sm-12 col-md-12 col-lg-12">
                   
                   

                  <form action="{{ $module_url_path }}/create_invoice/{{base64_encode($arr_milestone['milestone_id'])}}" method="post" id="frm-invoice-details" enctype="multipart/form-data" files ="true">
                    {{ csrf_field() }}
                     <div class="row">
                      <div class="col-lg-8">
                       <div class="row">
                              <div class="form_group">
                                 <div class="col-sm-5 col-md-4 col-lg-4">
                                    <div class="card_holder">{{ trans('new_translations.vat')}}(%) :</div>
                                 </div>
                                 <div class="col-sm-7 col-md-8 col-lg-8">
                                    <div class="fill_input"> <input class="clint-input" type="text" placeholder="{{ trans('new_translations.vat')}}" onkeyup="return chk_validation(this);" data-rule-min="1" data-rule-max="100" data-rule-number="true" name="vat_percentage" data-rule-required="true"></div>
                                 </div>
                                 <div class="clr"></div>
                              </div>
                           </div>
                      </div>
                      <div class="col-sm-12  col-md-6 col-lg-8">
                        <div class="user-box">
                        <div class="p-control-label">{{ trans('new_translations.logo')}}</div>
                           <div class="input-name">
                           <div class="p-control-label"></div>
                              <div class="upload-block">
                                 <input type="file" id="logo_image" style="visibility:hidden; height: 0;" name="logo_image">
                                 <div class="input-group ">
                                    <input type="text" class="form-control file-caption  kv-fileinput-caption" id="logo_image_name"  disabled="disabled" >
                                    <div class="btn btn-primary btn-file btn-gry">
                                       <a class="file" onclick="browseImage()">{{ trans('expert/profile/profile.text_browse') }}
                                       </a>
                                    </div>
                                    <div class="btn btn-primary btn-file remove" style="border-right:1px solid #fbfbfb !important;display:none;" id="btn_remove_image">
                                       <a class="" data-image-type="profile" onclick="removeBrowsedImage(this)"><i style="color: #ffffff;" class="fa fa-trash" aria-hidden="true"></i>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div id="msg_logo_image" style="color: red;font-size: 12px;font-weight:600;display: none;"></div>
                              <div class="user-box hidden-lg hidden-md hidden-sm hidden-xs">&nbsp;</div>
                              <div class="note_browse">{{ trans('expert/profile/profile.text_profile_image') }}</div>
                           </div>
                        </div>
                     </div>
                      </div>
                      
                      


                     
                     <span class='error'>{{ $errors->first('logo_image') }}</span>   

                      <br/>
                      <button type="submit" class="invoice-btn" style="max-width: 127px;">{{ trans('new_translations.create_invoice')}}</button>
                  </form>

                 </div>
              </div>
           </div>
        </div>
        
      </div>
   </div>
</div>

<script type="text/javascript">
  
  $('#frm-invoice-details').validate();   

  function chk_validation(ref)
  {
    var yourInput = $(ref).val();
    re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
    var isSplChar = re.test(yourInput);
    if(isSplChar)
    {
      var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
      $(ref).val(no_spl_char);
    }
  }


   $('#logo_image').change(function()
   {
     if($(this).val().length>0)
     {
       $("#btn_remove_image").show();
     }

     $('#logo_image_name').val($(this).val());
   });

   function browseImage()
 {
   
   $("#logo_image").trigger('click');

 }
  </script>
@stop



