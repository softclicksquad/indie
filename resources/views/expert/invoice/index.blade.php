@extends('expert.layout.master')                
@section('main_content')
<div class="container">
   <div class="row">
      <div class="col-sm-12 col-md-8 col-lg-9">
         <!-- Including top view of project details - Just pass project information array to the view -->
         {{-- @include('front.common._top_project_details',['projectInfo'=>$arr_project_info]) --}}
         <!-- Ends -->  
         
         
         <div class="search-grey-bx">
            <form action="{{ $module_url_path }}/store_bid_details" method="post" id="frm-bid-details" enctype="multipart/form-data">
               {{ csrf_field() }}
               <input type="hidden"  name="project_id" readonly="" value="{{ isset($arr_project_info['id']) ? base64_encode($arr_project_info['id']):''}}"/>  
               <div class="subm-text">{{ trans('new_translations.generate_invoice') }}</div>
               <div class="row">
                  <div class="col-sm-12 col-md-12 col-lg-12">
                     <div class="user-bx">
                        <div class="frm-nm">{{ trans('new_translations.from')}}<i style="color:red">&nbsp;*</i></div>
                        <textarea cols="" rows="" value="" name="invoice_from" data-rule-required="true" style="height:100px; padding:10px;" class="clint-input" placeholder="{{ trans('new_translations.from')}}"></textarea>
                        <span class="error">{{ $errors->first('invoice_from') }}</span>
                     </div>
                  </div>
                  <div class="col-sm-12 col-md-12 col-lg-12">
                     <div class="user-bx">
                        <div class="frm-nm">{{ trans('new_translations.to')}}<i style="color:red">&nbsp;*</i></div>
                        <textarea cols="" rows="" value="" name="invoice_to" data-rule-required="true" style="height:100px; padding:10px;" class="clint-input" 
                           placeholder="{{ trans('new_translations.to')}}" ></textarea>
                        <span class="error">{{ $errors->first('invoice_to') }}</span>
                     </div>
                  </div>
                   <div class="col-sm-12 col-md-12 col-lg-12">
                     <div class="user-bx new-users">
                           <div class="frm-nm">{{ trans('new_translations.title')}}<i style="color:red">&nbsp;*</i></div>
                           <input type="text" class="clint-input valid" value="" data-rule-required="true" name="title"  data-rule-min="1" placeholder="{{ trans('new_translations.title')}}" />
                           <span class="error">{{ $errors->first('title') }}</span>
                     </div>

                      <div class="user-bx new-users">
                        <div class="frm-nm">{{ trans('new_translations.cost')}}<i style="color:red">&nbsp;*</i>&nbsp;<i style="font-size: 11px;"></i></div>
                        <input type="text" name="expected_duration" placeholder="{{ trans('new_translations.cost')}}" value="" data-rule-required="true" data-rule-number="true" data-rule-min="0" onkeyup="return chk_validation(this);" class="clint-input valid"/>
                        <span class="error">{{ $errors->first('cost') }}</span>

                     </div>
                     <div id="btn_remove_image_cover" style="border-right:1px solid #fbfbfb !important;display:block;" class="btn btn-primary btn-file remove">
                     <a {{-- onclick="javascript:return addinvoice(this)"  --}} id="addinvoice" data-image-type="cover" class=""><i aria-hidden="true" class="fa fa-plus" style="color: #ffffff;"></i>
                     </a>
                    </div>
                  </div>
                  <div id="add_more"></div>


                  {{-- <div class="col-sm-4 col-md-4 col-lg-5">
                     <div class="user-bx">
                        <div class="frm-nm">Cost<i style="color:red">&nbsp;*</i>&nbsp;<i style="font-size: 11px;"></i></div>
                        <input type="text" name="expected_duration" placeholder="Cost" value="" data-rule-required="true" data-rule-number="true" data-rule-min="0" onkeyup="return chk_validation(this);" class="clint-input valid"/>
                        <span class="error">{{ $errors->first('expected_duration') }}</span>

                     </div>
                  </div> --}}
                  <div class="col-sm-4 col-md-4 col-lg-5"  style="float: right;">
                   <button type="submit" value="" class="det-sub-btn">{{ trans('expert/projects/add_project_bid.text_submit') }} </button>
                  </div>
               </div>
            </form>
         </div>
        
      </div>
   </div>
</div>
<!-- </div> -->
<script type="text/javascript">
   $('#frm-bid-details').validate();   
</script>
<script type="text/javascript">
  
  function chk_validation(ref)
  {
    var yourInput = $(ref).val();
    re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
    var isSplChar = re.test(yourInput);
    if(isSplChar)
    {
      var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
      $(ref).val(no_spl_char);
    }
  }

  $("#addinvoice").click(function(){
    html = '<div class="col-sm-12 col-md-12 col-lg-12">'+
                '<div class="user-bx new-users">'+
                           '{{--<div class="frm-nm">Title<i style="color:red">&nbsp;*</i></div>--}}'+
                           '<input type="text" class="clint-input valid" value="" data-rule-required="true" name="title"  data-rule-min="1" placeholder="{{ trans('new_translations.title')}}" />'+
                           '<span class="error">{{ $errors->first('title') }}</span>'+
                     '</div>'+
                     '<div class="user-bx new-users">'+
                        '{{--<div class="frm-nm">Cost<i style="color:red">&nbsp;*</i>&nbsp;<i style="font-size: 11px;"></i></div>--}}'+
                        '<input type="text" name="expected_duration" placeholder="Cost" value="" data-rule-required="true" data-rule-number="true" data-rule-min="0" onkeyup="return chk_validation(this);" class="clint-input valid"/>'+
                        '<span class="error">{{ $errors->first('cost') }}</span>'+

                     '</div>'+
                     '<div id="btn_remove_image_cover" style="border-right:1px solid #fbfbfb !important;display:block;" class="btn btn-primary btn-file remove">'+
                                 '<a onclick="javascript:return removeinvoice(this)" data-image-type="cover" class=""><i aria-hidden="true" class="fa fa-minus" style="color: #ffffff;"></i></a>'+
                    '</div>';
    $("#add_more").append(html);

  });
  /*$("#removeinvoice").click(function(){

  });*/
  function removeinvoice(ref)
  {

  }

   function removeBrowsedImage()
   {
     $('#bid_attachment_name').val("");
     $("#btn_remove_image").hide();
     $("#bid_attachment").val("");
   }
  
</script>
@stop