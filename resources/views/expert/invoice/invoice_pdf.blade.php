


<div>
<a href="{{url('/')}}">
<!--<img src="{{ url('/').'/front/images/virtual-logo.png' }}" alt="" width="150" height="40" /> -->
<img src="{{$temp_logo}}" alt=""/>
</a>
</div>

<font style="float: left;"><b>{{ trans('new_translations.invoice')}}: </b>#{{isset($arr_milestone['invoice_id'])?$arr_milestone['invoice_id']:""}}</font>
<br/>
              
              
<hr>
<table cellpadding="1px" cellspacing="1px">
                  
  <tr>
      <td style="width: 100px;">{{ trans('new_translations.from')}} :</td>
      <td>{{isset($arr_milestone['expert_details']['role_info']['first_name'])?$arr_milestone['expert_details']['role_info']['first_name']:""}} {{isset($arr_milestone['expert_details']['role_info']['last_name'])?$arr_milestone['expert_details']['role_info']['last_name']:""}}</td>

  </tr>  
  <tr>
      <td style="width: 100px;"></td>
      <td>{{isset($arr_milestone['expert_details']['role_info']['address'])?$arr_milestone['expert_details']['role_info']['address']:""}} 
      {{isset($arr_milestone['expert_details']['role_info']['city_details']['city_name'])?$arr_milestone['expert_details']['role_info']['city_details']['city_name']:""}},

      {{isset($arr_milestone['expert_details']['role_info']['state_details']['state_name'])?$arr_milestone['expert_details']['role_info']['state_details']['state_name']:""}},

      {{isset($arr_milestone['expert_details']['role_info']['country_details']['country_name'])?$arr_milestone['expert_details']['role_info']['country_details']['country_name']:""}}

      </td>
      
  </tr>
  <tr>  
     <td style="width: 100px;">{{ trans('new_translations.to')}} :</td>
      <td>{{isset($arr_milestone['client_details']['role_info']['first_name'])?$arr_milestone['client_details']['role_info']['first_name']:""}} {{isset($arr_milestone['client_details']['role_info']['last_name'])?$arr_milestone['client_details']['role_info']['last_name']:""}}</td>
  </tr>
   <tr>
      <td style="width: 100px;"></td>
      <td>{{isset($arr_milestone['client_details']['role_info']['address'])?$arr_milestone['client_details']['role_info']['address']:""}} 
      {{isset($arr_milestone['client_details']['role_info']['city_details']['city_name'])?$arr_milestone['client_details']['role_info']['city_details']['city_name']:""}},

      {{isset($arr_milestone['client_details']['role_info']['state_details']['state_name'])?$arr_milestone['client_details']['role_info']['state_details']['state_name']:""}},

      {{isset($arr_milestone['client_details']['role_info']['country_details']['country_name'])?$arr_milestone['client_details']['role_info']['country_details']['country_name']:""}}

      </td>
      
  </tr>
  <tr>    
      <td style="width: 100px;">{{ trans('new_translations.project_name')}} :</td>
      <td>{{isset($arr_milestone['project_details']['project_name'])?$arr_milestone['project_details']['project_name']:""}}</td>
  </tr>
  <tr>    
      <td style="width: 100px;">{{ trans('new_translations.date')}} :</td>
      <td>{{date('d M Y')}}</td>
  </tr>
  
  <br/><br/><br/>


@if(isset($arr_invoice_costs) && sizeof($arr_invoice_costs)>0 && isset($arr_invoice_costs['total_cost']) && isset($arr_invoice_costs['milestone_cost']) && isset($arr_invoice_costs['vat_cost']) && isset($arr_invoice_costs['vat_percentage']))

  <table style="border:1px solid silver; border-collapse: collapse;" cellspacing="8px" cellpadding="8px">
      <tr>
          <td style="width: 5%; border-color: #1B7AE0; border-spacing: 0px; border-style:none;">#</td>
          <td style="width: 40%; border-spacing: 0px; border-style:none;">{{isset($arr_milestone['milestone_details']['title'])?$arr_milestone['milestone_details']['title']:""}}</td>
          <td style="width: 15%; border-spacing: 0px; border-style:none;"></td>
          <td style="width: 20%; border-spacing: 0px; border-style:none;">{{isset($arr_milestone['project_details']['project_currency'])?$arr_milestone['project_details']['project_currency']:""}}{{$arr_invoice_costs['milestone_cost'] or ''}}</td>
      </tr>

       <tr>
          <td style="width: 5%; border-color: #1B7AE0; border-spacing: 0px; border-style:none;">#</td>
          <td style="width: 40%; border-spacing: 0px; border-style:none;">VAT</td>
          <td style="width: 15%; border-spacing: 0px; border-style:none;">{{$arr_invoice_costs['vat_percentage'] or '0'}} %</td>
          <td style="width: 20%; border-spacing: 0px; border-style:none;">{{isset($arr_milestone['project_details']['project_currency'])?$arr_milestone['project_details']['project_currency']:""}}{{$arr_invoice_costs['vat_cost'] or ''}}</td>
      </tr>

     <tr>
        <td style="width: 5%; border-spacing: 0px; border-style:none;"></td>
        <td style="width: 40%; border-spacing: 0px; border-style:none;"></td>
        <td style="width: 15%; border-spacing: 0px; border-style:none;">Total Cost</td>
        <td style="width: 20%; border-spacing: 0px; border-style:none;">{{isset($arr_milestone['project_details']['project_currency'])?$arr_milestone['project_details']['project_currency']:""}}{{isset($arr_milestone['milestone_details']['cost'])?number_format($arr_milestone['milestone_details']['cost'],2):""}}</td>
      </tr>
  </table>
@else
  <table style="border:1px solid silver; border-collapse: collapse;" cellspacing="8px" cellpadding="8px">
      <tr>
              <td style="width: 5%; border-color: #1B7AE0; border-spacing: 0px; border-style:none;">#</td>
              <td style="width: 40%; border-spacing: 0px; border-style:none;">{{isset($arr_milestone['milestone_details']['title'])?$arr_milestone['milestone_details']['title']:""}}</td>
              <td style="width: 15%; border-spacing: 0px; border-style:none;"></td>
              <td style="width: 20%; border-spacing: 0px; border-style:none;">{{isset($arr_milestone['project_details']['project_currency'])?$arr_milestone['project_details']['project_currency']:""}}{{isset($arr_milestone['milestone_details']['cost'])?number_format($arr_milestone['milestone_details']['cost'],2):""}}</td>
          </tr>
     <tr>
        <td style="width: 5%; border-spacing: 0px; border-style:none;"></td>
        <td style="width: 40%; border-spacing: 0px; border-style:none;"></td>
        <td style="width: 15%; border-spacing: 0px; border-style:none;">Total Cost</td>
        <td style="width: 20%; border-spacing: 0px; border-style:none;">{{isset($arr_milestone['project_details']['project_currency'])?$arr_milestone['project_details']['project_currency']:""}}{{isset($arr_milestone['milestone_details']['cost'])?number_format($arr_milestone['milestone_details']['cost'],2):""}}</td>
      </tr>
  </table>
@endif
</table>
