<!DOCTYPE html>
<html>
    <head>

    
        <title>{{config('app.project.name')}}</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 90%;
                background-color: #878787; 
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 70px;
            }

            .message {
                font-size: 25px;
                font-weight: bold;
                padding-top: 20px; 
            }
            

        </style>
    </head>
    <body>
        <div class="container">
            <div class="content" id="img-div">
                <div class="title">{{config('app.project.name')}}</div>
                <div class="message">{{ trans('new_translations.we_are_currently_down_for_maintenance')}}!</div>
            </div>
        </div>
    </body>
</html>
