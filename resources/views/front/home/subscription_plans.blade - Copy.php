@extends('front.layout.master')                
@section('main_content')
<div class="top-title-pattern">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="title-box-top">
               <h3>{{isset($page_title)?$page_title:''}}</h3>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="middle-container">
   <div class="container">
      <div class="pricing pricing-5 bottommargin clearfix">
         @if(isset($arr_packs) && sizeof($arr_packs)>0)
         @foreach($arr_packs as $key => $pack)
         <div class="pricing-box pricing-minimal">
            <div class="pricing-title">
               <h3>{{isset($pack['pack_name'])?$pack['pack_name']:''}}</h3>
            </div>
            <div class="pricing-price">
               <span class="price-unit">&#36;</span>{{isset($pack['pack_price'])?$pack['pack_price']:''}}
                @if(isset($pack['id']) && $pack['id']!=1)
               <span class="price-tenure">
               {{ trans('expert/subscription/packs.text_mo') }}
               </span>
               <div class="billed-month">{{ trans('expert/subscription/packs.text_billed_monthly') }}</div>
               @endif
            </div>
            <div class="pricing-features">
               <ul>
                  <li>
                     {{isset($pack['number_of_bids'])?$pack['number_of_bids']:''}} {{ trans('expert/subscription/packs.text_bids') }}
                  </li>
                  <li>
                     {{isset($pack['topup_bid_price'])?$pack['topup_bid_price']:''}} {{ trans('expert/subscription/packs.text_topup_bid') }}
                  </li>
                  <li>
                     {{isset($pack['number_of_categories'])?$pack['number_of_categories']:''}} {{ trans('expert/subscription/packs.text_categories') }}
                  </li>
                  <li>
                     {{isset($pack['number_of_subcategories'])?$pack['number_of_subcategories']:''}} {{ trans('expert/subscription/packs.text_subcategories') }}
                  </li>
                  <li>
                     {{isset($pack['number_of_skills'])?$pack['number_of_skills']:''}} {{ trans('expert/subscription/packs.text_skills') }}
                  </li>
                  <li>
                     {{isset($pack['number_of_favorites_projects'])?$pack['number_of_favorites_projects']:''}} {{ trans('expert/subscription/packs.text_favorites_projects') }}
                  </li>
                  <li>
                     {{isset($pack['number_of_payouts'])?$pack['number_of_payouts']:''}} {{ trans('expert/subscription/packs.text_payouts') }}
                  </li>
                  <li>
                     {{isset($pack['website_commision'])?$pack['website_commision']:''}} % {{ trans('expert/subscription/packs.text_website_commission') }}
                  </li>
               </ul>
            </div>
            <div class="clr"></div>
            <div class="pricing-action">
               <?php /*
               @if($key != 0)
                  @if(isset($arr_subscription['subscription_pack_id']) && isset($pack['id']) && ($arr_subscription['subscription_pack_id'] == $pack['id']) )
                       <a href="javascript: void(0);">
                        <button class="btn-paynow"><span class="btn1">
                        @if(isset($arr_subscription['subscription_pack_id']) && $arr_subscription['subscription_pack_id']==$pack['id'])
                        {{ trans('expert/subscription/packs.text_activated') }}
                        @else
                        {{ trans('expert/subscription/packs.text_pay_now') }}
                        @endif
                        </span>
                        <span style="margin-left:3px;" class="btn1 narr"><i aria-hidden="true" class="fa fa-angle-right"></i></span>
                        </button>
                        </a>
                  @else
                        <a href="{{isset($module_url_path)?$module_url_path.'/payment/'.base64_encode($pack['id']):''}}">
                        <button class="btn-paynow"><span class="btn1">
                        @if(isset($arr_subscription['subscription_pack_id']) && $arr_subscription['subscription_pack_id']==$pack['id'])
                        {{ trans('expert/subscription/packs.text_activated') }}
                        @else
                        {{ trans('expert/subscription/packs.text_pay_now') }}
                        @endif
                        </span>
                        <span style="margin-left:3px;" class="btn1 narr"><i aria-hidden="true" class="fa fa-angle-right"></i></span>
                        </button>
                        </a>
                  @endif
               @else
                  @if(isset($arr_subscription['subscription_pack_id']) && $arr_subscription['subscription_pack_id'] == 1)
                    <a href="javascript: void(0);" style="cursor: default;" class="btn-paynow"><span class="btn1">{{ trans('expert/subscription/packs.text_free') }}</span><span style="margin-left:3px;" class="btn1 narr"><i aria-hidden="true" class="fa fa-angle-right"></i></span>
                    </a>
                  @else 
                    <a href="javascript: void(0);" style="cursor: default;" class="btn-paynow"><span class="btn1">{{ trans('expert/subscription/packs.text_free') }}</span><span style="margin-left:3px;" class="btn1 narr"><i aria-hidden="true" class="fa fa-angle-right"></i></span>
                    </a>
                  @endif
               @endif */ ?>
            </div>
         </div>
         @endforeach
         @endif
      </div>
   </div>
</div>
@stop