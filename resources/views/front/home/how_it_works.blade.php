@extends('front.layout.master')                
@section('main_content')

<div class="page-header">
    <div class="container">
        <div class="page-name">
            <h2>How it Works</h2>
            <div class="brd-bottom">&nbsp;</div>
            <p><a href="{{url('/')}}">Home</a> / How it Works</p>
        </div>
    </div>
</div>  
<div class="how-it-section">
    <div class="container">
        <div class="how-it-wrapper">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6 swap-right">
                   <div class="how-it-img">
                       <img src="{{url('/public/front/images/how-it-img1.jpg')}}" class="img-responsive" alt="how it work image"/>
                   </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="how-it-content">
                        <div class="num">01</div>
                        <h5>Post Your Project</h5>
                        <p>Lorem ipsum dolor sit amet sectetuer esl adipiscing elit sed diam nonummy nibhi euismod tincidunt ut laoreet dolore amet magna. amet sectetuer esl adipiscing elit sed diam nonummy.olore amet magna. amet sectetuer esl adipiscing elit sed diam nonummy</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6">
                   <div class="how-it-img">
                       <img src="{{url('/public/front/images/how-it-img2.jpg')}}" class="img-responsive" alt="how it work image"/>
                   </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="how-it-content">
                        <div class="num">02</div>
                        <h5>Select an Expert</h5>
                        <p>Lorem ipsum dolor sit amet sectetuer esl adipiscing elit sed diam nonummy nibhi euismod tincidunt ut laoreet dolore amet magna. amet sectetuer esl adipiscing elit sed diam nonummy.olore amet magna. amet sectetuer esl adipiscing elit sed diam nonummy</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6 swap-right">
                   <div class="how-it-img">
                       <img src="{{url('/public/front/images/how-it-img3.jpg')}}" class="img-responsive" alt="how it work image"/>
                   </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="how-it-content">
                        <div class="num">03</div>
                        <h5>Work on Project</h5>
                        <p>Lorem ipsum dolor sit amet sectetuer esl adipiscing elit sed diam nonummy nibhi euismod tincidunt ut laoreet dolore amet magna. amet sectetuer esl adipiscing elit sed diam nonummy.olore amet magna. amet sectetuer esl adipiscing elit sed diam nonummy</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6">
                   <div class="how-it-img">
                       <img src="{{url('/public/front/images/how-it-img4.jpg')}}" class="img-responsive" alt="how it work image"/>
                   </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="how-it-content">
                        <div class="num">04</div>
                        <h5>Pay only when Satisfied</h5>
                        <p>Lorem ipsum dolor sit amet sectetuer esl adipiscing elit sed diam nonummy nibhi euismod tincidunt ut laoreet dolore amet magna. amet sectetuer esl adipiscing elit sed diam nonummy.olore amet magna. amet sectetuer esl adipiscing elit sed diam nonummy</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
<div class="about-bottom-strip">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="about-bottom">
                    <div class="about-icon1 bg-img">&nbsp;</div>
                    <div class="about-details">
                        <h2>10M+</h2>
                        <p>Registered freelancers</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="about-bottom">
                    <div class="about-icon2 bg-img">&nbsp;</div>
                    <div class="about-details">
                        <h2>$1B+</h2>
                        <p>Work done annually</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="about-bottom">
                    <div class="about-icon3 bg-img">&nbsp;</div>
                    <div class="about-details">
                        <h2>3M</h2>
                        <p>Jobs posted annually</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="about-bottom">
                    <div class="about-icon4 bg-img">&nbsp;</div>
                    <div class="about-details">
                        <h2>4M+</h2>
                        <p>Registered clients</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop   