@extends('front.layout.master')                
@section('main_content')
 <script src="{{url('/public')}}/front/js/accordian.js" type="text/javascript"></script>
  <!--Page-Titel-section-start-here-->
   <div class="faq-main-wrapper height-block">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-md-8 col-lg-8">
                <div class="frequently-as">
                    <h1>{{$faq_type}}</h1>
                </div>
            </div>
            <!-- <div class="col-sm-4 col-md-4 col-lg-4">
                <div class="search-block-fag">
                    <div class="form-group search">
                        <input type="text" class="input-box" placeholder="{{trans('faq.text_search')}}" />
                        <div class="search-ico">
                            <a href="#"> <i class="fa fa-search"></i> </a>
                        </div>
                        <span class="bar"></span>
                    </div>
                </div>
            </div> -->
        </div>
        <!--Page-Titel-section-end-here-->
        <!--Faq-page-section-start-here-->
        <div id='faq_acc' class="faq-wrapper">
            <ul>
              @if(isset($arr_question['data']) && !empty($arr_question['data']) && $arr_question['data'] != "")   
                @foreach($arr_question['data'] as $question)
                    <li class='has-sub'>
                        <a style="cursor:pointer;"><span class="title">{{$question['question_name'] or ''}}</span> <span class="icons"><span></span></span></a>
                        <ul>
                            <li>
                                <div class="">
                                    <div class="faq-text"><?php if(isset($question['question_ans']) && $question['question_ans'] !="") { echo trim(htmlspecialchars_decode($question['question_ans'], ENT_NOQUOTES)); } else { echo ""; };?></div>
                                </div>
                            </li>
                        </ul>
                    </li>
                @endforeach
              @else
              {{ trans('expert_listing/listing.text_sorry_no_records_found') }}
              @endif  
            </ul>


        </div>
        @if(isset($arr_question['data']) && !empty($arr_question['data']) && $arr_question['data'] != "")  
            <!-- Pagination Links -->
                  @include('front.common.pagination_view', ['paginator' => $arr_pagination])
            <!-- Pagination Links -->
        @endif 
        <div class="clearfix"></div>
        <!--Faq-page-section-end-here-->
    </div>
   </div> 
@stop   