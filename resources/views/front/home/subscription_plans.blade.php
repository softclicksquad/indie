@extends('front.layout.master')                
@section('main_content')
<style>
    .middle-container .container{position: relative}
    .subscription-dropdown-main{position: absolute;top: 0;right: 15px;width: 100px;}
</style>
<div class="top-title-pattern">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="title-box-top">
               <h3>{{isset($page_title)?$page_title:''}}</h3>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="middle-container subscription-plan-tab-section">
   <div class="container">
        <div data-responsive-tabs>
    			<nav>
    				<ul>
    					<li>
    						<a href="#one">Monthly</a>
    					</li>
    					<li>
    						<a href="#two">Quarterly</a>
    					</li>
    					<li>
    						<a href="#three">Yearly</a>
    					</li>										
    				</ul>
    			</nav>

          <input type="hidden" name="selected_currency" id="selected_currency">

    			<div class="content">
    				<section id="one">
    					<div class="pricing pricing-5 bottommargin clearfix">
                      @if(isset($arr_monthly_packs) && sizeof($arr_monthly_packs)>0)
                      @foreach($arr_monthly_packs as $monthly_key => $monthly_value)
                    
                      <div class="pricing-box pricing-minimal">
                         <div class="pricing-title">
                            <h3>{{isset($monthly_value['pack_name'])?$monthly_value['pack_name']:''}}</h3>
                         </div>
                         <div class="pricing-price">
                            <span class="price-unit">&#36;</span><span id="monthly_{{strtolower($monthly_value['pack_name'])}}">{{isset($monthly_value['pack_price'])?$monthly_value['pack_price']:''}} </span><span class="code-unit"></span>
                            @if(isset($monthly_value['id']) && $monthly_value['id']!=1)
                               <span class="price-tenure">
                               {{ trans('expert/subscription/packs.text_mo') }}
                               </span>
                               <div class="billed-month">{{ trans('expert/subscription/packs.text_billed_monthly') }}</div>
                            @endif
                         </div>
                         <input type="hidden" name="monthly_price" id="monthly_price_{{strtolower($monthly_value['pack_name'])}}" data-price-USD="{{$monthly_value['pack_price'] or '0'}}" data-price-EUR="{{$monthly_value['pack_price_eur'] or '0'}}" data-price-GBP="{{$monthly_value['pack_price_gbp'] or '0'}}" data-price-PLN="{{$monthly_value['pack_price_pln'] or '0'}}" data-price-CHF="{{$monthly_value['pack_price_chf'] or '0'}}" data-price-NOK="{{$monthly_value['pack_price_nok'] or '0'}}" data-price-SEK="{{$monthly_value['pack_price_sek'] or '0'}}" data-price-DKK="{{$monthly_value['pack_price_dkk'] or '0'}}" data-price-CAD="{{$monthly_value['pack_price_cad'] or '0'}}" data-price-ZAR="{{$monthly_value['pack_price_zar'] or '0'}}" data-price-AUD="{{$monthly_value['pack_price_aud'] or '0'}}" data-price-HKD="{{$monthly_value['pack_price_hkd'] or '0'}}" data-price-JPY="{{$monthly_value['pack_price_jpy'] or '0'}}" data-price-CZK="{{$monthly_value['pack_price_czk'] or '0'}}" >
                         <div class="pricing-features">
                            <ul>
                               <li>
                                  {{isset($monthly_value['number_of_bids'])?$monthly_value['number_of_bids']:''}} {{ trans('expert/subscription/packs.text_bids') }}
                               </li>
                               <li>
                                  {{isset($monthly_value['topup_bid_price'])?$monthly_value['topup_bid_price']:''}} {{ trans('expert/subscription/packs.text_topup_bid') }}
                               </li>
                               <li>
                                  {{isset($monthly_value['number_of_categories'])?$monthly_value['number_of_categories']:''}} {{ trans('expert/subscription/packs.text_categories') }}
                               </li>
                               <li>
                                  {{isset($monthly_value['number_of_subcategories'])?$monthly_value['number_of_subcategories']:''}} {{ trans('expert/subscription/packs.text_subcategories') }}
                               </li>
                               <li>
                                  {{isset($monthly_value['number_of_skills'])?$monthly_value['number_of_skills']:''}} {{ trans('expert/subscription/packs.text_skills') }}
                               </li>
                               <li>
                                  {{isset($monthly_value['number_of_favorites_projects'])?$monthly_value['number_of_favorites_projects']:''}} {{ trans('expert/subscription/packs.text_favorites_projects') }}
                               </li>
                               <li>
                                  {{isset($monthly_value['number_of_payouts'])?$monthly_value['number_of_payouts']:''}} {{ trans('expert/subscription/packs.text_payouts') }}
                               </li>
                               <li>
                                  {{isset($monthly_value['website_commision'])?$monthly_value['website_commision']:''}} % {{ trans('expert/subscription/packs.text_website_commission') }}
                               </li>
                            </ul>
                         </div>
                         <div class="clr"></div>
                      </div>
                      @endforeach
                      @endif
                   </div>
    				</section>

    				<section id="two">
    					<div class="pricing pricing-5 bottommargin clearfix">
                      @if(isset($arr_quarterly_packs) && sizeof($arr_quarterly_packs)>0)
                      @foreach($arr_quarterly_packs as $quarterly_key => $quarterly_value)
                      <div class="pricing-box pricing-minimal">
                         <div class="pricing-title">
                            <h3>{{isset($quarterly_value['pack_name'])?$quarterly_value['pack_name']:''}}</h3>
                         </div>
                         <div class="pricing-price">
                            <span class="price-unit">&#36;</span><span id="quarterly_{{strtolower($quarterly_value['pack_name'])}}">{{isset($quarterly_value['pack_price'])?$quarterly_value['pack_price']:''}}</span><span class="code-unit"></span>
                            @if(isset($quarterly_value['id']) && $quarterly_value['id']!=6)
                               <span class="price-tenure">
                               {{ trans('expert/subscription/packs.text_mo') }}
                               </span>
                               <div class="billed-month">
                                  {{ trans('expert/subscription/packs.text_billed_quarterly') }}
                               </div>
                            @endif
                         </div>

                         <input type="hidden" name="quarterly_price" id="quarterly_price_{{strtolower($quarterly_value['pack_name'])}}" data-price-USD="{{$quarterly_value['pack_price'] or '0'}}" data-price-EUR="{{$quarterly_value['pack_price_eur'] or '0'}}" data-price-GBP="{{$quarterly_value['pack_price_gbp'] or '0'}}" data-price-PLN="{{$quarterly_value['pack_price_pln'] or '0'}}" data-price-CHF="{{$quarterly_value['pack_price_chf'] or '0'}}" data-price-NOK="{{$quarterly_value['pack_price_nok'] or '0'}}" data-price-SEK="{{$quarterly_value['pack_price_sek'] or '0'}}" data-price-DKK="{{$quarterly_value['pack_price_dkk'] or '0'}}" data-price-CAD="{{$quarterly_value['pack_price_cad'] or '0'}}" data-price-ZAR="{{$quarterly_value['pack_price_zar'] or '0'}}" data-price-AUD="{{$quarterly_value['pack_price_aud'] or '0'}}" data-price-HKD="{{$quarterly_value['pack_price_hkd'] or '0'}}" data-price-JPY="{{$quarterly_value['pack_price_jpy'] or '0'}}" data-price-CZK="{{$quarterly_value['pack_price_czk'] or '0'}}" >

                         <div class="pricing-features">
                            <ul>
                               <li>
                                  {{isset($quarterly_value['number_of_bids'])?$quarterly_value['number_of_bids']:''}} {{ trans('expert/subscription/packs.text_bids') }}
                               </li>
                               <li>
                                  {{isset($quarterly_value['topup_bid_price'])?$quarterly_value['topup_bid_price']:''}} {{ trans('expert/subscription/packs.text_topup_bid') }}
                               </li>
                               <li>
                                  {{isset($quarterly_value['number_of_categories'])?$quarterly_value['number_of_categories']:''}} {{ trans('expert/subscription/packs.text_categories') }}
                               </li>
                               <li>
                                  {{isset($quarterly_value['number_of_subcategories'])?$quarterly_value['number_of_subcategories']:''}} {{ trans('expert/subscription/packs.text_subcategories') }}
                               </li>
                               <li>
                                  {{isset($quarterly_value['number_of_skills'])?$quarterly_value['number_of_skills']:''}} {{ trans('expert/subscription/packs.text_skills') }}
                               </li>
                               <li>
                                  {{isset($quarterly_value['number_of_favorites_projects'])?$quarterly_value['number_of_favorites_projects']:''}} {{ trans('expert/subscription/packs.text_favorites_projects') }}
                               </li>
                               <li>
                                  {{isset($quarterly_value['number_of_payouts'])?$quarterly_value['number_of_payouts']:''}} {{ trans('expert/subscription/packs.text_payouts') }}
                               </li>
                               <li>
                                  {{isset($quarterly_value['website_commision'])?$quarterly_value['website_commision']:''}} % {{ trans('expert/subscription/packs.text_website_commission') }}
                               </li>
                            </ul>
                         </div>
                         <div class="clr"></div>
                      </div>
                      @endforeach
                      @endif
                   </div>
    				</section>

    				<section id="three">
    					<div class="pricing pricing-5 bottommargin clearfix">
                      @if(isset($arr_yearly_packs) && sizeof($arr_yearly_packs)>0)
                      @foreach($arr_yearly_packs as $yearly_key => $yearly_value)
                      <div class="pricing-box pricing-minimal">
                          <div class="pricing-title">
                            <h3>{{isset($yearly_value['pack_name'])?$yearly_value['pack_name']:''}}</h3>
                          </div>
                          <div class="pricing-price">
                            <span class="price-unit">&#36;</span><span id="yearly_{{strtolower($yearly_value['pack_name'])}}">{{isset($yearly_value['pack_price'])?$yearly_value['pack_price']:''}}</span><span class="code-unit"></span>
                             @if(isset($yearly_value['id']) && $yearly_value['id']!=11)
                            <span class="price-tenure">
                            {{ trans('expert/subscription/packs.text_mo') }}
                            </span>
                            <div class="billed-month">{{ trans('expert/subscription/packs.text_billed_yearly') }}</div>
                            @endif
                          </div>

                          <input type="hidden" name="yearly_price" id="yearly_price_{{strtolower($yearly_value['pack_name'])}}" data-price-USD="{{$yearly_value['pack_price'] or '0'}}" data-price-EUR="{{$yearly_value['pack_price_eur'] or '0'}}" data-price-GBP="{{$yearly_value['pack_price_gbp'] or '0'}}" data-price-PLN="{{$yearly_value['pack_price_pln'] or '0'}}" data-price-CHF="{{$yearly_value['pack_price_chf'] or '0'}}" data-price-NOK="{{$yearly_value['pack_price_nok'] or '0'}}" data-price-SEK="{{$yearly_value['pack_price_sek'] or '0'}}" data-price-DKK="{{$yearly_value['pack_price_dkk'] or '0'}}" data-price-CAD="{{$yearly_value['pack_price_cad'] or '0'}}" data-price-ZAR="{{$yearly_value['pack_price_zar'] or '0'}}" data-price-AUD="{{$yearly_value['pack_price_aud'] or '0'}}" data-price-HKD="{{$yearly_value['pack_price_hkd'] or '0'}}" data-price-JPY="{{$yearly_value['pack_price_jpy'] or '0'}}" data-price-CZK="{{$yearly_value['pack_price_czk'] or '0'}}" >

                          <div class="pricing-features">
                            <ul>
                              <li>
                                {{isset($yearly_value['number_of_bids'])?$yearly_value['number_of_bids']:''}} {{ trans('expert/subscription/packs.text_bids') }}
                              </li>
                              <li>
                                  {{isset($yearly_value['topup_bid_price'])?$yearly_value['topup_bid_price']:''}} {{ trans('expert/subscription/packs.text_topup_bid') }}
                              </li>
                              <li>
                                  {{isset($yearly_value['number_of_categories'])?$yearly_value['number_of_categories']:''}} {{ trans('expert/subscription/packs.text_categories') }}
                              </li>
                              <li>
                                  {{isset($yearly_value['number_of_subcategories'])?$yearly_value['number_of_subcategories']:''}} {{ trans('expert/subscription/packs.text_subcategories') }}
                              </li>
                              <li>
                                  {{isset($yearly_value['number_of_skills'])?$yearly_value['number_of_skills']:''}} {{ trans('expert/subscription/packs.text_skills') }}
                              </li>
                              <li>
                                  {{isset($yearly_value['number_of_favorites_projects'])?$yearly_value['number_of_favorites_projects']:''}} {{ trans('expert/subscription/packs.text_favorites_projects') }}
                              </li>
                               <li>
                                  {{isset($yearly_value['number_of_payouts'])?$yearly_value['number_of_payouts']:''}} {{ trans('expert/subscription/packs.text_payouts') }}
                               </li>
                               <li>
                                  {{isset($yearly_value['website_commision'])?$yearly_value['website_commision']:''}} % {{ trans('expert/subscription/packs.text_website_commission') }}
                               </li>
                            </ul>
                         </div>
                         <div class="clr"></div>
                      </div>
                      @endforeach
                      @endif
                   </div>
    				</section>								
    			</div>
    		</div>

        <?php
            $selected_currency = 'USD';
        ?>

        <div class="subscription-dropdown-main">
            <div class="droup-select">
                <select class="droup getcat mrns tp-margn valid" data-rule-required="true" name="currency" id="currency">
                  @if(isset($arr_currency) && count($arr_currency)>0)
                    @foreach($arr_currency as $curr_key => $curr_value)
                      <option value="{{ isset($curr_value['currency_code'])?$curr_value['currency_code']:'' }}" @if($selected_currency == $curr_value['currency_code']) selected @endif>{{ isset($curr_value['currency_code'])?$curr_value['currency_code']:'' }}</option>
                    @endforeach
                  @endif
                </select>                
            </div>
        </div>

        <div class="clearfix"></div>
   </div>
</div>

<link rel="stylesheet" href="{{url('/public')}}/front/css/responsivetabs.css" />
<script src="{{url('/public')}}/front/js/responsivetabs.js"></script>

<script type="text/javascript">
  /*$('#currency').onClick(function(){
    var currency = $('#currency').val();
    console.log(currency);
  })*/
  var project_currency = {!! json_encode( project_converted_currency() ) !!};
  
  function change_currency() 
  {
      var currency = $('#currency').val();
      $('#selected_currency').val(currency);

      if(project_currency[''+currency+'']!=currency)
      {
        $('.code-unit').html('('+currency+')');
      }
      else
      {
        $('.code-unit').html('');
      }
      
      $('.price-unit').html(project_currency[''+currency+'']);

      var monthly_price_starter = $('#monthly_price_starter').attr('data-price-'+currency);
      var monthly_price_basic = $('#monthly_price_basic').attr('data-price-'+currency);
      var monthly_price_plus = $('#monthly_price_plus').attr('data-price-'+currency);
      var monthly_price_premium = $('#monthly_price_premium').attr('data-price-'+currency);
      var monthly_price_professional = $('#monthly_price_professional').attr('data-price-'+currency);

      $('#monthly_starter').html(monthly_price_starter);
      $('#monthly_basic').html(monthly_price_basic);
      $('#monthly_plus').html(monthly_price_plus);
      $('#monthly_premium').html(monthly_price_premium);
      $('#monthly_professional').html(monthly_price_professional);

      var quarterly_price_starter = $('#quarterly_price_starter').attr('data-price-'+currency);
      var quarterly_price_basic = $('#quarterly_price_basic').attr('data-price-'+currency);
      var quarterly_price_plus = $('#quarterly_price_plus').attr('data-price-'+currency);
      var quarterly_price_premium = $('#quarterly_price_premium').attr('data-price-'+currency);
      var quarterly_price_professional = $('#quarterly_price_professional').attr('data-price-'+currency);

      $('#quarterly_starter').html(quarterly_price_starter);
      $('#quarterly_basic').html(quarterly_price_basic);
      $('#quarterly_plus').html(quarterly_price_plus);
      $('#quarterly_premium').html(quarterly_price_premium);
      $('#quarterly_professional').html(quarterly_price_professional);

      var yearly_price_starter = $('#yearly_price_starter').attr('data-price-'+currency);
      var yearly_price_basic = $('#yearly_price_basic').attr('data-price-'+currency);
      var yearly_price_plus = $('#yearly_price_plus').attr('data-price-'+currency);
      var yearly_price_premium = $('#yearly_price_premium').attr('data-price-'+currency);
      var yearly_price_professional = $('#yearly_price_professional').attr('data-price-'+currency);

      $('#yearly_starter').html(yearly_price_starter);
      $('#yearly_basic').html(yearly_price_basic);
      $('#yearly_plus').html(yearly_price_plus);
      $('#yearly_premium').html(yearly_price_premium);
      $('#yearly_professional').html(yearly_price_professional);
  }

  

  $('#currency').change(function(){
      change_currency();
  });

</script>

<script>
    $(document).ready(function ()
    {	
        change_currency();
        $(document).on('responsive-tabs.initialised', function (event, el)
        {
            console.log(el);
        });

        $(document).on('responsive-tabs.change', function (event, el, newPanel)
        {
            console.log(el);
            console.log(newPanel);
        });

        $('[data-responsive-tabs]').responsivetabs(
        {
            initialised : function ()
            {
                console.log(this);
            },

            change : function (newPanel)
            {
                console.log(newPanel);
            }
        });


    });
</script>
@stop