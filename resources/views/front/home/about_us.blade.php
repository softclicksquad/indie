@extends('front.layout.master')                
@section('main_content')
<div class="page-header">
    <div class="container">
        <div class="page-name">
            <h2>{{isset($page_title)?$page_title:''}}</h2>
            <div class="brd-bottom">&nbsp;</div>
            <p><a href="{{url('/')}}">Home</a> / {{isset($page_title)?$page_title:''}}</p>
        </div>
    </div>
</div>
<div class="about-middle-section">
     <div class="container">
       <div class="why-choose-wrapper">
         <div class="about-middle-img"><img src="{{url('/public/front/images/about-img2.jpg')}}" class="img-responsive"/></div>
        <div class="why-choose-block">
            <h2>@php echo trans('new_translations.why_choose_us'); @endphp?</h2>
            <p>{!! isset($arr_page['page_desc'])?$arr_page['page_desc']:'' !!}</p>
        </div>
      
        </div>
    </div>
</div>
<div class="home-page">
<div class="how-it-work-section about-how-it">
	<div class="container">
		<h2><b>@php echo trans('new_translations.how_it_works'); @endphp</b></h2>
        <div class="row">
            <div class="col-sm-6 col-md-3 col-lg-3">
				<div class="how-it-block">
					<div class="how-it-circle bg-img how-it-icon1">&nbsp;</div>
					<h4>@php echo trans('new_translations.post_your_project'); @endphp</h4>
					<p>@php echo trans('new_translations.register_with_virtual_home'); @endphp @php echo trans('new_translations.concepts_and_post_your_project'); @endphp @php echo trans('new_translations.requirements'); @endphp</p>
					<div class="arrow-block bg-img"></div>
				</div>
			</div>
			<div class="col-sm-6 col-md-3 col-lg-3">
				<div class="how-it-block">
					<div class="how-it-circle bg-img how-it-icon2">&nbsp;</div>
					<h4>@php echo trans('new_translations.select_an_expert'); @endphp</h4>
                    <p>@php echo trans('new_translations.we_will_select_the_best_suited'); @endphp @php echo trans('new_translations.experts_to_work_on_your_project'); @endphp @php echo trans('new_translations.pick_one_or_more'); @endphp @php echo trans('new_translations.experts'); @endphp</p>
					<div class="arrow-block bg-img"></div>
				</div>
			</div>
			<div class="col-sm-6 col-md-3 col-lg-3">
				<div class="how-it-block">
					<div class="how-it-circle bg-img how-it-icon3">&nbsp;</div>
					<h4>@php echo trans('new_translations.work_on_the_project'); @endphp</h4>
                    <p>@php echo trans('new_translations.agree_on_the_project'); @endphp @php echo trans('new_translations.cost_and_get_started'); @endphp</p>
					<div class="arrow-block bg-img"></div>
				</div>
			</div>
			<div class="col-sm-6 col-md-3 col-lg-3">
				<div class="how-it-block">
					<div class="how-it-circle bg-img how-it-icon4">&nbsp;</div>
					<h4>@php echo trans('new_translations.pay_only_when_satisfied'); @endphp</h4>
                    <p>@php echo trans('new_translations.pay_the_project_fee'); @endphp @php echo trans('new_translations.once_you_notify_project'); @endphp @php echo trans('new_translations.completion'); @endphp</p>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<div class="about-bottom-strip">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="about-bottom">
                    <div class="about-icon1 bg-img">&nbsp;</div>
                    <div class="about-details">
                        <h2>10M+</h2>
                        <p>{{ trans('new_translations.registered_freelancers')}}</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="about-bottom">
                    <div class="about-icon2 bg-img">&nbsp;</div>
                    <div class="about-details">
                        <h2>$1B+</h2>
                        <p>{{ trans('new_translations.worth_of_work_done_annually')}}</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="about-bottom">
                    <div class="about-icon3 bg-img">&nbsp;</div>
                    <div class="about-details">
                        <h2>3M</h2>
                        <p>{{ trans('new_translations.jobs_posted_annually')}}</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="about-bottom">
                    <div class="about-icon4 bg-img">&nbsp;</div>
                    <div class="about-details">
                        <h2>4M+</h2>
                        <p>{{ trans('new_translations.registered_clients')}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop   