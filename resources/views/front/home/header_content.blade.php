<?php 
if (isset($mangopay_wallet_details) && count($mangopay_wallet_details)>0)
{
  foreach ($mangopay_wallet_details as $key => $value) 
  {
     if($value->Balance->Amount > 0)
     {
       $arr_data_bal['balance'][]  = isset($value->Balance->Amount)?$value->Balance->Amount/100:'0';
       $arr_data_bal['currency'][] = isset($value->Balance->Currency)?$value->Balance->Currency:'';
     }
  }
}

?>
<span>
@if(isset($arr_data_bal['balance']) && count($arr_data_bal['balance'])>0)
{{ max($arr_data_bal['balance'])}} 
@endif
</span>
<div class="money-dropdown">
<div class="arrow-up-mn"> <img src="{{url('/public')}}/front/images/arrows-tp.png" alt=""></div>
<div class="currency-title">Balances</div>
@if(isset($arr_data_bal['balance']) && count($arr_data_bal['balance'])>0)
@foreach($arr_data_bal['balance'] as $key=>$value)
<div class="currency-list">{{isset($value)?$value:''}} {{isset($arr_data_bal['currency'][$key])?$arr_data_bal['currency'][$key]:''}}</div>
@endforeach
@endif
  <a href="#add_money" class="mp-add-btn-text add-money-but" data-keyboard="false" data-backdrop="static" data-toggle="modal"> 
      <i class="fa fa-plus-circle"></i> Deposit Fund{{-- {{trans('common/wallet/text.text_payin')}} --}}
  </a>
</div>