@extends('front.layout.master')                
@section('main_content')
@php $user = \Sentinel::check();@endphp

@if(isset($user) && $user != null)
@php
   $user_first_name = '';
   $user_last_name  = '';
   $user_email      = '';
   $expert_details  = sidebar_information($user->id);
   if(isset($expert_details['user_details']['first_name']) && $expert_details['user_details']['first_name'] !=""){
     $user_first_name =  $expert_details['user_details']['first_name'];
   }
   if(isset($expert_details['user_details']['last_name']) && $expert_details['user_details']['last_name'] !=""){
     $user_last_name  =  substr($expert_details['user_details']['last_name'],0,1).'.';
   }
   if(isset($user->email) && $user->email !=""){
     $user_email  =  $user->email;
   }
@endphp
@endif
<div class="page-header">
    <div class="container">
        <div class="page-name">
            <h2>{{isset($page_title)?$page_title:''}}</h2>
            <div class="brd-bottom">&nbsp;</div>
            <p><a href="{{url('/')}}">Home</a> / <a href="{{url('/')}}/contact-us" class="act">{{isset($page_title)?$page_title:''}}</a></p>
        </div>
    </div>
</div>
<form name="contact-form" id="contact-form" method="POST" class="" action="{{url('/')}}/sendenquiry" enctype="multipart/form-data">
{{ csrf_field() }}
<div class="contact-form-section">
    <div class="container">
        <h2>{{ trans('contact_us.text_send_a') }} <span>{{ trans('contact_us.text_message') }}</span></h2>
        <p>{{ trans('contact_us.text_your_email_address_will_not_be_published_required_fields_are_marked') }}</p>
        <div class="contact-block">
            @include('front.layout._operation_status')
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>{{ trans('contact_us.text_name') }}<span class="astr"><i class="fa fa-asterisk"> </i></span></label>
                        <input value="{{isset($user_first_name) ? $user_first_name.' ':old('name')}}{{isset($user_last_name)?$user_last_name:''}}" placeholder="{{ trans('contact_us.text_enter_name') }}" type="text" data-rule-required="true" data-rule-maxlength="255" name="name" id="name" class="clint-input"   onkeyup="return chk_validation(this);"/>    
                        <div   class="error">{{ $errors->first('name') }}</div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>{{ trans('contact_us.text_email') }}<span class="astr"><i class="fa fa-asterisk"> </i></span></label>
                        <input value="{{isset($user_email) ? $user_email: old('email')}}" placeholder="{{ trans('contact_us.text_enter_email') }}" type="text" class="clint-input" name="email" id="email" data-rule-required="true" data-rule-email="true"/>
                        <span class='error'>{{ $errors->first('email') }}</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>{{ trans('contact_us.text_phone') }}<span class="astr"><i class="fa fa-asterisk"></i></span></label>
                        <input value="{{ old('contact_number') }}" placeholder="{{ trans('contact_us.text_enter_phone') }}" type="text" class="clint-input" name="contact_number" id="contact_number" data-rule-required="true" data-rule-number="true"  data-rule-minlength="10" data-rule-maxlength="12"   onkeyup="return chk_validation(this);"/>
                        <span class='error'>{{ $errors->first('contact_number') }}</span>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>{{ trans('contact_us.text_subject') }}<span class="astr"><i class="fa fa-asterisk"></i></span></label>
                        <input value="{{ old('enquiry_subject') }}" placeholder="{{ trans('contact_us.text_enter_subject') }}" type="text" class="clint-input" name="enquiry_subject" id="enquiry_subject" data-rule-required="true" data-rule-maxlength="250"  onkeyup="return chk_validation(this);" />
                        <span class='error'>{{ $errors->first('enquiry_subject') }}</span>
                    </div>
                </div>
            </div>

            @if(isset($user) && $user != null)
                <input type="hidden" name="report_user_id" id="report_user_id" value="{{$user->id}}">
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label>{{ trans('contact_us.text_report_a_problem') }} ({{ trans('contact_us.text_optional') }})</label>
                            <div class="droup-select input-bx">
                                 <select name="report_a_problem" id="report_a_problem" class="droup">
                                    <option value="">-- {{ trans('contact_us.text_select') }} --</option>
                                    <option value="Website Security">{{ trans('contact_us.text_website_security') }}</option>
                                    <option value="There is a bug on the website">{{ trans('contact_us.text_there_is_a_bug_on_the_website') }}</option>
                                    <option value="Expert report">{{ trans('contact_us.text_expert_report') }}</option>
                                    <option value="There is a suspicious activity">{{ trans('contact_us.text_there_is_a_suspicious_activity') }}</option>
                                    <option value="Other">{{ trans('contact_us.text_other') }}</option>
                                 </select>
                                 <div class="clr"></div>
                                 <span class='error' id="skill_name_error">{{ $errors->first('report_a_problem') }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="form-group">
                <label>{{ trans('contact_us.text_message') }}<span class="astr"><i class="fa fa-asterisk"></i></span></label>
                <textarea placeholder="{{ trans('contact_us.text_enter_message') }}" rows="" cols="" class="clint-input"  style="height:100px;" name="enquiry_message" id="enquiry_message" data-rule-required="true" >{{ old('enquiry_message') }}</textarea> 
                <span class='error'>{{ $errors->first('enquiry_message') }}</span>
            </div>
            <input type="hidden" name="recaptcha_response" id="recaptcha_response">
            {{-- <div class="g-recaptcha" data-sitekey="{{ config('app.project.RECAPTCHA_SITE_KEY') }}"></div>
            <span id="error-google-recaptcha" class="error"></span> --}}

            <button class="black-btn" type="button" onclick="validateContactForm(this);">{{ trans('contact_us.text_send_message') }}</button>
        </div>
    </div>
</div>
</form>
<iframe style="height:620px;width:100%;" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.it/maps?q={{isset($arr_settings['site_address'])?$arr_settings['site_address']:'Germany'}}&output=embed"></iframe>
<div class="clearfix"></div>
<div class="contact-info-section contact-form-section">
    <div class="container">
        <div class="col-sm-6 col-md-6 col-lg-6">
            <div class="contact-info-block text-center">
                <div class="contact-icon"><i class="fa fa-map-marker"></i></div>
                <h5>{{ trans('contact_us.text_our_location') }}</h5>
                <p>{{isset($arr_settings['site_address'])?$arr_settings['site_address']:''}}</p>
                <p>{{isset($arr_settings['site_email_address'])?$arr_settings['site_email_address']:''}}</p>
            </div>
        </div>
        <div class="col-sm-6 col-md-6 col-lg-6">
            <div class="contact-info-block text-center">
                <div class="contact-icon"><i class="fa fa-phone"></i></div>
                <h5>{{ trans('contact_us.text_contact_us') }}</h5>
                <p>Mobile: {{isset($arr_settings['site_contact_number'])?$arr_settings['site_contact_number']:''}}</p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyD8_ZbK-tv7P6IlJdv3DPrR-LHjVdTLZo0"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/additional-methods.min.js"></script>

{{-- <script src='https://www.google.com/recaptcha/api.js' async defer></script> --}}

<script src="https://www.google.com/recaptcha/api.js?render={{ config('app.project.RECAPTCHA_SITE_KEY') }}"></script>

<script type="text/javascript">
    var RECAPTCHA_SITE_KEY = "{{ config('app.project.RECAPTCHA_SITE_KEY') }}";
    $("#contact-form").validate({
        errorElement: 'span'
    });

    grecaptcha.ready(function() {
        grecaptcha.execute(RECAPTCHA_SITE_KEY, {action: 'contact'}).then(function(token) {
            $('#recaptcha_response').val(token);
        });
    });

    function validateContactForm(){
        if($("#contact-form").valid()){
            $("#contact-form").submit();
        }
        return false;
    }
</script>

<script type="text/javascript">
   // Function for special characters not accepts.
function chk_validation(ref){
   var yourInput = $(ref).val();
    re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
    var isSplChar = re.test(yourInput);
    if(isSplChar)
    {
      var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
      $(ref).val(no_spl_char);
    }
}
</script>
@stop