@extends('front.layout.master')                
@section('main_content')
   <div class="top-title-pattern">
      <div class="container">
         <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
             <div class="title-box-top"><h3>{{isset($page_title)?$page_title:''}}</h3></div> 
             <div class="top_breadcrumb"> <a href="{{url('/')}}">home</a> &nbsp;/&nbsp; <a href="" class="act">{{isset($page_title)?$page_title:''}} &nbsp;</a></div>
          </div>
       </div>
    </div>
 </div>
 <div class="middle-container privacy-bx">
   <div class="container">
      <div class="row">
           <div class="abt-head">
            <p>{!! isset($arr_page['page_desc'])?$arr_page['page_desc']:'' !!}</p>
         </div>
   </div>
</div>
</div>
@stop