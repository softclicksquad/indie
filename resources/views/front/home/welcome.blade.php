<!DOCTYPE html>
<html>
    <head>
        <title>{{ trans('new_translations.laravel')}}</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
                background-color: #878787; 
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
            

        </style>
    </head>
    <body>
        <div class="container">
            <div class="content" id="img-div">
                {{-- <div class="title">Laravel 5</div> --}}
                <p>
                    <img src="{{url('/public')}}/uploads/coming-soon-crop.jpg" border="1" style='width:100%;' alt="Null">
                </p>
            </div>
        </div>
    </body>
</html>
