@extends('front.layout.master')                
@section('main_content')
 <div class="top-title-pattern">
      <div class="container">
         <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
               <div class="title-box-top">
                  <h3>{{ trans('common/home.text_categories') }}</h3>
               </div>
               <div class="top_breadcrumb"> <a href="{{url('/')}}">{{ trans('auth/signup.text_home') }} </a> &nbsp;/&nbsp; <a href="javascript: void(0);" class="act">{{ trans('common/home.text_categories') }}</a></div>
            </div>
         </div>
      </div>
   </div>
   <div class="home-page">
    <div class="home-banner">
     <div class="offers-section">
      <div class="container">
        <h2>{!! trans('common/home.text_work_category') !!}</h2>
        <div class="row">
            @if(isset($arr_categories) && sizeof($arr_categories)>0)
                @foreach($arr_categories as $category)
                  <div class="offer-block-warpper">
                    <a class="offer-block" href="{{url('/experts/search/all/result?country=&expert_search_term=&category='.base64_encode($category['id']))}}">
                    <!-- <a class="offer-block" href="{{url('/projects/'.base64_encode($category['id']))}}"> -->
                      <div class="">
                            <?php if(isset($category['category_image']) && file_exists('public/uploads/admin/categories_images/'.$category['category_image'])){ ?>
                                    <img src="{{url('/public/'.config('app.project.img_path.category_image').$category['category_image'])}}">
                                  <?php } else { ?> 
                                    <img src="{{url('/public/front/images/not-found.png')}}" style="height:70px;width:70px">
                                  <?php } ?>
                                </div>
                      <span class="brd-box">&nbsp;</span>
                      @if(isset($category['category_title']) && strlen(($category['category_title'])) >20 ) @php $cat_dots='...'; @endphp @else @php $cat_dots = ''; @endphp @endif
                      <p title="{{ isset($category['category_title'])?$category['category_title']:'' }}">{{ isset($category['category_title'])?substr($category['category_title'],0,20).$cat_dots:'' }}</p>
                    </a>
                  </div>
                @endforeach
            @endif
        </div>
        @if(isset($arr_categories) && !empty($arr_categories) && $arr_categories != "")  
          <!-- Pagination Links -->
          <?php /* @include('front.common.pagination_view', ['paginator' => $arr_pagination]) */?>
          <!-- Pagination Links -->
        @endif
      </div>
     </div>
    </div>
  </div>
<div class="clearfix"></div>
@stop
