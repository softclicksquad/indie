@extends('front.layout.master')                
@section('main_content') 
<div class="middle-container">
   <div class="container">
      <br/>
      <?php 
         $sidebar_information= array();
         if (isset($arr_expert['user_id']))
         {
            $sidebar_information = sidebar_information($arr_expert['user_id']);
         }
      ?>
      <div class="row">
         <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="search-grey-bx">
               <div class="head_grn">{{ trans('expert/portfolio/portfolio.text_profile') }}</div>
               <div class="row">
                  <div class="col-sm-12 col-md-12 col-lg-12">
                     <div class="profil-man">
                        <div class="row">
                           <div class="col-sm-3 col-md-3 col-lg-3">
                              <div class="user-profile">
                                 <div class="profile-circle">
                                     @if(isset($arr_expert['user_details']['is_online']) && $arr_expert['user_details']['is_online']!='' && $arr_expert['user_details']['is_online']=='1')
                                      <div class="user-online-round"></div>
                                    @else
                                      <div class="user-offline-round"></div>
                                    @endif
                                    <div class="edit-hover">
                                       <li><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></li>
                                    </div>
                                    @if(isset($profile_img_public_path) && isset($arr_expert['profile_image']) && $arr_expert['profile_image']!=NULL  && file_exists('public/uploads/front/profile/'.$arr_expert['profile_image']))
                                    <img src="{{$profile_img_public_path.$arr_expert['profile_image']}}" alt=""/>
                                    @else
                                    <img src="{{$profile_img_public_path}}default_profile_image.png" alt="">
                                    @endif
                                 </div>
                              </div>
                              <div style="text-align:center;margin-bottom:15px">
                                 @if(isset($arr_expert['created_at']) && $arr_expert['created_at']!="")
                                 {{'Since '.date('M Y',strtotime($arr_expert['created_at'])).' on'}} <br/> {{config('app.project.name')}}
                                 @endif
                              </div>
                              <!-- <div style="text-align:center;margin-bottom:15px;" title="{{$sidebar_information['expert_timezone']}}">
                                 {{ trans('common/_expert_details.text_local_time') }} : {{$sidebar_information['expert_timezone_without_date']}}
                              </div> -->
                           </div>
                           <div class="col-sm-9 col-md-9 col-lg-9">
                              <div class="right-dashbord">
                                 <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                       <div class="head_grn">
                                          <?php 
                                             $expert_user_name = "---";
                                             $first_name       = isset($arr_expert['first_name']) ?$arr_expert['first_name']:"";
                                             $last_name        = isset($arr_expert['last_name']) ?substr($arr_expert['last_name'], 0,1).'.':"";
                                             $expert_user_name = $first_name.' '.$last_name;
                                          ?>
                                          {{ $expert_user_name or ''}}

                                      
                                            @if(isset($arr_expert['user_details']['kyc_verified']) && $arr_expert['user_details']['kyc_verified']!='' && $arr_expert['user_details']['kyc_verified'] == '1')
                                              <span class="verfy-new-arrow">
                                                  <a href="#" data-toggle="tooltip" title="Identity Verified"><img src="{{url('/public/front/images/verifild.png')}}" alt=""> </a>
                                              </span>
                                            @endif

                                       </div>

                                       <div class="address-mod-pro">
                                          <div class="icon-block"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                                          <div class="add-conte-block" onclick="searchByCountry(this)" 
                                             style="cursor: pointer;"   
                                             data-country-id="{{ isset($arr_expert['country'])?base64_encode($arr_expert['country']):'' }}">
                                             {{isset($arr_expert['country_details']['country_name'])?$arr_expert['country_details']['country_name']:'-'}}
                                          </div>
                                          <div class="clr"></div>
                                       </div>
                                       <div class="model-deceb-pro">
                                          <div class="icon-block"><i class="fa fa-info-circle" aria-hidden="true"></i></div>
                                          &nbsp;&nbsp;
                                          <div class="add-conte-block">{{ $arr_expert['about_me']  or '-'}}</div>
                                          <div class="clr"></div>
                                       </div>
                                       <div class="model-deceb-pro">
                                          <div class="icon-block"><i class="fa fa-graduation-cap" aria-hidden="true"></i></div>
                                          <div class="add-conte-block">
                                             @if(isset($arr_expert['expert_skills']) && sizeof($arr_expert['expert_skills'])>0)
                                             @foreach($arr_expert['expert_skills'] as $key => $exp_skill)
                                             @if(isset($exp_skill['skills']['skill_name']))
                                             @if(end($arr_expert['expert_skills'])==$exp_skill)
                                             <span style="cursor: pointer;font-size: 15px;" onclick="searchBySkill(this);" data-skill-id="{{ isset($exp_skill['skill_id'])?base64_encode($exp_skill['skill_id']):'' }}">{{$exp_skill['skills']['skill_name']}}</span>
                                             @else
                                             <span style="cursor: pointer;font-size: 15px;" onclick="searchBySkill(this);" data-skill-id="{{ isset($exp_skill['skill_id'])?base64_encode($exp_skill['skill_id']):'' }}">{{$exp_skill['skills']['skill_name'].','}}</span>
                                             @endif
                                             @endif
                                             @endforeach
                                             @endif
                                          </div>
                                          <div class="clr"></div>
                                       </div>
                                       <?php
                                          $arr_lang = [];
                                          
                                          if(isset($arr_expert['expert_spoken_languages']) && count($arr_expert['expert_spoken_languages']) > 0)
                                          {
                                             foreach ($arr_expert['expert_spoken_languages'] as $_key => $language) 
                                             {
                                                $arr_lang[] = $language['language'];
                                             }
                                          }
                                          
                                          ?>
                                       <div class="model-deceb-pro">
                                          <div class="icon-block"><i class="fa fa-language" aria-hidden="true"></i></div>
                                          &nbsp;&nbsp;
                                          <div class="add-conte-block">
                                             @if(count($arr_lang)>0)
                                             @foreach($arr_lang as $language)
                                             <span onclick="searchByLanguage(this)" 
                                                style="cursor: pointer;"   
                                                data-language="{{ isset($language)?$language:'' }}">
                                             {{isset($language)?$language:''}}@if($language != array_last($arr_lang)),@endif</span>
                                             @endforeach
                                             @endif
                                          </div>
                                          <div class="clr"></div>
                                       </div>
                                       
                                       <div class="model-deceb-pro">
                                          <div class="icon-block"><i class="fa fa-star" aria-hidden="true"></i></div>
                                          <div class="rate-t">
                                             <div class="rating-list"><span class="stars">{{isset($sidebar_information['average_rating'])?$sidebar_information['average_rating']:'0'}}</span></div>
                                             ({{isset($sidebar_information['average_rating'])?$sidebar_information['average_rating']:'0'}})
                                          </div>
                                          <div class="clr"></div>
                                       </div>
                                       <div class="model-deceb-pro">
                                          <div class="icon-block"><i  class="fa fa-clock-o" aria-hidden="true"></i></div>
                                             <span >
                                                @if($sidebar_information['last_login_duration'] == 'Active')
                                                   <a style="color:#4D4D4D;text-transform:none;cursor:text;" title="{{isset($sidebar_information['expert_timezone'])?$sidebar_information['expert_timezone']:'Not specify'}}">
                                                      <span class="flag-image"> 
                                                            @if(isset($sidebar_information['user_country_flag'])) 
                                                              @php 
                                                                 echo $sidebar_information['user_country_flag']; 
                                                              @endphp 
                                                            @elseif(isset($sidebar_information['user_country'])) 
                                                              @php 
                                                                echo $sidebar_information['user_country']; 
                                                              @endphp 
                                                            @else 
                                                              @php 
                                                                echo 'Not Specify'; 
                                                              @endphp 
                                                            @endif 
                                                         </span>
                                                      {{-- trans('common/_expert_details.text_local_time') --}}  {{isset($sidebar_information['expert_timezone_without_date'])?$sidebar_information['expert_timezone_without_date']:'Not specify'}}
                                                   </a>
                                                @else
                                                   <span class="flag-image"> 
                                                      @if(isset($sidebar_information['user_country_flag'])) 
                                                        @php 
                                                           echo $sidebar_information['user_country_flag']; 
                                                        @endphp 
                                                      @elseif(isset($sidebar_information['user_country'])) 
                                                        @php 
                                                          echo $sidebar_information['user_country']; 
                                                        @endphp 
                                                      @else 
                                                        @php 
                                                          echo 'Not Specify'; 
                                                        @endphp 
                                                      @endif 
                                                   </span>
                                                   <a style="color:#4D4D4D;text-transform:none;cursor:text;" title="{{$sidebar_information['last_login_full_duration']}}">
                                                      <span  title="{{$sidebar_information['last_login_full_duration']}}">{{$sidebar_information['last_login_duration']}}</span>  
                                                   </a>
                                                @endif
                                             </span>
                                          <div class="clr"></div>
                                       </div>
                                    </div>
                                 </div>
                                 <hr/>
                                 <div class="clr"></div>
                                  <div class="rating-bx">
                                     <div class="img-pro infg"><img src="{{url('/public')}}/front/images/project-inc.png" alt="" /></div>
                                     <div class="info-r-pro">
                                        <div align="center">{{isset($sidebar_information['completed_project_count'])?$sidebar_information['completed_project_count']:'0'}}</div>
                                        <span>{{ trans('common/expert_sidebar.text_completed_projects') }}</span>
                                     </div>
                                  </div>
                                  <div class="rating-bx">
                                     <div class="img-pro infg"><img src="{{url('/public')}}/front/images/project-inc.png" alt="" /></div>
                                     <div class="info-r-pro">
                                        <div align="center">{{isset($sidebar_information['ongoing_project_count'])?$sidebar_information['ongoing_project_count']:'0'}}</div>
                                        <span>{{ trans('common/expert_sidebar.text_ongoing_projects') }}</span>
                                     </div>
                                  </div>
                                  <div class="rating-bx">
                                     <div class="img-pro infg"><img src="{{url('/public')}}/front/images/reviews-inc.png" alt="" /></div>
                                     <div class="info-r-pro">
                                        <div align="center">{{isset($sidebar_information['review_count'])?$sidebar_information['review_count']:'0'}}</div>
                                        <span>
                                        @if(isset($sidebar_information['review_count']) && $sidebar_information['review_count'] == 1 )
                                        {{str_singular(trans('expert/portfolio/portfolio.text_review'))}} 
                                        @else 
                                        {{ trans('expert/portfolio/portfolio.text_review') }} 
                                        @endif
                                        </span>
                                     </div>
                                  </div>
                                  <div class="rating-bx">
                                     <div class="img-pro infg"><img src="{{url('/public')}}/front/images/rating-inc.png" alt="" /></div>
                                     <div class="info-r-pro">
                                        <div align="center">{{isset($sidebar_information['reputation'])?$sidebar_information['reputation']:'0.0'}}%</div>
                                        <span>{{ trans('expert/portfolio/portfolio.text_reputation') }}</span>
                                     </div>
                                  </div>
                                  <div class="rating-bx">
                                        <div class="img-pro infg"><i style="font-size: 1.7em;" class="fa fa-trophy"></i></div>
                                        <div class="info-r-pro">
                                           <div align="center" >{{isset($sidebar_information['contest_win_count'])?$sidebar_information['contest_win_count']:'0'}}</div>
                                           <span>{{trans('expert/portfolio/portfolio.text_won_contests')}}</span>
                                        </div>
                                  </div>
                              </div>
                           </div>
                        </div>
                        <div class="clr"></div>
                     </div>
                     <div class="row listing-detadfs">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                           <div class="main-tbs">
                              <!-- Details tabs -->
                              <ul class="nav nav-tabs">
                                 <li><a href="{{url('/')}}/experts/portfolio/{{base64_encode($arr_expert['user_id'])}}">{{ trans('expert/portfolio/portfolio.text_portfolio') }}</a></li>
                                 <li class="active"><a href="#reviews" aria-controls="reviews" data-toggle="tab">
                                    @if(isset($review_cnt)&& sizeof($review_cnt)>0) {{$review_cnt}} @else 0 @endif 
                                    @if(isset($review_cnt) && $review_cnt == 1)
                                    {{str_singular(trans('expert/portfolio/portfolio.text_review'))}} 
                                    @else 
                                    {{ trans('expert/portfolio/portfolio.text_review') }} 
                                    @endif
                                    </a>
                                 </li>
                              </ul>
                              <!-- Details panes -->
                              <div class="tab-content">
                                 <!--tab 2-->

                                 <div class="tab-pane" id="reviews">
                                    <div class="customer-review-port">
                                       @if(isset($arr_review['data']) && sizeof($arr_review['data'])>0)
                                       @foreach($arr_review['data'] as $review)
                                       <?php 
                                            $both_review_available = 2; //please remove this line
                                         // $both_review_available = check_both_review_available($review['project_id']);

                                          $user_name = "---"; 
                                          if(isset($review['client_details']['user_id']) && $review['client_details']['user_id'] != "")
                                          {
                                             $first_name  = isset($review['from_user_info']['first_name']) ?$review['from_user_info']['first_name']:"";
                                             $last_name   = isset($review['from_user_info']['last_name']) ?substr($review['from_user_info']['last_name'], 0,1).'.':"";
                                             $user_name   = $first_name.' '.$last_name; 
                                            
                                          }
                                          ?>
                                          @if(isset($both_review_available) && $both_review_available == '2')
                                           <div class="media">
                                              <div class="media-left">
                                                 <a href="#">
                                                @if($review['project_details']['project_type']=='0')
                                                 @if(isset($profile_img_public_path) && isset($review['client_details']['profile_image']) && $review['client_details']['profile_image']!="" && file_exists('public/uploads/front/profile/'.$review['client_details']['profile_image']))
                                                 <img class="media-object img-circle" src="{{$profile_img_public_path.$review['client_details']['profile_image']}}" alt="" />
                                                 @else
                                                 <img src="{{$profile_img_public_path.'no_gender.png'}}" alt="">
                                                 @endif
                                                @else
                                                   <img src="{{$profile_img_public_path.'Private.jpg'}}" alt="">
                                                @endif
                                                 </a>
                                              </div>
                                              <div class="media-body">
                                                  @if($review['project_details']['project_type']=='0')

                                                    <h5 class="media-heading">{{$user_name or ''}}</h5>

                                                  @endif

                                                 <h3 style="color:#289BDD;padding: 5px 0;" class="media-heading">

                                                    @if($review['project_details']['project_type']=='0')
                                                    @if(isset($review['project_details']['id']) && isset($review['project_details']['project_name']))
                                                    
                                                    {{isset($review['project_details']['project_name'])?$review['project_details']['project_name']:''}}
                                                    
                                                    @endif
                                                    @else
                                                      {{isset($review['project_details']['project_name'])?$review['project_details']['project_name']:''}}
                                                    @endif

                                                    </h3>
                                                 {{isset($review['review'])?str_limit($review['review'],$limit = 200 ,$end='...'):''}}</h4>
                                                 <div class="rating-list dpc-blc">
                                                    @if(isset($review['rating']))  
                                                    <span class="stars">{{trim($review['rating'])}}</span>
                                                    @endif
                                                    @if(isset($review['created_at']))
                                                    <?php $yrdata= strtotime($review['created_at']);?>
                                                    <p>{{date('d M Y', $yrdata)}}</p>
                                                    @endif
                                                 </div>
                                              </div>
                                           </div>
                                          @endif
                                       @endforeach
                                       @else
                                       <br/>
                                       <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;" align="center">
                                          <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                                             <div class="search-content-block">
                                                <div class="search-head" style="color:color:rgba(45, 45, 45, 0.8);" align="center">
                                                   {{ trans('expert/portfolio/portfolio.text_no_reviews_found') }}
                                                </div>
                                             </div>
                                          </td>
                                       </tr>
                                       @endif
                                    </div>
                                    <div class="clearfix"></div>
                                    @if(isset($arr_review['data']) && count($arr_review['data'])>0)
                                    <?php 
                                       $arr_pagination = $arr_review_links;
                                       ?>
                                    
                                    @include('front.common.pagination_view', ['paginator' => $arr_pagination])
                        
                                    @endif
                                 </div>
                                 <!--tab 2-->
                              </div>
                              <!-- Details tabs End --> 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<script type="text/javascript">
   $( document ).ready(function() {
   jQuery('#reviews').show();
   });
   function searchBySkill(ref) 
   {
      var skill_id = $(ref).attr('data-skill-id');
         
      if(skill_id != "")
      {
         window.location.href="{{url('/experts/search_skill')}}/"+skill_id;
      }
      else   
      {
         console.log('No skill information available.');
      }
    
   }
   function searchByCountry(ref) 
   {
      var country_id = $(ref).attr('data-country-id');
         
      if(country_id != "")
      {
         window.location.href="{{url('/experts/search_country')}}/"+country_id;
      }
      else   
      {
         console.log('No country information available.');
      }
    
   }
   function searchByLanguage(ref) 
   {
      var language = $(ref).attr('data-language');
         
      if(language != "")
      {
         window.location.href="{{url('/experts')}}?q="+language;
      }
      else   
      {
         console.log('No language information available.');
      }
   }
   
</script>
<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
@stop