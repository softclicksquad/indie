@extends('front.layout.master')                
@section('main_content') 
@php 
    $user = \Sentinel::check();
    $profile_img_public_path = url('/public').config('app.project.img_path.profile_image');
@endphp
<style type="text/css">
.footer{display: none;}
.user-details {width: 170px;background-color: #fff;padding: 0 0 0 20px;border-radius: 3px;text-align: center;}
.user-img {width: 75px;height: 75px;border-radius: 50%;margin: 0 auto 15px;box-shadow: 0 2px 6px rgba(0,0,0,0.2);background-color: #fff;
padding: 3px;border: 1px solid #e6e3e3;}
.user-info .name a {color: #2d2d2d;}    
.user-img img {border-radius: 50%;width: 100%;height: 100%;}
.user-info {font-size: 12px;line-height: 19px;}
.user-info i {display: inline-block;font-size: 16px;}
.user-info p:last-child(){margin: 0;}
.user-info .name{font-weight: 500;font-size: 15px;}
</style>

<div class="expert-listing-wrapper">
   <div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 col-md-7 col-lg-7">
           <div class="expert-left-section content-d">
            <form class="frm-subject select-block" action="{{$module_url_path}}/search/all/result" method="GET">
                <div class="search-filters">
                    <div class="row">
                        <?php
                          $search_country_id = $search_skill_id = $search_skill_name = "";
                          if(\Request::segment(2) == "search_country"){  
                             $search_country_id = \Request::segment(3);
                             if($search_country_id != "" ){
                                $search_country_id = base64_decode($search_country_id);
                             }
                          } else if(\Request::segment(2) == "search_skill"){
                             $search_skill_id = \Request::segment(3);
                             if($search_skill_id != "" ){
                                $search_skill_id = base64_decode($search_skill_id);
                                $obj_skill = \App\Models\SkillsModel::where('id',$search_skill_id)->first();   
                                if($obj_skill){
                                   $search_skill_name =  $obj_skill->skill_name;  
                                }
                             }   
                          }
                        ?>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="input-box">
                                <div class="search-icon bg-img">&nbsp;</div>
                                <input class="search-input" name="expert_search_term" id="expert_search_term" value="@if($search_skill_name != ""){{$search_skill_name}}@elseif(\Request::input('q') != ""){{\Request::input('q')}}@else{{\Request::input('expert_search_term')}}@endif" placeholder="{{trans('expert_listing/listing.placeholder_search_experts')}}" type="text"/>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="input-box">
                              <select class="select-box select-locations" name="country" id="country">
                                <option value="">-- {{trans('expert_listing/listing.text_search_country')}} --</option>
                                @if(isset($arr_country) && sizeof($arr_country))
                                  @foreach($arr_country as $country)
                                    @if(isset($country_id) && $country_id==$country['id'])
                                      <option  value="{{isset($country['id'])?$country['id']:''}}" {{'selected=selected'}}>{{isset($country['country_name'])?$country['country_name']:''}} ({{count($country['country_details'])}})</option>
                                    @elseif($search_country_id != "" && $search_country_id==$country['id'])
                                      <option  value="{{isset($country['id'])?$country['id']:''}}" {{'selected=selected'}}>{{isset($country['country_name'])?$country['country_name']:''}} ({{count($country['country_details'])}})</option>
                                    @else
                                      <option value="{{isset($country['id'])?$country['id']:''}}">{{isset($country['country_name'])?$country['country_name']:''}} ({{count($country['country_details'])}})</option>
                                    @endif
                                  @endforeach
                                @endif
                              </select>
                            </div>
                        </div>
                        <?php
                            $search_categories_id = "";
                            if(isset($_REQUEST['category']) && $_REQUEST['category'] != "")
                            {  
                               $search_categories_id = $_REQUEST['category'];
                               if($search_categories_id != "" ) 
                               {
                                  $search_categories_id = base64_decode($search_categories_id);
                               }
                            }
                        ?>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="input-box">
                                 <select class="select-box " name="category" id="category" onchange="return getSubCategory(this);">
                                    <option value="">-- {{ trans('common/home.text_categories') }} --</option>
                                     @if(isset($arr_categories) && sizeof($arr_categories)>0)
                                        @foreach($arr_categories as $categories)
                                           @if(isset($categories_id) && $categories_id==$categories['id'])
                                           <option  value="{{isset($categories['id'])?base64_encode($categories['id']):''}}" {{'selected=selected'}}>{{isset($categories['category_title'])?$categories['category_title']:''}} ({{count($categories['expert_categories'])}})</option>
                                           @elseif($search_categories_id != "" && $search_categories_id==$categories['id'])
                                           <option  value="{{isset($categories['id'])?base64_encode($categories['id']):''}}" {{'selected=selected'}}>{{isset($categories['category_title'])?$categories['category_title']:''}} ({{count($categories['expert_categories'])}})</option>
                                           @else
                                           <option value="{{isset($categories['id'])?base64_encode($categories['id']):''}}">{{isset($categories['category_title'])?$categories['category_title']:''}} ({{count($categories['expert_categories'])}})</option>
                                           @endif
                                        @endforeach
                                     @endif
                                 </select>
                            </div>
                        </div>

                        <?php
                            $search_sub_categories_id = "";
                            if(isset($_REQUEST['subcategory']) && $_REQUEST['subcategory'] != "")
                            {  
                               $search_sub_categories_id = $_REQUEST['subcategory'];
                               if($search_sub_categories_id != "" ) 
                               {
                                  $search_sub_categories_id = $search_sub_categories_id;
                               }
                            }
                        ?>

                        <div class="col-sm-12  col-md-6 col-lg-6">
                            <div class="input-box" id="subcategory_div">
                                        
                                           <select class="select-box " name="subcategory" id="subcategory"  value=""  data-rule-required="false">
                                           </select>
                                        
                                  <br />
                            </div>
                         </div>

                        <?php
                            $search_rating = "";
                            if(isset($_REQUEST['rating']) && $_REQUEST['rating'] != "")
                            {  
                               $search_rating = $_REQUEST['rating'];
                               if($search_rating != "" ) 
                               {
                                  $search_rating = $search_rating;
                               }
                            }
                        ?>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="input-box">
                                 <select class="select-box" id="rating" name="rating">
                                    <option value="">-- {{trans('expert_listing/listing.text_select_rating')}} --</option>
                                    <option value="1-2" @if(isset($search_rating) && $search_rating == '1-2') selected='selected' @endif>1★ & above</option>
                                    <option value="2-3" @if(isset($search_rating) && $search_rating == '2-3') selected='selected' @endif>2★ & above</option>
                                    <option value="3-4" @if(isset($search_rating) && $search_rating == '3-4') selected='selected' @endif>3★ & above</option>
                                    <option value="4-5" @if(isset($search_rating) && $search_rating == '4-5') selected='selected' @endif>4★ & above</option>
                                 </select>
                            </div>
                        </div>
                    </div>
                    <?php
                            $search_lang_name = "";
                            if(isset($_REQUEST['lang']) && $_REQUEST['lang'] != "")
                            {  
                               $search_lang_name = $_REQUEST['lang'];
                               if($search_lang_name != "" ) 
                               {
                                  $search_lang_name = $search_lang_name;
                               }
                            }
                        ?>
                    <div class="advance-filter" 
                         @if(isset($_GET['lang']) && $_GET['lang'] != "" || 
                                   isset($_GET['h_rate_min_val']) && $_GET['h_rate_min_val'] != "" || 
                                   isset($_GET['h_rate_max_val']) && $_GET['h_rate_max_val'] != "" ) style="display:block;" @else style="display:none;" @endif 
                         >
                        <div class="row">
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <div class="input-box">
                                     <select class="select-box" id="" name="lang">
                                         <option value="">-- {{ trans('client/profile/profile.text_spoken_languages') }} --</option>
                                         @if(isset($arr_lang) && sizeof($arr_lang)>0)
                                            @foreach($arr_lang as $lang)
                                               @if(isset($search_lang_name) && $search_lang_name==$lang['language'])
                                               <option  value="{{isset($lang['language'])?$lang['language']:''}}" {{'selected=selected'}}>{{isset($lang['language'])?$lang['language']:''}} ({{count($lang['expert_spoken_language'])}})</option>
                                               @elseif($search_lang_name != "" && $search_lang_name==$lang['language'])
                                               <option  value="{{isset($lang['language'])?$lang['language']:''}}" {{'selected=selected'}}>{{isset($lang['language'])?$lang['language']:''}} ({{count($lang['expert_spoken_language'])}})</option>
                                               @else
                                               <option value="{{isset($lang['language'])?$lang['language']:''}}">{{isset($lang['language'])?$lang['language']:''}} ({{count($lang['expert_spoken_language'])}})</option>
                                               @endif
                                            @endforeach
                                         @endif
                                     </select>
                                </div>
                            </div>
                             <?php
                                $search_profession_id = "";
                                if(isset($_REQUEST['profession']) && $_REQUEST['profession'] != "")
                                {  
                                   $search_profession_id = $_REQUEST['profession'];
                                   if($search_profession_id != "" ) 
                                   {
                                      $search_profession_id = base64_decode($search_profession_id);
                                   }
                                }
                            ?>
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <div class="input-box">
                                     <select class="select-box " name="profession" id="profession">
                                        <option value="">-- {{ trans('client/profile/profile.text_occupation') }} --</option>
                                         @if(isset($arr_professions) && sizeof($arr_professions)>0)
                                            @foreach($arr_professions as $profession)
                                               @if(isset($profession_id) && $profession_id==$profession['id'])
                                               <option  value="{{isset($profession['id'])?base64_encode($profession['id']):''}}" {{'selected=selected'}}>{{isset($profession['profession_title'])?$profession['profession_title']:''}} ({{count($profession['expert_profession'])}})</option>
                                               @elseif($search_profession_id != "" && $search_profession_id==$profession['id'])
                                               <option  value="{{isset($profession['id'])?base64_encode($profession['id']):''}}" {{'selected=selected'}}>{{isset($profession['profession_title'])?$profession['profession_title']:''}} ({{count($profession['expert_profession'])}})</option>
                                               @else
                                               <option value="{{isset($profession['id'])?base64_encode($profession['id']):''}}">{{isset($profession['profession_title'])?$profession['profession_title']:''}} ({{count($profession['expert_profession'])}})</option>
                                               @endif
                                            @endforeach
                                         @endif
                                     </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <div class="range-t input-bx" for="amount" style="width: 95.5%;margin-left: 10px;">
                                     <p>{{trans('expert_listing/listing.text_hourly_rate')}}</p>
                                     <div id="slider-price-range" class="slider-rang"></div>
                                     <div class="amount-no" id="slider_price_range_txt"></div>
                                     <input type="hidden" name="h_rate_min_val" id="h_rate_min_val" value="<?php echo $min_hourly_rate; ?>">
                                     <input type="hidden" name="h_rate_max_val" id="h_rate_max_val" value="<?php if(isset($_GET['h_rate_max_val']) && $_GET['h_rate_max_val'] != "") { echo $_GET['h_rate_max_val']; } else { echo $max_hourly_rate; } ?>">
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="search-btns">
                        @if(isset($_GET) && sizeof($_GET) > 0 || !empty(\Request::segment(2)) && !empty(\Request::segment(3)) && \Request::segment(2) == "search_skill" &&  \Request::segment(3) !="") 
                          <a href="{{$module_url_path}}" class="black-border-btn inline-btn ">{{trans('expert_listing/listing.text_reset_filter')}}</a>
                        @else
                          <a id="adv-filter" href="javascript:void(0)" class="black-border-btn inline-btn chg-width">{{trans('expert_listing/listing.text_advanced')}} {{trans('expert_listing/listing.text_search')}}</a>
                        @endif
                        <button id="btn_search" type="submit" class="black-btn inline-btn show_loader">{{trans('expert_listing/listing.text_search')}}</button>
                    </div>
                </div>
            </form>
            <div class="listing-section">
               <div class="result-section">
                  <form id="frm_show_cnt" class="form-horizontal show-entry-form" method="POST" action="{{$module_url_path}}/show_cnt">
                     {{ csrf_field() }}
                      <div class="row">
                          <div class="col-sm-6 col-md-6 col-lg-6">

                               @if(isset($arr_experts['data']) && (sizeof($arr_experts['data'])>0)) 
                               <p>{{trans('expert_listing/listing.text_showing')}} <b>{{sizeof($arr_experts['data'])}}</b> {{trans('expert_listing/listing.text_results')}}</p>
                               @else
                               <p>{{trans('expert_listing/listing.text_showing')}} <b>0</b> {{trans('expert_listing/listing.text_results')}}</p>
                               @endif
                          </div>
                          <div class="col-sm-6 col-md-6 col-lg-6">
                             <div class="sort-by">
                               <label>{{trans('expert_listing/listing.text_show')}}:</label>
                                  @php $show_cnt ='show_expert_listing_cnt';  @endphp
                                  @include('front.common.show-cnt-selectbox')
                                </div>
                          </div>
                      </div>
                  </form> 
                </div>
              @include('front.layout._operation_status')  
              <input type="hidden" value="{{\Request::input('q')}}" id="url_language"></input>
                 @if(isset($arr_experts['data']) && (sizeof($arr_experts['data'])>0)) 
                  @foreach($arr_experts['data'] as $key => $expert)
                    @if(isset($expert['user_id']) && $expert['user_details']['is_active']=='1')
                      <?php
                        $arr_profile_data = [];   
                        $arr_profile_data = sidebar_information($expert['user_id']);
                      ?> 
                      <div class="exp-list-block @if($key == 0) active @endif active-expert{{$key}} other-active-expert">
                          <!-- <a href="javascript:void(0)" class="mail-block" style="margin-right: 43px;"><i class="fa fa-envelope-o" aria-hidden="true"></i></a> -->
                          @if(isset($expert['latitude']) && $expert['latitude'] != null && isset($expert['longitude']) && $expert['longitude'] != null)
                          <a href="javascript:void(0)" class="mail-block highlight-marker active-expert{{$key}}  other-active-expert @if($key == 0) active @endif" map-index="{{$key}}" map-lat="{{$expert['latitude']}}" map-long="{{$expert['longitude']}}" title="View on map"><i class="fa fa-map-marker" aria-hidden="true"></i></a>
                          @endif
                          <div class="exp-img" >
                            <div class="listin-pro-image">
                            @if(isset($expert['user_details']['is_online']) && $expert['user_details']['is_online']!='' && $expert['user_details']['is_online']=='1')
                              <div class="user-online-round"></div>
                            @else
                              <div class="user-offline-round"></div>
                            @endif

                              <!-- <img src="{{url('/public/front/images/Applied3.jpg')}}" class="img-responsive"/> -->
                              @if(isset($profile_img_public_path) && isset($expert['profile_image']) && $expert['profile_image']!=NULL && file_exists('public/uploads/front/profile/'.$expert['profile_image']))
                              <img src="{{$profile_img_public_path.$expert['profile_image']}}" class="img-responsive" alt="">
                              @else
                              <img src="{{$profile_img_public_path}}default_profile_image.png" class="img-responsive" alt="">
                              @endif
                            </div>
                          </div>
                          <div class="exp-details">
                             <div class="row">
                              <div class="col-sm-12 col-md-12 col-lg-8">
                                  <div class="exp-name">
                                      <h5>
                                       <a href="{{ url('/')}}/experts/portfolio/{{base64_encode($expert['user_id'])}}">
                                          {{isset($expert['user_details']['role_info']['first_name'])?$expert['user_details']['role_info']['first_name']:'Unknown user'}} 
                                          @if(isset($expert['user_details']['role_info']['last_name']) && $expert['user_details']['role_info']['last_name'] !="") @php echo substr($expert['user_details']['role_info']['last_name'],0,1).'.'; @endphp @endif

                                       </a>
                                      </h5>
                                      @if($arr_profile_data['last_login_duration'] == 'Active')
                                         <span class="flag" title="{{isset($arr_profile_data['expert_timezone'])?$arr_profile_data['expert_timezone']:'Not Specify'}}">
                                            <span class="flag-image"> 
                                                @if(isset($arr_profile_data['user_country_flag'])) 
                                                  @php 
                                                     echo $arr_profile_data['user_country_flag']; 
                                                  @endphp 
                                                @elseif(isset($arr_profile_data['user_country'])) 
                                                  @php 
                                                    echo $arr_profile_data['user_country']; 
                                                  @endphp 
                                                @else 
                                                  @php 
                                                    echo 'Not Specify'; 
                                                  @endphp 
                                                @endif 
                                            </span>
                                            {{-- trans('common/_expert_details.text_local_time') --}}  {{isset($arr_profile_data['expert_timezone_without_date'])?$arr_profile_data['expert_timezone_without_date']:'Not Specify'}}
                                         </span>
                                      @else
                                         <span class="" title="{{$arr_profile_data['last_login_full_duration']}}">
                                            <span class="flag-image"> 
                                                @if(isset($arr_profile_data['user_country_flag'])) 
                                                  @php 
                                                     echo $arr_profile_data['user_country_flag']; 
                                                  @endphp 
                                                @elseif(isset($arr_profile_data['user_country'])) 
                                                  @php 
                                                    echo $arr_profile_data['user_country']; 
                                                  @endphp 
                                                @else 
                                                  @php 
                                                    echo 'Not Specify'; 
                                                  @endphp 
                                                @endif 
                                            </span>
                                            <!-- <i  class="fa fa-clock-o" aria-hidden="true"></i> -->
                                            <a style="color:#4D4D4D;font-size: 14px;text-transform:none;cursor:text;">
                                               {{$arr_profile_data['last_login_duration']}}
                                            </a>
                                         </span>
                                      @endif
                                      
                                      @if(isset($expert['user_details']['kyc_verified']) && $expert['user_details']['kyc_verified']!='' && $expert['user_details']['kyc_verified'] == '1')
                                        <span class="verfy-new-arrow">
                                            <a href="#" data-toggle="tooltip" title="Identity Verified"><img src="{{url('/public/front/images/verifild.png')}}" alt=""> </a>
                                        </span>
                                      @endif


                                  </div>
                                  <div class="exp-rating">
                                      <div class="designation" title="{{isset($expert['profession_details']['profession_title'])?$expert['profession_details']['profession_title']:'Not specified'}}">
                                        @php $adddots = ''; @endphp
                                        @if(isset($expert['profession_details']['profession_title']) && strlen($expert['profession_details']['profession_title']) > 13) @php $adddots = '..'; @endphp  @endif
                                        {{isset($expert['profession_details']['profession_title'])?substr($expert['profession_details']['profession_title'],0,13).$adddots:'Not specified'}}
                                      </div>
                                      <div class="avg-rating">
                                          <div class="avg">{{$arr_profile_data['average_rating'] or '0.0'}}</div>
                                          <ul>
                                              <span class="stars">{{$arr_profile_data['average_rating'] or '0'}}</span>
                                          </ul>
                                      </div>
                                  </div>
                                  <div class="location-block">
                                      <p><i class="fa fa-suitcase"></i> {{isset($arr_profile_data['completed_project_count'])?$arr_profile_data['completed_project_count']:'0'}} {{ucfirst(strtolower(trans('expert_listing/listing.text_projects')))}} <span>|</span> <i class="fa fa-map-marker"></i> {{isset($expert['city_details']['city_name'])?str_limit($expert['city_details']['city_name'],$limit = 20, $end='...').',':''}} {{isset($expert['country_details']['country_name'])?str_limit($expert['country_details']['country_name'],$limit = 20, $end='...'):''}}</p>
                                  </div>

                                  <div class="category-new-all">
                                      <ul> <!-- skill_id -->
                                          @if(isset($expert['expert_categories']) && sizeof($expert['expert_categories'])>0)
                                            @foreach($expert['expert_categories'] as $category)
                                              @php
                                                $search_category_title = "";
                                                $obj_category = \App\Models\CategoriesModel::where('id',$category['category_id'])->first();   
                                                if($obj_category)
                                                {
                                                   $search_category_title =  $obj_category->category_title;  
                                                }
                                              @endphp
                                              <li title="{{$search_category_title}}">{{$search_category_title}}</li>     
                                            @endforeach   
                                          @endif
                                      </ul>
                                  </div>


                                  <div class="skills-block">
                                      <ul> <!-- skill_id -->
                                          @if(isset($expert['expert_skills']) && sizeof($expert['expert_skills'])>0)
                                            @foreach($expert['expert_skills'] as $skill)
                                              @php
                                                $search_skill_name = "";
                                                $obj_skill = \App\Models\SkillsModel::where('id',$skill['skill_id'])->first();   
                                                if($obj_skill){
                                                   $search_skill_name =  $obj_skill->skill_name;  
                                                }
                                              @endphp
                                              <li title="{{$search_skill_name}}">{{$search_skill_name}}</li> 
                                            @endforeach   
                                          @endif
                                      </ul>
                                  </div>
                             </div>


                             <div class="col-sm-12 col-md-12 col-lg-4 pl-0">
                              <div class="exp-rate">
                                  <h5>{{config('app.currency_symbol')}} {{isset($expert['hourly_rate'])?$expert['hourly_rate']:'0.0'}}</h5>
                                  <p>Hourly Rate</p>
                                  <div class="search-btns">
                                      @if(isset($user) && $user != null && $user->inRole('client'))
                                      <a href="{{url('/client/projects/hire/'.base64_encode($expert['user_id']))}}" class="black-border-btn inline-btn">Hire</a>
                                      @endif
                                      @if(isset($user) && $user != null && $user->inRole('project_manager'))
                                      <a href="javascript:void(0)" 
                                         class="black-border-btn inline-btn applozic-launcher"
                                         data-mck-id="{{env('applozicUserIdPrefix')}}{{isset($expert['user_id'])?$expert['user_id']:'0'}}" 
                                         data-mck-name="{{isset($expert['first_name'])?$expert['first_name']:'Unknown user'}} 
                                         @if(isset($expert['last_name']) && $expert['last_name'] !="") @php echo substr($expert['last_name'],0,1).'.'; @endphp @endif"
                                      >Chat</a>
                                      @endif
                                      @if(isset($user) && $user != null && $user->inRole('client'))
                                      <div class="clearfix"></div>
                                      <a href="#invite_member" 
                                         data-keyboard="false" 
                                         data-src="@if(isset($profile_img_public_path) && isset($expert['profile_image']) && $expert['profile_image']!=NULL && file_exists('public/uploads/front/profile/'.$expert['profile_image']))
                                         {{$profile_img_public_path.$expert['profile_image']}}
                                         @else
                                         {{$profile_img_public_path.'default_profile_image.png'}}
                                         @endif"
                                         data-name="{{isset($expert['user_details']['role_info']['first_name'])?$expert['user_details']['role_info']['first_name']:'Unknown user'}}" 
                                         data-email="{{$expert['user_details']['email']}}" 
                                         data-backdrop="static" 
                                         data-expert-id="{{isset($expert['user_id'])?base64_encode($expert['user_id']):''}}" 
                                         data-toggle="modal" class="black-btn invite-expert inline-btn">
                                         {{trans('client/projects/invite_experts.text_invite_expert')}}
                                      </a> 
                                      @endif
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                    @endif
                  @endforeach  
                @else
                <br/>
                <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;" align="center">
                    <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                       <div class="search-content-block">
                          <div class="no-record">
                             {{ trans('expert_listing/listing.text_sorry_no_records_found') }}
                          </div>
                       </div>
                    </td>
                </tr>
                @endif
                <!--pagigation start here-->
                @include('front.common.pagination_view', ['paginator' => $arr_experts])
                <!--pagigation end here-->
            </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-5 col-lg-5 pr-0">
            <div class="listing-map">
                 <div id="map"></div>
            </div>
        </div>
    </div>
    </div>
</div>
@if(isset($user) && $user != null && $user->inRole('client'))
<!-- invite to project -->
@php
$client_projects = get_projects($user->id);
@endphp
<div class="modal fade invite-member-modal" id="invite_member" role="dialog">

    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
           <h2>{{trans('client/projects/invite_experts.text_invite_expert')}}</h2>
             <div class="invite-member-section">
                 <div class="member-strip"></div>
             </div>
             <form action="{{url('/')}}/client/projects/invite_experts" method="post" id="invite-member-form">
               {{csrf_field()}}
               <input type="hidden" name="experts" id="experts">
               <div class="invite-form project-not-found-div" style="display:none;">
                   
               </div>
               <div class="invite-form project-found-div" style="display:none;">
                   <div class="user-box">
                       <div class="input-name member-search-wrapper">
                          <div class="droup-select code-select">
                                <select class="selectpicker projects" data-live-search="true" name="project-id" id="project-id">
                                  <option value="">--{{trans('client/projects/invite_experts.text_select')}}--</option>
                                  <!-- @if(isset($client_projects) && (sizeof($client_projects)>0)) 
                                    @foreach($client_projects as $projects)
                                       <option value="{{base64_encode($projects['id'])}}"> 
                                        {{isset($projects['project_name'])?$projects['project_name']:'-'}} 
                                       </option>
                                    @endforeach
                                  @endif -->
                                </select>
                                <span class="error err" id="err_project"></span>
                          </div>
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="txt-edit"> {{trans('client/projects/post.text_project_subdescription')}}</div>
                       <span class="txt-edit" id="project_description_msg">
                        {{trans('client/projects/post.text_project_limit_description')}}
                       </span>
                       <div class="user-box">
                           <div class="input-name">
                              <input type="hidden" class="clint-input" readonly style="cursor:no-drop;" data-rule-required="true" name="expert-email" id="expert-email" placeholder="{{trans('client/projects/invite_experts.text_email_address')}}">
                           </div>
                       </div>
                       <div class="input-name">
                          <textarea class="client-taxtarea" rows="7" data-rule-required="true" data-rule-maxlength="1000" data-txt_gramm_id="21e619fb-536d-f8c4-4166-76a46ac5edce" onkeyup="javascript: return textCounter(this,1000);" id="invite-message" name="invite-message" placeholder="{{trans('client/projects/invite_experts.text_add_a_message')}}"></textarea>
                          <span class="error err" id="err_invite_message"></span>
                       </div>
                    </div>
                    <button type="submit" id="invite-expert" class="black-btn">{{trans('client/projects/invite_experts.text_invite')}}</button>
               </div>
             <form>
          </div>
       </div>
    </div>
</div>
<script type="text/javascript" src="{{url('/public')}}/front/js/bootstrap-select.min.js"></script>
<link href="{{url('/public')}}/front/css/bootstrap-select.min.css" type="text/css" rel="stylesheet" /> 
<script type="text/javascript">
$(document).ready(function(){
/*  jQuery("#subcategory").select2();*/
$("#invite-member-form").validate();
   $('#invite-expert').click(function(){
        var project      = $('#project-id').val();
        var message      = $('#invite-message').val();
        var flag         = 0;
        $('.err').html('');
        if(project == ""){
          $('#err_project').html("{{ trans('common/footer.text_this_field_is_required') }}");
          flag = 1;
        } 
        if(message == ""){
          $('#err_invite_message').html("{{ trans('common/footer.text_this_field_is_required') }}");
          flag = 1;
        } 
        if(flag == 1){
          return false;
        } else {
          return true;
        }
   });  
   $('.invite-expert').click(function(){
        $('.err').html('');
        var expert_id    = $(this).data('expert-id');
        var user_src     = $(this).data('src');
        var name         = $(this).data('name');
        var user_email   = $(this).data('email'); 
        var client_id    = "{{$user->id}}";
        if(client_id == null){
           client_id = 0;
        }
        var html  = "";
        var token = "<?php echo csrf_token(); ?>";
        $.ajax({
          type:"post",
          url:'{{ $module_url_path }}/get_projects',
          data:{expert_id: expert_id,client_id:client_id,_token:token},
          success:function(result){

            if(result != false){
                if(result == 'empty'){
                   $('.project-not-found-div').show();
                   $('.project-found-div').hide();
                   $('.project-not-found-div').html("{{ trans('expert_listing/listing.text_projects_not_matching_experts_skills_or_categories_please_try_another_one') }}");
                   $('#invite-message').html('');
                }
                else{
                $('.project-not-found-div').hide();
                $('.project-found-div').show();  
                $('#project-id').find('option').remove().end().append(result);
                $('.selectpicker').selectpicker('refresh');
                $("#invite-member-form").validate();  
                $('#invite-message').html('Hello '+name+', you have been invited to bid on a project. Please check details.');
                }
            }
          }
        });
        if(expert_id != ""){
          html = '<div class="member-info">'+
                     '<div class="member-img"><img src="'+user_src+'" class="img-responsive"/></div>'+
                     '<div class="member-details">'+
                         '<b>'+name+'</b>'+
                         /*'<p>'+user_email+'</p>'+*/
                     '</div>'+
                 '</div>'+
                 '<div class="remove-btn-block">'+
                     /*'<button class="remove-btn">Remove</button>'+*/
                 '</div>';
          $('.member-strip').html(html);
          $('#expert-email').val(user_email);
          $('#experts').val(expert_id);
        } else {
          $('.member-strip').html('');
          $('#expert-email').val('');
          $('#experts').val('0');
        }
     });
     $('.close-invite-expert').click(function(){
          $('.member-strip').html('');
          $('#expert-email').val('');
          $('#project-id').val('').trigger('change');
          $('#invite-message').val('');
          $('#project-url').val('');
          $('#experts').val('0');
     });
});
function textCounter(field,maxlimit){
  var countfield = 0;
  if ( field.value.length > maxlimit ){
     field.value = field.value.substring( 0, maxlimit );
     return false;
  } else {
      countfield = maxlimit - field.value.length;
      var message = '{{trans('client/projects/invite_experts.text_you_have_left')}} <b>'+countfield+'</b> {{trans('client/projects/invite_experts.text_characters_for_description')}}';
      jQuery('#project_description_msg').html(message);
      return true;
  }
}
</script>
<!-- end invite -->
@endif
<!-- Clustering  MAP -->
<!-- <script type="text/javascript" src="https://cdn.rawgit.com/googlemaps/v3-utility-library/master/markerclustererplus/src/markerclusterer.js"></script> -->
<script type="text/javascript" src="{{url('/public')}}/front/js/markerclusterer.js"></script>
<script>
var locations = [];
    locations = [
    <?php   
        if(isset($arr_experts['data']) && count($arr_experts['data']) > 0){
            foreach($arr_experts['data'] as $row){
                if(!empty($row['latitude']) && !empty($row['longitude'])){  
                    
                    if(isset($row['profile_image']) && $row['profile_image']!=NULL && file_exists('public/uploads/front/profile/'.$row['profile_image'])){
                    $img = url('/public').'/uploads/front/profile/'.$row['profile_image'];
                    }else{
                    $img = url('/public').'/uploads/front/profile/default_profile_image.png';  
                    }
                    $first_name  = 'User';
                    $last_name   = '';
                    if(isset($row["first_name"]) && $row["first_name"] !=""){
                    $first_name  = $row["first_name"];
                    }
                    if(isset($row["last_name"]) && $row["last_name"] !=""){
                    $last_name   = ucfirst(substr($row["last_name"], 0,1).'.');
                    }
                    $expert_name = $first_name.' '.$last_name;

                    $city_name    = '';
                    $country_name = '';
                    if(isset($row['city_details']['city_name']) && $row['city_details']['city_name'] !=""){
                      $city_name = $row['city_details']['city_name'].' ,';
                    }
                    if(isset($row['country_details']['country_name']) && $row['country_details']['country_name'] !=""){
                      $country_name = $row['country_details']['country_name'];
                    }
                    $address   = $city_name.$country_name;
                    ?>
                      ['<div class="user-details"><div class="user-img"><img src="<?php echo $img; ?>" class="img-responsive" alt=""/></div><div class="user-info"><p class="name"><a href="{{ url('/')}}/experts/portfolio/{{base64_encode($row['user_id'])}}"><?php echo $expert_name; ?></a></p><p><i class="fa fa-map-marker"></i> <?php echo $address; ?> </p></div></div>',<?php echo $row['latitude']; ?>,<?php echo $row['longitude']; ?>],
                    <?php
                }
            }
        }
    ?>
];
<?php
    // find center   
    if(isset($arr_experts['data']) && count($arr_experts['data']) > 0){
        foreach($arr_experts['data'] as $row) {
            if(!empty($row['latitude']) && !empty($row['longitude'])){  
               $minlat = false;
               $minlng = false;
               $maxlat = false;
               $maxlng = false;
                   if ($minlat === false) { $minlat = $row['latitude']; } else  { $minlat = ($row['latitude'] < $minlat) ? $row['latitude'] : $minlat; }
                   if ($maxlat === false) { $maxlat = $row['latitude']; } else  { $maxlat = ($row['latitude'] > $maxlat) ? $row['latitude'] : $maxlat; }
                   if ($minlng === false) { $minlng = $row['longitude']; } else { $minlng = ($row['longitude'] < $minlng) ? $row['longitude'] : $minlng; }
                   if ($maxlng === false) { $maxlng = $row['longitude']; } else { $maxlng = ($row['longitude'] > $maxlng) ? $row['longitude'] : $maxlng; }
                // Calculate the center
                $lat = $maxlat - (($maxlat - $minlat) / 2);
                $lon = $maxlng - (($maxlng - $minlng) / 2);
            }
        }
    }
?>
var defaultLat =  <?php if(isset($lat)){ echo $lat; } else { echo '19.997453'; } ?>;
var defaultLng =  <?php if(isset($lon)){ echo $lon; } else { echo '73.789802'; } ?>;
var _address   =  "";
var map;       
var mc;/*marker clusterer*/
var mcOptions = {
      gridSize: 20,
      maxZoom: 3,
      zoom:3,
      imagePath: "https://cdn.rawgit.com/googlemaps/v3-utility-library/master/markerclustererplus/images/m"
};
/*global infowindow*/
var infowindow = new google.maps.InfoWindow();
/*geocoder*/

var _address = _address;
mapInitialize(defaultLat, defaultLng,_address);
function createMarker(latlng,text){
var icon = {
    url: site_url+"/front/images/map-marker.png", // url
    //scaledSize: new google.maps.Size(30, 30),   // scaled size
    //origin: new google.maps.Point(0,0),         // origin
    //anchor: new google.maps.Point(0, 0)         // anchor
};  
var marker = new google.maps.Marker({
    position: latlng,
    map: map,
    icon: icon
});
/*get array of markers currently in cluster*/
var allMarkers = mc.getMarkers();
/*check to see if any of the existing markers match the latlng of the new marker*/
if (allMarkers.length != 0) {
  for (i=0; i < allMarkers.length; i++) {
    var existingMarker = allMarkers[i];
    var pos  = existingMarker.getPosition();
    if (latlng.equals(pos)) {
      text = text + " " + locations[i][0];
    }
  }
}
google.maps.event.addListener(marker, 'click', function(evt){
    infowindow.close();
    infowindow.setContent(text);
    infowindow.open(map,marker);
    map.setZoom(5);
    map.setCenter(marker.getPosition());
});
mc.addMarker(marker);
   return marker;
}
function mapInitialize(lat,lng,_address){
var geocoder = new google.maps.Geocoder(); 
 geocoder.geocode( { 'address': _address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      /*map.setCenter(results[0].geometry.location);*/
      map.setOptions({ maxZoom: 15 });
      map.fitBounds(results[0].geometry.viewport);
    }
  });
  var options = {
      zoom: 3,
      maxZoom: 10,
      center: new google.maps.LatLng(lat,lng), 
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      scrollwheel: true,
      zoomControlOptions: {
                 position: google.maps.ControlPosition.LEFT_TOP
                 //position: google.maps.ControlPosition.LEFT_CENTER
                 //position: google.maps.ControlPosition.RIGHT_CENTER
      },
      streetViewControlOptions: {
                 position: google.maps.ControlPosition.LEFT_TOP
      },
      streetViewControl: false,
      fullscreenControl: false,
  };
  map = new google.maps.Map(document.getElementById('map'), options); 

  /*marker cluster*/
  var gmarkers = [];
  mc = new MarkerClusterer(map, [], mcOptions);
  for (i=0; i<locations.length; i++){
      var latlng = new google.maps.LatLng(parseFloat(locations[i][1]),parseFloat(locations[i][2]));                
      gmarkers.push(createMarker(latlng,locations[i][0]));
  }
}

/* Highlight Marker */
  var gmarkers = [];
  mc = new MarkerClusterer(map, [], mcOptions);
  for (i=0; i<locations.length; i++){
      var latlng = new google.maps.LatLng(parseFloat(locations[i][1]),parseFloat(locations[i][2]));                
      gmarkers.push(createMarker(latlng,locations[i][0]));
  }
  $(".highlight-marker").click(function (){
    var index      = parseInt($(this).attr('map-index'), 10);
    var defaultLat =  $(this).attr('map-lat'); 
    var defaultLng =  $(this).attr('map-long'); 
    $('.other-active-expert').removeClass('active');
    $('.active-expert'+index).addClass('active');
    google.maps.event.trigger(gmarkers[index], 'click');
  });
/* End Highlight Marker */


$(document).ready(function(){
/* bydefault first index highlight */       
   google.maps.event.trigger(gmarkers[0], 'click');
/* end bydefault first index highlight */
});


</script>
<!-- End Clustering  MAP -->
<link href="{{url('/public')}}/front/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="{{url('/public')}}/front/js/jquery-ui.js" type="text/javascript"></script>
<script>
    /*price range slider*/
    $(function() {
       $("#slider-price-range").slider({
           range: true,
           min: 0,
           max: <?php echo $max_hourly_rate; ?>,
           values: [<?php echo $min_hourly_rate; ?>, <?php if(isset($_GET['h_rate_max_val']) && $_GET['h_rate_max_val'] != "") { echo $_GET['h_rate_max_val']; } else { echo $max_hourly_rate; } ?>],
           slide: function(event, ui) {
               $("#slider_price_range_txt").html("<span class='slider_price_min'>$ " + ui.values[0] + "</span>  <span class='slider_price_max'> $" + ui.values[1] + " </span>");
               $("#h_rate_min_val").val(ui.values[0]);
               $("#h_rate_max_val").val(ui.values[1]);
           }
       });
       $("#slider_price_range_txt").html("<span class='slider_price_min'> $" + $("#slider-price-range").slider("values", 0) + "</span>  <span class='slider_price_max'> $" + $("#slider-price-range").slider("values", 1) + "</span>");
    });  
    $('#adv-filter').click(function(){
        $('.advance-filter').slideToggle();
    })
    $(window).load(function(){
        if($(".expert-listing-wrapper").height() != 'undefined'){
            $(".listing-map iframe").css('height', $(".expert-listing-wrapper").height()); 
        }
    });
</script>
<script type="text/javascript">
$(document).ready( function (){
   var url_language = $('#url_language').val();
   if( typeof url_language != 'undefined' && url_language != "")
   {
      showProcessingOverlay();
      $('#btn_search').click();
   }
});
</script>
<!-- custom scrollbars plugin -->
<script type="text/javascript">
     /*scrollbar start*/
     $(".content-d").mCustomScrollbar({
       scrollInertia: 98
     });
</script>
<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<script type="text/javascript">

    $(document).ready(function(){
        var categoryId = $("#category").children("option:selected").val();
        var subcategory = "{{$search_sub_categories_id}}";
        if(categoryId!='')
        {
            $.ajax({
                url : '{{url('/')}}/experts/get_subcategory/'+categoryId,
                data: {   _token : "{{ csrf_token() }}", subcategory : subcategory },
                
                success : function(resp) 
                {
                    if(resp.status == 'success')
                    {
                        if(resp.html != undefined && resp.html != '')
                        {
                            options = '<select class="select-box " id="subcategory" name="subcategory" data-rule-required="false"><option value=""> -- Select Subcategory -- </option>'+resp.html+'</select>';
                            $("#subcategory_div").html('');
                            $("#subcategory_div").html(options);
                        }
                    }
                    else
                    {
                        options = '<select class="select-box " ><option value=""> -- Select Subcategory -- </option></select>';
                        $("#subcategory_div").html('');
                        $("#subcategory_div").html(options);
                    }
                }
            })
        }
    });

   function getSubCategory(ref)
    {    
        var categoryId = $(ref).children("option:selected").val();
        var i = $(ref).attr("data-cat-id");
        var subcategory = "{{$search_sub_categories_id}}";
        if(categoryId!='')
        {
            $.ajax({
                url : '{{url('/')}}/experts/get_subcategory/'+categoryId,
                data: {   _token : "{{ csrf_token() }}", },
                success : function(resp) 
                {
                    if(resp.status == 'success')
                    {
                        if(resp.html != undefined && resp.html != '')
                        {
                            options = '<select class="select-box " id="subcategory" name="subcategory" data-rule-required="false"><option value=""> -- Select Subcategory -- </option>'+resp.html+'</select>';
                            $("#subcategory_div").html('');
                            $("#subcategory_div").html(options);
                        }
                    }
                    else
                    {
                        options = '<select class="select-box " ><option value=""> -- Select Subcategory -- </option></select>';
                        $("#subcategory_div").html('');
                        $("#subcategory_div").html(options);
                    }
                }
            })
        }
        else
        {
            options = '<select class="select-box " ><option value=""> -- Select Subcategory -- </option></select>';
            $("#subcategory_div").html('');
            $("#subcategory_div").html(options);
        }
    }

</script>
@endsection