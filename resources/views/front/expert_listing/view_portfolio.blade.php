@extends('front.layout.master')                
@section('main_content') 
@php $user = \Sentinel::check();@endphp
<style type="text/css">
.webwing-gallery.img350 .webwing-medium-wrap {
    height: 302px !important;
    border-radius: 5px !important;
    position: relative !important;
    width: 50% !important;
    margin-left: 29px !important;
}

.expert_details_section-description table tr td {
padding: 8px;
width: 33.333%;
}
</style>
<div class="middle-container">
   <div class="container">
      <br/>
      <?php 
         $sidebar_information= array();
         if (isset($arr_expert['user_id']))
         {
            $sidebar_information = sidebar_information($arr_expert['user_id']);
         }
      ?>
      <div class="row">
         <div class="col-sm-12 col-md-12 col-lg-12">
            @include('front.layout._operation_status')
            <div class="search-grey-bx">
               <div class="head_grn">{{ trans('expert/portfolio/portfolio.text_profile') }}</div>
               <div class="row">
                  <div class="col-sm-12 col-md-12 col-lg-12">
                     <div class="profil-man">
                        <div class="row">
                           <div class="col-sm-3 col-md-3 col-lg-3">
                              <div class="user-profile">
                                 <div class="profile-circle">  
                                     
                                    @if(isset($arr_expert['user_details']['is_online']) && $arr_expert['user_details']['is_online']!='' && $arr_expert['user_details']['is_online']=='1')
                                      <div class="user-online-round"></div>
                                    @else
                                      <div class="user-offline-round"></div>
                                    @endif
                                     
                                     
                                    <div class="edit-hover">
                                       <li><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></li>
                                    </div>
                                    @if(isset($profile_img_public_path) && isset($arr_expert['profile_image']) && $arr_expert['profile_image']!=NULL && file_exists('public/uploads/front/profile/'.$arr_expert['profile_image']))
                                    <img src="{{$profile_img_public_path.$arr_expert['profile_image']}}" alt=""/>
                                    @else
                                    <img src="{{$profile_img_public_path}}default_profile_image.png" alt="">
                                    @endif
                                 </div>
                              </div>
                              <div style="text-align:center;margin-bottom:15px;">
                                 @if(isset($arr_expert['created_at']) && $arr_expert['created_at']!="")
                                 {{ trans('expert/portfolio/portfolio.text_since_on',array('reg_year'=>date('M Y',strtotime($arr_expert['created_at'])))) }}
                                 <br/> {{config('app.project.name')}}
                                 @endif
                              </div>
                              @if(isset($user) && $user != null && $user->inRole('client'))
                              <div class="model-deceb-pro">
                                 <div style="text-align:center;margin-bottom:15px;">
                                    <a href="#invite_member" 
                                          data-keyboard="false" 
                                          data-src="@if(isset($profile_img_public_path) && isset($arr_expert['profile_image']) && $arr_expert['profile_image']!=NULL && file_exists('public/uploads/front/profile/'.$arr_expert['profile_image']))
                                          {{$profile_img_public_path.$arr_expert['profile_image']}}
                                          @else
                                          {{$profile_img_public_path.'default_profile_image.png'}}
                                          @endif"
                                          data-name="{{isset($arr_expert['user_details']['role_info']['first_name'])?$arr_expert['user_details']['role_info']['first_name']:'Unknown user'}}" 
                                          data-email="{{$arr_expert['user_details']['email']}}" 
                                          data-backdrop="static" 
                                          data-expert-id="{{isset($arr_expert['user_id'])?base64_encode($arr_expert['user_id']):''}}" 
                                          data-toggle="modal" class="black-btn invite-expert">
                                          {{trans('client/projects/invite_experts.text_invite_expert')}}
                                    </a>
                                 </div>    
                              </div>    
                              @endif
                           </div>
                           <div class="col-sm-9 col-md-9 col-lg-9">
                              <div class="right-dashbord expert-details-info-section">
                                 <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                       <div class="head_grn">
                                        <div class="profile-name-section-block">
                                          <?php 
                                             $expert_user_name = "---";
                                             $first_name       = isset($arr_expert['first_name']) ?$arr_expert['first_name']:"";
                                             $last_name        = isset($arr_expert['last_name']) ?substr($arr_expert['last_name'], 0,1).'.':"";
                                             $expert_user_name = $first_name.' '.$last_name;
                                             ?>
                                             {{ $expert_user_name or ''}}


                                           </div>
                                           
                                            @if(isset($arr_expert['user_details']['kyc_verified']) && $arr_expert['user_details']['kyc_verified']!='' && $arr_expert['user_details']['kyc_verified'] == '1')
                                              <span class="verfy-new-arrow">
                                                  <a href="#" data-toggle="tooltip" title="Identity Verified"><img src="{{url('/public/front/images/verifild.png')}}" alt=""> </a>
                                              </span>
                                            @endif
                                       </div>
                                       <div class="address-mod-pro">
                                          <div class="icon-block"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                                          <div class="add-conte-block" onclick="searchByCountry(this)" 
                                             style="cursor: pointer;"   
                                             data-country-id="{{ isset($arr_expert['country'])?base64_encode($arr_expert['country']):'' }}">
                                             {{isset($arr_expert['country_details']['country_name'])?$arr_expert['country_details']['country_name']:'-'}}
                                          </div>
                                          <div class="clr"></div>
                                       </div>
                                       <div class="model-deceb-pro">
                                          <div class="icon-block"><i class="fa fa-star" aria-hidden="true"></i></div>
                                          <div class="rate-t">
                                             <div class="rating-list"><span class="stars">{{isset($sidebar_information['average_rating'])?$sidebar_information['average_rating']:'0'}}</span></div>
                                             ({{isset($sidebar_information['average_rating'])?$sidebar_information['average_rating']:'0'}})
                                          </div>
                                          <div class="clr"></div>
                                       </div>
                                       <div class="model-deceb-pro">
                                          <div class="icon-block"><i  class="fa fa-clock-o" aria-hidden="true"></i></div>
                                             <span >                              
                                                @if($sidebar_information['last_login_duration'] == 'Active')
                                                   <a style="color:#4D4D4D;text-transform:none;cursor:text;" title="{{isset($sidebar_information['expert_timezone'])?$sidebar_information['expert_timezone']:'Not specify'}}">
                                                      <span class="flag-image"> 
                                                         @if(isset($sidebar_information['user_country_flag'])) 
                                                           @php 
                                                              echo $sidebar_information['user_country_flag']; 
                                                           @endphp 
                                                         @elseif(isset($sidebar_information['user_country'])) 
                                                           @php 
                                                                echo $sidebar_information['user_country']; 
                                                           @endphp 
                                                         @else 
                                                           @php 
                                                             echo 'Not Specify'; 
                                                           @endphp 
                                                         @endif 
                                                      </span>
                                                      {{-- trans('common/_expert_details.text_local_time') --}}  {{isset($sidebar_information['expert_timezone_without_date'])?$sidebar_information['expert_timezone_without_date']:'Not specify'}}
                                                   </a>
                                                @else
                                                   <span class="flag-image"> 
                                                      @if(isset($sidebar_information['user_country_flag'])) 
                                                        @php 
                                                           echo $sidebar_information['user_country_flag']; 
                                                        @endphp 
                                                      @elseif(isset($sidebar_information['user_country'])) 
                                                        @php 
                                                          echo $sidebar_information['user_country']; 
                                                        @endphp 
                                                      @else 
                                                        @php 
                                                          echo 'Not Specify'; 
                                                        @endphp 
                                                      @endif 
                                                   </span>
                                                   <a style="color:#4D4D4D;text-transform:none;cursor:text;" title="{{$sidebar_information['last_login_full_duration']}}">
                                                      <span  title="{{$sidebar_information['last_login_full_duration']}}">{{$sidebar_information['last_login_duration']}}</span>  
                                                   </a>
                                                @endif
                                             </span>
                                          <div class="clr"></div>
                                       </div>
                                       <div class="expert_details_section">
                                          <div class="expert_details_section-label">
                                            <label>{{trans('expert/portfolio/portfolio.text_about_me')}} : </label>
                                            <div class="expert_details_section-description">
                                              {{ $arr_expert['about_me']  or '-'}}
                                            </div>
                                            <div class="clearfix"></div>
                                          </div>
                                          <div class="expert_details_section-label">
                                            <label>{{trans('expert/portfolio/portfolio.text_skills')}} : </label>
                                            <div class="expert_details_section-description">
                                              @if(isset($arr_expert['expert_skills']) && sizeof($arr_expert['expert_skills'])>0)
                                             @foreach($arr_expert['expert_skills'] as $key => $exp_skill)
                                             @if(isset($exp_skill['skills']['skill_name']))
                                             @if(end($arr_expert['expert_skills'])==$exp_skill)
                                             <span style="cursor: pointer;font-size: 15px;" onclick="searchBySkill(this);" data-skill-id="{{ isset($exp_skill['skill_id'])?base64_encode($exp_skill['skill_id']):'' }}">{{$exp_skill['skills']['skill_name']}}</span>
                                             @else
                                             <span style="cursor: pointer;font-size: 15px;" onclick="searchBySkill(this);" data-skill-id="{{ isset($exp_skill['skill_id'])?base64_encode($exp_skill['skill_id']):'' }}">{{$exp_skill['skills']['skill_name'].','}}</span>
                                             @endif
                                             @endif
                                             @endforeach
                                             @endif
                                            </div>
                                            <div class="clearfix"></div>
                                          </div>
                                          <?php
                                          $arr_lang = [];
                                          if(isset($arr_expert['expert_spoken_languages']) && count($arr_expert['expert_spoken_languages']) > 0)
                                          {
                                             foreach ($arr_expert['expert_spoken_languages'] as $_key => $language) 
                                             {
                                                $arr_lang[] = $language['language'];
                                             }
                                          }
                                          ?>
                                          <div class="expert_details_section-label">
                                            <label>{{trans('expert/portfolio/portfolio.text_languages')}} : </label>
                                            <div class="expert_details_section-description">
                                             @if(count($arr_lang)>0)
                                             @foreach($arr_lang as $language)
                                             <span onclick="searchByLanguage(this)" 
                                                style="cursor: pointer;"   
                                                data-language="{{ isset($language)?$language:'' }}">
                                             {{isset($language)?$language:''}}@if($language != array_last($arr_lang)),@endif</span>
                                             @endforeach
                                             @endif
                                            </div>
                                            <div class="clearfix"></div>
                                          </div>
                                          <div class="expert_details_section-label">
                                            <label>{{trans('expert/portfolio/portfolio.text_education')}} : </label>
                                            <div class="expert_details_section-description">
                                              {{$arr_expert['education_details']['title'] or 'NA'}}
                                            </div>
                                            <div class="clearfix"></div>
                                          </div>

                                          <div class="expert_details_section-label">
                                            <label>{{trans('expert/portfolio/portfolio.text_certification')}} : &nbsp;</label>
                                            <div class="expert_details_section-description">
                                              @if(isset($arr_expert['expert_certification_details']) && count($arr_expert['expert_certification_details'])>0)
                                              <table width="100%;">
                                                <thead>
                                                  <tr>
                                                    <th>Name</th>
                                                    <th>Initial Certification Date</th>
                                                    <th>Expiration Date</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  @foreach($arr_expert['expert_certification_details'] as $key=>$certificates)
                                                    <tr>
                                                      <td>{{$certificates['certification_name'] or 'NA'}}</td>
                                                      <td>{{isset($certificates['completion_year'])?date("d-M-y", strtotime($certificates['completion_year'])):''}}</td>
                                                      <td>{{isset($certificates['expire_year'])?date("d-M-y", strtotime($certificates['expire_year'])):''}}</td>
                                                    </tr>
                                                  @endforeach
                                                </tbody>
                                              </table>
                                              @else 
                                              {{'NA'}}
                                              @endif

                                            </div>
                                            <div class="clearfix"></div>
                                          </div>
                                          <div class="expert_details_section-label">
                                            <label>{{trans('expert/portfolio/portfolio.text_experience')}} : </label>
                                            <div class="expert_details_section-description">
                                              @if(isset($arr_expert['expert_work_experience']) && count($arr_expert['expert_work_experience'])>0)
                                              <table width="100%;">
                                                <thead>
                                                  <tr>
                                                    <th>Position/Role</th>
                                                    <th>Company Name</th>
                                                    <th>From - To</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  @foreach($arr_expert['expert_work_experience'] as $key=>$experience)
                                                    <tr>
                                                      <td>{{$experience['designation'] or 'NA'}}</td>
                                                      <td>{{$experience['company_name'] or 'NA'}}</td>
                                                      <td>{{$experience['from_year'] or 'NA'}}-{{$experience['to_year'] or 'NA'}}</td>
                                                    </tr>
                                                  @endforeach
                                                </tbody>
                                              </table>
                                              @else 
                                              {{'NA'}}
                                              @endif
                                            </div>
                                            <div class="clearfix"></div>
                                          </div>

                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <hr/>
                              <div class="clr"></div>
                              <div class="rating-bx">
                                 <div class="img-pro infg"><img src="{{url('/public')}}/front/images/project-inc.png" alt="" /></div>
                                 <div class="info-r-pro">
                                    <div align="center">{{isset($sidebar_information['completed_project_count'])?$sidebar_information['completed_project_count']:'0'}}</div>
                                    <span>{{ trans('common/expert_sidebar.text_completed_projects') }}</span>
                                 </div>
                              </div>
                              <div class="rating-bx">
                                 <div class="img-pro infg"><img src="{{url('/public')}}/front/images/project-inc.png" alt="" /></div>
                                 <div class="info-r-pro">
                                    <div align="center">{{isset($sidebar_information['ongoing_project_count'])?$sidebar_information['ongoing_project_count']:'0'}}</div>
                                    <span>{{ trans('common/expert_sidebar.text_ongoing_projects') }}</span>
                                 </div>
                              </div>
                              <div class="rating-bx">
                                 <div class="img-pro infg"><img src="{{url('/public')}}/front/images/reviews-inc.png" alt="" /></div>
                                 <div class="info-r-pro">
                                    <div align="center">{{isset($sidebar_information['review_count'])?$sidebar_information['review_count']:'0'}}</div>
                                    <span>
                                    @if(isset($sidebar_information['review_count']) && $sidebar_information['review_count'] == 1 )
                                    {{str_singular(trans('expert/portfolio/portfolio.text_review'))}} 
                                    @else 
                                    {{ trans('expert/portfolio/portfolio.text_review') }} 
                                    @endif
                                    </span>
                                 </div>
                              </div>
                              <div class="rating-bx">
                                 <div class="img-pro infg"><img src="{{url('/public')}}/front/images/rating-inc.png" alt="" /></div>
                                 <div class="info-r-pro">
                                    <div align="center">{{isset($sidebar_information['reputation'])?$sidebar_information['reputation']:'0.0'}}%</div>
                                    <span>{{ trans('expert/portfolio/portfolio.text_reputation') }}</span>
                                 </div>
                              </div>
                              <div class="rating-bx">
                                    <div class="img-pro infg"><i style="font-size: 1.7em;" class="fa fa-trophy"></i></div>
                                    <div class="info-r-pro">
                                       <div align="center" >{{isset($sidebar_information['contest_win_count'])?$sidebar_information['contest_win_count']:'0'}}</div>
                                       <span>{{trans('expert/portfolio/portfolio.text_won_contests')}}</span>
                                    </div>
                              </div>
                           </div>
                        </div>
                        <div class="clr"></div>
                     </div>
                     <div class="row listing-detadfs">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                           <div class="main-tbs">
                              <!-- Details tabs -->
                              <ul class="nav nav-tabs">
                                 <li class="active"><a href="#overview" aria-controls="overview" data-toggle="tab">{{ trans('expert/portfolio/portfolio.text_portfolio') }}</a></li>
                                 <li><a href="{{url('/')}}/experts/review/{{base64_encode($arr_expert['user_id'])}}">
                                    @if(isset($review_cnt)&& sizeof($review_cnt)>0) {{$review_cnt}} @else 0 @endif
                                    @if(isset($review_cnt) && $review_cnt == 1 )
                                    {{str_singular(trans('expert/portfolio/portfolio.text_review'))}} 
                                    @else 
                                    {{ trans('expert/portfolio/portfolio.text_review') }} 
                                    @endif
                                    </a>
                                 </li>
                              </ul>
                              <!-- Details panes 880,1011 -->
                              <div class="tab-content">
                                 <!--tab 1-->
                                 <div class="tab-pane active" id="overview">
                                    <div class="white-block-details padng-pros row">
                                       @if(isset($arr_portfolio['data']) && sizeof($arr_portfolio['data'])>0)
                                           <div id="portfollio-glry" class="webwing-gallery img350">
                                              <div class="prod-carousel">
                                                @if(isset($arr_portfolio['data']) && sizeof($arr_portfolio['data'])>0)
                                                <?php $i = 1; ?>    
                                                  @foreach($arr_portfolio['data'] as $portfolio)
                                                    @if(isset($portfolio['file_name']) && $portfolio['file_name']!=null && file_exists('public/uploads/front/portfolio/'.$portfolio['file_name']))
                                                      <img @if($i == 1) style="box-shadow: 12px 14px 11px #888888;" @endif class="portfollio_files" file-index="{{$i}}" src="{{$portfolio_img_public_path.$portfolio['file_name']}}"  data-medium-img="{{$portfolio_img_public_path.$portfolio['file_name']}}" data-big-img="{{$portfolio_img_public_path.$portfolio['file_name']}}" data-title="File {{$i}}" alt="">
                                                    @else
                                                      <!-- <img src="{{url('/front/cover_image/default_cover_photo.jpg')}}" data-medium-img="{{url('/front/cover_image/default_cover_photo.jpg')}}" data-big-img="{{url('/front/cover_image/default_cover_photo.jpg')}}" data-title="default_cover_photo.jpg" alt=""> -->
                                                    @endif
                                                 <?php $i++; ?>
                                                @endforeach
                                                @endif
                                              </div>
                                           </div>
                                       @else
                                       <br/>
                                       <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;" align="center">
                                          <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                                             <div class="search-content-block">
                                                <div class="search-head" style="color:rgba(45, 45, 45, 0.8);" align="center">
                                                   {{ trans('expert/portfolio/portfolio.text_no_images_found') }}
                                                </div>
                                             </div>
                                          </td>
                                       </tr>
                                       @endif              
                                       <div class="clr"></div>
                                    </div>

                                    @if(isset($arr_portfolio) && count($arr_portfolio)>0)
                                    <?php 
                                       $arr_pagination = $arr_portfolio_links;
                                    ?>
                                    <!--pagigation start here-->
                                    @include('front.common.pagination_view', ['paginator' => $arr_pagination])
                                    <!--pagigation end here-->
                                    @endif
                                    <div class="clearfix"></div>
                                 </div>
                                 <!--tab 1-->
                              </div>
                              <!-- Details tabs End --> 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
@if(isset($user) && $user != null && $user->inRole('client'))
<!-- invite to project -->
@php
$client_projects = get_projects($user->id);
@endphp
<div class="modal fade invite-member-modal" id="invite_member" role="dialog">
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
           <h2>{{trans('client/projects/invite_experts.text_invite_expert')}}</h2>
             <div class="invite-member-section">
                 <div class="member-strip"></div>
             </div>
             <form action="{{url('/')}}/client/projects/invite_experts" method="post" id="invite-member-form">
               {{csrf_field()}}
               <input type="hidden" name="experts" id="experts">
               <div class="invite-form project-not-found-div" style="display:none;">
                   
               </div>
               <div class="invite-form project-found-div" style="display:none;">
                   <div class="user-box">
                       <div class="input-name member-search-wrapper">
                          <div class="droup-select code-select">
                                <select class="selectpicker projects" data-live-search="true" name="project-id" id="project-id">
                                  <option value="">--{{trans('client/projects/invite_experts.text_select')}}--</option>
                                </select>
                                <span class="error err" id="err_project"></span>
                          </div>
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="txt-edit"> {{trans('client/projects/post.text_project_subdescription')}}</div>
                       <span class="txt-edit" id="project_description_msg">
                        {{trans('client/projects/post.text_project_limit_description')}}
                       </span>
                       <div class="user-box">
                           <div class="input-name">
                              <input type="hidden" class="clint-input" readonly style="cursor:no-drop;" data-rule-required="true" name="expert-email" id="expert-email" placeholder="{{trans('client/projects/invite_experts.text_email_address')}}">
                           </div>
                       </div>
                       <div class="input-name">
                          <textarea class="client-taxtarea" rows="7" data-rule-required="true" data-rule-maxlength="1000" data-txt_gramm_id="21e619fb-536d-f8c4-4166-76a46ac5edce" onkeyup="javascript: return textCounter(this,1000);" id="invite-message" name="invite-message" placeholder="{{trans('client/projects/invite_experts.text_add_a_message')}}"></textarea>
                          <span class="error err" id="err_invite_message"></span>
                       </div>
                    </div>
                    <button type="submit" id="invite-expert" class="black-btn">{{trans('client/projects/invite_experts.text_invite')}}</button>
               </div>
             <form>
          </div>
       </div>
    </div>
</div>
<script type="text/javascript" src="{{url('/public')}}/front/js/bootstrap-select.min.js"></script>
<link href="{{url('/public')}}/front/css/bootstrap-select.min.css" type="text/css" rel="stylesheet" /> 
<script type="text/javascript">
$(document).ready(function(){
   $("#invite-member-form").validate();
   $('#invite-expert').click(function(){
        var project      = $('#project-id').val();
        var message      = $('#invite-message').val();
        var flag         = 0;
        $('.err').html('');
        if(project == ""){
          $('#err_project').html("{{ trans('common/footer.text_this_field_is_required') }}");
          flag = 1;
        } 
        if(message == ""){
          $('#err_invite_message').html("{{ trans('common/footer.text_this_field_is_required') }}");
          flag = 1;
        } 
        if(flag == 1){
          return false;
        } else {
          return true;
        }
   });  
   $('.invite-expert').click(function(){
     $('.err').html('');
     var expert_id    = $(this).data('expert-id');
     var user_src     = $(this).data('src');
     var name         = $(this).data('name');
     var user_email   = $(this).data('email'); 
     var client_id    = "{{$user->id}}";
     if(client_id == null){
        client_id = 0;
     }
     var html  = "";
     var token = "<?php echo csrf_token(); ?>";
     $.ajax({
       type:"post",
       url:'{{ url("/experts") }}/get_projects',
       data:{expert_id: expert_id,client_id:client_id,_token:token},
       success:function(result){

         if(result != false){
             if(result == 'empty'){
                $('.project-not-found-div').show();
                $('.project-found-div').hide();
                $('.project-not-found-div').html("{{ trans('expert_listing/listing.text_projects_not_matching_experts_skills_or_categories_please_try_another_one') }}");
                $('#invite-message').html('');
             }
             else{
             $('.project-not-found-div').hide();
             $('.project-found-div').show();  
             $('#project-id').find('option').remove().end().append(result);
             $('.selectpicker').selectpicker('refresh');
             $("#invite-member-form").validate();  
             $('#invite-message').html('Hello '+name+', you have been invited to bid on a project. Please check details.');
             }
         }
       }
     });
     if(expert_id != ""){
       html = '<div class="member-info">'+
                  '<div class="member-img"><img src="'+user_src+'" class="img-responsive"/></div>'+
                  '<div class="member-details">'+
                      '<b>'+name+'</b>'+
                      /*'<p>'+user_email+'</p>'+*/
                  '</div>'+
              '</div>'+
              '<div class="remove-btn-block">'+
                  /*'<button class="remove-btn">Remove</button>'+*/
              '</div>';
       $('.member-strip').html(html);
       $('#expert-email').val(user_email);
       $('#experts').val(expert_id);
     } else {
       $('.member-strip').html('');
       $('#expert-email').val('');
       $('#experts').val('0');
     }
  });
  $('.close-invite-expert').click(function(){
       $('.member-strip').html('');
       $('#expert-email').val('');
       $('#project-id').val('').trigger('change');
       $('#invite-message').val('');
       $('#project-url').val('');
       $('#experts').val('0');
  });
});
function textCounter(field,maxlimit){
  var countfield = 0;
  if ( field.value.length > maxlimit ){
     field.value = field.value.substring( 0, maxlimit );
     return false;
  } else {
      countfield = maxlimit - field.value.length;
      var message = '{{trans('client/projects/invite_experts.text_you_have_left')}} <b>'+countfield+'</b> {{trans('client/projects/invite_experts.text_characters_for_description')}}';
      jQuery('#project_description_msg').html(message);
      return true;
  }
}
</script>
<!-- end invite -->
@endif
<link href="{{url('/public')}}/front/css/gallery.css" rel="stylesheet" />
<script src="{{url('/public')}}/front/js/gallery.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
      $('#portfollio-glry').webwingGallery({
          openGalleryStyle: 'transform',
          changeMediumStyle: true
      });
      $('.portfollio_files').click(function(){
         $('.file_index').html('File #'+$(this).attr('file-index'));
         $('.portfollio_files').removeAttr('style');
         $(this).attr('style','box-shadow:12px 14px 11px #888888');
      });

   });
</script>      
<script type="text/javascript">
   function searchBySkill(ref) 
   {
      var skill_id = $(ref).attr('data-skill-id');
         
      if(skill_id != "")
      {
         window.location.href="{{url('/experts/search_skill')}}/"+skill_id;
      }
      else   
      {
         console.log('No skill information available.');
      }
    
   }
   function searchByCountry(ref) 
   {
      var country_id = $(ref).attr('data-country-id');
         
      if(country_id != "")
      {
         window.location.href="{{url('/experts/search_country')}}/"+country_id;
      }
      else   
      {
         console.log('No country information available.');
      }
   }
   function searchByLanguage(ref) 
   {
      var language = $(ref).attr('data-language');
         
      if(language != "")
      {
         window.location.href="{{url('/experts')}}?q="+language;
      }
      else   
      {
         console.log('No language information available.');
      }
   }
</script>
<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
@stop