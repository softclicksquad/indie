@extends('front.layout.auth')

<style type="text/css">
  .fld-required {  color: #FF0000; }
  /* sign up popup start here*/
  .modal-dialog.congratulations-popup{width: 500px;}
  .check-img-block{height: 60px; width: 60px; border-radius: 50%; margin: 0 auto; border: 1px solid #63c65f; text-align: center; padding-top: 17px;}
  .congratulations-popup .modal-body{text-align: center;}
  .congratulations-popup .modal-body h4{font-size: 25px; font-weight: 600;}
  .congratulations-popup .modal-body .you-may-need-message{background: #e2eaea; padding: 15px; border: 1px solid #d2bbbf; color: #7d6c6e; text-transform: uppercase; border-radius: 5px; font-weight: 600; display: block; margin: 5px 0 20px;}
  .congratulations-popup .modal-body .you-may-need-message span{text-transform: none;}
  .congratulations-popup .modal-body p{margin: 15px 0;}
  .congratulations-popup .modal-body .continue-btn{background: #63c65f; text-transform: uppercase; border-color: #63c65f;}

      .modal-open .modal.fade .modal-dialog {
-webkit-transform: translate(0, 50%);
-ms-transform: translate(0, 50%);
-o-transform: translate(0, 50%);
transform: translate(0, 50%);
}

</style>
@section('main_content')
<!-- signup success module -->
<a id="signup_success_model"  data-backdrop="static"  data-toggle="modal" data-target="#myModal"></a>
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog congratulations-popup">
        <div class="modal-content">
            <div class="modal-body">
                <div class="check-img-block">
                    <img src="{{ url('/front/images/sign-up-check-img.png') }}" alt="" />
                </div>
                <h4>{{ trans('auth/signup.Congratulations') }}</h4>
                <p>{{ Session::get('success') }}</p>
                <span class="you-may-need-message">{{ trans('auth/signup.you_may_need_to_check_your_spam') }} <span>{{ trans('auth/signup.or') }}</span> {{ trans('auth/signup.junk_folder') }} <span>({{ trans('auth/signup.it_may_land_in_there') }})</span></span>
                <a href="{{ url('/info/how-it-works') }}" class="btn btn-info waves-effect continue-btn">{{ trans('common/header.text_how_it_works') }}</a>
            </div>                
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- signup success module -->


<div id="country_blocked" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog congratulations-popup">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 id="alert-modal-title" class="modal-title text-center">Oops!</h4>
          </div>
            <div class="modal-body">
               <div id="alert-modal-body" class="modal-body">
                <span class="you-may-need-message">We currently are not able to process any payments for persons living in the selected country. We are very sorry! Please check our website from time to time. As changes in regulations happen we would be very happy to welcome you to our website soon again.</span>
                </div>
            </div>                
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="login-section signup-section client-signup">

            <div class="sign-up-form client-login">
               <div class="pagi">
                  <div class="regi_toggle head-title">
                     <h2>{{ trans('auth/signup.text_sign_up_client') }}</h2>
                  </div>
                  <div class="clearfix"></div>
               </div>
               <div class="exper-clnt-login1">
                  <!--registration step two start here-->
                  <form action="{{ url($module_url_path) }}/client" method="POST" id="form-client-register" name="form-client-register" enctype="multipart/form-data" files="true">
                     {{ csrf_field() }}
                     <div id="reg2" class="container1">
                        <div class="">
                           {{ $CountryError or '' }}
                            @if(Session::has('error'))
                              @include('front.layout._operation_status')
                            @endif
                            @if (Session::has('success'))
                            <script type="text/javascript">
                            $(document).ready(function(){
                                $('#signup_success_model').click(); 
                            });
                            </script>
                            @endif 
                            @if(Session::has('country_blocked'))
                              <script type="text/javascript">
                                $(document).ready(function(){
                                   $('#country_blocked').modal('show');
                                });
                              </script>
                            @endif
                           
                          <div class="user-box">
                           <div class="control-label">{{ trans('auth/signup.text_email') }}:<span class="fld-required">*</span></div>
                           <div class="input-name"><input type="text"  name="email" class="clint-input" placeholder="{{ trans('auth/signup.entry_email') }}" data-rule-required="true" data-rule-email="true" value="{{old('email')}}"/></div>
                           <span class='error'>{{ $errors->first('email') }}</span>
                         </div>
                         <div class="clearfix"></div>
                         <div class="user-box">
                           <div class="control-label">{{ trans('auth/signup.text_country') }}:<span class="fld-required">*</span></div>
                           <div class="input-name">
                            <div class="droup-select signup-select">
                              @php 
                                  $client_country_id = 0; 
                              @endphp
                              @if(isset($arr_countries) && sizeof($arr_countries)>0)
                              <select class="droup" data-rule-required="true" name="country" id="country_client" onchange="return blocked_country(this);">
                                <option value="">{{ trans('auth/signup.entry_country') }}</option>
                                @foreach($arr_countries as $countries)
                                  @php
                                      $current_selected_country = '';
                                      if(isset($countries['country_name']) && $countries['country_name'] !="" && $client_country == $countries['country_name'] ){
                                        $current_selected_country = 'selected="selected"';
                                        $client_country_id = isset($countries['id'])?$countries['id']:""; 
                                      } 
                                  @endphp
                                  
                                <option 
                                        value="{{isset($countries['id'])?$countries['id']:""}}"
                                        {{isset($current_selected_country) ? $current_selected_country : ''}}
                                > {{isset($countries['country_name'])?$countries['country_name']:""}}</option>
                                @endforeach
                              </select>
                              @endif
                            </div>
                          </div>
                        </div>
                        <span class='error'>{{ $errors->first('country') }}</span>
                        <div class="clearfix"></div>
                        
                         <div class="user-box">
                           <div class="control-label">{{ trans('auth/signup.text_user_type') }}:<span class="fld-required">*</span></div>
                           <div class="input-name">
                            <div class="droup-select signup-select">
                              @if(isset($arr_countries) && sizeof($arr_countries)>0)
                              <select class="droup" data-rule-required="true" name="user_type" id="user_type">
                                <option value="">{{ trans('auth/signup.text_user_type_select') }}</option>
                                <option value="business">{{ trans('auth/signup.text_user_type_public') }}</option>
                                <option value="private">{{ trans('auth/signup.text_user_type_private') }}</option>
                              </select>
                              @endif
                            </div>
                          </div>
                        </div>
                        <span class='error'>{{ $errors->first('user_type') }}</span>

                        <div class="clearfix"></div>
                        <div class="user-box">
                         <div class="control-label">{{ trans('auth/signup.text_password') }}:<span class="fld-required">*</span></div>
                         <div class="input-name"><input type="password" id="password" name="password" data-rule-required='true' data-rule-minlength="8" placeholder="{{ trans('auth/signup.entry_password') }}" class="clint-input"/></div>
                       </div>
                       <span class='error'>{{ $errors->first('password') }}</span>
                       <div class="clearfix"></div>
                       <div class="user-box">
                         <div class="control-label">{{ trans('auth/signup.text_confirm_password') }}:<span class="fld-required">*</span></div>
                         <div class="input-name"><input type="password" name="confirm_password" id="confirm_password" data-rule-required='true' data-rule-minlength="8" data-rule-equalto="#password" placeholder="{{ trans('auth/signup.entry_confirm_password') }}" class="clint-input"/></div>
                         <span class='error'>{{ $errors->first('confirm_password') }}</span>
                       </div>

                        <div class="user-box">
                          <div class="check-box">
                              <p>
                               <input id="filled-in-box" class="filled-in" type="checkbox" name="check_terms" data-rule-required="true">
                               <label for="filled-in-box"></label>
                              </p>
                              
                          </div>
                          <div class="temsp-d">
                            <?php
                             if(isset($arr_static_terms['page_slug']))
                                $link_term = url('/info/'.$arr_static_terms['page_slug']);
                             else
                                $link_term = 'javascript: void(0)';
                             if (isset($arr_static_privacy['page_slug'])) 
                                $link_policy = url('/info/'.$arr_static_privacy['page_slug']);
                             else
                                $link_policy = 'javascript: void(0)';
                             ?>
                             {!! trans('client/projects/post.text_terms_final',array('link_term'=>$link_term,'link_policy'=>$link_policy)) !!}
                          </div>
                          <span for="check_terms" class="error"></span>
                        </div>




                       <input type="submit" id="csignup" class="normal-btn" value="{{ trans('auth/signup.text_signup') }}">

                       <div class="clearfix"></div>
                       <div class="alrdy-login">{{ trans('auth/signup.text_already_account') }} <a href="{{url('/login')}}"> {{ trans('auth/signup.text_login_here') }}</a></div>
                        </div>
                     </div>
                  </form>
                  <!--registration step third start here-->
               </div>
               <div class="clearfix"></div>
            </div>
      
</div>
<!--banner end--> 
<script type="text/javascript">
    $.validator.addMethod("pwcheck",function(value, element) {
        return /^^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/.test(value);
    });
    $("#form-client-register").validate({
      errorElement: 'span',
        rules: {
           password: 
                   {
                     required: true,
                     pwcheck: true,
                     minlength: 8
                   },
           confirm_password: 
                   {
                     required: true,
                     pwcheck: true,
                     minlength: 8
                   },
        },
    
     @if(isset($selected_lang) && App::isLocale('de'))
     messages: 
     {
          password:
               {
                 required: "Dieses Feld ist ein Pflichtfeld.",
                 pwcheck: "Geben Sie das Passwort mit Groß- und Kleinbuchstaben und Ziffern.",
                 minlength:"Passwort erforderlich mindestens 8 Zeichen.",
       
               },
          confirm_password:
               {
                 required: "Dieses Feld ist ein Pflichtfeld.",
                 pwcheck: "Geben Sie das Passwort mit Groß- und Kleinbuchstaben und Ziffern.",
                 minlength:"Passwort erforderlich mindestens 8 Zeichen.",
       
               },
      }
      @else
      messages: 
      {
           password:
                 {
                   required: "This field is required.",
                   pwcheck: "Password must have uppercase,lowercase & digit.",
                   minlength:"Password required minumum 8 characters.",
         
                 },
           confirm_password:
                 {
                   required: "This field is required.",
                   pwcheck: "Password must have uppercase,lowercase & digit.",
                   minlength:"Password required minumum 8 characters.",
         
                 },
       }
       @endif
    });
   
    var client_country_id = '{{isset($client_country_id) ? $client_country_id : ''}}';    
    var arr_blocked_country = '{{ isset($arr_mangopay_blocked_countries)?json_encode($arr_mangopay_blocked_countries):[] }}';
    
    $( document ).ready(function() {
      if($.inArray( client_country_id.toString(), arr_blocked_country ) != -1){
        $('#country_blocked').modal('toggle');
      }
    });

    function blocked_country(ref)
    {
        var country_id = $("#country_client").val();

         if($.inArray( country_id.toString(), arr_blocked_country ) != -1){
            $('#country_blocked').modal('toggle');
            $(ref).val('');
            return false;
        }
    }

</script>
@stop
