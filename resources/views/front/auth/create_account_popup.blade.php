<!-- Modal -->
 <div class="modal fade signup-landing-modal" id="signup_modal" role="dialog">
     <div class="modal-dialog modal-lg">
        <div class="modal-content">
           <button type="button" class="close" data-dismiss="modal"><img class="img-responsive" src="{{url('/public')}}/front/images/close-icon.png" alt="Close"/> </button>
           <div class="modal-body">
              <div class="signup-client-block">
                  <h2>{{ trans('auth/signup.text_i_am_a_client') }}</h2>
                  <h5>{{ trans('auth/signup.text_need_to_get_work_done') }}</h5>
                  <p>{{ trans('auth/signup.text_find_an_expertcolloborate_approve_amp_pay') }}</p>
                  <a href="{{url('/signup/client')}}" class="black-border-btn"><span></span> {{ trans('auth/signup.text_hire_an_expert') }}</a>
              </div>
              <div class="signup-client-block signup-expert-block">
                  <h2>{{ trans('auth/signup.text_i_am_an_expert') }}</h2>
                  <h5>{{ trans('auth/signup.text_looking_for_freelance_project') }}</h5>
                  <p>{{ trans('auth/signup.text_find_freelance_project_and_grow_your_business') }}</p>
                  <a href="{{url('/signup/expert')}}" class="black-border-btn"><span></span> {{ trans('auth/signup.text_work') }}</a>
              </div>
           </div>
        </div>
     </div>
 </div>
<!--model popup end here--> 