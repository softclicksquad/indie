@extends('front.layout.auth')                
@section('main_content')

<style type="text/css">
  .error {
    position: relative;
  }  
</style>

<div class="login-section pwd_section">
   <div class="container">
      <div class="login-email-form">
         <div class="col-lg-12">
            <div class="exper-clnt-login1">
               <form action="{{ url('/reset_password') }}" method="POST" id="form-reset-password" name="form-reset-password">
                  {{ csrf_field() }}
                  <div class="client-login">
                     <div class="head-title">{{ trans('auth/reset_password.text_heading') }}</div>
                     <div class="user-box">
                        <div class="control-label">{{ trans('auth/reset_password.text_password') }}:</div>
                        <div class="input-name"><input type="password" class="clint-input" placeholder="{{ trans('auth/reset_password.entry_password') }}" name="password" id="password" />
                           <span class='error'>{{ $errors->first('password') }}</span>
                        </div>
                     </div>
                     <div class="user-box">
                        <div class="control-label">{{ trans('auth/reset_password.text_confirm_password') }}:</div>
                        <div class="input-name">
                           <input type="password" class="clint-input" name="confirm_password" id="confirm_password" data-rule-required='true' data-rule-equalto="#password" placeholder="{{ trans('auth/reset_password.entry_confirm_password') }}" />
                           <span class='error'>{{ $errors->first('confirm_password') }}</span>
                        </div>
                     </div>
                     <input type="hidden" name="enc_id" value="{{ $enc_id or '' }}" />
                     <input type="hidden" name="enc_reminder_code"  value="{{ $enc_reminder_code or '' }}"/>
                     <button class="normal-btn" type="submit">{{ trans('auth/reset_password.text_reset') }}</button> 
                  </div>
               </form>
            </div>
            <div class="clearfix"></div>
         </div>
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript">
   $.validator.addMethod("pwcheck",
   function(value, element) {
      return /^^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/.test(value);
   });
    
   
    $("#form-reset-password").validate({
      errorElement: 'span',
       rules: {
     password: 
               {
                 required: true,
                 pwcheck: true,
                 minlength: 8
               }
         },
      @if(isset($selected_lang) && App::isLocale('de'))
     messages: 
     {
     password:
           {
             required: "Dieses Feld ist ein Pflichtfeld..",
             pwcheck: "Das Passwort muss in Großbuchstaben haben, Kleinschreibung und eine Ziffer",
             minlength:"Passwort erforderlich mindestens 8 Zeichen.",
   
           },
       }
       @else

      messages: 
     {
     password:
           {
             required: "This field is required.",
             pwcheck: "Password must have uppercase lowercase and one digit.",
             minlength:"Password required minumum 8 characters.",
   
           },
    
       }

       @endif
    });
   
   
</script>
@stop