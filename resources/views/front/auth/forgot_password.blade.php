@extends('front.layout.auth')
@section('main_content')
<!--banner start-->
<div class="login-section pwd_section">
   <div class="container">
      <div class="login-email-form">
       
            <div class="exper-clnt-login1">
               <div class="client-login">
                  <div class="head-title"> 
                  	<h2>{{ trans('auth/forgot_password.text_heading') }}</h2>
                  </div>
                  @include('front.layout._operation_status')
                  <form action="{{ url('/process_forgot_password') }}" method="POST" id="form-client-login" name="form-expert-login">
                     {{ csrf_field() }}
                     <div class="user-box">
                        <div class="control-label">{{ trans('auth/forgot_password.text_email') }}</div>
                        <div class="input-name"><input type="text" class="clint-input" placeholder="{{ trans('auth/forgot_password.entry_email') }}" name="email" />
                           <span class='error'>{{ $errors->first('email') }}</span>
                        </div>
                     </div>
                     <button class="normal-btn" type="submit">{{ trans('auth/forgot_password.text_send') }}</button> 
                     <div class="forget-pwd back-to-login"><a href="{{ url('/login') }}">{{ trans('auth/forgot_password.text_backto_login') }}</a></div>
                    </form>
               </div>
            </div>
            <div class="clearfix"></div>
         
         <div class="clearfix"></div>
      </div>
   </div>
</div>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript">
   $("#form-client-login").validate({
     errorElement: 'span',
      rules: {
        email:{
        required: true,
        email:true
     },
   }
   });
   
</script>
@stop