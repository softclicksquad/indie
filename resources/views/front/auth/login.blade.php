@extends('front.layout.auth')                
@section('main_content')
<style type="text/css">
.error{
  position: relative;
}

</style>
<div class="login-section main">
   <div class="container">
      <div class="login-wrapper">
      <div class="login-email-form">
         <div class="pagi">
            <div class="clearfix"></div>
         </div>
         <div class="exper-clnt-login1">
               <div style="display: none;" id="reg1" class="container1 act1" style="display: block;">
                  <form action="{{ url('/login') }}" method="POST" id="form-expert-login" name="form-expert-login">
                    {{ csrf_field() }}
                    <div class="client-login">
                       <div class="head-title"><h2>{{ trans('auth/login.text_login') }}</h2><p>Please Login your account.</p></div>
                       @include('front.layout._operation_status')
                       <div class="user-box">
                          <div class="control-label">{{ trans('auth/login.text_email') }} <span class="star">*</span></div>
                          <div class="input-name"><input type="text" class="clint-input" placeholder="{{ trans('auth/login.entry_email') }}" name="email" value="<?php if(!empty($_COOKIE['remember_me_email'])) { echo $_COOKIE['remember_me_email']; } ?>"/>
                          <span class='error'>{{ $errors->first('email') }}</span>
                          </div>
                       </div>
                       <div class="user-box">
                          <div class="control-label">{{ trans('auth/login.text_password') }} <span class="star">*</span></div>
                          <div class="input-name"><input type="password" class="clint-input" placeholder="{{ trans('auth/login.entry_password') }}" name="password" value="<?php if(!empty($_COOKIE['remember_me_password'])) { echo $_COOKIE['remember_me_password']; } ?>"/>
                          <span class='error'>{{ $errors->first('password') }}</span>
                          </div>
                       </div>
                       <div class="check-box">
                         <p>
                          <input id="filled-in-box" class="filled-in" type="checkbox" name="remember" <?php if(isset($_COOKIE['remember_me_email']) && $_COOKIE['remember_me_email'] != ""){ echo 'checked'; }?>>
                          <label for="filled-in-box">Remember me</label>
                         </p>
                    </div>
                    <div class="forget-pwd"><a href="{{ url('/forgot_password') }}">{{ trans('auth/login.text_forgot_password') }}</a></div>
                     <div class="clearfix"></div>
                      <div class="login-butn-wrapper">
                        <button class="normal-btn" type="submit">{{ trans('auth/login.text_login') }}</button>
                      </div> 
                     <!--<div class="divider">&nbsp;</div>-->
                     <div class="dont-acct">{{ trans('auth/login.text_dont_account') }} <a data-toggle="modal" href="#signup_modal"><span>{{ trans('auth/login.text_register_here') }}</a><!--  <a href="{{ url($module_url_path) }}">{{ trans('auth/login.text_register_here') }}</a> --></div>
                  </div>
                  </form>
               </div>
            </div>
            <div class="clearfix"></div>
      </div>
      </div>
   </div>
</div>
<script type="text/javascript">
   $(document).ready(function() {
     $('.container1').hide();
     $(".container1:first").addClass("act1").show(); 
     $('.regi_toggle button').click(function(){
       $('button').removeClass('act'); //remove the class from the button
       $(this).addClass('act'); //add the class to currently clicked button
       var target = "#" + $(this).data("target");
       $(".container1").not(target).hide();
       $(target).show();
         //target.slideToggle();
      });
   });
</script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript">
$.validator.addMethod("pwcheck",
function(value, element) {
return /^^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/.test(value);
});
$("#form-expert-login").validate({
errorElement: 'span',
rules: {
  email:{
    required: true,
    email:true
  },
  password: 
  {
    required: true,
    pwcheck: true,
    minlength: 8
  }
},
@if(isset($selected_lang) && App::isLocale('de'))
messages: 
{
  password:
  {
    required: "Dieses Feld ist ein Pflichtfeld..",
    pwcheck: "Ungültige Anmeldeinformationen. Bitte tragen Sie das richtige Passwort ein.",
    minlength:"Passwort sollte enthalten minumum 8 Zeichen.",
  },
 }
@else
messages: 
{
  password:
  {
    required: "This field is required.",
    pwcheck: "Invalid credentials !. Please Enter Correct Password.",
    minlength:"Password should contain minumum 8 characters.",
  },
 }
@endif
});
</script>
@stop