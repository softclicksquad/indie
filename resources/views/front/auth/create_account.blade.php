@extends('front.layout.auth')                
@section('main_content')

 <div class="top-title-pattern">
      <div class="container">
         <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
               <div class="title-box-top">
                  <h3>{{ trans('auth/signup.text_title') }}</h3>
               </div>
               <div class="top_breadcrumb"> <a href="{{url('/')}}">{{ trans('auth/signup.text_home') }} </a> &nbsp;/&nbsp; <a href="javascript: void(0);" class="act">{{ trans('auth/signup.text_title') }}</a></div>
            </div>
         </div>
      </div>
   </div>
    <div class="mida-new">
      <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="title-hiresa">
                    {{ trans('auth/signup.text_lets_start_with') }}
                    {{config('app.project.name')}} !<br>
                       {{ trans('auth/signup.text_tell_us_what_youre_looking_for') }}
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="tum">
                    <img src="{{url('/public')}}/front/images/user60.png" />
                     <div class="captin">
                      <h2>{{ trans('auth/signup.text_i_want_to_hire_a_expert') }}</h2>
                      <p class="space32">{{ trans('auth/signup.text_find_collaborate_with_and_pay_an_expert') }}</p>
                      <p class="space33"><a href="{{url('/signup/client')}}"> <button type="submit" class="normal-btn">{{ trans('auth/signup.text_hire') }}</button></a></p>
                      </div>                      
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6 ct">
                <div class="tum">
                      <img src="{{url('/public')}}/front/images/user601.png" />
                      <div class="captin">
                      <h2>{{ trans('auth/signup.text_i_m_looking_for_home_projets') }}</h2>
                      <p class="space32">{{ trans('auth/signup.text_find_freelance_projects_and_grow_your_business') }}</p>
                      <p class="space33"><a href="{{url('/signup/expert')}}"> <button type="submit" class="normal-btn ">{{ trans('auth/signup.text_work') }}</button></a>
                      </p>
                      </div>
               </div>
				<div class="orss">{{ trans('auth/signup.text_or') }}</div>
            </div>
          </div>
        </div>
       </div>   
   <div class="clearfix"></div>
@stop
