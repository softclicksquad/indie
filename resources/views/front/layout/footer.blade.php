<!-- Chat Api -->
<?php /* @include('ChatApIs.applozic.applozic') */?>
{{-- @include('ChatApIs.applozic.sidebox.sidebox') --}}

<?php 
    $add_money_url = 'javascript:void(0);';
    $user = Sentinel::check();  
    if(isset($user) && $user!=null)
    {
      if(isset($user) && $user->inRole('client'))
      {
        $add_money_url = url('/client/wallet/add_money');
      }
      elseif(isset($user) && $user->inRole('expert'))
      {
        $add_money_url = url('/expert/wallet/add_money');
      }
    }

    /*$user = Sentinel::check();  
    $wallet_bal = 0;
    $curr = get_default_currency($user->id);
    $mangopay_wallet_details = get_logged_user_wallet_details($curr);
    $wallet_bal = isset($mangopay_wallet_details->Balance->Amount)? $mangopay_wallet_details->Balance->Amount/100:'0';
    $wallet_currency = isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:'';
    //dd($arr_currency_backend);
    */
 ?>
<!-- add money model -->
<div class="modal fade invite-member-modal" id="add_money" role="dialog">
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <h4>{{trans('common/wallet/text.text_payin_in_to_wallet')}}</h4>
             </div>
             <form action="{{$add_money_url}}" method="post" id="mp-add-money-form">

                <div class="invite-form inter-amt">
                  <div class="user-box">
                      <div class="p-control-label">Currency <span class="star-col-block">*</span></div>
                      <div class="droup-select">
                        <select name="project_currency_code" id="project_currency_code_popup" onchange="set_currency_footer(this)" class="droup mrns tp-margn" data-rule-required="true">
                          @if(isset($arr_currency_backend) && sizeof($arr_currency_backend)>0)
                          <option value="">{{trans('client/projects/post.text_select_currency')}}</option>
                          @foreach($arr_currency_backend as $key => $currency)
                            <option 
                                @if(isset($currency['currency_code']) && isset($arr_data_bal['currency'][0]) && $arr_data_bal['currency'][0] == $currency['currency_code'] )
                                selected="" 
                                @endif
                                value="{{isset($currency['currency_code'])?$currency['currency_code']:''}}" 
                                data-code="{{isset($currency['currency'])?$currency['currency']:''}}" >
                                {{isset($currency['currency_code'])?$currency['currency_code']:''}} ({{isset($currency['description'])?$currency['description']:''}})
                          </option>
                          @endforeach
                          @endif
                      </select>
                      <span class='error'>{{ $errors->first('project_currency') }}</span>
                      <span class='error_select_currency' style="color:red;"></span>
                      </div>
                  </div>

              <div class="clearfix"></div>
               {{csrf_field()}}

                    <div class="user-box">
                       <div class="input-name">
                        <span class="amt-code">{{isset($arr_data_bal['currency'][0])?$arr_data_bal['currency'][0]:''}}</span>
                          <input type="text" class="clint-input char_restrict space_restrict" data-rule-required="true"  name="amount" id="amount_charge" placeholder="Enter amount">
                          <label for="amount_charge" class="error"></label>
                          <input type="hidden" name="currency_symbol" id="currency_symbol_code" >
                       </div>
                    </div>
                    
                    <div class="amt-info">
                      <span>Total Funds</span><span> <span class="amt-code">{{isset($arr_data_bal['currency'][0])?$arr_data_bal['currency'][0]:''}}</span> <span id="total_funds_amount"> 0.00 </span> </span>
                    </div>

                    <div class="amt-info">
                      <span>Service Fee</span><span> <span class="amt-code">{{isset($arr_data_bal['currency'][0])?$arr_data_bal['currency'][0]:''}}</span> <span id="service_charge"> 0.00 </span> </span>
                    </div>

                    <div class="amt-info no-border">
                      <span><b>Total</b></span><span><span class="amt-code">{{isset($arr_data_bal['currency'][0])?$arr_data_bal['currency'][0]:''}}</span> <span id="total_amount"> 0.00 </span> </span>
                    </div>

                    <button type="button" id="mp-add-money" class="black-btn">{{trans('common/wallet/text.text_payin')}}</button>
               </div>
             </form>
          </div>
       </div>
    </div>
</div>
<!-- end add money model -->

<!-- end chat api-->
     <script src="{{url('/public')}}/front/js/cookie.notice.js"></script>
     <!--Start Cookie Script--> 
     <!-- <div class="use-cookies-section-main">
         <div class="use-cookies-content-section">
             <div class="use-cookies-main-content">
                 We use cookies on this site to enhance your user experience
             </div>
             <div class="use-cookies-semi-content">
                 By clicking any link on this page you are giving your consent for us to set cookies.
             </div>
         </div>
         <div class="use-cookies-button-section">
             <button class="black-border-btn cookies-white-btn">OK, I agree </button>
             <button class="black-border-btn cookies-white-btn">No, give me more info </button>
         </div>
         <div class="clearcix"></div>
     </div> -->
     <!--End Cookie Script-->
     
      <!--footer start-->
      <div class="footer_copyright">
         <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
               <div class="copyright_l">{!! trans('common/footer.text_copyright') !!} </div>
                </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      <!-- jQuery -->
      @if(isset($selected_lang))
         @if(App::isLocale('de'))
            <script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/localization/messages_de.js"> </script>
         @endif
      @endif   
      {{-- image upload popup starts --}}
      @include('front.common._crop_uploaded_image')
      {{-- image upload popup ends --}}
      <script type='text/javascript' src="{{url('/public')}}/front/js/jquery-migrate-1.2.1.min.js"></script>
      <script type="text/javascript" language="javascript" src="{{url('/public')}}/front/js/flaunt.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="{{url('/public')}}/front/js/bootstrap.min.js" type="text/javascript"></script>
      <!-- inlcuding local storage js -->
      <script src="{{url('/public')}}/front/js/local_storage_store.min.js" type="text/javascript"></script>
      <!-- ends -->
      <a class="cd-top hidden-xs hidden-sm" href="#0">Top</a>
      <script type="text/javascript" language="javascript" src="{{url('/public')}}/front/js/backtotop.js"></script>
      <!--    Hide , show responsive menus  -->
      <script type="application/javascript">
        function check_decimal_number(n)
        {
          var result = (n - Math.floor(n)) !== 0; 
          if (result){
            return true;
          }
          return false; 
        }

        $('#mp-add-money').click(function() {
            $('#amount_charge').siblings('.error').html('').hide();
            if($('#mp-add-money-form').valid() == true){
              if( $("#project_currency_code_popup").val() == 'JPY' && check_decimal_number($('#amount_charge').val())){
                $('#amount_charge').siblings('.error').html('Decimal number not allowed for JPY currency.').show();
                return false;
              }
              $('#mp-add-money-form').submit();
            }
        });

        $('#amount_charge').keyup(function() {
          var token     = "<?php echo csrf_token(); ?>";
            var total = $('#amount_charge').val();
            var currency = $("#project_currency_code_popup option:selected").attr('data-code');
            $('#currency_symbol_code').val(currency);
            
            $('#total_funds_amount').text('0.0');
            $('#service_charge').text('0.0');
            $('#total_amount').text('0.0');

            if(currency==undefined)
            {
              $('.error_select_currency').text('Please select currency first');
              return false;
            }
            else
            {
              $('.error_select_currency').text('');
            }
            //var req_id = $(ref).attr('data-id');
            var url="{{url('/get_service_charge')}}";
                 $.ajax({
                type: 'post',
                contentType: 'application/x-www-form-urlencoded',
                data: $('#mp-add-money-form').serialize(),
                url:url,
                headers: {
                'X-CSRF-TOKEN': token
                },
                success: function(response) {
                    if(response.status!='error'){
                      $('#total_funds_amount').text(response.total_funds_amount);
                      $('#service_charge').text(response.service_charge);
                      $('#total_amount').text(response.total_amount);
                    }
                  }
              });
           });


      $(document).ready(function(){
        $('#region_id').click(function() {
          $('#reglon_tab').toggle();
          $('#region_id').prop('class','onclick-ft meshows');
        });
        $('#show_cnt').change(function(){
          $('#frm_show_cnt').submit();
        });
      });
      </script>
      <script type="text/javascript">
         var min_applicable_width = 980;
         $(document).ready(function() {
             applyResponsiveSlideUp($(this).width(), min_applicable_width);
         });
         function applyResponsiveSlideUp(current_width, min_applicable_width) { /* Set For Initial Screen */
             initResponsiveSlideUp(current_width, min_applicable_width); /* Listen Window Resize for further changes */
             $(window).bind('resize', function() {
                 if ($(this).width() <= min_applicable_width) {
                     unbindResponsiveSlideup();
                     bindResponsiveSlideup();
                 } else {
                     unbindResponsiveSlideup();
                 }
             });
         }
         function initResponsiveSlideUp(current_width, min_applicable_width) {
             if (current_width <= min_applicable_width) {
                 unbindResponsiveSlideup();
                 bindResponsiveSlideup();
             } else {
                 unbindResponsiveSlideup();
             }
         }
         function bindResponsiveSlideup() {
             $(".none-dpl").hide();
             $(".meshows").bind('click', function() {
                 var $ans = $(this).next(".none-dpl");
                 $ans.slideToggle();
                 $(".none-dpl").not($ans).slideUp();
                 $('.none-dpl').removeClass('active');
                 $('.meshows').not($(this)).removeClass('active');
                 $(this).toggleClass('active');
                 $(this).next('.none-dpl').toggleClass('active');
             });
         }
         function unbindResponsiveSlideup() {
             $(".meshows").unbind('click');
             $(".none-dpl").show();
         }
    /* script for clearing local storage on other pages */
    var pathArray = window.location.pathname.split( '/' );  
    if( typeof pathArray[2] != 'undefined' ){ 
      if(pathArray[4] == 'portfolio' &&  pathArray[5] == 'view'){
       console.log('Welcome to portfolio page.');
      }else{
        store.clear();          
      }
    }
    $(document).ready(function () {
     /*star rating demo*/
     $.fn.stars = function() {
       return $(this).each(function() {
         $(this).html($('<span />').width(Math.max(0, (Math.min(5, parseFloat($(this).html())))) * 20));
       });
     }
     $(function() {       
       $('span.stars').stars();
     });
     /* Page Loader Call */
     $('.show_loader').on('click',function () {
       showProcessingOverlay();
     });
  });
</script>  
@include('front.layout.loader')
<script type="text/javascript">
   $(document).ready(function() {
     if ($('#TaBle,.TaBle').length > 0){
        var oTable = $('#TaBle,.TaBle').dataTable({"bPaginate": false,"ordering": false,"info": false});
     }
   });
   $(document).ready(function(){
        $('input').attr('autocomplete', 'off');
   });
</script>
<script src="{{url('/public')}}/front/js/lightbox-plus-jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  $(".lightbox").click(function() {
    /*$('body').css('overflow', 'hidden');
      $('body').css('height', '100%');*/
  });
})
</script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var supportChatKey = '{{env('supportChatKey')}}';
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/'+supportChatKey+'/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<script type="text/javascript">
  function set_currency_footer(ref)
  {
    var token     = "<?php echo csrf_token(); ?>";
    var currency = $("#project_currency_code_popup option:selected").attr('data-code');
    // $('.amt-code').text(currency);
    $('.amt-code').text($(ref).val());

    $('#currency_symbol_code').val(currency);
    
    $('#total_funds_amount').text('0.0');
    $('#service_charge').text('0.0');
    $('#total_amount').text('0.0');

    if(currency==undefined)
    {
      $('.error_select_currency').text('Please select currency first');
      return false;
    }
    else
    {
      $('.error_select_currency').text('');
    }
    //var currency = $("#project_currency_code_popup option:selected").attr('data-code');
    //var req_id = $(ref).attr('data-id');
    var url="{{url('/get_service_charge')}}";
         $.ajax({
        type: 'post',
        contentType: 'application/x-www-form-urlencoded',
        data: $('#mp-add-money-form').serialize(),
        url:url,
        headers: {
        'X-CSRF-TOKEN': token
        },
        success: function(response) {
          if(response.status!='error'){
            $('#total_funds_amount').text(response.total_funds_amount);
            $('#service_charge').text(response.service_charge);
            $('#total_amount').text(response.total_amount);
          }
        }
      });

  }
</script>

@if(\Request::segment(2) != 'twilio-chat' && isset($twilio_access_token) && $twilio_access_token!='' )
  <script src="https://media.twiliocdn.com/sdk/js/chat/releases/4.0.0/twilio-chat.min.js"></script>
  <script src="{{url('/public')}}/front/js/jquery.toast.js"></script>
  <link href="{{url('/public')}}/front/css/jquery.toast.css" rel="stylesheet" type="text/css" />
  {{-- <link rel="stylesheet" href="{{url('/public/twilio-chat')}}/smiley/assets/sprites/emojione.sprites.css"/> --}}
  <style type="text/css">
    .emojione {
        width: 20px;
        height: 20px;
    }
  </style>
  <script type="text/javascript">
    var twilio_access_token = '{{ isset($twilio_access_token) ? $twilio_access_token : '' }}';
    var twilio_user_type = '{{ isset($twilio_user_type) ? $twilio_user_type : '' }}';
    var current_url_slug    = '{{ \Request::segment(2) }}';
    var chat_base_url       = '{{ url('/') }}';
    var TWILIO_CHAT_SERVICE_SID = 'IS4cebaef994fd43e98add440959a48929';
    var audio_ogg = new Audio('{{url('/public/twilio-chat/audio/chat.ogg')}}');
    var audio_mp3 = new Audio('{{url('/public/twilio-chat/audio/chat.mp3')}}');

    if( twilio_access_token != undefined && twilio_access_token != '' && current_url_slug != 'twilio-chat' ){
      
      $(document).ready(function () {
        var chatClient = false;
        Twilio.Chat.Client.create(twilio_access_token).then(client => {
          chatClient = client
          chatClient.on('messageAdded', function (message) {
            prepareAndShowTwilioToastNotication(message);
          });
          // when the access token is about to expire, refresh it
          chatClient.on('tokenAboutToExpire', function() {
            location.reload()
          });

          // if the access token already expired, refresh it
          chatClient.on('tokenExpired', function() {
            location.reload()
          });
        });
      });

      function prepareAndShowTwilioToastNotication(obj_message) {
        
        if( typeof obj_message == 'object' && obj_message.body != undefined && obj_message.channel.attributes != undefined ) {
          
          /*if(!eval(localStorage.sound)) {
            localStorage.sound = true;
          }*/

          var is_project_chat  = obj_message.channel.attributes.is_project_chat || false;
          var is_contest_chat  = obj_message.channel.attributes.is_contest_chat || false;

          var redirect_chat_url = chat_base_url + '/' + twilio_user_type + '/twilio-chat';
          var update_chat_channel_url = redirect_chat_url + '/update_channel';

          if(is_project_chat == true && obj_message.channel.attributes.project_bid_id!=undefined && obj_message.channel.attributes.project_bid_id!='') {
            redirect_chat_url = redirect_chat_url + '/projectbid/' + btoa(obj_message.channel.attributes.project_bid_id);
          } else if(is_contest_chat == true && obj_message.channel.attributes.contest_entry_id!=undefined && obj_message.channel.attributes.contest_entry_id!='') {
            redirect_chat_url = redirect_chat_url + '/contest/' + btoa(obj_message.channel.attributes.contest_entry_id);
          }

          var chat_name = '';
          if(twilio_user_type == 'client'){
            chat_name = obj_message.channel.attributes.client_chat_name || '';
          } else if(twilio_user_type == 'expert'){
            chat_name = obj_message.channel.attributes.expert_chat_name || '';  
          }
          
          if(redirect_chat_url !='' && chat_name !='' && obj_message.body !='' ){
            $.toast({
                text: obj_message.body + '<a target="_blank" href="'+redirect_chat_url+'"> click here </a>',
                heading: 'New message from ' + chat_name,
                icon: 'info',
                showHideTransition: 'fade',
                allowToastClose: true,
                hideAfter: false,
                stack: 1,
                position: 'bottom-right',          
                textAlign: 'left',
                loader: true,
                loaderBg: '#9EC600'
            });

            /*if(eval(localStorage.sound)) {
              audio_ogg.play().then(response => {
              }).catch(e => {
              })
              audio_mp3.play().then(response => {
              }).catch(e => {
              })
            }*/
            
            
            if( obj_message.channel.sid != undefined ) {
                $.ajax({
                  type: 'GET',
                  url: update_chat_channel_url,
                  data:{ channel_sid:obj_message.channel.sid },
                  success: function(response) {
                  }
                });
            }

          }
        }            
      }
    }

    var chat_appid  = '56378';
    var chat_auth   = 'd1a41466739250e8bca1d8dd3edd772c';
    var chat_id     = '{{ isset($user_auth_details['user_id']) ? $user_auth_details['user_id'] : 0 }}';
    var chat_name   = '{{ isset($user_auth_details['first_name']) ? $user_auth_details['first_name'] : '' }}';
    var chat_link   = ''; //Similarly populate it from session for user's profile link if exists
    var chat_avatar = '{{ isset($user_auth_details['profile_image']) ? $user_auth_details['profile_image'] : '' }}'; //Similarly populate it from session for user's avatar src if exists
    var chat_role   = ''; //Similarly populate it from session for user's role if exists
    
    var chat_friends = '0';
    if(chat_id == '5'){
      chat_friends = '6';
    } else if(chat_id == '6'){
      chat_friends = '5';
    }

    if(chat_id == '5' || chat_id == '6' ){
      (function() {
          var chat_css = document.createElement('link'); chat_css.rel = 'stylesheet'; chat_css.type = 'text/css'; chat_css.href = 'https://fast.cometondemand.net/'+chat_appid+'x_xchat.css';
          document.getElementsByTagName("head")[0].appendChild(chat_css);
          var chat_js = document.createElement('script'); chat_js.type = 'text/javascript'; chat_js.src = 'https://fast.cometondemand.net/'+chat_appid+'x_xchat.js'; var chat_script = document.getElementsByTagName('script')[0]; chat_script.parentNode.insertBefore(chat_js, chat_script);
      })();
    }
  </script>
@endif
</body>
</html>