
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type"    content="text/html; charset=utf-8" />
    <meta name="viewport"              content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description"           content="archexperts.com is an online marketplace for project outsourcing in 3D design environment.Start a project now and get your new interior and exterior designs quickly done by many very talented experts." />

    <meta name="google" value="notranslate">
    <meta http-equiv="Content-language" content="notranslate">
    <meta name="google" content="notranslate">
    <meta name="keywords"              content="archexperts" />
    <meta name="author"                content="archexperts" />
    <!-- ======================================================================== -->
    <title>{{ isset($page_title)?$page_title:"" }} - {{ config('app.project.name') }}</title>
    <link rel="icon" href="{{url('/public')}}/favicon.ico" type="image/x-icon" />
    <!-- Bootstrap Core CSS -->
    <link href="{{url('/public')}}/front/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- main CSS -->
    <link href="{{url('/public')}}/front/css/virtual-home.css?v=1" rel="stylesheet" type="text/css" />
    @if(Session::get('locale') == 'de')
    <link href="{{url('/public')}}/front/css/virtual-home_de.css" rel="stylesheet" type="text/css" />
    @endif  
    <!--font-awesome-css-start-here-->
    <link href="{{url('/public')}}/front/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--menu css-->
    <link href="{{url('/public')}}/front/css/menu.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{url('/public')}}/front/css/dropzone.css" rel="stylesheet" type="text/css" />


    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700&display=swap" rel="stylesheet">



    <!--Sweet alert css-->
    <link rel="stylesheet" type="text/css" href="{{url('/public')}}/front/css/sweetalert.css">

    <!--custom css-->
    <link href="{{url('/public')}}/front/css/custom.css" rel="stylesheet" type="text/css" />
    <link href="{{url('/public')}}/front/css/lightbox.min.css" rel="stylesheet" type="text/css" />
    <link href="{{url('/public')}}/front/css/loading_animate.css" rel="stylesheet" type="text/css" />
    <script src="{{url('/public')}}/front/js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <!--Jquery sweetalert js-->   
    <script type="text/javascript" src="{{url('/public/front/js/sweetalert.min.js')}}"></script>   
    <script type="text/javascript" src="{{url('/public/front/js/sweetalert_msg.js')}}"></script>   
    <!-- Jquery Validation Script Starts -->
    <script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/additional-methods.min.js"></script>
    <!-- Jquery Validation Script Ends -->
    <!-- custom scrollbars plugin -->
    <script src="{{url('/public')}}/front/js/dropzone.js"></script>

    <link href="{{url('/public')}}/front/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <script src="{{url('/public')}}/front/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="{{url('/public')}}/assets/T-validations/masterT-validations.js"></script>
    <!-- custom scrollbars plugin -->
    <link rel="stylesheet" type="text/css" href="{{url('/public')}}/front/css/1.10.12-datatables.min.css"/>
    <script type="text/javascript" src="{{url('/public')}}/front/js/1.10.12-datatables.min.js"></script>

    <!-- location -autocomplete -->
    <script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLKCmJEIrOrzdRklZHYer8q9qx2XLJ4Vs&sensor=false&libraries=places"></script>
    <script  src="{{url('/public')}}/front/js/geolocator/jquery.geocomplete.min.js"></script>
    <!-- end location -autocomplete --> 

    <!-- alertify -->
    <script src="{{url('/public')}}/assets/alertifyjs/alertify.min.js"></script>
    <script src="{{url('/public')}}/assets/alertifyjs/alertify.js"></script>
    <link rel="stylesheet" href="{{url('/public')}}/assets/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="{{url('/public')}}/assets/alertifyjs/alertify.min.css" id="toggleCSS" />
    <!-- end alertify -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" integrity="sha256-Z8TW+REiUm9zSQMGZH4bfZi52VJgMqETCbPFlGRB1P8=" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.js" integrity="sha256-ZvMf9li0M5GGriGUEKn1g6lLwnj5u+ENqCbLM5ItjQ0=" crossorigin="anonymous"></script>

    <style>
        .nav-list .nav-item.bg.after-login-walet{padding-right: 12px;cursor: pointer}
        .after-login-walet img{width: 22px;display: inline-block;vertical-align: middle}
        .nav-list .nav-item.bg.after-login-walet span{display: inline-block;vertical-align: middle;color: #3d3d3d;font-size: 14px;padding-left: 5px}
        .after-login-walet .money-dropdown li{padding-right: 10px;}
        .after-login-walet .money-dropdown{top: 20px;}
        .after-login-walet .money-dropdown.active{top:35px;}
        .after-login-walet .money-dropdown a{text-transform: none;display: inline-block;padding: 7px 10px;}
        .money-dropdown {position: absolute;background-color: #fff;box-shadow: 0 1px 5px rgba(0,0,0,0.2);top: 30px;right: 0;visibility: hidden;opacity: 0;transition: 0.5s;width:200px;}
        .money-dropdown.active {top: 40px;visibility: visible;opacity: 1;}
        .currency-list {padding: 6px 15px;color: #222;font-size: 13px;}
        .currency-title {padding: 6px 15px;margin-bottom: 10px; color: #000;border-bottom: 1px solid #e3e3e3;background-color: #f5f5f5;}
        .after-login-walet .money-dropdown .add-money-but{background-color: #2d2d2d;color: #fff !important;display: block;width: 100%; height: 40px;line-height:25px;text-align: center;font-family: 'robotoregular'!important;margin-top: 10px;}
        .after-login-walet .money-dropdown .arrow-up-mn {position: absolute;top: -14px;right: 10px;filter: contrast(0.9);}
    </style>

</head>
<script type="text/javascript">
    var site_url    = "<?php echo url('/'); ?>";
    var csrf_token  = "{{csrf_token()}}";  
    var applocal    = "{{Session::get('locale')}}";
    var currentdate = "<?php echo date('Y-m-d'); ?>";
</script>
<?php  
$user = Sentinel::check();  
$profile_img_public_path = url('/public').config('app.project.img_path.profile_image');

if(isset($profile_img_public_path) && isset($user_auth_details['profile_image']) && $user_auth_details['profile_image']!="" && file_exists('public/uploads/front/profile/'.$user_auth_details['profile_image']))
{
    $profile_image = isset($user_auth_details['profile_image'])?$profile_img_public_path.$user_auth_details['profile_image']:'';
}
else
{
    $profile_image = $profile_img_public_path.'default_profile_image.png';
}
/*$arr_data_bal = [];
$mangopay_wallet_details = get_logged_user_wallet_details();

if (isset($mangopay_wallet_details) && count($mangopay_wallet_details)>0)
{
foreach ($mangopay_wallet_details as $key => $value) 
{
if($value->Balance->Amount > 0)
{
$arr_data_bal['balance'][]  = isset($value->Balance->Amount)?$value->Balance->Amount/100:'0';
$arr_data_bal['currency'][] = isset($value->Balance->Currency)?$value->Balance->Currency:'';
}
}
}
*/
//dd(array_merge($arr_data_bal['balance'],$arr_data_bal['currency']));
//dd(max($arr_data_bal['balance']));
//dd($arr_data_bal);
?>
<body class='notranslate <?php if($user== false){ echo 'no-chat'; } ?>'>
    <?php $arr_data = get_static_page(); ?>
    <div class="header  <?php if($user){ echo "after-login-header"; } 
    if($user != null && $user->inRole('expert')) { echo " expert-login-header"; }  
    if($user != null && $user->inRole('project_manager')) { echo " project-manager-header"; }
    if($user != null && $user->inRole('recruiter')) { echo " recruiter-header"; }
    ?>">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
         <div class="logo">
          <a href="{{url('/')}}" class="hidden-xs hidden-sm hidden-md"><img class="img-responsive" src="{{url('/public')}}/front/images/virtual-logo.png" alt="Archexperts logo"/> </a>   
          <div class="phone-logo"><a href="{{url('/')}}" class="hidden-lg"><img class="img-responsive" src="{{url('/public')}}/front/images/small-logo.png" alt=""  /></a></div>
      </div>
      <!-- Language Selection -->
   <!--<div class="btn-group right-sml hidden-sm hidden-md hidden-lg">
      <button data-toggle="dropdown" class="btn btn-warning parcel lag dropdown-toggle"  type="button">
      @if(isset($selected_lang))
      @if(App::isLocale('en'))
      <img src="{{url('/public')}}/front/images/uk_l.png" alt="English"/>
      @elseif(App::isLocale('de'))
      <img src="{{url('/public')}}/front/images/germany_l.png" alt="Deutsch"/>
      @endif
      @endif
      </button>
      <button data-toggle="dropdown" class="btn btn-warning dropdown-toggle drp-btn" type="button">
      <span class="caret top_caret"></span>
      <span class="sr-only"></span>
      </button>
      <ul class="dropdown-menu top_drop sien fnts">
         <div class="arrow-up-mn show-res-moboile"> <img src="{{url('/public')}}/front/images/arrows-tp.png" alt=""/></div>
         <li><a href="{{url('/set_lang')}}/en"><img src="{{url('/public')}}/front/images/uk_l.png" alt="English"/> English</a>
         </li>
         <li><a href="{{url('/set_lang')}}/de"><img src="{{url('/public')}}/front/images/germany_l.png" alt="Deutsch"/> Deutsch </a>
         </li>
      </ul>
   </div> 
-->
<!-- End Language Selection -->

<!-- notification responsive-->
<div class="right-sml hidden-md hidden-lg">
    <?php 
    $user = Sentinel::check();
    if($user){
     if(isset($user) && $user->inRole('project_manager')){
      $path = "project_manager";  
  } elseif (isset($user) && $user->inRole('client'))  {
      $path = "client"; 
  } elseif (isset($user) && $user->inRole('expert'))  {
      $path = "expert"; 
  }elseif (isset($user) && $user->inRole('recruiter'))  {
      $path = "recruiter"; 
  }
  else {
      $path = url('/login');
  }
}
?>
@if($user)
@if(isset($arr_notification['notifications']))
<?php
$notification_count = 0;
if(isset($arr_notification['unread_messages']) && ($arr_notification['unread_messages'] > 0) ){
 $notification_count = 1;
}
$notification_count = count($arr_notification['notifications']) + $notification_count;
?>
<li class="nav-item last-br bg new-muns">
    <div class="btn-group mgrds newwwwwww">
     <button data-toggle="dropdown" class="conut-circl btn btn-warning parcel lag dropdown-toggle"  type="button">{{$notification_count}}</button>
     <button data-toggle="dropdown" class="btn btn-warning parcel lag dropdown-toggle"  type="button">
         @if(isset($selected_lang))
         @if(App::isLocale('en'))
         <img src="{{url('/public')}}/front/images/bell-icns.png" alt="English"/>
         @elseif(App::isLocale('de'))
         <img src="{{url('/public')}}/front/images/bell-icns.png" alt="German"/>
         @endif
         @endif
     </button> 
     <button data-toggle="dropdown" class="btn btn-warning dropdown-toggle drp-btn" type="button">
         <span class="caret top_caret"></span>
         <span class="sr-only"></span>
     </button>
     <ul class="dropdown-menu notifi-drop top_drop sien fnts scls" style="overflow: auto;">
      <div class="arrow-up-mn1"> <img src="{{url('/public')}}/front/images/arrows-tp.png" alt=""/></div>
      @if(isset($arr_notification['unread_messages']) &&  ( $arr_notification['unread_messages'] > 0) )
      <li>
        <a href="{{url('/')}}/{{$path}}/inbox">{{ trans('common/header.text_you_have') }} {{ $arr_notification['unread_messages'] }} {{ trans('common/header.text_new_messages') }}</a>
    </li>
    @endif
    @if($notification_count > 0)
      @foreach($arr_notification['notifications'] as $key => $notification ) 
        <?php
            if($key>=5)
            {
              continue;
            }

            $notification_text = '';
            if(isset($notification['notification_text_en']) && $notification['notification_text_en']!=null && isset($selected_lang) && App::isLocale('en')) {
              $notification_text = isset($notification['notification_text_en']) ? $notification['notification_text_en'] : '';
            }
            elseif(isset($notification['notification_text_de']) && $notification['notification_text_de']!=null && isset($selected_lang) && App::isLocale('de')) {
              $notification_text = isset($notification['notification_text_de']) ? $notification['notification_text_de'] : '';
            }

            if(strpos($notification_text, '-') != false) {
              $arr_notification_title = explode('-', $notification_text);
              $first_notification_title  = isset($arr_notification_title[0]) ? $arr_notification_title[0] : '';
              $first_notification_title = str_limit($first_notification_title, 10, '..');
              $second_notification_title = isset($arr_notification_title[1]) ? $arr_notification_title[1] : '';
              $notification_text = $first_notification_title .' - '. $second_notification_title;  
            }
        ?> 
        <li><a style="text-transform: initial !important;" onclick="return notification_seen('{{url('/public')}}/{{ $notification['url'] }}','{{ $notification['id'] }}')" href="javascript:void(0);">
          {{$notification_text or ''}}</a>
        </li>
      @endforeach
    @else
      <li>
        <a href="javascript:void(0);">{{ trans('common/header.text_no_notifications') }}</a>
      </li>
    @endif
</ul>
</div>
</li>
@endif   
@endif
</div>                  
<!--end notification responsive-->
<!--end profile menu-->

<div class="right-sml hidden-md hidden-lg profile-name-main-section">
    @if(isset($user_auth_details['is_login']) && $user_auth_details['is_login']==TRUE)
    <li class="nav-item last-br bg">
        <div class="btn-group">
         <button data-toggle="dropdown"  class="btn btn-warning parcel lag dropdown-toggle minwdth" type="button">
            @if(isset($user_auth_details['profile_image']) && $user_auth_details['profile_image']!="" )
            <img src="{{$user_auth_details['profile_image']}}" style="height:30px;width:30px;border-radius: 50px;" alt=""/>
            @else
            <img src="{{url('/public')}}/front/images/user_name.png" alt=""/>
            @endif
            {{isset($user_auth_details['first_name'])?substr($user_auth_details['first_name'],0,10):'Unknown user'}} {{-- @if(isset($user_auth_details['last_name']) && $user_auth_details['last_name'] !="") @php echo substr($user_auth_details['last_name'],0,1).'.'; @endphp @endif --}}
        </button>
        <button data-toggle="dropdown" class="btn btn-warning dropdown-toggle drp-btn dropdown-toggle block-drop-arrow" type="button">
         <span class="caret top_caret"></span>
         <span class="sr-only">Toggle Dropdown</span>
     </button>
     <ul class="dropdown-menu dash-menus-drop top_drop sien fnts right-arrw">
      <div class="arrow-up-mn show-res-moboile"> <img src="{{url('/public')}}/front/images/arrows-tp.png" alt=""/></div>
      @if(isset($user) && $user!=null)
      @if($user->inRole('client'))
      <li><a href="{{url('/client/dashboard')}}">{{ trans('common/header.text_dashboard') }}</a></li>
      <li><a href="{{url('/client/profile')}}">{{ trans('common/header.text_my_profile') }}</a></li>
      <li><a href="{{url('/client/wallet/dashboard')}}">{{ trans('common/header.text_my_wallet') }}</a></li>
      <li><a href="{{url('/client/twilio-chat/chat_list')}}">{{ trans('common/header.text_messages') }}</a></li>
      <li><a href="{{url('/client/change_password')}}">{{ trans('common/header.text_change_password') }}</a></li>
      @elseif($user->inRole('expert'))
      <li><a href="{{url('/expert/dashboard')}}">{{ trans('common/header.text_dashboard') }}</a></li>
      <li><a href="{{url('/expert/profile')}}">{{ trans('common/header.text_my_profile') }}</a></li>
      <li><a href="{{url('/expert/wallet/dashboard')}}">{{ trans('common/header.text_my_wallet') }}</a></li>
      <li><a href="{{url('/expert/twilio-chat/chat_list')}}">{{ trans('common/header.text_messages') }}</a></li>
      <li><a href="{{url('/expert/change_password')}}">{{ trans('common/header.text_change_password') }}</a></li>
      @elseif($user->inRole('project_manager'))
      <li><a href="{{url('/project_manager/dashboard')}}">{{ trans('common/header.text_dashboard') }}</a></li>
      <li><a href="{{url('/project_manager/profile')}}">{{ trans('common/header.text_my_profile') }}</a></li>
      <li><a class="applozic-launcher" href="javascript:void(0){{-- {{url('/project_manager/inbox')}} --}}">{{ trans('common/header.text_messages') }}</a></li>
      <li><a href="{{url('/project_manager/change_password')}}">{{ trans('common/header.text_change_password') }}</a></li>
      @elseif($user->inRole('recruiter'))
      <li><a href="{{url('/recruiter/dashboard')}}">{{ trans('common/header.text_dashboard') }}</a></li>
      <li><a href="{{url('/recruiter/profile')}}">{{ trans('common/header.text_my_profile') }}</a></li>
      <li><a class="applozic-launcher" href="javascript:void(0){{-- {{url('/recruiter/inbox')}} --}}">{{ trans('common/header.text_messages') }}</a></li>
      <li><a href="{{url('/recruiter/change_password')}}">{{ trans('common/header.text_change_password') }}</a></li>
      @endif
      <li><a href="{{url('/logout')}}">{{ trans('common/header.text_logout') }}</a></li>
      @endif
  </ul>
</div>
</li>                     
@endif
</div>

<!--end profile menu-->
<div class="right-sml hidden-md hidden-lg profile-name-main-section">
    @if(isset($user_auth_details['is_login']) && $user_auth_details['is_login']==TRUE)
    <li class="nav-item last-br bg hidden-xs after-login-menus after-login-walet">
        <div class="btn-group">
            <button data-toggle="dropdown"  class="btn btn-warning parcel lag dropdown-toggle " type="button">
                <img src="{{url('/public')}}/front/images/header-walet-icon.png" alt="English"/>
            {{-- <div id="replace_content" style="display: inline-block;">
             <span>
              0
            </span>
            <div class="money-dropdown">
              <div class="arrow-up-mn"> <img src="{{url('/public')}}/front/images/arrows-tp.png" alt=""></div>
              <div class="currency-title">Balances</div>

              <div class="currency-list">
                <a href="javascript:void(0)" class="mp-add-btn-text add-money-but payout-btn-section"> 
                    <i class="fa fa-minus-circle" aria-hidden="true"></i>Withdraw
                </a>
                <a href="#add_money" class="mp-add-btn-text add-money-but" data-keyboard="false" data-backdrop="static" data-toggle="modal"> 
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>Add Funds
                </a>
            </div>
          </div>
      </div> --}}
      <span>
        <?php //$arr_data_bal = Session::get('arr_data_bal'); ?>
        
        {{isset($arr_data_bal['currency'][0])?$arr_data_bal['currency'][0]:''}} 
        
        @if(isset($arr_data_bal['currency'][0]) && $arr_data_bal['currency'][0] == 'JPY')
          {{isset($arr_data_bal['balance'][0])?$arr_data_bal['balance'][0]:'0'}}
        @else
          {{isset($arr_data_bal['balance'][0])?number_format($arr_data_bal['balance'][0],2):'0.00'}}
        @endif
    </span>
</button>
@if($user != null && $user->mp_wallet_created=='Yes')
<button data-toggle="dropdown" class="btn btn-warning dropdown-toggle drp-btn dropdown-toggle block-drop-arrow" type="button">
   <span class="caret top_caret"></span>
   <span class="sr-only">Toggle Dropdown</span>
</button>
@endif
<ul class="dropdown-menu top_drop sien fnts right-arrw dash-menus-drop">
    <div class="arrow-up-mn"> <img src="{{url('/public')}}/front/images/arrows-tp.png" alt=""/></div>
    
    @if(isset($arr_data_bal['balance']) && count($arr_data_bal['balance'])>0)
    @foreach($arr_data_bal['balance'] as $key=>$value)
    <li><a href="javascript:void(0)">{{isset($arr_data_bal['currency'][$key])?$arr_data_bal['currency'][$key]:''}} {{isset($value)?number_format($value,2):''}} </a> </li>
    @endforeach
    @endif
    @if($user != null && $user->inRole('expert'))
    @if(isset($count_mangopay_wallet) && $count_mangopay_wallet > 1)
    <li><a href="{{url('/')}}/expert/dashboard"> See all balances</a></li>
    @endif
    @endif
    @if($user != null && $user->inRole('client'))
    <li><a href="{{url('/')}}/client/dashboard"> See all balances</a></li>
    @endif

    <div class="clearfix"></div>
    @if($user != null && $user->mp_wallet_created=='Yes')
    @if($user != null && $user->inRole('expert'))
    <a href="{{url('/')}}/expert/wallet/withdraw" class="mp-add-btn-text add-money-but payout-btn-section" style="background:#ed6c59;"> 
      Withdraw
  </a>
  @endif
  @if($user != null && $user->inRole('client'))
  <a href="{{url('/')}}/client/wallet/withdraw" class="mp-add-btn-text add-money-but payout-btn-section" style="background:#ed6c59;"> 
      <i class="fa fa-minus-circle" aria-hidden="true"></i>Withdraw
  </a>
  @endif

  <a href="#add_money" class="mp-add-btn-text add-money-but" data-keyboard="false" data-backdrop="static" data-toggle="modal" style="width:49.5%;background:#a3d06e;"><i class="fa fa-plus-circle" aria-hidden="true"></i>Add Funds
</a>
@endif

</ul>                                      
</div>
</li> 
@endif
</div>
<?php $user = Sentinel::check(); 
//$expert_info  = sidebar_information($user->id);
//dd($expert_info);

?>

<div class="rgt">
    <div class="nav">
        <ul class="menus nav-list mobi-menus">
          
         <li class="nav-item lef-ds">
          <a class="open-sub-menu text-transformation" @if(isset($user) && $user!=false) href="javascript:void(0);" @else href="{{url('/')}}/projects" @endif onclick="toggleBrowse(this);" id="project_browse" >{{ trans('common/header.text_browse') }}</a>   
          @if(isset($user) && $user!=false)                           
          <ul class="nav-submenu brows nav-submenu-open project_browse" style="display: none;">
             <div class="arrow-up-mn"> <img alt="" src="{{url('/public')}}/front/images/arrows-tp.png"></div>
             <div class="main-menus"  id="browseProject">
              <ul class="navbarss">
               <li class="nav-submenu-item "><a  style="color: #000000;" class="text-transformation" href="{{url('/')}}/projects">{{ trans('common/header.text_browse_jobs') }}</a></li>
               <?php if($user != null && $user->inRole('client')) {?>
                   <li class="nav-submenu-item"> <a style="color: #000000;" href="{{url('/')}}/client/projects/posted">My jobs</a></li>
                   <?php } else if($user != null && $user->inRole('expert')) {?>
                       <li class="nav-submenu-item"> <a style="color: #000000;" href="{{url('/')}}/expert/projects/ongoing">Ongoing jobs</a></li>
                       <?php } ?>
                       
                       <?php if($user != null && $user->inRole('expert')) {  
                          $expert_info  = sidebar_information($user->id);
                  //dd($expert_info);
                          ?>

                          <li class="nav-submenu-item "><a  style="color: #000000;" class="text-transformation" href="{{url('/')}}/projects/search/all/result?search={{$expert_info['ex_category_arr']}}{{$expert_info['ex_subcategory_arr']}}{{$expert_info['ex_skills']}}&max_fixed_amt={{$expert_info['max_fixed_amt']}}&min_fixed_amt={{$expert_info['min_fixed_amt']}}&max_project_amt={{$expert_info['max_project_amt']}}&min_project_amt={{$expert_info['min_project_amt']}}&is_job_matching=1">{{ trans('common/header.text_jobs_matching_my_skills_and_categories') }}</a></li>
                          <?php } ?>
                      </ul>
                  </div>
                  <div class="clr"></div>
              </ul>
              @endif
          </li>
          <li class="nav-item lef-ds">
              <a href="{{url('/')}}/experts">{{ trans('common/header.text_expert') }}</a>
          </li>
   <li class="nav-item lef-ds">
      <a class="open-sub-menu text-transformation" @if(isset($user) && $user!=false) href="javascript:void(0);" @else href="{{ url('/')}}/contests" @endif onclick="toggleBrowse(this);" id="project_contest" >{{trans('contets_listing/listing.text_contests')}}</a>      
      @if(isset($user) && $user!=false)                        
      <ul class="nav-submenu brows nav-submenu-open project_contest" style="display: none;">
         <div class="arrow-up-mn"> <img alt="" src="{{url('/public')}}/front/images/arrows-tp.png"></div>
         <div class="main-menus"  id="browseProject">
          <ul class="navbarss">
           <li class="nav-submenu-item "><a  style="color: #000000;" class="text-transformation" href="{{ url('/')}}/contests">{{ trans('common/header.text_browse_contest') }}</a></li>
           <?php if($user != null && $user->inRole('client')) {?>
               <li class="nav-submenu-item"> <a style="color: #000000;" href="{{url('/')}}/client/contest/posted">{{trans('client/contest/post.text_contests')}}</a></li>
               <?php } else if($user != null && $user->inRole('expert')) {?>
                   <li class="nav-submenu-item"> <a style="color: #000000;" href="{{url('/')}}/expert/contest/applied-contest">{{trans('client/contest/post.text_contests')}}</a></li>
                   <?php } ?>
                   
                   
                   <?php $user = Sentinel::check(); if($user != null && $user->inRole('expert')) {  
                      $expert_info  = sidebar_information($user->id);

                      ?>
                      <li class="nav-submenu-item "><a  style="color: #000000;" class="text-transformation" href="{{url('/')}}/contests/search?search={{$expert_info['ex_cat_arr']}}{{$expert_info['ex_subcategory_arr']}}{{$expert_info['ex_skills']}}&is_job_matching=1">{{ trans('common/header.text_contest_matching_my_skills_and_categories') }}</a></li>
                      <?php } ?>
                  </ul>
              </div>
              <div class="clr"></div>
          </ul>
          @endif
      </li>

      <li class="nav-item">
          @if(isset($arr_data['is_active']) && $arr_data['is_active']=="1")
          <a href="{{ url('/')}}/info/how-it-works">{{ trans('common/header.text_how_it_works') }}</a>
          @else
          <a href="javascript:void(0);" style="cursor: default;" >{{ trans('common/header.text_how_it_works') }}</a>
          @endif
      </li>                            
      @if(isset($user) && $user!=FALSE && $user->inRole('client'))
      <li class="nav-item hidden-md hidden-lg">  <a href="{{url('/')}}/client/projects/post">{{ trans('common/header.text_post_a_job') }}</a></li>
      <li class="nav-item hidden-md hidden-lg"><a href="{{url('/')}}/client/contest/post">{{trans('controller_translations.page_title_post_a_contest')}}</a></li>
      @endif 
      @if(isset($user_auth_details['is_login']) && $user_auth_details['is_login']==TRUE)
    <!-- <li class="nav-item last-br bg fnts hidden-xs">
      <div class="btn-group">
         <button data-toggle="dropdown" class="btn btn-warning parcel lag dropdown-toggle"  type="button">
         @if(isset($selected_lang))
         @if(App::isLocale('en'))
         <img src="{{url('/public')}}/front/images/uk_l.png" alt="English"/>
         @elseif(App::isLocale('de'))
         <img src="{{url('/public')}}/front/images/germany_l.png" alt="Deutsch"/>
         @endif
         @endif
         </button>
         <button data-toggle="dropdown" class="btn btn-warning dropdown-toggle drp-btn" type="button">
         <span class="caret top_caret"></span>
         <span class="sr-only"></span>
         </button>
         <ul class="dropdown-menu top_drop sien fnts right-arrw">
            <div class="arrow-up-mn"> <img src="{{url('/public')}}/front/images/arrows-tp.png" alt=""/></div>
            <li><a href="{{url('/set_lang')}}/en"><img src="{{url('/public')}}/front/images/uk_l.png" alt="English"/>English</a>
            </li>
            <li><a href="{{url('/set_lang')}}/de"><img src="{{url('/public')}}/front/images/germany_l.png" alt="Deutsch"/>Deutsch</a>
            </li>
         </ul>
      </div>
  </li> -->
  <?php
  $user = Sentinel::check();
  if(isset($user) && $user->inRole('project_manager'))
  {
    $path = "project_manager";  
} 
elseif (isset($user) && $user->inRole('recruiter')) 
{
    $path = "recruiter"; 
}
elseif (isset($user) && $user->inRole('client')) 
{
    $path = "client"; 
}
elseif (isset($user) && $user->inRole('expert')) 
{
    $path = "expert"; 
}
else
{
    $path = url('/login');
}
?>
@if($user)
@if(isset($arr_notification['notifications']))
<?php
$notification_count    = 0;
if(isset($arr_notification['unread_messages']) && ($arr_notification['unread_messages'] > 0) ){
 $notification_count = 1;
}
$notification_count    = count($arr_notification['notifications']) + $notification_count;
?>
<li class="nav-item last-br bg new-muns hidden-xs hidden-sm after-login-menus">
    <div class="btn-group mgrds olddddd">
     <button data-toggle="dropdown" class="conut-circl btn btn-warning parcel lag dropdown-toggle"  type="button">{{$notification_count}}</button>
     <button data-toggle="dropdown" class="btn btn-warning parcel lag dropdown-toggle"  type="button">
         @if(isset($selected_lang))
         @if(App::isLocale('en'))
         <img src="{{url('/public')}}/front/images/bell-icns.png" alt="English"/>
         @elseif(App::isLocale('de'))
         <img src="{{url('/public')}}/front/images/bell-icns.png" alt="German"/>
         @endif
         @endif
     </button>                                     
     <span class="sr-only"></span>
 </button>
 <ul class="dropdown-menu notifi-drop top_drop sien fnts scls notification-drop-section-block" style="overflow: auto;">
  <div class="arrow-up-mn1"> <img src="{{url('/public')}}/front/images/arrows-tp.png" alt=""/></div>
  @if(isset($arr_notification['unread_messages']) &&  ( $arr_notification['unread_messages'] > 0) )
  <li>
    <a href="{{url('/')}}/{{$path}}/inbox">{{ trans('common/header.text_you_have') }} {{ $arr_notification['unread_messages'] }} {{ trans('common/header.text_new_messages') }}</a>
</li>
@endif
@if($notification_count > 0)
@foreach($arr_notification['notifications'] as $key => $notification )
  <?php
      if($key>=5)
      {
        continue;
      }

      $notification_text = '';
      if(isset($notification['notification_text_en']) && $notification['notification_text_en']!=null && isset($selected_lang) && App::isLocale('en')) {
        $notification_text = isset($notification['notification_text_en']) ? $notification['notification_text_en'] : '';
      }
      elseif(isset($notification['notification_text_de']) && $notification['notification_text_de']!=null && isset($selected_lang) && App::isLocale('de')) {
        $notification_text = isset($notification['notification_text_de']) ? $notification['notification_text_de'] : '';
      }

      if(strpos($notification_text, '-') != false) {
        $arr_notification_title = explode('-', $notification_text);
        $first_notification_title  = isset($arr_notification_title[0]) ? $arr_notification_title[0] : '';
        $first_notification_title = str_limit($first_notification_title, 10, '..');
        $second_notification_title = isset($arr_notification_title[1]) ? $arr_notification_title[1] : '';
        $notification_text = $first_notification_title .' - '. $second_notification_title;  
      }
  ?> 
  <li><a style="text-transform: initial !important;" onclick="return notification_seen('{{url('/')}}/{{ $notification['url'] }}','{{ $notification['id'] }}')" href="javascript:void(0);">
    {{$notification_text or ''}}</a>
  </li> 
@endforeach
@else
<li>
    <a href="javascript:void(0);">{{ trans('common/header.text_no_notifications') }}</a>
</li>
@endif
<li><a class="view-all-btn-section" href="{{url('/')}}/{{$path}}/notifications">View All</a></li>
</ul>
</div>
</li>
@endif   
@endif
<li class="nav-item last-br bg hidden-xs hidden-sm after-login-menus">
  <div class="btn-group">
   <button data-toggle="dropdown"  class="btn btn-warning parcel lag dropdown-toggle " type="button">
     @if(isset($user_auth_details['profile_image']) && $user_auth_details['profile_image']!="" )
     <img src="{{$user_auth_details['profile_image']}}" style="height:30px;width:30px;border-radius: 50px;" alt=""/>
     @else
     <img src="{{url('/public')}}/front/images/user_name.png" alt=""/>
     @endif
     

     {{isset($user_auth_details['first_name'])?substr($user_auth_details['first_name'],0,10):'Unknown user'}} {{-- @if(isset($user_auth_details['last_name']) && $user_auth_details['last_name'] !="") @php echo substr($user_auth_details['last_name'],0,1); @endphp @endif --}}
 </button>
  @if($user != null && $user->mp_wallet_created=='Yes')
   <button data-toggle="dropdown" class="btn btn-warning dropdown-toggle drp-btn dropdown-toggle block-drop-arrow" type="button">
   <span class="caret top_caret"></span>
   <span class="sr-only">Toggle Dropdown</span>
   </button>
   @endif
<ul class="dropdown-menu top_drop sien fnts right-arrw dash-menus-drop">
    <div class="arrow-up-mn"> <img src="{{url('/public')}}/front/images/arrows-tp.png" alt=""/></div>
    @if(isset($user) && $user!=null)
    @if($user->inRole('client'))
    <li><a href="{{url('/client/dashboard')}}">{{ trans('common/header.text_dashboard') }}</a></li>
    <li><a href="{{url('/client/profile')}}">{{ trans('common/header.text_my_profile') }}</a></li>
    <li><a href="{{url('/client/wallet/dashboard')}}">{{ trans('common/header.text_my_wallet') }} </a></li>
    <li><a href="{{url('/client/twilio-chat/chat_list')}}">{{ trans('common/header.text_messages') }}</a></li>
    <li><a href="{{url('/client/tickets')}}">{{ trans('common/common.support_ticket') }}</a></li>
    <li><a href="{{url('/client/change_password')}}">{{ trans('common/header.text_change_password') }}</a></li>
    @elseif($user->inRole('expert'))
    <li><a href="{{url('/expert/dashboard')}}">{{ trans('common/header.text_dashboard') }}</a></li>
    <li><a href="{{url('/expert/profile')}}">{{ trans('common/header.text_my_profile') }}</a></li>
    <li><a href="{{url('/expert/wallet/dashboard')}}">{{ trans('common/header.text_my_wallet') }} </a></li>
    <li><a href="{{url('/expert/twilio-chat/chat_list')}}">{{ trans('common/header.text_messages') }}</a></li>
    <li><a href="{{url('/expert/tickets')}}">{{ trans('common/common.support_ticket') }}</a></li>
    <li><a href="{{url('/expert/change_password')}}">{{ trans('common/header.text_change_password') }}</a></li>
    @elseif($user->inRole('project_manager'))
    <li><a href="{{url('/project_manager/dashboard')}}">{{ trans('common/header.text_dashboard') }}</a></li>
    <li><a href="{{url('/project_manager/profile')}}">{{ trans('common/header.text_my_profile') }}</a></li>
    <li><a class="applozic-launcher" href="javascript:void(0){{-- {{url('/project_manager/inbox')}} --}}">{{ trans('common/header.text_messages') }}</a></li>
    <li><a href="{{url('/project_manager/change_password')}}">{{ trans('common/header.text_change_password') }}</a></li>
    @elseif($user->inRole('recruiter'))
    <li><a href="{{url('/recruiter/dashboard')}}">{{ trans('common/header.text_dashboard') }}</a></li>
    <li><a href="{{url('/recruiter/profile')}}">{{ trans('common/header.text_my_profile') }}</a></li>
    <li><a class="applozic-launcher" href="javascript:void(0){{-- {{url('/recruiter/inbox')}} --}}">{{ trans('common/header.text_messages') }}</a></li>
    <li><a href="{{url('/recruiter/change_password')}}">{{ trans('common/header.text_change_password') }}</a></li>
    @endif
    <li><a style="cursor:pointer;" href="javascript:void(0)" onclick="confirmDeleteAccount()" class="delete_account" >{{ trans('common/header.text_delete_account') }}</a></li>
    <script type="text/javascript">
     function confirmDeleteAccount(ref) {
      swal({
          title: "Are you sure, you want to delete your account?",
          text: "",
          icon: "warning",
          
          dangerMode: true,
          showCancelButton: true,
          confirmButtonColor: '#DD6B55',
          confirmButtonText: 'Yes, I am sure!',
          cancelButtonText: "No",
          closeOnConfirm: false,
          closeOnCancel: false
      },
      function(isConfirm){

         if (isConfirm){
          showProcessingOverlay();
          window.location.href="{{url('/delete_account')}}";
          return true;

      } else {
        swal.close();
        return false;
    }
});
  }
</script>
<li><a href="{{url('/logout')}}">{{ trans('common/header.text_logout') }}</a></li>
@endif
</ul>
</div>
</li>

@if($user->inRole('client') || $user->inRole('expert'))
<li class="nav-item last-br bg hidden-xs hidden-sm after-login-menus after-login-walet">
    <div class="btn-group">
        <button data-toggle="dropdown"  class="btn btn-warning parcel lag dropdown-toggle " type="button">
            <img src="{{url('/public')}}/front/images/header-walet-icon.png" alt="English"/>
            <span>
                <?php //$arr_data_bal = Session::get('arr_data_bal'); ?>
                
                {{isset($arr_data_bal['currency'][0])?$arr_data_bal['currency'][0]:''}} 

                @if(isset($arr_data_bal['currency'][0]) && $arr_data_bal['currency'][0] == 'JPY')
                  {{isset($arr_data_bal['balance'][0])?$arr_data_bal['balance'][0]:'0'}}
                @else
                  {{isset($arr_data_bal['balance'][0])?number_format($arr_data_bal['balance'][0],2):'0.00'}}
                @endif

            </span>
        </button>
         @if($user != null && $user->mp_wallet_created=='Yes')
        <button data-toggle="dropdown" class="btn btn-warning dropdown-toggle drp-btn dropdown-toggle block-drop-arrow" type="button">
           <span class="caret top_caret"></span>
           <span class="sr-only">Toggle Dropdown</span>
       </button>
       @endif
       <ul class="dropdown-menu top_drop sien fnts right-arrw dash-menus-drop">
        <div class="arrow-up-mn"> <img src="{{url('/public')}}/front/images/arrows-tp.png" alt=""/></div>
        @if($user != null && $user->inRole('expert'))
        @if(isset($count_mangopay_wallet) && $count_mangopay_wallet > 1)
        <li><a href="{{url('/')}}/expert/dashboard"> See all balances</a></li>
        @endif
        @endif
        @if($user != null && $user->inRole('client'))
        <li><a href="{{url('/')}}/client/dashboard"> See all balances</a></li>
        @endif

        <div class="clearfix"></div>
        @if($user != null && $user->mp_wallet_created=='Yes')
        @if($user != null && $user->inRole('expert'))
        <a href="{{url('/')}}/expert/wallet/withdraw" class="mp-add-btn-text add-money-but payout-btn-section" style="width:49.5%;background:#ed6c59;"> 
          <i class="fa fa-minus-circle" aria-hidden="true"></i>Withdraw
         </a>
      @endif
      @if($user != null && $user->inRole('client'))
      <a href="{{url('/')}}/client/wallet/withdraw" class="mp-add-btn-text add-money-but payout-btn-section" style="background:#ed6c59;"> 
          <i class="fa fa-minus-circle" aria-hidden="true"></i>Withdraw
      </a>
      @endif
      @if($user != null && $user->inRole('expert'))
      <a href="#add_money" class="mp-add-btn-text add-money-but" data-keyboard="false" data-backdrop="static" data-toggle="modal" style="width:49.5%;background:#a3d06e;"> 
          <i class="fa fa-plus-circle" aria-hidden="true"></i>Add Funds{{-- {{trans('common/wallet/text.text_payin')}} --}}
      </a>
      @endif
      @if($user != null && $user->inRole('client'))
      <a href="#add_money" class="mp-add-btn-text add-money-but" data-keyboard="false" data-backdrop="static" data-toggle="modal" style="width:53%;background:#a3d06e;"> <i class="fa fa-plus-circle" aria-hidden="true"></i>Add Funds
    </a>
    @endif
    @endif
</ul>                                      
</div>
</li> 
@endif                         


@else
<li class="nav-item"><a data-toggle="modal" href="#signup_modal"><span><img src="{{url('/public')}}/front/images/edit-icon.png" alt="edit icon"/> </span> {{ trans('common/header.text_sign_up') }}</a></li>
<li class="nav-item"><a href="{{url('/login')}}"> <span> <img src="{{url('/public')}}/front/images/login-icon.png" alt="login icon"/> </span>{{ trans('common/header.text_login') }}</a></li>
<li class="nav-item hidden-md hidden-lg"><a href="{{url('/login')}}"> <span> <img src="{{url('/public')}}/front/images/login-icon.png" alt="login icon"/> </span>{{ trans('common/header.text_post_a_job') }}</a></li>
<li class="nav-item hidden-md hidden-lg"><a href="{{url('/login')}}"> {{trans('controller_translations.page_title_post_a_contest')}}</a></li>

   <!-- <li class="nav-item last-br bg hidden-xs">
      <div class="btn-group">
         <button data-toggle="dropdown" class="btn btn-warning parcel lag dropdown-toggle" type="button">
         @if(isset($selected_lang))
         @if(App::isLocale('en'))
         <img src="{{url('/public')}}/front/images/uk_l.png" alt="English"/>
         @elseif(App::isLocale('de'))
         <img src="{{url('/public')}}/front/images/germany_l.png" alt="German"/>
         @endif
         @endif
         </button>
         <button data-toggle="dropdown" class="btn btn-warning dropdown-toggle drp-btn" type="button">
         <span class="caret top_caret"></span>
         <span class="sr-only"></span>
         </button>
         <ul class="dropdown-menu top_drop sien fnts right-arrw">
            <div class="arrow-up-mn"><img src="{{url('/public')}}/front/images/arrows-tp.png" alt=""/></div>
            <li><a href="{{url('/set_lang')}}/en"><img src="{{url('/public')}}/front/images/uk_l.png" alt=""/> English </a>
            </li>
            <li><a href="{{url('/set_lang')}}/de"><img src="{{url('/public')}}/front/images/germany_l.png" alt=""/> Deutsch </a>
            </li>
         </ul>
      </div>
  </li> -->
  @endif
</ul>
</div>
@if(isset($user_auth_details['is_login']) && $user_auth_details['is_login']==TRUE)
@if(isset($user) && $user!=null)
@if($user->inRole('client'))
<div class="header-buttons hidden-xs hidden-sm ">
  <a href="{{url('/')}}/client/projects/post" class="black-btn"><span class="post-job-icon">&nbsp;</span> {{ trans('common/header.text_post_a_job') }}</a>
  <a href="{{url('/')}}/client/contest/post" class="black-border-btn"><i style="font-size: 1.5em;" class="fa fa-trophy"></i>{{trans('controller_translations.page_title_post_a_contest')}}</a>
</div>
@endif
@endif
@else
<div class="header-buttons hidden-xs hidden-sm">
    <a href="{{url('/')}}/login" class="black-btn"><span class="post-job-icon">&nbsp;</span> {{ trans('common/header.text_post_a_job') }}</a>
    <a href="{{url('/')}}/login" class="black-border-btn"><i style="font-size: 1.5em;" class="fa fa-trophy"></i>{{trans('controller_translations.page_title_post_a_contest')}}</a>
</div>
@endif
</div>
</div>
</div>
</div>


@include('front.auth.create_account_popup')
<script type="text/javascript">
    /*Function for notification seen*/
    function notification_seen(url,id) {
        <?php 
        $user = Sentinel::check();
        if($user){
            if($user->inRole('expert'))
            {
                $user_type = 'expert';
            } 
            else if($user->inRole('client')) 
            {
                $user_type = 'client';
            } 
            else if($user->inRole('recruiter')) 
            {
                $user_type = 'recruiter';
            } 
            else 
            {
                $user_type = 'project_manager';
            }
            ?>
            var user_type = '<?php echo $user_type;?>'
            var site_url  = '{{url('/')}}';
            if(url && url!="" && id && id!="")
            {
                jQuery.ajax({
                    url:site_url+'/'+user_type+'/projects/notification/'+btoa(id),
                    type:'GET',                        
                    dataType:'json',
                    success:function(response)
                    {  
// console.log(response.status);return false;
                        if(response.status=="success"){
                         window.location.href = url;
                     } else {
    //window.location.href = '{{url('/')}}';
                     }
                 }
             });
            }
            <?php } ?>
        }
        /* browse skills */
        function browseSkills(ref){
            var skill_id = $(ref).attr('skill-id');
            if(skill_id != ""){
// window.location.href="{{url('/public')}}/projects/skills/"+skill_id;
window.location.href="{{url('/')}}/projects/";
}
}
function browseExpertsSkills(ref){
    var skill_id = $(ref).attr('skill-id');
    if(skill_id != ""){
        window.location.href="{{url('/')}}/experts/search_skill/"+skill_id;
    }
}
function browseCategories(ref){
    var category_id = $(ref).attr('category-id');
    if(category_id != ""){
        window.location.href="{{url('/')}}/projects/"+category_id;
    } 
}
function toggleActive(ref){ 
    $(ref).parent().find('ul').slideToggle(); 
}
function toggleBrowse(ref){ 
    var style = $(ref).next().css('display');
    var Id = $(ref).attr('id');          
    if(style == 'none'){
        $(ref).next().prop('display','block');
        if(Id == 'expert_browse'){
            $('.project_contest').hide(); 
            $('.project_browse').hide();
        } else if(Id == 'project_browse') {
            $('.expert_browse').hide();
            $('.project_contest').hide();
        } else if(Id == 'project_contest') {
            $('.expert_browse').hide();
            $('.project_browse').hide();
        }
    } else {
        $(ref).next().prop('display','none');
    }
    return ;
}
/* script to hide browse on header when click other than those classes */ 
$("body").on('click',function(event){  
    if($(event.target).hasClass('open-sub-menu') || $(event.target).hasClass('nav-submenu-open') || $(event.target).hasClass('sub') || $(event.target).hasClass('nav') ||$(event.target).hasClass('nav-submenu-item')  ){
        return ;
    }
    $(".nav-submenu-open").hide();
});
</script>
<script>
    $(".after-login-walet").on("click", function(){
        $(".money-dropdown").toggleClass("active"); 
    });
</script>
{{-- <script type="text/javascript">
$( document ).ready(function() {
var url="{{url('/get_wallet_details')}}";
$.ajax({
type: 'GET',
contentType: 'application/x-www-form-urlencoded',
//data: $('#mp-add-money-form').serialize(),
url:url,
// headers: {
// 'X-CSRF-TOKEN': $('#token').val()
// },
success: function(response) {

console.log(response);
$('#replace_content').empty();
$('#replace_content').append(response);

}
});
});
</script> --}}
<!--headar end-->
<div class="blank-div"></div>