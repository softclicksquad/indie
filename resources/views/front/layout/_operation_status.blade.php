<div id="op_status">
    @if(Session::has('success_sticky'))
    <div class="alert alert-success alert-dismissible close-message">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {!! Session::get('success_sticky') !!}
    </div>
  @endif  

  @if(Session::has('success'))
    <div class="alert alert-success alert-dismissible close-message">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {!! Session::get('success') !!}
    </div>
  @endif  
  
  @if(Session::has('error'))
    <div class="alert alert-danger alert-dismissible close-message">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {!! Session::get('error') !!}
    </div>
  @endif

  @if(Session::has('error_sticky'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {!! Session::get('error_sticky') !!}
    </div>
  @endif

</div>

<script type="text/javascript">
 
   setTimeout(function() {
           $('.close-message').fadeOut('fast');
           }, 10000);
          // <-- time in milliseconds
 </script>