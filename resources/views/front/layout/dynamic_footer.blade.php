 <div class="footer">
         <div class="container">
             <div class="footer-bg">
                <div class="row">
               <div class="col-sm-12 col-md-3 col-lg-3 abc">
                   <div class="footer_heading">{{ trans('common/footer.text_company_info')}}</div>
                  <div class="footer-inner-content">
                  <div class="menu_name">
                     <ul>

                        @if(isset($static_pages) && sizeof($static_pages)>0)
                           @foreach($static_pages as $pages)

                              @if(isset($pages['page_slug']) && $pages['page_name']!="")

                                 <li><a href="{{url('/')}}/info/{{$pages['page_slug']}}">{{$pages['page_name']}}</a></li>

                              @endif

                           @endforeach
                        @endif
                     </ul>
                  </div>
                </div>
               </div>
               <div class="col-sm-12 col-md-3 col-lg-3 abc">
                  <div class="footer_heading"> Browse Top Categories</div>
                  <div class="footer-inner-content">
                  <div class="menu_name">
                     <ul>

                        @if(isset($footer_categories) && sizeof($footer_categories)>0)
                          @foreach($footer_categories as $cat)
                              <li><a href="{{url('/projects')}}" style="cursor: pointer;">{{isset($cat['category_title'])?$cat['category_title']:''}}</a></li>      
                          @endforeach
                        @endif
                       <!-- <li><a href="#">3D Interior</a></li>
                            <li><a href="#">Architects</a></li> -->
                     </ul>
                  </div>
                 </div>
               </div>
               <div class="col-sm-12 col-md-3 col-lg-3 abc">
                  <div class="footer_heading">{{ trans('common/footer.text_connect_with_us') }}</div>
                  <div class="footer-inner-content">
                  <div class="menu_name">
                     <ul>
                        <li>
                           <a href="{{url('/')}}/contact-us">{{ trans('common/footer.text_contact_us') }}</a>
                        </li>
                        <?php $user = \Sentinel::check();?>
                        <li>
                           @if(isset($user) && $user != null && $user->inRole('expert'))
                           <a href="{{url('/')}}/expert/subscription">{{ trans('common/expert_sidebar.text_subscription') }} Plans</a>
                           @else
                           <a href="{{url('/')}}/subscription-plans">{{ trans('common/expert_sidebar.text_subscription') }} Plans</a>
                           @endif
                        </li>
                        <li>
                        <a href="{{url('/')}}/faq/client">{{trans('faq.text_faq_for_client')}}</a>
                        {{-- <a href="{{url('/public')}}/faq">{{trans('faq.text_faq')}}?</a> --}}
                        </li>
                        <li>
                        <a href="{{url('/')}}/faq/expert">{{trans('faq.text_faq_for_expert')}}</a>
                        </li>
                        <li>
                        <a href="{{url('/')}}/blog">Blog</a>
                        </li>
                     </ul>
                  </div>
                 </div>
               </div>
               <div class="col-sm-12 col-md-3 col-lg-3 abc">
               <div class="row" id="newsletter_op_status" style="display: none">
               <div class="alert alert-success" id="status_holder"></div>
               </div>
                  <div class="footer_heading">{{ trans('common/footer.text_newsletter') }}</div>
                  <div class="footer-inner-content">
                  <form action="{{url('/newsletter')}}" method="POST" id="form-newsletter" name="form-newsletter">
                  {{ csrf_field() }}
                  <div class="newsletter">
                     <input type="text" name="email" id="email" data-rule-required="true" data-rule-email="true" class="news-in" placeholder="{{ trans('common/footer.text_your_email') }}"/>
                      <input class="newbut" value="" type="button" onclick="processNewsletter()"/>
                       <span class='error new-eror'>{{ $errors->first('email') }}</span> 
                  </div>
                  </form>
                   </div>
                  <div class="footer_heading mrgs-tn">{{ trans('common/footer.text_follow_us') }}</div>
                  <div class="footer-inner-content">
                  <div class="social_icon">
                     <ul>
                        <li><a href="{{isset($arr_settings['fb_url'])?$arr_settings['fb_url']:'javascript:void(0)'}}" class="fb" target="_blank"> &nbsp;</a> </li>
                        <li><a href="{{isset($arr_settings['twitter_url'])?$arr_settings['twitter_url']:'javascript:void(0)'}}" class="twitter" target="_blank"> &nbsp;</a> </li>
                        <li><a href="{{isset($arr_settings['linkedin_url'])?$arr_settings['linkedin_url']:'javascript:void(0)'}}" class="in" target="_blank"> &nbsp;</a> </li>
                        <li><a href="{{isset($arr_settings['google_plus_url'])?$arr_settings['google_plus_url']:'javascript:void(0)'}}" class="google" target="_blank"> &nbsp;</a> </li>
                     </ul>
                  </div>
                </div> 
               </div>
            </div>
             </div>
         </div>
      </div>
    <script type="text/javascript">
    $(function() {
        $(".footer_heading").on("click", function() {
            $(this).toggleClass("active");
            $(this).next(".footer-inner-content").slideToggle("slow");
            $(this).parent(".abc").siblings().find(".footer-inner-content").slideUp();
            $(this).parent(".abc").siblings().children().removeClass("active");
        });
    });
</script>

<script type="text/javascript">
     $("#form-newsletter").validate({
      errorElement: 'span',
      });


   var url = "{{url('/')}}";

   function processNewsletter()
   {
      if($("#form-newsletter").valid())
      {
        
         $.ajax({
            url:url+"/signup_newsletter",
            type:'POST',
            data:$("#form-newsletter").serialize(),
            dataType:'json',
            success:function(response)
            {
              $("#form-newsletter")[0].reset();
              if(response.status=="SUCCESS")
              {
                $("#status_holder").removeClass("alert-danger").addClass('alert-success');
                $("#status_holder").html(response.msg);
                $("#newsletter_op_status").fadeIn();
              }
              else
              {
                $("#status_holder").removeClass("alert-success").addClass('alert-danger');
                $("#status_holder").html(response.msg);
                $("#newsletter_op_status").fadeIn();
              }

              setTimeout(function()
              {
                $("#newsletter_op_status").fadeOut();
                $("#status_holder").html("");
              },5000);
            }

         });
      }
   }
</script>