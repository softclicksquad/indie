<style type="text/css">
   .preloader{
      position: relative;
      margin: 0 auto;
      width: 100px;
    }
    .preloader:before{
        content: '';
        display: block;
        padding-top: 100%;
    }
    .circular {
      animation: rotate 2s linear infinite;
      height: 50px;
      transform-origin: center center;
      width: 50px;
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      margin: auto;
    }
    .path {
      stroke-dasharray: 1, 200;
      stroke-dashoffset: 0;
      animation: dash 1.5s ease-in-out infinite, color 6s ease-in-out infinite;
      stroke-linecap: round;
    }
    @keyframes rotate {
      100% {
        transform: rotate(360deg);
      }
    }
    @keyframes dash {
      0% {
        stroke-dasharray: 1, 200;
        stroke-dashoffset: 0;
      }
      50% {
        stroke-dasharray: 89, 200;
        stroke-dashoffset: -35px;
      }
      100% {
        stroke-dasharray: 89, 200;
        stroke-dashoffset: -124px;
      }
    }
    @keyframes color {
      100%,
      0% {
        stroke: #d62d20;
      }
      40% {
        stroke: #0057e7;
      }
      66% {
        stroke: #008744;
      }
      80%,
      90% {
        stroke: #ffa700;
      }
    }
  </style> 
<script>
 /* Processing Overlay  LOADER*/
  function showProcessingOverlay(text){
    /*var doc_height = $(document).height();
     var doc_width = $(document).width();
     var spinner_html = "";
     spinner_html += '<div class="sk-cube1 sk-cube"></div>';
     spinner_html += '<div class="sk-cube2 sk-cube"></div>';
     spinner_html += '<div class="sk-cube4 sk-cube"></div>';
     spinner_html += '<div class="sk-cube3 sk-cube"></div>';
     $("body").append("<div id='global_processing_overlay'><div class='sk-folding-cube'>"+spinner_html+"</div></div>");
     $("#global_processing_overlay").height(doc_height)
     .css({
       'opacity' : 0.5,
       'position': 'fixed',
       'top': 0,
       'left': 0,
       'background-color': 'black',
       'width': '100%',
       'z-index': 999999,
       'text-align': 'center',
       'vertical-align': 'middle',
       'margin': 'auto',
     });   */                         

    if(text == "" || text == undefined){ text = "";}
      if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading'){

        
      jQuery('body').append('<div id="resultLoading" style="display:none"><div><svg class="circular" viewBox="25 25 50 50"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg><div>'+text+'</div></div><div class="bg"></div></div>');
    }
    jQuery('#resultLoading').css({
    'width':'100%',
    'height':'100%',
    'position':'fixed',
    'z-index':'10000000',
    'top':'0',
    'left':'0',
    'right':'0',
    'bottom':'0',
    'margin':'auto'
    });

    jQuery('#resultLoading .bg').css({
    'background':'rgb(255, 255, 255 , 0.28)',
    'opacity':'0.7',
    'width':'100%',
    'height':'100%',
    'position':'absolute',
    'top':'0'
    });

    jQuery('#resultLoading>div:first').css({
    'width': '250px',
    'height':'75px',
    'text-align': 'center',
    'position': 'fixed',
    'top':'0',
    'left':'0',
    'right':'0',
    'bottom':'0',
    'margin':'auto',
    'font-size':'16px',
    'z-index':'10',
    'color':'#ffffff'

    });
    jQuery('#resultLoading .bg').height('100%');
    jQuery('#resultLoading').fadeIn(300);
    jQuery('body').css('cursor', 'wait'); 
  }
  function hideProcessingOverlay(){
    //$("#global_processing_overlay").remove();
    jQuery('#resultLoading .bg').height('100%');
    jQuery('#resultLoading').fadeOut(300);
    jQuery('body').css('cursor', 'default');
  } 
  $(window).on('beforeunload', function(){
  showProcessingOverlay('');
  });
  $(window).bind("load", function() { 
  hideProcessingOverlay();
  });

  $(window).bind("pageshow", function(event) {
    if (event.originalEvent.persisted) {
        //window.location.reload()
        hideProcessingOverlay();
    }
 });
</script>