@if(isset($arr_comments) && is_array($arr_comments) && sizeof($arr_comments)>0)
@foreach($arr_comments as $comment)
    @php 
                            if(isset($comment['user']['profile_image']))
                            {
                                $user_image = $user_image_public_path.$comment['user']['profile_image'];
                            }
                            else
                            {
                                $user_image = url('public/uploads/default-profile.png');
                            }

                            @endphp
<div class="comment-box">
    <div class="comment-profile-img">
        {{-- <img src="{{$user_image}}" alt="profile image"> --}}
    </div>
    <div class="comment-text">
        <h3>{{ $comment['user_name'] or ''}}</h3>
        <div class="comment-date-time">{{ get_comment_date_time($comment['created_at']) }}</div>
        <p class="profile-comment">{{ $comment['comment'] or ''}}</p>
    </div>
</div>
@endforeach
@endif