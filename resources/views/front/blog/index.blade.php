@extends('front.layout.master')                
@section('main_content')
    <div class="box-sizing">
        <div class="container">                        
            <div class="row">

                @if(isset($arr_blogs) && count($arr_blogs)>0)
                @foreach($arr_blogs as $key=>$value)
                <a href="{{url('/public')}}/blog/details/{{$value['slug']}}">
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="card-layout">
                        <div class="blog-img">
                            <span class="img-hover-label green">{{isset($value['category']['name'])?$value['category']['name']:''}}</span>
                            <div class="card-image">
                            @if(isset($value['image']) && $value['image']!='' && file_exists($blog_image_base_path.$value['image']))
                                <img src="{{$blog_image_public_path.$value['image']}}" alt="better-health">
                            @else
                                <img src="{{url('/public')}}/front/images/break-pads.jpg" alt="better-health">
                            @endif
                            </div>
                            <div class="shadow"></div>
                            <div class="calender">
                                <span>{{time_elapsed_string($value['created_at'])}}</span>
                               
                            </div>
                        </div>
                        <h2 class="blog-title">{{isset($value['name'])?$value['name']:''}}</h2>
                        <div class="blog-post">
                            {{isset($value['description'])?substr(strip_tags($value['description']), 0, 70):''}}
                        </div>
                        <div class="border-bottom"></div>
                        <div class="comment view-eye">
                            <i class="fa fa-eye"></i>
                            <a href="{{url('/')}}/blog/details/{{$value['slug']}}">{{isset($value['view_count'])?$value['view_count']:0}} View</a>
                        </div>
                        <div class="comment">
                            <i class="fa fa-commenting"></i>
                            <a href="{{url('/')}}/blog/details/{{$value['slug']}}">{{isset($value['comment_count'])?$value['comment_count']:0}} Comment</a>
                        </div>
                    </div>
                </div>
                </a>
                @endforeach
                @endif
               
            </div>
            <div class="product-pagi-block">
                {{ $obj_blogs->links() }}
                </div>
            {{-- <div class="page-navigation">
                        <ul class="page-nav">
                            <a href="#"><i class="fa fa-angle-left"></i></a>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>                            
                            <a href="#"><i class="fa fa-angle-right"></i></a>
                        </ul>
                    </div>   --}}          
        </div>
    </div>
    
@stop