@if(isset($arr_comments) && is_array($arr_comments) && sizeof($arr_comments)>0)
<div class="comments">
    <h2> {{ $comment_count or '0' }} comments</h2>
    @foreach($arr_comments as $comment)
    @php 
                            if(isset($comment['user']['profile_image']))
                            {
                                $user_image = $user_image_public_path.$comment['user']['profile_image'];
                            }
                            else
                            {
                                $user_image = url('uploads/default-profile.png');
                            }

                            @endphp
    <div class="comment-box">
        <div class="comment-profile-img">
            {{-- <img src="{{$user_image}}" alt="profile image"> --}}
        </div>
        <div class="comment-text">
            <h3>{{ $comment['user_name'] or ''}}</h3>
            <div class="comment-date-time">{{ get_comment_date_time($comment['created_at']) }}</div>
            <p class="profile-comment">{{ $comment['comment'] or ''}}</p>
        </div>
    </div>
    <script type="text/javascript">
     var page = 1;

function loadComments() {
    page++, $.ajax({
        url: "?page=" + page,
        type: "get",
        beforeSend: function() {
            $("#btn-load-more").html('<i class="fa fa-spinner fa-spin"></i> Loading')
        }
    }).done(function(e) {
        "end" == e.status ? $("#btn-load-more").html("No More Comments..") : ($("#btn-load-more").html("Load More"), $(".comments:last").append(e))
    })
}
</script>
@endforeach
</div>
@endif