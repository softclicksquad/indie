@extends('front.layout.master')                
@section('main_content')
   <div class="box-sizing">
        <div class="container">  
        @php 
        if(isset($arr_blog['image']))
        {
            $blog_image_path = $blog_image_public_path.$arr_blog['image'];
        }
        else
        {
            $blog_image_path = get_default_logo_image(420,690,50,'No Blog Image');
        }
        @endphp          
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="post">
                <div class="blog-post-title">
                    <h1>{{isset($arr_blog['name'])? $arr_blog['name'] :''}}</h1>
                </div>
                <div class="blog-author">
                <p class="text-color">by admin</p> 
                <p>{{ get_blog_formatted_date($arr_blog['created_at']) }}</p>                               
                </div>
                <div class="blog-post-img">
                    <img src="{{$blog_image_path or ''}}" alt="better health">
                </div>

                <div class="blog-post-1">
                    {!!$arr_blog['description'] or ''!!}
                </div>
                </div>

                <div class="dynamic-view">
                    @if(isset($arr_comments) && is_array($arr_comments) && sizeof($arr_comments)>0)
                    <div class="comments">
                        <h2> {{ $comment_count or '0' }} comments</h2>
                        @foreach($arr_comments as $comment)
                        <div class="comment-box">
                            @php 
                            if(isset($comment['user']['profile_image']))
                            {
                                $user_image = $user_image_public_path.$comment['user']['profile_image'];
                            }
                            else
                            {
                                $user_image = url('uploads/default-profile.png');
                            }

                            @endphp
                            {{-- <div class="comment-profile-img">
                                <img src="{{$user_image}}" alt="profile image">
                            </div> --}}
                            <div class="comment-text">
                                <h3>{{ $comment['user_name'] or ''}}</h3>
                                <div class="comment-date-time">{{ get_comment_date_time($comment['created_at'])}}</div>
                                <p class="profile-comment">{{ $comment['comment'] or ''}}</p>
                            </div>
                        </div>
                        @endforeach
                        <script type="text/javascript">
                           var page = 1;

                           function loadComments() {
                            page++, $.ajax({
                                url: "?page=" + page,
                                type: "get",
                                beforeSend: function() {
                                    $("#btn-load-more").html('<i class="fa fa-spinner fa-spin"></i> Loading')
                                }
                            }).done(function(e) {
                                "end" == e.status ? $("#btn-load-more").html("No More Comments..") : ($("#btn-load-more").html("Load More"), $(".comments:last").append(e))
                            })
                        }
                    </script>
                </div>
                @endif
            </div>

                @if(isset($arr_comments) && is_array($arr_comments) && sizeof($arr_comments)>0)
            <div class="post-comment login-btn-section">
                <a href="javascript:void(0)" class="normal-btn" onclick="loadComments()" id="btn-load-more">Load More</a>
            </div>
            @else
            <div style="margin-top:35px "></div> 
            @endif

            @php
            $tags = isset($arr_blog['tags'])? $arr_blog['tags']:null;
            @endphp

            @if(isset($tags))
            @php
            $arr_tags = explode(', ', $tags);
            @endphp
            @if(isset($arr_tags) && is_array($arr_tags) && sizeof($arr_tags)>0)

            <div class="comment-section">
                <h2>Tags</h2>
                <div class="div-tags">
                    @foreach($arr_tags as $str_tag)
                    <a href="{{ url('/blog/'.strtolower($str_tag)) }}"> {{ $str_tag or '' }}</a>
                    @endforeach
                </div>
            </div>
            @endif
            @endif
            {{-- @if(check_user_login()) --}}
            <div class="comment-section">
                <h2>Leave Your Comment</h2>

                <div class="alert alert-success alert-dismissible" hidden="hidden" id="div-comment-success">
                    <a href="#" class="close close-btn1" data-dismiss="alert" aria-label="close">&times;</a>
                    <span id="comment-success"></span>
                </div>

                <div class="alert alert-danger alert-dismissible" hidden="hidden" id="div-comment-error">
                    <a href="#" class="close close-btn1" data-dismiss="alert" aria-label="close">&times;</a>
                    <span id="comment-error"></span>
                </div>

                <form method="post" id="form-comment">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <textarea name="message" class="input-box" placeholder="Message*" id="message" cols="30" rows="10"></textarea>
                        <span class="bar"></span>
                    </div>                    
                    <div class="inline">
                        <div class="form-group">
                            <input type="text" class="clint-input" value="" placeholder="Your Name*" name="user_name" oninput="chk_validation(this)">
                            <span class="bar"></span>
                        </div>
                        <div class="form-group">
                            <input type="text" class="clint-input" placeholder="Your Email*" name="email">
                            <span class="bar"></span>
                        </div>
                    </div>
                    <input type="hidden" name="enc_id" value="{{ encrypt_data($arr_blog['id']) }}">

                    <div class="post-comment login-btn-section">
                        <button type="submit" class="normal-btn">Post Comment</button>
                    </div>
                </form>

            </div>
            {{-- @endif --}}
        </div>

            </div>            
        </div>
        </div>
    </div>
<script type="text/javascript">
$(document).ready(function() {
$("#form-comment").validate({
rules: {
message: {
    required: !0,
    maxlength: 300
},
user_name: {
    required: !0,
    maxlength: 255
},
email: {
    required: !0,
    email: !0,
    maxlength: 255
}
},
highlight: function(e) {},
errorClass: "error",
errorElement: "div",
errorPlacement: function(e, r) {
e.insertAfter(r.next())
}
}), $("#form-comment").submit(function(e) {
if (e.preventDefault(), $("#form-comment").valid()) {
var r = $("#form-comment").serialize();
if (r) {
    $.ajax({
        type: "post",
        dataType: "json",
        data: r,
        url: "{{$module_url_path.'/submit-comment'}}",
        beforeSend: showProcessingOverlay(),
        success: function(e) {
            if(e.message=='Please Login First.')
            {
                setTimeout(function(){ window.location.href= "{{url('/')}}/login"; }, 1000);
                
            }
            "success" == e.status ? ($("#div-comment-error").hide(), $("#comment-success").html(e.message), $("#div-comment-success").show(), $("#form-comment").trigger("reset"), $.ajax({
                url: window.location.href
            }).done(function(e) {
                $(".dynamic-view").html(e)
            }), hideProcessingOverlay()) : ($("#div-comment-success").hide(), $("#comment-error").html(e.message), $("#div-comment-error").show(), hideProcessingOverlay())
        }
    })
}
}
})
});
</script>
@stop