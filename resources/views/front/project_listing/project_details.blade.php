@extends('front.layout.master')                
@section('main_content')
<div class="middle-container">
     <div class="container">
        <div class="row">
              <?php
              //dd($all_projects_count);
              $category_id = "";
              if(isset($projectInfo['category_id']) && $projectInfo['category_id'] != "" ){
                 $category_id = $projectInfo['category_id'];
              }
              ?>
              <!-- Sidebar starts -->            
              @include('front.project_listing._sidebar_project_listing')
              <!-- Sidebar Ends-->
           <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
              <!-- Including top view of project details - Just pass project information array to the view -->
              @include('front.common._top_project_details',['projectInfo'=>$projectInfo])
              <!-- Ends -->   
           </div>
        </div>
     </div>
  </div>
</div>
<!-- Modal include-->
@include('client.projects.invite_expert') 
<!-- model popup end here--> 
<link href="{{url('/public')}}/front/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="{{url('/public')}}/front/js/jquery-ui.js" type="text/javascript"></script>
<script>
  $('.toggle-btn').click(function(){
      $(this).next('.add-skills-check-block').slideToggle();
      $(this).find('.search-free-slills-arrow i').toggleClass('fa-angle-down fa-angle-up');
  });
</script>  
<script type="text/javascript">
  /* Autosearching according to skills */
  $(document).ready( function () {
     $("input[name='search']").autocomplete({
        minLength:0,
        source:"{{ url('/autocomplete') }}",
        select:function(event,ui)
        {
          $("input[name='search']").val(ui.item.label);
        },
        response: function (event, ui){
        }
     });
  });
</script>
<script type="text/javascript">
    function submitSearch(ref){
      var sort_by; 
      sort_by = ref.value;
      $('#sort_by').prop('value',sort_by);
      $('#submit').trigger('click');
    }
    function searchBySkill(ref){
      var skill_name = $(ref).attr('data-skill-name');
      if(skill_name.trim() != ""){
        $('input[name="search"]').prop('value',skill_name.trim());
        $('#submit').trigger('click');
      } else  {
        console.log('No skill information available.');
      }
    }
    function addOrRemoveFavourite(ref){
      var project_id = $(ref).attr('data-project-id');
      var action     = $(ref).attr('data-action');
      var confirm_message = '{!! trans('expert/projects/favourite_projects.confirm_add_to_favourite') !!}';
      if(!confirm(confirm_message)){
        return false;
      }
      if(project_id != ""){
        var data  = { project_id:project_id,_token:"{{csrf_token()}}",action:action };
        $.ajax({
        url:"{{url('/public')}}"+'/projects/add_or_remove_favourite',
        type:'POST',
        dataType:'json',
        data: data,
        beforeSend: function(){
          showProcessingOverlay();                  
        },
        success:function(response){
          if(response.status == "FAVOURITE"){
            console.log('Project added to favourites successfully.');
          }
          if(response.status=="UNFAVOURITE"){
            console.log('Project added to favourites successfully.');
          }
          window.location.reload();
        }
        });
      }
    }
</script>
@stop