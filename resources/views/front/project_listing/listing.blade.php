@extends('front.layout.master')
@section('main_content')
<style type="text/css">
    .moretext {
        color: rgb(0, 0, 0);
        text-decoration: none;
        text-transform: capitalize;
    }
</style>

<?php $user = Sentinel::check(); ?>
<div class="search-freelance-gray-bg search-project">
    <div class="container">
        <div class="row">
            <!-- Sidebar starts -->
            @include('front.project_listing._sidebar_project_listing')
            <!-- Sidebar Ends-->
            <div class="col-sm-12 col-md-9 col-lg-9">
                @include('front.layout._operation_status')
                <div class="result-section">
                    <form id="frm_show_cnt" class="show-entry-form" method="POST" action="{{$module_url_path}}/show_cnt">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group white-bg">
                                    <div class="search-filter2">
                                        <input type="text" name="search" id="pro-search" value="{{ Request::get('search') }}" data-rule-required="true" placeholder="{{trans('project_listing/listing.text_search')}}" />
                                        <button type="submit" id="submit" style="background-color: Transparent;border:0;" class="search-icon bg-img show_loader submit_btn1"></button>
                                    </div>
                                </div>


                            </div>

                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <div class="showing-sort-bx">
                                <div class="showing-result-bx">
                                    @if(isset($arr_projects) && (sizeof($arr_projects)>0))
                                    <p>{{trans('expert_listing/listing.text_showing')}} <b>{{isset($arr_projects['total'])?$arr_projects['total']:'0'}}</b> {{trans('expert_listing/listing.text_results')}}</p>
                                    @else
                                    <p>{{trans('expert_listing/listing.text_showing')}} <b>0</b> {{trans('expert_listing/listing.text_results')}}</p>
                                    @endif
                                </div>
                                <div class="sort-by">
                                    <label>{{trans('expert_listing/listing.text_show')}}:</label>
                                    @php $show_cnt ='show_job_listing_cnt'; @endphp
                                    @include('front.common.show-cnt-selectbox')
                                </div>
                                 </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="clearfix"></div>
                @if(isset($arr_projects['data']) && sizeof($arr_projects['data'])>0)
                @foreach($arr_projects['data'] as $key=>$proRec)

                <div class="white-block-bg-no-padding">
                    @if(isset($proRec['highlight_end_date']) &&
                    $proRec['highlight_end_date'] !="" &&
                    $proRec['highlight_end_date'] !=null &&
                    $proRec['highlight_end_date'] !="0000-00-00 00:00:00" &&
                    date('Y-m-d', strtotime($proRec['highlight_end_date'])) >= date('Y-m-d') )
                    <div class="white-block-bg big-space hover highlite-bx">
                        @else
                        {{-- <div class="white-block-bg big-space hover @if(strpos($proRec['paid_services'],'highlight') !== false) highlite-bx @endif"> --}}
                        <div class="white-block-bg big-space hover @if(isset($proRec['highlight_end_date']) &&
                                        $proRec['highlight_end_date'] !="" &&
                                        $proRec['highlight_end_date'] !=null &&
                                        $proRec['highlight_end_date'] !="0000-00-00 00:00:00" &&
                                        date('Y-m-d', strtotime($proRec['highlight_end_date'])) >= date('Y-m-d') ) highlite-bx @endif">
                            @endif
                            <div class="project-left-side">
                                @if(isset($proRec['urjent_heighlight_end_date']) &&
                                $proRec['urjent_heighlight_end_date'] !="" &&
                                $proRec['urjent_heighlight_end_date'] !=null &&
                                $proRec['urjent_heighlight_end_date'] !="0000-00-00 00:00:00" &&
                                date('Y-m-d H:i:s', strtotime($proRec['urjent_heighlight_end_date'])) >= date('Y-m-d H:i:s') )
                                <div class="promoted-label urgent-class">Urgent</div>
                                @endif
                                <div class="search-freelancer-user-head">
                                    <a @if($proRec['project_type']==1 && Sentinel::check()==false || $proRec['project_type']==1 && Sentinel::check() !=false && $user->inRole('client') && $proRec['client_user_id'] !=$user->id) href="javascript:void(0)" style="cursor:no-drop;" @else href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}" @endif >{{$proRec['project_name'] or '-'}}</a>

                                </div>



                                <div class="search-freelancer-user-location">
                                    <span class="gavel-icon"><img src="{{url('/public')}}/front/images/bid.png" alt="" /></span> <span class="dur-txt"> {{trans('project_listing/listing.text_bids')}} {{isset($proRec['bid_count'])?$proRec['bid_count']:'0'}}</span>
                                </div>
                                @php $postes_time_ago = time_ago($proRec['created_at']); @endphp
                                <div class="search-freelancer-user-location">
                                    <span class="gavel-icon"><img src="{{url('/public')}}/front/images/clock.png" alt="" /></span> <span class="dur-txt"> {{trans('project_listing/listing.text_project_posted_on')}}: {{$postes_time_ago}}</span>
                                </div>
                                <div class="search-freelancer-user-location">
                                    <span class="gavel-icon"><img src="{{url('/public')}}/front/images/pro-time.png" alt="" /></span> <span class="dur-txt">Open for Bids : <span id='bids_left_timer{{$proRec["id"]}}'></span></span>
                                </div>

                                <div class="clearfix"></div>
                                @if($proRec['project_type'] == 1 && Sentinel::check() == false || $proRec['project_type'] == 1 && Sentinel::check() != false && $user->inRole('client') && $proRec['client_user_id'] !=$user->id)
                                <div class="more-project-dec"> <i> {{trans('controller_translations.private_project_note')}}</i></div>
                                @else
                                <div class="more-project-dec">{{str_limit($proRec['project_description'],157)}}
                                    @if(strlen(trim($proRec['project_description'])) > 157)
                                    <a class="moretext" href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}">
                                        {{trans('project_manager/projects/postedprojects.text_more')}}
                                    </a>
                                    @endif
                                </div>

                                <div class="category-new"> 
                                    <img src="{{url('/public')}}/front/images/tag-2.png" alt="" /> {{isset($proRec['category_details']['category_title'])?$proRec['category_details']['category_title']:'NA'}}
                                    
                                    &nbsp;&nbsp;
                                    <img src="{{url('/public')}}/front/images/tag-2.png" alt="" /> {{isset($proRec['sub_category_details']['subcategory_title'])?$proRec['sub_category_details']['subcategory_title']:'NA'}}

                                </div>

                                @if(isset($proRec['project_skills']) && count($proRec['project_skills']) > 0)
                                <div class="skils-project">
                                    <img src="{{url('/public')}}/front/images/tag.png" alt="" />
                                    <ul>
                                    @foreach($proRec['project_skills'] as $key=> $skills)
                                    @if(isset($skills['skill_data']['skill_name']) && $skills['skill_data']['skill_name'] != "")
                                    <li>{{ str_limit($skills['skill_data']['skill_name'],18) }}
                                        @if($key!=count($proRec['project_skills'])-1)
                                        ,
                                        @endif
                                    </li>
                                    @endif
                                    @endforeach
                                    </ul>
                                </div>
                                @endif
                                @endif



                            </div>
                            <?php if((isset($proRec['highlight_end_date']) &&
                                $proRec['highlight_end_date'] !="" &&
                                $proRec['highlight_end_date'] !=null &&
                                $proRec['highlight_end_date'] !="0000-00-00 00:00:00" &&
                                date('Y-m-d', strtotime($proRec['highlight_end_date'])) >= date('Y-m-d') ) || (isset($proRec['is_nda']) && $proRec['is_nda'] == 1) || (isset($proRec['is_urgent']) && $proRec['is_urgent'] == 1) || (isset($proRec['project_type']) && $proRec['project_type'] == 1))
                            { ?>
                            <div class="batches">
                             <?php }  ?>
                                <!-- 0 = no-featured / 1 = featured -->
                                @if(isset($proRec['highlight_end_date']) &&
                                $proRec['highlight_end_date'] !="" &&
                                $proRec['highlight_end_date'] !=null &&
                                $proRec['highlight_end_date'] !="0000-00-00 00:00:00" &&
                                date('Y-m-d', strtotime($proRec['highlight_end_date'])) >= date('Y-m-d') )
                                {{-- <span class="private hightlight">HIGHLIGHTED</span> --}}
                                @endif
                                <!-- 0 = no-nda / 1 = nda -->
                                @if(isset($proRec['is_nda']) && $proRec['is_nda'] == 1)
                                <span class="private nda">NDA</span>
                                @endif

                                <!-- 0 = no-urgent / 1 = urgent -->
                                @if(isset($proRec['is_urgent']) && $proRec['is_urgent'] == 1)
                                <!-- <span class="private">Urgent</span> -->
                                @endif


                                <!-- 0 = public / 1 = privare -->
                                @if(isset($proRec['project_type']) && $proRec['project_type'] == 1)
                                <span class="private pri-div">Private</span>
                                @endif
                            <?php if((isset($proRec['highlight_end_date']) &&
                                $proRec['highlight_end_date'] !="" &&
                                $proRec['highlight_end_date'] !=null &&
                                $proRec['highlight_end_date'] !="0000-00-00 00:00:00" &&
                                date('Y-m-d', strtotime($proRec['highlight_end_date'])) >= date('Y-m-d') ) || (isset($proRec['is_nda']) && $proRec['is_nda'] == 1) || (isset($proRec['is_urgent']) && $proRec['is_urgent'] == 1) || (isset($proRec['project_type']) && $proRec['project_type'] == 1))
                            { ?>
                            </div>
                            <?php }?>

                            <div class="search-project-listing-right contest-right">
                                <div class="search-project-right-price">
                                    <?php
                                        $pricing_method = "";
                                        if(isset($proRec['project_pricing_method']) && $proRec['project_pricing_method'] == '1')
                                        {
                                            $pricing_method = trans('project_listing/listing.text_budget');
                                        }
                                        else if(isset($proRec['project_pricing_method']) && $proRec['project_pricing_method'] == '2')
                                        {
                                            $pricing_method = trans('project_listing/listing.text_hourly_rate'); 
                                        }
                                    ?>

                                    <?php 
                                        
                                        $min_project_converted_price = $max_project_converted_price = $customize_project_converted_price = 0;
                                        $role = FALSE;
                                        $from_currency = isset($proRec['project_currency_code'])?$proRec['project_currency_code']:''; 
                                        $to_currency   = isset($arr_settings['default_currency_code'])?
                                                                      $arr_settings['default_currency_code']:'';
                                        $project_cost     = isset($proRec['project_cost'])?$proRec['project_cost']:'0';
                                               
                                        $min_project_cost = $max_project_cost = 0;
                                        $is_custom_price   = isset($proRec['is_custom_price'])?$proRec['is_custom_price']:'0';

                                        if($is_custom_price == '0') {
                                            $min_project_cost             = isset($proRec['min_project_cost'])?$proRec['min_project_cost']:'0';
                                            $min_project_converted_price  = currencyConverterAPI($from_currency,$to_currency,$min_project_cost);
                                            $min_project_converted_price  = number_format(floor($min_project_converted_price));
                                        }
                                        
                                        $max_project_cost             = isset($proRec['max_project_cost'])?$proRec['max_project_cost']:'0';
                                        $max_project_converted_price  = currencyConverterAPI($from_currency,$to_currency,$max_project_cost);
                                        $max_project_converted_price  = number_format(floor($max_project_converted_price));

                                        if($is_custom_price == '1') 
                                        {
                                            $customize_project_cost             = isset($proRec['project_cost'])?$proRec['project_cost']:'0';
                                            $customize_project_converted_price  = currencyConverterAPI($from_currency,$to_currency,$customize_project_cost);
                                            $customize_project_converted_price  = number_format(floor($customize_project_converted_price));
                                        }

                                        // $converted_price  = currencyConverterAPI($from_currency,$to_currency,$project_cost);
                                        // $converted_price  = number_format(floor($converted_price));
                                       
                                        if($user!=FALSE)
                                        {
                                          $role = $user->inRole('expert');
                                        }  

                                        $text_hour = '';
                                        if(isset($proRec['project_pricing_method']) && $proRec['project_pricing_method'] == '2')
                                        {
                                           $text_hour = trans('common/_top_project_details.text_hour');
                                        }
                                    ?>
                                    @if(($min_project_converted_price!= 0 && $max_project_converted_price!=0) && ($min_project_converted_price==$max_project_converted_price))
                                        Over
                                    @endif

                                    {{$arr_settings['default_currency']}}
                                    @if($is_custom_price == '0')

                                        @if(($min_project_converted_price!= 0 && $max_project_converted_price!=0) && ($min_project_converted_price!=$max_project_converted_price))
                                            {{ $min_project_converted_price }}-{{ $max_project_converted_price }}{{ $text_hour }}
                                        @elseif(($min_project_converted_price!= 0 && $max_project_converted_price!=0) && ($min_project_converted_price==$max_project_converted_price))
                                            {{ $min_project_converted_price }}{{ $text_hour }}
                                        @elseif($min_project_converted_price!= 0 && $max_project_converted_price == 0)
                                            {{ $min_project_converted_price }}{{ $text_hour }}
                                        @elseif($min_project_converted_price== 0 && $max_project_converted_price != 0)
                                            {{ $max_project_converted_price }}{{ $text_hour }}
                                        @endif

                                    @else   
                                        {{ $customize_project_converted_price }}{{ $text_hour }}
                                    @endif

                                    {{-- @if($user!= FALSE && $role==FALSE)
                                    {{isset($proRec['project_currency'])?$proRec['project_currency']:'$'}} 
                                    {{isset($proRec['project_cost'])? number_format(floor($proRec['project_cost'])):'0'}}
                                    @else
                                    {{$arr_settings['default_currency']}} {{$converted_price}}
                                    @endif --}}
                                    <span>{{$pricing_method or ''}}</span>
                                </div>
                                <div class="search-project-right-price">
                                    {{-- <span id='bids_left_timer{{$proRec["id"]}}' style="margin-top:0px;">-</span> --}}{{-- <span>Open for Bids</span> --}}
                                    <?php 
                                        $arr_diff_between_two_date = get_array_diff_between_two_date(date('Y-m-d H:i:s'),$proRec['bid_closing_date']);
                                        $str_days_diff = isset($arr_diff_between_two_date['str_days_diff']) ? $arr_diff_between_two_date['str_days_diff'] : '';
                                    ?>
                                    <script type="text/javascript">
                                        var projectid = '<?php echo $proRec["id"]; ?>';
                                        
                                        var contestid     = '{{ isset($contest["id"]) ? $contest["id"] : '' }}';
                                        var str_days_diff = '{{ isset($str_days_diff) ? $str_days_diff : '' }}';

                                        if (str_days_diff == '') {
                                            $('#bids_left_timer'+projectid).html('{{trans('client/projects/ongoing_projects.text_next_expire')}}');
                                            $('#bids_left_timer'+projectid).css('color', 'red');
                                        } else {
                                            $('#bids_left_timer'+projectid).html(str_days_diff);
                                        }
                                    </script>
                                </div>
                                <div class="clearfix"></div>
                                @if($user!= FALSE)
                                    @if(isset($proRec['is_bid_exists']) && $proRec['is_bid_exists'] == '1')
                                        @if(isset($proRec['is_project_rejected_by_expert']) && $proRec['is_project_rejected_by_expert']=='1')
                                            <a href="javascript:void(0)" class="black-border-btn black-hover">
                                            {{trans('project_listing/listing.text_project_rejected')}}
                                            </a>
                                        @else
                                            <a href="javascript:void(0)" class="black-border-btn active-green ">
                                                {{trans('project_listing/listing.text_applied')}}
                                            </a>
                                        @endif
                                    @elseif($user->inRole('expert'))
                                        @if(isset($proRec['bid_closing_date']) && $proRec['bid_closing_date'] > date('Y-m-d'))
                                            @if($user->mp_wallet_created == 'Yes')
                                                <a href="{{url('/')}}/expert/bids/add/{{base64_encode($proRec['id'])}}" class="black-border-btn black-hover">
                                                    {{trans('project_listing/listing.text_apply')}}
                                                </a>
                                            @else
                                                <button class="black-border-btn black-hover" data-toggle="modal" data-target="#place_bid_restrication">{{trans('project_listing/listing.text_apply')}}</button>
                                            @endif
                                        @endif
                                    @endif
                                @else
                                    <a @if($proRec['project_type']==1 && Sentinel::check()==false) href="javascript:void(0)" style="cursor:no-drop;" @else href="{{url('/')}}/login" @endif class="black-border-btn black-hover">
                                    {{trans('project_listing/listing.text_apply')}}
                                    </a>
                                @endif
                                <!-- <a href="javascript:void(0)" class="black-border-btn">Apply Now</a> -->
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @else
                    <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;" align="center">
                        <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                            <div class="search-content-block">
                                <div class="no-record">
                                    {{ trans('project_listing/listing.text_sorry_no_records_found') }}
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endif

                    <!--pagigation start here-->
                    {{-- @if(isset($obj_project) && !empty($obj_project)) --}}
                    @if($obj_project!='')
                    <div class="product-pagi-block">
                        {!! $obj_project->appends(request()->except(['page','_token'])) !!}
                    </div>
                    @endif
                    {{-- @else
                    @include('front.common.pagination_view', ['paginator' => $arr_projects])
                  @endif --}}
                    {{-- @include('front.common.pagination_view', ['paginator' => $arr_projects]) --}}
                    <!--pagigation end here-->
                </div>
            </div>
        </div>
    </div>

    <div id="place_bid_restrication" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog congratulations-popup">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 id="alert-modal-title" class="modal-title text-center">Alert !</h4>
              </div>
                <div class="modal-body">
                   <div id="alert-modal-body" class="modal-body">
                    <span class="you-may-need-message">For placing bid first to update profile details <a href="{{ url('/expert/profile') }}"> Update Profile</a></span>
                    </div>
                </div>                
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <script type="text/javascript">
        /* Autosearching according to skills */
        $(document).ready(function() {
            $("input[name='search']").autocomplete({
                minLength: 0,
                source: "{{ url('/autocomplete') }}",
                select: function(event, ui) {
                    $("input[name='search']").val(ui.item.label);
                    $('.submit_btn').click();
                },
                response: function(event, ui) {}
            });
        });
    </script>
    <script type="text/javascript">
        function submitSearch(ref) {
            var sort_by;
            sort_by = ref.value;
            $('#sort_by').prop('value', sort_by);
            $('#submit').trigger('click');
        }

        function searchBySkill(ref) {
            var skill_name = $(ref).attr('data-skill-name');
            if (skill_name.trim() != "") {
                $('input[name="search"]').prop('value', skill_name.trim());
                $('#submit').trigger('click');
            } else {
                console.log('No skill information available.');
            }
        }

        function addOrRemoveFavourite(ref) {
            var project_id = $(ref).attr('data-project-id');
            var action = $(ref).attr('data-action');
            var confirm_message = '{{trans('expert/projects/favourite_projects.confirm_add_to_favourite')}}';
            if (!confirm(confirm_message)) {
                return false;
            }
            if (project_id != "") {
                var data = {
                    project_id: project_id,
                    _token: "{{csrf_token()}}",
                    action: action
                };
                $.ajax({
                    url: "{{url('/')}}" + '/projects/add_or_remove_favourite',
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    beforeSend: function() {
                        showProcessingOverlay();
                    },
                    success: function(response) {
                        if (response.status == "FAVOURITE") {
                            console.log('Project added to favourites successfully.');
                        }
                        if (response.status == "UNFAVOURITE") {
                            console.log('Project added to favourites successfully.');
                        }
                        window.location.reload();
                    }
                });
            }
        }
    </script>
    @endsection