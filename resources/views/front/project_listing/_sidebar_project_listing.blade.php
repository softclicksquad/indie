<form action="{{$module_url_path}}/search/all/result" id="frm_search_project" method="any">

<style>
    [type="checkbox"].filled-in:checked + label::before{top: 6px;left: 1px;}
    .left-category-bx .check-box{padding-left: 4px;}
    .main-category-title [type="checkbox"] + label{width: 100%;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;}
    [type="checkbox"].filled-in:checked + label::after {top: 4px;}
</style>

<div class="col-sm-5 col-md-3 col-lg-3">
    <div class="filter-arrow-icon-section">
                        <i class="fa fa-filter"></i>
                        <i class="fa fa-times"></i>
                    </div>
    <div class="main-left-bar">
        <div class="mobile-title">Filtter</div>
        

    <div class="filter-block">
      <button type="submit" id="submit" style="display:none;background-color: Transparent;border:0;" class="search-icon bg-img show_loader submit_btn"></button>

        <?php
          /* Removing key from session after login from browse project page */

          if(Session::get('is_browse_url') && Session::get('is_browse_url') == "YES"){
              Session::forget('is_browse_url');
          }
          /* ends */
          $category_id = "";
          if(isset($_REQUEST['category']) && $_REQUEST['category'] != "") {
            $category_id = $_REQUEST['category'];
            if(is_array($category_id))
            {
              $category_id='';
            }
          }

          $sub_category_id = "";
          if(isset($_REQUEST['subcategory']) && $_REQUEST['subcategory'] != "") {
            $sub_category_id = $_REQUEST['subcategory'];
          }
          
          $is_job_matching = "0";
          if(isset($_REQUEST['is_job_matching']) && $_REQUEST['is_job_matching'] != 0) {
            $is_job_matching = '1';
          }
          if($is_job_matching == '1'){
            $expert_section_info = [];
            if(isset($user) && $user != null && $user->inRole('expert')) {  
                $expert_section_info  = sidebar_information($user->id);
            }
          }
        ?>
         <div class="left-category-bx"><h2>{{trans('project_listing/listing.text_fixed_rate')}} (In US Dollar) {{-- ({{$arr_settings['default_currency']}}) {{$arr_settings['default_currency_code']}} --}} </h2>
          <div class="range-t input-bx" for="fixed_rate">
              <div id="slider-price-range-fixed" class="slider-rang"></div>
              <div class="amount-no" id="slider_price_range_txt-fixed"></div>
              <input type="hidden" id="min_fixed_amt" name="min_fixed_amt" value="{{isset($min_fixed_amt)?$min_fixed_amt:0}}">
              <input type="hidden" id="max_fixed_amt" name="max_fixed_amt" value="{{isset($max_fixed_amt)?$max_fixed_amt:0}}">
              <input type="hidden" id="is_job_matching" name="is_job_matching" value="{{isset($is_job_matching) && $is_job_matching == '1' ? '1' : '0'}}">

              @if(isset($is_job_matching) && $is_job_matching == '1')
                @if(isset($expert_section_info['arr_category_ids']) && count($expert_section_info['arr_category_ids'])>0)
                  @foreach( $expert_section_info['arr_category_ids'] as $category_id )
                    <input type="hidden" name="category[]" value="{{ $category_id or '' }}">
                  @endforeach
                @endif

                @if(isset($expert_section_info['arr_sub_category_ids']) && count($expert_section_info['arr_sub_category_ids'])>0)
                  @foreach( $expert_section_info['arr_sub_category_ids'] as $subcategory_id )
                    <input type="hidden" name="subcategory[]" value="{{ $subcategory_id or '' }}">
                  @endforeach
                @endif

                @if(isset($expert_section_info['arr_skills_ids']) && count($expert_section_info['arr_skills_ids'])>0)
                  @foreach( $expert_section_info['arr_skills_ids'] as $skills_id )
                    <input type="hidden" name="skills[]" value="{{ $skills_id or '' }}">
                  @endforeach
                @endif
              @endif

          </div>
          </div>
          <div class="clearfix"></div>
        <div class="left-category-bx "><h2>{{trans('client/projects/post.text_hourly_title')}} (In US Dollar) {{-- ({{$arr_settings['default_currency']}}) {{$arr_settings['default_currency_code']}} --}} </h2>
          <div class="range-t input-bx" for="amount">
              <div id="slider-price-range-hourly" class="slider-rang"></div>
              <div class="amount-no" id="slider_price_range_txt-hourly"></div>
              <input type="hidden" id="min_contest_amt-hourly" name="min_project_amt" value="{{isset($min_project_amt)?$min_project_amt:0}}">
              <input type="hidden" id="max_contest_amt-hourly" name="max_project_amt" value="{{isset($max_project_amt)?$max_project_amt:0}}">
          </div>
          </div> 
          
          {{--  <div class="left-category-bx">
                    <h2>Category</h2>
                    <div class="main-category-title">
                        <ul>
                            <li>
                                <a class="category-main-title" href="javascript:void(0);">Architecture<span>08</span></a>
                                <ul class="sub-ul">
                                    <li>
                                        <div class="check-box">
                                            <p>
                                                <input class="filled-in subcategory" name="sub" id="sub-id" type="checkbox">
                                                <label for="sub-id">sub menu 1</label>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>

                                    <li>
                                        <div class="check-box">
                                            <p>
                                                <input class="filled-in subcategory" name="sub" id="sub-id" type="checkbox">
                                                <label for="sub-id">sub menu 1</label>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>

                                    <li>
                                        <div class="check-box">
                                            <p>
                                                <input class="filled-in subcategory" name="sub" id="sub-id" type="checkbox">
                                                <label for="sub-id">sub menu 1</label>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>

                                    <li>
                                        <div class="check-box">
                                            <p>
                                                <input class="filled-in subcategory" name="sub" id="sub-id" type="checkbox">
                                                <label for="sub-id">sub menu 1</label>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>

                                </ul>
                            </li>                            
                            <li>
                                <a class="category-main-title" href="javascript:void(0);">Fire Protection<span>08</span></a>
                                <ul class="sub-ul">
                                    <li>
                                        <div class="check-box">
                                            <p>
                                                <input class="filled-in subcategory" name="sub" id="sub-id" type="checkbox">
                                                <label for="sub-id">sub menu 1</label>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>

                                    <li>
                                        <div class="check-box">
                                            <p>
                                                <input class="filled-in subcategory" name="sub" id="sub-id" type="checkbox">
                                                <label for="sub-id">sub menu 1</label>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>

                                    <li>
                                        <div class="check-box">
                                            <p>
                                                <input class="filled-in subcategory" name="sub" id="sub-id" type="checkbox">
                                                <label for="sub-id">sub menu 1</label>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>

                                    <li>
                                        <div class="check-box">
                                            <p>
                                                <input class="filled-in subcategory" name="sub" id="sub-id" type="checkbox">
                                                <label for="sub-id">sub menu 1</label>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>

                                </ul>
                            </li>                            
                            <li>
                                <a class="category-main-title" href="javascript:void(0);">Protection Android<span>08</span></a>
                                <ul class="sub-ul">
                                    <li>
                                        <div class="check-box">
                                            <p>
                                                <input class="filled-in subcategory" name="sub" id="sub-id" type="checkbox">
                                                <label for="sub-id">sub menu 1</label>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>

                                    <li>
                                        <div class="check-box">
                                            <p>
                                                <input class="filled-in subcategory" name="sub" id="sub-id" type="checkbox">
                                                <label for="sub-id">sub menu 1</label>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>

                                    <li>
                                        <div class="check-box">
                                            <p>
                                                <input class="filled-in subcategory" name="sub" id="sub-id" type="checkbox">
                                                <label for="sub-id">sub menu 1</label>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>

                                    <li>
                                        <div class="check-box">
                                            <p>
                                                <input class="filled-in subcategory" name="sub" id="sub-id" type="checkbox">
                                                <label for="sub-id">sub menu 1</label>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>

                                </ul>
                            </li>
                        </ul>
                    </div>
                    <script>
                        $(".category-main-title").on("click", function() {
                            $(this).siblings(".sub-ul").slideToggle("slow");
                            $(this).parent().siblings().find(".sub-ul").slideUp("slow");
                        });
                    </script>
                </div> --}}

        <div class="left-category-bx">
          
        <h2>{{trans('project_listing/listing.text_category')}}</h2>
            <div class="main-category-title">
            <ul>
                <li @if($category_id=='') class='active' @endif> <a href="{{url('/projects')}}"> All <span> ({{ isset($all_project_count)?$all_project_count:0 }})</span> </a> </li>

                  @if(isset($arr_sidebar_data) && sizeof($arr_sidebar_data)>0)
                  @foreach($arr_sidebar_data as $proRec)
                        <?php  $selected = ""; 
                          if(isset($category_id) && $category_id != ""){
                            if(base64_encode($proRec['category_id']) == $category_id){
                              $selected = 'active';
                            }
                          }
                        ?>
                        <li class="{{$selected}}"> <a onclick="submit_category_search(this)" data-category="{{base64_encode($proRec['category_id'])}}">{{ str_limit(isset($proRec['category_title']) ? $proRec['category_title']:'',20) }}<span> ({{ isset($proRec['project_count']) ? $proRec['project_count']:0 }})</span> </a> 

                          @if($selected!='')
                            @if(isset($arr_subcategory) && count($arr_subcategory) > 0)
                            <?php $cnt=0; 
                              $searchVal = [];
                              if(!empty($_REQUEST['subcategory'])){
                                $searchVal = $_GET['subcategory'];
                              }
                              if(!empty(\Request::segment(2)) && \Request::segment(2) == 'subcategory' && !empty(\Request::segment(3)) && \Request::segment(3) != ''){
                                array_push($searchVal, \Request::segment(3));
                              }
                            ?>
                            @php $cnt=0;  @endphp
                              @foreach($arr_subcategory as $subcategory)
                                @if($cnt <= 6) 
                                  <div class="check-box cate_chk">
                                      <p>
                                          <input value="<?php echo base64_encode($subcategory['id']); ?>" class="filled-in subcategory" name="subcategory[]" id="skill{{$subcategory['id']}}" type="checkbox" <?php if(isset($searchVal) &&  in_array(base64_encode($subcategory['id']), $searchVal)) { echo 'checked'; } ?> >
                                          <label for="skill{{$subcategory['id']}}">{{str_limit(isset($subcategory['subcategory_title'])?$subcategory['subcategory_title']:'',20)}}</label>
                                      </p>
                                  </div>
                                @endif
                                 @if($cnt > 6) 
                                    <div class="check-box cate_chk view-more-content-skill"  <?php if(isset($searchVal) &&  in_array(base64_encode($subcategory['id']), $searchVal)) {  } else { echo 'style="display:none"';} ?>>
                                        <p>
                                            <input value="<?php echo base64_encode($subcategory['id']); ?>" class="filled-in subcategory" name="subcategory[]" id="skill{{$subcategory['id']}}" type="checkbox" <?php if(isset($searchVal) &&  in_array(base64_encode($subcategory['id']), $searchVal)) { echo 'checked'; } ?> >
                                            <label for="skill{{$subcategory['id']}}">{{str_limit(isset($subcategory['subcategory_title'])?$subcategory['subcategory_title']:'',20)}}</label>
                                        </p>
                                    </div>
                                 @endif 
                                 @php $cnt++; @endphp     
                              @endforeach    
                              <?php if($cnt > 6){ 
                                    echo '<a class="read-more more-btn-block-skill" href="javascript:void(0);">More..</a>
                                          <a href="javascript:void(0);" class="read-more hide-btn-block-skill" style="display:none">Hide</a>
                                          <div class="clr"></div>';
                              } ?>  
                            
                            @endif
                            @endif

                       </li>
                  @endforeach
                @endif  
                </ul>
            </div>
        </div>
        @if(isset($is_job_matching) && $is_job_matching == '0')
          <input type="hidden" name="category" id="category" value="{{isset($category_id)?$category_id:0}}">
        @endif
        <div class="clearfix"></div>
        
      </div>
      
          <div class="left-category-bx toggle-btn"><h2>Project Features</h2><span class="search-free-slills-arrow"></span>
          <?php  
            $searchProjectVal = [];
            if(!empty($_REQUEST['project'])){
              $searchProjectVal = $_GET['project'];
            }
          ?>
          <div class="add-skills-check-block content-d">
              <div class="check-box">
                  <p>
                      <input <?php if(isset($_REQUEST['project']) &&  in_array('featured', $searchProjectVal)) { echo 'checked'; } ?> id="featured_ptoject" name="project[]" value="featured" class="filled-in project_features" type="checkbox">
                      <label for="featured_ptoject">Highlighted 
                          <span class="tooltip-block"><span class="info-icon"><i class="fa fa-question-circle"></i></span><span class="tooltip-content">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span></span>
                      </label>
                  </p>
              </div>
              <div class="check-box">
                  <p>
                      <input <?php if(isset($_REQUEST['project']) &&  in_array('private', $searchProjectVal)) { echo 'checked'; } ?> id="private_project" name="project[]" value="private" class="filled-in project_features" type="checkbox">
                      <label for="private_project">Private 
                          <span class="tooltip-block"><span class="info-icon"><i class="fa fa-question-circle"></i></span><span class="tooltip-content">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span></span>
                      </label>
                  </p>
              </div>
              <div class="check-box">
                  <p>
                      <input <?php if(isset($_REQUEST['project']) &&  in_array('urgent', $searchProjectVal)) { echo 'checked'; } ?> id="urgent_project" name="project[]" value="urgent" class="filled-in project_features" type="checkbox">
                      <label for="urgent_project">Urgent 
                          <span class="tooltip-block"><span class="info-icon"><i class="fa fa-question-circle"></i></span><span class="tooltip-content">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span></span>
                      </label>
                  </p>
              </div>

              <div class="check-box">
                  <p>
                      <input <?php if(isset($_REQUEST['project']) &&  in_array('nda', $searchProjectVal)) { echo 'checked'; } ?> id="nda_project" name="project[]" value="nda" class="filled-in project_features" type="checkbox">
                      <label for="nda_project">NDA 
                          <span class="tooltip-block"><span class="info-icon"><i class="fa fa-question-circle"></i></span><span class="tooltip-content">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span></span>
                      </label>
                  </p>
              </div>
          </div>
          </div>
    
  </div>
</div>
</form> 
<link href="{{url('/public')}}/front/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="{{url('/public')}}/front/js/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript">
  $(function() {
        $("#slider-price-range-hourly").slider({
            range: true,
            min: {{isset($min_project_amt)?$min_project_amt:0}},
            max: {{isset($max_project_amt)?$max_project_amt:0}},
            values: [<?php if(isset($_GET['min_project_amt']) && $_GET['min_project_amt'] != "") { echo $_GET['min_project_amt']; } else { echo $min_project_amt; } ?>, <?php if(isset($_GET['max_project_amt']) && $_GET['max_project_amt'] != "") { echo $_GET['max_project_amt']; } else { echo $max_project_amt; } ?>],
            change: function(event, ui) 
            {
                $("#slider_price_range_txt-hourly").html("<span class='slider_price_min-hourly' style='float:left'>{{$arr_settings['default_currency']}} " + ui.values[0] + "</span>  <span class='slider_price_max-hourly' style='float:right'>{{$arr_settings['default_currency']}} " + ui.values[1] + " </span>");
                $("#min_contest_amt-hourly").val(ui.values[0]);
                $("#max_contest_amt-hourly").val(ui.values[1]);
                $('.submit_btn').click();
                // $("#frm_search_project").submit();
            }
        });
        $("#slider_price_range_txt-hourly").html("<span class='slider_price_min-hourly' style='float:left'> {{$arr_settings['default_currency']}} " + $("#slider-price-range-hourly").slider("values", 0) + "</span>  <span class='slider_price_max-hourly' style='float:right'>{{$arr_settings['default_currency']}} " + $("#slider-price-range-hourly").slider("values", 1) + "</span>");
       /* $("#slider_price_range_txt-hourly").html("<span class='slider_price_min-hourly' style='float:left'>" + $("#slider-price-range-hourly").slider("values", 0) + "</span>  <span class='slider_price_max-hourly' style='float:right'>" + $("#slider-price-range-hourly").slider("values", 1) + "</span>");*/
    });
  
  $(function() {
        $("#slider-price-range-fixed").slider({
             range: true,
             min: {{isset($min_fixed_amt)?$min_fixed_amt:0}},
             max: {{isset($max_fixed_amt)?$max_fixed_amt:0}},
             values: [<?php if(isset($_GET['min_fixed_amt']) && $_GET['min_fixed_amt'] != "") { echo $_GET['min_fixed_amt']; } else { echo $min_fixed_amt; } ?>, <?php if(isset($_GET['max_fixed_amt']) && $_GET['max_fixed_amt'] != "") { echo $_GET['max_fixed_amt']; } else { echo $max_fixed_amt; } ?>],
            change: function(event, ui) 
            {
                //console.log(ui.values[0],ui.values[1]);
                $("#slider_price_range_txt-fixed").html("<span class='slider_price_min-fixed' style='float:left'>{{$arr_settings['default_currency']}} "+ui.values[0]+"</span><span class='slider_price_max-fixed' style='float:right'>{{$arr_settings['default_currency']}} "+ui.values[1]+"</span>");
                $("#min_fixed_amt").val(ui.values[0]);
                $("#max_fixed_amt").val(ui.values[1]);
                $('.submit_btn').click();
                // $("#frm_search_project").submit();
            }
        });

        /*$("#slider_price_range_txt-fixed").html("<span class='slider_price_min-fixed' style='float:left'>"+$("#slider-price-range-fixed").slider("values",0)+"</span><span class='slider_price_max-fixed' style='float:right'>"+$("#slider-price-range-fixed").slider("values", 1)+"</span>");*/

        $("#slider_price_range_txt-fixed").html("<span class='slider_price_min-fixed' style='float:left'>{{$arr_settings['default_currency']}} "+$("#slider-price-range-fixed").slider("values",0)+"</span><span class='slider_price_max-fixed' style='float:right'>{{$arr_settings['default_currency']}} "+$("#slider-price-range-fixed").slider("values", 1)+"</span>");
    });
</script>

<script>
$('.toggle-btn').click(function(){
    $(this).next('.add-skills-check-block').slideToggle();
    $(this).find('.search-free-slills-arrow i').toggleClass('fa-angle-down fa-angle-up');
})
</script> 
<script type="text/javascript">
 $(".more-btn-block-skill").click(function() {
        $(this).parent().find(".view-more-content-skill").slideDown("slow");
        $(this).hide();
        $(this).parent().find(".hide-btn-block-skill").show();
 });
 $(".hide-btn-block-skill").click(function() {
      $(this).parent().find(".view-more-content-skill").slideUp("slow");
      $(this).parent().find(".more-btn-block-skill").show();
      $(this).hide();
 });

function submit_category_search(ref)
{
  var cat_id = $(ref).attr('data-category');
  $('#category').val(cat_id);
  $('.submit_btn').click();
}

function submit_sub_category_search(ref)
{
  var cat_id = $(ref).attr('data-subcategory');
  $('#subcategory').val(cat_id);

  $('.submit_btn').click();
}
 
 $('.subcategory,.price,.project_features').click(function() {
    setTimeout(function(){
      $('.submit_btn').click();  
    },2);   
 });
 $('.fixed_rate').click(function(){
    setTimeout(function(){
      $('.hourly_rate').prop('checked',false);
      $('.submit_btn').click();  
    },2);
 });
 $('.hourly_rate').click(function(){
    setTimeout(function(){
      $('.fixed_rate').prop('checked',false);
      $('.submit_btn').click();  
    },2);
 });
 $('#pro-search').blur(function(){
    setTimeout(function(){
      var search_val = $('#pro-search').val();
      if(search_val != ""){
        $('.submit_btn').click();  
      }
    },2);
 });
</script> 

 <script>
        $(".filter-arrow-icon-section").on("click", function(){
            $("body").toggleClass("filter-open");            
        });
    </script>