@extends('front.layout.master')                
@section('main_content')
<style type="text/css">
.moretext{
    color: rgb(0, 0, 0);
    text-decoration: none;
    text-transform: capitalize;
}

</style>
<?php $user = Sentinel::check();  ?>
 <div class="search-freelance-gray-bg search-project">
        <div class="container">
            <div class="row">
                <!-- Sidebar starts -->            
                 @include('front.project_listing._sidebar_project_listing')
                <!-- Sidebar Ends-->
                <div class="col-sm-7 col-md-9 col-lg-9">
                  @include('front.layout._operation_status')
                    <div class="result-section">
                      <form id="frm_show_cnt" class="form-horizontal show-entry-form" method="POST" action="{{$module_url_path}}/show_cnt">
                         {{ csrf_field() }}
                          <div class="row">
                              <div class="col-sm-6 col-md-6 col-lg-6">

                                   @if(isset($arr_projects) && (sizeof($arr_projects)>0)) 
                                   <p>{{trans('expert_listing/listing.text_showing')}} <b>{{isset($arr_projects['total'])?$arr_projects['total']:'0'}}</b> {{trans('expert_listing/listing.text_results')}}</p>
                                   @else
                                   <p>{{trans('expert_listing/listing.text_showing')}} <b>0</b> {{trans('expert_listing/listing.text_results')}}</p>
                                   @endif
                              </div>
                              <div class="col-sm-6 col-md-6 col-lg-6">
                                 <div class="sort-by">
                                   <label>{{trans('expert_listing/listing.text_show')}}:</label>
                                      @php $show_cnt ='show_job_listing_cnt';  @endphp
                                      @include('front.common.show-cnt-selectbox')
                                    </div>
                              </div>
                          </div>
                      </form> 
               </div>

               
                <div class="clearfix"></div>
                    @if(isset($arr_projects['data']) && sizeof($arr_projects['data'])>0)
                        @foreach($arr_projects['data'] as $proRec)
                            <div class="white-block-bg-no-padding">
                              @if(isset($proRec['highlight_end_date'])       && 
                                                $proRec['highlight_end_date'] !=""   && 
                                                $proRec['highlight_end_date'] !=null && 
                                                $proRec['highlight_end_date'] !="0000-00-00 00:00:00" && 
                                                date('Y-m-d', strtotime($proRec['highlight_end_date'])) >= date('Y-m-d') )
                                <div class="white-block-bg big-space hover highlite-bx">
                              @else
                                <div class="white-block-bg big-space hover">
                              @endif
                                  
                                        @if(isset($proRec['urjent_heighlight_end_date'])       && 
                                                          $proRec['urjent_heighlight_end_date'] !=""   && 
                                                          $proRec['urjent_heighlight_end_date'] !=null && 
                                                          $proRec['urjent_heighlight_end_date'] !="0000-00-00 00:00:00" && 
                                                          date('Y-m-d H:i:s', strtotime($proRec['urjent_heighlight_end_date'])) >= date('Y-m-d H:i:s') )
                                        <div class="promoted-label">URGENT</div>
                                        @endif
                                        <div class="search-freelancer-user-head">
                                            <a @if($proRec['project_type'] == 1 && Sentinel::check() == false || $proRec['project_type'] == 1 && Sentinel::check() != false && $user->inRole('client') && $proRec['client_user_id'] !=$user->id) href="javascript:void(0)" style="cursor:no-drop;" @else href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}" @endif >{{$proRec['project_name'] or '-'}}</a>
                                            <div class="batches">
                                                <!-- 0 = no-featured / 1 = featured -->
                                                @if(isset($proRec['highlight_end_date'])       && 
                                                          $proRec['highlight_end_date'] !=""   && 
                                                          $proRec['highlight_end_date'] !=null && 
                                                          $proRec['highlight_end_date'] !="0000-00-00 00:00:00" && 
                                                          date('Y-m-d', strtotime($proRec['highlight_end_date'])) >= date('Y-m-d') )
                                                <span class="private">HIGHLIGHTED</span>
                                                @endif
                                                <!-- 0 = no-nda / 1 = nda -->
                                                @if(isset($proRec['is_nda']) && $proRec['is_nda'] == 1)
                                                <span class="private">NDA</span>
                                                @endif

                                                <!-- 0 = no-urgent / 1 = urgent -->
                                                @if(isset($proRec['is_urgent']) && $proRec['is_urgent'] == 1)
                                                <!-- <span class="private">Urgent</span> -->
                                                @endif


                                                <!-- 0 = public / 1 = privare -->
                                                @if(isset($proRec['project_type']) && $proRec['project_type'] == 1)
                                                <span class="private">Private</span>
                                                @endif
                                                <!-- <span class="">
                                                    @if(isset($user) && $user == TRUE && $user->inRole('expert') )  
                                                      @if(isset($proRec['favourite_project'][0]['expert_user_id']) && $proRec['favourite_project'][0]['expert_user_id'] == $user->id )
                                                        <span title="Favourite" class="favourite pull-right" style="color: #ffbefb;" ><i class="fa fa-heart"></i></span>
                                                      @else
                                                        <span title="Add To Favourite ?" onclick="addOrRemoveFavourite(this)"  data-action="{{ base64_encode('add') }}" style="cursor: pointer;" data-project-id="{{isset($proRec['id'])?base64_encode($proRec['id']):''}}" class="favourite pull-right" ><i class="fa fa-heart"></i></span>
                                                      @endif
                                                    @endif
                                                </span> -->
                                            </div>
                                        </div>
                                        
<!--                                        <div class="search-freelancer-user-location"><span class="gavel-icon"><i class="fa fa-gavel"></i></span>   <span class="freelancer-user-line">|</span></div>-->
                                    
                                    <div class="search-freelancer-user-location">
                            <span class="gavel-icon"><img src="{{url('/public')}}/front/images/bid.png" alt="" /></span> <span class="dur-txt"> {{trans('project_listing/listing.text_bids')}} {{isset($proRec['bid_count'])?$proRec['bid_count']:'0'}}</span> 
                        </div>

                                        @php $postes_time_ago = time_ago($proRec['created_at']); @endphp
                                   
                                    
                                    
                                            <div class="search-freelancer-user-location">
                            <span class="gavel-icon"><img src="{{url('/public')}}/front/images/clock.png" alt="" /></span> <span class="dur-txt"> {{trans('project_listing/listing.text_project_posted_on')}}: {{$postes_time_ago}}</span> 
                        </div>
                                    
                                    

                                        <div class="clearfix"></div>
                                        @if($proRec['project_type'] == 1 && Sentinel::check() == false || $proRec['project_type'] == 1 && Sentinel::check() != false && $user->inRole('client') && $proRec['client_user_id'] !=$user->id)
                                        <div class="more-project-dec"> <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span><i> {{trans('controller_translations.private_project_note')}}</i></div>
                                        @else
                                            <div class="more-project-dec">{{str_limit($proRec['project_description'],340)}} 
                                                 @if(strlen(trim($proRec['project_description'])) > 340)
                                                 <a class="moretext" href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}">
                                                 {{trans('project_manager/projects/postedprojects.text_more')}}
                                                 </a>
                                                 @endif
                                            </div>
                                            <div class="category-new">{{isset($proRec['category_details']['category_title'])?$proRec['category_details']['category_title']:'NA'}}</div>
                                            <div class="clearfix"></div>
                                            @if(isset($proRec['project_skills'])  && count($proRec['project_skills']) > 0)
                                             @foreach($proRec['project_skills'] as $key=> $skills)
                                              @if(isset($skills['skill_data']['skill_name']) &&  $skills['skill_data']['skill_name'] != "")
                                                <!-- <a onclick="searchBySkill(this)" style="cursor: pointer;" data-skill-name="{{$skills['skill_data']['skill_name']}}" data-skill-id="{{ isset($skills['skill_id'])? base64_encode($skills['skill_id']):''}}" > --> <li style="font-size:12px;" class="search-project-skils">{{ str_limit($skills['skill_data']['skill_name'],18) }}</li><!-- </a> -->
                                              @endif
                                             @endforeach
                                            @endif   
                                        @endif
                                    
                                    <div class="search-project-listing-right contest-right">
                                        <div class="search-project-right-price">
                                            <?php
                                              $pricing_method = "";
                                              if(isset($proRec['project_pricing_method']) && $proRec['project_pricing_method'] == '1')
                                              {
                                                $pricing_method = trans('project_listing/listing.text_budget');
                                              }
                                              else if(isset($proRec['project_pricing_method']) && $proRec['project_pricing_method'] == '2')
                                              {
                                                $pricing_method = trans('project_listing/listing.text_hourly_rate'); 
                                              }

                                            ?>
                                           
                                            <?php 
                                                   
                                                   $role = FALSE;
                                                   $from_currency = isset($proRec['project_currency_code'])?$proRec['project_currency_code']:''; 
                                                   $to_currency   = isset($arr_settings['default_currency_code'])?
                                                                          $arr_settings['default_currency_code']:'';
                                                   $project_cost     = isset($proRec['project_cost'])?$proRec['project_cost']:'0';
                                                   $converted_price  = currencyConverterAPI($from_currency,$to_currency,$project_cost);
                                                   $converted_price  = number_format($converted_price,2);
                                                   
                                                   if($user!=FALSE)
                                                   {
                                                      $role = $user->inRole('expert');
                                                   }    
                                                   

                                            ?>
                                            @if($user!= FALSE && $role==FALSE)
                                            {{isset($proRec['project_currency'])?$proRec['project_currency']:'$'}} {{isset($proRec['project_cost'])?$proRec['project_cost']:'0'}}
                                            @else
                                              {{$to_currency}} {{$converted_price}}
                                            @endif
                                              @if(isset($proRec['project_pricing_method']) && $proRec['project_pricing_method'] == '2')
                                
                                                {{trans('common/_top_project_details.text_hour')}}
                                              @endif


                                            <span>{{$pricing_method or ''}}</span>
                                        </div>
                                        <div class="search-project-right-price">
                                            <span id='bids_left_timer{{$proRec["id"]}}' style="margin-top:0px;">-</span><span>Open for Bids</span>
                                            <?php 
                                                $timeFirst      = strtotime(date('Y-m-d H:i:s'));
                                                $timeSecond     = strtotime($proRec['bid_closing_date']);
                                                $diffInSeconds = $timeSecond - $timeFirst;
                                                if($diffInSeconds <= 0){
                                                  $diffInSeconds = '0';
                                                }
                                            ?>
                                            <script type="text/javascript">
                                                  var time        = '<?php echo $diffInSeconds; ?>';
                                                  var projectid = '<?php echo $proRec["id"]; ?>';
                                                  if(time < 0) {  time = 0; }
                                                  var c           = time; // in seconds
                                                  var t;
                                                  var days    = parseInt( c / 86400 ) % 365;
                                                  var hours   = parseInt( c / 3600 ) % 24;
                                                  var minutes = parseInt( c / 60 ) % 60;
                                                  var seconds = c % 60;
                                                  //var result  = (days < 10 ? "0" + days : days) + ":"+ (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
                                                  var d = ""; 
                                                  var h = ""; 
                                                  var m = "";
                                                  var s = ""; 

                                                  if(days     !=  ""  &&  days     !="0"){  d  =  (days+'d')     +  " ";  }
                                                  if(hours    !=  ""  &&  hours    !="0"){  h  =  (hours+'h')    +  " ";  }
                                                  if(minutes  !=  ""  &&  minutes  !="0"){  m  =  (minutes+'m')  +  " ";  }
                                                  if(seconds  !=  ""  &&  seconds  !="0"){  s  =  (seconds+'s')  +  " ";  }
                                                  
                                                  if(d != "" && h !=""){
                                                  var result    = d + h;
                                                  }else if(h != "" && m !=""){
                                                  var result    = h + m;
                                                  }else if(m !=""){
                                                  var result    = m;
                                                  }else if(s !=""){
                                                  var result    = s;
                                                  }
                                                  //var result    = d + h + m + s;
                                                  if(result == "NaN:NaN:NaN:NaN"){ 
                                                    $('#bids_left_timer'+projectid).html('{{trans('client/projects/ongoing_projects.text_next_expire')}}');
                                                    $('#bids_left_timer'+projectid).css('color','red');
                                                  } else {
                                                    $('#bids_left_timer'+projectid).html(result);
                                                    $('#bids_left_timer'+projectid).css('color','green');
                                                    $('#bids_left_timer'+projectid).css('font-size','16px');
                                                    $('#bids_left_timer'+projectid).css('font-weight','bold');
                                                  }
                                                  if(c == 0 ){
                                                    $('#bids_left_timer'+projectid).html('{{trans('client/projects/ongoing_projects.text_next_expire')}}');
                                                    $('#bids_left_timer'+projectid).css('color','red');
                                                  }
                                                  c = c - 1;
                                              </script> 
                                        </div>
                                        <div class="clearfix"></div>

                                        @if($user!= FALSE)
                                            @if(isset($proRec['is_bid_exists'])  && $proRec['is_bid_exists'] == '1')
                                              @if(isset($proRec['is_project_rejected_by_expert']) && $proRec['is_project_rejected_by_expert']=='1')
                                                <a href="javascript:void(0)" class="black-border-btn">
                                                   {{trans('project_listing/listing.text_project_rejected')}}
                                                </a>
                                              @else
                                                <a href="javascript:void(0)" class="black-border-btn active-green" ><i class="fa fa-check-circle" aria-hidden="true"></i>
                                                   {{trans('project_listing/listing.text_applied')}} 
                                                </a>
                                              @endif
                                            @elseif($user->inRole('expert'))
                                                @if(isset($proRec['bid_closing_date']) && $proRec['bid_closing_date'] > date('Y-m-d'))
                                                <a href="{{url('/public')}}/expert/bids/add/{{base64_encode($proRec['id'])}}" class="black-border-btn">
                                                  {{trans('project_listing/listing.text_apply')}} 
                                                </a>
                                                @endif
                                            @endif
                                        @else
                                            <a @if($proRec['project_type'] == 1 && Sentinel::check() == false) href="javascript:void(0)" style="cursor:no-drop;" @else href="{{url('/public')}}/login" @endif  class="black-border-btn">
                                             {{trans('project_listing/listing.text_apply')}}
                                            </a>
                                        @endif 
                                       <!-- <a href="javascript:void(0)" class="black-border-btn">Apply Now</a> -->
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        @endforeach    
                    @else
                    <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;" align="center">
                      <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                         <div class="search-content-block">
                            <div class="no-record">
                                  {{ trans('project_listing/listing.text_sorry_no_records_found') }}
                               </div>                                                        
                           </div>
                       </td>
                    </tr>
                    @endif

                    <!--pagigation start here-->
                    {{-- @if(isset($obj_project) && !empty($obj_project)) --}}
                    <div class="product-pagi-block">
                    {!! $obj_project->appends(request()->except(['page','_token'])) !!}
                    
                  </div>
                  {{-- @else
                    @include('front.common.pagination_view', ['paginator' => $arr_projects])
                  @endif --}}
                       {{-- @include('front.common.pagination_view', ['paginator' => $arr_projects]) --}}
                    <!--pagigation end here-->
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    /* Autosearching according to skills */
    $(document).ready( function () {
       $("input[name='search']").autocomplete({
          minLength:0,
          source:"{{ url('/autocomplete') }}",
          select:function(event,ui)
          {
            $("input[name='search']").val(ui.item.label);
            $('.submit_btn').click();  
          },
          response: function (event, ui){
          }
       });
    });
</script>
<script type="text/javascript">
    function submitSearch(ref){
      var sort_by; 
      sort_by = ref.value;
      $('#sort_by').prop('value',sort_by);
      $('#submit').trigger('click');
    }
    function searchBySkill(ref){
      var skill_name = $(ref).attr('data-skill-name');
      if(skill_name.trim() != ""){
        $('input[name="search"]').prop('value',skill_name.trim());
        $('#submit').trigger('click');
      } else  {
        console.log('No skill information available.');
      }
    }
    function addOrRemoveFavourite(ref){
      var project_id = $(ref).attr('data-project-id');
      var action     = $(ref).attr('data-action');
      var confirm_message = '{!! trans('expert/projects/favourite_projects.confirm_add_to_favourite') !!}';
      if(!confirm(confirm_message)){
        return false;
      }
      if(project_id != ""){
        var data  = { project_id:project_id,_token:"{{csrf_token()}}",action:action };
        $.ajax({
        url:"{{url('/public')}}"+'/projects/add_or_remove_favourite',
        type:'POST',
        dataType:'json',
        data: data,
        beforeSend: function(){
          showProcessingOverlay();                  
        },
        success:function(response){
          if(response.status == "FAVOURITE"){
            console.log('Project added to favourites successfully.');
          }
          if(response.status=="UNFAVOURITE"){
            console.log('Project added to favourites successfully.');
          }
          window.location.reload();
        }
        });
      }
    }
</script>
@endsection