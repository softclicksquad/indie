<form action="{{$module_url_path}}/search/all/result" id="frm_search_project" method="any">
<div class="col-sm-5 col-md-3 col-lg-3">
    <div class="filter-arrow-icon-section">
                        <i class="fa fa-filter"></i>
                        <i class="fa fa-times"></i>
                    </div>
    <div class="main-left-bar">
        <div class="mobile-title">Filtter</div>
        
<!--
    <div class="search-free-keyword-head">{{ trans('project_listing/listing.text_filter') }} @if(!empty($_REQUEST)) <a style="margin: 0px 0 0;" href="{{url('/projects')}}" class="back-btn pull-right">
      
        Reset</a>@endif
    </div>
-->
    <div class="filter-block">
      <button type="submit" id="submit" style="display:none;background-color: Transparent;border:0;"class="search-icon bg-img show_loader submit_btn"></button>
<!--        <div class="search-free-keyword-head">{{ trans('project_listing/listing.text_keywords') }}</div>-->
<!--
        <div class="form-group white-bg">
            <div class="search-filter2">
                <input type="text" name="search" id="pro-search" value="{{ Request::get('search') }}" data-rule-required="true" placeholder="{{trans('project_listing/listing.text_search')}}" />
                <button type="submit" id="submit" style="background-color: Transparent;border:0;"class="search-icon bg-img show_loader submit_btn"></button>
            </div>
        </div>
-->
        <?php
          /* Removing key from session after login from browse project page */
          if(Session::get('is_browse_url') && Session::get('is_browse_url') == "YES"){
              Session::forget('is_browse_url');
          }
          /* ends */
          $category_id = "";
          if(isset($_REQUEST['category']) && $_REQUEST['category'] != "") {
            $category_id = $_REQUEST['category'];
          }

          $sub_category_id = "";
          if(isset($_REQUEST['subcategory']) && $_REQUEST['subcategory'] != "") {
            $sub_category_id = $_REQUEST['subcategory'];
          }
          
        ?>
         <div class="left-category-bx"><h2>{{trans('project_listing/listing.text_fixed_rate')}} ({{$arr_settings['default_currency']}})</h2>
          <div class="range-t input-bx" for="fixed_rate">
              <div id="slider-price-range-fixed" class="slider-rang"></div>
              <div class="amount-no" id="slider_price_range_txt-fixed"></div>
              <input type="hidden" id="min_fixed_amt" name="min_fixed_amt" value="{{isset($min_fixed_amt)?$min_fixed_amt:0}}">
              <input type="hidden" id="max_fixed_amt" name="max_fixed_amt" value="{{isset($max_fixed_amt)?$max_fixed_amt:0}}">
          </div>
          </div>
          <div class="clearfix"></div>
        <div class="left-category-bx "><h2>{{trans('client/projects/post.text_hourly_title')}} ({{$arr_settings['default_currency']}})</h2>
          <div class="range-t input-bx" for="amount">
              <div id="slider-price-range-hourly" class="slider-rang"></div>
              <div class="amount-no" id="slider_price_range_txt-hourly"></div>
              <input type="hidden" id="min_contest_amt-hourly" name="min_project_amt" value="{{isset($min_project_amt)?$min_project_amt:0}}">
              <input type="hidden" id="max_contest_amt-hourly" name="max_project_amt" value="{{isset($max_project_amt)?$max_project_amt:0}}">
          </div>
          </div> 
        <div class="left-category-bx">
        <h2>{{trans('project_listing/listing.text_category')}}</h2>
            <div class="main-category-title">
            <ul>
                 <li @if($category_id=='') class='active' @endif> <a> All <span> ({{ isset($arr_projects['total'])?$arr_projects['total']:0 }}) </a> </li>

                @if(isset($arr_sidebar_data) && sizeof($arr_sidebar_data)>0)
                  @foreach($arr_sidebar_data as $proRec)
                        <?php  $selected = ""; 
                          if(isset($category_id) && $category_id != ""){
                            if(base64_encode($proRec['category_id']) == $category_id){
                              $selected = 'active';
                            }
                          }
                        ?>
                      <li class="{{$selected}}"> <a onclick="submit_category_search(this)" data-category="{{base64_encode($proRec['category_id'])}}">{{ str_limit(isset($proRec['category_title']) ? $proRec['category_title']:'',15) }}<span> ({{ isset($proRec['project_count']) ? $proRec['project_count']:0 }}) </a> </li>
                  @endforeach
                @endif  
                </ul>
            </div>
        </div>
        <input type="hidden" name="category" id="category" value="{{isset($category_id)?$category_id:0}}">
        @if(isset($arr_subcategory) && sizeof($arr_subcategory)>0)
        <div class="left-category-bx">
        <h2>Sub Category</h2>
            <div class="main-category-title">
            <ul>
                @if(isset($arr_subcategory) && sizeof($arr_subcategory)>0)
                @foreach($arr_subcategory as $subcategory_val) 
                <?php   $selected = ""; 
                        if(isset($sub_category_id) && $sub_category_id != ""){
                          if(base64_encode($subcategory_val['id']) == $sub_category_id){
                            $selected = 'active';
                          }
                        }
                  ?>
                      <li class="{{$selected}}"> <a onclick="submit_sub_category_search(this)" data-subcategory="{{base64_encode($subcategory_val['id'])}}">{{$subcategory_val['subcategory_title']}} </a> </li>
                  @endforeach
                @endif  
                </ul>
            </div>
        </div>
         <input type="hidden" name="subcategory" id="subcategory" value="{{isset($sub_category_id)?$sub_category_id:''}}">
        @endif
        <div class="clearfix"></div>
        </div>
      
          <div class="left-category-bx toggle-btn"><h2>Project Features</h2><span class="search-free-slills-arrow"></span>
          <?php  
            $searchProjectVal = [];
            if(!empty($_REQUEST['project'])){
              $searchProjectVal = $_GET['project'];
            }
          ?>
          <div class="add-skills-check-block content-d">
              <div class="check-box">
                  <p>
                      <input <?php if(isset($_REQUEST['project']) &&  in_array('featured', $searchProjectVal)) { echo 'checked'; } ?> id="featured_ptoject" name="project[]" value="featured" class="filled-in project_features" type="checkbox">
                      <label for="featured_ptoject">Highlighted 
                          <span class="tooltip-block"><span class="info-icon"><i class="fa fa-question-circle"></i></span><span class="tooltip-content">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span></span>
                      </label>
                  </p>
              </div>
              <div class="check-box">
                  <p>
                      <input <?php if(isset($_REQUEST['project']) &&  in_array('private', $searchProjectVal)) { echo 'checked'; } ?> id="private_project" name="project[]" value="private" class="filled-in project_features" type="checkbox">
                      <label for="private_project">Private 
                          <span class="tooltip-block"><span class="info-icon"><i class="fa fa-question-circle"></i></span><span class="tooltip-content">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span></span>
                      </label>
                  </p>
              </div>
              <div class="check-box">
                  <p>
                      <input <?php if(isset($_REQUEST['project']) &&  in_array('nda', $searchProjectVal)) { echo 'checked'; } ?> id="nda_project" name="project[]" value="nda" class="filled-in project_features" type="checkbox">
                      <label for="nda_project">NDA 
                          <span class="tooltip-block"><span class="info-icon"><i class="fa fa-question-circle"></i></span><span class="tooltip-content">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span></span>
                      </label>
                  </p>
              </div>
          </div>
          </div>
    
  </div>
</div>
</form> 
<link href="{{url('/public')}}/front/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="{{url('/public')}}/front/js/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript">
  $(function() {
        $("#slider-price-range-hourly").slider({
            range: true,
            min: {{isset($min_project_amt)?$min_project_amt:0}},
             max: {{isset($max_project_amt)?$max_project_amt:0}},
            values: [<?php if(isset($_GET['min_project_amt']) && $_GET['min_project_amt'] != "") { echo $_GET['min_project_amt']; } else { echo $min_project_amt; } ?>, <?php if(isset($_GET['max_project_amt']) && $_GET['max_project_amt'] != "") { echo $_GET['max_project_amt']; } else { echo $max_project_amt; } ?>],
            change: function(event, ui) 
            {
                $("#slider_price_range_txt-hourly").html("<span class='slider_price_min-hourly' style='float:left'>{{$arr_settings['default_currency']}} " + ui.values[0] + "</span>  <span class='slider_price_max-hourly' style='float:right'>{{$arr_settings['default_currency']}} " + ui.values[1] + " </span>");
                $("#min_contest_amt-hourly").val(ui.values[0]);
                $("#max_contest_amt-hourly").val(ui.values[1]);
                $('.submit_btn').click();
                // $("#frm_search_project").submit();
            }
        });
        $("#slider_price_range_txt-hourly").html("<span class='slider_price_min-hourly' style='float:left'> {{$arr_settings['default_currency']}} " + $("#slider-price-range-hourly").slider("values", 0) + "</span>  <span class='slider_price_max-hourly' style='float:right'>{{$arr_settings['default_currency']}} " + $("#slider-price-range-hourly").slider("values", 1) + "</span>");
    });
  
  $(function() {
        $("#slider-price-range-fixed").slider({
             range: true,
             min: {{isset($min_fixed_amt)?$min_fixed_amt:0}},
             max: {{isset($max_fixed_amt)?$max_fixed_amt:0}},
             values: [<?php if(isset($_GET['min_fixed_amt']) && $_GET['min_fixed_amt'] != "") { echo $_GET['min_fixed_amt']; } else { echo $min_fixed_amt; } ?>, <?php if(isset($_GET['max_fixed_amt']) && $_GET['max_fixed_amt'] != "") { echo $_GET['max_fixed_amt']; } else { echo $max_fixed_amt; } ?>],
            change: function(event, ui) 
            {
                //console.log(ui.values[0],ui.values[1]);
                $("#slider_price_range_txt-fixed").html("<span class='slider_price_min-fixed' style='float:left'>{{$arr_settings['default_currency']}} "+ui.values[0]+"</span><span class='slider_price_max-fixed' style='float:right'>{{$arr_settings['default_currency']}} "+ui.values[1]+"</span>");
                $("#min_fixed_amt").val(ui.values[0]);
                $("#max_fixed_amt").val(ui.values[1]);
                $('.submit_btn').click();
                // $("#frm_search_project").submit();
            }
        });
        $("#slider_price_range_txt-fixed").html("<span class='slider_price_min-fixed' style='float:left'>{{$arr_settings['default_currency']}} "+$("#slider-price-range-fixed").slider("values",0)+"</span><span class='slider_price_max-fixed' style='float:right'>{{$arr_settings['default_currency']}} "+$("#slider-price-range-fixed").slider("values", 1)+"</span>");
    });
</script>

<script>
$('.toggle-btn').click(function(){
    $(this).next('.add-skills-check-block').slideToggle();
    $(this).find('.search-free-slills-arrow i').toggleClass('fa-angle-down fa-angle-up');
})
</script> 
<script type="text/javascript">
 $(".more-btn-block-skill").click(function() {
        $(this).parent().find(".view-more-content-skill").slideDown("slow");
        $(this).hide();
        $(this).parent().find(".hide-btn-block-skill").show();
 });
 $(".hide-btn-block-skill").click(function() {
      $(this).parent().find(".view-more-content-skill").slideUp("slow");
      $(this).parent().find(".more-btn-block-skill").show();
      $(this).hide();
 });

function submit_category_search(ref)
{
  var cat_id = $(ref).attr('data-category');
  $('#category').val(cat_id);
  $('.submit_btn').click();
}

function submit_sub_category_search(ref)
{
  var cat_id = $(ref).attr('data-subcategory');
  $('#subcategory').val(cat_id);

  $('.submit_btn').click();
}

 // $("#category").change(function(){
    
 // });
 $("#sub_category").change(function(){
    $('.submit_btn').click();
 });
 $('.skills,.price,.project_features').click(function() {
    setTimeout(function(){
      $('.submit_btn').click();  
    },2);   
 });
 $('.fixed_rate').click(function(){
    setTimeout(function(){
      $('.hourly_rate').prop('checked',false);
      $('.submit_btn').click();  
    },2);
 });
 $('.hourly_rate').click(function(){
    setTimeout(function(){
      $('.fixed_rate').prop('checked',false);
      $('.submit_btn').click();  
    },2);
 });
 $('#pro-search').blur(function(){
    setTimeout(function(){
      var search_val = $('#pro-search').val();
      if(search_val != ""){
        $('.submit_btn').click();  
      }
    },2);
 });
</script> 

 <script>
        $(".filter-arrow-icon-section").on("click", function(){
            $("body").toggleClass("filter-open");            
        });
    </script>