@extends('front.layout.auth')
@php $user = \Sentinel::check();@endphp 
@section('main_content') 
@php if(isset($arr_contest_entry_data['contest_entry_files'][0]['file_no'])){ $first_file = $arr_contest_entry_data['contest_entry_files'][0]['file_no']; } else { $first_file = 0; } @endphp
<div class="gray-wrapper">
  <div class="container">
    <div class="row">
      <div class="col-lg-1"></div>
      <div class="col-sm-12 col-md-12 col-lg-10">
        @include('front.layout._operation_status')
        <div class="coll-listing-section">
          <div class="contest-titile">
            <div class="sm-logo d-inlin"><i style="font-size: 1.5em;" class="fa fa-trophy"></i></div>
            <span class="d-inlin">{{ trans('contets_listing/listing.text_contest')}}</span>
          </div>
          <a href="{{ url()->previous() }}" class="back-btn"><i class="fa fa-reply-all"></i> {{trans('client/contest/common.text_back')}}</a>
          <div class="clearfix"></div>
        </div>
        <div class="contest-main-title">
          @if(\Session::has('active_file'))
          @php $active_file = \Session::get('active_file'); @endphp
          <script type="text/javascript">var active_file =  {{$active_file}}; setTimeout(function(){ $('[file-index='+active_file+']').click(); },500);</script>
          @endif
          {{isset($arr_contest_entry_data['contest_details']['contest_title'])?$arr_contest_entry_data['contest_details']['contest_title']:""}}
          @if(isset($arr_contest_entry_data['is_winner']) && $arr_contest_entry_data['contest_details']['winner_choose'])
          @if($arr_contest_entry_data['is_winner'] == 'YES')
            <span class="pull-right"><img style="cursor: default;height: 40px;" src="{{url('/public')}}/front/images/archexpertdefault/contest-winner.png">{{trans('client/contest/common.text_winner')}}</span>   
          @elseif($arr_contest_entry_data['contest_details']['winner_choose'] == 'YES' && $arr_contest_entry_data['is_winner'] == 'NO' && get_user_role($user->id) == 'expert')
            <span class="pull-right"><small><i style="color:#9e9e9e;font-family: cursive;">{{trans('client/contest/common.text_unfortunately_you_not_a_winner')}}...</i></small></span>               
          @elseif(get_user_role($user->id) == 'expert')
            <span class="pull-right"><small><i style="color:#9e9e9e;font-family: cursive;">{{trans('client/contest/common.text_the_winner_not_declared_yet')}}...</i></small></span>
          @endif
          @endif 
        </div>
        <!-- gallery hear -->
        @php
        $timeline_img_public_path = url('/public').config('app.project.img_path.cover_image');
        $timeline_img_base_path   = base_path().'/public'.config('app.project.img_path.cover_image');
        @endphp
        <div class="contest-slider">
          <h4 class="entry-title">{{trans('client/contest/common.text_entry')}} {{isset($arr_contest_entry_data['entry_id'])?'#'.$arr_contest_entry_data['entry_id']:"-"}} <br><small style="font-size: 40%;">{{isset($arr_contest_entry_data['entry_number'])?'#'.$arr_contest_entry_data['entry_number']:"-"}}</small></h4>
          <div id="example1" class="webwing-gallery img350">
            <div class="prod-carousel">
              @if(isset($arr_contest_entry_data['contest_entry_files']) && sizeof($arr_contest_entry_data['contest_entry_files'])>0)
              <?php $i = 1; ?>    
              @foreach($arr_contest_entry_data['contest_entry_files'] as $files)
              @if(isset($files['is_admin_deleted']) && $files['is_admin_deleted'] == '1')
              <img @if($i == 1) style="box-shadow: 12px 14px 11px #888888" @endif src="{{get_resized_image_path('temp_file','temp','100','100','Deleted By Admin')}}"  data-medium-img="{{get_resized_image_path('temp_file','temp','100','100','Deleted By Admin')}}" class="entry_files  filesrindex{{$i}}" file-index="{{$files['file_no']}}" data-big-img="{{get_resized_image_path('temp_file','temp','100','100','Deleted By Admin')}}" data-title="{{get_resized_image_path('temp_file','temp','100','100','Deleted By Admin')}}" title="{{trans('client/contest/common.text_file')}} #{{$files['file_no']}}" alt="">
              @elseif(isset($files['image']) && $files['image']!=null && file_exists($contest_send_entry_base_file_path.$files['image']))
              <img @if($i == 1) style="box-shadow: 12px 14px 11px #888888" @endif src="{{url('/public/uploads/front/postcontest/send_entry/'.$files['image']) }}"  data-medium-img="{{url('/public/uploads/front/postcontest/send_entry/'.$files['image']) }}" class="entry_files" file-index="{{$files['file_no']}}" data-big-img="{{url('/public/uploads/front/postcontest/send_entry/'.$files['image']) }}" data-title="{{trans('client/contest/common.text_file')}} #{{$files['file_no']}}" title="{{trans('client/contest/common.text_file')}} #{{$files['file_no']}}" alt="" attr-work-done = "{{isset($files['is_own_work'])?$files['is_own_work']:"-"}}">
              @else
              <!-- <img src="{{url('/front/cover_image/default_cover_photo.jpg')}}" data-medium-img="{{url('/front/cover_image/default_cover_photo.jpg')}}" data-big-img="{{url('/front/cover_image/default_cover_photo.jpg')}}" data-title="default_cover_photo.jpg" alt=""> -->
              @endif
              <?php $i++; ?>
              @endforeach
              @endif
            </div>
            @php
            $sidebar_information      = sidebar_information($arr_contest_entry_data['expert_id']);
            $profile_img_public_path  = url('/public').config('app.project.img_path.profile_image');
            $profile_img_base_path    = base_path().'/public'.config('app.project.img_path.profile_image');
            $timeline_img_public_path = url('/public').config('app.project.img_path.cover_image');
            $timeline_img_base_path   = base_path().'/public'.config('app.project.img_path.cover_image');
            @endphp
            <div class="layout-details for-color-chg">
              <div class="layout-details-left">
                <h5>{{isset($arr_contest_entry_data['title'])?str_limit($arr_contest_entry_data['title'],30,'...'):""}}</h5>
                <div class="avg-rating">
                  <!-- <div class="avg" style="margin-left:0px;">{{$sidebar_information['average_rating']}}</div> -->
                  <ul>
                    @if(isset($arr_contest_entry_data['contest_entry_files']) && sizeof($arr_contest_entry_data['contest_entry_files'])>0)
                    @php $existed_ratings = [];  @endphp
                    @foreach($arr_contest_entry_data['contest_entry_files'] as $fkey => $files)
                    @foreach($arr_contest_entry_data['contest_entry_files_rating'] as $rkey => $rating)
                    @if($rating['file_no'] == $files['file_no'])
                    <span @if($fkey == 0) style="display:block;" @else style="display:none;" @endif class="stars file-rtn file-rtn{{$files['file_no']}}" style="margin-left: -3px;">{{$rating['rating']}}</span>
                    @php $existed_ratings[] = $rating['file_no'];  @endphp
                    @endif   
                    @endforeach
                    @if(!in_array($files['file_no'],$existed_ratings))
                    <span @if($fkey == 0) style="display:block;" @else style="display:none;" @endif class="stars file-rtn file-rtn{{$files['file_no']}}" style="margin-left: -3px;">0</span>
                    @endif
                    @endforeach
                    @endif
                  </ul>
                </div>
              </div>
              @if($arr_contest_entry_data['sealed_entry'] == '1')
                <span class="sealed-entry"><i class="fa fa-lock"></i></span>   
              @endif
              <div class="layout-details-right">
                <div class="coll-person d-inlin">
                  
                  <?php
                    if(isset($profile_img_public_path) && isset($sidebar_information['user_details']['profile_image']) && $sidebar_information['user_details']['profile_image']!="" && file_exists('public/uploads/front/profile/'.$sidebar_information['user_details']['profile_image']))
                    {
                      $profile_image = isset($sidebar_information['user_details']['profile_image'])?$profile_img_public_path.$sidebar_information['user_details']['profile_image']:'';
                    }
                    else
                    {
                      $profile_image = $profile_img_public_path.'default_profile_image.png';
                    }
                    ?>


                    <div class="collaboration-img">
                        <img src="{{$profile_image}}" class="img-responsive" alt=""/>
                    </div>
                   

                

                  <div class="coll-name">
                    <h5>
                      <a  title="Expert" href="{{url('/experts/portfolio/'.base64_encode(isset($arr_contest_entry_data['expert_id'])?$arr_contest_entry_data['expert_id']:'0'))}}">{{isset($sidebar_information['user_details']['first_name'])?$sidebar_information['user_details']['first_name'].'  '.str_limit($sidebar_information['user_details']['last_name'],1,'.'):""}}</a>
                    </h5>
                    @if($sidebar_information['last_login_duration'] == 'Active')
                    <span class="flag" title="{{isset($sidebar_information['expert_timezone'])?$sidebar_information['expert_timezone']:'-'}}">
                      <span class="flag-image"> 
                        @if(isset($sidebar_information['user_country_flag'])) 
                        @php 
                        echo $sidebar_information['user_country_flag']; 
                        @endphp 
                        @elseif(isset($sidebar_information['user_country'])) 
                        @php 
                        echo $sidebar_information['user_country']; 
                        @endphp 
                        @else 
                        @php 
                        echo '-'; 
                        @endphp 
                        @endif 
                      </span>
                      {{isset($sidebar_information['expert_timezone_without_date'])?$sidebar_information['expert_timezone_without_date']:'-'}}
                    </span>
                    @else
                    <span class="flag" title="{{$sidebar_information['last_login_full_duration']}}">
                      <span class="flag-image"> 
                        @if(isset($sidebar_information['user_country_flag'])) 
                        @php 
                        echo $sidebar_information['user_country_flag']; 
                        @endphp 
                        @elseif(isset($sidebar_information['user_country'])) 
                        @php 
                        echo $sidebar_information['user_country']; 
                        @endphp 
                        @else 
                        @php 
                        echo '-'; 
                        @endphp 
                        @endif 
                      </span>
                      {{$sidebar_information['last_login_duration'] or ''}}
                    </span>
                    @endif
                  </div>
                </div>
                <div class="chat d-inlin">
                    <a href="#add_comment" data-toggle="modal"> 
                      <small class="file_index" style="color:#2d2d2d;">
                        <span class="file_index">{{trans('client/contest/common.text_file')}} #{{$first_file}}</span>
                      </small>
                    </a> 
                    @if(isset($user->id) && isset($arr_contest_entry_data['id']) && isset($arr_contest_entry_data['expert_id']) && $user->id == $arr_contest_entry_data['expert_id'] && isset($arr_contest_entry_data['is_chat_initiated']) && $arr_contest_entry_data['is_chat_initiated'] == '1')
                      <a href="{{ url('expert/twilio-chat/contest/') . '/' . base64_encode($arr_contest_entry_data['id']) }}" style="margin-top:5px;" class="applozic-launcher" ><i class="fa fa-comment-o"></i></a>
                    @else
                      <i class="fa fa-comment-o"></i>
                    @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

          @if(isset($arr_contest_entry_data['contest_entry_files']) && sizeof($arr_contest_entry_data['contest_entry_files'])>0)
            @foreach($arr_contest_entry_data['contest_entry_files'] as $fkey => $files)
              @if($fkey == 0)
                @if(isset($files['is_own_work']) && $files['is_own_work'] == 0)
                  <div style="padding-bottom: 11px; padding-top: 6px;">
                    <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span>
                    {{isset($sidebar_information['user_details']['first_name'])?$sidebar_information['user_details']['first_name'].'  '.str_limit($sidebar_information['user_details']['last_name'],1,'.'):""}} : 
                    <span class="text_wrk_done">{{ trans('contets_listing/listing.text_all_work_was_done_by_me_or_our_company') }}</span> 
                  </div>
                @elseif(isset($files['is_own_work']) && $files['is_own_work'] == 1)
                  <div style="padding-bottom: 11px; padding-top: 6px;">
                    <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span>
                    {{isset($sidebar_information['user_details']['first_name'])?$sidebar_information['user_details']['first_name'].'  '.str_limit($sidebar_information['user_details']['last_name'],1,'.'):""}} : 
                    <span class="text_wrk_done">{{ trans('contets_listing/listing.text_the_work_was_partially_done_by_me_or_our_company') }}</span>
                  </div>
                @else
                  <div style="padding-bottom: 11px; padding-top: 6px;">
                    <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span>
                    {{isset($sidebar_information['user_details']['first_name'])?$sidebar_information['user_details']['first_name'].'  '.str_limit($sidebar_information['user_details']['last_name'],1,'.'):""}} : 
                    <span class="text_wrk_done">Not available</span>
                  </div>
                @endif  
              @endif  
            @endforeach
          @endif

          <div class="clearfix"></div>
          <div class="responce_status"></div>
          <div class="clearfix"></div>

          @if($user != null) 
            @if($user->inRole('expert') && $arr_contest_entry_data['expert_id'] == $user->id)
              {{-- @if($arr_contest_entry_data['is_winner'] == 'YES')
                <div class="comment-section">
                  <span><i class="fa fa-file-image-o" aria-hidden="true"></i> {{ trans('contets_listing/listing.text_remaining_files_to_upload')}} : <span id="remaining_files_cnt">{{isset($arr_contest_entry_data['contest_entry_files'])?25-count($arr_contest_entry_data['contest_entry_files']):'0'}}</span> / 25</span></span>            
                    @if(isset($arr_contest_entry_data['contest_entry_files']) && count($arr_contest_entry_data['contest_entry_files']) <= 24)
                      <span class="pull-right file-upload-btn" style="margin-top:-10px;"><a href="#cnst-send-entry" data-keyboard="false" data-backdrop="static"  data-toggle="modal" class="black-btn"><i class="fa fa-upload" aria-hidden="true"></i> {{ trans('contets_listing/listing.text_upload_more')}}</a></span>
                    @endif
                </div>
              @endif --}}

              @if($arr_contest_entry_data['contest_details']['winner_choose'] == 'NO')
                <div class="comment-section">
                  <span><i class="fa fa-file-image-o" aria-hidden="true"></i> {{ trans('contets_listing/listing.text_remaining_files_to_upload')}} : <span id="remaining_files_cnt">{{isset($arr_contest_entry_data['contest_entry_files'])?25-count($arr_contest_entry_data['contest_entry_files']):'0'}}</span> / 25</span></span>

                    @if(isset($arr_contest_entry_data['contest_entry_files']) && count($arr_contest_entry_data['contest_entry_files']) <= 24)
                      <span class="pull-right file-upload-btn newwwwwwwwwwwww"><a href="#cnst-send-entry" data-keyboard="false" data-backdrop="static"  data-toggle="modal" class="black-btn"><i class="fa fa-upload" aria-hidden="true"></i> {{ trans('contets_listing/listing.text_upload_more')}}</a></span>
                    @endif
                  <div class="clearfix"></div>
                </div>
              @elseif($arr_contest_entry_data['contest_details']['winner_choose'] == 'YES' && $contest_winner_id==$user->id)
                <div class="comment-section">
                  <span><i class="fa fa-file-image-o" aria-hidden="true"></i> {{ trans('contets_listing/listing.text_remaining_files_to_upload')}} : <span id="remaining_files_cnt">{{isset($arr_contest_entry_data['contest_entry_files'])?25-count($arr_contest_entry_data['contest_entry_files']):'0'}}</span> / 25</span></span>

                    @if(isset($arr_contest_entry_data['contest_entry_files']) && count($arr_contest_entry_data['contest_entry_files']) <= 24)
                      <span class="pull-right file-upload-btn newwwwwwwwwwwww"><a href="#cnst-send-entry" data-keyboard="false" data-backdrop="static"  data-toggle="modal" class="black-btn"><i class="fa fa-upload" aria-hidden="true"></i> {{ trans('contets_listing/listing.text_upload_more')}}</a></span>
                    @endif
                  <div class="clearfix"></div>
                </div>
              @endif
            @endif

            @if($arr_contest_entry_data['is_winner'] == 'YES' && $user->inRole('expert') && $arr_contest_entry_data['expert_id'] == $user->id || $user->inRole('client') && $arr_contest_entry_data['client_user_id'] == $user->id)  
            <h4>Handover here the final work to the client</h4>
              <div class="comment-section">
                <span><i class="fa fa-file-archive-o" aria-hidden="true"></i> Uploaded Original Files (Zip) : <span>{{isset($arr_contest_entry_data['contest_entry_original_files'])?count($arr_contest_entry_data['contest_entry_original_files']):0}}</span> / 3</span></span>
                <div class="clearfix"></div>
                <hr>
                <div class="clearfix"></div>
                <table class="table">
                  <thead class="thead-dark">
                    <tr>
                      <th scope="col">File Name</th>
                      <th scope="col">Uploaded</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if(isset($arr_contest_entry_data['contest_entry_original_files']) && count($arr_contest_entry_data['contest_entry_original_files']) > 0)
                    @foreach($arr_contest_entry_data['contest_entry_original_files'] as $oufkey => $original_files)
                    @if(isset($original_files['file_name']) && $original_files['file_name'] != "" && file_exists('public/uploads/front/postcontest/send_entry/original_files/'.$original_files['file_name']))
                    <tr>
                      <td>{{$original_files['file_name'] or ''}}<!-- <img style="height:26px;width:34px;" src="{{url('/public/front/images/file_formats/zip.png')}}"> --></td>
                      <td>{{time_ago($original_files['created_at'])}}</td>
                      <td>
                       <a href="{{url('/public/uploads/front/postcontest/send_entry/original_files/'.$original_files['file_name'])}}" download><i class="fa fa-file-archive-o" aria-hidden="true"></i> Download</a>
                     </td>
                   </tr>
                   @endif
                   @endforeach
                   @else
                   <tr>
                    <td scope="row" colspan="3" align="center">No files uploaded yet....</td>
                  </tr>
                  @endif
                  </tbody>
                </table>
              </div>
            @endif 
          @endif
          <div class="clearfix"></div>
          <div class="comment-section">
            <h4>{{isset($arr_contest_entry_data['title'])?$arr_contest_entry_data['title']:""}}</h4>
            <div class="slider-details">
              {{isset($arr_contest_entry_data['description'])?$arr_contest_entry_data['description']:""}}
            </div>
          </div>
      </div>
      <!-- gallery hear End -->
      <div class="comment-section">
        @php if(isset($arr_contest_entry_data['contest_entry_files'][0]['file_no'])){ $first_file = $arr_contest_entry_data['contest_entry_files'][0]['file_no']; } else { $first_file = 0; } @endphp
        <a href="#add_comment" data-toggle="modal" id="add-cmt-btn" class="black-btn add_comment">{{trans('client/contest/common.text_add_comment_on')}} <span class="file_index">{{trans('client/contest/common.text_file')}} #{{$first_file}}</span></a>
        <div class="comment-block-section">
          <h4>{{trans('client/contest/common.text_comments')}} <span class="file_index">{{trans('client/contest/common.text_file')}} #{{$first_file}}</span> (0)</h4>
          <div class="comment-block">{{trans('client/contest/common.text_no_comment_on')}} {{trans('client/contest/common.text_file')}} #{{$first_file}}</div>
        </div>
      </div>
    </div>
    <div class="col-lg-1"></div>
    <!-- add comment Modal start-->
    <div class="modal fade add-comment-modal" id="add_comment" role="dialog">
      <div class="modal-dialog ">
       <div class="modal-content">
        <div class="modal-body">
          <h2 class="file_index"><span class="file_index">{{trans('client/contest/common.text_file')}} #{{$first_file}}</span></h2>
          <form action="{{url('/expert/contest/store-comment')}}" id="file-cmnt-form" name="file-cmnt-form" method="POST">
            {{csrf_field()}}
            <input type="hidden" placeholder="Contest id" name="contest_id" id="contest_id" value="{{isset($arr_contest_entry_data['contest_id'])?$arr_contest_entry_data['contest_id']:"0"}}">
            <input type="hidden" placeholder="Contest entry id" name="contest_entry_id" id="contest_entry_id" value="{{isset($arr_contest_entry_data['id'])?$arr_contest_entry_data['id']:"0"}}">
            <input type="hidden" placeholder="File no" name="file_no" id="file_no">
            <div class="form-group">
              <label>{{trans('client/contest/common.text_add_commnet')}} <span style="color:red">*</span></label>
              <textarea rows="5" name="file_cmnt" id="file_cmnt" data-rule-required="true" placeholder="{{trans('client/contest/common.text_description')}}"></textarea>
              <span class="error" id="err_file_cmnt"></span>
            </div>
            <div class="modal-btns">
              <button type="button" id="cancel-cmnt" class="black-border-btn inline-btn" data-dismiss="modal">{{trans('client/contest/common.text_cancel')}}</button>
              <button type="submit" id="submit-cmnt" class="black-btn inline-btn">{{trans('client/contest/common.text_submit')}}</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!--add comment Modal end here--> 
  
  <!-- gallery -->
</div>
</div>
</div>   
@if($user != null) 
@if($user->inRole('expert'))
<!-- Model -->
<div class="modal fade invite-member-modal" id="cnst-send-entry" role="dialog">
  <div class="modal-dialog modal-lg">
   <button type="button" id="btn_close_popup" class="close" ><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
   <div class="modal-content">
    <div class="modal-body">
     <h2>{{ trans('contets_listing/listing.text_upload')}}</h2>
     <div class="invite-form project-found-div">
      <div class="user-box">
       <div class="input-name">
        <div class="txt-edit"> <b>{{ trans('contets_listing/listing.text_attachment_instructions') }} :</b> </div>
        <!--image upload start-->
        <div class="fallback dfvdfvdfvdf dropzone-section" id="dropzone_image_div">
          <form action="{{url('/public')}}/expert/contest/update_entry" class="dropzone" id="dropzonewidget">
            {{csrf_field()}}
            <div id="dropzone_append_id"></div>
          </form>
          <div class="post-img-note-block">
            <div class="error" id="err_image_file"></div>
            <label id="basic-error" class="validation-error-label" for="basic"></label>
            <div class="note-block">
              <b>{{ trans('contets_listing/listing.text_note') }} : </b> 
              {{ trans('contets_listing/listing.text_select_atleast_one_image_to_upload_Only_jpg_png_jpeg_images_allowed_with_max_size_1mb_You_can_upload_upto_5_images_at_a_time') }}
            </div>
          </div>
          <hr>
          <div class="terms">
            <div class="radio-buttons">
              <input class="filled-in " type="radio" value="0" name="work-done-by-me" >
              <font class="txt-edit">{{ trans('contets_listing/listing.text_all_work_was_done_by_me_or_our_company') }}</font>
            </div>
            <div class="radio-buttons">
              <input class="filled-in" type="radio" value="1" name="work-done-by-me" >
              <font class="txt-edit">{{ trans('contets_listing/listing.text_the_work_was_partially_done_by_me_or_our_company') }}</font >
            </div>
            <div class="error" id="work-done-by-me-err"></div>
            <label class="error" for="work-done-by-me"></label>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
    <button type="button" id="cancel_btn" style="width: 185px;" class="black-border-btn inline-btn">{{ trans('contets_listing/listing.text_cancel') }}</button>
    <button type="button" id="btn_post_images" class="black-btn inline-btn" style="height: auto;">{{ trans('contets_listing/listing.text_upload')}}</button>
  </div>
  
</div>
</div>
</div>
</div>
@endif
@endif  

<script type="text/javascript">
  Dropzone.options.dropzonewidget = 
  {
    addRemoveLinks: true,
    maxFiles: $('#remaining_files_cnt').html(),
    parallelUploads: $('#remaining_files_cnt').html(),
    uploadMultiple: true,
    maxFilesize: 5,
    autoProcessQueue: false,
    acceptedFiles: '.jpg, .JPG, .png, .PNG, .jpeg, .JPEG,',
    dictMaxFilesExceeded: "{{ trans('contets_listing/listing.text_dictMaxFilesExceeded') }}",
    dictFileTooBig: "{{ trans('contets_listing/listing.text_image_is_too_large_please_upload_upto_1mb_image') }}",
    dictDefaultMessage: "{{ trans('contets_listing/listing.text_drop_your_images_here_to_upload') }}",
    success: function( file, response ) 
    {
      hideProcessingOverlay(); 
      if(response.status=='error')
      {
        swal("",response.message,'error');
        this.removeFile(file); 
      }
      else
      {   
        var msg = response.message;
        if(response.status == 'success'){
          var response_status = '<div class="alert alert-success alert-dismissible">'+
          '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
          '<span aria-hidden="true">&times;</span>'+
          '</button>'
          +response.message+
          '&nbsp; <a href="'+site_url+'/contests/show_contest_entry_details/'+'{{isset($arr_contest_entry_data['id'])?base64_encode($arr_contest_entry_data['id']):base64_encode(0)}}'+'"><i class="fa fa-refresh" aria-hidden="true"></i> Reload</a></div>';
          $('.responce_status').html(response_status);
          this.removeFile(file); 
          $("#cnst-send-entry").modal('hide'); 
          var remaining_files_cnt = $('#remaining_files_cnt').html();
          var new_remaining_files_cnt =    (parseInt(remaining_files_cnt) - 1);  
          $('#remaining_files_cnt').html(new_remaining_files_cnt); 
          if(new_remaining_files_cnt == 0){
            $('.file-upload-btn').hide();  
          }
        }
        else{
          var response_status = '<div class="alert alert-danger alert-dismissible">'+
          '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
          '<span aria-hidden="true">&times;</span>'+
          '</button>'
          +response.message+
          '&nbsp; <a href="'+site_url+'/contests/show_contest_entry_details/'+'{{isset($arr_contest_entry_data['id'])?base64_encode($arr_contest_entry_data['id']):base64_encode(0)}}'+'"><i class="fa fa-refresh" aria-hidden="true"></i> Reload</a></div>';
          $('.responce_status').html(response_status);
          $('.responce_status').focus();
        }
      }
    },
    init: function() {
      var myDropzone = this;
      this.on("error", function(file, message) { 
        swal("",message,'error');
        this.removeFile(file); 
      });
      $('#btn_post_images').on("click", function() {
        $('#err_image_file').html('');
        $('#work-done-by-me-err').html('');
        if (!myDropzone.files || !myDropzone.files.length) 
        {
          $('#err_image_file').html('{{ trans('contets_listing/listing.text_this_field_is_required') }}');
          return false;
        }else if(!$("input[name='work-done-by-me']:checked").val()){
         $('#work-done-by-me-err').html('This field is required.').css('color','red'); 
         return false;
       }
       else
       {
        myDropzone.processQueue();  
      }
      $('#basic-error').html('');    
    });
      myDropzone.on("sending", function(file, xhr, data) {
        showProcessingOverlay();
        if($("input[name='work-done-by-me']:checked").val()){
          var work_done_by_me = $("input[name='work-done-by-me']:checked").val();
          data.append("work_done_by_me",work_done_by_me);
        }
        data.append("_token", $('meta[name="token"]').attr('content'));
        data.append("contest_entry_id",'{{isset($arr_contest_entry_data['id'])?$arr_contest_entry_data['id']:"0"}}');
      });
    }
  };
  function makeStatusMessageHtml(status, message)
  {
    str = '<div class="alert alert-'+status+' alert-dismissable"><a class="close" data-dismiss="alert" aria-label="close">×</a><strong>'+status+' :</strong>'+message+'</div>';
    return str;
  }
  $("#btn_close_popup,#cancel_btn").on('click',function(e)
  {
    $('#err_image_file').html('');
    var myDropzone = Dropzone.forElement("#dropzonewidget");
    myDropzone.removeAllFiles(true); 
    $("#cnst-send-entry").modal('hide');
  });
</script>
<link href="{{url('/public')}}/front/css/gallery.css" rel="stylesheet" />
<script src="{{url('/public')}}/front/js/gallery.min.js"></script>
<script type="text/javascript">
  function validateContestCommentForm(){
    var validator = $("#frmContestComment").validate({
      errorClass:"error",
      errorElement:'div', 
      errorPlacement: function (error, element) 
      {
        error.insertAfter(element);
      }
    }); 
  }
</script>
<script type="text/javascript">
  $("#file-cmnt-form").validate();
  $(document).ready(function() {
        // call ajax for first
        var token = csrf_token;
        /* loader start */
        $(".comment-block-section").css('opacity','0.5');
        /* loader start */
        var file_no           = '{{isset($arr_contest_entry_data['contest_entry_files'][0]['file_no'])?$arr_contest_entry_data['contest_entry_files'][0]['file_no']:'0'}}';
        var contest_id        = $('#contest_id').val();
        var contest_entry_id  = $('#contest_entry_id').val();
        $.ajax({
          'url':site_url+'/'+'{{get_user_role($user->id)}}'+'/contest/get-comments',
          'method':"POST",
          'data':{file_no:file_no,contest_id:contest_id,contest_entry_id:contest_entry_id,_token:token},
          success:function(result){
            $('.comment-block-section').html(result);
            $(".comment-block-section").css('opacity','5');
          }
        }) 
        $('#example1').webwingGallery({
          openGalleryStyle: 'transform',
          changeMediumStyle: true
        });
        $('.entry_files').click(function(){
         var file_work_done_value = $(this).attr("attr-work-done");
         if(file_work_done_value != null){
            $('.note_wrk_done').show();  
            if(file_work_done_value == 0){
              $(".text_wrk_done").html("{{ trans('contets_listing/listing.text_all_work_was_done_by_me_or_our_company') }}");
            } else if(file_work_done_value == 1) {
             $(".text_wrk_done").html("{{ trans('contets_listing/listing.text_the_work_was_partially_done_by_me_or_our_company') }}"); 
            } else {
             $(".text_wrk_done").html("Not Available");  
            }
         }
         var file_no           = $(this).attr('file-index');
         var contest_id        = $('#contest_id').val();
         var contest_entry_id  = $('#contest_entry_id').val();
         $('#file_no').val(file_no); 
         $('.file_index').html('{{trans('client/contest/common.text_file')}} #'+file_no);
         $('.entry_files').removeAttr('style');
         $('.file-rtn').hide();
         $('.file-rtn'+file_no).show();
         $(this).attr('style','box-shadow:12px 14px 11px #888888');
           // call ajax for comment
           var token = csrf_token;
           /* loader start */
           //$("#add-cmt-btn").hide();
           $(".comment-block-section").css('opacity','0.5');
           /* loader start */
           $.ajax({
            'url':site_url+'/'+'{{get_user_role($user->id)}}'+'/contest/get-comments',
            'method':"POST",
            'data':{file_no:file_no,contest_id:contest_id,contest_entry_id:contest_entry_id,_token:token},
            success:function(result){
              $('.comment-block-section').html(result);
              $(".comment-block-section").css('opacity','5');
            }
          })
         });
        $('.add_comment').click(function(){
         var file_no        = $('#file_no').val();
         if(file_no == ''){file_no = '{{isset($arr_contest_entry_data['contest_entry_files'][0]['file_no'])?$arr_contest_entry_data['contest_entry_files'][0]['file_no']:'0'}}';}
         $('#file_no').val(file_no);
       });
        $('#cancel-cmnt').click(function(){
         $('#err_file_cmnt').val('');
         $('#file_cmnt').val('');
       });
        $(document).on('click','.shwmore',function(){
         var nextshow  = $(this).attr('shw');
         var total_cnt = $('#cmt_cnt').html();
         $('.show'+nextshow).show();
         $('.shwmore').attr('shw',(parseInt(nextshow)*2));
         if(parseInt(nextshow) >= parseInt(total_cnt)){$('.shwmore').hide();}
       });
      });
    </script>
    @stop