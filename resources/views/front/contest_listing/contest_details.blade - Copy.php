@extends('front.layout.auth') @section('main_content')
@php $user = \Sentinel::check();@endphp 

<link rel="stylesheet" href="{{url('/public')}}/front/css/gallery.css" type="text/css" />
<link rel="stylesheet" href="{{url('/public')}}/front/css/lightgallery.css" type="text/css" />
<script type="text/javascript" src="{{url('/public')}}/front/js/gallery.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/front/js/lightgallery.js"></script>

<div class="page-top-header">
  <div class="container">
    <div class="row">
      <div class="col-sm-7 col-md-8 col-lg-8">
        <div class="text-left">
          <h3 title="{{isset($arr_contest['contest_title'])?$arr_contest['contest_title']:""}}">{{isset($arr_contest['contest_title'])?str_limit($arr_contest['contest_title'],100,'...'):""}}</h3>
          <?php 
          $postes_time_ago = time_ago($arr_contest['created_at']);
          ?>
            <div class="category-main-new">
                <span>{{isset($arr_contest['category_details']['category_title'])?$arr_contest['category_details']['category_title']:""}}</span>            
            </div>
            
             <div class="category-main-new new-sub-category">
                <span>{{isset($arr_contest['sub_category_details']['subcategory_title'])?$arr_contest['sub_category_details']['subcategory_title']:""}}</span> {{$postes_time_ago}}           
            </div>

            <div class="new-skills-add">
                <span class="skills-new-title">Skills :</span>
                <span>
          @foreach($arr_contest['contest_skills'] as $skill)
            {{isset($skill['skill_data']['skill_name'])?$skill['skill_data']['skill_name']:"Skill name"}},
          @endforeach 
                    </span>
                </div>

          </div>
        </div>
        <div class="col-sm-5 col-md-4 col-lg-4">
          <div class="text-right">
            <div class="contest-prize">{{ trans('contets_listing/listing.text_contest_prize') }} <span>{{isset($arr_contest['contest_currency'])?$arr_contest['contest_currency']:""}} {{isset($arr_contest['contest_price'])?$arr_contest['contest_price']:""}}</span></div>
            <p class="status">{{ trans('contets_listing/listing.text_status') }}: 
              @if(isset($arr_contest['contest_status']) && $arr_contest['contest_status']=='0')
              <span class="featured">{{ trans('contets_listing/listing.text_open') }}</span>
              @elseif(isset($arr_contest['contest_status']) && $arr_contest['contest_status']=='1')
              <span class="featured">{{ trans('contets_listing/listing.text_ending') }}</span>
              @elseif(isset($arr_contest['contest_status']) && $arr_contest['contest_status']=='2')
              <span class="featured" style="background-color:#7a7f7a">{{ trans('contets_listing/listing.text_ended') }}</span>
              @endif
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="gray-wrapper remve-bx-shadow">
    <div class="container">
      @include('front.layout._operation_status')
      <div class="row">
        <div class="col-sm-7 col-md-8 col-lg-8">
          <div class="contest-details-left">
            <div class="entries-block">
              <ul>
                <li><span>@if(isset($arr_contest['contest_entry'])) @php echo count($arr_contest['contest_entry']); @endphp @else {{'0'}} @endif</span>{{ trans('contets_listing/listing.text_total_entries') }}</li>
                @if($arr_contest['winner_choose'] == 'YES')
                <li><span><img style="height: 40px;" src="{{url('/public')}}/front/images/archexpertdefault/contest-winner.png"></span>Winner Declared</li>   
                @else
                <li><span id="days_left_timer{{$arr_contest["id"]}}"></span><p id="days_left_timer_text{{$arr_contest["id"]}}">{{ trans('contets_listing/listing.text_left') }}</p></li>
                <?php 
                $timeFirst      = strtotime(date('Y-m-d H:i:s'));
                $timeSecond     = strtotime($arr_contest['contest_end_date']);
                $diffInSeconds  = $timeSecond - $timeFirst;
                if($diffInSeconds <= 0){
                  $diffInSeconds = '0';
                }
                ?>
                <script type="text/javascript">
                  var time        = '<?php echo $diffInSeconds; ?>';
                  var contestid   = '<?php echo $arr_contest["id"]; ?>';
                  if(time < 0) {  time = 0; }
var c           = time; // in seconds
var t;
var days    = parseInt( c / 86400 ) % 365;
var hours   = parseInt( c / 3600 ) % 24;
var minutes = parseInt( c / 60 ) % 60;
var seconds = c % 60;
var d = ""; 
var h = ""; 
var m = "";
var s = ""; 
if(days     !=  ""  &&  days     !="0"){  d  =  (days+'d')     +  " ";  }
if(hours    !=  ""  &&  hours    !="0"){  h  =  (hours+'h')    +  " ";  }
if(minutes  !=  ""  &&  minutes  !="0"){  m  =  (minutes+'m')  +  " ";  }
if(seconds  !=  ""  &&  seconds  !="0"){  s  =  (seconds+'s')  +  " ";  }

if(d != "" && h !=""){
  var result    = d + h;
}else if(h != "" && m !=""){
  var result    = h + m;
}else if(m !=""){
  var result    = m;
}else if(s !=""){
  var result    = s;
}
if(result == "NaN:NaN:NaN:NaN"){ 
  $('#days_left_timer'+contestid).html('{{trans('client/projects/ongoing_projects.text_next_expire')}}');
  $('#days_left_timer'+contestid).css('color','red');
  $('#days_left_timer_text'+contestid).html('');
} else {
  $('#days_left_timer'+contestid).html(result);
  $('#days_left_timer'+contestid).css('color','green');
}
if(c == 0 ){
  $('#days_left_timer'+contestid).html('{{trans('client/projects/ongoing_projects.text_next_expire')}}');
  $('#days_left_timer'+contestid).css('color','red');
  $('#days_left_timer_text'+contestid).html('');
}
c = c - 1;
</script> 
@endif     
</ul>
<div class="clearfix"></div>
</div>

<div class="contest-detail-block">
  <h4>{{ trans('contets_listing/listing.text_contest_details') }}</h4>
  <p>{{isset($arr_contest['contest_description'])?$arr_contest['contest_description']:""}}</p>
  <div class="attachments-block">

    @php $download_file = "download"; $user_role = ""; @endphp
    @if(isset($user) && $user != null)
    @php $user_role    = get_user_role($user->id); @endphp
    @if($user_role != 'expert')  
    @if($user->id  != $arr_contest['client_user_id'])
    @php $download_file = ""; @endphp
    @endif  
    @endif
    @else
    @php $download_file = ""; @endphp 
    @endif

    @if(isset($arr_contest['contest_post_documents']) && count($arr_contest['contest_post_documents'])>0)
      <h4> {{ trans('contets_listing/listing.text_attachments') }} 
      
      @if(isset($user) && $user!=null)
        @if($user_role == 'expert' || $user->id == $arr_contest['client_user_id'])
          <a href="{{url('/contests/details/make_zip/')}}/{{base64_encode($arr_contest['id'])}}" style="font-size: 17px;float: right;" class="close-overlay">
              <i class="fa fa-file-archive-o right"></i> {{trans("new_translations.text_download_all_files")}}
          </a> </h4>
            
           <div class="project-list mile-list nw-bold">
    <div class="demo-gallery dark mrb35 new-add-photo">
      <ul class="fixed-size list-unstyled row">
         @if(isset($arr_contest['contest_post_documents']) && count($arr_contest['contest_post_documents'])>0)
          @foreach($arr_contest['contest_post_documents'] as $data)

             <?php 
            $contest_attachment     = isset($data['image_name'])?$data['image_name']:"";
            $get_attatchment_format = get_attatchment_format($contest_attachment);
            ?>
            @if(isset($get_attatchment_format) && $get_attatchment_format != '')
            @if(trim($get_attatchment_format) == 'pdf')
                 <li data-src="{{ url('/')}}/front/images/file_formats/pdf.png">
                    <a href="javascript:void(0);">
                        <img class="img-responsive" src="{{url('/public')}}/front/images/file_formats/pdf.png" alt="pre">
                        <div class="demo-gallery-poster">
                         <img src="{{url('/public')}}/front/images/zoom.png"  class="img-responsive" alt=""/>
                        </div>
                    </a>
                   
                    <div class="down-arrow">
                        <a {{$download_file}} href="{{url('/public')}}/{{config('app.project.img_path.contest_files')}}{{$data['image_name']}}"
                            style="display: block;position: relative;text-align: center;"><i class="fa fa-cloud-download"></i></a>
                    </div>
                 </li>
            @elseif(trim($get_attatchment_format) == 'xlsx' || trim($get_attatchment_format) == 'xls')
                <li data-src="{{ url('/')}}/front/images/file_formats/xlsx.png">
                <a href="javascript:void(0);">
                    <img class="img-responsive" src="{{url('/public')}}/front/images/file_formats/xlsx.png" alt="pre">
                    <div class="demo-gallery-poster">
                     <img src="{{url('/public')}}/front/images/zoom.png"  class="img-responsive" alt=""/>
                    </div>
                </a>
                
                <div class="down-arrow">
                <a {{$download_file}} href="{{url('/public')}}/{{config('app.project.img_path.contest_files')}}{{$data['image_name']}}" style="display: block;position: relative;text-align: center;"><i class="fa fa-cloud-download"></i></a>
                </div>
            </li>
            @elseif(trim($get_attatchment_format) == 'txt')
                <li data-src="{{ url('/')}}/front/images/file_formats/txt.png">
                <a href="javascript:void(0);">
                    <img class="img-responsive" src="{{url('/public')}}/front/images/file_formats/txt.png" alt="pre">
                    <div class="demo-gallery-poster">
                     <img src="{{url('/public')}}/front/images/zoom.png"  class="img-responsive" alt=""/>
                    </div>
                </a>
                <div class="down-arrow">
                <a {{$download_file}} href="{{url('/public')}}/{{config('app.project.img_path.contest_files')}}{{$data['image_name']}}"
                            style="display: block;position: relative;text-align: center;"><i class="fa fa-cloud-download"></i></a>
                </div>
            </li>
            @elseif(trim($get_attatchment_format) == 'zip')
            <li data-src="{{ url('/')}}/front/images/file_formats/zip.png">
                <a href="javascript:void(0);">
                    <img class="img-responsive" src="{{url('/public')}}/front/images/file_formats/zip.png" alt="pre">
                    <div class="demo-gallery-poster">
                     <img src="{{url('/public')}}/front/images/zoom.png"  class="img-responsive" alt=""/>
                    </div>
                </a>

                <div class="down-arrow">
                <a {{$download_file}} href="{{url('/public')}}/{{config('app.project.img_path.contest_files')}}{{$data['image_name']}}"
                            style="display: block;position: relative;text-align: center;"><i class="fa fa-cloud-download"></i></a>
                </div>
            </li>
                {{-- <img src="{{ url('/')}}/front/images/file_formats/zip.png" data-medium-img="{{ url('/')}}/front/images/file_formats/zip.png" data-big-img="{{ url('/')}}/front/images/file_formats/zip.png" alt=""> --}}
            @elseif(trim($get_attatchment_format) == 'doc')
                <li data-src="{{ url('/')}}/front/images/file_formats/doc.png">
                <a href="javascript:void(0);">
                    <img class="img-responsive" src="{{url('/public')}}/front/images/file_formats/doc.png" alt="pre">
                    <div class="demo-gallery-poster">
                     <img src="{{url('/public')}}/front/images/zoom.png"  class="img-responsive" alt=""/>
                    </div>
                </a>
                <div class="down-arrow">
                <a {{$download_file}} href="{{url('/public')}}/{{config('app.project.img_path.contest_files')}}{{$data['image_name']}}"
                            style="display: block;position: relative;text-align: center;"><i class="fa fa-cloud-download"></i></a>
                </div>
            </li>
            @elseif(trim($get_attatchment_format) == 'docx')
                <li data-src="{{ url('/')}}/front/images/file_formats/docx.png">
                <a href="javascript:void(0);">
                    <img class="img-responsive" src="{{url('/public')}}/front/images/file_formats/docx.png" alt="pre">
                    <div class="demo-gallery-poster">
                     <img src="{{url('/public')}}/front/images/zoom.png"  class="img-responsive" alt=""/>
                    </div>
                </a>
                <div class="down-arrow">
                <a {{$download_file}} href="{{url('/public')}}/{{config('app.project.img_path.contest_files')}}{{$data['image_name']}}"
                            style="display: block;position: relative;text-align: center;"><i class="fa fa-cloud-download"></i></a>
                </div>
            </li>
            @elseif(trim($get_attatchment_format) == 'odt')
                <li data-src="{{ url('/')}}/front/images/file_formats/odt.png">
                <a href="javascript:void(0);">
                    <img class="img-responsive" src="{{url('/public')}}/front/images/file_formats/odt.png" alt="pre">
                    <div class="demo-gallery-poster">
                     <img src="{{url('/public')}}/front/images/zoom.png"  class="img-responsive" alt=""/>
                    </div>
                </a>
                <div class="down-arrow">
                <a {{$download_file}} href="{{url('/public')}}/{{config('app.project.img_path.contest_files')}}{{$data['image_name']}}"
                            style="display: block;position: relative;text-align: center;"><i class="fa fa-cloud-download"></i></a>
                </div>
            </li>
            @else
            <li data-src="{{url('/public')}}{{config('app.project.img_path.contest_files')}}{{$data['image_name']}}">
                <a href="javascript:void(0);">
                    <img class="img-responsive" src="{{url('/public')}}{{config('app.project.img_path.contest_files')}}{{$data['image_name']}}" alt="pre">
                    <div class="demo-gallery-poster">
                     <img src="{{url('/public')}}/front/images/zoom.png"  class="img-responsive" alt=""/>
                    </div>
                </a>
                <div class="down-arrow">
                <a {{$download_file}} href="{{url('/public')}}/{{config('app.project.img_path.contest_files')}}{{$data['image_name']}}"
                            style="display: block;position: relative;text-align: center;"><i class="fa fa-cloud-download"></i></a>
                </div>
            </li>
            @endif
             @endif
             @endforeach
             @endif
      </ul>
    </div>
  </div>

        @else
          <br><br><i><p class="contest-listing-note-section" style="color: red; font-size: 15px;">{{trans('contets_listing/listing.text_please_login_as_expert_to_download_and_see_original_attachment')}}</p></i>
        @endif
      @else
        <br><br><i><p class="contest-listing-note-section" style="color: red; font-size: 15px;">{{trans('contets_listing/listing.text_please_login_as_expert_to_download_and_see_original_attachment')}}</p></i>
      @endif
    @endif

   
  </div>


</div>
<div class="clearfix"></div>
@if(isset($user) && $user != false)
@if(!$user->inRole('expert'))
 <div class="skills-req contest-details-skills-req">
  <h4>{{ trans('contets_listing/listing.text_skills_required') }}</h4>
  @if(isset($arr_contest['contest_skills']) && sizeof($arr_contest['contest_skills'])>0)
  <ul>
    @foreach($arr_contest['contest_skills'] as $skill)
    <li>{{isset($skill['skill_data']['skill_name'])?$skill['skill_data']['skill_name']:""}}</li>
    @endforeach 
  </ul>
  @else
  {{ trans('contets_listing/listing.text_no_data_found') }}
  @endif
  <div class="clearfix"></div>
</div> 
@endif
@endif
@if(isset($arr_contest['contest_additional_info']) && $arr_contest['contest_additional_info'] !="")
<div class="contest-detail-block">
  <h4>{{ trans('contets_listing/listing.text_addition_information') }}</h4>
  <p>{{$arr_contest['contest_additional_info'] or ''}}</p>
  <div class="clearfix"></div>
</div>
@endif

@if(isset($user) && $user != false) 

@if(isset($arr_contest['contest_end_date']) && date('Y-m-d') < $arr_contest['contest_end_date'])
@if($user->inRole('expert'))
@if(count($arr_contest['contest_entry']) <= 0) 
@if($arr_contest['winner_choose'] == 'NO')
@if($is_skill_category_matched == true)
<div class="send-enq-section">
  {{-- <p>{{ trans('contets_listing/listing.text_be_the_first_to_send_entry') }}</p>
  <a class="black-border-btn" href="#cnst-send-entry" data-keyboard="false" data-backdrop="static"  data-toggle="modal">{{ trans('contets_listing/listing.text_send_entry') }}</a> --}}
  <div class="clearfix"></div>
</div>
@else
<div class="skills-req">
  <span style="font-family: 'robotolight';">{{trans('controller_translations.msg_skill_category_did_not_match_contest')}}</span>
  </div> 
  @endif 
  @endif 
  @endif 
  @endif
  @endif  
  @endif 

</div>
</div>
@php
$sidebar_information     = sidebar_information($arr_contest['client_user_id']);
$profile_img_public_path = url('/').config('app.project.img_path.profile_image');
$profile_img_base_path   = base_path().'/public'.config('app.project.img_path.profile_image');
@endphp

<div class="col-sm-5 col-md-4 col-lg-4">
  <div class="contest-details-right text-center">
    <h3>{{ trans('contets_listing/listing.text_employer') }}</h3>
    <div class="contest-profile-block">
      <div class="con-profile-section">
        @if(isset($sidebar_information['user_details']['profile_image']) && $sidebar_information['user_details']['profile_image']!=null && file_exists($profile_img_base_path.$sidebar_information['user_details']['profile_image']))
        <img src="{{$profile_img_public_path.$sidebar_information['user_details']['profile_image']}}" class="img-responsive" alt=""/>
        @else
        <img src="{{$profile_img_public_path.'default_profile_image.png'}}" class="img-responsive" alt=""/>
        @endif
      </div>
      <div class="profile-content">
        <h5>{{isset($sidebar_information['user_details']['first_name'])?$sidebar_information['user_details']['first_name'].'  '.str_limit($sidebar_information['user_details']['last_name'],1,'.'):""}}</h5>
        <p>{{isset($sidebar_information['user_details']['country_details']['country_name'])?$sidebar_information['user_details']['country_details']['country_name']:""}}</p>
        <div class="avg-rating">
          <div class="avg"  style="margin-left:0px;">{{$sidebar_information['average_rating']}}</div>
          <ul>
            <span class="stars">{{$sidebar_information['average_rating']}}</span>
          </ul>
        </div>
      </div>
      <div class="entries-block">
        <ul>
          <li><span title="jobs+contest">{{isset($sidebar_information['contest_and_project_posted_count'])?$sidebar_information['contest_and_project_posted_count']:"0"}}</span> {{ trans('contets_listing/listing.text_projects_posted') }}</li>
          <li><span>{{isset($arr_contest['contest_currency'])?$arr_contest['contest_currency']:""}} {{isset($sidebar_information['total_project_cost'])?$sidebar_information['total_project_cost']:"0"}}</span> {{ trans('contets_listing/listing.text_total_spent') }}</li>
        </ul>
        <div class="clearfix"></div>
      </div>
    </div>
    @if(isset($user) && $user != false) 
    @if(isset($arr_contest['contest_end_date']) && date('Y-m-d') < $arr_contest['contest_end_date'])
    @if($user->inRole('expert'))
    @if($arr_contest['winner_choose'] == 'NO')
    @if($is_entry_exists=='0')
    @if($is_skill_category_matched == true)
    <a href="#cnst-send-entry" data-keyboard="false" data-backdrop="static"  data-toggle="modal" class="black-btn">{{ trans('contets_listing/listing.text_send_entry') }}</a>
    @else
    <div class="skills-req">
      <span style="font-family: 'robotolight';">{{trans('controller_translations.msg_skill_category_did_not_match_contest')}}</span>
    </div> 
    @endif
    @else
    <div class="skills-req">
      {{-- <span style="font-family: 'robotolight';"><i>{{trans("new_translations.text_you_have_already_sent_an_entry")}}</i></span> --}}
    </div> 

    <a href="{{url('/expert/contest/show_contest_entry_details/')}}/{{isset($arr_contest_expert['id'])?base64_encode($arr_contest_expert['id']):base64_encode(0)}}" data-keyboard="false" data-backdrop="static" data-toggle="modal" class="black-btn"><i class="fa fa-upload" aria-hidden="true"></i> {{trans("new_translations.text_upload_more_entries")}}</a>
    @endif
    @endif
    @endif
    @endif
    @endif  
  </div>
</div>
</div>
<!-- Winner -->
<div class="entries-recev-section">
  @if(isset($arr_contest_winner) && sizeof($arr_contest_winner)>0)
  <h4>                           
    <span><img style="height: 40px;" src="{{url('/public')}}/front/images/archexpertdefault/contest-winner.png"></span>
    Winner
  </h4>
  <div class="contest-bottom-list">
    <div class="row">  
      @php
      $profile_img_public_path  = url('/').config('app.project.img_path.profile_image');
      $profile_img_base_path    = base_path().'/public'.config('app.project.img_path.profile_image');
      $timeline_img_public_path = url('/').config('app.project.img_path.cover_image');
      $timeline_img_base_path   = base_path().'/public'.config('app.project.img_path.cover_image');
      @endphp            
      @if(isset($arr_contest_winner) && sizeof($arr_contest_winner)>0)
      @php
      $expert_sidebar_information      = sidebar_information(isset($arr_contest_winner['expert_id'])?$arr_contest_winner['expert_id']:"");
      @endphp
      <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 coll-list">
        @if($arr_contest_winner['is_winner'] == 'YES')
        <span class="contest-winner"><img style="cursor: default;height: 40px;" src="{{url('/public')}}/front/images/archexpertdefault/contest-winner.png"></span>   
        @endif
        <div class="coll-list-block" style="box-shadow : 0 5px 20px rgba(0,0,0,0.2)">
          <div class="coll-img p-relative">
            <!-- Image --> 
            @if(isset($arr_contest_winner['contest_entry_files']))
            <!-- entry fist image -->
            @foreach($arr_contest_winner['contest_entry_files'] as $imgkey => $img)
            @php $explod = explode('.',$arr_contest_winner['contest_entry_files'][$imgkey]['image']);@endphp 
            @if(isset($explod[1]) && $explod[1] == 'jpg'  ||
              $explod[1] == 'jpeg' ||
              $explod[1] == 'JPG'  ||
              $explod[1] == 'png'  ||
              $explod[1] == 'PNG'  ||
              $explod[1] == 'JPEG' 
              )
              @if(isset($arr_contest_winner['contest_entry_files'][$imgkey]['image']) && $arr_contest_winner['contest_entry_files'][$imgkey]['image']!=null && file_exists('public/uploads/front/postcontest/send_entry/'.$arr_contest_winner['contest_entry_files'][$imgkey]['image']))
              <img src="{{url('/uploads/front/postcontest/send_entry/'.$arr_contest_winner['contest_entry_files'][$imgkey]['image'])}}" class="img-responsive" alt=""/>
              @else
              <img src="{{$timeline_img_public_path.'/resize_cache/default-371x250.jpg'}}" class="img-responsive" alt=""/>
              @endif
              @else
              <!-- cover image -->
              @if(isset($expert_sidebar_information['user_details']['cover_image']) && $expert_sidebar_information['user_details']['cover_image']!=null && file_exists($timeline_img_base_path.$expert_sidebar_information['user_details']['cover_image']))
              <img src="{{$timeline_img_public_path.$expert_sidebar_information['user_details']['cover_image']}}" class="img-responsive" alt=""/>
              @else
              <img src="{{$timeline_img_public_path.'/resize_cache/default-371x250.jpg'}}" class="img-responsive" alt=""/>
              @endif
              <!-- end cover image -->
              @endif
              @endforeach  
              <!-- end entry first image --> 
              @else
              <!-- cover image -->
              @if(isset($expert_sidebar_information['user_details']['cover_image']) && $expert_sidebar_information['user_details']['cover_image']!=null && file_exists($timeline_img_base_path.$expert_sidebar_information['user_details']['cover_image']))
              <img src="{{$timeline_img_public_path.$expert_sidebar_information['user_details']['cover_image']}}" class="img-responsive" alt=""/>
              @else
              <img src="{{$timeline_img_public_path.'/resize_cache/default-371x250.jpg'}}" class="img-responsive" alt=""/>
              @endif
              <!-- end cover image -->
              @endif                     
              <!-- Image -->
              <div class="design-title">
                <!--<p>{{isset($arr_contest_winner['title'])?str_limit($arr_contest_winner['title'],'30','...'):""}}</p>-->
                <div class="id-block">{{trans('client/contest/common.text_entry')}} {{isset($arr_contest_winner['entry_id'])?'#'.$arr_contest_winner['entry_id']:"-"}}</div>
                <div class="avg-rating">
<!-- <span>{{isset($entry['contest_entry_files_highest_rating'][0]['rating'])?$entry['contest_entry_files_highest_rating'][0]['rating']:0}}</span>
  <div class="avg" style="margin-left:0px;"><i class="fa fa-star"></i></div> -->
  <ul>
    <span class="stars">{{isset($arr_contest_winner['contest_entry_files_highest_rating'][0]['rating'])?$arr_contest_winner['contest_entry_files_highest_rating'][0]['rating']:0}}</span>
  </ul>
</div>
</div>
<?php 
$post_contest_user_id  = isset($arr_contest['client_user_id'])?$arr_contest['client_user_id']:""; 
$apply_expert_user_id  = isset($arr_contest_winner['expert_id'])?$arr_contest_winner['expert_id']:"";
?>
</div>
<div class="coll-details">
  <div class="coll-person">
    <div class="collaboration-img">
      @if(isset($expert_sidebar_information['user_details']['profile_image']) && $expert_sidebar_information['user_details']['profile_image']!=null && file_exists($profile_img_base_path.$expert_sidebar_information['user_details']['profile_image']))
      <img src="{{$profile_img_public_path.$expert_sidebar_information['user_details']['profile_image']}}" class="img-responsive" alt=""/>
      @else
      <img src="{{$profile_img_public_path.'default_profile_image.png'}}" class="img-responsive" alt=""/>
      @endif
      @if(isset($expert_sidebar_information['last_login_duration']) && $expert_sidebar_information['last_login_duration']=='Active')
      <div class="online-status"> <img src="{{url('/public/front/images/active.png')}}"   class=""></div>
      @else
      <div class="online-status"> <img src="{{url('/public/front/images/deactive.png')}}" class=""></div>
      @endif
    </div>
    <div class="coll-name">
      <h5>{{isset($expert_sidebar_information['user_details']['first_name'])?$expert_sidebar_information['user_details']['first_name'].'  '.str_limit($expert_sidebar_information['user_details']['last_name'],1,'.'):""}}</h5>
      @if($expert_sidebar_information['last_login_duration'] == 'Active')
      <span class="flag" title="{{isset($expert_sidebar_information['expert_timezone'])?$expert_sidebar_information['expert_timezone']:'-'}}">
        <span class="flag-image"> 
          @if(isset($expert_sidebar_information['user_country_flag'])) 
          @php 
          echo $expert_sidebar_information['user_country_flag']; 
          @endphp 
          @elseif(isset($expert_sidebar_information['user_country'])) 
          @php 
          echo $expert_sidebar_information['user_country']; 
          @endphp 
          @else 
          @php 
          echo '-'; 
          @endphp 
          @endif 
        </span>
        {{-- trans('common/_expert_details.text_local_time') --}}  {{isset($expert_sidebar_information['expert_timezone_without_date'])?$expert_sidebar_information['expert_timezone_without_date']:'-'}}
      </span>
      @else
      <span class="" title="{{$expert_sidebar_information['last_login_full_duration']}}">
        <span class="flag-image"> 
          @if(isset($expert_sidebar_information['user_country_flag'])) 
          @php 
          echo $expert_sidebar_information['user_country_flag']; 
          @endphp 
          @elseif(isset($expert_sidebar_information['user_country'])) 
          @php 
          echo $expert_sidebar_information['user_country']; 
          @endphp 
          @else 
          @php 
          echo '-'; 
          @endphp 
          @endif 
        </span>
        <!-- <i  class="fa fa-clock-o" aria-hidden="true"></i> -->
        <a style="color:#4D4D4D;font-size: 14px;text-transform:none;cursor:text;">
          {{$expert_sidebar_information['last_login_duration']}}
        </a>
      </span>
      @endif
    </div>
  </div>
  <div class="clearfix"></div>
</div>
</div>
</div>
@endif
</div>
</div>            
@endif
</div> 
<!-- Winner -->


<!-- // HIde entry listings -->

@if(isset($arr_contest_entry_data['data']) && sizeof($arr_contest_entry_data['data'])>0 || isset($arr_contest['contest_entry']) && sizeof($arr_contest['contest_entry'])>0)
<div class="contest-bottom-list">
  <div class="row">  
    @php
    $profile_img_public_path  = url('/').config('app.project.img_path.profile_image');
    $profile_img_base_path    = base_path().'/public'.config('app.project.img_path.profile_image');
    $timeline_img_public_path = url('/').config('app.project.img_path.cover_image');
    $timeline_img_base_path   = base_path().'/public'.config('app.project.img_path.cover_image');
    @endphp
    
    @if(isset($arr_contest_expert) && sizeof($arr_contest_expert)>0)
    @php
    $expert_sidebar_information      = sidebar_information(isset($arr_contest_expert['expert_id'])?$arr_contest_expert['expert_id']:"");
    $higest_rating_file = '';

    if($arr_contest_expert['contest_entry_files_highest_rating']!=null && count($arr_contest_expert['contest_entry_files_highest_rating']>0))
    {
        $higest_rating_file =  $arr_contest_expert['contest_entry_files_highest_rating'][0]['file_no'];        
    }
    @endphp
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 coll-list">
      @if($arr_contest_expert['is_winner'] == 'YES')
      <span class="contest-winner"><img style="cursor: default;height: 40px;" src="{{url('/public')}}/front/images/archexpertdefault/contest-winner.png"></span>   
      @elseif($arr_contest_expert['sealed_entry'] == '1')
      <span class="sealed-entry"><i class="fa fa-lock"></i></span>   
      @endif
      <div class="coll-list-block" style="box-shadow : 0 5px 20px rgba(0,0,0,0.2)">
        <div class="coll-img p-relative">
          <!-- Image --> 


          @if(isset($arr_contest_expert['contest_entry_files']))
          <!-- entry fist image -->
          @foreach($arr_contest_expert['contest_entry_files'] as $imgkey => $img)

          @php $explod = explode('.',$arr_contest_expert['contest_entry_files'][$imgkey]['image']);@endphp 
          @if(isset($explod[1]) && $explod[1] == 'jpg'  ||
            $explod[1] == 'jpeg' ||
            $explod[1] == 'JPG'  ||
            $explod[1] == 'png'  ||
            $explod[1] == 'PNG'  ||
            $explod[1] == 'JPEG' 
            )
            @if($arr_contest_expert['contest_entry_files'][$imgkey]['file_no'] == $higest_rating_file)
              @if(isset($arr_contest_expert['contest_entry_files'][$imgkey]['image']) && $arr_contest_expert['contest_entry_files'][$imgkey]['image']!=null && file_exists('public/uploads/front/postcontest/send_entry/'.$arr_contest_expert['contest_entry_files'][$imgkey]['image']))
              <img src="{{url('/uploads/front/postcontest/send_entry/'.$arr_contest_expert['contest_entry_files'][$imgkey]['image'])}}" class="img-responsive" alt=""/>
              @else
              <img src="{{$timeline_img_public_path.'/resize_cache/default-371x250.jpg'}}" class="img-responsive" alt=""/>
              @endif
            @elseif(isset($arr_contest_expert['contest_entry_files'][$imgkey]['image']) && $arr_contest_expert['contest_entry_files'][$imgkey]['image']!=null && file_exists('public/uploads/front/postcontest/send_entry/'.$arr_contest_expert['contest_entry_files'][$imgkey]['image']))
                <img src="{{url('/uploads/front/postcontest/send_entry/'.$arr_contest_expert['contest_entry_files'][$imgkey]['image'])}}" class="img-responsive" alt=""/>
              @else
                <img src="{{$timeline_img_public_path.'/resize_cache/default-371x250.jpg'}}" class="img-responsive" alt=""/>
            @endif
            @endif
            @endforeach  
            @endif                     
 

            <div class="design-title">
              <div class="id-block">{{trans('client/contest/common.text_entry')}} {{isset($arr_contest_expert['entry_id'])?'#'.$arr_contest_expert['entry_id']:"-"}}</div>
              <div class="avg-rating">
                <ul>
                  <span class="stars">{{isset($arr_contest_expert['contest_entry_files_highest_rating'][0]['rating'])?$arr_contest_expert['contest_entry_files_highest_rating'][0]['rating']:0}}</span>
                </ul>
              </div>
            </div>

            <?php 
            $post_contest_user_id  = isset($arr_contest['client_user_id'])?$arr_contest['client_user_id']:""; 
            $apply_expert_user_id  = isset($arr_contest_expert['expert_id'])?$arr_contest_expert['expert_id']:"";
            ?>

            <div class="list-hover">
              @if(isset($user) && $user != false && $user->inRole('expert')) 
              @if($user->id == $apply_expert_user_id)                      
              <a href="{{$module_url_path}}/show_contest_entry_details/{{base64_encode($arr_contest_expert['id'])}}" class="view-button" style="cursor: pointer;">{{trans('contets_listing/listing.text_entry_details') }}</a>
              @endif  
              @elseif(isset($user) && $user != false && $user->inRole('client'))
              @if($user->id == $post_contest_user_id)
              <a href="{{$module_url_path}}/show_contest_entry_details/{{base64_encode($arr_contest_expert['id'])}}" class="view-button" style="cursor: pointer;">{{trans('contets_listing/listing.text_entry_details') }}</a> 
              @endif 
              @else
              <a href="{{url('/login')}}" class="view-button">Login</a>  
              @endif    
            </div>
          </div>
          <div class="coll-details">
            <div class="coll-person">
<!-- <div class="collaboration-img">
@if(isset($expert_sidebar_information['user_details']['profile_image']) && $expert_sidebar_information['user_details']['profile_image']!=null && file_exists($profile_img_base_path.$expert_sidebar_information['user_details']['profile_image']))
<img src="{{$profile_img_public_path.$expert_sidebar_information['user_details']['profile_image']}}" class="img-responsive" alt=""/>
@else
<img src="{{$profile_img_public_path.'default_profile_image.png'}}" class="img-responsive" alt=""/>
@endif

@if(isset($expert_sidebar_information['last_login_duration']) && $expert_sidebar_information['last_login_duration']=='Active')
<div class="online-status"> <img src="{{url('/public/front/images/active.png')}}"   class=""></div>
@else
<div class="online-status"> <img src="{{url('/public/front/images/deactive.png')}}" class=""></div>
@endif
</div> -->
<div class="coll-name" style="margin-left: 0px;">
  <h5>{{isset($expert_sidebar_information['user_details']['first_name'])?$expert_sidebar_information['user_details']['first_name'].'  '.str_limit($expert_sidebar_information['user_details']['last_name'],1,'.'):""}}</h5>
  @if($expert_sidebar_information['last_login_duration'] == 'Active')
  <span class="flag" title="{{isset($expert_sidebar_information['expert_timezone'])?$expert_sidebar_information['expert_timezone']:'-'}}">
    <span class="flag-image"> 
      @if(isset($expert_sidebar_information['user_country_flag'])) 
      @php 
      echo $expert_sidebar_information['user_country_flag']; 
      @endphp 
      @elseif(isset($expert_sidebar_information['user_country'])) 
      @php 
      echo $expert_sidebar_information['user_country']; 
      @endphp 
      @else 
      @php 
      echo '-'; 
      @endphp 
      @endif 
    </span>
    {{-- trans('common/_expert_details.text_local_time') --}}  {{isset($expert_sidebar_information['expert_timezone_without_date'])?$expert_sidebar_information['expert_timezone_without_date']:'-'}}
  </span>
  @else
  <span class="" title="{{$expert_sidebar_information['last_login_full_duration']}}">
    <span class="flag-image"> 
      @if(isset($expert_sidebar_information['user_country_flag'])) 
      @php 
      echo $expert_sidebar_information['user_country_flag']; 
      @endphp 
      @elseif(isset($expert_sidebar_information['user_country'])) 
      @php 
      echo $expert_sidebar_information['user_country']; 
      @endphp 
      @else 
      @php 
      echo '-'; 
      @endphp 
      @endif 
    </span>
    <!-- <i  class="fa fa-clock-o" aria-hidden="true"></i> -->
    <a style="color:#4D4D4D;font-size: 14px;text-transform:none;cursor:text;">
      {{$expert_sidebar_information['last_login_duration']}}
    </a>
  </span>
  @endif
</div>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
@endif
@foreach($arr_contest_entry_data['data'] as $key=>$entry)
@php
$sidebar_information  = sidebar_information($entry['expert_id']);
@endphp
<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 coll-list">
  <div class="coll-list-block sealed-block">
    @if($entry['is_winner'] == 'YES')
    <span class="contest-winner"><img style="cursor: default;height: 40px;" src="{{url('/public')}}/front/images/archexpertdefault/contest-winner.png"></span>   
    @elseif($entry['sealed_entry'] == '1')
    <span class="sealed-entry"><i class="fa fa-lock"></i></span>   
    @endif
    <div class="coll-img p-relative">
      <!-- Image --> 
      
      @if(isset($entry['contest_entry_files']))

      <?php
        $higest_rating_file = '';

        if($entry['contest_entry_files_highest_rating'] != null && count($entry['contest_entry_files_highest_rating'])>0)
        {
            $higest_rating_file = $entry['contest_entry_files_highest_rating'][0]['file_no'];             
        }

      ?>

      <!-- entry fist image -->
      @foreach($entry['contest_entry_files'] as $imgkey => $img)
      @php $explod = explode('.',$entry['contest_entry_files'][$imgkey]['image']);@endphp 
      @if(isset($explod[1]) && $explod[1] == 'jpg'  ||
        $explod[1] == 'jpeg' ||
        $explod[1] == 'JPG'  ||
        $explod[1] == 'png'  ||
        $explod[1] == 'PNG'  ||
        $explod[1] == 'JPEG' 
        )
        @if($entry['contest_entry_files'][$imgkey]['file_no'] == $higest_rating_file)

          @if(isset($entry['contest_entry_files'][$imgkey]['image']) && $entry['contest_entry_files'][$imgkey]['image']!=null && file_exists('public/uploads/front/postcontest/send_entry/'.$entry['contest_entry_files'][$imgkey]['image']))
          <img src="{{url('/uploads/front/postcontest/send_entry/'.$entry['contest_entry_files'][$imgkey]['image'])}}" class="img-responsive" alt=""/>
          @else
          <img src="{{$timeline_img_public_path.'/resize_cache/default-371x250.jpg'}}" class="img-responsive" alt=""/>
          @endif
        @elseif(isset($entry['contest_entry_files'][$imgkey]['image']) && $entry['contest_entry_files'][$imgkey]['image']!=null && file_exists('public/uploads/front/postcontest/send_entry/'.$entry['contest_entry_files'][$imgkey]['image']))
            <img src="{{url('/uploads/front/postcontest/send_entry/'.$entry['contest_entry_files'][$imgkey]['image'])}}" class="img-responsive" alt=""/>
          @else
            <img src="{{$timeline_img_public_path.'/resize_cache/default-371x250.jpg'}}" class="img-responsive" alt=""/>
        @endif
        @endif
        @endforeach  
        @endif                     
        <!-- Image -->

        <div class="design-title">
          <div class="id-block">{{trans('client/contest/common.text_entry')}} {{isset($entry['entry_id'])?'#'.$entry['entry_id']:"-"}}</div>
          <div class="avg-rating">
            <ul>
            <span class="stars">{{isset($entry['contest_entry_files_highest_rating'][0]['rating'])?$entry['contest_entry_files_highest_rating'][0]['rating']:0}}</span>
            </ul>
          </div>
        </div>
        <?php 
        $post_contest_user_id  = isset($arr_contest['client_user_id'])?$arr_contest['client_user_id']:""; 
        $apply_expert_user_id  = isset($entry['expert_id'])?$entry['expert_id']:"";

        ?>
        <div class="list-hover">
          @if($entry['sealed_entry'] == '0')   
          @if(isset($user) && $user != false && $user->inRole('expert')) 
          {{--  @if($user->id != $apply_expert_user_id)      --}}                 
          <a href="{{$module_url_path}}/show_contest_entry_details/{{base64_encode($entry['id'])}}" class="view-button" style="cursor: pointer;">{{trans('contets_listing/listing.text_entry_details') }}</a>
          {{-- @endif   --}}
          @elseif(isset($user) && $user != false && $user->inRole('client'))
          @if($user->id == $post_contest_user_id)
          <a href="{{$module_url_path}}/show_contest_entry_details/{{base64_encode($entry['id'])}}" class="view-button" style="cursor: pointer;">{{trans('contets_listing/listing.text_entry_details') }}</a> 
          @endif 
          @else
          <a href="{{url('/login')}}" class="view-button">Login</a>  
          @endif   
          @else
          <a style="cursor:no-drop;" href="javascript:void(0)" class="view-button">Sealed</a> 
          @endif 
        </div>
</div>

<div class="coll-details">
  <div class="coll-person">
<!-- <div class="collaboration-img">
@if(isset($sidebar_information['user_details']['profile_image']) && $sidebar_information['user_details']['profile_image']!=null && file_exists($profile_img_base_path.$sidebar_information['user_details']['profile_image']))
<img src="{{$profile_img_public_path.$sidebar_information['user_details']['profile_image']}}" class="img-responsive" alt=""/>
@else
<img src="{{$profile_img_public_path.'default_profile_image.png'}}" class="img-responsive" alt=""/>
@endif

@if(isset($sidebar_information['last_login_duration']) && $sidebar_information['last_login_duration']=='Active')
<div class="online-status"> <img src="{{url('/public/front/images/active.png')}}"   class=""></div>
@else
<div class="online-status"> <img src="{{url('/public/front/images/deactive.png')}}" class=""></div>
@endif
</div> -->
<div class="coll-name" style="margin-left: 0px;">
  <h5>{{isset($sidebar_information['user_details']['first_name'])?$sidebar_information['user_details']['first_name'].'  '.str_limit($sidebar_information['user_details']['last_name'],1,'.'):""}}</h5>
  @if($sidebar_information['last_login_duration'] == 'Active')
  <span class="flag" title="{{isset($sidebar_information['expert_timezone'])?$sidebar_information['expert_timezone']:'-'}}">
    <span class="flag-image"> 
      @if(isset($sidebar_information['user_country_flag'])) 
      @php 
      echo $sidebar_information['user_country_flag']; 
      @endphp 
      @elseif(isset($sidebar_information['user_country'])) 
      @php 
      echo $sidebar_information['user_country']; 
      @endphp 
      @else 
      @php 
      echo '-'; 
      @endphp 
      @endif 
    </span>
    {{-- trans('common/_expert_details.text_local_time') --}}  {{isset($sidebar_information['expert_timezone_without_date'])?$sidebar_information['expert_timezone_without_date']:'-'}}
  </span>
  @else
  <span class="" title="{{$sidebar_information['last_login_full_duration']}}">
    <span class="flag-image"> 
      @if(isset($sidebar_information['user_country_flag'])) 
      @php 
      echo $sidebar_information['user_country_flag']; 
      @endphp 
      @elseif(isset($sidebar_information['user_country'])) 
      @php 
      echo $sidebar_information['user_country']; 
      @endphp 
      @else 
      @php 
      echo '-'; 
      @endphp 
      @endif 
    </span>
    <!-- <i  class="fa fa-clock-o" aria-hidden="true"></i> -->
    <a style="color:#4D4D4D;font-size: 14px;text-transform:none;cursor:text;">
      {{$sidebar_information['last_login_duration']}}
    </a>
  </span>
  @endif
</div>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
@endforeach
</div>
</div>
</div>
<!--pagigation start here-->
@include('front.common.pagination_view', ['paginator' => $arr_pagination])
<!--pagigation end here-->              
@endif
<!-- // hide entry end -->
</div>

@if($arr_contest['winner_choose'] == 'NO')
@include('front.contest_listing.send_entry_model')
@endif
<script type="text/javascript">
  function textCounter(field,maxlimit){
    var countfield = 0;
    if( field.value.length > maxlimit ){
      field.value = field.value.substring( 0, maxlimit );
      return false;
    } else {
      countfield = maxlimit - field.value.length;
      var message = '{{trans('client/projects/invite_experts.text_you_have_left')}} <b>'+countfield+'</b> {{trans('client/projects/invite_experts.text_characters_for_description')}}';
      jQuery('#cnt_desc_msg').html(message);
      return true;
    }
  }
</script>

<script type="text/javascript">

  $(document).ready(function(){

    $('.close-overlay').click(function(){

      setTimeout(function(){
        hideProcessingOverlay();
      },500);

    });

    $('.example1').webwingGallery({
        openGalleryStyle: 'transform',
        changeMediumStyle: true
    });   
  });

 $('.fixed-size').lightGallery({
            width: '700px',
            height: '470px',
            mode: 'lg-fade',
            addClass: 'fixed-size',
            counter: false,
            download: false,
            startClass: '',
            enableSwipe: false,
            enableDrag: false,
            speed: 500
        });
</script>
@stop