@extends('front.layout.master') @section('main_content')
@php $user = \Sentinel::check();@endphp
<div class="search-freelance-gray-bg search-project">
    <div class="container">
        <div class="row">
            @include('front.contest_listing._sidebar_contest_listing')
            <div class="col-sm-7 col-md-9 col-lg-9">
                @include('front.layout._operation_status')
                <div class="result-section">
                    <div class="row">

                        @if(isset($arr_pagination) && count($arr_pagination)>0)
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <p>{{trans('expert_listing/listing.text_showing')}} <b>{{isset($arr_pagination)?$arr_pagination->total():"0"}}</b> {{trans('expert_listing/listing.text_results')}}</p>
                        </div>
                        @endif
                        <form action="{{$module_url_path}}/search" id="frm_pagination" method="any">
                            <div class="col-sm-3 col-md-4 col-lg-4">
                                <div class="sort-by">
                                    <label>{{trans('expert_listing/listing.text_show')}}:</label>
                                    <select name="show_posted" class="select-box" id="show_posted">
                                        <option value="recently-posted" @if(isset($_REQUEST['showedby']) && $_REQUEST['showedby']=="recently-posted" ) {{'selected="selected"'}} @endif>{{trans('client/contest/common.text_recently_posted')}} </option> <option value="ending-soon" @if(isset($_REQUEST['showedby']) && $_REQUEST['showedby']=="ending-soon" ) {{'selected="selected"'}} @endif>{{trans('client/contest/common.text_ending_soon')}} </option> </select> </div> </div> </form> <form action="{{$module_url_path}}/show_cnt" id="frm_pagination" method="any">
                                            <div class="col-sm-3 col-md-2 col-lg-2">
                                                <div class="sort-by">
                                                    <label>{{trans('expert_listing/listing.text_show')}}:</label>
                                                    @php $show_cnt ='show_contest_listing_cnt'; @endphp
                                                    @include('front.common.show-cnt-selectbox')
                                                </div>
                                            </div>
                        </form>
                    </div>
                </div>
                @if(isset($arr_contests['data']) && sizeof($arr_contests['data'])>0)
                @foreach($arr_contests['data'] as $contest)
                <div class="white-block-bg-no-padding">
                    <div class="white-block-bg big-space hover">
                        <div class="project-left-side">
                            <div class="batches">
                                @if(isset($contest['contest_status']) && $contest['contest_status']=='0')
                                <span class="featured">{{trans('client/contest/common.text_open')}}</span>
                                @elseif(isset($contest['contest_status']) && $contest['contest_status']=='1')
                                <span class="featured">{{trans('client/contest/common.text_ending')}}</span>
                                @elseif(isset($contest['contest_status']) && $contest['contest_status']=='2')
                                <span class="featured" style="background-color:#7a7f7a">{{trans('client/contest/common.text_ended')}}</span>
                                @endif
                            </div>
                            <div class="search-freelancer-user-head">
                                <a href="{{ $module_url_path }}/details/{{ base64_encode($contest['id'])}}">{{isset($contest['contest_title'])?str_limit($contest['contest_title'],50,'...'):""}}</a>
                              
                            </div>
                            <div class="contest-inline-blocks">
                                <div class="search-freelancer-user-location">
                                    <span class="gavel-icon"><img src="{{url('/public')}}/front/images/bid.png" alt="" /></span> {{isset($contest['contest_entry'])?count($contest['contest_entry']):'0'}} {{ trans('contets_listing/listing.text_total_entries') }} <span class="freelancer-user-line">|</span>
                                </div>
                                @php $postes_time_ago = time_ago($contest['created_at']); @endphp
                                <div class="search-freelancer-user-location"> <span class="gavel-icon"><img src="{{url('/public')}}/front/images/clock.png" alt="" /></span> {{trans('project_listing/listing.text_project_posted_on')}}: {{$postes_time_ago}}</div>

                                <div class="clearfix"></div>
                                <div class="more-project-dec">{{isset($contest['contest_description'])?str_limit($contest['contest_description'],30,'...'):""}}
                                </div>

                                <div class="category-new"><img src="{{url('/public')}}/front/images/tag-2.png" alt="" /> {{isset($contest['category_details']['category_title'])?$contest['category_details']['category_title']:'NA'}} </div>
                                <div class="category-new"><img src="{{url('/public')}}/front/images/tag-2.png" alt="" /> {{isset($contest['sub_category_details']['subcategory_title'])?$contest['sub_category_details']['subcategory_title']:'NA'}} </div>
                                <div class="clearfix"></div>
                                <div class="skils-project">
                                    @if(isset($contest['contest_skills']) && sizeof($contest['contest_skills'])>0)
                                    <img src="{{url('/public')}}/front/images/tag.png" alt="" />
                                    <ul>
                                        @foreach($contest['contest_skills'] as $skill)
                                        <li>{{isset($skill['skill_data']['skill_name'])?$skill['skill_data']['skill_name']:""}}</li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <?php 
                            $from_currency = isset($contest['contest_currency'])?$contest['contest_currency']:''; 
                            $to_currency   = isset($arr_settings['default_currency_code'])?
                                              $arr_settings['default_currency_code']:'';
                            $contest_price     = isset($contest['contest_price'])?$contest['contest_price']:'0';
                            $converted_price  = currencyConverterAPI($from_currency,$to_currency,$contest_price);
                            $converted_price  = number_format(floor($converted_price));   
                        ?>

                        <div class="search-project-listing-right contest-right">
                            <div class="search-project-right-price">
                                {{$arr_settings['default_currency']}} {{$converted_price}}
                                <span>{{trans('contets_listing/listing.text_prize')}}</span>
                            </div>
                            @if($contest['winner_choose'] == 'YES')
                            <div class="search-project-right-price"><img style="cursor: default;height: 40px;" src="{{url('/public')}}/front/images/archexpertdefault/contest-winner.png"></div>
                            @else
                            <div class="search-project-right-price"><span id='days_left_timer{{$contest["id"]}}' style="margin-top:0px;">{{trans('client/projects/ongoing_projects.text_next_expire')}} </span><span>{{trans('client/contest/posted.text_time_left')}}</span></div>
                            <?php

                                $arr_diff_between_two_date = get_array_diff_between_two_date(date('Y-m-d H:i:s'),$contest['contest_end_date']);
                                $str_days_diff = isset($arr_diff_between_two_date['str_days_diff']) ? $arr_diff_between_two_date['str_days_diff'] : '';
                            ?>
                            <script type="text/javascript">
                            
                                var contestid     = '{{ isset($contest["id"]) ? $contest["id"] : '' }}';
                                var str_days_diff = '{{ isset($str_days_diff) ? $str_days_diff : '' }}';

                                if (str_days_diff == '') {
                                    $('#days_left_timer'+contestid).html('{{trans('client/projects/ongoing_projects.text_next_expire')}}');
                                    $('#days_left_timer'+contestid).css('color', 'red');
                                } else {
                                    $('#days_left_timer'+contestid).html(str_days_diff);
                                    $('#days_left_timer'+contestid).css('color', 'green');
                                    $('#days_left_timer'+contestid).css('font-size', '16px');
                                    $('#days_left_timer'+contestid).css('font-weight', 'bold');
                                }

                            </script>
                            @endif
                            <div class="clearfix"></div>
                            <a class="black-border-btn" href="{{ $module_url_path }}/details/{{ base64_encode($contest['id'])}}">{{ trans('contets_listing/listing.text_entry_details') }}</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                @endforeach
                @else
                <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;" align="center">
                    <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                        <div class="search-content-block">
                            <div class="no-record">
                                {{ trans('project_listing/listing.text_sorry_no_records_found') }}
                            </div>
                        </div>
                    </td>
                </tr>
                @endif
                <!--pagigation start here-->
                @include('front.common.pagination_view', ['paginator' => $arr_pagination])
                <!--pagigation end here-->
            </div>
        </div>
    </div>
</div>
@include('front.contest_listing.send_entry_model')
<!-- custom scrollbars plugin -->
<link href="{{url('/public')}}/front/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="{{url('/public')}}/front/js/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript">
    function textCounter(field, maxlimit) {
        var countfield = 0;
        if (field.value.length > maxlimit) {
            field.value = field.value.substring(0, maxlimit);
            return false;
        } else {
            countfield = maxlimit - field.value.length;
            var message = '{{trans('client/projects/invite_experts.text_you_have_left')}} <b>' +countfield+ '</b> {{trans('client/projects/invite_experts.text_characters_for_description')}}';
            jQuery('#cnt_desc_msg').html(message);
            return true;
        }
    }
</script>
<script>
    $('#show_posted').change(function() {
        var showedby = $(this).val();
        $('#showedby').val(showedby);
        setTimeout(function() {
            $("#frm_search").submit();
        }, 2);
    });
    $(".list_show_entry").click(function() {
        var contest_id = $(this).attr('data-contest-id');
        var client_user_id = $(this).attr('data-client-user-id');
        $("#view_type").val('lisintg');
        $("#contest_id").val(contest_id);
        $("#client_user_id").val(client_user_id);
        $("#cnst-send-entry").modal("show");

    });


    $("#show_cnt").change(function() {
        $("#frm_pagination").submit();
    });
    $('.toggle-btn').click(function() {
        $('.add-skills-check-block').slideToggle();
        $(this).find('.search-free-slills-arrow i').toggleClass('fa-angle-down fa-angle-up');
    });
</script>
<!-- custom scrollbars plugin -->
<script type="text/javascript">
    /*scrollbar start*/
    $(".content-d").mCustomScrollbar({
        scrollInertia: 98
    });
</script>
@stop