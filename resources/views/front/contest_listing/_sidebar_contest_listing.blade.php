<form action="{{$module_url_path}}/search" id="frm_search" method="any">
<input type="hidden" name="showedby" id="showedby"  @if(isset($_REQUEST['showedby']) && $_REQUEST['showedby'] != "") value="{{$_REQUEST['showedby']}}"@else value="recently-posted" @endif>
<div class="col-sm-4 col-md-3 col-lg-3">
    <div class="contest-leftbar">
        <h4>{{ trans('project_listing/listing.text_filter') }} @if(!empty($request_data)) <a style="margin: 0px 0 0;" href="{{url('/contests/search')}}" class="back-btn pull-right"><!--<i class="fa fa-reply-all"></i>--> Reset</a>@endif </h4>

        <?php
          
          $category_id = "";
          if(isset($_REQUEST['cat']) && $_REQUEST['cat'] != "") {
            $category_id = $_REQUEST['cat'];
            if(is_array($category_id))
            {
              $category_id='';
            }
          }
          //dd($category_id);

          $sub_category_id = "";
          if(isset($_REQUEST['subcategory']) && $_REQUEST['subcategory'] != "") {
            $sub_category_id = $_REQUEST['subcategory'];
          }
          
          $is_job_matching = "0";
          if(isset($_REQUEST['is_job_matching']) && $_REQUEST['is_job_matching'] != 0) {
            $is_job_matching = '1';
          }
          
          if($is_job_matching == '1'){
            $expert_section_info = [];
            if(isset($user) && $user != null && $user->inRole('expert')) {  
                $expert_section_info  = sidebar_information($user->id);
            }
          }

        ?>
        <div class="filter-block">
            <div class="white-block-bg">
                <div class="budget-section">
                    <div class="search-free-keyword-head {{-- toggle-btn --}}">{{ trans('contets_listing/listing.text_prize') }} (In US Dollar)
                        <div class="tooltip-block">
                           <div class="info-icon"><img src="{{url('/public')}}/front/images/info-icon.png" class="img-responsive" alt=""/></div>
                           <span class="tooltip-content">{{ trans('contets_listing/listing.text_gross_amount_includes_VAT_or_GST_in_contest_owner_country') }}</span>
                        </div>
                    </div>
                    <div class="range-t input-bx" for="amount">
                        <div id="slider-price-range" class="slider-rang"></div>
                        <div class="amount-no" id="slider_price_range_txt"></div>
                        <input type="hidden" id="min_contest_amt" name="min_contest_amt" value="{{isset($min_contest_amt)?$min_contest_amt:0}}">
                        <input type="hidden" id="max_contest_amt" name="max_contest_amt" value="{{isset($max_contest_amt)?$max_contest_amt:0}}">
<input type="hidden" id="is_job_matching" name="is_job_matching" value="{{isset($is_job_matching) && $is_job_matching == '1' ? '1' : '0'}}">
                        @if(isset($is_job_matching) && $is_job_matching == '1')
                          @if(isset($expert_section_info['arr_category_ids']) && count($expert_section_info['arr_category_ids'])>0)
                            @foreach( $expert_section_info['arr_category_ids'] as $category_id )
                              <input type="hidden" name="category[]" value="{{ $category_id or '' }}">
                            @endforeach
                          @endif

                          @if(isset($expert_section_info['arr_sub_category_ids']) && count($expert_section_info['arr_sub_category_ids'])>0)
                            @foreach( $expert_section_info['arr_sub_category_ids'] as $subcategory_id )
                              <input type="hidden" name="subcategory[]" value="{{ $subcategory_id or '' }}">
                            @endforeach
                          @endif

                          @if(isset($expert_section_info['arr_skills_ids']) && count($expert_section_info['arr_skills_ids'])>0)
                            @foreach( $expert_section_info['arr_skills_ids'] as $skills_id )
                              <input type="hidden" name="skills[]" value="{{ $skills_id or '' }}">
                            @endforeach
                          @endif
                        @endif
                    </div>
                </div>
            </div>
            <div class="clr"></div> 





         <div class="left-category-bx">
          
        <h2>{{trans('project_listing/listing.text_category')}}</h2>
            <div class="main-category-title">
            <ul>
                <li @if($category_id=='') class='active' @endif> <a href="{{url('/contests')}}"> All <span> ({{ isset($all_contest_count)?$all_contest_count:0 }})</span> </a> </li>

                  @if(isset($arr_sidebar_data) && sizeof($arr_sidebar_data)>0)
                  @foreach($arr_sidebar_data as $proRec)
                        <?php  $selected = ""; 
                          if(isset($category_id) && $category_id != ""){
                            if(base64_encode($proRec['category_id']) == $category_id){
                              $selected = 'active';
                            }
                          }
                        ?>
                        <li class="{{$selected}}"> <a onclick="submit_category_search(this)" data-category="{{base64_encode($proRec['category_id'])}}">{{ str_limit(isset($proRec['category_title']) ? $proRec['category_title']:'',20) }}<span> ({{ isset($proRec['contest_count']) ? $proRec['contest_count']:0 }})</span> </a> 

                          @if($selected!='')
                            @if(isset($arr_subcategory) && count($arr_subcategory) > 0)
                            <?php $cnt=0; 
                              $searchVal = [];
                              if(!empty($_REQUEST['subcategory'])){
                                $searchVal = $_GET['subcategory'];
                              }
                              if(!empty(\Request::segment(2)) && \Request::segment(2) == 'subcategory' && !empty(\Request::segment(3)) && \Request::segment(3) != ''){
                                array_push($searchVal, \Request::segment(3));
                              }
                            ?>
                            @php $cnt=0;  @endphp
                              @foreach($arr_subcategory as $subcategory)
                                @if($cnt <= 6) 
                                  <div class="check-box cate_chk">
                                      <p>
                                          <input value="<?php echo base64_encode($subcategory['id']); ?>" class="filled-in subcategory" name="subcategory[]" id="skill{{$subcategory['id']}}" type="checkbox" <?php if(isset($searchVal) &&  in_array(base64_encode($subcategory['id']), $searchVal)) { echo 'checked'; } ?> >
                                          <label for="skill{{$subcategory['id']}}">{{str_limit(isset($subcategory['subcategory_title'])?$subcategory['subcategory_title']:'',20)}}</label>
                                      </p>
                                  </div>
                                @endif
                                 @if($cnt > 6) 
                                    <div class="check-box cate_chk view-more-content-skill"  <?php if(isset($searchVal) &&  in_array(base64_encode($subcategory['id']), $searchVal)) {  } else { echo 'style="display:none"';} ?>>
                                        <p>
                                            <input value="<?php echo base64_encode($subcategory['id']); ?>" class="filled-in subcategory" name="subcategory[]" id="skill{{$subcategory['id']}}" type="checkbox" <?php if(isset($searchVal) &&  in_array(base64_encode($subcategory['id']), $searchVal)) { echo 'checked'; } ?> >
                                            <label for="skill{{$subcategory['id']}}">{{str_limit(isset($subcategory['subcategory_title'])?$subcategory['subcategory_title']:'',20)}}</label>
                                        </p>
                                    </div>
                                 @endif 
                                 @php $cnt++; @endphp     
                              @endforeach    
                              <?php if($cnt > 6){ 
                                    echo '<a class="read-more more-btn-block-skill" href="javascript:void(0);">More..</a>
                                          <a href="javascript:void(0);" class="read-more hide-btn-block-skill" style="display:none">Hide</a>
                                          <div class="clr"></div>';
                              } ?>  
                            
                            @endif
                            @endif

                       </li>
                  @endforeach
                @endif  
                </ul>
            </div>
        </div>
        @if(isset($is_job_matching) && $is_job_matching == '0')
          <input type="hidden" name="cat" id="category" value="{{isset($category_id)?$category_id:0}}">
        @endif      
        <div class="clearfix"></div> 

            <!-- <div class="faq-section">
                <button type="submit" class="black-border-btn">Search</button>
            </div>  -->

            <div class="faq-section">
                <div class="search-free-keyword-head">{{ trans('contets_listing/listing.text_FAQ') }}</div>
                <div class="faq-list-section">
                   <h4>{{ trans('contets_listing/listing.text_how_to_start_contest') }}</h4>
                    <ul>
                        <li><i class="fa fa-check"></i> <span>{{ trans('contets_listing/listing.text_sign_in') }}</span></li>
                        <li><i class="fa fa-check"></i> <span>{{ trans('contets_listing/listing.text_complete_your_profile') }}</span></li>
                        <li><i class="fa fa-check"></i> <span>{{ trans('contets_listing/listing.text_click_on_start_contest') }}</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<script type="text/javascript">
    $('.toggle-btn-contest-category').click(function(){
        $(this).next('.add-skills-check-block').slideToggle();
        $(this).find('.search-free-slills-arrow i').toggleClass('fa-angle-down fa-angle-up');
    })
    $(".more-btn-block-skill").click(function() {
            $(this).parent().find(".view-more-content-skill").slideDown("slow");
            $(this).hide();
            $(this).parent().find(".hide-btn-block-skill").show();
    });
    $(".hide-btn-block-skill").click(function() {
          $(this).parent().find(".view-more-content-skill").slideUp("slow");
          $(this).parent().find(".more-btn-block-skill").show();
          $(this).hide();
    });
    // $('.category-search').click(function(){
    //     setTimeout(function(){   
    //         $("#frm_search").submit();
    //     },2);
    // });

function submit_category_search(ref)
{
  var cat_id = $(ref).attr('data-category');
  $('#category').val(cat_id);
  //$('.submit_btn').click();
  $("#frm_search").submit();
}

$('.subcategory,.project_features').click(function() {
    setTimeout(function(){
      //$('.submit_btn').click();  
      $("#frm_search").submit();
    },2);   
 });
    /*price range slider*/
    $(function() {
        $("#slider-price-range").slider({
            range: true,
            min: {{isset($min_contest_amt)?$min_contest_amt:0}},
            max: parseFloat({{ $max_contest_amt }}).toFixed(2),
            //max: {{isset($max_contest_amt)?$max_contest_amt:0}},
            values: [<?php if(isset($_GET['min_contest_amt']) && $_GET['min_contest_amt'] != "") { echo $_GET['min_contest_amt']; } else { echo $min_contest_amt; } ?>, <?php if(isset($_GET['max_contest_amt']) && $_GET['max_contest_amt'] != "") { echo $_GET['max_contest_amt']; } else { echo $max_contest_amt; } ?>],
            change: function(event, ui) 
            {
                //$("#slider_price_range_txt").html("<span class='slider_price_min'>$ " + parseFloat(ui.values[0]).toFixed(2) + "</span>  <span class='slider_price_max'>$ " + parseFloat(ui.values[1]).toFixed(2) + " </span>");
                $("#slider_price_range_txt").html("<span class='slider_price_min'>$ " + ui.values[0] + "</span>  <span class='slider_price_max'>$ " + ui.values[1] + " </span>");
                $("#min_contest_amt").val(ui.values[0]);
                $("#max_contest_amt").val(ui.values[1]);
                $("#frm_search").submit();
            }
        });
        $("#slider_price_range_txt").html("<span class='slider_price_min'> $ " + $("#slider-price-range").slider("values", 0) + "</span>  <span class='slider_price_max'>$ " + $("#slider-price-range").slider("values", 1) + "</span>");

        //$("#slider_price_range_txt").html("<span class='slider_price_min'> $ " + parseFloat($("#slider-price-range").slider("values", 0)).toFixed(2) + "</span>  <span class='slider_price_max'> $ " + parseFloat($("#slider-price-range").slider("values", 1)).toFixed(2) + "</span>");
    });
</script>