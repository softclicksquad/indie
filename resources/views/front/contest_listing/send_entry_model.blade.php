
  @if($user != null) 
  @if($user->inRole('expert'))
  <!-- Model -->
  <div class="modal fade invite-member-modal" id="cnst-send-entry" role="dialog">
    <div class="modal-dialog modal-lg">
     <button type="button" id="btn_close_popup" class="close" {{-- data-dismiss="modal" --}}><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
     <div class="modal-content">
      <div class="modal-body">
       <h2>{{ trans('contets_listing/listing.text_send_entry') }}</h2>
       <div class="invite-form project-found-div">
        <div class="user-box">
         <div class="txt-edit"> <b>{{ trans('contets_listing/listing.text_title') }}</b> </div>
         <div class="user-box">
           <div class="input-name">
             <input type="hidden" id="contest_id" name="contest_id" value="{{isset($arr_contest["id"])?$arr_contest["id"]:""}}">
             <input type="hidden" id="client_user_id" name="client_user_id" value="{{isset($arr_contest["client_user_id"])?$arr_contest["client_user_id"]:""}}">
             <input type="text" class="clint-input beginningSpace_restrict" name="cnst_title" id="cnst_title" placeholder="{{ trans('contets_listing/listing.text_enter_title') }}">
             <input type="hidden" name="view_type" id="view_type" value="details">
             <div id="err_cnst_title" class="error"></div>
           </div>
         </div>
         <div class="txt-edit"> <b>{{ trans('contets_listing/listing.text_enter_your_entry_details') }}</b> </div>
         <span class="txt-edit" id="cnt_desc_msg">
          {{trans('client/projects/post.text_project_limit_description')}}
        </span>
        <div class="input-name">
          <textarea class="client-taxtarea beginningSpace_restrict" rows="5" data-rule-required="true" data-rule-maxlength="1000" data-txt_gramm_id="21e619fb-536d-f8c4-4166-76a46ac5edce" onkeyup="javascript: return textCounter(this,1000);" id="cnt_enrtry_desc" name="cnt_enrtry_desc" placeholder="{{ trans('contets_listing/listing.text_Provide_general_info_about_your_entry_e_g_what_you_can_deliver_and_when_why_you_think_you_can_do_this_contest_etc') }}"></textarea>
          <span class="error err" id="err_cnt_enrtry_desc"></span>
        </div>
        <div class="input-name">
          <div class="txt-edit"> <b>{{ trans('contets_listing/listing.text_attachment_instructions') }} :</b> </div>
          <!--image upload start-->
          <div class="fallback dfvdfvdfvdf dropzone-section" id="dropzone_image_div">
           <form action="{{url('/')}}/expert/contest/send_entry" class="dropzone" id="dropzonewidget">
            {{csrf_field()}}
            <div id="dropzone_append_id"></div>
          </form>
          <div class="post-img-note-block">
            <div class="error" id="err_image_file"></div>
            <label id="basic-error" class="validation-error-label" for="basic"></label>
            <div class="note-block">
              <b><span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span> : </b> 
              {{ trans('contets_listing/listing.text_select_atleast_one_image_to_upload_Only_jpg_png_jpeg_images_allowed_with_max_size_1mb_You_can_upload_upto_5_images_at_a_time') }}
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
      <hr>
      <div class="input-name">
        <div class="terms">
          <div class="check-box">
            <p>
             <input class="filled-in" type="checkbox" name="sealed_entry" id="sealed_entry">
             <label for="sealed_entry"></label>
           </p>
         </div>
         <div class="temsp-d">
          <span class="txt-edit"><b>{{ trans('contets_listing/listing.text_sealed_entry') }}</b></span> <small><i>( <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span> {{ trans('contets_listing/listing.text_sealed_entry_note') }} ).</i></small> 
        </div>
      </div>    
    </div>
    
    <div class="clearfix"></div>
    <div class="input-name" id="seal-entry-payment-section" style="display:none;">
      <input type="hidden" id="wall-balance" name="wall-balance" value="0" placeholder="Wallet balance">
      <input type="hidden" id="seal_entry_price" name="seal_entry_price" value="0" placeholder="Wallet balance">
      <input type="hidden" id="seal_entry_remaning_quantity" name="seal_entry_remaning_quantity" value="0">

      <div id="sealed-entry-message-section">

        @if(isset($arr_expert_sealed_entry_details['remaning_sealed_entries']) && $arr_expert_sealed_entry_details['remaning_sealed_entries'] > 0)
            <div class="note-block">
              <b> Credits available:{{ isset($arr_expert_sealed_entry_details['remaning_sealed_entries']) ? $arr_expert_sealed_entry_details['remaning_sealed_entries'] : '0' }} </b>
            </div>
            <div class="clearfix"></div>
            <small>
              <i>( <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span> 
                Your <b class="seal_entry_price">1 Sealed entry</b> quantity will be used for sealing your listing ).
              </i>
            </small>          
        @else
          
          <div class="note-block">
            <b> Credits available:{{ isset($arr_expert_sealed_entry_details['remaning_sealed_entries']) ? $arr_expert_sealed_entry_details['remaning_sealed_entries'] : '0' }} </b>
          </div>
          <div class="clearfix"></div>
          <a class="black-border-btn btn-right-mrg" href="#sealed_entry_topup" class="mp-add-btn-text add-money-but" data-keyboard="false" data-backdrop="static" data-toggle="modal">Top Up Credits</a>

        @endif
      </div>
      
      {{-- <div style="border-style:solid;margin-top:10px; padding :10px;" class="terms"> --}}
        {{-- <div class="check-box">
          <span class="txt-edit"><b>{{ trans('contets_listing/listing.text_wallet_balance') }}      : </b><span id="wallet_amount">USD 0 </span></span> 
        </div>
        <div class="clearfix"></div>
        <div class="check-box">
          <span class="txt-edit"><b>{{ trans('contets_listing/listing.text_sealed_entry_amount') }} : </b><span class="seal_entry_price"> USD 0</span></span> 
        </div>
        <div class="clearfix"></div> --}}
      {{--   <div class="temsp-d">
          <span class="txt-edit">
              <small id="sealed-entry-message-section">
                <i>( <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span> 
                  Your <b class="seal_entry_price">1 Sealed entry</b> quantity will be used for sealing your listing ).
                </i>
              </small>
              
          </span> 
        </div>
      </div>
       --}}   
      <div id="payment_method_err" class="error"></div> 
    </div>
     <hr>
    <div class="terms">
      {{-- <div class="check-box">
        <input class="filled-in required" type="checkbox" id="work-done-by-me" name="work-done-by-me" autocomplete="off">
        <label class="error" for="work-done-by-me"></label>
      </div>
      <div class="txt-edit">
        All work is done by me.
      </div>
      <div class="error" id="work-done-by-me-err"></div> --}}
        <div class="radio-buttons">
          <input class="filled-in " type="radio" value="0" name="work-done-by-me" >
          <font class="txt-edit">{{ trans('contets_listing/listing.text_all_work_was_done_by_me_or_our_company') }}</font>
        </div>
        <div class="radio-buttons">
          <input class="filled-in" type="radio" value="1" name="work-done-by-me" >
          <font class="txt-edit">{{ trans('contets_listing/listing.text_the_work_was_partially_done_by_me_or_our_company') }}</font >
        </div>
        <div class="error" id="work-done-by-me-err"></div>
        <label class="error" for="work-done-by-me"></label>
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="clearfix"></div>
  <button type="button" id="cancel_btn" style="width: 185px;" class="black-border-btn inline-btn">{{ trans('contets_listing/listing.text_cancel') }}</button>
  <button type="button" id="btn_post_images" class="black-btn inline-btn" style="height: auto;">{{ trans('contets_listing/listing.text_send_entry') }}</button>
</div>
</div>
</div>
</div>
</div>
@endif
@endif

<!-- add money model -->
<div class="modal fade invite-member-modal" id="sealed_entry_topup" role="dialog">
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <center><h4>Top Up Credits for Seal Entry</h4></center>
             </div>
             <form action="{{url('/expert/sealedentrieshistory/payment')}}" method="post" id="sealed_entry_form">
                {{csrf_field()}}
                <input type="hidden" name="currency_code" id="currency_code" value="{{ isset($arr_data_bal['currency'][0]) ? $arr_data_bal['currency'][0] : 'USD' }}">
                <input type="hidden" name="currency_symbol" id="currency_symbol" value="{{ isset($arr_data_bal['currency'][0]) ? $arr_data_bal['currency'][0] : 'USD' }}">
                <input type="hidden" name="contest_id" id="contest_id" value="{{ isset($arr_contest['id']) ? base64_encode($arr_contest['id']) : '' }}">
                <input type="hidden" name="is_json_data" id="is_json_data" value="0">
                
                <div class="invite-form inter-amt">
                  <div class="user-box">
                      <div class="p-control-label">Sealed Entry Quantity <span class="star-col-block">*</span></div>
                      <div class="droup-select">
                        <select name="sealed_entry_rates_id" id="sealed_entry_rates_id" class="droup mrns tp-margn" data-rule-required="true">
                          <option value="">How many credits would you like to get?</option>
                          @if(isset($arr_sealed_entry_rates) && sizeof($arr_sealed_entry_rates)>0)
                            @foreach($arr_sealed_entry_rates as $key => $rates)
                              <option 
                                value="{{isset($rates['id'])?$rates['id']:''}}"
                                data-amount="{{isset($rates['converted_price']) ? number_format($rates['converted_price'],2) :'0.0'}}">
                                {{isset($rates['quantity'])?$rates['quantity']:''}} - {{isset($rates['currency_code'])?$rates['currency_code']:''}} {{isset($rates['converted_price']) ? number_format($rates['converted_price'],2) :'0.0'}}
                              </option>
                            @endforeach
                          @endif
                        </select>
                        <span class='error'>{{ $errors->first('sealed_entry_rates_id') }}</span>
                        <span class='error_sealed_entry_quantity' style="color:red;"></span>
                      </div>
                  </div>

                  <div class="clearfix"></div>
                  
                  {{-- <div class="amt-info">
                    <span>Total Funds</span><span> <span class="amt-code">{{ isset($user_auth_details['currency_code']) ? $user_auth_details['currency_code'] : 'USD' }}</span> <span id="sealed_entry_total_amount"> 0.00 </span> </span>
                  </div> --}}

                    <div class="amt-info">
                      <span>Wallet Amount</span><span> <span class="amt-code">{{ isset($user_auth_details['currency_code']) ? $user_auth_details['currency_code'] : 'USD' }}</span> <span id="sealed_entry_wallet_amount"> 0.00 </span> </span>
                    </div>

                  <div class="amt-info no-border">
                    <span><b>Total Amount</b></span><span><span class="amt-code">{{ isset($user_auth_details['currency_code']) ? $user_auth_details['currency_code'] : 'USD' }}</span> <span id="sealed_entry_paid_amount"> 0.00 </span> </span>
                  </div>

                  <button type="submit" id="btn_topup_sealed_entry" class="black-btn">Pay Now</button>
               </div>
             </form>
          </div>
       </div>
    </div>
</div>
<!-- end add money model -->
<script type="text/javascript">
  
  var currency_code = '{{isset($arr_data_bal['currency'][0])?$arr_data_bal['currency'][0]:'USD'}}';
  var current_wallet_balance = '{{isset($arr_data_bal['balance'][0])?floatval($arr_data_bal['balance'][0]):0}}';
  current_wallet_balance = parseFloat(current_wallet_balance);
  
  var convert_rates_url = '{{url('/expert/sealedentrieshistory/convert_rates')}}';
  var sealedentries_payment_url = '{{url('/expert/sealedentrieshistory/payment')}}';

  $('#sealed_entry_rates_id').on('change',function(){
      if($(this).val()!=''){
        var sealed_entry_rates_id = $(this).val();  
        $.ajax({
          type: 'GET',
          data: { 'sealed_entry_rates_id' : sealed_entry_rates_id,'currency_code':currency_code },
          url:convert_rates_url,
          beforeSend:function(){
            $('#sealed_entry_total_amount').text('0.0');
            $('#sealed_entry_wallet_amount').text('0.0');
            $('#sealed_entry_paid_amount').text('0.0');
            showProcessingOverlay('');
          },
          success: function(response) {
              hideProcessingOverlay();
              if(response.status!='error'){
                if(response.is_higt_wallet_amount == '1') {
                  $('#sealed_entry_total_amount').parent().parent().hide();
                  $('#sealed_entry_wallet_amount').parent().parent().hide();
                } else {
                  $('#sealed_entry_total_amount').parent().parent().show();
                  $('#sealed_entry_wallet_amount').parent().parent().show();
                }
                $('#sealed_entry_total_amount').text(response.price);
                $('#sealed_entry_wallet_amount').text(response.wallet_amount);
                $('#sealed_entry_paid_amount').text(response.paid_amount);
              } else{
                $('#sealed_entry_total_amount').text('0.0');
                $('#sealed_entry_wallet_amount').text('0.0');
                $('#sealed_entry_paid_amount').text('0.0');
              }
            }
        });
      }
  });

  $('#btn_topup_sealed_entry').click(function() {
      if($('#sealed_entry_form').valid()){
        var selected_amount = parseFloat($("#sealed_entry_rates_id option:selected").attr('data-amount'));

        if(current_wallet_balance>=selected_amount){
          $('#is_json_data').val('1');
          $.ajax({
            type: 'POST',
            data: $('#sealed_entry_form').serialize(),
            url:sealedentries_payment_url,
            beforeSend:function(){
              showProcessingOverlay('');
            },
            success: function(response) {
                hideProcessingOverlay();
                if(response.status == 'success'){
                  $('#sealed_entry_topup').modal('toggle');
                  get_seal_entry_payment();
                  return false;
                } else {
                  alert(response.message);
                  return false;
                }
                return false; 
              }
          });

        } else {
          $('#sealed_entry_form').submit();
          return true;
        }
        return false;
      }
  });

</script>
<script type="text/javascript">
  $(document).ready(function(){
        // get payment details for seal entry
        get_seal_entry_payment();
        $('#sealed_entry').click(function(){
          if($(this).prop('checked')) {
              // something when checked
              get_seal_entry_payment();
              $('#seal-entry-payment-section').show();
            } else {
              $('#seal-entry-payment-section').hide();
              // something else when not
          }
        });

        $('#btn_post_images').click(function(){
          $("#payment_method_err").html('');
          if($('#sealed_entry').prop('checked')) {
               // check wallet balance availability
               var seal_entry_remaning_quantity = parseInt($('#seal_entry_remaning_quantity').val());
               if(seal_entry_remaning_quantity<=0){
                 $("#payment_method_err").html('Sorry, Your sealed entry quantity is not suffeciant to make transaction for seal entry.');
                 return false;
               }
             } 
        });
      });

  function get_seal_entry_payment(){
    var base_url = '{{url('/')}}';
    var token    = '{{csrf_token()}}';

    $.ajax({
      Type:"POST",
      url:base_url+'/contests/get-seal-entry-payment-details',
      data:{_token:token},
      dataType: "json",
      success:function(result){
        // console.log(result);
        $('#sealed-entry-message-section').html(result.seal_entry_html);
        $('#seal_entry_remaning_quantity').val(result.remaning_sealed_entries);
      }
    });
  }

  Dropzone.options.dropzonewidget = 
  {
    addRemoveLinks: true,
    maxFiles: 5,
    parallelUploads: 5,
    uploadMultiple: true,
    maxFilesize: 5,
    autoProcessQueue: false,
    acceptedFiles: '.jpg, .JPG, .png, .PNG, .jpeg, .JPEG,',
    dictMaxFilesExceeded: "{{ trans('contets_listing/listing.text_you_can_upload_only_5_images_at_a_time') }}",
    dictFileTooBig: "{{ trans('contets_listing/listing.text_image_is_too_large_please_upload_upto_1mb_image') }}",
    dictDefaultMessage: "{{ trans('contets_listing/listing.text_drop_your_images_here_to_upload') }}",
    success: function( file, response ) 
    {
      hideProcessingOverlay(); 
        /*console.log(response);
        return false;*/
        if(response.status=='error')
        {
          swal("",response.message,'error');
          this.removeFile(file); 
          $("#cnst_title").val();
          $('#cnt_enrtry_desc').val();
        }
        else
        {   
          if(response.view_type=='lisintg')
          {
            var msg = response.message;
            swal("",msg,'success');
            this.removeFile(file); 
            $("#cnst_title").val('');
            $('#cnt_enrtry_desc').val('');
            $("#cnst-send-entry").modal('hide');  
          }
          else
          {
            location.reload();
          }
        }
      },

      init: function() {
        var myDropzone = this;
        this.on("error", function(file, message) { 
          swal("",message,'error');
          this.removeFile(file); 

        });
        $('#btn_post_images').on("click", function() 
        {
          $('.error').html('');
          if($('#cnst_title').val()=="" && $("#cnt_enrtry_desc").val()=="" && !$("#work-done-by-me").prop('checked') && !myDropzone.files || !myDropzone.files.length)
          {
            $('#err_cnst_title').html('{{ trans('contets_listing/listing.text_this_field_is_required') }}');  
            $("#err_cnt_enrtry_desc").html('{{ trans('contets_listing/listing.text_this_field_is_required') }}'); 
            $('#err_image_file').html('{{ trans('contets_listing/listing.text_this_field_is_required') }}');
            $("#work-done-by-me-err").html('{{ trans('contets_listing/listing.text_this_field_is_required') }}');
            return false;
          }  
          else if($('#cnst_title').val()=="")
          {
            $('#err_cnst_title').html('{{ trans('contets_listing/listing.text_this_field_is_required') }}');
            return false;
          }
          else if($("#cnt_enrtry_desc").val()=="")
          {
            $('#err_cnst_title').html('');
            $("#err_cnt_enrtry_desc").html('{{ trans('contets_listing/listing.text_this_field_is_required') }}');
            return false;
          }
          else if(!$("input[name='work-done-by-me']:checked").val()){
            $("#work-done-by-me-err").html('{{ trans('contets_listing/listing.text_this_field_is_required') }}');
            return false;
          }
          else
          {
              if($('#sealed_entry').prop('checked')) {
                $("#payment_method_err").html('');
                var seal_entry_remaning_quantity = parseInt($('#seal_entry_remaning_quantity').val());
                if(seal_entry_remaning_quantity<=0){
                  $("#payment_method_err").html('Sorry, Your sealed entry quantity is not suffeciant to make transaction for seal entry.');
                  return false;
                }
              }

            if (!myDropzone.files || !myDropzone.files.length) 
            {
              $('#err_image_file').html('{{ trans('contets_listing/listing.text_this_field_is_required') }}');
              $('#err_cnst_title').html('');
              $("#err_cnt_enrtry_desc").html('');
              $('#sealed_entry').prop('checked',false); 
              return false;
            }
            else
            {
              myDropzone.processQueue();  
              $('#err_cnt_enrtry_desc').val('');
              $('#cnt_enrtry_desc').html('');             
              $('#sealed_entry').prop('checked',false); 
            }

          }
          $('#basic-error').html('');    
        });
        myDropzone.on("sending", function(file, xhr, data) {
          showProcessingOverlay();
          var seal_entry_remaning_quantity = $('#seal_entry_remaning_quantity').val();
          data.append("_token", $('meta[name="token"]').attr('content'));
          data.append("client_user_id",$("#client_user_id").val());
          data.append("contest_id",$("#contest_id").val());
          data.append("cnst_title",$("#cnst_title").val());
          // data.append("cnst_wallet_balance",wallet_balance);
          // data.append("cnst_seal_entry_price",seal_entry_price);
          data.append("seal_entry_remaning_quantity",seal_entry_remaning_quantity);
          data.append("cnt_enrtry_desc", $('#cnt_enrtry_desc').val());
          data.append("view_type",$('#view_type').val());
          if ($('#sealed_entry').is(":checked")){
            data.append("sealed_entry",'on');
          } else {
            data.append("sealed_entry",'off');
          }
          if($("input[name='work-done-by-me']:checked").val()){
            var work_done_by_me = $("input[name='work-done-by-me']:checked").val();
            data.append("work_done_by_me",work_done_by_me);
          }
        });
      }
    };
    function makeStatusMessageHtml(status, message){
      str = '<div class="alert alert-'+status+' alert-dismissable"><a class="close" data-dismiss="alert" aria-label="close">×</a><strong>'+status+' :</strong>'+message+'</div>';
      return str;
    }
    $("#btn_close_popup,#cancel_btn").on('click',function(e)
    {
      $("#cnst_title").val('');
      $('#cnt_enrtry_desc').val('');
      $('#err_cnst_title').html('');  
      $("#err_cnt_enrtry_desc").html(''); 
      $('#err_image_file').html('');
      var myDropzone = Dropzone.forElement("#dropzonewidget");
      myDropzone.removeAllFiles(true); 
      $("#cnst-send-entry").modal('hide');
      $('#sealed_entry').prop('checked',false); 
    });
  </script>