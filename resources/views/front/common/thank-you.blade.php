@extends('front.layout.auth')                
@section('main_content')

<div class="login-section">
   <div class="container">
      <div class="" style=" min-height: 400px;">
         <div class="col-lg-12">
            <div class="thank-you">
           
           <div class="tnk-txts">
            <h1> {{trans('common/success_page.text_thank_you')}}</h1>
            {{ trans('new_translations.you_have_subscribed')}} {{config('app.config.project.name')}} {{ trans('new_translations.newsletter_successfully')}}.
          {{--   We need to confirm your email address.
            <br/>To complete the subscription process, please click the link in the email we just sent you. --}}
          </div>
          <div class="clr"></div>
          <a href="{{url('/')}}" class="job-btn" style="max-width: 308px;">{{ trans('new_translations.go_to_website')}}</a>
            </div>
            <div class="clearfix"></div>
         </div>

         <div class="clearfix"></div>
      </div>
   </div>
</div>
 <script type="text/javascript">
 
  (function (global, $) {

    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";
        global.setTimeout(function () 
        {
            global.location.href += "!";
        }, 50);
    };

    global.onhashchange = function () {
        if (global.location.hash != _hash) {
            global.location.hash = _hash;
        }
    };

    global.onload = function () 
    {
        noBackPlease();
        // disables backspace on page except on input fields and textarea..
        $(document.body).keydown(function (e) {
            var elm = e.target.nodeName.toLowerCase();
            if (e.which == 8 && (elm !== 'input' && elm  !== 'textarea')) 
            {
                e.preventDefault();
            }
            // stopping event bubbling up the DOM tree..
            e.stopPropagation();
        });
    };
})(window, jQuery || window.jQuery);

  </script>
@stop