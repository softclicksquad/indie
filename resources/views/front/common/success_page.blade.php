@extends('front.layout.auth')                
@section('main_content')
<div class="login-section thank-you-wrapper">
      <div class="thank-you">
          <div class="suuccess-img"><img src="{{url('/public')}}/front/images/success-icon.png" class="img-responsive" alt=""/></div>
           <h1> {{trans('common/success_page.text_thank_you')}}</h1>
            @if(Session::has('payment_success_msg'))
                <p>{{ Session::get('payment_success_msg') }}</p>
                @if(Session::has('payment_return_url'))
                <div class="clr"></div>
                  <a href="{{url(Session::get('payment_return_url'))}}" class="job-btn" style="max-width: 180px;">
                     @if(Session::get('payment_return_url')=='/expert/profile')
                     {{ trans('new_translations.link_go_to_profile')}}
                     @else
                     {{ trans('new_translations.go_to_website')}}
                     @endif
                  </a>
                @endif
            @else
            <p>{{trans('common/success_page.text_payment_transaction_successful')}}</p>    
            @endif
      </div>
    <div class="clearfix"></div>
</div>
<script type="text/javascript">
(function (global, $) {
  var _hash = "!";
  var noBackPlease = function () {
    global.location.href += "#";
    global.setTimeout(function () 
    {
      global.location.href += "!";
    }, 50);
  };
  global.onhashchange = function () {
    if (global.location.hash != _hash) {
      global.location.hash = _hash;
    }
  };
  global.onload = function () 
  {
    noBackPlease();
// disables backspace on page except on input fields and textarea..
$(document.body).keydown(function (e) {
  var elm = e.target.nodeName.toLowerCase();
  if (e.which == 8 && (elm !== 'input' && elm  !== 'textarea')) 
  {
    e.preventDefault();
  }
// stopping event bubbling up the DOM tree..
e.stopPropagation();
});
};
})(window, jQuery || window.jQuery);
</script>
@stop