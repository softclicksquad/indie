@extends('front.layout.auth')                
@section('main_content')
<style type="text/css">
.thank-you { text-align: center; color: #FFFFFF; font-size: 18px;}
.thank-you h1 { font-size: 90px; text-align: center; }
.thank-you .message { margin-top: 30px; }
</style>
<div class="login-section">
  <div class="container">
    <div class="" style=" min-height: 400px;">
      <div class="col-lg-12">
        <div class="thank-you">
          <h1> {{trans('common/cancel_page.text_sorry')}}</h1>
          <div class="thank-you message">
              @if(Session::has('payment_error_msg'))
              <p> {{ Session::get('payment_error_msg') }}</p>
              @else
              {{trans('common/cancel_page.text_payment_transaction_sorry')}}
              @endif
              <?php
                $user = Sentinel::check();
                $path = FALSE;
                if($user){
                  if (isset($user) && $user->inRole('client')) {
                    $path = "client"; 
                  } elseif (isset($user) && $user->inRole('expert'))  {
                    $path = "expert"; 
                  }
                }
              ?>
              <br/>
              @if(isset($user) && isset($path) && $path!=FALSE)
              <a href="{{url('/')}}/{{$path}}/dashboard">
                <button class="job-btn" type="button" style="max-width: 180px;">{{ trans('new_translations.go_to_website')}}</button>
              </a>
              @endif
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</div>
<script type="text/javascript">
(function (global, $) {
  var _hash = "!";
  var noBackPlease = function () {
    global.location.href += "#";
    global.setTimeout(function () 
    {
      global.location.href += "!";
    }, 50);
  };
  global.onhashchange = function () {
    if (global.location.hash != _hash) {
      global.location.hash = _hash;
    }
  };
  global.onload = function () 
  {
    noBackPlease();
    $(document.body).keydown(function (e) {
      var elm = e.target.nodeName.toLowerCase();
      if (e.which == 8 && (elm !== 'input' && elm  !== 'textarea')) 
      {
        e.preventDefault();
      }
      e.stopPropagation();
    });
  };
})(window, jQuery || window.jQuery);
</script>
@stop