<link rel="stylesheet" href="{{url('/public')}}/front/css/gallery.css" type="text/css" />
<link rel="stylesheet" href="{{url('/public')}}/front/css/lightgallery.css" type="text/css" />
<script type="text/javascript" src="{{url('/public')}}/front/js/gallery.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/front/js/lightgallery.js"></script>
<?php $module_url_path = url('/expert/projects'); ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" integrity="sha256-Z8TW+REiUm9zSQMGZH4bfZi52VJgMqETCbPFlGRB1P8=" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.js" integrity="sha256-ZvMf9li0M5GGriGUEKn1g6lLwnj5u+ENqCbLM5ItjQ0=" crossorigin="anonymous"></script>

{{-- <link rel="stylesheet" href="{{url('/public')}}/front/css/lightgallery.css" type="text/css" /> --}}

<?php 
    $user = Sentinel::check();
    //$user->inRole('client')
    //$user->inRole('expert')
?>

<div class="search-grey-bx white-wrapper">
    <div class="ongonig-project-section">
        <div class="upload-btn p-relative pull-right">
            @if(isset($projectInfo['project_status']) && 
                $projectInfo['project_status'] == '4' || 
                isset($projectInfo['project_status']) && 
                $projectInfo['project_status'] == '3')
                @if($user->inRole('client'))            
                @elseif($user->inRole('expert'))
                @endif
                @endif
            </div>
            <div class="dispute-head">
                {{$projectInfo['project_name'] or ''}}
                <div class="batches">

                    @if(isset($projectInfo['highlight_end_date'])       && 
                        $projectInfo['highlight_end_date'] !=""   && 
                        $projectInfo['highlight_end_date'] !=null && 
                        $projectInfo['highlight_end_date'] !="0000-00-00 00:00:00" && 
                        date('Y-m-d', strtotime($projectInfo['highlight_end_date'])) >= date('Y-m-d') )
                        <span class="private hightlight">Highlighted</span>
                    @endif

                    @if(isset($projectInfo['urjent_heighlight_end_date'])       && 
                        $projectInfo['urjent_heighlight_end_date'] !=""   && 
                        $projectInfo['urjent_heighlight_end_date'] !=null && 
                        $projectInfo['urjent_heighlight_end_date'] !="0000-00-00 00:00:00" && 
                        date('Y-m-d', strtotime($projectInfo['urjent_heighlight_end_date'])) >= date('Y-m-d') )
                        <span class="private hightlight">Urgent</span>
                    @endif

                    @if(isset($projectInfo['is_nda']) && $projectInfo['is_nda'] == 1)
                        <span class="private nda">NDA</span>
                    @endif

                    @if(isset($projectInfo['project_type']) && $projectInfo['project_type'] == 1)
                        <span class="private pri-div">Private</span>
                    @endif
                </div>

                </div>
                <div class="clearfix"></div>        
                @include('front.layout._operation_status')
                <div class="project-user-details">
                    <div class="pro-title">
                        <?php if($user !=null &&  isset($projectInfo['client_details']['role_info']['first_name']) && $projectInfo['client_details']['role_info']['first_name']!= ""){ ?> <a href="{{url('/clients/portfolio/'.base64_encode($projectInfo['client_details']['id']))}}"><?php } ?>
                            {{isset($projectInfo['client_details']['role_info']['first_name'])?$projectInfo['client_details']['role_info']['first_name']:'Unknown user'}} 
                            @if(isset($projectInfo['client_details']['role_info']['last_name']) && $projectInfo['client_details']['role_info']['last_name'] !="") @php echo substr($projectInfo['client_details']['role_info']['last_name'],0,1).'.'; @endphp @endif
                            <?php if($user !=null &&  isset($projectInfo['client_details']['role_info']['first_name']) && $projectInfo['client_details']['role_info']['first_name']!= ""){ ?> </a><?php } ?>
                        </div>
                        @if(isset($user) && $user != null)
                            @if(isset($projectInfo['project_handle_by']) && $projectInfo['project_handle_by']=='2' && $user->inRole('recruiter') && $projectInfo['project_recruiter_user_id'] == $user->id)
                                <div class="clearfix"></div>
                            <div class="invite-expert-btns">
                                @if(isset($user) && $user != null)
                                @if($user->inRole('recruiter'))
                                    @php 
                                        $arr_clent_data = sidebar_information($projectInfo['client_user_id']); 
                                    @endphp
                                <a href="javascript:void(0)" style="margin-top:5px;" class="applozic-launcher" data-mck-id="{{env('applozicUserIdPrefix')}}{{$projectInfo['client_user_id']}}" data-mck-name="{{isset($arr_clent_data['user_details']['first_name'])?$arr_clent_data['user_details']['first_name']:'Unknown user'}} 
                                @if(isset($arr_clent_data['user_details']['last_name']) && $arr_clent_data['user_details']['last_name'] !="") @php echo substr($arr_clent_data['user_details']['last_name'],0,1).'.'; @endphp @endif" ><i class="fa fa-comment-o"></i> {{trans('client/projects/project_details.message')}}</a>
                                @endif
                                @endif
                            </div>
                            @endif 
                        @endif
                        <div class="pro-title">
                            <div class="rate-t">
                                <div class="rating-list">
                                    <div class="rating-list">
                                        <span class="stars">{{isset($arr_profile_data['average_rating'])? $arr_profile_data['average_rating'] :0  }}</span>
                                    </div>
                                </div><span>(&nbsp;{{ $arr_profile_data['average_rating'] or '0' }}&nbsp;)</span>
                                <span class="revie-txt-block">&nbsp;(&nbsp;{{ $arr_profile_data['review_count'] or '0' }}&nbsp;{{trans('common/_top_project_details.text_reviews')}}&nbsp;)</span>
                            </div>
                        </div>
                
                        <div class="category-det">
                            <p><b>{{trans('common/_top_project_details.text_category')}} :</b> {{$projectInfo['category_details']['category_title'] or 'NA'}}</p>
                            <span>|</span>
                            <p><b>{{trans('common/_top_project_details.text_sub_category')}} :</b> {{$projectInfo['sub_category_details']['subcategory_title'] or 'NA'}}</p>
                            <span>|</span> 
                            {{-- Recruiter --}}
                                @if(isset($projectInfo['project_handle_by']) && $projectInfo['project_handle_by'] == '1' )
                                    <p>
                                        <b>{{trans('common/_top_project_details.text_recruite_by')}} :</b>
                                        <?php 
                                        $handle_by = trans('common/_top_project_details.text_handled_by_client');
                                        if($user != FALSE){
                                            if($user->id == $projectInfo['client_user_id'] ){
                                                $handle_by = trans('common/_top_project_details.text_handled_by_myself');
                                            }
                                        }
                                        ?>
                                        {{$handle_by}}
                                    </p>

                                @elseif(isset($projectInfo['project_handle_by']) && $projectInfo['project_handle_by'] == '2' )
                                    <p>
                                        <b>{{trans('common/_top_project_details.text_recruite_by')}} :</b>
                                        {{trans('common/_top_project_details.text_handled_by_Recruiter')}}
                                        @if(isset($user) && $user != null)
                                        @if($user->inRole('client'))
                                            @php 
                                                $arr_manager_data = sidebar_information($projectInfo['project_recruiter_user_id']); 
                                            @endphp

                                            @if(isset($projectInfo['project_recruiter_user_id']) && $projectInfo['project_recruiter_user_id']!='')
                                                <a href="#" class="applozic-launcher" data-mck-id="{{env('applozicUserIdPrefix')}}{{$projectInfo['project_recruiter_user_id']}}" data-mck-name="{{isset($arr_manager_data['user_details']['first_name'])?$arr_manager_data['user_details']['first_name']:'Unknown user'}}   @if(isset($arr_manager_data['user_details']['last_name']) && $arr_manager_data['user_details']['last_name'] !="") 
                                            @php echo substr($arr_manager_data['user_details']['last_name'],0,1).'.'; @endphp @endif" >
                                            <i class="fa fa-comment-o"></i> {{trans('client/projects/project_details.message')}}
                                        </a>
                                        @endif

                                        @endif
                                        @endif
                                    </p>
                                @endif
                            {{--  End Recruiter --}}
                            <span>|</span>
                            {{--Project manager--}}
                                @if(isset($projectInfo['project_handle_by_pm']) && $projectInfo['project_handle_by_pm'] == '0' )
                                    <p>
                                        <b>{{trans('common/_top_project_details.text_handled_by')}} :</b>
                                        <?php 
                                        $handle_by = trans('common/_top_project_details.text_handled_by_client');
                                        if($user != FALSE){
                                            if($user->id == $projectInfo['client_user_id'] ){
                                                $handle_by = trans('common/_top_project_details.text_handled_by_myself');
                                            }
                                        }
                                        ?>
                                        {{$handle_by}}
                                    </p>
                                @elseif(isset($projectInfo['project_handle_by_pm']) && $projectInfo['project_handle_by_pm'] == '1' )
                                   
                                @endif
                            {{-- End Project manager--}}
                        </div>
                        <input type="hidden" readonly="" name="project_id" value="{{isset($projectInfo['id'])?base64_encode($projectInfo['id']):''}}"/> 
                        <?php
                        $arr_profile_data = [];
                        if(isset($projectInfo['client_user_id']) && $projectInfo['client_user_id'] != ""){
                            $arr_profile_data = sidebar_information($projectInfo['client_user_id']);
                        }
                        ?>
                    </div>
                        
                    @if(isset($projectInfo['project_skills']) && count($projectInfo['project_skills']) > 0)
                        <div class="skils-project tp-kil-hov">
                                <img src="{{url('/public')}}/front/images/tag.png" alt="" />
                            <ul>
                                 @foreach($projectInfo['project_skills'] as $key=>$skills) @if(isset($skills['skill_data']['skill_name']) && $skills['skill_data']['skill_name'] != "")
                                <li>{{ str_limit($skills['skill_data']['skill_name'],25) }}</li>
                                @endif @endforeach 
                            </ul>
                        </div>
                    @endif
                   
                    <div class="det-divider"></div>
                    <div class="project-title">
                        <div class="pro-title project-discription">{{trans('common/_top_project_details.text_project_description')}}</div>
                        <div class="more-project-dec" style="height:auto;">{{$projectInfo['project_description'] or ''}}</div>
                    </div>
                    
                    @if(isset($projectInfo['project_additional_info']) && $projectInfo['project_additional_info'] != '')
                    <div class="det-divider"></div>
                    <div class="project-title">
                        <div class="pro-title project-discription">{{trans('common/_top_project_details.text_project_additional_info')}}</div>
                        <div class="more-project-dec" style="height:auto;">{{$projectInfo['project_additional_info'] or ''}}</div>
                    </div>
                    @endif
                    <div class="clr"></div>
                    <div class="det-divider"></div>
                    <div class="project-list mile-list nw-bold project-details-list bid-action-buttons job-details-section-chage">
                        <ul>
                            <?php 
                            $text_hour = '';
                            if(isset($projectInfo['project_pricing_method']) && $projectInfo['project_pricing_method'] == '2'){
                                $rate_title = trans('common/_top_project_details.text_hourly_rate');
                                $text_hour = trans('common/_top_project_details.text_hour');
                            } elseif(isset($projectInfo['project_pricing_method']) && $projectInfo['project_pricing_method'] == '1') {
                                $rate_title = trans('common/_top_project_details.text_budget') ;
                            }

                            $projects_cost = explode('-',$projectInfo['project_cost']);
                            if( (isset($projects_cost[0]) && $projects_cost[0]!='') && (isset($projects_cost[1]) && $projects_cost[1]!=''))
                            {
                                $projects_cost = number_format(floor($projects_cost[0])).'-'.number_format(floor($projects_cost[1]));
                            }
                            elseif($projects_cost[0]!='')
                            {
                                $projects_cost = number_format(floor($projects_cost[0]));
                            }

                            ?>
                            <li>
                                <span class="projrct-prce new-proct">
                                    @if(isset($projectInfo['project_currency']) && $projectInfo['project_currency']=="$")
                                        {{$projectInfo['project_currency_code']}} {{ $projects_cost }}{{$text_hour}}
                                    @else
                                        {{isset($projectInfo['project_currency_code'])?$projectInfo['project_currency_code']:''}} {{ $projects_cost }}{{$text_hour}}
                                    @endif 
                                </span>

                                @if(isset($user) && $user != null)
                                    @if($user->inRole('expert'))
                                        @if($projectInfo['project_currency_code'] != $user->currency_code)
                                            @if(isset($projectInfo['is_custom_price']) && $projectInfo['is_custom_price']==0)
                                                @if($projectInfo['min_project_cost'] == $projectInfo['max_project_cost'])
                                                    ({{ $user->currency_code ?? '' }} {{ number_format(floor(currencyConverterAPI($projectInfo['project_currency_code'],$user->currency_code,$projectInfo['min_project_cost']))) }}{{$text_hour}})
                                                @else
                                                    ({{ $user->currency_code ?? '' }} {{ number_format(floor(currencyConverterAPI($projectInfo['project_currency_code'],$user->currency_code,$projectInfo['min_project_cost']))) }}-{{ number_format(floor(currencyConverterAPI($projectInfo['project_currency_code'],$user->currency_code,$projectInfo['max_project_cost']))) }}{{$text_hour}})
                                                @endif
                                                
                                            @else
                                                ({{ $user->currency_code ?? '' }} {{ number_format(floor(currencyConverterAPI($projectInfo['project_currency_code'],$user->currency_code,$projectInfo['project_cost']))) }})
                                            @endif
                                        @endif
                                    @endif
                                @endif

                                <div class="frm-nm">{{ isset($rate_title)?$rate_title:'' }}</div>
                            </li>

                            <li>
                                <span class="projrct-prce new-proct" title="Expected Duration">
                                {{isset($projectInfo['project_expected_duration'])?$projectInfo['project_expected_duration']:''}}
                                </span>
                                <div class="frm-nm">Duration</div>
                            </li>
                            
                            @if(isset($projectInfo['project_post_documents']) && count($projectInfo['project_post_documents'])>0)
                            <div class="attachments-block">

                                <?php 
                                    $download_file = 'download';
                                    if(isset($user) && $user != null)
                                    {

                                    }
                                    else
                                    {
                                        $download_file = '';
                                    }
                                ?>

                                @if(isset($projectInfo['project_post_documents']) && count($projectInfo['project_post_documents'])>0)
                                    <div style="font-size: 18px; font-family: 'robotomedium'; color: #555;">{{ trans('contets_listing/listing.text_attachments') }} : 
                                       
                                    @if(isset($user) && $user!=null)
                                        @if($user->inRole('expert') || $user->inRole('recruiter') || $user->inRole('project_manager') || $user->id == $projectInfo['client_user_id'])
                                            <a href="{{url('/projects/details/make_zip/')}}/{{base64_encode($projectInfo['id'])}}" style="font-size: 17px;float: right;" class="close-overlay">
                                                <i class="fa fa-file-archive-o right"></i> {{trans("new_translations.text_download_all_files")}}
                                            </a> </div><br>
                                        
                                        
                                    @else
                                      <br><i><p class="contest-listing-note-section" style="color: red; font-size: 15px;">{{trans('contets_listing/listing.text_please_login_as_expert_to_download_and_see_original_attachment')}}</p></i>
                                    @endif
                                @else
                                    <br><i><p class="contest-listing-note-section" style="color: red; font-size: 15px;">{{trans('contets_listing/listing.text_please_login_as_expert_to_download_and_see_original_attachment')}}</p></i>
                                @endif
                                @endif

                            </div>
                            @endif
                             @if(isset($user) && $user!=null)
                             @if($user->inRole('expert') || $user->inRole('recruiter') || $user->inRole('project_manager') || $user->id == $projectInfo['client_user_id'])
                                    
                                     <div class="project-list mile-list nw-bold">
                                      <div class="demo-gallery dark mrb35 new-add-photo">
                                      <ul class="fixed-size list-unstyled">
                                      @if(isset($projectInfo['project_post_documents']) && !empty($projectInfo['project_post_documents']))
                                      @foreach($projectInfo['project_post_documents'] as $data)
                                      <?php 
                                      $contest_attachment     = isset($data['image_name'])?$data['image_name']:"";
                                      $get_attatchment_format = get_attatchment_format($contest_attachment);
                                      $arr_image_formates     = ['jpg','png','jpeg','PNG','JPEG','JPG','gif','GIF'];
                                      ?>
                                      @if(isset($get_attatchment_format) && $get_attatchment_format != '' && !in_array($get_attatchment_format,$arr_image_formates))
                                      @if(file_exists(base_path().'/public/front/images/file_formats/'.$get_attatchment_format.'.png'))
                                           <li data-src="{{ url('/')}}/front/images/file_formats/{{$get_attatchment_format}}.png">
                                              <a href="javascript:void(0);">
                                                  <img class="img-responsive" src="{{url('/public')}}/front/images/file_formats/{{$get_attatchment_format}}.png" alt="pre">
                                                  <div class="demo-gallery-poster">
                                                   <img src="{{url('/public')}}/front/images/zoom.png"  class="img-responsive" alt=""/>
                                                  </div>
                                              </a>
                                           </li>
                                      @else
                                       <li data-src="{{ url('/')}}/front/images/file_formats/Default.png">
                                              <a href="javascript:void(0);">
                                                  <img class="img-responsive" src="{{url('/public')}}/front/images/file_formats/Default.png" alt="pre">
                                                  <div class="demo-gallery-poster">
                                                   <img src="{{url('/public')}}/front/images/zoom.png"  class="img-responsive" alt=""/>
                                                  </div>
                                              </a>
                                           </li>
                                      @endif
                                      @else
                                        <li data-src="{{url('/public')}}{{config('app.project.img_path.project_attachment')}}{{$data['image_name']}}">
                                          <a href="javascript:void(0);">
                                              <img class="img-responsive" src="{{url('/public')}}{{config('app.project.img_path.project_attachment')}}{{$data['image_name']}}" alt="pre">
                                              <div class="demo-gallery-poster">
                                               <img src="{{url('/public')}}/front/images/zoom.png"  class="img-responsive" alt=""/>
                                              </div>
                                          </a>
                                        </li>
                                       @endif
                                       @endforeach
                                       @endif

                                       </ul>
                                    <div class="clearfix"></div>
                                    </div>
                                    <div class="file-download">
                                      <ul>
                                         @if(isset($projectInfo['project_post_documents']) && count($projectInfo['project_post_documents'])>0)
                                          @foreach($projectInfo['project_post_documents'] as $data)
                                                 <li >
                                                    <div class="down-arrow">
                                                        <a {{$download_file}} href="{{url('/public')}}{{config('app.project.img_path.project_attachment')}}{{$data['image_name']}}"
                                                            style="display: block;position: relative;text-align: center;"><i class="fa fa-cloud-download"></i></a>
                                                    </div>
                                                 </li>
                                             @endforeach
                                             @endif
                                      </ul>
                                    </div>
                                  </div>
                    @endif
                    @endif
                    @php

                            $is_sent = 0;
                            $is_sent = find_key_value_in_all_array($projectInfo['project_bids_infos'],'bid_status','4');
                            @endphp

                            @if($user != null) 
                            @if($user->inRole('client') && $projectInfo['project_status'] == 1 || $projectInfo['project_status'] == 2) 
                            @php 
                            $timeFirst     = strtotime(date('Y-m-d H:i:s'));
                            $timeSecond    = strtotime($projectInfo['bid_closing_date']);
                            $diffInSeconds = $timeSecond - $timeFirst;
                            @endphp
                            @if($user != null && $user->inRole('client') || $user->inRole('recruiter')) 
                            @if($projectInfo['is_hire_process'] == 'NO')
                            @if($diffInSeconds >'0')
                            @if($projectInfo['project_handle_by']=='1' && $user->inRole('client') && $projectInfo['client_user_id'] == $user->id)
                                @if(isset($is_sent) && $is_sent == '0')
                                    <div class="clearfix"></div>
                                    <div class="invite-expert-btns">
                                        <li style="background:none; padding:0px;width: 180px;">
                                            <a href="#invite_member" style="margin: 1px 0 0 0;" data-keyboard="false" data-backdrop="static" data-project-id="{{ base64_encode($projectInfo['id'])}}" data-toggle="modal" class="black-btn invite-expert">{{trans('client/projects/invite_experts.text_invite_expert')}}</a>
                                        </li>
                                    </div>
                                @endif
                            @elseif($projectInfo['project_handle_by']=='2' && $user->inRole('recruiter') && $projectInfo['project_recruiter_user_id'] == $user->id)
                            @if(isset($is_sent) && $is_sent == '0')
                                <div class="clearfix"></div>
                            <div class="invite-expert-btns">
                                <li style="background:none; padding:0px;width: 180px;">
                                    <a href="#invite_member" style="margin: 1px 0 0 0;" data-keyboard="false" data-backdrop="static" data-project-id="{{ base64_encode($projectInfo['id'])}}" data-toggle="modal" class="black-btn invite-expert">{{trans('client/projects/invite_experts.text_invite_expert')}}</a>
                                </li>
                            </div>
                            @endif
                            @endif
                            @endif 
                            @endif
                            @endif
                            @endif 
                            @endif
                            <?php if(isset($projectInfo['client_details']['role_info']['email']) && $projectInfo['client_details']['role_info']['email']!= "" || isset($projectInfo['client_details']['email']) && $projectInfo['client_details']['email'] !=""){ ?> 
                            @if($user != FALSE && $user->inRole('client') && $projectInfo['project_status']=='5' && \Request::segment(1) == "client" )
                            <a class="view_btn pull-right" href="{{url('/')}}/client/projects/post/{{ base64_encode($projectInfo['id'])}}">
                                {{trans('client/projects/canceled_projects.text_post_again')}} 
                            </a>
                            @endif
                        </ul>
                        <?php
                        $is_bids_page = FALSE;
                        if(Request::segment(2) == 'bids') {
                            $is_bids_page = TRUE;
                        }
                        $is_bid_exists = FALSE;
                        if(isset($projectInfo['is_bid_exists']) && $projectInfo['is_bid_exists'] == '1'){
                            $is_bid_exists = TRUE;
                        }
                        ?>
                        @php
                        $timeFirst      = strtotime(date('Y-m-d H:i:s'));
                        $timeSecond     = strtotime($projectInfo['bid_closing_date']);
                        $diffInSeconds = $timeSecond - $timeFirst;
                        if($diffInSeconds <= 0){
                            $diffInSeconds = '0';
                        }
                        @endphp
                        @if($diffInSeconds > 0)
                        @if($is_bid_exists == TRUE)
                        @elseif( $user!= FALSE && $user->inRole('expert') && $is_bids_page == FALSE && $is_bid_exists == FALSE && $projectInfo['project_status']!='5' && $projectInfo['project_status']!='3') &nbsp;
                        <a href="{{url('/')}}/expert/bids/add/{{base64_encode($projectInfo['id'])}}" class="view_btn hidden-xs hidden-sm hidden-md"><i class="fa fa-money" aria-hidden="true"></i> {{trans('common/_top_project_details.text_apply')}} </a> @elseif(!$user && $is_bids_page == FALSE && $is_bid_exists == FALSE && $projectInfo['project_status']!='5' && $projectInfo['project_status']!='3') &nbsp;
                        <a href="{{ url('/login') }}" class="view_btn hidden-xs hidden-sm hidden-md"><i class="fa fa-money" aria-hidden="true"></i> {{trans('common/_top_project_details.text_apply')}} </a> 
                        @endif 
                        @endif
    
                        @if($is_bid_exists == TRUE)

                            @php
                                $is_award_request_received = project_award_request($user->id,$projectInfo['id']);

                            @endphp
                           {{--  <a href="javascript:void(0)" class="view_btn hidden-xs hidden-sm hidden-md active-green"><i class="fa fa-check-circle" aria-hidden="true"></i> &nbsp;Bid Placed</a> --}}

                            <div class="search-project-listing-left left-btns">

                                @if(isset($projectInfo['is_project_rejected_by_expert']) && $projectInfo['is_project_rejected_by_expert']=='1')
                                    <a href="javascript:void(0)" class="black-border-btn">
                                       {{trans('project_listing/listing.text_project_rejected')}}
                                    </a>
                                @elseif(isset($is_award_request_received) && $is_award_request_received=='1')
                                    @if(isset($projectInfo['expert_user_id']) && $projectInfo['expert_user_id']=='' && Request::Segment(3)!='bid')
                                        <a class="black-border-btn active-green" onclick="confirmaccept(this)" data-project-id="{{ isset($projectInfo['id'])?base64_encode($projectInfo['id']):'' }}"><span class=""> {{trans('expert/projects/awarded_projects.text_accept')}} </span>
                                        </a>

                                        <a class="black-btn" onclick="rejectProject(this)" data-project-id="{{ isset($projectInfo['id'])?base64_encode($projectInfo['id']):'' }}"><span class=""> {{trans('expert/projects/awarded_projects.text_reject')}} </span>
                                        </a>
                                    @endif
                                @endif
                                
                                @if(isset($arr_bid_data['is_chat_initiated']) &&  $arr_bid_data['is_chat_initiated'] == "1")
                                  <a href="{{url('/expert/twilio-chat/projectbid')}}/{{base64_encode($arr_bid_data['id'])}}" class="applozic-launcher">
                                      <button class="black-btn" >{{trans('client/projects/project_details.message')}}</button>
                                  </a>
                                @endif

                                @if($projectInfo['project_status'] == '3')
                                   <a href="javascript:void(0);">
                                    <button class="black-btn new-btn-class active-green"><font color="white"> <i class="fa fa-check-circle" aria-hidden="true"></i> Completed</font> </button>
                                    </a>
                                @else
                                    {{-- <a href="javascript:void(0);">
                                    <button class="black-btn new-btn-class active-green"><font color="white"> <i class="fa fa-check-circle" aria-hidden="true"></i> Bid Placed</font> </button>
                                    </a> --}}
                                @endif


                                @if(isset($arr_milestone_data['data']) && sizeof($arr_milestone_data['data'])>0)
                                @if(isset($arr_milestone) && $arr_milestone == '0')
                                <?php  $segment = \Request::segment(2); ?> 
                                @if( isset($segment) && $segment != 'dispute') 
                                    @if($user != FALSE && $user->inRole('expert') && $projectInfo['project_status']=='4' && \Request::segment(1) == "expert" ) &nbsp; 
                                        @if($projectInfo['has_completion_request'] == '0')
                                            <a style="max-width: 185px !important;cursor: pointer;" onclick="confirmProjectComplete();" title="Request Client To Complete Project" class="black-btn">
                                                {{trans('common/_top_project_details.text_request_to_complete')}} 
                                            </a> 
                                        @elseif($projectInfo['has_completion_request'] == '1')
                                            <a style="max-width: 230px !important;cursor: pointer;" title="Request Client To Complete Project" class="black-btn new-btn-class active-green"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;
                                                {{trans('common/_top_project_details.text_completion_request_sent')}} 
                                            </a> 
                                        @endif 
                                    @endif  
                                @endif
                                @endif
                            @endif

                            </div>

                        @endif

                       

                       {{--  @if( isset($segment) && $segment != 'dispute')  --}}
                        @if($user != FALSE && $user->inRole('client') && $projectInfo['project_status']=='4' && \Request::segment(1) == "client" ) &nbsp;

                            @if($projectInfo['has_completion_request'] == '1')
                                <a style="max-width: 185px !important;cursor: pointer;" onclick="confirmProjectCompletionAccept();" title="Accept Project Completion Request From Expert" class="view_btn">
                                {{trans('common/_top_project_details.text_complete_project')}} 
                                 </a> 
                            @elseif($projectInfo['has_completion_request'] == '2')
                                <a style="max-width: 185px !important;cursor: pointer;" onclick="confirmProjectCompletionAccept();" title="Request Client To Complete Project" class="view_btn">
                                {{trans('common/_top_project_details.text_project_completed')}} 
                                </a> 
                            @endif 
                        @endif 
                        {{-- @endif --}}
                        <?php } ?>
                    </div>
                    <div class="clr"></div>

                </div>
            </div>

            {{-- tag to open modal --}}
            <a data-toggle="modal" style="display: none;" data-target="#myModal" id="open_modal"></a>
            {{-- ends --}}
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog modal-lg">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 style="color: #0E99E5;" class="modal-title"><b>
                                    {{ trans('new_translations.reject_project_bid_title')}}
                                </b></h4>
                        </div>
                        <div class="modal-body">
                            <form method="post" id="frm_reject_modal" action="{{url('/')}}/expert/projects/awarded/reject">
                                <div class="row">
                                    {{csrf_field()}}
                                    <input type="hidden" readonly="" name="project_id" id="modal_project_id" value="" />

                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <div class="user-bx">
                                            <textarea cols="" rows="" name="rejection_reason" id="rejection_reason" class="det-in1" style="font-size: 14px;font-style:normal;color: #3B3B3D;" placeholder="{{ trans('new_translations.enter_comments')}}.."></textarea>
                                            <div id="chk" style="display: none;" class="error">{{ trans('new_translations.this_field_is_required')}}</div>
                                        </div>
                                        <button type="button" id="submit-btn" class="sb-det-btn">{{ trans('new_translations.submit')}}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <script type="text/javascript">
                $(document).ready(function(){

                $('.close-overlay').click(function(){

                    setTimeout(function(){
                        hideProcessingOverlay();
                    },500);

                });

                $('.example1').webwingGallery({
                    openGalleryStyle: 'transform',
                    changeMediumStyle: true
                });  
            });
            </script>

            <script type="text/javascript">

                var project_id = $('input[name="project_id"]').val();
                function projectInfo() {
                    var project_id = $('input[name="project_id"]').val();
                    if (project_id == "") {
                        alertify.alert('Correct project information not available.');
                        return false;
                    }
                    return true;
                }

                confirm_msg = "Do you really want to sent project completion request ?";
                function confirmProjectComplete() {
                    projectInfo();
                    var project_id = $('input[name="project_id"]').val();
                    swal({
                              title: "Do you really want to sent project completion request ?",
                              text: "",
                              icon: "warning",
                              
                              dangerMode: true,
                                showCancelButton: true,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: 'Yes, I am sure!',
                                cancelButtonText: "No",
                                closeOnConfirm: false,
                                closeOnCancel: false
                             },
                             function(isConfirm){

                               if (isConfirm){
                                var project_id = $('input[name="project_id"]').val();
                                 showProcessingOverlay();
                                 window.location.href = "{{url('/expert/request_to_complete')}}/" + project_id;
                                    return true;

                                } else {
                                    swal.close();
                                  return false;
                                }
                             });
                }

                function confirmProjectCompletionAccept() {
                    projectInfo();
                    var project_id = $('input[name="project_id"]').val();
                    var confirm_msg = "{!! trans('common/_top_project_details.project_complete_request')!!}";
                    swal({
                              title: confirm_msg,
                              text: "",
                              icon: "warning",
                              
                              dangerMode: true,
                                showCancelButton: true,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: 'Yes, I am sure!',
                                cancelButtonText: "No",
                                closeOnConfirm: false,
                                closeOnCancel: false
                             },
                             function(isConfirm){

                               if (isConfirm){
                                var project_id = $('input[name="project_id"]').val();
                                 showProcessingOverlay();
                                 window.location.href = "{{url('/client/accept_completion_request')}}/" + project_id;
                                    return true;

                                } else {
                                    swal.close();
                                  return false;
                                }
                             });
                }

                /* Accept project code */
                function confirmaccept(ref) {
                    var confirm_message = "{!! trans('expert/projects/awarded_projects.confirm_accept_project') !!}";
                    swal({
                              title: confirm_message,
                              text: "",
                              icon: "warning",
                              
                              dangerMode: true,
                                showCancelButton: true,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: 'Yes, I am sure!',
                                cancelButtonText: "No",
                                closeOnConfirm: false,
                                closeOnCancel: false
                             },
                             function(isConfirm){

                               if (isConfirm){
                                 var project_id = $(ref).attr('data-project-id'); 
                                 showProcessingOverlay();
                                 window.location.href = "{{ $module_url_path }}/awarded/accept/" + project_id;
                                    return true;

                                } else {
                                    swal.close();
                                  return false;
                                }
                             });
                }
                /* reject project code */
                $('#submit-btn').click(function() {
                    if ($('#rejection_reason').val() == '') {
                        $('#chk').show();
                        return false;
                    } else if ($('#rejection_reason').val() != '') {
                        swal({
                              title: "Are you sure? You want to reject project ?",
                              text: "",
                              icon: "warning",
                              
                              dangerMode: true,
                                showCancelButton: true,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: 'Yes, I am sure!',
                                cancelButtonText: "No",
                                closeOnConfirm: false,
                                closeOnCancel: false
                             },
                             function(isConfirm){

                               if (isConfirm){
                                  
                                 showProcessingOverlay();
                                 $('#frm_reject_modal').submit();
                                    return true;

                                } else {
                                    swal.close();
                                  return false;
                                }
                             });
                    }
                });

                function rejectProject(ref) {
                    var project_id = $(ref).attr('data-project-id');
                    if (project_id.trim() != "") {
                        $('#open_modal').click();
                        $('#modal_project_id').prop('value', project_id.trim());
                    } else {
                        $('#chk').html('Error while rejecting project. Please try after some time.');
                        return false;
                    }
                }

            </script>
    
      <script>
        $('.fixed-size').lightGallery({
            width: '700px',
            height: '470px',
            mode: 'lg-fade',
            addClass: 'fixed-size',
            counter: false,
            download: false,
            startClass: '',
            enableSwipe: false,
            enableDrag: false,
            speed: 500
        });
    </script>