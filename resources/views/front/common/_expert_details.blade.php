<style type="text/css">
.expert_details_section-description table tr td {
padding: 8px;
width: 33.333%;
}
</style>
<div class="right_side_section">
     <div class="head_grn">{{ trans('common/_expert_details.text_expert_details') }}</div>
     <div class="row">
       <div class="change-pwd-form">
         <div class="col-sm-12 col-md-12 col-lg-12">
          <div class="overview-clint">
            <div class="row">
             <div class="col-sm-12  col-md-12 col-lg-5">
              <div class="user-profile">
               <div class="profile-circle">
                @if(isset($arr_project_bid_info['user_details']['is_online']) && $arr_project_bid_info['user_details']['is_online']!='' && $arr_project_bid_info['user_details']['is_online']=='1')
                  <div class="user-online-round"></div>
                @else
                  <div class="user-offline-round"></div>
                @endif
            
              <?php 
                $expert_user_name = "---";  
                if(isset($arr_expert_details['user_id']) && $arr_expert_details['user_id'] != "")
                {
                  $user = Sentinel::findById($arr_expert_details['user_id']);
                  if(isset($user) && $user != FALSE)
                  { 
                    if(isset($user->user_name))
                    {
                      $expert_user_name = $user->user_name;
                    }
                  }
                }
                /* getting city and country name from helper function */
                if(isset($arr_expert_details['country'])){
                  $country_name = get_country_name($arr_expert_details['country']);
                  $city_name    = get_city_name($arr_expert_details['city']);
                } else {
                  $country_name = "";
                  $city_name    = "";
                }
              ?>
              <?php
                  $arr_profile_data = [];
                  if(isset($arr_expert_details['user_id']) && $arr_expert_details['user_id'] != "")
                  {
                    $arr_profile_data = sidebar_information($arr_expert_details['user_id']);
                  }

              ?>
              @if(isset($arr_expert_details['profile_image']) && $arr_expert_details['profile_image'] != "" && file_exists('public/uploads/front/profile/'.$arr_expert_details['profile_image']))   
                  <img src="{{ url('/uploads/front/profile/').'/'.$arr_expert_details['profile_image']}}" alt=""/>
              @else
                  <img src="{{  url('/uploads/front/profile/default_profile_image.png')}}" alt=""/>
              @endif
               </div>
               <div class="profile-name1">
                {{isset($arr_expert_details['first_name'])?$arr_expert_details['first_name']:'Unknown user'}} 
                @if(isset($arr_expert_details['last_name']) && $arr_expert_details['last_name'] !="") @php echo substr($arr_expert_details['last_name'],0,1).'.'; @endphp @endif
                

               {{--  @if(isset($arr_project_bid_info['user_details']['is_online']) && $arr_project_bid_info['user_details']['is_online']!='' && $arr_project_bid_info['user_details']['is_online']=='1')
                      <img src="{{url('/public/front/images/active.png')}}"   class="active-deactive-user">
                @else
                      <img src="{{url('/public/front/images/deactive.png')}}" class="active-deactive-user">
                @endif --}}

                @if(isset($arr_project_bid_info['user_details']['kyc_verified']) && $arr_project_bid_info['user_details']['kyc_verified']!='' && $arr_project_bid_info['user_details']['kyc_verified'] == '1')
                  <span class="verfy-new-arrow">
                      <a href="#" data-toggle="tooltip" title="Identity Verified"><img src="{{url('/public/front/images/verifild.png')}}" alt=""> </a>
                  </span>
                @endif

               </div>
                @if($arr_profile_data['last_login_duration'] == 'Active')
                   <span class="" title="{{isset($arr_profile_data['expert_timezone'])?$arr_profile_data['expert_timezone']:'Not Specify'}}">
                      <span style="color:#4D4D4D;font-size: 14px;text-transform:none;cursor:text;">
                        <span class="flag-image"> 
                            @if(isset($arr_profile_data['user_country_flag'])) 
                              @php 
                                 echo $arr_profile_data['user_country_flag']; 
                              @endphp 
                            @elseif(isset($arr_profile_data['user_country'])) 
                              @php 
                                echo $arr_profile_data['user_country']; 
                              @endphp 
                            @else 
                              @php 
                                echo 'Not Specify'; 
                              @endphp 
                            @endif 
                         </span>
                        {{-- trans('common/_expert_details.text_local_time') --}}  {{isset($arr_profile_data['expert_timezone_without_date'])?$arr_profile_data['expert_timezone_without_date']:'Not Specify'}}</span>
                   </span>
                @else
                   <span class="" title="{{$arr_profile_data['last_login_full_duration']}}">
                      <span class="flag-image"> 
                          @if(isset($arr_profile_data['user_country_flag'])) 
                            @php 
                               echo $arr_profile_data['user_country_flag']; 
                            @endphp 
                          @elseif(isset($arr_profile_data['user_country'])) 
                            @php 
                              echo $arr_profile_data['user_country']; 
                            @endphp 
                          @else 
                            @php 
                              echo 'Not Specify'; 
                            @endphp 
                          @endif 
                      </span>
                      <i  class="fa fa-clock-o" aria-hidden="true"></i>
                      <a style="color:#4D4D4D;font-size: 14px;text-transform:none;cursor:text;">
                         {{$arr_profile_data['last_login_duration']}}
                      </a>
                   </span>
                @endif
               
             </div>
            </div>
           <div class="col-sm-12  col-md-12 col-lg-7">
            <div class="rating-profile container-rating-profile add-new-center ">
              <div class="rating_profile"  style="max-width: 350px; width: 100%;">
               <div class="rating-title">{{ trans('common/_expert_details.text_rating') }} <span>:</span> </div>
               <div class="rate-t">
                <div class="rating-list"><span class="stars">{{isset($arr_profile_data['average_rating'])? $arr_profile_data['average_rating']:0  }}</span></div>

                @if(isset($arr_profile_data['average_rating']) && $arr_profile_data['average_rating'] != "" )   
                 ( {{ number_format(floatval($arr_profile_data['average_rating']), 1, '.', '')  }} )
                @else
                 ( '0.0' )
                @endif
                {{--  ({{ $arr_profile_data['average_rating'] or '0' }}) --}}
              </div>
            </div>
            <div class="rating_profile">
             <div class="rating-title">{{ trans('common/_expert_details.text_projects') }}  <span>:</span> </div>
             <div class="star-rat">{{isset($arr_profile_data['project_count'])? $arr_profile_data['project_count']:0  }}</div>
            </div>
            <div class="rating_profile">
              <div class="rating-title">{{ trans('common/expert_sidebar.text_ongoing_projects') }} </div>
              <div class="star-rat">{{isset($arr_profile_data['ongoing_project_count'])?$arr_profile_data['ongoing_project_count']:'0'}}</div>
            </div>
            <div class="rating_profile">
              <div class="rating-title">{{ trans('common/expert_sidebar.text_completed_projects') }} </div>
              <div class="star-rat">{{isset($arr_profile_data['completed_project_count'])?$arr_profile_data['completed_project_count']:'0'}}</div>
            </div>
            <div class="rating_profile">
             <div class="rating-title">{{ trans('common/_expert_details.text_review') }} <span>:</span> </div>
             <div class="star-rat">{{isset($arr_profile_data['review_count'])? $arr_profile_data['review_count']:0  }}</div>
            </div>
            <div class="rating_profile">
             <div class="rating-title">{{ trans('common/_expert_details.text_earned') }}&nbsp;(USD)<span>:</span> </div>
             <div class="star-rat">{{isset($arr_profile_data['total_project_cost'])? $arr_profile_data['total_project_cost']:0  }}</div>
            </div>
            <div class="rating_profile">
             <div class="rating-title">{{ trans('common/_expert_details.text_reputation') }}<span>:</span> </div>
             <div class="star-rat">{{isset($arr_profile_data['reputation'])? $arr_profile_data['reputation']:0  }}%</div>
            </div>
         </div>
       </div>
     </div>
   </div>
 </div> 
 <div class="col-sm-12 col-md-12 col-lg-12">
   <div class="change-pwd-form">
    {{-- <div class="user-box">
      <div class="row">
       <div class="col-xs-12 col-sm-12 col-lg-3 b-control-label">{{ trans('common/_expert_details.text_username') }}&nbsp;:</div>
       <div class="col-sm-12 col-md-12 col-lg-9 marg_left input-name">{{$expert_user_name}}</div>                                                                         
      </div>
     </div> --}}
     <?php 
     $arr_spoken_language = [];
     $str_lang = "";
     if(isset($arr_profile_data['user_details']['expert_spoken_languages']) && count($arr_profile_data['user_details']['expert_spoken_languages']) > 0 )
     {
        $arr_tmp_language = $arr_profile_data['user_details']['expert_spoken_languages'];
        foreach ($arr_tmp_language as $key => $langauge) 
        { 
          if(isset($langauge['language']) && $langauge['language'] != "")
          {
            array_push($arr_spoken_language ,$langauge['language']);
          }
        }
        if(count($arr_spoken_language) > 0 )
        {
           $str_lang = implode(', ', $arr_spoken_language);
        }
     }
     $str_lang = trim($str_lang,','); 
     ?>

      <div class="expert_details_section">
        <div class="expert_details_section-label">
          <label>{{ trans('common/_expert_details.text_about_me') }} : </label>
          <div class="expert_details_section-description">
           {{$arr_expert_details['about_me'] or ''}}
          </div>
          <div class="clearfix"></div>
        </div>
      </div>

      <div class="expert_details_section">
        <div class="expert_details_section-label">
          <label>{{ trans('common/_expert_details.text_spoken_languages') }} : </label>
          <div class="expert_details_section-description">
            {{$str_lang or ''}}
          </div>
          <div class="clearfix"></div>
        </div>
      </div>

      <div class="expert_details_section">
        <div class="expert_details_section-label">
          <label>{{trans('common/_expert_details.text_local_time')}} : </label>
          <div class="expert_details_section-description">
            {{isset($arr_profile_data['expert_timezone'])?$arr_profile_data['expert_timezone_without_date']:'Not specify'}}
          </div>
          <div class="clearfix"></div>
        </div>
      </div>

      <div class="expert_details_section">
        <div class="expert_details_section-label">
          <label>{{ trans('common/_expert_details.text_education') }} : </label>
          <div class="expert_details_section-description">
            {{$user_details['education_details']['title'] or 'NA'}}
          </div>
          <div class="clearfix"></div>
        </div>
      </div>

      <div class="expert_details_section">
        <div class="expert_details_section-label">
          <label>{{ trans('common/_expert_details.text_certificates') }} : </label>
          <div class="expert_details_section-description">
            @if(isset($user_details['expert_certification_details']) && count($user_details['expert_certification_details'])>0)
            <table width="100%;">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Initial Certification Date</th>
                  <th>Expiration Date</th>
                </tr>
              </thead>
              <tbody>
                @foreach($user_details['expert_certification_details'] as $key=>$certificates)
                  <tr>
                    <td>{{$certificates['certification_name'] or 'NA'}}</td>
                    <td>{{isset($certificates['completion_year'])?date("d-M-y", strtotime($certificates['completion_year'])):''}}</td>
                    <td>{{isset($certificates['expire_year'])?date("d-M-y", strtotime($certificates['expire_year'])):''}}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            @else 
            {{'NA'}}
            @endif
          </div>
          <div class="clearfix"></div>
        </div>
      </div>

      <div class="expert_details_section">
        <div class="expert_details_section-label">
          <label>{{ trans('common/_expert_details.text_experience') }} : </label>
          <div class="expert_details_section-description">

            @if(isset($user_details['expert_work_experience']) && count($user_details['expert_work_experience'])>0)
            <table width="100%;">
              <thead>
                <tr>
                  <th>Position/Role</th>
                  <th>Company Name</th>
                  <th>From - To</th>
                </tr>
              </thead>
              <tbody>
                @foreach($user_details['expert_work_experience'] as $key=>$experience)
                  <tr>
                    <td>{{$experience['designation'] or 'NA'}}</td>
                    <td>{{$experience['company_name'] or 'NA'}}</td>
                    <td>{{$experience['from_year'] or 'NA'}}-{{$experience['to_year'] or 'NA'}}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            @else 
            {{'NA'}}
            @endif

           
          </div>
          <div class="clearfix"></div>
        </div>
      </div>

   </div>
   <div class="clr"></div>
 </div>
</div>
</div>
</div>
<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>