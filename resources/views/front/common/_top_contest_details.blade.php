<style type="text/css">
/*.doc-block-attchment{width: 183px;background-color: #f4f4f4;border-radius: 3px;padding: 4px;position: relative;display: inline-block;vertical-align: middle;margin: 0px 0px 28px 4px;height: 49px;}*/
</style>
<link rel="stylesheet" href="{{url('/public')}}/front/css/gallery.css" type="text/css" />
<link rel="stylesheet" href="{{url('/public')}}/front/css/lightgallery.css" type="text/css" />
<script type="text/javascript" src="{{url('/public')}}/front/js/gallery.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/front/js/lightgallery.js"></script>
<?php $user = Sentinel::check();?>
<div class="search-grey-bx white-wrapper">
    <div class="ongonig-project-section">
        @include('front.layout._operation_status')
        <div class="project-user-details">
            <div class="pro-title">
                <a href="javascript:void(0)"><h2>{{isset($arr_contest_details['contest_title'])?ucfirst($arr_contest_details['contest_title']):"N/A"}}</h2>
                </a>
            </div>
            <div class="category-det">
                <p><b>Category : </b>{{isset($arr_contest_details['category_details']['category_title'])?$arr_contest_details['category_details']['category_title']:''}}</p>
                <span>|</span>                
                <p><b>{{trans('common/_top_project_details.text_sub_category')}} :</b> {{isset($arr_contest_details['sub_category_details']['subcategory_title'])?$arr_contest_details['sub_category_details']['subcategory_title']:''}}</p>
                <span>|</span>

                @if(isset($arr_contest_details['contest_skills']) && count($arr_contest_details['contest_skills']) > 0) 
                @foreach($arr_contest_details['contest_skills'] as $key=>$skills) 
                @if(isset($skills['skill_data']['skill_name']) && $skills['skill_data']['skill_name'] != "")
                <li style="font-size:12px;" class="search-project-skils">{{ str_limit($skills['skill_data']['skill_name'],25) }}</li>
                @endif
                @endforeach
                @endif                
            </div>
            <div class="clearfix"></div>
            <div class="category-det">             
                <p><b><i class="fa fa-money" aria-hidden="true"></i> Contest Prize : </b>{{$arr_contest_details['contest_currency'] or "N/A"}} {{$arr_contest_details['contest_price'] or "N/A"}} 
                </p>
                <p><b></b></p><span>|</span>
                <p><b><i class="fa fa-lightbulb-o" aria-hidden="true"></i> Status : </b>
                    @if(isset($arr_contest_details['contest_status']) && $arr_contest_details['contest_status']==0) {{"OPEN"}} @endif
                    @if(isset($arr_contest_details['contest_status']) && $arr_contest_details['contest_status']==1) {{"ENDING"}} @endif
                    @if(isset($arr_contest_details['contest_status']) && $arr_contest_details['contest_status']==2) {{"CLOSED"}} @endif
                </p>
                <p><b></b></p><span>|</span>
                <p><b><i class="fa fa-sign-in" aria-hidden="true"></i> No. of Entries : </b> {{count($arr_contest_details['contest_entry'])}} </p>
            </div>
                <input type="hidden" readonly="" name="project_id" value=""/> 
        </div>
        <div class="det-divider"></div>
        <div class="project-title">
            <div class="pro-title"> Description :</div>
            <div class="sml-des-txt">  {{$arr_contest_details['contest_description'] or "N/A"}}</div>            
        </div><br>
        
        @if($arr_contest_details['contest_additional_info'] != null)
            <div class="det-divider"></div>
            <div class="project-title">
                <div class="pro-title newww "> Additional Information :</div>
                <div class="sml-des-txt">  {{$arr_contest_details['contest_additional_info'] or "N/A"}}</div>
                <div class="more-project-dec" style="height:auto;"></div>
            </div>
        @endif

        <div class="attachments-block">

            @php $download_file = "download"; $user_role = ""; @endphp
            @if(isset($user) && $user != null)
            @php $user_role    = get_user_role($user->id); @endphp
            @if($user_role != 'expert')  
            @if($user->id  != $arr_contest_details['client_user_id'])
            @php $download_file = ""; @endphp
            @endif  
            @endif
            @else
            @php $download_file = ""; @endphp 
            @endif
            
            @if(isset($arr_contest_details['contest_post_documents']) && count($arr_contest_details['contest_post_documents'])>0)
               <div style="font-size: 15px; font-style: bold; color: #555;">{{ trans('contets_listing/listing.text_attachments') }} : 
              
              @if(isset($user) && $user!=null)
                @if($user_role == 'expert' || $user->id == $arr_contest_details['client_user_id'])
                  <a href="{{url('/contests/details/make_zip/')}}/{{base64_encode($arr_contest_details['id'])}}" style="font-size: 17px;float: right;" class="close-overlay">
                      <i class="fa fa-file-archive-o right"></i> {{trans("new_translations.text_download_all_files")}}
                  </a> </div><br>
                  <div class="project-list mile-list nw-bold">
                  <div class="demo-gallery dark mrb35 new-add-photo">
                  <ul class="fixed-size list-unstyled">
                  @foreach($arr_contest_details['contest_post_documents'] as $data)
                  <?php 
                  $contest_attachment     = isset($data['image_name'])?$data['image_name']:"";
                  $get_attatchment_format = get_attatchment_format($contest_attachment);
                  $arr_image_formates     = ['jpg','png','jpeg','PNG','JPEG','JPG','gif','GIF'];
                  ?>
                  @if(isset($get_attatchment_format) && $get_attatchment_format != '' && !in_array($get_attatchment_format,$arr_image_formates))
                  @if(file_exists(base_path().'/public/front/images/file_formats/'.$get_attatchment_format.'.png'))
                       <li data-src="{{ url('/')}}/front/images/file_formats/{{$get_attatchment_format}}.png">
                          <a href="javascript:void(0);">
                              <img class="img-responsive" src="{{url('/public')}}/front/images/file_formats/{{$get_attatchment_format}}.png" alt="pre">
                              <div class="demo-gallery-poster">
                               <img src="{{url('/public')}}/front/images/zoom.png"  class="img-responsive" alt=""/>
                              </div>
                          </a>
                       </li>
                  @else
                   <li data-src="{{ url('/')}}/front/images/file_formats/Default.png">
                          <a href="javascript:void(0);">
                              <img class="img-responsive" src="{{url('/public')}}/front/images/file_formats/Default.png" alt="pre">
                              <div class="demo-gallery-poster">
                               <img src="{{url('/public')}}/front/images/zoom.png"  class="img-responsive" alt=""/>
                              </div>
                          </a>
                       </li>
                  @endif
                  @else
                  <li data-src="{{url('/public')}}{{config('app.project.img_path.contest_files')}}{{$data['image_name']}}">
                      <a href="javascript:void(0);">
                          <img class="img-responsive" src="{{url('/public')}}{{config('app.project.img_path.contest_files')}}{{$data['image_name']}}" alt="pre">
                          <div class="demo-gallery-poster">
                           <img src="{{url('/public')}}/front/images/zoom.png"  class="img-responsive" alt=""/>
                          </div>
                      </a>
                  </li>
                   @endif
                   @endforeach

                   </ul>
                <div class="clearfix"></div>
                </div>
                <div class="file-download">
                  <ul>
                     @if(isset($arr_contest_details['contest_post_documents']) && count($arr_contest_details['contest_post_documents'])>0)
                      @foreach($arr_contest_details['contest_post_documents'] as $data)
                             <li >
                                <div class="down-arrow">
                                    <a {{$download_file}} href="{{url('/public')}}/{{config('app.project.img_path.contest_files')}}{{$data['image_name']}}"
                                        style="display: block;position: relative;text-align: center;"><i class="fa fa-cloud-download"></i></a>
                                </div>
                             </li>
                         @endforeach
                         @endif
                  </ul>
                </div>
              </div>
            </div>
                @else
                  <br><br><i><p class="contest-listing-note-section" style="color: red; font-size: 15px;">{{trans('contets_listing/listing.text_please_login_as_expert_to_download_and_see_original_attachment')}}</p></i>
                @endif
              @else
                <br><br><i><p class="contest-listing-note-section" style="color: red; font-size: 15px;">{{trans('contets_listing/listing.text_please_login_as_expert_to_download_and_see_original_attachment')}}</p></i>
              @endif
            @endif

        </div>

        <div class="clr"></div>
        <div class="clr"></div>
    </div>
</div>

        <script type="text/javascript">

            $(document).ready(function(){

                $('.close-overlay').click(function(){

                    setTimeout(function(){
                        hideProcessingOverlay();
                    },500);

                });

                $('.example1').webwingGallery({
                    openGalleryStyle: 'transform',
                    changeMediumStyle: true
                });  
            });
$('.fixed-size').lightGallery({
            width: '700px',
            height: '470px',
            mode: 'lg-fade',
            addClass: 'fixed-size',
            counter: false,
            download: false,
            startClass: '',
            enableSwipe: false,
            enableDrag: false,
            speed: 500
        });
        </script>