<!DOCTYPE html>
<html>
    <head>
          <title>404 - {{ config('app.project.name') }}</title>
          <meta http-equiv="Content-Type"    content="text/html; charset=utf-8" />
          <meta name="viewport"              content="width=device-width, initial-scale=1.0" />
          <meta http-equiv="X-UA-Compatible" content="IE=edge" />
          <meta name="description"           content="archexperts.com is an online marketplace for project outsourcing in 3D design environment.Start a project now and get your new interior and exterior designs quickly done by many very talented experts." />
          <meta name="keywords"              content="TOM FLETCHER" />
          <meta name="author"                content="TOM FLETCHER" />
          <!-- ======================================================================== -->
          <link rel="icon" href="{{url('/public')}}/favicon.ico" type="image/x-icon" />
          <!-- Bootstrap Core CSS -->
          <link href="{{url('/public')}}/front/css/bootstrap.css" rel="stylesheet" type="text/css" />
          <!-- main CSS -->
          <link href="{{url('/public')}}/front/css/virtual-home.css" rel="stylesheet" type="text/css" />        
          <style>.login-section.wrapper-404{margin: -20px 0 0;}</style>    
    </head>
    <body>
     <link href="{{url('/public')}}/front/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="{{url('/public')}}/front/css/virtual-home.css" rel="stylesheet" type="text/css" /> 
      <div class="login-section thank-you-wrapper wrapper-404">
            <div class="thank-you">
                <h1>404</h1>
                <h4> {{ trans('new_translations.something_wrong_here')}}...!</h4>
                <p>{{ trans('new_translations.we_can_t_find_the_page')}}.</p>
                <a href="{{url('/')}}">
                    <button class="job-btn" type="button">
                        {{ trans('new_translations.home')}}
                    </button>
                </a>
            </div>
        </div>
    </body>
</html>
