<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    
    <title>{{ config('app.project.name') }} Video Meeting</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{url('/public/twilio-chat')}}/video/assets/css/bootstrap.min.css">
    {{-- <link rel="stylesheet" type="text/css" href="{{url('/public/twilio-chat')}}/video/assets/css/font-awesome.min.css"> --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
    <link rel="stylesheet" type="text/css" href="{{url('/public/twilio-chat')}}/video/assets/css/style.css">

    <style type="text/css">
        .screen-share-desktop{ color: #f06060; };

        .my-live-video-section {
            position: absolute;
            z-index: 99;
            bottom: -13px;
            right: 52px;
            height: 126px;
            border: 3px solid #fff;
            border-radius: 6px;
        }

        div#my-video-section video {
            position: absolute;
            z-index: 99;
            bottom: -13px;
            right: 52px;
            height: 126px;
            border: 3px solid #fff;
            border-radius: 6px;
        }

    </style>
</head>

<body>
    <div class="main-wrapper">
        
        <div class="page-wrapper">
            <div class="chat-main-row">
                <div class="chat-main-wrapper">
                    <div class="col-lg-12 message-view chat-view">
                        <div class="chat-window">
                            <div class="fixed-header">
								<div class="navbar">
                                    <div class="user-details mr-auto">
                                        <div class="float-left user-img m-r-10">
                                            <a href="javascript:void(0);"><img src="{{url('/public/twilio-chat')}}/video/assets/img/user.jpg" alt="" class="w-40 rounded-circle"></a>
                                        </div>
                                        <div class="user-info float-left">
                                            <a href="javascript:void(0);">
                                                <span class="font-bold">
                                                    {{ isset($arr_to_user_details['to_user_name']) ? $arr_to_user_details['to_user_name'] : '-' }}
                                                    @if(isset($arr_to_user_details['is_admin_chat_initiated']) && $arr_to_user_details['is_admin_chat_initiated'] == '1')
                                                    | {{ isset($arr_to_user_details['admin_user_name']) ? $arr_to_user_details['admin_user_name'] : '-' }}
                                                    @endif
                                                </span>
                                            </a>
                                        </div>
                                    </div>
								</div>
                            </div>
                            <div class="chat-contents">
                                <div class="chat-content-wrap">
                                    <div class="user-video-wrap">
                                        <div class="user-video-box" id="remote-video-section">
                                            <div class="user-video">
                                                <img src="{{url('/public/twilio-chat')}}/video/assets/img/telephone_gif.gif" alt="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="my-video" id="my-video-section">
                                        <ul>
                                            <li>
                                            <div class="user-video">
                                                <img src="{{url('/public/twilio-chat')}}/video/assets/img/user.jpg" class="img-fluid" alt="">
                                            </div>
                                            </li>
                                        </ul>
                                    </div>
                                   
                                    {{-- <div class="user-video" id="remote-video-section">
                                        <img src="{{url('/public/twilio-chat')}}/video/assets/img/telephone_gif.gif" alt="">
                                    </div>
                                    <div class="my-video my-live-video-section" id="my-video-section">
                                        <ul>
                                            <li>
                                                <img src="{{url('/public/twilio-chat')}}/video/assets/img/user.jpg" class="img-fluid" alt="">
                                            </li>
                                        </ul>
                                    </div> --}}

                                </div>
                            </div>
                            <div class="chat-footer">
                                <div class="call-icons">
                                    <ul class="call-items">
                                        <li class="call-item">
                                            <a href="javascript:void(0);" id="btn_mute_audio">
                                                <i class="fa fa-microphone microphone"></i>
                                            </a>
                                        </li>
                                        <li class="call-item">
                                            <a href="javascript:void(0);" id="btn_end_call">
                                                <i style="color: #f06060;" class="fa fa-phone phone"></i>
                                            </a>
                                        </li>

                                        <li class="call-item">
                                            <a href="javascript:void(0);" id="btn_hide_video">
                                                <i class="fa fa-video camera"></i>
                                            </a>
                                        </li>

                                        <li class="call-item">
                                            <a href="javascript:void(0);" id="btn_share_screen" title="Share Screen" data-placement="top" data-toggle="tooltip">
                                                <i class="fa fa-desktop camera"></i>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>

                            <div id="log" style="display: none;"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="sidebar-overlay" data-reff=""></div>
    <script src="{{url('/public/twilio-chat')}}/video/assets/js/jquery-3.2.1.min.js"></script>
	<script src="{{url('/public/twilio-chat')}}/video/assets/js/popper.min.js"></script>
    <script src="{{url('/public/twilio-chat')}}/video/assets/js/bootstrap.min.js"></script>
    {{-- <script src="{{url('/public/twilio-chat')}}/video/assets/js/jquery.slimscroll.js"></script> --}}
    <script src="{{url('/public/twilio-chat')}}/video/assets/js/app.js"></script>

    {{-- <script src="//media.twiliocdn.com/sdk/js/common/v0.1/twilio-common.min.js"></script>
    <script src="//media.twiliocdn.com/sdk/js/video/releases/1.14.0/twilio-video.js"></script> --}}

    <script src="//sdk.twilio.com/js/video/releases/2.8.0/twilio-video.min.js"></script>

    <script type="text/javascript">

        var telephone_gif = '{{url('/public/twilio-chat/video/assets/img/telephone_gif.gif')}}';
        var twilio_chat_base_url = '{{url('/public/twilio-chat')}}';
        var video_call_mp3 = new Audio(twilio_chat_base_url + '/audio/video-call.mp3');
        var call_failed_mp3 = new Audio(twilio_chat_base_url + '/audio/call-failed.mp3');

        var access_token    = '{{ isset($access_token) ? $access_token : '' }}';
        var login_user_id   = '{{ isset($login_user_id) ? $login_user_id : '' }}';
        var login_user_name = '{{ isset($login_user_name) ? $login_user_name : '' }}';

        var uniqueName    = '{{ isset($obj_video_room_resource->uniqueName) ? $obj_video_room_resource->uniqueName : '' }}'

        // Check for WebRTC
        /*if (!navigator.webkitGetUserMedia && !navigator.mozGetUserMedia) {
          alert('WebRTC is not available in your browser.');
        }*/

        // When we are about to transition away from this page, disconnect
        // from the room, if joined.
        window.addEventListener('beforeunload', leaveRoomIfJoined);

        $( document ).ready(function() {

            setTimeout(function(){
                connectTwilioVideo();
            },1000);

        });

        var activeRoom;
        var previewTracks;
        var identity;
        var roomName;

        $('#btn_end_call').on('click',function(){
            leaveRoomIfJoined();
            window.close();
        });

        $('#btn_mute_audio').on('click',function(){
            let ref = $(this).children('.fa');
            if( true == $(ref).hasClass('fa-microphone') ) {
                if( activeRoom != undefined ){
                    activeRoom.localParticipant.audioTracks.forEach(publication => {
                      publication.track.disable();
                    });
                }
                $(ref).removeClass('fa-microphone').addClass('fa-microphone-slash');
                $(ref).css({"color":"#f06060"});

            } else {
                if( activeRoom != undefined ){
                    activeRoom.localParticipant.audioTracks.forEach(publication => {
                      publication.track.enable();
                    });
                }
                $(ref).removeClass('fa-microphone-slash').addClass('fa-microphone');
                $(ref).css({"color":""});
            }
        });

        $('#btn_hide_video').on('click',function(){
            let ref = $(this).children('.fa');
            if( true == $(ref).hasClass('fa-video') ) {
                if( activeRoom != undefined ){
                    activeRoom.localParticipant.videoTracks.forEach(publication => {
                      publication.track.disable();
                    });
                }
                $(ref).removeClass('fa-video').addClass('fa-video-slash');
                $(ref).css({"color":"#f06060"});
            } else {
                if( activeRoom != undefined ){
                    activeRoom.localParticipant.videoTracks.forEach(publication => {
                      publication.track.enable();
                    });
                }
                $(ref).removeClass('fa-video-slash').addClass('fa-video');
                $(ref).css({"color":""});                
            }
        });
        
        $('#btn_share_screen').on('click',function(){
            let ref = $(this).children('.fa');
            if( true == $(ref).hasClass('screen-share-desktop') ) {
                $(ref).removeClass('screen-share-desktop');
            } else {
                $(ref).addClass('screen-share-desktop');
            }
        });

        function connectTwilioVideo() {

            var connectOptions = { name: uniqueName };
            if (previewTracks) {
                connectOptions.tracks = previewTracks;
            }

            Twilio.Video.connect(access_token, connectOptions).then(roomJoined, function(error) {
                log('Could not connect to Twilio: ' + error.message);
            });
        }

        // Successfully connected!
        function roomJoined(room) {
            activeRoom = room;

            log("Joined as '" + login_user_name + "'");
 
            // Draw local video, if not already previewing
            var previewContainer = document.getElementById('my-video-section');
            if (!previewContainer.querySelector('video')) {
                $('#my-video-section').html('');
                var tracks = Array.from(room.localParticipant.tracks.values());
                tracks.forEach(function(track) {
                    previewContainer.appendChild(track.track.attach());
                });
            }

            if(activeRoom.participants != undefined && Array.from(activeRoom.participants).length == 0 ) {
                playVideoCallAudio();
            }

            if(activeRoom.participants != undefined && Array.from(activeRoom.participants).length > 0 ) {
                pauseVideoCallAudio();
                $('#remote-video-section').html('');
            }
            
            room.participants.forEach(participant => {
                participant.tracks.forEach(publication => {
                    publication.on('subscribed', function(track){
                        track.on('disabled', () => {
                            if(track.kind == 'video'){
                                $('#'+participant.sid).find("video").hide();
                            }
                            console.log('disabled event called',track,participant);
                            /* Hide the associated <video> element and show an avatar image. */
                        });

                        track.on('enabled', () => {
                            if(track.kind == 'video'){
                                $('#'+participant.sid).find("video").show();
                            }
                            console.log('enabled event called',track,participant);
                            /* Hide the associated <video> element and show an avatar image. */
                        });
                    });
                    // publication.on('subscribed', handleTrackDisabled);
                });

                const participantDiv    = document.createElement("div");
                const participantSubDiv = document.createElement("div");

                participantSubDiv.className = 'user-video';
                participantDiv.id = participant.sid;
                participantDiv.className = 'user-video-box';
                participant.on('trackSubscribed', track => {
                    participantSubDiv.appendChild(track.attach());
                    participantDiv.appendChild(participantSubDiv);
                    document.getElementById('remote-video-section').appendChild(participantDiv);
                });
            });

            // Attach the Participant's Media to a <div> element.
            room.on('participantConnected', participant => {
                log(`Participant "${participant.identity}" connected`);
                
                pauseVideoCallAudio();
                $('#remote-video-section').html('');
                
                const participantDiv    = document.createElement("div");
                const participantSubDiv = document.createElement("div");

                participantSubDiv.className = 'user-video';
                participantDiv.id = participant.sid;
                participantDiv.className = 'user-video-box';

                participant.on('trackSubscribed', track => {
                    participantSubDiv.appendChild(track.attach());
                    participantDiv.appendChild(participantSubDiv);
                    document.getElementById('remote-video-section').appendChild(participantDiv);
                });
            });

            // // When a participant disconnects, note in log
            room.on('participantDisconnected', function(participant) {
                
                log("Participant '" + participant.identity + "' left the room");
                
                const participantDiv = document.getElementById(participant.sid);
                participantDiv.parentNode.removeChild(participantDiv);

                if(activeRoom.participants != undefined && Array.from(activeRoom.participants).length == 0 ) {
                    $('#remote-video-section').html('<img src="'+telephone_gif+'">');

                    alert('All Participant left the Meeting');
                    setTimeout(function(){
                        call_failed_mp3.play().then(response => {
                        }).catch(e => {
                            log(e);
                        });
                        $('#btn_end_call').trigger('click');
                    },3000);
                    return;
                }

            });

            /*activeRoom.localParticipant.tracks.forEach((publication) => {
               const track = publication.track;
               // stop releases the media element from the browser control
               // which is useful to turn off the camera light, etc.
               track.stop();
               const elements = track.detach();
               elements.forEach((element) => element.remove());
             });*/

            room.on('disconnected', room => {
                log('Left');
                // Detach the local media elements
                room.localParticipant.tracks.forEach(publication => {
                    var attachedElements = publication.track.detach();
                    attachedElements.forEach(element => element.remove())
                });

                activeRoom = null;
            });

            function handleTrackDisabled(track) {
              // console.log('handleTrackDisabled',track);
              track.on('disabled', () => {
                console.log('disabled event called',track);
                /* Hide the associated <video> element and show an avatar image. */
              });
            }

            function handleTrackEnabled(track) {
              // console.log('handleTrackEnabled',track);
              track.on('enabled', () => {
                console.log('enabled event called',track);
                /* Hide the associated <video> element and show an avatar image. */
              });
            }
        }

        function attachTracks(tracks, container) {
            tracks.forEach(function(track) {
                container.appendChild(track.track.attach());
            });
        }

        function attachParticipantTracks(participant, container) {
            var tracks = Array.from(participant.tracks.values());
            attachTracks(tracks, container);
        }

        function detachTracks(tracks) {
            tracks.forEach(function(track) {
                track.track.detach().forEach(function(detachedElement) {
                detachedElement.remove();
                });
            });
        }

        function detachParticipantTracks(participant) {
            var tracks = Array.from(participant.tracks.values());
            detachTracks(tracks);
        }

        function leaveRoomIfJoined() {
          if (activeRoom) {
            activeRoom.disconnect();
          }
        }

        function playVideoCallAudio() {
            if(typeof(video_call_mp3) == 'object'){
                video_call_mp3.loop = true;
                video_call_mp3.play().then(response => {
                }).catch(e => {
                    log(e);
                });

                setTimeout(function(){
                    if(activeRoom.participants != undefined && Array.from(activeRoom.participants).length == 0 ) {
                        video_call_mp3.pause();
                        call_failed_mp3.play().then(response => {
                        }).catch(e => {
                            log(e);
                        });

                        setTimeout(function(){
                            $('#btn_end_call').trigger('click');
                        },1000);
                    }
                },100000);
            }
        }
        
        function pauseVideoCallAudio() {
            if(typeof(video_call_mp3) == 'object'){
                video_call_mp3.pause();
            }
        }

        // Activity log
        function log(message) {
          var logDiv = document.getElementById('log');
          logDiv.innerHTML += '<p>&gt;&nbsp;' + message + '</p>';
          logDiv.scrollTop = logDiv.scrollHeight;
        }
    </script>
</body>

</html>