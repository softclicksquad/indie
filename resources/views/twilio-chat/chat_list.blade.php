@if(isset($user_type) && $user_type == 'client')
  @extends('client.layout.master')
@elseif(isset($user_type) && $user_type == 'expert')
  @extends('expert.layout.master')
  @else
  @extends('client.layout.master')
@endif
             
@section('main_content')
    
    <link href="{{url('/public')}}/front/css/twillo-chat.css" rel="stylesheet" type="text/css" />

<div class="col-sm-7 col-md-8 col-lg-9">
  @include('front.layout._operation_status')
   <div class="right_side_section min-height">
    
    <div id="frame">
      <div id="sidepanel">
        @if( isset($arr_chat_list) && count($arr_chat_list)>0 )
          @foreach( $arr_chat_list as $chat_list )            
            <div id="profile" class="">
                  <div class="wrap">
                    <img id="profile-img" src="{{ url('/public/uploads/front/profile/default_profile_image.png') }}" class="online" alt="">
                    <p>{{ isset($chat_list['group_name']) ? str_limit($chat_list['group_name'],20) : '' }}</p>
                    <i class="fa fa-chevron-down expand-button" aria-hidden="true"></i>
                    @if( isset($chat_list['chat_list']) && count($chat_list['chat_list'])>0 )
                      <div id="expanded" class="sub-chat-list">
                        @foreach( $chat_list['chat_list'] as $key => $list )
                          <a href="{{ isset($list['redirect_url']) ? $list['redirect_url'] : 'javascript:void(0);' }}"> <div>  {{ isset($list['chat_name']) ? $list['chat_name'] : '' }}  </div> </a>  
                        @endforeach
                      </div>
                    @endif
                  </div>
              </div>

          @endforeach
        @endif
      </div>

      <div class="content">
      </div>  
    </div>      
      
   </div>
</div>

<script >

$(".expand-button").click(function() {
  
  if($(this).parent().parent().hasClass('expanded')){
    $(this).parent().parent().removeClass('expanded');
  } else {
    $(this).parent().parent().addClass('expanded');
  }
});

</script>

@stop