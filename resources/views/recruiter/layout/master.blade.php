<!-- HEader -->        
@include('front.layout.header')    
        
<!-- BEGIN Sidebar -->

<!-- END Sidebar -->

<!-- BEGIN Content -->

@include('recruiter.layout.sidebar')


    @yield('main_content')

    <!-- END Main Content -->

<!-- Footer -->    


@include('recruiter.layout.dynamic_footer') 


@include('front.layout.footer')    
                
              