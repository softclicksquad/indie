<?php
  
  $currentUrl  = Route::getCurrentRoute()->getPath();
  $project_type = "";   
  $user = Sentinel::check(); 
  $sidebar_information = array();  
   if($user){
    $profile_img_public_path = url('/').config('app.project.img_path.profile_image');
    $sidebar_information = sidebar_information($user->id);
   }
   
  $url = \URL::previous();
  $arr_url = [];

  $check_current_url = ['recruiter/projects/canceled','recruiter/projects/ongoing','recruiter/projects/open','recruiter/projects/completed'];

  if($url != "" && !in_array($currentUrl, $check_current_url) )
  {  
    $arr_url =  explode('/', $url);
    
    if(isset($arr_url[7]) && $arr_url[7] == 'completed')
    {
      $project_type = 'completed';   
    }
    else if(isset($arr_url[7]) && $arr_url[7] == 'open')
    {
      $project_type = 'open';   
    }
  }
?>
{{-- Top Navbar --}}
   <div class="top-title-pattern pattern-title-top top-links">
      <div class="container">
         <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
               <div class="title-box-top">  
                 <div id="region_id" class="meshows hidden-lg hidden-md">Projects </div>                             
                 <ul id="reglon_tab" class="none-dpl">
                     <li @if($currentUrl=='recruiter/projects/open') class="top-menu-active" @elseif($project_type == 'open') class="top-menu-active"  @endif>
                       <a href="{{url('/recruiter/projects/open')}}" @if($currentUrl=='recruiter/projects/open') style="color: #000000;"  @elseif($project_type == 'open') style="color: #000000;" @endif >{{ trans('common/project_manager_sidebar.text_open') }}
                        <button data-toggle="dropdown" class="btn-group mgrds conut-circl btn btn-warning parcel lag dropdown-toggle btn-prj-count" type="button">{{isset($sidebar_information['open_project_count'])?$sidebar_information['open_project_count']:'0'}}</button>
                       </a>
                    </li> 

                    <li @if($currentUrl=='recruiter/projects/completed') class="top-menu-active" @elseif($project_type == 'completed') class="top-menu-active"  @endif>
                       <a href="{{url('/recruiter/projects/completed')}}" @if($currentUrl=='recruiter/projects/completed') style="color: #000000;"  @elseif($project_type == 'completed') style="color: #000000;" @endif >Completed
                        <button data-toggle="dropdown" class="btn-group mgrds conut-circl btn btn-warning parcel lag dropdown-toggle btn-prj-count" type="button">{{isset($sidebar_information['completed_project_count'])?$sidebar_information['completed_project_count']:'0'}}</button>
                       </a>
                    </li> 
          
                 </ul>                 
               </div>
            </div>
         </div>
      </div>
   </div>

<div class="middle-container">
<div class="container">
<div class="row">
<div class="col-sm-5 col-md-4 col-lg-3">
   <div class="left-sidebar">

      <?php $user = Sentinel::check(); 
         if($user)
         {
          $sidebar_information =sidebar_information($user->id);
          $profile_img_public_path = url('/').config('app.project.img_path.profile_image');
         }
      ?>

        <div class="user-profile">
           <div class="profile-circle">
            <div class="client_image client-show-photo"> <input type="file" id="profile_image" class="file">

             @if(isset($profile_img_public_path) && isset($sidebar_information['user_details']['profile_image']) && $sidebar_information['user_details']['profile_image']!="")
               <img id="set_image" style="cursor: default;"  src="{{isset($sidebar_information['user_details']['profile_image'])?$profile_img_public_path.$sidebar_information['user_details']['profile_image']:''}}"/>
               @else
               <img id="set_image" style="cursor: default;"  src="{{$profile_img_public_path.'default_profile_image.png'}}"/>
               @endif
               
              {{--  YODA SYNTAX --}}
              @if('profile' == \Request::segment(2) )
              <div class="client-photo__upload-icon"  onclick="openCropModal()" >
                <span class="client-show-icon icon-edit_white" ><img  src="{{url('/public')}}/front/images/edit-file.png" alt=""/><br/>Edit
                </span>
              </div>
              @endif

            </div>
            
          </div>

          <div class="profile-name">
              {{isset($sidebar_information['user_details']['first_name'])?$sidebar_information['user_details']['first_name']:'Recruiter'}} 
              {{isset($sidebar_information['user_details']['last_name'])?substr($sidebar_information['user_details']['last_name'],0,1).'.':''}}
              {{-- @if($sidebar_information['last_login_duration'] == 'Active')
                 <img src="{{url('/public/front/images/active.png')}}"   class="active-deactive-user">
              @else
                 <img src="{{url('/public/front/images/deactive.png')}}" class="active-deactive-user">
              @endif --}}
              @if(isset($user_auth_details['is_online']) && $user_auth_details['is_online']!='' && $user_auth_details['is_online'] == '1')
                <img src="{{url('/public/front/images/active.png')}}"   class="active-deactive-user">
              @else
                 <img src="{{url('/public/front/images/deactive.png')}}" class="active-deactive-user">
              @endif
         </div>
        </div>
      <div class="rating-profile">      
        <div class="rating-title">{{ trans('common/project_manager_sidebar.text_projects') }} :
            <b>{{isset($sidebar_information['project_count'])?$sidebar_information['project_count']:'0'}}</b>
        </div>
       </div>
           <div class="rating-profile">      
            <div class="rating-title">{{ trans('common/project_manager_sidebar.text_availability_status') }} :
            <b>
            @if(isset($sidebar_information['is_available']) && $sidebar_information['is_available']==1) 
            {{ trans('common/project_manager_sidebar.text_availability_yes') }}
            @elseif(isset($sidebar_information['is_available']) && $sidebar_information['is_available']==0) 
            {{ trans('common/project_manager_sidebar.text_availability_no') }}
            @endif
            </b>         
               </div>
       </div>
      <div class="left-side-tabs">
         <ul>
            <li @if($currentUrl=='recruiter/dashboard') class="active-p" @endif>
               <a href="{{url('/recruiter/dashboard')}}" class="dashboards-p">{{ trans('common/project_manager_sidebar.text_dashboard') }}</a>
            </li>
            <li @if($currentUrl=='recruiter/profile') class="active-p" @endif>
               <a href="{{url('/recruiter/profile')}}" class="edit-profile3">{{ trans('common/project_manager_sidebar.text_edit_profile') }}</a>
            </li>
            <li @if($currentUrl=='recruiter/change_password') class="active-p" @endif>
               <a href="{{url('/recruiter/change_password')}}" class="change-p-3">{{ trans('common/project_manager_sidebar.text_change_password') }}</a>
            </li> 

            <li @if($currentUrl=='recruiter/availability') class="active-p" @endif><a href="{{url('/recruiter/availability')}}" class="availability-p">{{ trans('common/project_manager_sidebar.text_availability') }}</a>
            </li>

            <li @if($currentUrl=='recruiter/projects/open') class="active-p" @endif>
               <a href="{{url('/recruiter/projects/open')}}" class="applied-p">{{ trans('common/project_manager_sidebar.text_open_projects') }}</a>
            </li>   

            <li @if($currentUrl=='recruiter/projects/completed') class="active-p" @endif>
               <a href="{{url('/recruiter/projects/completed')}}" class="completed-p-4">{{ trans('common/project_manager_sidebar.text_completed') }}</a>
            </li>   

            <li @if($currentUrl=='recruiter/inbox') class="active-p" @endif>
             <a href="javascript:void(0){{-- {{url('project_manager/inbox')}} --}}" class="applozic-launcher inbox-p-8">{{ trans('common/project_manager_sidebar.text_chats') }}</a>
            </li>   
         </ul>
      </div>
   </div>
</div>   


<script type="text/javascript">
   $('#image').on('click', function() 
   { 
         $('#profile_image').click(); 
   });
   $('#profile_image').change(function() {
      var formData = new FormData();
      formData.append('file', $('input[type=file]')[0].files[0]);

            $.ajax({
               url: '{{url('/')}}/recruiter/store_profile_image?_token='+"{{ csrf_token()}}",
               type: 'POST',
               data: formData,
               cache: false,
               contentType: false,
               processData: false,
               success:function(response)
               {
                  if(response.status=="SUCCESS")
                  {   
                     $('#set_image').attr("src",response.image_name);
                     console.log(response.msg); 
                  }
                  else if(response.status=="ERROR")
                  { 
                     alertify.alert(response.msg);
                     // window.location.reload(); 
                  }
               }
            });
      });
</script>