@extends('recruiter.layout.master')                
@section('main_content')
<div class="col-sm-7 col-md-8 col-lg-9">
   <form action="{{ url($module_url_path) }}/manage" method="POST" id="form-manage-projects" name="form-manage-projects" enctype="multipart/form-data" files ="true">
      {{ csrf_field() }}
   
  <div class="right_side_section">
    <div class="col-lg-8">
     @include('front.layout._operation_status')
     <div class="head_grn">{{trans('project_manager/projects/open_projects.text_open_project_title')}}</div>
    </div>
    <div class="col-lg-4">
      <div class="droup-select">
      <select class="droup mrns tp-margn" onchange="sortByHandledBy(this)">
        <option value="">-- Sort By --</option>
        <option @if(isset($sort_by) && $sort_by == 'asc') selected="" @endif value="asc">Ascending</option>
        <option @if(isset($sort_by) && $sort_by == 'desc') selected="" @endif value="desc">Descending</option>
      </select>
      </div>
    </div>
    <div class="clearfix"></div>

   @if(isset($arr_open_projects['data']) && sizeof($arr_open_projects['data'])>0)
    @foreach($arr_open_projects['data'] as $proRec)
    <div class="ongonig-project-section client-dash">
      <div class="project-title">  
        <a href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}">
        <h3>{{$proRec['project_name']}}</h3>
        </a>
        <?php 
          $count_project_bid = \App\Models\ProjectsBidsModel::where('project_id',$proRec['id'])->count();
          if(isset($proRec['is_awarded']) && $proRec['is_awarded'] == '1')
          {
            $count_project_bid = 1;
          }
        ?>

        <div class="search-freelancer-user-location">
            <span class="gavel-icon"><img src="https://192.168.1.77/archexperts/public/front/images/bid.png" alt=""></span> <span class="dur-txt"> Bids:&nbsp;{{$count_project_bid or '0'}}</span> <span class="freelancer-user-line">|</span>
        </div>
        <div class="search-freelancer-user-location"> 
          <span class="gavel-icon"><img src="https://192.168.1.77/archexperts/public/front/images/clock.png" alt=""></span> <span class="dur-txt"> {{trans('project_manager/projects/open_projects.text_project_duration')}}: {{$proRec['project_expected_duration']}} {{trans('project_manager/projects/open_projects.text_days')}}</span><span class="freelancer-user-line">|</span>
        </div>
        <div class="search-freelancer-user-location"> 
          <span class="gavel-icon"><img src="https://192.168.1.77/archexperts/public/front/images/calendar.png" alt=""></span> <span class="dur-txt"> Assign date: {{isset($proRec['recruiter_assign_date'])?date('d M Y',strtotime($proRec['recruiter_assign_date'])):'-'}}</span><span class="freelancer-user-line">|</span>
        </div>
        
        <div class="search-freelancer-user-location"> 
          <span class="gavel-icon"><img src="https://192.168.1.77/archexperts/public/front/images/calendar.png" alt=""></span> <span class="dur-txt"> Due date: {{isset($proRec['bid_closing_date'])?date('d M Y',strtotime($proRec['bid_closing_date'])):'-'}}</span>
        </div>
        <div class="more-project-dec">{{str_limit($proRec['project_description'],350)}}</div>
      </div>
      
      <div class="row">

        <div class="category-new">{{$proRec['category_details']['category_title'] or 'NA'}}</div>
        <div class="clearfix"></div>

        @if(isset($proRec['project_skills']) && count($proRec['project_skills']) > 0)
        @foreach($proRec['project_skills'] as $key=> $skills)
        @if(isset($skills['skill_data']['skill_name']) && $skills['skill_data']['skill_name'] != "")
          <div class="skils-project">
              <ul>
                <li>{{ str_limit($skills['skill_data']['skill_name'],25) }}</li>
              </ul>
          </div>
        @endif
        @endforeach
        @endif

        <div class="col-sm-12 col-md-12 col-lg-12">  <a href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}" class="view_btn hidden-xs hidden-sm hidden-md">
          <i class="fa fa-eye" aria-hidden="true"></i> {{trans('project_manager/projects/open_projects.text_view')}} 
          </a>
        </div>
      </div>
    </div>
    <div class="clr"></div>
    <hr/>
    @endforeach
    @else
      <div class="search-grey-bx">
          <div class="no-record" >
             {{trans('expert/projects/awarded_projects.text_sorry_no_record_found')}}
          </div>
      </div>
    @endif

  </div>
    <!-- Paination Links -->
      @include('front.common.pagination_view', ['paginator' => $arr_pagination])
    <!-- Paination Links -->

   </form>
</div>

<script type="text/javascript">
  function sortByHandledBy(ref){  
      var sort_by = $(ref).val();
      if(sort_by != ""){
        window.location.href="{{ url('project_manager/projects/open') }}"+"?sort_by="+sort_by;
      }
      else{
        window.location.href="{{ url('project_manager/projects/open') }}";
      }
      return true;
    }
</script>

@stop