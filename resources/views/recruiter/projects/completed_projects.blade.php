@extends('recruiter.layout.master')                
@section('main_content')


<div class="col-sm-7 col-md-8 col-lg-9">
<div class="row">
    <form action="{{ url($module_url_path) }}/manage" method="POST" id="form-manage-projects" name="form-manage-projects" enctype="multipart/form-data" files="true">
        {{ csrf_field() }}
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="right_side_section">
              <div class="col-lg-8">
                @include('front.layout._operation_status')
                <div class="head_grn">{{trans('project_manager/projects/completed_projects.text_completed_project_title')}}</div>
              </div>
              <div class="col-lg-4">
            <div class="droup-select">
            <select class="droup mrns tp-margn" onchange="sortByHandledBy(this)">
              <option value="">-- Sort By --</option>
              <option @if(isset($sort_by) && $sort_by == 'asc') selected="" @endif value="asc">Ascending</option>
              <option @if(isset($sort_by) && $sort_by == 'desc') selected="" @endif value="desc">Descending</option>
            </select>
            </div>
    </div>
    <div class="clearfix"></div>
                @if(isset($arr_completed_projects['data']) && sizeof($arr_completed_projects['data'])>0) @foreach($arr_completed_projects['data'] as $proRec)
                <div class="ongonig-project-section client-dash">
                    <div class="project-title">

                        <a href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}">
                            <h3>{{$proRec['project_name']}}</h3>
                        </a>
                        
                        <div class="search-freelancer-user-location"> <span class="gavel-icon"><img src="https://192.168.1.77/archexperts/public/front/images/clock.png" alt=""></span> <span class="dur-txt"> {{trans('project_manager/projects/open_projects.text_project_duration')}}: {{$proRec['project_expected_duration']}} {{trans('project_manager/projects/open_projects.text_days')}}</span>
                        </div>
                        <div class="more-project-dec">{{str_limit($proRec['project_description'],350)}}</div>
                    </div>
                    
                    <div class="row">
                      <div class="category-new">{{$proRec['category_details']['category_title'] or 'NA'}}</div>
                      <div class="clearfix"></div>

                      @if(isset($proRec['project_skills']) && count($proRec['project_skills']) > 0)
                      @foreach($proRec['project_skills'] as $key=> $skills)
                      @if(isset($skills['skill_data']['skill_name']) && $skills['skill_data']['skill_name'] != "")
                        <div class="skils-project">
                            <ul>
                              <li>{{ str_limit($skills['skill_data']['skill_name'],25) }}</li>
                            </ul>
                        </div>
                      @endif
                      @endforeach
                      @endif
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12"></div>
                </div>
                    
                    

                </div>
                <hr/> @endforeach @else
                <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;">
                    <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                        <div class="search-content-block">
                            <div class="search-head" style="color:red;">
                                {{trans('project_manager/projects/completed_projects.text_no_record_found')}}
                            </div>
                        </div>
                    </td>
                </tr>
                @endif


            </div>
            <!-- Pagination -->
            @include('front.common.pagination_view', ['paginator' => $arr_completed_projects])

        </div>
    </form>
</div>
</div>
<script type="text/javascript">
  function sortByHandledBy(ref){  
      var sort_by = $(ref).val();
      if(sort_by != ""){
        window.location.href="{{ url('recruiter/projects/completed') }}"+"?sort_by="+sort_by;
      }
      else{
        window.location.href="{{ url('recruiter/projects/completed') }}";
      }
      return true;
    }
</script>

@stop