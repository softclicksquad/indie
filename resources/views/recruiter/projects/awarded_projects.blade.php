@extends('recruiter.layout.master')
@section('main_content')
<div class="col-sm-7 col-md-8 col-lg-9">
<div class="row">
   <div class="col-sm-12 col-md-12 col-lg-12">
      <div class="right_side_section proect-listing dispute-projects">
        <div class="col-lg-8">
         @include('front.layout._operation_status')
         <div class="head_grn">{{trans('client/projects/awarded_projects.text_awarded_title')}}</div>
        </div>
          <div class="col-lg-4">
            <div class="droup-select">
            <select class="droup mrns tp-margn" onchange="sortByHandledBy(this)">
              <option value="">-- Sort By --</option>
              <option @if(isset($sort_by) && $sort_by == 'asc') selected="" @endif value="asc">Ascending</option>
              <option @if(isset($sort_by) && $sort_by == 'desc') selected="" @endif value="desc">Descending</option>
              {{-- <option @if(isset($sort_by) && $sort_by == 'is_urgent') selected="" @endif value="is_urgent">Urgent</option>
              <option @if(isset($sort_by) && $sort_by == 'public_type') selected="" @endif value="public_type">Public</option>
              <option @if(isset($sort_by) && $sort_by == 'private_type') selected="" @endif value="private_type">Private</option>
              <option @if(isset($sort_by) && $sort_by == 'is_nda') selected="" @endif value="is_nda">NDA</option> --}}
            </select>
            </div>
          </div>
          <div class="clearfix"></div>
         @if(isset($arr_awarded_projects['data']) && sizeof($arr_awarded_projects['data'])>0)
         @foreach($arr_awarded_projects['data'] as $awardedRec)
         <div class="white-block-bg-no-padding">
         <div class="white-block-bg big-space hover">
             <div class="search-project-listing-left">
                 <div class="project-title">
                       <a href="{{ $module_url_path }}/details/{{ base64_encode($awardedRec['id'])}}">
                          <h3>{{$awardedRec['project_name']}}</h3>
                       </a>
                       <div class="more-project-dec">{{str_limit($awardedRec['project_description'],350)}}</div>
                 </div>
                <div class="skill-de-content">
                  <div class="skils-project">
                     <ul>
                        @if(isset($awardedRec['project_skills'])  && count($awardedRec['project_skills']) > 0)
                        @foreach($awardedRec['project_skills'] as $key=>$skills) 
                        @if(isset($skills['skill_data']['skill_name']) &&  $skills['skill_data']['skill_name'] != "")
                        <li style="font-size:12px; " >{{ str_limit($skills['skill_data']['skill_name'],18) }}</li>
                        @endif
                        @endforeach
                        @endif    
                     </ul>
                  </div>
                </div>
                <a class="black-btn" href="{{ $module_url_path }}/details/{{ base64_encode($awardedRec['id'])}}">{{--  class="view_btn hidden-xs hidden-sm hidden-md"> --}}
                {{trans('client/projects/awarded_projects.text_view')}} 
                </a>
             </div>
            <div class="clearfix"></div>
            
         </div>
         </div>
         @endforeach
         @else
              <div class="search-grey-bx">
                  <div class="no-record" >
                     {{trans('expert/projects/awarded_projects.text_sorry_no_record_found')}}
                  </div>
              </div>
         @endif
      </div>
      <!-- Paination Links -->
      @include('front.common.pagination_view', ['paginator' => $arr_pagination])
      <!-- Paination Links -->
   </div>
   </div>
   </div>
</div>
<script type="text/javascript">
  function sortByHandledBy(ref){  
      var sort_by = $(ref).val();
      if(sort_by != ""){
        window.location.href="{{ url('project_manager/projects/awarded') }}"+"?sort_by="+sort_by;
      }
      else{
        window.location.href="{{ url('project_manager/projects/awarded') }}";
      }
      return true;
    }
</script>
@stop
