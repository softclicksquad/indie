<?php
   $user = Sentinel::check();
   if(!$user)
   {
     return redirect('/login');
   }
   ?>
<!--  Here Front Layout is used -->
@extends('front.layout.master')                
@section('main_content')
<div class="top-title-pattern">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="title-box-top">
               <h3>{{ $page_title or ''}}</h3>
            </div>
         </div>
      </div>
   </div>
</div>
<?php
   $arr_threads = [];
   
   if(isset($arr_notification['thread_info_of_msgs']) && count($arr_notification['thread_info_of_msgs']) > 0)
   {
     foreach ($arr_notification['thread_info_of_msgs'] as $key => $msg) 
     {
       if($msg['thread_id'] != "" && ( (int) $msg['unread_messages'] > 0 ) ) 
       {
         array_push($arr_threads,$msg['thread_id']);
       }
     }
   }
   ?>
<div class="middle-container">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="row">
               <div class="col-md-4 col-lg-3">
                  <div class="dash left-side-tabs profile-l ">
                     <ul class="sub_respo1">
                        <!--search section-->
                        <li class="search_blue">
                           <input type="text" class="search_msg" name="search" id="search"/>
                        </li>
                        <!--search section-->
                        <div class="insta_gram content-kgfd1" id="insta_gram">
                           @if(isset($arr_sidebar_data) && count($arr_sidebar_data)>0)
                           @foreach($arr_sidebar_data as $key => $chat_data )
                           <?php
                              $is_unread = '0';
                                      
                              if(isset($chat_data['message_thread_id']) && $chat_data['message_thread_id'] != "")
                              {
                                if(in_array($chat_data['message_thread_id'],$arr_threads))
                                {
                                  $is_unread = '1';
                                }
                              }
                              
                              $recipient_first_name = isset($chat_data['chat_participants'][0]['role_info'][0]['first_name']) ? $chat_data['chat_participants'][0]['role_info'][0]['first_name']:'';
                              $recipient_last_name = isset($chat_data['chat_participants'][0]['role_info'][0]['last_name']) ? $chat_data['chat_participants'][0]['role_info'][0]['last_name']:'';
                              
                              
                              $recipient_name = "---";  
                              $_user_id  = isset($chat_data['chat_participants']['0']['user_id']) ? $chat_data['chat_participants']['0']['user_id']:"";
                              
                               if(isset($_user_id) && $_user_id != "")
                               {
                                 $user = Sentinel::findById($_user_id);
                                 if(isset($user) && $user != FALSE)
                                 { 
                                   if(isset($user->user_name))
                                   {
                                     $recipient_name = $user->user_name;
                                   }
                                 }
                               }
                              
                              /*$recipient_name = $recipient_first_name.' '.$recipient_last_name;*/
                              
                              $message_body = isset($chat_data['messages'][0]['body']) ? $chat_data['messages'][0]['body']:'';
                              
                              $message_time = isset($chat_data['messages'][0]['time_ago']) ? $chat_data['messages'][0]['time_ago']:'';
                              if($message_time == "")
                              {
                                $message_time = "No chat";
                              }
                              
                              $project_id   = isset($chat_data['project_id']) ? base64_encode($chat_data['project_id']) : '';
                              $recipient_id = isset($chat_data['chat_participants'][0]['user_id']) ? base64_encode($chat_data['chat_participants']['0']['user_id']) : '';
                              
                              $profile_image = isset($chat_data['chat_participants'][0]['role_info'][0]['profile_image']) ?$chat_data['chat_participants'][0]['role_info'][0]['profile_image']:'';
                              
                              if($profile_image == "")
                              {
                                $profile_image = "default_profile_image.png";
                              }
                              
                              $msg_class = "";
                              
                              $url_project_id =  \Request::segment(2);
                              $url_recipient_id =  \Request::segment(3);
                              
                              $side_bar_project_name = isset($chat_data['project_data']['project_name'])?$chat_data['project_data']['project_name']:"";
                              
                              if(($recipient_id == $url_recipient_id) && ($project_id == $url_project_id ) )
                              {
                                $msg_class ="active-msg"; 
                              }
                              
                              ?>
                           <li class="{{$msg_class}}" style="cursor: pointer;" onclick="go_to_conversation('{{ $project_id }}','{{ $recipient_id }}')" >
                              <div class="massanger_user">
                                 <div class="user_img"><img alt="" src="{{ url('/')}}/uploads/front/profile/{{$profile_image}}"/>
                                    @if($is_unread == '1')
                                    <i class="fa fa-circle" style="padding-left:18px;padding-top:4px;color: #2ee66e"></i>
                                    @elseif($message_body != "")
                                    <i class="fa fa-circle" style="padding-left:18px;padding-top:4px;color: #b0b0b0"></i>
                                    @else
                                    <i class="fa fa-circle" style="padding-left:18px;padding-top:4px;color: #ededed"></i>
                                    @endif
                                 </div>
                                 <div class="user_details">
                                    <div class="time" style="float: right;font-size:11px; position: absolute; right: 0px; top: -5px;"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;{{ $message_time }}</div>
                                    <div class="user_name">{{str_limit($side_bar_project_name,20) }}</div>
                                    <div>( {{str_limit($recipient_name,20) }} )</div>
                                    <div class="user_msg">@if($message_body != "" ){{ str_limit($message_body,20) }}@else {{ 'No Chat' }} @endif</div>
                                 </div>
                              </div>
                           </li>
                           @endforeach 
                           @endif
                        </div>
                     </ul>
                     <!--message conversion end here-->
                  </div>
               </div>
               <?php 
                  $user = Sentinel::check();
                  
                  $project_link = url('/login');
                  
                  if($user)
                  {
                      if(isset($user) && $user->inRole('project_manager'))
                      {
                        $project_link =  url("/project_manager/projects/details/".$enc_project_id);  
                      } 
                      elseif (isset($user) && $user->inRole('client')) 
                      {
                        $project_link = url("/client/projects/details/".$enc_project_id); 
                      }
                      elseif (isset($user) && $user->inRole('expert')) 
                      {
                        $project_link = url("/expert/projects/details/".$enc_project_id);
                      }
                  }
                  
                  ?>
               <div class="col-md-8 col-lg-9">
                  <div class="right_side_section">
                     <div>
                        <div class="title_convers">
                           <a href="{{$project_link}}">{{ $project_name  or '' }} </a><span style="font-size: 15px;color:#697070;">(&nbsp;{{ trans('conversation_and_inbox.text_chat') }}&nbsp;)</span>
                           <span style="font-size: 15px;color:#697070;">
                              &nbsp;@if(isset($arr_user_available['is_available']) && $arr_user_available['is_available']=="1")
                              <!-- {{"Available"}} -->{{ trans('conversation_and_inbox.text_available') }}
                              @else
                              {{"Not available"}}
                              @endif&nbsp;
                           </span>
                        </div>
                        <div class="insta_gram content-kgfd" id="insta_gram">
                           <div id="conversation_id" style="padding-top: 10px;">
                              @if(isset($arr_conversation) && count($arr_conversation) > 0 )
                              @foreach($arr_conversation as $key=> $chat )
                              <?php
                                 $profile_chat_image  =  isset($chat['role_info'][0]['profile_image']) ? $chat['role_info'][0]['profile_image']:'';
                                 if($profile_chat_image == "")
                                 {
                                   $profile_chat_image = "default_profile_image.png";
                                 }
                                 
                                 $is_project_manager = 0;
                                 
                                 $check_user = Sentinel::findById($chat['user_id']);  
                                 
                                 if($check_user->inRole('project_manager'))
                                 {
                                   $is_project_manager = 1;
                                 }
                                 
                                 $_user_name = "---";  
                                 $_chat_user_id  = isset($chat['user_id']) ? $chat['user_id']:"";
                                 
                                  if(isset($_chat_user_id) && $_chat_user_id != "")
                                  {
                                    $_user = Sentinel::findById($_chat_user_id);
                                    if(isset($_user) && $_user != FALSE)
                                    { 
                                      if(isset($_user->user_name))
                                      {
                                        $_user_name = $_user->user_name;
                                      }
                                    }
                                  }
                                 
                                 ?>  
                              @if($chat['user_id'] == Sentinel::check()->id)
                              <div class="chat_right_side">
                                 <span><img src="{{ url('/')}}/uploads/front/profile/{{ $profile_chat_image }}" alt=""/></span>
                                 <div class="triangle-left right">
                                    <b @if($is_project_manager != 1) style="cursor: pointer;" onclick="showUserDetails(this);" @endif data-user-id="{{ isset($chat['user_id'])?base64_encode($chat['user_id']):'' }}">{{$_user_name or ''}}</b>
                                    {{-- <span>( {{ isset($project_name) ? str_limit($project_name,18):'' }} )</span> --}}
                                    <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;{{ $chat['time_ago']  or ''}}
                                    </div>
                                    <p style="color: #131514" >{{$chat['body']}}</p>
                                 </div>
                                 <div class="clr"></div>
                              </div>
                              @else
                              <div class="chat_left_side">
                                 <span><img src="{{ url('/')}}/uploads/front/profile/{{ $profile_chat_image }}" alt=""/></span> 
                                 <div class="triangle-right left">
                                    <b @if($is_project_manager != 1) style="cursor: pointer;" onclick="showUserDetails(this);" @endif  data-user-id="{{ isset($chat['user_id'])?base64_encode($chat['user_id']):'' }}" >{{$_user_name or ''}}</b>
                                    {{-- <span>( {{ isset($project_name) ? str_limit($project_name,18):'' }} )</span> --}}
                                    <div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;{{ $chat['time_ago']  or ''}}</div>
                                    <p style="color: #131514">{{$chat['body']}}</p>
                                 </div>
                                 <div class="clr"></div>
                              </div>
                              @endif
                              @endforeach
                              @else
                              <div style="margin-bottom: 30px;">
                                 <center>
                                    <h4>
                                       {{ trans('conversation_and_inbox.text_no_conversations')  }}   
                                    </h4>
                                 </center>
                              </div>
                              @endif  
                           </div>
                        </div>
                        <div class="clr"></div>
                        <div class="msg_input">
                           <input type="hidden" name="project_name" readonly="" value="{{ isset($project_name) ? str_limit($project_name,18):'' }}"></input>
                           <input type="hidden" name="thread_id"  readonly="" value="{{ isset($thread_id) ? base64_encode($thread_id):'' }}"></input>
                           <input type="hidden" name="user_id"  readonly="" value="{{ isset($user->id) ? base64_encode($user->id):'' }}"></input>
                           <input type="hidden" name="arr_conversation_count" readonly="" value="{{ isset($arr_conversation) ? count($arr_conversation):'0' }}" ></input>
                           <input type="text" placeholder="{{ trans('conversation_and_inbox.text_type_your_message_here') }}" name="message" tabindex="1" class="msg-in"/>
                           <a onclick="send_message()" style="cursor: pointer;" class="submit-msg"><i aria-hidden="true" tabindex="2" style="margin-left:20px;margin-top:10px; " class="fa fa-paper-plane"></i></a>
                           <div class="clearfix"></div>
                           <span class="error" id="error_msg"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
   $('#frm_send_message').validate();
   
     setInterval(function() {
        
     }, 5000);      
   
    /* custom scrollbars plugin*/
     (function($){
         $(window).load(function(){
             $(".content-kgfd").mCustomScrollbar({
                 scrollButtons:{
                     enable:true
                 },
                
             });
             
             /* This function drags scroller down */                
   
             scroll_bottom();
   
             $(".content-kgfd1").mCustomScrollbar({
                 scrollButtons:{
                     enable:true
                 }
             });
         });
     })(jQuery);
   
   function scroll_bottom()
   {
      $('.content-kgfd').mCustomScrollbar("scrollTo","bottom",{
                 scrollInertia:2000
           });
   }
   
   /* custom scrollbars plugin */
   
   function go_to_conversation(project_id,recipient_id) 
   {
     if(recipient_id == "") { return false; }
     if(project_id == "")   { return false; }   
     window.location.href = "{{url('/public')}}"+"/conversation/"+project_id+'/'+recipient_id;   
   }
   
   function send_message() 
   {
     thread_id = $('input[name="thread_id"]').val();
     message   = $('input[name="message"]').val();
     project_name  = $('input[name="project_name"]').val();
   
     arr_conversation_count  = $('input[name="arr_conversation_count"]').val();
     
     if(message == "")
     {
       $('#error_msg').fadeIn(1000);
       $('#error_msg').html('Please Enter your message.');
       $('#error_msg').fadeOut(5000);
       return false;
     }
   
     $.ajax({
        url: "{{url('/public')}}/message/send?_token="+"{{ csrf_token() }}",
        type: 'POST',
        data : {thread_id:thread_id , message:message },
        success: function(response) 
        {
           $('input[name="message"]').val(''); 
           if(arr_conversation_count == '0' )
           {
             window.location.reload();
             return false;
           }
   
           if(response.status == "SUCCESS")
           { 
             var str = "";
             str += '<div class="chat_right_side"><span><img src="{{ url('/')}}/uploads/front/profile/'+response.profile_image+'"alt=""/></span>';
             str += '<div class="triangle-left right">';
             str +='<b>'+response.user_name+'</b>';
             str +='<div class="time"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;'+response.time_ago+'</div>';
             str +='<p style="color: #131514">'+message+'</p></div><div class="clr"></div></div>'; 
             
             $('#conversation_id').append(str);
             scroll_bottom();  
           }  
        },
     }); 
   }
   
   
   $(document).ready(function()
   {
   $('#search').keyup(function()
   {
     searchContent($(this).val());
   });
   
   /* Updating last seen in threads table on page load with ajax call */
   
   var thread_id = $('input[name="thread_id"]').val();
   var user_id = $('input[name="user_id"]').val();
   var json = { thread_id:thread_id ,user_id:user_id };
   
   if(thread_id != "" && user_id != "" )
   {
       $.ajax({
         data:json,
         dataType:'json',
         url : "{{ url('/')}}/message/update_last_read",
         type : 'get',
         success : function (response)
         { 
           if(response.status == "SUCCESS")
           {
             console.log('Last Read Updated Successfully');
           }
         }
       });
   }
   
   /* ends */
   });
   
   function searchContent(inputVal)
   {
   var search_section = $('#insta_gram');
   
   search_section.find('li').each(function(index, row)
   {
     var allCells = $(row).find('.user_details');
     if(allCells.length >0)
     {
       var found = false;
       allCells.each(function(index, div)
       {
         var regExp = new RegExp(inputVal, 'i');
         if(regExp.test($(div).text()))
         {
           found = true;
           return false;
         }
       });
       if(found == true)$(row).show();else $(row).hide();
     }
   });
   }
   
   /* to show user details page */
   
   function showUserDetails(ref)
   {
   var user_id = $(ref).attr('data-user-id');
   
   if(user_id != "")
   {
     window.location.href="{{url('/')}}"+"/user_details/"+user_id;
   } 
   }
   
</script>
@stop

