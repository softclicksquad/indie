<?php
  $user = Sentinel::check();
  if ($user->inRole('project_manager'))
  {
    $folder     = "project_manager";  
  } 
  elseif ($user->inRole('client')) 
  {
    $folder     = "client";
  }
  elseif ($user->inRole('expert')) 
  {
    $folder     = "expert";
  }
?>

@extends($folder.'.layout.master')  

@section('main_content')

    <?php
      $arr_threads = [];
      
      if(isset($arr_notification['thread_info_of_msgs']) && count($arr_notification['thread_info_of_msgs']) > 0)
      {
        foreach ($arr_notification['thread_info_of_msgs'] as $key => $msg) 
        {
          if($msg['thread_id'] != "" && ( (int) $msg['unread_messages'] > 0 ) ) 
          {
            array_push($arr_threads,$msg['thread_id']);
          }
        }
      }
    ?>



    <div class="col-sm-7 col-md-8 col-lg-9 margin-top-50">
      <div class="right_side_section">
          <div class="head_grn" style="border-bottom:0;"><i class="fa fa-envelope"></i> {{ trans('conversation_and_inbox.text_messages') }} 
          <span style="float: right;font-size: 15px;cursor: pointer;" ><a onclick="window.location.reload()" ><i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;{{ trans('conversation_and_inbox.text_refresh') }} </a></span>
          </div>

          <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12">
           
            <div class="content-kgfd">
              <div class="table-responsive">
               <table cellspacing="0" cellpadding="0" border="0" width="100%" class="table addrsbox ">
                  <tbody>


                    @if(isset($arr_conversation) && count($arr_conversation) > 0 )  
                      @foreach($arr_conversation as $key => $chat_data )

                      <?php
                        $is_unread = '0';
                        if(isset($chat_data['message_thread_id']) && $chat_data['message_thread_id'] != "")
                        {
                          if(in_array($chat_data['message_thread_id'],$arr_threads))
                          {
                            $is_unread = '1';
                          }
                        }

                        $project_name = isset($chat_data['project_data']['project_name'])?str_limit($chat_data['project_data']['project_name'],18):'';
                        $user_name    = "---";  
                        $_user_id     = isset($chat_data['chat_participants']['0']['user_id']) ? $chat_data['chat_participants']['0']['user_id']:"";
                         if(isset($_user_id) && $_user_id != "")
                         {
                           $user = Sentinel::findById($_user_id);
                           if(isset($user) && $user != FALSE)
                           { 
                             if(isset($user->user_name))
                             {
                               $user_name = $user->user_name;
                             }
                           }
                         }
                         /*$first_name  = isset($chat_data['chat_participants']['0']['role_info']['0']['first_name']) ?$chat_data['chat_participants']['0']['role_info']['0']['first_name']:"";
                         $last_name   = isset($chat_data['chat_participants']['0']['role_info']['0']['last_name']) ?substr($chat_data['chat_participants']['0']['role_info']['0']['last_name'], 0,1).'.':"";
                         $user_name        = $first_name.' '.$last_name;*/

                      ?>

                     <tr class="cont_text" style="cursor: pointer;" onclick="go_to_conversation('{{base64_encode($chat_data['project_id'])}}','{{isset($chat_data['chat_participants']['0']['user_id']) ? base64_encode($chat_data['chat_participants']['0']['user_id']) :"" }}')">
                     

                        <td><span><i class="fa fa-circle {{get_message_user_class_by_role($_user_id)}}"></i></span></td>

                        <a href="">

                         @if($is_unread == '1') 
                            <td><b>{{$user_name or ''}} 
                            &nbsp;@if($project_name != ""){{ '( '.$project_name.' )' }} @endif
                            </b></td>
                          @else 
                            <td>{{$user_name or ''}} 
                            &nbsp;@if($project_name != ""){{ '( '.$project_name.' )' }} @endif
                            </td>
                          @endif 

                        </a>


                        
                        @if(isset($chat_data['messages'][0]['body']))
                          @if($is_unread == '1') 
                            <td><b>{{ str_limit($chat_data['messages'][0]['body'],30) }}</b></td>
                          @else 
                            <td>{{ str_limit($chat_data['messages'][0]['body'],30) }}</td>
                          @endif  
                        @else  
                          <td>{{trans('conversation_and_inbox.text_no_message')}}</td>
                        @endif
  
                        @if(isset($chat_data['messages'][0]['created_at']) && ( strtotime(date('Y-m-d',strtotime($chat_data['messages'][0]['created_at']))) == strtotime(date('Y-m-d'))) )
                          <td style="text-align: right;" >{{ date("H:i A", strtotime($chat_data['messages'][0]['created_at'])) }}</td>
                        @elseif(isset($chat_data['messages'][0]['created_at']))  
                          <td style="text-align: right;" >{{ date("d M Y", strtotime($chat_data['messages'][0]['created_at'])) }}</td>
                        @else
                          <td style="text-align: right;">--</td>
                        @endif
                     
                     </tr>
                      @endforeach
                    @else   
                      <tr class="cont_text">                                   
                        <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                            <div class="search-grey-bx">
                                <div class="search-head" style="color:rgba(45, 45, 45, 0.8);">
                                   {{trans('conversation_and_inbox.text_sorry_no_messages_found')}}  
                                </div>                                                        
                            </div>
                        </td>
                      </tr>
                    @endif 
                  </tbody>
               </table>
               </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  

<script type="text/javascript">
  
  function go_to_conversation(project_id,recipient_id) 
  {
    if(recipient_id == "") { return false; }
    if(project_id == "")   { return false; }   
    window.location.href = "{{url('/public')}}"+"/conversation/"+project_id+'/'+recipient_id;   
  }

</script>

<!-- cusotm scrollbar start -->
 <script>
    (function($){
        $(window).load(function(){
            $(".content-kgfd").mCustomScrollbar({
                scrollButtons:{
                    enable:true
                }
            });
            
        });
    })(jQuery);
  </script>
<!-- cusotm scrollbar ends -->
@stop
