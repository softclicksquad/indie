@extends('project_manager.layout.master')                
@section('main_content')

<div class="col-sm-7 col-md-8 col-lg-9">
   <div class="right_side_section min-height">
      <div class="head_grn">{{ trans('project_manager/profile/change_password.text_heading') }}</div>
      @include('front.layout._operation_status')
      <form action="{{url('/project_manager/update_password')}}" method="POST" id="form-change-password" name="form-change-password">
         {{ csrf_field() }}
         <div class="row">
            <div class="col-lg-6">
               <div class="change-pwd-form">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('project_manager/profile/change_password.text_current_password') }}:</div>
                     <div class="input-name"><input type="password" placeholder="{{ trans('project_manager/profile/change_password.entry_current_password') }}" class="clint-input" name="current_password" id="current_password" data-rule-required='true'></div>
                     <span class='error'>{{ $errors->first('current_password') }}</span>
                  </div>
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('project_manager/profile/change_password.text_new_password') }}:</div>
                     <div class="input-name"><input type="password" placeholder="{{ trans('project_manager/profile/change_password.entry_new_password') }}" class="clint-input" name="password" id="password"></div>
                     <span class='error'>{{ $errors->first('password') }}</span>
                  </div>
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('project_manager/profile/change_password.text_confirm_password') }}:</div>
                     <div class="input-name"><input type="password" placeholder="{{ trans('project_manager/profile/change_password.entry_confirm_password') }}" class="clint-input" name="confirm_password" id="confirm_password" data-rule-required='true' data-rule-equalto="#password"></div>
                     <span class='error'>{{ $errors->first('confirm_password') }}</span>
                  </div>
                  <button class="normal-btn pull-right" type="submit">{{ trans('project_manager/profile/change_password.text_update') }}</button>
               </div>
            </div>
            <div class="col-lg-3">&nbsp;</div>
         </div>
      </form>
   </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $.validator.addMethod("pwcheck",
   function(value, element) {
      return /^^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/.test(value);
   });
});

    $("#form-change-password").validate({
      errorElement: 'span',
       rules: {
     current_password: 
               {
                 required: true,
                 pwcheck: true,
                 minlength: 8,
               },
      password: 
               {
                 required: true,
                 pwcheck: true,
                 minlength: 8,
                 
               },
      confirm_password: 
               {
                 required: true,
                 pwcheck: true,
                 minlength: 8,
                 
               },
         },
    @if(isset($selected_lang) && App::isLocale('de'))
     messages: 
     {
     current_password:
           {
             required: "Dieses Feld ist ein Pflichtfeld..",
             pwcheck: "Das Passwort muss in Großbuchstaben haben, Kleinschreibung und eine Ziffer",
             minlength:"Passwort erforderlich mindestens 8 Zeichen.",
   
           },
     password:
           {
             required: "Dieses Feld ist ein Pflichtfeld.",
             pwcheck: "Das Passwort muss in Großbuchstaben haben, Kleinschreibung und eine Ziffer.",
             minlength:"Passwort erforderlich mindestens 8 Zeichen.",
   
           },
    confirm_password: 
           {
              required: "Dieses Feld ist ein Pflichtfeld.",
              pwcheck: "Das Passwort muss in Großbuchstaben haben, Kleinschreibung und eine Ziffer.",
              minlength:"Passwort erforderlich mindestens 8 Zeichen.",
           },
       }
    @else

      messages: 
     {
     current_password:
           {
             required: "This field is required.",
             pwcheck: "Password must have uppercase lowercase and one digit.",
             minlength:"Password required minumum 8 characters.",
   
           },
     password:
           {
             required: "This field is required.",
             pwcheck: "Password must have uppercase,lowercase and one digit.",
             minlength:"Password required minumum 8 characters.",
   
           },
    confirm_password: 
           {
              required: "This field is required.",
              pwcheck: "Password must have uppercase,lowercase and one digit.",
              minlength:"Password required minumum 8 characters.",
           }
       }

    @endif
    });
   
     
</script>
@stop