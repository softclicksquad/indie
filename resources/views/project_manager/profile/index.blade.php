@extends('project_manager.layout.master')                
@section('main_content')

<div class="col-sm-7 col-md-8 col-lg-9">
   <form action="{{ url($module_url_path) }}/update" method="POST" id="form-client_profile" name="form-client_profile" enctype="multipart/form-data" files ="true">
      {{ csrf_field() }}
      <div class="right_side_section">
         @include('front.layout._operation_status')
         <div class="head_grn">{{ trans('project_manager/profile/profile.text_heading') }}</div>
         <div class="row">
            <div class="change-pwd-form">
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('project_manager/profile/profile.text_first_name') }}<span>*</span></div>
                     <div class="input-name">
                        <input type="text" class="clint-input" placeholder="{{ trans('project_manager/profile/profile.entry_first_name') }}" name="first_name" id="first_name" value="{{isset($arr_project_manager['first_name'])?$arr_project_manager['first_name']:''}}" data-rule-required="true" data-rule-maxlength="250" onkeyup="return chk_validation(this);"/>
                        <span class='error'>{{ $errors->first('first_name') }}</span>
                     </div>
                  </div>
               </div>
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('project_manager/profile/profile.text_last_name') }}<span>*</span></div>
                     <div class="input-name"><input type="text" class="clint-input" placeholder="{{ trans('project_manager/profile/profile.entry_last_name') }}" name="last_name" id="last_name" value="{{isset($arr_project_manager['last_name'])?$arr_project_manager['last_name']:''}}" data-rule-required="true" data-rule-maxlength="250" onkeyup="return chk_validation(this);"/>
                        <span class='error'>{{ $errors->first('last_name') }}</span>
                     </div>
                  </div>
               </div>
               <div class='clr'></div>
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('project_manager/profile/profile.text_email') }}<span>*</span></div>
                     <div class="input-name"><input type="text" class="clint-input" placeholder="{{ trans('project_manager/profile/profile.entry_email') }}"  id="email" value="{{isset($arr_project_manager['user_details']['email'])?$arr_project_manager['user_details']['email']:''}}" disabled="true" data-rule-required="true" /></div>
                  </div>
               </div>

               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('project_manager/profile/profile.text_phone_number') }}<span>*</span></div>
                     <div class="input-name">
                      <?php /*   <input type="text" class="clint-input" 
                           placeholder="{{ trans('project_manager/profile/profile.entry_phone_number') }}" 
                           name="phone_number" id="phone_number" 
                           value="{{isset($arr_project_manager['phone'])?$arr_project_manager['phone']:''}}" 
                           data-rule-required="true" data-rule-maxlength="17" data-rule-minlength="10" data-rule-number="true"/> */ ?>
                        
                        <div class="row">
                           <div class="col-xs-3 col-sm-2 col-md-3 col-lg-2"><input type="text" class="clint-input" value="+" readonly></div>
                           
                           <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 p-l-r"><input type="text" class="clint-input" placeholder="{{ trans('project_manager/profile/profile.entry_phone_code') }}" name="phone_code" id="phone_code" value="{{isset($arr_project_manager['phone_code'])?$arr_project_manager['phone_code']:''}}" data-rule-required="true" data-rule-maxlength="6" data-rule-minlength="2" data-rule-number="true" onkeyup="return chk_validation(this);">
                            <span class='error'>{{ $errors->first('phone_code') }}</span>
                           </div>


                           <div class="col-xs-7 col-sm-8 col-md-7 col-lg-8"><input type="text" class="clint-input"  placeholder="{{ trans('project_manager/profile/profile.entry_phone_number') }}" 
                           name="phone_number" id="phone_number" 
                           value="{{isset($arr_project_manager['phone'])?$arr_project_manager['phone']:''}}" 
                           data-rule-required="true" data-rule-maxlength="17" data-rule-minlength="10" data-rule-number="true"></div>
                        </div>
                       
                        <span class='error'>{{ $errors->first('phone_number') }}</span>
                     </div>
                  </div>
               </div>
               <div class='clr'></div>
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('project_manager/profile/profile.text_user_name') }}<span>*</span></div>
                     <div class="input-name"><input disabled="" type="text" class="clint-input" name="user_name" placeholder="{{ trans('project_manager/profile/profile.text_user_name') }}" data-rule-maxlength="100" id="user_name" value="{{isset($arr_project_manager['user_details']['user_name'])?$arr_project_manager['user_details']['user_name']:''}}" data-rule-required="true" /></div>
                  </div>
               </div>

               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('project_manager/profile/profile.text_country') }}<span>*</span></div>
                     <div class="input-name">
                        <div class="droup-select">
                           <select class="droup" data-rule-required="true" name="country" id="country_client" onchange="javascript: return loadStates(this);">
                              <option value="">{{ trans('project_manager/profile/profile.entry_country') }}</option>
                              @if(isset($arr_countries) && sizeof($arr_countries)>0)
                              @foreach($arr_countries as $countries)
                              <option value="{{isset($countries['id'])?$countries['id']:""}}" 
                              @if(isset($arr_project_manager['country']) && $countries['id']==$arr_project_manager['country'])
                              {{'selected'}}
                              @endif
                              >
                              {{isset($countries['country_name'])?$countries['country_name']:"" }}
                              </option>
                              @endforeach
                              @endif
                           </select>
                           <span class='error'>{{ $errors->first('country') }}</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class='clr'></div>
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('project_manager/profile/profile.text_state') }}<span>*</span></div>
                     <div class="input-name">
                        <div class="droup-select">
                           <select class="droup" data-rule-required="true" name="state" id="state_client" onchange="javascript: return loadCities(this);">
                              <option value="">{{ trans('project_manager/profile/profile.entry_state') }}</option>
                              @if(isset($arr_project_manager['country_details']['states']) && sizeof($arr_project_manager['country_details']['states'])>0)
                              @foreach($arr_project_manager['country_details']['states'] as $states)
                              <option value="{{isset($states['id'])?$states['id']:""}}"
                              @if($states['id']==$arr_project_manager['state'])
                              selected="true" 
                              @endif
                              >
                              {{isset($states['state_name'])?$states['state_name']:"" }}
                              </option>
                              @endforeach
                              @endif
                           </select>
                           <span class='error'>{{ $errors->first('state') }}</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('project_manager/profile/profile.text_city') }}<span>*</span></div>
                     <div class="input-name">
                     <div class="droup-select">
                        <select class="droup" data-rule-required="true" name="city" id="city_client">
                           <option value="">{{ trans('project_manager/profile/profile.entry_city') }}</option>
                           @if(isset($arr_project_manager['state_details']['cities']) && sizeof($arr_project_manager['state_details']['cities'])>0)
                           @foreach($arr_project_manager['state_details']['cities'] as $cities)
                           <option value="{{isset($cities['id'])?$cities['id']:""}}" 
                           @if($cities['id']==$arr_project_manager['city'])
                           selected="true" 
                           @endif
                           >
                           {{isset($cities['city_name'])?$cities['city_name']:"" }}
                           </option>
                           @endforeach
                           @endif
                        </select>
                        <span class='error'>{{ $errors->first('city') }}</span>
                     </div>
                  </div>
                  </div>
               </div>
               <div class='clr'></div>
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('project_manager/profile/profile.text_postal_code') }}<span>*</span></div>
                     <div class="input-name"><input type="text" class="clint-input" placeholder="{{ trans('project_manager/profile/profile.entry_postal_code') }}" name="zip" id="zip" value="{{isset($arr_project_manager['zip'])?$arr_project_manager['zip']:''}}" data-rule-required="true" data-rule-maxlength="10" data-rule-minlength="4" onkeyup="return chk_validation(this);"/>
                        <span class='error'>{{ $errors->first('zip') }}</span>                                                            
                     </div>
                  </div>
               </div>
               
               
               <?php /*<div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('project_manager/profile/profile.text_vat_number') }}<span>*</span></div>
                     <div class="input-name"><input type="text" class="clint-input" placeholder="{{ trans('project_manager/profile/profile.entry_vat_number') }}" name="vat_number" id="vat_number" value="{{isset($arr_project_manager['vat_number'])?$arr_project_manager['vat_number']:''}}"  data-rule-required="true" data-rule-maxlength="20"/>
                        <span class='error'>{{ $errors->first('vat_number') }}</span>                                                            
                     </div>
                  </div>
               </div> */ ?>
              
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('project_manager/profile/profile.text_address') }}<span>*</span></div>
                     <div class="input-name"><textarea rows="" cols="" class="clint-input" placeholder="{{ trans('project_manager/profile/profile.entry_address') }}" name="address" id="address"  data-rule-required="true" data-rule-maxlength="450">{{isset($arr_project_manager['address'])?$arr_project_manager['address']:''}}</textarea>
                        <span class='error'>{{ $errors->first('address') }}</span>                                                            
                     </div>
                  </div>
               </div>

               <div class='clr'></div>

               <?php /* <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('project_manager/profile/profile.text_spoken_languages') }}</div>
                     <div class="input-name"><input type="text" class="clint-input" placeholder="{{ trans('project_manager/profile/profile.entry_spoken_languages') }}" name="spoken_languages" id="spoken_languages" value="{{isset($arr_project_manager['spoken_languages'])?$arr_project_manager['spoken_languages']:''}}" />                                                            </div>
                  </div>
               </div> 
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('project_manager/profile/profile.text_time_zone') }}</div>
                     <div class="input-name">
                        <div class="droup-select">
                           <select class="droup" name="timezone" id="timezone">
                              <option value="">{{ trans('project_manager/profile/profile.text_time_zone') }}</option>
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-sm-12  col-md-12 col-lg-12">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('project_manager/profile/profile.text_skills') }}</div>
                     <div class="input-name">
                        <div class="droup-select">
                           
                           <select class="droup" name="skills[]" id="skills" multiple="multiple">
                           @if(isset($arr_skills) && sizeof($arr_skills)>0)
                              @foreach($arr_skills as $skill)

                              <option  value="{{isset($skill['id'])?$skill['id']:''}}"
                                          @if(isset($arr_project_manager['expert_skills']) && sizeof($arr_project_manager['expert_skills']))
                                             @foreach($arr_project_manager['expert_skills'] as $expt_skill)
                                                @if(isset($expt_skill['skill_id']) && $expt_skill['skill_id']==$skill['id'])
                                                   selected='selected'
                                                @endif
                                             @endforeach
                                          @endif
                                           >{{isset($skill['skill_name'])?$skill['skill_name']:''}}</option>
                              @endforeach
                           @endif
                           </select>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                     <div class="p-control-label">{{ trans('project_manager/profile/profile.text_compny_name') }}</div>
                     <div class="input-name"><input type="text" class="clint-input" placeholder="{{ trans('project_manager/profile/profile.entry_compny_name') }}" name="company_name" id="company_name" value="{{isset($arr_project_manager['company_name'])?$arr_project_manager['company_name']:''}}"/>                                                            </div>
                  </div>
               </div> */ ?>

               <?php /*
               <div class="col-sm-12  col-md-6 col-lg-6">
                  <div class="user-box">
                    <div class="p-control-label">{{ trans('project_manager/profile/profile.text_profile_image_label') }}</div>

                     <div class="input-name">
                        <div class="upload-block">
                           <input type="file" id="profile_image" style="visibility:hidden; height: 0;" name="profile_image">
                           <div class="input-group">
                              <?php
                                 $style_one = 'none';

                                 if(isset($arr_project_manager['profile_image']) && $arr_project_manager['profile_image']!="")
                                 {
                                    $style_one = 'block';
                                 }
                              ?>
                              <input type="text" class="form-control file-caption  kv-fileinput-caption" id="profile_image_name" disabled="disabled" value="{{$arr_project_manager['profile_image'] or ''}}">
                              <div class="btn btn-primary btn-file btn-gry">
                                 <a class="file" onclick="openCropModal()">{{ trans('project_manager/profile/profile.text_upload') }}
                                 </a>
                              </div>
                              <div class="btn btn-primary btn-file remove" style="border-right:1px solid #fbfbfb !important;display:{{$style_one}};" id="btn_remove_image">
                                 <a class="" onclick="removeBrowsedImage()"><i class="fa fa-trash" style="color: #ffffff;" aria-hidden="true"></i>
                                 </a>
                              </div>
                           </div>
                        </div>
                        <div id="msg_profile_image" style="color: red;font-size: 12px;font-weight:600;display: none;"></div>
                        <div class="user-box hidden-lg hidden-md hidden-sm hidden-xs">&nbsp;</div>
                        <div class="note_browse">{{ trans('project_manager/profile/profile.text_profile_image') }}</div>
                     </div>
                  </div>
               </div>
               */ ?>


               <?php /* 
               <div class="col-sm-12">
                  <div class="terms_us">
                     <div class="check-bx">
                        <input type="checkbox" id="checkbx1" class="css-checkbox" checked="checked" name="radiog_dark">
                        <label class="css-label radGroup2" for="checkbx1"><span style="color:red;">*</span>Confirmation that all filled date are correct and I am the author of the work samples</label>
                     </div>
                  </div>
               </div>
               */ ?>
               <div class="col-sm-12 col-md-12 col-lg-12"><button class="normal-btn pull-right" type="submit">{{ trans('project_manager/profile/profile.text_update') }}</button></div>
            </div>
         </div>
      </div>
   </form>
</div>



<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/timezones/dist/timezones.full.js"></script>
<link href="{{url('/public')}}/assets/select2/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="{{url('/public')}}/assets/select2/dist/js/select2.full.js"></script>

<script type="text/javascript">
 $("#form-client_profile").validate({
          errorElement: 'span',
         errorPlacement: function (error, element) 
         {
            if(element.attr("name") == 'phone_code')
            {
               error.insertAfter($("#phone_number"));
            }
            else
            {
               error.insertAfter(element);
            }
         },
         groups: {
                 phone_number: "phone_code phone_number"
             },

   });
$( document ).ready(function() {

   jQuery("#skills").select2();
   //star rating demo
   $(function() {       
     $('span.stars').stars();
   });

   $.fn.stars = function() 
   {
     return $(this).each(function() {
       $(this).html($('<span />').width(Math.max(0, (Math.min(5, parseFloat($(this).html())))) * 20));
     });
   }
   // This is the simple bit of jquery to duplicate the hidden field to subfile
   $('#profile_image').change(function()
   {
     /*if($(this).val().length>0)
     {
       $("#btn_remove_image").show();
     }   
     $('#profile_image_name').val($(this).val());*/
   });
   
   //times js init
   $('#timezone').timezones();

   setTimezone();
   
 });
   

function setTimezone() 
{
   
   var selected_timezone = "{{isset($arr_project_manager['timezone'])?$arr_project_manager['timezone']:''}}";

   if (selected_timezone && selected_timezone!="") 
      {
         $("#timezone").val(selected_timezone);
      }
   
}

 function browseImage()
 {
    $("#profile_image").trigger('click');
 }


 function removeBrowsedImage()
 {
   if(!confirm('Are you sure ? Do you want to delete profile image'))
   {
      return false;
   }

   $('#profile_image_name').val("");
   $("#btn_remove_image").hide();
   $("#profile_image").val("");

   $.ajax({
      url:"{{url('/public')}}"+"/project_manager/delete_image",
      success:function (response)
      {
         if(typeof response.status != "undefined" && response.status == "SUCCESS_PROFILE")
         {
            $('#msg_profile_image').html(response.msg).show();
         }
      }
   }); 

 }


</script>
<script type="text/javascript">
   $("#form-client_profile").validate({
         errorElement: 'span',
         });
    function loadStates(ref)   
    {
        var selected_country = jQuery(ref).val();
   
        if(selected_country && selected_country!="" && selected_country!=0)
        {
   
         jQuery('select[id="state_client"]').find('option').remove().end().append('<option value="">Select State</option>').val('');
         jQuery('select[id="city_client"]').find('option').remove().end().append('<option value="">Select City</option>').val('');
   
              jQuery.ajax({
                              url:'{{url("/")}}/locations/get_states/'+btoa(selected_country),
                              type:'GET',
                              data:'flag=true',
                              dataType:'json',
                              beforeSend:function()
                              {
                                  jQuery('select[id="state_client"]').attr('readonly','readonly');
                              },
                              success:function(response)
                              {
                                  if(response.status=="success")
                                  {
                                      jQuery('select[id="state_client"]').removeAttr('readonly');
   
                                      if(typeof(response.states) == "object")
                                      {
                                         var option = '<option value="">Select State</option>'; 
                                         jQuery(response.states).each(function(index,state)
                                         {
                                              option+='<option value="'+state.id+'">'+state.state_name+'</option>';
                                         });
   
                                         jQuery('select[id="state_client"]').html(option);
                                      }
   
                                  }
                                  return false;
                              }    
              });
         }
    }
   
   function loadCities(ref)   
    {
        var selected_state = jQuery(ref).val();
   
        if(selected_state && selected_state!="" && selected_state!=0)
        {
   
         jQuery('select[id="city_client"]').find('option').remove().end().append('<option value="">Select City</option>').val('');
   
              jQuery.ajax({
                              url:'{{url("/")}}/locations/get_cities/'+btoa(selected_state),
                              type:'GET',
                              data:'flag=true',
                              dataType:'json',
                             beforeSend:function()
                              {
                                  jQuery('select[id="city_client"]').attr('readonly','readonly');
                              },
                              success:function(response)
                              {
                                  if(response.status=="success")
                                  {
                                      jQuery('select[id="city_client"]').removeAttr('readonly');
   
                                      if(typeof(response.cities) == "object")
                                      {
                                         var option = '<option value="">Select City</option>'; 
                                         jQuery(response.cities).each(function(index,city)
                                         {
                                              option+='<option value="'+city.id+'">'+city.city_name+'</option>';
                                         });
   
                                         jQuery('select[id="city_client"]').html(option);
                                      }
   
                                  }
                                  return false;
                              }    
                        });
         }
    }
    $('#phone_number').keypress(function(eve) 
  {

    if(eve.which == 8 || eve.which == 190) 
      {
        return true;
      }
  else if ((eve.which != 46 || $(this).val().indexOf('.') != -1) && (eve.which < 48 || eve.which > 57) || (eve.which == 46 && $(this).caret().start == 0) ) {
    eve.preventDefault();
  }
     
// this part is when left part of number is deleted and leaves a . in the leftmost position. For example, 33.25, then 33 is deleted
 $('#phone_number').keyup(function(eve) {
  if($(this).val().indexOf('.') == 0) {    $(this).val($(this).val().substring(1));
  }
 });
});

 /*$('#zip').keyup(function()
{
 var yourInput = $(this).val();
 re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
 var isSplChar = re.test(yourInput);
 if(isSplChar)
 {
   var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
   $(this).val(no_spl_char);
 }
});*/
// Function for special characters not accepts.
function chk_validation(ref)
{
   var yourInput = $(ref).val();
    re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
    var isSplChar = re.test(yourInput);
    if(isSplChar)
    {
      var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
      $(ref).val(no_spl_char);
    }
}

function open_modal()
{
   $('#open_modal').click();
   /*
   $(".cropper-container cropper-bg").css('width','555');
   $(".cropper-container cropper-bg").css('height','469');
   */
}
</script>
@stop