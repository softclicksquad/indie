@extends('front.layout.master')
@section('main_content') 
<div class="middle-container">
   <div class="container">
      <br/>
      <div class="row">
         <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="search-grey-bx">
               <div class="head_grn">{{ trans('expert/portfolio/portfolio.text_profile') }}</div>
               <div class="row">
                  <div class="col-sm-12 col-md-12 col-lg-12">
                     <div class="profil-man">
                        <div class="row">
                           <div class="col-sm-3 col-md-3 col-lg-3">
                              <div class="user-profile">
                                 <div class="profile-circle">
                                    <div class="edit-hover">
                                       <li><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></li>
                                    </div>
                                    @if(isset($profile_img_public_path) && isset($arr_project_manager['profile_image']) && $arr_project_manager['profile_image']!=NULL)
                                    <img src="{{$profile_img_public_path.$arr_project_manager['profile_image']}}" alt=""/>
                                    @else
                                    <img src="{{$profile_img_public_path}}default_profile_image.png" alt="">
                                    @endif
                                 </div>
                              </div>
                               <div style="text-align:center;margin-bottom:15px;">
                              @if(isset($arr_project_manager['created_at']) && $arr_project_manager['created_at']!="")
                                 {{'Since '.date('M Y',strtotime($arr_project_manager['created_at'])).' on'}} <br/> {{config('app.project.name')}}
                              @endif
                              </div>
                           </div>
                           <div class="col-sm-9 col-md-9 col-lg-9">
                              <div class="right-dashbord">
                                 <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
                                       <div class="head_grn">
                                          @if(isset($arr_project_manager['first_name']) && isset($arr_project_manager['last_name']))
                                          {{ucfirst($arr_project_manager['first_name'])}}&nbsp;{{ucfirst($arr_project_manager['last_name'])}}
                                          @endif
                                       </div>
                                       <div class="address-mod-pro"><i class="fa fa-map-marker" aria-hidden="true"></i>

                                       {{isset($arr_project_manager['country_details']['country_name'])?$arr_project_manager['country_details']['country_name']:''}}</div>
                                       
                                       {{-- <div class="model-deceb-pro">
                                       <i class="fa fa-language" aria-hidden="true"></i>&nbsp;&nbsp;{{ $arr_project_manager['spoken_languages']  or ''}}
                                       </div> --}}
                                    </div>
                                 </div>
                                 <hr/>


                                 <?php 
                                       $sidebar_information= array();
                                       if (isset($arr_project_manager['user_id']))
                                       {
                                          $sidebar_information = sidebar_information($arr_project_manager['user_id']);
                                       }
                                  ?>

                                 <div class="clr"></div>
                                 <div class="rating-bx">
                                    <div class="img-pro infg"><img src="{{url('/public')}}/front/images/project-inc.png" alt="" /></div>
                                    <div class="info-r-pro"><div align="center" >{{isset($sidebar_information['project_count'])?$sidebar_information['project_count']:'0'}}</div><span>{{ trans('expert/portfolio/portfolio.text_projects') }} </span></div>
                                 </div>
                                 {{-- <div class="rating-bx">
                                    <div class="img-pro infg"><img src="{{url('/public')}}/front/images/star-inc.png" alt="" /></div>
                                    <div class="info-r-pro">
                                    <div align="center" >{{isset($sidebar_information['completion_rate'])?$sidebar_information['completion_rate']:'0.0'}}% </div><span>{{ trans('expert/portfolio/portfolio.text_completion_rate') }}</span></div>
                                 </div>
                                 <div class="rating-bx">
                                    <div class="img-pro infg"><img src="{{url('/public')}}/front/images/reviews-inc.png" alt="" /></div>
                                    <div class="info-r-pro"><div align="center" >{{isset($sidebar_information['review_count'])?$sidebar_information['review_count']:'0'}}</div><span>{{ trans('expert/portfolio/portfolio.text_review') }}</span></div>
                                 </div>
                                 <div class="rating-bx">
                                    <div class="img-pro infg"><img src="{{url('/public')}}/front/images/rating-inc.png" alt="" /></div>
                                    <div class="info-r-pro"><div align="center" >{{isset($sidebar_information['reputation'])?$sidebar_information['reputation']:'0.0'}}%</div> <span>{{ trans('expert/portfolio/portfolio.text_reputation') }}</span></div>
                                 </div> --}}

                              </div>
                           </div>
                           <div class="clr"></div>
                        </div>
                        
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="{{url('/public')}}/front/js/lightbox-plus-jquery.min.js"></script>

<script type="text/javascript">
   $( document ).ready(function() 
   {
      $('#reviews').show();
   });

   /* Star rating demo */ 
   $.fn.stars = function() 
   {
    return $(this).each(function() {
      $(this).html($('<span />').width(Math.max(0, (Math.min(5, parseFloat($(this).html())))) * 20));
    });
   }
   $(function() {       
    $('span.stars').stars();
   });

</script>
  


@stop

