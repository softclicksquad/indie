<!-- HEader -->        
@include('front.layout.header')    
        
<!-- BEGIN Sidebar -->

<!-- END Sidebar -->

<!-- BEGIN Content -->

@include('project_manager.layout.sidebar')


    @yield('main_content')

    <!-- END Main Content -->

<!-- Footer -->    


@include('project_manager.layout.dynamic_footer') 


@include('front.layout.footer')    
                
              