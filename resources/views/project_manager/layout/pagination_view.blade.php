 <div class="product-pagi-block">
   @if ($arr_pagination->lastPage() > 1 && isset($arr_pagination))
   <?php $arr_pagination->appends(\Request::all()); ?>
   <ul>
      <li>
         <a class="arrow-block-img {{ ($arr_pagination->currentPage() == 1) ? '' : '' }}" href="{{ $arr_pagination->url(1) }}" >
            <i class="fa fa-angle-left"></i>
         </a>
      </li>
      
      @for ($i = 1; $i <= $arr_pagination->lastPage(); $i++)
      <li>

         <a  class="{{ ($arr_pagination->currentPage() == $i) ? ' pegi-active' : '' }}" href="{{ $arr_pagination->url($i) }}">{{ $i }}</a>

      </li>
      @endfor

      <li>
         <a class="arrow-block-img {{ ($arr_pagination->currentPage() == $arr_pagination->lastPage()) ? '' : '' }}" href="{{ $arr_pagination->url($arr_pagination->currentPage()+1) }}" >
            <i class="fa fa-angle-right"></i>
         </a>
      </li>
   </ul>
   @endif
</div>