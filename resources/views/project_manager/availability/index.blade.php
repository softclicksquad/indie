@extends('project_manager.layout.master')                
@section('main_content')
<div class="col-sm-7 col-md-8 col-lg-9">
   <form action="{{url('/project_manager/update_availability')}}" method="POST" id="form-project_manager_availability" name="form-project_manager_availability" enctype="multipart/form-data" files ="true">
      {{ csrf_field() }}
      <div class="right_side_section">
         @include('front.layout._operation_status')
         <div class="head_grn">{{ trans('project_manager/availability/availability.text_availability') }}</div>
         <div class="row">
            <div class="change-pwd-form">
               <div class="col-sm-6 col-md-6 col-lg-6">
                                 <div class="user-box">
<!--                                     <div class="control-label">{{ trans('expert/profile/profile.text_availability') }}:</div>
 -->                                    <div class="input-name">
                                    <div class="radio_area regi">
                                       <input type="radio" name="user_availability" id="radio_available" 
                                         @if(isset($arr_proj_manager_details['is_available']) && ($arr_proj_manager_details['is_available']==1))
                                         checked="" 
                                         @endif 
                                       class="css-checkbox" checked="" value="1">
                                       <label for="radio_available" class="css-label radGroup1">{{ trans('project_manager/availability/availability.text_available') }}</label>
                                    </div>
                                    <div class="radio_area regi">
                                       <input type="radio" name="user_availability" id="radio_not_available" 
                                        @if(isset($arr_proj_manager_details['is_available']) && ($arr_proj_manager_details['is_available']==0))
                                         checked="" 
                                         @endif 
                                       class="css-checkbox" value="0">
                                       <label for="radio_not_available" class="css-label radGroup1">{{ trans('project_manager/availability/availability.text_unavailable')  }}</label>
                                    </div>
                                    </div>
                                    <span class='error'>{{ $errors->first('user_availability') }}</span>
                                 </div>
                </div>
               <div class="col-sm-12 col-md-12 col-lg-12"><button class="normal-btn pull-right" type="submit">{{ trans('project_manager/availability/availability.text_update') }}</button></div>
            </div>
         </div>
      </div>
   </form>
</div>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/timezones/dist/timezones.full.js"></script>
<link href="{{url('/public')}}/assets/select2/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="{{url('/public')}}/assets/select2/dist/js/select2.full.js"></script>

<script type="text/javascript" src="{{url('/public')}}/assets/languages/all_langs.js"></script>

<script type="text/javascript">
$( document ).ready(function() {

   jQuery("#skills").select2();
   jQuery("#categories").select2();
  
   
   //star rating demo
   $(function() {       
     $('span.stars').stars();
   });

   $.fn.stars = function() 
   {
     return $(this).each(function() {
       $(this).html($('<span />').width(Math.max(0, (Math.min(5, parseFloat($(this).html())))) * 20));
     });
   }
  
  
   set_languages('spoken_languages');

   <?php
         $spk_lang = array();
         if (isset($arr_expert_details['spoken_languages']) && $arr_expert_details['spoken_languages']!="") 
         {
           $spk_lang = explode(",", $arr_expert_details['spoken_languages']);
         }
    ?>
   

    jQuery("#spoken_languages").select2();
 });

function set_languages(control_id) 
{
    if (control_id) 
    {
        var all_lang = get_languages();

        if(all_lang)
        {
            if(typeof(all_lang) == "object")
            {
                var option = '<option value="">Select languages</option>'; 
                for(lang in all_lang)
                {
                  <?php
                     if (isset($spk_lang) && sizeof($spk_lang)>0) 
                     {

                        foreach ($spk_lang as $spklang) 
                        {
                           
                        ?>
                           var currnt_lang = '<?php echo $spklang;?>';
                           if (currnt_lang==all_lang[lang].name) 
                              {
                                 option+='<option selected value="'+all_lang[lang].name+'">'+all_lang[lang].name+'</option>';
                              }
                              else
                              {
                                 option+='<option value="'+all_lang[lang].name+'">'+all_lang[lang].name+'</option>';
                              }
                           
               <?php    }
                     }
                  ?>
                   
                }
               
               jQuery('select[id="'+control_id+'"]').html(option);
            }
        }
    }
     
}
   


</script>

<script type="text/javascript">
   $("#form-project_manager_availability").validate({
         errorElement: 'span',
         });

</script>
@stop