@extends('project_manager.layout.master')
@section('main_content')

<div class="col-sm-7 col-md-8 col-lg-9">
    @include('front.layout._operation_status')
    <div class="dashboard-box">
        <div class="head_grn">{{ trans('dashboard/dashboard.welcome_to_dashboard') }}</div>
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <a class="pink-bx" href="{{ $module_url_path }}/projects/ongoing">
                    <div class="dash-top-block">
                        <div class="dash-icon">
                            <img src="{{url('/public')}}/front/images/dash-icon1.png" class="img-responsive" alt="Notpad icon">
                        </div>
                        <div class="dash-content">
                            <span class="dash-count">{{ isset($count_ongoing_projects)? $count_ongoing_projects:'0'}}</span><br />
                            {{ trans('dashboard/dashboard.ongoing_projects') }}
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6">
                <a class="pink-bx" href="{{ $module_url_path }}/projects/completed">
                    <div class="dash-top-block">
                        <div class="dash-icon">
                            <img src="{{url('/public')}}/front/images/dash-icon2.png" class="img-responsive" alt="Notpad icon">
                        </div>
                        <div class="dash-content">
                            <span class="dash-count">{{ isset($count_completed_projects)? $count_completed_projects:'0'}}</span><br />
                            {{ trans('dashboard/dashboard.completed_projects') }}
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6">
                <a class="pink-bx" href="{{ $module_url_path }}/projects/open">
                    <div class="dash-top-block">
                        <div class="dash-icon">
                            <img src="{{url('/public')}}/front/images/dash-icon5.png" class="img-responsive" alt="Notpad icon">
                        </div>
                        <div class="dash-content">
                            <span class="dash-count">{{ isset($count_open_projects)? $count_open_projects:'0'}}</span>
                            <br />{{ trans('dashboard/dashboard.open_projects') }}
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6">
                <a class="pink-bx" href="{{ $module_url_path }}/projects/canceled">
                    <div class="dash-top-block marg-rgt">
                        <div class="dash-icon">
                            <img src="{{url('/public')}}/front/images/cancel-projects-icon-img.png" class="img-responsive" alt="star icon" />
                        </div>
                        <div class="dash-content">
                            <span class="dash-count">{{ isset($count_canceled_projects)? $count_canceled_projects:'0'}}</span>
                            <br />{{ trans('dashboard/dashboard.canceled_projects') }}
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

@stop