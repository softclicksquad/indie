@extends('project_manager.layout.master')                
@section('main_content')

<link rel="stylesheet" href="{{url('/public')}}/assets/jquery-ui/jquery-ui.min.css">
<script src="{{url('/public')}}/assets/jquery-ui/jquery-ui.min.js"></script>

<div class="col-sm-7 col-md-8 col-lg-9">
   <div class="row">
  <div class="col-sm-7 col-md-8 col-lg-12">
  <div class="right_side_section">
 
    <div class="col-lg-8">
   @include('front.layout._operation_status')
    <div class="head_grn">{{trans('project_manager/projects/ongoing_projects.text_ongoing_project_title')}}
    </div>
  </div>
    <div class="col-lg-4">
      <div class="droup-select">
      <select class="droup mrns tp-margn" onchange="sortByHandledBy(this)">
        <option value="">-- Sort By --</option>
        <option @if(isset($sort_by) && $sort_by == 'asc') selected="" @endif value="asc">Ascending</option>
        <option @if(isset($sort_by) && $sort_by == 'desc') selected="" @endif value="desc">Descending</option>
      </select>
      </div>
    </div>
    <div class="clearfix"></div>
  
        
    @if(isset($arr_ongoing_projects['data']) && sizeof($arr_ongoing_projects['data'])>0)
      @foreach($arr_ongoing_projects['data'] as $proRec)
      <div class="ongonig-project-section client-dash">
        <div class="project-title">  
          <a href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}">
          <h3>{{$proRec['project_name']}}</h3>
          </a>

          <div class="search-freelancer-user-location"> <span class="gavel-icon"><img src="{{url('/public')}}/front/images/clock.png" alt=""></span> <span class="dur-txt"> {{trans('project_manager/projects/open_projects.text_project_duration')}}: {{$proRec['project_expected_duration']}} {{trans('project_manager/projects/open_projects.text_days')}}</span>
          </div>
          <div class="more-project-dec">{{str_limit($proRec['project_description'],350)}}</div>
        </div>
        
        <div class="row">
          <div class="category-new">{{$proRec['category_details']['category_title'] or 'NA'}}</div>
          <div class="clearfix"></div>

          @if(isset($proRec['project_skills']) && count($proRec['project_skills']) > 0)
          @foreach($proRec['project_skills'] as $key=> $skills)
          @if(isset($skills['skill_data']['skill_name']) && $skills['skill_data']['skill_name'] != "")
            <div class="skils-project">
                <ul>
                  <li>{{ str_limit($skills['skill_data']['skill_name'],25) }}</li>
                </ul>
            </div>
          @endif
          @endforeach
          @endif

          <div class="col-sm-12 col-md-12 col-lg-12" style="margin-top: 10px;">    
            <a class="black-border-btn" href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}">
               {{trans('project_manager/projects/ongoing_projects.text_view')}} 
            </a>

            <a class="black-btn" href="{{ $module_url_path }}/milestones/{{ base64_encode($proRec['id'])}}"  >
             {{trans('project_manager/projects/ongoing_projects.text_milestones')}} 
            </a>
          </div>
        </div>
      </div>
      <div class="clr"></div>
      <hr/>
      @endforeach
    @else
        <div class="search-grey-bx">
            <div class="no-record">
                {{trans('expert/projects/awarded_projects.text_sorry_no_record_found')}}
            </div>
        </div>
    @endif

  </div>
    <!-- Paination Links -->
      @include('front.common.pagination_view', ['paginator' => $arr_ongoing_projects])
    <!-- Paination Links -->
</div>
 </div>  
</div>


<script type="text/javascript">
  

    $(document).ready( function () 
    {
      /* $("input[name='search_project']").autocomplete(
       {
          minLength:0,
          source:"{{ url('/search_project') }}",
          select:function(event,ui)
          {
            $("input[name='search_project']").val(ui.item.label);
          },
          response: function (event, ui)
          {

          }
       });*/

    });

</script>
<script type="text/javascript">
  function sortByHandledBy(ref){  
      var sort_by = $(ref).val();
      if(sort_by != ""){
        window.location.href="{{ url('project_manager/projects/ongoing') }}"+"?sort_by="+sort_by;
      }
      else{
        window.location.href="{{ url('project_manager/projects/ongoing') }}";
      }
      return true;
    }
</script>


@stop