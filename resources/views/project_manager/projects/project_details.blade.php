@extends('project_manager.layout.master')                
@section('main_content')
<style type="text/css">
  table { border-collapse: collapse; }
  td, th { border: 1px solid #999; padding: 0.5rem; text-align: left;}
</style> 
<!-- <div class="middle-container"> -->
      <div class="container">
         <!-- <br/> -->
         <div class="row">
           <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
           {{--@include('front.layout._operation_status')--}}
            <!-- Including top view of project details - Just pass project information array to the view -->
               @include('front.common._top_project_details',['projectInfo'=>$projectInfo])
            <!-- Ends -->
            @if(isset($arr_project_bid_info['data']) && count($arr_project_bid_info['data'])>0 )   
               @foreach($arr_project_bid_info['data'] as $key => $project_bid)

                  <?php
                      $arr_profile_data = [];  
                      if(isset($project_bid['expert_details']['user_id']) && $project_bid['expert_details']['user_id'] != "")
                      {
                        $arr_profile_data = sidebar_information($project_bid['expert_details']['user_id']);
                      }
                  ?>
                  
                  @if($projectInfo['is_awarded'] == '1' && $projectInfo['expert_user_id'] == $project_bid['expert_details']['user_id'] )               
                     <div class="search-grey-bx projt-detls">
                        <div class="row">
                           <div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
                              <div class="going-profile-detail">
                                 <div class="listin-pro-image">

                                    @if(isset($project_bid['expert_details']['profile_image']) && $project_bid['expert_details']['profile_image'] != ""  && file_exists('public/uploads/front/profile/'.$project_bid['expert_details']['profile_image']) ) 
                                       <div class="going-pro">
                                        <img src="{{ url('/uploads/front/profile/').'/'.$project_bid['expert_details']['profile_image']}}" alt="pic"/>
                                      </div>
                                      @else
                                      <div class="going-pro">
                                        <img src="{{  url('/uploads/front/profile/default_profile_image.png')}}" alt="pic"/>
                                      </div>
                                      @endif

                                    @if(isset($project_bid['user_details']['is_online']) && $project_bid['user_details']['is_online']!='' && $project_bid['user_details']['is_online'] == '1')
                                      <div class="user-online-round"></div>
                                    @else
                                      <div class="user-offline-round"></div>
                                    @endif

                                  </div>

                              <?php 
                              $user_name = get_username($projectInfo['expert_user_id']);
                              ?>
                                 <div class="going-pro-content">
                                    <div class="profile-name" style="display:inline-block;color:#2d2d2d;"><a href="{{url('/project_manager/projects/more_details/'.base64_encode($project_bid['project_id']).'/'.base64_encode($project_bid['expert_details']['user_id']))}}" style="color:#2d2d2d;">
                                        <!-- {{$project_bid['expert_details']['first_name']}} {{$project_bid['expert_details']['last_name']}} -->
                                        {{isset($project_bid['expert_details']['first_name'])?$project_bid['expert_details']['first_name']:'Unknown user'}} 
                                        @if(isset($project_bid['expert_details']['last_name']) && $project_bid['expert_details']['last_name'] !="") @php echo substr($project_bid['expert_details']['last_name'],0,1).'.'; @endphp @endif
                                        <!-- ({{$project_bid['user_details']['email'] or ''}}) -->

                                       
                                        </a>

                                        @if(isset($project_bid['user_details']['kyc_verified']) && $project_bid['user_details']['kyc_verified']!='' && $project_bid['user_details']['kyc_verified'] == '1')
                                        <span class="verfy-new-arrow">
                                            <a href="#" data-toggle="tooltip" title="Identity Verified"><img src="{{url('/public/front/images/verifild.png')}}" alt=""> </a>
                                        </span>
                                        @endif


                                      </div><br/>
                                      @if($arr_profile_data['last_login_duration'] == 'Active')
                                         <span class="pull-left" title="{{isset($arr_profile_data['expert_timezone'])?$arr_profile_data['expert_timezone']:'Not Specify'}}">
                                            <span style="color:#4D4D4D;font-size: 14px;text-transform:none;cursor:text;">
                                              <span class="flag-image"> 
                                                @if(isset($arr_profile_data['user_country_flag'])) 
                                                  @php 
                                                     echo $arr_profile_data['user_country_flag']; 
                                                  @endphp 
                                                @elseif(isset($arr_profile_data['user_country'])) 
                                                  @php 
                                                    echo $arr_profile_data['user_country']; 
                                                  @endphp 
                                                @else 
                                                  @php 
                                                    echo 'Not Specify'; 
                                                  @endphp 
                                                @endif 
                                             </span>
                                              {{-- trans('common/_expert_details.text_local_time') --}}  {{isset($arr_profile_data['expert_timezone_without_date'])?$arr_profile_data['expert_timezone_without_date']:'Not Specify'}}</span>
                                         </span>
                                      @else
                                         <span class="flag-image"> 
                                              @if(isset($arr_profile_data['user_country_flag'])) 
                                                @php 
                                                   echo $arr_profile_data['user_country_flag']; 
                                                @endphp 
                                              @elseif(isset($arr_profile_data['user_country'])) 
                                                @php 
                                                  echo $arr_profile_data['user_country']; 
                                                @endphp 
                                              @else 
                                                @php 
                                                  echo 'Not Specify'; 
                                                @endphp 
                                              @endif 
                                          </span>
                                         {{-- <div style="color:#494949;" class="sub-project-dec" title="{{$arr_profile_data['last_login_full_duration']}}"> --}}
                                           <i  class="fa fa-clock-o" aria-hidden="true"></i><span> {{$arr_profile_data['last_login_duration']}}
                                         {{-- </div> --}}
                                      @endif
                                      <br>
                                      <div class="pro-txt-title">
                                      <span>{{trans('common/common.text_profession')}} : </span>{{$project_bid['user_details']['role_info']['profession_details']['profession_title'] or ''}}
                                    </div>
                                      <div class="skill-de-content">
                                        <div class="skils-project tp-kil-hov">
                                          <ul>
                                            {{trans('common/common.text_categories')}} : 
                                            @php
                                            $experts_cat =isset($project_bid['expert_details']['expert_categories'])?$project_bid['expert_details']['expert_categories']:[]; 
                                            @endphp
                                            @if(isset($experts_cat) && !empty($experts_cat))
                                            @foreach($experts_cat as $c)
                                            <li style="font-size:12px;">{{ isset($c['categories']['category_title'])?str_limit($c['categories']['category_title'],18):'N/A'}}</li>
                                            @endforeach
                                            @endif
                                          </ul>
                                        </div>
                                      </div>
                                      <div class="clearfix"></div>
                                      <div style="color:#494949;" class="sub-project-dec"></div><br>
                                    <div class="more-project-dec" style="height:auto;">
                                      {{str_limit($project_bid['bid_description'],250)}}

                                       @if(isset($project_bid['bid_description']) && strlen(trim($project_bid['bid_description'])) > 1550)  
                                        <a href="{{url('/project_manager/projects/more_details/'.base64_encode($project_bid['project_id']).'/'.base64_encode($project_bid['expert_details']['user_id']))}}" style="color:#0e81c2;">{{trans('project_manager/projects/project_details.more')}}</a>
                                      @endif
                                        <div class="clr"></div>
                                        <br/>
                                        <a href="{{url('/project_manager/projects/more_details/'.base64_encode($project_bid['project_id']).'/'.base64_encode($project_bid['expert_details']['user_id']))}}">
                                          <button type="button" class="awd_btn"><i class="fa fa-eye"></i>&nbsp;{{trans('project_manager/projects/project_details.view')}}</button>
                                        </a>
                                        <a href="{{url('/')}}/conversation/{{base64_encode($project_bid['project_id'])}}/{{base64_encode($project_bid['expert_details']['user_id'])}}" class="applozic-launcher">
                                          <button  class="msg_btn" >{{ trans('common/project_manager_sidebar.text_chat') }}</button>
                                        </a>
                                    </div>
                                 </div>
                              </div>
                           </div>

                             <div class="col-sm-12 col-md-4 col-lg-4  br-left">
                                <div class="rating-profile profile-rating-block">
                                  <div class="projrct-prce1">
                                    @if(isset($projectInfo['project_currency']) && $projectInfo['project_currency']=="$")
                                    <span>
                                      <img src="{{url('/public')}}/front/images/doller-img.png" alt="img"/> 
                                    </span> <span><!-- {{$project_bid['bid_cost']}} -->{{$projectInfo['project_currency']}}
                                    {{isset($project_bid['bid_cost'])?$project_bid['bid_cost']:0}}</span>
                                    @else
                                    <span>
                                      <img src="{{url('/public')}}/front/images/doller-img-new-1.png" alt="img"/> 
                                    </span> <span><!-- {{$project_bid['bid_cost']}} -->{{$projectInfo['project_currency']}}
                                    {{isset($project_bid['bid_cost'])?$project_bid['bid_cost']:0}}</span>
                                    @endif
                                  </div>
                                  <div class="rating_profile project-details-rating-profile">
                                      <div class="rate-t">
                                          <div class="rating-title1">
                                             {{trans('client/projects/project_details.duration')}} : 
                                             <span class="rating-value-span">{{$project_bid['bid_estimated_duration']}} {{trans('client/projects/project_details.days')}}</span>
                                          </div>           
                                          <div class="rating-title1">
                                             {{trans('client/projects/project_details.rating')}} :
                                             <span class="rating-value-span">
                                              <span class="rating-list"><span class="rating-list"><span class="stars">{{isset($arr_profile_data['average_rating'])? $arr_profile_data['average_rating']:'0'  }}</span></span></span>
                                              @if(isset($arr_profile_data['average_rating']) && $arr_profile_data['average_rating'] != "" )   
                                              <span> ( {{ number_format(floatval($arr_profile_data['average_rating']), 1, '.', '')  }} )</span>
                                              @else
                                              <span>( '0.0' )</span>
                                              @endif   
                                              {{-- (&nbsp;{{ $arr_profile_data['average_rating'] or '0' }}&nbsp;) --}}
                                            </span>
                                          </div>           
                                      </div>
                                      <div class="rating_profile">
                                          <div class="rating-title1">
                                              {{trans('client/projects/project_details.review')}} : 
                                              <span class="star-rat rating-value-span"><span>{{ $arr_profile_data['review_count'] or '0' }}</span></span>
                                          </div>         
                                      </div>
                                      <div class="rating_profile">
                                          <div class="rating-title1">
                                              {{trans('client/projects/project_details.completion_rate')}} :
                                              <span class="star-rat rating-value-span">
                                                  <span>{{  isset($arr_profile_data['completion_rate']) ? round((float)$arr_profile_data['completion_rate'], 2):0 }}&nbsp;%</span>
                                              </span>
                                          </div>         
                                      </div>
                                      <div class="rating_profile">
                                          <div class="rating-title1">
                                              {{trans('client/projects/project_details.reputation')}} : 
                                              <span class="star-rat rating-value-span">
                                                  <span>{{  isset($arr_profile_data['reputation']) ? round((float)$arr_profile_data['completion_rate'], 2):0 }}&nbsp;%</span>
                                              </span>
                                          </div>         
                                      </div>
                                  </div>
                                </div>
                            </div>
                     </div>
                   </div>
                  @break
               @elseif($projectInfo['is_awarded'] != '1' )

                        <div class="search-grey-bx">
                           <div class="row">
                              <div class="col-sm-12 col-md-8 col-lg-8">
                                 <div class="going-profile-detail">
                                  <div class="listin-pro-image">
                                     @if(isset($project_bid['expert_details']['profile_image']) && $project_bid['expert_details']['profile_image'] != ""  && file_exists('public/uploads/front/profile/'.$project_bid['expert_details']['profile_image']) ) 
                                       <div class="going-pro">
                                        <img src="{{ url('/uploads/front/profile/').'/'.$project_bid['expert_details']['profile_image']}}" alt="pic"/>
                                      </div>
                                      @else
                                      <div class="going-pro">
                                        <img src="{{  url('/uploads/front/profile/default_profile_image.png')}}" alt="pic"/>
                                      </div>
                                      @endif

                                     @if(isset($project_bid['user_details']['is_online']) && $project_bid['user_details']['is_online']!='' && $project_bid['user_details']['is_online'] == '1')
                                      <div class="user-online-round"></div>
                                    @else
                                      <div class="user-offline-round"></div>
                                    @endif
                                  </div>

      
                                    <div class="going-pro-content">
                                       <div class="profile-name" style="display:inline-block;color:#2d2d2d;"><a href="{{url('/project_manager/projects/more_details/'.base64_encode($project_bid['project_id']).'/'.base64_encode($project_bid['expert_details']['user_id']))}}" style="color:#2d2d2d;">
                                        <!-- {{$project_bid['expert_details']['first_name']}} {{$project_bid['expert_details']['last_name']}} -->
                                        {{isset($project_bid['expert_details']['first_name'])?$project_bid['expert_details']['first_name']:'Unknown user'}} 
                                        @if(isset($project_bid['expert_details']['last_name']) && $project_bid['expert_details']['last_name'] !="") @php echo substr($project_bid['expert_details']['last_name'],0,1).'.'; @endphp @endif
                                        <!-- ({{$project_bid['user_details']['email'] or ''}}) -->
                                        
                                        </a>
                                         @if(isset($project_bid['user_details']['kyc_verified']) && $project_bid['user_details']['kyc_verified']!='' && $project_bid['user_details']['kyc_verified'] == '1')
                                        <span class="verfy-new-arrow">
                                            <a href="#" data-toggle="tooltip" title="Identity Verified"><img src="{{url('/public/front/images/verifild.png')}}" alt=""> </a>
                                        </span>
                                        @endif
                                      </div><br/>
                                      @if($arr_profile_data['last_login_duration'] == 'Active')
                                         <span class="pull-left" title="{{isset($arr_profile_data['expert_timezone'])?$arr_profile_data['expert_timezone']:'Not Specify'}}">
                                            <span style="color:#4D4D4D;font-size: 14px;text-transform:none;cursor:text;">
                                              <span class="flag-image"> 
                                                @if(isset($arr_profile_data['user_country_flag'])) 
                                                  @php 
                                                     echo $arr_profile_data['user_country_flag']; 
                                                  @endphp 
                                                @elseif(isset($arr_profile_data['user_country'])) 
                                                  @php 
                                                    echo $arr_profile_data['user_country']; 
                                                  @endphp 
                                                @else 
                                                  @php 
                                                    echo 'Not Specify'; 
                                                  @endphp 
                                                @endif 
                                             </span> 
                                              {{-- trans('common/_expert_details.text_local_time') --}} {{isset($arr_profile_data['expert_timezone_without_date'])?$arr_profile_data['expert_timezone_without_date']:'Not Specify'}}</span>
                                         </span>
                                      @else
                                         <span class="flag-image"> 
                                              @if(isset($arr_profile_data['user_country_flag'])) 
                                                @php 
                                                   echo $arr_profile_data['user_country_flag']; 
                                                @endphp 
                                              @elseif(isset($arr_profile_data['user_country'])) 
                                                @php 
                                                  echo $arr_profile_data['user_country']; 
                                                @endphp 
                                              @else 
                                                @php 
                                                  echo 'Not Specify'; 
                                                @endphp 
                                              @endif 
                                          </span>
                                         {{-- <div style="color:#494949;" class="sub-project-dec" title="{{$arr_profile_data['last_login_full_duration']}}"> --}}
                                           <i  class="fa fa-clock-o" aria-hidden="true"></i><span> {{$arr_profile_data['last_login_duration']}}
                                         {{-- </div> --}}
                                      @endif
                                      <br>
                                      <div class="pro-txt-title">
                                      <span>{{trans('common/common.text_profession')}} : </span>{{$project_bid['user_details']['role_info']['profession_details']['profession_title'] or ''}}
                                    </div>
                                      <div class="skill-de-content">
                                        <div class="skils-project tp-kil-hov">
                                          <ul>
                                            {{trans('common/common.text_categories')}} : 
                                            @php
                                            $experts_cat =isset($project_bid['expert_details']['expert_categories'])?$project_bid['expert_details']['expert_categories']:[]; 
                                            @endphp
                                            @if(isset($experts_cat) && !empty($experts_cat))
                                            @foreach($experts_cat as $c)
                                            <li style="font-size:12px;">{{ isset($c['categories']['category_title'])?str_limit($c['categories']['category_title'],18):'N/A'}}</li>
                                            @endforeach
                                            @endif
                                          </ul>
                                        </div>
                                      </div>
                                      <div class="clearfix"></div>
                                       <div style="color:#494949;" class="sub-project-dec"><!-- Interior Designers &amp; Decorators --> </div><br>
                                       <div class="more-project-dec" style="height:auto;">{{str_limit($project_bid['bid_description'],250)}}
                                         @if(isset($project_bid['bid_description']) && strlen(trim($project_bid['bid_description'])) > 250)
                                           <a href="{{url('/project_manager/projects/more_details/'.base64_encode($project_bid['project_id']).'/'.base64_encode($project_bid['expert_details']['user_id']))}}" style="color:#0e81c2;">{{trans('project_manager/projects/project_details.more')}}
                                           </a>
                                         @endif
                                          <div class="clr"></div>
                                          <br/>
                                          {{--  BLock to check whether the project is rejected --}}
                                          @if(isset($project_bid['bid_status']) && $project_bid['bid_status'] == '3')
                                            <a href="javascript:void(0);"><button type="button" class="info" style="cursor: default;"> {{trans('project_manager/projects/project_details.rejected')}}</button></a>
                                          @else
                                            <a href="{{url('/')}}/conversation/{{base64_encode($project_bid['project_id'])}}/{{base64_encode($project_bid['expert_details']['user_id'])}}" class="applozic-launcher">
                                              <button  class="msg_btn" >{{ trans('common/project_manager_sidebar.text_chat') }}</button>
                                            </a>
                                          @endif     
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-sm-12 col-md-4 col-lg-4  br-left">
                                <div class="rating-profile profile-rating-block">
                                  <div class="projrct-prce1">
                                    @if(isset($projectInfo['project_currency']) && $projectInfo['project_currency']=="$")
                                    <span>
                                      <img src="{{url('/public')}}/front/images/doller-img.png" alt="img"/> 
                                    </span> <span><!-- {{$project_bid['bid_cost']}} -->{{$projectInfo['project_currency']}}
                                    {{isset($project_bid['bid_cost'])?$project_bid['bid_cost']:0}}</span>
                                    @else
                                    <span>
                                      <img src="{{url('/public')}}/front/images/doller-img-new-1.png" alt="img"/> 
                                    </span> <span><!-- {{$project_bid['bid_cost']}} -->{{$projectInfo['project_currency']}}
                                    {{isset($project_bid['bid_cost'])?$project_bid['bid_cost']:0}}</span>
                                    @endif
                                  </div>
                                  <div class="rating_profile project-details-rating-profile">
                                      <div class="rate-t">
                                          <div class="rating-title1">
                                             {{trans('client/projects/project_details.duration')}} : 
                                             <span class="rating-value-span">{{$project_bid['bid_estimated_duration']}} {{trans('client/projects/project_details.days')}}</span>
                                          </div>           
                                          <div class="rating-title1">
                                             {{trans('client/projects/project_details.rating')}} :
                                             <span class="rating-value-span">
                                              <span class="rating-list"><span class="rating-list"><span class="stars">{{isset($arr_profile_data['average_rating'])? $arr_profile_data['average_rating']:'0'  }}</span></span></span>
                                              @if(isset($arr_profile_data['average_rating']) && $arr_profile_data['average_rating'] != "" )   
                                              <span> ( {{ number_format(floatval($arr_profile_data['average_rating']), 1, '.', '')  }} )</span>
                                              @else
                                              <span>( '0.0' )</span>
                                              @endif   
                                              {{-- (&nbsp;{{ $arr_profile_data['average_rating'] or '0' }}&nbsp;) --}}
                                            </span>
                                          </div>           
                                      </div>
                                      <div class="rating_profile">
                                          <div class="rating-title1">
                                              {{trans('client/projects/project_details.review')}} : 
                                              <span class="star-rat rating-value-span"><span>{{ $arr_profile_data['review_count'] or '0' }}</span></span>
                                          </div>         
                                      </div>
                                      <div class="rating_profile">
                                          <div class="rating-title1">
                                              {{trans('client/projects/project_details.completion_rate')}} :
                                              <span class="star-rat rating-value-span">
                                                  <span>{{  isset($arr_profile_data['completion_rate']) ? round((float)$arr_profile_data['completion_rate'], 2):0 }}&nbsp;%</span>
                                              </span>
                                          </div>         
                                      </div>
                                      <div class="rating_profile">
                                          <div class="rating-title1">
                                              {{trans('client/projects/project_details.reputation')}} : 
                                              <span class="star-rat rating-value-span">
                                                  <span>{{  isset($arr_profile_data['reputation']) ? round((float)$arr_profile_data['completion_rate'], 2):0 }}&nbsp;%</span>
                                              </span>
                                          </div>         
                                      </div>
                                  </div>
                                </div>
                              </div>
                        </div>
                     </div>
               @endif
               @endforeach
            @else
               <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;">   
                  <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                       <div class="search-grey-bx">
                           <div class="search-head" style="color:red;text-align: center">
                               {{trans('project_manager/projects/project_details.currently_no_bids_are_available')}}
                           </div>                                                        
                       </div>
                   </td>
                </tr>
            @endif
            {{-- ++++++++++++++++++++++ Project Attchment section ++++++++++++++++++++++++++++++++++++ --}}
            @if(isset($arr_project_attachment) && count($arr_project_attachment) > 0 && isset($projectInfo['project_status']) && ($projectInfo['project_status'] == '4' || $projectInfo['project_status'] == '3') )
            <?php 
               $user = Sentinel::check();
            ?>
            <div class="search-grey-bx ">
               <div class="row">
                  <div class="col-sm-12 col-md-12 col-lg-12" > 
                     <div class="profile-name" style="display:inline-block;color:#C32880;font-size: 18px; margin-bottom: 10px;">{{ trans('new_translations.project_attachments')}}</div>
                      <span id="msg_bid_attach" style="color: red;" ></span>
                      <div class="table-responsive table-block-main">
                          <table style="margin-top:15px; width:100%" >
                          <thead>
                            <tr>
                              <th>{{ trans('new_translations.sr_no')}}</th>
                              <th>{{ trans('new_translations.document_name')}}</th>
                              <th>{{ trans('new_translations.download')}}</th>
                            </tr>
                          </thead>
                          <tbody>
                          @foreach($arr_project_attachment as $key => $attchment )
                            @if($attchment['attachment'] != "")
                            <tr>
                              <td>{{$key + 1}}</td>
                              <td>{{ $attchment['attachment'] or '' }}</td>
                              <td width="265px"><a title="Download Project Attchment" download href="{{$expert_project_attachment_public_path}}{{$attchment['attachment']}}" >
                                <span style="color: #0E81C2;"><i class="fa fa-download"></i></span>
                             </a>
                                @if(isset($user) && $user->inRole('expert'))
                                &nbsp;<span style="color: #C32880;" ><i style="cursor: pointer;" onclick="deleteProjectAttachment('{{base64_encode($attchment['id'])}}')" class="fa fa-trash"></i></span>
                                @endif
                             </td>
                            </tr>
                            @endif
                           @endforeach 
                          </tbody>
                        </table>
                    </div>
                  </div> 
               </div>
            </div>   
            @endif    
            {{-- ++++++++++++++++++++++ Project Attchment section ends ++++++++++++++++++++++ --}}
            @if($projectInfo['is_awarded'] != '1')
               <!-- Pagination -->

               @include('front.common.pagination_view', ['paginator' => $arr_pagination])
               <!-- Pagination Ends -->
            @endif
      </div>
   </div>
</div>
<!-- Modal include-->
@include('client.projects.invite_expert') 
<!-- model popup end here--> 
<script>
   function confirm_suggested(url){
      var confirm_suggest = 'Are you sure? You want to suggest ?';
      alertify.confirm(confirm_suggest, function (e) {
          if (e) {
              if(url != "" && url != undefined){
                showProcessingOverlay();
                window.location.href=url;
              }
              return true;
          } else {
              return false;
          }
      });
   }
   function confirm_unsuggested(url) {
      var confirm_unsuggest = 'Are you sure? You want to unsuggest ?';
      alertify.confirm(confirm_suggest, function (e) {
          if (e) {
              if(url != "" && url != undefined){
                showProcessingOverlay();
                window.location.href=url;
              }
              return true;
          } else {
              return false;
          }
      });
   }
</script>
<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
@stop