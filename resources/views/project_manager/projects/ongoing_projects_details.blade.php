@extends('project_manager.layout.master')                
@section('main_content')
@if(isset($selected_lang) && App::isLocale('de'))
<style type="text/css">
  .rating-title {
    color: #737373;
    display: inline-block;
    font-family: "open sans";
    font-size: 13.5px;
    margin-bottom: 10px;
    max-width: 198px;
    padding: -1 15px;
    width: 100%;
}
</style>
@endif
<div class="col-sm-7 col-md-8 col-lg-9">  
    
    {{-- Includeing expert Details view --}}
      @include('front.common._expert_details',['arr_expert_details'=> isset($arr_project_bid_info['role_info'])? $arr_project_bid_info['role_info']:[],'user_details'=> $arr_project_bid_info['expert_details']? $arr_project_bid_info['expert_details']:[] ]) 
    {{-- Ends --}}

      <br/>

       {{-- Bid Information --}}

               @if(isset($arr_project_bid_info) && count($arr_project_bid_info) > 0 )

                    <div class="search-grey-bx">
                    <div class="row">
                       <div class="col-sm-12 col-md-8 col-lg-9">
                          <div class="going-profile-detail">
                             <div class="going-pro-content">
                                <div class="profile-name" style="display:inline-block;color:#c42881;">{{ trans('new_translations.bid_proposal')}}</div>
                                <div style="margin-bottom: 12px;"></div>
                                <div class="more-project-dec" style="height:auto;">
                                  {{isset($arr_project_bid_info['bid_description'])?$arr_project_bid_info['bid_description']:''}}

                                    {{-- <a href="#" style="color:#0e81c2;">More</a> --}}
                                <div class="clr"></div>
                                <br/>
                                </div>
                             </div>
                          </div>
                            @if(isset($arr_project_bid_info['bid_attachment']) && trim($arr_project_bid_info['bid_attachment']) != "")
                           <div class="user-box" style="max-width: 300px; width: 100%;">
                           <div class="p-control-label"><i class="fa fa-paperclip"></i> {{ trans('expert/projects/add_project_bid.text_attachment') }}</div>
                             <div class="input-name">
                                <div class="upload-block">
                                  <div class="input-group ">
                                      <input type="text" class="form-control file-caption  kv-fileinput-caption" id="profile_image_name" disabled="disabled" value="{{$arr_project_bid_info['bid_attachment'] or ''}}" >
                                    
                                      <div class="btn btn-primary btn-file btn-gry">
                                        <a title="Download Previous Proposal" download href="{{$bid_attachment_public_path}}{{$arr_project_bid_info['bid_attachment']}}" >
                                          <span style="color: #0E81C2;"><i class="fa fa-download"></i></span>
                                       </a>    
                                      </div>
                                   </div>
                                </div>
                                <div id="msg_bid_attach" style="color: red;font-size: 12px;font-weight:600;display: none;"></div>
                                <div class="user-box hidden-lg">&nbsp;</div>
                                <div class="note_browse"></div>
                             </div>
                          </div> 
                       @endif  
                       </div>


                       <div class="col-sm-12 col-md-4 col-lg-3">
                          <div class="rating-profile1 br-left" style="margin-top: 51px;">
                             
                             <div title="Bid Cost" class="projrct-prce1" >
                             @if(isset($arr_project_bid_info['project_details']['project_currency']) && $arr_project_bid_info['project_details']['project_currency']=="$")
                             <span>
                             <img src="{{url('/public')}}/front/images/doller-img.png" alt="img"/> 
                             </span>{{$arr_project_bid_info['project_details']['project_currency']}}
                             {{isset($arr_project_bid_info['bid_cost'])?$arr_project_bid_info['bid_cost']:0}}
                             @else
                             <span>
                             <img src="{{url('/public')}}/front/images/doller-img-new-1.png" alt="img"/> 
                             </span>{{$arr_project_bid_info['project_details']['project_currency']}}
                             {{isset($arr_project_bid_info['bid_cost'])?$arr_project_bid_info['bid_cost']:0}}
                             @endif
                           {{--   <span>
                             <img src="{{url('/public')}}/front/images/doller-img.png" alt="img"/> 
                             </span>$&nbsp;
                             {{isset($arr_bid_data['bid_cost'])?$arr_bid_data['bid_cost']:0}} --}}
                             </div>
                             
                             <div title="Project Duration" class="projrct-prce1"><span><img src="{{url('/public')}}/front/images/bid-time.png" alt="img"/> </span> {{isset($arr_project_bid_info['bid_estimated_duration'])?$arr_project_bid_info['bid_estimated_duration']:0}}&nbsp;Days
                             </div>
                             
                              {{-- @if(isset($arr_bid_data['bid_attachment']) && trim($arr_bid_data['bid_attachment']) != "")
                              <div class="projrct-prce1" >
                                 <a title="Download Previous Proposal" download href="{{$bid_attachment_public_path}}{{$arr_project_bid_info['bid_attachment']}}" >
                                    <span style="color: #0E81C2;"><i class="fa fa-download"></i></span>
                                 </a>
                               </div>
                              @endif  --}}

                          </div>
                       </div>
                    </div>
                 </div>

              @endif
              {{-- Bid Information Ends --}}
</div>
@stop