@extends('project_manager.layout.master')                
@section('main_content')


<div class="col-sm-7 col-md-8 col-lg-9">
<div class="row">
    <form action="{{ url($module_url_path) }}/manage" method="POST" id="form-manage-projects" name="form-manage-projects" enctype="multipart/form-data" files="true">
        {{ csrf_field() }}
        <div class="col-sm-7 col-md-8 col-lg-12">
            <div class="right_side_section">
                @include('front.layout._operation_status')
                <div class="head_grn">{{trans('project_manager/projects/canceled_projects.text_canceled_project_title')}}</div>
                @if(isset($arr_canceled_projects['data']) && sizeof($arr_canceled_projects['data'])>0) 
                  @foreach($arr_canceled_projects['data'] as $proRec)

                  <div class="ongonig-project-section client-dash">
                    <div class="project-title">  
                      <a href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}">
                      <h3>{{$proRec['project_name']}}</h3>
                      </a>

                      <div class="search-freelancer-user-location"> <span class="gavel-icon"><img src="{{url('/public')}}/front/images/clock.png" alt=""></span> <span class="dur-txt"> {{trans('project_manager/projects/open_projects.text_project_duration')}}: {{$proRec['project_expected_duration']}} {{trans('project_manager/projects/open_projects.text_days')}}</span>
                      </div>
                      <div class="more-project-dec">{{str_limit($proRec['project_description'],350)}}</div>
                    </div>
                    
                    <div class="row">
                      <div class="category-new">{{$proRec['category_details']['category_title'] or 'NA'}}</div>
                      <div class="clearfix"></div>

                      @if(isset($proRec['project_skills']) && count($proRec['project_skills']) > 0)
                      @foreach($proRec['project_skills'] as $key=> $skills)
                      @if(isset($skills['skill_data']['skill_name']) && $skills['skill_data']['skill_name'] != "")
                        <div class="skils-project">
                            <ul>
                              <li>{{ str_limit($skills['skill_data']['skill_name'],25) }}</li>
                            </ul>
                        </div>
                      @endif
                      @endforeach
                      @endif

                    
                    </div>
                  </div>
                  <div class="clr"></div>
                  <hr/>
                  @endforeach 
                @else
                  <div class="search-grey-bx">
                      <div class="no-record">
                          {{trans('expert/projects/awarded_projects.text_sorry_no_record_found')}}
                      </div>
                  </div>
                @endif


            </div>
            <!-- Pagination -->
            @include('front.common.pagination_view', ['paginator' => $arr_canceled_projects])

        </div>
    </form>
    </div>
    
</div>


@stop