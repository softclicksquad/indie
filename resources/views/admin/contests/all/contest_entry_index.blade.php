@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/data-tables/latest/dataTables.bootstrap.min.css">
<div class="page-title">
   <div></div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-suitcase"></i>
      <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-suitcase"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-suitcase"></i>
               {{$contest_title}}
               <span class="divider">
                    <i class="fa fa-angle-right"></i> 
                  </span>  
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
            @include('admin.layout._operation_status')
            <form name="frm_manage" id="frm_manage" method="POST" class="form-horizontal" action="{{$module_url_path}}/multi_action">
               {{ csrf_field() }}
               <div class="btn-toolbar pull-right clearfix">
                  <div class="btn-group"> 
                     <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                        title="Refresh" 
                        href="{{ $module_url_path }}"
                        style="text-decoration:none;">
                     <i class="fa fa-repeat"></i>
                     </a> 
                  </div>
               </div>
               <br/><br/>
               <div class="clearfix"></div>
               <div class="table-responsive" style="border:0">
                  <input type="hidden" name="multi_action" value="" />
                  <table class="table table-advance"  id="table1" >
                     <thead>
                        <tr>
                           <th>Entry Number</th>
                           <th>Entry Title</th>
                           <th>Expert Email</th>
                           <th>Is Sealed</th>
                           <th>Winner Choosed</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        @if(isset($arr_info) && sizeof($arr_info)>0)
                        @foreach($arr_info as $contest_entry)
                        <tr>
                           <td>{{$contest_entry['entry_number'] or 'N/A'}}</td>
                           <td> {{isset($contest_entry['title'])?$contest_entry['title']:''}}</td>
                           <td>{{$contest_entry['expert_details']['user_details']['email'] or 'N/A'}}</td>
                           <td> 
                           @if(isset($contest_entry['sealed_entry']) && $contest_entry['sealed_entry'] ==0){{'Not Sealed'}} @endif
                           @if(isset($contest_entry['sealed_entry']) && $contest_entry['sealed_entry'] ==1){{'Sealed'}} @endif
                           </td>
                           <td> {{isset($contest_entry['is_winner'])?$contest_entry['is_winner']:''}} </td>
                           <td> 
                            <a href="{{ $module_url_path.'/show-entry-details/'.base64_encode($contest_entry['id']) }}"  title="Show" class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip">
                              <i class="fa fa-eye" ></i>
                            </a>

                            @if(isset($contest_entry['is_admin_chat_initiated']) && $contest_entry['is_admin_chat_initiated'] == '1')
                              <a target="_blank" class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" href="{{ url('/admin/twilio-chat/contest').'/'.base64_encode($contest_entry['id']) }}" data-original-title="View">
                               <i class="fa fa-comment" ></i>
                              </a>
                            @endif
                            
                          </td>
                        </tr>
                        @endforeach
                        @endif
                     </tbody>
                  </table>
                </div>
               <div>   
              </div>
            </form>
         </div>
      </div>
   </div>
</div>
<!-- END Main Content -->
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#entriesmenu').show();
  })
   function confirm_delete() {
      alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
          if (e) {
              return true;
          } else {
              return false;
          }
      });
   }
   function check_multi_action(frm_id,action) {
     var frm_ref = jQuery("#"+frm_id);
     if(jQuery(frm_ref).length && action!=undefined && action!=""){
       if(action == 'delete'){
            if (!confirm_delete()) {
              return false;
            }
       }
       /* Get hidden input reference */
       var input_multi_action = jQuery('input[name="multi_action"]');
       if(jQuery(input_multi_action).length){
         /* Set Action in hidden input*/
         jQuery('input[name="multi_action"]').val(action);
         /*Submit the referenced form */
         jQuery(frm_ref)[0].submit();
       } else {
         console.warn("Required Hidden Input[name]: multi_action Missing in Form ")
       }
     } else {
         console.warn("Required Form[id]: "+frm_id+" Missing in Current category ")
     }
   }
</script>
@stop