@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->

<div class="page-title">
 <div>
 </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
 <ul class="breadcrumb">
  <li>
   <i class="fa fa-home"></i>
   <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
 </li>
 <span class="divider">
  <i class="fa fa-angle-right"></i>
  <i class="fa fa-gears"></i>
  @php 
  $previous_page = "Manage All Contest Entries";
  $back_url      = $module_url_path.'/manage';    
  @endphp
  <a href="{{ $back_url or '#'}}">{{ $previous_page or ''}}</a>
</span> 
<span class="divider">
  <i class="fa fa-angle-right"></i>
  <i class="fa fa-gear"></i>
</span>
<li class="active">{{ $page_title or 'N/A' }}</li>
</ul>
</div>
<!-- END Breadcrumb -->

<!-- BEGIN Main Content -->
<div class="row">
 <div class="col-md-12">
  <div class="box">
   <div class="box-title">
    <h3>
     <i class="fa fa-gear"></i>
     {{$arr_info['title'] or 'N/A'}}
     <span class="divider">
      <i class="fa fa-angle-right"></i> 
    </span>  
    {{ $page_title or 'N/A' }}
  </h3>
  <div class="box-tool">
   <a data-action="collapse" href="#"></a>
   <a data-action="close" href="#"></a>
 </div>
</div>
<div class="box-content">
 @include('admin.layout._operation_status')
 <div class="alert alert-success" id="ajax_success" style="display:none;">
  <button data-dismiss="alert" class="close">×</button>
  <strong>Success!</strong>
  <div id="ajax_sub_success"></div>
</div>
<div class="alert alert-danger" id="ajax_error" style="display:none;">
  <button data-dismiss="alert" class="close">×</button>
  <strong>Error!</strong> 
  <div id="ajax_sub_error"></div>
</div>
<div class="row">
  <form name="validation-form" id="validation-form" method="POST" action="javascript:void(0)" class="form-horizontal"  enctype="multipart/form-data">
   {{ csrf_field() }}
   <div class="col-md-6">
    <div class="form-group">
     <label class="col-sm-3 control-label">Contest Title:</label>
     <div class="col-sm-9 controls">
      <div class="client-name-block"></div>
      <div class="client-name-block">{{$arr_info['contest_details']['contest_title'] or 'N/A'}}</div>
    </div>
  </div>
  <div class="form-group">
   <label class="col-sm-3 control-label">Contest Entry Number:</label>
   <div class="col-sm-9 controls">
    <div class="client-name-block">{{$arr_info['entry_number'] or 'N/A'}}</div>
  </div>
</div>
<div class="form-group">
 <label class="col-sm-3 control-label">Contest Entry Title:</label>
 <div class="col-sm-9 controls">
  <div class="client-name-block"></div>
  <div class="client-name-block">{{$arr_info['title'] or 'N/A'}}</div>
</div>
</div>
<div class="form-group">
 <label class="col-sm-3 control-label">Contest Entry Description:</label>
 <div class="col-sm-9 controls">
  <div class="client-name-block" style="text-align: justify;">{{$arr_info['description'] or 'N/A'}}</div>
</div>
</div>
<div class="form-group">
 <label class="col-sm-3 control-label">Contest Entry Status:</label>
 <div class="col-sm-9 controls">
  <div class="client-name-block">
   @if(isset($arr_info['sealed_entry']) && $arr_info['sealed_entry'] == '0')
   <label>NOT SEALED</label>
   @endif
   @if(isset($arr_info['sealed_entry']) && $arr_info['sealed_entry'] == '1')
   <label>SEALED</label>
   @endif
 </div>
 <div class="client-name-block"></div>
</div>
</div>
<input type="hidden" name="contest_id" value="@if(isset($arr_info['id'])){{base64_encode($arr_info['id'])}} @endif" id="contest_id">
<div class="form-group">
 <label class="col-sm-3 control-label">Winner Choosed:</label>
 <div class="col-sm-9 controls">
  <div class="client-name-block">
    {{$arr_info['is_winner'] or 'N/A'}}
  </div>
  <div class="client-name-block"></div>
</div>
</div>
<div class="form-group">
 <label class="col-sm-3 control-label">Client :</label>
 <div class="col-sm-9 controls">
  <div class="client-name-block">
    {{$arr_info['client_user_details']['first_name'] or 'N/A'}}
    @php $client_lastname = isset($arr_info['client_user_details']['last_name'])? substr($arr_info['client_user_details']['last_name'], 0,1):''; @endphp 
    {{ $client_lastname or 'N/A'}}
  </div>
</div>
</div>
<div class="form-group">
 <label class="col-sm-3 control-label">Client Email:</label>
 <div class="col-sm-9 controls">
  <div class="client-name-block">
    {{$arr_info['client_user_details']['user_details']['email'] or 'N/A'}}
  </div>
</div>
</div>
<div class="form-group">
 <label class="col-sm-3 control-label">Client Phone Number:</label>
 <div class="col-sm-9 controls">
  <div class="client-name-block">
    +{{$arr_info['client_user_details']['phone_code'] or 'N/A'}}
    {{$arr_info['client_user_details']['phone_number'] or 'N/A'}}
  </div>
</div>
</div>
<div class="form-group">
 <label class="col-sm-3 control-label">Expert:</label>
 <div class="col-sm-9 controls">
  <div class="client-name-block">
    {{$arr_info['expert_details']['first_name'] or 'N/A'}}
    @php $expert_lastname = isset($arr_info['expert_details']['last_name'])? substr($arr_info['expert_details']['last_name'], 0,1):''; @endphp 
    {{ $expert_lastname or 'N/A'}}
  </div>
</div>
</div>
<div class="form-group">
 <label class="col-sm-3 control-label">Expert Email:</label>
 <div class="col-sm-9 controls">
  <div class="client-name-block">
    {{$arr_info['expert_details']['user_details']['email'] or 'N/A'}}
  </div>
</div>
</div>
<div class="form-group">
 <label class="col-sm-3 control-label">Expert Phone Number:</label>
 <div class="col-sm-9 controls">
  <div class="client-name-block">
    +{{$arr_info['expert_details']['phone_code'] or 'N/A'}}
    {{$arr_info['expert_details']['phone_number'] or 'N/A'}}
  </div>
</div>
</div>

<div class="form-group">
    <div class="col-sm-3">
      <label class="control-label pull-right"> File Attachments:</label>
    </div>  
    <div class="col-sm-9">
      @if(isset($arr_info) && isset($arr_info['contest_entry_files']) && sizeof($arr_info['contest_entry_files'])>0)
        @php $files = 0; @endphp
        @foreach($arr_info['contest_entry_files'] as $key=>$value)
          @php 
          $is_file_exist   = file_exists('public/uploads/front/postcontest/send_entry/'.$value['image']); 
          @endphp 
            @if($is_file_exist)
                  @php 
                  $file_attachment = isset($value['image'])?$value['image'] :'NA';
                  $file_format     = get_attatchment_format($file_attachment);
                  $files++;
                  @endphp
                  <div class="col-sm-4 controls">
                     <div class="coll-img">
                        <div class="img-block">
                          @if($value['is_admin_deleted']==0)
                            @if(isset($file_format) &&
                              $file_format == 'doc'  ||
                              $file_format == 'docx' ||
                              $file_format == 'odt'  ||
                              $file_format == 'pdf'  ||
                              $file_format == 'txt'  ||
                              $file_format == 'xlsx')
                              <div class="image-section">
                              <img src="{{url('/public')}}/front/images/file_formats/{{$file_format}}.png" class="img-responsive" style="height: 100px;">
                              </div>
                            @else
                              <div class="image-section">
                                <div class="img">
                                <img src="{{url('/public')}}{{config('app.project.img_path.contest_send_entry_files')}}{{$value['image'] or 'no-image.jpg'}}" class="img-responsive" style="height: 100px;">
                                </div>                     
                              </div>
                            @endif
                            {{-- <div align="right"> --}}
                            <label><i style="font-size:1.3em;">File#{{$value['file_no'] or 'N/A'}}</i></label>
                            
                            <a style="font-size:1.4em;float: right;" title ="Remove" class="remove-file" value="{{$value['id']}}" file-name="{{$value['image'] or ''}}"><i class="fa fa-remove"></i></a>
                            {{-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; --}}
                             <a style="font-size:1.4em; float: right;padding-right: 5px;" download href="{{url('/public')}}{{config('app.project.img_path.contest_send_entry_files')}}{{$value['image'] or 'no-image.jpg'}}" title="Download"><i class="fa fa-cloud-download"></i></a> 
                            
                            {{-- </div> --}}
                          @else
                          <div class="image-section">
                              <img src="{{url('/public')}}{{config('app.project.img_path.contest_send_entry_files')}}/no-image.jpg"style="height: 100px;">
                              <br>
                                 <b><i> Deleted By Admin</i></b><label><i style="font-size:1.0em;">File#{{$value['file_no'] or 'N/A'}}</i></label>
                                 <br>
                            {{-- <div class="img">
                              <img src="{{get_resized_image_path('temp_file','temp',"100","129","Deleted By Admin")}}">
                                <br>
                                 <label><b><i style="font-size:1.0em;">File#{{$value['file_no'] or 'N/A'}}</i></b></label>
                                <br>
                                 <i> Deleted By Admin</i>
                            </div> --}}
                          </div>
                          @endif
                        </div>
                      </div>
                  </div>
                  @php $output = ($files%3); if($output == 0){ echo '<div class="clearfix"></div><br><br>'; }  @endphp
            @endif
        @endforeach
      @else
        No Attament Available
      @endif
      </div>
    </div>
</div>
</form>
</div>          
</div>
</div>
</div>
</div>
<!-- END Main Content -->
<script type="text/javascript">
  $(document).ready(function(){
    $('#entriesmenu').show();
  })
  $('.remove-file').click(function(){
    var file_id   = parseInt($(this).attr("value"));
    var file_name = $(this).attr("file-name");
    alertify.confirm("Are you sure? You you want to remove this file", function (e) {
          if (e) {
              remove_file(file_id,file_name);
              return true;
          } else {
              return false;
          }
      });
  });
  function remove_file(file_id='',file_name){ 
    var token = "{{csrf_token()}}";
    $.ajax({
      url :"{{$module_url_path}}/manage/remove-file",
      method :"POST",
      data:{_token : token, file_id: file_id,file_name:file_name},
      success :function(res){
        if(res) {
          location.reload();  
        }
      }
    })
  }
</script>
@stop