@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
 <div>
 </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
 <ul class="breadcrumb">
  <li>
   <i class="fa fa-home"></i>
   <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
 </li>
 <span class="divider">
  <i class="fa fa-angle-right"></i>
  <i class="fa fa-gears"></i>
  @php 
  $previous_page = "Manage All Contests";
  $back_url      = $module_url_path.'/manage';    
  @endphp
  <a href="{{ $back_url or '#'}}">{{ $previous_page or ''}}</a>
</span> 
<span class="divider">
  <i class="fa fa-angle-right"></i>
  <i class="fa fa-gear"></i>
</span>
<li class="active">{{ $page_title or 'N/A' }}</li>
</ul>
</div>
<!-- END Breadcrumb -->

<!-- BEGIN Main Content -->
<div class="row">
 <div class="col-md-12">
  <div class="box">
   <div class="box-title">
    <h3>
     <i class="fa fa-gear"></i>
     {{$arr_info['contest_title'] or 'N/A'}}
     <span class="divider">
      <i class="fa fa-angle-right"></i> 
    </span>  
    {{ $page_title or 'N/A' }}
  </h3>
  <div class="box-tool">
   <a data-action="collapse" href="#"></a>
   <a data-action="close" href="#"></a>
 </div>
</div>
<div class="box-content">
 @include('admin.layout._operation_status')
 <div class="alert alert-success" id="ajax_success" style="display:none;">
  <button data-dismiss="alert" class="close">×</button>
  <strong>Success!</strong>
  <div id="ajax_sub_success"></div>
</div>
<div class="alert alert-danger" id="ajax_error" style="display:none;">
  <button data-dismiss="alert" class="close">×</button>
  <strong>Error!</strong> 
  <div id="ajax_sub_error"></div>
</div>
<div class="row">
  <form name="validation-form" id="validation-form" method="POST" action="javascript:void(0)" class="form-horizontal"  enctype="multipart/form-data">
   {{ csrf_field() }}
   <div class="col-md-6">
    <div class="form-group">
     <label class="col-sm-3 control-label">Contest Title:</label>
     <div class="col-sm-9 controls">
      <div class="client-name-block">{{$arr_info['contest_title'] or 'N/A'}}</div>
    </div>
  </div>
  <div class="form-group">
   <label class="col-sm-3 control-label">Contest Category:</label>
   <div class="col-sm-9 controls">
    <div class="client-name-block"></div>
    <div class="client-name-block">{{$arr_info['category_details']['category_title'] or 'N/A'}}</div>
  </div>
</div>
<div class="form-group">
  <label class="col-sm-3 control-label">Client Name:</label>
  <div class="col-sm-9 controls">
    <div class="client-name-block"></div>
    <div class="client-name-block">
      {{$arr_info['client_user_details']['first_name'] or 'N/A'}}
      @php $last_name = isset($arr_info['client_user_details']['last_name'])? substr($arr_info['client_user_details']['last_name'],0,1):''; @endphp
      {{$last_name or 'N/A'}}
    </div>
  </div>
</div>
<div class="form-group">
  <label class="col-sm-3 control-label">Client Email:</label>
  <div class="col-sm-9 controls">
    <div class="client-name-block"></div>
    <div class="client-name-block">
      {{$arr_info['user_details']['email'] or 'N/A'}}
    </div>
  </div>
</div>
<div class="form-group">
  <label class="col-sm-3 control-label">Client Mobile Number:</label>
  <div class="col-sm-9 controls">
    <div class="client-name-block"></div>
    <div class="client-name-block">
      +{{$arr_info['client_user_details']['phone_code'] or 'N/A'}}
      {{$arr_info['client_user_details']['phone_number'] or 'N/A'}}
    </div>
  </div>
</div>
<div class="form-group">
 <label class="col-sm-3 control-label">Contest Description:</label>
 <div class="col-sm-9 controls">
  <div class="client-name-block" style="text-align: justify;">{{$arr_info['contest_description'] or 'N/A'}}</div>
</div>
</div>
<div class="form-group">
 <label class="col-sm-3 control-label">Contest Additional Information:</label>
 <div class="col-sm-9 controls">
  <div class="client-name-block" style="text-align: justify;">{{$arr_info['contest_additional_info'] or 'N/A'}}</div>
</div>
</div>
<div class="form-group">
 <label class="col-sm-3 control-label">End Date:</label>
 <div class="col-sm-9 controls">
  <div class="client-name-block">{{$arr_info['contest_end_date'] or 'N/A'}}</div>
  <div class="client-name-block"></div>
</div>
</div>
<div class="form-group">
 <label class="col-sm-3 control-label">Contest Cost:</label>
 <div class="col-sm-9 controls">
  <div class="client-name-block">
  </div>
  <div class="client-name-block">{{$arr_info['contest_price'] or 'N/A'}} {{$arr_info['contest_currency'] or 'N/A'}}</div>
</div>
</div>
<div class="form-group">
 <label class="col-sm-3 control-label">Contest Status:</label>
 <div class="col-sm-9 controls">
  <div class="client-name-block">
   @if(isset($arr_info['contest_status']) && $arr_info['contest_status'] == '0')
   <a href="javascript:void(0)" class="btn btn-success">OPEN</a>
   @endif
   @if(isset($arr_info['contest_status']) && $arr_info['contest_status'] == '1')
   <a href="javascript:void(0)" class="btn default-btn">ENDING</a>
   @endif
   @if(isset($arr_info['contest_status']) && $arr_info['contest_status'] == '2')
   <l <a href="javascript:void(0)" class="btn btn-danger">ENDED</a>
     @endif
   </div>
   <div class="client-name-block"></div>
 </div>
</div>
<input type="hidden" name="contest_id" value="{{base64_encode($arr_info['id'])}}" id="contest_id">
<div class="form-group">
 <label class="col-sm-3 control-label">Winner Choosed:</label>
 <div class="col-sm-9 controls">
  <div class="client-name-block">
    {{$arr_info['winner_choose'] or 'N/A'}}
  </div>
  <div class="client-name-block"></div>
</div>
</div>
{{-- <div class="form-group">
 <label class="col-sm-3 control-label">Contest Attachment :</label>
 <div class="col-sm-9 controls">
  <div class="coll-img p-relative">
    @php 
    $file_attachment = isset($arr_info['contest_attachment'])?$arr_info['contest_attachment'] :'NA';
    $file_format     = get_attatchment_format($file_attachment);

    $file            = url('/').config('app.project.img_path.contest_files').$arr_info['contest_attachment'];

                      // echo $file_attachment ;
    @endphp 
    @if($file_attachment) 
    @php 
    $is_file_exist   = file_exists('public/uploads/front/postcontest/'.$file_attachment); 
    @endphp

    @if(isset($file_format) &&
      $file_format == 'doc'  ||
      $file_format == 'docx' ||
      $file_format == 'odt'  ||
      $file_format == 'pdf'  ||
      $file_format == 'txt'  ||
      $file_format == 'xlsx')
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 coll-list">
        <div class="coll-list-block">
          <div class="coll-img p-relative">
            @if($is_file_exist)
            <img src="{{url('/public')}}/front/images/file_formats/{{$file_format}}.png" class="img-responsive">
            @else
            <img src="{{url('/public')}}/front/images/file_formats/image.png"  class="img-responsive" alt=""/>
            @endif
            <div class="coll-details">
              <div class="coll-person">
                <div class="coll-name"><!-- File #1 --></div>
              </div>
                                        <!-- <div class="avg-rating">
                                            <ul>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                            </ul>
                                          </div> -->
                                        </div>
                                        <div class="list-hover">
                                        </div>
                                      </div>
                                      <div class="design-title">
                                        <a style="font-size:2em;" download href="{{url('/public')}}{{config('app.project.img_path.contest_files')}}{{$arr_info['contest_attachment']}}" class="view-button"><i class="fa fa-cloud-download"></i></a>
                                      </div>
                                    </div>
                                  </div>
                                  @else

                                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 coll-list">
                                    <div class="coll-list-block">
                                      <div class="coll-img p-relative">
                                        @if($is_file_exist)
                                        <img src="{{url('/public')}}{{config('app.project.img_path.contest_files')}}{{$arr_info['contest_attachment']}}" height="100" width="100" class="img-responsive">
                                        @else
                                        <img src="{{url('/public')}}/front/images/file_formats/image.png"  class="img-responsive" alt=""/>
                                        @endif
                                        <div class="coll-details">
                                          <div class="coll-person">
                                            <div class="coll-name"><!-- File #1 --></div>
                                          </div>
                                        <!-- <div class="avg-rating">
                                            <ul>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                            </ul>
                                          </div> -->
                                        </div>
                                        <div class="list-hover">
                                        </div>
                                      </div>
                                      <div class="design-title">
                                        <a style="font-size:2em;" download href="{{url('/public')}}{{config('app.project.img_path.contest_files')}}{{$arr_info['contest_attachment']}}" class="view-button"><i class="fa fa-cloud-download"></i></a>
                                      </div>
                                    </div>
                                  </div>

                                  @endif

                                  @else

                                  No Attachment Available 

                                  @endif

                                  

                                </div>
                                <div class="client-name-block"></div>
                              </div>
                            </div> --}}
                          </div>
                        </form>
                      </div>          
                    </div>
                  </div>
                </div>
              </div>
              <!-- END Main Content -->
              @stop