@extends('admin.layout.master')
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-desktop"></i>
      <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-list"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->

<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-text-width"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
            @include('admin.layout._operation_status') 
            <div class="tabbable">
               <form name="packg-validation-form" id="packg-validation-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/update" enctype="multipart/form-data"  files ="true">
                  {{ csrf_field() }}
                  <input type="hidden" name="pricing_id" id="pricing_id" value="@if(isset($pricing['id']) && $pricing['id']!=""){{$pricing['id']}}@else'0'@endif">
                  <div class="tab-pane" >
                     <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label" for="page_title">Price Type<i class="red">*</i></label>
                        <div class="col-sm-6 col-lg-4 controls">
                           <select class="form-control" id="price_type" name="price_type">
                              <option value="fix"       @if(isset($pricing['price_type']) && $pricing['price_type']=="fix"){{'selected'}}@endif>Fix</option>
                              <option value="prcentage" @if(isset($pricing['price_type']) && $pricing['price_type']=="prcentage"){{'selected'}}@endif>Percentage</option>
                           </select>
                           <span class='error'>{{ $errors->first('price') }}</span>
                        </div>
                     </div>
                  </div>
                  <div class="tab-pane" >
                     <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label" for="page_title">Price<i class="red">*</i></label>
                        <div class="col-sm-6 col-lg-4 controls">
                           <input type="text" name="price" id="price" class="form-control" value="@if(isset($pricing['price']) && $pricing['price']!=""){{$pricing['price']}}@else{{old('price')}}@endif" data-rule-required="true" data-rule-maxlength="255" data-rule-number='true' placeholder="Price">
                           <span class='error'>{{ $errors->first('price') }}</span>
                           <span class='error' id="price_error"></span>

                        </div>
                     </div>
                  </div>
                  <br>
                  <div class="form-group">
                     <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                        <input type="submit" value="Save" onclick="return validate();" class="btn btn btn-primary">
                        <a href="{{ $module_url_path}}" class="btn btn btn-danger">Cancel</a>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
 $(document).ready(function(){
   $('#packg-validation-form').validate();
 });

 function validate()
 {
   var price_type = $('#price_type').val();
   var price = $('#price').val();
   $('#price_error').html('');

   if(price_type == 'prcentage' && (price<1 || price>99))
   {
      $('#price_error').html('Enter valid percentage');
      return false;
   }
   return true;
 }
</script>
@stop
