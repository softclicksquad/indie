  @extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/data-tables/latest/dataTables.bootstrap.min.css">
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-suitcase"></i>
      <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-suitcase"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-suitcase"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
            @include('admin.layout._operation_status')
            <form name="frm_manage" id="frm_manage" method="POST" class="form-horizontal" action="{{$module_url_path}}/multi_action">
               {{ csrf_field() }}
               <div class="btn-toolbar pull-right clearfix">
                  <div class="btn-group">
                     <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                        title="Add Category" 
                        href="{{ $module_url_path.'/create'}}" 
                        style="text-decoration:none;">
                     <i class="fa fa-plus"></i>
                     </a> 
                  </div>
                  <div class="btn-group">
                     <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                        title="Multiple Active/Unblock" 
                        href="javascript:void(0);" 
                        onclick="javascript : return check_multi_action('frm_manage','activate');" 
                        style="text-decoration:none;">
                     <i class="fa fa-unlock"></i>
                     </a> 
                  </div>
                  <div class="btn-group">
                     <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                        title="Multiple Deactive/Block" 
                        href="javascript:void(0);" 
                        onclick="javascript : return check_multi_action('frm_manage','deactivate');"  
                        style="text-decoration:none;">
                     <i class="fa fa-lock"></i>
                     </a> 
                  </div>
                  <div class="btn-group">    
                     <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                        title="Multiple Delete" 
                        href="javascript:void(0);" 
                        onclick="javascript : return check_multi_action('frm_manage','delete');"  
                        style="text-decoration:none;">
                     <i class="fa fa-trash-o"></i>
                     </a>
                  </div>
                  <div class="btn-group"> 
                     <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                        title="Refresh" 
                        href="{{ $module_url_path }}/manage"
                        style="text-decoration:none;">
                     <i class="fa fa-repeat"></i>
                     </a> 
                  </div>
               </div>
               <br/><br/>
               <div class="clearfix"></div>
               <div class="table-responsive" style="border:0">
                  <input type="hidden" name="multi_action" value="" />
                  <table class="table table-advance"  id="table1" >
                     <thead>
                        <tr>
                           <th > <input type="checkbox" name="mult_change" id="mult_change" /></th>
                           <th>Highlighted day's</th>
                           <th>USD Prize</th>
                           <th>EUR Prize</th>
                           <th>GBP Prize</th>
                           <th>PLN Prize</th>
                           <th>CHF Prize</th>
                           <th>NOK Prize</th>
                           <th>SEK Prize</th>
                           <th>DKK Prize</th>
                           <th>CAD Prize</th>
                           <th>ZAR Prize</th>
                           <th>AUD Prize</th>
                           <th>HKD Prize</th>
                           <th>CZK Prize</th>
                           <th>JPY Prize</th>
                           <th>Status</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        @if(isset($packages) && sizeof($packages)>0)
                        @foreach($packages as $pack)
                        <tr>
                           <td> 
                              <input type="checkbox" 
                                     name="checked_record[]"  
                                     value="{{ base64_encode($pack['id']) }}" /> 
                           </td>
                           <td> {{isset($pack['highlited_days'])?$pack['highlited_days']:''}} Day's</td>
                           <td> USD {{isset($pack['USD_prize'])?$pack['USD_prize']:'0'}} </td>
                           <td> EUR {{isset($pack['EUR_prize'])?$pack['EUR_prize']:'0'}} </td>
                           <td> GBP {{isset($pack['GBP_prize'])?$pack['GBP_prize']:'0'}} </td>
                           <td> PLN {{isset($pack['PLN_prize'])?$pack['PLN_prize']:'0'}} </td>
                           <td> CHF {{isset($pack['CHF_prize'])?$pack['CHF_prize']:'0'}} </td>
                           <td> NOK {{isset($pack['NOK_prize'])?$pack['NOK_prize']:'0'}} </td>
                           <td> SEK {{isset($pack['SEK_prize'])?$pack['SEK_prize']:'0'}} </td>
                           <td> DKK {{isset($pack['DKK_prize'])?$pack['DKK_prize']:'0'}} </td>
                           <td> CAD {{isset($pack['CAD_prize'])?$pack['CAD_prize']:'0'}} </td>
                           <td> ZAR {{isset($pack['ZAR_prize'])?$pack['ZAR_prize']:'0'}} </td>
                           <td> AUD {{isset($pack['AUD_prize'])?$pack['AUD_prize']:'0'}} </td>
                           <td> HKD {{isset($pack['HKD_prize'])?$pack['HKD_prize']:'0'}} </td>
                           <td> CZK {{isset($pack['CZK_prize'])?$pack['CZK_prize']:'0'}} </td>
                           <td> JPY {{isset($pack['JPY_prize'])?$pack['JPY_prize']:'0'}} </td>
                           <td> 
                                @if(isset($pack['status']) && $pack['status']==1)
                                <a href="{{ $module_url_path.'/activate/'.base64_encode($pack['id']) }}"  class="btn btn-danger"><i class="fa fa-lock"></i></a>
                                @else
                                <a href="{{ $module_url_path.'/deactivate/'.base64_encode($pack['id']) }}"  class="btn btn-success"><i class="fa fa-unlock"></i></a>
                                @endif
                           </td>
                           <td style="white-space:nowrap"> 
                              <a href="{{ $module_url_path.'/edit/'.base64_encode($pack['id']) }}"  title="Edit" class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip">
                              <i class="fa fa-edit" ></i>
                              </a>  
                              &nbsp;  
                             <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" href="javascript:void(0)" 
                                 onclick="javascript:return confirm_delete('{{ $module_url_path.'/delete/'.base64_encode($pack['id'])}}')"  title="Delete">
                              <i class="fa fa-trash" ></i>
                              </a>   
                           </td>
                        </tr>
                        @endforeach
                        @endif
                     </tbody>
                  </table>
                </div>
               <div>   
              </div>
            </form>
         </div>
      </div>
   </div>
</div>
<!-- END Main Content -->
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
   function confirm_delete(url) {
      alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
          if (e) {
              if(url != "" && url != undefined){
                showProcessingOverlay();
                window.location.href=url;
              }
              return true;
          } else {
              return false;
          }
      });
   }
   function check_multi_action(frm_id,action)
   {
     var frm_ref = jQuery("#"+frm_id);
     if(jQuery(frm_ref).length && action!=undefined && action!="")
     {
      /* Get hidden input reference */
       var checked_record = jQuery('input[name="checked_record[]"]:checked');
       if (jQuery(checked_record).size() < 1)
       {
          alertify.alert('Please select at least one record.');
          return false;
       }
       var input_multi_action = jQuery('input[name="multi_action"]');
       if(jQuery(input_multi_action).length){
         /* Set Action in hidden input*/
         jQuery('input[name="multi_action"]').val(action);
         /*Submit the referenced form */
                alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
                if (e) {
                    showProcessingOverlay();
                    jQuery(frm_ref)[0].submit();
                    return true;
                } else {
                    return false;
                }
            });
       } else {
         console.warn("Required Hidden Input[name]: multi_action Missing in Form ")
       }
     } else {
         console.warn("Required Form[id]: "+frm_id+" Missing in Current category ")
     }
   }
</script>
@stop