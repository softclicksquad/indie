@extends('admin.layout.master')
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-desktop"></i>
      <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-list"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->

<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-text-width"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
            @include('admin.layout._operation_status') 
            <div class="tabbable">
               <form name="packg-validation-form" id="packg-validation-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/update" enctype="multipart/form-data"  files ="true">
                  {{ csrf_field() }}
                  <input type="hidden" name="package_id" id="package_id" value="@if(isset($pacakage['id']) && $pacakage['id']!=""){{$pacakage['id']}}@else'0'@endif">
                  <div class="tab-pane" >
                     <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label" for="page_title">Highlight Day's<i class="red">*</i></label>
                        <div class="col-sm-6 col-lg-4 controls">
                           <input type="text" name="highlight_days" class="form-control" value="@if(isset($pacakage['highlited_days']) && $pacakage['highlited_days']!=""){{$pacakage['highlited_days']}}@else{{old('highlited_days')}}@endif" data-rule-required="true" data-rule-maxlength="255" data-rule-number='true' placeholder="Enter Highlight days">
                           <span class='error'>{{ $errors->first('highlight_days') }}</span>
                        </div>
                     </div>
                  </div>
                  <div class="tab-pane" >
                     <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label" for="page_title">USD Price<i class="red">*</i></label>
                        <div class="col-sm-6 col-lg-4 controls">
                           <input type="text" name="USD_prize" value="@if(isset($pacakage['USD_prize']) && $pacakage['USD_prize']!=""){{$pacakage['USD_prize']}}@else{{old('USD_prize')}}@endif" class="form-control" value="{{old('USD_prize')}}" data-rule-required="true" data-rule-maxlength="255" data-rule-number='true' placeholder="USD prize">
                           <span class='error'>{{ $errors->first('USD_prize') }}</span>
                        </div>
                     </div>
                  </div>
                  <br>
                  
                  <div class="tab-pane" >
                     <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label" for="page_title">EUR prize<i class="red">*</i></label>
                        <div class="col-sm-6 col-lg-4 controls">
                           <input type="text" name="EUR_prize" value="@if(isset($pacakage['EUR_prize']) && $pacakage['EUR_prize']!=""){{$pacakage['EUR_prize']}}@else{{old('EUR_prize')}}@endif" class="form-control" value="{{old('EUR_prize')}}" data-rule-required="true" data-rule-maxlength="255" data-rule-number='true' placeholder="EUR prize">
                           <span class='error'>{{ $errors->first('EUR_prize') }}</span>
                        </div>
                     </div>
                  </div>
                  <br>
                  <div class="tab-pane" >
                     <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label" for="page_title">GBP prize<i class="red">*</i></label>
                        <div class="col-sm-6 col-lg-4 controls">
                           <input type="text" name="GBP_prize" value="@if(isset($pacakage['GBP_prize']) && $pacakage['GBP_prize']!=""){{$pacakage['GBP_prize']}}@else{{old('GBP_prize')}}@endif" class="form-control" value="{{old('GBP_prize')}}" data-rule-required="true" data-rule-maxlength="255" data-rule-number='true' placeholder="GBP prize">
                           <span class='error'>{{ $errors->first('GBP_prize') }}</span>
                        </div>
                     </div>
                  </div>
                  <br>
                  <div class="tab-pane" >
                     <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label" for="page_title">PLN prize<i class="red">*</i></label>
                        <div class="col-sm-6 col-lg-4 controls">
                           <input type="text" name="PLN_prize" value="@if(isset($pacakage['PLN_prize']) && $pacakage['PLN_prize']!=""){{$pacakage['PLN_prize']}}@else{{old('PLN_prize')}}@endif" class="form-control" value="{{old('PLN_prize')}}" data-rule-required="true" data-rule-maxlength="255" data-rule-number='true' placeholder="PLN prize">
                           <span class='error'>{{ $errors->first('PLN_prize') }}</span>
                        </div>
                     </div>
                  </div>
                  <br>
                  <div class="tab-pane" >
                     <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label" for="page_title">CHF prize<i class="red">*</i></label>
                        <div class="col-sm-6 col-lg-4 controls">
                           <input type="text" name="CHF_prize" value="@if(isset($pacakage['CHF_prize']) && $pacakage['CHF_prize']!=""){{$pacakage['CHF_prize']}}@else{{old('CHF_prize')}}@endif" class="form-control" value="{{old('CHF_prize')}}" data-rule-required="true" data-rule-maxlength="255" data-rule-number='true' placeholder="CHF prize">
                           <span class='error'>{{ $errors->first('CHF_prize') }}</span>
                        </div>
                     </div>
                  </div>
                  <br>
                  <div class="tab-pane" >
                     <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label" for="page_title">NOK prize<i class="red">*</i></label>
                        <div class="col-sm-6 col-lg-4 controls">
                           <input type="text" name="NOK_prize" value="@if(isset($pacakage['NOK_prize']) && $pacakage['NOK_prize']!=""){{$pacakage['NOK_prize']}}@else{{old('NOK_prize')}}@endif" class="form-control" value="{{old('NOK_prize')}}" data-rule-required="true" data-rule-maxlength="255" data-rule-number='true' placeholder="NOK prize">
                           <span class='error'>{{ $errors->first('NOK_prize') }}</span>
                        </div>
                     </div>
                  </div>
                  <br>
                  <div class="tab-pane" >
                     <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label" for="page_title">SEK prize<i class="red">*</i></label>
                        <div class="col-sm-6 col-lg-4 controls">
                           <input type="text" name="SEK_prize" value="@if(isset($pacakage['SEK_prize']) && $pacakage['SEK_prize']!=""){{$pacakage['SEK_prize']}}@else{{old('SEK_prize')}}@endif" class="form-control" value="{{old('SEK_prize')}}" data-rule-required="true" data-rule-maxlength="255" data-rule-number='true' placeholder="SEK prize">
                           <span class='error'>{{ $errors->first('SEK_prize') }}</span>
                        </div>
                     </div>
                  </div>
                  <br>
                  <div class="tab-pane" >
                     <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label" for="page_title">DKK prize<i class="red">*</i></label>
                        <div class="col-sm-6 col-lg-4 controls">
                           <input type="text" name="DKK_prize" value="@if(isset($pacakage['DKK_prize']) && $pacakage['DKK_prize']!=""){{$pacakage['DKK_prize']}}@else{{old('DKK_prize')}}@endif" class="form-control" value="{{old('DKK_prize')}}" data-rule-required="true" data-rule-maxlength="255" data-rule-number='true' placeholder="DKK prize">
                           <span class='error'>{{ $errors->first('DKK_prize') }}</span>
                        </div>
                     </div>
                  </div>
                  <br>
                  <div class="tab-pane" >
                     <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label" for="page_title">CAD prize<i class="red">*</i></label>
                        <div class="col-sm-6 col-lg-4 controls">
                           <input type="text" name="CAD_prize" value="@if(isset($pacakage['CAD_prize']) && $pacakage['CAD_prize']!=""){{$pacakage['CAD_prize']}}@else{{old('CAD_prize')}}@endif" class="form-control" value="{{old('CAD_prize')}}" data-rule-required="true" data-rule-maxlength="255" data-rule-number='true' placeholder="CAD prize">
                           <span class='error'>{{ $errors->first('CAD_prize') }}</span>
                        </div>
                     </div>
                  </div>
                  <br>
                   <div class="tab-pane" >
                     <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label" for="page_title">ZAR prize<i class="red">*</i></label>
                        <div class="col-sm-6 col-lg-4 controls">
                           <input type="text" name="ZAR_prize" value="@if(isset($pacakage['ZAR_prize']) && $pacakage['ZAR_prize']!=""){{$pacakage['ZAR_prize']}}@else{{old('ZAR_prize')}}@endif" class="form-control" value="{{old('ZAR_prize')}}" data-rule-required="true" data-rule-maxlength="255" data-rule-number='true' placeholder="ZAR prize">
                           <span class='error'>{{ $errors->first('ZAR_prize') }}</span>
                        </div>
                     </div>
                  </div>
                  <br>
                  <div class="tab-pane" >
                     <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label" for="page_title">AUD prize<i class="red">*</i></label>
                        <div class="col-sm-6 col-lg-4 controls">
                           <input type="text" name="AUD_prize" value="@if(isset($pacakage['AUD_prize']) && $pacakage['AUD_prize']!=""){{$pacakage['AUD_prize']}}@else{{old('AUD_prize')}}@endif" class="form-control" value="{{old('AUD_prize')}}" data-rule-required="true" data-rule-maxlength="255" data-rule-number='true' placeholder="AUD prize">
                           <span class='error'>{{ $errors->first('AUD_prize') }}</span>
                        </div>
                     </div>
                  </div>
                  <br>
                  <div class="tab-pane" >
                     <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label" for="page_title">HKD prize<i class="red">*</i></label>
                        <div class="col-sm-6 col-lg-4 controls">
                           <input type="text" name="HKD_prize" value="@if(isset($pacakage['HKD_prize']) && $pacakage['HKD_prize']!=""){{$pacakage['HKD_prize']}}@else{{old('HKD_prize')}}@endif" class="form-control" value="{{old('HKD_prize')}}" data-rule-required="true" data-rule-maxlength="255" data-rule-number='true' placeholder="HKD prize">
                           <span class='error'>{{ $errors->first('HKD_prize') }}</span>
                        </div>
                     </div>
                  </div>
                  <br>  
                  <div class="tab-pane" >
                     <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label" for="page_title">CZK prize<i class="red">*</i></label>
                        <div class="col-sm-6 col-lg-4 controls">
                           <input type="text" name="CZK_prize" value="@if(isset($pacakage['CZK_prize']) && $pacakage['CZK_prize']!=""){{$pacakage['CZK_prize']}}@else{{old('CZK_prize')}}@endif" class="form-control" value="{{old('CZK_prize')}}" data-rule-required="true" data-rule-maxlength="255" data-rule-number='true' placeholder="CZK prize">
                           <span class='error'>{{ $errors->first('CZK_prize') }}</span>
                        </div>
                     </div>
                  </div>
                  <br>
                  <div class="tab-pane" >
                     <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label" for="page_title">JPY prize<i class="red">*</i></label>
                        <div class="col-sm-6 col-lg-4 controls">
                           <input type="text" name="JPY_prize" value="@if(isset($pacakage['JPY_prize']) && $pacakage['JPY_prize']!=""){{$pacakage['JPY_prize']}}@else{{old('JPY_prize')}}@endif" class="form-control" value="{{old('JPY_prize')}}" data-rule-required="true" data-rule-maxlength="255" data-rule-number='true' placeholder="JPY prize">
                           <span class='error'>{{ $errors->first('JPY_prize') }}</span>
                        </div>
                     </div>
                  </div>
                  <br>
                  <div class="form-group">
                     <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                        <input type="submit" value="Save" class="btn btn btn-primary">
                        <a href="{{url('/admin/project-priority-pacakges/manage')}}" class="btn btn btn-danger">Cancel</a>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
 $(document).ready(function(){
   $('#packg-validation-form').validate();
 });
</script>
@stop
