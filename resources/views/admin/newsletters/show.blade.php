      @extends('admin.layout.master')         


      @section('main_content')
      <!-- BEGIN Page Title -->
      <div class="page-title">
          <div>

          </div>
      </div>
      <!-- END Page Title -->

      <!-- BEGIN Breadcrumb -->
      <div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-desktop"></i>
      <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-list"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
      <!-- END Breadcrumb -->

 
        <!-- START Main Content -->


          <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-title">
                        <h3><i class="fa fa-file"></i> Newsletter Details</h3>
                        <div class="box-tool">
                          
                        </div>
                    </div>
                    <div class="box-content">
                        <div class="row">
                        <div class="col-md-1">
                        </div>
                            <div class="col-md-9 user-profile-info">
                                <p><span><b>Title:</b></span>{{ isset($arr_newsletter['title'])?$arr_newsletter['title']:'-' }}</p>

                                 <p><span><b>Subject:</b></span>{{ isset($arr_newsletter['subject'])?$arr_newsletter['subject']:'-' }}</p>

                                 <p><span><b>Message:</b></span> 
                                 <textarea name="news_message" class="form-control">{{isset($arr_newsletter['news_message'])?$arr_newsletter['news_message']:''}}</textarea>
                                 </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <!-- END Main Content -->

<script type="text/javascript">
   function saveTinyMceContent()
   {
     tinyMCE.triggerSave();
   }
   
   
   $(document).ready(function()
   {
     tinymce.init({
       selector: 'textarea',
       height:350,
       readonly:1,
       plugins: [
       'advlist autolink lists link image charmap print preview anchor',
       'searchreplace visualblocks code fullscreen',
       'insertdatetime media table contextmenu paste code'
       ],
       toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
       content_css: [
       '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
       '//www.tinymce.com/css/codepen.min.css'
       ]
     });  
   });
</script>
  @stop