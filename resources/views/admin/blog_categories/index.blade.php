@extends('admin.layout.master')    
@section('main_content')

<!-- BEGIN Page Title -->
<div class="page-title">
	<div>

	</div>
</div>
<!-- END Page Title -->

<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
		</li>
		<span class="divider">
			<i class="fa fa-angle-right"></i>
			<i class="fa fa-road"></i>
			<a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
		</span> 
		<span class="divider">
			<i class="fa fa-angle-right"></i>
			<i class="fa fa-road"></i>
		</span>
		<li class="active">{{ $page_title or ''}}</li>
	</ul>
</div>
<!-- END Breadcrumb -->

<!-- BEGIN Main Content -->
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-title">
				<h3>
					<i class="fa fa-road"></i>
					{{ isset($page_title)?$page_title:"" }}
				</h3>
				<div class="box-tool">
					<a data-action="collapse" href="#"></a>
					<a data-action="close" href="#"></a>
				</div>
			</div>

			<div class="box-content">

				@include('admin.layout._operation_status')

				<form name="frm_manage" id="frm_manage" method="POST" class="form-horizontal" action="{{url($module_url_path)}}/multi-action">
					{{ csrf_field() }}

					<div class="btn-toolbar pull-right clearfix">

						<div class="btn-group">
							<a href="{{ $module_url_path.'/create'}}" class="btn btn-primary btn-add-new-records"  title="Add CMS">Add Category</a>                      
						</div>

						<div class="btn-group">
							<a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
								title="Multiple Active/Unblock" 
								href="javascript:void(0);" 
								onclick="javascript : return check_multi_action('frm_manage','activate');" 
								style="text-decoration:none;">
								<i class="fa fa-unlock"></i>
							</a> 
						</div>
						<div class="btn-group">
							<a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
								title="Multiple Deactive/Block" 
								href="javascript:void(0);" 
								onclick="javascript : return check_multi_action('frm_manage','deactivate');"  
								style="text-decoration:none;">
								<i class="fa fa-lock"></i>
							</a> 
						</div>
						<div class="btn-group">
							<a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
								title="Refresh" 
								href="{{ $module_url_path }}"
								style="text-decoration:none;">
								<i class="fa fa-repeat"></i>
							</a> 
						</div>
					</div>
					<br/><br/><br/>
					<div class="clearfix"></div>
					<div class="table-responsive">

						<input type="hidden" name="multi_action" value="" />

						<table class="table table-advance" id="table1" >
							<thead>
								<tr class="border-solid" style="max-width: 10px">
									<th><input type="checkbox" class="filled-in" name="selectall" id="select_all" onchange="chk_all(this)" /><label for="selectall"></label>
									</th>
									<th>Category Name</th>
									<th>Status</th>
									<th>Added On</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@if(isset($arr_blog_cats['data']) && !empty($arr_blog_cats['data']))
								@foreach($arr_blog_cats['data'] as $data)
								<tr>
									<td><input type="checkbox" name="checked_record[]" value="{{base64_encode($data['id'])}}" /></td>
									<td>{{ $data['name'] or '' }}</td>
									<td>
										@if(isset($data['is_active']) && $data['is_active']==1)
				                        <a href="{{ $module_url_path.'/deactivate/'}}{{isset($data['id'])?base64_encode($data['id']):''}}" class="btn btn-success">Active</a>
				                        @else
				                        <a href="{{ $module_url_path.'/activate/'}}{{isset($data['id'])?base64_encode($data['id']):''}}" class="btn btn-danger">Inactive</a>
				                        @endif
									</td>
									<td>{{isset($data['created_at']) && $data['created_at']!=""? date('d F, Y',strtotime($data['created_at'])):"NA"}}
									</td>
									<td>
										<a class='btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip' href="{{ $module_url_path.'/edit/'.base64_encode($data['id']) }}" title='Edit' ><i class='fa fa-pencil-square-o' ></i></a>
									</td>
								</tr>
								@endforeach
								@else
								<h3>No data found</h3>
								@endif
							</tbody>
						</table>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">

function check_multi_action(frm_id,action){
	var frm_ref = jQuery("#"+frm_id);
	if(jQuery(frm_ref).length && action!=undefined && action!="")
	{
		var checked_record = jQuery('input[name="checked_record[]"]:checked');
		if (jQuery(checked_record).size() < 1)
		{
			alertify.alert('please select at least one record.');
			return false;
		}
		if(action == 'delete')
		{
			if (!confirm_delete()){
				return false;
			}
		}
		var input_multi_action = jQuery('input[name="multi_action"]');
		if(jQuery(input_multi_action).length)
		{
			jQuery('input[name="multi_action"]').val(action);
			jQuery(frm_ref)[0].submit();
		}
		else
		{
			console.warn("Required Hidden Input[name]: multi_action Missing in Form ")
		}
	}
	else
	{
		console.warn("Required Form[id]: "+frm_id+" Missing in Current category ")
	}
}
</script>
@endsection