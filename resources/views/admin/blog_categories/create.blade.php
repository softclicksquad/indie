@extends('admin.layout.master')    
@section('main_content')
<!-- BEGIN Main Content -->

<!-- BEGIN Page Title -->
<div class="page-title">
	<div>

	</div>
</div>
<!-- END Page Title -->

<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
		</li>
		<span class="divider">
			<i class="fa fa-angle-right"></i>
			<i class="fa fa-road"></i>
			<a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
		</span> 
		<span class="divider">
			<i class="fa fa-angle-right"></i>
			<i class="fa fa-road"></i>
		</span>
		<li class="active">{{ $page_title or ''}}</li>
	</ul>
</div>
<!-- END Breadcrumb -->

<!-- BEGIN Main Content -->
<div class="row">
	<div class="col-md-12">

		<div class="box">
			<div class="box-title">
				<h3>
					<i class="fa fa-road"></i>
					{{ isset($page_title)?$page_title:"" }}
				</h3>
				<div class="box-tool">
					<a data-action="collapse" href="#"></a>
					<a data-action="close" href="#"></a>
				</div>
			</div>

			<div class="box-content">

				@include('admin.layout._operation_status')

				<form class="form-horizontal" id="frm_add_front_page" name="frm_add_front_page" action="{{$module_url_path}}/store" method="post" enctype="multipart/form-data">
					{{csrf_field()}}
					<fieldset class="content-group">
						<div class="row">
							<div class="col-lg-8">
								<div class="form-group">
									<label class="control-label col-sm-4 col-md-4 col-lg-3" for="name">Category Name<i class="red">*</i></label>
									<div class="col-sm-8 col-md-8 col-lg-9">
										<input type="text" name="name" id="name" class="form-control" placeholder="Category Name" data-rule-required="true" data-rule-maxlength="100" tabindex="1" >
										<span class="error">{{ $errors->first('name') }} </span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group text-right">
							<div class="col-lg-8">
								<a href="{{ $module_url_path or '' }}" class="btn gray-btn">Cancel</a href="#">
									<button type="submit" class="btn green-btn" id="btn_add_front_page" tabindex="6">Save</button>
							</div>
						</div>
					</fieldset>

				</form>
			</div>
		</div>

	</div>
</div>

<script>
$(document).ready(function() {
    var e = $("input[name=_token]").val();
    $("#frm_add_front_page").validate({
        highlight: function(e) {},
        ignore: [],
        rules: {
            name: {
                remote: {
                    headers: {
                        "X-CSRF-Token": e
                    },
                    url: "{{ $module_url_path. '/check-category' }}",
                    type: "post",
                    dataType: "json",
                    data: {
                        name: function() {
                            return $("#name").val()
                        }
                    }
                }
            }
        },
        messages: {
            name: {
                remote: "Category already exist in system."
            }
        }
    })/*, $("#frm_add_front_page").submit(function() {
        if ($("#frm_add_front_page").valid()) return showProcessingOverlay(), !0
    })*/
});
</script>

  @endsection


  