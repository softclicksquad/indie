@extends('admin.layout.master')          

@section('main_content')
  
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/data-tables/latest/') }}/dataTables.bootstrap.min.css">

    <!-- BEGIN Page Title -->
    <div class="page-title">
        <div>

        </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
     <div id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{ url('/admin/dashboard') }}">Dashboard</a>
            </li>

            <span class="divider">
               <i class="fa fa-angle-right"></i>   
            </span>
            <li>
            
            <i class="fa fa-times-circle-o"></i>
               <a href="{{ $module_url_path }}">{{$module_title}}</a>
            </li>   
            
            <span class="divider">
                <i class="fa fa-angle-right"></i><i class="fa fa-envelope-o"></i>
            </span>

            <li class="active">{{ isset($page_title)?$page_title:"" }}</li>
        </ul>
      </div>
    <!-- END Breadcrumb -->
    
    <!-- BEGIN Main Content -->

    <div class="row">
        <div class="col-md-12" >
            <div class="box box-red">
                <div class="box-title">
                    <h3><i class="fa fa-envelope-o"></i>Messages</h3>
                </div>
                
                <div class="box-content" style="height: 500px;overflow-y: scroll;">
                    <ul class="messages messages-chatty ">
                       @if(isset($arr_conversation) && count($arr_conversation) > 0) 
                          @foreach( $arr_conversation as $key => $message)
                              <?php
                                $first_name = isset($message['role_info'][0]['first_name'])?$message['role_info'][0]['first_name']:'';
                                $last_name  = isset($message['role_info'][0]['last_name'])?$message['role_info'][0]['last_name']:'';
                                $name       = $first_name.' '.$last_name;
                                $profile_image = isset($message['role_info'][0]['profile_image'])?$message['role_info'][0]['profile_image']:'default_profile_image.png';
                              ?>
                            @if($message['user_id'] == $client_user_id)    
                              
                            <li >
                                <img src="{{url('/public')}}/uploads/front/profile/{{$profile_image}}" alt="">
                                <div style="background: #bedaf9;">
                                    <div style="margin-bottom:10px;">
                                        <h5 style="color: #666363">{{ $name or 'Unknown user' }}</h5>
                                        <span class="time"><i class="fa fa-clock-o"></i>&nbsp;{{ date('d M Y H:i', strtotime($message['created_at'])) }}</span>
                                    </div>
                                    <p style="font-family: Tahoma;color: #070303;font-size: 15px;">{{ $message['body'] or ''}}</p>
                                </div>
                            </li>

                            @else
                            <li class="right" >
                                <img src="{{url('/public')}}/uploads/front/profile/{{$profile_image}}" alt="">
                                <div style="background: #e2eaf3;">
                                    <div style="margin-bottom:10px;">
                                        <h5 style="color: #2f2b2b">{{ $name or 'Unknown user' }}</h5>
                                        <span class="time"><i class="fa fa-clock-o"></i>&nbsp;{{ date('d M Y H:i', strtotime($message['created_at'])) }}</span>
                                    </div>
                                    <p style="font-family: Tahoma;color: #040101;font-size: 15px;">{{ $message['body'] or ''}}</p>
                                </div>
                            </li>
                            @endif
                          @endforeach  
                        @else
                        <div class="alert alert-danger alert-dismissible">
                            There are no messages for this conversation.
                        </div>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
<!-- END Main Content -->

@stop                    