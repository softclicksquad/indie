@extends('admin.layout.master')          

    @section('main_content')
  
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/data-tables/latest/') }}/dataTables.bootstrap.min.css">
    <!-- BEGIN Page Title -->
    <div class="page-title">
        <div>

        </div>
    </div>
    <!-- END Page Title -->
    
    <!-- BEGIN Breadcrumb -->
     <div id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{ url('/admin/dashboard') }}">Dashboard</a>
            </li>

            <span class="divider">
               <i class="fa fa-angle-right"></i>   
            </span>
            <li>
            
            <i class="fa fa-times-circle-o"></i>
               <a href="{{ $module_url_path }}">View&nbsp;{{$module_title or ''}}</a>
            </li>   
            
            <span class="divider">
                <i class="fa fa-angle-right"></i><i class="fa fa-times-circle-o"></i>
            </span>

            <li class="active">{{ isset($page_title)?$page_title:"" }}</li>
        </ul>
      </div>
    <!-- END Breadcrumb -->

    <!-- BEGIN Main Content -->
    <div class="row">
      <div class="col-md-12">

          <div class="box">
            <div class="box-title">
              <h3>
                <i class="fa fa-times-circle-o"></i>
                  {{ $page_title or '' }}
              </h3>
            <div class="box-tool">
                <a data-action="collapse" href="#"></a>
                <a data-action="close" href="#"></a>
            </div>
        </div>
        <div class="box-content">
        
          <form class="form-horizontal" id="frm_manage" method="POST" action="{{ url('/web_admin/restaurantReviews/multi_action') }}">

            {{ csrf_field() }}

            <div class="col-md-10">
            

            <div id="ajax_op_status">
                
            </div>
            <div class="alert alert-danger" id="no_select" style="display:none;"></div>
            <div class="alert alert-warning" id="warning_msg" style="display:none;"></div>
          </div>
          <div class="btn-toolbar pull-right clearfix">
            <!--- Add new record - - - -->
               <div class="btn-group">
                                 
                </div>
            <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - -->
              

            <div class="btn-group"> 
              
                  <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                     title="Refresh" 
                     onclick="window.location.reload()" 
                     style="text-decoration:none;">
                     <i class="fa fa-repeat"></i>
                  </a> 
              
            </div>
          </div>
          <br/><br/>
          <div class="clearfix"></div>
          
          @include('admin.layout._operation_status')
          
          <div class="table-responsive" style="border:0">

            <input type="hidden" name="multi_action" value="" />

            <table class="table table-advance"  id="user_manage" >
              <thead>
                <tr>
                  <th style="width: 60px;">Sr. No.</th>
                  <th>Project Name</th>  
                  <th>Title</th>  
                  <th>Description</th>
                  <th>Added By</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>

              @if(isset($arr_data ) && sizeof($arr_data )>0)
              @foreach($arr_data as $key => $dispute_data)  
                  
                  <tr>
                    
                    <td>
                      {{ $key+1 }}
                    </td>
                    
                    <td>
                      {{ isset($dispute_data['project_details']['project_name']) ? str_limit($dispute_data['project_details']['project_name'],30):"" }}
                    </td>      

                    <td>
                      {{ isset($dispute_data['title']) ? str_limit($dispute_data['title'],30):"" }}
                    </td>    

                    <td>
                      {{ isset($dispute_data['description']) ? str_limit($dispute_data['description'],60):"" }}
                    </td>
                    
                    <td>
                        @if($dispute_data['added_by'] == '1')
                          <span class="badge badge-xlarge badge-warning">Client</span>
                        @elseif($dispute_data['added_by'] == '2') 
                          <span class="badge badge-xlarge badge-lime">Expert</span>
                        @endif  
                    </td>
                    <td>
                      <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" href="{{$module_url_path}}/details/{{base64_encode($dispute_data['project_id'])}}"  data-original-title="View Dispute">
                        <i class="fa fa-eye" ></i>
                      </a>
                      &nbsp;
                      <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" href="{{$module_url_path}}/messages/{{base64_encode($dispute_data['project_id'])}}"  data-original-title="View Expert Messages">
                        <i class="fa fa-envelope" ></i>
                      </a>
                      &nbsp;
                      @if(isset($dispute_data['project_details']['project_manager_user_id']) && $dispute_data['project_details']['project_manager_user_id'] != "" )
                        <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" href="{{$module_url_path}}/manager_messages/{{base64_encode($dispute_data['project_id'])}}"  data-original-title="View Manager Messages">
                          <i class="fa fa-envelope" ></i>
                       </a>
                      @else
                       <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" disabled="" href="javascript:void(0);"  data-original-title="View Manager Messages">
                          <i class="fa fa-envelope" ></i>
                       </a>
                      @endif
                    </td>
                  </tr>
                @endforeach
                @endif
              </tbody>
            </table>
          </div>
        </form>
      </div>
  </div>
</div>
</div>
<!-- END Main Content -->
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">

   $(document).ready(function()
    {
        $("#user_manage").DataTable();
    });
  
</script>

@stop                    


