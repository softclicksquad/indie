@extends('admin.layout.master')          

@section('main_content')
  
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/data-tables/latest/') }}/dataTables.bootstrap.min.css">

    <!-- BEGIN Page Title -->
    <div class="page-title">
        <div>

        </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
     <div id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{ url('/admin/dashboard') }}">Dashboard</a>
            </li>

            <span class="divider">
               <i class="fa fa-angle-right"></i>   
            </span>
            <li>
            
            <i class="fa fa-times-circle-o"></i>
               <a href="{{ $module_url_path }}">{{$module_title}}</a>
            </li>   
            
            <span class="divider">
                <i class="fa fa-angle-right"></i><i class="fa fa-times-circle-o"></i>
            </span>

            <li class="active">{{ isset($page_title)?$page_title:"" }}</li>
        </ul>
      </div>
    <!-- END Breadcrumb -->


    <!-- BEGIN Main Content -->
    <div class="row">

      
      <div class="col-md-12">


          <div class="box">
            <div class="box-title">
              <h3>
                <i class="fa fa-times-circle-o"></i>
                  {{ $arr_data['project_details']['project_name'] or '' }} 
                  <span class="divider">
                    <i class="fa fa-angle-right"></i>
                  </span> 
                  {{ $page_title }}
            </h3>
            <div class="box-tool">
                <a data-action="collapse" href="#"></a>
                <a data-action="close" href="#"></a>
            </div>
        </div>
        <div class="box-content">
  
        @include('admin.layout._operation_status')
        
        <form class="form-horizontal" id="validation-form" method="POST" action="{{ $module_url_path }}/comments" >
        {{ csrf_field() }}
           <div class="form-group">
                <label class="col-sm-3 col-lg-2 control-label" for="email_id">Project Name</label>
                <div class="col-sm-6 col-lg-6 controls">
                    <input class="form-control" style="background:#f3f4f5;"   value="{{ $arr_data['project_details']['project_name'] or '' }}" readonly="" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-lg-2 control-label" for="email_id">Dispute Title</label>
                <div class="col-sm-6 col-lg-6 controls">
                    <input class="form-control" style="background:#f3f4f5;"   value="{{ $arr_data['title'] or '' }}" readonly="" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-lg-2 control-label" for="rating">Dispute Description</label>
                <div class="col-sm-6 col-lg-6 controls">
                     <textarea class="form-control" style="background:#f3f4f5;height: 150px;"  readonly="">{{ $arr_data['description'] or '' }}</textarea>
                </div>
            </div>


            <?php
              $added_by = "";
              $against = "";
              $added_by_name = "";
              $against_name = "";
              
              if(isset($arr_data['added_by']) && $arr_data['added_by'] == '1')
              {
                $added_by = "Client";
                $against  = "Expert";

                $added_by_first_name = isset($arr_data['clients_details']['first_name']) ? $arr_data['clients_details']['first_name']:"";
                $added_by_last_name = isset($arr_data['clients_details']['last_name']) ? $arr_data['clients_details']['last_name']:"";
                $added_by_name =  $added_by_first_name.' '.$added_by_last_name;

                $against_first_name = isset($arr_data['experts_details']['first_name']) ? $arr_data['experts_details']['first_name']:"";
                $against_last_name = isset($arr_data['experts_details']['last_name']) ? $arr_data['experts_details']['last_name']:"";
                $against_name =  $against_first_name.' '.$against_last_name;

              }
              else if(isset($arr_data['added_by']) && $arr_data['added_by'] == '2')
              {
                $added_by = "Expert";
                $against  = "Client";

                $against_first_name = isset($arr_data['clients_details']['first_name']) ? $arr_data['clients_details']['first_name']:"";
                $against_last_name = isset($arr_data['clients_details']['last_name']) ? $arr_data['clients_details']['last_name']:"";
                $against_name =  $against_first_name.' '.$against_last_name;

                $added_by_first_name = isset($arr_data['experts_details']['first_name']) ? $arr_data['experts_details']['first_name']:"";
                $added_by_last_name = isset($arr_data['experts_details']['last_name']) ? $arr_data['experts_details']['last_name']:"";
                $added_by_name =  $added_by_first_name.' '.$added_by_last_name;
                
              }
            ?>

            <div class="form-group">
                <label class="col-sm-3 col-lg-2 control-label" for="message">Added By&nbsp;{{ $added_by }}</label>
                <div class="col-sm-6 col-lg-6 controls">
                    <input class="form-control" style="background:#f3f4f5;"   value="{{ $added_by_name  }}" readonly="" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-lg-2 control-label" for="message">Against&nbsp;{{ $against }}</label>
                <div class="col-sm-6 col-lg-6 controls">
                    <input class="form-control" style="background:#f3f4f5;"  value="{{$against_name}}" readonly="" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-lg-2 control-label" for="message">Date</label>
                <div class="col-sm-6 col-lg-6 controls">
                    <input class="form-control" style="background:#f3f4f5;"  value="{{ isset($arr_data['created_at']) ? date('d M Y',strtotime($arr_data['created_at'])):'' }}" readonly="" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-lg-2 control-label" for="comments">Admin Comments</label>
                <div class="col-sm-6 col-lg-6 controls">
                     <textarea class="form-control" name="comments" placeholder="Comments" style="height: 150px;">{{ $arr_data['admin_comments'] or ''  }}</textarea>
                     <span class='error'>{{ $errors->first('comments') }}</span>
                </div>
            </div>

            <input type="hidden" name="dispute_id" value="{{ isset($arr_data['id']) ? base64_encode($arr_data['id']):'' }}" readonly="" > </input>

            <div class="form-group">
              <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
              <input type="submit" class="btn btn-primary" value="Save" ></input>&nbsp;
              <a href="{{ $module_url_path }}"> 
                <input type="button"  class="btn btn-primary" value="Back">
              </a>  
            </div>
            </div>
    
            </form>

    

</div>
</div>
</div>
</div>
<!-- END Main Content -->

@stop                    