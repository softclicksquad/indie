@extends('admin.layout.master')                

    @section('main_content')

     <!-- BEGIN Page Title -->
    <link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/data-tables/latest/dataTables.bootstrap.min.css">
    <?php
    if(isset($start_date))
    {
       $selected_start_date = date("m/d/Y", strtotime($start_date));
    } 
    if(isset($end_date))
    {
       $selected_end_date = date("m/d/Y", strtotime($end_date));
    } 
  
    ?>
    <div class="page-title">
        <div>

        </div>
    </div>
    <!-- END Page Title -->
    
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
       <ul class="breadcrumb">
          <li>
             <i class="fa fa-home"></i>
             <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
          </li>
          <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-money"></i>
          <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
          </span> 
          <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-money"></i>
          </span>
          <li class="active">{{ $page_title or ''}}</li>
       </ul>
    </div>

    <form action="{{$module_url_path}}/filter" method="POST" id="validation-form" name="frm-user-report">
    {{csrf_field()}}
    <div class="form-group">
          <label class="col-sm-3 col-lg-1 control-label">Start date</label>
          <div class="col-sm-9 col-lg-2 controls">
              <input type="text" id="start_date" value="<?php if(isset($selected_start_date)) { echo $selected_start_date; }?>" data-rule-required="true" placeholder="MM/DD/YYYY" class="date-picker form-control" name="start_date" >
              <span class="help-block">{{ $errors->first('start_date') }}</span>
          </div>
      </div>

    <div class="clearfix"></div>
     <div class="form-group">
          <label class="col-sm-3 col-lg-1 control-label">End date</label>
          <div class="col-sm-9 col-lg-2 controls">
              <input type="text" id="end_date" value="<?php if(isset($selected_end_date)) { echo $selected_end_date; }?>" data-rule-required="true" placeholder="MM/DD/YYYY" class="date-picker form-control" name="end_date" >
              <span class="help-block">{{ $errors->first('end_date') }}</span>
          </div>
      </div>

      <div class="clearfix"></div>
      <div class="col-sm-9 col-lg-3 controls">
        <input type="submit" class="btn btn btn-primary" value="Apply">
      </div>
     <div class="clearfix"></div>
     </form>
     </br></br>
    <!-- END Breadcrumb -->

    <!-- BEGIN Main Content -->
    <div class="row">
      <div class="col-md-12">

          <div class="box">
            <div class="box-title">
              <h3>
                <i class="fa fa-money"></i>
                {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
                <a data-action="collapse" href="#"></a>
                <a data-action="close" href="#"></a>
            </div>
        </div>
        <div class="box-content">
          
          @include('admin.layout._operation_status')
          
          <form name="frm_manage" id="frm_export_csv" method="POST" class="form-horizontal" action="{{$module_url_path}}/export">
               {{ csrf_field() }}

            
          <div class="btn-toolbar pull-right clearfix">

          
           <!-- <div class="btn-group">
             <a href="{{ $module_url_path.'/create'}}" class="btn btn-primary btn-add-new-records"  title="Add CMS">Add Project Manager</a>                      
          </div> -->
    
          <div class="btn-group"> 

          @if(isset($arr_all_transactions) && sizeof($arr_all_transactions)>0)
            <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                   title="Export CSV" 
                   style="text-decoration:none;" onclick="javascript:return export_csv('frm_export_csv');">
                   <i class="fa fa-file"></i>
            </a>
          @endif

             <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                title="Refresh" 
                href="{{ $module_url_path }}"
                style="text-decoration:none;">
             <i class="fa fa-repeat"></i>
             </a> 

              <input type="hidden" name="start_date" value="<?php if(isset($selected_start_date)) { echo $selected_start_date; }?>">
              <input type="hidden" name="end_date" value="<?php if(isset($selected_end_date)) { echo $selected_end_date; }?>">
          </div>
          
          </div>
          <br/><br/>
          <div class="clearfix"></div>
          <div class="table-responsive" style="border:0">

            <input type="hidden" name="multi_action" value="" />

            <table class="table table-advance"  id="table1" >
              <thead>
                <tr>
                  <th>Name</th> 
                  <th>Email</th> 
                  <th>Transaction Type</th> 
                  <th>Payment Amount</th>
                  <th>Payment Method</th>
                  <th>Payment Status</th>
                  <th>Payment Date</th>

                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                
                @if(isset($arr_all_transactions) && sizeof($arr_all_transactions)>0)
                  @foreach($arr_all_transactions as $transaction)

                  <tr>
                  <td>
                  @if(isset($transaction['role_info']['name']))
                    {{$transaction['role_info']['name']}}
                  @else
                  {{isset($transaction['role_info']['first_name'])? ucfirst($transaction['role_info']['first_name']):''}} 
                  {{isset($transaction['role_info']['last_name'])? ucfirst($transaction['role_info']['last_name']):''}}
                  @endif
                  </td>
                   <td>{{isset($transaction['user_details']['email'])?$transaction['user_details']['email']:''}}</td>
                    <td>  @if(isset($transaction['transaction_type']) && $transaction['transaction_type']=='1') 
                    {{'Subscription'}} 
                    @elseif( isset($transaction['transaction_type']) && $transaction['transaction_type']=='2')
                    {{'Milestone'}} 
                     @elseif( isset($transaction['transaction_type']) && $transaction['transaction_type']=='3')
                    {{'Release Milestones'}} 
                    @elseif( isset($transaction['transaction_type']) && $transaction['transaction_type']=='4')
                    {{'Topup Bid'}} 
                    @elseif( isset($transaction['transaction_type']) && $transaction['transaction_type']=='5')
                    {{'Project Payment'}} 
                    @endif
                    </td>
                    <td>
                       {{isset($transaction['currency'])?$transaction['currency']:''}}&nbsp;{{isset($transaction['paymen_amount'])?$transaction['paymen_amount']:'0' }}
                    </td> 

                    <td>   
                      @if(isset($transaction['payment_method']) && $transaction['payment_method']=='1')
                     {{'Paypal'}}
                      @elseif(isset($transaction['payment_method']) && $transaction['payment_method']=='2')
                      {{'Stripe'}} 
                      @else{{'Free'}} 
                      @endif 
                    </td>
                    

                    <td>   
                      @if(isset($transaction['payment_status']) && $transaction['payment_status']=='0')
                        Fail
                      @elseif(isset($transaction['payment_status']) && $transaction['payment_status']=='1')
                        Paid
                      @elseif(isset($transaction['payment_status']) && $transaction['payment_status']=='2')
                        Paid
                      @elseif(isset($transaction['payment_status']) && $transaction['payment_status']=='3')
                        Loss
                      @else
                        Fail  
                      @endif
                      <td>
                      @if(isset($transaction['payment_date']) && $transaction['payment_date']!='0000-00-00 00:00:00')
                      {{isset($transaction['payment_date'])?date("d-M-Y", strtotime($transaction['payment_date'])):''}}
                      @else
                      -
                      @endif
                      </td>
                    </td>
                    <td>

                     <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" href="{{ $module_url_path.'/show/'.base64_encode($transaction['id']) }}"  title="View">
                      <i class="fa fa-eye" ></i>
                    </a>  
                    </td> 
                  </tr>

                  @endforeach
                @endif
                 
              </tbody>
            </table>
          </div>
        <div>   </div>
         
          </form>
      </div>
  </div>
</div>
</div>
<!-- END Main Content -->
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/dataTables.bootstrap.min.js"></script>
 <link rel="stylesheet" type="text/css"  href="{{url('/public')}}/front/css/jquery-ui.css"/>
 <script src="{{url('/public')}}/front/js/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

   $("#start_date" ).datepicker({
      dateFormat: 'mm/dd/yy',
    
   }).on('change',function(e)
   {
    var date2 = $('#start_date').datepicker('getDate');
    date2.setDate(date2.getDate() + 1);
    //sets minDate to dt1 date + 1
    $('#end_date').datepicker('option', 'minDate', date2);

     var start_date = $("#start_date").val();
     var end_date = $("#end_date").val();
   });
   
  $( "#end_date" ).datepicker({
      dateFormat: 'mm/dd/yy',
      'startDate': new Date(),
      minDate: 'today',
   }).on('change',function(e)
   { 
       var start_date = $("#start_date").val();
       var end_date = $("#end_date").val();
   });
});

    function export_csv(frm_id)
    {
      var frm_ref = jQuery("#"+frm_id);
      //console.log(frm_ref);return false;
      if(frm_ref)
      {

        /* Get hidden input reference */
        var start_date = jQuery('input[name="start_date"]');
        var end_date = jQuery('input[name="end_date"]');
    
        if(jQuery(start_date).length && jQuery(end_date).length)
        {
          /* Set Action in hidden input*/
          jQuery('input[name="start_date"]').val();
          jQuery('input[name="end_date"]').val();
          /*Submit the referenced form */
          jQuery(frm_ref).submit();
        }
        else
        {
          console.warn("Required Hidden Input[name]: start date Missing in Form ")
        }
      }
      else
      {
          console.warn("Required Form[id]: "+frm_id+" Missing in Current Page ")
      }
    }
   
</script>

@stop                    


