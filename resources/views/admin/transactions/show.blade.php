@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-desktop"></i>
      <a href="{{ $module_url_path }}">Transactions</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-list"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-list"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
               @include('admin.layout._operation_status')
           <div class="alert alert-success" id="ajax_success" style="display:none;">
            <button data-dismiss="alert" class="close">×</button>
            <strong>Success!</strong>
            <div id="ajax_sub_success"></div>
         </div>
         <div class="alert alert-danger" id="ajax_error" style="display:none;">
            <button data-dismiss="alert" class="close">×</button>
            <strong>Error!</strong> 
            <div id="ajax_sub_error"></div>
         </div>
           
              <div class="row">

               @if(isset($arr_info) && sizeof($arr_info)>0)
              <form name="validation-form" id="validation-form" method="POST" action="" class="form-horizontal">
               {{ csrf_field() }}
                <div class="col-md-6">
                  <div class="form-group">
                     <label class="col-sm-3 control-label">User Name:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">
                        @if(isset($arr_info['role_info']['name']))
                          {{$arr_info['role_info']['name']}}
                        @else
                          {{isset($arr_info['role_info']['first_name'])?$arr_info['role_info']['first_name']:''}} 
                          {{isset($arr_info['role_info']['last_name'])?$arr_info['role_info']['last_name']:''}}
                        @endif   
                        </div>
                     </div>
                  </div>
                  
                  
                  <div class="form-group">
                     <label class="col-sm-3 control-label">User Email:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block"><a href="mailto:{{isset($arr_info['user_details']['email'])?$arr_info['user_details']['email']:''}}">{{isset($arr_info['user_details']['email'])?$arr_info['user_details']['email']:''}}</a></div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>

                   
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Transaction Type:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">
                          @if(isset($arr_info['transaction_type']) && $arr_info['transaction_type']=='1')
                          {{'Subscription'}}
                          @elseif(isset($arr_info['transaction_type']) && $arr_info['transaction_type']=='2')
                          {{'Milestone'}}
                          @elseif(isset($arr_info['transaction_type']) && $arr_info['transaction_type']=='3')
                          {{'Release Milestone'}}
                          @elseif(isset($arr_info['transaction_type']) && $arr_info['transaction_type']=='4')
                          {{'Topup Bid'}}
                          @elseif( isset($arr_info['transaction_type']) && $arr_info['transaction_type']=='5')
                          {{'Project Payment'}} 
                          @endif
                        </div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>

                   <div class="form-group">
                     <label class="col-sm-3 control-label">Payment Amount:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['currency'])?$arr_info['currency']:''}}&nbsp;&nbsp;{{isset($arr_info['paymen_amount'])?$arr_info['paymen_amount']:''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                   @if(isset($arr_info['payment_method']) && $arr_info['payment_method']=='1')
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Payment Method:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{'Paypal'}}</div>
                         <div class="client-name-block"></div>
                     </div>
                  </div>
                  @elseif(isset($arr_info['payment_method']) && $arr_info['payment_method']=='2')
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Payment Method:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{'Stripe'}}</div>
                         <div class="client-name-block"></div>
                     </div>
                  </div>
                  @else
                   <div class="form-group">
                     <label class="col-sm-3 control-label">Payment Method:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{'Free'}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  @endif
                   @if(isset($arr_info['payment_status']) && $arr_info['payment_status']=='0')
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Payment Status:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{'Fail'}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  @elseif(isset($arr_info['payment_status']) && $arr_info['payment_status']=='1')
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Payment Status:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{'Paid'}}</div>
                         <div class="client-name-block"></div>
                     </div>
                  </div>
                  @elseif(isset($arr_info['payment_status']) && $arr_info['payment_status']=='2')
                   <div class="form-group">
                     <label class="col-sm-3 control-label">Payment Status:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{'Paid'}}</div>
                         <div class="client-name-block"></div>
                     </div>
                  </div>
                  @else
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Payment Status:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{'Fail'}}</div>
                         <div class="client-name-block"></div>
                     </div>
                  </div>
                  @endif
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Payment Date:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['payment_date'])&&$arr_info['payment_date']!='0000-00-00 00:00:00'?date("d-M-Y", strtotime($arr_info['payment_date'])):'-'}}</div>
                         <div class="client-name-block"></div>
                     </div>
                  </div>
                </div>
                </form>
             @endif
            </div>
            </div>
       </div>
   </div>
</div>

@stop