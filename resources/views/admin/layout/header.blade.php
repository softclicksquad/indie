<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>{{ isset($page_title)?$page_title:"" }} - {{ config('app.project.name') }}</title>
        <link rel="icon" href="{{url('/public')}}/favicon.ico" type="image/x-icon" />
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <!--base css styles-->
        <link rel="stylesheet" href="{{url('/public')}}/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{url('/public')}}/assets/font-awesome/css/font-awesome.min.css">
        <!--page specific css styles-->
        <link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/bootstrap-fileupload/bootstrap-fileupload.css" />
        <!--flaty css styles-->
        <link rel="stylesheet" href="{{url('/public')}}/admin/css/flaty.css">
        <link rel="stylesheet" href="{{url('/public')}}/admin/css/flaty-responsive.css">
        <link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/bootstrap-switch/static/stylesheets/bootstrap-switch.css" />
        <link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
        <!-- Auto load email address -->
        <link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/chosen-bootstrap/chosen.min.css" />
        <!--basic scripts-->
        <script>window.jQuery || document.write('<script src="{{url('/public')}}/assets/jquery/jquery-2.1.4.min.js"><\/script>')</script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/jquery-tags-input/jquery.tagsinput.css" />
        <link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/bootstrap-duallistbox/duallistbox/bootstrap-duallistbox.css" />
        <link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/dropzone/downloads/css/dropzone.css" />
        <link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/bootstrap-colorpicker/css/colorpicker.css" />
        <link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/chosen-bootstrap/chosen.min.css" />
        <link href="{{url('/public')}}/admin/css/components.css" rel="stylesheet" type="text/css">

        <script src="{{url('/public')}}/assets/T-validations/masterT-validations.js"></script>

        <!-- alertify Here -->
          <script src="{{url('/public')}}/assets/alertifyjs/alertify.min.js"></script>
          <script src="{{url('/public')}}/assets/alertifyjs/alertify.js"></script>
          <link rel="stylesheet" href="{{url('/public')}}/assets/alertifyjs/css/alertify.css" />
          {{-- <link rel="stylesheet" href="{{url('/public')}}/assets/alertifyjs/css/alertify.min.css" id="toggleCSS" /> --}}
          <!-- end alertify -->
    </head>
    <body>
        <!-- BEGIN Navbar -->
        <div id="navbar" class="navbar">
            <button type="button" class="navbar-toggle navbar-btn collapsed" data-toggle="collapse" data-target="#sidebar">
                <span class="fa fa-bars"></span>
            </button>
            <a class="navbar-brand" href="#">
                <small>
                    <i class="fa fa-desktop"></i>
                    {{ config('app.project.name') }} Admin
                </small>
            </a>
            <!-- BEGIN Navbar Buttons -->
            <ul class="nav flaty-nav pull-right">
                <!-- BEGIN Button Notifications -->
                <li class="hidden-xs">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="fa fa-bell"></i>
                        <span class="badge badge-important">{{ isset($arr_notification['notifications'])?count($arr_notification['notifications']):'0' }}</span>
                    </a>
                    <!-- BEGIN Notifications Dropdown -->
                    <ul class="dropdown-navbar dropdown-menu noti-drop-main-section" style="height: 300px;width:350px;overflow-y: scroll;"  >
                        <li class="nav-header"  >
                            <i class="fa fa-warning"></i>
                             @if(isset($arr_notification['notifications']) && count($arr_notification['notifications']) == 1)
                                {{ isset($arr_notification['notifications'])?count($arr_notification['notifications']):'0' }}&nbsp;Notification
                             @elseif(isset($arr_notification['notifications']) && count($arr_notification['notifications']) > 1)
                                {{ isset($arr_notification['notifications'])?count($arr_notification['notifications']):'0' }}&nbsp;Notifications
                             @else
                                No New Notifications  
                             @endif
                        </li>
                        @if(isset($arr_notification['notifications'])  &&  count($arr_notification['notifications'])  > 0 )
                            @foreach($arr_notification['notifications'] as $key => $notification)
                            <li class="notify">

                                @if(isset($notification['notification_text_en']) && $notification['notification_text_en']!=null)
                                @if($notification['url'] != "")
                                    <a onclick="return notification_seen('{{url('/')}}/{{ $notification['url'] }}','{{ $notification['id'] }}')" href="javascript:void(0);">
                                    <div>
                                        <b>{{$notification['notification_text_en'] or ''}}</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>{{humanTiming(strtotime($notification['created_at'])).' Ago'}}</span>
                                    </div>
                                    </a>
                                @else
                                    <a href="javascript:void(0)">
                                        <b>{{$notification['notification_text_en'] or ''}}</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>{{humanTiming(strtotime($notification['created_at'])).' Ago'}}</span>
                                    </a>
                                @endif
                                @endif
                            </li>
                            @endforeach
                        @endif

                    </ul>
                    <!-- END Notifications Dropdown -->
                </li>
                <!-- END Button Notifications -->
                <!-- BEGIN Button User -->
                <li class="user-profile">
                    <a data-toggle="dropdown" href="#" class="user-menu dropdown-toggle">
                        @if(isset($admin_profile_img) && $admin_profile_img!="")
                        <img class="nav-user-photo" src="{{$admin_profile_img}}" alt="" />
                        @endif
                        <span class="hhh" id="user_info">
                         @if(isset($admin_name) && $admin_name!="")
                            {{$admin_name}}
                         @else
                            Admin
                         @endif
                        </span>
                        <i class="fa fa-caret-down"></i>
                    </a>

                    <!-- BEGIN User Dropdown -->
                    <ul class="dropdown-menu dropdown-navbar" id="user_menu">
                        <li>
                            <a href="{{ url('/'.$admin_panel_slug) }}/profile" >
                                <i class="fa fa-user"></i>
                                Profile
                            </a>    
                        </li>    
                        <li>
                            <a href="{{ url('/'.$admin_panel_slug) }}/change_password" >
                                <i class="fa fa-key"></i>
                                Change Password
                            </a>    
                        </li>    
                        <li class="divider"></li>
                        <li>
                             <a href="{{ url('/'.$admin_panel_slug) }}/logout "> 
                                <i class="fa fa-power-off"></i>
                                Logout
                            </a>
                        </li>
                    </ul>
                    <!-- BEGIN User Dropdown -->
                </li>
                <!-- END Button User -->
            </ul>
            <!-- END Navbar Buttons -->
        </div>
        <!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
<script type="text/javascript">
/*Function for notification seen*/
function notification_seen(url,id) {
    <?php  
        $user = Sentinel::check();
        if($user){
            if($user->inRole('admin') || $user->inRole('subadmin')){
                $user_type = 'admin'; ?>
                var user_type = '<?php echo $user_type;?>';
                var site_url  = '{{url('/')}}';
                if(url && url!="" && id && id!=""){
                    jQuery.ajax({
                        url:site_url+'/'+user_type+'/projects/notification/'+btoa(id),
                        type:'GET',                        
                        dataType:'json',
                        success:function(response){  
                        // console.log(response.status);return false;
                            if(response.status=="success"){
                                window.location.href = url;
                            } else {
                                window.location.href = '{{url('/')}}'
                            }
                        }
                    });
                } <?php 
            }
        } 
       ?>
    }
</script>