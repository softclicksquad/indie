<div id="sidebar" class="navbar-collapse collapse">
   <!-- BEGIN Navlist -->
    @if($user = Sentinel::check())
      
        @php
            $user_role = get_user_role($user->id);
            $arr_user = $user->toArray();
            $arr_permissions = isset($arr_user['permissions']) ? $arr_user['permissions'] : '';

        @endphp

      <ul class="nav nav-list">
         <li class="<?php  if(Request::segment(2) == 'dashboard'){ echo 'active'; } ?>">
            <a href="{{ url($admin_panel_slug.'/dashboard')}}">
            <i class="fa fa-dashboard"></i>
            <span>Dashboard</span>
            </a>
         </li>

         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'categories'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/categories')}}">
                   <i class="fa fa-list-ul"></i>
                   <span>Categories</span>
               <b class="arrow fa fa-angle-right"></b>
               </a>
           </li> 
         @elseif($user_role == 'subadmin' && array_key_exists('categories',$arr_permissions))
            <li class="<?php  if(Request::segment(2) == 'categories'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/categories')}}">
                   <i class="fa fa-list-ul"></i>
                   <span>Categories</span>
               <b class="arrow fa fa-angle-right"></b>
               </a>
           </li> 
         @endif

         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'subcategories'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/subcategories')}}">
                   <i class="fa fa-list-ul"></i>
                   <span>Sub Categories</span>
               <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('subcategories',$arr_permissions))
            <li class="<?php  if(Request::segment(2) == 'subcategories'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/subcategories')}}">
                   <i class="fa fa-list-ul"></i>
                   <span>Sub Categories</span>
               <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @endif  

         @if($user_role == 'admin')

            <li class="<?php  if(Request::segment(2) == 'twilio-chat'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/twilio-chat/chat_list')}}">
                   <i class="fa fa-comment"></i>
                   <span>Manage Chat</span>
               <b class="arrow fa fa-angle-right"></b>
               </a>
           </li> 
        @elseif($user_role == 'subadmin' && array_key_exists('twilio-chat',$arr_permissions))
          <li class="<?php  if(Request::segment(2) == 'twilio-chat'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/twilio-chat/chat_list')}}">
                   <i class="fa fa-comment"></i>
                   <span>Manage Chat</span>
               <b class="arrow fa fa-angle-right"></b>
               </a>
          </li> 
        @endif

         @if($user_role == 'admin')

            <li class="<?php  if(Request::segment(2) == 'currency'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/currency')}}">
                   <i class="fa fa-dollar"></i>
                   <span>Manage Currency</span>
               <b class="arrow fa fa-angle-right"></b>
               </a>
           </li> 

         @endif

         @if($user_role == 'admin')

            <li class="<?php  if(Request::segment(2) == 'currency_rate'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/currency_rate')}}">
                   <i class="fa fa-dollar"></i>
                   <span>Manage Currency Rates</span>
               <b class="arrow fa fa-angle-right"></b>
               </a>
           </li> 

         @endif

         @if($user_role == 'admin')

            <li class="<?php  if(Request::segment(2) == 'deposite_currency'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/deposite_currency')}}">
                   <i class="fa fa-dollar"></i>
                   <span>Deposite Currency</span>
               <b class="arrow fa fa-angle-right"></b>
               </a>
           </li> 

         @endif

         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'sealed_entry_rates'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/sealed_entry_rates')}}">
                   <i class="fa fa-list-ul"></i>
                   <span>Sealed Entry Rates</span>
               <b class="arrow fa fa-angle-right"></b>
               </a>
           </li> 
         @elseif($user_role == 'subadmin' && array_key_exists('sealed_entry_rates',$arr_permissions))
            <li class="<?php  if(Request::segment(2) == 'sealed_entry_rates'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/sealed_entry_rates')}}">
                   <i class="fa fa-list-ul"></i>
                   <span>Sealed Entry Rates</span>
               <b class="arrow fa fa-angle-right"></b>
               </a>
           </li> 
         @endif



         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'professions'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/professions')}}">
                  <i class="fa fa-list-ul"></i>
                  <span>Expert Professions</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('expert_professions',$arr_permissions))
            <li class="<?php  if(Request::segment(2) == 'professions'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/professions')}}">
                  <i class="fa fa-list-ul"></i>
                  <span>Expert Professions</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @endif
         
         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'project_manager'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/project_manager')}}">
                  <i class="fa fa-user-plus"></i>
                  <span>Project Managers</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('project_manager',$arr_permissions))
            <li class="<?php  if(Request::segment(2) == 'project_manager'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/project_manager')}}">
                  <i class="fa fa-user-plus"></i>
                  <span>Project Managers</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @endif

         @if($user_role == 'admin')
            <li class="<?php  if(\Request::segment(3) == "posted"){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/projects/posted')}}">
                  <i class="fa fa-user-plus"></i>
                  <span>Assign Project M.</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('project_manager',$arr_permissions))
            <li class="<?php  if(\Request::segment(3) == "posted" ){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/projects/posted')}}">
                  <i class="fa fa-user-plus"></i>
                  <span>Assign Project M.</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @endif

         @if($user_role == 'admin')
            <li class="<?php  if(\Request::segment(2) == "recruiter"){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/recruiter')}}">
                  <i class="fa fa-user-plus"></i>
                  <span>Recruiters</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('recruiter',$arr_permissions))
            <li class="<?php  if(\Request::segment(2) == "recruiter" ){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/recruiter')}}">
                  <i class="fa fa-user-plus"></i>
                  <span>Recruiters</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @endif

         @if($user_role == 'admin')
            <li class="<?php  if(\Request::segment(3) == "posted_recruiter"){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/projects/posted_recruiter')}}">
                  <i class="fa fa-user-plus"></i>
                  <span>Assign Recruiters</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('recruiter',$arr_permissions))
            <li class="<?php  if(\Request::segment(3) == "posted_recruiter" ){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/projects/posted_recruiter')}}">
                  <i class="fa fa-user-plus"></i>
                  <span>Assign Recruiters</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @endif

         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'clients' && Request::segment(3) != 'deleted'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/clients')}}" class="dropdown-toggle">
                  <i class="fa fa-user-plus"></i>
                  <span>Clients</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('clients',$arr_permissions))
            <li class="<?php  if(Request::segment(2) == 'clients' && Request::segment(3) != 'deleted'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/clients')}}" class="dropdown-toggle">
                  <i class="fa fa-user-plus"></i>
                  <span>Clients</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @endif

         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'clients' && Request::segment(3)=='deleted'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/clients/deleted')}}" class="dropdown-toggle">
                  <i class="fa fa-user-plus"></i>
                  <span>Deleted Clients</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('clients',$arr_permissions))
            <li class="<?php  if(Request::segment(2) == 'clients' && Request::segment(3)=='deleted'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/clients/deleted')}}" class="dropdown-toggle">
                  <i class="fa fa-user-plus"></i>
                  <span>Deleted Clients</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @endif

         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'experts' && Request::segment(3)!='deleted'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/experts')}}" class="dropdown-toggle">
                  <i class="fa fa-user-plus"></i>
                  <span>Experts</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('experts',$arr_permissions))
            <li class="<?php  if(Request::segment(2) == 'experts' && Request::segment(3)!='deleted'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/experts')}}" class="dropdown-toggle">
                  <i class="fa fa-user-plus"></i>
                  <span>Experts</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @endif

         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'experts' && Request::segment(3)=='deleted'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/experts/deleted')}}" class="dropdown-toggle">
                  <i class="fa fa-user-plus"></i>
                  <span>Deleted Experts</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('experts',$arr_permissions))
            <li class="<?php  if(Request::segment(2) == 'experts' && Request::segment(3)=='deleted'){ echo 'active'; } ?>">
               <a href="{{ url($admin_panel_slug.'/experts/deleted')}}" class="dropdown-toggle">
                  <i class="fa fa-user-plus"></i>
                  <span>Deleted Experts</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @endif

         @if($user_role == 'admin')
            <li class="<?php  if((Request::segment(2) == 'projects' || Request::segment(2) == 'bids') && (Request::segment(3) =='open' || Request::segment(3)=='ongoing' || Request::segment(3)=='completed' || Request::segment(3)=='canceled' || Request::segment(3)=='all')){ echo 'active'; } ?>">
               <a href="javascript:void(0)" class="dropdown-toggle">
                  <i class="fa fa-gears"></i>
                  <span>Projects</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu"> 
                  {{-- <li style="display: block;" @if(\Request::segment(3) == "posted" ) class="active" @endif><a href="{{ url($admin_panel_slug.'/projects/posted')}}">Assign Project Manager</a></li> --}}                 
                  {{-- <li style="display: block;" @if(\Request::segment(3) == "posted_recruiter" ) class="active" @endif><a href="{{ url($admin_panel_slug.'/projects/posted_recruiter')}}">Assign Recruiter</a></li> --}}
                  <li style="display: block;" @if(\Request::segment(3) == "open" ) class="active" @endif ><a href="{{ url($admin_panel_slug.'/projects/open')}}">Open Projects</a></li>
                  <li style="display: block;" @if(\Request::segment(3) == "ongoing" ) class="active" @endif><a href="{{ url($admin_panel_slug.'/projects/ongoing')}}">Ongoing Projects</a></li>

                  <li style="display: block;" @if(\Request::segment(3) == "completed" ) class="active" @endif ><a href="{{ url($admin_panel_slug.'/projects/completed')}}">Completed Projects</a></li>
                  <li style="display: block;" @if(\Request::segment(3) == "canceled" ) class="active" @endif ><a href="{{ url($admin_panel_slug.'/projects/canceled')}}">Canceled Projects</a></li>
                  <li style="display: block;" @if(\Request::segment(3) == "all" ) class="active" @endif ><a href="{{ url($admin_panel_slug.'/projects/all')}}">All Projects</a></li>
               </ul>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('projects',$arr_permissions))
             <li class="<?php  if((Request::segment(2) == 'projects' || Request::segment(2) == 'bids') && (Request::segment(3) =='open' || Request::segment(3)=='ongoing' || Request::segment(3)=='completed' || Request::segment(3)=='canceled' || Request::segment(3)=='all')){ echo 'active'; } ?>">
               <a href="javascript:void(0)" class="dropdown-toggle">
                  <i class="fa fa-gears"></i>
                  <span>Projects</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu">
                  {{-- @if($user_role == 'admin')
                     <li style="display: block;" @if(\Request::segment(3) == "posted" ) class="active" @endif><a href="{{ url($admin_panel_slug.'/projects/posted')}}">Assign Project Manager</a></li>
                  @elseif($user_role == 'subadmin' && array_key_exists('project_manager',$arr_permissions))
                     <li style="display: block;" @if(\Request::segment(3) == "posted" ) class="active" @endif><a href="{{ url($admin_panel_slug.'/projects/posted')}}">Assign Project Manager</a></li>
                  @endif

                  @if($user_role == 'admin')
                     <li style="display: block;" @if(\Request::segment(3) == "posted_recruiter" ) class="active" @endif><a href="{{ url($admin_panel_slug.'/projects/posted_recruiter')}}">Assign Recruiter</a></li>
                  @elseif($user_role == 'subadmin' && array_key_exists('recruiter',$arr_permissions))
                     <li style="display: block;" @if(\Request::segment(3) == "posted_recruiter" ) class="active" @endif><a href="{{ url($admin_panel_slug.'/projects/posted_recruiter')}}">Assign Recruiter</a></li>
                  @endif --}}

                  <li style="display: block;" @if(\Request::segment(3) == "open" ) class="active" @endif ><a href="{{ url($admin_panel_slug.'/projects/open')}}">Open Projects</a></li>
                  <li style="display: block;" @if(\Request::segment(3) == "ongoing" ) class="active" @endif><a href="{{ url($admin_panel_slug.'/projects/ongoing')}}">Ongoing Projects</a></li>

                  <li style="display: block;" @if(\Request::segment(3) == "completed" ) class="active" @endif ><a href="{{ url($admin_panel_slug.'/projects/completed')}}">Completed Projects</a></li>
                  <li style="display: block;" @if(\Request::segment(3) == "canceled" ) class="active" @endif ><a href="{{ url($admin_panel_slug.'/projects/canceled')}}">Canceled Projects</a></li>
                  <li style="display: block;" @if(\Request::segment(3) == "all" ) class="active" @endif ><a href="{{ url($admin_panel_slug.'/projects/all')}}">All Projects</a></li>
               </ul>
            </li>
         @endif

         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'contests'){ echo 'active'; } ?>">
               <a href="javascript:void(0)" class="dropdown-toggle">
                  <i class="fa fa-trophy"></i>
                  <span>Contests</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu">
                  <li style="display: block;" @if(\Request::segment(3) == "pricing" ) class="active" @endif><a href="{{ url($admin_panel_slug.'/contests/pricing')}}">Pricing</a></li>
                  <li style="display: block;" @if(\Request::segment(3) == "manage" ) class="active" @endif><a href="{{ url($admin_panel_slug.'/contests/manage')}}">All Contests</a></li>
                  <li style="display: block;" @if(\Request::segment(3) == "entries" ) class="submenu" @endif><a href="#" style="display: none;" id="entriesmenu">Entries</a></li>
               </ul>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('contests',$arr_permissions))
            <li class="<?php  if(Request::segment(2) == 'contests'){ echo 'active'; } ?>">
               <a href="javascript:void(0)" class="dropdown-toggle">
                  <i class="fa fa-trophy"></i>
                  <span>Contests</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu">
                  <li style="display: block;" @if(\Request::segment(3) == "pricing" ) class="active" @endif><a href="{{ url($admin_panel_slug.'/contests/pricing')}}">Pricing</a></li>
                  <li style="display: block;" @if(\Request::segment(3) == "manage" ) class="active" @endif><a href="{{ url($admin_panel_slug.'/contests/manage')}}">All Contests</a></li>
                  <li style="display: block;" @if(\Request::segment(3) == "entries" ) class="submenu" @endif><a href="#" style="display: none;" id="entriesmenu">Entries</a></li>
               </ul>
            </li>
         @endif

         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'dispute'){ echo 'active'; } ?>">
               <a href="javascript:void(0)" class="dropdown-toggle">
                  <i class="fa fa-times-circle-o"></i>
                  <span>Dispute</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu">
                  <li style="display: block;"><a href="{{ url($admin_panel_slug.'/dispute')}}">Manage</a></li>
               </ul>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('dispute',$arr_permissions))
            <li class="<?php  if(Request::segment(2) == 'dispute'){ echo 'active'; } ?>">
               <a href="javascript:void(0)" class="dropdown-toggle">
                  <i class="fa fa-times-circle-o"></i>
                  <span>Dispute</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu">
                  <li style="display: block;"><a href="{{ url($admin_panel_slug.'/dispute')}}">Manage</a></li>
               </ul>
            </li>
         @endif

         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'skills'){ echo 'active'; } ?>">
               <a href="javascript:void(0)" class="dropdown-toggle">
                  <i class="fa fa-graduation-cap"></i>
                  <span>Skills</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu">
                  <li style="display: block;"><a href="{{ url($admin_panel_slug.'/skills')}}">Manage </a></li>
               </ul>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('skills',$arr_permissions))
            <li class="<?php  if(Request::segment(2) == 'skills'){ echo 'active'; } ?>">
               <a href="javascript:void(0)" class="dropdown-toggle">
                  <i class="fa fa-graduation-cap"></i>
                  <span>Skills</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu">
                  <li style="display: block;"><a href="{{ url($admin_panel_slug.'/skills')}}">Manage </a></li>
               </ul>
            </li>
         @endif

         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'countries' || Request::segment(2) == 'states' || Request::segment(2) == 'cities'){ echo 'active'; } ?>">
               <a href="javascript:void(0)" class="dropdown-toggle">
                  <i class="fa fa-road"></i>
                  <span>Locations</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu">
                  <li style="display: block;" class="<?php  if(Request::segment(2) == 'countries'){ echo 'active'; } ?>"><a href="{{ url($admin_panel_slug.'/countries')}}">Countries </a></li>
                  <li style="display: block;" class="<?php  if(Request::segment(2) == 'states'){ echo 'active'; } ?>"><a href="{{ url($admin_panel_slug.'/states')}}">States </a></li>
                  <li style="display: block;" class="<?php  if(Request::segment(2) == 'cities'){ echo 'active'; } ?>"><a href="{{ url($admin_panel_slug.'/cities')}}">Cities </a></li>
               </ul>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('locations',$arr_permissions))
            <li class="<?php  if(Request::segment(2) == 'countries' || Request::segment(2) == 'states' || Request::segment(2) == 'cities'){ echo 'active'; } ?>">
               <a href="javascript:void(0)" class="dropdown-toggle">
                  <i class="fa fa-road"></i>
                  <span>Locations</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu">
                  <li style="display: block;" class="<?php  if(Request::segment(2) == 'countries'){ echo 'active'; } ?>"><a href="{{ url($admin_panel_slug.'/countries')}}">Countries </a></li>
                  <li style="display: block;" class="<?php  if(Request::segment(2) == 'states'){ echo 'active'; } ?>"><a href="{{ url($admin_panel_slug.'/states')}}">States </a></li>
                  <li style="display: block;" class="<?php  if(Request::segment(2) == 'cities'){ echo 'active'; } ?>"><a href="{{ url($admin_panel_slug.'/cities')}}">Cities </a></li>
               </ul>
            </li>
         @endif

            <?php /*
            <li class="<?php  if(Request::segment(2) == 'newsletters' || Request::segment(2) == 'newsletter_subscriber'){ echo 'active'; } ?>">
            <a href="javascript:void(0)" class="dropdown-toggle">
            <i class="fa fa-newspaper-o"></i>
            <span>Newsletters</span>
            <b class="arrow fa fa-angle-right"></b>
            </a>
            <ul class="submenu">
            <li style="display: block;" class="<?php  if(Request::segment(2) == 'newsletters'){ echo 'active'; } ?>"><a href="{{ url($admin_panel_slug.'/newsletters')}}">Manage Newsletters </a></li>
            <li style="display: block;" class="<?php  if(Request::segment(2) == 'newsletters' && Request::segment(3) == 'send'){ echo 'active'; } ?>"><a href="{{ url($admin_panel_slug.'/newsletters/send')}}">Send Newsletters </a></li>
            <li style="display: block;" class="<?php  if(Request::segment(2) == 'newsletter_subscriber'){ echo 'active'; } ?>"><a href="{{ url($admin_panel_slug.'/newsletter_subscriber')}}">Newsletter subscriber</a></li>
            </ul>
            </li>
            */?>

         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'static_pages'){ echo 'active'; } ?>">
               <a href="javascript:void(0)" class="dropdown-toggle">
                  <i class="fa fa-file-text"></i>
                  <span>CMS</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu">
                  <li style="display: block;"><a href="{{ url($admin_panel_slug.'/static_pages')}}">Manage </a></li>
               </ul>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('static_pages',$arr_permissions))
            <li class="<?php  if(Request::segment(2) == 'static_pages'){ echo 'active'; } ?>">
               <a href="javascript:void(0)" class="dropdown-toggle">
                  <i class="fa fa-file-text"></i>
                  <span>CMS</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu">
                  <li style="display: block;"><a href="{{ url($admin_panel_slug.'/static_pages')}}">Manage </a></li>
               </ul>
            </li>
         @endif

         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'contact_inquiries'){ echo 'active'; } ?>">
               <a href="{{url('/')}}/admin/contact_inquiries" >
                  <i class="fa fa-envelope-square"></i>
                  <span>Contact Inquiries</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('contact_inquiries',$arr_permissions))
            <li class="<?php  if(Request::segment(2) == 'contact_inquiries'){ echo 'active'; } ?>">
               <a href="{{url('/')}}/admin/contact_inquiries" >
                  <i class="fa fa-envelope-square"></i>
                  <span>Contact Inquiries</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @endif

         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'project_milestones' || Request::segment(2) == 'milestones'){ echo 'active'; } ?>">
               <a href="javascript:void(0)" class="dropdown-toggle">
                  <i class="fa fa-star"></i>
                  <span>Milestones</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu">
                  <li style="display: block;"><a href="{{ url($admin_panel_slug.'/project_milestones')}}">Manage</a></li>
                  <li style="display: block;" class="<?php  if(Request::segment(2) == 'milestones' && Request::segment(3) == 'refund'){ echo 'active'; } ?>"><a href="{{ url($admin_panel_slug.'/milestones/refund/requests')}}">Refund requests</a></li>
               </ul>
            </li>
         @endif

         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'subscription_packs'){ echo 'active'; } ?>">
               <a href="{{url('/')}}/admin/subscription_packs" >
                  <i class="fa fa-suitcase"></i>
                  <span>Subscription Packs</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @endif

         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'project-priority-pacakges'){ echo 'active'; } ?>">
               <a href="{{url('/')}}/admin/project-priority-pacakges/manage" >
                  <i class="fa fa-suitcase"></i>
                  <span>Priority pacakges</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('project-priority-pacakges',$arr_permissions))
            <li class="<?php  if(Request::segment(2) == 'project-priority-pacakges'){ echo 'active'; } ?>">
               <a href="{{url('/')}}/admin/project-priority-pacakges/manage" >
                  <i class="fa fa-suitcase"></i>
                  <span>Priority pacakges</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @endif
            <!-- <li class="<?php // if(Request::segment(2) == 'payment_settings'){ echo 'active'; } ?>">
            <a href="{{url('/')}}/admin/payment_settings" >
            <i class="fa fa-money"></i>
            <span>Payment Settings</span>
            <b class="arrow fa fa-angle-right"></b>
            </a>
            </li> -->

         @if($user_role == 'admin')
            <li class="<?php if(Request::segment(2) == 'wallet'){ echo 'active'; } ?>">
               <a href="javascript:void(0)" class="dropdown-toggle">
                  <i class="fa fa-money"></i>
                  <span>Wallets</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu">
                  <li style="display: block;" class="<?php  if(Request::segment(2) == 'wallet' && Request::segment(3) == 'fee'){ echo 'active'; } ?>"><a href="{{ url($admin_panel_slug.'/wallet/fee')}}">Fee Wallet </a></li>
                  <li style="display: block;" class="<?php  if(Request::segment(2) == 'wallet' && Request::segment(3) == 'archexpert'){ echo 'active'; } ?>"><a href="{{ url($admin_panel_slug.'/wallet/archexpert')}}">Archexpert Wallet </a></li>
               </ul>
            </li> 
         @elseif($user_role == 'subadmin' && array_key_exists('wallets',$arr_permissions))
            <li class="<?php if(Request::segment(2) == 'wallet'){ echo 'active'; } ?>">
               <a href="javascript:void(0)" class="dropdown-toggle">
                  <i class="fa fa-money"></i>
                  <span>Wallets</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu">
                  <li style="display: block;" class="<?php  if(Request::segment(2) == 'wallet' && Request::segment(3) == 'fee'){ echo 'active'; } ?>"><a href="{{ url($admin_panel_slug.'/wallet/fee')}}">Fee Wallet </a></li>
                  <li style="display: block;" class="<?php  if(Request::segment(2) == 'wallet' && Request::segment(3) == 'archexpert'){ echo 'active'; } ?>"><a href="{{ url($admin_panel_slug.'/wallet/archexpert')}}">Archexpert Wallet </a></li>
               </ul>
            </li>
         @endif

         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'subadmins'){ echo 'active'; } ?>">
               <a href="{{url('/')}}/admin/subadmins" >
                  <i class="fa fa-users"></i>
                  <span>Sub-Admins</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @endif

         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'site_settings'){ echo 'active'; } ?>">
               <a href="{{url('/')}}/admin/site_settings" >
                  <i class="fa  fa-wrench"></i>
                  <span>Site Settings</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @endif

         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'support_ticket'){ echo 'active'; } ?>">
               <a href="javascript:void(0)" class="dropdown-toggle" >
                  <i class="fa fa-ticket"></i>
                  <span>Support Tickets</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu">
                  <li style="display: block;"><a href="{{ url($admin_panel_slug.'/support_ticket')}}">Manage</a></li>
                  <li style="display: block;"><a href="{{ url($admin_panel_slug.'/support_ticket/support_categories')}}">Department Categories</a></li>
               </ul>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('support_ticket',$arr_permissions))
            <li class="<?php  if(Request::segment(2) == 'support_ticket'){ echo 'active'; } ?>">
               <a href="javascript:void(0)" class="dropdown-toggle" >
                  <i class="fa fa-ticket"></i>
                  <span>Support Tickets</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu">
                  <li style="display: block;"><a href="{{ url($admin_panel_slug.'/support_ticket')}}">Manage</a></li>
               </ul>
            </li>
         @endif

         @if($user_role == 'admin')
            <li <?php if(Request::segment(2) == 'faq') { ?>  class="active"  <?php } ?>>  
               <a href="javascript:void(0)" class="dropdown-toggle">
                  <i class="fa fa-question-circle"></i>
                  <span>FAQ</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu">
                  <li style="display: block;"><a href="{{ url($admin_panel_slug.'/faq')}}">Manage</a></li>
               </ul>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('faq',$arr_permissions))
            <li <?php if(Request::segment(2) == 'faq') { ?>  class="active"  <?php } ?>>  
               <a href="javascript:void(0)" class="dropdown-toggle">
                  <i class="fa fa-question-circle"></i>
                  <span>FAQ</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu">
                  <li style="display: block;"><a href="{{ url($admin_panel_slug.'/faq')}}">Manage</a></li>
               </ul>
            </li>
         @endif

         @if($user_role == 'admin')
            <li <?php if(Request::segment(2) == 'blogs') { ?>  class="active"  <?php } ?>>  
               <a href="javascript:void(0)" class="dropdown-toggle">
                  <i class="fa fa-comment"></i>
                  <span>Blogs</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu">
                  <li style="display: block;"><a href="{{ url($admin_panel_slug.'/blogs')}}">Manage</a></li>
                  <li style="display: block;" class="@if(Request::segment(2) == 'blogs' && Request::segment(3) == 'category') active @endif"><a class="sidebar_option" href="{{ url($admin_panel_slug.'/blogs/category')}}">Categories </a></li>
               </ul>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('blogs',$arr_permissions))
            <li <?php if(Request::segment(2) == 'blogs') { ?>  class="active"  <?php } ?>>  
               <a href="javascript:void(0)" class="dropdown-toggle">
                  <i class="fa fa-comment"></i>
                  <span>Blogs</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu">
                  <li style="display: block;"><a href="{{ url($admin_panel_slug.'/faq')}}">Manage</a></li>
                  <li style="display: block;" class="@if(Request::segment(2) == 'blogs' && Request::segment(3) == 'category') active @endif"><a class="sidebar_option" href="{{ url($admin_panel_slug.'/blogs/category')}}">Categories </a></li>
               </ul>
            </li>
         @endif

         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'transactions'){ echo 'active'; } ?>">
               <a href="{{url('/')}}/admin/transactions" >
                  <i class="fa  fa-money  "></i>
                  <span>Transactions</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('transactions',$arr_permissions))
            <li class="<?php  if(Request::segment(2) == 'transactions'){ echo 'active'; } ?>">
               <a href="{{url('/')}}/admin/transactions" >
                  <i class="fa  fa-money  "></i>
                  <span>Transactions</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @endif

         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'email'){ echo 'active'; } ?>">
               <a href="{{url('/')}}/admin/email/listing" >
                  <i class="fa fa-envelope-o"></i>
                  <span>Send Mail</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('email',$arr_permissions))
            <li class="<?php  if(Request::segment(2) == 'email'){ echo 'active'; } ?>">
               <a href="{{url('/')}}/admin/email/listing" >
                  <i class="fa fa-envelope-o"></i>
                  <span>Send Mail</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li>
         @endif


         @if($user_role == 'admin')
            {{-- <li class="<?php  if(Request::segment(2) == 'email_template'){ echo 'active'; } ?>">
               <a href="{{url('/')}}/admin/email_template" >
                  <i class="fa fa-envelope-o"></i>
                  <span>Email Template</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li> --}}
         @elseif($user_role == 'subadmin' && array_key_exists('email_template',$arr_permissions))
            {{-- <li class="<?php  if(Request::segment(2) == 'email_template'){ echo 'active'; } ?>">
               <a href="{{url('/')}}/admin/email_template" >
                  <i class="fa fa-envelope-o"></i>
                  <span>Email Template</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
            </li> --}}
         @endif



         @if($user_role == 'admin')
            <li class="<?php  if(Request::segment(2) == 'reports'){ echo 'active'; } ?>">
               <a href="javascript:void(0)" class="dropdown-toggle">
                  <i class="fa fa-file-text"></i>
                  <span>Reports</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu">
                  <li style="display: block;"><a href="{{ url($admin_panel_slug.'/reports')}}">User reports </a></li>
               </ul>
            </li>
         @elseif($user_role == 'subadmin' && array_key_exists('reports',$arr_permissions))
            <li class="<?php  if(Request::segment(2) == 'reports'){ echo 'active'; } ?>">
               <a href="javascript:void(0)" class="dropdown-toggle">
                  <i class="fa fa-file-text"></i>
                  <span>Reports</span>
                  <b class="arrow fa fa-angle-right"></b>
               </a>
               <ul class="submenu">
                  <li style="display: block;"><a href="{{ url($admin_panel_slug.'/reports')}}">User reports </a></li>
               </ul>
            </li>
         @endif

@endif
<!-- END Navlist -->
<!-- BEGIN Sidebar Collapse Button -->
<div id="sidebar-collapse" class="visible-lg">
   <i class="fa fa-angle-double-left"></i>
</div>
<!-- END Sidebar Collapse Button -->
</div>
<div id="main-content" class="middle-content" style=''>