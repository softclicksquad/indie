    @extends('admin.layout.master')


    @section('main_content')
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>

      </div>
    </div>
    <!-- END Page Title -->

    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="fa fa-home"></i>
          <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
        </li>
        <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-file-text"></i>
          <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
        </span> 
        <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-edit"></i>
        </span>
        <li class="active">{{ $page_title or ''}}</li>
      </ul>
    </div>
    <!-- END Breadcrumb -->


    <!-- BEGIN Main Content -->
    <div class="row">
      <div class="col-md-12">

        <div class="box">
          <div class="box-title">
            <h3>
              <i class="fa fa-edit"></i>
              {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
              <a data-action="collapse" href="#"></a>
              <a data-action="close" href="#"></a>
            </div>
          </div>
          <div class="box-content">

            @include('admin.layout._operation_status') 

            <div class="tabbable">
              <form name="validation-form" id="validation-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/update/{{$enc_id}}" enctype="multipart/form-data"> 
              {{ csrf_field() }}


               <ul  class="nav nav-tabs">
                @include('admin.layout._multi_lang_tab')
              </ul>                                
              <div id="myTabContent1" class="tab-content">

                @if(isset($arr_lang) && sizeof($arr_lang)>0)
                @foreach($arr_lang as $lang)
                 <?php 
                      /* Locale Variable */  
                      $locale_page_name ="";
                      $locale_page_title = "";
                      $locale_meta_title = "";
                      $locale_meta_keyword = "";
                      $locale_meta_desc = "";
                      $locale_page_desc = "";
                      

                      if(isset($arr_static_page['translations'][$lang['locale']]))
                      {
                          $locale_page_name = $arr_static_page['translations'][$lang['locale']]['page_name'];
                          $locale_page_title = $arr_static_page['translations'][$lang['locale']]['page_title'];
                          $locale_meta_title = $arr_static_page['translations'][$lang['locale']]['meta_title'];
                          $locale_meta_keyword = $arr_static_page['translations'][$lang['locale']]['meta_keyword'];
                          $locale_meta_desc = $arr_static_page['translations'][$lang['locale']]['meta_desc'];
                          $locale_page_desc = $arr_static_page['translations'][$lang['locale']]['page_desc'];
                      }
                  ?>
                <div class="tab-pane fade {{ $lang['locale']=='en'?'in active':'' }}"
                id="{{ $lang['locale'] }}">

                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="page_name">Page Name<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                    

                    @if($lang['locale'] == 'en') 
                        <input type="text" name="page_name_{{$lang['locale']}}" class="form-control" value="{{$locale_page_name}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Page Name">
                    @else
                           <input type="text" name="page_name_{{$lang['locale']}}" class="form-control" value="{{$locale_page_name}}" data-rule-required="true" data-rule-maxlength="255"  placeholder="Page Name">
                    @endif    


                    <span class='error'>{{ $errors->first('page_name_'.$lang['locale']) }}</span>
                  </div>
                </div>


                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="page_title">Page Title<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                    

                    @if($lang['locale'] == 'en') 
                        <input type="text" name="page_title_{{$lang['locale']}}" class="form-control" value="{{$locale_page_title}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Page Title">
                    @else
                           <input type="text" name="page_title_{{$lang['locale']}}" class="form-control" value="{{$locale_page_title}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Page Title">
                    @endif    


                    <span class='error'>{{ $errors->first('page_title_'.$lang['locale']) }}</span>
                  </div>
                </div>


                 <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="meta_keyword">Meta Title<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                    

                    @if($lang['locale'] == 'en')        
                        <input type="text" name="meta_title_{{$lang['locale']}}" class="form-control" value="{{$locale_meta_title}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Meta Title ">
                    @else
                        <input type="text" name="meta_title_{{$lang['locale']}}" class="form-control" value="{{$locale_meta_title}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Meta Title">

                    @endif

                    <span class='error'>{{ $errors->first('meta_title_'.$lang['locale']) }}</span>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="meta_keyword">Meta Keyword<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                    

                    @if($lang['locale'] == 'en')        
                        <input type="text" name="meta_keyword_{{$lang['locale']}}" class="form-control" value="{{$locale_meta_keyword}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Meta Keyword">
                    @else
                        <input type="text" name="meta_keyword_{{$lang['locale']}}" class="form-control" value="{{$locale_meta_keyword}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Meta Keyword">

                    @endif

                    <span class='error'>{{ $errors->first('meta_keyword_'.$lang['locale']) }}</span>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="meta_desc">Meta Description<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">

                    @if($lang['locale'] == 'en')        
                        
                        <input type="text" name="meta_desc_{{$lang['locale']}}" class="form-control" value="{{$locale_meta_desc}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Meta Description"> 
                    @else
                    
                        <input type="text" name="meta_desc_{{$lang['locale']}}" data-rule-required="true" data-rule-maxlength="255" class="form-control" value="{{$locale_meta_desc}}" placeholder="Meta Description"> 
                    @endif


                    <span class='error'>{{ $errors->first('meta_desc_'.$lang['locale']) }}</span>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="page_desc">Page Content<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-8 controls">
                    

                    @if($lang['locale'] == 'en')        
                        

                        <textarea name="page_desc_{{$lang['locale']}}" rows="7" class="form-control" data-rule-required="true"  placeholder="Page Content">{{$locale_page_desc}}</textarea>
                    @else
                         <textarea name="page_desc_{{$lang['locale']}}" rows="7" class="form-control" data-rule-required="true" placeholder="Page Content">{{$locale_page_desc}}</textarea>
                    @endif

                    <span class='error'>{{ $errors->first('page_desc_'.$lang['locale']) }}</span>
                  </div>
                </div>


             
                
              </div>
              @endforeach
              @endif
            </div>

            <br>
            <div class="form-group">
              <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
               <input type="submit" value="Save" class="btn btn btn-primary">
              </div>
            </div>
            </form>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- END Main Content -->

  <script type="text/javascript">

    function saveTinyMceContent()
    {
      tinyMCE.triggerSave();
    }

    $(document).ready(function()
    {
      tinymce.init({
        selector: 'textarea',
        height:350,
        plugins: [
        'advlist autolink lists link charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime table contextmenu paste code'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
        content_css: [
        '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
        '//www.tinymce.com/css/codepen.min.css'
        ]
      });  
    });
  </script>

  @stop
