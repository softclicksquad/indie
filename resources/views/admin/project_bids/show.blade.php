@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-money"></i>
      <a href="{{ $module_url_path }}/{{base64_encode($arr_info['project_details']['id'])}}">{{ 'Manage ' }}{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-money"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-money"></i>
               {{ $arr_info['project_details']['project_name'] or '' }}
                @if(isset($arr_info['project_details']['project_name']) &&  $arr_info['project_details']['project_name'] != "" )
                  <span class="divider">
                    <i class="fa fa-angle-right"></i> 
                  </span>  
                @endif

               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
               @include('admin.layout._operation_status')
           <div class="alert alert-success" id="ajax_success" style="display:none;">
            <button data-dismiss="alert" class="close">×</button>
            <strong>Success!</strong>
            <div id="ajax_sub_success"></div>
         </div>
         <div class="alert alert-danger" id="ajax_error" style="display:none;">
            <button data-dismiss="alert" class="close">×</button>
            <strong>Error!</strong> 
            <div id="ajax_sub_error"></div>
         </div>
           
              <div class="row">

               @if(isset($arr_info) && sizeof($arr_info)>0)
              <form name="validation-form" id="validation-form" method="POST" action="" class="form-horizontal"  enctype="multipart/form-data">
               {{ csrf_field() }}
                <div class="col-md-6">
                   
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Project Name :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['project_details']['project_name'])?ucfirst($arr_info['project_details']['project_name']):'-'}}</div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Project Start Date :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['project_details']['project_start_date'])?$arr_info['project_details']['project_start_date']:'-'}}</div>
                     </div>
                  </div>
                   <div class="form-group">
                     <label class="col-sm-3 control-label">Project End Date :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['project_details']['project_end_date'])?$arr_info['project_details']['project_end_date']:'-'}}</div>
                     </div>
                  </div>
                    <div class="form-group">
                     <label class="col-sm-3 control-label">Project Description :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['project_details']['project_description'])?str_limit($arr_info['project_details']['project_description'],$limit = 200, $end='...'):'-'}}</div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Project Manage By :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">
                        @if($arr_info['project_details']['project_handle_by']=='1'){{'Self by client'}}@endif
                        @if($arr_info['project_details']['project_handle_by']=='2'){{'By project manager'}}@endif
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Bid Cost :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['project_details']['project_currency'])?$arr_info['project_details']['project_currency']:'-'}} {{isset($arr_info['bid_cost'])?$arr_info['bid_cost']:'-'}}</div>
                     </div>
                  </div>
                   <div class="form-group">
                     <label class="col-sm-3 control-label">Bid Duration :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['bid_estimated_duration'])?$arr_info['bid_estimated_duration']:'-'}}</div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Bid Description :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['bid_description'])?str_limit($arr_info['bid_description'],$limit = 200, $end='...'):'-'}}</div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Bid Attachment :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">@if(isset($arr_info['bid_attachment']) && $arr_info['bid_attachment']!="")
                        {{$arr_info['bid_attachment']}}
                        <a href="{{$bid_attachment_public_path.$arr_info['bid_attachment']}}" download>&nbsp;<i class="fa fa-download"></i>&nbsp;Download</a>
                        @else {{'-'}} @endif
                        </div>
                     </div>
                  </div>
                   <div class="form-group">
                     <label class="col-sm-3 control-label">Bid Status :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">
                        @if($arr_info['bid_status']=='0'){{'Pending'}}@endif
                        @if($arr_info['bid_status']=='1'){{'Assigned To Expert'}}@endif
                        @if($arr_info['bid_status']=='2'){{'Ignored'}}@endif
                        @if($arr_info['bid_status']=='3'){{'Rejected By Expert'}}@endif
                        @if($arr_info['bid_status']=='4'){{'Waiting For Acceptance of Expert'}}@endif
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Expert Name :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">
                        @if(isset($arr_info['role_info']['first_name']) && isset($arr_info['role_info']['last_name']))
                        {{ucfirst($arr_info['role_info']['first_name'])}}&nbsp;{{ucfirst($arr_info['role_info']['last_name'])}}
                        @endif
                        </div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Phone Number :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['role_info']['phone_code'])?$arr_info['role_info']['phone_code']:''}}{{isset($arr_info['role_info']['phone_number'])?$arr_info['role_info']['phone_number']:''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Address :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['role_info']['address'])?$arr_info['role_info']['address']:''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Company Name:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['role_info']['company_name'])?ucfirst($arr_info['role_info']['company_name']):''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>

                  <div class="form-group">
                     <label class="col-sm-3 control-label">Vat Number:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['role_info']['vat_number'])?$arr_info['role_info']['vat_number']:''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>

                  <?php 

                  $arr_spoken_lang     = [];
                  $arr_tmp_spoken_lang = [];
                  $str_lang  = "";

                  if(isset($arr_info['project_details']['expert_info']['expert_spoken_languages']))
                  {
                     $arr_tmp_spoken_lang = $arr_info['project_details']['expert_info']['expert_spoken_languages'];
                     
                     if(count($arr_tmp_spoken_lang))
                     {
                        foreach ($arr_tmp_spoken_lang as $key => $lang_data) 
                        {  
                           if(isset($lang_data['language']) && $lang_data['language'] != "")
                           {
                              array_push($arr_spoken_lang, $lang_data['language']);
                           }
                        } 
                     }

                     if(count($arr_spoken_lang) > 0)
                     {  
                        $str_lang = implode(', ', $arr_spoken_lang);
                     }
                  }

                  ?>

                   <div class="form-group">
                     <label class="col-sm-3 control-label">Experts Spoken languages:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{trim($str_lang,',')}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                </div>
              </form>
               @endif
            </div>
            </div>
       </div>
   </div>
</div>

@stop