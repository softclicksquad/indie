@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
        </li>
        <span class="divider">
            <i class="fa fa-angle-right"></i>
            <i class="fa fa-desktop"></i>
            <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
        </span> 
        <span class="divider">
            <i class="fa fa-angle-right"></i>
            <i class="fa fa-list"></i>
        </span>
        <li class="active">{{ $page_title or ''}}</li>
    </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3>
                    <i class="fa fa-list"></i>
                    {{ isset($page_title)?$page_title:"" }}
                </h3>
                <div class="box-tool">
                    <a data-action="collapse" href="#"></a>
                    <a data-action="close" href="#"></a>
                </div>
            </div>
            <div class="box-content" >
                @include('admin.layout._operation_status')

                <div class="row">
                    <div class="col-md-12">
                        <a class="btn btn-success pull-right" href="{{ $module_url_path.'/resolve_ticket/'.base64_encode($arr_ticket['id']) }}">Resolve Ticket</a>
                    </div>
                    <br>
                    <br>
                    <div class="clearfix"></div>
                    <div class="col-md-9">
                        <div class="well">
                            <h4>{{ $arr_ticket['title'] or '' }}</h4>
                            <p>{!! $arr_ticket['description'] !!}</p>
                        </div>
                        @if(isset($arr_threads) && !empty($arr_threads))
                        <ul class="mail-messages">
                            @foreach($arr_threads as $row)
                                @if(!empty($row['get_user_details']))
                                <li>
                                    <div class="mail-msg-header">
                                        <img src="{{ $user_img_path.$row['get_user_details']['role_info']['profile_image'] }}" alt="avatar">
                                        <div class="msg-sender-recver">
                                            <strong>{{ $row['get_user_details']['role_info']['first_name'] }} 
                                                {{ $row['get_user_details']['role_info']['last_name'] }}
                                            </strong><br>
                                            <i>{{ $row['get_user_details']['email'] or '' }}</i>
                                        </div>

                                        <div class="msg-collapse">
                                            <a href="#" class="show-tooltip" data-placement="left" data-original-title="Collapse/Uncollapse"><i class="fa fa-chevron-down"></i></a>
                                        </div>

                                        <div class="msg-options">
                                            <i>{{ date('M d, Y h:i A', strtotime($row['created_at'])) }}</i>
                                        </div>
                                    </div>

                                    <div class="mail-msg-container">
                                        <div class="mail-msg-content">
                                            {!! $row['description'] !!}
                                        </div>
                                        @if(isset($row['get_files']) && !empty($row['get_files']))
                                            <div class="mail-msg-footer">
                                                {{-- <p>Attachments <i class="gray">(3 files, 1.66 Mb)</i> <a class="font-size-9" href="#">Download All</a></p> --}}
                                                <ul class="mail-attachments">
                                                    @foreach($row['get_files'] as $file)
                                                    <li>
                                                        <span class="atch-option-iconic">
                                                            <a href="{{ $support_ticket_path.$file['file'] }}" class="show-tooltip" download="" data-placement="top" data-original-title="Download"><i class="fa fa-download"></i></a>
                                                        </span>
                                                        <span>{{ $file['file'] or '' }}</span>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                </li>
                                @elseif(!empty($row['get_admin_details']))
                                <li>
                                    <div class="mail-msg-header">
                                        <img src="{{$admin_img_path.$row['get_admin_details']['role_info']['image']}}" alt="avatar">
                                        <div class="msg-sender-recver">
                                            <strong>{{ $row['get_admin_details']['role_info']['name'] or '' }}</strong><br>
                                            <i>{{ $row['get_admin_details']['email'] or '' }}</i>
                                        </div>

                                        {{-- <div class="msg-collapse">
                                            <a href="#" class="show-tooltip" data-placement="left" data-original-title="Collapse/Uncollapse"><i class="fa fa-chevron-down"></i></a>
                                        </div> --}}

                                        <div class="msg-options">
                                            <i>{{ date('M d, Y h:i A', strtotime($row['created_at'])) }}</i>
                                        </div>
                                    </div>

                                    <div class="mail-msg-container">
                                        <div class="mail-msg-content">
                                            {!! $row['description'] !!}
                                        </div>
                                        @if(isset($row['get_files']) && !empty($row['get_files']))
                                            <div class="mail-msg-footer">
                                                {{-- <p>Attachments <i class="gray">(3 files, 1.66 Mb)</i> <a class="font-size-9" href="#">Download All</a></p> --}}
                                                <ul class="mail-attachments">
                                                    @foreach($row['get_files'] as $file)
                                                    <li>
                                                        <span class="atch-option-iconic">
                                                            <a href="{{ $support_ticket_path.$file['file'] }}" class="show-tooltip" download="" data-placement="top" data-original-title="Download"><i class="fa fa-download"></i></a>
                                                        </span>
                                                        <span>{{ $file['file'] or '' }}</span>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                </li>
                                @endif
                            @endforeach
                        </ul>
                        @endif

                        <br><br><hr>

                        <ul class="mail-messages">
                            <li class="msg-reply">
                                <form class="horizontal-form form-horizontal" method="post" action="{{ $module_url_path.'/submit_reply/'.base64_encode($arr_ticket['id']) }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label">Reply message</label>
                                        <div class="col-sm-3 col-lg-10 controls">
                                            <textarea class="form-control" name="reply_msg" placeholder="Replay to this mail" rows="5"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label">Attach file</label>
                                        <div class="col-sm-3 col-lg-10 controls">
                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <div class="input-group">
                                                    <div class="form-control uneditable-input">
                                                        <i class="fa fa-file fileupload-exists"></i> 
                                                        <span class="fileupload-preview"></span>
                                                    </div>
                                                    <div class="input-group-btn">
                                                        <a class="btn bun-default btn-file">
                                                            <span class="fileupload-new">Select file</span>
                                                            <span class="fileupload-exists">Change</span>
                                                            <input type="file" name="upload_file[]" class="file-input" multiple="">
                                                        </a>
                                                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3 col-lg-12 control">
                                            <button class="btn btn-primary"><i class="fa fa-reply"></i> Reply</button>
                                        </div>
                                    </div>
                                </form>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">Ticket Info</h4>
                            </div>
                            <div class="panel-body">
                                <dl class="">
                                <dt>Ticket Opened By :</dt>
                                <dd>
                                    {{ $arr_ticket['get_user_details']['role_info']['first_name'] or '' }} 
                                    {{ $arr_ticket['get_user_details']['role_info']['last_name'] or '' }}
                                    <br>
                                    {{ $arr_ticket['get_user_details']['email'] or '' }}
                                </dd>
                                <br>
                                <dt>Opened On. :</dt>
                                <dd>{{ date('D, jS M Y h:i A', strtotime($arr_ticket['created_at'])) }}</dd>
                                <br>
                                <dt>Ticket No. :</dt>
                                <dd>{{ $arr_ticket['ticket_number'] or '' }}</dd>
                                <br>
                                <dt>Department :</dt>
                                <dd>{{ isset($arr_ticket['get_category']) && isset($arr_ticket['get_category']['title']) ? $arr_ticket['get_category']['title'] : '-' }}</dd>
                                <br>
                                <dt>Priority :</dt>
                                <dd> {{ strtoupper($arr_ticket['priority']) or '' }} </dd>
                            </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- END Main Content -->
<link href="{{url('/public')}}/admin/css/select2.min.css" rel="stylesheet" />

<script src="{{url('/public')}}/admin/js/select2.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $(".project_manager").select2();
    });
</script>
@stop