@extends('admin.layout.master')
@section('main_content')

<?php

$cnt_open_tickets = get_tickets_count('in_progress');
$cnt_reopened_tickets = get_tickets_count('re_opened');
$cnt_resolved_tickets = get_tickets_count('resolved');
$cnt_closed_tickets = get_tickets_count('closed');

//$cnt_open_tickets += $cnt_reopened_tickets;

//dd($cnt_open_tickets, $cnt_resolved_tickets, $cnt_closed_tickets, $cnt_reopened_tickets);

?>

<!-- BEGIN Page Title -->
<link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/data-tables/latest/dataTables.bootstrap.min.css">
    
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
        </li>
        <span class="divider">
            <i class="fa fa-angle-right"></i>
            <i class="fa fa-users"></i>
            <a href="{{ $module_url_path }}">{{ isset($page_title)?str_plural($page_title):"" }}</a>
        </span>
        <span class="divider">
            <i class="fa fa-angle-right"></i>
            <i class="fa fa-users"></i>
        </span>
        <li class="active">{{ isset($page_title)?str_plural($page_title):"" }}</li>
    </ul>
</div>
<!-- END Breadcrumb -->

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3>
                <i class="fa fa-users"></i>
                {{ isset($page_title)?str_plural($page_title):"" }}
                </h3>
                <div class="box-tool">
                    <a data-action="collapse" href="#"></a>
                    <a data-action="close" href="#"></a>
                </div>
            </div>
            <div class="box-content">
                @include('admin.layout._operation_status')
                <form name="frm_manage" id="frm_manage" method="POST" class="form-horizontal" action="{{$module_url_path}}/multi_action">
                    {{ csrf_field() }}

                    <div class="tabbable">
                        <ul id="myTab1" class="nav nav-tabs">
                            <li class="<?php echo (Request::has('type') && Request::input('type') == 'in_progress') || !Request::has('type') ? 'active' : ''; ?>">
                                <a href="{{ $module_url_path }}"> Open ({{ $cnt_open_tickets or '' }}) </a>
                            </li>
                            <li class="<?php echo Request::has('type') && Request::input('type') == 're-opened' ? 'active' : ''; ?>" >
                                <a href="{{ $module_url_path.'?type=re-opened' }}"> Re-opened ({{ $cnt_reopened_tickets or '' }}) </a>
                            </li>
                            <li class="<?php echo Request::has('type') && Request::input('type') == 'resolved' ? 'active' : ''; ?>" >
                                <a href="{{ $module_url_path.'?type=resolved' }}"> Resolved ({{ $cnt_resolved_tickets or '' }}) </a>
                            </li>
                            <li class="<?php echo Request::has('type') && Request::input('type') == 'closed' ? 'active' : ''; ?>" >
                                <a href="{{ $module_url_path.'?type=closed' }}"> Closed ({{ $cnt_closed_tickets or '' }}) </a>
                            </li>
                        </ul>

                        <br/>
                        <div class="clearfix"></div>

                        <div class="btn-toolbar pull-right clearfix">
                            <div class="btn-group"> 
                                <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                                    title="Refresh" 
                                    href="{{ $module_url_path }}"
                                    style="text-decoration:none;">
                                    <i class="fa fa-repeat"></i>
                                </a> 
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <br>

                        <div class="table-responsive" style="border:0">
                            <input type="hidden" name="multi_action" value="" />
                            <table class="table table-advance"  id="table1" >
                                <thead>
                                    <tr>
                                        <th style="width:18px"> <input type="checkbox" name="mult_change" id="mult_change" /></th>
                                        <th>Ticket Id</th>
                                        <th>User Email ID</th>
                                        <th>Subject</th>
                                        <th>Department</th>
                                        <th>Date Created</th>
                                        @if(Request::has('type') && Request::input('type') != 'resolved' && Request::input('type') != 'closed' || !Request::has('type'))
                                        <th>Status</th>
                                        @endif
                                        @if((Request::has('type') && (Request::input('type') == 'in_progress') || Request::input('type') == 're-opened') || !Request::has('type'))
                                        <th>Action</th>
                                        @endif
                                    </tr>
                                </thead>

                                <tbody>
                                    @if(isset($arr_tickets) && sizeof($arr_tickets)>0)
                                        @foreach($arr_tickets as $arr_ticket)
                                        <tr class="{{ $arr_ticket['priority'] == 'urgent' ? 'table-flag-red' : 'table-flag-blue' }}">
                                            <td>
                                                <input type="checkbox" name="checked_record[]" value="{{ base64_encode($arr_ticket['user_id']) }}" /> 
                                            </td>

                                            <td> {{ isset($arr_ticket['ticket_number'])?$arr_ticket['ticket_number']:'' }}</td>

                                            <td> {{ isset($arr_ticket['get_user_details']) && isset($arr_ticket['get_user_details']['email'])?$arr_ticket['get_user_details']['email']:'' }}</td>

                                            <td> {{ isset($arr_ticket['title']) ? substr($arr_ticket['title'], 0,50).'...' :''  }} </td>

                                            <td> {{ isset($arr_ticket['get_category']) && !empty($arr_ticket['get_category']) ? $arr_ticket['get_category']['title'] : ''  }} </td>

                                            <td> {{ isset($arr_ticket['created_at']) ? date('jS M Y', strtotime($arr_ticket['created_at'])) : '' }} </td>

                                            @if(Request::has('type') && Request::input('type') != 'resolved' && Request::input('type') != 'closed' || !Request::has('type'))

                                            <td>
                                                @if(isset($arr_ticket['status']) && ($arr_ticket['status'] == 'in_progress' || $arr_ticket['status'] == 're_opened') )
                                                    @if(isset($arr_ticket['last_reply_by']) && $arr_ticket['last_reply_by'] == 'user')
                                                    <span class="label label-warning">Need Action</span>
                                                    @else
                                                    <span class="label label-gray">Waiting for Reply</span>
                                                    @endif
                                                @elseif(isset($arr_ticket['status']) && $arr_ticket['status'] == 'closed' )
                                                <span class="label label-success">Closed</span>
                                                {{-- @elseif(isset($arr_ticket['status']) && $arr_ticket['status'] == 're_opened' )
                                                <span class="label label-info">Re-opened</span> --}}
                                                @endif
                                            </td>
                                            @endif
                                            @if((Request::has('type') && (Request::input('type') == 'in_progress') || Request::input('type') == 're-opened') || !Request::has('type'))
                                            <td>
                                                <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" href="{{ $module_url_path.'/reply/'.base64_encode($arr_ticket['id']) }}" title="Reply"><i class="fa fa-reply" ></i>
                                                </a>
                                                <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" href="javascript:void(0)" onclick="javascript:return confirm_resolve('{{ $module_url_path.'/resolve_ticket/'.base64_encode($arr_ticket['id'])}}')" title="Resolve"><i class="fa fa-check" ></i>
                                                </a>
                                            </td>
                                            @endif
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>

                    </div>{{-- Tabable --}}
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Main Content -->
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
   function confirm_resolve(url) {
      alertify.confirm("Are you sure? You want to resolve this ticket", function (e) {
          if (e) {
              if(url != "" && url != undefined){
                showProcessingOverlay();
                window.location.href=url;
              }
              return true;
          } else {
              return false;
          }
      });
   }
   function check_multi_action(frm_id,action){
     var frm_ref = jQuery("#"+frm_id);
     if(jQuery(frm_ref).length && action!=undefined && action!=""){
       var checked_record = jQuery('input[name="checked_record[]"]:checked');
       if (jQuery(checked_record).size() < 1){
          alertify.alert('please select at least one record.');
          return false;
       }
       if(action == 'delete'){
          if (!confirm_delete()){
              return false;
          }
       }
       /* Get hidden input reference */
       var input_multi_action = jQuery('input[name="multi_action"]');
       if(jQuery(input_multi_action).length){
         /* Set Action in hidden input*/
         jQuery('input[name="multi_action"]').val(action);
         /*Submit the referenced form */
         jQuery(frm_ref)[0].submit();
       } else {
         console.warn("Required Hidden Input[name]: multi_action Missing in Form ")
       }
     } else {
         console.warn("Required Form[id]: "+frm_id+" Missing in Current Page ")
     }
   }
</script>
@stop