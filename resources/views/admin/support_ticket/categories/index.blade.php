@extends('admin.layout.master')
@section('main_content')

<!-- BEGIN Page Title -->
<link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/data-tables/latest/dataTables.bootstrap.min.css">
    
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
        </li>
        <span class="divider">
            <i class="fa fa-angle-right"></i>
            <i class="fa fa-users"></i>
            <a href="{{ $module_url_path }}">{{ isset($page_title)?str_plural($page_title):"" }}</a>
        </span> 
        <span class="divider">
            <i class="fa fa-angle-right"></i>
            <i class="fa fa-users"></i>
        </span>
        <li class="active">{{ isset($page_title)?str_plural($page_title):"" }}</li>
    </ul>
</div>
<!-- END Breadcrumb -->

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3>
                <i class="fa fa-users"></i>
                {{ isset($page_title)?str_plural($page_title):"" }}
                </h3>
                <div class="box-tool">
                    <a data-action="collapse" href="#"></a>
                    <a data-action="close" href="#"></a>
                </div>
            </div>
            <div class="box-content">
                @include('admin.layout._operation_status')
                <form name="frm_manage" id="frm_manage" method="POST" class="form-horizontal" action="{{$module_url_path}}/multi_action">
                    {{ csrf_field() }}

                    <div class="tabbable">
                        <br/>
                        <div class="clearfix"></div>

                        <div class="btn-toolbar pull-right clearfix">

                            <div class="btn-group">
                                <a href="{{ $module_url_path.'/create'}}" class="btn btn-primary btn-add-new-records" title="Create Ticket">Create Category</a>                      
                            </div>

                            {{-- <div class="btn-group">
                                <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                                    title="Multiple Active/Unblock" 
                                    href="javascript:void(0);" 
                                    onclick="javascript : return check_multi_action('frm_manage','activate');" 
                                    style="text-decoration:none;">
                                    <i class="fa fa-unlock"></i>
                                </a> 
                            </div>
                            <div class="btn-group">
                                <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                                    title="Multiple Deactive/Block" 
                                    href="javascript:void(0);" 
                                    onclick="javascript : return check_multi_action('frm_manage','deactivate');"  
                                    style="text-decoration:none;">
                                 <i class="fa fa-lock"></i>
                                </a>
                            </div> --}}
                            <div class="btn-group"> 
                                <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                                    title="Refresh" 
                                    href="{{ $module_url_path }}"
                                    style="text-decoration:none;">
                                    <i class="fa fa-repeat"></i>
                                </a> 
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <br>

                        <div class="table-responsive" style="border:0">
                            <input type="hidden" name="multi_action" value="" />
                            <table class="table table-advance"  id="table1" >
                                <thead>
                                    <tr>
                                        {{-- <th style="width:18px"> <input type="checkbox" name="mult_change" id="mult_change" /></th> --}}
                                        <th>Department</th>
                                        <th>Date Created</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @if(isset($arr_categories) && sizeof($arr_categories)>0)
                                        @foreach($arr_categories as $arr_category)
                                        <tr>
                                            {{-- <td>
                                                <input type="checkbox" name="checked_record[]" value="{{ isset($arr_category['user_id']) ? base64_encode($arr_category['user_id']) : '' }}" /> 
                                            </td> --}}

                                            <td> {{ isset($arr_category['title'])?$arr_category['title']:''  }} </td>

                                            <td> {{ isset($arr_category['created_at']) ? date('D, jS M Y h:i a', strtotime($arr_category['created_at'])) : '' }} </td>
                                            <td>
                                                @if(isset($arr_category['get_tickets']) && empty($arr_category['get_tickets']))
                                                <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" href="javascript:void(0)" title="Delete" onclick="javascript:return confirm_delete('{{ $module_url_path.'/delete/'.base64_encode($arr_category['id'])}}')">
                                                <i class="fa fa-trash" ></i>
                                                </a>
                                                @else
                                                <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip disabled" href="javascript:void(0)" title="Delete">
                                                <i class="fa fa-trash" ></i>
                                                </a>
                                                @endif
                                                <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" href="{{ $module_url_path.'/edit/'.base64_encode($arr_category['id']) }}" title="Edit">
                                                <i class="fa fa-pencil" ></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>

                    </div>{{-- Tabable --}}
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Main Content -->
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
   function confirm_delete(url) {
      alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
          if (e) {
              if(url != "" && url != undefined){
                showProcessingOverlay();
                window.location.href=url;
              }
              return true;
          } else {
              return false;
          }
      });
   }
   function check_multi_action(frm_id,action){
     var frm_ref = jQuery("#"+frm_id);
     if(jQuery(frm_ref).length && action!=undefined && action!=""){
       var checked_record = jQuery('input[name="checked_record[]"]:checked');
       if (jQuery(checked_record).size() < 1){
          alertify.alert('please select at least one record.');
          return false;
       }
       if(action == 'delete'){
          if (!confirm_delete()){
              return false;
          }
       }
       /* Get hidden input reference */
       var input_multi_action = jQuery('input[name="multi_action"]');
       if(jQuery(input_multi_action).length){
         /* Set Action in hidden input*/
         jQuery('input[name="multi_action"]').val(action);
         /*Submit the referenced form */
         jQuery(frm_ref)[0].submit();
       } else {
         console.warn("Required Hidden Input[name]: multi_action Missing in Form ")
       }
     } else {
         console.warn("Required Form[id]: "+frm_id+" Missing in Current Page ")
     }
   }
</script>
@stop