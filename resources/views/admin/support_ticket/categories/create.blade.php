@extends('admin.layout.master')            
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
        </li>
        <span class="divider">
            <i class="fa fa-angle-right"></i>
            <i class="fa fa-users"></i>
            <a href="{{ $module_url_path }}">Manage&nbsp;{{ isset($module_title) ? str_plural($module_title):"" }}</a>
        </span> 
        <span class="divider">
            <i class="fa fa-angle-right"></i>
            <i class="fa fa-plus-circle"></i>
        </span>
        <li class="active">{{ $page_title or ''}}</li>
    </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3>
                    <i class="fa fa-plus-circle"></i>
                    {{ isset($page_title)?$page_title:"" }}
                </h3>
                <div class="box-tool">
                    <a data-action="collapse" href="#"></a>
                    <a data-action="close" href="#"></a>
                </div>
            </div>
            <div class="box-content">
                @include('admin.layout._operation_status')  
                <form name="validation-form" id="validation-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/store" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label" for="email">Title<i class="red">*</i></label>
                        <div class="col-sm-6 col-lg-4 controls">
                            <input type="text" name="title" class="form-control" value="{{old('title')}}" data-rule-required="true" placeholder="Title">
                            <span class='error'>{{ $errors->first('title') }}</span>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                            <input type="submit" value="Save" class="btn btn btn-primary">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Main Content -->
<script type="text/javascript">

$(document).ready(function() {
    
    $.validator.addMethod("pwcheck", function(value, element) {
        return /^^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/.test(value);
    });


    $("#validation-form").validate({
      errorElement: 'span',
       rules: {
      password: 
               {
                 required: true,
                 pwcheck: true,
                 minlength: 8,
                 
               },
         },
   messages: 
     {
     password:
           {
             required: "This field is required.",
             pwcheck: "Password must have uppercase,lowercase and one digit.",
             minlength:"Password required minumum 8 characters.",
   
           },
       }
    });
   });
     
</script>
@stop