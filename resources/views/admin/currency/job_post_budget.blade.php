@extends('admin.layout.master')
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
    <div>
    </div>
</div>
<!-- END Page Title -->

<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
        </li>
        <span class="divider">
            <i class="fa fa-angle-right"></i>
            <i class="fa fa-desktop"></i>
            <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
        </span> 
        <span class="divider">
            <i class="fa fa-angle-right"></i>
            <i class="fa fa-list"></i>
        </span>
        <li class="active">{{ $page_title or ''}}</li>
    </ul>
</div>
<!-- END Breadcrumb -->

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3>
                    <i class="fa fa-text-width"></i>
                    {{ isset($page_title)?$page_title:"" }}
                </h3>
                <div class="box-tool">
                    <a data-action="collapse" href="#"></a>
                    <a data-action="close" href="#"></a>
                </div>
            </div>
            <div class="box-content">
                @include('admin.layout._operation_status') 
                <div class="tabbable">
                    {{-- Fixed price Form Start --}}
                    <form name="validation-form" id="validation-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/store_job_post_budget/{{ base64_encode($arr_currency['id']) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <h4 class="lead">Fixed Price Budget</h4>

                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label">Tiny<i class="red">*</i></label>
                            <div class="col-sm-3 col-lg-2 controls">
                                <div class="input-group">
                                    <span class="input-group-addon">{{ $arr_currency['currency'] or '' }}</span>
                                    <input type="text" name="tiny_from" value="{{ $arr_currency['job_post_budget']['tiny_from'] or '' }}" class="form-control" data-rule-required="true" data-rule-digits="true" data-rule-min="0" placeholder="From">
                                </div>
                                <span class='error'>{{ $errors->first('tiny_from') }}</span>
                            </div>
                            <div class="col-sm-3 col-lg-2 controls">
                                <div class="input-group">
                                    <span class="input-group-addon">{{ $arr_currency['currency'] or '' }}</span>
                                    <input type="text" name="tiny_to" value="{{ $arr_currency['job_post_budget']['tiny_to'] or '' }}" class="form-control" data-rule-required="true" data-rule-digits="true" data-rule-min="0" placeholder="To">
                                </div>
                                <span class='error'>{{ $errors->first('tiny_to') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label">Small<i class="red">*</i></label>
                            <div class="col-sm-3 col-lg-2 controls">
                                <div class="input-group">
                                    <span class="input-group-addon">{{ $arr_currency['currency'] or '' }}</span>
                                    <input type="text" name="small_from" value="{{ $arr_currency['job_post_budget']['small_from'] or '' }}" class="form-control" data-rule-required="true" data-rule-digits="true" data-rule-min="0" placeholder="From">
                                </div>
                                <span class='error'>{{ $errors->first('small_from') }}</span>
                            </div>
                            <div class="col-sm-3 col-lg-2 controls">
                                <div class="input-group">
                                    <span class="input-group-addon">{{ $arr_currency['currency'] or '' }}</span>
                                    <input type="text" name="small_to" value="{{ $arr_currency['job_post_budget']['small_to'] or '' }}" class="form-control" data-rule-required="true" data-rule-digits="true" data-rule-min="0" placeholder="To">
                                </div>
                                <span class='error'>{{ $errors->first('small_to') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label">Medium<i class="red">*</i></label>
                            <div class="col-sm-3 col-lg-2 controls">
                                <div class="input-group">
                                    <span class="input-group-addon">{{ $arr_currency['currency'] or '' }}</span>
                                    <input type="text" name="medium_from" value="{{ $arr_currency['job_post_budget']['medium_from'] or '' }}" class="form-control" data-rule-required="true" data-rule-digits="true" data-rule-min="0" placeholder="From">
                                </div>
                                <span class='error'>{{ $errors->first('medium_from') }}</span>
                            </div>
                            <div class="col-sm-3 col-lg-2 controls">
                                <div class="input-group">
                                    <span class="input-group-addon">{{ $arr_currency['currency'] or '' }}</span>
                                    <input type="text" name="medium_to" value="{{ $arr_currency['job_post_budget']['medium_to'] or '' }}" class="form-control" data-rule-required="true" data-rule-digits="true" data-rule-min="0" placeholder="To">
                                </div>
                                <span class='error'>{{ $errors->first('medium_to') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label">Large<i class="red">*</i></label>
                            <div class="col-sm-3 col-lg-2 controls">
                                <div class="input-group">
                                    <span class="input-group-addon">{{ $arr_currency['currency'] or '' }}</span>
                                    <input type="text" name="large_from" value="{{ $arr_currency['job_post_budget']['large_from'] or '' }}" class="form-control" data-rule-required="true" data-rule-digits="true" data-rule-min="0" placeholder="From">
                                </div>
                                <span class='error'>{{ $errors->first('large_from') }}</span>
                            </div>
                            <div class="col-sm-3 col-lg-2 controls">
                                <div class="input-group">
                                    <span class="input-group-addon">{{ $arr_currency['currency'] or '' }}</span>
                                    <input type="text" name="large_to" value="{{ $arr_currency['job_post_budget']['large_to'] or '' }}" class="form-control" data-rule-required="true" data-rule-digits="true" data-rule-min="0" placeholder="To">
                                </div>
                                <span class='error'>{{ $errors->first('large_to') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label">Big<i class="red">*</i></label>
                            <div class="col-sm-3 col-lg-2 controls">
                                <div class="input-group">
                                    <span class="input-group-addon">{{ $arr_currency['currency'] or '' }}</span>
                                    <input type="text" name="big_from" value="{{ $arr_currency['job_post_budget']['big_from'] or '' }}" class="form-control" data-rule-required="true" data-rule-digits="true" data-rule-min="0" placeholder="From">
                                </div>
                                <span class='error'>{{ $errors->first('big_from') }}</span>
                            </div>
                            <div class="col-sm-3 col-lg-2 controls">
                                <div class="input-group">
                                    <span class="input-group-addon">{{ $arr_currency['currency'] or '' }}</span>
                                    <input type="text" name="big_to" value="{{ $arr_currency['job_post_budget']['big_to'] or '' }}" class="form-control" data-rule-required="true" data-rule-digits="true" data-rule-min="0" placeholder="To">
                                </div>
                                <span class='error'>{{ $errors->first('big_to') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label">Jumbo<i class="red">*</i></label>
                            <div class="col-sm-3 col-lg-2 controls">
                                <div class="input-group">
                                    <span class="input-group-addon">{{ $arr_currency['currency'] or '' }}</span>
                                    <input type="text" name="jumbo_from" value="{{ $arr_currency['job_post_budget']['jumbo_from'] or '' }}" class="form-control" data-rule-required="true" data-rule-digits="true" data-rule-min="0" placeholder="From">
                                </div>
                                <span class='error'>{{ $errors->first('jumbo_from') }}</span>
                            </div>
                        </div>

                        <br>

                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                <input type="submit" value="Save" class="btn btn btn-primary">
                            </div>
                        </div>
                    </form>

                    {{-- Fixed price Form End --}}

                    <hr>

                    {{-- Hourly price Form Start --}}

                    <?php
                        $arr_from = $arr_to = [];
                        $hourly_price = $arr_currency['job_post_budget']['hourly_price'];
                        $arr_hourly_price = unserialize($arr_currency['job_post_budget']['hourly_price']);

                        if(!empty($arr_hourly_price)){
                            $arr_from = array_keys($arr_hourly_price);
                            $arr_to = array_values($arr_hourly_price);
                        }

                    ?>

                    <form id="hourly-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/store_job_post_hourly_budget/{{ base64_encode($arr_currency['id']) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <h4 class="lead">Hourly Price Budget</h4>

                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label"></label>
                            <div class="col-sm-3 col-lg-2 controls">
                                <div class="input-group">
                                    <span class="input-group-addon">{{ $arr_currency['currency'] or '' }}</span>
                                    <input type="text" name="from[]" value="{{ $arr_from[0] or '' }}" class="form-control" data-rule-required="true" data-rule-digits="true" data-rule-min="0" placeholder="From">
                                </div>
                                <span class='error'>{{ $errors->first('tiny_from') }}</span>
                            </div>
                            <div class="col-sm-3 col-lg-2 controls">
                                <div class="input-group">
                                    <span class="input-group-addon">{{ $arr_currency['currency'] or '' }}</span>
                                    <input type="text" name="to[]" value="{{ $arr_to[0] or '' }}" class="form-control" data-rule-required="true" data-rule-digits="true" data-rule-min="0" placeholder="To">
                                </div>
                                <span class='error'>{{ $errors->first('tiny_to') }}</span>
                            </div>
                            <label>/ Hour</label>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label"></label>
                            <div class="col-sm-3 col-lg-2 controls">
                                <div class="input-group">
                                    <span class="input-group-addon">{{ $arr_currency['currency'] or '' }}</span>
                                    <input type="text" name="from[]" value="{{ $arr_from[1] or '' }}" class="form-control" data-rule-required="true" data-rule-digits="true" data-rule-min="0" placeholder="From">
                                </div>
                                <span class='error'>{{ $errors->first('tiny_from') }}</span>
                            </div>
                            <div class="col-sm-3 col-lg-2 controls">
                                <div class="input-group">
                                    <span class="input-group-addon">{{ $arr_currency['currency'] or '' }}</span>
                                    <input type="text" name="to[]" value="{{ $arr_to[1] or '' }}" class="form-control" data-rule-required="true" data-rule-digits="true" data-rule-min="0" placeholder="To">
                                </div>
                                <span class='error'>{{ $errors->first('tiny_to') }}</span>
                            </div>
                            <label>/ Hour</label>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label"></label>
                            <div class="col-sm-3 col-lg-2 controls">
                                <div class="input-group">
                                    <span class="input-group-addon">{{ $arr_currency['currency'] or '' }}</span>
                                    <input type="text" name="from[]" value="{{ $arr_from[2] or '' }}" class="form-control" data-rule-required="true" data-rule-digits="true" data-rule-min="0" placeholder="From">
                                </div>
                                <span class='error'>{{ $errors->first('tiny_from') }}</span>
                            </div>
                            <div class="col-sm-3 col-lg-2 controls">
                                <div class="input-group">
                                    <span class="input-group-addon">{{ $arr_currency['currency'] or '' }}</span>
                                    <input type="text" name="to[]" value="{{ $arr_to[2] or '' }}" class="form-control" data-rule-required="true" data-rule-digits="true" data-rule-min="0" placeholder="To">
                                </div>
                                <span class='error'>{{ $errors->first('tiny_to') }}</span>
                            </div>
                            <label>/ Hour</label>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label"></label>
                            <div class="col-sm-3 col-lg-2 controls">
                                <div class="input-group">
                                    <span class="input-group-addon">{{ $arr_currency['currency'] or '' }}</span>
                                    <input type="text" name="from[]" value="{{ $arr_from[3] or '' }}" class="form-control" data-rule-required="true" data-rule-digits="true" data-rule-min="0" placeholder="From">
                                    <input type="hidden" name="to[]" value="{{ $arr_to[3] or '0' }}">
                                </div>
                                <span class='error'>{{ $errors->first('tiny_from') }}</span>
                            </div>
                            <label>/ Hour</label>
                        </div>

                        <br>
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                <input type="submit" value="Save" class="btn btn btn-primary">
                            </div>
                        </div>

                    </form>

                    {{-- Hourly price Form Closed --}}

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function(){

        $("#validation-form").validate(function() {
            /*errorPlacement : function (error, element) {
                error.insertAfter( element.parent("div"));
            }*/

        });

        $("#hourly-form").validate(function() {
            /*errorPlacement : function (error, element) {
                error.insertAfter( element.parent("div"));
            }*/

        });

    });

</script>
@stop