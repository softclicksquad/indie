@extends('admin.layout.master')            
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-desktop"></i>
      <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-list"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-text-width"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
            @include('admin.layout._operation_status')  
            <form name="validation-form" id="validation-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/update/{{$enc_id}}" enctype="multipart/form-data">
               {{ csrf_field() }}
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="page_title">Quantity<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">

                     <select name="quantity" class="form-control" data-rule-required="true">
                        <option value="">--- Select Quantity ---</option>
                        @if(isset($arr_quantity) && sizeof($arr_quantity)>0)
                           @foreach($arr_quantity as $quantity)
                              <option value="{{ isset($quantity) ? $quantity : 0 }}"
                              @if(isset($arr_data['quantity']) && isset($quantity) && $arr_data['quantity'] == $quantity )
                              selected="" 
                              @endif
                              >{{ isset($quantity) ? $quantity : 0 }}</option>
                           @endforeach
                        @endif
                     </select>
                     <span class='error'>{{ $errors->first('quantity') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="name">Price in USD ($)<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="price" class="form-control" value="{{ isset($arr_data['price']) ? $arr_data['price'] : 0 }}" data-rule-required="true" data-rule-maxlength="255" data-rule-number='true' placeholder="Price in USD ($)">
                     <span class='error'>{{ $errors->first('price') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                     <input type="submit" value="Update" class="btn btn btn-primary">
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@stop