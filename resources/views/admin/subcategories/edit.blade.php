@extends('admin.layout.master')
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-desktop"></i>
      <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-list"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-text-width"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
            @include('admin.layout._operation_status') 
            <div class="tabbable">
               <form name="validation-form" id="validation-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/update/{{$enc_id}}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <ul  class="nav nav-tabs">
                     @include('admin.layout._multi_lang_tab')
                  </ul>
                  <div id="myTabContent1" class="tab-content">
                     @if(isset($arr_lang) && sizeof($arr_lang)>0)
                     @foreach($arr_lang as $lang)
                     <?php 
                        /* Locale Variable */  
                        $locale_subcategory_title = "";

                        if(isset($arr_subcategory['translations'][$lang['locale']]))
                        {
                        
                            $locale_subcategory_title = isset($arr_subcategory['translations'][$lang['locale']]['subcategory_title'])?$arr_subcategory['translations'][$lang['locale']]['subcategory_title']:'' ;
                        }
                        ?>
                     <div class="tab-pane fade {{ $lang['locale']=='en'?'in active':'' }}"
                        id="{{ $lang['locale'] }}">

                       @if($lang['locale'] == 'en') 
                        <div class="form-group">
                           <label class="col-sm-3 col-lg-2 control-label" for="page_title">Category Name<i class="red">*</i></label>
                           <div class="col-sm-6 col-lg-4 controls">
                              
                              <select name="parent_category" class="form-control" data-rule-required="true">
                                 <option value="">--- Select Category ---</option>
                                 @if(isset($arr_categories) && sizeof($arr_categories)>0)
                                    @foreach($arr_categories as $category)
                                       <option value="{{isset($category['id'])?$category['id']:''}}"
                                       @if(isset($arr_subcategory['category_id']) && $arr_subcategory['category_id']==$category['id'])
                                          selected="true" 
                                       @endif
                                       >{{isset($category['category_title'])?$category['category_title']:''}}</option>
                                    @endforeach
                                 @endif
                              </select>
                              <span class='error'>{{ $errors->first('parent_category') }}</span>
                           </div>
                        </div>
                        @endif

                        <div class="form-group">
                           <label class="col-sm-3 col-lg-2 control-label" for="page_title">Subcategory Title<i class="red">*</i></label>
                           <div class="col-sm-6 col-lg-4 controls">
                              @if($lang['locale'] == 'en') 
                              <input type="text" name="subcategory_title_{{$lang['locale']}}" class="form-control" value="{{isset($locale_subcategory_title)?$locale_subcategory_title:''}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Subcategory Title">
                              @else
                              <input type="text" name="subcategory_title_{{$lang['locale']}}" class="form-control" value="{{isset($locale_subcategory_title)?$locale_subcategory_title:''}}" placeholder="Subcategory Title" data-rule-maxlength="255">
                              @endif
                              <span class='error'>{{ $errors->first('subcategory_title_'.$lang['locale']) }}</span>
                           </div>
                        </div>

                     </div>
                     @endforeach
                     @endif
                  </div>
                  <br>
                  <div class="form-group">
                     <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                        <input type="submit" value="Save" class="btn btn btn-primary">
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- END Main Content -->
@stop
