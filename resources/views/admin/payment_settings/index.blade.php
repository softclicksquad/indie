    @extends('admin.layout.master')                
    @section('main_content')
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{ url('/'.$admin_panel_slug.'/dashboard') }}">Dashboard</a>
            </li>
            <span class="divider">
                <i class="fa fa-angle-right"></i>
            </span>
            
            <li class="active">  {{ isset($page_title)?$page_title:"" }}</li>
        </ul>
    </div>
    <!-- BEGIN Main Content -->
        <div class="row">
        <div class="col-md-12">
            <div class="box box-blue">
                <div class="box-title">
                    <h3><i class="fa fa-money"></i> Payment Settings</h3>
                </div>
                <div class="box-content">
                   @include('admin.layout._operation_status')  
                    <form name="validation-form" id="validation-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/update" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label">Paypal Client ID<span class="red">*</span></label>
                            <div class="col-sm-9 col-lg-4 controls">
                                
                                  <input type="text" class="form-control" name="paypal_client_id" id="paypal_client_id" data-rule-required="true" value="{{isset($payment_details['paypal_client_id'])?$payment_details['paypal_client_id']:''}}" />
                                <span class='error'>{{ $errors->first('paypal_client_id') }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label">Paypal Secret Key<span class="red">*</span></label>
                            <div class="col-sm-9 col-lg-4 controls">
                                 
                                  <input type="text" class="form-control" name="paypal_secret_key" id="paypal_secret_key" data-rule-required="true"  value="{{isset($payment_details['paypal_secret_key'])?$payment_details['paypal_secret_key']:''}}"/>
                                <span class='error'>{{ $errors->first('paypal_secret_key') }}</span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label">Paypal (Payment) Mode<span class="red">*</span></label>
                            <div class="col-sm-9 col-lg-4 controls">
                                 
                                 <select class="form-control" name="paypal_payment_mode" id="paypal_payment_mode" data-rule-number="true">
                                     <option value="">--- Select Payment Mode ---</option>


                                     <option value="1" {{isset($payment_details['paypal_payment_mode']) && $payment_details['paypal_payment_mode']==1?'selected':''}} >Sandbox Mode</option>
                                     <option value="2" {{isset($payment_details['paypal_payment_mode']) && $payment_details['paypal_payment_mode']==2?'selected':''}}>Live Mode</option>
                                 </select>

                                 <span class='error'>{{ $errors->first('paypal_payment_mode') }}</span>
                                
                                  {{-- <input type="text" class="form-control" name="paypal_payment_mode" id="paypal_payment_mode" data-rule-number="true" value="{{isset($payment_details['paypal_payment_mode'])?$payment_details['paypal_payment_mode']:''}}"/> --}}
                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label">Stripe Secret Key<span class="red">*</span></label>
                            <div class="col-sm-9 col-lg-4 controls">
                                
                                  <input type="text" class="form-control" name="stripe_secret_key" id="stripe_secret_key" data-rule-required="true" value="{{isset($payment_details['stripe_secret_key'])?$payment_details['stripe_secret_key']:''}}" />
                                <span class='error'>{{ $errors->first('stripe_secret_key') }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label">Stripe Publishable Key<span class="red">*</span></label>
                            <div class="col-sm-9 col-lg-4 controls">
                                 
                                  <input type="text" class="form-control" name="stripe_publishable_key" id="stripe_publishable_key" data-rule-required="true"  value="{{isset($payment_details['stripe_publishable_key'])?$payment_details['stripe_publishable_key']:''}}"/>
                                <span class='error'>{{ $errors->first('stripe_publishable_key') }}</span>
                            </div>
                        </div>
                        
                       
                        
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3 col-lg-4 col-lg-offset-2">
                                <button type="submit" class="btn btn-primary">Update</button>
                                {{-- <button type="button" class="btn">Cancel</button> --}}
                            </div>
                       </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END Main Content -->
@stop