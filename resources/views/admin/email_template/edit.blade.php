    @extends('admin.layout.master')


    @section('main_content')
    <!-- BEGIN Page Title -->

    <div class="page-title">
      <div>

      </div>
    </div>
    <!-- END Page Title -->

    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="fa fa-home"></i>
          <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
        </li>
        <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-file-text"></i>
          <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
        </span> 
        <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-edit"></i>
        </span>
        <li class="active">{{ $page_title or ''}}</li>
      </ul>
    </div>
    <!-- END Breadcrumb -->


    <!-- BEGIN Main Content -->
    <div class="row">
      <div class="col-md-12">

        <div class="box">
          <div class="box-title">
            <h3>
              <i class="fa fa-edit"></i>
              {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
              <a data-action="collapse" href="#"></a>
              <a data-action="close" href="#"></a>
            </div>
          </div>
          <div class="box-content">

            @include('admin.layout._operation_status') 

            <div class="tabbable">
              <form name="validation-form" id="validation-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/update/{{$enc_id}}" enctype="multipart/form-data"> 
              {{ csrf_field() }}
              <fieldset class="content-group">	
				<div class="row">
					<div class="col-lg-8">
						<div class="form-group">
							<label class="control-label col-sm-4 col-md-4 col-lg-3" for="template_name"> Template Name<i class="red">*</i></label>
							<div class="col-sm-8 col-md-8 col-lg-9">
								<input type="text" name="template_name" value="{{$arr_email_template['template_name'] or ''}}" id="template_name" class="form-control" placeholder="Template Name" data-rule-required="true"  data-rule-maxlength="60" readonly="readonly">
								<span class="error">{{ $errors->first('template_name') }} </span>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-8">
						<div class="form-group">
							<label class="control-label col-sm-4 col-md-4 col-lg-3" for="template_from"> Template From<i class="red">*</i></label>
							<div class="col-sm-8 col-md-8 col-lg-9">
								<input type="text" name="template_from" value="{{$arr_email_template['template_from'] or ''}}" id="template_from" class="form-control" placeholder="Template From" data-rule-required="true"  data-rule-maxlength="60" value="">
								<span class="error">{{ $errors->first('template_from') }} </span>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-8">
						<div class="form-group">
							<label class="control-label col-sm-4 col-md-4 col-lg-3" for="template_from_mail"> Template From Email<i class="red">*</i></label>
							<div class="col-sm-8 col-md-8 col-lg-9">
								<input type="text" name="template_from_mail" value="{{$arr_email_template['template_from_mail'] or ''}}" id="template_from_mail" class="form-control" placeholder="Template From Email" data-rule-required="true"  data-rule-email="true" value="">
								<span class="error">{{ $errors->first('template_from_mail') }} </span>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-8">
						<div class="form-group">
							<label class="control-label col-sm-4 col-md-4 col-lg-3" for="template_subject"> Template Subject<i class="red">*</i></label>
							<div class="col-sm-8 col-md-8 col-lg-9">
								<input type="text" name="template_subject" value="{{$arr_email_template['template_subject'] or ''}}" id="template_subject" class="form-control" placeholder="Template Subject" data-rule-required="true"  data-rule-maxlength="60" value="">
								<span class="error">{{ $errors->first('template_subject') }} </span>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-8">
						<div class="form-group">
							<label class="control-label col-sm-4 col-md-4 col-lg-3" for="template_html"> Template Body<i class="red">*</i></label>
							<div class="col-sm-8 col-md-8 col-lg-9">
								<textarea  name="template_html" id="template_html" class="form-control" data-rule-required="true" rows="8">{{$arr_email_template['template_html'] or ''}}</textarea>
								<span class="error err_email_content">{{ $errors->first('template_html') }} </span>

								@if(isset($arr_variables) && sizeof($arr_variables)>0 && !empty($arr_variables))
								<br>
								<div class="note"><b>Note : </b>Please don't change the following variables in the email template body.</div>
								<br>
								<span> Variables: </span>

								@foreach($arr_variables as $variable)
								<br> <label> {{ $variable }} </label> 
								@endforeach
								@endif
							</div>
						</div>
					</div>
				</div>	
				<div class="form-group text-right">
					<div class="col-lg-8">
						<button type="submit" class="btn blue-btn" id="btn_update_email_template">Update</button>
						{{-- <a href="javascript:void(0)" name="preview" id="preview" class="btn back-btn"><i class="fa fa-eye"></i> Preview</a> --}}
					</div>
				</div>
				</fieldset>
             	

            <br>
           {{--  <div class="form-group">
              <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
               <input type="submit" value="Save" class="btn btn btn-primary">
              </div>
            </div> --}}
            </form>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- END Main Content -->
<script src="{{url('assets\tinymce\js\tinymce/jquery.tinymce.min.js')}}"></script>
<script src="{{url('assets\tinymce\js\tinymce/tinymce.min.js')}}"></script>

  <script type="text/javascript">

    function saveTinyMceContent()
    {
      tinyMCE.triggerSave();
    }

    
    $(document).ready(function()
    {
      tinymce.init({
        selector: 'textarea',
        height:350,
        plugins: [
        'advlist autolink lists link charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime table contextmenu paste code'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
        content_css: [
        '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
        '//www.tinymce.com/css/codepen.min.css'
        ]
      });  
    });
  </script>

  @stop
