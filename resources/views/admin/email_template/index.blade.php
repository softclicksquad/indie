@extends('admin.layout.master')    
@section('main_content')

<!-- Page header -->
{{-- @include('admin.layout.breadcrumb')   --}}
<!-- /page header -->

<!-- BEGIN Main Content -->

<!-- Content area -->
<div class="content">

		@include('admin.layout._operation_status')
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">{{$module_title or ''}}</h5>
				{{-- <div class="heading-elements">
					<ul class="icons-list">
					</ul>
				</div> --}}
			</div>
			<hr class="horizotal-line">
			<form name="frm_manage" id="frm_manage" method="POST" class="form-horizontal" action="{{url($module_url_path)}}/multi_action">
				{{ csrf_field() }}
				<input type="hidden" name="multi_action" value="" />
				<div class="table-responsive">
					<table class="table dataTable no-footer custome-table" id="table1">
						<thead>
							<tr class="border-solid">
								<th>Name </th>
								<th>Subject</th>
								<th>From</th>
								<th>From Email</th>
								<th align="center">Actions</th>
							</tr>
						</thead>
						<tbody>
                                @if(isset($arr_template) && sizeof($arr_template)>0)
                                @foreach($arr_template as $data)
                                <tr>
                                    
                                    <td style="vertical-align: unset;"> {{ isset($data['template_name'])?$data['template_name']:'' }} </td>
                                    <td style="vertical-align: unset;"> {{ isset($data['template_subject'])?$data['template_subject']:'' }} </td>

                                    <td style="vertical-align: unset;"> {{ isset($data['template_from'])?$data['template_from']:'' }} </td>

                                    <td style="vertical-align: unset;"> {{ isset($data['template_from_mail'])?$data['template_from_mail']:'' }} </td>

                                    <td style="vertical-align: unset;"> 
                                        <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" href="{{ $module_url_path.'/edit/'.base64_encode($data['id']) }}"  title="Edit">
                                            <i class="fa fa-edit" ></i>
                                        </a>

                                        <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" href="javascript:void(0)" 
                                            onclick="javascript:return confirm_delete('{{ $module_url_path.'/delete/'.base64_encode($data['id'])}}')"  title="Delete">
                                            <i class="fa fa-trash" ></i>
                                        </a>
                                        &nbsp;
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                        </tbody>
					</table>
				</div>
			</form>
		</div>
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
   function confirm_delete(url) {
      alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
          if (e) {
              if(url != "" && url != undefined){
                showProcessingOverlay();
                window.location.href=url;
              }
              return true;
          } else {
              return false;
          }
      });
   }
</script>
@endsection


