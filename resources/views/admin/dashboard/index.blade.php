@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<?php
$user = Sentinel::check();
if(!$user)
{
    return redirect('/admin/login'); 
}   
else
{
    $user_role = get_user_role($user->id);
    $arr_permissions = isset($user['permissions']) ? $user['permissions'] : '';
} 

?>
<!-- BEGIN Content -->
<div id="main-content">
<!-- BEGIN Page Title -->
<div class="page-title">
    <div>
        <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
    </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li class="active"><i class="fa fa-home"></i> Home</li>
    </ul>
</div>
<!-- END Breadcrumb -->
    <!-- BEGIN Tiles -->
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    @if(isset($user_role) && $user_role == 'admin')
                        <a href="{{url('/')}}/admin/projects/open">
                            <div class="col-md-6">
                                <div class="tile tile-violet">
                                    <div class="img">
                                        <i class="fa fa-folder-open-o fa-lg"></i>
                                    </div>
                                    <div class="content">
                                        <p class="big">{{ isset($arr_projects['2']) ? count($arr_projects['2']):'0' }}</p>
                                        <p class="title">Open Projects</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="{{url('/')}}/admin/projects/ongoing">
                            <div class="col-md-6">
                                <div class="tile tile-magenta">
                                    <div class="img">
                                        <i class="fa fa-gear fa-lg"></i>
                                    </div>
                                    <div class="content">
                                        <p class="big">{{ isset($arr_projects['4']) ? count($arr_projects['4']):'0' }}</p>
                                        <p class="title">Ongoing Projects</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="{{url('/')}}/admin/projects/all">
                            <div class="col-md-6">
                                <div class="tile tile-red">
                                    <div class="img">
                                        <i class="fa fa-gears fa-lg"></i>
                                    </div>
                                    <div class="content">
                                        <p class="big">{{ isset($all_projects_count) ? $all_projects_count:'0' }}</p>
                                        <p class="title">All Projects</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @elseif(isset($user_role) && $user_role == 'subadmin' && array_key_exists('projects',$arr_permissions))
                        <a href="{{url('/')}}/admin/projects/open">
                            <div class="col-md-6">
                                <div class="tile tile-violet">
                                    <div class="img">
                                        <i class="fa fa-folder-open-o fa-lg"></i>
                                    </div>
                                    <div class="content">
                                        <p class="big">{{ isset($arr_projects['2']) ? count($arr_projects['2']):'0' }}</p>
                                        <p class="title">Open Projects</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="{{url('/')}}/admin/projects/ongoing">
                            <div class="col-md-6">
                                <div class="tile tile-magenta">
                                    <div class="img">
                                        <i class="fa fa-gear fa-lg"></i>
                                    </div>
                                    <div class="content">
                                        <p class="big">{{ isset($arr_projects['4']) ? count($arr_projects['4']):'0' }}</p>
                                        <p class="title">Ongoing Projects</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="{{url('/')}}/admin/projects/all">
                            <div class="col-md-6">
                                <div class="tile tile-red">
                                    <div class="img">
                                        <i class="fa fa-gears fa-lg"></i>
                                    </div>
                                    <div class="content">
                                        <p class="big">{{ isset($all_projects_count) ? $all_projects_count:'0' }}</p>
                                        <p class="title">All Projects</p>
                                    </div>
                                </div>
                            </div>
                        </a>                        
                    @endif

                    @if(isset($user_role) && $user_role == 'admin')
                        <a href="{{url('/')}}/admin/projects/posted">
                            <div class="col-md-6">
                                <div class="tile tile-pink">
                                    <div class="img">
                                        <i class="fa fa-hand-o-right fa-lg"></i>
                                    </div>
                                    <div class="content">
                                        <p class="big">{{ isset($arr_projects['1']) ? count($arr_projects['1']):'0' }}</p>
                                        <p class="title">Assign Project Manager</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @elseif(isset($user_role) && $user_role == 'subadmin' && array_key_exists('project_manager',$arr_permissions) && array_key_exists('projects',$arr_permissions))
                        <a href="{{url('/')}}/admin/projects/posted">
                            <div class="col-md-6">
                                <div class="tile tile-pink">
                                    <div class="img">
                                        <i class="fa fa-hand-o-right fa-lg"></i>
                                    </div>
                                    <div class="content">
                                        <p class="big">{{ isset($arr_projects['1']) ? count($arr_projects['1']):'0' }}</p>
                                        <p class="title">Assign Project Manager</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endif
                    
                    @if(isset($user_role) && $user_role == 'admin')
                        <a href="{{url('/')}}/admin/clients">
                        <div class="col-md-6">
                            <div class="tile tile-blue">
                                <div class="img">
                                    <i class="fa  fa-users fa-lg"></i>
                                </div>
                                <div class="content">
                                    <p class="big">{{ isset($client_count) ? $client_count:'0' }}</p>
                                    <p class="title">Clients</p>
                                </div>
                            </div>
                        </div>
                        </a>
                    @elseif(isset($user_role) && $user_role == 'subadmin' && array_key_exists('clients',$arr_permissions))
                        <a href="{{url('/')}}/admin/clients">
                        <div class="col-md-6">
                            <div class="tile tile-blue">
                                <div class="img">
                                    <i class="fa  fa-users fa-lg"></i>
                                </div>
                                <div class="content">
                                    <p class="big">{{ isset($client_count) ? $client_count:'0' }}</p>
                                    <p class="title">Clients</p>
                                </div>
                            </div>
                        </div>
                        </a>
                    @endif

                    @if(isset($user_role) && $user_role == 'admin')
                        <a href="{{url('/')}}/admin/experts">
                            <div class="col-md-6">
                                <div class="tile tile-dark-blue">
                                    <div class="img">
                                        <i class="fa fa-users fa-lg"></i>
                                    </div>
                                    <div class="content">
                                        <p class="big">{{ isset($experts_count) ? $experts_count:'0' }}</p>
                                        <p class="title">Experts</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @elseif(isset($user_role) && $user_role == 'subadmin' && array_key_exists('experts',$arr_permissions))
                        <a href="{{url('/')}}/admin/experts">
                            <div class="col-md-6">
                                <div class="tile tile-dark-blue">
                                    <div class="img">
                                        <i class="fa fa-users fa-lg"></i>
                                    </div>
                                    <div class="content">
                                        <p class="big">{{ isset($experts_count) ? $experts_count:'0' }}</p>
                                        <p class="title">Experts</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    @if(isset($user_role) && $user_role == 'admin')
                        <a href="{{url('/')}}/admin/projects/completed">
                            <div class="col-md-6">
                                <div class="tile tile-lime">
                                    <div class="img">
                                        <i class="fa fa-check fa-lg"></i>
                                    </div>
                                    <div class="content">
                                        <p class="big">{{ isset($arr_projects['3']) ? count($arr_projects['3']):'0' }}</p>
                                        <p class="title">Completed Projects</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="{{url('/')}}/admin/projects/canceled">
                            <div class="col-md-6">
                                <div class="tile tile-red">
                                    <div class="img">
                                        <i class="fa fa-times fa-lg" ></i>
                                    </div>
                                    <div class="content">
                                        <p class="big">{{ isset($arr_projects['5']) ? count($arr_projects['5']):'0' }}</p>
                                        <p class="title">Cancelled Projects</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @elseif(isset($user_role) && $user_role == 'subadmin' && array_key_exists('projects',$arr_permissions))
                        <a href="{{url('/')}}/admin/projects/completed">
                            <div class="col-md-6">
                                <div class="tile tile-lime">
                                    <div class="img">
                                        <i class="fa fa-check fa-lg"></i>
                                    </div>
                                    <div class="content">
                                        <p class="big">{{ isset($arr_projects['3']) ? count($arr_projects['3']):'0' }}</p>
                                        <p class="title">Completed Projects</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="{{url('/')}}/admin/projects/canceled">
                            <div class="col-md-6">
                                <div class="tile tile-red">
                                    <div class="img">
                                        <i class="fa fa-times fa-lg" ></i>
                                    </div>
                                    <div class="content">
                                        <p class="big">{{ isset($arr_projects['5']) ? count($arr_projects['5']):'0' }}</p>
                                        <p class="title">Cancelled Projects</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endif

                    @if(isset($user_role) && $user_role == 'admin')
                        <a href="{{url('/')}}/admin/categories">
                            <div class="col-md-6">
                                <div class="tile tile-gray">
                                    <div class="img">
                                        <i class="fa fa-tasks fa-lg"></i>
                                    </div>
                                    <div class="content">
                                        <p class="big">{{ isset($project_category_count) ? $project_category_count:'0' }}</p>
                                        <p class="title">Project Categories</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @elseif(isset($user_role) && $user_role == 'subadmin' && array_key_exists('categories',$arr_permissions))
                        <a href="{{url('/')}}/admin/categories">
                            <div class="col-md-6">
                                <div class="tile tile-gray">
                                    <div class="img">
                                        <i class="fa fa-tasks fa-lg"></i>
                                    </div>
                                    <div class="content">
                                        <p class="big">{{ isset($project_category_count) ? $project_category_count:'0' }}</p>
                                        <p class="title">Project Categories</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endif


                    @if(isset($user_role) && $user_role == 'admin')
                        <a href="{{url('/')}}/admin/subscription_packs">
                        <div class="col-md-6">
                            <div class="tile tile-blue">
                                <div class="img">
                                    <i class="fa fa-suitcase fa-lg"></i>
                                </div>
                                <div class="content">
                                    <p class="big">{{ isset($subscription_packs_count) ? $subscription_packs_count:'0' }}</p>
                                    <p class="title">Subscriptions</p>
                                </div>
                            </div>
                        </div>
                        </a>
                    @endif

                    @if(isset($user_role) && $user_role == 'admin')
                        <a href="{{url('/')}}/admin/project_manager">
                            <div class="col-md-6">
                                <div class="tile tile-magenta">
                                    <div class="img">
                                        <i class="fa fa-users fa-lg"></i>
                                    </div>
                                    <div class="content">
                                        <p class="big">{{ isset($project_manager_count) ? $project_manager_count:'0' }}</p>
                                        <p class="title">Project Managers</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @elseif(isset($user_role) && $user_role == 'subadmin' && array_key_exists('project_manager',$arr_permissions))
                        <a href="{{url('/')}}/admin/project_manager">
                            <div class="col-md-6">
                                <div class="tile tile-magenta">
                                    <div class="img">
                                        <i class="fa fa-users fa-lg"></i>
                                    </div>
                                    <div class="content">
                                        <p class="big">{{ isset($project_manager_count) ? $project_manager_count:'0' }}</p>
                                        <p class="title">Project Managers</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endif


                    @if(isset($user_role) && $user_role == 'admin')
                        <a href="{{url('/')}}/admin/subadmins">
                        <div class="col-md-6">
                            <div class="tile tile-orange">
                                <div class="img">
                                    <i class="fa fa-users fa-lg"></i>
                                </div>
                                <div class="content">
                                    <p class="big">{{ isset($subadmin_count) ? $subadmin_count:'0' }}</p>
                                    <p class="title">Subadmins</p>
                                </div>
                            </div>
                        </div>
                        </a>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
              <div class="col-md-6">
                <div class="row">
                    @if(isset($user_role) && $user_role == 'admin')
                        <a href="{{url('/')}}/admin/contact_inquiries">
                            <div class="col-md-6">
                                <div class="tile tile-dark-blue">
                                    <div class="img">
                                        <i class="fa fa-envelope-square"></i>
                                    </div>
                                    <div class="content">
                                        <p class="big">{{ isset($contact_enquiry_count)?$contact_enquiry_count:"0" }}</p>
                                        <p class="title">Contact Inquiries</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @elseif(isset($user_role) && $user_role == 'subadmin' && array_key_exists('contact_inquiries',$arr_permissions))    
                        <a href="{{url('/')}}/admin/contact_inquiries">
                            <div class="col-md-6">
                                <div class="tile tile-dark-blue">
                                    <div class="img">
                                        <i class="fa fa-envelope-square"></i>
                                    </div>
                                    <div class="content">
                                        <p class="big">{{ isset($contact_enquiry_count)?$contact_enquiry_count:"0" }}</p>
                                        <p class="title">Contact Inquiries</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endif
                    <!-- <a href="javascript:void(0)">
                        <div class="col-md-6">
                            <div class="tile tile-violet">
                                <div class="img">
                                    <i class="fa fa-money"></i>
                                </div>
                                <div class="content">
                                    <p class="big">₹ {{ isset($arr_milestone['total_milestone'])?$arr_milestone['total_milestone']:"0" }}</p>
                                    <p class="title">Total Milestone</p>
                                </div>
                            </div>
                        </div>
                    </a> -->
                </div>    
            </div>
            <!-- <div class="col-md-6">
                <div class="row">
                    <a href="javascript:void(0)">
                        <div class="col-md-6">
                            <div class="tile tile-magenta">
                                <div class="img">
                                    <i class="fa fa-money"></i>
                                </div>
                                <div class="content">
                                    <p class="big">₹ {{ isset($arr_milestone['total_milestone_year'])?$arr_milestone['total_milestone_year']:"0" }}</p>
                                    <p class="title">Total Milestone {{date('Y')}}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="javascript:void(0)">
                        <div class="col-md-6">
                            <div class="tile tile-lime">
                                <div class="img">
                                    <i class="fa fa-money"></i>
                                </div>
                                <div class="content">
                                    <p class="big">₹ {{ isset($arr_milestone['total_milestone_month'])?$arr_milestone['total_milestone_month']:"0" }}</p>
                                    <p class="title">Total Milestone {{date('M Y')}}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>    
            </div> -->
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-content">
                        <div id="chartContainer" style="height: 400px; width: 100%;"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                 <div class="box">
                     <div class="box-content">
                         <?php echo date('F Y',strtotime(date('Y-m-d H:i:s'))); ?> 
                         <div id="registers-chart" style="margin-top:20px; position:relative; height: 380px;"></div>
                     </div>
                 </div>
             </div>  
        </div>
    <!-- END Tiles -->
</div>
<!-- END Content -->
<script src="{{url('/public')}}/assets/flot/canvasjs.min.js"></script>
<script type="text/javascript">
//graph
$(function() {
    //---------------------------- Dashboard project status Pie Chart -------------------------//
    window.onload = function () {
         CanvasJS.addColorSet("greenShades",
                [//colorSet Array
                "#e79712",
                "#009dc6",
                "#65b11d",
                "#8b4787",
                "#b9231f",            
                ]);
        var chart = new CanvasJS.Chart("chartContainer",{
            title:{
               /* text: "Pie Chart"*/
            },
            colorSet: "greenShades",
            data: [{
                type: "pie",
                showInLegend: true,
                toolTipContent: "{legendText}: <strong>{y}%</strong>",
                indexLabel: "{label} {y}%",
                dataPoints: [
                <?php if(isset($arr_data) && sizeof($arr_data)>0)
                {
                    foreach ($arr_data as  $project) 
                    {
                     ?>
                     <?php echo isset($project)?$project:"";?>
                    <?php 
                    } 
                }
                ?>
                ]
            }
            ]
        });
       chart.render();
    }
    //---------------------------- Dashboard project status Pie Chart -------------------------//
});
</script>
<script>
$(function() {
    /*---------------------------- Dashboard Registers Chart in current month -------------------------*/
      if (jQuery.plot) {
        /*define placeholder class*/
        var placeholder = $("#registers-chart");
        if ($(placeholder).size() == 0) {return;}
        <?php
        /* Making Array for graphs */
        $date  = new \DateTime();
        $month = (int) $date->format('m');
        $year  = (int) $date->format('Y');
        $days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $arr_expert_dates = $arr_expert = [];
        $arr_client_dates = $arr_client = [];
        if(isset($arr_expert_data) && count($arr_expert_data) > 0 )
        {       
            foreach ($arr_expert_data as $key => $value) 
            {
                array_push($arr_expert_dates, (int) $value['date'] ); 
                $arr_expert[$value['date']] =  $value['expert_register_count'];
            }
        }
        if(isset($arr_client_data) && count($arr_client_data) > 0 )
        {       
            foreach ($arr_client_data as $key => $value_nxt) 
            {
                array_push($arr_client_dates, (int) $value_nxt['date'] ); 
                $arr_client[$value_nxt['date']] =  $value_nxt['client_register_count'];
            }
        }
        ?>
        var d1 = [<?php for($i =1;$i <= $days_in_month; $i++) { ?>          
           [<?php echo $i ?>,
                <?php 
                    if(isset($arr_expert_dates) && count($arr_expert_dates) > 0 && isset($arr_expert) )
                    {   
                        if(in_array($i ,$arr_expert_dates)) 
                        {
                            ?>
                            <?php echo isset($arr_expert[$i])?$arr_expert[$i]:0;?>],
                            <?php  
                        }
                        else
                        {
                            ?>
                            0],
                            <?php
                        }
                    }
                    else
                    {
                    ?>
                        0],                        
                    <?php
                    }
                   
                 }
                 ?>
          ];
          var d2 = [<?php for($j = 1;$j <= $days_in_month; $j++) { ?>          
           [<?php echo $j ?>,
                <?php 
                    if(isset($arr_client_dates) && count($arr_client_dates) > 0)
                    {   
                        if(in_array($j ,$arr_client_dates)) 
                        {
                            ?>
                            <?php echo isset($arr_client[$j])?$arr_client[$j]:0;?>],
                            <?php  
                        }
                        else
                        {
                            ?>
                            0],
                            <?php
                        }
                    }
                    else
                    {
                    ?>
                        0],                        
                    <?php
                    }
                 }
                ?>
          ];
        var chartColours = ['#381095', '#ed7a53', '#9FC569', '#bbdce3', '#9a3b1b', '#5a8022', '#2c7282'];
        /* graph options*/
        var options = {
                grid: {
                    show: true,
                    aboveData: true,
                    color: "#3f3f3f" ,
                    labelMargin: 5,
                    axisMargin: 0, 
                    borderWidth: 0,
                    borderColor:null,
                    minBorderMargin: 5 ,
                    clickable: true, 
                    hoverable: true,
                    autoHighlight: true,
                    mouseActiveRadius: 20
                },
                series: {
                    grow: {
                        active: false,
                        stepMode: "linear",
                        steps: 50,
                        stepDelay: true
                    },
                    lines: {
                        show: true,
                        fill: true,
                        lineWidth: 3,
                        steps: false
                        },
                    points: {
                        show:true,
                        radius: 4,
                        symbol: "circle",
                        fill: true,
                        borderColor: "#fff"
                    }
                },
                legend: { 
                    position: "ne", 
                    margin: [0,-25], 
                    noColumns: 0,
                    labelBoxBorderColor: null,
                    labelFormatter: function(label, series) {
                        // just add some space to labes
                        return label+'&nbsp;&nbsp;';
                    }
                },
                yaxis: { min: 0 ,tickSize:1 ,tickDecimals: 0 },
                xaxis: {ticks:11, tickDecimals: 0},
                colors: chartColours,
                shadowSize:1,
                tooltip: true, //activate tooltip
                tooltipOpts: {
                    content: "%s : %y.0",
                    defaultTheme: false,
                    shifts: {
                        x: -30,
                        y: -50
                    }
                }
            };
            $.plot(placeholder, [
            {
                label: "Experts Register", 
                data: d1,
                lines: {fillColor: "#eee9fb"},
                points: {fillColor: "#381095"}
            }, 
            {
                label: "Clients Register", 
                data: d2,
                lines: {fillColor: "#fff8f2"},
                points: {fillColor: "#ed7a53"}
            } 
        ], options);
    }
});
</script>
@stop