@extends('admin.layout.master')                

    @section('main_content')

    <!-- BEGIN Page Title -->
    <link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/data-tables/latest/dataTables.bootstrap.min.css">
    <div class="page-title">
        <div>

        </div>
    </div>
    <!-- END Page Title -->

    <?php 
          $previous_url = url()->previous();
          $arr_segments = [];
          $arr_segments = explode('/',$previous_url);

          if(isset($arr_segments['7']) && $arr_segments['7'] == 'completed' )
          {
             $previous_page = "Manage Completed Projects";
             $back_url      = url('/').'/admin/projects/completed';
          }
          else if(isset($arr_segments['7']) && $arr_segments['7'] == 'canceled' )
          {
             $previous_page = "Manage Completed Projects";
             $back_url      = url('/').'/admin/projects/canceled';
          }
          else if(isset($arr_segments['7']) && $arr_segments['7'] == 'ongoing' )
          {
             $previous_page = "Manage Ongoing Projects";
             $back_url      = url('/').'/admin/projects/ongoing';
          }
          else
          {
            $previous_page = "Manage All Projects";
            $back_url      = url('/').'/admin/projects/all';
          }    
    ?>


    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
       <ul class="breadcrumb">
          <li>
             <i class="fa fa-home"></i>
             <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
          </li>
          <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-gear"></i>
             <a href="{{ $back_url }}" >{{ $previous_page or ''}}</a>
          </span> 
          <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-star"></i>
          </span>
          <li class="active">{{ $page_title or ''}}</li>
       </ul>
    </div>
    <!-- END Breadcrumb -->

    <!-- BEGIN Main Content -->
    <div class="row">
      <div class="col-md-12">

          <div class="box">
            <div class="box-title">
              <h3>
                <i class="fa fa-star"></i>
                {{ $arr_all_milestones[0]['project_details']['project_name']  or ''}}
                
                 @if(isset($arr_all_milestones[0]['project_details']['project_name']) &&  $arr_all_milestones[0]['project_details']['project_name'] != "" )
                  <span class="divider">
                    <i class="fa fa-angle-right"></i> 
                  </span>  
                @endif
                {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
                <a data-action="collapse" href="#"></a>
                <a data-action="close" href="#"></a>
            </div>
        </div>
        <div class="box-content">
          
          @include('admin.layout._operation_status')
          
          <form name="frm_manage" id="frm_manage" method="POST" class="form-horizontal" action="{{$module_url_path}}/multi_action">
               {{ csrf_field() }}
          <div class="btn-toolbar pull-right clearfix">
          <div class="btn-group"> 
             <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                title="Refresh" 
                onclick="window.location.reload()" 
                style="text-decoration:none;">
             <i class="fa fa-repeat"></i>
             </a> 
          </div>
          
          </div>
          <br/><br/>
          <div class="clearfix"></div>
          <div class="table-responsive" style="border:0">

            <input type="hidden" name="multi_action" value="" />

            <table class="table table-advance"  id="table1" >
              <thead>
                <tr>
<!--                   <th style="width:18px"> <input type="checkbox" name="mult_change" id="mult_change" /></th>
 -->             
                  <th>Title</th>
                  <th>Created By</th>
                  <th>Cost</th>
                  <th>Description</th>
                  <th>Date</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                
                @if(isset($arr_all_milestones) && sizeof($arr_all_milestones)>0)
                  @foreach($arr_all_milestones as $milestone)
                  @if(isset($milestone['transaction_details']['payment_status']) && ($milestone['transaction_details']['payment_status']=='1' || $milestone['transaction_details']['payment_status']=='2'))
                  <tr>
                   
                    <td>{{ isset($milestone['title'])?str_limit(ucfirst($milestone['title']),$limit = 30, $end='...'):''}}</td>
                   
                    <td>
                    @if(isset($milestone['created_by']) && $milestone['created_by']=='1')Client @endif
                    @if(isset($milestone['created_by']) && $milestone['created_by']=='2')Project Manager @endif
                    </td>
                    <td>{{ isset($milestone['project_details']['project_currency'])?$milestone['project_details']['project_currency']:'' }}&nbsp;{{ isset($milestone['cost'])?$milestone['cost']:''  }}</td>
                    <td> 
                    {{ isset($milestone['description'])?str_limit($milestone['description'],$limit = 15, $end='...'):'' }}
                    </td> 

                    <td>
                    {{ isset($milestone['updated_at'])?date("d M Y", strtotime($milestone['updated_at'])):''}}
                   </td>
                    <td>
                    @if(isset($milestone['status']) && $milestone['status']=='0')Created @endif
                    @if(isset($milestone['status']) && $milestone['status']=='1')Paid & Assigned @endif
                    @if(isset($milestone['status']) && $milestone['status']=='2')Completed @endif
                    </td>
                    <td>
                    <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" href="{{ $module_url_path.'/show/'.base64_encode($milestone['id']) }}" data-original-title="View">
                     <i class="fa fa-eye" ></i>
                     </a>
                     </td>
                  </tr>
                  @endif
                  @endforeach
                @endif
                 
              </tbody>
            </table>
          </div>
      
          </form>
      </div>
  </div>
</div>
</div>
<!-- END Main Content -->
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
   function confirm_delete(url) {
      alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
          if (e) {
              if(url != "" && url != undefined){
                showProcessingOverlay();
                window.location.href=url;
              }
              return true;
          } else {
              return false;
          }
      });
   }
   function check_multi_action(frm_id,action) {
     var frm_ref = jQuery("#"+frm_id);
     if(jQuery(frm_ref).length && action!=undefined && action!="") {
       if(action == 'delete') {
            if (!confirm_delete()) {
                return false;
            }
       }
       /* Get hidden input reference */
       var input_multi_action = jQuery('input[name="multi_action"]');
       if(jQuery(input_multi_action).length) {
         /* Set Action in hidden input*/
         jQuery('input[name="multi_action"]').val(action);
         /*Submit the referenced form */
         jQuery(frm_ref)[0].submit();
       } else {
         console.warn("Required Hidden Input[name]: multi_action Missing in Form ")
       }
     } else {
         console.warn("Required Form[id]: "+frm_id+" Missing in Current category ")
     }
   }
</script>
@stop                    


