@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-star"></i>
      <a href="{{ $module_url_path }}/{{base64_encode($arr_info['project_details']['id'])}}">{{ 'Manage ' }}{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-star"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-star"></i>
               {{$arr_info['project_details']['project_name']  or ''}}
               <span class="divider">
                  <i class="fa fa-angle-right"></i>   
               </span>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
               @include('admin.layout._operation_status')
           <div class="alert alert-success" id="ajax_success" style="display:none;">
            <button data-dismiss="alert" class="close">×</button>
            <strong>Success!</strong>
            <div id="ajax_sub_success"></div>
         </div>
         <div class="alert alert-danger" id="ajax_error" style="display:none;">
            <button data-dismiss="alert" class="close">×</button>
            <strong>Error!</strong> 
            <div id="ajax_sub_error"></div>
         </div>
           
              <div class="row">

               @if(isset($arr_info) && sizeof($arr_info)>0)
              <form name="validation-form" id="validation-form" method="POST" action="" class="form-horizontal"  enctype="multipart/form-data">
               {{ csrf_field() }}
                <div class="col-md-6">
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Project Name :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['project_details']['project_name'])?$arr_info['project_details']['project_name']:''}}</div>
                     </div>
                  </div>
           
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Title :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['title'])?ucfirst($arr_info['title']):''}}</div>
                     </div>
                  </div>
                    <div class="form-group">
                     <label class="col-sm-3 control-label">Created By :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">
                        @if(isset($arr_info['created_by']) && $arr_info['created_by']=='1') Client @endif
                        @if(isset($arr_info['created_by']) && $arr_info['created_by']=='2') Project Manager @endif
                        </div>
                     </div>
                  </div>
                   <div class="form-group">
                     <label class="col-sm-3 control-label">Cost :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['project_details']['project_currency'])?$arr_info['project_details']['project_currency']:''}}&nbsp;{{isset($arr_info['cost'])?$arr_info['cost']:'0'}}</div>
                     </div>
                  </div>
                  
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Description :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['description'])?str_limit($arr_info['description'],$limit = 200, $end='...'):''}}</div>
                     </div>
                  </div>
                
                   <div class="form-group">
                     <label class="col-sm-3 control-label">Status :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">
                        @if(isset($arr_info['status']) && $arr_info['status']=='0') Created @endif
                        @if(isset($arr_info['status']) && $arr_info['status']=='1') Paid & Assigned @endif
                        @if(isset($arr_info['status']) && $arr_info['status']=='2') Completed @endif
                        </div>
                     </div>
                  </div>
                 
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Invoice Id :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['invoice_id'])?$arr_info['invoice_id']:''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>

                   <div class="form-group">
                     <label class="col-sm-3 control-label">Transaction Type :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">
                        @if(isset($arr_info['transaction_details']['transaction_type']) && $arr_info['transaction_details']['transaction_type']=='1') Subscription @endif
                        @if(isset($arr_info['transaction_details']['transaction_type']) && $arr_info['transaction_details']['transaction_type']=='2') Milestone @endif
                        @if(isset($arr_info['transaction_details']['transaction_type']) && $arr_info['transaction_details']['transaction_type']=='3') Release Milestone @endif                        
                        </div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>

                   <div class="form-group">
                     <label class="col-sm-3 control-label">Payment Amount :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['project_details']['project_currency'])?$arr_info['project_details']['project_currency']:''}}&nbsp;{{isset($arr_info['transaction_details']['paymen_amount'])?$arr_info['transaction_details']['paymen_amount']:'0'}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>

                   <div class="form-group">
                     <label class="col-sm-3 control-label">Payment Method :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">
                        @if(isset($arr_info['transaction_details']['payment_method']) && $arr_info['transaction_details']['payment_method']=='0') Free @endif
                        @if(isset($arr_info['transaction_details']['payment_method']) && $arr_info['transaction_details']['payment_method']=='1') Paypal @endif
                        @if(isset($arr_info['transaction_details']['payment_method']) && $arr_info['transaction_details']['payment_method']=='2') Stripe @endif                        
                        </div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>

                  <div class="form-group">
                     <label class="col-sm-3 control-label">Payment Status :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">
                        @if(isset($arr_info['transaction_details']['payment_status']) && $arr_info['transaction_details']['payment_status']=='0') Pending @endif
                        @if(isset($arr_info['transaction_details']['payment_status']) && $arr_info['transaction_details']['payment_status']=='1') Approved @endif
                        @if(isset($arr_info['transaction_details']['payment_status']) && $arr_info['transaction_details']['payment_status']=='2') Paid @endif                        
                        @if(isset($arr_info['transaction_details']['payment_status']) && $arr_info['transaction_details']['payment_status']=='3') Loss @endif                        
                        </div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>

                  <div class="form-group">
                     <label class="col-sm-3 control-label">Payment Date :</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['transaction_details']['payment_date'])?date("d/m/Y", strtotime($arr_info['transaction_details']['payment_date'])):''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>

              
                </div>
              </form>
               @endif
            </div>
            </div>
       </div>
   </div>
</div>

@stop