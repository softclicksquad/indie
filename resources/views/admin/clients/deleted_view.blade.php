@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
 <div>
 </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
 <ul class="breadcrumb">
  <li>
   <i class="fa fa-home"></i>
   <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
 </li>
 <span class="divider">
  <i class="fa fa-angle-right"></i>
  <i class="fa fa-gears"></i>
   <a href="{{ $module_url_path }}">{{$module_title}}</a>
</span> 
<span class="divider">
  <i class="fa fa-angle-right"></i>
  <i class="fa fa-gear"></i>
</span>
<li class="active">{{ $page_title or 'N/A' }}</li>
</ul>
</div>
<!-- END Breadcrumb -->

<!-- BEGIN Main Content -->
<div class="row">
 <div class="col-md-12">
  <div class="box">
   <div class="box-title">
    <h3>
     <i class="fa fa-gear"></i>
     {{$arr_info['contest_title'] or 'N/A'}}
     <span class="divider">
      <i class="fa fa-angle-right"></i> 
    </span>  
    {{ $page_title or 'N/A' }}
  </h3>
  <div class="box-tool">
   <a data-action="collapse" href="#"></a>
   <a data-action="close" href="#"></a>
 </div>
</div>
<div class="box-content">
<div class="row">
  <form name="validation-form" id="validation-form" method="POST" action="javascript:void(0)" class="form-horizontal"  enctype="multipart/form-data">
   {{ csrf_field() }}
  <div class="col-md-6">
    <div class="form-group">
      <label class="col-sm-3 control-label">First Name:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block">{{$arr_client['first_name'] or 'N/A'}}</div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label">Last Name:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_client['last_name'] or 'N/A'}}</div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label">Phone Number:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">+{{$arr_client['phone_code'] or 'N/A'}} {{$arr_client['phone_number'] or 'N/A'}}</div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label">Country:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_client['country_details']['country_name'] or 'N/A'}}</div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label">State:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_client['state_details']['state_name'] or 'N/A'}}</div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label">City:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_client['city_details']['city_name'] or 'N/A'}}</div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label">Zip Code:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_client['zip'] or 'N/A'}}</div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label">Company Name:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_client['company_name'] or 'N/A'}}</div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label">Owner Name:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_client['owner_name'] or 'N/A'}}</div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label">Company Size:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_client['company_size'] or 'N/A'}}</div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label">Founding year:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_client['founding_year'] or 'N/A'}}</div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label">Vat Number:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_client['vat_number'] or 'N/A'}}</div>
      </div>
    </div>
     <div class="form-group">
      <label class="col-sm-3 control-label">Spoken Languages:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_client['spoken_languages'] or 'N/A'}}</div>
      </div>
    </div>
     <div class="form-group">
      <label class="col-sm-3 control-label">Time Zone:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_client['timezone'] or 'N/A'}}</div>
      </div>
    </div>
     <div class="form-group">
      <label class="col-sm-3 control-label">Occupation:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_client['occupation'] or 'N/A'}}</div>
      </div>
    </div>
     <div class="form-group">
      <label class="col-sm-3 control-label">Website:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_client['website'] or 'N/A'}}</div>
      </div>
    </div>
      <div class="form-group">
      <label class="col-sm-3 control-label">About you:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_client['profile_summary'] or 'N/A'}}</div>
      </div>
    </div>
  </div>

          </form>
        </div>          
      </div>
    </div>
  </div>
</div>
<!-- END Main Content -->
@stop