@extends('admin.layout.master')                

    @section('main_content')

     <!-- BEGIN Page Title -->
    <link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/data-tables/latest/dataTables.bootstrap.min.css">
    <div class="page-title">
        <div>

        </div>
    </div>
    <!-- END Page Title -->
    
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
       <ul class="breadcrumb">
          <li>
             <i class="fa fa-home"></i>
             <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
          </li>
          <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-user-plus"></i>
          </span>
          <li class="active">{{ isset($page_title)?str_plural($page_title):"" }}</li>
       </ul>
    </div>
    <!-- END Breadcrumb -->

    <!-- BEGIN Main Content -->
    <div class="row">
      <div class="col-md-12">

          <div class="box">
            <div class="box-title">
              <h3>
                <i class="fa fa-user-plus"></i>
                {{ isset($page_title)?str_plural($page_title):"" }}
            </h3>
            <div class="box-tool">
                <a data-action="collapse" href="#"></a>
                <a data-action="close" href="#"></a>
            </div>
        </div>
        <div class="box-content">
          
          @include('admin.layout._operation_status')
          
          <form name="frm_manage" id="frm_manage" method="POST" class="form-horizontal" action="{{$module_url_path}}/multi_action">
          {{ csrf_field() }}
          
          <br/><br/>
          <div class="clearfix"></div>
          <div class="table-responsive" style="border:0">

            <input type="hidden" name="multi_action" value="" />

            <table class="table table-advance"  id="table1" >
              <thead>
                <tr>
                  <th style="width:18px"> <input type="checkbox" name="mult_change" id="mult_change" /></th>
                  <th>Name</th> 
                  <th>Email</th>
                  <th>Registration <br> Date</th> 
                  <th>Deletion <br> Date</th> 
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                
                @if(isset($arr_deleted_client) && sizeof($arr_deleted_client)>0)
                  @foreach($arr_deleted_client as $client_user)
                  
                  <tr>
                    <td> 
                      <input type="checkbox" 
                             name="checked_record[]"  
                             value="{{ base64_encode($client_user['id']) }}" /> 
                    </td>
                    <td> 
                        {{ isset($client_user['client_details']['first_name'])?$client_user['client_details']['first_name']:'-'  }}
                        {{ isset($client_user['client_details']['last_name'])?$client_user['client_details']['last_name']:'-'  }}
                    </td> 
                    <td>{{ isset($client_user['email'])?$client_user['email']:'' }} </td> 
                    <td>{{date('d M Y',strtotime($client_user['created_at']))}}</td>
                    <td>{{date('d M Y',strtotime($client_user['deleted_at']))}}</td>
                     <td>
                        @if(isset($client_user['client_details']) && !empty($client_user['client_details'])>0)
                        <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" href="{{ $module_url_path.'/view/'.base64_encode($client_user['id']) }}"  title="View">
                        <i class="fa fa-eye" ></i>
                        </a>  
                        @endif
                     </td>
                  </tr>

                  @endforeach
                @endif
                 
              </tbody>
            </table>
          </div>
        <div>   </div>
         
          </form>
      </div>
  </div>
</div>
</div>
<!-- END Main Content -->
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/dataTables.bootstrap.min.js"></script>
@stop                    


