@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-desktop"></i>
      <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-list"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-list"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content" >
               @include('admin.layout._operation_status')  
            <form name="validation-form" id="validation-form" method="POST" class="form-horizontal"  enctype="multipart/form-data">
               {{ csrf_field() }}
              <div class="row">

               @if(isset($arr_info) && sizeof($arr_info)>0)
               <form class="form-horizontal" action="" novalidate name="project" method="post">
                  {{ csrf_field() }}
                <div class="col-md-6">
                   
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Project Name:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['project_name'])?$arr_info['project_name']:''}}</div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Project Category:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['category_details']['category_title'])?$arr_info['category_details']['category_title']:''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Client Name:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{ ucfirst($arr_info['client_details']['user_name']) }}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Client Email:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block"><a href="mailto:{{isset($arr_info['client_details']['email'])?$arr_info['client_details']['email']:''}}">{{isset($arr_info['client_details']['email'])?$arr_info['client_details']['email']:''}}</a></div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Skill:</label>
                     <div class="col-sm-9 controls">
                     <?php $str = "";?>
                            @if(isset($arr_info['project_skills']) && count($arr_info['project_skills']) > 0)
                              @foreach($arr_info['project_skills'] as $key=> $skill) 
                                @if(isset($skill['skill_data']['skill_name']) && $skill['skill_data']['skill_name'] )
                                    <?php  $str .= $skill['skill_data']['skill_name'].' ,'; ?>
                                @endif
                              @endforeach
                            @endif

                            {{ trim($str,' ,') }}   
                        
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Project Summary:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['project_description'])?$arr_info['project_description']:''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Start Date:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['project_start_date'])?$arr_info['project_start_date']:''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">End Date:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['project_end_date'])?$arr_info['project_end_date']:''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>

                  <div class="form-group">
                     <label class="col-sm-3 control-label">Attachment:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">
                        @if(isset($arr_info['project_attachment']) && $arr_info['project_attachment']!="")
                         {{ $arr_info['project_attachment'] }}
                        <a href="{{$project_attachment_public_path.$arr_info['project_attachment']}}" download="">&nbsp;<i class="fa fa-download"></i>&nbsp;Download</a>
                        @else
                        {{'-'}}
                        @endif
                        </div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>

                  <div class="form-group">
                     <label class="col-sm-3 control-label">Project Duration:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['project_expected_duration'])?$arr_info['project_expected_duration']:''}} days</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  @if($arr_info['project_pricing_method']=='2')
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Hourly Rate:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['project_currency'])?$arr_info['project_currency']:''}}&nbsp;{{isset($arr_info['project_cost'])?$arr_info['project_cost']:''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  @else
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Fixed Price:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['project_currency'])?$arr_info['project_currency']:''}}&nbsp;{{isset($arr_info['project_cost'])?$arr_info['project_cost']:''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  @endif
                </div>
              </form>
            </div>
         @endif
      </div>
   </div>
</div>
</div>
<!-- END Main Content -->
@stop