@extends('admin.layout.master')
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-desktop"></i>
      <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-list"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-text-width"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
            @include('admin.layout._operation_status') 
            <div class="tabbable">
               <form name="validation-form" id="validation-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/store" enctype="multipart/form-data"  files ="true">
                  {{ csrf_field() }}
                  <ul  class="nav nav-tabs">
                     @include('admin.layout._multi_lang_tab')
                  </ul>
                  <div id="myTabContent1" class="tab-content">
                     @if(isset($arr_lang) && sizeof($arr_lang)>0)
                     @foreach($arr_lang as $lang)

                     <div class="tab-pane fade {{ $lang['locale']=='en'?'in active':'' }}"
                        id="{{ $lang['locale'] }}">

                        @if($lang['locale'] == 'en') 
                        <div class="form-group">
                           <label class="col-sm-3 col-lg-2 control-label">Category Image:</label>
                           <div class="col-sm-9 col-lg-2 controls">
                              <div class="fileupload fileupload-new" data-provides="fileupload">
                                 <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                                    @if(isset($admin_img_path) && isset($arr_profile['image']))
                                    <img src="{{$admin_img_path.$arr_profile['image']}}" alt="" />
                                    @endif
                                 </div>
                                 <div class="fileupload-preview fileupload-exists img-thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                 <div>
                                    <span class="btn btn-default btn-file" style="height:32px;">
                                    <span class="fileupload-new">Select Image</span>
                                    <span class="fileupload-exists">Change</span>
                                    <input type="file" class="file-input" name="category_image" id="category_image"  />
                                    <input type="hidden" class="form-control" name="image" id="oldprofileimage" value="" />
                                    </span>
                                    <a href="#" id="remove" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                 </div>
                              </div>
                           </div>
                           <div class="clearfix"></div>
                        </div>
                        @endif    
                        
                        <div class="form-group">
                           <label class="col-sm-3 col-lg-2 control-label" for="page_title">Category Title<i class="red">*</i></label>
                           <div class="col-sm-6 col-lg-4 controls">
                              @if($lang['locale'] == 'en') 
                              <input type="text" name="category_title_{{$lang['locale']}}" class="form-control" value="{{old('category_title_'.$lang['locale'])}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Category Title">
                              @else
                              <input type="text" name="category_title_{{$lang['locale']}}" class="form-control" value="{{old('category_title_'.$lang['locale'])}}" placeholder="Category Title" data-rule-maxlength="255">
                              @endif    
                              <span class='error'>{{ $errors->first('category_title_'.$lang['locale']) }}</span>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-3 col-lg-2 control-label" for="meta_keyword">Meta Tag<i class="red">*</i></label>
                           <div class="col-sm-6 col-lg-4 controls">
                              @if($lang['locale'] == 'en')        
                              <input type="text" name="meta_tag_{{$lang['locale']}}" class="form-control" value="{{old('meta_tag_'.$lang['locale'])}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Meta Tag ">
                              @else
                              <input type="text" name="meta_tag_{{$lang['locale']}}" class="form-control" value="{{old('meta_tag_'.$lang['locale'])}}" placeholder="Meta Tag">
                              @endif
                              <span class='error'>{{ $errors->first('meta_tag_'.$lang['locale']) }}</span>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-3 col-lg-2 control-label" for="description">Description<i class="red">*</i></label>
                           <div class="col-sm-6 col-lg-4 controls">
                              @if($lang['locale'] == 'en')        
                              <textarea name="description_{{$lang['locale']}}" rows="3" class="form-control"  placeholder="Description" data-rule-required="true">{{old('description_'.$lang['locale'])}}</textarea>
                              @else
                              <textarea name="description_{{$lang['locale']}}" rows="3" class="form-control" data-rule-maxlength="1000" placeholder="Description">{{old('description_'.$lang['locale'])}}</textarea>
                              @endif
                              <span class='error'>{{ $errors->first('description_'.$lang['locale']) }}</span>
                           </div>
                        </div>
                     </div>
                     @endforeach
                     @endif
                  </div>
                  <br>
                  <div class="form-group">
                     <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                        <input type="submit" value="Save" class="btn btn btn-primary">
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
@stop
