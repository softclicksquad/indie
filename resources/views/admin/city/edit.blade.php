@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-road"></i>
      <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-edit"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-edit"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
            @include('admin.layout._operation_status')  
            <form name="validation-form" id="validation-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/update/{{$enc_id}}" enctype="multipart/form-data">
               {{ csrf_field() }}

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="country_name">Country Name<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <select type="text" name="country" class="form-control" data-rule-required="true" onchange="javascript: return loadStates(this);">
                           <option value="">---Select Location---</option>
                        @if(isset($arr_countries) && sizeof($arr_countries)>0)
                           @foreach($arr_countries as $country)
                           <option value="{{isset($country['id'])?$country['id']:''}}"
                           @if(isset($arr_city['state_details']['country_id']) && $arr_city['state_details']['country_id']==$country['id'])
                              selected="true" 
                           @endif
                           >{{isset($country['country_name'])?$country['country_name']:''}}
                           </option>
                           @endforeach
                        @endif
                     </select>
                     <span class='error'>{{ $errors->first('country') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="state">State Name<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <select type="text" name="state" class="form-control" data-rule-required="true">
                        <option value="">---Select State---</option>
                        @if(isset($arr_city['state_details']['country_details']['states']) && sizeof($arr_city['state_details']['country_details']['states'])>0)
                           @foreach($arr_city['state_details']['country_details']['states'] as $state)
                              <option value="{{isset($state['id'])?$state['id']:''}}"
                              @if(isset($arr_city['state_details']['id']) && $arr_city['state_details']['id']==$state['id'])
                                 selected="true" 
                              @endif
                              >{{isset($state['state_name'])?$state['state_name']:''}}
                              </option>
                           @endforeach
                        @endif
                     </select>
                     <span class='error'>{{ $errors->first('state') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="city_name">City Name<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="city_name" class="form-control" value="{{isset($arr_city['city_name'])?$arr_city['city_name']:''}}" data-rule-required="true" data-rule-maxlength="255" placeholder="City Name">
                     <span class='error'>{{ $errors->first('city_name') }}</span>
                  </div>
               </div>

               <br>
               <div class="form-group">
                  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                     <input type="submit" value="Save" class="btn btn btn-primary">
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<!-- END Main Content -->
<script type="text/javascript">
 function loadStates(ref)   
 {
     var selected_country = jQuery(ref).val();

     if(selected_country && selected_country!="" && selected_country!=0)
     {
           jQuery.ajax({
                           url:'{{$module_url_path}}/get_states/'+btoa(selected_country),
                           type:'GET',
                           data:'flag=true',
                           dataType:'json',
                           beforeSend:function()
                           {
                               jQuery('select[name="state"]').attr('readonly','readonly');
                           },
                           success:function(response)
                           {
                               if(response.status=="success")
                               {
                                   jQuery('select[name="state"]').removeAttr('readonly');

                                   if(typeof(response.states) == "object")
                                   {
                                      var option = '<option value="">---Select State---</option>'; 
                                      jQuery(response.states).each(function(index,state)
                                      {
                                           option+='<option value="'+state.id+'">'+state.state_name+'</option>';
                                      });

                                      jQuery('select[name="state"]').html(option);
                                   }

                               }
                               return false;
                           }    
           });
      }
 }
</script>

@stop
