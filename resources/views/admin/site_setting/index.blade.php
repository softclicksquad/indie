@extends('admin.layout.master')

@section('main_content')

<!-- BEGIN Page Title -->
<div class="page-title">
    <div>

    </div>
</div>
<!-- END Page Title -->

<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
        </li>
        <span class="divider">
            <i class="fa fa-angle-right"></i>
            <i class="fa fa-wrench"></i>
        </span>
        <li class="active"> {{ isset($page_title)?$page_title:"" }}</li>
    </ul>
</div>
<!-- END Breadcrumb -->

<!-- BEGIN Main Content -->


<div class="row">
    <div class="col-md-12">
        <div class="box box-blue">
            <div class="box-title">
                <h3><i class="fa fa-wrench"></i> Settings</h3>
                <div class="box-tool">
                </div>
            </div>
            <div class="box-content">
                <div id="op_status">
                    @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ Session::get('success') }}
                    </div>
                    @endif

                    @if(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ Session::get('error') }}
                    </div>
                    @endif
                </div>

                <form class="form-horizontal" id="validation-form" method="POST" action="{{ url($admin_panel_slug) }}/site_settings/update/{{base64_encode($arr_site_settings['site_settting_id'])}}">
                    {{ csrf_field() }}

                    <h2 class="web-title-new">Site Setting</h2>
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label class="col-sm-3 col-lg-4 control-label">Website Name<i class="red">*</i></label>
                                <div class="col-sm-9 col-lg-6 controls">
                                    <input type="text" class="form-control" name="site_name" id="site_name" data-rule-required="true" value="{{isset($arr_site_settings['site_name'])?$arr_site_settings['site_name']:''}}" />
                                    <span class='error'>{{ $errors->first('site_name') }}</span>
                                </div>
                            </div>
                        </div>




                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label class="col-sm-3 col-lg-4 control-label">Website Status<i class="red">*</i></label>
                                <div class="col-sm-9 col-lg-6 controls">
                                    <label class="radio">
                                        <input type="radio" value="1" checked="" name="site_status" @if(isset($arr_site_settings['site_status']) && $arr_site_settings['site_status']==1) checked="true" @endif>
                                        Live (Online)
                                    </label>
                                    <label class="radio">
                                        <input type="radio" value="0" name="site_status" @if(isset($arr_site_settings['site_status']) && $arr_site_settings['site_status']==0) checked="true" @endif>
                                        Maintenance Mode (Offline)
                                    </label>
                                </div>
                                <span class='error'>{{ $errors->first('site_status') }}</span>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>

                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label class="col-sm-3 col-lg-4 control-label" for="category_name">Address<i class="red">*</i></label>
                                <div class="col-sm-10 col-lg-6 controls">
                                    <input type="text" class="form-control" name="site_address" id="site_address" data-rule-required="true" value="{{isset($arr_site_settings['site_address'])?$arr_site_settings['site_address']:''}}" />
                                    <span class='error'>{{ $errors->first('site_address') }}</span>
                                </div>
                            </div>

                        </div>

                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label class="col-sm-3 col-lg-4 control-label">Contact Number<i class="red">*</i></label>
                                <div class="col-sm-9 col-lg-6 controls">
                                    <input type="text" class="form-control" name="site_contact_number" id="site_contact_number" data-rule-required="true" data-rule-minlength="6" data-rule-maxlength="16" data-rule-number="true" value="{{isset($arr_site_settings['site_contact_number'])?$arr_site_settings['site_contact_number']:''}}" />
                                    <span class='error'>{{ $errors->first('site_contact_number') }}</span>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label class="col-sm-3 col-lg-4 control-label" for="category_name">Meta Description<i class="red"></i></label>
                                <div class="col-sm-9 col-lg-6 controls">
                                    <input type="text" class="form-control" name="meta_desc" id="meta_desc" data-rule-required="true" value="{{isset($arr_site_settings['meta_desc'])?$arr_site_settings['meta_desc']:''}}" />
                                    <span class='error'>{{ $errors->first('meta_desc') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label class="col-sm-3 col-lg-4 control-label">Meta Keyword</label>
                                <div class="col-sm-9 col-lg-6 controls">
                                    <input type="text" class="form-control" name="meta_keyword" id="meta_keyword" data-rule-required="true" value="{{isset($arr_site_settings['meta_keyword'])?$arr_site_settings['meta_keyword']:''}}" />
                                    <span class='error'>{{ $errors->first('meta_keyword') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label class="col-sm-3 col-lg-4 control-label">Email<i class="red">*</i></label>
                                <div class="col-sm-9 col-lg-6 controls">
                                    <input type="text" class="form-control" name="site_email_address" id="site_email_address" data-rule-required="true" value="{{isset($arr_site_settings['site_email_address'])?$arr_site_settings['site_email_address']:''}}" />
                                    <span class='error'>{{ $errors->first('site_email_address') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-6 col-lg-6">

                            <div class="form-group">
                                <label class="col-sm-3 col-lg-4 control-label">Facebook URL<i class="red">*</i></label>
                                <div class="col-sm-9 col-lg-6 controls">
                                    <input type="text" class="form-control" name="fb_url" id="fb_url" data-rule-required="true" data-rule-url="true" value="{{isset($arr_site_settings['fb_url'])?$arr_site_settings['fb_url']:''}}" />
                                    <span class='error'>{{ $errors->first('fb_url') }}</span>
                                </div>
                            </div>
                        </div>










                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label class="col-sm-3 col-lg-4 control-label">Twitter URL<i class="red">*</i></label>
                                <div class="col-sm-9 col-lg-6 controls">
                                    <input type="text" class="form-control" name="twitter_url" id="twitter_url" data-rule-required="true" data-rule-url="true" value="{{isset($arr_site_settings['twitter_url'])?$arr_site_settings['twitter_url']:''}}" />
                                    <span class='error'>{{ $errors->first('twitter_url') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label class="col-sm-3 col-lg-4 control-label">Linkedin URL<i class="red">*</i></label>
                                 <div class="col-sm-9 col-lg-6 controls">
                                    <input type="text" class="form-control" name="linkedin_url" id="linkedin_url" data-rule-required="true" data-rule-url="true" value="{{isset($arr_site_settings['linkedin_url'])?$arr_site_settings['linkedin_url']:''}}" />
                                    <span class='error'>{{ $errors->first('linkedin_url') }}</span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label class="col-sm-3 col-lg-4 control-label">Google+ URL <i class="red">*</i></label>
                             <div class="col-sm-9 col-lg-6 controls">
                                <input type="text" class="form-control" name="google_plus_url" id="site_name" data-rule-required="true" data-rule-url="true" value="{{isset($arr_site_settings['google_plus_url'])?$arr_site_settings['google_plus_url']:''}}" />
                                <span class='error'>{{ $errors->first('google_plus_url') }}</span>
                            </div>
                        </div>
                              </div>
                        </div>
                    
                    
                    
                    <div class="site-setting-section">
                       <h2 class="web-title-new">Other Setting</h2>
                    <div class="row">


                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label class="col-sm-3 col-lg-4 control-label">Mailchimp api key <i class="red">*</i></label>
                                 <div class="col-sm-9 col-lg-6 controls">
                                    <input type="text" class="form-control" name="mailchimp_api_key" id="site_name" data-rule-required="true" value="{{isset($arr_site_settings['mailchimp_api_key'])?$arr_site_settings['mailchimp_api_key']:''}}" />
                                    <span class='error'>{{ $errors->first('mailchimp_api_key') }}</span>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label class="col-sm-3 col-lg-4 control-label">Mailchimp list id <i class="red">*</i></label>
                                 <div class="col-sm-9 col-lg-6 controls">
                                    <input type="text" class="form-control" name="mailchimp_list_id" id="site_name" data-rule-required="true" value="{{isset($arr_site_settings['mailchimp_list_id'])?$arr_site_settings['mailchimp_list_id']:''}}" />
                                    <span class='error'>{{ $errors->first('mailchimp_list_id') }}</span>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label class="col-sm-3 col-lg-4 control-label">Project Cost<i class="red">*</i> </label>
                                 <div class="col-sm-9 col-lg-6 controls">
                                    <input type="text" class="form-control" name="project_cost" id="project_cost" data-rule-required="true" data-rule-number="true" data-rule-min="1" value="{{isset($arr_site_settings['project_cost'])?$arr_site_settings['project_cost']:''}}" />
                                    <span class='error'>{{ $errors->first('project_cost') }}</span>

                                    <label class="control-label" style="text-align:left;">According to that price client paid fee for project manager </label>
                                </div>
                            </div>
                        </div>
                    

 <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label class="col-sm-3 col-lg-4 control-label">Default Currency<i class="red">*</i> </label>

                         <div class="col-sm-9 col-lg-6 controls">
                            <input type="text" class="form-control" name="default_currency" id="default_currency" data-rule-required="true" maxlength="10" value="{{isset($arr_site_settings['default_currency'])?$arr_site_settings['default_currency']:''}}" />
                            <span class='error'>{{ $errors->first('default_currency') }}</span>
                        </div>
                    </div> </div>


      <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label class="col-sm-3 col-lg-4 control-label">Default Currency Code<i class="red">*</i> </label>

                         <div class="col-sm-9 col-lg-6 controls">
                            <input type="text" class="form-control" name="default_currency_code" id="default_currency_code" data-rule-required="true" maxlength="10" value="{{isset($arr_site_settings['default_currency_code'])?$arr_site_settings['default_currency_code']:''}}" />
                            <span class='error'>{{ $errors->first('default_currency_code') }}</span>

                            <label class="control-label" style="text-align:left;">According to that price client paid fee for project manager </label>
                        </div>
                    </div> </div>



 <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label class="col-sm-3 col-lg-4 control-label">Project Manager Fix price ($)<i class="red">*</i> </label>

                         <div class="col-sm-9 col-lg-6 controls">
                            <input type="text" class="form-control" name="website_pm_initial_cost" id="website_pm_initial_cost" data-rule-required="true" data-rule-number="true" data-rule-min="1" value="{{isset($arr_site_settings['website_pm_initial_cost'])?$arr_site_settings['website_pm_initial_cost']:''}}" />
                            <span class='error'>{{ $errors->first('website_pm_initial_cost') }}</span>

                            <label class="control-label" style="text-align:left;">If project cost is less , then use fix price</label>
                        </div>
                    </div> </div>
     
     
 <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label class="col-sm-3 col-lg-4 control-label">Project Manager % price <i class="red">*</i> </label>
                         <div class="col-sm-9 col-lg-6 controls">
                            <input type="text" class="form-control" name="wesite_project_manager_commission" id="wesite_project_manager_commission" data-rule-required="true" data-rule-number="true" data-rule-min="1" data-rule-max="100" value="{{isset($arr_site_settings['wesite_project_manager_commission'])?$arr_site_settings['wesite_project_manager_commission']:''}}" />
                            <span class='error'>{{ $errors->first('wesite_project_manager_commission') }}</span>
                            <label class="control-label" style="text-align:left;">If project cost is greater , then use % price on total project cost</label>
                        </div>
                    </div> </div>
     
     
 <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label class="col-sm-3 col-lg-4 control-label">NDA Price ($) <i class="red">*</i> </label>
                         <div class="col-sm-9 col-lg-6 controls">
                            <input type="text" class="form-control" name="nda_price" id="nda_price" data-rule-required="true" data-rule-number="true" data-rule-min="1" data-rule-maxlength="9" value="{{isset($arr_site_settings['nda_price'])?$arr_site_settings['nda_price']:''}}" />
                            <span class='error'>{{ $errors->first('nda_price') }}</span>
                        </div>
                    </div> </div>
     
     
 <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label class="col-sm-3 col-lg-4 control-label">Urgent Price ($) <i class="red">*</i> </label>
                         <div class="col-sm-9 col-lg-6 controls">
                            <input type="text" class="form-control" name="urgent_price" id="urgent_price" data-rule-required="true" data-rule-number="true" data-rule-maxlength="9" data-rule-min="1" value="{{isset($arr_site_settings['urgent_price'])?$arr_site_settings['urgent_price']:''}}" />
                            <span class='error'>{{ $errors->first('urgent_price') }}</span>
                        </div>
                    </div> </div>
     
     
 <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label class="col-sm-3 col-lg-4 control-label">Private Price ($) <i class="red">*</i> </label>
                         <div class="col-sm-9 col-lg-6 controls">
                            <input type="text" class="form-control" name="private_price" id="private_price" data-rule-required="true" data-rule-number="true" data-rule-min="1" data-rule-maxlength="9" value="{{isset($arr_site_settings['private_price'])?$arr_site_settings['private_price']:''}}" />
                            <span class='error'>{{ $errors->first('private_price') }}</span>
                        </div>
                    </div> </div>
     
     
 <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label class="col-sm-3 col-lg-4 control-label">Recruiter Price ($) <i class="red">*</i> </label>
                         <div class="col-sm-9 col-lg-6 controls">
                            <input type="text" class="form-control" name="recruiter_price" id="recruiter_price" data-rule-required="true" data-rule-number="true" data-rule-maxlength="9" data-rule-min="1" value="{{isset($arr_site_settings['recruiter_price'])?$arr_site_settings['recruiter_price']:''}}" />
                            <span class='error'>{{ $errors->first('recruiter_price') }}</span>
                        </div>
                    </div> </div>
     
     
     



                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                            <input type="submit" value="Update" class="btn btn btn-primary">
                        </div>
                    </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Main Content -->

<script type="text/javascript">
    /*$('#wesite_project_manager_commission').keypress(function(eve) 
  {
     if (eve.which != 8 && eve.which != 0 && (eve.which < 48 || eve.which > 57)) 
    {
       eve.preventDefault();
    }
  });

 $('#website_pm_initial_cost').keypress(function(eve) 
  {
     if (eve.which != 8 && eve.which != 0 && (eve.which < 48 || eve.which > 57)) 
    {
       eve.preventDefault();
    }
  });*/

    /* Function for special characters not accepts.*/
</script>

@endsection