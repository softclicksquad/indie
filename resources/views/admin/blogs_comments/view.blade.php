
@extends('admin.layout.master')    
@section('main_content')
<!-- Page header -->
@include('admin.layout.breadcrumb')  
<!-- /page header -->


<style type="text/css">
.cropit-preview{
    background-color:#f8f8f8;
    background-size:cover;
    border:1px solid #ccc;
    border-radius:3px;
    margin-top:7px;
    width:690px;
    height:396px
}
.text-data {
    position: relative;
    top: 9px;
}
</style>
<div class="content">
	<div class="panel panel-flat">
		@include('admin.layout._operation_status')
		<div class="panel-heading page-name">
			<h5 class="panel-title">{{$page_title or ''}}</h5>
		</div>

		<div class="panel-body">
			<form class="form-horizontal" id="frm_add_front_page" name="frm_add_front_page" action="{{$module_url_path}}/store" method="post" enctype="multipart/form-data">
				{{csrf_field()}}
				<fieldset class="content-group">	
					<div class="row">
						<div class="col-lg-8">
							<div class="form-group">
								<label class="control-label col-sm-4 col-md-4 col-lg-3" for="name">Blog Image</label>
								<div class="col-sm-8 col-md-8 col-lg-9">
									<div class="image-editor">
										<input type="file" name="image-editor" class="cropit-image-input" style="display: none;" data-rule-required='true' id="image">
										@php 
										if(isset($arr_data['image']))
										{
										$image_url = $blog_image_public_path.$arr_data['image'];

										}
										else
										{
										$image_url = get_default_image(396,690,25,'No Advertisement' );
											
										}
										@endphp
										<div class="cropit-preview">
											<img class="cropit-preview-image" src="{{ $image_url }}" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-8">
							<div class="form-group">
								<label class="control-label col-sm-4 col-md-4 col-lg-3" for="name">Blog Name</label>
								<div class="col-sm-8 col-md-8 col-lg-9">
									<div class="text-data">{{ $arr_data['name'] or 'NA' }}</div>							
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-lg-8">
							<div class="form-group">
								<label class="control-label col-sm-4 col-md-4 col-lg-3" for="plan_description">Blog Category</label>
								<div class="col-sm-8 col-md-8 col-lg-9">
									<div class="text-data">{{ $arr_data['category']['name'] or 'NA' }}</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-8">
							<div class="form-group">
								<label class="control-label col-sm-4 col-md-4 col-lg-3" for="plan_description">Blog Description</label>
								<div class="col-sm-8 col-md-8 col-lg-9">
									<div class="text-data">{!! $arr_data['description'] or 'NA' !!}</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group text-right">
						<div class="col-lg-8">
							<a href="{{ $module_url_path or '' }}" class="btn gray-btn">Back</a>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
	@endsection


