@extends('admin.layout.master')    
@section('main_content')
<!-- Content area -->

<!-- BEGIN Page Title -->
<div class="page-title">
	<div>

	</div>
</div>
<!-- END Page Title -->

<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
		</li>
		<span class="divider">
			<i class="fa fa-angle-right"></i>
			<i class="fa fa-road"></i>
			<a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
		</span> 
		<span class="divider">
			<i class="fa fa-angle-right"></i>
			<i class="fa fa-road"></i>
		</span>
		<li class="active">{{ $page_title or ''}}</li>
	</ul>
</div>
<!-- END Breadcrumb -->

<!-- BEGIN Main Content -->

<div class="row">

	<div class="col-md-12">
		<div class="box">
			<div class="box-title">
				<h3>
					<i class="fa fa-road"></i>
					{{ isset($page_title)?$page_title:"" }}
				</h3>
				<div class="box-tool">
					<a data-action="collapse" href="#"></a>
					<a data-action="close" href="#"></a>
				</div>
			</div>
			<div class="box-content">
				@include('admin.layout._operation_status')

				<form name="frm_manage" id="frm_manage" method="POST" class="form-horizontal" action="{{url($module_url_path)}}/multi-action">
					{{ csrf_field() }}
					<input type="hidden" name="multi_action" value="" />

					<div class="table-responsive">
						<table class="table no-footer custome-table large-table" id="tbl_activitylog_listing">
							<thead>
								<tr class="border-solid" style="max-width: 10px">
									<th>
										<div class="check-box">
											<input type="checkbox" class="filled-in" name="selectall" id="select_all" onchange="chk_all(this)" />
											<label for="selectall">
												
											</label>
										</div>
									</th>

									<th {{-- style="width: 50%" --}}>
										<div class="dataTables_filter">Comment</div>
									</th>

									<th><div class="dataTables_filter">Comment By</div></th>

									<th><div class="dataTables_filter">Email</div></th>
									<th>Commnet On</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							@if(isset($arr_comments['data']) && !empty($arr_comments['data']))
								@foreach($arr_comments['data'] as $data)
									<tr>
										<td>
											<input type="checkbox" name="checked_record[]" value="{{base64_encode($data['id'])}}" />
										</td>
										<td>{{ $data['comment'] or '' }}</td>
										<td>{{ $data['user_name'] or '' }}</td>
										<td>{{ $data['user_email'] or '' }}</td>
										<td>{{isset($data['created_at']) && $data['created_at']!=""? date('d F, Y',strtotime($data['created_at'])):"NA"}}</td>
										<td>
											<a class='btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip' href="javascript:void(0)" title='Delete' onclick="javascript:return confirm_delete('{{ $module_url_path.'/delete/'.base64_encode($data['id'])}}')" ><i class='fa fa-trash' ></i></a>
										</td>
									</tr>
								@endforeach
							@else
							<h3>No data found</h3>
							@endif
							</tbody>
						</table>
					</div>
				</form>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
function confirm_delete(url) {
	alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
		if (e) {
			if(url != "" && url != undefined){
				showProcessingOverlay();
				window.location.href=url;
			}
			return true;
		} else {
			return false;
		}
	});
}
</script>
@endsection