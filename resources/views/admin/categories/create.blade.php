@extends('admin.layout.master')
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->

<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-desktop"></i>
      <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-list"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->

<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-text-width"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
            @include('admin.layout._operation_status') 
            <div class="tabbable">
               <form name="validation-form" id="validation-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/store" enctype="multipart/form-data"  files ="true">
                  {{ csrf_field() }}
                  <ul  class="nav nav-tabs">
                     @include('admin.layout._multi_lang_tab')
                  </ul>
                  <div id="myTabContent1" class="tab-content">
                     @if(isset($arr_lang) && sizeof($arr_lang)>0)
                        @foreach($arr_lang as $lang)
                        <div class="tab-pane fade {{ $lang['locale']=='en'?'in active':'' }}"
                           id="{{ $lang['locale'] }}">

                           @if($lang['locale'] == 'en') 
                              <div class="form-group">
                                  <label class="col-sm-3 col-lg-2 control-label"> Image <i class="red">*</i> </label>
                                  <div class="col-sm-9 col-lg-10 controls">
                                     <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new img-thumbnail" style="width: 70px; height: 70px;"></div>
                                        <div class="fileupload-preview fileupload-exists img-thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                        <div>
                                           <span class="btn btn-default btn-file">
                                              <span class="fileupload-new" >Select image</span> 
                                              <span class="fileupload-exists">Change</span>
                                              <input data-rule-required="true"  onChange="Upload()" type="file" name="image" id="ad_image" class="file-input">
                                           </span> 
                                           <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>
                                       <span><i class="file_err">Image should be in jpg,jpeg,png Format (60 X 60).</i></span> 
                                     </div>
                                      <span class='help-block'>{{ $errors->first('image') }}</span>  
                                  </div>
                              </div>  
                           @endif 
                           <div class="form-group">
                              <label class="col-sm-3 col-lg-2 control-label" for="page_title">Category Title<i class="red">*</i></label>
                              <div class="col-sm-6 col-lg-4 controls">
                                 @if($lang['locale'] == 'en') 
                                 <input type="text" name="category_title_{{$lang['locale']}}" class="form-control" value="{{old('category_title_'.$lang['locale'])}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Category Title">
                                 @else
                                 <input type="text" name="category_title_{{$lang['locale']}}" class="form-control" value="{{old('category_title_'.$lang['locale'])}}" placeholder="Category Title" data-rule-required="true" data-rule-maxlength="255">
                                 @endif    
                                 <span class='error'>{{ $errors->first('category_title_'.$lang['locale']) }}</span>
                              </div>
                           </div>
                           
                        </div>
                        @endforeach
                     @endif
                  </div>
                  <br>
                  <div class="form-group">
                     <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                        <input type="submit" value="Save" class="btn btn btn-primary">
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
function Upload() {
    $('.file_err').css('color','black');
    //Get reference of FileUpload.
    var ad_image = document.getElementById("ad_image");
    //Check whether the file is valid Image.
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+()$");
    if (regex.test(ad_image.value.toLowerCase())) {
        //Check whether HTML5 is supported.
        if (typeof (ad_image.files) != "undefined") {
            var file_extension = ad_image.value.substring(ad_image.value.lastIndexOf('.')+1, ad_image.length); 
            if(file_extension != 'jpg' && file_extension != 'jpeg' && file_extension != 'png'){
               setTimeout(function(event){
                  $('a.fileupload-exists').click();
               },200); 
               $('.file_err').css('color','red');
               $('.file_err').html("Image should be in jpg,jpeg,png Format (60 X 60).");
               return false;
            } 
            //Initiate the FileReader object.
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(ad_image.files[0]);
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();
                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;
                //Validate the File Height and Width.
                image.onload = function () {
                    var height = this.height;
                    var width  = this.width;
                     if (height != 60 && width != 60) {
                        setTimeout(function(){
                           $('a.fileupload-exists').click();
                        },200); 
                        $('.file_err').css('color','red');
                        $('.file_err').html("Image should be in jpg,jpeg,png Format (60 X 60).");
                        event.preventDefault(); 
                        return false;
                    }
                    $('.file_err').css('color','green');
                    return true;
                };
            }
        } else {
            $('.file_err').css('color','red');
            $('.file_err').html("This browser does not support HTML5.");
            return false;
        }
    } else {
        $('.file_err').css('color','red');
        $('.file_err').html("Image should be in jpg,jpeg,png Format (60 X 60)");
        return false;
    }
}
</script>
@stop
