@extends('admin.layout.master')                
    @section('main_content')
    <!-- BEGIN Page Title -->
    <link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/data-tables/latest/dataTables.bootstrap.min.css">
    <div class="page-title">
      <div></div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
       <ul class="breadcrumb">
          <li>
             <i class="fa fa-home"></i>
             <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
          </li>
          <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-star"></i>
          <a href="javascript:void(0);">{{ $module_title or ''}}</a>
          </span> 
          <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-star"></i>
          </span>
          <li class="active">{{ $page_title or ''}}</li>
       </ul>
    </div>
    <!-- END Breadcrumb -->
    <!-- BEGIN Main Content -->
    <div class="row">
      <div class="col-md-12">

          <div class="box">
            <div class="box-title">
              <h3>
                <i class="fa fa-star"></i>
                {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
                <a data-action="collapse" href="#"></a>
                <a data-action="close" href="#"></a>
            </div>
        </div>
        <div class="box-content">
          @include('admin.layout._operation_status')
          <form name="frm_manage" id="frm_manage" method="POST" class="form-horizontal" action="{{$module_url_path}}/multi_action">
          {{ csrf_field() }}
          <div class="btn-toolbar pull-right clearfix">
           <!--  <div class="btn-group">    
             <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                title="Multiple Delete" 
                href="javascript:void(0);" 
                onclick="javascript : return check_multi_action('frm_manage','delete');"  
                style="text-decoration:none;">
             <i class="fa fa-trash-o"></i>
             </a>
          </div> -->
          <div class="btn-group"> 
             <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                title="Refresh" 
                onclick="window.location.reload()" 
                style="text-decoration:none;">
             <i class="fa fa-repeat"></i>
             </a> 
          </div>
          </div>
          <br/><br/>
          <div class="clearfix"></div>
          <div class="table-responsive" style="border:0">
            <input type="hidden" name="multi_action" value="" />
            <table class="table table-advance"  id="table1" >
              <thead>
                <tr>
                 <th>Project Name</th> 
                  <th>Cost</th>
                  <th>Client</th>
                  <th>Expert Name</th>
                  <th>Options</th>
                </tr>
              </thead>
              <tbody>
                @if(isset($arr_project_data) && sizeof($arr_project_data)>0)
                  @foreach($arr_project_data as $project)
                  <tr>
                    <td> 
                      {{ isset($project['project_name'])?str_limit($project['project_name'],$limit = 50, $end='...'):'' }}
                    </td> 
                    <td> 
                      {{ isset($project['project_cost'])?$project['project_cost']:'' }}
                    </td>
                    <td>
                      {{ isset($project['client_info']['first_name'])?$project['client_info']['first_name']:'' }}&nbsp;{{ isset($project['client_info']['last_name'])?$project['client_info']['last_name']:'' }}
                    </td>
                    <td>
                      {{ isset($project['expert_info']['first_name'])?$project['expert_info']['first_name']:'' }}&nbsp;{{ isset($project['expert_info']['last_name'])?$project['expert_info']['last_name']:'' }}
                    </td>
                    <td>
                        <a class="btn btn-primary" href="{{ $module_url_path }}/all/{{ base64_encode($project['id']) }}"  title="Milestones">
                          Milestones
                        </a>
                     </a>
                     </td>
                  </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
          </div>
        </form>
      </div>
  </div>
</div>
</div>
<!-- END Main Content -->
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
   function confirm_delete(url) {
      alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
          if (e) {
              if(url != "" && url != undefined){
                showProcessingOverlay();
                window.location.href=url;
              }
              return true;
          } else {
              return false;
          }
      });
   }
   function check_multi_action(frm_id,action){
     var frm_ref = jQuery("#"+frm_id);
     if(jQuery(frm_ref).length && action!=undefined && action!=""){
       if(action == 'delete'){
            if (!confirm_delete()){
                return false;
            }
       }
       /* Get hidden input reference */
       var input_multi_action = jQuery('input[name="multi_action"]');
       if(jQuery(input_multi_action).length){
         /* Set Action in hidden input*/
         jQuery('input[name="multi_action"]').val(action);
         /*Submit the referenced form */
         jQuery(frm_ref)[0].submit();
       } else {
         console.warn("Required Hidden Input[name]: multi_action Missing in Form ")
       }
     } else {
         console.warn("Required Form[id]: "+frm_id+" Missing in Current category ")
     }
   }
</script>
@stop                    


