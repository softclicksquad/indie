@extends('admin.layout.master')                
    @section('main_content')
    <!-- BEGIN Page Title -->
    <link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/data-tables/latest/dataTables.bootstrap.min.css">
    <div class="page-title">
      <div></div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
       <ul class="breadcrumb">
          <li>
             <i class="fa fa-home"></i>
             <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
          </li>
          <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-star"></i>
          <a href="javascript:void(0);">{{ $module_title or ''}}</a>
          </span> 
          <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-star"></i>
          </span>
          <li class="active">{{ $page_title or ''}}</li>
       </ul>
    </div>
    <!-- END Breadcrumb -->
    <!-- BEGIN Main Content -->
    <div class="row">
      <div class="col-md-12">
          <div class="box">
            <div class="box-title">
              <h3>
                <i class="fa fa-star"></i>
                {{ isset($page_title)?$page_title:"" }}
              </h3>
            <div class="box-tool">
              <a data-action="collapse" href="#"></a>
              <a data-action="close" href="#"></a>
            </div>
        </div>
        <div class="box-content">
          @include('admin.layout._operation_status')
          <div id="_operation_status"></div>
          <form name="frm_manage" id="frm_manage" method="POST" class="form-horizontal" action="{{$module_url_path}}/multi_action">
          {{ csrf_field() }}
          <div class="btn-toolbar pull-right clearfix">
            <div class="btn-group"> 
               <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                  title="Refresh" 
                  href="{{url('/admin/milestones/refund/requests')}}" 
                  style="text-decoration:none;">
                  <i class="fa fa-repeat"></i>
               </a> 
            </div>
          </div>
          <br/><br/>
          <div class="clearfix"></div>
          <div class="table-responsive" style="border:0">
            <input type="hidden" name="multi_action" value="" />
            <table class="table table-advance"  id="table1" >
              <thead>
                <tr>
                  <th>Project Name</th> 
                  <th>Milestone Transaction ID</th>
                  <th>Milestone</th> 
                  <th>Milestone amount</th>
                  <th>Refund amount</th>
                  <th>Client</th>
                  <th>Refund reason</th>
                  <th>Milestone created</th>
                  <th>Refund requested</th>
                  <th>Options</th>
                </tr>
              </thead>
              <tbody>
                @if(isset($refund_requests) && sizeof($refund_requests)>0)
                  @foreach($refund_requests as $requests)
                  <tr>
                    <td> 
                      <a href="{{url('/')}}/admin/projects/show/{{ isset($requests['project_id'])?base64_encode($requests['project_id']):base64_encode('0') }}" target="_BLANK">{{ isset($requests['project_details']['project_name'])?$requests['project_details']['project_name']:'' }}</a>
                    </td>
                    <td> 
                      {{ isset($requests['milestone_details']['TransactionId'])?$requests['milestone_details']['TransactionId']:'-' }}
                    </td>
                    <td> 
                      <a href="{{url('/')}}/admin/project_milestones/show/{{ isset($requests['milestone_id'])?base64_encode($requests['milestone_id']):base64_encode('0') }}" target="_BLANK">{{ isset($requests['milestone_details']['title'])?$requests['milestone_details']['title']:'' }}</a>
                    </td>
                    <td> 
                      {{ isset($requests['milestone_details']['cost'])?$requests['milestone_details']['cost']:'0' }} {{'USD'}}
                    </td>
                    <td> 
                      {{ isset($requests['milestone_details']['cost'])?$requests['milestone_details']['cost']:'0' }} {{'USD'}}
                    </td>
                    <td> 
                      {{ isset($requests['project_details']['client_details']['role_info']['first_name'])?$requests['project_details']['client_details']['role_info']['first_name']:'' }} 
                      {{ isset($requests['project_details']['client_details']['role_info']['last_name'])?str_limit($requests['project_details']['client_details']['role_info']['last_name'],$limit = 1, $end='.'):'' }}
                    </td>
                    <td> 
                      {{ isset($requests['Refund_reason'])?$requests['Refund_reason']:'-' }}
                    </td>
                    <td> 
                      {{ isset($requests['milestone_details']['created_at'])?time_ago($requests['milestone_details']['created_at']):'-' }}
                    </td> 
                    <td> 
                      {{ isset($requests['created_at'])?time_ago($requests['created_at']):'-' }}
                    </td>
                    <td> 
                      @if(isset($requests['is_refunded']) && $requests['is_refunded'] == 'no')
                      <a class="btn btn-primary btn-xs mangopay-widget refund"
                         data-refundid              = "{{isset($requests['id'])?$requests['id']:'0'}}"
                         data-milestoneid           = "{{isset($requests['milestone_id'])?$requests['milestone_id']:'0'}}"
                         data-invoice_id            = "{{isset($requests['milestone_details']['invoice_id'])?$requests['milestone_details']['invoice_id']:'0'}}"
                         data-project_id            = "{{isset($requests['project_id'])?$requests['project_id']:'0'}}"
                         data-authorid              = "{{isset($requests['milestone_details']['AuthorId'])?$requests['milestone_details']['AuthorId']:'0'}}"
                         data-client_user_id              = "{{isset($requests['milestone_details']['client_user_id'])?$requests['milestone_details']['client_user_id']:'0'}}"
                         data-transactionid         = "{{isset($requests['milestone_details']['TransactionId'])?$requests['milestone_details']['TransactionId']:'0'}}" 
                         data-mp_transaction_status = "{{isset($requests['milestone_details']['mp_transaction_status'])?$requests['milestone_details']['mp_transaction_status']:''}}" 
                         data-mp_transaction_type   = "{{isset($requests['milestone_details']['mp_transaction_type'])?$requests['milestone_details']['mp_transaction_type']:''}}"
                         data-refund_amount="{{ isset($requests['milestone_details']['cost'])?$requests['milestone_details']['cost']:'0' }}"
                      > Refund </a>
                      @elseif(isset($requests['is_refunded']) && $requests['is_refunded'] == 'yes')
                      <a class="btn btn-primary btn-xs mangopay-widget" style="background-color:grey;cursor:no-drop;">
                        Refunded 
                      </a>
                      <br>
                        {{ isset($requests['updated_at'])?time_ago($requests['updated_at']):'-' }}<br>
                        Refund Transaction Id : {{ isset($requests['InitialTransactionId'])?$requests['InitialTransactionId']:'-' }}<br>
                        Author Id : {{ isset($requests['AuthorId'])?$requests['AuthorId']:'-' }}<br>
                      @endif
                    </td>
                  </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $(document).on('click','.refund',function(){
        var AuthorId              = $(this).data('authorid'); 
        var TransactionId         = $(this).data('transactionid'); 
        var mp_transaction_status = $(this).data('mp_transaction_status'); 
        var mp_transaction_type   = $(this).data('mp_transaction_type'); 
        var refund_amount         = $(this).data('refund_amount'); 
        var refundid              = $(this).data('refundid'); 
        var milestoneid           = $(this).data('milestoneid'); 
        var invoice_id            = $(this).data('invoice_id'); 
        var project_id            = $(this).data('project_id'); 
        var client_user_id        = $(this).data('client_user_id'); 
        alertify.confirm("Are you sure? You want to refund this milestone", function (e) {
            if (e) {
                var token                 = '{{csrf_token()}}';
                var site_url              = '{{url("/")}}';
                showProcessingOverlay();
                $.ajax({
                   'method':"POST",
                   'url':site_url+'/admin/milestones/refund/process',
                   'data':{_token:token,
                            AuthorId:AuthorId,
                            mp_transaction_type:mp_transaction_type,
                            refund_amount:refund_amount,
                            mp_transaction_status:mp_transaction_status,
                            refundid:refundid,
                            milestoneid:milestoneid,
                            invoice_id:invoice_id,
                            project_id:project_id,
                            client_user_id:client_user_id,
                            TransactionId:TransactionId
                          },
                   success:function(result){
                     hideProcessingOverlay();
                     $('#_operation_status').html(result);
                   }
                });
                return true;
            } else {
                return false;
            }
        });
    })
  });
</script>
@stop                    


