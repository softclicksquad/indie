@extends('admin.layout.master')                

    @section('main_content')

    <style type="text/css">
    .add-padding {
      padding: 3px;
      width: 150px;
    }
    </style>


    <!-- BEGIN Page Title -->
    <link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/data-tables/latest/dataTables.bootstrap.min.css">
    <div class="page-title">
        <div>

        </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
       <ul class="breadcrumb">
          <li>
             <i class="fa fa-home"></i>
             <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
          </li>
          <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-star"></i>
          <a href="{{$module_url_path}}">{{ $page_title }}</a>
          </span> 
          <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-star"></i>
          </span>
          <li class="active">
                @if(isset($arr_milestones) && sizeof($arr_milestones)>0)
                  @foreach($arr_milestones as $milestone)
                    {{ isset($milestone['project_details']['project_name'])?str_limit($milestone['project_details']['project_name'],50):''  }}&nbsp;{{ $module_title or ''}}
                    @break
                  @endforeach
                @endif  
          </li>
       </ul>
    </div>
    <!-- END Breadcrumb -->

    <!-- BEGIN Main Content -->
    <div class="row">
      <div class="col-md-12">

          <div class="box">
            <div class="box-title">
              <h3>
                <i class="fa fa-star"></i>
                @if(isset($arr_milestones) && sizeof($arr_milestones)>0)
                  @foreach($arr_milestones as $milestone)
                    {{ isset($milestone['project_details']['project_name'])?str_limit($milestone['project_details']['project_name'],50):''  }}&nbsp;{{ $module_title or ''}}
                    @break
                  @endforeach
                @endif  
            </h3>
            <div class="box-tool">
                <a data-action="collapse" href="#"></a>
                <a data-action="close" href="#"></a>
            </div>
        </div>
        <div class="box-content">
          
          @include('admin.layout._operation_status')
          
          <form name="frm_manage" id="frm_manage" method="POST" class="form-horizontal" action="{{$module_url_path}}/multi_action">
               {{ csrf_field() }}

            
          <div class="btn-toolbar pull-right clearfix">
           <!--  <div class="btn-group">    
             <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                title="Multiple Delete" 
                href="javascript:void(0);" 
                onclick="javascript : return check_multi_action('frm_manage','delete');"  
                style="text-decoration:none;">
             <i class="fa fa-trash-o"></i>
             </a>
          </div> -->
          <div class="btn-group"> 
             <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                title="Refresh" 
                href="javascript:void(0)"
                style="text-decoration:none;">
             <i class="fa fa-repeat"></i>
             </a> 
          </div>
          
          </div>
          <br/><br/>
          <div class="clearfix"></div>
          <div class="table-responsive" style="border:0">

            <input type="hidden" name="multi_action" value="" />

            <table class="table table-advance"  id="table1" >
              <thead>
                <tr>

                  <th>Expert</th>
                  <th>Milestone Title</th>
                  <th>Milestone Cost</th>
                  <th>Subscription <br/>Pack</th>
                  <th>Website <br/> Commission in (%)</th>                    
                  <th>Website <br/> Commission</th>                    
                  <th>Cost From<br/> Client</th>                    
                  <th>Cost To<br/> Expert</th>                    
                  <th>Project Manager <br/>Commission</th>                    
                  <th>Release Status</th>
                  {{--<th>Make Payment</th>--}}
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @if(isset($arr_milestones) && sizeof($arr_milestones)>0)
                  @foreach($arr_milestones as $milestone)
                  @if(isset($milestone['transaction_details']['payment_status']) && ($milestone['transaction_details']['payment_status']=='1' || $milestone['transaction_details']['payment_status']=='2'))
                  @if(isset($milestone['milestone_release_details']) && $milestone['milestone_release_details'] != null )
                  <tr>
                    <td>{{ isset($milestone['expert_details']['first_name'])?$milestone['expert_details']['first_name']:'-'}} {{ isset($milestone['expert_details']['last_name'])?$milestone['expert_details']['last_name']:''}}</td>
                    <td>{{ isset($milestone['title'])?str_limit(ucfirst($milestone['title']),$limit = 30, $end='...'):''}}</td>
                    <td>{{ isset($milestone['project_details']['project_currency'])?$milestone['project_details']['project_currency']:''  }}&nbsp;{{ isset($milestone['cost'])?$milestone['cost']:''  }}</td>
                    <td>{{ isset($milestone['expert_subscription']['pack_name'])?str_limit(ucfirst($milestone['expert_subscription']['pack_name'])):''}}</td>
                    <td>{{ isset($milestone['expert_subscription']['website_commision'])?$milestone['expert_subscription']['website_commision']:'0'}}&nbsp;%</td>
                    <td>{{ isset($milestone['project_details']['project_currency'])?$milestone['project_details']['project_currency']:''  }}&nbsp;{{ isset($milestone['cost_website_commission'])?$milestone['cost_website_commission']:''  }}</td>
                    <td>{{ isset($milestone['project_details']['project_currency'])?$milestone['project_details']['project_currency']:''  }}&nbsp;{{ isset($milestone['cost_from_client'])?$milestone['cost_from_client']:''  }}</td>
                    <td>{{ isset($milestone['project_details']['project_currency'])?$milestone['project_details']['project_currency']:''  }}&nbsp;{{ isset($milestone['cost_to_expert'])?$milestone['cost_to_expert']:''  }}</td>
                    <td>{{ isset($milestone['project_details']['project_currency'])?$milestone['project_details']['project_currency']:''  }}&nbsp;{{ isset($milestone['cost_project_manager_commission'])?$milestone['cost_project_manager_commission']:''  }}</td>
                    <td>
                          @if($milestone['milestone_release_details']['status'] == '0')
                              <span class="badge badge-xlarge badge-warning">Requested By Expert</span>
                          @elseif($milestone['milestone_release_details']['status'] == '1')
                              <span class="badge badge-xlarge badge-success">Approved By Client</span>
                          @elseif($milestone['milestone_release_details']['status'] == '2')
                              <span class="badge badge-xlarge badge-pink">Paid to Expert</span>
                          @endif
                    </td>
                    <td>
                      <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" href="{{ $module_url_path.'/show/'.base64_encode($milestone['id']) }}" data-original-title="View">
                        <i class="fa fa-eye" ></i>
                      </a>
                    </td>
                  </tr>
                   @endif
                  @endif
                 @endforeach
                @endif
              </tbody>
            </table>
          </div>
        </form>
      </div>
  </div>
</div>
</div>
<!-- END Main Content -->
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
   function confirm_delete(url) {
      alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
          if (e) {
              if(url != "" && url != undefined){
                showProcessingOverlay();
                window.location.href=url;
              }
              return true;
          } else {
              return false;
          }
      });
   }
   function check_multi_action(frm_id,action){
     var frm_ref = jQuery("#"+frm_id);
     if(jQuery(frm_ref).length && action!=undefined && action!=""){
       if(action == 'delete'){
            if (!confirm_delete()){
                return false;
            }
       }
       /* Get hidden input reference */
       var input_multi_action = jQuery('input[name="multi_action"]');
       if(jQuery(input_multi_action).length){
         /* Set Action in hidden input*/
         jQuery('input[name="multi_action"]').val(action);
         /*Submit the referenced form */
         jQuery(frm_ref)[0].submit();
       } else {
         console.warn("Required Hidden Input[name]: multi_action Missing in Form ")
       }
     } else {
         console.warn("Required Form[id]: "+frm_id+" Missing in Current category ")
     }
   }
</script>
<script type="text/javascript">
</script>
@stop                    


