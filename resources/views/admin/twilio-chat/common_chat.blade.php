@extends('admin.layout.master')            
@section('main_content')

<link href="{{url('/public')}}/front/css/twillo-chat.css" rel="stylesheet" type="text/css" />

<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-comment"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-comment"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
         </div>
         <div class="box-content">
            @include('admin.layout._operation_status')

            @php
                $enc_parent_id = 0;
                $enc_sub_id    = isset($arr_data['id']) ? $arr_data['id'] : 0;
                if(isset($is_project_chat) && $is_project_chat == '1') {
                    $enc_parent_id = isset($arr_data['project_id']) ? $arr_data['project_id'] : 0;
                } elseif(isset($is_contest_chat) && $is_contest_chat == '1') {
                    $enc_parent_id = isset($arr_data['contest_id']) ? $arr_data['contest_id'] : 0;
                }

                $chat_type = '';
                $channel_sid = isset($obj_channel_resource->sid) ? $obj_channel_resource->sid : '';
                if( isset($is_project_chat) && $is_project_chat == '1') {
                  $chat_type = 'project';
                } else if( isset($is_contest_chat) && $is_contest_chat == '1') {
                  $chat_type = 'contest';
                }
            @endphp
            
            <div id="frame">
              <div id="sidepanel">
                @if( isset($arr_chat_list) && count($arr_chat_list)>0 )
                  @foreach( $arr_chat_list as $chat_list )
                    
                    @php
                      $expanded_class = $online_class = '';
                      $online_style = 'border: none;';
                      if(isset($chat_list['id']) && isset($enc_parent_id) && $enc_parent_id == $chat_list['id'] ){
                        $expanded_class = 'expanded';
                        $online_class   = 'online';
                        $online_style   = '';
                      }
                    @endphp            
                      <div id="profile" class="{{ $expanded_class }}">
                          <div class="wrap">
                            <img id="profile-img" src="{{ url('/public/front/images/default_group_chat.png') }}" class="{{ $online_class }}" style="{{  $online_style }}" alt="">
                            <p>{{ isset($chat_list['group_name']) ? str_limit($chat_list['group_name'],20) : '' }}</p>
                            <i class="fa fa-chevron-down expand-button" aria-hidden="true"></i>
                            @if( isset($chat_list['chat_list']) && count($chat_list['chat_list'])>0 )
                              <div id="expanded" class="sub-chat-list">
                                @foreach( $chat_list['chat_list'] as $key => $list )

                                @php
                                    $bg_color = '';
                                    if(isset($list['project_bid_id']) && isset($enc_sub_id) && $enc_sub_id == $list['project_bid_id'] ){
                                      $bg_color = 'background: #21568c;';
                                    }             
                                @endphp
                                  <a href="{{ isset($list['redirect_url']) ? $list['redirect_url'] : 'javascript:void(0);' }}"> <div style="{{ $bg_color  }}">  {{ isset($list['chat_name']) ? $list['chat_name'] : '' }}  </div> </a>  
                                @endforeach
                              </div>
                            @endif
                          </div>
                      </div>

                  @endforeach
                @endif
              </div>

               <div class="content">
                  <div class="contact-profile">
                     <img src="{{ url('/public/front/images/default_group_chat.png') }}" alt="" />
                     <p>{{ isset($arr_chat_user['client_user_name']) ? $arr_chat_user['client_user_name'] : '' }} | {{ isset($arr_chat_user['expert_user_name']) ? $arr_chat_user['expert_user_name'] : '' }}</p>
                  </div>
                  <div class="messages">
                     <ul>
                        
                        @if(isset($obj_message_list) && count($obj_message_list)>0)
                          @foreach( $obj_message_list as $message )
                            @php
                                $created_date = isset($message->time_ago) ? $message->time_ago : '';
                            @endphp

                            @if( isset($message->from) && isset($from_user_id) && $message->from == $from_user_id) 
                                <li class="replies">
                                  <img src="{{ isset($message->profile_image) ? $message->profile_image : url('/public/uploads/front/profile/default_profile_image.png') }}" alt="" />
                                  <p><strong>{{ isset($message->body) ? $message->body : '' }}</strong> <br> <em>{{ isset($created_date) ? $created_date : '' }}</em></p>
                                </li>    
                            @else
                                <li class="sent">
                                  <img src="{{ isset($message->profile_image) ? $message->profile_image : url('/public/uploads/front/profile/default_profile_image.png') }}" alt="" />
                                  <p><em>{{ isset($message->from_user_name) ? $message->from_user_name : '' }}</em> <br> <strong>{{ isset($message->body) ? $message->body : '' }}</strong> <br> <em>{{ isset($created_date) ? $created_date : '' }}</em></p>
                                </li>
                            @endif
                          @endforeach
                        @endif
                     </ul>
                  </div>
                  <div class="message-input">
                     <div class="wrap">
                        <input type="text" placeholder="Write your message..." />
                        <i class="fa fa-paperclip attachment" aria-hidden="true"></i>
                        <button class="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                     </div>
                  </div>
               </div>
          </div>


         </div>
      </div>
   </div>
</div>
<!-- END Main Content -->
<input type="hidden" name="arr_chat_user" id="arr_chat_user" value=" {{ isset($arr_chat_user) ? json_encode($arr_chat_user) : '' }} " >

<script src="https://media.twiliocdn.com/sdk/js/chat/releases/4.0.0/twilio-chat.min.js"></script>
<script type="text/javascript">
  $(document).ready(function () {
    setTimeout(function(){
      showProcessingOverlay();
    },100)
  });

  var arr_chat_user = [];
  if($('#arr_chat_user').val()!='') {
    arr_chat_user = JSON.parse($('#arr_chat_user').val());
  }

  var chatClient;
  var projectChannel;
  var accessToken          = '{{ isset($access_token) ? $access_token : '' }}';
  var fromUserId           = '{{ isset($from_user_id) ? $from_user_id : '' }}';
  var channelResourceSid   = '{{ isset($channel_resource_sid) ? $channel_resource_sid : '' }}';
  var fromUserProfileImage = '{{ url('/public/front/images/default_admin_image.jpg') }}';
  var toUserProfileImage   = '{{ isset($to_user_profile_image) ? $to_user_profile_image : url('/public/uploads/front/profile/default_profile_image.png') }}';
  var timezoneName         = '{{ isset($login_user_timezone) ? $login_user_timezone : config('app.timezone') }}';
  
  Twilio.Chat.Client.create(accessToken).then(client => {
    chatClient = client;

    chatClient.getSubscribedChannels().then(createOrJoinProjectChannel);

    // when the access token is about to expire, refresh it
    chatClient.on('tokenAboutToExpire', function() {
      refreshAccessToken();
    });

    // if the access token already expired, refresh it
    chatClient.on('tokenExpired', function() {
      refreshAccessToken();
    });

  }).catch(error => {
      console.error(error);
      console.log('There was an error creating the chat client:<br/>' + error, true);
      console.log('Please check your .env file.', false);
      hideProcessingOverlay();
  });

  $(document).ready(function() {
      setTimeout(function(){
        $('.messages').scrollTop($('.messages')[0].scrollHeight);
        hideProcessingOverlay();
      },1000);
  });

  $(".expand-button").click(function() {
    if($(this).parent().parent().hasClass('expanded')){
      $(this).parent().parent().removeClass('expanded');
    } else {
      $(this).parent().parent().addClass('expanded');
    }
  });

  function refreshAccessToken() {
    if( fromUserId != '') {
      $.ajax({
        url :"{{url('/admin/twilio-chat/refresh_token')}}",
        method :"GET",
        success :function(response){
          if(response.status == 'success'){
            chatClient.updateToken(response.status.access_token);
          } else {
            alert(response.msg);
            setTimeout(function(){
              location.reload();
            },5000);
            return false;
          }
        }
      })
    }
  }

  function createOrJoinProjectChannel() {
    // Get the general chat channel, which is where all the messages are
    // sent in this simple application
    console.log('Attempting to join '+ channelResourceSid +' chat channel...');
    chatClient.getChannelBySid(channelResourceSid)
      .then(function(channel) {
        projectChannel = channel;
        console.log('Found '+ channelResourceSid +' channel:');
        setupProjectChannel();
        hideProcessingOverlay();
      }).catch(function() {
        // If it doesn't exist, let's create it
        console.log('Creating '+ channelResourceSid +' channel');
        chatClient.createChannel({
          uniqueName: channelResourceSid,
          friendlyName: channelResourceSid + ' Chat Channel'
        }).then(function(channel) {
          console.log('Created '+ channelResourceSid +' channel:');
          projectChannel = channel;
          setupProjectChannel();
          hideProcessingOverlay();
        }).catch(function(channel) {
          console.log('Channel could not be created:');
          hideProcessingOverlay();
        });
    });
  }

  // Set up channel after it has been found
  function setupProjectChannel() {
    // Listen for new messages sent to the channel
    projectChannel.on('messageAdded', function(message) {
      printMessage(message.author, message.body,message.dateCreated);
    });
  }
  
  function printMessage(fromUser, message, str_date) {
    var class_name = 'sent';
    var profile_image = toUserProfileImage;
    var to_user_name = ''
    if(fromUserId == fromUser) {
      class_name = 'replies';
      profile_image = fromUserProfileImage;
    } else {
      class_name = 'replies';
      profile_image = fromUserProfileImage;
      to_user_name = '';
      if(arr_chat_user.expert_user_id != undefined && arr_chat_user.expert_user_id == fromUser) {
        class_name = 'sent';
        profile_image = arr_chat_user.expert_profile_image;
        to_user_name = '<em>' + arr_chat_user.expert_user_name + '</em> <br>';
      } else if(arr_chat_user.client_user_id != undefined && arr_chat_user.client_user_id == fromUser) {
        class_name = 'sent';
        profile_image = arr_chat_user.client_profile_image;
        to_user_name = '<em>' + arr_chat_user.client_user_name + '</em> <br>';
      }  else if(arr_chat_user.admin_user_id != undefined && arr_chat_user.admin_user_id == fromUser) {
        class_name = 'sent';
        profile_image = arr_chat_user.admin_profile_image;
        to_user_name = '<em>' + arr_chat_user.admin_user_name + '</em> <br>';
      }
    }
    $('<li class="'+ class_name +'"><img src="'+ profile_image +'" alt="" /><p> ' + to_user_name + ' <strong>' + message + '</strong><br/><em> '+ convertTimezoneAndFormatDate(str_date.toString(), timezoneName) +' </em></p></li>').appendTo($('.messages ul'));
    $('.messages').scrollTop($('.messages')[0].scrollHeight);
  }


  
  function convertTimezoneAndFormatDate(date, tzString) {

    return 'just now';
    if(!typeof date === "string") {
      return '';
    }

    var options = { timeZone: tzString, weekday: 'short', year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };
    var str_date = new Date(date).toLocaleString("en-US", options);
    if(str_date == ''){
      return '';
    }

    var arr_data = str_date.split(",");
    if(arr_data[0]!=undefined && arr_data[1]!=undefined && arr_data[3]!=undefined ) {
      return arr_data[0] + ', ' + arr_data[1] + ' ' + arr_data[3]; 
    }
    return '';   
  }

  function newMessage() {
          
    if (projectChannel === undefined) {
      alert('The Chat Service is not configured property. Please reload the page.');
      return;
    }

    var message = $(".message-input input").val();
    if($.trim(message) == '') {
      return false;
    }
          
    $('.message-input input').val(null);
    projectChannel.sendMessage(message)
  };
         
  $('.submit').click(function() {
    newMessage();
  });
         
  $(window).on('keydown', function(e) {
    if (e.which == 13) {
      newMessage();
      return false;
    }
  });

</script>
@stop
