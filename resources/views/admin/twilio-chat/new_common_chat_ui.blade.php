
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('/public/twilio-chat')}}/assets/images/favicon.png">
    <title>{{ isset($page_title) ? $page_title : config('app.project.name') . ' Chat ' }}</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{url('/public/twilio-chat')}}/assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Animation CSS -->
    <link href="{{url('/public/twilio-chat')}}/assets/css/animate.css" rel="stylesheet">
    <link href="{{url('/public/twilio-chat')}}/assets/css/custom.css" rel="stylesheet" id="style">
    <!-- Theme light dark version CSS -->
    <link href="{{url('/public/twilio-chat')}}/assets/css/style.css" rel="stylesheet">
    <link href="{{url('/public/twilio-chat')}}/assets/css/style-light.css"  id="maintheme" rel="stylesheet">
    <!-- color CSS you can use different color css from css/colors folder -->
    <!-- We have chosen the skin-blue (blue.css) for this starter
         page. However, you can choose any other skin from folder css / colors .
    -->
    <link href="{{url('/public/twilio-chat')}}/assets/css/colors/green.css" id="theme"  rel="stylesheet">
    <!-- Emoji One JS -->
    <link rel="stylesheet" href="{{url('/public/twilio-chat')}}/smiley/assets/sprites/emojione.sprites.css"/>
    <script src="{{url('/public/twilio-chat')}}/smiley/js/emojione.min.js"></script>
    <script type="text/javascript">
        var twilio_chat_base_url = '{{url('/public/twilio-chat')}}';
        // #################################################
        // # Optional

        // default is PNG but you may also use SVG
        emojione.imageType = 'png';
        emojione.sprites = false;

        // default is ignore ASCII smileys like :) but you can easily turn them on
        emojione.ascii = true;

        // if you want to host the images somewhere else
        // you can easily change the default paths
        /*emojione.imagePathPNG = twilio_chat_base_url + '/smiley/assets/png/';
        emojione.imagePathSVG = twilio_chat_base_url + '/smiley/assets/svg/';*/

        // #################################################
    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<body>

<!-- Preloader -->

<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>

@php
    $enc_parent_id = 0;
    $enc_sub_id    = isset($arr_data['id']) ? $arr_data['id'] : 0;
    if(isset($is_project_chat) && $is_project_chat == '1') {
        $enc_parent_id = isset($arr_data['project_id']) ? $arr_data['project_id'] : 0;
    } elseif(isset($is_contest_chat) && $is_contest_chat == '1') {
        $enc_parent_id = isset($arr_data['contest_id']) ? $arr_data['contest_id'] : 0;
    }

    $is_group_chat = (isset($obj_channel_resource->membersCount) && $obj_channel_resource->membersCount > 2 ) ? '1' : '0';
    $chat_type = '';
    $channel_sid = isset($obj_channel_resource->sid) ? $obj_channel_resource->sid : '';
    if( isset($is_project_chat) && $is_project_chat == '1') {
      $chat_type = 'project';
    } else if( isset($is_contest_chat) && $is_contest_chat == '1') {
      $chat_type = 'contest';
    }
@endphp

<div id="wchat">
    <div class="wchat-wrapper wchat-wrapper-web wchat-wrapper-main">
        <div tabindex="-1" class="wchat two">
           
            <!-- .chat-left-panel -->
            <div id="side" class="wchat-list wchat-one chat-left-aside left">
                <div class="open-panel"><i class="ti-angle-right"></i></div>
                <div class="chat-left-inner">
                    <div id="my-profile" style="display: none;">
                    </div>
                    <div id="contact-list">
                        <header class="wchat-header wchat-chat-header top">
                            <div class="chat-avatar">
                                <div class="avatar icon-user-default" style="height: 40px; width: 40px;">
                                    <div class="avatar-body userimage"><img src="{{url('/public/front/images/default_admin_image.jpg')}}" class="avatar-image is-loaded" width="100%"></div>
                                </div>

                            </div>
                            <div class="chat-body">
                                <div class="chat-main"><h2 class="chat-title" dir="auto"><span class="wchatellips personName">{{ config('app.project.name') }}</span></h2></div>
                            </div>
                            <div class="wchat-chat-controls">
                                <div class="menu menu-horizontal">
                                    <div class="menu-item active dropdown pull-right">
                                        <button class="icon dropdown-toggle" data-toggle="dropdown" href="#"><span class="font-19"><i class="icon icon-options-vertical"></i></span></button>
                                        <ul class="dropdown-menu dropdown-user animated flipInY">
                                            <li><a href="{{ url('/') .'/'. $user_type . '/dashboard'}}"><i class="ti-dashboard"></i> My Dashboard</a></li>
                                            <li><a href="{{ url('/') .'/'. $user_type . '/profile'}}"><i class="ti-user"></i> My Profile</a></li>
                                        </ul>
                                    </div>
                                    <div class="menu-item right-side-toggle"><button class="icon ti-settings font-20" title="Attach"></button><span></span></div>
                                    <div class="menu-item active dropdown pull-right">
                                        <button onclick="document.location.href='{{ url('/') .'/'. $user_type . '/dashboard'}}';" class="icon ti-share-alt font-20" title="Back"></button><span></span>
                                    </div>
                                </div>
                            </div>
                        </header>
                        @if( isset($arr_chat_list) && count($arr_chat_list)>0 )
                            <div class="form-material">
                                <input class="form-control p-lr-20 live-search-box search_bg" id="searchbox" type="text" placeholder="Search By Username or Email">
                            </div>

                            <div class="contact-drawer">
                                <ul class="chatonline drawer-body contact-list" id="display" data-list-scroll-container="true" style="display: block;">
                                    
                                    @foreach( $arr_chat_list as $chat_list )

                                        @php
                                            $is_active_class = '';
                                            $online_style = 'border: none;';
                                            if(isset($chat_list['id']) && isset($enc_parent_id) && $enc_parent_id == $chat_list['id'] ){
                                              $is_active_class = 'active';
                                            }
                                        @endphp
                                        <li class="person chatboxhead active expand-sub-list" href="javascript:void(0)">
                                            <a href="javascript:void(0)">
                                                <span class="userimage profile-picture min-profile-picture"><img src="{{ isset($chat_list['group_chat_image']) ? $chat_list['group_chat_image'] : url('/public/uploads/front/profile/default_profile_image.png') }}" alt="Deven" class="avatar-image is-loaded bg-theme" width="100%"></span>
                                                <span>
                                                    <span class="bname personName">{{ isset($chat_list['group_name']) ? $chat_list['group_name'] : '' }}</span>
                                                    <span class="personStatus"></span>
                                                    <span class="count"></span><br>
                                                    <small class="preview"></small>
                                                </span>
                                            </a>
                                        @if( isset($chat_list['chat_list']) && count($chat_list['chat_list'])>0 )
                                          <ul class="chatonline drawer-body contact-list sub-chat-list" data-list-scroll-container="true" @if(isset($is_active_class) && $is_active_class == '' ) style="display: none;" @endif >
                                            @foreach( $chat_list['chat_list'] as $key => $list )
                                                <li class="person chatboxhead" href="javascript:void(0)">
                                                    <a href="{{ isset($list['redirect_url']) ? $list['redirect_url'] : 'javascript:void(0);' }}">
                                                        <span class="userimage profile-picture min-profile-picture"><img src="{{ isset($list['group_chat_image']) ? $list['group_chat_image'] : url('/public/uploads/front/profile/default_profile_image.png') }}" alt="Deven" class="avatar-image is-loaded bg-theme" width="100%"></span>
                                                        <span>
                                                            <span class="bname personName">{{ isset($list['chat_name']) ? $list['chat_name'] : '' }}</span>
                                                            <span class="personStatus"></span>
                                                            <span class="count"></span><br>
                                                            <small class="preview"></small>
                                                        </span>
                                                    </a>
                                                </li>
                                            @endforeach
                                          </ul>
                                        @endif
                                        </li>
                                        
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- .chat-left-panel -->
            <!-- .chat-right-panel -->

            <div tabindex="-1" id="main right" class="pane wchat-chat wchat-two chat-right-aside right">


                <div class="wchat-chat-tile"></div>

                <header class="wchat-header wchat-chat-header top" data-user="">
                    <button class="icon m-r-5 hidden-sm hidden-md hidden-lg open-panel" href="#"><span class="font-19"><i class="icon ti-arrow-left"></i></span></button>
                    <div class="chat-avatar" id="launchProfile">
                        <div class="avatar icon-user-default" style="height: 40px; width: 40px;">
                            <div class="avatar-body userimage profile-picture">
                                @if(isset($is_group_chat) && $is_group_chat == '1' )
                                    <img src="{{ url('/public/front/images/default_group_chat.png') }}" class="avatar-image is-loaded bg-theme" width="100%" />
                                @else
                                    <img src="{{ isset($to_user_profile_image) ? $to_user_profile_image : url('/public/uploads/front/profile/default_profile_image.png') }}" class="avatar-image is-loaded bg-theme" width="100%" />
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="chat-body">
                        <div class="chat-main">
                            <h2 class="chat-title" dir="auto">
                                <span class="wchatellips personName">
                                    {{ isset($arr_chat_user['client_user_name']) ? $arr_chat_user['client_user_name'] : '' }} | {{ isset($arr_chat_user['expert_user_name']) ? $arr_chat_user['expert_user_name'] : '' }}
                                </span>
                            </h2>
                        </div>
                        <div class="chat-status wchatellips" id="typing_on">
                            {{-- last seen today at 8:52 PM --}}
                        </div>
                    </div>
                    <div class="wchat-chat-controls">
                        <div class="menu menu-horizontal">
                            <div class="menu-item active dropdown pull-right">
                                <button id="MobileChromeplaysound" class="hidden"></button>
                                <button class="icon dropdown-toggle font-19" data-toggle="dropdown" href="#" id="mute-sound"><i class="icon icon-volume-off"></i></button>
                            </div>
                            <div class="mega-dropdown  pull-right hidden"> <button class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" aria-expanded="false"><span class="font-19"><i class="icon fa fa-paperclip"></i></span></button>
                                <ul class="dropdown-menu mega-dropdown-menu animated bounceInDown" style="width: 100px;right: -13px;box-shadow:0 0 0 rgba(0, 0, 0, 0.05) !important;background: none;">
                                    <li class="col-sm-12 demo-box">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="white-box text-center bg-purple uploadFile" id="uploadFile"><a href="#" class="text-white" data-toggle="tooltip" title="" data-original-title="Photos"><i class="icon ti-gallery font-19"></i></a></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="white-box text-center bg-success uploadFile"><a href="#" class="text-white" data-toggle="tooltip" title="" data-original-title="Videos"><i class="icon icon-camrecorder font-19"></i></a></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="white-box text-center bg-info uploadFile"><a href="#" class="text-white" data-toggle="tooltip" title="" data-original-title="Document"><i class="icon icon-doc font-19"></i></a></div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="menu-item active dropdown pull-right hidden">
                                <button class="icon dropdown-toggle" data-toggle="dropdown" href="#"><span class="font-19"><i class="icon icon-options-vertical"></i></span></button>
                                <ul class="dropdown-menu dropdown-user animated flipInY">
                                    <li><a href="javascript:void(0)" onclick="javascript:ShowProfile();"><i class="ti-user"></i> User Profile</a></li>
                                    <li><a href="javascript:void(0)"><i class="ti-wallet"></i> Email This Chat</a></li>
                                    <li><a href="javascript:void(0)"><i class="icon-doc"></i> Save Chat</a></li>
                                </ul>
                                <!-- /.dropdown-user -->
                            </div>
                            <div class="menu-item right-side-toggle hidden-xs hidden"><button class="icon ti-settings font-20" title="Attach"></button><span></span></div>
                        </div>
                    </div>
                </header>

                <div class="wchat-body wchat-chat-tile-container" style="background-size: cover;">
                    <div>
                        <span>
                            <div class="scroll-down" style="transform: scaleX(1) scaleY(1); opacity: 1; visibility:hidden;">
                                <span class="ti-angle-down"></span>
                            </div>
                        </span>
                        <div class="wchat-chat-msgs wchat-chat-body lastTabIndex" tabindex="0">
                            <div class="wchat-chat-empty"></div>
                            <div class="message-list">
                                <div class="chat-list" id="resultchat">
                                    <!--Here content comes dynamically-->

                                    <div id="chatbox" class="chat chatboxcontent active-chat">
                                        
                                        @if(isset($obj_message_list) && count($obj_message_list)>0)
                                            @foreach( $obj_message_list as $message )
                                                @php
                                                  $created_date = isset($message->time_ago) ? $message->time_ago : '';
                                                @endphp

                                                @if( isset($message->from) && isset($from_user_id) && $message->from == $from_user_id) 
                                                    <div class="col-xs-12 p-b-10 odd">
                                                        <div class="chat-image  profile-picture max-profile-picture">
                                                            <img src="{{ isset($message->profile_image) ? $message->profile_image : url('/public/uploads/front/profile/default_profile_image.png') }}">
                                                        </div>
                                                        <div class="chat-body">
                                                            <div class="chat-text">
                                                                <h4>{{ isset($message->from_user_name) ? $message->from_user_name : '' }}</h4>
                                                                <p>{!! isset($message->body) ? $message->body : '' !!}</p>
                                                                <b>{{ isset($created_date) ? $created_date : '' }}</b>
                                                                {{-- <span class="msg-status msg-mega"><i class="fa fa-check"></i></span> --}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="col-xs-12 p-b-10">
                                                        <div class="chat-image  profile-picture max-profile-picture">
                                                            <img src="{{ isset($message->profile_image) ? $message->profile_image : url('/public/uploads/front/profile/default_profile_image.png') }}" class="bg-theme">
                                                        </div>
                                                        <div class="chat-body">
                                                            <div class="chat-text">
                                                                <h4>{{ isset($message->from_user_name) ? $message->from_user_name : '' }}</h4>
                                                                <p>{!! isset($message->body) ? $message->body : '' !!}</p>
                                                                <b>{{ isset($created_date) ? $created_date : '' }}</b>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="wchat-filler" style="height: 0px;"></div>
                <footer tabindex="-1" class="wchat-footer wchat-chat-footer">
                    <div id="chatFrom">
                        <!--TextArea Dinamic -->
                        <div class="block-wchat">
                            <div id="typing_on"></div>
                            <button class="icon ti-face-smile font-24 btn-emoji" onclick="javascript:chatemoji()" href="javascript:void(0)" id="toggle-emoji"></button>
                            <div tabindex="-1" class="input-container">
                                <div tabindex="-1" class="input-emoji">
                                    <div class="input-placeholder" style="visibility: visible;display:none;">Type a message</div>
                                    <textarea class="input chatboxtextarea" id="chatboxtextarea" name="chattxt" onkeydown="javascript:return checkChatBoxInputKey(event,this);" contenteditable spellcheck="true" style="resize:none;height:20px" placeholder="Type a message"></textarea>
                                </div>
                            </div>
                            <button onclick="javascript:return clickTosendMessage();" class="btn-icon icon-send fa fa-paper-plane-o font-24 send-container"></button>
                        </div>
                    </div>

                    @include('twilio-chat.chat_emoji')

                    <span class="mentions-positioning-container"></span>
                </footer>
                <span></span>
            </div>
            <!-- .chat-right-panel -->
        </div>
    </div>
</div>


<!-- .right-sidebar -->
<div class="right-sidebar">
    <div class="slimscrollright">
        <div class="rpanel-title"> Theme Modify<span><i class="ti-close right-side-toggle"></i></span> </div>
        <div class="r-panel-body">
            <ul>
                <li><b>Layout Options</b></li>

            </ul>
            <ul id="mainthemecolors" class="m-t-20">
                <li><b>Theme (Light/Dark)</b></li>
                <li><a href="javascript:void(0)" maintheme="style-light" class="light-theme working">1</a></li>
                <li><a href="javascript:void(0)" maintheme="style-dark" class="dark-theme">2</a></li>
            </ul>
            <ul id="themecolors" class="m-t-20">
                <li><b>With Color Effect</b></li>
                <li><a href="javascript:void(0)" theme="default" class="default-theme">1</a></li>
                <li><a href="javascript:void(0)" theme="green" class="green-theme">2</a></li>
                <li><a href="javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
                <li><a href="javascript:void(0)" theme="blue" class="blue-theme">4</a></li>
                <li><a href="javascript:void(0)" theme="purple" class="purple-theme working">5</a></li>
                <li><a href="javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
            </ul>

        </div>
    </div>
</div>
<!-- /.right-sidebar -->

<!-- Media Uploader -->
<!--This div for modal light box chat box image-->
<div id="lightbox" style="display: none;">
    <p>
        <img src="{{url('/public/twilio-chat')}}/plugins/images/close-icon-white.png"
             width="30px" style="cursor: pointer"/>
    </p>
    <div id="content">
        <img src="#" />
    </div>
</div>
<!--This div for modal light box chat box image-->

<!-- jQuery -->
<script src="{{url('/public/twilio-chat')}}/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{url('/public/twilio-chat')}}/assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="{{url('/public/twilio-chat')}}/assets/js/custom.js"></script>
<!--Style Switcher -->
<script src="{{url('/public/twilio-chat')}}/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
<!--Style Switcher -->
<!--ChatJs -->
<script type="text/javascript" src="{{url('/public/twilio-chat')}}/chatjs/lightbox.js"></script>
<script type="text/javascript" src="{{url('/public/twilio-chat')}}/chatjs/inbox.js"></script>
<script type="text/javascript" src="{{url('/public/twilio-chat')}}/chatjs/custom.js"></script>
<!--ChatJs-->

<input type="hidden" name="arr_chat_user" id="arr_chat_user" value=" {{ isset($arr_chat_user) ? json_encode($arr_chat_user) : '' }} " >

<script src="https://media.twiliocdn.com/sdk/js/chat/releases/4.0.0/twilio-chat.min.js"></script>

<script type="text/javascript">
    var arr_chat_user = [];
    if($('#arr_chat_user').val()!='') {
        arr_chat_user = JSON.parse($('#arr_chat_user').val());
    }

    var chatClient;
    var projectChannel;
    var accessToken          = '{{ isset($access_token) ? $access_token : '' }}';
    var fromUserId           = '{{ isset($from_user_id) ? $from_user_id : '' }}';
    var channelResourceSid   = '{{ isset($channel_resource_sid) ? $channel_resource_sid : '' }}';
    var fromUserProfileImage = '{{ isset($from_user_profile_image) ? $from_user_profile_image : url('/public/uploads/front/profile/default_profile_image.png') }}';
    
    Twilio.Chat.Client.create(accessToken).then(client => {
        chatClient = client;

        chatClient.getSubscribedChannels().then(createOrJoinProjectChannel);

        // when the access token is about to expire, refresh it
        chatClient.on('tokenAboutToExpire', function() {
          refreshAccessToken();
        });

        // if the access token already expired, refresh it
        chatClient.on('tokenExpired', function() {
          refreshAccessToken();
        });

    }).catch(error => {
        console.error(error);
        console.log('There was an error creating the chat client:<br/>' + error, true);
        console.log('Please check your .env file.', false);
    });

    function refreshAccessToken() {
        if( fromUserId != '') {
            $.ajax({
                url :"{{$module_url_path}}/refresh_token",
                method :"GET",
                success :function(response){
                    if(response.status == 'success'){
                        chatClient.updateToken(response.status.access_token);
                    } else {
                        alert(response.msg);
                        setTimeout(function(){
                            location.reload();
                        },5000);
                        return false;
                    }
                }
            });
        }
    }

    function createOrJoinProjectChannel() {
        // Get the general chat channel, which is where all the messages are
        // sent in this simple application
        console.log('Attempting to join '+ channelResourceSid +' chat channel...');
        chatClient.getChannelBySid(channelResourceSid)
          .then(function(channel) {
            projectChannel = channel;
            console.log('Found '+ channelResourceSid +' channel:');
            setupProjectChannel();
        }).catch(function() {
            // If it doesn't exist, let's create it
            console.log('Creating '+ channelResourceSid +' channel');
            chatClient.createChannel({
              uniqueName: channelResourceSid,
              friendlyName: channelResourceSid + ' Chat Channel'
            }).then(function(channel) {
              console.log('Created '+ channelResourceSid +' channel:');
              projectChannel = channel;
              setupProjectChannel();
            }).catch(function(channel) {
              console.log('Channel could not be created:');
            });
        });
    }

    // Set up channel after it has been found
    function setupProjectChannel() {
        // Listen for new messages sent to the channel
        projectChannel.on('messageAdded', function(message) {
            printMessageContentHTML(message.author, message.body );
        });
    }

    function printMessageContentHTML(fromUser, message ) {
        
        var class_name = profile_image = user_name = '';

        if(fromUserId == fromUser) {
            class_name = 'odd';
        } 

        if(class_name == '' && eval(localStorage.sound)){
            audiomp3.play();
            audioogg.play();
        }

        if(arr_chat_user.expert_user_id != undefined && arr_chat_user.expert_user_id == fromUser) {
            profile_image = arr_chat_user.expert_profile_image;
            user_name = arr_chat_user.expert_user_name;
        } else if(arr_chat_user.client_user_id != undefined && arr_chat_user.client_user_id == fromUser) {
            profile_image = arr_chat_user.client_profile_image;
            user_name = arr_chat_user.client_user_name;
        }  else if(arr_chat_user.admin_user_id != undefined && arr_chat_user.admin_user_id == fromUser) {
            profile_image = arr_chat_user.admin_profile_image;
            user_name = arr_chat_user.admin_user_name;
        }

        var messageHtml =   '<div class="col-xs-12 p-b-10 '+ class_name +'">' +
                                '<div class="chat-image  profile-picture max-profile-picture">' +
                                    '<img src="'+profile_image+'">' +
                                '</div>' +
                                '<div class="chat-body">' +
                                    '<div class="chat-text">' +
                                        '<h4>'+user_name+'</h4>' +
                                        '<p>'+message+'</p>' +
                                        '<b>just now</b>' +
                                    '</div>' +
                                '</div>' +
                            '</div>';

        $("#chatbox").append(messageHtml);

        scrollDown();
        var adjustedHeight = $(".chatboxtextarea").clientHeight;
        var maxHeight = 40;

        if (maxHeight > adjustedHeight) {
            adjustedHeight = Math.max($(".chatboxtextarea").scrollHeight, adjustedHeight);
            if (maxHeight)
                adjustedHeight = Math.min(maxHeight, adjustedHeight);
            if (adjustedHeight > $(".chatboxtextarea").clientHeight)
                $($(".chatboxtextarea")).css('height',adjustedHeight+8 +'px');
        } else {
            $($(".chatboxtextarea")).css('overflow','auto');
        }
        return false;
    }

    $(".expand-sub-list").click(function() {
        if($(this).find('.sub-chat-list').is(':visible')) {
           $(this).find('.sub-chat-list').hide(); 
        } else {
            $(this).find('.sub-chat-list').show();
        }
    });
</script>
</body>
</html>
