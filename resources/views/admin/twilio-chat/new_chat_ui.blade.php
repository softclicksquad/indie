
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('/public/twilio-chat')}}/assets/images/favicon.png">
    <title>{{ isset($page_title) ? $page_title : config('app.project.name') . ' Chat' }}</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{url('/public/twilio-chat')}}/assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Animation CSS -->
    <link href="{{url('/public/twilio-chat')}}/assets/css/animate.css" rel="stylesheet">
    <link href="{{url('/public/twilio-chat')}}/assets/css/custom.css" rel="stylesheet" id="style">
    <!-- Theme light dark version CSS -->
    <link href="{{url('/public/twilio-chat')}}/assets/css/style.css" rel="stylesheet">
    <link href="{{url('/public/twilio-chat')}}/assets/css/style-light.css"  id="maintheme" rel="stylesheet">
    <!-- color CSS you can use different color css from css/colors folder -->
    <!-- We have chosen the skin-blue (blue.css) for this starter
         page. However, you can choose any other skin from folder css / colors .
    -->
    <link href="{{url('/public/twilio-chat')}}/assets/css/colors/green.css" id="theme"  rel="stylesheet">
    <!-- Emoji One JS -->
    <link rel="stylesheet" href="{{url('/public/twilio-chat')}}/smiley/assets/sprites/emojione.sprites.css"/>
    <script src="{{url('/public/twilio-chat')}}/smiley/js/emojione.min.js"></script>
    <script type="text/javascript">
        var twilio_chat_base_url = '{{url('/public/twilio-chat')}}';
        // #################################################
        // # Optional

        // default is PNG but you may also use SVG
        emojione.imageType = 'png';
        emojione.sprites = false;

        // default is ignore ASCII smileys like :) but you can easily turn them on
        emojione.ascii = true;

        // if you want to host the images somewhere else
        // you can easily change the default paths
        /*emojione.imagePathPNG = twilio_chat_base_url + '/smiley/assets/png/';
        emojione.imagePathSVG = twilio_chat_base_url + '/smiley/assets/svg/';*/

        // #################################################
    </script>
</head>
<body>

<!-- Preloader -->

<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>



<div id="wchat">
    <div class="wchat-wrapper wchat-wrapper-web wchat-wrapper-main">
        <div tabindex="-1" class="wchat two">
            <div class="drawer-manager">
                <span class="pane wchat-one"></span>
                <span class="pane wchat-two">
                    <div id="get-error">

                    </div>
                    <div id="showErrorModal" data-toggle="modal" data-target=".bs-example-modal-sm"></div>
                    <div class="pane pane-intro" id="pane-intro" style="visibility: visible">
                        <div class="intro-body">
                            <div class="intro-image" style="opacity: 1; transform: scale(1);"></div>
                            <div class="intro-text-container" style="opacity: 1; transform: translateY(0px);">
                                <h1 class="intro-title">Welcome to {{ config('app.project.name') }} Chat</h1>
                                <div class="intro-text">No Conversation sync. Please search users and start chat.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="uploader" style="display: none">
                        <p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
                    </div>

                </span>
                <span class="pane wchat-three" style="transition: all 0.3s ease;">
                    <span class="flow-drawer-container" style="transform: translateX(0px);border: 1px solid rgba(0, 0, 0, .08);display:block;">
                        <div class="drawer drawer-info">
                            <header class="wchat-header">
                                <div class="header-close">
                                    <button><span class="icon icon-x  ti-close"></span></button>
                                </div>
                                <div class="header-body">
                                    <div class="header-main">
                                        <div class="header-title">Profile info</div>
                                    </div>
                                </div>
                            </header>
                            <div class="drawer-body" id="userProfile" data-list-scroll-container="true">
                                <!--Here Profile comes dynamically-->
                            </div>
                        </div>
                    </span>
                </span>
            </div>

            <!-- .chat-left-panel -->
            <div id="side" class="wchat-list wchat-one chat-left-aside left">
                <div class="open-panel"><i class="ti-angle-right"></i></div>
                <div class="chat-left-inner">
                    <div id="my-profile" style="display: none;">
                    </div>
                    <div id="contact-list">
                        <header class="wchat-header wchat-chat-header top">
                            <div class="chat-avatar">
                                <div class="avatar icon-user-default" style="height: 40px; width: 40px;">
                                    <div class="avatar-body userimage"><img src="{{url('/public/front/images/default_admin_image.jpg')}}" class="avatar-image is-loaded" width="100%"></div>
                                </div>

                            </div>
                            <div class="chat-body">
                                <div class="chat-main"><h2 class="chat-title" dir="auto"><span class="wchatellips personName">{{ config('app.project.name') }}</span></h2></div>
                            </div>
                            <div class="wchat-chat-controls">
                                <div class="menu menu-horizontal">
                                    <div class="menu-item active dropdown pull-right">
                                        <button class="icon dropdown-toggle" data-toggle="dropdown" href="#"><span class="font-19"><i class="icon icon-options-vertical"></i></span></button>
                                        <ul class="dropdown-menu dropdown-user animated flipInY">
                                            <li><a href="{{ url('/') .'/'. $user_type . '/dashboard'}}"><i class="ti-dashboard"></i> My Dashboard</a></li>
                                        </ul>
                                    </div>
                                    <div class="menu-item right-side-toggle"><button class="icon ti-settings font-20" title="Attach"></button><span></span></div>
                                    <div class="menu-item active dropdown pull-right">
                                        <button onclick="document.location.href='{{ url('/') .'/'. $user_type . '/dashboard'}}';" class="icon ti-share-alt font-20" title="Back"></button><span></span>
                                    </div>
                                </div>
                            </div>
                        </header>
                        @if( isset($arr_chat_list) && count($arr_chat_list)>0 )
                            <div class="form-material">
                                <input class="form-control p-lr-20 live-search-box search_bg" id="searchbox" type="text" placeholder="Search By Username or Email">
                            </div>

                            <div class="contact-drawer">
                                <ul class="chatonline drawer-body contact-list" id="display" data-list-scroll-container="true" style="display: block;">
                                    
                                    @foreach( $arr_chat_list as $chat_list )
                                        <li class="person chatboxhead active expand-sub-list" href="javascript:void(0)">
                                            <a href="javascript:void(0)">
                                                <span class="userimage profile-picture min-profile-picture"><img src="{{ isset($chat_list['group_chat_image']) ? $chat_list['group_chat_image'] : url('/public/uploads/front/profile/default_profile_image.png') }}" alt="Deven" class="avatar-image is-loaded bg-theme" width="100%"></span>
                                                <span>
                                                    <span class="bname personName">{{ isset($chat_list['group_name']) ? $chat_list['group_name'] : '' }}</span>
                                                    <span class="personStatus"></span>
                                                    <span class="count"></span><br>
                                                    <small class="preview"></small>
                                                </span>
                                            </a>
                                        @if( isset($chat_list['chat_list']) && count($chat_list['chat_list'])>0 )
                                          <ul class="chatonline drawer-body contact-list sub-chat-list" data-list-scroll-container="true" style="display: none;">
                                            @foreach( $chat_list['chat_list'] as $key => $list )
                                                <li class="person chatboxhead" href="javascript:void(0)">
                                                    <a href="{{ isset($list['redirect_url']) ? $list['redirect_url'] : 'javascript:void(0);' }}">
                                                        <span class="userimage profile-picture min-profile-picture"><img src="{{ isset($list['group_chat_image']) ? $list['group_chat_image'] : url('/public/uploads/front/profile/default_profile_image.png') }}" alt="Deven" class="avatar-image is-loaded bg-theme" width="100%"></span>
                                                        <span>
                                                            <span class="bname personName">{{ isset($list['chat_name']) ? $list['chat_name'] : '' }}</span>
                                                            <span class="personStatus"></span>
                                                            <span class="count"></span><br>
                                                            <small class="preview"></small>
                                                        </span>
                                                    </a>
                                                </li>
                                            @endforeach
                                          </ul>
                                        @endif
                                        </li>
                                        
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- .chat-left-panel -->
            <!-- .chat-right-panel -->

        </div>
    </div>
</div>


<!-- .right-sidebar -->
<div class="right-sidebar">
    <div class="slimscrollright">
        <div class="rpanel-title"> Theme Modify<span><i class="ti-close right-side-toggle"></i></span> </div>
        <div class="r-panel-body">
            <ul>
                <li><b>Layout Options</b></li>

            </ul>
            <ul id="mainthemecolors" class="m-t-20">
                <li><b>Theme (Light/Dark)</b></li>
                <li><a href="javascript:void(0)" maintheme="style-light" class="light-theme working">1</a></li>
                <li><a href="javascript:void(0)" maintheme="style-dark" class="dark-theme">2</a></li>
            </ul>
            <ul id="themecolors" class="m-t-20">
                <li><b>With Color Effect</b></li>
                <li><a href="javascript:void(0)" theme="default" class="default-theme">1</a></li>
                <li><a href="javascript:void(0)" theme="green" class="green-theme">2</a></li>
                <li><a href="javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
                <li><a href="javascript:void(0)" theme="blue" class="blue-theme">4</a></li>
                <li><a href="javascript:void(0)" theme="purple" class="purple-theme working">5</a></li>
                <li><a href="javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
            </ul>

        </div>
    </div>
</div>
<!-- /.right-sidebar -->

<!-- Media Uploader -->
<!--This div for modal light box chat box image-->
<div id="lightbox" style="display: none;">
    <p>
        <img src="{{url('/public/twilio-chat')}}/plugins/images/close-icon-white.png"
             width="30px" style="cursor: pointer"/>
    </p>
    <div id="content">
        <img src="#" />
    </div>
</div>
<!--This div for modal light box chat box image-->

<!-- jQuery -->
<script src="{{url('/public/twilio-chat')}}/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{url('/public/twilio-chat')}}/assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="{{url('/public/twilio-chat')}}/assets/js/custom.js"></script>
<!--Style Switcher -->
<script src="{{url('/public/twilio-chat')}}/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
<!--Style Switcher -->
<!--ChatJs -->
<script type="text/javascript" src="{{url('/public/twilio-chat')}}/chatjs/lightbox.js"></script>
{{-- <script type="text/javascript" src="{{url('/public/twilio-chat')}}/chatjs/inbox.js"></script> --}}
{{-- <script type="text/javascript" src="{{url('/public/twilio-chat')}}/chatjs/custom.js"></script> --}}
<!--ChatJs-->
<script type="text/javascript">
    $(".expand-sub-list").click(function() {
        if($(this).find('.sub-chat-list').is(':visible')) {
           $(this).find('.sub-chat-list').hide(); 
        } else {
            $(this).find('.sub-chat-list').show();
        }
    });
</script>
</body>
</html>
