@extends('admin.layout.master')            
@section('main_content')

<link href="{{url('/public')}}/front/css/twillo-chat.css" rel="stylesheet" type="text/css" />

<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-comment"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-comment"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
         </div>
         <div class="box-content">
            @include('admin.layout._operation_status')

            <div id="frame">
              <div id="sidepanel">
                @if( isset($arr_chat_list) && count($arr_chat_list)>0 )
                  @foreach( $arr_chat_list as $chat_list )            
                    <div id="profile" class="">
                          <div class="wrap">
                            <img id="profile-img" src="{{ url('/public/front/images/default_group_chat.png') }}" style="border: none;" alt="">
                            <p>{{ isset($chat_list['group_name']) ? str_limit($chat_list['group_name'],20) : '' }}</p>
                            <i class="fa fa-chevron-down expand-button" aria-hidden="true"></i>
                            @if( isset($chat_list['chat_list']) && count($chat_list['chat_list'])>0 )
                              <div id="expanded" class="sub-chat-list">
                                @foreach( $chat_list['chat_list'] as $key => $list )
                                  <a href="{{ isset($list['redirect_url']) ? $list['redirect_url'] : 'javascript:void(0);' }}"> <div>  {{ isset($list['chat_name']) ? $list['chat_name'] : '' }}  </div> </a>  
                                @endforeach
                              </div>
                            @endif
                          </div>
                      </div>

                  @endforeach
                @endif
              </div>

              <div class="content">
              </div>  
            </div>


         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
  $(".expand-button").click(function() {
    if($(this).parent().parent().hasClass('expanded')){
      $(this).parent().parent().removeClass('expanded');
    } else {
      $(this).parent().parent().addClass('expanded');
    }
  });

</script>
@stop
