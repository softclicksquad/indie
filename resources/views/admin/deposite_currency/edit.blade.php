@extends('admin.layout.master')
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-desktop"></i>
      <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-edit"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-edit"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
            @include('admin.layout._operation_status') 
            <div class="tabbable">
               <form name="validation-form" id="validation-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/update/{{$enc_id}}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                   <div>
                        <div class="tab-pane">
                           <div class="form-group">
                              <label class="col-sm-3 col-lg-2 control-label" for="page_title">Currency Symbol<i class="red">*</i></label>
                              <div class="col-sm-6 col-lg-4 controls">
                                 
                                 <input type="text" name="currency" class="form-control" data-rule-required="true" data-rule-maxlength="255" placeholder="Currency Symbol" value="{{isset($arr_currency['currency'])?$arr_currency['currency']:''}}">
                                     
                                 <span class='error'>{{ $errors->first('currency') }}</span>
                              </div>
                           </div>
                            <div class="form-group">
                              <label class="col-sm-3 col-lg-2 control-label" for="page_title">Currency Code<i class="red">*</i></label>
                              <div class="col-sm-6 col-lg-4 controls">
                                 
                                 <input type="text" name="currency_code" class="form-control" data-rule-required="true" data-rule-maxlength="255" placeholder="Currency Code" value="{{isset($arr_currency['currency_code'])?$arr_currency['currency_code']:''}}">
                                     
                                 <span class='error'>{{ $errors->first('currency_code') }}</span>
                              </div>
                           </div>
                           
                        </div>
                        
                  </div>
                  <br>
                  <div class="form-group">
                     <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                        <input type="submit" value="Update" class="btn btn btn-primary">
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- END Main Content -->
<script type="text/javascript">
function Upload() {
    $('.file_err').css('color','black');
    //Get reference of FileUpload.
    var ad_image = document.getElementById("ad_image");
    //Check whether the file is valid Image.
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+()$");
    if (regex.test(ad_image.value.toLowerCase())) {
        //Check whether HTML5 is supported.
        if (typeof (ad_image.files) != "undefined") {
            var file_extension = ad_image.value.substring(ad_image.value.lastIndexOf('.')+1, ad_image.length); 
            if(file_extension != 'jpg' && file_extension != 'jpeg' && file_extension != 'png'){
               setTimeout(function(event){
                  $('a.fileupload-exists').click();
               },200); 
               $('.file_err').css('color','red');
               $('.file_err').html("Image should be in jpg,jpeg,png Format (60 X 60).");
               return false;
            } 
            //Initiate the FileReader object.
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(ad_image.files[0]);
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();
                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;
                //Validate the File Height and Width.
                image.onload = function () {
                    var height = this.height;
                    var width  = this.width;
                     if (height != 60 && width != 60) {
                        setTimeout(function(){
                           $('a.fileupload-exists').click();
                        },200); 
                        $('.file_err').css('color','red');
                        $('.file_err').html("Image should be in jpg,jpeg,png Format (60 X 60).");
                        event.preventDefault(); 
                        return false;
                    }
                    $('.file_err').css('color','green');
                    return true;
                };
            }
        } else {
            $('.file_err').css('color','red');
            $('.file_err').html("This browser does not support HTML5.");
            return false;
        }
    } else {
        $('.file_err').css('color','red');
        $('.file_err').html("Image should be in jpg,jpeg,png Format (60 X 60)");
        return false;
    }
}
</script>
@stop
