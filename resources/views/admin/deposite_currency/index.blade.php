@extends('admin.layout.master')
@section('main_content')
<link rel="stylesheet" type="text/css" href="{{ url('/assets/data-tables/latest/') }}/dataTables.bootstrap.min.css">
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-bars"></i>
      <a href="{{ $module_url_path }}">{{ $page_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-bars"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-bars"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">

            @include('admin.layout._operation_status')  

            <form name="validation-form" id="validation-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/store">
               {{ csrf_field() }}

               <div class="table-responsive" style="border:0">
                  
                  <table class="table table-advance"  id="" >
                     <thead>
                        <tr>                           
                           <th>Currency</th>
                           <th>Min Amount</th>
                           <th>Max Amount</th>
                           <th>Fixed Min Processing charge</th>
                           <th>Max Processing charge(%)</th>                           
                        </tr>
                     </thead>
                     <tbody>
                      @if(isset($arr_currency) && sizeof($arr_currency)>0)
                        @foreach($arr_currency as $currency)
                        <tr>
                           <td style="vertical-align: unset;"> 
                              {{isset($currency['currency_code'])?$currency['currency_code']:''}} ({{isset($currency['currency'])?$currency['currency']:''}})
                              <input type="hidden" name="currency_id[]" value="{{isset($currency['id'])?$currency['id']:''}}" >
                           </td>
                           <td style="vertical-align: unset;"> 
                            <input class="form-control" type="text" placeholder="Min Amount" name="min_amount[]" value="{{isset($currency['deposite']['min_amount'])?$currency['deposite']['min_amount']:''}}" data-rule-required="true" data-rule-maxlength="40" >
                             </td>
                           <td style="vertical-align: unset;"> <input class="form-control" type="text" placeholder="Max Amount" name="max_amount[]" value="{{isset($currency['deposite']['max_amount'])?$currency['deposite']['max_amount']:''}}" data-rule-required="true" data-rule-maxlength="40"> </td>
                           
                           <td style="vertical-align: unset;"> <input class="form-control" type="text" placeholder="Min Amount Charge" name="min_amount_charge[]" value="{{isset($currency['deposite']['min_amount_charge'])?$currency['deposite']['min_amount_charge']:''}}" data-rule-required="true" data-rule-maxlength="40"> </td>

                           <td style="vertical-align: unset;"> <input class="form-control" type="text" placeholder="Max Amount Charge" name="max_amount_charge[]" value="{{isset($currency['deposite']['max_amount_charge'])?$currency['deposite']['max_amount_charge']:''}}" data-rule-required="true" data-rule-maxlength="40"> </td>
                           
                        </tr>
                        @endforeach
                        @endif
                     </tbody>
                  </table>

                  <input type="submit" value="Save" class="btn btn btn-primary">
                
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<!-- END Main Content -->
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
   function confirm_delete(url) {
      alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
          if (e) {
              showProcessingOverlay();
              window.location.href=url;
              return true;
          } else {
              return false;
          }
      });
   }

   function confirm_action_one(url) {
      alertify.confirm("Are you sure? You want to activate this record(s)", function (e) {
          if (e) {
              showProcessingOverlay();
              window.location.href=url;
              return true;
          } else {
              return false;
          }
      });
   }


   function confirm_action_two(url) {
      alertify.confirm("Are you sure? You want to deactivate this record(s)", function (e) {
          if (e) {
              showProcessingOverlay();
              window.location.href=url;
              return true;
          } else {
              return false;
          }
      });
   }

   function check_multi_action(frm_id,action)
   {
     var frm_ref = jQuery("#"+frm_id);
     if(jQuery(frm_ref).length && action!=undefined && action!="")
     {
      /* Get hidden input reference */
       var checked_record = jQuery('input[name="checked_record[]"]:checked');
       if (jQuery(checked_record).size() < 1)
       {
          alertify.alert('Please select at least one record.');
          return false;
       }
       var input_multi_action = jQuery('input[name="multi_action"]');
       if(jQuery(input_multi_action).length){
         /* Set Action in hidden input*/
         jQuery('input[name="multi_action"]').val(action);
         /*Submit the referenced form */
                alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
                if (e) {
                    showProcessingOverlay();
                    jQuery(frm_ref)[0].submit();
                    return true;
                } else {
                    return false;
                }
            });
       } else {
         console.warn("Required Hidden Input[name]: multi_action Missing in Form ")
       }
     } else {
         console.warn("Required Form[id]: "+frm_id+" Missing in Current category ")
     }
   }
</script>
@stop