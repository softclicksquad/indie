@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url('/'.$admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      </span>
      <li class="active">  {{ isset($page_title)?$page_title:"" }}</li>
   </ul>
</div>
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box box-blue">
         <div class="box-title">
            <h3><i class="fa fa-file"></i> Payment Methods</h3>
         </div>
         <div class="box-content">
            @include('admin.layout._operation_status')  
            <form name="validation-form" id="validate-form-payment" method="POST" class="form-horizontal" action="{{url(config('app.project.admin_panel_slug').'/payment/paynow')}}">
               {{ csrf_field() }}
               <!-- payment_obj_id is id for which user is going to pay like subscription pack or milestone -->
               <input type="hidden" name="payment_obj_id" id="payment_obj_id" value=" {{isset($arr_milestone['id'])?$arr_milestone['id']:'0'}}">
               <!-- transaction_type is type for which user making this transaction like subscription pack or milestone -->
               <input type="hidden" name="transaction_type" id="transaction_type" value="3">

               <!-- expert id for goto payment in to get setting fo -->
               <input type="hidden" name="expert_user_id" id="expert_user_id" value="{{isset($arr_milestone['expert_user_id'])?$arr_milestone['expert_user_id']:'0'}}">


               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Milestone Cost :</label>
                  <div class="col-sm-9 controls">
                     <label class="control-label" style="text-align:left;">{{isset($arr_milestone['project_details']['project_currency'])?$arr_milestone['project_details']['project_currency']:''}}{{isset($arr_milestone['cost'])?number_format($arr_milestone['cost'],2):''}}</label>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Cost To Expert :</label>
                  <div class="col-sm-9 controls">
                     <label class="control-label" style="text-align:left;">{{isset($arr_milestone['project_details']['project_currency'])?$arr_milestone['project_details']['project_currency']:''}}{{isset($arr_milestone['cost_to_expert'])?number_format($arr_milestone['cost_to_expert'],2):''}}</label>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Payment Method:</label>
                  <div class="col-sm-9 col-lg-10 controls">
                     <label class="radio-inline" onclick="javascript: return setOption('paypal');">
                     <input type="radio" name="payment_method" id="radio_paypal" class="css-checkbox" checked="" value="1">
                     <label for="radio_paypal" class="css-label radGroup1"><img src="{{url('/public')}}/front/images/paypal-payment.png" alt="paypal payment method"/></label>
                     
                     </label>

                    <!-- <label class="radio-inline" onclick="javascript: return setOption('stripe');">
                     <input type="radio" name="payment_method" id="radio_stripe" class="css-checkbox" value="2" 
                     @if(old('payment_method')==2)
                     checked="" 
                     @endif
                     >
                     <label for="radio_stripe" class="css-label radGroup1"><img src="{{url('/public')}}/front/images/stripe2.png" alt="Stripe payment method" width="148"/></label>
                     </label> -->

                  </div>
               </div>

               <div class="row" style="display:none;" id="section-card-details">
                  <div class="col-sm-3 col-lg-2 control-label">
                  </div>
                  <div class="col-sm-6">
                     <div class="panel panel-primary">
                        <div class="panel-heading">
                           <h4 class="panel-title">Card Details</h4>
                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                              <div class="col-sm-12 col-lg-10 controls">
                                 Card Number<span class="red">*</span>
                                 <input type="text" class="form-control" laceholder="Enter Card Number" name="cardNumber" id="cardNumber" required autofocus value="{{old('cardNumber')}}"/>
                                 <span class='error'>{{ $errors->first('cardNumber') }}</span>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-12 col-lg-3 controls">
                                 Expiration Month<span class="red">*</span>
                                 <input type="text" class="form-control" placeholder="MM" name="cardExpiryMonth" id="cardExpiryMonth" data-rule-minlength="2" data-rule-maxlength="2" required data-rule-number='true' value="{{old('cardExpiryMonth')}}"/>
                                 <span class='error'>{{ $errors->first('cardExpiryMonth') }}</span>
                              </div>
                              <div class="col-sm-12 col-lg-3 controls">
                                 Expiration Year<span class="red">*</span>
                                 <input type="text" class="form-control" placeholder="YYYY" name="cardExpiryYear" id="cardExpiryYear" data-rule-minlength="4" data-rule-maxlength="4" data-rule-number='true' required value="{{old('cardExpiryYear')}}"/>
                                 <span class='error'>{{ $errors->first('cardExpiryYear') }}</span>
                              </div>
                              <div class="col-sm-12 col-lg-4 controls">
                                 Card CV Code<span class="red">*</span>
                                 <input type="text" class="form-control" placeholder="Enter CV Code" name="cardCVC" id="cardCVC" required value="{{old('cardCVC')}}" />
                                 <span class='error'>{{ $errors->first('cardCVC') }}</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-9 col-sm-offset-3 col-lg-4 col-lg-offset-2">
                     <button type="submit" class="btn btn-primary" id="btn-form-payment-submit">Pay Now</button>
                     {{-- <button type="button" class="btn">Cancel</button> --}}
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript" src="{{url('/public')}}/assets/payment/jquery.payment.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript">

   $( document ).ready(function() 
   {
      var is_stripe = jQuery('#radio_stripe').prop('checked');
       if (is_stripe==true)
       {
         jQuery('#section-card-details').show();
       }
       else
       {
         jQuery('#section-card-details').hide();
       }
       
   });

   $('input[name=cardNumber]').payment('formatCardNumber');
   $('input[name=cardCVC]').payment('formatCardCVC');
   
   jQuery("#validate-form-payment").validate({
      errorElement: 'span',   
   });
   
   $('#validate-form-payment').submit(function () 
   {
      jQuery('#btn-form-payment-submit').prop('disabled', true);
   });


   function setOption(opt) 
   {
       if (opt=='stripe')
       {
         jQuery('#section-card-details').show();
         jQuery('#radio_stripe').prop('checked',true);
       }
       else
       {
         jQuery('#section-card-details').hide();
         jQuery('#radio_paypal').prop('checked',true);
       }

   }




  $('#cardExpiryYear').keypress(function(eve) 
  {
      if(eve.which == 8 || eve.which == 190) 
      {
        return true;
      }
      else if (eve.which != 8 && eve.which != 0 && (eve.which < 48 || eve.which > 57)) {
       eve.preventDefault();
     }

      if($('#cardExpiryYear').val().length>3) 
      {
         eve.preventDefault();
      }
   });

  $('#cardExpiryMonth').keypress(function(eve) 
  {
      if(eve.which == 8 || eve.which == 190) 
      {
        return true;
      }
      else if (eve.which != 8 && eve.which != 0 && (eve.which < 48 || eve.which > 57)) {
       eve.preventDefault();
     }

      if($('#cardExpiryMonth').val().length>1) 
      {
         eve.preventDefault();
      }
   });

</script>


<!-- END Main Content -->
@stop

