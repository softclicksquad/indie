    @extends('admin.layout.master')                
    @section('main_content')
    
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{ url('/'.$admin_panel_slug.'/dashboard') }}">Dashboard</a>
            </li>
            <span class="divider">
                <i class="fa fa-angle-right"></i>
            </span>
            
            <li class="active">  {{ isset($page_title)?$page_title:"" }}</li>
        </ul>
    </div>
    <!-- BEGIN Main Content -->
        <div class="row">
        <div class="col-md-12">
            <div class="box box-blue">
                <div class="box-title">
                    <h3><i class="fa fa-file"></i> Payment Error</h3>
                    
                </div>
                <div class="box-content">
                <div class="row">
                   

                   <div class="col-md-2"></div>
                   <div class="col-md-8">
                   <center>
                    <div class="alert alert-danger">
                    <h1>Sorry ... !</h1>
                    <p>Your payment transaction could not be completed, please try after some time. </p>
                    </div>
                    </center>
                    </div>
                    <div class="col-md-2"></div>
                   
                </div>
                </div>
            </div>
        </div>
    </div>
<!-- END Main Content -->


<script type="text/javascript">
 
  (function (global, $) {

    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";
        global.setTimeout(function () 
        {
            global.location.href += "!";
        }, 50);
    };

    global.onhashchange = function () {
        if (global.location.hash != _hash) {
            global.location.hash = _hash;
        }
    };

    global.onload = function () 
    {
        noBackPlease();
        // disables backspace on page except on input fields and textarea..
        $(document.body).keydown(function (e) {
            var elm = e.target.nodeName.toLowerCase();
            if (e.which == 8 && (elm !== 'input' && elm  !== 'textarea')) 
            {
                e.preventDefault();
            }
            // stopping event bubbling up the DOM tree..
            e.stopPropagation();
        });
    };
    })(window, jQuery || window.jQuery);

  </script>

@stop