@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/data-tables/latest/dataTables.bootstrap.min.css">
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-suitcase"></i>
      <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-suitcase"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-suitcase"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
            @include('admin.layout._operation_status')
            <form name="frm_manage" id="frm_manage" method="POST" class="form-horizontal" action="{{$module_url_path}}/multi_action">
               {{ csrf_field() }}
               <div class="btn-toolbar pull-right clearfix">

               <!--   <div class="btn-group">
                     <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                        title="Multiple Active/Unblock" 
                        href="javascript:void(0);" 
                        onclick="javascript : return check_multi_action('frm_manage','activate');" 
                        style="text-decoration:none;">
                     <i class="fa fa-unlock"></i>
                     </a> 
                  </div>
                  <div class="btn-group">
                     <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                        title="Multiple Deactive/Block" 
                        href="javascript:void(0);" 
                        onclick="javascript : return check_multi_action('frm_manage','deactivate');"  
                        style="text-decoration:none;">
                     <i class="fa fa-lock"></i>
                     </a> 
                  </div> -->

                  <div class="btn-group"> 
                     <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                        title="Refresh" 
                        href="{{ $module_url_path }}"
                        style="text-decoration:none;">
                     <i class="fa fa-repeat"></i>
                     </a> 
                  </div>
               </div>
               <br/><br/>
               <div class="clearfix"></div>
               <div class="table-responsive" style="border:0">
                  <input type="hidden" name="multi_action" value="" />
                  <table class="table table-advance"  id="table1" >
                     <thead>
                        <tr>
                          <!-- <th style="width:18px"> <input type="checkbox" name="mult_change" id="mult_change" /></th> -->
                           <th>Name</th>
                           <th>Type</th>
                           <th>Price (USD)</th>
                           <th>Price (EUR)</th>
                           <th>Off</th>
                           <th>Commision</th>
                           <th>5 Bids Price</th>
                           <th>Bids</th>
                           <th>Categories</th>
                           <th>Subcategories</th>
                           <th>Skills</th>
                           <th>Favorites<br/>Projects</th>
                          <!-- <th>Email</th>
                           <th>Chat</th>
                           <th>Status</th> -->
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        @if(isset($arr_packs) && sizeof($arr_packs)>0)
                        @foreach($arr_packs as $pack)
                        <tr>
                          <!-- <td> 
                              <input type="checkbox" 
                                 name="checked_record[]"  
                                 value="{{ base64_encode($pack['id']) }}" /> 
                           </td> -->
                           <td> {{ isset($pack['pack_name'])?$pack['pack_name']:''  }}

                           <!-- @if(isset($pack['is_default']) && $pack['is_default']==1)
                              
                                &nbsp;&nbsp; <span class="badge badge-info"> Default </span>
                              
                            @endif 

                            -->

                            </td>
                             <td> {{ isset($pack['type'])?$pack['type']:'' }} </td>
                           <td> {{ isset($pack['pack_price'])?number_format($pack['pack_price'],2):'0' }} </td>
                            <td> {{ isset($pack['pack_price_eur'])?number_format($pack['pack_price_eur'],2):'0' }} </td>
                           <td> {{ isset($pack['off'])?$pack['off'].' %':'0' }} </td>
                           <td> {{ isset($pack['website_commision'])?$pack['website_commision'].' %':'0' }} </td>

                           <td> {{ isset($pack['topup_bid_price'])?number_format($pack['topup_bid_price'],2):'0' }} </td>

                           <td> {{ isset($pack['number_of_bids'])?$pack['number_of_bids']:'0'  }} </td>
                           <td> {{ isset($pack['number_of_categories'])?$pack['number_of_categories']:'0' }} </td>
                           <td> {{ isset($pack['number_of_subcategories'])?$pack['number_of_subcategories']:'0' }} </td>
                           <td> {{ isset($pack['number_of_skills'])?$pack['number_of_skills']:'0'  }} </td>
                           <td> {{ isset($pack['number_of_favorites_projects'])?$pack['number_of_favorites_projects']:'0'  }} </td>
                           
                         <!--  <td>
                              @if(isset($pack['allow_email']) && $pack['allow_email']==1)
                              <p class="">
                                 <span class="badge badge-lime"> YES </span>
                              </p>
                              @else
                              <p class="">
                                 <span class="badge badge-important">NO </span>
                              </p>
                              @endif
                           </td>
                           <td>
                              @if(isset($pack['allow_chat']) && $pack['allow_chat']==1)
                              <p class="">
                                 <span class="badge badge-lime"> YES </span>
                              </p>
                              @else
                              <p class="">
                                 <span class="badge badge-important">NO </span>
                              </p>
                              @endif
                           </td> -->
                          <!-- <td>
                              @if(isset($pack['is_active']) && $pack['is_active']==1)
                              <a href="{{ $module_url_path.'/deactivate/'.base64_encode($pack['id']) }}" class="btn btn-success">Active</a>
                              @else
                              <a href="{{ $module_url_path.'/activate/'.base64_encode($pack['id']) }}" class="btn btn-danger">Inactive</a>
                              @endif
                           </td> -->
                           <td> 
                              <a href="{{ $module_url_path.'/edit/'.base64_encode($pack['id']) }}"  title="Edit" class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip">
                              <i class="fa fa-edit" ></i>
                              </a>  
                              &nbsp;  
                             <!--  <a href="{{ $module_url_path.'/set_default/'.base64_encode($pack['id']) }}"  title="Set Default" class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip">
                              <i class="fa fa-check-square-o" ></i>
                              </a>  
                              &nbsp;   -->
                           </td>
                        </tr>
                        @endforeach
                        @endif
                     </tbody>
                  </table>
               </div>
               <div>   </div>
            </form>
         </div>
      </div>
   </div>
</div>
<!-- END Main Content -->
<script type="text/javascript">
   function confirm_delete(url) {
      alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
          if (e) {
              if(url != "" && url != undefined){
                showProcessingOverlay();
                window.location.href=url;
              }
              return true;
          } else {
              return false;
          }
      });
   }
   function check_multi_action(frm_id,action){
     var frm_ref = jQuery("#"+frm_id);
     if(jQuery(frm_ref).length && action!=undefined && action!=""){
       if(action == 'delete'){
            if (!confirm_delete()){
                return false;
            }
       }
       /* Get hidden input reference */
       var input_multi_action = jQuery('input[name="multi_action"]');
       if(jQuery(input_multi_action).length){
         /* Set Action in hidden input*/
         jQuery('input[name="multi_action"]').val(action);
         /*Submit the referenced form */
         jQuery(frm_ref)[0].submit();
       } else {
         console.warn("Required Hidden Input[name]: multi_action Missing in Form ")
       }
     } else {
         console.warn("Required Form[id]: "+frm_id+" Missing in Current category ")
     }
   }
</script>
@stop