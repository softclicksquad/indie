@extends('admin.layout.master')                
@section('main_content')
<style>
    .pack-price-in-section input{margin-bottom: 15px;}
</style>
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-suitcase"></i>
      <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-edit"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-edit"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
            @include('admin.layout._operation_status')  
            <form name="validation-form" id="validation-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/update/{{$enc_id}}" enctype="multipart/form-data">
               {{ csrf_field() }}

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="type">Pack Type<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input  type="text" name="type" class="form-control" value="{{isset($arr_pack['type'])?$arr_pack['type']:''}}" readonly>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="pack_name">Pack Name<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="pack_name" class="form-control" value="{{isset($arr_pack['pack_name'])?$arr_pack['pack_name']:''}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Pack Name">
                     <span class='error'>{{ $errors->first('pack_name') }}</span>
                  </div>
               </div>

               {{-- <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="pack_price">Pack Price (USD)<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="pack_price" class="form-control" value="{{isset($arr_pack['pack_price'])?$arr_pack['pack_price']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true" placeholder="Pack Price">
                     <span class='error'>{{ $errors->first('pack_price') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="pack_price_eur">Pack Price (EUR)<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="pack_price_eur" class="form-control" value="{{isset($arr_pack['pack_price_eur'])?$arr_pack['pack_price_eur']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true" placeholder="Pack Price">
                     <span class='error'>{{ $errors->first('pack_price_eur') }}</span>
                  </div>
               </div> --}}

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="number_of_bids">Number Of Bids<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="number_of_bids" class="form-control" value="{{isset($arr_pack['number_of_bids'])?$arr_pack['number_of_bids']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true" placeholder="Number Of Bids">
                     <span class='error'>{{ $errors->first('number_of_bids') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="topup_bid_price">5 Bids Price<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="topup_bid_price" class="form-control" value="{{isset($arr_pack['topup_bid_price'])?$arr_pack['topup_bid_price']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true" placeholder="5 Bids Price">
                     <span class='error'>{{ $errors->first('topup_bid_price') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="number_of_categories">Number Of Categories<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="number_of_categories" class="form-control" value="{{isset($arr_pack['number_of_categories'])?$arr_pack['number_of_categories']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true" data-rule-pattern="^[0-9]*$" data-msg-pattern="Please enter valid data" placeholder="Number Of Categories">
                     <span class='error'>{{ $errors->first('number_of_categories') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="number_of_categories">Number Of Subcategories under each category<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="number_of_subcategories" class="form-control" value="{{isset($arr_pack['number_of_subcategories'])?$arr_pack['number_of_subcategories']:''}}" data-rule-required="true" data-rule-maxlength="9" data-rule-number="true" placeholder="Number Of Subcategories" data-rule-pattern="^[0-9]*$" data-msg-pattern="Please enter valid data">
                     <span class='error'>{{ $errors->first('number_of_subcategories') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="number_of_skills">Number Of Skills<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="number_of_skills" class="form-control" value="{{isset($arr_pack['number_of_skills'])?$arr_pack['number_of_skills']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true" data-rule-pattern="^[0-9]*$" data-msg-pattern="Please enter valid data" placeholder="Number Of Skills">
                     <span class='error'>{{ $errors->first('number_of_skills') }}</span>
                  </div>
               </div>

                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="number_of_favorites_projects">Number Of Favorites Projects<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="number_of_favorites_projects" class="form-control" value="{{isset($arr_pack['number_of_favorites_projects'])?$arr_pack['number_of_favorites_projects']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true" placeholder="Number Of Favorites Projects">
                     <span class='error'>{{ $errors->first('number_of_favorites_projects') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="number_of_payouts">Number Of Payout<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="number_of_payouts" class="form-control" value="{{isset($arr_pack['number_of_payouts'])?$arr_pack['number_of_payouts']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true" placeholder="Number Of Payout">
                     <span class='error'>{{ $errors->first('number_of_payouts') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="off">off (%)<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="off" class="form-control" value="{{isset($arr_pack['off'])?$arr_pack['off']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true" placeholder="Off ">
                     <span class='error'>{{ $errors->first('off') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="website_commision">Website Commision (%)<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="website_commision" class="form-control" value="{{isset($arr_pack['website_commision'])?$arr_pack['website_commision']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true" placeholder="Website commision">
                     <span class='error'>{{ $errors->first('website_commision') }}</span>
                  </div>
               </div>

               <div class="form-group pack-price-in-section">
                  <label class="col-sm-3 col-lg-2 control-label" for="pack_price">Pack Price In</label>
                  <div class="col-sm-9 col-lg-10 controls">
                    <div class="row">
                      <label class="col-sm-1 col-lg-1 control-label" for="pack_price">USD<i class="red">*</i></label>
                      <div class="col-sm-1 col-lg-1 controls">
                         <input type="text" name="pack_price" class="form-control" value="{{isset($arr_pack['pack_price'])?$arr_pack['pack_price']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true" >
                         <span class='error'>{{ $errors->first('pack_price') }}</span>
                      </div>

                      <label class="col-sm-1 col-lg-1 control-label" for="pack_price_eur">EUR<i class="red">*</i></label>
                      <div class="col-sm-1 col-lg-1 controls">
                         <input type="text" name="pack_price_eur" class="form-control" value="{{isset($arr_pack['pack_price_eur'])?$arr_pack['pack_price_eur']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true" >
                         <span class='error'>{{ $errors->first('pack_price_eur') }}</span>
                      </div>

                      <label class="col-sm-1 col-lg-1 control-label" for="pack_price_gbp">GBP<i class="red">*</i></label>
                      <div class="col-sm-1 col-lg-1 controls">
                         <input type="text" name="pack_price_gbp" class="form-control" value="{{isset($arr_pack['pack_price_gbp'])?$arr_pack['pack_price_gbp']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true" >
                         <span class='error'>{{ $errors->first('pack_price_gbp') }}</span>
                      </div>

                      <label class="col-sm-1 col-lg-1 control-label" for="pack_price_pln">PLN<i class="red">*</i></label>
                      <div class="col-sm-1 col-lg-1 controls">
                         <input type="text" name="pack_price_pln" class="form-control" value="{{isset($arr_pack['pack_price_pln'])?$arr_pack['pack_price_pln']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true" >
                         <span class='error'>{{ $errors->first('pack_price_pln') }}</span>
                      </div>

                      <label class="col-sm-1 col-lg-1 control-label" for="pack_price_chf">CHF<i class="red">*</i></label>
                      <div class="col-sm-1 col-lg-1 controls">
                         <input type="text" name="pack_price_chf" class="form-control" value="{{isset($arr_pack['pack_price_chf'])?$arr_pack['pack_price_chf']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true" >
                         <span class='error'>{{ $errors->first('pack_price_chf') }}</span>
                      </div>

                      <label class="col-sm-1 col-lg-1 control-label" for="pack_price_nok">NOK<i class="red">*</i></label>
                      <div class="col-sm-1 col-lg-1 controls">
                         <input type="text" name="pack_price_nok" class="form-control" value="{{isset($arr_pack['pack_price_nok'])?$arr_pack['pack_price_nok']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true" >
                         <span class='error'>{{ $errors->first('pack_price_nok') }}</span>
                      </div>

                      <label class="col-sm-1 col-lg-1 control-label" for="pack_price_sek">SEK<i class="red">*</i></label>
                      <div class="col-sm-1 col-lg-1 controls">
                         <input type="text" name="pack_price_sek" class="form-control" value="{{isset($arr_pack['pack_price_sek'])?$arr_pack['pack_price_sek']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true">
                         <span class='error'>{{ $errors->first('pack_price_sek') }}</span>
                      </div>

                      <label class="col-sm-1 col-lg-1 control-label" for="pack_price_dkk">DKK<i class="red">*</i></label>
                      <div class="col-sm-1 col-lg-1 controls">
                         <input type="text" name="pack_price_dkk" class="form-control" value="{{isset($arr_pack['pack_price_dkk'])?$arr_pack['pack_price_dkk']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true">
                         <span class='error'>{{ $errors->first('pack_price_dkk') }}</span>
                      </div>

                      <label class="col-sm-1 col-lg-1 control-label" for="pack_price_cad">CAD<i class="red">*</i></label>
                      <div class="col-sm-1 col-lg-1 controls">
                         <input type="text" name="pack_price_cad" class="form-control" value="{{isset($arr_pack['pack_price_cad'])?$arr_pack['pack_price_cad']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true">
                         <span class='error'>{{ $errors->first('pack_price_cad') }}</span>
                      </div>

                      <label class="col-sm-1 col-lg-1 control-label" for="pack_price_zar">ZAR<i class="red">*</i></label>
                      <div class="col-sm-1 col-lg-1 controls">
                         <input type="text" name="pack_price_zar" class="form-control" value="{{isset($arr_pack['pack_price_zar'])?$arr_pack['pack_price_zar']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true">
                         <span class='error'>{{ $errors->first('pack_price_zar') }}</span>
                      </div>

                      <label class="col-sm-1 col-lg-1 control-label" for="pack_price_aud">AUD<i class="red">*</i></label>
                      <div class="col-sm-1 col-lg-1 controls">
                         <input type="text" name="pack_price_aud" class="form-control" value="{{isset($arr_pack['pack_price_aud'])?$arr_pack['pack_price_aud']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true">
                         <span class='error'>{{ $errors->first('pack_price_aud') }}</span>
                      </div>

                      <label class="col-sm-1 col-lg-1 control-label" for="pack_price_hkd">HKD<i class="red">*</i></label>
                      <div class="col-sm-1 col-lg-1 controls">
                         <input type="text" name="pack_price_hkd" class="form-control" value="{{isset($arr_pack['pack_price_hkd'])?$arr_pack['pack_price_hkd']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true">
                         <span class='error'>{{ $errors->first('pack_price_hkd') }}</span>
                      </div>

                      <label class="col-sm-1 col-lg-1 control-label" for="pack_price_jpy">JPY<i class="red">*</i></label>
                      <div class="col-sm-1 col-lg-1 controls">
                         <input type="text" name="pack_price_jpy" class="form-control" value="{{isset($arr_pack['pack_price_jpy'])?$arr_pack['pack_price_jpy']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true">
                         <span class='error'>{{ $errors->first('pack_price_jpy') }}</span>
                      </div>

                      <label class="col-sm-1 col-lg-1 control-label" for="pack_price_czk">CZK<i class="red">*</i></label>
                      <div class="col-sm-1 col-lg-1 controls">
                         <input type="text" name="pack_price_czk" class="form-control" value="{{isset($arr_pack['pack_price_czk'])?$arr_pack['pack_price_czk']:''}}" data-rule-required="true"  data-rule-maxlength="9" data-rule-number="true">
                         <span class='error'>{{ $errors->first('pack_price_czk') }}</span>
                      </div>
                      </div>
                  </div>

                  

               </div>

               

              <!--    
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Communication Via Email<i class="red">*</i></label>
                  <div class="col-sm-9 col-lg-10 controls">
                     <label class="radio-inline">
                        
                        <input type="radio" value="1" name="allow_email"
                        @if(isset($arr_pack['allow_email']) && $arr_pack['allow_email']==1)
                        checked="" 
                        @endif
                        >
                         <p class="">
                           <span class="badge badge-lime"> YES </span>
                        </p>
                     </label>
                     <label class="radio-inline">
                        <input type="radio" value="0" name="allow_email"
                        @if(isset($arr_pack['allow_email']) && $arr_pack['allow_email']==0)
                        checked="" 
                        @endif
                        >
                        <p class="">
                           <span class="badge badge-important"> NO </span>
                        </p>
                     </label>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Communication Via Chat<i class="red">*</i></label>
                  <div class="col-sm-9 col-lg-10 controls">
                     <label class="radio-inline">
                        <input type="radio" value="1" name="allow_chat"
                        @if(isset($arr_pack['allow_chat']) && $arr_pack['allow_chat']==1)
                        checked="" 
                        @endif
                        >
                        <p class="">
                           <span class="badge badge-lime"> YES </span>
                        </p>
                     </label>
                     <label class="radio-inline">
                        <input type="radio" value="0" name="allow_chat"
                        @if(isset($arr_pack['allow_chat']) && $arr_pack['allow_chat']==0)
                        checked="" 
                        @endif
                        >
                        <p class="">
                           <span class="badge badge-important"> NO </span>
                        </p>
                     </label>
                  </div>
               </div> -->


               <br>
               <div class="form-group">
                  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                     <input type="submit" value="Update" class="btn btn btn-primary">
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<!-- END Main Content -->
@stop
