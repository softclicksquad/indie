    @extends('admin.layout.master')                
    @section('main_content')
    
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{ url('/'.$admin_panel_slug.'/dashboard') }}">Dashboard</a>
            </li>
            <span class="divider">
                <i class="fa fa-angle-right"></i>
            </span>
            
            <li class="active">  {{ isset($page_title)?$page_title:"" }}</li>
        </ul>
    </div>
    <!-- BEGIN Main Content -->
        <div class="row">
        <div class="col-md-12">
            <div class="box box-blue">
                <div class="box-title">
                    <h3><i class="fa fa-file"></i> Edit Profile</h3>
                    
                </div>
                <div class="box-content">
                    <form class="form-horizontal" 
                        id="validation-form" 
                        method="POST" 
                        action="{{ url($module_url_path) }}/update"
                        enctype="multipart/form-data" 
                        files ="true"
                        >   
                        {{ csrf_field() }}

                        <div id="op_status">
                            @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                {{ Session::get('success') }}
                            </div>
                          @endif  

                          @if(Session::has('error'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">x</span>
                                </button>
                                {{ Session::get('error') }}
                            </div>
                          @endif
                        </div>


                        <div class="form-group">
                          <label class="col-sm-3 col-lg-2 control-label">Image:</label>
                          <div class="col-sm-9 col-lg-2 controls">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">

                                        @if(isset($admin_img_path) && isset($arr_profile['image']))
                                        <img src="{{$admin_img_path.$arr_profile['image']}}" alt="" />
                                        @endif
                                </div>
                                <div class="fileupload-preview fileupload-exists img-thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <div>
                                    <span class="btn btn-default btn-file" style="height:32px;">
                                            <span class="fileupload-new">Select Image</span>
                                            <span class="fileupload-exists">Change</span>
                                            <input type="file" class="file-input" name="image" id="image"  />
                                            <input type="hidden" class="form-control" name="image" id="oldprofileimage" value="" />
                                            
                                    </span>
                                    <a href="#" id="remove" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                </div>
                            </div>
                            
                            
                          </div>
                          <div class="clearfix"></div>
                          
                    </div>
                       <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label"></label>
                            <div class="col-sm-9 col-lg-4 controls">
                            <span class="label label-important">NOTE!</span>
                                <span>Image Formate: jpeg,png,gif and Size should be less than or equalto 300px*300px.</span>    
                                  
                            </div>
                        </div>

                         <input type="hidden" class="form-control" name="old_profile_image" value="{{isset($arr_profile['image'])?$arr_profile['image']:''}}" />

                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label">Name<span class="red">*</span></label>
                            <div class="col-sm-9 col-lg-4 controls">
                                
                                  <input type="text" class="form-control" name="name" id="name" data-rule-required="true" value="{{isset($arr_profile['name'])?$arr_profile['name']:''}}" />
                                <span class='error'>{{ $errors->first('name') }}</span>
                            </div>
                        </div>

                        <?php 
                            $admin_email = "";
                            
                            $user = Sentinel::check();

                            if($user)
                            {
                                $admin_email = $user->email;
                            }
                        ?>

                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label">Email<span class="red">*</span></label>
                            <div class="col-sm-9 col-lg-4 controls">
                                  <input type="text" class="form-control" name="email" id="email" data-rule-required="true" data-rule-email="true" value="{{$admin_email}}" />
                                <span class='error'>{{ $errors->first('email') }}</span>
                                <br>
                                <span class="label label-important">NOTE!</span>
                                <span>This Email Id will be used to receive emails sent to admin from experts, clients and project managers.</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label">Contact Number<span class="red">*</span></label>
                            <div class="col-sm-9 col-lg-4 controls">
                                 
                                  <input type="text" class="form-control" name="contact_number" id="contact_number" data-rule-required="true" data-rule-minlength="6" data-rule-maxlength="16" data-rule-number="true" value="{{isset($arr_profile['contact_number'])?$arr_profile['contact_number']:''}}"/>
                                <span class='error'>{{ $errors->first('contact_number') }}</span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label">Fax</label>
                            <div class="col-sm-9 col-lg-4 controls">
                                 
                                  <input type="text" class="form-control" name="fax" id="fax" data-rule-number="true" value="{{isset($arr_profile['fax'])?$arr_profile['fax']:''}}"/>
                                <span class='error'>{{ $errors->first('fax') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label">Address<span class="red">*</span></label>
                            <div class="col-sm-9 col-lg-4 controls">

                                <textarea class="form-control" name="address" data-rule-required="true">{{isset($arr_profile['address'])?$arr_profile['address']:''}}</textarea>
                                
                                <span class='error'>{{ $errors->first('address') }}</span>
                            </div>
                        </div>
                        
                       
                        
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3 col-lg-4 col-lg-offset-2">
                                <button type="submit" class="btn btn-primary">Update</button>
                                {{-- <button type="button" class="btn">Cancel</button> --}}
                            </div>
                       </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<!-- END Main Content -->
@stop