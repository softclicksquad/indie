    @extends('admin.layout.master')


    @section('main_content')
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>

      </div>
    </div>
    <!-- END Page Title -->

    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="fa fa-home"></i>
          <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
        </li>
        <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-desktop"></i>
          <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
        </span> 
        <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-list"></i>
        </span>
        <li class="active">{{ $page_title or ''}}</li>
      </ul>
    </div>
    <!-- END Breadcrumb -->


    <!-- BEGIN Main Content -->
    <div class="row">
      <div class="col-md-12">

        <div class="box">
          <div class="box-title">
            <h3>
              <i class="fa fa-text-width"></i>
              {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
              <a data-action="collapse" href="#"></a>
              <a data-action="close" href="#"></a>
            </div>
          </div>
          <div class="box-content">

            @include('admin.layout._operation_status') 

            <div class="tabbable">
              <form name="validation-form" id="validation-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/send_mail" enctype="multipart/form-data"> 
              {{ csrf_field() }}


               <ul  class="nav nav-tabs">
                @include('admin.layout._multi_lang_tab')
              </ul>                                
              <div id="myTabContent1" class="tab-content">

              
                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="page_name">Title<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                      <input type="text" name="title" id="title" class="form-control" placeholder="Title">
                    <span class='error'>{{ $errors->first('title') }}</span>
                  </div>
                </div>

                 <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="page_name">Subject<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                      <input type="text" name="subject" id="subject" class="form-control" placeholder="Subject">
                    <span class='error'>{{ $errors->first('subject') }}</span>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="page_desc">Message<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-8 controls">
                         <textarea name="message" id="message" rows="7" class="form-control"  placeholder="Message"></textarea>
                    <span class='error'>{{ $errors->first('message') }}</span>
                  </div>
                </div>
                
              </div>
            
            </div>

            <br>
            <div class="form-group">
              <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
               <input type="submit" value="Save" class="btn btn btn-primary">
              </div>
            </div>
            </form>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- END Main Content -->

  <script type="text/javascript">

    function saveTinyMceContent()
    {
      tinyMCE.triggerSave();
    }

    $(document).ready(function()
    {
      tinymce.init({
        selector: 'textarea',
        height:350,
        plugins: [
        'advlist autolink lists link charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime table contextmenu paste code'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
        content_css: [
        '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
        '//www.tinymce.com/css/codepen.min.css'
        ]
      });  
    });
  </script>

  @stop
