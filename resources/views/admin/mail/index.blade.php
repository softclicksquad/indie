@extends('admin.layout.master')
  @section('main_content')
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="fa fa-home"></i>
          <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
        </li>
        <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-envelope-o"></i>
          <a href="{{ $module_url_path.'/listing' }}">{{ 'Sent listings'}}</a>
        </span> 
        <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-envelope-o"></i>
        </span>
        <li class="active">{{ $page_title or ''}}</li>
      </ul>
    </div>
    <!-- END Breadcrumb -->
    <!-- BEGIN Main Content -->
    <div class="row">
      <div class="col-md-12">

        <div class="box">
          <div class="box-title">
            <h3>
              <i class="fa fa-envelope-o"></i>
              {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
              <a data-action="collapse" href="#"></a>
              <a data-action="close" href="#"></a>
            </div>
          </div>
          <div class="box-content">

            @include('admin.layout._operation_status') 

            <div class="tabbable">
            
               <ul  class="nav nav-tabs">
                @include('admin.layout._multi_lang_tab')
              </ul>                                
              <div id="myTabContent1" class="tab-content">
              <form name="validation-form" id="validation-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/send" enctype="multipart/form-data"> 
              {{ csrf_field() }}

                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="page_name">Mail To<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                  <input class="form-control" placeholder="To" name="message_to" id="message_to" data-rule-required="true" data-rule-email="true"/>
                    <span class='error'>{{ $errors->first('message_to') }}</span>
                  </div>
                </div>

                 <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="page_name">Subject<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                      <input type="text" name="subject" id="subject" data-rule-required="true" data-rule-maxlength="255" class="form-control" placeholder="Subject">
                    <span class='error'>{{ $errors->first('subject') }}</span>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="page_desc">Message<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-8 controls">
                         <textarea name="message" id="message" rows="7" data-rule-required="true" data-rule-maxlength="255" class="form-control"  placeholder="Message"></textarea>
                    <span class='error'>{{ $errors->first('message') }}</span>
                  </div>
                </div>
                <br>
              <div class="form-group">
              <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
               <input type="submit" value="Send" class="btn btn btn-primary">
              </div>
             </div>
            </form>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
  <!-- END Main Content -->

  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script type="text/javascript">

  var availableUses = [];

  $("#message_to").keyup(function() 
  {
    var search_text =  $("#message_to").val();
    var url = "{{ $module_url_path.'/search_users/'}}"+search_text;    
    if (search_text!="") 
    {
        $.ajax({

            type:'GET',
            url: url,
            data: search_text,
            dataType: 'json',
            success: function (data) 
            {
              availableUses.splice(0,availableUses.length);

              if (data.length > 0) 
              {
                jQuery.each(data,function(index,mail)
                {
                    availableUses.push(mail.email); 
                    //console.log(mail);
                });
              }
              else
              {
                availableUses.push('no user found'); 
              }
              
                      
              $( "#message_to" ).autocomplete({
                source: availableUses
              });  
              
              return false;
            }
          });
    }
  });

  $( "#message_to" ).autocomplete({
      /*source: availableTags*/
    });

  
  </script>

@stop                    
