@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-envelope-o"></i>
          <a href="{{ $module_url_path.'/'.'listing'}}">{{ 'Listing' }}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-envelope-o"></i>
      </span>
      <li class="active">{{ isset($page_title)?str_plural($page_title):"" }}</li>
   </ul>
</div>
<!-- END Breadcrumb -->

<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-envelope-o"></i>
               {{ isset($arr_info['project_name'])?$arr_info['project_name']:"" }}
                @if(isset($arr_info['project_name']) &&  $arr_info['project_name'] != "" )
                  <span class="divider">
                    <i class="fa fa-angle-right"></i> 
                  </span>  
                @endif
                {{ isset($page_title)?str_plural($page_title):"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
            <div class="row">
               @if(isset($sent_mails_details) && sizeof($sent_mails_details)>0)
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Mail number:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($sent_mails_details['unique_id'])?$sent_mails_details['unique_id']:''}}</div>
                     </div>
                  </div>
                </div>
                <div class="clearfix"></div><br>
                <div class="col-md-6">
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Mail from:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($sent_mails_details['mail_from'])?$sent_mails_details['mail_from']:''}}</div>
                     </div>
                  </div>
                </div>
                <div class="clearfix"></div><br>
                <div class="col-md-6">
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Mail to:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($sent_mails_details['mail_to'])?$sent_mails_details['mail_to']:''}}</div>
                     </div>
                  </div>
                </div>
                <div class="clearfix"></div><br>
                <div class="col-md-6">
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Subject:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($sent_mails_details['subject'])?$sent_mails_details['subject']:''}}</div>
                     </div>
                  </div>
                </div>
                <div class="clearfix"></div><br>
                <div class="col-md-6">
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Message:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($sent_mails_details['message'])?$sent_mails_details['message']:''}}</div>
                     </div>
                  </div>
                </div>
                <div class="clearfix"></div><br>
                <div class="col-md-6">
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Sent date:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">
                          <?php echo date('j M  Y', strtotime($sent_mails_details['created_at']))." at ".date("g:i a", strtotime($sent_mails_details['created_at'])); ?>
                        </div>
                     </div>
                  </div>
                </div>
                <div class="clearfix"></div><br>
                <div class="col-md-6">
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Is sent:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">
                          @if($sent_mails_details['is_sent'] == 'YES') 
                            <span class="btn btn-success btn-add-new-records" title="{{isset($sent_mails_details['is_sent'])?$sent_mails_details['is_sent']:'-'}}">{{isset($sent_mails_details['is_sent'])?$sent_mails_details['is_sent']:'-'}}</span> 
                          @else
                            <span class="btn btn-danger btn-add-new-records"  title="{{isset($sent_mails_details['is_sent'])?$sent_mails_details['is_sent']:'-'}}">{{isset($sent_mails_details['is_sent'])?$sent_mails_details['is_sent']:'-'}}</span>     
                          @endif
                        </div>
                     </div>
                  </div>
                </div>
                <div class="clearfix"></div><br>
                <div class="col-md-6">
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Un-sent reason:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">
                          {{isset($sent_mails_details['unsent_reasone'])?$sent_mails_details['unsent_reasone']:'-'}}
                        </div>
                     </div>
                  </div>
                </div>
               @endif
           </div>
         </div>
       </div>
   </div>
 </div>   
@stop