@extends('admin.layout.master')                
    @section('main_content')
    <!-- BEGIN Page Title -->
    <link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/data-tables/latest/dataTables.bootstrap.min.css">
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
       <ul class="breadcrumb">
          <li>
             <i class="fa fa-home"></i>
             <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
          </li>
          <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-user-plus"></i>
          </span>
          <li class="active">{{ isset($page_title)?str_plural($page_title):"" }}</li>
       </ul>
    </div>
    <!-- END Breadcrumb -->
    <!-- BEGIN Main Content -->
    <div class="row">
      <div class="col-md-12">
          <div class="box">
            <div class="box-title">
              <h3>
                <i class="fa fa-envelope-o"></i>
                {{ isset($page_title)?str_plural($page_title):"" }}
            </h3>
            <div class="box-tool">
                <a data-action="collapse" href="#"></a>
                <a data-action="close" href="#"></a>
            </div>
        </div>
        <div class="box-content">
          @include('admin.layout._operation_status')
          <form name="frm_manage" id="frm_manage" method="POST" class="form-horizontal" action="{{$module_url_path}}/multi_action">
            {{ csrf_field() }}
            <div class="btn-toolbar pull-right clearfix">
            <div class="btn-group">
               <a href="{{ $module_url_path}}" class="btn btn-primary btn-add-new-records"  title="Send">Send</a>                      
            </div>
            </div>
            <br/><br/>
            <div class="clearfix"></div>
            <div class="table-responsive" style="border:0">
              <input type="hidden" name="multi_action" value="" />
              <table class="table table-advance"  id="table1" >
                <thead>
                  <tr>
                    <th>Mail number</th>
                    <th>Mail from</th>
                    <th>Mail to</th>
                    <th>Subject</th>
                    <th>Sent date</th> 
                    <th>Is sent</th> 
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @if(isset($arr_sent_mails) && sizeof($arr_sent_mails)>0)
                    @foreach($arr_sent_mails as $sent_mails)
                    <tr>
                        <td> 
                           {{isset($sent_mails['unique_id'])?$sent_mails['unique_id']:'-'}}
                        </td> 
                        <td> 
                           {{isset($sent_mails['mail_from'])?$sent_mails['mail_from']:'-'}}
                        </td> 
                        <td> 
                           {{isset($sent_mails['mail_to'])?$sent_mails['mail_to']:'-'}}
                        </td> 
                        <td> 
                           {{isset($sent_mails['subject'])?$sent_mails['subject']:'-'}}
                        </td> 
                        <td> 
                           <?php echo date('j M  Y', strtotime($sent_mails['created_at']))." at ".date("g:i a", strtotime($sent_mails['created_at'])); ?>
                        </td>
                        <td> 
                          @if($sent_mails['is_sent'] == 'YES') 
                            <span class="btn btn-success btn-add-new-records" title="{{isset($sent_mails['is_sent'])?$sent_mails['is_sent']:'-'}}">{{isset($sent_mails['is_sent'])?$sent_mails['is_sent']:'-'}}</span> 
                          @else
                            <span class="btn btn-danger btn-add-new-records"  title="{{isset($sent_mails['is_sent'])?$sent_mails['is_sent']:'-'}}">{{isset($sent_mails['is_sent'])?$sent_mails['is_sent']:'-'}}</span>     
                          @endif
                        </td> 
                        <td>
                            <a  class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" href="{{ $module_url_path.'/details/'.base64_encode($sent_mails['id']) }}"  title="Edit">
                             <i class="fa fa-eye"></i>
                            </a>  
                         </td>
                    </tr>
                    @endforeach
                  @endif
                </tbody>
              </table>
            </div>
          <div>   
        </div>
      </form>
    </div>
  </div>
</div>
</div>
<!-- END Main Content -->
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/dataTables.bootstrap.min.js"></script>
@stop                    


