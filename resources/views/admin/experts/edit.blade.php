@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-users"></i>
      <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-edit"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-edit"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
            @include('admin.layout._operation_status')  
            <form name="validation-form" id="validation-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/update/{{$enc_id}}" enctype="multipart/form-data">
               {{ csrf_field() }}

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Profile Image:</label>
                  <div class="col-sm-9 col-lg-2 controls">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">

                                @if(isset($profile_img_path) && isset($arr_expert['profile_image']))
                                <img src="{{$profile_img_path.$arr_expert['profile_image']}}" alt="" />
                                @else
                                <img src="{{url('/').'/uploads/front/profile/default_profile_image.png'}}" alt="" />
                                
                                @endif
                        </div>
                    </div>
                  </div>
              </div>
               
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="email">Email</label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="email" class="form-control" value="{{isset($arr_expert['user_details']['email'])?$arr_expert['user_details']['email']:''}}" data-rule-required="true"  data-rule-required="email" placeholder="Email" disabled="true">
                     <span class='error'>{{ $errors->first('email') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="">Username</label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="" disabled="" class="form-control" value="{{ $arr_expert['user_details']['user_name'] or '-'}}" placeholder="Username">
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="first_name">First Name<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="first_name" class="form-control" value="{{isset($arr_expert['first_name'])?$arr_expert['first_name']:''}}" data-rule-required="true" data-rule-maxlength="255" placeholder="First Name">
                     <span class='error'>{{ $errors->first('first_name') }}</span>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="last_name">Last Name<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="last_name" class="form-control" value="{{isset($arr_expert['last_name'])?$arr_expert['last_name']:''}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Last Name">
                     <span class='error'>{{ $errors->first('last_name') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="phone">Contact Number<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-1 controls">
                     <input type="text" name="phone_code" class="form-control" value="{{isset($arr_expert['phone_code'])?$arr_expert['phone_code']:''}}" data-rule-required="true" data-rule-maxlength="6" data-rule-minlength="2" data-rule-number="true" placeholder="Code">
                     <span class='error'>{{ $errors->first('phone_code') }}</span>
                  </div>
                  <div class="col-sm-6 col-lg-3 controls">
                     <input type="text" name="phone_number" class="form-control" value="{{isset($arr_expert['phone_number'])?$arr_expert['phone_number']:''}}" data-rule-required="true" data-rule-minlength="10" data-rule-maxlength="12" data-rule-number="true" placeholder="Contact Number">
                     <span class='error'>{{ $errors->first('phone') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="country_name">Country<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                    <!--  <select type="text" name="country" class="form-control" data-rule-required="true">
                           <option value="">---Select Country---</option> -->
                        @if(isset($arr_countries) && sizeof($arr_countries)>0)
                        <select class="form-control" data-rule-required="true" name="country" id="country_expert" onchange="javascript: return loadStates(this);">
                           <option value="">---Select Country---</option>

                           @foreach($arr_countries as $countries)
                           @if($countries['id']==$arr_expert['country'])

                           <option value="{{isset($arr_expert['country'])?$arr_expert['country']:""}}" selected="selected">
                           {{isset($arr_expert['country_details']['country_name'])?$arr_expert['country_details']['country_name']:"" }}
                          </option>

                           @else
                            <option value="{{isset($countries['id'])?$countries['id']:""}}">
                           {{isset($countries['country_name'])?$countries['country_name']:"" }}
                           </option>

                        @endif
                        @endforeach
                     </select>
                     @endif
                     <span class='error'>{{ $errors->first('country') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="state_name">State<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                    <!--  <select type="text" name="country" class="form-control" data-rule-required="true">
                           <option value="">---Select Country---</option> -->
                       
                        <select class="form-control" data-rule-required="true" name="state" id="state_expert" onchange="javascript: return loadCities(this);">
                         @if(isset($arr_expert['country_details']['states']) && sizeof($arr_expert['country_details']['states'])>0)
                           <option value="">---Select State---</option>

                           @foreach($arr_expert['country_details']['states'] as $states)
                           @if($states['id']==$arr_expert['state'])

                              <option value="{{isset($arr_expert['state'])?$arr_expert['state']:""}}" selected="selected">
                                 {{isset($arr_expert['state_details']['state_name'])?$arr_expert['state_details']['state_name']:"" }}
                                </option>
                            @else
                              <option value="{{isset($states['id'])?$states['id']:""}}">
                                 {{isset($states['state_name'])?$states['state_name']:"" }}
                                 </option>

                              @endif
                        @endforeach
                        @endif
                     </select>
                     
                     <span class='error'>{{ $errors->first('state') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="country_name">City<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                    <!--  <select type="text" name="country" class="form-control" data-rule-required="true">
                           <option value="">---Select Country---</option> -->
                            <select class="form-control" data-rule-required="true" name="city" id="city_expert">
                        @if(isset($arr_expert['state_details']['cities']) && sizeof($arr_expert['state_details']['cities'])>0)
                       
                           <option value="">---Select City---</option>

                           @foreach($arr_expert['state_details']['cities'] as $cities)
                           @if($cities['id']==$arr_expert['city'])

                              <option value="{{isset($arr_expert['city'])?$arr_expert['city']:""}}" selected="selected">
                                 {{isset($arr_expert['city_details']['city_name'])?$arr_expert['city_details']['city_name']:"" }}
                                </option>
                            @else
                              <option value="{{isset($states['id'])?$states['id']:""}}">
                                 {{isset($states['state_name'])?$states['state_name']:"" }}
                                 </option>

                              @endif
                        @endforeach
                     
                     @endif
                     </select>
                     <span class='error'>{{ $errors->first('city') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="address">Address <i class="red">*</i></label>
                <div class="col-sm-6 col-lg-4 controls">
                <textarea name="address" class="form-control" data-rule-required="true" data-rule-maxlength="425">{!! isset($arr_expert['address'])?$arr_expert['address']:''!!}</textarea>
                <span class='error'>{{ $errors->first('address') }}</span>
                </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="phone">Zip Code<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="zip_code" class="form-control" value="{{isset($arr_expert['zip'])?$arr_expert['zip']:''}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Zip Code">
                     <span class='error'>{{ $errors->first('zip') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="phone">Company Name<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="company_name" class="form-control" value="{{isset($arr_expert['company_name'])?$arr_expert['company_name']:''}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Company Name">
                     <span class='error'>{{ $errors->first('company_name') }}</span>
                  </div>
               </div>


               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="phone">Vat Number<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="vat_number" class="form-control" value="{{isset($arr_expert['vat_number'])?$arr_expert['vat_number']:''}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Vat Number">
                     <span class='error'>{{ $errors->first('vat_number') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="">Spoken Languages </label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" class="form-control" value="{{isset($arr_expert['spoken_languages'])?$arr_expert['spoken_languages']:''}}"  placeholder="-" disabled="true" >
                     
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="">Time Zone </label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" class="form-control" value="{{isset($arr_expert['timezone'])?$arr_expert['timezone']:''}}"  placeholder="-" disabled="true" >
                     
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="">Skills </label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <textarea class="form-control" placeholder="-" disabled="true" >@if(isset($arr_expert['expert_skills']) && sizeof($arr_expert['expert_skills'])>0) @foreach($arr_expert['expert_skills'] as $skills) {{isset($skills['skills']['skill_name'])?$skills['skills']['skill_name']:''}},@endforeach
              @else{{'-'}}       
               @endif
               </textarea>   
                     
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="">Categories </label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <textarea class="form-control" placeholder="-" disabled="true" >@if(isset($arr_expert['expert_categories']) && sizeof($arr_expert['expert_categories'])>0) @foreach($arr_expert['expert_categories'] as $categories) {{isset($categories['categories']['category_title'])?$categories['categories']['category_title']:''}},@endforeach
               @else{{'-'}}      
               @endif
               </textarea>   
                 </div>
               </div>


               
               <br>
               <div class="form-group">
                  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                     <input type="submit" value="Save" class="btn btn btn-primary">
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
 function loadStates(ref)   
 {
     var selected_country = jQuery(ref).val();

     if(selected_country && selected_country!="" && selected_country!=0)
     {

      jQuery('select[id="state_expert"]').find('option').remove().end().append('<option value="">Select State</option>').val('');
      jQuery('select[id="city_expert"]').find('option').remove().end().append('<option value="">Select City</option>').val('');

           jQuery.ajax({
                           url:'{{url("/")}}/locations/get_states/'+btoa(selected_country),
                           type:'GET',
                           data:'flag=true',
                           dataType:'json',
                           beforeSend:function()
                           {
                               jQuery('select[id="state_expert"]').attr('readonly','readonly');
                           },
                           success:function(response)
                           {
                               if(response.status=="success")
                               {
                                   jQuery('select[id="state_expert"]').removeAttr('readonly');

                                   if(typeof(response.states) == "object")
                                   {
                                      var option = '<option value="">Select State</option>'; 
                                      jQuery(response.states).each(function(index,state)
                                      {
                                           option+='<option value="'+state.id+'">'+state.state_name+'</option>';
                                      });

                                      jQuery('select[id="state_expert"]').html(option);
                                   }

                               }
                               return false;
                           }    
           });
      }
 }

function loadCities(ref)   
 {
     var selected_state = jQuery(ref).val();

     if(selected_state && selected_state!="" && selected_state!=0)
     {

      jQuery('select[id="city_expert"]').find('option').remove().end().append('<option value="">Select City</option>').val('');

           jQuery.ajax({
                           url:'{{url("/")}}/locations/get_cities/'+btoa(selected_state),
                           type:'GET',
                           data:'flag=true',
                           dataType:'json',
                          beforeSend:function()
                           {
                               jQuery('select[id="city_expert"]').attr('readonly','readonly');
                           },
                           success:function(response)
                           {
                               if(response.status=="success")
                               {
                                   jQuery('select[id="city_expert"]').removeAttr('readonly');

                                   if(typeof(response.cities) == "object")
                                   {
                                      var option = '<option value="">Select City</option>'; 
                                      jQuery(response.cities).each(function(index,city)
                                      {
                                           option+='<option value="'+city.id+'">'+city.city_name+'</option>';
                                      });

                                      jQuery('select[id="city_expert"]').html(option);
                                   }

                               }
                               return false;
                           }    
                     });
      }
 }

</script>
@stop
