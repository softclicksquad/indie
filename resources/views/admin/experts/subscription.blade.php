
@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-suitcase"></i>
      <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-suitcase"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-suitcase"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content" >
               @include('admin.layout._operation_status')  
              <div class="row">

               @if(isset($arr_info) && sizeof($arr_info)>0)
               <form class="form-horizontal" action="" novalidate name="project" method="post">
                  {{ csrf_field() }}
                <div class="col-md-6">

                 <div class="form-group">
                     <label class="col-sm-3 control-label">Invoice id:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['invoice_id'])?$arr_info['invoice_id']:''}}</div>
                     </div>
                  </div>
                   
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Subscription name:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['pack_name'])?ucfirst($arr_info['pack_name']):''}}</div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Subscription price:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['pack_price'])?$arr_info['pack_price']:''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Purchase date:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block"> 
                        {{isset($arr_info['created_at'])?date('d F, Y',strtotime($arr_info['created_at'])):''}}
                        </div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
{{-- @php print_r($arr_info); @endphp --}}
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Expiry date:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['expiry_at'])?date('d M Y',strtotime($arr_info['expiry_at'])):'None'}}</div>
                     </div>
                  </div>
                  {{-- {{dump($arr_info['expiry_at'])}} --}}
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Validity:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['validity'])?$arr_info['validity']:'' }}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Number of bids:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['number_of_bids'])?$arr_info['number_of_bids']:''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Topup bid price:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['topup_bid_price'])?$arr_info['topup_bid_price']:''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Number of categories:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['number_of_categories'])?$arr_info['number_of_categories']:''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Number of skills:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['number_of_skills'])?$arr_info['number_of_skills']:''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                   <div class="form-group">
                     <label class="col-sm-3 control-label">Number of favorites projects:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['number_of_favorites_projects'])?$arr_info['number_of_favorites_projects']:''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Website commission:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['website_commision'])?$arr_info['website_commision']:''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                   <div class="form-group">
                     <label class="col-sm-3 control-label">Email Allow:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">
                        @if(isset($arr_info['allow_email']) && $arr_info['allow_email']=="1") 
                        {{"Yes"}}
                        @else
                        {{"No"}}
                        @endif </div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                   <div class="form-group">
                     <label class="col-sm-3 control-label">Chat Allow:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block"> 
                        @if(isset($arr_info['allow_chat']) && $arr_info['allow_chat']=="1") 
                        {{"Yes"}}
                        @else
                        {{"No"}}
                        @endif</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>

                  
                </div>

              </form>
              @else
             <div class="col-md-12" style="text-align: center;">
             {{"No data available"}}
             </div>
                @endif
                </div>
               
             
            </div>
      </div>
   </div>
</div>
<!-- END Main Content -->
@stop