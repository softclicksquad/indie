@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
 <div>
 </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
 <ul class="breadcrumb">
  <li>
   <i class="fa fa-home"></i>
   <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
 </li>
 <span class="divider">
  <i class="fa fa-angle-right"></i>
  <i class="fa fa-gears"></i>
   <a href="{{ $module_url_path }}">{{$module_title}}</a>
</span> 
<span class="divider">
  <i class="fa fa-angle-right"></i>
  <i class="fa fa-gear"></i>
</span>
<li class="active">{{ $page_title or 'N/A' }}</li>
</ul>
</div>
<!-- END Breadcrumb -->

<!-- BEGIN Main Content -->
<div class="row">
 <div class="col-md-12">
  <div class="box">
   <div class="box-title">
    <h3>
     <i class="fa fa-gear"></i>
     {{$arr_info['contest_title'] or 'N/A'}}
     <span class="divider">
      <i class="fa fa-angle-right"></i> 
    </span>  
    {{ $page_title or 'N/A' }}
  </h3>
  <div class="box-tool">
   <a data-action="collapse" href="#"></a>
   <a data-action="close" href="#"></a>
 </div>
</div>
<div class="box-content">
<div class="row">
  <form name="validation-form" id="validation-form" method="POST" action="javascript:void(0)" class="form-horizontal"  enctype="multipart/form-data">
   {{ csrf_field() }}
  <div class="col-md-6">
    <div class="form-group">
      <label class="col-sm-3 control-label">First Name:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block">{{$arr_expert['first_name'] or 'N/A'}}</div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label">Last Name:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_expert['last_name'] or 'N/A'}}</div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label">Contact Number:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">+{{$arr_expert['phone_code'] or 'N/A'}} {{$arr_expert['phone_number'] or 'N/A'}}</div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label">Country:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_expert['country_details']['country_name'] or 'N/A'}}</div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label">State:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_expert['state_details']['state_name'] or 'N/A'}}</div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label">City:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_expert['city_details']['city_name'] or 'N/A'}}</div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label">Address:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_expert['city_details']['city_name'] or 'N/A'}}</div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label">Zip Code:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_expert['zip'] or 'N/A'}}</div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label">Company Name:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_expert['company_name'] or 'N/A'}}</div>
      </div>
    </div>
    
    <div class="form-group">
      <label class="col-sm-3 control-label">Vat Number:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_expert['vat_number'] or 'N/A'}}</div>
      </div>
    </div>
     <div class="form-group">
      <label class="col-sm-3 control-label">Spoken Languages:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_expert['spoken_languages'] or 'N/A'}}</div>
      </div>
    </div>
     <div class="form-group">
      <label class="col-sm-3 control-label">Time Zone:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_expert['timezone'] or 'N/A'}}</div>
      </div>
    </div>

     <div class="form-group">
      <label class="col-sm-3 control-label">Hourly Rate (USD):</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">{{$arr_expert['hourly_rate'] or 'N/A'}}</div>
      </div>
    </div>

     <div class="form-group">
      <label class="col-sm-3 control-label">Skills:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">@if(isset($arr_expert['expert_skills']) && sizeof($arr_expert['expert_skills'])>0) @foreach($arr_expert['expert_skills'] as $skills) {{isset($skills['skills']['skill_name'])?$skills['skills']['skill_name']:''}},@endforeach
              @else{{'-'}}       
               @endif</div>
      </div>
    </div>

     <div class="form-group">
      <label class="col-sm-3 control-label">Categories:</label>
      <div class="col-sm-9 controls">
        <div class="client-name-block"></div>
        <div class="client-name-block">@if(isset($arr_expert['expert_categories']) && sizeof($arr_expert['expert_categories'])>0) @foreach($arr_expert['expert_categories'] as $categories) {{isset($categories['categories']['category_title'])?$categories['categories']['category_title']:''}},@endforeach
               @else{{'-'}}      
               @endif</div>
      </div>
    </div>
    

  </div>

          </form>
        </div>          
      </div>
    </div>
  </div>
</div>
<!-- END Main Content -->
@stop