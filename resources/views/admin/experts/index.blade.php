@extends('admin.layout.master')                

    @section('main_content')

     <!-- BEGIN Page Title -->
    <link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/data-tables/latest/dataTables.bootstrap.min.css">
    <div class="page-title">
        <div>

        </div>
    </div>
    <!-- END Page Title -->
    
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
       <ul class="breadcrumb">
          <li>
             <i class="fa fa-home"></i>
             <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
          </li>
          <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-user-plus"></i>
          <a href="{{ $module_url_path }}">{{ isset($page_title)?str_plural($page_title):"" }}</a>
          </span> 
          <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-user-plus"></i>
          </span>
          <li class="active">{{ $page_title or ''}}</li>
       </ul>
    </div>
    <!-- END Breadcrumb -->

    <!-- BEGIN Main Content -->
    <div class="row">
      <div class="col-md-12">

          <div class="box">
            <div class="box-title">
              <h3>
                <i class="fa fa-user-plus"></i>
                {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
                <a data-action="collapse" href="#"></a>
                <a data-action="close" href="#"></a>
            </div>
        </div>
        <div class="box-content">
          
          @include('admin.layout._operation_status')
          
          <form name="frm_manage" id="frm_manage" method="POST" class="form-horizontal" action="{{$module_url_path}}/multi_action">
          {{ csrf_field() }}
          <div class="btn-toolbar pull-right clearfix">
          <div class="btn-group">
             <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                title="Multiple Active/Unblock" 
                href="javascript:void(0);" 
                onclick="javascript : return check_multi_action('frm_manage','activate');" 
                style="text-decoration:none;">
             <i class="fa fa-unlock"></i>
             </a> 
          </div>
          <div class="btn-group">
             <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                title="Multiple Deactive/Block" 
                href="javascript:void(0);" 
                onclick="javascript : return check_multi_action('frm_manage','deactivate');"  
                style="text-decoration:none;">
             <i class="fa fa-lock"></i>
             </a> 
          </div>
          <div class="btn-group"> 
             <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                title="Refresh" 
                href="{{ $module_url_path or "javascript:void(0)" }}"
                style="text-decoration:none;">
             <i class="fa fa-repeat"></i>
             </a> 
          </div>
          </div>
          <br/><br/>
          <div class="clearfix"></div>
          <div class="table-responsive" style="border:0">
            <input type="hidden" name="multi_action" value="" />
            <table class="table table-advance"  id="table1" >
              <thead>
                <tr>
                  <th style="width:18px"> <input type="checkbox" name="mult_change" id="mult_change" /></th>
                  <th>Name</th> 
                  <th>Email</th>
                  <th>Contact</th>
                  <th>Address</th>
                  <th>Subscription Purchase date</th>
                  <th>Subscription Expiry date</th>
                  <th>Status</th> 
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @if(isset($arr_expert_user) && sizeof($arr_expert_user)>0)
                  @foreach($arr_expert_user as $expert_user)
                  <tr>
                    <td> 
                      <input type="checkbox" 
                             name="checked_record[]"  
                             value="{{base64_encode($expert_user['user_id'])}}" /> 
                    </td>
                    <td> 
                      {{ isset($expert_user['first_name'])?$expert_user['first_name']:''  }}
                      {{ isset($expert_user['last_name'])?$expert_user['last_name']:''  }}
                    </td> 
                    <td> {{ isset($expert_user['user_details']['email'])?$expert_user['user_details']['email']:'' }} </td> 
                    <td>{{ isset($expert_user['phone_code'])?$expert_user['phone_code']:''  }}{{ isset($expert_user['phone_number'])?$expert_user['phone_number']:''  }}</td>
                    <td>{{ isset($expert_user['address'])?str_limit($expert_user['address'], 40):''  }}</td>
                    <td>{{isset($expert_user['subscription_details']['created_at']) && $expert_user['subscription_details']['created_at']!=""? date('d F, Y',strtotime($expert_user['subscription_details']['created_at'])):"NA"}}</td>  
                    <td>{{isset($expert_user['subscription_details']['expiry_at']) && $expert_user['subscription_details']['expiry_at']!=""? date('d F, Y',strtotime($expert_user['subscription_details']['expiry_at'])):"NA"}}</td>  
                    <td>
                        @if(isset($expert_user['user_details']['is_active']) && $expert_user['user_details']['is_active']==1)
                        <a href="{{ $module_url_path.'/deactivate/'}}{{isset($expert_user['user_id'])?base64_encode($expert_user['user_id']):''}}" class="btn btn-success">Active</a>
                        @else
                        <a href="{{ $module_url_path.'/activate/'}}{{isset($expert_user['user_id'])?base64_encode($expert_user['user_id']):''}}" class="btn btn-danger">Inactive</a>
                        @endif
                        <a href="{{ $module_url_path.'/projects/'}}{{isset($expert_user['user_id'])?base64_encode($expert_user['user_id']):''}}" class="btn btn-primary">Projects Info</a>
                        <a href="{{ $module_url_path.'/subscription/'}}{{isset($expert_user['subscription_details']['id'])? base64_encode($expert_user['subscription_details']['id']):'' }}" class="btn btn-danger">Subscription</a>
                     </td>
                     <td>
                        <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" href="{{ $module_url_path.'/edit/'}}{{isset($expert_user['id'])?base64_encode($expert_user['id']):''}}"  title="Edit">
                        <i class="fa fa-edit" ></i>
                        </a>  
                     </td>
                  </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
          </div>
        <div>   
     </div>
    </form>
  </div>
</div>
</div>
</div>
<!-- END Main Content -->
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
   function confirm_delete(url) {
      alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
          if (e) {
              if(url != "" && url != undefined){
                showProcessingOverlay();
                window.location.href=url;
              }
              return true;
          } else {
              return false;
          }
      });
   }
   function check_multi_action(frm_id,action){
     var frm_ref = jQuery("#"+frm_id);
     if(jQuery(frm_ref).length && action!=undefined && action!=""){
       var checked_record = jQuery('input[name="checked_record[]"]:checked');
       if (jQuery(checked_record).size() < 1){
          alertify.alert('please select at least one record.');
          return false;
       }
       if(action == 'delete') {
          if (!confirm_delete()){
              return false;
          }
       }
       /* Get hidden input reference */
       var input_multi_action = jQuery('input[name="multi_action"]');
       if(jQuery(input_multi_action).length){
         /* Set Action in hidden input*/
         jQuery('input[name="multi_action"]').val(action);
         /*Submit the referenced form */
         jQuery(frm_ref)[0].submit();
       } else {
         console.warn("Required Hidden Input[name]: multi_action Missing in Form ")
       }
     } else {
         console.warn("Required Form[id]: "+frm_id+" Missing in Current category ")
     }
   }
</script>
@stop                    


