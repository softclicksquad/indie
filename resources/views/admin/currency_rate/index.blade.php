@extends('admin.layout.master')
@section('main_content')
<link rel="stylesheet" type="text/css" href="{{ url('/assets/data-tables/latest/') }}/dataTables.bootstrap.min.css">

<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-bars"></i>
      <a href="{{ $module_url_path }}">{{ $page_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-bars"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3>
                    <i class="fa fa-bars"></i>
                    {{ isset($page_title)?$page_title:"" }}
                </h3>
                <div class="box-tool">
                    <a data-action="collapse" href="#"></a>
                    <a data-action="close" href="#"></a>
                </div>
            </div>
                
            <div class="box-content">
                <h3 style="text-align: center;" >Last Updated At : {{ isset($last_updated_at) ? $last_updated_at : '' }} </h3>
            </div>                
            <br/>

            <?php
                $arr_style = ['box-red','box-orange','box-yellow','box-pink','box-magenta','box-gray','box-black'];
            ?>

            @if(isset($arr_currency_conversion) && sizeof($arr_currency_conversion)>0)
                @foreach($arr_currency_conversion as $currency => $currency_conversion)
                <?php
                    $curr_key = array_rand($arr_style); 
                ?>
                    <div class="col-md-4">
                        <div class="box {{ isset($arr_style[$curr_key]) ? $arr_style[$curr_key] : '' }}">
                            <div class="box-title">
                                <h3><i class="fa fa-money"></i> <strong>1 {{ isset($currency)?$currency:'' }}</strong></h3>
                            
                            </div>
                            <div class="box-content">
                                @if(isset($currency_conversion) && is_array($currency_conversion))
                                    @foreach($currency_conversion as $key => $value)
                                        <?php

                                            $to_currency_code = isset($value['to_currency_code']) ? $value['to_currency_code'] : '';
                                            $conversion_rate  = isset($value['conversion_rate']) ? $value['conversion_rate'] : '';
                                        ?>
                                        <p> <strong> {{$key+1}} : {{ $to_currency_code }} ({{ $conversion_rate }})</strong></p>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif

        </div>
    </div>
</div>
@stop