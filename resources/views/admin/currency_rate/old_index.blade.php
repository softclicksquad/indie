@extends('admin.layout.master')
@section('main_content')
<link rel="stylesheet" type="text/css" href="{{ url('/assets/data-tables/latest/') }}/dataTables.bootstrap.min.css">
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-bars"></i>
      <a href="{{ $module_url_path }}">{{ $page_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-bars"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3>
                    <i class="fa fa-bars"></i>
                    {{ isset($page_title)?$page_title:"" }}
                </h3>
                <div class="box-tool">
                    <a data-action="collapse" href="#"></a>
                    <a data-action="close" href="#"></a>
                </div>
            </div>
            <div class="box-content">
 
                <h3 style="text-align: center;" >Last Updated At : {{ isset($last_updated_at) ? $last_updated_at : '' }} </h3>
                <form name="frm_manage" id="frm_manage" method="POST" class="form-horizontal">
                    
                    <div class="clearfix"></div>
                    <div class="table-responsive" style="border:0">
                        <input type="hidden" name="multi_action" value="" />
                        <table class="table table-advance"  id="table1" >
                            <thead>
                                <tr>
                                    <th style="padding-left: 50px;">Base Currency Rate</th>
                                    <th>Currency Rate Details</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($arr_currency_conversion) && sizeof($arr_currency_conversion)>0)
                                @foreach($arr_currency_conversion as $currency => $currency_conversion)
                                    <tr>
                                        <td style="padding-left: 50px;"> <strong>1 {{ isset($currency)?$currency:'' }}</strong> </td>
                                        
                                        <td>
                                            @if(isset($currency_conversion) && is_array($currency_conversion))
                                                @foreach($currency_conversion as $key => $value)
                                                    <?php
                                                        $first_side_html = $second_side_html = '';

                                                        $to_currency_code = isset($value['to_currency_code']) ? $value['to_currency_code'] : '';
                                                        $conversion_rate  = isset($value['conversion_rate']) ? $value['conversion_rate'] : '';
                                                    ?>
                                                    <p> <strong> {{$key+1}} : {{ $to_currency_code }} ({{ $conversion_rate }})</strong></p>
                                                @endforeach
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Main Content -->
{{-- <script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/dataTables.bootstrap.min.js"></script> --}}

@stop