@extends('admin.layout.master')
@section('main_content')
<link rel="stylesheet" type="text/css" href="{{ url('/assets/data-tables/latest/') }}/dataTables.bootstrap.min.css">
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-file-text"></i>
      <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-file-text"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-file-text"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">

            @include('admin.layout._operation_status')  

            <form name="frm_manage" id="frm_manage" method="POST" class="form-horizontal" action="{{$module_url_path}}/multi_action">
               {{ csrf_field() }}
               <div class="col-md-10">
                  <div id="ajax_op_status">
                  </div>
                  <div class="alert alert-danger" id="no_select" style="display:none;"></div>
                  <div class="alert alert-warning" id="warning_msg" style="display:none;"></div>
               </div>
               <div class="btn-toolbar pull-right clearfix">
                  <div class="btn-group">
                     <a href="{{ $module_url_path.'/create'}}" class="btn btn-primary btn-add-new-records"  title="Add FAQ">Add FAQ</a>                      
                  </div>
                  <div class="btn-group">
                     <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                        title="Multiple Active/Unblock" 
                        href="javascript:void(0);" 
                        onclick="javascript : return check_multi_action('frm_manage','activate');" 
                        style="text-decoration:none;">
                     <i class="fa fa-unlock"></i>
                     </a> 
                  </div>
                  <div class="btn-group">
                     <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                        title="Multiple Deactive/Block" 
                        href="javascript:void(0);" 
                        onclick="javascript : return check_multi_action('frm_manage','deactivate');"  
                        style="text-decoration:none;">
                     <i class="fa fa-lock"></i>
                     </a> 
                  </div>
                  <div class="btn-group">    
                     <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                        title="Multiple Delete" 
                        href="javascript:void(0);" 
                        onclick="javascript : return check_multi_action('frm_manage','delete');"  
                        style="text-decoration:none;">
                     <i class="fa fa-trash-o"></i>
                     </a>
                  </div>
                  <div class="btn-group"> 
                     <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                        title="Refresh" 
                        href="{{ $module_url_path }}"
                        style="text-decoration:none;">
                     <i class="fa fa-repeat"></i>
                     </a> 
                  </div>
               </div>
               <br/>
               <br/>
               <div class="clearfix"></div>
               <div class="table-responsive" style="border:0">
                  <input type="hidden" name="multi_action" value="" />
                  <table class="table table-advance"  id="table1" >
                     <thead>
                        <tr>
                           <th style="width:18px"> <input type="checkbox" name="mult_change" id="mult_change" value="delete" /></th>
                           <th>Question Name</th>
                           <th>FAQ Type</th>
                           <th>Status</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($arr_question as $page)
                        <tr>
                           <td> 
                              <input type="checkbox" 
                                 name="checked_record[]"  
                                 value="{{ base64_encode($page['id']) }}" /> 
                           </td>
                           <td> {{ isset($page['question_name'])?$page['question_name']:'' }} </td>
                           <td> @if(isset($page['faq_type']) && $page['faq_type']=='1') 
                                For Client
                                @elseif(isset($page['faq_type']) && $page['faq_type']=='2')
                                For Expert
                                @else
                                NA
                                @endif
                           </td>
                           <td>
                              @if($page['is_active']==1)
                              <a href="{{ $module_url_path.'/deactivate/'.base64_encode($page['id']) }}" class="btn btn-success">Active</a>
                              @else
                              <a href="{{ $module_url_path.'/activate/'.base64_encode($page['id']) }}" class="btn btn-danger">Inactive</a>
                              @endif
                           </td>
                           <td> 
                              <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" href="{{ $module_url_path.'/edit/'.base64_encode($page['id']) }}"  title="Edit">
                              <i class="fa fa-edit" ></i>
                              </a>  
                              &nbsp;  
                              <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" href="javascript:(0)" 
                                 onclick="javascript:return confirm_delete('{{ $module_url_path.'/delete/'.base64_encode($page['id'])}}')"  title="Delete">
                              <i class="fa fa-trash" ></i>
                              </a>   
                           </td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<!-- END Main Content -->
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
   function confirm_delete(url) {
      alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
          if (e) {
              if(url != "" && url != undefined){
                showProcessingOverlay();
                window.location.href=url;
              }
              return true;
          } else {
              return false;
          }
      });
   }
   function check_multi_action(frm_id,action)
   {
     var frm_ref = jQuery("#"+frm_id);
     if(jQuery(frm_ref).length && action!=undefined && action!="")
     {
      /* Get hidden input reference */
       var checked_record = jQuery('input[name="checked_record[]"]:checked');
       if (jQuery(checked_record).size() < 1)
       {
          alertify.alert('Please select at least one record.');
          return false;
       }
       var input_multi_action = jQuery('input[name="multi_action"]');
       if(jQuery(input_multi_action).length){
         /* Set Action in hidden input*/
         jQuery('input[name="multi_action"]').val(action);
         /*Submit the referenced form */
                alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
                if (e) {
                    showProcessingOverlay();
                    jQuery(frm_ref)[0].submit();
                    return true;
                } else {
                    return false;
                }
            });
       } else {
         console.warn("Required Hidden Input[name]: multi_action Missing in Form ")
       }
     } else {
         console.warn("Required Form[id]: "+frm_id+" Missing in Current category ")
     }
   }
</script>
@stop