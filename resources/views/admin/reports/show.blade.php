@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-desktop"></i>
      <a href="{{ $module_url_path }}">Transactions</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-list"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-list"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
               @include('admin.layout._operation_status')
           <div class="alert alert-success" id="ajax_success" style="display:none;">
            <button data-dismiss="alert" class="close">×</button>
            <strong>Success!</strong>
            <div id="ajax_sub_success"></div>
         </div>
         <div class="alert alert-danger" id="ajax_error" style="display:none;">
            <button data-dismiss="alert" class="close">×</button>
            <strong>Error!</strong> 
            <div id="ajax_sub_error"></div>
         </div>
           
              <div class="row">

               @if(isset($arr_user) && sizeof($arr_user)>0)
              <form name="validation-form" id="validation-form" method="POST" action="" class="form-horizontal">
               {{ csrf_field() }}
                <div class="col-md-6">
                  <div class="form-group">
                     <label class="col-sm-3 control-label">User Name:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_user['role_info']['first_name'])?$arr_user['role_info']['first_name']:'-'}} 
                          {{isset($arr_user['role_info']['last_name'])?$arr_user['role_info']['last_name']:'-'}}
                        </div>
                     </div>
                  </div>
                  
                  
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Email:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block"><a href="mailto:{{isset($arr_user['email'])?$arr_user['email']:''}}">{{isset($arr_user['email'])?$arr_user['email']:'-'}}</a></div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>

                   <div class="form-group">
                     <label class="col-sm-3 control-label">Type:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_user['roles'][0]['name'])?$arr_user['roles'][0]['name']:'-'}} 
                        </div>
                     </div>
                  </div>

                   <div class="form-group">
                     <label class="col-sm-3 control-label">Phone Number:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_user['role_info']['phone_code'])?$arr_user['role_info']['phone_code']:'-'}}{{isset($arr_user['role_info']['phone_number'])?$arr_user['role_info']['phone_number']:'-'}} 
                        </div>
                     </div>
                  </div>

                   <div class="form-group">
                     <label class="col-sm-3 control-label">Country:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_user['role_info']['country_details']['country_name'])?$arr_user['role_info']['country_details']['country_name']:'-'}} 
                        </div>
                     </div>
                  </div>

                  <div class="form-group">
                     <label class="col-sm-3 control-label">State:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_user['role_info']['state_details']['state_name'])?$arr_user['role_info']['state_details']['state_name']:'-'}} 
                        </div>
                     </div>
                  </div>

                  <div class="form-group">
                     <label class="col-sm-3 control-label">City:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_user['role_info']['city_details']['city_name'])?$arr_user['role_info']['city_details']['city_name']:'-'}} 
                        </div>
                     </div>
                  </div>

                  <div class="form-group">
                     <label class="col-sm-3 control-label">Address:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_user['role_info']['address'])?$arr_user['role_info']['address']:'-'}} 
                        </div>
                     </div>
                  </div>

                  <div class="form-group">
                     <label class="col-sm-3 control-label">Company name:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_user['role_info']['company_name'])?$arr_user['role_info']['company_name']:'-'}} 
                        </div>
                     </div>
                  </div>

                  <div class="form-group">
                     <label class="col-sm-3 control-label">Spoken language:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_user['role_info']['spoken_languages'])?$arr_user['role_info']['spoken_languages']:'-'}} 
                        </div>
                     </div>
                  </div>

                   <div class="form-group">
                     <label class="col-sm-3 control-label">Time Zone:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_user['role_info']['timezone'])?$arr_user['role_info']['timezone']:'-'}} 
                        </div>
                     </div>
                  </div>

                  
                </div>
                </form>
             @endif
            </div>
            </div>
       </div>
   </div>
</div>

@stop