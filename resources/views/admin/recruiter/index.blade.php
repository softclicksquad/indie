@extends('admin.layout.master')                

    @section('main_content')

    <!-- BEGIN Page Title -->
    <link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/data-tables/latest/dataTables.bootstrap.min.css">
    
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
       <ul class="breadcrumb">
          <li>
             <i class="fa fa-home"></i>
             <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
          </li>
          <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-users"></i>
          <a href="{{ $module_url_path }}">{{ isset($page_title)?str_plural($page_title):"" }}</a>
          </span> 
          <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-users"></i>
          </span>
          <li class="active">{{ isset($page_title)?str_plural($page_title):"" }}</li>
       </ul>
    </div>
    <!-- END Breadcrumb -->

    <!-- BEGIN Main Content -->
    <div class="row">
      <div class="col-md-12">

          <div class="box">
            <div class="box-title">
              <h3>
                <i class="fa fa-users"></i>
                {{ isset($page_title)?str_plural($page_title):"" }}
            </h3>
            <div class="box-tool">
                <a data-action="collapse" href="#"></a>
                <a data-action="close" href="#"></a>
            </div>
        </div>
        <div class="box-content">
          
          @include('admin.layout._operation_status')
          
          <form name="frm_manage" id="frm_manage" method="POST" class="form-horizontal" action="{{$module_url_path}}/multi_action">
               {{ csrf_field() }}

            
          <div class="btn-toolbar pull-right clearfix">

          
           <div class="btn-group">
             <a href="{{ $module_url_path.'/create'}}" class="btn btn-primary btn-add-new-records"  title="Add Project Manager">Add Recruiter</a>                      
          </div>
          

          {{-- <div class="btn-group">
             <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                title="Add Category" 
                href="{{ $module_url_path.'/create'}}" 
                style="text-decoration:none;">
             <i class="fa fa-plus"></i>
             </a> 
          </div> --}}

          <div class="btn-group">
             <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                title="Multiple Active/Unblock" 
                href="javascript:void(0);" 
                onclick="javascript : return check_multi_action('frm_manage','activate');" 
                style="text-decoration:none;">
             <i class="fa fa-unlock"></i>
             </a> 
          </div>
          <div class="btn-group">
             <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                title="Multiple Deactive/Block" 
                href="javascript:void(0);" 
                onclick="javascript : return check_multi_action('frm_manage','deactivate');"  
                style="text-decoration:none;">
             <i class="fa fa-lock"></i>
             </a> 
          </div>
          <div class="btn-group"> 
             <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                title="Refresh" 
                href="{{ $module_url_path }}"
                style="text-decoration:none;">
             <i class="fa fa-repeat"></i>
             </a> 
          </div>
          
          </div>
          <br/><br/>
          <div class="clearfix"></div>
          <div class="table-responsive" style="border:0">

            <input type="hidden" name="multi_action" value="" />

            <table class="table table-advance"  id="table1" >
              <thead>
                <tr>
                  <th style="width:18px"> <input type="checkbox" name="mult_change" id="mult_change" /></th>
                  <th>Name</th> 
                  <th>Email</th>
                  <th>Contact</th>
                  <th>Address</th>
                  <th>Status</th> 
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                
                @if(isset($arr_project_manager) && sizeof($arr_project_manager)>0)
                  @foreach($arr_project_manager as $project_manager)

                  <tr>
                    <td> 
                      <input type="checkbox" 
                             name="checked_record[]"  
                             value="{{ base64_encode($project_manager['user_id']) }}" /> 
                    </td>
                    
                    <td> {{ isset($project_manager['first_name'])?$project_manager['first_name']:''  }}
                        {{ isset($project_manager['last_name'])?$project_manager['last_name']:''  }}
                     </td> 
                    <td> {{ isset($project_manager['user_details']['email'])?$project_manager['user_details']['email']:'' }} </td> 
                    <td>{{ isset($project_manager['phone_code'])?$project_manager['phone_code']:''  }}{{ isset($project_manager['phone'])?$project_manager['phone']:''  }}</td>
                    <td>{{ isset($project_manager['address'])?str_limit($project_manager['address'], 40):''  }}</td>
                    <td>
                        @if(isset($project_manager['user_details']['is_active']) && $project_manager['user_details']['is_active']==1)
                        <a href="{{ $module_url_path.'/deactivate/'.base64_encode($project_manager['user_id']) }}" class="btn btn-success">Active</a>
                        @else
                        <a href="{{ $module_url_path.'/activate/'.base64_encode($project_manager['user_id']) }}" class="btn btn-danger">Inactive</a>
                        @endif
                        <a href="{{ $module_url_path.'/projects/'.base64_encode($project_manager['user_id']) }}" class="btn btn-success">Projects</a>
                     </td>
                     <td>
                        <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" href="{{ $module_url_path.'/edit/'.base64_encode($project_manager['id']) }}"  title="Edit">
                        <i class="fa fa-edit" ></i>
                        </a>  
                     </td>
                  </tr>

                  @endforeach
                @endif
                 
              </tbody>
            </table>
          </div>
        <div>   </div>
         
          </form>
      </div>
  </div>
</div>
</div>
<!-- END Main Content -->
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
   function confirm_delete(url) {
      alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
          if (e) {
              if(url != "" && url != undefined){
                showProcessingOverlay();
                window.location.href=url;
              }
              return true;
          } else {
              return false;
          }
      });
   }
   function check_multi_action(frm_id,action){
     var frm_ref = jQuery("#"+frm_id);
     if(jQuery(frm_ref).length && action!=undefined && action!=""){
       var checked_record = jQuery('input[name="checked_record[]"]:checked');
       if (jQuery(checked_record).size() < 1){
          alertify.alert('please select at least one record.');
          return false;
       }
       if(action == 'delete'){
          if (!confirm_delete()){
              return false;
          }
       }
       /* Get hidden input reference */
       var input_multi_action = jQuery('input[name="multi_action"]');
       if(jQuery(input_multi_action).length){
         /* Set Action in hidden input*/
         jQuery('input[name="multi_action"]').val(action);
         /*Submit the referenced form */
         jQuery(frm_ref)[0].submit();
       } else {
         console.warn("Required Hidden Input[name]: multi_action Missing in Form ")
       }
     } else {
         console.warn("Required Form[id]: "+frm_id+" Missing in Current Page ")
     }
   }
</script>
@stop                    


