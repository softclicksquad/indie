@extends('admin.layout.master')                

    @section('main_content')

    <!-- BEGIN Page Title -->
    <link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/data-tables/latest/dataTables.bootstrap.min.css">
    <div class="page-title">
        <div>

        </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
       <ul class="breadcrumb">
          <li>
             <i class="fa fa-home"></i>
             <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
          </li>
          <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-desktop"></i>
          <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
          </span> 
          <span class="divider">
          <i class="fa fa-angle-right"></i>
          <i class="fa fa-list"></i>
          </span>
          <li class="active">{{ $page_title or ''}}</li>
       </ul>
    </div>
    <!-- END Breadcrumb -->

    <!-- BEGIN Main Content -->
    <div class="row">
      <div class="col-md-12">

          <div class="box">
            <div class="box-title">
              <h3>
                <i class="fa fa-list"></i>
                {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
                <a data-action="collapse" href="#"></a>
                <a data-action="close" href="#"></a>
            </div>
        </div>
        <div class="box-content">
          
          @include('admin.layout._operation_status')
          
          <form name="frm_manage" id="frm_manage" method="POST" class="form-horizontal" action="{{$module_url_path}}/multi_action">
               {{ csrf_field() }}

            
          <div class="btn-toolbar pull-right clearfix">
            <div class="btn-group">    
             <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                title="Multiple Delete" 
                href="javascript:void(0);" 
                onclick="javascript : return check_multi_action('frm_manage','delete');"  
                style="text-decoration:none;">
             <i class="fa fa-trash-o"></i>
             </a>
          </div>
          <div class="btn-group"> 
             <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                title="Refresh" 
                href="{{ $module_url_path }}/new"
                style="text-decoration:none;">
             <i class="fa fa-repeat"></i>
             </a> 
          </div>
          
          </div>
          <br/><br/>
          <div class="clearfix"></div>
          <div class="table-responsive" style="border:0">

            <input type="hidden" name="multi_action" value="" />

            <table class="table table-advance"  id="table1" >
              <thead>
                <tr>
                  <th style="width:18px"> <input type="checkbox" name="mult_change" id="mult_change" /></th>
                  <th>Project Name</th> 
                  <th>Category</th>
                  <th>Client Name</th>
                  <th>Client Email</th>
                  <th>Handle By</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                
                @if(isset($arr_projects) && sizeof($arr_projects)>0)
                  @foreach($arr_projects as $projects)

                  <tr>
                    <td> 
                      <input type="checkbox" 
                             name="checked_record[]"  
                             value="{{ base64_encode($projects['id']) }}" /> 
                    </td>
                    
                    <td> {{ isset($projects['project_name'])?$projects['project_name']:''  }}
                     </td> 
                    <td> {{ isset($projects['category_details']['category_title'])?$projects['category_details']['category_title']:'' }} </td> 
                    <td>{{ isset($projects['client_details']['user_name'])?$projects['client_details']['user_name']:''  }}</td>
                    <td>{{ isset($projects['client_details']['email'])?$projects['client_details']['email']:''  }}</td>

                    <td>@if($projects['project_handle_by']=='1'){{'Myself'}}@endif
                        @if($projects['project_handle_by']=='2'){{'Recruiter'}}@endif
                    </td>
                    <td>@if($projects['project_status']=='1'){{'Posted'}}@endif
                    @if($projects['project_status']=='2'){{'Open'}}@endif
                    @if($projects['project_status']=='3'){{'Closed'}}@endif
                    @if($projects['project_status']=='4'){{'Ongoing'}}@endif
                    @if($projects['project_status']=='5'){{'Canceled'}}@endif</td>
                    <td>
                    <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" href="{{ $module_url_path.'/show/'.base64_encode($projects['id']) }}"  title="View">
                        <i class="fa fa-eye" ></i>
                        </a>
                  </td>
                  </tr>

                  @endforeach
                @endif
                 
              </tbody>
            </table>
          </div>
      
         
          </form>
      </div>
  </div>
</div>
</div>
<!-- END Main Content -->
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
   function confirm_delete(url) {
      alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
          if (e) {
              if(url != "" && url != undefined){
                showProcessingOverlay();
                window.location.href=url;
              }
              return true;
          } else {
              return false;
          }
      });
   }
   function check_multi_action(frm_id,action) {
     var frm_ref = jQuery("#"+frm_id);
     if(jQuery(frm_ref).length && action!=undefined && action!=""){
       if(action == 'delete') {
            if (!confirm_delete()){
                return false;
            }
       }
       /* Get hidden input reference */
       var input_multi_action = jQuery('input[name="multi_action"]');
       if(jQuery(input_multi_action).length) {
         /* Set Action in hidden input*/
         jQuery('input[name="multi_action"]').val(action);
         /*Submit the referenced form */
         jQuery(frm_ref)[0].submit();
       } else {
         console.warn("Required Hidden Input[name]: multi_action Missing in Form ")
       }
     } else {
         console.warn("Required Form[id]: "+frm_id+" Missing in Current category ")
     }
   }
</script>
@stop                    


