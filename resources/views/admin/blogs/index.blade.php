@extends('admin.layout.master')
@section('main_content')

<style type="text/css">
.divpagination{margin: 0;text-align: right!important;white-space: nowrap;color: #fff;}
</style>

<!-- BEGIN Page Title -->

<div class="page-title">
	<div>

	</div>
</div>
<!-- END Page Title -->

<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
		</li>
		<span class="divider">
			<i class="fa fa-angle-right"></i>
			<i class="fa fa-road"></i>
			<a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
		</span> 
		<span class="divider">
			<i class="fa fa-angle-right"></i>
			<i class="fa fa-road"></i>
		</span>
		<li class="active">{{ $page_title or ''}}</li>
	</ul>
</div>
<!-- END Breadcrumb -->

<!-- BEGIN Main Content -->
<div class="row">
	<div class="col-md-12">

		<div class="box">
			<div class="box-title">
				<h3>
					<i class="fa fa-road"></i>
					{{ isset($page_title)?$page_title:"" }}
				</h3>
				<div class="box-tool">
					<a data-action="collapse" href="#"></a>
					<a data-action="close" href="#"></a>
				</div>
			</div>
			<div class="box-content">

				@include('admin.layout._operation_status')

				<form name="from_search" id="from_search" method="GET" class="form-horizontal" action="{{$module_url_path}}">

					<div id="section_search">
						<div class="form-group">
							<div class="col-md-5">
								<div class="col-md-4">
									<input type="text" class="form-control" name="search_text" placeholder="Search here" value="{{\Request::input('search_text')}}">
									<span class='error'>{{ $errors->first('search_text') }}</span>
								</div>
								<input type="submit" value="Search" class="btn btn btn-primary">
							</div>
						</div>
					</div>
				</form>

				<form name="frm_manage" id="frm_manage" method="POST" class="form-horizontal" action="{{$module_url_path}}/multi_action">
					{{ csrf_field() }}

					<div class="btn-toolbar pull-right clearfix">

						<div class="btn-group">
							<a href="{{ $module_url_path.'/create'}}" class="btn btn-primary btn-add-new-records"  title="Add CMS">Add Blog</a>                      
						</div>

						<div class="btn-group">
							<a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
								title="Multiple Active/Unblock" 
								href="javascript:void(0);" 
								onclick="javascript : return check_multi_action('frm_manage','activate');" 
								style="text-decoration:none;">
								<i class="fa fa-unlock"></i>
							</a> 
						</div>
						<div class="btn-group">
							<a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
								title="Multiple Deactive/Block" 
								href="javascript:void(0);" 
								onclick="javascript : return check_multi_action('frm_manage','deactivate');"  
								style="text-decoration:none;">
								<i class="fa fa-lock"></i>
							</a> 
						</div>
						<div class="btn-group">
							<a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
								title="Refresh" 
								href="{{ $module_url_path }}"
								style="text-decoration:none;">
								<i class="fa fa-repeat"></i>
							</a> 
						</div>
					</div>
					<br/><br/><br/>
					<div class="clearfix"></div>
					<div class="table-responsive" style="border:0">

						<input type="hidden" name="multi_action" value="" />

						<table class="table table-advance" id="table1" >
							<thead>
								<tr>
									<th style="width:18px"> <input type="checkbox" name="mult_change" id="mult_change" /></th>
									<th>Name</th> 
									<th>Status</th>
									<th>Added On</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@if(isset($arr_blogs['data']) && !empty($arr_blogs['data']))
									@foreach($arr_blogs['data'] as $data)
									{{-- {{ dd($data) }} --}}
									<tr>
										<td>
											<input type="checkbox" name="checked_record[]" value="{{base64_encode($data['id'])}}" />
										</td>
										<td>{{ $data['name'] or '' }}</td>
										<td>
											@if(isset($data['is_active']) && $data['is_active']==1)
					                        <a href="{{ $module_url_path.'/deactivate/'}}{{isset($data['id'])?base64_encode($data['id']):''}}" class="btn btn-success">Active</a>
					                        @else
					                        <a href="{{ $module_url_path.'/activate/'}}{{isset($data['id'])?base64_encode($data['id']):''}}" class="btn btn-danger">Inactive</a>
					                        @endif
										</td>
										<td>{{isset($data['created_at']) && $data['created_at']!=""? date('d F, Y',strtotime($data['created_at'])):"NA"}}
										</td>
										<td>
											<a class='btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip' href="{{ $module_url_path.'/view/'.base64_encode($data['id']) }}" title='View' ><i class='fa fa-eye' ></i></a>
											<a class='btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip' href="{{ $module_url_path.'/edit/'.base64_encode($data['id']) }}" title='Edit' ><i class='fa fa-pencil-square-o' ></i></a>
											@if($data['comment_count'] > 0)
											<a class='btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip' href="{{ $module_url_path.'/comments/'.base64_encode($data['id']) }}" title='Comments' ><i class='fa fa-comment' ></i></a>
											@endif
											<a class='btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip' href="javascript:void(0)" title='Delete' onclick="javascript:return confirm_delete('{{ $module_url_path.'/delete/'.base64_encode($data['id'])}}')" ><i class='fa fa-trash' ></i></a>
										</td>
									</tr>
									@endforeach
								@else
								<h3>No data found</h3>
								@endif
							</tbody>
						</table>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
{{-- <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script> --}}
<!-- END Main Content -->
<script type="text/javascript">

$(document).ready(function() {

});

function confirm_delete(url) {
	alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
		if (e) {
			if(url != "" && url != undefined){
				showProcessingOverlay();
				window.location.href=url;
			}
			return true;
		} else {
			return false;
		}
	});
}
function check_multi_action(frm_id,action){
	var frm_ref = jQuery("#"+frm_id);
	if(jQuery(frm_ref).length && action!=undefined && action!="")
	{
		var checked_record = jQuery('input[name="checked_record[]"]:checked');
		if (jQuery(checked_record).size() < 1)
		{
			alertify.alert('please select at least one record.');
			return false;
		}
		if(action == 'delete')
		{
			if (!confirm_delete()){
				return false;
			}
		}
		var input_multi_action = jQuery('input[name="multi_action"]');
		if(jQuery(input_multi_action).length)
		{
			jQuery('input[name="multi_action"]').val(action);
			jQuery(frm_ref)[0].submit();
		}
		else
		{
			console.warn("Required Hidden Input[name]: multi_action Missing in Form ")
		}
	}
	else
	{
		console.warn("Required Form[id]: "+frm_id+" Missing in Current category ")
	}
}

function confirm_delete(url) {
  alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
      if (e) {
          showProcessingOverlay();
          window.location.href=url;
          return true;
      } else {
          return false;
      }
  });
}

</script>
@stop