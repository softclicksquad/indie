@extends('admin.layout.master')    
@section('main_content')
<!-- Page header -->
<!-- /page header -->


<style type="text/css">
/*.cropit-preview{
    background-color:#f8f8f8;
    background-size:cover;
    border:1px solid #ccc;
    border-radius:3px;
    margin-top:7px;
    width:690px;
    height:396px
}
.text-data {
    position: relative;
    top: 9px;
}*/
</style>
<div class="page-title">
	<div>

	</div>
</div>
<!-- END Page Title -->

<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
		</li>
		<span class="divider">
			<i class="fa fa-angle-right"></i>
			<i class="fa fa-road"></i>
			<a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
		</span> 
		<span class="divider">
			<i class="fa fa-angle-right"></i>
			<i class="fa fa-road"></i>
		</span>
		<li class="active">{{ $page_title or ''}}</li>
	</ul>
</div>
<!-- END Breadcrumb -->

<!-- BEGIN Main Content -->
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-title">
				<h3>
					<i class="fa fa-road"></i>
					{{ isset($page_title)?$page_title:"" }}
				</h3>
				<div class="box-tool">
					<a data-action="collapse" href="#"></a>
					<a data-action="close" href="#"></a>
				</div>
			</div>
			<div class="box-content">
				@include('admin.layout._operation_status')

				<div class="panel-body">
					<form class="form-horizontal" id="frm_add_front_page" name="frm_add_front_page" action="{{$module_url_path}}/store" method="post" enctype="multipart/form-data">
						{{csrf_field()}}
						<fieldset class="content-group">	
							<div class="row">
								<div class="col-lg-8">
									<div class="form-group">
										<label class="control-label col-sm-4 col-md-4 col-lg-3" for="name">Blog Image</label>
										<div class="col-sm-8 col-md-8 col-lg-9">
											<div class="image-editor">
												<input type="file" name="image-editor" class="cropit-image-input" style="display: none;" data-rule-required='true' id="image">
												@php
												if(isset($arr_data['image'])) {
													$image_url = $blog_image_public_path.$arr_data['image'];
												}else{
													$image_url = get_default_image(396,690,25,'No Advertisement' );
												}
												@endphp
												<div class="cropit-preview">
													<img class="cropit-preview-image" src="{{ $image_url }}" />
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-8">
									<div class="form-group">
										<label class="control-label col-sm-4 col-md-4 col-lg-3" for="name">Blog Name</label>
										<div class="col-sm-8 col-md-8 col-lg-9">
											<div class="text-data">{{ $arr_data['name'] or 'NA' }}</div>							
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-lg-8">
									<div class="form-group">
										<label class="control-label col-sm-4 col-md-4 col-lg-3" for="plan_description">Blog Category</label>
										<div class="col-sm-8 col-md-8 col-lg-9">
											<div class="text-data">{{ $arr_data['category']['name'] or 'NA' }}</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-8">
									<div class="form-group">
										<label class="control-label col-sm-4 col-md-4 col-lg-3" for="plan_description">Blog Description</label>
										<div class="col-sm-8 col-md-8 col-lg-9">
											<div class="text-data">{!! $arr_data['description'] or 'NA' !!}</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-8">
									<div class="form-group">
										<label class="control-label col-sm-4 col-md-4 col-lg-3" for="plan_description">Tags</label>
										<div class="col-sm-8 col-md-8 col-lg-9">
											<div class="text-data">{{ $arr_data['tags'] or 'NA' }}</div>
										</div>
									</div>
								</div>
							</div>

							<div class="form-group text-right">
								<div class="col-lg-8">
									@if($arr_data['comments'])
									<a href="{{ $module_url_path.'/comments/'.$enc_id }}" class="btn green-btn">Comments</a>
									@endif
									<a href="{{ $module_url_path or '' }}" class="btn gray-btn">Back</a>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection