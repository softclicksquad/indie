@extends('admin.layout.master')    
@section('main_content')

<style type="text/css">
.cropit-preview-image-container:active{
    cursor:move
}
.cropit-image-zoom-input{
    width:66%!important
}
.cropit-preview{
    background-color:#f8f8f8;
    background-size:cover;
    border:1px solid #ccc;
    border-radius:3px;
    margin-top:7px;
    width:690px;
    height:396px
}
.image-size-label{
    margin-top:10px
}
.cropit-preview-image-container{
    cursor:pointer
}
</style>

<!-- BEGIN Page Title -->
<div class="page-title">
	<div>

	</div>
</div>
<!-- END Page Title -->

<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
		</li>
		<span class="divider">
			<i class="fa fa-angle-right"></i>
			<i class="fa fa-road"></i>
			<a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
		</span> 
		<span class="divider">
			<i class="fa fa-angle-right"></i>
			<i class="fa fa-road"></i>
		</span>
		<li class="active">{{ $page_title or ''}}</li>
	</ul>
</div>
<!-- END Breadcrumb -->

<div class="content">
	<div class="panel panel-flat">
		@include('admin.layout._operation_status')
		<div class="panel-heading page-name">
			<h5 class="panel-title">{{$page_title or ''}}</h5>
		</div>

		<div class="panel-body">
			<form class="form-horizontal" id="frm_add_front_page" name="frm_add_front_page" action="{{$module_url_path}}/store" method="post" enctype="multipart/form-data">
				{{csrf_field()}}
				<fieldset class="content-group">	
					<div class="row">
						<div class="col-lg-8">
							<div class="form-group">
								<label class="control-label col-sm-4 col-md-4 col-lg-3" for="name">Blog Image<i class="red">*</i></label>
								<div class="col-sm-8 col-md-8 col-lg-9">
									<div class="image-editor">
										<input type="file" name="image-editor" class="cropit-image-input" style="display: none;" data-rule-required='true' id="image">
										@php 
										$image_url = get_default_image(396,690,40,'No Blog Image' );
										@endphp
										<input type="hidden" id="default-image" value="{{ $image_url }}">
										<div class="cropit-preview">
											<img class="cropit-preview-image" src="{{ $image_url }}" />
										</div>
										<div class="image-size-label">
											<a href="javascript:void(0)" class="btn gray-btn btn-upload"> Select Image</a>
										</div>
										<input type="range" class="cropit-image-zoom-input">
										<input type="hidden" name="image-data" class="hidden-image-data" id="image-data" />
									</div>
									<i class="red">{!! get_image_upload_note('advertisement',390,690) !!}</i>
									<div class="error" id="err_logo"></div>
									<span class="error">{{ $errors->first('name') }} </span>
								</div>
							</div>
						</div>
					</div>

					<div class="row" style=" margin-top: 35px;">
						<div class="col-lg-8">
							<div class="form-group">
								<label class="control-label col-sm-4 col-md-4 col-lg-3" for="name">Blog Name<i class="red">*</i></label>
								<div class="col-sm-8 col-md-8 col-lg-9">
									<input type="text" name="name" id="name" class="form-control" placeholder="Blog Name" data-rule-required="true" data-rule-maxlength="100" tabindex="1" >
									<span class="error">{{ $errors->first('name') }} </span>
								</div>
							</div>
						</div>
					</div>
					@php 
					$arr_category = get_blog_categories();
					@endphp
					<div class="row">
						<div class="col-lg-8">
							<div class="form-group">
								<label class="control-label col-sm-4 col-md-4 col-lg-3" for="validity">Blog Category<i class="red">*</i></label>
								<div class="col-sm-8 col-md-8 col-lg-9">
									<select class="form-control" id="category" name="category" data-rule-required="true">
										<option value="">Select Category</option>
										@if(isset($arr_category) && is_array($arr_category) && sizeof($arr_category)>0)
										@foreach ($arr_category as $category)
										<option value="{{ base64_encode($category['id']) }}" @if(isset($arr_data['category_id']) && $arr_data['category_id']==$category['id']) selected="selected" @endif>{{ $category['name'] or '' }}</option>

										@endforeach
										@else
										<option value="">No Categories Added</option>
										@endif
									</select>
									<span class="error">{{ $errors->first('category') }} </span>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-8">
							<div class="form-group">
								<label class="control-label col-sm-4 col-md-4 col-lg-3" for="blog_description">Blog Description<i class="red">*</i></label>
								<div class="col-sm-8 col-md-8 col-lg-9">
									<textarea  name="blog_description" id="blog_description" class="form-control" data-rule-required="true" rows="15" tabindex="5">	"</textarea>
									<span class="error">{{ $errors->first('blog_description') }} </span>
								</div>
							</div>
						</div>
					</div>
					<input name="image" type="file" class="hidden tinymce_upload" onchange="">
					
					<div class="row">
						<div class="col-lg-8">
							<div class="form-group">
								<label class="control-label col-sm-4 col-md-4 col-lg-3" for="name">Tags</label>
								<div class="col-sm-8 col-md-8 col-lg-9">
									<input type="text" class="form-control tokenfield" value="" placeholder="Tags" name="tags">
									<span class="error">{{ $errors->first('name') }} </span>
								</div>
							</div>
						</div>
					</div>
					
					<div class="form-group text-right">
						<div class="col-lg-8">
							<a href="{{ $module_url_path or '' }}" class="btn gray-btn">Cancel</a href="#">
								<button type="submit" class="btn green-btn" id="btn_add_front_page" tabindex="6">Save</button>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
	<script src="{{url('/public')}}/admin/js/cropit/jquery.cropit.js"></script>
	<script src="{{url('/public')}}/admin/js/pages/form_tags_input.js"></script>
	<script src="{{url('/public')}}/admin/js/plugins/forms/tags/tokenfield.min.js"></script>
	<script src="{{url('/public')}}/admin/js/plugins/forms/tags/tagsinput.min.js"></script>

	<script>
		tinymce.init(
		{ 
			selector:'#blog_description',
			valid_elements: "*[*]",
			force_p_newlines : false,
			forced_root_block : '',
			paste_data_images: true,
            plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code'
            ],

            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            video_template_callback: function(data) 
            {
			   return '<video width="' + data.width + '" height="' + data.height + '"' + (data.poster ? ' poster="' + data.poster + '"' : '') + ' controls="controls">\n' + '<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + (data.source2 ? '<source src="' + data.source2 + '"' + (data.source2mime ? ' type="' + data.source2mime + '"' : '') + ' />\n' : '') + '</video>';
			},
            valid_elements : '*[*]',
            image_advtab: true,
            file_picker_callback: function(callback, value, meta) {
              if (meta.filetype == 'image') {
                $('.tinymce_upload').trigger('click');
                $('.tinymce_upload').on('change', function() {
                  var file = this.files[0];
                  var reader = new FileReader();
                  reader.onload = function(e) {
                    callback(e.target.result, {
                      alt: ''
                    });
                  };
                  reader.readAsDataURL(file);
                });
              }
            },
            content_css: [
            '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            '//www.tinymce.com/css/codepen.min.css'
            ] 
		});
	</script>
	<script>
		// tinymce.init({ selector:'#blog_description',file_picker_types: 'file image media',valid_elements: "*[*]",force_p_newlines : false,forced_root_block : '',  plugins: "link"});
	
		$(document).ready(function() {
		$('#btn_add_front_page').click(function(){
				tinyMCE.triggerSave();
			});
    $(".image-editor").cropit({
        smallImage: "allow"
    }), $("input[name=_token]").val(), $("#frm_add_front_page").validate({
        highlight: function(e) {},
        ignore: [],
        errorPlacement: function(e, a) {
            "image-editor" === $(a).attr("name") ? e.appendTo("#err_logo") : e.insertAfter(a)
        }
    }), $("#frm_add_front_page").submit(function() {
        var e = $(".image-editor").cropit("export");
        if ($(".hidden-image-data").val(e), $("#frm_add_front_page").valid()) return showProcessingOverlay(), !0
    }), $(".btn-upload").click(function() {
        $(".cropit-image-input").trigger("click")
    })
}), $(document).ready(function() {
    var e = document.getElementById("image"),
        a = $("#default-image").val();
    $(e).change(function() {
        if (e.files && e.files[0]) {
            var i = e.files,
                t = i[0].name.substring(i[0].name.lastIndexOf(".") + 1),
                n = new FileReader;
            if ("JPEG" != t && "jpeg" != t && "jpg" != t && "JPG" != t && "png" != t && "PNG" != t) return showAlert("Sorry, " + i[0].name + " is invalid, allowed extensions are: jpeg , jpg , png", "error"), $("#image").val(""), $("#image-data").val(""), $(".cropit-preview-image").attr("src", a), !1;
            n.onload = function(e) {
                var a = new Image;
                a.src = e.target.result, a.onload = function() {
                    this.height, this.width
                }
            }, n.readAsDataURL(e.files[0])
        }
    })
});
	</script>

	@endsection


