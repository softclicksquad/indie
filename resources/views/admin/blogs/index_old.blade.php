@extends('admin.layout.master')    
@section('main_content')
<!-- Page header  TEST-->
<!-- /page header -->

<!-- BEGIN Main Content -->
<!-- Content area -->
<div class="content">
	<div class="panel panel-flat">
		@include('admin.layout._operation_status')
		<div class="panel-heading">
			<h5 class="panel-title">{{$module_title or ''}}</h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li>
						<a href="{{ $module_url_path.'/create' }}" class="btn btn-default btn-rounded show-tooltip" title="Create"><i class="fa fa-plus"></i></a>
					</li>
					<li>
						<a href="javascript:void(0)" class="btn btn-default btn-rounded show-tooltip" title="In-Activate Multiple" onclick="check_multi_action('frm_manage','deactivate')"><i class="fa fa-lock"></i></a>
					</li>
					<li>
						<a href="javascript:void(0)" class="btn btn-default btn-rounded show-tooltip" title="Activate Multiple" onclick="check_multi_action('frm_manage','activate')"><i class="fa fa-unlock"></i></a>
					</li>
					<li>
						<a href="{{$module_url_path}}" class="btn btn-default btn-rounded show-tooltip" title="Refresh"><i class="fa fa-refresh"></i></a>
					</li>
					<li>
						<a href="javascript:void(0)" class="btn btn-default btn-rounded show-tooltip" title="Delete Multiple" onclick="check_multi_action('frm_manage','delete')"><i class="fa fa-trash"></i></a>
					</li>		
				</ul>
			</div>
		</div>
		<hr class="horizotal-line">
		<form name="frm_manage" id="frm_manage" method="POST" class="form-horizontal" action="{{url($module_url_path)}}/multi-action">
			{{ csrf_field() }}
			<input type="hidden" name="multi_action" value="" />

			<div class="table-responsive">
				<table class="table dataTable no-footer custome-table large-table" id="tbl_activitylog_listing">
					<thead>
						<tr class="border-solid" style="max-width: 10px">
							<th>
								<div class="check-box">
									<input type="checkbox" class="filled-in" name="selectall" id="select_all" onchange="chk_all(this)" />
									<label for="selectall">
										
									</label>
								</div>
							</th>

							<th>Advertisement</th>

							<th>
								<div class="dataTables_filter">Name
									<input type="text" name="q_name" placeholder="Search" class="search-block-new-table column_filter"/>
								</div>
							</th>

							<th>
								<div class="dataTables_filter">Status
									<select class="search-block-new-table column_filter" id="q_status" name="q_status">
										<option value="">Select Status</option>
										<option value="1">Active</option>
										<option value="0">In-Active</option>
									</select>
								</div>
							</th>
							<th>
								Added On
							</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</form>
	</div>

	<script type="text/javascript">function filterData() {
    table_module.draw()
}
$(document).ready(function() {
    table_module = $("#tbl_activitylog_listing").DataTable({
        processing: !1,
        serverSide: !0,
        searchDelay: 350,
        autoWidth: !1,
        bFilter: !1,
        columnDefs: [{
            targets: 0,
            searchable: !1,
            orderable: !1,
            className: "dt-body-center"
        }],
        ajax: {
            url: "{{ $module_url_path}}/load_data",
            data: function(e) {
                e["column_filter[q_name]"] = $("input[name='q_name']").val(), e["column_filter[q_status]"] = $("select[name='q_status']").val()
            }
        },
        columns: [{
            render: function(e, a, t, l) {
                return '<div class="check-box"><input type="checkbox" class="filled-in case" name="checked_record[]"  d="mult_change_' + t.id + '" value="' + t.id + '" /><label for="mult_change_' + t.id + "></label></div>"
            },
            orderable: !1,
            searchable: !1
        }, {
            data: "advertisement",
            orderable: !1,
            searchable: !0,
            name: "advertisement"
        }, {
            data: "name",
            orderable: !1,
            searchable: !0,
            name: "name"
        },  {
            data: "status_btn",
            orderable: !1,
            searchable: !0,
            name: "status_btn"
        }, {
            data: "created_at",
            orderable: !1,
            searchable: !0,
            name: "created_at"
        }, {
            data: "action_btn",
            orderable: !1,
            searchable: !0,
            name: "action_btn"
        }]
    })
}), $("input.column_filter").on("keyup", function() {
    filterData()
}), $("select.column_filter").on("change ", function() {
    filterData()
});</script>
@endsection


