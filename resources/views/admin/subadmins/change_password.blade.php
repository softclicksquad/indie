@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-users"></i>
      <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-edit"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-edit"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
            @include('admin.layout._operation_status')  
            <form name="validation-form" id="validation-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/update_password/{{$enc_id}}" enctype="multipart/form-data">
               {{ csrf_field() }}
               
               <div class="form-group">
                <label class="col-sm-3 col-lg-2 control-label">New password<i class="red">*</i></label>
                <div class="col-sm-9 col-lg-4 controls">
                    <input type="password" class="form-control" name="password" id="password" data-rule-minlength="8" data-rule-required="true"/>
                    <span class='help-block'>{{ $errors->first('password') }}</span>
                </div>
               </div>
               <div class="form-group">
                   <label class="col-sm-3 col-lg-2 control-label">Re-type new password<i class="red">*</i></label>
                   <div class="col-sm-9 col-lg-4 controls">
                       <input type="password" class="form-control" name="confirm_password" id="confirm_password" data-rule-minlength="8" data-rule-required="true" data-rule-equalto="#password"/>
                       <span class='help-block'>{{ $errors->first('confirm_password') }}</span>
                   </div>
               </div>

               <br>
               <div class="form-group">
                  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                     <input type="submit" value="Save" class="btn btn btn-primary">
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>


<script type="text/javascript">
$(document).ready(function() {
    $.validator.addMethod("pwcheck",
   function(value, element) {
      return /^^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/.test(value);
   });


    $("#validation-form").validate({
      errorElement: 'span',
       rules: {
      password: 
               {
                 required: true,
                 pwcheck: true,
                 minlength: 8,
                 
               },
      confirm_password: 
               {
                 required: true,
                 pwcheck: true,
                 minlength: 8,
                 
               },
         },
      messages: 
     {
     current_password:
           {
             required: "This field is required.",
             pwcheck: "Password must have uppercase lowercase and one digit.",
             minlength:"Password required minumum 8 characters.",
   
           },
     password:
           {
             required: "This field is required.",
             pwcheck: "Password must have uppercase,lowercase and one digit.",
             minlength:"Password required minumum 8 characters.",
   
           },
       }
    });
   });
     
</script>
@stop