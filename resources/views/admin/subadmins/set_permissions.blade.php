@extends('admin.layout.master')                

@section('main_content')

<!-- BEGIN Page Title -->
<link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/data-tables/latest/dataTables.bootstrap.min.css">

<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
        </li>
        <span class="divider">
            <i class="fa fa-angle-right"></i>
            <i class="fa fa-users"></i>
            <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
        </span> 
        <span class="divider">
            <i class="fa fa-angle-right"></i>
            <i class="fa fa-users"></i>
        </span>
        <li class="active">{{ $page_title or ''}}</li>
    </ul>
</div>
<!-- END Breadcrumb -->

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3>
                    <i class="fa fa-users"></i>
                    {{ isset($page_title)?$page_title:"" }}
                </h3>                
            </div>                
            <div class="box-content">              
                <h4>
                    Sub-Admin Name : {{$sub_admin_name or 'NA'}} 
                </h4>
                @include('admin.layout._operation_status')

                <form name="frm_manage" id="frm_manage" method="POST" class="form-horizontal" action="{{$module_url_path}}/store_permissions" onsubmit="return validation();">
                    {{ csrf_field() }}

                    <div class="table-responsive" style="border:0">
                        <table class="table table-advance"  id="table2" >
                            <thead>
                                <tr>
                                    <th>Module Name</th> 
                                    <th>Permission</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>All</td>
                                    <td> <input type="checkbox" name="all_permissions[]" class="selectall"> </td>
                                </tr>

                                @if(isset($arr_modules) && sizeof($arr_modules)>0)
                                @foreach($arr_modules as $modules)

                                <tr>
                                    <td>{{$modules['title'] or 'NA'}}</td>
                                    <td>
                                        <input type="checkbox" name="permissions[moderator][{{$modules['slug']}}]" class="case moduleWiseCheckboxSelection" id="{{ $modules['slug'] == 'support_ticket' ? 'support_ticket_checkbox' : '' }}" 
                                        @if(isset($arr_permissions) && count($arr_permissions)>0)
                                            @foreach($arr_permissions as $key=>$data)
                                                @if($key == $modules['slug'])
                                                checked="true"
                                                @endif
                                            @endforeach
                                        @endif>
                                        @if($modules['slug'] == 'support_ticket' && isset($support_cats) && !empty($support_cats))
                                        <select name="support_category" class="form-control input-sm" id="support_category" style="margin-left: 5%">
                                            <option value=""> Select Category </option>
                                            @foreach($support_cats as $category)
                                            <option value="{{ $category['id'] or '' }}" {{ $support_cat_id == $category['id'] ? 'selected' : '' }} > {{ $category['title'] or '' }}  </option>
                                            @endforeach
                                        </select>
                                        @endif
                                    </td>
                                    </tr>

                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <input type="hidden" name="sub_admin_id" value="{{$sub_admin_id or '0'}}">
                        <div><input type="submit" name="submit" value="Add" class="btn btn btn-primary"></div>        
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END Main Content -->

    <script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/dataTables.bootstrap.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function() {
            $('#table2').dataTable({
                "bPaginate": false,
                "bFilter": false,
                "bInfo": false, });
        });

        $('.selectall').click(function(){

            if($('.selectall').prop('checked') == true){
                $('.case').prop('checked','true');
            }else{
                $('.case').removeAttr('checked');
            }
        });

        function validation()
        {
            var is_checked = $('.moduleWiseCheckboxSelection').is(":checked");
            if(is_checked<=0)
            {
                alertify.alert('Please select atleast one recoed');
                return false;
            }
            else if($("#support_ticket_checkbox").is(":checked") && ($('#support_category').val() == '' || $('#support_category').val() == undefined) ){
                alertify.alert('Please select Support Category');
                return false;
            }
            else
            {
                return true;
            }
        }

    </script>
    @stop                    


