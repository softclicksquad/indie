@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-users"></i>
      <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-edit"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-edit"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
            @include('admin.layout._operation_status')  
            <form name="validation-form" id="validation-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/update/{{$enc_id}}" enctype="multipart/form-data">
               {{ csrf_field() }}
               
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="email">Email<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="email" class="form-control" value="{{isset($arr_subadmin['user_details']['email'])?$arr_subadmin['user_details']['email']:''}}" data-rule-required="true"  data-rule-email="true" placeholder="Email">
                     <span class='error'>{{ $errors->first('email') }}</span>
                  </div>
                  <input type="hidden" value="{{base64_encode($arr_subadmin['user_id'])}}" name="user_id">
               </div>

               {{-- <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="first_name">Password<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="password" name="password" class="form-control" value="{{old('password')}}" data-rule-required="true" data-rule-minlength="8" placeholder="Password">
                     <span class='error'>{{ $errors->first('password') }}</span>
                  </div>
               </div> --}}

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="name">Name<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="name" class="form-control" value="{{isset($arr_subadmin['name'])?$arr_subadmin['name']:''}}" data-rule-required="true" data-rule-maxlength="255" placeholder="First Name">
                     <span class='error'>{{ $errors->first('name') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="contact_number">Contact Number<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="contact_number" class="form-control" value="{{isset($arr_subadmin['contact_number'])?$arr_subadmin['contact_number']:''}}" data-rule-required="true" data-rule-number="true" data-rule-minlength="6" data-rule-maxlength="16" placeholder="Contact Number">
                     <span class='error'>{{ $errors->first('contact_number') }}</span>
                  </div>
               </div>

                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="fax">Fax Number<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="fax" class="form-control" value="{{isset($arr_subadmin['fax'])?$arr_subadmin['fax']:''}}" data-rule-required="true" data-rule-maxlength="12" placeholder="Fax Number">
                     <span class='error'>{{ $errors->first('fax') }}</span>
                  </div>
               </div>


               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="address">Address <i class="red">*</i></label>
                <div class="col-sm-6 col-lg-4 controls">
                <textarea name="address" class="form-control" data-rule-required="true" data-rule-maxlength="425">{!! isset($arr_subadmin['address'])?$arr_subadmin['address']:''!!}</textarea>
                <span class='error'>{{ $errors->first('address') }}</span>
                </div>
               </div>

                {{-- <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="address">Attachment <i class="red">*</i></label>
                <div class="col-sm-6 col-lg-3 controls">
                      <input type="file" name="file_attachment" id="attachment">  
                        <span class="label label-important">NOTE!</span>
                        <span>Upload File: jpg,jpeg,gif,png,pdf,doc </span>
                </div>
               
                @if(isset($arr_subadmin['file_attachment']) && $arr_subadmin['file_attachment']!="")
                <a href="{{$module_url_path}}/download/{{$enc_id}}"><span class="glyphicon glyphicon-download-alt"></span>  Download Attachment</a>
                @else
                <a href="javascript:void(0);"><span class="glyphicon glyphicon-download-alt"></span>  Download Attachment</a>
                @endif
               </div> --}}

               <br>
               <div class="form-group">
                  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                     <input type="submit" value="Save" class="btn btn btn-primary">
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>

@stop
