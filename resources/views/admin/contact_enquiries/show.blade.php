@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-envelope-square"></i>
      <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-envelope-square"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-envelope-square"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
            @include('admin.layout._operation_status')  
            <form name="validation-form" id="validation-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/reply/{{$enc_id}}" enctype="multipart/form-data">
               {{ csrf_field() }}
               <div class="row">
                  <div class="col-md-12 user-profile-info">
                     <p><span>Inquiry ID:</span> {{isset($arr_enquiry['enquiry_indentifier'])?$arr_enquiry['enquiry_indentifier']:''}}</p>
                     <p><span>Name:</span> {{isset($arr_enquiry['name'])?$arr_enquiry['name']:''}}</p>
                     <p><span>Email:</span> <a href="mailto:{{isset($arr_enquiry['email'])?$arr_enquiry['email']:''}}">{{isset($arr_enquiry['email'])?$arr_enquiry['email']:''}}</a></p>
                     <p><span>Contact Number:</span>{{isset($arr_enquiry['contact_number'])?$arr_enquiry['contact_number']:''}}</p>
                     <p><span>Report a problem?:</span> {{isset($arr_enquiry['is_report_a_problem'])?$arr_enquiry['is_report_a_problem']:''}}</p>
                     <p><span>Report a problem :</span> {{isset($arr_enquiry['report_a_problem'])?$arr_enquiry['report_a_problem']:''}}</p>
                     <p><span>Subject:</span> {{isset($arr_enquiry['enquiry_subject'])?$arr_enquiry['enquiry_subject']:''}}</p>
                     <p><span>Inquiry:</span> {{isset($arr_enquiry['enquiry_message'])?$arr_enquiry['enquiry_message']:''}}</p>
                     <p><span>Date/Time:</span>
                     <?php 
                      if(isset($arr_enquiry['created_at']) && $arr_enquiry['created_at'] != "" && $arr_enquiry['created_at']!='0000-00-00 00:00:00')
                      {
                        $tmp_date = new \DateTime($arr_enquiry['created_at']);
                        $date     = $tmp_date->format('d/m/Y H:i');
                      }
                     ?> 
                     {{isset($date)?$date.'  CET':'-'  }}
                     </p>
                     @if(isset($arr_enquiry['reply_status']) && $arr_enquiry['reply_status']==0)
                     <p><span>Message:</span> 
                     <div class="form-group">
                        <div class="col-sm-6 col-lg-8 controls">
                           <textarea name="reply_message" class="form-control" data-rule-required="true" required></textarea>
                           <span class='error'>{{ $errors->first('reply_message') }}</span>
                        </div>
                     </div>
                     </p>
                     <p><span><input type="submit" value="Reply" class="btn btn btn-primary"></span></p>
                     @elseif(isset($arr_enquiry['reply_status']) && $arr_enquiry['reply_status']==1)
                     <p><span>Message:</span>
                        {{isset($arr_enquiry['reply_message']) && $arr_enquiry['reply_message']!=NULL?$arr_enquiry['reply_message']:''}}
                     </p>
                     @endif
                     <p><span>Reply Time:</span>
                     <?php 
                      $reply_time = 'Not replyed';
                      if(isset($arr_enquiry['reply_date_time']) && $arr_enquiry['reply_date_time'] != "" && $arr_enquiry['reply_date_time']!='0000-00-00 00:00:00' && $arr_enquiry['reply_date_time']!=null &&
                         isset($arr_enquiry['created_at']) && $arr_enquiry['created_at'] != "" && $arr_enquiry['created_at']!='0000-00-00 00:00:00')
                      {
                        
                          $datetime1  = new \DateTime($arr_enquiry['created_at']);
                          $datetime2  = new \DateTime($arr_enquiry['reply_date_time']);
                          $interval   = $datetime1->diff($datetime2);
                          $year       = $interval->format('%y');
                          $month      = $interval->format('%m');
                          $day        = $interval->format('%d');
                          $hours      = $interval->format('%h');
                          $minutes    = $interval->format('%i');
                          $second     = $interval->format('%s');
                          $y = $mon = $d = $h = $m = $s= "";
                          if(isset($year)      &&  $year    !=  "0"){  $y    =  $year.' years ';    }
                          if(isset($month)     &&  $month   !=  "0"){  $mon  =  $month.' months ';   }
                          if(isset($day)       &&  $day     !=  "0"){  $d    =  $day.' days ';     }
                          if(isset($hours)     &&  $hours   !=  "0"){  $h    =  $hours.' hours ';   }
                          if(isset($minutes)   &&  $minutes !=  "0"){  $m    =  $minutes.' min ';   }
                          if(isset($second)    &&  $second  !=  "0"){  $s    =  $second.' seconds ';  }


                          if($y != ""){
                          $reply_time   = 'After '.$y;
                          }else if($mon != ""){
                          $reply_time   = 'After '.$mon;
                          }else if($d != ""){
                          $reply_time   = 'After '.$d;
                          }else if($h != ""){
                          $reply_time   = 'After '.$h;
                          }else if($m != ""){
                          $reply_time   = 'After '.$m;
                          }else{
                          $reply_time   = 'After '.$s;
                          }
                      }
                     ?> 
                     {{isset($reply_time)?$reply_time:'Not replyed'}}
                     </p>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<!-- END Main Content -->
@stop