@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-gears"></i>

        <?php 
          $previous_url = url()->previous();
          $arr_segments = [];
          $arr_segments = explode('/',$previous_url);

          if(isset($arr_segments['7']) && $arr_segments['7'] == 'all' )
          {
             $previous_page = "Manage All Projects";
             $back_url      = $module_url_path.'/all';
          }
          else if(isset($arr_info['project_status']) && $arr_info['project_status'] != ""  )
          {
            $back_url = "";  
            if(isset($arr_info['id']) && $arr_info['id'] != "" )
            {
               $project_id = $arr_info['id'];
            }  

            if($arr_info['project_status'] == '1')
            {
              $previous_page = "Manage Assign Project Manager";
              $back_url      = $module_url_path.'/posted';
            }
            else if($arr_info['project_status'] == '2')
            {
              $previous_page = "Manage Open Projects";
              $back_url      = $module_url_path.'/open';
            }
            else if($arr_info['project_status'] == '3')
            {
              $previous_page = "Manage Completed Projects";
              $back_url      = $module_url_path.'/completed';
            }
            else if($arr_info['project_status'] == '4')
            {
              $previous_page = "Manage Ongoing Projects";
              $back_url      = $module_url_path.'/ongoing';
            }
            else if($arr_info['project_status'] == '5')
            {
              $previous_page = "Manage Canceled Projects";
               $back_url      = $module_url_path.'/canceled';
            }
          }
          else
          {
            $previous_page = "Manage All Projects";
            $back_url      = $module_url_path.'/all';
          }    
        ?>

          <a href="{{ $back_url or '#'}}">{{ $previous_page or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-gear"></i>
      </span>
      <li class="active">{{ 'Project Details' }}</li>
   </ul>
</div>
<!-- END Breadcrumb -->

<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-gear"></i>
               {{ isset($arr_info['project_name'])?$arr_info['project_name']:"" }}

                @if(isset($arr_info['project_name']) &&  $arr_info['project_name'] != "" )
                  <span class="divider">
                    <i class="fa fa-angle-right"></i> 
                  </span>  
                @endif
                {{ 'Project Details' }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
               @include('admin.layout._operation_status')
           <div class="alert alert-success" id="ajax_success" style="display:none;">
            <button data-dismiss="alert" class="close">×</button>
            <strong>Success!</strong>
            <div id="ajax_sub_success"></div>
         </div>
         <div class="alert alert-danger" id="ajax_error" style="display:none;">
            <button data-dismiss="alert" class="close">×</button>
            <strong>Error!</strong> 
            <div id="ajax_sub_error"></div>
         </div>
              <div class="row">
               @if(isset($arr_info) && sizeof($arr_info)>0)
              <form name="validation-form" id="validation-form" method="POST" action="{{$module_url_path.'/update_project_status'}}" class="form-horizontal"  enctype="multipart/form-data">
               {{ csrf_field() }}
                <div class="col-md-6">
                   
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Project Name:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['project_name'])?$arr_info['project_name']:''}}</div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Project Category:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['category_details']['category_title'])?$arr_info['category_details']['category_title']:''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Client Name:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{ ucfirst($arr_info['client_details']['user_name']) }}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Client Email:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block"><a href="mailto:{{isset($arr_info['client_details']['email'])?$arr_info['client_details']['email']:''}}">{{isset($arr_info['client_details']['email'])?$arr_info['client_details']['email']:''}}</a></div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Skills:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">
                        <?php $str = "";?>
                            @if(isset($arr_info['project_skills']) && count($arr_info['project_skills']) > 0)
                              @foreach($arr_info['project_skills'] as $key=> $skill) 
                                @if(isset($skill['skill_data']['skill_name']) && $skill['skill_data']['skill_name'] )
                                    <?php  $str .= $skill['skill_data']['skill_name'].' ,'; ?>
                                @endif
                              @endforeach
                            @endif

                            {{ trim($str,' ,') }}
                        </div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Project Summary:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['project_description'])?$arr_info['project_description']:''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Start Date:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['project_start_date'])? date('d M Y',strtotime($arr_info['project_start_date'])):''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">End Date:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['project_end_date'])? date('d M Y',strtotime($arr_info['project_end_date'])):''}}</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Project Duration:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['project_expected_duration'])?$arr_info['project_expected_duration']:''}} days</div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                 
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Project Cost:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['project_currency'])?$arr_info['project_currency']:''}}&nbsp;{{isset($arr_info['project_cost'])?$arr_info['project_cost']:''}}
                            @if($arr_info['project_pricing_method']=='2')
                              {{ 'Per Hour' }}
                            @else
                              {{ 'Budget' }}
                            @endif
                        </div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>

                  @if(isset($arr_info['project_attachment']) && $arr_info['project_attachment'] != "")

                    <div class="form-group">
                     <label class="col-sm-3 control-label">Project Attchments:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">{{isset($arr_info['project_attachment'])?$arr_info['project_attachment']:''}} <a download href="{{url('/public')}}{{config('app.project.img_path.project_attachment')}}{{$arr_info['project_attachment']}}"> Download</a></div>
                        <div class="client-name-block"></div>
                     </div>
                    </div>
                  @endif
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Project Status:</label>
                     <div class="col-sm-9 controls">
                        <div class="client-name-block">
                          <select name="project_status" data-id="" data-rule-required="true" class="form-control">
                            {{--<option value="">--Select--</option>--}}
                            @if(isset($arr_info['project_status']) && $arr_info['project_status'] != ""  )
                              @if($arr_info['project_status'] == '1')
                                <option value="2" @if($arr_info['project_status']=='2') {{'selected="selected"'}}@endif>Open</option>
                                {{--<option value="3" @if($arr_info['project_status']=='3') {{'selected="selected"'}}@endif>Completed</option>--}}
                                <option value="5" @if($arr_info['project_status']=='5') {{'selected="selected"'}}@endif>Canceled</option>  
                              @elseif($arr_info['project_status'] == '2')
                                <option value="2" @if($arr_info['project_status']=='2') {{'selected="selected"'}}@endif>Open</option>
                               {{--  <option value="4" @if($arr_info['project_status']=='4') {{'selected="selected"'}}@endif>Ongoing</option> --}}
                                <option value="5" @if($arr_info['project_status']=='5') {{'selected="selected"'}}@endif>Canceled</option> 
                              @elseif($arr_info['project_status'] == '3')
                                <option value="3" @if($arr_info['project_status']=='3') {{'selected="selected"'}}@endif>Completed</option>
                              @elseif($arr_info['project_status'] == '4')
                                <option value="4" @if($arr_info['project_status']=='4') {{'selected="selected"'}}@endif>Ongoing</option>
                                <option value="3" @if($arr_info['project_status']=='3') {{'selected="selected"'}}@endif>Completed</option>
                                <option value="5" @if($arr_info['project_status']=='5') {{'selected="selected"'}}@endif>Canceled</option> 
                              @elseif($arr_info['project_status'] == '5')
                                <option value="5" @if($arr_info['project_status']=='5') {{'selected="selected"'}}@endif>Canceled</option> 
                              @endif
                            @endif
                          </select>
                        </div>
                        <div class="client-name-block"></div>
                     </div>
                  </div>
                  <input type="hidden" name="project_id" value="{{$arr_info['id']}}" id="project_id">

                  @if($arr_info['project_handle_by_pm']=='1')
                    @if(isset($arr_info['project_manager_user_id']) && $arr_info['project_manager_user_id']!="")

                     <div class="form-group">
                       <label class="col-sm-3 control-label">Project Manager:</label>
                       <div class="col-sm-9 controls">
                          <div class="client-name-block">
                            <input type="hidden" name="pre_project_manager_id"  id="pre_project_manager_id" value="{{isset($arr_info['project_manager_info']['user_id'])?$arr_info['project_manager_info']['user_id']:''}}">
                            {{isset($arr_info['project_manager_info']['first_name'])?$arr_info['project_manager_info']['first_name']:''}}
                            {{isset($arr_info['project_manager_info']['last_name'])?$arr_info['project_manager_info']['last_name']:''}}
                          </div>
                          <div class="client-name-block"></div>
                       </div>
                     </div>

                      <div class="form-group">
                       <label class="col-sm-3 control-label">PM Email:</label>
                       <div class="col-sm-9 controls">
                          <div class="client-name-block">
                          {{isset($arr_info['project_manager_details']['email'])?$arr_info['project_manager_details']['email']:''}}
                          </div>
                          <div class="client-name-block"></div>
                       </div>
                     </div>

                     <div class="form-group">
                       <label class="col-sm-3 control-label">Change Manager:</label>
                       <div class="col-sm-9 controls">
                          <div class="client-name-block">
                            <select name="project_manager" data-rule-required="true" data-id="" class="form-control project_manager">
                            <option value="">--Select--</option>
                             @foreach($arr_manager as $manager)
                            
                              <option value="{{$manager['user_id']}}" @if($manager['user_id']==$arr_info['project_manager_user_id']) {{'selected="selected"'}}@endif>{{$manager['first_name']}}</option>
                             
                             @endforeach
                          </select>
                          </div>
                          <div class="client-name-block"></div>
                       </div>
                     </div>
                    @else
                    <div class="form-group">
                       <label class="col-sm-3 control-label">Project Manager:</label>
                       <div class="col-sm-9 controls">
                          <div class="client-name-block">
                            <select name="project_manager" data-rule-required="true" data-id="" class="form-control project_manager">
                            <option value="">--Select--</option>
                             @foreach($arr_manager as $manager)
                            
                             <option value="{{$manager['user_id']}}" @if($manager['user_id']==$arr_info['project_manager_user_id']) {{'selected="selected"'}}@endif>{{$manager['first_name']}}</option>
                           
                             @endforeach
                          </select>
                          </div>
                          <div class="client-name-block"></div>
                       </div>
                    </div>
                      <div style="margin-left: 208px;width: 600px;" class="alert alert-info">
                          <strong>Info!</strong> Project Managers whose status is available are only shown here.
                      </div>
                    @endif
                  @endif



                  @if(isset($arr_info['project_status']) && ($arr_info['project_status'] == '3' || $arr_info['project_status'] == '4' || $arr_info['project_status'] == '5')  )
                  <div class="form-group">
                     <label class="col-sm-3 control-label">Expert's Work Attchments:</label>
                      <div class="col-sm-9 controls">
                        <style type="text/css">

                          table {
                            border-collapse: collapse;
                          }

                          td, th {
                            border: 1px solid #999;
                            padding: 0.5rem;
                            text-align: left; 
                          }

                        </style>  

                            @if(isset($arr_info['experts_project_attachment']) && count($arr_info['experts_project_attachment']) > 0 )
                            <div class="table-responsive table-block-main">
                                    <table style="margin-top:8px; width:100%" >
                                  <thead>
                                    <tr>
                                      <th>Sr. No.</th>
                                      <th>Document Name</th>
                                      <th>Download</th>
                                    </tr>
                                  </thead>

                                  <tbody>
                                  @foreach($arr_info['experts_project_attachment'] as $key => $attchment )
                                    @if($attchment['attachment'] != "")
                                    <tr>
                                      <td>{{$key + 1}}</td>
                                      <td>{{ $attchment['attachment'] or '' }}</td>
                                      <td><a title="Download Project Attchment" download href="{{$expert_project_attachment_public_path or ''}}{{$attchment['attachment']}}" >
                                        <span style="color: #0E81C2;"><i class="fa fa-download"></i></span>
                                     </a>

                                        @if(isset($user) && $user->inRole('expert'))

                                        &nbsp;<span style="color: #C32880;" ><i style="cursor: pointer;" onclick="deleteProjectAttachment('{{base64_encode($attchment['id'])}}')" class="fa fa-trash"></i></span>
                                        
                                        @endif

                                     </td>
                                    </tr>
                                    @endif
                                   @endforeach 
                                  </tbody>
                                </table>
                       </div>
                       @else
                        No work related files are uploaded by expert.
                       @endif

                     </div>
                  </div>

                  @endif

                  <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-3">
                       <input type="button" onclick="javascript:return isconfirm()" class="btn btn btn-primary" value="Save">
                    </div>
                  </div>
                  
                </div>

              </form>
            </div>
               @endif
            </div>
       </div>
   </div>
</div>

<!-- END Main Content -->
<link href="{{url('/public')}}/admin/css/select2.min.css" rel="stylesheet" />
<script src="{{url('/public')}}/admin/js/select2.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $(".project_manager").select2();
});
</script>
<script type="text/javascript">
   function isconfirm(url) {
      alertify.confirm("Are you sure? You want to perform this action", function (e) {
          if (e) {
              $('#validation-form').submit();
          } else {
              return false;
          }
      });
   }
</script>
@stop