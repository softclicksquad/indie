@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<div class="page-title">
   <div>
   </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
   <ul class="breadcrumb">
      <li>
         <i class="fa fa-home"></i>
         <a href="{{ url($admin_panel_slug.'/dashboard') }}">Dashboard</a>
      </li>
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-desktop"></i>
      <a href="{{ $module_url_path }}">{{ $module_title or ''}}</a>
      </span> 
      <span class="divider">
      <i class="fa fa-angle-right"></i>
      <i class="fa fa-list"></i>
      </span>
      <li class="active">{{ $page_title or ''}}</li>
   </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="box">
         <div class="box-title">
            <h3>
               <i class="fa fa-edit"></i>
               {{ isset($page_title)?$page_title:"" }}
            </h3>
            <div class="box-tool">
               <a data-action="collapse" href="#"></a>
               <a data-action="close" href="#"></a>
            </div>
         </div>
         <div class="box-content">
            @include('admin.layout._operation_status')  
            <form name="validation-form" id="validation-form" method="POST" class="form-horizontal" action="{{$module_url_path}}/update/{{$enc_id}}" enctype="multipart/form-data">
               {{ csrf_field() }}
               
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="email">Email<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="email" class="form-control" value="{{isset($arr_client['user_details']['email'])?$arr_client['user_details']['email']:''}}" data-rule-required="true"  data-rule-required="email" placeholder="Email" disabled="true">
                     <span class='error'>{{ $errors->first('email') }}</span>
                  </div>
               </div>

               {{-- <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="first_name">Password<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="password" name="password" class="form-control" value="{{old('password')}}" data-rule-required="true" data-rule-minlength="8" placeholder="Password">
                     <span class='error'>{{ $errors->first('password') }}</span>
                  </div>
               </div> --}}

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="first_name">First Name<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="first_name" class="form-control" value="{{isset($arr_client['first_name'])?$arr_client['first_name']:''}}" data-rule-required="true" data-rule-maxlength="255" placeholder="First Name">
                     <span class='error'>{{ $errors->first('first_name') }}</span>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="last_name">Last Name<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="last_name" class="form-control" value="{{isset($arr_client['last_name'])?$arr_client['last_name']:''}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Last Name">
                     <span class='error'>{{ $errors->first('last_name') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="phone">Contact Number<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="phone_number" class="form-control" value="{{isset($arr_client['phone_number'])?$arr_client['phone_number']:''}}" data-rule-required="true" data-rule-minlength="10" data-rule-maxlength="12" data-rule-number="true" placeholder="Contact Number">
                     <span class='error'>{{ $errors->first('phone') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="country_name">Country<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                    <!--  <select type="text" name="country" class="form-control" data-rule-required="true">
                           <option value="">---Select Country---</option> -->
                        @if(isset($arr_countries) && sizeof($arr_countries)>0)
                        <select class="form-control" data-rule-required="true" name="country" id="country_client" onchange="javascript: return loadStates(this);">
                           <option value="">---Select Country---</option>

                           @foreach($arr_countries as $countries)
                           @if($countries['id']==$arr_client['country'])

                           <option value="{{isset($arr_client['country'])?$arr_client['country']:""}}" selected="selected">
                           {{isset($arr_client['country_details']['country_name'])?$arr_client['country_details']['country_name']:"" }}
                          </option>

                           @else
                            <option value="{{isset($countries['id'])?$countries['id']:""}}">
                           {{isset($countries['country_name'])?$countries['country_name']:"" }}
                           </option>

                        @endif
                        @endforeach
                     </select>
                     @endif
                     <span class='error'>{{ $errors->first('country') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="state_name">State<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                    <!--  <select type="text" name="country" class="form-control" data-rule-required="true">
                           <option value="">---Select Country---</option> -->
                        @if(isset($arr_client['country_details']['states']) && sizeof($arr_client['country_details']['states'])>0)
                        <select class="form-control" data-rule-required="true" name="state" id="state_client" onchange="javascript: return loadCities(this);">
                           <option value="">---Select State---</option>

                           @foreach($arr_client['country_details']['states'] as $states)
                           @if($states['id']==$arr_client['state'])

                              <option value="{{isset($arr_client['state'])?$arr_client['state']:""}}" selected="selected">
                                 {{isset($arr_client['state_details']['state_name'])?$arr_client['state_details']['state_name']:"" }}
                                </option>
                            @else
                              <option value="{{isset($states['id'])?$states['id']:""}}">
                                 {{isset($states['state_name'])?$states['state_name']:"" }}
                                 </option>

                              @endif
                        @endforeach
                     </select>
                     @endif
                     <span class='error'>{{ $errors->first('state') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="country_name">City<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                    <!--  <select type="text" name="country" class="form-control" data-rule-required="true">
                           <option value="">---Select Country---</option> -->
                        @if(isset($arr_client['state_details']['cities']) && sizeof($arr_client['state_details']['cities'])>0)
                        <select class="form-control" data-rule-required="true" name="city" id="city_client">
                           <option value="">---Select City---</option>

                           @foreach($arr_client['state_details']['cities'] as $cities)
                           @if($cities['id']==$arr_client['city'])

                              <option value="{{isset($arr_client['city'])?$arr_client['city']:""}}" selected="selected">
                                 {{isset($arr_client['city_details']['city_name'])?$arr_client['city_details']['city_name']:"" }}
                                </option>
                            @else
                              <option value="{{isset($states['id'])?$states['id']:""}}">
                                 {{isset($states['state_name'])?$states['state_name']:"" }}
                                 </option>

                              @endif
                        @endforeach
                     </select>
                     @endif
                     <span class='error'>{{ $errors->first('city') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="address">Address <i class="red">*</i></label>
                <div class="col-sm-6 col-lg-4 controls">
                <textarea name="address" class="form-control" data-rule-required="true" data-rule-maxlength="425">{!! isset($arr_client['address'])?$arr_client['address']:''!!}</textarea>
                <span class='error'>{{ $errors->first('address') }}</span>
                </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="phone">Zip Code<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="zip_code" class="form-control" value="{{isset($arr_client['zip'])?$arr_client['zip']:''}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Zip Code">
                     <span class='error'>{{ $errors->first('zip') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="phone">Company Name<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="company_name" class="form-control" value="{{isset($arr_client['company_name'])?$arr_client['company_name']:''}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Company Name">
                     <span class='error'>{{ $errors->first('company_name') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="phone">Owner Name<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="owner_name" class="form-control" value="{{isset($arr_client['owner_name'])?$arr_client['owner_name']:''}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Owner Name">
                     <span class='error'>{{ $errors->first('owner_name') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="phone">Company Size<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="company_size" class="form-control" value="{{isset($arr_client['company_size'])?$arr_client['company_size']:''}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Company Size">
                     <span class='error'>{{ $errors->first('company_size') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="country_name">Founding year<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                        <select class="form-control" data-rule-required="true" name="founding_year" id="founding_year">
                           <option value="">Select Year</option>
                           @for ($year = 1950; $year <= date('Y'); $year++)
                           @if($arr_client['founding_year']==$year)
                              <option value="{{isset($arr_client['founding_year'])?$arr_client['founding_year']:"1950"}}" selected="selected">{{isset($arr_client['founding_year'])?$arr_client['founding_year']:"1950"}}</option>
                           @else
                              <option value="{{$year}}">{{$year}}</option>
                           @endif
                           @endfor
                        </select>
                     </select>
                     <span class='error'>{{ $errors->first('city') }}</span>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label" for="phone">Vat Number<i class="red">*</i></label>
                  <div class="col-sm-6 col-lg-4 controls">
                     <input type="text" name="vat_number" class="form-control" value="{{isset($arr_client['vat_number'])?$arr_client['vat_number']:''}}" data-rule-required="true" data-rule-maxlength="255" placeholder="Vat Number">
                     <span class='error'>{{ $errors->first('vat_number') }}</span>
                  </div>
               </div>

               <br>
               <div class="form-group">
                  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                     <input type="submit" value="Save" class="btn btn btn-primary">
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
 function loadStates(ref)   
 {
     var selected_country = jQuery(ref).val();

     if(selected_country && selected_country!="" && selected_country!=0)
     {

      jQuery('select[id="state_client"]').find('option').remove().end().append('<option value="">Select State</option>').val('');
      jQuery('select[id="city_client"]').find('option').remove().end().append('<option value="">Select City</option>').val('');

           jQuery.ajax({
                           url:'{{url("/")}}/locations/get_states/'+btoa(selected_country),
                           type:'GET',
                           data:'flag=true',
                           dataType:'json',
                           beforeSend:function()
                           {
                               jQuery('select[id="state_client"]').attr('readonly','readonly');
                           },
                           success:function(response)
                           {
                               if(response.status=="success")
                               {
                                   jQuery('select[id="state_client"]').removeAttr('readonly');

                                   if(typeof(response.states) == "object")
                                   {
                                      var option = '<option value="">Select State</option>'; 
                                      jQuery(response.states).each(function(index,state)
                                      {
                                           option+='<option value="'+state.id+'">'+state.state_name+'</option>';
                                      });

                                      jQuery('select[id="state_client"]').html(option);
                                   }

                               }
                               return false;
                           }    
           });
      }
 }

function loadCities(ref)   
 {
     var selected_state = jQuery(ref).val();

     if(selected_state && selected_state!="" && selected_state!=0)
     {

      jQuery('select[id="city_client"]').find('option').remove().end().append('<option value="">Select City</option>').val('');

           jQuery.ajax({
                           url:'{{url("/")}}/locations/get_cities/'+btoa(selected_state),
                           type:'GET',
                           data:'flag=true',
                           dataType:'json',
                          beforeSend:function()
                           {
                               jQuery('select[id="city_client"]').attr('readonly','readonly');
                           },
                           success:function(response)
                           {
                               if(response.status=="success")
                               {
                                   jQuery('select[id="city_client"]').removeAttr('readonly');

                                   if(typeof(response.cities) == "object")
                                   {
                                      var option = '<option value="">Select City</option>'; 
                                      jQuery(response.cities).each(function(index,city)
                                      {
                                           option+='<option value="'+city.id+'">'+city.city_name+'</option>';
                                      });

                                      jQuery('select[id="city_client"]').html(option);
                                   }

                               }
                               return false;
                           }    
                     });
      }
 }

</script>
@stop
