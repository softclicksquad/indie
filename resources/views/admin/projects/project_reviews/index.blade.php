@extends('admin.layout.master')          

    @section('main_content')
  
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/data-tables/latest/') }}/dataTables.bootstrap.min.css">
    <!-- BEGIN Page Title -->
    <div class="page-title">
        <div>

        </div>
    </div>
    <!-- END Page Title -->
    <?php 
          $previous_url = url()->previous();
          $arr_segments = [];
          $arr_segments = explode('/',$previous_url);

          if(isset($arr_segments['7']) && $arr_segments['7'] == 'all' )
          {
             $previous_page = "Manage All Projects";
             $back_url      = $module_url_path.'/all';
          }
          else if(isset($arr_project_reviews['project_status']) && $arr_project_reviews['project_status'] != ""  )
          {
            $back_url = "";  

            if(isset($arr_project_reviews['id']) && $arr_project_reviews['id'] != "" )
            {
              $project_id = $arr_project_reviews['id'];
            } 

            if($arr_project_reviews['project_status'] == '3')
            {
              $previous_page = "Manage Completed Projects";
              $back_url      = $module_url_path.'/completed';
            }
          }
          else
          {
            $previous_page = "Manage All Projects";
            $back_url      = $module_url_path.'/all';
          }    
      ?>
    <!-- BEGIN Breadcrumb -->
     <div id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{ url('/admin/dashboard') }}">Dashboard</a>
            </li>

            <span class="divider">
               <i class="fa fa-angle-right"></i>   
                
            </span>
            <li>
            
            <i class="fa fa-gears"></i>
               <a href="{{ $back_url }}">{{$previous_page}}</a>
            </li>   
            
            <span class="divider">
                <i class="fa fa-angle-right"></i><i class="fa fa-star-half-full"></i>
            </span>

            <li class="active">{{ isset($page_title)?$page_title:"" }}</li>
        </ul>
      </div>
    <!-- END Breadcrumb -->

    <!-- BEGIN Main Content -->
    <div class="row">
      <div class="col-md-12">

          <div class="box">
            <div class="box-title">
              <h3>
                <i class="fa fa-star-half-full"></i>
                  {{ 'Reviews ' }}
                <span class="divider">
                  <i class="fa fa-angle-right"></i>
                </span>
                {{ $arr_project_reviews['project_name'] or '' }}
            </h3>
            <div class="box-tool">
                <a data-action="collapse" href="#"></a>
                <a data-action="close" href="#"></a>
            </div>
        </div>
        <div class="box-content">
        
          @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ Session::get('success') }}
            </div>
          @endif  

          @if(Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ Session::get('error') }}
            </div>
          @endif
          <form class="form-horizontal" id="frm_manage" method="POST" action="{{ url('/web_admin/restaurantReviews/multi_action') }}">

            {{ csrf_field() }}

            <div class="col-md-10">
            

            <div id="ajax_op_status">
                
            </div>
            <div class="alert alert-danger" id="no_select" style="display:none;"></div>
            <div class="alert alert-warning" id="warning_msg" style="display:none;"></div>
          </div>
          <div class="btn-toolbar pull-right clearfix">
            <!--- Add new record - - - -->
               <div class="btn-group">
                                 
                </div>
            <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - -->
            
            <div class="btn-group"> 
              
              
                  <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" 
                     title="Refresh" 
                     onclick="window.location.reload()" 
                     style="text-decoration:none;">
                     <i class="fa fa-repeat"></i>
                  </a> 
               
              
             
            </div>
          </div>
          <br/><br/>
          <div class="clearfix"></div>
          <div class="table-responsive" style="border:0">

            <input type="hidden" name="multi_action" value="" />

            <table class="table table-advance"  id="user_manage" >
              <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>From</th>  
                  <th>Ratings</th>
                  <th>Review</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              @if(isset($arr_project_reviews['project_reviews'] ) && sizeof($arr_project_reviews['project_reviews'] )>0)
              @foreach($arr_project_reviews['project_reviews'] as $key => $project_review)  
                
                  <tr>
                    
                    <td>{{ $key+1 }}</td>
                    
                    <td>
                        {{ $project_review['user_details']['user_name'] or '' }}
                    </td>    

                    <td>
                      @for($i=0; $i<$project_review['rating']; $i++)
                        <img alt="" src="{{url('/public')}}/front/images/star1.png">
                      @endfor
                    </td>
                    
                    <td>
                        {{ $project_review['review'] }}
                    </td>

                    <td>
                      <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" href="{{ $module_url_path.'/reviews/details/'.base64_encode($project_review['id']) }}"  data-original-title="Edit">
                        <i class="fa fa-edit" ></i>
                     </a>
                    </td>
                  
                  </tr>
                
                @endforeach
                @endif
                  
              </tbody>
            </table>
          </div>

          </form>
      </div>
  </div>
</div>
</div>
<!-- END Main Content -->
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/assets/data-tables/latest') }}/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#user_manage").DataTable();
    });
    function confirm_delete(url) {
      alertify.confirm("Are you sure? You want to delete this record(s)", function (e) {
          if (e) {
              if(url != "" && url != undefined){
                showProcessingOverlay();
                window.location.href=url;
              }
              return true;
          } else {
              return false;
          }
      });
   }
    function check_multi_action(frm_id,action) {
      var frm_ref = jQuery("#"+frm_id);
      if(jQuery(frm_ref).length && action!=undefined && action!=""){
        /* Get hidden input reference */
        var input_multi_action = jQuery('input[name="multi_action"]');
        if(jQuery(input_multi_action).length) {
          /* Set Action in hidden input*/
          jQuery('input[name="multi_action"]').val(action);
          /*Submit the referenced form */
          jQuery(frm_ref)[0].submit();
        } else {
          console.warn("Required Hidden Input[name]: multi_action Missing in Form ")
        }
      } else {
          console.warn("Required Form[id]: "+frm_id+" Missing in Current Page ")
      }
    }
</script>
@stop                    


