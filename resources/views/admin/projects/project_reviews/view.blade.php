@extends('admin.layout.master')          

    @section('main_content')
  
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/data-tables/latest/') }}/dataTables.bootstrap.min.css">
    <!-- BEGIN Page Title -->
    <div class="page-title">
        <div>

        </div>
    </div>
    <!-- END Page Title -->
    <?php 

          if(isset($arr_project_review['project_id']) && $arr_project_review['project_id'] != "" )
          {
            $previous_page = "View Project Reviews";
            $back_url      = $module_url_path.'/reviews/'.base64_encode($arr_project_review['project_id']);    
          }
          else
          {
            $previous_page = "Manage Completed Projects";
            $back_url      = $module_url_path.'/completed';
          }    
      ?>
    <!-- BEGIN Breadcrumb -->
     <div id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{ url('/admin/dashboard') }}">Dashboard</a>
            </li>

            <span class="divider">
               <i class="fa fa-angle-right"></i>   
            </span>
            <li>
            
            <i class="fa fa-eye"></i>
               <a href="{{ $back_url }}">{{$previous_page}}</a>
            </li>   
            
            <span class="divider">
                <i class="fa fa-angle-right"></i><i class="fa fa-star-half-full"></i>
            </span>

            <li class="active">{{ isset($page_title)?$page_title:"" }}</li>
        </ul>
      </div>
    <!-- END Breadcrumb -->

    <!-- BEGIN Main Content -->
    <div class="row">

      <div class="col-md-12">
        @include('front.layout._operation_status')
          <div class="box">
            <div class="box-title">
              <h3>
                <i class="fa fa-star-half-full"></i>
                    {{ $arr_project_review['project_details']['project_name'] or '' }}{{ ' Review' }} 
            </h3>
            <div class="box-tool">
                <a data-action="collapse" href="#"></a>
                <a data-action="close" href="#"></a>
            </div>
        </div>
        <div class="box-content">
            
          @if(isset($arr_project_review) && sizeof($arr_project_review)>0)
            <form class="form-horizontal" id="validation-form" method="POST" action="{{$module_url_path}}/reviews_update" enctype="multipart/form-data">

            {{ csrf_field() }}
            <input type="hidden"  name="review_id" id="id" readonly="" value="{{ base64_encode( $arr_project_review['id'])}}">

           <div class="form-group">
                <label class="col-sm-3 col-lg-2 control-label" for="email_id">From</label>
                <div class="col-sm-6 col-lg-4 controls">
                    <input class="form-control" name="email_id" id="email_id"  value="{{ $arr_project_review['user_details']['user_name'] or '' }}" readonly="" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-lg-2 control-label" for="email_id">Email Id</label>
                <div class="col-sm-6 col-lg-4 controls">
                    <input class="form-control" name="email_id" id="email_id"  value="{{ $arr_project_review['user_details']['email'] or '' }}" readonly="" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-lg-2 control-label" for="rating">Rating</label>
                <div class="col-sm-6 col-lg-4 controls">
                    @for($i=0; $i<$arr_project_review['rating']; $i++)
                         <img alt="" src="{{url('/public')}}/front/images/star1.png">
                    @endfor
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 col-lg-2 control-label" for="message">Review</label>
                <div class="col-sm-6 col-lg-4 controls">
                    <textarea class="form-control" data-rule-required="true" name="message" id="message" >{{ $arr_project_review['review'] or '' }}</textarea>
                </div>
                <span class='error'>{{ $errors->first('message') }}</span>
            </div>

            <div class="form-group">
              <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
              <input type="submit"  class="btn btn-primary" value="Update">
              <a href="{{ $back_url }}"> 
                <input type="button"  class="btn btn-primary" value="Back">
              </a>  
            </div>
            </div>
    
            </form>

    @endif

</div>
</div>
</div>
</div>
<!-- END Main Content -->

@stop                    