@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<?php
$user = Sentinel::check();
if(!$user){ return redirect('/admin/login'); }    
?>
<!-- BEGIN Content -->
<div id="main-content">
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{ url('/'.$admin_panel_slug.'/dashboard') }}">Dashboard</a>
        </li>
        <span class="divider">
            <i class="fa fa-angle-right"></i>
        </span>
        <li class="active">  {{ isset($page_title)?$page_title:"" }}</li>
    </ul>
</div>
<!-- END Breadcrumb -->
    <!-- BEGIN Tiles -->
        <div class="row">
            <div class="col-md-12">          
            @include('front.layout._operation_status')
            </div>
            <!-- section 1 -->
            <div class="col-md-6">
                <div class="row">
                  @if(isset($client_ewallet_details) && sizeof($client_ewallet_details) > 0)
                        <!--Section 1: User Details -->
                              <div class="col-md-7">
                                  <div class="tile tile-violet">
                                      <div class="img">
                                          <i class="fa fa-user"></i>
                                      </div>
                                      <div class="content">
                                          <div class="user-details-section">
                                              <p>
                                                <b>Client Id </b> <span>{{ isset($client_ewallet_details->ClientId)? $client_ewallet_details->ClientId:''}}</span>
                                                <br>
                                                <b>Email</b> <span>{{ isset($client_ewallet_details->AdminEmails[0])? $client_ewallet_details->AdminEmails[0]:''}}</span>
                                              </p>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                        <!--User Details -->

                        <!--Section 2: Wallet Details -->
                        @if(isset($client_wallet_details) && $client_wallet_details !="" && !empty($client_wallet_details))
                          @foreach($client_wallet_details as $key => $fees_wallet)
                            @if($fees_wallet->Id == "FEES_USD")  
                              <div class="col-md-5">
                                  <div class="tile tile-violet">
                                      <div class="dash-user-details">
                                        <div class="user-details-section">
                                            <div class="content">
                                                <p class="big">{{isset($fees_wallet->Balance->Amount)?$fees_wallet->Balance->Amount/100:''}} {{$fees_wallet->Balance->Currency or ''}}</p>
                                                <p class="title">Your collected fees</p>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                              </div>
                            @endif
                          @endforeach  
                        @else
                            <div class="col-md-6">
                                <div class="tile tile-violet">
                                    <div class="dash-user-details">
                                      <div class="user-details-section">
                                          <div class="content">
                                              <p class="big">0 USD</p>
                                              <p class="title">Your collected fees</p>
                                          </div>
                                      </div>
                                  </div>
                                </div>
                            </div>
                        @endif
                        <!-- End Wallet Details-->
                  @else
                    <div class="col-md-6">
                        <div class="tile tile-violet">
                            <div class="img">
                                <img style="width: 67px;height: 56px;margin-left:3px;" src="{{url('/public')}}/front/images/archexpertdefault/wallet.png" alt="mangopay">
                            </div>
                            <div class="content">
                                <p class="big">&nbsp;</p>
                                <p class="title">Account not created yet</p>
                            </div>
                        </div>
                    </div>
                  @endif  
                </div>
            </div>
        </div>
    <!-- END Tiles -->
</div>

@stop