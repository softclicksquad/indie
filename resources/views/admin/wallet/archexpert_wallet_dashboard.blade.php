@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<?php
$user = Sentinel::check();
if(!$user){ return redirect('/admin/login'); }    
?>

<style type="text/css">
  .wallet-dropdwon-content-main{background: #ffffff;color: #000000;position: absolute;top: 37px;width: 90px;right: 10px;z-index: 9;transform: translateY(-15px);-webkit-transform: translateY(-15px);-moz-transform: translateY(-15px);-ms-transform: translateY(-15px);-o-transform: translateY(-15px);opacity: 0;visibility: hidden;transition: 0.5s}
  .wallet-dropdwon-content{height: 200px;overflow: auto;}
  .wallet-dropdwon-content-main li,.wallet-dropdwon-content-main ul{list-style-type: none;padding: 0;margin: 0}
  .arrow-up-mn {position: absolute;top: -16px;right: 11px;}
  .wallet-dropdwon-content-main li{padding: 5px 10px;border-bottom: 1px solid #dddddd;cursor: pointer;}
  .dropdwon-open .wallet-dropdwon-content-main{transform: translateY(0);-webkit-transform: translateY(0);-moz-transform: translateY(0);-ms-transform: translateY(0);-o-transform: translateY(0);opacity: 1;visibility: visible;transition: 0.5s}
  .user-details-section{display: none}
  .wallet-1-open .wallet-1,
  .wallet-2-open .wallet-2,
  .wallet-3-open .wallet-3,
  .wallet-4-open .wallet-4,
  .wallet-5-open .wallet-5,
  .wallet-6-open .wallet-6,
  .wallet-7-open .wallet-7,
  .wallet-8-open .wallet-8,
  .wallet-9-open .wallet-9,
  .wallet-10-open .wallet-10,
  .wallet-11-open .wallet-11,
  .wallet-12-open .wallet-12,
  .wallet-13-open .wallet-13,
  .wallet-14-open .wallet-14,
  .wallet-1-open .table-1,
  .wallet-2-open .table-2,
  .wallet-3-open .table-3,
  .wallet-4-open .table-4,
  .wallet-5-open .table-5,
  .wallet-6-open .table-6,
  .wallet-7-open .table-7,
  .wallet-8-open .table-8,
  .wallet-9-open .table-9,
  .wallet-10-open .table-10,
  .wallet-11-open .table-11,
  .wallet-12-open .table-12,
  .wallet-13-open .table-13,
  .wallet-14-open .table-14{display: block;}
  

</style>


<!-- BEGIN Content -->
<div id="main-content">
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{ url('/'.$admin_panel_slug.'/dashboard') }}">Dashboard</a>
        </li>
        <span class="divider">
            <i class="fa fa-angle-right"></i>
        </span>
        <li class="active">  {{ isset($page_title)?$page_title:"" }}</li>
    </ul>
</div>
<!-- END Breadcrumb -->
    <!-- BEGIN Tiles -->
        <div class="row">
            <div class="col-md-12">          
            @include('front.layout._operation_status')
            </div>
            <!-- section 1 -->
            <div class="col-md-6">
                <div class="row">
                  @if(isset($mp_wallet_created) && $mp_wallet_created == 'Yes')
                    <!--Section 1: User Details -->
                          <div class="col-md-6">
                              <div class="tile tile-violet">
                                  <div class="img">
                                      <i class="fa fa-user"></i>
                                  </div>
                                  <div class="content">
                                      <div class="user-details-section">
                                          <p>
                                            <b>{{trans('common/wallet/text.text_user_id')}}</b> <span>{{ isset($mango_user_detail->Id)? $mango_user_detail->Id:''}}</span>
                                            <br>
                                            <b>{{trans('common/wallet/text.text_email')}}</b> <span>{{ isset($mango_user_detail->Email)? $mango_user_detail->Email:''}}</span>
                                          </p>
                                      </div>
                                  </div>
                              </div>
                          </div>
                    <!--User Details -->

                    <!--Section 2: Wallet Details -->
                    
                        <div class="col-md-6">
                            <div class="tile tile-violet" style="overflow: unset;">
                                <div class="dash-user-details">
                                  <div class="title" style="float: left;"><i class="fa fa-money" aria-hidden="true"></i> {{trans('common/wallet/text.text_wallet')}} </div>
                                  <div class="wallet-dropdwon-section" style="float: right;">
                                    <div class="wallet-dropdwon-head">
                                        Wallet Currency <i class="fa fa-angle-down"></i>
                                    </div>
                                    <div class="wallet-dropdwon-content-main">
                                        <div class="arrow-up-mn"> <img src="{{url('/public')}}/front/images/arrows-tp.png" alt=""></div>
                                        <div class="wallet-dropdwon-content">
                                        <ul>
                                          <li class="currency-li currency1"> USD</li>
                                          <li class="currency-li currency2"> EUR</li>
                                          <li class="currency-li currency3"> GBP</li>
                                          <li class="currency-li currency4"> PLN</li>
                                          <li class="currency-li currency5"> CHF</li>
                                          <li class="currency-li currency6"> NOK</li>
                                          <li class="currency-li currency7"> SEK</li>
                                          <li class="currency-li currency8"> DKK</li>
                                          <li class="currency-li currency9"> CAD</li>
                                          <li class="currency-li currency10"> ZAR</li>
                                          <li class="currency-li currency11"> AUD</li>
                                          <li class="currency-li currency12"> HKD</li>
                                          <li class="currency-li currency13"> CZK</li>
                                          <li class="currency-li currency14"> JPY</li>
                                        </ul>
                                    </div>
                                    </div>
                                </div> 
                                <div class="clearfix"></div> 
                                @if(isset($mangopay_wallet_details) && $mangopay_wallet_details !="" && !empty($mangopay_wallet_details))
                                @foreach($mangopay_wallet_details as $key=>$value)              
                                  <div class="user-details-section wallet-{{$key+1}}">
                                      <input type="hidden" name="wallet_amout" id="wallet_amout" value="{{ isset($value->Balance->Amount)? $value->Balance->Amount/100:'0'}}">
                                      <table class="table" style="margin-bottom:15px">
                                        <tbody>     
                                          <tr>
                                            <td>
                                              <span class="card-id">{{ isset($value->Id)? $value->Id:''}} </span>
                                            </td>
                                            <td>
                                              <div class="description" style="font-size: 11px;">{{isset($value->Description)? $value->Description:''}}</div>
                                            </td>
                                            <td style="font-size: 17px;width: 120px;">
                                              <div class="high-text-up">

                                                <span class="mp-amount">
                                                  {{ isset($value->Balance->Amount)? $value->Balance->Amount/100:'0'}}
                                                </span>
                                                <span>
                                                  {{ isset($value->Balance->Currency)? $value->Balance->Currency:''}}
                                                </span>
                                              </div>
                                              <div class="small-text-down" style="font-size: 10px;">
                                                {{trans('common/wallet/text.text_money_balance')}} 
                                              </div>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                  </div>
                              @endforeach
                              @endif              
                              </div>
                            </div>
                        </div>
                    

                    <!-- End Wallet Details-->
                    <div class="clearfix"></div>
                    <hr style="margin-top: 2px;margin-bottom: 10px; border-color: black; width: 96%;">
                    <!--Section 5: Transactions Details -->
                    <div class="col-md-12">
                      <div class="dash-user-details">
                        <div class="title">
                          <i class="fa fa-credit-card" aria-hidden="true"></i> {{trans('common/wallet/text.text_recent_transactions')}}
                          <div class="col-sm-2 col-md-2 col-lg-2 pull-right">
                            <form id="mp-transaction-link-page" action="{{url('/admin/wallet/archexpert')}}" method="get">
                              <!-- {{csrf_field()}} -->
                              <select  name="page_cnt" class="select-box form-control" id="page_cnt" style="font-size: smaller;">
                                  @if(isset($_REQUEST['page_cnt']) && $_REQUEST['page_cnt'] != "")
                                    @php $active_page = $_REQUEST['page_cnt']; @endphp
                                  @else
                                    @php $active_page = $mangopay_transaction_pagi_links_count; @endphp
                                  @endif
                                  @for($i=1;$i <= $mangopay_transaction_pagi_links_count;$i++)
                                  @php $nxt = ($i*config('app.project.pagi_cnt')); @endphp
                                  <option value="{{$i}}" @if($i == $active_page) selected="selected" @endif>{{$nxt-config('app.project.pagi_cnt')}} - {{$nxt}}</option>
                                  @endfor
                              </select>
                            </form>
                          </div>
                        </div>
                          @if(isset($mangopay_transaction_details) && count($mangopay_transaction_details)>0 !="" && !empty($mangopay_transaction_details))
                          @foreach($mangopay_transaction_details as $indexOf=>$value)
                         
                        <div class="user-details-section table-{{$indexOf+1}}">
                        <table id="TaBle" class="theme-table invoice-table-s table" style="border: 1px solid rgb(239, 239, 239); margin-bottom:0;">
                         <thead class="tras-client-tbl">
                            <tr>
                               <th>{{trans('common/wallet/text.text_creation')}}</th>
                               <th>{{trans('common/wallet/text.text_id')}}</th>
                               <th>{{trans('common/wallet/text.text_type')}}</th>
                               <th>Debited Amount</th>
                               <th>Credited Amount</th>
                               <th>{{trans('common/wallet/text.text_status')}}</th>
                               <th>{{trans('common/wallet/text.text_result')}}</th>
                            </tr>
                         </thead>
                         <tbody>

                            @if(isset($value) && count($value)>0)
                              @if(isset($value[$indexOf]) && $value[$indexOf]!=false)
                              @foreach($value as $transaction)
                                <tr>
                                   <td>
                                       {{date('Y-m-d',$transaction->CreationDate)}} <br> 
                                       {{date('h:i:s',$transaction->CreationDate)}} <br>
                                       {{date('a',$transaction->CreationDate)}}
                                   </td>
                                   <td>{{$transaction->Id or '-'}}</td>
                                   <td width="15%">
                                    {{$transaction->Type or '-'}}
                                    @if(isset($transaction->Type) && $transaction->Type == 'TRANSFER' && isset($transaction->CreditedWalletId) && $transaction->CreditedWalletId == $mangopay_wallet_details[$indexOf]->Id)
                                    <br><i class="fa fa-sign-in" style="color:green;" aria-hidden="true"> Credited</i> 
                                    @elseif(isset($transaction->Type) && $transaction->Type == 'TRANSFER' && isset($transaction->DebitedWalletId) && $transaction->DebitedWalletId == $mangopay_wallet_details[$indexOf]->Id)
                                    <br><i class="fa fa-sign-out"style="color:red;" aria-hidden="true"> Debited</i> 
                                    @endif
                                   </td>
                                   <td><span @if($transaction->Status == 'SUCCEEDED') style="color:green" @else style="color:red" @endif>{{ isset($transaction->DebitedFunds->Amount)? $transaction->DebitedFunds->Amount/100:'0'}}</span> <b>{{$transaction->DebitedFunds->Currency or 'USD'}}</b> <br>incl. {{ isset($transaction->Fees->Amount)? $transaction->Fees->Amount/100:'0'}} fees </td>
                                   <td><span @if($transaction->Status == 'SUCCEEDED') style="color:green" @else style="color:red" @endif>{{ isset($transaction->CreditedFunds->Amount)? $transaction->CreditedFunds->Amount/100:'0'}}</span> <b>{{$transaction->DebitedFunds->Currency or 'USD'}}</b></td>
                                   <td @if($transaction->Status == 'SUCCEEDED') style="color:green" @else style="color:red" @endif>{{$transaction->Status or '-'}}</td>
                                   <td width="30%">{{$transaction->ResultCode or '-'}} :{{$transaction->ResultMessage or '-'}}  <br><i><b>Tag: {{$transaction->Tag or '-'}}<b></i></td>
                                </tr>
                              @endforeach
                              @endif
                            @else
                            <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;" align="center">
                              <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                                 <div class="search-content-block">
                                    <div class="no-record">
                                        {{ trans('expert/transactions/packs.text_sorry_no_records_found')}}                           
                                      </div>
                                    </div>
                                 </td>
                             </tr>
                            @endif
                         </tbody>
                      </table>
                      </div>
                      @endforeach
                    @else
                      -- {{trans('common/wallet/text.text_transactions_note')}} --
                    @endif 
                    </div>
                    </div>
                    <!--End Transactions Details -->

                  @else
                    <a href="{{url('/admin/wallet/create_user')}}">
                        <div class="col-md-6">
                            <div class="tile tile-violet">
                                <div class="img">
                                    <img style="width: 67px;height: 56px;margin-left:3px;" src="{{url('/public')}}/front/images/archexpertdefault/wallet.png" alt="mangopay">
                                </div>
                                <div class="content">
                                    <p class="big"><i class="fa fa-plus" aria-hidden="true"></i></p>
                                    <p class="title">Create Account</p>
                                </div>
                            </div>
                        </div>
                    </a>
                  @endif  
                </div>
            </div>

            <!-- section 2 -->
            <!--Section 4: Bank Details -->
            <div class="col-md-3">
                <div class="row">
                  @if(isset($mp_wallet_created) && $mp_wallet_created == 'Yes')
                    <!--Section 4: Bank Details -->
                        @if(isset($mangopay_bank_details) && $mangopay_bank_details !="" && !empty($mangopay_bank_details))
                          @foreach($mangopay_bank_details as $bankkey => $bank)
                          <div class="col-md-12" >
                          <div class="tile tile-violet" style="height: auto;">
                          <div class="dash-user-details">
                            @if($bankkey == 0)
                                <div class="title">
                                   <a href="#add_bank" class="pull-right" data-keyboard="false" data-backdrop="static" data-toggle="modal"> <i class="fa fa-plus-circle" style="color:white;" aria-hidden="true"></i> </a> 
                                   <i class="fa fa-university" aria-hidden="true"></i> {{trans('common/wallet/text.text_bank_accounts')}} 
                                </div>
                            <hr style="margin-top: 2px;margin-bottom: 10px;">
                            @endif
                              <div class="bnk">
                                  <span class="pull-right" ><a href="#cashout" data-keyboard="false" style="color:#e9f0f9;" class="cashout-model" cashout-bank-id="{{ isset($bank->Id)? $bank->Id:''}}" data-backdrop="static" data-toggle="modal"><i class="fa fa-money" aria-hidden="true"> {{trans('common/wallet/text.text_payout')}} </a></i></span>
                                  <span>{{ isset($bank->Id)? $bank->Id:''}}</span> 
                                  <div class="clearfix"></div>
                                  <span> <small> {{trans('common/wallet/text.text_bank_type')}} </small> <small class="pull-right">{{ isset($bank->Type)? $bank->Type:''}}</small></span>
                                  <div class="clearfix"></div>
                                  <span> <small> {{trans('common/wallet/text.text_owner')}} </small>  <small class="pull-right">{{ isset($bank->OwnerName)? $bank->OwnerName:''}}</small></span>
                                  @if($bank->Type == 'IBAN') 
                                    <div class="clearfix"></div>
                                    <span> <small> IBAN </small> <small class="pull-right">{{ isset($bank->Details->IBAN)? $bank->Details->IBAN:''}}</small></span>
                                    <div class="clearfix"></div>
                                    <span> <small> BIC </small> <small class="pull-right">{{ isset($bank->Details->BIC)? $bank->Details->BIC:''}}</small></span>
                                  @endif
                                  @if($bank->Type == 'GB') 
                                    <div class="clearfix"></div>
                                    <span> <small> {{trans('common/wallet/text.text_account_number')}} </small> <small class="pull-right">{{ isset($bank->Details->AccountNumber)? $bank->Details->AccountNumber:''}}</small></span>
                                    <div class="clearfix"></div>
                                    <span> <small> {{trans('common/wallet/text.text_sort_code')}} </small> <small class="pull-right">{{ isset($bank->Details->SortCode)? $bank->Details->SortCode:''}}</small></span>
                                  @endif
                                  @if($bank->Type == 'US') 
                                    <div class="clearfix"></div>
                                    <span> <small> {{trans('common/wallet/text.text_account_number')}} </small> <small class="pull-right">{{ isset($bank->Details->AccountNumber)? $bank->Details->AccountNumber:''}}</small></span>
                                    <div class="clearfix"></div>
                                    <span> <small> ABA </small> <small class="pull-right">{{ isset($bank->Details->ABA)? $bank->Details->ABA:''}}</small></span>
                                    <div class="clearfix"></div>
                                    <span> <small> {{trans('common/wallet/text.text_deposit_coount_type')}} </small>  <small class="pull-right">{{ isset($bank->Details->DepositAccountType)? $bank->Details->DepositAccountType:''}}</small></span>
                                  @endif
                                  @if($bank->Type == 'CA') 
                                    <div class="clearfix"></div>
                                    <span> BANK NAME  <small class="pull-right">{{ isset($bank->Details->BankName)? $bank->Details->BankName:''}}</small></span>
                                    <div class="clearfix"></div>
                                    <span> INSTITUTION NUMBER  <small class="pull-right">{{ isset($bank->Details->InstitutionNumber)? $bank->Details->InstitutionNumber:''}}</small></span>
                                    <div class="clearfix"></div>
                                    <span> {{trans('common/wallet/text.text_branch_code')}}  <small class="pull-right">{{ isset($bank->Details->BranchCode)? $bank->Details->BranchCode:''}}</small></span>
                                    <div class="clearfix"></div>
                                    <span> {{trans('common/wallet/text.text_account_number')}}  <small class="pull-right">{{ isset($bank->Details->AccountNumber)? $bank->Details->AccountNumber:''}}</small></span>
                                  @endif
                                  @if($bank->Type == 'OTHER') 
                                    <div class="clearfix"></div>
                                    <span> {{trans('common/wallet/text.text_account_number')}}  <small class="pull-right">{{ isset($bank->Details->AccountNumber)? $bank->Details->AccountNumber:''}}</small></span>
                                    <div class="clearfix"></div>
                                    <span> BIC  <small class="pull-right">{{ isset($bank->Details->BIC)? $bank->Details->BIC:''}}</small></span>
                                  @endif
                              </div>
                              </div>
                            </div>

                          </div>
                          <div class="clearfix"></div>
                          @endforeach
                        @else
                          <div class="col-md-12" >
                            <div class="tile tile-violet">
                              <div class="dash-user-details">
                                <div class="title">
                                 <a href="#add_bank" class="pull-right" data-keyboard="false" data-backdrop="static" data-toggle="modal"> <i class="fa fa-plus-circle" style="color:white;" aria-hidden="true"></i> </a> 
                                 <i class="fa fa-university" aria-hidden="true"></i> {{trans('common/wallet/text.text_bank_accounts')}} 
                                </div>
                              </div>
                              <hr style="margin-top: 2px;margin-bottom: 10px;">
                              <span><i>-- {{trans('common/wallet/text.text_bank_account_note')}} --</span></i>
                            </div>
                          </div>
                        @endif
                      <!--End Bank Details -->
                  @endif  
                </div>
            </div>
            <!--End Bank Details -->

            <!-- section 3 -->
            <!--Section 3: Kyc Details -->
            <div class="col-md-3">
                <div class="row">
                  @if(isset($mp_wallet_created) && $mp_wallet_created == 'Yes')
                      @if(isset($mangopay_kyc_details) && $mangopay_kyc_details !="" && !empty($mangopay_kyc_details))
                          <div class="col-md-12" >
                            <div class="tile tile-violet">
                              <div class="dash-user-details">
                                <div class="title">
                                 <a href="#upload_kyc_docs" class="pull-right" data-keyboard="false" data-backdrop="static" data-toggle="modal"> <i class="fa fa-plus-circle" style="color:white;" aria-hidden="true"></i> </a> 
                                 <i class="fa fa-file-text" aria-hidden="true"></i> {{trans('common/wallet/text.text_kyc_documents')}}
                                </div>
                              </div>
                              <hr style="margin-top: 2px;margin-bottom: 10px;">
                              @foreach($mangopay_kyc_details as $kyckey => $documents)
                                <span>{{ isset($documents->Id)? $documents->Id:''}}-{{ isset($documents->Type)? $documents->Type:''}} 
                                    @if(isset($documents->Status) && $documents->Status == "VALIDATED") 
                                    <small class="pull-right" style="color:#16864A;" >{{ isset($documents->Status)? $documents->Status:''}} </small>
                                    @elseif(isset($documents->Status) && $documents->Status == "VALIDATION_ASKED")
                                    <small class="pull-right" style="color:#FFC300;" >{{ isset($documents->Status)? $documents->Status:''}} </small>
                                    @elseif(isset($documents->Status) && $documents->Status == "REFUSED")
                                    <small class="pull-right" style="color:#CD1D35;" >{{ isset($documents->Status)? $documents->Status:''}} 
                                      <br>
                                      <small style="color:grey;"><i> {{ isset($documents->RefusedReasonType)? $documents->RefusedReasonType:''}} </i> </small>
                                    </small>
                                    @endif 
                                </span>
                                <div class="clearfix"></div>
                                <!-- Type / Status / UserId / RefusedReasonType / RefusedReasonMessage / ProcessedDate / Id / Tag / CreationDate -->
                              @endforeach
                            </div>
                          </div>
                      @else
                        <div class="col-md-12" >
                          <div class="tile tile-violet">
                            <div class="dash-user-details">
                              <div class="title">
                               <a href="#upload_kyc_docs" class="pull-right" data-keyboard="false" data-backdrop="static" data-toggle="modal"> <i class="fa fa-plus-circle" style="color:white;" aria-hidden="true"></i> </a> 
                               <i class="fa fa-file-text" aria-hidden="true"></i> {{trans('common/wallet/text.text_kyc_documents')}}
                              </div>
                            </div>
                            <hr style="margin-top: 2px;margin-bottom: 10px;">
                            <span><i>-- {{trans('common/wallet/text.text_kyc_documents_note')}} --</span></i>
                          </div>
                        </div>
                      @endif  
                  @endif  
                </div>
            </div>
            <!--End Kyc Details -->
        </div>
    <!-- END Tiles -->
</div>


<!-- add bank model -->
<div class="modal fade invite-member-modal" id="add_bank" role="dialog">
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <h4>{{trans('common/wallet/text.text_new_bank_account')}}</h4>
             </div>
             <form action="{{url('/admin/wallet/add_bank')}}" method="post" class="form-horizontal" id="mp-add-bank-acc-form">
               {{csrf_field()}}
               <div class="invite-form">
                    <div class="user-box">
                      <select class="clint-input" id="bank_type" name="bank_type" data-rule-required="true" class="form-control">
                        <option value="IBAN">IBAN</option>
                        <option value="GB">GB</option>
                        <option value="US">US</option>
                        <option value="CA">CA</option>
                        <option value="OTHER">OTHER</option>
                      </select>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input space_restrict" data-rule-required="true" name="FirstName" id="FirstName" placeholder="{{trans('common/wallet/text.text_first_name')}}">
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input space_restrict" data-rule-required="true" name="LastName" id="LastName" placeholder="{{trans('common/wallet/text.text_last_name')}}">
                       </div>
                    </div>
                    <div class="user-box">
                           <div class="input-name">
                              <textarea type="text" class="clint-input" data-rule-required="true" name="Address" id="Address" placeholder="{{trans('common/wallet/text.text_enter_address')}}"></textarea>
                           </div>
                        </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input space_restrict" data-rule-required="true" name="City" id="City" placeholder="{{trans('common/wallet/text.text_city')}}">
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input space_restrict" data-rule-required="true" name="PostalCode" id="PostalCode" placeholder="{{trans('common/wallet/text.text_PostalCode')}}">
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input space_restrict" data-rule-required="true" name="Region" id="Region" placeholder="{{trans('common/wallet/text.text_region')}}">
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <select class="clint-input space_restrict" id="Country" name="Country" data-rule-required="true">
                            <option value="">-- {{trans('common/wallet/text.text_please_select_country')}} --</option>
                            <option value="AD">AD</option>
                            <option value="AE">AE</option>
                            <option value="AF">AF</option>
                            <option value="AG">AG</option>
                            <option value="AI">AI</option>
                            <option value="AL">AL</option>
                            <option value="AM">AM</option>
                            <option value="AN">AN</option>
                            <option value="AO">AO</option>
                            <option value="AQ">AQ</option>
                            <option value="AR">AR</option>
                            <option value="AS">AS</option>
                            <option value="AT">AT</option>
                            <option value="AU">AU</option>
                            <option value="AW">AW</option>
                            <option value="AX">AX</option>
                            <option value="AZ">AZ</option>
                            <option value="BA">BA</option>
                            <option value="BB">BB</option>
                            <option value="BD">BD</option>
                            <option value="BE">BE</option>
                            <option value="BF">BF</option>
                            <option value="BG">BG</option>
                            <option value="BH">BH</option>
                            <option value="BI">BI</option>
                            <option value="BJ">BJ</option>
                            <option value="BL">BL</option>
                            <option value="BM">BM</option>
                            <option value="BN">BN</option>
                            <option value="BO">BO</option>
                            <option value="BQ">BQ</option>
                            <option value="BR">BR</option>
                            <option value="BS">BS</option>
                            <option value="BT">BT</option>
                            <option value="BV">BV</option>
                            <option value="BW">BW</option>
                            <option value="BY">BY</option>
                            <option value="BZ">BZ</option>
                            <option value="CA">CA</option>
                            <option value="CC">CC</option>
                            <option value="CD">CD</option>
                            <option value="CF">CF</option>
                            <option value="CG">CG</option>
                            <option value="CH">CH</option>
                            <option value="CI">CI</option>
                            <option value="CK">CK</option>
                            <option value="CL">CL</option>
                            <option value="CM">CM</option>
                            <option value="CN">CN</option>
                            <option value="CO">CO</option>
                            <option value="CR">CR</option>
                            <option value="CU">CU</option>
                            <option value="CV">CV</option>
                            <option value="CW">CW</option>
                            <option value="CX">CX</option>
                            <option value="CY">CY</option>
                            <option value="CZ">CZ</option>
                            <option value="DE">DE</option>
                            <option value="DJ">DJ</option>
                            <option value="DK">DK</option>
                            <option value="DM">DM</option>
                            <option value="DO">DO</option>
                            <option value="DZ">DZ</option>
                            <option value="EC">EC</option>
                            <option value="EE">EE</option>
                            <option value="EG">EG</option>
                            <option value="EH">EH</option>
                            <option value="ER">ER</option>
                            <option value="ES">ES</option>
                            <option value="ET">ET</option>
                            <option value="FI">FI</option>
                            <option value="FJ">FJ</option>
                            <option value="FK">FK</option>
                            <option value="FM">FM</option>
                            <option value="FO">FO</option>
                            <option value="FR">FR</option>
                            <option value="GA">GA</option>
                            <option value="GB">GB</option>
                            <option value="GD">GD</option>
                            <option value="GE">GE</option>
                            <option value="GF">GF</option>
                            <option value="GG">GG</option>
                            <option value="GH">GH</option>
                            <option value="GI">GI</option>
                            <option value="GL">GL</option>
                            <option value="GM">GM</option>
                            <option value="GN">GN</option>
                            <option value="GP">GP</option>
                            <option value="GQ">GQ</option>
                            <option value="GR">GR</option>
                            <option value="GS">GS</option>
                            <option value="GT">GT</option>
                            <option value="GU">GU</option>
                            <option value="GW">GW</option>
                            <option value="GY">GY</option>
                            <option value="HK">HK</option>
                            <option value="HM">HM</option>
                            <option value="HN">HN</option>
                            <option value="HR">HR</option>
                            <option value="HT">HT</option>
                            <option value="HU">HU</option>
                            <option value="ID">ID</option>
                            <option value="IE">IE</option>
                            <option value="IL">IL</option>
                            <option value="IM">IM</option>
                            <option value="IN">IN</option>
                            <option value="IO">IO</option>
                            <option value="IQ">IQ</option>
                            <option value="IR">IR</option>
                            <option value="IS">IS</option>
                            <option value="IT">IT</option>
                            <option value="JE">JE</option>
                            <option value="JM">JM</option>
                            <option value="JO">JO</option>
                            <option value="JP">JP</option>
                            <option value="KE">KE</option>
                            <option value="KG">KG</option>
                            <option value="KH">KH</option>
                            <option value="KI">KI</option>
                            <option value="KM">KM</option>
                            <option value="KN">KN</option>
                            <option value="KP">KP</option>
                            <option value="KR">KR</option>
                            <option value="KW">KW</option>
                            <option value="KY">KY</option>
                            <option value="KZ">KZ</option>
                            <option value="LA">LA</option>
                            <option value="LB">LB</option>
                            <option value="LC">LC</option>
                            <option value="LI">LI</option>
                            <option value="LK">LK</option>
                            <option value="LR">LR</option>
                            <option value="LS">LS</option>
                            <option value="LT">LT</option>
                            <option value="LU">LU</option>
                            <option value="LV">LV</option>
                            <option value="LY">LY</option>
                            <option value="MA">MA</option>
                            <option value="MC">MC</option>
                            <option value="MD">MD</option>
                            <option value="ME">ME</option>
                            <option value="MF">MF</option>
                            <option value="MG">MG</option>
                            <option value="MH">MH</option>
                            <option value="MK">MK</option>
                            <option value="ML">ML</option>
                            <option value="MM">MM</option>
                            <option value="MN">MN</option>
                            <option value="MO">MO</option>
                            <option value="MP">MP</option>
                            <option value="MQ">MQ</option>
                            <option value="MR">MR</option>
                            <option value="MS">MS</option>
                            <option value="MT">MT</option>
                            <option value="MU">MU</option>
                            <option value="MV">MV</option>
                            <option value="MW">MW</option>
                            <option value="MX">MX</option>
                            <option value="MY">MY</option>
                            <option value="MZ">MZ</option>
                            <option value="NA">NA</option>
                            <option value="NC">NC</option>
                            <option value="NE">NE</option>
                            <option value="NF">NF</option>
                            <option value="NG">NG</option>
                            <option value="NI">NI</option>
                            <option value="NL">NL</option>
                            <option value="NO">NO</option>
                            <option value="NP">NP</option>
                            <option value="NR">NR</option>
                            <option value="NU">NU</option>
                            <option value="NZ">NZ</option>
                            <option value="OM">OM</option>
                            <option value="PA">PA</option>
                            <option value="PE">PE</option>
                            <option value="PF">PF</option>
                            <option value="PG">PG</option>
                            <option value="PH">PH</option>
                            <option value="PK">PK</option>
                            <option value="PL">PL</option>
                            <option value="PM">PM</option>
                            <option value="PN">PN</option>
                            <option value="PR">PR</option>
                            <option value="PS">PS</option>
                            <option value="PT">PT</option>
                            <option value="PW">PW</option>
                            <option value="PY">PY</option>
                            <option value="QA">QA</option>
                            <option value="RE">RE</option>
                            <option value="RO">RO</option>
                            <option value="RS">RS</option>
                            <option value="RU">RU</option>
                            <option value="RW">RW</option>
                            <option value="SA">SA</option>
                            <option value="SB">SB</option>
                            <option value="SC">SC</option>
                            <option value="SD">SD</option>
                            <option value="SE">SE</option>
                            <option value="SG">SG</option>
                            <option value="SH">SH</option>
                            <option value="SI">SI</option>
                            <option value="SJ">SJ</option>
                            <option value="SK">SK</option>
                            <option value="SL">SL</option>
                            <option value="SM">SM</option>
                            <option value="SN">SN</option>
                            <option value="SO">SO</option>
                            <option value="SR">SR</option>
                            <option value="SS">SS</option>
                            <option value="ST">ST</option>
                            <option value="SV">SV</option>
                            <option value="SX">SX</option>
                            <option value="SY">SY</option>
                            <option value="SZ">SZ</option>
                            <option value="TC">TC</option>
                            <option value="TD">TD</option>
                            <option value="TF">TF</option>
                            <option value="TG">TG</option>
                            <option value="TH">TH</option>
                            <option value="TJ">TJ</option>
                            <option value="TK">TK</option>
                            <option value="TL">TL</option>
                            <option value="TM">TM</option>
                            <option value="TN">TN</option>
                            <option value="TO">TO</option>
                            <option value="TR">TR</option>
                            <option value="TT">TT</option>
                            <option value="TV">TV</option>
                            <option value="TW">TW</option>
                            <option value="TZ">TZ</option>
                            <option value="UA">UA</option>
                            <option value="UG">UG</option>
                            <option value="UM">UM</option>
                            <option value="US">US</option>
                            <option value="UY">UY</option>
                            <option value="UZ">UZ</option>
                            <option value="VA">VA</option>
                            <option value="VC">VC</option>
                            <option value="VE">VE</option>
                            <option value="VG">VG</option>
                            <option value="VI">VI</option>
                            <option value="VN">VN</option>
                            <option value="VU">VU</option>
                            <option value="WF">WF</option>
                            <option value="WS">WS</option>
                            <option value="YE">YE</option>
                            <option value="YT">YT</option>
                            <option value="ZA">ZA</option>
                            <option value="ZM">ZM</option>
                            <option value="ZW">ZW</option>
                          </select>
                       </div>
                    </div>
                    
                    <!-- for-iban-account -->
                    <div id="for-iban-account">
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="IBAN" id="IBAN" placeholder="{{trans('common/wallet/text.text_enter_iban')}}">
                           </div>
                        </div>
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="BIC" id="BIC" placeholder="{{trans('common/wallet/text.text_enter_bic')}}">
                           </div>
                        </div>
                    </div>  
                    
                    <!-- for-gb-account -->
                    <div id="for-gb-account" style="display:none;">
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="gbSortCode" id="gbSortCode" placeholder="{{trans('common/wallet/text.text_enter_sort_code')}}">
                           </div>
                        </div>
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="gbAccountNumber" id="gbAccountNumber" placeholder="{{trans('common/wallet/text.text_enter_account_number')}}">
                           </div>
                        </div>
                    </div>  

                    <!-- for-us-account -->
                    <div id="for-us-account" style="display:none;">
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="usAccountNumber" id="usAccountNumber" placeholder="{{trans('common/wallet/text.text_enter_account_number')}}">
                           </div>
                        </div>
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="usABA" id="usABA" placeholder="{{trans('common/wallet/text.text_enter_aba')}}">
                           </div>
                        </div>
                        <div class="user-box">
                          <div class="input-name">
                            <select class="clint-input" id="usDepositAccountType" name="usDepositAccountType">
                              <option value="CHECKING">CHECKING</option>
                              <option value="SAVINGS">SAVINGS</option>
                            </select>
                          </div>
                        </div>
                    </div>

                    <!-- for-ca-account -->
                    <div id="for-ca-account" style="display:none;"> 
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="caAccountNumber" id="caAccountNumber" placeholder="{{trans('common/wallet/text.text_enter_account_number')}}">
                           </div>
                        </div> 
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="caBranchCode" id="caBranchCode" placeholder="{{trans('common/wallet/text.text_enter_branch_code')}}">
                           </div>
                        </div>
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input" data-rule-required="true" name="caBankName" id="caBankName" placeholder="{{trans('common/wallet/text.text_enter_bank_name')}}">
                           </div>
                        </div>
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="caInstitutionNumber" id="caInstitutionNumber" placeholder="{{trans('common/wallet/text.text_enter_institution_number')}}">
                           </div>
                        </div>
                    </div>

                    <!-- for-other-account -->
                    <div id="for-other-account" style="display:none;"> 
                      <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="otherAccountNumber" id="otherAccountNumber" placeholder="{{trans('common/wallet/text.text_enter_account_number')}}">
                           </div>
                      </div>
                      <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="otherBIC" id="otherBIC" placeholder="{{trans('common/wallet/text.text_enter_bic')}}">
                           </div>
                      </div>
                    </div>  
                <button type="submit" id="mp-add-bank" class="black-btn">{{trans('common/wallet/text.text_add_bank')}}</button>
               </div>
             </form>
          </div>
       </div>
    </div>
</div>
<!-- end add bank model -->

<!-- add kyc model -->
<div class="modal fade invite-member-modal" id="upload_kyc_docs" role="dialog" >
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <h4>{{trans('common/wallet/text.text_kyc_documents_upload')}}</h4>
             </div>
             <form action="{{url('/admin/wallet/upload_kyc_docs')}}" method="post" id="mp-upload-document-form" enctype="multipart/form-data">
               {{csrf_field()}}
               <div class="invite-form">
                    <div class="user-box">
                       <div class="input-name">
                          <select class="clint-input space_restrict" id="kyc_upload_documents_natural_types" name="kyc_upload_documents_natural_types" data-rule-required="true">
                            <option value="IDENTITY_PROOF">{{trans('common/wallet/text.text_identity_proof')}}</option>
                            <option value="ADDRESS_PROOF">{{trans('common/wallet/text.text_address_proof')}}</option>
                          </select>
                       </div>
                    </div>
                    {{trans('common/wallet/text.text_accepted_for_the_documents')}}
                    <div class="user-box">
                       <div class="input-name">
                          <input type="file" class="clint-input" name="Kyc_doc" id="Kyc_doc" placeholder="Select document" data-rule-required="true">
                          <span class="error kyc_doc_file_err"></span>
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <textarea type="text" class="clint-input" name="Kyc_Tag" id="Kyc_Tag" placeholder="{{trans('common/wallet/text.text_enter_tag')}}"></textarea>
                       </div>
                     </div>
                    <button type="submit" id="mp-upload-document" class="black-btn">{{trans('common/wallet/text.text_upload_document')}}</button>
               </div>
             </form>
          </div>
       </div>
    </div>
</div>
<!-- end add kyc model -->

<!-- cashout model -->
<div class="modal fade invite-member-modal" id="cashout" role="dialog">
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <h4>{{trans('common/wallet/text.text_payout')}}</h4>
             </div>
             <form action="{{url('/admin/wallet/cashout_in_bank')}}" method="post" id="mp-cashout-form">
                {{csrf_field()}}
                <input type="hidden" value="0" class="clint-input char_restrict space_restrict" data-rule-required="true" name="act_cashout_amount" id="act_cashout_amount" placeholder="act cashout amout">
                <input type="hidden" value="{{ isset($mangopay_wallet_details->Id)? $mangopay_wallet_details->Id:''}}" class="clint-input char_restrict space_restrict" data-rule-required="true" name="walletId" id="walletId" placeholder="walletId">
                <input type="hidden" value="{{ isset($bank->Id)? $bank->Id:''}}" class="clint-input char_restrict space_restrict" data-rule-required="true" name="bankId" id="bankId" placeholder="bankId">
                
                <div class="invite-form">
                    <span>{{trans('common/wallet/text.text_bank')}}              : <span class="cashout-bank-id"> {{ isset($bank->Id)? $bank->Id:''}} </span> </span>
                    <div class="clearfix"></div>
                    <span>{{trans('common/wallet/text.text_available_balance')}} : {{ isset($mangopay_wallet_details->Balance->Amount)? $mangopay_wallet_details->Balance->Amount/100:'0'}} {{ isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:''}}</span>
                    <div class="clearfix"></div>
                    <hr>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input char_restrict space_restrict" data-rule-required="true" name="cashout_amount" id="cashout_amount" placeholder="{{trans('common/wallet/text.text_enter_amount_in')}} {{config('app.project_currency.$')}}">
                          <span class="error" id="cashout_amount_err"></span>
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <textarea type="text" class="clint-input" name="Tag" id="Tag" placeholder="{{trans('common/wallet/text.text_enter_tag')}}"></textarea>
                       </div>
                    </div>
                    <button type="submit" id="mp-cashout" class="black-btn">{{trans('common/wallet/text.text_make_payout')}}</button>
                </div>
             </form>
          </div>
       </div>
    </div>
</div>
<!-- end cashout model -->

<script type="text/javascript">
 $(document).ready(function(){
   $('.cashout-model').click(function(){
      var bank_id = $(this).attr('cashout-bank-id');
      $('#bankId').val(bank_id);
      $('.cashout-bank-id').html(bank_id);
   });
   $(document).on('click', '.add-btn', function (e) {
      var check_active = $(this).next('.money-drop').attr('class');
      if (check_active.indexOf("active") >= 0){
        $(this).next('.money-drop').removeClass('active');
      } else {
        $('.money-drop').removeClass('active');
        $(this).next('.money-drop').addClass('active');
      }
   }); 
   $('#cashout_amount').keyup(function(){
      $('#cashout_amount_err').html('');
      var wallet_amout       = $('#wallet_amout').val();
      var act_cashout_amount = $(this).val();
      if(act_cashout_amount == ""){
        act_cashout_amount = 0;
      }
      if(parseFloat(wallet_amout) <= parseFloat(act_cashout_amount)){
        $('#cashout_amount_err').html('{{trans('common/wallet/text.text_please_enter_cashout_amount_less_than_wallet_amount')}}');
        $('#act_cashout_amount').val(wallet_amout);
      } else {
        $('#act_cashout_amount').val(act_cashout_amount);
      } 
   });
   $('#cashout_amount').blur(function(){
      $('#cashout_amount_err').html('');
      var wallet_amout       = $('#wallet_amout').val();
      var act_cashout_amount = $('#cashout_amount').val();
      if(act_cashout_amount == ""){
        act_cashout_amount = 0;
      }
      if(parseFloat(wallet_amout) <= parseFloat(act_cashout_amount)){
        $('#cashout_amount_err').html('{{trans('common/wallet/text.text_please_enter_cashout_amount_less_than_wallet_amount')}}');
        $('#act_cashout_amount').val(wallet_amout);
      } else {
        $('#act_cashout_amount').val(act_cashout_amount);
      } 
   });
   $('#mp-cashout').click(function(){
      $('#cashout_amount_err').html('');
      var wallet_amout       = $('#wallet_amout').val();
      var act_cashout_amount = $('#cashout_amount').val();
      if(act_cashout_amount == ""){
        act_cashout_amount = 0;
      }
      if(parseFloat(wallet_amout) <= parseFloat(act_cashout_amount)){
        $('#cashout_amount_err').html('{{trans('common/wallet/text.text_please_enter_cashout_amount_less_than_wallet_amount')}}');
        $('#act_cashout_amount').val(wallet_amout);
        return false;
      } else {
        $('#act_cashout_amount').val(act_cashout_amount);
        return true;
      }
   });
   
   // for refund 
    $(document).on('click','#refund-module',function(){
      var AuthorId       = $(this).data('authorid');
      var RefundAmount   = $(this).data('refundamount');
      var RefundCurrency = $(this).data('refundcurrency');
      var payinId        = $(this).data('payinid');
      $('#act_refund_amount').val(RefundAmount);
      $('#refund_amount').html(RefundAmount);
      $('#authorId').val(AuthorId);
      $('#currency').val(RefundCurrency);
      $('#refund_amount_currency').html(RefundCurrency);
      $('#author_id_text').html(AuthorId);
      $('#payinId').val(payinId);
    });
 }); 
</script>

<script>
 $(document).ready(function(){
    $("#mp-add-bank-acc-form").validate();
    $("#mp-upload-document-form").validate();
    $("#mp-cashout-form").validate();
    $("#mp-refund-form").validate();
    $(document).on('click','.close',function(){
      $("#mp-add-bank-acc-form")[0].reset();
      $("#mp-upload-document-form")[0].reset();
      $("#mp-cashout-form")[0].reset();
      $("#mp-refund-form")[0].reset();
      $('label.error,.error').html('');
    });
    $(document).on('change','#page_cnt',function(){
      $('#mp-transaction-link-page').submit();
    });
    $("#Kyc_doc").on("change", function () {
       showProcessingOverlay();
       $('.kyc_doc_file_err,.error').html('');
       var file_name       = this.files[0].name;
       var explode         = file_name.split('.');
       var file_extension  = explode['1'];
       var flag = 0;
       if(file_extension != 'pdf'  &&
          file_extension != 'jpeg' &&
          file_extension != 'jpg'  &&
          file_extension != 'gif'  &&
          file_extension != 'png'){
          $('.kyc_doc_file_err').css('color','red');
          $('.kyc_doc_file_err').html('{{trans('common/wallet/text.text_image_should_be')}}');
          flag = 1;
       } 
       if(this.files[0].size > 7000000) {
          $('.kyc_doc_file_err').css('color','red');
          $('.kyc_doc_file_err').html('{{trans('common/wallet/text.text_please_upload_file')}}');
          flag = 1;
       }
       if(flag == 1){
         hideProcessingOverlay(); 
         $(this).val('');
         return false;
       } else {
        hideProcessingOverlay(); 
        return true; 
       }
    }); 
    $("#bank_type").on("change", function () {
      var bank_type = $(this).val();
      if(bank_type == 'IBAN'){
        $('#for-iban-account').show();
        $('#for-gb-account').hide();
        $('#for-us-account').hide();
        $('#for-ca-account').hide();
        $('#for-other-account').hide();
        $("#mp-add-bank-acc-form")[0].reset();
        $('#bank_type option[value=IBAN]').attr('selected','selected');
      } else if(bank_type == 'GB'){
        $('#for-iban-account').hide();
        $('#for-gb-account').show();
        $('#for-us-account').hide();
        $('#for-ca-account').hide();
        $('#for-other-account').hide(); 
        $("#mp-add-bank-acc-form")[0].reset();
        $('#bank_type option[value=GB]').attr('selected','selected');
      } else if(bank_type == 'US'){
        $('#for-iban-account').hide();
        $('#for-gb-account').hide();
        $('#for-us-account').show();
        $('#for-ca-account').hide();
        $('#for-other-account').hide();
        $("#mp-add-bank-acc-form")[0].reset();
        $('#bank_type option[value=US]').attr('selected','selected');
      } else if(bank_type == 'CA'){
        $('#for-iban-account').hide();
        $('#for-gb-account').hide();
        $('#for-us-account').hide();
        $('#for-ca-account').show();
        $('#for-other-account').hide();
        $("#mp-add-bank-acc-form")[0].reset();
        $('#bank_type option[value=CA]').attr('selected','selected');
      } else if(bank_type == 'OTHER'){
        $('#for-iban-account').hide();
        $('#for-gb-account').hide();
        $('#for-us-account').hide();
        $('#for-ca-account').hide();
        $('#for-other-account').show();
        $("#mp-add-bank-acc-form")[0].reset();
        $('#bank_type option[value=OTHER]').attr('selected','selected');
      } 
    });
 });    
</script>


<script type="text/javascript">
  $(".wallet-dropdwon-head").on("click", function(){
    $(".wallet-dropdwon-section").toggleClass("dropdwon-open")
  });
  $(".currency-li").on("click", function(){
    $(".wallet-dropdwon-section").removeClass("dropdwon-open")
  });
  $(".currency1").on("click", function(){
    $("body").addClass("wallet-1-open").removeClass("wallet-2-open wallet-3-open wallet-4-open wallet-5-open wallet-6-open wallet-7-open wallet-8-open wallet-9-open wallet-10-open wallet-11-open wallet-12-open wallet-13-open wallet-14-open")
  });

  $(".currency2").on("click", function(){
    $("body").addClass("wallet-2-open").removeClass("wallet-1-open wallet-3-open wallet-4-open wallet-5-open wallet-6-open wallet-7-open wallet-8-open wallet-9-open wallet-10-open wallet-11-open wallet-12-open wallet-13-open wallet-14-open")
  });

  $(".currency3").on("click", function(){
    $("body").addClass("wallet-3-open").removeClass("wallet-1-open wallet-2-open wallet-4-open wallet-5-open wallet-6-open wallet-7-open wallet-8-open wallet-9-open wallet-10-open wallet-11-open wallet-12-open wallet-13-open wallet-14-open")
  });

  $(".currency4").on("click", function(){
    $("body").addClass("wallet-4-open").removeClass("wallet-2-open wallet-3-open wallet-1-open wallet-5-open wallet-6-open wallet-7-open wallet-8-open wallet-9-open wallet-10-open wallet-11-open wallet-12-open wallet-13-open wallet-14-open")
  });

  $(".currency5").on("click", function(){
    $("body").addClass("wallet-5-open").removeClass("wallet-2-open wallet-3-open wallet-4-open wallet-1-open wallet-6-open wallet-7-open wallet-8-open wallet-9-open wallet-10-open wallet-11-open wallet-12-open wallet-13-open wallet-14-open")
  });

  $(".currency6").on("click", function(){
    $("body").addClass("wallet-6-open").removeClass("wallet-2-open wallet-3-open wallet-4-open wallet-5-open wallet-1-open wallet-7-open wallet-8-open wallet-9-open wallet-10-open wallet-11-open wallet-12-open wallet-13-open wallet-14-open")
  });

  $(".currency7").on("click", function(){
    $("body").addClass("wallet-7-open").removeClass("wallet-2-open wallet-3-open wallet-4-open wallet-5-open wallet-6-open wallet-1-open wallet-8-open wallet-9-open wallet-10-open wallet-11-open wallet-12-open wallet-13-open wallet-14-open")
  });

  $(".currency8").on("click", function(){
    $("body").addClass("wallet-8-open").removeClass("wallet-2-open wallet-3-open wallet-4-open wallet-5-open wallet-6-open wallet-7-open wallet-1-open wallet-9-open wallet-10-open wallet-11-open wallet-12-open wallet-13-open wallet-14-open")
  });

  $(".currency9").on("click", function(){
    $("body").addClass("wallet-9-open").removeClass("wallet-2-open wallet-3-open wallet-4-open wallet-5-open wallet-6-open wallet-7-open wallet-8-open wallet-1-open wallet-10-open wallet-11-open wallet-12-open wallet-13-open wallet-14-open")
  });

  $(".currency10").on("click", function(){
    $("body").addClass("wallet-10-open").removeClass("wallet-2-open wallet-3-open wallet-4-open wallet-5-open wallet-6-open wallet-7-open wallet-8-open wallet-9-open wallet-1-open wallet-11-open wallet-12-open wallet-13-open wallet-14-open")
  });

  $(".currency11").on("click", function(){
    $("body").addClass("wallet-11-open").removeClass("wallet-2-open wallet-3-open wallet-4-open wallet-5-open wallet-6-open wallet-7-open wallet-8-open wallet-9-open wallet-10-open wallet-1-open wallet-12-open wallet-13-open wallet-14-open")
  });

  $(".currency12").on("click", function(){
    $("body").addClass("wallet-12-open").removeClass("wallet-2-open wallet-3-open wallet-4-open wallet-5-open wallet-6-open wallet-7-open wallet-8-open wallet-9-open wallet-10-open wallet-11-open wallet-1-open wallet-13-open wallet-14-open")
  });

  $(".currency13").on("click", function(){
    $("body").addClass("wallet-13-open").removeClass("wallet-2-open wallet-3-open wallet-4-open wallet-5-open wallet-6-open wallet-7-open wallet-8-open wallet-9-open wallet-10-open wallet-11-open wallet-12-open wallet-1-open wallet-14-open")
  });

  $(".currency14").on("click", function(){
    $("body").addClass("wallet-14-open").removeClass("wallet-2-open wallet-3-open wallet-4-open wallet-5-open wallet-6-open wallet-7-open wallet-8-open wallet-9-open wallet-10-open wallet-11-open wallet-12-open wallet-13-open wallet-1-open")
  });
</script>

@stop