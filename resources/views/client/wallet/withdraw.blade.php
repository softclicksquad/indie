@extends('client.layout.master')                
@section('main_content')

<div class="col-sm-7 col-md-8 col-lg-9">
    <div class="head_grn">
        Withdrawal Request
    </div>
    <div class="row withdrawal-request-main-section">
        <div class="col-sm-5 col-md-4 col-lg-9">
            <div class="user-details-section bank-account-content-section-main">
         @if(isset($mangopay_bank_details) && $mangopay_bank_details !="" && !empty($mangopay_bank_details))
         @foreach($mangopay_bank_details as $bankkey => $bank)
           <div class="bank-details-section">
           <span class="payout-btn-seciton-main">
                <a cashout-bank-id="{{ isset($bank->Id)? $bank->Id:''}}" href="{{url('/')}}/client/wallet/cashout_request/{{$bank->Id}}">
                    <i class="fa fa-money"></i> {{trans('common/wallet/text.text_payout')}} 
                </a>
           </span>
           <div class="bank-account-number">
                <div class="section-head-block-name">{{trans('common/wallet/text.text_bank_type')}}</div> 
                <div class="section-content-block"> :&nbsp; {{ isset($bank->Type)? $bank->Type:''}} </div>
                <div class="clearfix"></div>
            </div>
            <div class="bank-account-number">
                <div class="section-head-block-name"> {{trans('common/wallet/text.text_owner')}}</div> 
                <div class="section-content-block"> :&nbsp; {{ isset($bank->OwnerName)? $bank->OwnerName:''}}</div>
                <div class="clearfix"></div>
            </div>
             @if($bank->Type == 'IBAN')                                 
                <div class="bank-account-number">
                    <div class="section-head-block-name">IBAN</div> 
                    <div class="section-content-block"> :&nbsp; {{ isset($bank->Details->IBAN)? $bank->Details->IBAN:''}}</div>
                    <div class="clearfix"></div>
                </div>
                <div class="bank-account-number">
                    <div class="section-head-block-name">BIC</div> 
                    <div class="section-content-block"> :&nbsp; {{ isset($bank->Details->BIC)? $bank->Details->BIC:''}}</div>
                    <div class="clearfix"></div>
                </div>
            @endif
            @if($bank->Type == 'GB')  
                <div class="bank-account-number">
                   <div class="section-head-block-name">{{trans('common/wallet/text.text_account_number')}}</div>
                   <div class="section-content-block"> :&nbsp; {{ isset($bank->Details->AccountNumber)? $bank->Details->AccountNumber:''}}</div>
                    <div class="clearfix"></div>
                </div>
                <div class="bank-account-number">
                    <div class="section-head-block-name">{{trans('common/wallet/text.text_sort_code')}}</div> 
                    <div class="section-content-block"> :&nbsp; {{ isset($bank->Details->SortCode)? $bank->Details->SortCode:''}}</div>
                    <div class="clearfix"></div>
                </div>
            @endif
            @if($bank->Type == 'US')                                 
                <div class="bank-account-number">
                    <div class="section-head-block-name">{{trans('common/wallet/text.text_account_number')}}</div> 
                    <div class="section-content-block"> :&nbsp; {{ isset($bank->Details->AccountNumber)? $bank->Details->AccountNumber:''}}</div>
                    <div class="clearfix"></div>
                </div>
                <div class="bank-account-number">
                    <div class="section-head-block-name">ABA</div> 
                    <div class="section-content-block"> :&nbsp; {{ isset($bank->Details->ABA)? $bank->Details->ABA:''}}</div>
                    <div class="clearfix"></div>
                </div>
                <div class="bank-account-number">
                    <div class="section-head-block-name">{{trans('common/wallet/text.text_deposit_coount_type')}}</div> 
                    <div class="section-content-block"> :&nbsp; {{ isset($bank->Details->DepositAccountType)? $bank->Details->DepositAccountType:''}}</div>
                    <div class="clearfix"></div>
                </div>
           @endif
           @if($bank->Type == 'CA') 
                 <div class="bank-account-number">
                    <div class="section-head-block-name">Bank Name</div> 
                    <div class="section-content-block"> :&nbsp; {{ isset($bank->Details->BankName)? $bank->Details->BankName:''}}</div>
                    <div class="clearfix"></div>
                    </div>
                <div class="bank-account-number">
                    <div class="section-head-block-name">Institution Number</div> 
                    <div class="section-content-block"> :&nbsp; {{ isset($bank->Details->InstitutionNumber)? $bank->Details->InstitutionNumber:''}}</div>
                    <div class="clearfix"></div>
                </div>
                <div class="bank-account-number">
                    <div class="section-head-block-name">{{trans('common/wallet/text.text_branch_code')}}</div> 
                    <div class="section-content-block"> :&nbsp; {{ isset($bank->Details->BranchCode)? $bank->Details->BranchCode:''}}</div>
                    <div class="clearfix"></div>
                </div>
                  <div class="bank-account-number">
                    <div class="section-head-block-name">{{trans('common/wallet/text.text_account_number')}}</div> 
                    <div class="section-content-block"> :&nbsp; {{ isset($bank->Details->AccountNumber)? $bank->Details->AccountNumber:''}}</div>
                    <div class="clearfix"></div>
                </div>                                
             @endif

             @if($bank->Type == 'OTHER')  
                  <div class="bank-account-number">
                      <div class="section-head-block-name">{{trans('common/wallet/text.text_account_number')}}</div> 
                      <div class="section-content-block"> :&nbsp; {{ isset($bank->Details->AccountNumber)? $bank->Details->AccountNumber:''}}</div>
                      <div class="clearfix"></div>
                  </div>
                  <div class="bank-account-number">
                      <div class="section-head-block-name">BIC</div> 
                      <div class="section-content-block"> :&nbsp; {{ isset($bank->Details->BIC)? $bank->Details->BIC:''}}</div>
                      <div class="clearfix"></div>
                  </div>                               
             @endif
             </div>                           
             @endforeach
             @else
             <span class="no-bank-added"><i>-- {{trans('common/wallet/text.text_bank_account_note')}} --</i></span>
             @endif
             </div>  
             <div class="withdraw-action-btns">
                {{-- <button class="normal-btn withdraw-continue-btn" type="submit">Continue</button> --}}
                <button class="normal-btn withdraw-add-bank" type="submit"> <a href="#add_bank" class="mp-add-btn-text" data-keyboard="false" data-backdrop="static" data-toggle="modal"> Add Bank </a> </button>
                <div class="clearfix"></div>
             </div>
        </div>
        <div class="col-sm-5 col-md-4 col-lg-3">            
            <div class="currencies-section-main-block">
                <div class="currencies-section-th">
                    <div class="currencies-section-title">
                        Currency
                    </div>
                    <div class="currencies-amount-title">
                        Total
                    </div>
                    <div class="clearfix"></div>
                </div>
                @if(isset($arr_data['balance']) && count($arr_data['balance'])>0)
                @foreach($arr_data['balance'] as $key=>$value)
                <div class="currencies-section-block">
                    <div class="currencies-section-title">
                        {{isset($arr_data['currency'][$key])?$arr_data['currency'][$key]:''}}
                    </div>
                    <div class="currencies-amount-title">
                        {{isset($value)?number_format($value,2):''}}
                    </div>
                    <div class="clearfix"></div>
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </div>
</div>


<!-- add bank model -->
<div class="modal fade invite-member-modal" id="add_bank" role="dialog">
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <h4 style="margin-left: 30px;">{{trans('common/wallet/text.text_new_bank_account')}}</h4>
             </div>
             <form action="{{url('/client/wallet/add_bank')}}" method="post" id="mp-add-bank-acc-form">
               {{csrf_field()}}
               <div class="invite-form">
                    <div class="user-box">
                      <select class="clint-input" id="bank_type" name="bank_type" data-rule-required="true">
                        <option value="IBAN">IBAN</option>
                        <option value="GB">GB</option>
                        <option value="US">US</option>
                        <option value="CA">CA</option>
                        <option value="OTHER">OTHER</option>
                      </select>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input space_restrict" data-rule-required="true" name="FirstName" id="FirstName" placeholder="{{trans('common/wallet/text.text_first_name')}}">
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input space_restrict" data-rule-required="true" name="LastName" id="LastName" placeholder="{{trans('common/wallet/text.text_last_name')}}">
                       </div>
                    </div>
                    <div class="user-box">
                           <div class="input-name">
                              <textarea type="text" class="clint-input" data-rule-required="true" name="Address" id="Address" placeholder="{{trans('common/wallet/text.text_enter_address')}}"></textarea>
                           </div>
                        </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input space_restrict" data-rule-required="true" name="City" id="City" placeholder="{{trans('common/wallet/text.text_city')}}">
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input space_restrict" data-rule-required="true" name="PostalCode" id="PostalCode" placeholder="{{trans('common/wallet/text.text_PostalCode')}}">
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input space_restrict" data-rule-required="true" name="Region" id="Region" placeholder="{{trans('common/wallet/text.text_region')}}">
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <select class="clint-input space_restrict" id="Country" name="Country" data-rule-required="true">
                            <option value="">-- {{trans('common/wallet/text.text_please_select_country')}} --</option>
                            @foreach($arr_country as $country_val)
                              <option value="{{$country_val}}">{{$country_val}}</option>
                            @endforeach
                          </select>
                       </div>
                    </div>
                    
                    <!-- for-iban-account -->
                    <div id="for-iban-account">
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="IBAN" id="IBAN" placeholder="{{trans('common/wallet/text.text_enter_iban')}}">
                           </div>
                        </div>
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="BIC" id="BIC" placeholder="{{trans('common/wallet/text.text_enter_bic')}}">
                           </div>
                        </div>
                    </div>  
                    
                    <!-- for-gb-account -->
                    <div id="for-gb-account" style="display:none;">
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="gbSortCode" id="gbSortCode" placeholder="{{trans('common/wallet/text.text_enter_sort_code')}}">
                           </div>
                        </div>
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="gbAccountNumber" id="gbAccountNumber" placeholder="{{trans('common/wallet/text.text_enter_account_number')}}">
                           </div>
                        </div>
                    </div>  

                    <!-- for-us-account -->
                    <div id="for-us-account" style="display:none;">
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="usAccountNumber" id="usAccountNumber" placeholder="{{trans('common/wallet/text.text_enter_account_number')}}">
                           </div>
                        </div>
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="usABA" id="usABA" placeholder="{{trans('common/wallet/text.text_enter_aba')}}">
                           </div>
                        </div>
                        <div class="user-box">
                          <div class="input-name">
                            <select class="clint-input" id="usDepositAccountType" name="usDepositAccountType">
                              <option value="CHECKING">CHECKING</option>
                              <option value="SAVINGS">SAVINGS</option>
                            </select>
                          </div>
                        </div>
                    </div>

                    <!-- for-ca-account -->
                    <div id="for-ca-account" style="display:none;"> 
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="caAccountNumber" id="caAccountNumber" placeholder="{{trans('common/wallet/text.text_enter_account_number')}}">
                           </div>
                        </div> 
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="caBranchCode" id="caBranchCode" placeholder="{{trans('common/wallet/text.text_enter_branch_code')}}">
                           </div>
                        </div>
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input" data-rule-required="true" name="caBankName" id="caBankName" placeholder="{{trans('common/wallet/text.text_enter_bank_name')}}">
                           </div>
                        </div>
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="caInstitutionNumber" id="caInstitutionNumber" placeholder="{{trans('common/wallet/text.text_enter_institution_number')}}">
                           </div>
                        </div>
                    </div>

                    <!-- for-other-account -->
                    <div id="for-other-account" style="display:none;"> 
                      <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="otherAccountNumber" id="otherAccountNumber" placeholder="{{trans('common/wallet/text.text_enter_account_number')}}">
                           </div>
                      </div>
                      <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="otherBIC" id="otherBIC" placeholder="{{trans('common/wallet/text.text_enter_bic')}}">
                           </div>
                      </div>
                    </div>  
                <button type="submit" id="mp-add-bank" class="black-btn">{{trans('common/wallet/text.text_add_bank')}}</button>
               </div>
             </form>
          </div>
       </div>
    </div>
</div>
<!-- end add bank model -->


<!-- cashout model -->
<div class="modal fade invite-member-modal" id="cashout" role="dialog">
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <h4 style="margin-left: 180px;">{{trans('common/wallet/text.text_payout')}}</h4>
             </div>
             <form action="{{url('/expert/wallet/cashout_in_bank')}}" method="post" id="mp-cashout-form">
                {{csrf_field()}}
                <div class="invite-form inter-amt">

                  <span>{{trans('common/wallet/text.text_bank')}}: <span class="cashout-bank-id"> {{ isset($bank->Id)? $bank->Id:''}} </span> </span>
                    <div class="clearfix"></div>
                    <span>{{trans('common/wallet/text.text_available_balance')}} : <span class="mangopay-wallet-balance"> </span> <span class="amount-currency-code"></span> 
                    </span>


                <div class="user-box">
                      <div class="p-control-label">Currency <span class="star-col-block">*</span></div>
                      <div class="droup-select">
                        <select name="currency_code" id="project_currency_code_dashboard" onchange="set_currency_dashboard(this)" class="droup mrns tp-margn" data-rule-required="true">
                          @if(isset($arr_currency_backend) && sizeof($arr_currency_backend)>0)
                          <option value="">{{trans('client/projects/post.text_select_currency')}}</option>
                          @foreach($arr_currency_backend as $key => $currency)
                          <option value="{{isset($currency['currency_code'])?$currency['currency_code']:''}}" data-code="{{isset($currency['currency'])?$currency['currency']:''}}" >
                              {{isset($currency['currency_code'])?$currency['currency_code']:''}}
                          </option>
                          @endforeach
                          @endif
                      </select>
                      <span class='error'>{{ $errors->first('project_currency') }}</span>
                      </div>
                  </div>
                    <div class="user-box">
                       <div class="input-name">
                        <span class="amt-code">$</span>
                       <input type="text" class="clint-input char_restrict space_restrict" data-rule-required="true" name="cashout_amount" id="cashout_amount" placeholder="">
                       </div>
                          <span class="error" id="cashout_amount_err"></span>
                    </div>
                <div class="clearfix"></div>
                <div class="user-box">
                       <div class="input-name">
                          <textarea type="text" class="clint-input" name="Tag" id="Tag" placeholder="{{trans('common/wallet/text.text_enter_tag')}}"></textarea>
                       </div>
                    </div>
                    

                <input type="hidden" name="currency_symbol" id="currency_symbol_code" >
                <input type="hidden" value="0" class="clint-input char_restrict space_restrict" data-rule-required="true" name="act_cashout_amount" id="act_cashout_amount" placeholder="act cashout amout">
                <input type="hidden" value="" class="clint-input char_restrict space_restrict" data-rule-required="true" name="walletId" id="walletId" placeholder="walletId">
                <input type="hidden" value="{{ isset($bank->Id)? $bank->Id:''}}" class="clint-input char_restrict space_restrict" data-rule-required="true" name="bankId" id="bankId" placeholder="bankId">
                
                <div class="invite-form">
                    <button type="submit" id="mp-cashout" class="black-btn">{{trans('common/wallet/text.text_make_payout')}}</button>
                </div>
              </div>
             </form>
          </div>
       </div>
    </div>
</div>
<!-- end cashout model -->
<link rel="stylesheet" href="{{url('/public')}}/front/css/gallery.css" type="text/css" />
<script type="text/javascript" src="{{url('/public')}}/front/js/gallery.min.js"></script>
<script>
  function set_currency_dashboard()
  {
    var token     = "<?php echo csrf_token(); ?>";
    var currency = $("#project_currency_code_dashboard option:selected").attr('data-code');
    var currency_code = $("#project_currency_code_dashboard option:selected").val();
    
    $('.amt-code').text(currency);
    
    var url="{{url('/get_wallet_balance')}}";
    $.ajax({
        type: 'GET',
        contentType: 'application/x-www-form-urlencoded',
        data: $('#mp-cashout-form').serialize(),
        url:url,
        headers: {
        'X-CSRF-TOKEN': token
        },
        success: function(response) {
          console.log(response);
          $('.mangopay-wallet-balance').html(response.balance);
          $('.amount-currency-code').text(currency_code);
          $('#act_cashout_amount').val(response.balance);
          $('#walletId').val(response.walletId);
          $('#bankId').val(response.bankId);
          }
    });

  }

    $(document).ready(function() {
        $('#example1, #example2,#example3, #example4').webwingGallery({
            openGalleryStyle: 'transform',
            changeMediumStyle: true
        });
    });
</script>

<script type="text/javascript">
 $(document).ready(function(){
   $('.cashout-model').click(function(){
      var bank_id = $(this).attr('cashout-bank-id');
      $('#bankId').val(bank_id);
      $('.cashout-bank-id').html(bank_id);
   });

   $('#cashout_amount').keyup(function(){
      var currency = $("#project_currency_code_dashboard option:selected").attr('data-code');

      if(currency=='undefined' || currency==null || currency=='')
      {
        $('#cashout_amount_err').html('Please select currency first');
        return false;
      }
         $('#cashout_amount_err').html('');
      var wallet_amout       = $('#act_cashout_amount').val();
      var act_cashout_amount = $(this).val();
      if(act_cashout_amount == ""){
        act_cashout_amount = 0;
      }
      if(parseFloat(wallet_amout) <= parseFloat(act_cashout_amount)){
        $('#cashout_amount_err').html('{{trans('common/wallet/text.text_please_enter_cashout_amount_less_than_wallet_amount')}}');
        //$('#act_cashout_amount').val(wallet_amout);
      } else {
        //$('#act_cashout_amount').val(act_cashout_amount);
      } 
   });
   $('#cashout_amount').blur(function(){
      $('#cashout_amount_err').html('');
      var wallet_amout       = $('#wallet_amout').val();
      var act_cashout_amount = $('#cashout_amount').val();
      if(act_cashout_amount == ""){
        act_cashout_amount = 0;
      }
      if(parseFloat(wallet_amout) <= parseFloat(act_cashout_amount)){
        $('#cashout_amount_err').html('{{trans('common/wallet/text.text_please_enter_cashout_amount_less_than_wallet_amount')}}');
        //$('#act_cashout_amount').val(wallet_amout);
      } else {
        //$('#act_cashout_amount').val(act_cashout_amount);
      } 
   });
   $('#mp-cashout').click(function(){
      $('#cashout_amount_err').html('');
      var wallet_amout       = $('#act_cashout_amount').val();
      var act_cashout_amount = $('#cashout_amount').val();
      if(act_cashout_amount == ""){
        act_cashout_amount = 0;
      }
      if(parseFloat(wallet_amout) <= parseFloat(act_cashout_amount)){
        $('#cashout_amount_err').html('{{trans('common/wallet/text.text_please_enter_cashout_amount_less_than_wallet_amount')}}');
        //$('#act_cashout_amount').val(wallet_amout);
        return false;
      } else {
        //$('#act_cashout_amount').val(act_cashout_amount);
        return true;
      }
   });
 }); 
</script>
{{-- <script type="text/javascript">
 $(document).ready(function(){
  $('.cashout-model').click(function(){
      var bank_id = $(this).attr('cashout-bank-id');
      $('#bankId').val(bank_id);
      $('.cashout-bank-id').html(bank_id);
   });
   $('#cashout_amount').keyup(function(){
      $('#cashout_amount_err').html('');
      var wallet_amout       = $('#wallet_amout').val();
      var act_cashout_amount = $(this).val();
      if(act_cashout_amount == ""){
        act_cashout_amount = 0;
      }
      if(parseFloat(wallet_amout) <= parseFloat(act_cashout_amount)){
        $('#cashout_amount_err').html('{{trans('common/wallet/text.text_please_enter_cashout_amount_less_than_wallet_amount')}}');
        $('#act_cashout_amount').val(wallet_amout);
      } else {
        $('#act_cashout_amount').val(act_cashout_amount);
      } 
   });
   $('#cashout_amount').blur(function(){
      $('#cashout_amount_err').html('');
      var wallet_amout       = $('#wallet_amout').val();
      var act_cashout_amount = $('#cashout_amount').val();
      if(act_cashout_amount == ""){
        act_cashout_amount = 0;
      }
      if(parseFloat(wallet_amout) <= parseFloat(act_cashout_amount)){
        $('#cashout_amount_err').html('{{trans('common/wallet/text.text_please_enter_cashout_amount_less_than_wallet_amount')}}');
        $('#act_cashout_amount').val(wallet_amout);
      } else {
        $('#act_cashout_amount').val(act_cashout_amount);
      } 
   });
   $('#mp-cashout').click(function(){
      $('#cashout_amount_err').html('');
      var wallet_amout       = $('#wallet_amout').val();
      var act_cashout_amount = $('#cashout_amount').val();
      if(act_cashout_amount == ""){
        act_cashout_amount = 0;
      }
      if(parseFloat(wallet_amout) <= parseFloat(act_cashout_amount)){
        $('#cashout_amount_err').html('{{trans('common/wallet/text.text_please_enter_cashout_amount_less_than_wallet_amount')}}');
        $('#act_cashout_amount').val(wallet_amout);
        return false;
      } else {
        $('#act_cashout_amount').val(act_cashout_amount);
        return true;
      }
   });
   
   // for refund 
    $(document).on('click','#refund-module',function(){
      var AuthorId       = $(this).data('authorid');
      var RefundAmount   = $(this).data('refundamount');
      var RefundCurrency = $(this).data('refundcurrency');
      var payinId        = $(this).data('payinid');
      $('#act_refund_amount').val(RefundAmount);
      $('#refund_amount').html(RefundAmount);
      $('#authorId').val(AuthorId);
      $('#currency').val(RefundCurrency);
      $('#refund_amount_currency').html(RefundCurrency);
      $('#author_id_text').html(AuthorId);
      $('#payinId').val(payinId);
    });
 }); 
</script> --}}

<script>
 $(document).ready(function(){
    $("#mp-add-money-form").validate();
    $("#mp-add-bank-acc-form").validate();
    $("#mp-upload-document-form").validate();
    $("#mp-cashout-form").validate();
    $("#mp-refund-form").validate();
    $(document).on('click', '.add-btn', function (e) {
        var check_active = $(this).next('.money-drop').attr('class');
        if (check_active.indexOf("active") >= 0){
          $(this).next('.money-drop').removeClass('active');
        } else {
          $('.money-drop').removeClass('active');
          $(this).next('.money-drop').addClass('active');
        }
    }); 
    $(document).on('click','.close',function(){
      $("#mp-add-money-form")[0].reset();
      $("#mp-add-bank-acc-form")[0].reset();
      $("#mp-upload-document-form")[0].reset();
      $("#mp-cashout-form")[0].reset();
      $("#mp-refund-form")[0].reset();
      $('label.error,.error').html('');
    });
    $(document).on('change','#page_cnt',function(){
      $('#mp-transaction-link-page').submit();
    });
    $("#Kyc_doc").on("change", function () {
       showProcessingOverlay();
       $('.kyc_doc_file_err,.error').html('');
       var file_name       = this.files[0].name;
       var explode         = file_name.split('.');
       var file_extension  = explode['1'];
       var flag = 0;
       if(file_extension != 'pdf'  &&
          file_extension != 'jpeg' &&
          file_extension != 'jpg'  &&
          file_extension != 'gif'  &&
          file_extension != 'png'){
          $('.kyc_doc_file_err').css('color','red');
          $('.kyc_doc_file_err').html('{{trans('common/wallet/text.text_image_should_be')}}');
          flag = 1;
       } 
       if(this.files[0].size > 7000000) {
          $('.kyc_doc_file_err').css('color','red');
          $('.kyc_doc_file_err').html('{{trans('common/wallet/text.text_please_upload_file')}}');
          flag = 1;
       }
       if(flag == 1){
         hideProcessingOverlay(); 
         $(this).val('');
         return false;
       } else {
        hideProcessingOverlay(); 
        return true; 
       }
    }); 
    $("#bank_type").on("change", function () {
      var bank_type = $(this).val();
      if(bank_type == 'IBAN'){
        $('#for-iban-account').show();
        $('#for-gb-account').hide();
        $('#for-us-account').hide();
        $('#for-ca-account').hide();
        $('#for-other-account').hide();
        $("#mp-add-bank-acc-form")[0].reset();
        $('#bank_type option[value=IBAN]').attr('selected','selected');
      } else if(bank_type == 'GB'){
        $('#for-iban-account').hide();
        $('#for-gb-account').show();
        $('#for-us-account').hide();
        $('#for-ca-account').hide();
        $('#for-other-account').hide(); 
        $("#mp-add-bank-acc-form")[0].reset();
        $('#bank_type option[value=GB]').attr('selected','selected');
      } else if(bank_type == 'US'){
        $('#for-iban-account').hide();
        $('#for-gb-account').hide();
        $('#for-us-account').show();
        $('#for-ca-account').hide();
        $('#for-other-account').hide();
        $("#mp-add-bank-acc-form")[0].reset();
        $('#bank_type option[value=US]').attr('selected','selected');
      } else if(bank_type == 'CA'){
        $('#for-iban-account').hide();
        $('#for-gb-account').hide();
        $('#for-us-account').hide();
        $('#for-ca-account').show();
        $('#for-other-account').hide();
        $("#mp-add-bank-acc-form")[0].reset();
        $('#bank_type option[value=CA]').attr('selected','selected');
      } else if(bank_type == 'OTHER'){
        $('#for-iban-account').hide();
        $('#for-gb-account').hide();
        $('#for-us-account').hide();
        $('#for-ca-account').hide();
        $('#for-other-account').show();
        $("#mp-add-bank-acc-form")[0].reset();
        $('#bank_type option[value=OTHER]').attr('selected','selected');
      } 
    });
 });    
</script> 

<script>
    $(".wallet-dropdwon-head").on("click", function(){
        $(this).parent(".wallet-dropdwon-section").toggleClass("dropdown-open-main")
    });
    $(".currency-li").on("click", function(){
        $(this).parent().parent().parent().parent(".wallet-dropdwon-section").removeClass("dropdown-open-main")
    });    
    $(".currency1").on("click", function(){
        $(".dashboard-box").addClass("section-1-open").removeClass("section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency2").on("click", function(){
        $(".dashboard-box").addClass("section-2-open").removeClass("section-1-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency3").on("click", function(){
        $(".dashboard-box").addClass("section-3-open").removeClass("section-1-open section-2-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency4").on("click", function(){
        $(".dashboard-box").addClass("section-4-open").removeClass("section-1-open section-2-open section-3-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency5").on("click", function(){
        $(".dashboard-box").addClass("section-5-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency6").on("click", function(){
        $(".dashboard-box").addClass("section-6-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency7").on("click", function(){
        $(".dashboard-box").addClass("section-7-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency8").on("click", function(){
        $(".dashboard-box").addClass("section-8-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency9").on("click", function(){
        $(".dashboard-box").addClass("section-9-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency10").on("click", function(){
        $(".dashboard-box").addClass("section-10-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency11").on("click", function(){
        $(".dashboard-box").addClass("section-11-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-12-open section-13-open section-14-open")
    });    
    $(".currency12").on("click", function(){
        $(".dashboard-box").addClass("section-12-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-13-open section-14-open")
    });    
    $(".currency13").on("click", function(){
        $(".dashboard-box").addClass("section-13-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-14-open")
    });    
    $(".currency14").on("click", function(){
        $(".dashboard-box").addClass("section-14-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open")
    });
</script>
      
@stop