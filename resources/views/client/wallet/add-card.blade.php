@extends('client.layout.master')                
@section('main_content')
    <form action="<?php echo $createdCardRegister['cardRegistrationURL']; ?>" method="post">
    <input type="hidden" name="data" value="<?php echo $createdCardRegister['preregistrationData']; ?>" />
    <input type="hidden" name="accessKeyRef" value="<?php echo $createdCardRegister['accessKey']; ?>" />
    <input type="hidden" name="returnURL" value="<?php echo $returnUrl; ?>" />

    <label for="cardNumber">Card Number</label>
    <input type="text" name="cardNumber" value="" />
    <div class="clear"></div>

    <label for="cardExpirationDate">Expiration Date</label>
    <input type="text" name="cardExpirationDate" value="" />
    <div class="clear"></div>

    <label for="cardCvx">CVV</label>
    <input type="text" name="cardCvx" value="" />
    <div class="clear"></div>

    <input type="submit" value="Pay" />
</form>
@stop