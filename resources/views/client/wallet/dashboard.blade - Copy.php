@extends('client.layout.master')                
@section('main_content')
<style type="text/css">td{border-top: 0px !important;}</style>
@if(isset($mp_wallet_created) && $mp_wallet_created == 'Yes')
    <div class="col-sm-7 col-md-8 col-lg-9">
      @include('front.layout._operation_status')
      <div class="dashboard-box mangopay-dash section-1-open">
         <div class="head_grn"><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"></div>
         <div class="row">
            <!--Section 1: User Details -->


            <div class="col-sm-12 col-md-6 col-lg-6" style="padding: 15px;">
                <div class="dash-user-details">
                    <div class="title">
                       <i class="fa fa-user" aria-hidden="true"></i> {{trans('common/wallet/text.text_user_details')}}
                      <span class="pull-right" ><a href="#edit_user_details" class="mp-add-btn-text" data-keyboard="false" data-toggle="modal" data-backdrop="static"><i class="fa fa-edit"></i></a></span>
                    </div>

                    <div class="user-details-section">
                        <p><b>{{trans('common/wallet/text.text_user_id')}}     </b> <span>{{ isset($mango_user_detail->Id)? $mango_user_detail->Id:''}}</span></p>
                        <!-- <p><b>{{trans('common/wallet/text.text_full_name')}}   </b> <span>{{ isset($mango_user_detail->FirstName)? $mango_user_detail->FirstName:''}}  {{ isset($mango_user_detail->LastName)? $mango_user_detail->LastName:''}}</span></p> -->
                        <p><b>{{trans('common/wallet/text.text_email')}}       </b> <span>{{ isset($mango_user_detail->Email)? $mango_user_detail->Email:''}}</span></p>
                        <p><b>{{trans('common/wallet/text.text_full_nationality')}} </b> <span>{{ isset($mango_user_detail->Nationality)? $mango_user_detail->Nationality:''}}</span></p>
                        <p><b>{{trans('common/wallet/text.text_full_residence')}}   </b> <span>{{ isset($mango_user_detail->CountryOfResidence)? $mango_user_detail->CountryOfResidence:''}}</span></p>
                    </div>
                </div>
            </div>
            <!--User Details -->

            <!--Section 2: Wallet Details -->
            @if(isset($mangopay_wallet_details) && $mangopay_wallet_details !="" && !empty($mangopay_wallet_details))
            <div class="col-sm-12 col-md-6 col-lg-6" style="padding: 15px;">
                <div class="dash-user-details">
                    <div class="title">
                        <i class="fa fa-money" aria-hidden="true"></i> {{trans('common/wallet/text.text_wallet')}} 
                        <div class="wallet-dropdwon-section">
                            <div class="wallet-dropdwon-head">
                                Wallet Currency <i class="fa fa-angle-down"></i>
                            </div>
                            <div class="wallet-dropdwon-content-main">
                                <div class="arrow-up-mn"> <img src="http://192.168.1.65/archexpert/public/front/images/arrows-tp.png" alt=""></div>
                                <div class="wallet-dropdwon-content" style="width: auto;">
                                <ul>
                                    @if(isset($mangopay_wallet_details) && count($mangopay_wallet_details)>0)
                                    @foreach($mangopay_wallet_details as $key=>$value)
                                      <li class="currency-li currency{{$key+1}}">{{ isset($value->Balance->Currency)? $value->Balance->Currency:''}} </li>
                                    @endforeach
                                    @endif
                                </ul>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="user-details-section">
                      @if(isset($mangopay_wallet_details) && count($mangopay_wallet_details)>0)
                        @foreach($mangopay_wallet_details as $key=>$value)
                        <div class="wallet-content-section wallet-{{$key+1}}">
                        <input type="hidden" name="wallet_amout" id="wallet_amout" value="{{ isset($value->Balance->Amount)? $value->Balance->Amount/100:'0'}}">
                        <table class="table" style="margin-bottom:15px">
                          <tbody>     
                            <tr>
                              <td>
                                <span class="card-id">{{ isset($value->Id)? $value->Id:''}} </span>
                              </td>
                              <td>
                                <div class="description" style="font-size: 11px;">{{isset($value->Description)? $value->Description:''}}</div>
                              </td>
                              <td style="font-size: 17px;width: 120px;">
                                <div class="high-text-up">

                                  <span class="mp-amount">
                                    {{ isset($value->Balance->Amount)? $value->Balance->Amount/100:'0'}}
                                  </span>
                                  <span>
                                    {{ isset($value->Balance->Currency)? $value->Balance->Currency:''}}
                                  </span>
                                </div>
                                <div class="small-text-down" style="font-size: 10px;">
                                  {{trans('common/wallet/text.text_money_balance')}} 
                                </div>
                              </td>
                              <td class="text-right">
                                <span class="add-btn" ><i class="fa fa-ellipsis-h"></i></span>
                                  <div class="money-drop">
                                    <ul>
                                      <a href="#add_money" class="mp-add-btn-text"  data-keyboard="false" data-backdrop="static" data-toggle="modal"> <li><i class="fa fa-plus-circle" aria-hidden="true"></i> {{trans('common/wallet/text.text_payin')}} </li> </a>
                                      <!-- <li>Create pay-out</li>
                                      <li>Create transfer</li> -->
                                    </ul>
                                  </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
            @else
            <div class="dashboard-box mangopay-dash" style="padding: 15px;">
                <div class="row">
                  <div class="col-sm-6 col-md-3 col-lg-3">
                    <div class="pink-bx">
                      <span class="dash-top-block">
                        <div class="dash-icon">
                          <a href="{{url('/client/wallet/create_wallet_using_mp_owner_id')}}/{{ isset($mango_user_detail->Id)?base64_encode($mango_user_detail->Id):''}}">
                            <img src="{{url('/public/front/images/add-btn.png')}}" class="img-responsive" alt="wallet"/>
                          </a> 
                        </div>
                        <span class="clearfix"></span>
                      </span>
                      <span class="dash-bottom-block">
                        {{trans('common/wallet/text.text_create_wallet')}}
                      </span>
                    </div>
                  </div>
                </div>
            </div>
            @endif  
            <!-- End Wallet Details-->
            <div class="clearfix"></div>
            
            <!--Section 3: Kyc Details -->
            <div class="col-sm-12 col-md-6 col-lg-6" style="padding: 15px;">
              <div class="dash-user-details">
                  <div class="title">
                    <i class="fa fa-file-text" aria-hidden="true"></i> {{trans('common/wallet/text.text_kyc_documents')}}
                    {{-- @if(isset($mangopay_kyc_details) && $mangopay_kyc_details =="" && empty($mangopay_kyc_details)) --}}
                    <span class="add-btn pull-right" ><i class="fa fa-ellipsis-h"></i></span>
                    <div class="money-drop pull-right">
                        <ul>
                          <a href="#upload_kyc_docs" class="mp-add-btn-text" data-keyboard="false" data-backdrop="static" data-toggle="modal"> <li> <i class="fa fa-plus-circle" aria-hidden="true"></i> {{trans('common/wallet/text.text_upload_kyc_documents')}} </li> </a>
                        </ul>
                    </div>
                    {{-- @endif --}}
                  </div>
                  <div class="user-details-section">
                    @if(isset($mangopay_kyc_details) && $mangopay_kyc_details !="" && !empty($mangopay_kyc_details) && !empty($mangopay_kyc_details))
                      @foreach($mangopay_kyc_details as $kyckey => $documents)
                        <span>{{ isset($documents->Id)? $documents->Id:''}}-{{ isset($documents->Type)? $documents->Type:''}} 
                            @if(isset($documents->Status) && $documents->Status == "VALIDATED") 
                            <small class="pull-right" style="color:#16864A;" >{{ isset($documents->Status)? $documents->Status:''}} </small>
                            @elseif(isset($documents->Status) && $documents->Status == "VALIDATION_ASKED")
                            <small class="pull-right" style="color:#FFC300;" >{{ isset($documents->Status)? $documents->Status:''}} </small>
                            @elseif(isset($documents->Status) && $documents->Status == "CREATED")
                            <small class="pull-right" style="color:#FFC300;" >{{ isset($documents->Status)? $documents->Status:''}} </small>
                            @elseif(isset($documents->Status) && $documents->Status == "REFUSED")
                            <small class="pull-right" style="color:#CD1D35;" >{{ isset($documents->Status)? $documents->Status:''}} 
                              <br>
                              <small style="color:grey;"><i> {{ isset($documents->RefusedReasonType)? $documents->RefusedReasonType:''}} </i> </small>
                            </small>
                            @endif 
                        </span>
                        <div class="clearfix"></div>
                        <!-- Type / Status / UserId / RefusedReasonType / RefusedReasonMessage / ProcessedDate / Id / Tag / CreationDate -->
                      @endforeach
                    @else
                        @if(isset($is_blocked_country) && $is_blocked_country!='' && $is_blocked_country != '1')
                            <span><i>-- {{trans('common/wallet/text.text_kyc_documents_note')}} --</i></span>
                        @else
                            <span><i>-- Due to anti-money laundering policy, we are not allowed to accept the creation of user residing in: Afghanistan, Bahamas, Bosnia and Herzegovina, Botswana, Cambodia, Democratic People's Republic of Korea, Ethiopia, Ghana, Guyana, Iran, Irak, Laos, Uganda, Pakistan, Serbia, Sri Lanka, Syria, Trinidad and Tobago, Tunisia, Vanuatu, Yemen. We are very sorry. --</i></span>
                        @endif  
                    @endif
                  </div>
              </div>
            </div>
            <!--End Kyc Details -->

            <!--Section 4: Bank Details -->
            <div class="col-sm-12 col-md-6 col-lg-6" style="padding: 15px;">
              <div class="dash-user-details">
                  <div class="title">
                    <i class="fa fa-university" aria-hidden="true"></i> {{trans('common/wallet/text.text_bank_accounts')}} 
                    <span class="add-btn pull-right" ><i class="fa fa-ellipsis-h"></i></span>
                    <div class="money-drop pull-right">
                        <ul>
                            <a href="#add_bank" class="mp-add-btn-text" data-keyboard="false" data-backdrop="static" data-toggle="modal"> <li><i class="fa fa-plus-circle" aria-hidden="true"></i> {{trans('common/wallet/text.text_create_new_account')}} </li> </a>
                        </ul>
                    </div>  
                  </div>
                  <div class="user-details-section bank-account-content-section-main">
                      @if(isset($mangopay_bank_details) && $mangopay_bank_details !="" && !empty($mangopay_bank_details))
                        @foreach($mangopay_bank_details as $bankkey => $bank)                          
                              <span class="pull-right" ><a href="#cashout" data-keyboard="false" class="cashout-model" cashout-bank-id="{{ isset($bank->Id)? $bank->Id:''}}" data-backdrop="static" data-toggle="modal"><i class="fa fa-money" aria-hidden="true"></i> {{trans('common/wallet/text.text_payout')}} </a></span>
                              <span>{{ isset($bank->Id)? $bank->Id:''}}</span> 
                              <div class="clearfix"></div>
                              <p> 
                                  <b> {{trans('common/wallet/text.text_bank_type')}} </b> 
                                  <span>{{ isset($bank->Type)? $bank->Type:''}}</span>
                              </p>                              
                              <p> 
                                  <b> {{trans('common/wallet/text.text_owner')}} </b>  
                                  <span>{{ isset($bank->OwnerName)? $bank->OwnerName:''}}</span>
                              </p>
                              @if($bank->Type == 'IBAN')                                 
                                <p> 
                                    <b> IBAN </b> 
                                    <span>{{ isset($bank->Details->IBAN)? $bank->Details->IBAN:''}}</span>
                                </p>                                
                                <p> 
                                    <b> BIC </b> 
                                    <span>{{ isset($bank->Details->BIC)? $bank->Details->BIC:''}}</span>
                                </p>
                              @endif
                              @if($bank->Type == 'GB')                                 
                                <p> 
                                    <b> {{trans('common/wallet/text.text_account_number')}} </b> 
                                    <span>{{ isset($bank->Details->AccountNumber)? $bank->Details->AccountNumber:''}}</span>
                                </p>                                
                                <p> 
                                    <b> {{trans('common/wallet/text.text_sort_code')}} </b> 
                                    <span>{{ isset($bank->Details->SortCode)? $bank->Details->SortCode:''}}</span>
                                </p>
                              @endif
                              @if($bank->Type == 'US')                                 
                                <p> 
                                    <b> {{trans('common/wallet/text.text_account_number')}} </b> 
                                    <span>{{ isset($bank->Details->AccountNumber)? $bank->Details->AccountNumber:''}}</span>
                                </p>                                
                                <p> 
                                    <b> ABA </b> 
                                    <span>{{ isset($bank->Details->ABA)? $bank->Details->ABA:''}}</span>
                                </p>                                
                                <p> 
                                    <b> {{trans('common/wallet/text.text_deposit_coount_type')}} </b>  
                                    <span>{{ isset($bank->Details->DepositAccountType)? $bank->Details->DepositAccountType:''}}</span>
                                </p>
                              @endif
                              @if($bank->Type == 'CA')                                 
                                <p> 
                                    <b>BANK NAME </b>
                                    <span>{{ isset($bank->Details->BankName)? $bank->Details->BankName:''}}</span>
                                </p>                                
                                <p> 
                                    <b>INSTITUTION NUMBER  </b>
                                    <span>{{ isset($bank->Details->InstitutionNumber)? $bank->Details->InstitutionNumber:''}}</span>
                                </p>                                
                                <p> 
                                    <b>{{trans('common/wallet/text.text_branch_code')}} </b>
                                    <span>{{ isset($bank->Details->BranchCode)? $bank->Details->BranchCode:''}}</span>
                                </p>                                
                                <p> 
                                    <b>{{trans('common/wallet/text.text_account_number')}}  </b>
                                    <span>{{ isset($bank->Details->AccountNumber)? $bank->Details->AccountNumber:''}}</span>
                                </p>
                              @endif
                              @if($bank->Type == 'OTHER')                                 
                                <p> 
                                    <b>{{trans('common/wallet/text.text_account_number')}}  </b>
                                    <span>{{ isset($bank->Details->AccountNumber)? $bank->Details->AccountNumber:''}}</span>
                                </p>                                
                                <p> 
                                    <b>BIC  </b>
                                    <span>{{ isset($bank->Details->BIC)? $bank->Details->BIC:''}}</span>
                                </p>
                              @endif                          
                          <div class="clearfix"></div>
                          <br>
                        @endforeach
                      @else
                       <span><i>-- {{trans('common/wallet/text.text_bank_account_note')}} --</span></i>
                      @endif
                  </div>
                </div>
            </div>
            <!--End Bank Details -->
            <div class="clearfix"></div>

            {{-- <div class="col-sm-12 col-md-6 col-lg-6" style="padding: 15px;">
              <div class="dash-user-details">
                  <div class="title">
                    <i class="fa fa-university" aria-hidden="true"></i> Card Details 
                    <span class="add-btn pull-right" ><i class="fa fa-ellipsis-h"></i></span>
                    <div class="money-drop pull-right">
                        <ul>
                            <a href="#add_card" class="mp-add-btn-text" data-keyboard="false" data-backdrop="static" data-toggle="modal"> <li><i class="fa fa-plus-circle" aria-hidden="true"></i> Add new card </li> </a>
                        </ul>
                    </div>  
                  </div>
                  <div class="user-details-section">
                      @if(isset($mangopay_bank_details) && $mangopay_bank_details !="" && !empty($mangopay_bank_details))
                        @foreach($mangopay_bank_details as $bankkey => $bank)
                          <div class="bnk">
                              <span class="pull-right" ><a href="#cashout" data-keyboard="false" class="cashout-model" cashout-bank-id="{{ isset($bank->Id)? $bank->Id:''}}" data-backdrop="static" data-toggle="modal"><i class="fa fa-money" aria-hidden="true"> {{trans('common/wallet/text.text_payout')}} </a></i></span>
                              <span>{{ isset($bank->Id)? $bank->Id:''}}</span> 
                              <div class="clearfix"></div>
                              <span> <small> {{trans('common/wallet/text.text_bank_type')}} </small> <small class="pull-right">{{ isset($bank->Type)? $bank->Type:''}}</small></span>
                              <div class="clearfix"></div>
                              <span> <small> {{trans('common/wallet/text.text_owner')}} </small>  <small class="pull-right">{{ isset($bank->OwnerName)? $bank->OwnerName:''}}</small></span>
                              @if($bank->Type == 'IBAN') 
                                <div class="clearfix"></div>
                                <span> <small> IBAN </small> <small class="pull-right">{{ isset($bank->Details->IBAN)? $bank->Details->IBAN:''}}</small></span>
                                <div class="clearfix"></div>
                                <span> <small> BIC </small> <small class="pull-right">{{ isset($bank->Details->BIC)? $bank->Details->BIC:''}}</small></span>
                              @endif
                              @if($bank->Type == 'GB') 
                                <div class="clearfix"></div>
                                <span> <small> {{trans('common/wallet/text.text_account_number')}} </small> <small class="pull-right">{{ isset($bank->Details->AccountNumber)? $bank->Details->AccountNumber:''}}</small></span>
                                <div class="clearfix"></div>
                                <span> <small> {{trans('common/wallet/text.text_sort_code')}} </small> <small class="pull-right">{{ isset($bank->Details->SortCode)? $bank->Details->SortCode:''}}</small></span>
                              @endif
                              @if($bank->Type == 'US') 
                                <div class="clearfix"></div>
                                <span> <small> {{trans('common/wallet/text.text_account_number')}} </small> <small class="pull-right">{{ isset($bank->Details->AccountNumber)? $bank->Details->AccountNumber:''}}</small></span>
                                <div class="clearfix"></div>
                                <span> <small> ABA </small> <small class="pull-right">{{ isset($bank->Details->ABA)? $bank->Details->ABA:''}}</small></span>
                                <div class="clearfix"></div>
                                <span> <small> {{trans('common/wallet/text.text_deposit_coount_type')}} </small>  <small class="pull-right">{{ isset($bank->Details->DepositAccountType)? $bank->Details->DepositAccountType:''}}</small></span>
                              @endif
                              @if($bank->Type == 'CA') 
                                <div class="clearfix"></div>
                                <span> BANK NAME  <small class="pull-right">{{ isset($bank->Details->BankName)? $bank->Details->BankName:''}}</small></span>
                                <div class="clearfix"></div>
                                <span> INSTITUTION NUMBER  <small class="pull-right">{{ isset($bank->Details->InstitutionNumber)? $bank->Details->InstitutionNumber:''}}</small></span>
                                <div class="clearfix"></div>
                                <span> {{trans('common/wallet/text.text_branch_code')}}  <small class="pull-right">{{ isset($bank->Details->BranchCode)? $bank->Details->BranchCode:''}}</small></span>
                                <div class="clearfix"></div>
                                <span> {{trans('common/wallet/text.text_account_number')}}  <small class="pull-right">{{ isset($bank->Details->AccountNumber)? $bank->Details->AccountNumber:''}}</small></span>
                              @endif
                              @if($bank->Type == 'OTHER') 
                                <div class="clearfix"></div>
                                <span> {{trans('common/wallet/text.text_account_number')}}  <small class="pull-right">{{ isset($bank->Details->AccountNumber)? $bank->Details->AccountNumber:''}}</small></span>
                                <div class="clearfix"></div>
                                <span> BIC  <small class="pull-right">{{ isset($bank->Details->BIC)? $bank->Details->BIC:''}}</small></span>
                              @endif
                          </div>
                          <div class="clearfix"></div>
                          <br>
                        @endforeach
                      @else
                       <span><i>-- No Cards available --</span></i>
                      @endif
                  </div>
                </div>
            </div> --}}









            <!--Section 5: Transactions Details -->
            <?php /*<div class="col-sm-12 col-md-12 col-lg-12 recent-transactions" style="padding: 15px;">
              <div class="dash-user-details">
                  <div class="title">
                    <i class="fa fa-credit-card-alt" aria-hidden="true"></i> {{trans('common/wallet/text.text_recent_transactions')}}
                    <div class="col-sm-2 col-md-2 col-lg-2 pull-right">
                      <form id="mp-transaction-link-page" action="{{url('/client/wallet/dashboard')}}" method="get">
                        <!-- {{csrf_field()}} -->
                        <select  name="page_cnt" class="select-box" id="page_cnt" style="font-size: smaller;">
                            @if(isset($_REQUEST['page_cnt']) && $_REQUEST['page_cnt'] != "")
                              @php $active_page = $_REQUEST['page_cnt']; @endphp
                            @else
                              @php $active_page = $mangopay_transaction_pagi_links_count; @endphp
                            @endif
                            @for($i=1;$i <= $mangopay_transaction_pagi_links_count;$i++)
                            @php $nxt = ($i*config('app.project.pagi_cnt')); @endphp
                            <option value="{{$i}}" @if($i == $active_page) selected="selected" @endif>{{$nxt-config('app.project.pagi_cnt')}} - {{$nxt}}</option>
                            @endfor
                        </select>
                      </form>
                    </div>
                  </div>
                  <div class="user-details-section">
                    @if(isset($mangopay_transaction_details) && $mangopay_transaction_details !="" && !empty($mangopay_transaction_details))
                      @foreach($mangopay_transaction_details as $index=>$data)
                      <div class="table-section-main table-{{$index+1}}">
                      <table id="TaBle" class="theme-table invoice-table-s table" style="border: 1px solid rgb(239, 239, 239); margin-bottom:0;">
                         <thead class="tras-client-tbl">
                            <tr>
                               <th>{{trans('common/wallet/text.text_creation')}}</th>
                               <th>{{trans('common/wallet/text.text_id')}}</th>
                               <th>{{trans('common/wallet/text.text_type')}}</th>
                               <th>Debited Amount</th>
                               <th>Credited Amount</th>
                               <th>{{trans('common/wallet/text.text_status')}}</th>
                               <th>{{trans('common/wallet/text.text_result')}}</th>
                            </tr>
                         </thead>
                         <tbody>
                            @if(isset($mangopay_transaction_details[$index]) && sizeof($mangopay_transaction_details[$index])>0)
                            @if($mangopay_transaction_details[$index]!=false)
                              @foreach(array_reverse($mangopay_transaction_details[$index]) as $transaction)
                              {{-- {{dd($transaction)}} --}}
                                <tr>
                                   <td>
                                       {{date('Y-m-d',$transaction->CreationDate)}} <br> 
                                       {{date('h:i:s',$transaction->CreationDate)}} <br>
                                       {{date('a',$transaction->CreationDate)}}
                                   </td>
                                   <td>{{$transaction->Id or '-'}}</td>
                                   <td width="15%">
                                    {{$transaction->Type or '-'}}
                                    @if(isset($transaction->Type) && $transaction->Type == 'TRANSFER' && isset($transaction->CreditedWalletId) && $transaction->CreditedWalletId == $mangopay_wallet_details[$index]->Id)
                                    <br><i class="fa fa-sign-in" style="color:green;" aria-hidden="true"> Credited</i> 
                                    @elseif(isset($transaction->Type) && $transaction->Type == 'TRANSFER' && isset($transaction->DebitedWalletId) && $transaction->DebitedWalletId == $mangopay_wallet_details[$index]->Id)
                                    <br><i class="fa fa-sign-out"style="color:red;" aria-hidden="true"> Debited</i> 
                                    @endif
                                   </td>
                                   <td><span @if($transaction->Status == 'SUCCEEDED') style="color:green" @else style="color:red" @endif>{{ isset($transaction->DebitedFunds->Amount)? $transaction->DebitedFunds->Amount/100:'0'}}</span> <b>{{$transaction->DebitedFunds->Currency or 'USD'}}</b> <br>incl. {{ isset($transaction->Fees->Amount)? $transaction->Fees->Amount/100:'0'}} fees </td>
                                   <td><span @if($transaction->Status == 'SUCCEEDED') style="color:green" @else style="color:red" @endif>{{ isset($transaction->CreditedFunds->Amount)? $transaction->CreditedFunds->Amount/100:'0'}}</span> <b>{{$transaction->DebitedFunds->Currency or 'USD'}}</b></td>
                                   <td @if($transaction->Status == 'SUCCEEDED') style="color:green" @else style="color:red" @endif>{{$transaction->Status or '-'}}</td>
                                   <td width="30%">{{$transaction->ResultCode or '-'}} :{{$transaction->ResultMessage or '-'}}  <br><i><b>Tag: {{$transaction->Tag or '-'}}<b></i></td>
                                </tr>
                              @endforeach
                              @endif
                            @else
                            <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;" align="center">
                              <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                                 <div class="search-content-block">
                                    <div class="no-record">
                                        {{ trans('expert/transactions/packs.text_sorry_no_records_found')}}                           
                                      </div>
                                    </div>
                                 </td>
                             </tr>
                            @endif
                         </tbody>
                      </table>
                      </div>
                      @endforeach
                    @else
                      -- {{trans('common/wallet/text.text_transactions_note')}} --
                    @endif  
                  </div>
              </div>
            </div> */?>
            <!--End Transactions Details -->
        </div>
      </div>
    </div>
@else
<div class="col-sm-7 col-md-8 col-lg-9">
  @include('front.layout._operation_status')
  <div class="dashboard-box mangopay-dash">
     <div class="head_grn"><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"></div>
     <div class="row">
        <div class="col-sm-6 col-md-3 col-lg-3">
              <div class="pink-bx">
                  <span class="dash-top-block">
                     <div class="dash-icon">
                      <a href="{{url('/client/wallet/create_user')}}">
                       <img src="{{url('/public/front/images/add-btn.png')}}" class="img-responsive" alt="wallet"/>
                      </a> 
                     </div>
                     <span class="clearfix"></span>
                  </span>
                  <span class="dash-bottom-block">
                      {{trans('common/wallet/text.text_archexpert_wallet')}}  
                  </span>
              </div>
         </div>
     </div>
  </div>
</div>
@endif

<!-- add money model -->
{{-- <div class="modal fade invite-member-modal" id="add_money" role="dialog">
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <h4>{{trans('common/wallet/text.text_payin_in_to_wallet')}}</h4>
             </div>
             <form action="{{url('/client/wallet/add_money')}}" method="post" id="mp-add-money-form">
               {{csrf_field()}}
               <div class="invite-form">
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input char_restrict space_restrict" data-rule-required="true" name="amount" id="amount" placeholder="{{trans('common/wallet/text.text_enter_amount_in')}} {{$currency_code}}">
                       </div>
                    </div>
                    <button type="submit" id="mp-add-money" class="black-btn">{{trans('common/wallet/text.text_payin')}}</button>
               </div>
             </form>
          </div>
       </div>
    </div>
</div> --}}
<!-- end add money model -->



<!-- add card model -->

<div class="modal fade invite-member-modal" id="add_card" role="dialog">
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <h4 style="margin-left: 160px;">Add new card</h4>
             </div>
             <form action="#{{-- {{url('/client/wallet/add_bank')}} --}}" method="post" id="mp-add-bank-acc-form">
               {{csrf_field()}}
               <div class="invite-form">
                    
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input space_restrict" data-rule-required="true" name="Card_holder_name" id="Card_holder_name" placeholder="Card holder name">
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input space_restrict" data-rule-required="true" name="card_number" id="card_number" placeholder="Card number">
                       </div>
                    </div>
                    <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="expire_month" id="expire_month" placeholder="Expire month">
                           </div>
                        </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input space_restrict" data-rule-required="true" name="expire_year" id="expire_year" placeholder="Expire year">
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input space_restrict" data-rule-required="true" name="cvv_no" id="cvv_no" placeholder="Cvv">
                       </div>
                    </div>
                <button type="submit" id="mp-add-bank" class="black-btn">Add Card</button>
               </div>
             </form>
          </div>
       </div>
    </div>
</div>
<!-- end add card model -->






<!-- add bank model -->
<div class="modal fade invite-member-modal" id="add_bank" role="dialog">
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <h4 style="margin-left: 30px;">{{trans('common/wallet/text.text_new_bank_account')}}</h4>
             </div>
             <form action="{{url('/client/wallet/add_bank')}}" method="post" id="mp-add-bank-acc-form">
               {{csrf_field()}}
               <div class="invite-form">
                    <div class="user-box">
                      <select class="clint-input" id="bank_type" name="bank_type" data-rule-required="true">
                        <option value="IBAN">IBAN</option>
                        <option value="GB">GB</option>
                        <option value="US">US</option>
                        <option value="CA">CA</option>
                        <option value="OTHER">OTHER</option>
                      </select>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input space_restrict" data-rule-required="true" name="FirstName" id="FirstName" placeholder="{{trans('common/wallet/text.text_first_name')}}">
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input space_restrict" data-rule-required="true" name="LastName" id="LastName" placeholder="{{trans('common/wallet/text.text_last_name')}}">
                       </div>
                    </div>
                    <div class="user-box">
                           <div class="input-name">
                              <textarea type="text" class="clint-input" data-rule-required="true" name="Address" id="Address" placeholder="{{trans('common/wallet/text.text_enter_address')}}"></textarea>
                           </div>
                        </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input space_restrict" data-rule-required="true" name="City" id="City" placeholder="{{trans('common/wallet/text.text_city')}}">
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input space_restrict" data-rule-required="true" name="PostalCode" id="PostalCode" placeholder="{{trans('common/wallet/text.text_PostalCode')}}">
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input space_restrict" data-rule-required="true" name="Region" id="Region" placeholder="{{trans('common/wallet/text.text_region')}}">
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <select class="clint-input space_restrict" id="Country" name="Country" data-rule-required="true">
                            <option value="">-- {{trans('common/wallet/text.text_please_select_country')}} --</option>
                            @foreach($arr_country as $country_val)
                              <option value="{{$country_val}}">{{$country_val}}</option>
                            @endforeach
                          </select>
                       </div>
                    </div>
                    
                    <!-- for-iban-account -->
                    <div id="for-iban-account">
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="IBAN" id="IBAN" placeholder="{{trans('common/wallet/text.text_enter_iban')}}">
                           </div>
                        </div>
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="BIC" id="BIC" placeholder="{{trans('common/wallet/text.text_enter_bic')}}">
                           </div>
                        </div>
                    </div>  
                    
                    <!-- for-gb-account -->
                    <div id="for-gb-account" style="display:none;">
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="gbSortCode" id="gbSortCode" placeholder="{{trans('common/wallet/text.text_enter_sort_code')}}">
                           </div>
                        </div>
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="gbAccountNumber" id="gbAccountNumber" placeholder="{{trans('common/wallet/text.text_enter_account_number')}}">
                           </div>
                        </div>
                    </div>  

                    <!-- for-us-account -->
                    <div id="for-us-account" style="display:none;">
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="usAccountNumber" id="usAccountNumber" placeholder="{{trans('common/wallet/text.text_enter_account_number')}}">
                           </div>
                        </div>
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="usABA" id="usABA" placeholder="{{trans('common/wallet/text.text_enter_aba')}}">
                           </div>
                        </div>
                        <div class="user-box">
                          <div class="input-name">
                            <select class="clint-input" id="usDepositAccountType" name="usDepositAccountType">
                              <option value="CHECKING">CHECKING</option>
                              <option value="SAVINGS">SAVINGS</option>
                            </select>
                          </div>
                        </div>
                    </div>

                    <!-- for-ca-account -->
                    <div id="for-ca-account" style="display:none;"> 
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="caAccountNumber" id="caAccountNumber" placeholder="{{trans('common/wallet/text.text_enter_account_number')}}">
                           </div>
                        </div> 
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="caBranchCode" id="caBranchCode" placeholder="{{trans('common/wallet/text.text_enter_branch_code')}}">
                           </div>
                        </div>
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input" data-rule-required="true" name="caBankName" id="caBankName" placeholder="{{trans('common/wallet/text.text_enter_bank_name')}}">
                           </div>
                        </div>
                        <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="caInstitutionNumber" id="caInstitutionNumber" placeholder="{{trans('common/wallet/text.text_enter_institution_number')}}">
                           </div>
                        </div>
                    </div>

                    <!-- for-other-account -->
                    <div id="for-other-account" style="display:none;"> 
                      <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="otherAccountNumber" id="otherAccountNumber" placeholder="{{trans('common/wallet/text.text_enter_account_number')}}">
                           </div>
                      </div>
                      <div class="user-box">
                           <div class="input-name">
                              <input type="text" class="clint-input space_restrict" data-rule-required="true" name="otherBIC" id="otherBIC" placeholder="{{trans('common/wallet/text.text_enter_bic')}}">
                           </div>
                      </div>
                    </div>  
                <button type="submit" id="mp-add-bank" class="black-btn">{{trans('common/wallet/text.text_add_bank')}}</button>
               </div>
             </form>
          </div>
       </div>
    </div>
</div>
<!-- end add bank model -->



<!-- Update user details model -->
<div class="modal fade invite-member-modal" id="edit_user_details" role="dialog">
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <h4>Edit {{trans('common/wallet/text.text_user_details')}}</h4>
             </div>
             <form action="{{url('/client/wallet/update_user_details')}}" method="post" id="mp-add-bank-acc-form">
               {{csrf_field()}}
               <div class="invite-form">
                  <div class="user-box">
                    <b>Nationality</b>
                    <div class="input-name">
                      <select class="clint-input space_restrict" id="nationality" name="nationality" data-rule-required="true">
                        <option value="">- Please select your nationality -</option>
                        @foreach($arr_country as $country_val)
                          <option value="{{$country_val}}" @if(isset($mango_user_detail->Nationality) && $mango_user_detail->Nationality!='' && $mango_user_detail->Nationality == $country_val) selected @endif>{{$country_val}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="user-box">
                    <b>Residence</b>
                    <div class="input-name">
                      <select class="clint-input space_restrict" id="residence" name="residence" data-rule-required="true">
                        <option value="">- Please select your country -</option>
                        @foreach($arr_country as $country_val)
                          <option value="{{$country_val}}" @if(isset($mango_user_detail->CountryOfResidence) && $mango_user_detail->CountryOfResidence!='' && $mango_user_detail->CountryOfResidence == $country_val) selected @endif>{{$country_val}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                <button type="submit" id="mp-update-user" class="black-btn">Update</button>
               </div>
             </form>
          </div>
       </div>
    </div>
</div>
<!-- end update user details model -->

<!-- add kyc model -->
<div class="modal fade invite-member-modal" id="upload_kyc_docs" role="dialog" >
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <h4>{{trans('common/wallet/text.text_kyc_documents_upload')}}</h4>
             </div>
             <form action="{{url('/client/wallet/upload_kyc_docs')}}" method="post" id="mp-upload-document-form" enctype="multipart/form-data">
               {{csrf_field()}}
               <input type="hidden" name="user_type" value="{{$user_type}}">
               <div class="invite-form">
                    {{-- <div class="user-box">
                       <div class="input-name">
                          <select class="clint-input space_restrict" id="kyc_upload_documents_natural_types" name="kyc_upload_documents_natural_types" data-rule-required="true">
                            <option value="IDENTITY_PROOF">{{trans('common/wallet/text.text_identity_proof')}}</option>
                          </select>
                       </div>
                    </div>
                    {{trans('common/wallet/text.text_accepted_for_the_documents')}} --}}

                   <div class="user-box">
                      <b>Identity Proof</b>
                      <div class="id-proof-main-section">
                        <div class="input-name">
                          <input type="file" class="clint-input" name="id_proof" id="Kyc_doc" placeholder="Select document" data-rule-required="true">
                          <span class="error kyc_doc_file_err"></span>
                        </div>
                        <div class="id-proof-img-section">
                                <div id="example1" class="webwing-gallery">
                                    <div class="prod-carousel">
                                        <img src="{{url('/public')}}/front/images/identity.jpg" data-medium-img="{{url('/public')}}/front/images/identity.jpg" data-big-img="{{url('/public')}}/front/images/identity.jpg" data-title="" alt="">
                                        <img src="{{url('/public')}}/front/images/passport.jpg" data-medium-img="{{url('/public')}}/front/images/passport.jpg" data-big-img="{{url('/public')}}/front/images/passport.jpg" alt="">
                                    </div>
                                </div>                               
                          </div>
                          <div class="id-proof-img-section">
                               <div id="example2" class="webwing-gallery">
                                    <div class="prod-carousel">
                                        <img src="{{url('/public')}}/front/images/passport.jpg" data-medium-img="{{url('/public')}}/front/images/passport.jpg" data-big-img="{{url('/public')}}/front/images/passport.jpg" alt="">
                                        <img src="{{url('/public')}}/front/images/identity.jpg" data-medium-img="{{url('/public')}}/front/images/identity.jpg" data-big-img="{{url('/public')}}/front/images/identity.jpg" data-title="" alt="">                                        
                                    </div>
                                </div>                               
                          </div>
                      </div>
                      Eg. <span id="id_proof_note" style="color:#ff9d00">Passport, national ID card, driving license</span>
                    </div>
                    
                    @if(isset($user_type) && $user_type!='' && $user_type=='business')
                    <div class="user-box">
                      <b>Registration Proof</b>
                       <div class="input-name">
                          <input type="file" class="clint-input" name="reg_proof" id="Kyc_doc1" placeholder="Select document" data-rule-required="true">
                           <span class="error kyc_doc_file_err1"></span>
                       </div>
                      Eg. <span id="reg_proof_note" style="color:#ff9d00">The company registration document is a written statement from the Government in your country which confirms that the company legally exists.</span>
                    </div>
                    @endif
                    <div class="user-box">
                       <div class="input-name">
                          <textarea type="text" class="clint-input" name="Kyc_Tag" id="Kyc_Tag" placeholder="{{trans('common/wallet/text.text_enter_tag')}}"></textarea>
                       </div>
                    </div>

                    <button type="submit" id="mp-upload-document" class="black-btn">UPLOAD</button>
               </div>
             </form>
          </div>
       </div>
    </div>
</div>
<!-- end add kyc model -->

<!-- cashout model -->
<div class="modal fade invite-member-modal" id="cashout" role="dialog">
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <h4 style="margin-left: 172px;">{{trans('common/wallet/text.text_payout')}}</h4>
             </div>
             <form action="{{url('/client/wallet/cashout_in_bank')}}" method="post" id="mp-cashout-form">
                {{-- {{csrf_field()}}
                <input type="hidden" value="0" class="clint-input char_restrict space_restrict" data-rule-required="true" name="act_cashout_amount" id="act_cashout_amount" placeholder="act cashout amout">
                <input type="hidden" value="{{ isset($mangopay_wallet_details->Id)? $mangopay_wallet_details->Id:''}}" class="clint-input char_restrict space_restrict" data-rule-required="true" name="walletId" id="walletId" placeholder="walletId">
                <input type="hidden" value="{{ isset($bank->Id)? $bank->Id:''}}" class="clint-input char_restrict space_restrict" data-rule-required="true" name="bankId" id="bankId" placeholder="bankId">
                
                <div class="invite-form">
                    <span>{{trans('common/wallet/text.text_bank')}}              : <span class="cashout-bank-id"> {{ isset($bank->Id)? $bank->Id:''}} </span> </span>
                    <div class="clearfix"></div>
                    <span>{{trans('common/wallet/text.text_available_balance')}} : {{ isset($mangopay_wallet_details->Balance->Amount)? $mangopay_wallet_details->Balance->Amount/100:'0'}} {{ isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:''}}</span>
                    <div class="clearfix"></div>
                    <hr>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="text" class="clint-input char_restrict space_restrict" data-rule-required="true" name="cashout_amount" id="cashout_amount" placeholder="{{trans('common/wallet/text.text_enter_amount_in')}} {{config('app.project_currency.$')}}">
                          <span class="error" id="cashout_amount_err"></span>
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <textarea type="text" class="clint-input" name="Tag" id="Tag" placeholder="{{trans('common/wallet/text.text_enter_tag')}}"></textarea>
                       </div>
                    </div>
                    <button type="submit" id="mp-cashout" class="black-btn">{{trans('common/wallet/text.text_make_payout')}}</button>
                </div> --}}
                {{csrf_field()}}
                <div class="invite-form inter-amt">

                  <span>{{trans('common/wallet/text.text_bank')}}: <span class="cashout-bank-id"> {{ isset($bank->Id)? $bank->Id:''}} </span> </span>
                    <div class="clearfix"></div>
                    <span>{{trans('common/wallet/text.text_available_balance')}} : <span class="mangopay-wallet-balance"> </span> <span class="amount-currency-code"></span> 
                    </span>


                <div class="user-box">
                      <div class="p-control-label">Currency <span class="star-col-block">*</span></div>
                      <div class="droup-select">
                        <select name="currency_code" id="project_currency_code_dashboard" onchange="set_currency_dashboard(this)" class="droup mrns tp-margn" data-rule-required="true">
                          @if(isset($arr_currency_backend) && sizeof($arr_currency_backend)>0)
                          <option value="">{{trans('client/projects/post.text_select_currency')}}</option>
                          @foreach($arr_currency_backend as $key => $currency)
                          <option value="{{isset($currency['currency_code'])?$currency['currency_code']:''}}" data-code="{{isset($currency['currency'])?$currency['currency']:''}}" >
                              {{isset($currency['currency_code'])?$currency['currency_code']:''}}
                          </option>
                          @endforeach
                          @endif
                      </select>
                      <span class='error'>{{ $errors->first('project_currency') }}</span>
                      </div>
                  </div>
                    <div class="user-box">
                       <div class="input-name">
                        <span class="amt-code">$</span>
                       <input type="text" class="clint-input char_restrict space_restrict" data-rule-required="true" name="cashout_amount" id="cashout_amount" >
                       </div>
                          <span class="error" id="cashout_amount_err"></span>
                    </div>
                <div class="clearfix"></div>
                <div class="user-box">
                       <div class="input-name">
                          <textarea type="text" class="clint-input" name="Tag" id="Tag" placeholder="{{trans('common/wallet/text.text_enter_tag')}}"></textarea>
                       </div>
                    </div>
                    

                <input type="hidden" name="currency_symbol" id="currency_symbol_code" >
                <input type="hidden" value="0" class="clint-input char_restrict space_restrict" data-rule-required="true" name="act_cashout_amount" id="act_cashout_amount" placeholder="act cashout amout">
                <input type="hidden" value="" class="clint-input char_restrict space_restrict" data-rule-required="true" name="walletId" id="walletId" placeholder="walletId">
                <input type="hidden" value="{{ isset($bank->Id)? $bank->Id:''}}" class="clint-input char_restrict space_restrict" data-rule-required="true" name="bankId" id="bankId" placeholder="bankId">
                
                <div class="invite-form">
                    <button type="submit" id="mp-cashout" class="black-btn">{{trans('common/wallet/text.text_make_payout')}}</button>
                </div>
              </div>
             </form>
          </div>
       </div>
    </div>
</div>
<!-- end cashout model -->

<!-- refund model -->
<div class="modal fade invite-member-modal" id="refund" role="dialog">
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">
             <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
             <div class="invite-member-section">
                 <h4>{{trans('common/wallet/text.text_payin_refund')}}</h4>
             </div>
             <form action="{{url('/client/wallet/payin_refund')}}" method="post" id="mp-refund-form">
                {{csrf_field()}}
                <input type="hidden" value="0" class="clint-input char_restrict space_restrict" data-rule-required="true" name="act_refund_amount" id="act_refund_amount" placeholder="act refund amout">
                <input type="hidden" value="{{ isset($mangopay_wallet_details->Id)? $mangopay_wallet_details->Id:''}}" class="clint-input char_restrict space_restrict" data-rule-required="true" name="walletId" id="walletId" placeholder="walletId">
                <input type="hidden" value="0" class="clint-input char_restrict space_restrict" data-rule-required="true" name="authorId" id="authorId" placeholder="authorId">
                <input type="hidden" value="0" class="clint-input char_restrict space_restrict" data-rule-required="true" name="currency" id="currency" placeholder="currency">
                <input type="hidden" value="0" class="clint-input char_restrict space_restrict" data-rule-required="true" name="payinId" id="payinId" placeholder="payinId">
                <div class="invite-form">
                    <span>{{trans('common/wallet/text.text_available_balance')}} : {{ isset($mangopay_wallet_details->Balance->Amount)? $mangopay_wallet_details->Balance->Amount/100:'0'}} {{ isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:''}}</span>
                    <div class="clearfix"></div>
                    <span>Author Id : <span id="author_id_text">0</span></span>
                    <div class="clearfix"></div>
                    <hr>
                    <span>{{trans('common/wallet/text.text_requested_refund_amount')}} : <span id="refund_amount">0</span> <span id="refund_amount_currency">{{ isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:''}}</span></span>
                    <div class="clearfix"></div>
                    <div class="user-box">
                       <div class="input-name">
                          <textarea type="text" class="clint-input" name="Tag" id="Tag" placeholder="{{trans('common/wallet/text.text_enter_tag')}}"></textarea>
                       </div>
                    </div>
                    <button type="submit" id="mp-refund" class="black-btn">{{trans('common/wallet/text.text_refund')}}</button>
                </div>
             </form>
          </div>
       </div>
    </div>
</div>
<!-- end refund model -->

<link rel="stylesheet" href="{{url('/public')}}/front/css/gallery.css" type="text/css" />
<script type="text/javascript" src="{{url('/public')}}/front/js/gallery.min.js"></script>
<script>
  function set_currency_dashboard()
  {
    var token     = "<?php echo csrf_token(); ?>";
    var currency = $("#project_currency_code_dashboard option:selected").attr('data-code');
    var currency_code = $("#project_currency_code_dashboard option:selected").val();
    
    $('.amt-code').text(currency);
    
    var url="{{url('/get_wallet_balance')}}";
    $.ajax({
        type: 'GET',
        contentType: 'application/x-www-form-urlencoded',
        data: $('#mp-cashout-form').serialize(),
        url:url,
        headers: {
        'X-CSRF-TOKEN': token
        },
        success: function(response) {
          console.log(response);
          $('.mangopay-wallet-balance').html(response.balance);
          $('.amount-currency-code').text(currency_code);
          $('#act_cashout_amount').val(response.balance);
          $('#walletId').val(response.walletId);
          $('#bankId').val(response.bankId);
          }
    });

  }

    $(document).ready(function() {
        $('#example1, #example2,#example3, #example4').webwingGallery({
            openGalleryStyle: 'transform',
            changeMediumStyle: true
        });
    });
</script>

<script type="text/javascript">
 $(document).ready(function(){
   $('.cashout-model').click(function(){
      var bank_id = $(this).attr('cashout-bank-id');
      $('#bankId').val(bank_id);
      $('.cashout-bank-id').html(bank_id);
   });

   $('#cashout_amount').keyup(function(){
      var currency = $("#project_currency_code_dashboard option:selected").attr('data-code');

      if(currency=='undefined' || currency==null || currency=='')
      {
        $('#cashout_amount_err').html('Please select currency first');
        return false;
      }
         $('#cashout_amount_err').html('');
      var wallet_amout       = $('#act_cashout_amount').val();
      var act_cashout_amount = $(this).val();
      if(act_cashout_amount == ""){
        act_cashout_amount = 0;
      }
      if(parseFloat(wallet_amout) <= parseFloat(act_cashout_amount)){
        $('#cashout_amount_err').html('{{trans('common/wallet/text.text_please_enter_cashout_amount_less_than_wallet_amount')}}');
        //$('#act_cashout_amount').val(wallet_amout);
      } else {
        //$('#act_cashout_amount').val(act_cashout_amount);
      } 
   });
   $('#cashout_amount').blur(function(){
      $('#cashout_amount_err').html('');
      var wallet_amout       = $('#wallet_amout').val();
      var act_cashout_amount = $('#cashout_amount').val();
      if(act_cashout_amount == ""){
        act_cashout_amount = 0;
      }
      if(parseFloat(wallet_amout) <= parseFloat(act_cashout_amount)){
        $('#cashout_amount_err').html('{{trans('common/wallet/text.text_please_enter_cashout_amount_less_than_wallet_amount')}}');
        //$('#act_cashout_amount').val(wallet_amout);
      } else {
        //$('#act_cashout_amount').val(act_cashout_amount);
      } 
   });
   $('#mp-cashout').click(function(){
      $('#cashout_amount_err').html('');
      var wallet_amout       = $('#act_cashout_amount').val();
      var act_cashout_amount = $('#cashout_amount').val();
      if(act_cashout_amount == ""){
        act_cashout_amount = 0;
      }
      if(parseFloat(wallet_amout) <= parseFloat(act_cashout_amount)){
        $('#cashout_amount_err').html('{{trans('common/wallet/text.text_please_enter_cashout_amount_less_than_wallet_amount')}}');
        //$('#act_cashout_amount').val(wallet_amout);
        return false;
      } else {
        //$('#act_cashout_amount').val(act_cashout_amount);
        return true;
      }
   });
 }); 
</script>
{{-- <script type="text/javascript">
 $(document).ready(function(){
  $('.cashout-model').click(function(){
      var bank_id = $(this).attr('cashout-bank-id');
      $('#bankId').val(bank_id);
      $('.cashout-bank-id').html(bank_id);
   });
   $('#cashout_amount').keyup(function(){
      $('#cashout_amount_err').html('');
      var wallet_amout       = $('#wallet_amout').val();
      var act_cashout_amount = $(this).val();
      if(act_cashout_amount == ""){
        act_cashout_amount = 0;
      }
      if(parseFloat(wallet_amout) <= parseFloat(act_cashout_amount)){
        $('#cashout_amount_err').html('{{trans('common/wallet/text.text_please_enter_cashout_amount_less_than_wallet_amount')}}');
        $('#act_cashout_amount').val(wallet_amout);
      } else {
        $('#act_cashout_amount').val(act_cashout_amount);
      } 
   });
   $('#cashout_amount').blur(function(){
      $('#cashout_amount_err').html('');
      var wallet_amout       = $('#wallet_amout').val();
      var act_cashout_amount = $('#cashout_amount').val();
      if(act_cashout_amount == ""){
        act_cashout_amount = 0;
      }
      if(parseFloat(wallet_amout) <= parseFloat(act_cashout_amount)){
        $('#cashout_amount_err').html('{{trans('common/wallet/text.text_please_enter_cashout_amount_less_than_wallet_amount')}}');
        $('#act_cashout_amount').val(wallet_amout);
      } else {
        $('#act_cashout_amount').val(act_cashout_amount);
      } 
   });
   $('#mp-cashout').click(function(){
      $('#cashout_amount_err').html('');
      var wallet_amout       = $('#wallet_amout').val();
      var act_cashout_amount = $('#cashout_amount').val();
      if(act_cashout_amount == ""){
        act_cashout_amount = 0;
      }
      if(parseFloat(wallet_amout) <= parseFloat(act_cashout_amount)){
        $('#cashout_amount_err').html('{{trans('common/wallet/text.text_please_enter_cashout_amount_less_than_wallet_amount')}}');
        $('#act_cashout_amount').val(wallet_amout);
        return false;
      } else {
        $('#act_cashout_amount').val(act_cashout_amount);
        return true;
      }
   });
   
   // for refund 
    $(document).on('click','#refund-module',function(){
      var AuthorId       = $(this).data('authorid');
      var RefundAmount   = $(this).data('refundamount');
      var RefundCurrency = $(this).data('refundcurrency');
      var payinId        = $(this).data('payinid');
      $('#act_refund_amount').val(RefundAmount);
      $('#refund_amount').html(RefundAmount);
      $('#authorId').val(AuthorId);
      $('#currency').val(RefundCurrency);
      $('#refund_amount_currency').html(RefundCurrency);
      $('#author_id_text').html(AuthorId);
      $('#payinId').val(payinId);
    });
 }); 
</script> --}}

<script>
 $(document).ready(function(){
    $("#mp-add-money-form").validate();
    $("#mp-add-bank-acc-form").validate();
    $("#mp-upload-document-form").validate();
    $("#mp-cashout-form").validate();
    $("#mp-refund-form").validate();
    $(document).on('click', '.add-btn', function (e) {
        var check_active = $(this).next('.money-drop').attr('class');
        if (check_active.indexOf("active") >= 0){
          $(this).next('.money-drop').removeClass('active');
        } else {
          $('.money-drop').removeClass('active');
          $(this).next('.money-drop').addClass('active');
        }
    }); 
    $(document).on('click','.close',function(){
      $("#mp-add-money-form")[0].reset();
      $("#mp-add-bank-acc-form")[0].reset();
      $("#mp-upload-document-form")[0].reset();
      $("#mp-cashout-form")[0].reset();
      $("#mp-refund-form")[0].reset();
      $('label.error,.error').html('');
    });
    $(document).on('change','#page_cnt',function(){
      $('#mp-transaction-link-page').submit();
    });
    $("#Kyc_doc").on("change", function () {
       showProcessingOverlay();
       $('.kyc_doc_file_err,.error').html('');
       var file_name       = this.files[0].name;
       var explode         = file_name.split('.');
       var file_extension  = explode['1'];
       var flag = 0;
       if(file_extension != 'pdf'  &&
          file_extension != 'jpeg' &&
          file_extension != 'jpg'  &&
          file_extension != 'gif'  &&
          file_extension != 'png'){
          $('.kyc_doc_file_err').css('color','red');
          $('.kyc_doc_file_err').html('{{trans('common/wallet/text.text_image_should_be')}}');
          flag = 1;
       } 
       if(this.files[0].size > 7000000) {
          $('.kyc_doc_file_err').css('color','red');
          $('.kyc_doc_file_err').html('{{trans('common/wallet/text.text_please_upload_file')}}');
          flag = 1;
       }
       if(flag == 1){
         hideProcessingOverlay(); 
         $(this).val('');
         return false;
       } else {
        hideProcessingOverlay(); 
        return true; 
       }
    }); 
    $("#bank_type").on("change", function () {
      var bank_type = $(this).val();
      if(bank_type == 'IBAN'){
        $('#for-iban-account').show();
        $('#for-gb-account').hide();
        $('#for-us-account').hide();
        $('#for-ca-account').hide();
        $('#for-other-account').hide();
        $("#mp-add-bank-acc-form")[0].reset();
        $('#bank_type option[value=IBAN]').attr('selected','selected');
      } else if(bank_type == 'GB'){
        $('#for-iban-account').hide();
        $('#for-gb-account').show();
        $('#for-us-account').hide();
        $('#for-ca-account').hide();
        $('#for-other-account').hide(); 
        $("#mp-add-bank-acc-form")[0].reset();
        $('#bank_type option[value=GB]').attr('selected','selected');
      } else if(bank_type == 'US'){
        $('#for-iban-account').hide();
        $('#for-gb-account').hide();
        $('#for-us-account').show();
        $('#for-ca-account').hide();
        $('#for-other-account').hide();
        $("#mp-add-bank-acc-form")[0].reset();
        $('#bank_type option[value=US]').attr('selected','selected');
      } else if(bank_type == 'CA'){
        $('#for-iban-account').hide();
        $('#for-gb-account').hide();
        $('#for-us-account').hide();
        $('#for-ca-account').show();
        $('#for-other-account').hide();
        $("#mp-add-bank-acc-form")[0].reset();
        $('#bank_type option[value=CA]').attr('selected','selected');
      } else if(bank_type == 'OTHER'){
        $('#for-iban-account').hide();
        $('#for-gb-account').hide();
        $('#for-us-account').hide();
        $('#for-ca-account').hide();
        $('#for-other-account').show();
        $("#mp-add-bank-acc-form")[0].reset();
        $('#bank_type option[value=OTHER]').attr('selected','selected');
      } 
    });
 });    
</script> 

<script>
    $(".wallet-dropdwon-head").on("click", function(){
        $(this).parent(".wallet-dropdwon-section").toggleClass("dropdown-open-main")
    });
    $(".currency-li").on("click", function(){
        $(this).parent().parent().parent().parent(".wallet-dropdwon-section").removeClass("dropdown-open-main")
    });    
    $(".currency1").on("click", function(){
        $(".dashboard-box").addClass("section-1-open").removeClass("section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency2").on("click", function(){
        $(".dashboard-box").addClass("section-2-open").removeClass("section-1-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency3").on("click", function(){
        $(".dashboard-box").addClass("section-3-open").removeClass("section-1-open section-2-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency4").on("click", function(){
        $(".dashboard-box").addClass("section-4-open").removeClass("section-1-open section-2-open section-3-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency5").on("click", function(){
        $(".dashboard-box").addClass("section-5-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency6").on("click", function(){
        $(".dashboard-box").addClass("section-6-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency7").on("click", function(){
        $(".dashboard-box").addClass("section-7-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency8").on("click", function(){
        $(".dashboard-box").addClass("section-8-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency9").on("click", function(){
        $(".dashboard-box").addClass("section-9-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency10").on("click", function(){
        $(".dashboard-box").addClass("section-10-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency11").on("click", function(){
        $(".dashboard-box").addClass("section-11-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-12-open section-13-open section-14-open")
    });    
    $(".currency12").on("click", function(){
        $(".dashboard-box").addClass("section-12-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-13-open section-14-open")
    });    
    $(".currency13").on("click", function(){
        $(".dashboard-box").addClass("section-13-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-14-open")
    });    
    $(".currency14").on("click", function(){
        $(".dashboard-box").addClass("section-14-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open")
    });
</script>
      
@stop