@extends('client.layout.master')                
@section('main_content')

<style>
    .transaction-table-main-section ul.dropdown-menu.dash-menus-drop{width: 100px;height: 250px;overflow: auto}
    .transaction-table-main-section ul.dropdown-menu.dash-menus-drop li{padding: 5px;border-bottom: 1px solid #dddddd}
/*    table.dataTable tbody th, table.dataTable tbody td{white-space: nowrap}*/
</style>

<div class="col-sm-7 col-md-8 col-lg-9 recent-transactions dashboard-box section-1-open" style="padding: 15px;">
  <div class="dash-user-details">
      <div class="title">
        <i class="fa fa-credit-card-alt" aria-hidden="true"></i> {{trans('common/wallet/text.text_recent_transactions')}}
        <div class="col-sm-2 col-md-2 col-lg-2 pull-right">
          <form id="mp-transaction-link-page" action="{{url('/client/wallet/transactions')}}" method="get">
            <!-- {{csrf_field()}} -->
<!--
            <select  name="page_cnt" class="select-box" id="page_cnt" style="font-size: smaller;">
                @if(isset($_REQUEST['page_cnt']) && $_REQUEST['page_cnt'] != "")
                  @php $active_page = $_REQUEST['page_cnt']; @endphp
                @else
                  @php $active_page = $mangopay_transaction_pagi_links_count; @endphp
                @endif
                @for($i=1;$i <= $active_page;$i++)
                @php $nxt = ($i*config('app.project.pagi_cnt')); @endphp
                <option value="{{$i}}" @if($i == $active_page) selected="selected" @endif>{{$nxt-config('app.project.pagi_cnt')}} - {{$nxt}}</option>
                @endfor
            </select>
-->
             <div class="btn-group transaction-table-main-section">
                 <button data-toggle="dropdown" class="btn btn-warning parcel lag dropdown-toggle " type="button">Wallet Currency</button>
                 <button data-toggle="dropdown" class="btn btn-warning dropdown-toggle drp-btn dropdown-toggle block-drop-arrow" type="button">
                     <span class="caret top_caret"></span>
                     <span class="sr-only">Toggle Dropdown</span>
                 </button>
                 <ul class="dropdown-menu top_drop sien fnts right-arrw dash-menus-drop">
                     <div class="arrow-up-mn"> <img src="http://192.168.1.65/archexpert/public/front/images/arrows-tp.png" alt=""></div>
                     <li class="currency-li currency1">USD </li>
                     <li class="currency-li currency2">EUR </li>
                     <li class="currency-li currency3">GBP </li>
                     <li class="currency-li currency4">PLN </li>
                     <li class="currency-li currency5">CHF </li>
                     <li class="currency-li currency6">NOK </li>
                     <li class="currency-li currency7">SEK </li>
                     <li class="currency-li currency8">DKK </li>
                     <li class="currency-li currency9">CAD </li>
                     <li class="currency-li currency10">ZAR </li>
                     <li class="currency-li currency11">AUD </li>
                     <li class="currency-li currency12">HKD </li>
                     <li class="currency-li currency13">CZK </li>
                     <li class="currency-li currency14">JPY </li>
                 </ul>
             </div>
          </form>
        </div>
      </div>

      <div class="user-details-section">
        @if(isset($mangopay_transaction_details) && !empty($mangopay_transaction_details))
        @foreach($mangopay_transaction_details as $index=>$data)
        <div class="table-section-main table-{{$index+1}}">
          <table id="TaBle" class="theme-table invoice-table-s table" style="border: 1px solid rgb(239, 239, 239); margin-bottom:0;">
             <thead class="tras-client-tbl">
                <tr>
                   <th>{{trans('common/wallet/text.text_creation')}}</th>
                   <th>{{trans('common/wallet/text.text_id')}}</th>
                   <th>{{trans('common/wallet/text.text_type')}}</th>
                   <th>Debited Amount</th>
                   <th>Credited Amount</th>
                   <th>{{trans('common/wallet/text.text_status')}}</th>
                   <th>{{trans('common/wallet/text.text_result')}}</th>
                </tr>
             </thead>
             <tbody>
                @if(isset($mangopay_transaction_details[$index]) && count($mangopay_transaction_details[$index])>0)
                  @if($mangopay_transaction_details[$index]!=false)
                  @foreach(array_reverse($mangopay_transaction_details[$index]) as $transaction)
                    <tr>
                       <td>
                           {{date('Y-m-d',$transaction->CreationDate)}} <br> 
                           {{date('h:i:s',$transaction->CreationDate)}} <br>
                           {{date('a',$transaction->CreationDate)}}
                       </td>
                       <td>{{$transaction->Id or '-'}}</td>
                       <td width="15%">
                        {{$transaction->Type or '-'}}
                        @if(isset($transaction->Type) && $transaction->Type == 'TRANSFER' && isset($transaction->CreditedWalletId) && $transaction->CreditedWalletId == $mangopay_wallet_details[$index]->Id)
                        <br><i class="fa fa-sign-in" style="color:green;" aria-hidden="true"> Credited</i> 
                        @elseif(isset($transaction->Type) && $transaction->Type == 'TRANSFER' && isset($transaction->DebitedWalletId) && $transaction->DebitedWalletId == $mangopay_wallet_details[$index]->Id)
                        <br><i class="fa fa-sign-out"style="color:red;" aria-hidden="true"> Debited</i> 
                        @endif
                       </td>
                       <td><span @if($transaction->Status == 'SUCCEEDED') style="color:green" @else style="color:red" @endif>{{ isset($transaction->DebitedFunds->Amount)? $transaction->DebitedFunds->Amount/100:'0'}}</span> <b>{{$transaction->DebitedFunds->Currency or 'USD'}}</b> <br>incl. {{ isset($transaction->Fees->Amount)? $transaction->Fees->Amount/100:'0'}} fees </td>
                       <td><span @if($transaction->Status == 'SUCCEEDED') style="color:green" @else style="color:red" @endif>{{ isset($transaction->CreditedFunds->Amount)? $transaction->CreditedFunds->Amount/100:'0'}}</span> <b>{{$transaction->DebitedFunds->Currency or 'USD'}}</b></td>
                       <td @if($transaction->Status == 'SUCCEEDED') style="color:green" @else style="color:red" @endif>{{$transaction->Status or '-'}}</td>
                        <td width="30%">{{$transaction->ResultCode or '-'}} :{{$transaction->ResultMessage or '-'}}  <br><i><b>Tag: {{$transaction->Tag or '-'}}</b></i></td>
                    </tr>
                  @endforeach
                  @endif
                @else
                <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;" align="center">
                  <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                     <div class="search-content-block">
                        <div class="no-record">
                            {{ trans('expert/transactions/packs.text_sorry_no_records_found')}}                           
                          </div>
                        </div>
                     </td>
                 </tr>
                @endif
             </tbody>
          </table>
          </div>
          @endforeach
        @else
          -- {{trans('common/wallet/text.text_transactions_note')}} --
        @endif  
      </div>
  </div>
</div>

<script type="text/javascript">

$(document).on('change','#page_cnt',function(){
   $('#mp-transaction-link-page').submit();
 });

</script> 

<script>
    $(".wallet-dropdwon-head").on("click", function(){
        $(this).parent(".wallet-dropdwon-section").toggleClass("dropdown-open-main")
    });
    $(".currency-li").on("click", function(){
        $(this).parent().parent().parent().parent(".wallet-dropdwon-section").removeClass("dropdown-open-main")
    });    
    $(".currency1").on("click", function(){
        $(".dashboard-box").addClass("section-1-open").removeClass("section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency2").on("click", function(){
        $(".dashboard-box").addClass("section-2-open").removeClass("section-1-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency3").on("click", function(){
        $(".dashboard-box").addClass("section-3-open").removeClass("section-1-open section-2-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency4").on("click", function(){
        $(".dashboard-box").addClass("section-4-open").removeClass("section-1-open section-2-open section-3-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency5").on("click", function(){
        $(".dashboard-box").addClass("section-5-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency6").on("click", function(){
        $(".dashboard-box").addClass("section-6-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency7").on("click", function(){
        $(".dashboard-box").addClass("section-7-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency8").on("click", function(){
        $(".dashboard-box").addClass("section-8-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-9-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency9").on("click", function(){
        $(".dashboard-box").addClass("section-9-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-10-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency10").on("click", function(){
        $(".dashboard-box").addClass("section-10-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-11-open section-12-open section-13-open section-14-open")
    });    
    $(".currency11").on("click", function(){
        $(".dashboard-box").addClass("section-11-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-12-open section-13-open section-14-open")
    });    
    $(".currency12").on("click", function(){
        $(".dashboard-box").addClass("section-12-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-13-open section-14-open")
    });    
    $(".currency13").on("click", function(){
        $(".dashboard-box").addClass("section-13-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-14-open")
    });    
    $(".currency14").on("click", function(){
        $(".dashboard-box").addClass("section-14-open").removeClass("section-1-open section-2-open section-3-open section-4-open section-5-open section-6-open section-7-open section-8-open section-9-open section-10-open section-11-open section-12-open section-13-open")
    });
</script>   
@stop