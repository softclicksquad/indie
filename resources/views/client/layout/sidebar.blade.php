<?php
  $currentUrl   = Route::getCurrentRoute()->getPath();
  $user         = Sentinel::check(); 
  $sidebar_data = array();
  if($user)
  {
    $profile_img_public_path = url('/public').config('app.project.img_path.profile_image');
    $sidebar_data            = sidebar_information($user->id);
  }
  //$wallet_bal = 0;
  //$mangopay_wallet_details = get_logged_user_wallet_details();
  //$wallet_bal = isset($mangopay_wallet_details->Balance->Amount)? $mangopay_wallet_details->Balance->Amount/100:'0';
  //$wallet_currency = isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:'';


  $project_type  = "";   
  $url             = \URL::current();
  $arr_url       = [];
  $check_current_url = ['client/projects/posted',
                        'client/projects/awarded',
                        'client/projects/ongoing',
                        'client/projects/open',
                        'client/projects/completed',
                        'client/projects/canceled',
                        'client/contest/posted'];
  if($url != "" && !in_array($currentUrl, $check_current_url))
  {  
    $arr_url =  explode('/', $url);
    if(isset($arr_url[6]) && $arr_url[6] == 'projects' && isset($arr_url[7]) && $arr_url[7] == 'posted'){
      $project_type = 'proposted';   
    }
    if(isset($arr_url[6]) && $arr_url[6] == 'projects' && isset($arr_url[7]) && $arr_url[7] == 'awarded'){
      $project_type = 'awarded';   
    }
    else if(isset($arr_url[6]) && $arr_url[6] == 'projects' && isset($arr_url[7]) && $arr_url[7] == 'ongoing'){
      $project_type = 'ongoing';   
    }
    else if(isset($arr_url[6]) && $arr_url[6] == 'projects' && isset($arr_url[7]) && $arr_url[7] == 'open'){
      // $project_type = 'open';   
    }
    else if(isset($arr_url[6]) && $arr_url[6] == 'projects' && isset($arr_url[7]) && $arr_url[7] == 'completed'){
      $project_type = 'completed';   
    }
    else if(isset($arr_url[6]) && $arr_url[6] == 'projects' && isset($arr_url[7]) && $arr_url[7] == 'canceled'){
      $project_type = 'canceled';   
    }
    else if(isset($arr_url[6]) && $arr_url[6] == 'contest' && isset($arr_url[7]) && $arr_url[7] == 'posted'){
      $project_type = 'cnposted';   
    }
  }
?>
{{-- Top Navbar --}}
<div class="top-title-pattern pattern-title-top top-links project-status-strip">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="title-box-top">
                    <!--<div id="region_id" class="meshows hidden-lg hidden-md">Projects </div>    -->
                    <h4 class="visible-xs">{{ trans('common/client_sidebar.text_projects') }} <span class="arrow"><i class="fa fa-angle-down"></i></span></h4>
                    <ul>
                        <!--id="reglon_tab" class="none-dpl"-->
                        <li @if($currentUrl=='client/projects/posted' ) class="top-menu-active" ) class="top-menu-active" @endif>
                            <a @if($currentUrl=='client/projects/posted' ) style="color: #000;" @elseif($project_type=='proposted' && Request::segment(2)=='projects' ) style="color: #000;" @endif href="{{url('/')}}/client/projects/posted">{{ trans('common/client_sidebar.text_posted') }}
                                <button class="btn-prj-count" type="button">
                                    @if(isset($sidebar_data['posted_project_count']) && $sidebar_data['posted_project_count'])
                                    @if($sidebar_data['posted_project_count'] >99)
                                    @php $posted_project_count = '99+'; @endphp
                                    @else
                                    @php $posted_project_count = $sidebar_data['posted_project_count']; @endphp
                                    @endif
                                    @else
                                    @php $posted_project_count = 0; @endphp
                                    @endif
                                    {{$posted_project_count}}
                                </button>
                            </a>
                        </li>
                        <li @if($currentUrl=='client/projects/awarded' ) class="top-menu-active" @elseif($project_type=='awarded' ) class="top-menu-active" @endif>
                            <a @if($currentUrl=='client/projects/awarded' ) style="color: #000;" @elseif($project_type=='awarded' ) style="color: #000;" @endif href="{{url('/')}}/client/projects/awarded">{{ trans('common/client_sidebar.text_awarded') }}
                                <button class="btn-prj-count" type="button">
                                    @if(isset($sidebar_data['awarded_project_count']) && $sidebar_data['awarded_project_count'])
                                    @if($sidebar_data['awarded_project_count'] >99)
                                    @php $awarded_project_count = '99+'; @endphp
                                    @else
                                    @php $awarded_project_count = $sidebar_data['awarded_project_count']; @endphp
                                    @endif
                                    @else
                                    @php $awarded_project_count = 0; @endphp
                                    @endif
                                    {{$awarded_project_count}}
                                </button>
                            </a>
                        </li>

                        <li @if($currentUrl=='client/projects/ongoing' ) class="top-menu-active" @elseif($project_type=='ongoing' ) class="top-menu-active" @endif>
                            <a @if($currentUrl=='client/projects/ongoing' ) style="color: #000;" @elseif($project_type=='ongoing' ) style="color: #000;" @endif href="{{url('/client/projects/ongoing')}}">{{ trans('common/client_sidebar.text_ongoing') }}
                                <button class="btn-prj-count" type="button">
                                    @if(isset($sidebar_data['ongoing_project_count']) && $sidebar_data['ongoing_project_count'])
                                    @if($sidebar_data['ongoing_project_count'] >99)
                                    @php $ongoing_project_count = '99+'; @endphp
                                    @else
                                    @php $ongoing_project_count = $sidebar_data['ongoing_project_count']; @endphp
                                    @endif
                                    @else
                                    @php $ongoing_project_count = 0; @endphp
                                    @endif
                                    {{$ongoing_project_count}}
                                </button></a>
                        </li>

                        <li @if($currentUrl=='client/projects/completed' ) class="top-menu-active" @elseif($project_type=='completed' ) class="top-menu-active" @endif>
                            <a @if($currentUrl=='client/projects/completed' ) style="color: #000;" @elseif($project_type=='completed' ) style="color: #000;" @endif href="{{url('/client/projects/completed')}}">{{ trans('common/client_sidebar.text_completed') }}
                                <button class="btn-prj-count" type="button">
                                    @if(isset($sidebar_data['completed_project_count']) && $sidebar_data['completed_project_count'])
                                    @if($sidebar_data['completed_project_count'] >99)
                                    @php $completed_project_count = '99+'; @endphp
                                    @else
                                    @php $completed_project_count = $sidebar_data['completed_project_count']; @endphp
                                    @endif
                                    @else
                                    @php $completed_project_count = 0; @endphp
                                    @endif
                                    {{$completed_project_count}}
                                </button>
                            </a>
                        </li>

                        <li @if($currentUrl=='client/projects/canceled' ) class="top-menu-active" @elseif($project_type=='canceled' ) class="top-menu-active" @endif>
                            <a @if($currentUrl=='client/projects/canceled' ) style="color: #000;" @elseif($project_type=='canceled' ) style="color: #000;" @endif href="{{url('/client/projects/canceled')}}">{{ trans('common/client_sidebar.text_canceled') }}
                                <button class=" btn-prj-count" type="button">
                                    @if(isset($sidebar_data['canceled_project_count']) && $sidebar_data['canceled_project_count'])
                                    @if($sidebar_data['canceled_project_count'] >99)
                                    @php $canceled_project_count = '99+'; @endphp
                                    @else
                                    @php $canceled_project_count = $sidebar_data['canceled_project_count']; @endphp
                                    @endif
                                    @else
                                    @php $canceled_project_count = 0; @endphp
                                    @endif
                                    {{$canceled_project_count}}
                                </button>
                            </a>
                        </li>

                        <li @if($currentUrl=='client/contest/posted' ) class="top-menu-active" ) class="top-menu-active" @endif>
                            <a @if($currentUrl=='client/contest/posted' || Request::segment(2)=='contest' ) style="color: #000;" @endif href="{{url('/client/contest/posted')}}">{{trans('client/contest/post.text_contests')}}
                                <button class=" btn-prj-count" type="button">
                                    @if(isset($sidebar_data['contest_posted_count']) && $sidebar_data['contest_posted_count'])
                                    @if($sidebar_data['contest_posted_count'] >99)
                                    @php $contest_posted_count = '99+'; @endphp
                                    @else
                                    @php $contest_posted_count = $sidebar_data['contest_posted_count']; @endphp
                                    @endif
                                    @else
                                    @php $contest_posted_count = 0; @endphp
                                    @endif
                                    {{$contest_posted_count or ''}}
                                </button>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@php $user = Sentinel::check(); @endphp
@if($user)
@php
$sidebar_information =sidebar_information($user->id);
$profile_img_public_path = url('/').config('app.project.img_path.profile_image');
@endphp
@endif

<div class="middle-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-md-3 col-lg-3">
                <div class="left-sidebar user-left-bar-main">
                    <div class="user-profile">

                       {{--  <div class="profile-circle">
                            @if(isset($user_auth_details['is_online']) && $user_auth_details['is_online']!='' && $user_auth_details['is_online'] == '1')
                            <div class="user-online-round"></div>
                            @else
                            <div class="user-offline-round"></div>
                            @endif

                            <div class="client_image client-show-photo"> <input type="file" id="profile_image" class="file">

                                @if(isset($profile_img_public_path) && isset($sidebar_information['user_details']['profile_image']) && $sidebar_information['user_details']['profile_image']!="" && file_exists('public/uploads/front/profile/'.$sidebar_information['user_details']['profile_image']))
                                <img id="set_image" style="cursor: default;" src="{{isset($sidebar_information['user_details']['profile_image'])?$profile_img_public_path.$sidebar_information['user_details']['profile_image']:''}}" />
                                @else
                                <img id="set_image" style="cursor: default;" src="{{$profile_img_public_path.'default_profile_image.png'}}" />
                                @endif
                                @if( 'profile' == \Request::segment(2) )
                                <div class="client-photo__upload-icon" onclick="openCropModal()">
                                    <span class="client-show-icon icon-edit_white"><img src="{{url('/public')}}/front/images/edit-file.png" alt="" />
                                    <div class="edit-profile-name">{{trans('new_translations.edit')}}</div>
                                    </span>
                                </div>
                                @endif
                            </div>
                        </div> --}}

                        <div class="profile-name">
                            <span class="profile-new-name"> {{isset($sidebar_data['user_details']['first_name'])?$sidebar_data['user_details']['first_name']:'Unknown user'}}
                                @if(isset($sidebar_data['user_details']['last_name']) && $sidebar_data['user_details']['last_name'] !="") @php echo substr($sidebar_data['user_details']['last_name'],0,1).'.'; @endphp @endif </span>

                            @if(isset($user_auth_details['kyc_verified']) && $user_auth_details['kyc_verified'] == '1')
                            <span class="verfy-new-arrow">
                                <a href="#" data-toggle="tooltip" title="Identity Verified"><img src="{{url('/public')}}/front/images/verifild.png" alt=""> </a>
                            </span>
                            @endif

                        </div>
                        <div class="city_map_detail priminum_number" title="{{isset($sidebar_data['client_timezone'])?$sidebar_data['client_timezone']:'Not specify'}}">
                            <span class="flag-image">
                                @if(isset($sidebar_data['user_country_flag']))
                                @php
                                echo $sidebar_data['user_country_flag'];
                                @endphp
                                @elseif(isset($sidebar_data['user_country']))
                                @php
                                echo $sidebar_data['user_country'];
                                @endphp
                                @else
                                @php
                                echo 'Not Specify';
                                @endphp
                                @endif
                            </span>
                            {{-- trans('common/_expert_details.text_local_time') --}} {{isset($sidebar_data['client_timezone_without_date'])?$sidebar_data['client_timezone_without_date']:'Not specify'}}
                        </div>
                        <div class="city_map_detail priminum_number">
                            {{isset($sidebar_data['user_type'])?ucfirst($sidebar_data['user_type']):''}} {{'Client'}}
                        </div>



                    </div>
                    <div class="rating-profile">
                        <div class="rating_profile">
                            <div class="rating-title">{{ trans('common/client_sidebar.text_rating') }} <b class="rate-t sidebar-opt-value">
                                    <div class="rating-list"><span class="stars">{{isset($sidebar_data['average_rating'])?$sidebar_data['average_rating']:'0'}}</span></div>
                                    ({{isset($sidebar_data['average_rating'])?$sidebar_data['average_rating']:'0'}})
                                </b> </div>

                        </div>
                        <div class="rating_profile">
                            <div class="rating-title">{{ trans('common/client_sidebar.text_projects') }} : <b>{{isset($sidebar_data['project_count'])?$sidebar_data['project_count']:'0'}}</b></div>
                        </div>
                       
                        <div class="rating_profile">
                            <div class="rating-title">{{ trans('common/client_sidebar.text_ongoing_projects') }} : <b>{{isset($sidebar_data['ongoing_project_count'])?$sidebar_data['ongoing_project_count']:'0'}}</b></div>
                        </div>

                        <div class="rating_profile">
                            <div class="rating-title">{{ trans('common/client_sidebar.text_completed_projects') }} : <b>{{isset($sidebar_data['completed_project_count'])?$sidebar_data['completed_project_count']:'0'}}</b></div>
                        </div>


                        <div class="rating_profile">
                            <div class="rating-title">{{ trans('common/client_sidebar.text_review') }} : <b>{{isset($sidebar_data['review_count'])?$sidebar_data['review_count']:'0'}}</b></div>
                        </div>
                        <div class="rating_profile">
                            <!-- <div class="rating-title">{{ trans('common/client_sidebar.text_spend') }}&nbsp;(USD/EUR)</div> -->
                            <div class="rating-title">{{ trans('common/client_sidebar.text_spend') }}&nbsp;(USD) : <b>{{isset($sidebar_data['total_project_cost'])?$sidebar_data['total_project_cost']:'0'}}</b></div>
                        </div>
                        <div class="rating_profile">
                            <div class="rating-title">{{ trans('common/client_sidebar.text_reputation') }} : <b>{{isset($sidebar_data['reputation'])?$sidebar_data['reputation']:'0.0'}}%</b></div>
                        </div>
                        <div class="rating_profile">
                            <div class="rating-title">{{ trans('common/client_sidebar.text_availability_status') }} : <b>@if(isset($sidebar_data['is_available']) && $sidebar_data['is_available']==1)
                                    {{ trans('common/client_sidebar.text_availability_yes') }}
                                    @elseif(isset($sidebar_data['is_available']) && $sidebar_data['is_available']==0)
                                    {{ trans('common/client_sidebar.text_availability_no') }}
                                    @endif</b></div>
                        </div>
                        
                    </div>
                    <div class="left-side-tabs">
                        <div class="dashboard-menu-head">Dashboard Menu <span><i class="fa fa-angle-down"></i></span></div>
                        <ul class="responsive-menu-hide-section">
                            <li @if($currentUrl=='client/dashboard' ) class="active-p" @endif>
                                <a href="{{url('/')}}/client/dashboard" class="dashboards-p">{{ trans('common/client_sidebar.text_dashboard') }}</a></li>
                            <li>
                                <a class="acc-setting"> User Setting <span class="arrow"><i @if($currentUrl=='client/profile' || $currentUrl=='client/availability' ) class="fa fa-angle-up" @else class="fa fa-angle-down" @endif></i></span></a>
                                <ul class="sub-menu" @if($currentUrl=='client/profile' || $currentUrl=='client/change_password' || $currentUrl=='client/availability' ) style="display:block;" @else style="display:none" @endif>
                                    <li @if($currentUrl=='client/profile' ) class="active-p" @endif><a href="{{url('/')}}/client/profile" class="edit-profile3">{{ trans('common/client_sidebar.text_edit_profile') }} </a> </li>
                                    {{-- <li @if($currentUrl=='client/change_password' ) class="active-p" @endif><a href="{{url('/client/change_password')}}" class="change-p-3">{{ trans('common/client_sidebar.text_change_password') }}</a></li> --}}
                                    <li @if($currentUrl=='client/availability' ) class="active-p" @endif><a href="{{url('/client/availability')}}" class="availability-p">{{ trans('common/client_sidebar.text_availability') }}</a></li>

{{--                                     <li @if($currentUrl=='client/availability' ) class="active-p" @endif>
                                        <a href="#" class="transactions-icns">Payment and financials</a></li> --}}
                                        @if(isset($user->mp_wallet_created) && $user->mp_wallet_created == 'Yes')
                                        <li @if($currentUrl=='client/wallet/dashboard' ) class="active-p" @endif><a href="{{url('/client/wallet/dashboard')}}" class="mangopay-p"><img style="width: 27px;height: 25px;" src="{{url('/public/front/images/archexpertdefault/wallet.png')}}" alt="mangopay"> Payment and financials</a></li>
                                        @endif

                                        <li @if($currentUrl=='client/wallet/transactions' ) class="active-p" @endif><a href="{{url('/client/wallet/transactions')}}" class="mangopay-p"><img style="width: 27px;height: 25px;" src="{{url('/public/front/images/archexpertdefault/wallet.png')}}" alt="mangopay"> Mangopay Transactions</a></li>

                                </ul>
                            </li>

                            <li>
                                <a class="ongoing-projects-icon">{{ trans('common/client_sidebar.text_my_projects') }} <span class="arrow"><i @if($currentUrl=='client/projects/posted' || $currentUrl=='client/projects/open' || $currentUrl=='client/projects/awarded' || $currentUrl=='client/projects/ongoing' || $currentUrl=='client/projects/completed' || $currentUrl=='client/projects/canceled' ) class="fa fa-angle-up" @else class="fa fa-angle-down" @endif></i></span></a>
                                <ul class="sub-menu" @if($currentUrl=='client/projects/posted' || $currentUrl=='client/projects/open' || $currentUrl=='client/projects/awarded' || $currentUrl=='client/projects/ongoing' || $currentUrl=='client/projects/completed' || $currentUrl=='client/projects/canceled' ) style="display:block;" @else style="display:none" @endif>
                                    <li @if($currentUrl=='client/projects/posted' ){ class="active-p" @endif><a href="{{url('/')}}/client/projects/posted" class="applied-p">{{ trans('common/client_sidebar.text_posted_projects') }}</a></li>
                                    {{-- <li @if($currentUrl=='client/projects/open') class="active-p" @endif><a href="{{url('/client/projects/open')}}" class="ongoing-pd-3">{{ trans('common/client_sidebar.text_open_projects') }}</a>
                            </li> --}}
                            <li @if($currentUrl=='client/projects/awarded' ) class="active-p" @endif><a href="{{url('/client/projects/awarded')}}" class="completed-p-4">{{ trans('common/client_sidebar.text_awarded_projects') }}</a></li>
                            <li @if($currentUrl=='client/projects/ongoing' ) class="active-p" @endif><a href="{{url('/client/projects/ongoing')}}" class="ongoing-projects-icon">{{ trans('common/client_sidebar.text_ongoing_projects') }}</a></li>
                            <li @if($currentUrl=='client/projects/completed' ) class="active-p" @endif><a href="{{url('/client/projects/completed')}}" class="completed-p-4">{{ trans('common/client_sidebar.text_completed_projects') }}</a></li>
                            <li @if($currentUrl=='client/projects/canceled' ) class="active-p" @endif><a href="{{url('/client/projects/canceled')}}" class="cancell-p">{{ trans('common/client_sidebar.text_canceled_projects') }}</a></li>
                        </ul>
                        </li>

                        <li>
                            <a class="contest-icon">{{trans('client/contest/post.text_contests')}} <span class="arrow"><i @if($currentUrl=='client/contest/posted' || Request::segment(2)=='contest' ) class="fa fa-angle-up" @else class="fa fa-angle-down" @endif></i></span></a>
                            <ul class="sub-menu" @if($currentUrl=='client/contest/posted' || Request::segment(2)=='contest' ) style="display:block;" @else style="display:none" @endif>
                                <li @if($currentUrl=='client/contest/posted' || Request::segment(2)=='posted' ){ class="active-p" @endif><a href="{{url('/')}}/client/contest/posted" class="applied-p">{{trans('client/contest/post.text_posted_contest')}}</a></li>
                                {{-- <li @if($currentUrl=='client/contest/ongoing' || Request::segment(2)=='ongoing' ){ class="active-p" @endif><a href="{{url('/')}}/client/contest/ongoing" class="ongoing-projects-icon"> Ongoing Contests</a></li> --}}
                                <li @if($currentUrl=='client/contest/completed' || Request::segment(2)=='completed' ){ class="active-p" @endif><a href="{{url('/')}}/client/contest/completed" class="completed-p-4">Completed Contests</a></li>
                                <li @if($currentUrl=='client/contest/expired' ) class="active-p" @endif><a href="{{url('/client/contest/expired')}}" class="cancell-p">Canceled Contests</a></li>
                            </ul>
                        </li>

                        @if(isset($user->mp_wallet_created) && $user->mp_wallet_created == 'Yes')
                        <li>
                            <a class="transactions-icns">{{ trans('common/expert_sidebar.text_payment') }} <span class="arrow"><i class="fa fa-angle-down"></i></span></a>
                            <ul class="sub-menu" @if($currentUrl=='client/wallet/dashboard' ) style="display:block;" @else style="display:none" @endif>
                                <li @if($currentUrl=='client/wallet/withdraw' ) class="active-p" @endif><a href="{{url('/client/wallet/withdraw')}}" class="mangopay-p"><img style="width: 27px;height: 25px;" src="{{url('/public/front/images/archexpertdefault/wallet.png')}}" alt="mangopay">&nbsp;&nbsp; Withdraw</a></li>

                                {{-- <li @if($currentUrl=='client/wallet/dashboard' ) class="active-p" @endif><a href="{{url('/client/wallet/dashboard')}}" class="mangopay-p"><img style="width: 27px;height: 25px;" src="{{url('/public/front/images/archexpertdefault/wallet.png')}}" alt="mangopay">&nbsp;&nbsp; Withdraw</a></li> --}}


                                <li @if($currentUrl=='client/transactions' ) class="active-p" @endif><a href="{{url('/client/transactions')}}" class="transactions-icns">{{ trans('common/client_sidebar.text_transactions') }}</a></li>
                            </ul>
                        </li>
                        @endif

                        <li @if($currentUrl=='client/projects/dispute' ) class="active-p" @endif><a href="{{url('/client/projects/dispute')}}" class="dispute-icn">{{ trans('common/client_sidebar.text_dispute') }}</a></li>
                        <li @if($currentUrl=='client/twilio-chat/chat_list' ) class="active-p" @endif>
                            <a href="{{url('client/twilio-chat/chat_list')}}" class="inbox-p-8">{{ trans('common/client_sidebar.text_messages') }}</a>
                            {{-- <a href="javascript:void(0)" class="applozic-launcher inbox-p-8">{{ trans('common/client_sidebar.text_messages') }}</a> --}}
                        </li>

                        <!-- Support Ticket -->
                        <li @if($currentUrl=='client/tickets' ) class="active-p" @endif><a href="{{url('/client/tickets')}}" class="support-ticket-icn">
                            {{ trans('common/common.support_ticket') }}
                        </a></li>
                        <!-- Support Ticket -->

                        {{-- <li>
                            <a class="transactions-icns">{{ trans('common/expert_sidebar.support') }} Support <span class="arrow"><i class="fa fa-angle-down"></i></span></a>
                            <ul class="sub-menu">
                                <li class="active-p"><a href="{{url('/client/support/')}}" class="mangopay-p">
                                    <img style="width: 27px;height: 25px;" src="{{url('/public/front/images/archexpertdefault/wallet.png')}}" alt="mangopay">&nbsp;&nbsp; Manage</a></li>
                                <li @if($currentUrl=='client/support/create' ) class="active-p" @endif><a href="{{url('/client/support/create')}}" class="transactions-icns">Add</a></li>
                            </ul>
                        </li> --}}

                        </ul>
                    </div>
                </div>
            </div>

            <script>
                $('.title-box-top h4').click(function() {
                    $(this).next('ul').slideToggle();
                    $(this).find('.arrow i').toggleClass('fa-angle-down fa-angle-up');
                });
            </script>
            <script>
                $('.left-side-tabs li').click(function() {
                    $(this).children('.sub-menu').slideToggle();
                    $(this).children('a').find('.arrow i').toggleClass('fa-angle-down fa-angle-up');
                    $(this).siblings().find(".sub-menu").slideUp("slow");
                    $(this).siblings().children('a').find('.arrow i').removeClass('fa-angle-up').addClass("fa-angle-down");
                });
            </script>
            {{-- <script type="text/javascript">
                $('#image').on('click', function() {
                    $('#profile_image').click();
                });
                $('#profile_image').change(function() {
                    var formData = new FormData();
                    formData.append('file', $('input[type=file]')[0].files[0]);
                    $.ajax({
                        url: '{{ url(' / ') }}/client/store_profile_image?_token=' + "{{ csrf_token()}}",
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            if (response.status == "SUCCESS") {
                                $('#set_image').attr("src", response.image_name);
                                console.log(response.msg);
                            } else if (response.status == "ERROR") {
                                console.log(response.msg);
                            }
                        }
                    });
                });
            </script> --}}

            <script>
                $(document).ready(function() {
                    $('[data-toggle="tooltip"]').tooltip();
                });
            </script>

            <script>
                $(".dashboard-menu-head").on("click", function() {
                    $(".responsive-menu-hide-section").slideToggle("slow");
                    $(this).toggleClass("active");
                })
            </script>