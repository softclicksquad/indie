<!-- HEader -->        
@include('front.layout.header')    
        
<!-- BEGIN Sidebar -->

<!-- END Sidebar -->

<!-- BEGIN Content -->

    @yield('main_content')

    <!-- END Main Content -->

<!-- Footer -->    

@include('client.layout.dynamic_footer') 

@include('front.layout.footer')    
                
              