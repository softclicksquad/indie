@extends('client.layout.master')
@section('main_content')
<style type="text/css">
    .table td a{color: #838383;}
    .sort-by{border: 1px solid #ddd;margin: 20px 0 -31px;;max-width: 75px;width: 100%;border-radius: 3px;position: relative;z-index: 1;}
    /*file input css start here*/
.btn-file input[type=file] {position: absolute;top: 0;right: 0;min-width: 100%;min-height: 100%;text-align: right;opacity: 0;background: none repeat scroll 0 0 transparent;cursor: inherit;display: block;}
.btn.btn-primary.btn-file {background: #2d2d2d;border-color: #2d2d2d;border-radius: 0 ;color: #fff;display: block;height: 40px;float: right;padding-top: 7px;max-width: 92px; width: 100%;position: absolute;right: 0;top: 0;border-radius: 0 3px 3px 0;z-index: 1}
.user-box .input-group {width: 100%;border-radius: 3px;position: relative;border: none}
.ticket-add-tab-section .file { color: #fff;opacity: 1}
.tickets-listing-menus{border-bottom: 1px solid #e1e1e1;}
.tickets-listing-menus li{display: inline-block;vertical-align: middle;position: relative;}
.tickets-listing-menus li:last-child:before{content: "";height: 25px;width: 1px;background: #e1e1e1;position: absolute;right: 0;top: 0;bottom: 0;margin: auto -1px auto 0}
.tickets-listing-menus li:first-child:before{content: "";height: 25px;width: 1px;background: #e1e1e1;position: absolute;left: 0;top: 0;bottom: 0;margin: auto 0px auto -1px}
.tickets-listing-menus li a{color: #333; background: #fff; padding: 10px 15px;font-size: 16px;display: block;}
.tickets-listing-menus li.active{position: relative; z-index: 2; top: 2px; margin-top: -2px; border-bottom: 1px solid #fff; border-top: 1px solid #e1e1e1; border-left: 1px solid #e1e1e1; border-right: 1px solid #e1e1e1;}
/*file input css end here*/
</style>
<!-- <div class="middle-container"> -->
<div class="container">
    <!-- <br/> -->
    <div class="row">
        <div class="col-sm-7 col-md-8 col-lg-9">
            <div class="search-grey-bx white-wrapper" style="min-height: 300px;">
                <div class="head_grn">
<!--                    {{ trans('client/transactions/packs.text_heading') }}-->
                   Your Tickets
                </div>

                <div class="tickets-listing-menus">
                    <ul>
                        <li @if((Request::has('type') && Request::input('type') != 'closed') || !Request::has('type')) class="active" @endif><a href="{{ $module_url_path }}">Open Tickets (0) </a></li>
                        <li @if(Request::has('type') && Request::input('type') == 'closed') class="active" @endif><a href="{{ $module_url_path.'?type=closed' }}">Resolved (12) </a></li>
                    </ul>
                    <div class="create-ticket-btn">
                        <a href="javascript:void(0)" class="btn-create-btn">
                            Create Ticket
                        </a>
                    </div>
                </div>
                @if(isset($arr_tickets) && empty($arr_tickets))
                    @if((Request::has('type') && Request::input('type') != 'closed') || !Request::has('type'))
                    <div class="">
                        <div class="add-ticket-form-link">
                            There are no tickets to display in this tab. <a class="open-add-ticket-form" href="javascript:void(0)">Click here to open a new ticket</a>
                        </div>
                        <div class="ticket-add-tab-section">
                            <div class="head_grn">Add Support Ticket</div>                    
                            <div class="user-box">
                                <div class="p-control-label">Department <span>*</span></div>
                                <div class="input-name">
                                    <div class="droup-select">
                                        <select class="droup" data-rule-required="true" name="country">
                                            <option value="">General</option>
                                            <option value="">General</option>
                                       </select>
                                       <span class='error'></span>
                                    </div>
                                 </div>
                            </div>                
                            <div class="user-box">
                                <div class="p-control-label">Subject <span>*</span></div>
                                <div class="input-name">
                                    <input type="text" class="clint-input" placeholder="" name="first_name" value="" data-rule-required="true" data-rule-maxlength="250"/>
                                </div>
                            </div>                   
                            <div class="user-box">
                                <div class="p-control-label">Message <span>*</span></div>
                                <div class="input-name"><textarea style="padding: 5px;" rows="" cols="" type="text" class="client-taxtarea" placeholder="" name="profile_summary" data-rule-maxlength="500"></textarea> </div>
                            </div>   
                            <div class="user-box">
                                <div class="p-control-label">Subject <span>*</span></div>
                                <div class="input-name">
                                    <div class="upload-block">
                                        <input type="file" id="pdffile" style="visibility:hidden; height: 0;" name="file">
                                        <div class="input-group ">
                                            <input type="text" class="clint-input file-caption  kv-fileinput-caption" placeholder="" id="subfile" />
                                            <div class="btn btn-primary btn-file"><a class="file" onclick="$('#pdffile').click();">Browse...</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>                
                        </div>
                    </div>
                    @endif
                @else
                <div class="table-responsive">
                    <form id="frm_show_cnt" class="form-horizontal show-entry-form" method="POST" action="{{url('client/transactions/show_cnt')}}">
                        {{ csrf_field() }}                                    
                        <div class="sort-by">
                            @php $show_cnt ='show_transaction_cnt';  @endphp
                            @include('front.common.show-cnt-selectbox')
                        </div>                                    
                    </form>
                    <table class="theme-table invoice-table-s table TaBle" style="border: 1px solid rgb(239, 239, 239); margin-bottom:0;">
                        <thead class="tras-client-tbl">
                            <tr>
                                <th>Reference</th>
                                <th>Subject</th>
                                <th>Department</th>
                                <th>Date Created</th>
                                <th>Action</th>                           
                            </tr>
                        </thead>
                        <tbody>
                            <tr role="row" class="odd">
                                <td>INV201910074F89BE</td>
                                <td>Project fee adjust request for <br>Project ID: 17478418</td>
                                <td>General</td>
                                <td>Wed, 8th May 2019 <br>12:16am</td>
                                <td style="text-align: center"><a href="javascript:void(0)"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            <tr role="row" class="even">
                                <td>INV201910074F89BE</td>
                                <td>Project fee adjust request for <br>Project ID: 17478418</td>
                                <td>General</td>
                                <td>Wed, 8th May 2019 <br>12:16am</td>
                                <td style="text-align: center"><a href="javascript:void(0)"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            <tr role="row" class="odd">
                                <td>INV201910074F89BE</td>
                                <td>Project fee adjust request for <br>Project ID: 17478418</td>
                                <td>General</td>
                                <td>Wed, 8th May 2019 <br>12:16am</td>
                                <td style="text-align: center"><a href="javascript:void(0)"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            <tr role="row" class="even">
                                <td>INV201910074F89BE</td>
                                <td>Project fee adjust request for <br>Project ID: 17478418</td>
                                <td>General</td>
                                <td>Wed, 8th May 2019 <br>12:16am</td>
                                <td style="text-align: center"><a href="javascript:void(0)"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            <tr role="row" class="odd">
                                <td>INV201910074F89BE</td>
                                <td>Project fee adjust request for <br>Project ID: 17478418</td>
                                <td>General</td>
                                <td>Wed, 8th May 2019 <br>12:16am</td>
                                <td style="text-align: center"><a href="javascript:void(0)"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            <tr role="row" class="even">
                                <td>INV201910074F89BE</td>
                                <td>Project fee adjust request for <br>Project ID: 17478418</td>
                                <td>General</td>
                                <td>Wed, 8th May 2019 <br>12:16am</td>
                                <td style="text-align: center"><a href="javascript:void(0)"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            <tr role="row" class="odd">
                                <td>INV201910074F89BE</td>
                                <td>Project fee adjust request for <br>Project ID: 17478418</td>
                                <td>General</td>
                                <td>Wed, 8th May 2019 <br>12:16am</td>
                                <td style="text-align: center"><a href="javascript:void(0)"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            <tr role="row" class="even">
                                <td>INV201910074F89BE</td>
                                <td>Project fee adjust request for <br>Project ID: 17478418</td>
                                <td>General</td>
                                <td>Wed, 8th May 2019 <br>12:16am</td>
                                <td style="text-align: center"><a href="javascript:void(0)"><i class="fa fa-eye"></i></a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                @endif
            </div>
        <!-- Paination Links -->
        @include('front.common.pagination_view', ['paginator' => $arr_transactions])
        <!-- Paination Links -->
        </div>
    </div>
</div>
<!-- </div> -->
<link rel="stylesheet" href="{{url('/public')}}/front/css/responsivetabs.css" />
<script src="{{url('/public')}}/front/js/responsivetabs.js"></script>
<script>

    $(document).ready(function ()
    {	
        $(document).on('responsive-tabs.initialised', function (event, el)
        {
            console.log(el);
        });

        $(document).on('responsive-tabs.change', function (event, el, newPanel)
        {
            console.log(el);
            console.log(newPanel);
        });

        $('[data-responsive-tabs]').responsivetabs(
        {
            initialised : function ()
            {
                console.log(this);
            },

            change : function (newPanel)
            {
                console.log(newPanel);
            }
        });
    });
</script>

<script>
    $(".open-add-ticket-form").on("click", function(){
       $(this).parent().siblings(".ticket-add-tab-section").slideToggle("slow"); 
    });
</script>

@stop

