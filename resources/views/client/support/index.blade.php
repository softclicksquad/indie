@extends('client.layout.master')
@section('main_content')
<style type="text/css">
    .table td a{color: #838383;}
    .sort-by{border: 1px solid #ddd;margin: 20px 0 -31px;;max-width: 75px;width: 100%;border-radius: 3px;position: relative;z-index: 1;}
    /*file input css start here*/
.btn-file input[type=file] {position: absolute;top: 0;right: 0;min-width: 100%;min-height: 100%;text-align: right;opacity: 0;background: none repeat scroll 0 0 transparent;cursor: inherit;display: block;}
.btn.btn-primary.btn-file {background: #2d2d2d;border-color: #2d2d2d;border-radius: 0 ;color: #fff;display: block;height: 40px;float: right;padding-top: 7px;max-width: 92px; width: 100%;position: absolute;right: 0;top: 0;border-radius: 0 3px 3px 0;z-index: 1}
.user-box .input-group {width: 100%;border-radius: 3px;position: relative;border: none}
.ticket-add-tab-section .file { color: #fff;opacity: 1}
.tickets-listing-menus{border-bottom: 1px solid #e1e1e1;}
.tickets-listing-menus li{display: inline-block;vertical-align: middle;position: relative;}
.tickets-listing-menus li:last-child:before{content: "";height: 25px;width: 1px;background: #e1e1e1;position: absolute;right: 0;top: 0;bottom: 0;margin: auto -1px auto 0}
.tickets-listing-menus li:first-child:before{content: "";height: 25px;width: 1px;background: #e1e1e1;position: absolute;left: 0;top: 0;bottom: 0;margin: auto 0px auto -1px}
.tickets-listing-menus li a{color: #333; background: #fff; padding: 10px 15px;font-size: 16px;display: block;}
.tickets-listing-menus li.active{position: relative; z-index: 2; top: 2px; margin-top: -2px; border-bottom: 1px solid #fff; border-top: 1px solid #e1e1e1; border-left: 1px solid #e1e1e1; border-right: 1px solid #e1e1e1;}
/*file input css end here*/
</style>
<!-- <div class="middle-container"> -->
<?php
$arr_tickets = $obj_ticket->toArray();

$cnt_open_tickets = get_tickets_count('in_progress', $user_id);
$cnt_reopened_tickets = get_tickets_count('re_opened', $user_id);
$cnt_resolved_tickets = get_tickets_count('resolved', $user_id);
$cnt_closed_tickets = get_tickets_count('closed', $user_id);

$cnt_open_tickets += $cnt_reopened_tickets;

//dd($cnt_open_tickets, $cnt_resolved_tickets, $cnt_closed_tickets, $cnt_reopened_tickets);

?>

<div class="container">
    <!-- <br/> -->
    <div class="row">
        <div class="col-sm-7 col-md-8 col-lg-9">
            <div class="search-grey-bx white-wrapper" style="min-height: 300px;">
                @include('front.layout._operation_status')
                <div class="head_grn">
                    {{ trans('common/support.your_tickets') }}
                </div>

                <div class="tickets-listing-menus">
                    <ul>
                        <li @if((Request::has('type') && (Request::input('type') == 'in_progress' || Request::input('type') == 're_opened')) || !Request::has('type')) class="active" @endif><a href="{{ $module_url_path }}">Open ({{ $cnt_open_tickets or 0 }}) </a></li>
                        <li @if(Request::has('type') && Request::input('type') == 'resolved') class="active" @endif><a href="{{ $module_url_path.'?type=resolved' }}">Resolved ({{ $cnt_resolved_tickets or '' }}) </a></li>
                        <li @if(Request::has('type') && Request::input('type') == 'closed') class="active" @endif><a href="{{ $module_url_path.'?type=closed' }}">Closed ({{ $cnt_closed_tickets or '' }}) </a></li>
                    </ul>
                    <div class="create-ticket-btn">
                        <a href="{{ $module_url_path.'/create' }}" class="btn-create-btn">
                            Create Ticket
                        </a>
                    </div>
                </div>
                @if(isset($arr_tickets) && isset($arr_tickets['data']) && empty($arr_tickets['data']))
                    <div class="">
                        <div class="add-ticket-form-link">
                        @if((Request::has('type') && Request::input('type') != 'closed') || !Request::has('type'))
                            There are no tickets to display in this tab. <a class="open-add-ticket-form" href="{{ $module_url_path.'/create' }}">Click here to open a new ticket</a>
                        @else
                            No data found.
                        @endif
                        </div>
                    </div>
                @else
                <div class="table-responsive">
                    <form id="frm_show_cnt" class="form-horizontal show-entry-form" method="POST" action="{{url('client/transactions/show_cnt')}}">
                        {{ csrf_field() }}                                    
                        <div class="sort-by">
                            @php $show_cnt ='show_transaction_cnt';  @endphp
                            @include('front.common.show-cnt-selectbox')
                        </div>                                    
                    </form>
                    <table class="theme-table invoice-table-s table TaBle" style="border: 1px solid rgb(239, 239, 239); margin-bottom:0;">
                        <thead class="tras-client-tbl">
                            <tr>
                                <th>Ticket ID</th>
                                <th>Date</th>
                                <th>Subject</th>
                                <th>Department</th>
                                @if(Request::has('type') && Request::input('type') != 'resolved' && Request::input('type') != 'closed' || !Request::has('type'))
                                <th>Status</th>
                                @endif
                                {{-- @if(Request::has('type') && Request::input('type') != 'resolved' || !Request::has('type')) --}}
                                <th>Action</th>
                                {{-- @endif --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($arr_tickets['data'] as $key => $ticket)
                            <tr role="row" class="{{ $key % 2 == 0 ? 'odd' : 'even' }}">

                                <td>
                                    <a href="{{ $module_url_path.'/details/'.$ticket['ticket_number'] }}">{{ $ticket['ticket_number'] }}</a>
                                </td>

                                <td> {!! date('d M y', strtotime($ticket['created_at'])) !!}</td>

                                <td>{{ substr($ticket['title'], 0, 35).'...' }}</td>

                                <td>{{ isset($ticket['get_category']) && !empty($ticket['get_category']) ? $ticket['get_category']['title'] : '' }}</td>

                                @if(Request::has('type') && Request::input('type') != 'resolved' && Request::input('type') != 'closed' || !Request::has('type'))
                                <td>
                                    @if(isset($ticket['status']) && ($ticket['status'] == 'in_progress' || $ticket['status'] == 're_opened'))
                                    {{-- <span class="label label-default">
                                        @if($ticket['status'] == 'in_progress')
                                        Open
                                        @elseif($ticket['status'] == 're_opened')
                                        Re-Opened
                                        @endif
                                    </span>
                                    &nbsp; --}}
                                    @endif
                                    @if(isset($ticket['last_reply_by']) && $ticket['last_reply_by'] == 'admin')
                                    <span class="label"  style="background-color: #6c757d;color:white;">Need Action</span>
                                    @else
                                    <span class="label" style="color: #212529;background-color: #ffc107;">Waiting for Reply</span>
                                    @endif
                                </td>
                                @endif

                                {{-- @if(Request::has('type') && Request::input('type') != 'resolved' || !Request::has('type')) --}}
                                <td>
                                    @if((Request::has('type') && (Request::input('type') != 'closed' && Request::input('type') != 'resolved')) || !Request::has('type'))
                                    <a href="{{ $module_url_path.'/details/'.$ticket['ticket_number'] }}" title="Reply">
                                        <span class="label label-info"><i class="fa fa-reply"></i> Reply</a></span>
                                    <a href="{{ $module_url_path.'/resolve_ticket/'.base64_encode($ticket['id']) }}" title="Resolve">
                                        <span class="label" style="color: #fff;background-color: #28a745;"><i class="fa fa-check"></i> Resolve</a></span>
                                    @elseif(Request::has('type') && (Request::input('type') == 'closed' || Request::input('type') == 'resolved'))
                                    <a href="{{ $module_url_path.'/reopen_ticket/'.base64_encode($ticket['id']) }}" title="Re-Open">
                                        <span class="label label-primary"><i class="fa fa-ticket"></i> Re-Open</a></span>
                                    @endif
                                </td>
                                {{-- @endif --}}

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
                <div class="product-pagi-block">
                {{ $obj_ticket->links() }}
                </div>
            </div>
        <!-- Paination Links -->

        @include('front.common.pagination_view', ['paginator' => $arr_tickets])
        <!-- Paination Links -->
        </div>
    </div>
</div>
<!-- </div> -->
<link rel="stylesheet" href="{{url('/public')}}/front/css/responsivetabs.css" />
<script src="{{url('/public')}}/front/js/responsivetabs.js"></script>
<script>

    $(document).ready(function ()
    {	
        $(document).on('responsive-tabs.initialised', function (event, el)
        {
            console.log(el);
        });

        $(document).on('responsive-tabs.change', function (event, el, newPanel)
        {
            console.log(el);
            console.log(newPanel);
        });

        $('[data-responsive-tabs]').responsivetabs(
        {
            initialised : function ()
            {
                console.log(this);
            },

            change : function (newPanel)
            {
                console.log(newPanel);
            }
        });
    });
</script>

<script>
    /*$(".open-add-ticket-form").on("click", function(){
       $(this).parent().siblings(".ticket-add-tab-section").slideToggle("slow"); 
    });*/
</script>

@stop

