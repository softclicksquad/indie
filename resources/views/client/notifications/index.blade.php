@extends('client.layout.master')
@section('main_content')
<style type="text/css">
    .table td a{color: #838383;}
    .sort-by{border: 1px solid #ddd;margin: 20px 0 -31px;;max-width: 75px;width: 100%;border-radius: 3px;position: relative;z-index: 1;}
    /*file input css start here*/
.btn-file input[type=file] {position: absolute;top: 0;right: 0;min-width: 100%;min-height: 100%;text-align: right;opacity: 0;background: none repeat scroll 0 0 transparent;cursor: inherit;display: block;}
.btn.btn-primary.btn-file {background: #2d2d2d;border-color: #2d2d2d;border-radius: 0 ;color: #fff;display: block;height: 40px;float: right;padding-top: 7px;max-width: 92px; width: 100%;position: absolute;right: 0;top: 0;border-radius: 0 3px 3px 0;z-index: 1}
.user-box .input-group {width: 100%;border-radius: 3px;position: relative;border: none}
.ticket-add-tab-section .file { color: #fff;opacity: 1}
.tickets-listing-menus{border-bottom: 1px solid #e1e1e1;}
.tickets-listing-menus li{display: inline-block;vertical-align: middle;position: relative;}
.tickets-listing-menus li:last-child:before{content: "";height: 25px;width: 1px;background: #e1e1e1;position: absolute;right: 0;top: 0;bottom: 0;margin: auto -1px auto 0}
.tickets-listing-menus li:first-child:before{content: "";height: 25px;width: 1px;background: #e1e1e1;position: absolute;left: 0;top: 0;bottom: 0;margin: auto 0px auto -1px}
.tickets-listing-menus li a{color: #333; background: #fff; padding: 10px 15px;font-size: 16px;display: block;}
.tickets-listing-menus li.active{position: relative; z-index: 2; top: 2px; margin-top: -2px; border-bottom: 1px solid #fff; border-top: 1px solid #e1e1e1; border-left: 1px solid #e1e1e1; border-right: 1px solid #e1e1e1;}
/*file input css end here*/
</style>
<!-- <div class="middle-container"> -->

<div class="container">
    <!-- <br/> -->
    <div class="row">
        <div class="col-sm-7 col-md-8 col-lg-9">
            <div class="search-grey-bx white-wrapper" style="min-height: 300px;">
                @include('front.layout._operation_status')
                <div class="head_grn">
                    Notifications
                </div>

                <div class="tickets-listing-menus">
                    
                </div>
                @if(isset($arr_data['data']) && empty($arr_data['data']))
                    <div class="">
                        <div class="add-ticket-form-link">
                            No notification found.
                        </div>
                    </div>
                @else
                <div class="table-responsive">
                    
                    <table class="theme-table invoice-table-s table TaBle" style="border: 1px solid rgb(239, 239, 239); margin-bottom:0;">
                        <thead class="tras-client-tbl">
                            <tr>
                                <th>#</th>
                                <th>Notification</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $from = $arr_data['from']; 
                                $total = $arr_data['total'] + 1; 
                            ?>
                            @foreach($arr_data['data'] as $key => $value)
                            <tr role="row" class="even">
                                <td>
                                    {{ $total - $from }}    
                                </td>
                                <td>
                                    <a href="{{url('/')}}/{{ $value['url'] }}" target="_blank">
                                        {{isset($value['notification_text_en'])?$value['notification_text_en']:''}}
                                    </a>
                                </td>
                            </tr>
                            <?php
                                $from++;
                            ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
                <div class="product-pagi-block">
                {{ $obj_data->links() }}
                </div>
            </div>
        <!-- Paination Links -->

        @include('front.common.pagination_view', ['paginator' => $arr_data])
        <!-- Paination Links -->
        </div>
    </div>
</div>
<!-- </div> -->


@stop