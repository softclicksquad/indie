@extends('client.layout.postmaster')                
@section('main_content')
<style type="text/css">
   .col-lg-2 { width: 20.667%; } .audi-job-bx h3 { text-transform:none; }
</style>
<link href="{{url('/public')}}/assets/select2/dist/css/select2.min.css" type="text/css" rel="stylesheet" />

<script type="text/javascript" src="{{url('/public')}}/assets/select2/dist/js/select2.full.js"></script>
<div class="middle-container">
   <form action="{{ url($module_url_path) }}/create" method="POST" id="form-post_projects" name="form-post_projects" enctype="multipart/form-data" files ="true">
      {{ csrf_field() }}
      <!-- for hire expert -->
      @if(isset($expert_user_id) && $expert_user_id != "")
        @php
           $hired_expert_first_name = 'Unknown';
           $hired_expert_last_name = '';
           $expert_details = sidebar_information(base64_decode($expert_user_id));
           if(isset($expert_details['user_details']['first_name']) && $expert_details['user_details']['first_name'] !=""){
             $hired_expert_first_name =  $expert_details['user_details']['first_name'];
           }
           if(isset($expert_details['user_details']['last_name']) && $expert_details['user_details']['last_name'] !=""){
             $hired_expert_last_name  =  substr($expert_details['user_details']['last_name'],0,1).'.';
           }
        @endphp
      @endif
      <input type="hidden" id="hire_expert_user_id" name="hire_expert_user_id" value="{{isset($expert_user_id)?$expert_user_id:''}}">
      <!-- end for hire expert -->
      <div class="container">
         <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
               @include('front.layout._operation_status')
               <div class="contest-title">
                   <h2>{{trans('client/projects/post.text_post_job')}}</h2>
               </div>
               <div class="audio-job">
                  <!-- <h5>{{trans('client/projects/post.text_cat_subtitle')}}</h5> -->
                  <div class="audi-job-bx post-job-wrapper">
                     
                     <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-6">
                           <div class="user-box">
                             <div class="p-control-label">{{trans('client/projects/post.text_category_title')}} <span class="star-col-block">*</span></div>
                              <div class="droup-select">
                                 <select class="droup getcat mrns tp-margn" data-rule-required="true" name="category" id="category">
                                    <option value="">{{trans('client/projects/post.text_select_category')}}</option>
                                    @if(isset($arr_categories) && sizeof($arr_categories)>0)
                                    @foreach($arr_categories as $categories)
                                    <option value="{{$categories['id']}}" 
                                    @if(old('category')==$categories['id'] || isset($project_details['category_id']) && $project_details['category_id']==$categories['id']) selected="" @endif>
                                    @if(isset($categories['category_title'])){{$categories['category_title']}}@endif</option>
                                    @endforeach
                                    @endif            
                                 </select>
                                 <span class='error'>{{ $errors->first('category') }}</span>
                              </div>
                           </div>
                        </div>

                  {{-- Added by amol --}}
                        <div class="col-sm-12 col-md-6 col-lg-6">
                           <div class="user-box">
                             <div class="p-control-label">{{trans('client/projects/post.text_sub_category_title')}} <span class="star-col-block">*</span></div>
                              <div class="droup-select">
                                 <select class="droup subcategory mrns tp-margn" data-rule-required="true" name="sub_category" id="sub_category">
                                           
                                 </select>
                                 <span class='error'>{{ $errors->first('sub_category') }}</span>
                              </div>
                           </div>
                        </div>
                        {{-- End by amol --}}
                        
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="user-box">
                               <div class="p-control-label">{{trans('client/projects/post.text_project_details')}} <span class="star-col-block">*</span></div>
                                  <input type="text" name="project_name" id="project_name" class="input-lsit beginningSpace_restrict" placeholder="{{trans('client/projects/post.entry_project_details')}}" data-rule-required="true"  
                                  @if(isset($project_details['project_name']) && $project_details['project_name'] != "") 
                                    value="{{$project_details['project_name']}}" 
                                  @elseif(isset($expert_user_id) && $expert_user_id != "")
                                    value="Individual project for {{$hired_expert_first_name or 'Unknown'}} {{$hired_expert_last_name or ''}}"
                                  @else 
                                    value="{{old('project_name')}}"
                                  @endif
                                  >
                              <span class='error'>{{ $errors->first('project_name') }}</span>
                           </div>
                        </div>
                        
                        <div class="clr"></div>
                        @php $skills_arr = []; @endphp
                        @if(isset($project_details['project_skills']))
                           @foreach($project_details['project_skills'] as $project_skills)
                             @php $skills_arr[] = $project_skills['skill_id']; @endphp
                           @endforeach
                        @endif
                        <div class="col-sm-12 col-md-12 col-lg-12">
                           <div class="user-box">
                              <div class="p-control-label">{{trans('client/projects/ongoing_projects.text_skills')}} <span class="inline-note"> ({{trans('client/projects/post.text_project_subskills')}})</span></div>
                         
                              <div class="droup-select multiselect-block">
                                 <select name="project_skills[]" id="project_skills" class="droup" multiple="multiple">
                                    <option value="">{{trans('client/projects/post.text_select_skills_needed')}}</option>
                                    @if(isset($arr_skills) && sizeof($arr_skills)>0)
                                       @foreach($arr_skills as $skills)
                                          @if(isset($skills['skill_name']))
                                             <option value="{{$skills['id']}}" @if(old('skill_name')==$skills['id'] || in_array($skills['id'] , $skills_arr)) selected=""  @endif>{{ $skills['skill_name']}}</option>
                                          @endif
                                       @endforeach
                                    @endif
                                 </select>
                                 <div class="clr"></div>
                                 <div class="err_projects_skills">
                                 </div>
                                 <span class='error' id="skill_name_error">{{ $errors->first('project_skills') }}</span>
                              </div>
                           </div>
                        </div>
                        <div class="clr"></div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                           <div class="user-box edit-method">
                              <div class="p-control-label">{{trans('client/projects/post.text_project_description')}} <span class="star-col-block">*</span></div>
                              <div class="txt-edit"> {{trans('client/projects/post.text_project_subdescription')}}</div>
                              <!-- <div class="txt-edit"> {{trans('client/projects/post.text_project_limit_description')}}</div> -->
                              <span class="txt-edit" id="project_description_msg">
                              {{trans('client/projects/post.text_project_limit_description')}}
                              </span>
                              <textarea @if(isset($project_details['project_description']) && $project_details['project_description'] != "") value="{{$project_details['project_description']}}" @else value="{{old('project_description')}}" @endif data-rule-required="true" data-rule-maxlength="1000" rows="10" cols="30" class="text-area beginningSpace_restrict" id="project_description" name="project_description" data-gramm="" data-txt_gramm_id="21e619fb-536d-f8c4-4166-76a46ac5edce" onkeyup="javascript: return textCounter(this,1000);" placeholder="{{trans('client/projects/post.entry_project_description')}}">@if(isset($project_details['project_description']) && $project_details['project_description'] != ""){{$project_details['project_description']}}@else{{old('project_description')}}@endif</textarea>
                              <span class='error'>{{ $errors->first('project_description') }}</span>
                           </div>
                        </div>
                        
                        <div class="clr"></div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="user-box">
                                        <div class="p-control-label">{{trans('client/projects/post.text_project_upload')}}
                                        </div>
                                        <div class="picture-div new-picture" style="" id="pictures_div">
                                            <div class="update-pro-img-main"> 
                                                <div class="lab_img" id="lab_1">
                                                   <div class="col-sm-12 col-lg-12 col-lg-12" style="float:right;">                                       
                                                      <span>
                                                      <a href="javascript:void(0);" id='remove_project' class="remove_project" style="display:none;" >
                                                      <span class="glyphicon glyphicon-minus-sign" style="font-size: 20px;"></span>
                                                      </a>
                                                      </span>
                                                   </div>
                                                   <div class="" id="add_lab_div">
                                                      <div class="add_pht upload-pic loc_add_pht" id="div_blank" onclick="return addpictures(this)"  style="height: 120px;width: 120px; float: left;"> 
                                                        <img src="{{url('/public')}}/front/images/plus-img.png" alt="user pic"  style="width:100%;height:100%;" /></div>
                                                      <div class="show_photos" id="show_photos" style="width: auto; display: initial;float: none;"></div>
                                                      <div id="div_hidden_photo_list" class="div_hidden_photo_list">
                                                         <input type="file" name="contest_attachments[]" id="contest_attachments" class="contest_attachments" style="display:none" />
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>                       
                                                <input type="hidden" name="file_name_lab" id="file_name_lab"  >
                                                 
                                            <div class="clearfix"></div>                     
                                            <div class="error-red" id="err_other_image"></div>                                          
                                        </div>
                                        <br>
                                        <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span> {{trans('client/contest/post.text_contest_attachments_msg')}}

                                    </div>
                                </div>
                                <div class="clr"></div>

                        <div class="col-sm-12 col-md-12 col-lg-12">
                        <!--<h3>{{trans('client/projects/post.text_project_timeframe')}}</h3>-->
                        </div>
                        <?php /* <div class="col-sm-12 col-md-8 col-lg-8">
                           <div class="row">
                                 <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="user-box">
                                          <div class="p-control-label">{{trans('client/projects/post.text_project_start_date')}}</div>
                                          <div class="input-datepickr input-bx input-lsit">
                                             <input data-rule-required="true" class="datepicker-start box-de" placeholder="{{trans('client/projects/post.entry_project_start_date')}}" id="start_date" name="start_date" type="text" data-show-preview="false" value="{{old('start_date')}}">
                                             <i class="fa fa-calendar" id="first_date"></i>
                                             <span class='error'>{{ $errors->first('start_date') }}</span>
                                          </div>
                                    </div>
                                  </div>
                                 <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="user-box">
                                          <div class="p-control-label">{{trans('client/projects/post.text_project_end_date')}}</div>
                                          <div class="input-datepickr input-bx input-lsit">
                                             <input data-rule-required="true" class="datepicker-start box-de" placeholder="{{trans('client/projects/post.entry_project_end_date')}}" id="end_date" name="end_date" type="text" data-show-preview="false" value="{{old('end_date')}}">
                                             <i class="fa fa-calendar" id="last_date"></i>
                                             <span class='error'>{{ $errors->first('end_date') }}</span>
                                          </div>
                                     </div>
                                 </div>
                              </div>
                        </div> */ ?>

                        <div class="col-sm-12 col-md-6 col-lg-6">
                          
                              <div class="user-box radio-wrapper">
                                 <div class="p-control-label">{{trans('client/projects/post.text_project_budget')}} <span class="star-col-block">*</span></div>
                                 <div class="radio_area regi">
                                    <input type="radio" class="css-checkbox" id="radio_fixed" value="1" name="price_method" checked="checked" 
                                    @if(old('price_method')==1  || isset($project_details['project_pricing_method']) && $project_details['project_pricing_method']==1)
                                    checked="" 
                                    @endif
                                    ><label class="css-label radGroup1" for="radio_fixed">{{trans('client/projects/post.text_pay_fixed_price')}} </label>
                                 </div>
                                 <div class="radio_area regi">
                                    <input type="radio" class="css-checkbox" id="radio_hourly" value="2" name="price_method"
                                    @if(old('price_method')==2  || isset($project_details['project_pricing_method']) && $project_details['project_pricing_method']==2)
                                    checked="" 
                                    @endif
                                    >
                                    <label class="css-label radGroup1" for="radio_hourly">{{trans('client/projects/post.text_hourly_rate')}}</label>
                                 </div>
                              </div>
                           
                        </div>

                        {{-- <div class="col-sm-12 col-md-6 col-lg-6">
                           <div class="user-box">
                              <div class="p-control-label">{{trans('client/projects/post.text_project_duration')}} <span class="star-col-block">*</span></div>
                              <div class="input-bx"><input data-rule-required="true" type="text" name="project_duration" id="project_duration" class="input-lsit beginningSpace_restrict char_restrict" placeholder="{{trans('client/projects/post.entry_project_duration')}}"  value="{{old('project_duration')}}">
                                    <span class='error'>{{ $errors->first('project_duration') }}</span>
                              </div>
                           </div>
                        </div> --}}

                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <div class="user-box">
                                <div class="p-control-label dr_label">Duration<span class="star-col-block">*</span></div>
                                <div class="calender-input droup-select">
                                    <select name="project_duration" id="project_duration" class="droup mrns tp-margn" data-rule-required="true" >
                                         <option value="">Select duration</option>
                                         @if(isset($arr_project_duration) && sizeof($arr_project_duration)>0)
                                            @foreach($arr_project_duration as $dr_key => $dr_value)
                                                <option value="{{ $dr_value }}">{{ $dr_value }}</option>
                                            @endforeach
                                         @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                       
                        <div class="clr"></div>

                        <div class="col-sm-12 col-md-4 col-lg-4">
                            <div class="user-box">
                                <div class="p-control-label"> In which currency do you want to run this offer? <span class="star-col-block">*</span></div>
                                <div class="droup-select">
                                    <select name="project_currency_code" id="project_currency_code" onchange="set_currency_code()" class="droup mrns tp-margn" data-rule-required="true">
                                        @if(isset($arr_currency) && sizeof($arr_currency)>0)
                                        <option value="">{{trans('client/projects/post.text_select_currency')}}</option>
                                        @foreach($arr_currency as $key => $currency)
                                        <option value="{{isset($currency['currency_code'])?$currency['currency_code']:''}}" data-code="{{isset($currency['currency'])?$currency['currency']:''}}" @if(old('project_currency')==$currency['currency_code'] || isset($project_details['project_currency']) && $project_details['project_currency']==$currency['currency_code']) selected="" @endif data-id="{{ $currency['id'] or '' }}">
                                            {{isset($currency['currency_code'])?$currency['currency_code']:''}} ( {{ isset($currency['description'])?$currency['description']:'' }} )
                                        </option>
                                        @endforeach
                                        @endif
                                    </select>
                                    <span class='error'>{{ $errors->first('project_currency') }}</span>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="project_currency" id="project_currency" value="">

                        <div class="col-sm-12 col-md-4 col-lg-4" id="section_hour_per_week" style="display:none">
                            <div class="input-bx d-se">
                                <div class="p-control-label">Hours per week?<span class="star-col-block">*</span></div>
                                <div class="droup-select input-bx">
                                    <select name="hour_per_week" id="hour_per_week" class="droup mrns tp-margn form-control" onchange="set_custom_hour_per_week()" data-rule-required="true">
                                        <option value="">Select hours/week</option>
                                        @if(isset($arr_hours_per_week) && sizeof($arr_hours_per_week)>0)
                                        @foreach($arr_hours_per_week as $hour_key => $hour_value)
                                        <option value="{{$hour_key}}" data-text="{{$hour_value}}">{{$hour_value}}
                                        </option>
                                        @endforeach
                                        @endif
                                        {{-- <option value="HOUR_CUSTOM_RATE">Customize Hour</option> --}}
                                    </select>
                                    <span class='error'>{{ $errors->first('hour_per_week') }}</span>
                                    <span id="err_hour_per_week" class='error'></span>
                                </div>
                            </div>
                        </div>

                        {{-- <div class="col-sm-12 col-md-4 col-lg-4" id="hour_custom_rate" style="display: none">
                            <div class="input-bx d-se">
                                <div class="p-control-label hour_custom_rate_description"><span class="star-col-block">*</span></div>
                                <div class="cust-budget input-bx">
                                    <input name="hour_custom_rate" data-rule-required="true" data-rule-number="true" data-rule-min="1" type="text" class="input-lsit"></input>
                                    <span class='error'>{{ $errors->first('hour_custom_rate') }}</span>
                                </div>
                            </div>
                        </div> --}}

                        <div class="col-sm-12 col-md-4 col-lg-4" id="section_fixed_price">
                           <div class="user-box">
                              <div class="p-control-label">{{trans('client/projects/post.text_project_cost')}} <span class="star-col-block">*</span></div>
                              <div class="droup-select">
                                 <select name="fixed_rate" id="project_cost" onchange="set_custom_rates('1')" class="droup mrns tp-margn" data-rule-required="true">
                                    @if(isset($aar_fixed_rates) && sizeof($aar_fixed_rates)>0)
                                    <option value="">{{trans('client/projects/post.text_select_project_cost')}}</option>
                                    @foreach($aar_fixed_rates as $key => $fix_rate)
                                    <option value="{{$key}}"
                                    @if(old('project_cost')==$key)  selected=""   @endif
                                    >{{$fix_rate}}</option>
                                    @endforeach
                                    @endif
                                    <option value="CUSTOM_RATE" >{{trans('client/projects/post.text_customize_budget')}}</option>
                                 </select>
                                 <span class='error'>{{ $errors->first('fixed_rate') }}</span>
                                 <span class='error'>{{ $errors->first('hourly_rate') }}</span>
                              </div>
                           </div>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4"  id="section_hourly_rate" style="display:none">
                           <div class="input-bx d-se">
                              <div class="p-control-label">{{trans('client/projects/post.text_hourly_title')}} <span class="star-col-block">*</span></div>
                              <div class="droup-select input-bx">
                                 <select name="hourly_rate" id="hourly_rate" class="droup mrns tp-margn" onchange="set_custom_rates('2')" data-rule-required="true">
                                    <option value="">{{trans('client/projects/post.text_select_hourly_rate')}}</option>
                                    @if(isset($aar_hourly_rates) && sizeof($aar_hourly_rates)>0)
                                    @foreach($aar_hourly_rates as $key => $fix_rate)
                                    <option value="{{$key}}"
                                    @if(old('project_cost')==$key) selected=""  @endif
                                    >{{$fix_rate}}
                                    </option>
                                    @endforeach
                                    @endif
                                    <option value="CUSTOM_RATE" >Customize</option>
                                 </select>
                                 <span class='error'>{{ $errors->first('fixed_rate') }}</span>
                                 <span class='error'>{{ $errors->first('hourly_rate') }}</span>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4" id="custom_rate" style="display: none">
                           <div class="input-bx d-se">
                              <div class="p-control-label description"><span class="star-col-block">*</span></div>
                              <div class="cust-budget input-bx">
                                 <input name="custom_rate" data-rule-required="true" data-rule-number="true" data-rule-max="25000"  data-rule-min="1" type="text" class="input-lsit"  ></input>   
                                 <span class='error'>{{ $errors->first('custom_rate') }}</span>
                              </div>
                           </div>
                        </div>
                        <div class="clr"></div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                           <div class="input-bx">
                              <h3>{{trans('client/projects/post.text_handle_by')}}</h3>
                              <div class="input-bx">
                                 <div class="radio_area regi">
                                    <input type="radio" class="css-checkbox" id="radio3" value="1" name="handle_by" checked="checked" 
                                    @if(old('handle_by')==1 || isset($project_details['project_handle_by']) && $project_details['project_handle_by']==1)
                                    checked="" 
                                    @endif
                                    >
                                    <label class="css-label radGroup1" for="radio3">{{trans('client/projects/post.text_myself')}}</label>
                                 </div>
                                 @if(isset($expert_user_id) && $expert_user_id != "")
                                 @else
                                    <div class="radio_area regi">
                                       <input type="radio" class="css-checkbox" id="radio4" value="2" name="handle_by"
                                       @if(old('handle_by')==2 || isset($project_details['project_handle_by']) && $project_details['project_handle_by']==2) checked=""  @endif
                                       >
                                       <label class="css-label radGroup1" for="radio4">{{trans('client/projects/post.text_project_manager')}}</label>
                                    </div>
                                 @endif
                              </div>
                              <div style=" display: none;" id="myself_note" class="alert alert-info info-box">
                                 <strong>{{ trans('new_translations.info')}}!</strong> {{ trans('new_translations.if_you_choose_myself')}}. 
                              </div>
                              <div style="display: none;" id="project_manager_note" class="alert alert-info info-box">
                                 <strong>{{ trans('new_translations.info')}}!</strong> 
                                 @if(isset($arr_settings['website_pm_initial_cost']))
                                 {!! trans('new_translations.if_you_choose_project_manager',array('initial_cost'=>$arr_settings['website_pm_initial_cost'])) !!} 
                                 @else
                                 {!! trans('new_translations.if_you_choose_project_manager',array('initial_cost'=>29)) !!} 
                                 @endif
                              </div>
                           </div>
                        </div>
                      
                        <div class="clr"></div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                           <div class="right-side"><button type="submit" class="normal-btn pull-right">{{trans('client/projects/post.text_post_job')}}</button></div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<!--date picker start here-->
<link rel="stylesheet" type="text/css"  href="{{url('/public')}}/front/css/jquery-ui.css"/>
<script src="{{url('/public')}}/front/js/jquery-ui.js" type="text/javascript"></script>
<script> 
  $(document).ready(function(){
     $("#bid_closing_date" ).datepicker({
        dateFormat: 'yy-mm-dd',
        'startDate': new Date(),
        minDate: 'today',
     }) 
     $("#start_date" ).datepicker({
        dateFormat: 'yy-mm-dd',
        'startDate': new Date(),
        minDate: 'today',
     }).on('change',function(e)
     {
      var date2 = $('#start_date').datepicker('getDate');
      date2.setDate(date2.getDate() + 1);
      //sets minDate to dt1 date + 1
      $('#end_date').datepicker('option', 'minDate', date2);
       var start_date = $("#start_date").val();
       var end_date = $("#end_date").val();
       updateProjectDuration(start_date,end_date);
     });
     $('#first_date').click(function ()
     {
        $('#first_date').datepicker("show");
     });
     $( "#end_date" ).datepicker({
        dateFormat: 'yy-mm-dd',
        'startDate': new Date(),
        minDate: 'today',
     }).on('change',function(e)
     { 
         var start_date = $("#start_date").val();
         var end_date = $("#end_date").val();
     
         updateProjectDuration(start_date,end_date);
     });
     $('#last_date').click(function ()
     {
        $('#last_date').datepicker("show");
     });
     /* showing note for myself  and project manager option */
     if($('#radio3').is(":checked"))
     {
        $('#myself_note').show();
     }
     if($('#radio4').is(":checked"))
     {
        $('#project_manager_note').show();
     }
     $("#radio3").click(function()
     {
        $('#myself_note').show();
        $('#project_manager_note').hide();
     });
     $("#radio4").click(function()
     {
        $('#project_manager_note').show();
        $('#myself_note').hide();
     });
   });
</script>
<script>
   function updateProjectDuration(start_date,end_date)
   {  
      start_date = new Date(start_date);
      end_date   = new Date(end_date);
      if(start_date==false || end_date==false){
        return false;
      }
      project_duration = dateDiffInDays(start_date,end_date);
      if(!isNaN(project_duration)){
         $("#project_duration").val(project_duration);
      }
   }
   function dateDiffInDays(a, b) {
    var _MS_PER_DAY = 1000 * 60 * 60 * 24;
    // Discard the time and time-zone information.
    var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
   } 
</script>
<script type="text/javascript">
   $("#form-post_projects").validate({
         errorElement: 'span',
         errorPlacement: function (error, element) 
         {
            if(element.attr("name") == 'check_terms')
            {  
               error.appendTo($("#_err_term"));  
            }
            else if(element.attr("id") == 'project_skills')
            {
               error.appendTo($(".err_projects_skills"));  
            }
            else
            {
               error.insertAfter(element);
            }
         }
   });
   
   function browseImage()
   {
      $("#project_attachment").trigger('click');
   }
   function removeBrowsedImage()
   {
      $('#project_attachment_name').val("");
      $("#btn_remove_image").hide();
      $("#project_attachment").val("");
   }

   function set_currency_code()
    {
        var currency = $("#project_currency_code option:selected").attr('data-code');

        var budgetType = $('input[name=price_method]:checked').val();

        budgetType = budgetType == '2' ? 'hourly' : 'fixed';

        var currId = $("#project_currency_code option:selected").data('id');

        if(budgetType == 'hourly'){
            $("#hourly_rate").attr("disabled", true);
        }else{
            $("#project_cost").attr("disabled", true);
        }

        $.ajax({
            beforeSend: function() {
                showProcessingOverlay();
            },
            type: "POST",
            url: site_url + '/client/projects/get_currency_budget_html' + "?_token={{ csrf_token() }}",
            data: {
                cuurency_id : currId,
                budget_type : budgetType
            },
            cache: false,
            success: function(resp) {
                hideProcessingOverlay();
                var data = JSON.parse(resp);
                if(data.status == 'success'){
                    if(budgetType == 'hourly'){
                        $("#hourly_rate").html(data.html);
                        $("#hourly_rate").removeAttr('disabled');
                    }else{
                        $("#project_cost").html(data.html);
                        $("#project_cost").removeAttr('disabled');
                    }
                }else{
                    $html = '<option>Select job budget</option>';
                    if(budgetType == 'hourly'){
                        $("#hourly_rate").html($html);
                        $("#hourly_rate").removeAttr('disabled');
                    }else{
                        $("#project_cost").html($html);
                        $("#project_cost").removeAttr('disabled');
                    }
                }
            }
        });

        $.ajax({
            beforeSend: function() {
                //showProcessingOverlay();
            },
            type: "POST",
            url: site_url + '/client/projects/get_highlighted_html' + "?_token={{ csrf_token() }}",
            data: {
                cuurency_id : currId,
                budget_type : budgetType
            },
            cache: false,
            success: function(resp) {
                //hideProcessingOverlay();
                var data = JSON.parse(resp);
                console.log(data);
                
                if(data.status == 'success'){
                    $("#project_highlight").html('');
                    $("#project_highlight").html(data.html);
                }
            }
        });

        $('#project_currency').val(currency);

        /*$('#project_cost > option').each(function() {
            var txt = $(this).data('text');
            if(txt != undefined){
                $(this).text(currency+' '+txt);
            }
        });*/

        /*$('#hourly_rate > option').each(function() {
            var txt = $(this).data('text');
            if(txt != undefined){
                $(this).text(currency+' '+txt);
            }
        });*/
    }
    
   $(document).ready(function()
   {
     // This is the simple bit of jquery to duplicate the hidden field to subfile
     $('#project_attachment').change(function()
     {
       if($(this).val().length>0)
       {
        $("#btn_remove_image").show();
     }
   
     $('#project_attachment_name').val($(this).val());
     });
   
     $(".getcat").change(function() {
            var id = $(this).val();
            var site_url = "{{url('/')}}";
            var dataString = 'id=' + id;
            var token = $('input[name="_token"]').val();
            $.ajax({
                type: "POST",
                url: site_url + '/client/projects/subcatdata' + "?_token=" + token,
                data: dataString,
                cache: false,
                success: function(html) {
                    $(".subcategory").html(html);
                }
            });
        });

     // set project prising method options
     $('#radio_hourly').click(function()
     {
          $("#section_hourly_rate").show();
          $("#section_hour_per_week").show();
          $("#section_fixed_price").hide();
          $(".dr_label").html('How long do you need support?'+'<span class="star-col-block">*</span>');
      });
     $('#radio_fixed').click(function()
     {
           $("#section_hourly_rate").hide();
           $("#section_hour_per_week").hide();
           $("#section_fixed_price").show();
           $(".dr_label").html('Duration?'+'<span class="star-col-block">*</span>');
      });
     
     if (jQuery('#radio_hourly').prop('checked')) 
     {
       $("#section_hourly_rate").show();
       $("#section_hour_per_week").show();
        $("#section_fixed_price").hide();
     }
     else if (jQuery('#radio_fixed').prop('checked'))
     {
         $("#section_hourly_rate").hide();
         $("#section_hour_per_week").hide();
         $("#section_fixed_price").show();
     } 
     /* Initializing multiple skills data */
     $('#project_skills').select2({
       maximumSelectionLength: 5,
       placeholder: '{{trans('client/projects/ongoing_projects.text_skills')}}',
       allowClear: true
     });
   });
   /* function for setting custom  input box rates */
   function set_custom_rates(rate_type)
   {
     if(rate_type == '1')
     {
        ref = "project_cost";
        msg = "{{trans('client/projects/post.text_customize_budget')}}";
     }
   
     if(rate_type == '2')
     {
        ref = "hourly_rate";
        msg = "{{trans('client/projects/post.text_adjust_hour')}}";
     }
   
     if($('#'+ref).val() == 'CUSTOM_RATE')
     {
        $('.description').html(msg+'<span class="star-col-block">*</span>');
        $('#custom_rate').show();
     }
     else
     {
        $('.description').html('');
        $('#custom_rate').hide();
     } 
   }

   function set_custom_hour_per_week() {
        if ($('#hour_per_week').val() == 'HOUR_CUSTOM_RATE') {
            $('.hour_custom_rate_description').html('Customize Hour'+'<span class="star-col-block">*</span>');
            $('#hour_custom_rate').show();
        } else {
            $('.hour_custom_rate_description').html('');
            $('#hour_custom_rate').hide();
        }
    }

   function textCounter(field,maxlimit){
      var countfield = 0;
      if ( field.value.length > maxlimit ) {
        field.value = field.value.substring( 0, maxlimit );
        return false;
      } else {
         countfield = maxlimit - field.value.length;
         var message = '{{trans('client/projects/invite_experts.text_you_have_left')}} <b>'+countfield+'</b> {{trans('client/projects/invite_experts.text_characters_for_description')}}';
         jQuery('#project_description_msg').html(message);
         return true;
      }
   }
</script>
<script type="text/javascript">
function Upload() {
    $('.note').css('color','black');
    //Get reference of FileUpload.
    $('.img-preview').attr('src', ""); 
    var logo_id = document.getElementById("logo-id");
    //Check whether the file is valid Image.
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+()$");
    if (regex.test(logo_id.value.toLowerCase())) {
        //Check whether HTML5 is supported.
        if (typeof (logo_id.files) != "undefined") {
            var file_extension = logo_id.value.substring(logo_id.value.lastIndexOf('.')+1, logo_id.length); 
            var size = logo_id.files[0].size;
            if(size > 2000000){
               $('.note').css('color','red');
               $('.note').html("{{trans('client/projects/post.text_project_validsizeupload')}}");
               document.getElementById("logo-id").value = "";
               return false;
            }

            if(file_extension != 'jpg' && 
               file_extension != 'jpeg' &&
               file_extension != 'gif' &&
               file_extension != 'png' &&
               file_extension != 'xlsx' &&
               file_extension != 'pdf' &&
               file_extension != 'docx' &&
               file_extension != 'doc' &&
               file_extension != 'txt' &&
               file_extension != 'odt'){
               $('.note').css('color','red');
               $('.note').html("{{trans('client/projects/post.text_project_validupload')}}");
               document.getElementById("logo-id").value = "";
               return false;
            } 
            //Initiate the FileReader object.
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(logo_id.files[0]);
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();
                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;
                var size  = this.size;
                //Validate the File Height and Width.
                if(file_extension == 'xlsx' ||
                 file_extension == 'pdf' ||
                 file_extension == 'docx' ||
                 file_extension == 'doc' ||
                 file_extension == 'txt' ||
                 file_extension == 'odt'){
                    $('.img-preview').attr('src', site_url+'/front/images/file_formats/'+file_extension+'.png'); 
                }
                image.onload = function (e) {
                    var height = this.height;
                    var width  = this.width;
                    $('.note').html("{{trans('client/projects/post.text_project_validupload')}}");
                    $('.note').css('color','#646464');
                    if(file_extension == 'jpg' || 
                       file_extension == 'jpeg' ||
                       file_extension == 'gif' ||
                       file_extension == 'png'){
                            $('.img-preview').attr('src', image.src);
                       }  
                    return true;
                };
            }
        } else {
            $('.note').css('color','red');
            $('.note').html("This browser does not support HTML5.");
            document.getElementById("logo-id").value = "";
            return false;
        }
    } else {
        $('.note').css('color','red');
        $('.note').html("{{trans('client/projects/post.text_project_validupload')}}");
        document.getElementById("logo-id").value = "";
        return false;
    }
}
</script>

<script type="text/javascript">
    /*Multiple file upload demo starts here*/
    
    var arr_job_document_formats = {!! json_encode($arr_job_document_formats) !!};
    
    function addpictures(ref)
    { 
        var new_images = $("input[name='contest_attachments[]']").map(function() {
               return $(this).val();
           }).get();

        var count = new_images.length;
        
        if(count > 10)
        {
            swal('U can upload maximum 10 files');
            return false;
        }            

        $('#err_other_image').html('');
        var image_id = $(ref).closest('.lab_img').attr('id');
        var length = $('.lab_img').length;
        var view_photo_cnt = jQuery('#'+image_id).find('.photo_view').length                                
        jQuery('#'+image_id).last().find( ".div_hidden_photo_list" ).last().find( "input[name='contest_attachments[]']:last" ).click(); 
        jQuery('#'+image_id).last().find( ".div_hidden_photo_list" ).last().find( "input[name='contest_attachments[]']:last" ).change(function()
        { 
            var files      = this.files;
            var exist_file = $('#file_name_lab').val();
            
            if(exist_file == files[0]['name']) 
            { 
                return false; 
            }
            else 
            {
                $('#file_name_lab').val(files[0]['name']);
                for (var i=0, l=files.length; i<l; i++) 
                {
                    var max_size = 5000000;
                    var current_size = files[i].size;
                    if (max_size>=current_size) 
                    {
                        var file = files[i];

                        var prjct_id = image_id.split('_');
                        jQuery('#'+image_id).find('#image'+prjct_id[1]+'_'+(view_photo_cnt+1)).attr('value',files[i]['name']);
                        var img, reader, xhr;
                        img = document.createElement("img");
                        reader = new FileReader();
                        img = new Image();      

                        var ext      =   files[i]['name'].split('.').pop();  

                        if ($.inArray(ext, arr_job_document_formats) == -1)
                        {                                          
                            swal('File type not allowed');
                            return false;
                        }
                        else
                        {
                            img.onload = function()
                            {
                                                 
                            }                            
                        }

                        reader.onload = (function (theImg) 
                        {      
                            if(ext == 'docx' || ext == 'doc' || ext == 'pdf' || ext == 'zip' || ext == 'mp3' || ext == 'odt' || ext == 'txt' || ext == 'xlsx')
                            {                          
                                var image_src = '{{url('/public')}}/front/images/file_formats/'+ext+'.png';      
                                return function (evt){                                       
                                    theImg.src = evt.target.result;                                
                                    var html = "<div class='photo_view2' onclick='remove_this(this);' style='width:120px;height:120%;position:relative;display: inline-block;'><img src="+ image_src +" class='add_pht' id='add_pht upload-pic' style='float: left; padding: 0px ! important; margin:0' width='120' height='120'><div class='overlay2'><span class='plus2'><i class='fa fa-trash-o' aria-hidden='true'></i></span></div></div>";
                                    jQuery('#'+image_id).last().find('.show_photos').append(html);
                                    jQuery('#'+image_id).last().find('.div_hidden_photo_list').append('<input type="file" name="contest_attachments[]" id="contest_attachments" class="contest_attachments" style="display:none" />');   
                                    //countImages();          
                                    $('#file_name_lab').val('');
                                    };
                            }
                            else if(ext == 'jpg' || ext == 'png' || ext == 'jpeg' || ext == 'gif' || ext == 'JPG' || ext == 'PNG' || ext == 'JPEG' || ext == 'GIF')
                            { 
                                return function (evt){                                       
                                    theImg.src = evt.target.result;                                
                                    var html = "<div class='photo_view2' onclick='remove_this(this);' style='width:120px;height:120%;position:relative;display: inline-block;'><img src="+ evt.target.result +" class='add_pht' id='add_pht upload-pic' style='float: left; padding: 0px ! important; margin:0' width='120' height='120'><div class='overlay2'><span class='plus2'><i class='fa fa-trash-o' aria-hidden='true'></i></span></div></div>";
                                    jQuery('#'+image_id).last().find('.show_photos').append(html);
                                    jQuery('#'+image_id).last().find('.div_hidden_photo_list').append('<input type="file" name="contest_attachments[]" id="contest_attachments" class="contest_attachments" style="display:none" />');   
                                    //countImages();          
                                    $('#file_name_lab').val('');
                                    };
                            }
                            else
                            {
                                var image_src = '{{url('/public')}}/uploads/front/default/default.jpg';  
                                return function (evt){   
                                    theImg.src = evt.target.result;                                
                                    var html = "<div class='photo_view2' onclick='remove_this(this);' style='width:120px;height:120%;position:relative;display: inline-block;'><img src="+ image_src +" class='add_pht' id='add_pht upload-pic' style='float: left; padding: 0px ! important; margin:0' width='120' height='120'><div class='overlay2'><span class='plus2'><i class='fa fa-trash-o' aria-hidden='true'></i></span></div></div>";
                                    jQuery('#'+image_id).last().find('.show_photos').append(html);
                                    jQuery('#'+image_id).last().find('.div_hidden_photo_list').append('<input type="file" name="contest_attachments[]" id="contest_attachments" class="contest_attachments" style="display:none" />');   
                                    //countImages();          
                                    $('#file_name_lab').val('');
                                    };
                            } 

                        }(img));
                        reader.readAsDataURL(file);                        
                    }
                    else
                    {
                        swal('File size should be less than 5MB');
                        return false;
                    }                    
                };
            }        
        });          
   } 

   function remove_this(elm)
    {
       var this_index = jQuery(elm).index();
       jQuery('.lab_img').find(".div_hidden_photo_list").find("input").eq(this_index).remove();
       jQuery(elm).remove();
       //countImages();
    }  
  
    function countImages(){

        var imagecount = $(".photo_view2").length;
        if(imagecount>5){
            $('#err_other_image').html(err_max_images);
        }else{
            $('#err_other_image').html('');
        }
    }

    /*Multiple file upload demo end here*/
</script>
@stop