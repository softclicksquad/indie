<style type="text/css">
.moretext{
  color: rgb(0, 0, 0);
  text-decoration: none;
  text-transform: capitalize;
}
/* Button used to open the contact form - fixed at the bottom of the page */
.open-button {
  background-color: #555;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  opacity: 0.8;
  position: fixed;
  bottom: 23px;
  right: 28px;
  width: 280px;
}
/* The popup form - hidden by default */
.form-popup {
  display: none;
  position: fixed;
  /*bottom: 0;*/
  /*right: 15px;*/
  overflow: hidden;
  border: 3px solid #f1f1f1;
  z-index: 9;
  top: 50%;
  left: 50%;
  margin-right: -50%;
  transform: translate(-50%, -50%)
}

/* Add styles to the form container */
.form-container {
  max-width: 300px;
  padding: 10px;
  background-color: white;
  border-style: solid; 
  border-width: thin;
  border-color: #2d2d2d; /*#a4a4a5, #ddddde*/
}

/* Full-width input fields */
.form-container input[type=text], .form-container input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  border: none;
  background: #f1f1f1;
}


/* When the inputs get focus, do something */
.form-container input[type=text]:focus, .form-container input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Set a style for the submit/login button */
.form-container .btn {
/*  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  width: 100%;
  margin-bottom:10px;
  opacity: 0.8;*/
    width: 99%;
}

/* Add a red background color to the cancel button */
.form-container .cancel {
  width: 99%;
}

/* Add some hover effects to buttons */
.form-container .btn:hover, .open-button:hover {
  opacity: 1;
}
.shortlist_desc{
  padding: 7px;
  resize: vertical;
  max-height: 370px;
}
.btn-space {
    margin-bottom: 4px;
}
</style>
<div class="form-popup form-container" id="add-info-form">
  <form class="form-container">
    <span><b>Add a note that's visible only for you</b></span>
    <textarea placeholder="Enter note" id="shortlist_desc" name="shortlist_desc" class="shortlist_desc btn-space" rows="10" cols="35" style="margin-bottom: 10px;"></textarea>
    <span id="add_info_error" style="color: red;"></span>
    <button type="button" class="black-border-btn btn btn-space" id="sbt-add-info" value=""> <span id="info-sbt-btn-text">Add</span></button>
    <input type="hidden" name="bid_id" id="bid_id">
    <button type="button" class="black-border-btn btn cancel btn-space" onclick="closeForm()">Cancel</button>
    <button type="button" class="black-border-btn btn btn-space" id="sbt-delete-info" value=""> <span id="info-sbt-btn-text">Delete</span></button>
  </form>
</div>
<script>
  $('.add-info-btn').click(function(){
    btn_id       = $(this).attr("id");
    bid_id   = $('.bid_id_'+btn_id).val();
    contest_nm   = $('.contest_name_'+btn_id).val();
    contest_info = $('.contest_info_'+btn_id).val();
    if(contest_info != '')
    {
      $('#sbt-add-info').text('Update');
    }
    $('#sbt-add-info').attr('value',bid_id);
    $('#contestnm').html(contest_nm);
    $('#shortlist_desc').val(contest_info);
  })  

  $('#sbt-add-info').click(function(){

    $('#add_info_error').html('');
    btn_id           = $(this).attr("id");
    bid_id       = $('#bid_id').val(); 
    shortlist_desc = $('#shortlist_desc').val();
    token = '{{csrf_token()}}';

     $.ajax({
        url     : '{{$module_url_path}}/adding_shortlist_desc',
        type    : "POST",
        data    : {bid_id : bid_id,_token : token,shortlist_desc :shortlist_desc},
        success : function(res){
          $('.cd-top').click();
          location.reload();
        }
      })
  })

  $('#sbt-delete-info').click(function(){
    $('#add_info_error').html('');
    btn_id           = $(this).attr("id");
    bid_id       = $('#bid_id').val(); 
    token = '{{csrf_token()}}';
      $.ajax({
        url     : '{{$module_url_path}}/remove_shortlist_desc/'+bid_id,
        type    : "GET",
        success : function(res){
          $('.cd-top').click();
          location.reload();
        }
      })
  })

  

  function openForm(ref) {
    var bid_id = $(ref).attr('data-bid-id');
    var shortlist_desc = $(ref).attr('title');
    $('#shortlist_desc').val(shortlist_desc);
    $('#bid_id').val(bid_id);    
    document.getElementById("add-info-form").style.display = "block";
  }
  function closeForm() {
    $('.shortlist_desc').val('');
    document.getElementById("add-info-form").style.display = "none";
  }
</script>