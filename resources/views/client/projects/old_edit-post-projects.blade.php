@extends('client.layout.postmaster')                
@section('main_content')
<link href="{{url('/public')}}/assets/select2/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="{{url('/public')}}/assets/select2/dist/js/select2.full.js"></script>
<div class="middle-container">
   <form action="{{url('/')}}/client/projects/update/{{$id}}" method="POST" id="form-post_projects" name="form-post_projects" enctype="multipart/form-data" files ="true">
      {{ csrf_field() }}
      <input type="hidden" readonly="" id="project_id" value="{{isset($arr_info['id']) ? base64_encode($arr_info['id']):''}}"></input>
      <div class="container">
         <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
               @include('front.layout._operation_status')
               <div class="contest-title">
                   <h2>{{trans('client/projects/updatepost.text_update_job')}}</h2>
               </div>
               <div class="audio-job">
                  <div class="audi-job-bx post-job-wrapper">
                   
                     <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-6">
                          <div class="user-box">
                          <div class="p-control-label">{{trans('client/projects/updatepost.text_category_title')}}</div>
                           <div class="input-bx">
                              <div class="droup-select">
                                 <select class="droup getcat mrns tp-margn" data-rule-required="true" name="category" id="category">
                                    <option value="">{{ trans('new_translations.select_category')}}</option>
                                    @if(isset($arr_categories) && sizeof($arr_categories)>0)
                                    @foreach($arr_categories as $categories)
                                    @if($categories['id']==$arr_info['category_id'])
                                    <option value="{{$categories['id']}}" selected="selected">@if(isset($categories['category_title'])){{$categories['category_title']}}@endif</option>
                                    @else
                                    <option value="{{$categories['id']}}">@if(isset($categories['category_title'])){{$categories['category_title']}}@endif</option>
                                    @endif
                                    @endforeach
                                    @endif
                                 </select>
                                 <span class='error'>{{ $errors->first('category') }}</span>
                              </div>
                           </div>
                           </div>
                        </div>
                        
                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <div class="user-box">
                            <div class="p-control-label">{{trans('client/projects/updatepost.text_project_details')}}</div>
                            <div class="input-bx"><input data-rule-required="true" type="text" name="project_name" id="project_name" class="input-lsit beginningSpace_restrict" placeholder="{{trans('client/projects/updatepost.entry_project_details')}}" value="{{$arr_info['project_name'] or ''}}">
                              <span class='error'>{{ $errors->first('project_name') }}</span>
                            </div>
                            </div>
                        </div>
                       
                        <div class="clr"></div>
                        <?php
                           $project_has_skills  = [];
                           if(isset($arr_info['project_skills']) && count($arr_info['project_skills'])>0) {
                             foreach ($arr_info['project_skills'] as $key => $project_skills) {
                               array_push($project_has_skills, isset($project_skills['skill_id']) ? $project_skills['skill_id']:"");            
                             }
                           }
                        ?>  
                        <div class="col-sm-12 col-md-12 col-lg-12">
                           <div class="user-box">
                              <div class="p-control-label">{{trans('client/projects/updatepost.text_project_skills')}} <span class="inline-note">( <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span> {{trans('client/projects/updatepost.text_project_subskills')}})</span></div>
                             
                              <div class="droup-select">
                                 <select data-rule-required="true" name="project_skills[]" id="project_skills" multiple="multiple" class="droup">
                                    <option>--Select Skill--</option>
                                    @if(isset($arr_skills) && sizeof($arr_skills)>0)
                                       @foreach($arr_skills as $skills)
                                          @if(isset($skills['skill_name']) && $skills['skill_name']!="")
                                          <option value="{{$skills['id']}}"  @if(in_array($skills['id'],$project_has_skills)) selected="selected" @endif  >
                                          {{$skills['skill_name']}}
                                          </option>
                                          @endif
                                       @endforeach
                                    @endif
                                 </select>
                                 <span class='error'>{{ $errors->first('project_skills') }}</span>
                              </div>
                           </div>
                        </div>
                       
                        <div class="clr"></div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                           <div class="user-box edit-method">
                              <div class="p-control-label">{{trans('client/projects/updatepost.text_project_description')}}</div>
                              <div class="txt-edit"> {{trans('client/projects/updatepost.text_project_subdescription')}}</div>
                              <!-- <div class="txt-edit"> {{trans('client/projects/updatepost.text_project_limit_description')}}</div>-->
                              <span class="txt-edit" id="project_description_msg">
                                 {{trans('client/projects/updatepost.text_project_limit_description')}}
                              </span>

                              <textarea data-rule-required="true"  data-rule-maxlength="1000" rows="10" cols="30" class="text-area beginningSpace_restrict" id="project_description" name="project_description" data-gramm="" data-txt_gramm_id="21e619fb-536d-f8c4-4166-76a46ac5edce" placeholder="{{trans('client/projects/updatepost.entry_project_description')}}" onkeyup="javascript: return textCounter(this,1000);" >{{$arr_info['project_description']}}</textarea>
                              <span class='error'>{{ $errors->first('project_description') }}</span>
                           </div>
                        </div>
                        <div class="clr"></div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                           <div class="user-box">
                              <div class="p-control-label">{{trans('client/projects/post.text_project_upload')}}
                              </div>
                              <div class="attachment-block">
                                  <div class="update-pic-btns">
                                      <button href="#" class="up-btn"><i class="fa fa-cloud-upload"></i> Upload File</button>
                                      <input id="logo-id" name="project_attachment" onchange="Upload()" type="file" class="attachment_upload" >
                                  </div>
                                  <div class="clearfix"></div>
                                  <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span> <div class="note">{{trans('client/projects/post.text_project_validupload')}}</div>
                                  <div class="clearfix"></div>
                                  <div class="file-img">
                                    @if(isset($arr_info['project_attachment']) &&  $arr_info['project_attachment'] != "" && file_exists('public/uploads/front/postprojects/'.$arr_info['project_attachment']))
                                       @php $explode = explode('.',$arr_info['project_attachment']); @endphp
                                          @if(isset($explode[1]) && 
                                                    $explode[1] == 'jpg'  || 
                                                    $explode[1] == 'jpeg' ||
                                                    $explode[1] == 'gif'  ||
                                                    $explode[1] == 'png') 
                                            @php $attc_src = url('/').config('app.project.img_path.project_attachment').$arr_info['project_attachment']; @endphp
                                          @elseif(isset($explode[1]))
                                            @php $attc_src = url('/front/images/file_formats/'.$explode[1].'.png'); @endphp
                                          @else
                                            @php $attc_src = url('/front/images/doc-image.png'); @endphp
                                          @endif 
                                    @endif
                                  <img src="{{isset($attc_src)?$attc_src:''}}" class="img-responsive img-preview" alt=""/></div>
                              </div>
                           </div>
                        </div>
                       
                        <div class="clr"></div>
                        <div class="col-sm-12 col-md-6 col-lg-6">
                          <!-- <h3>{{trans('client/projects/updatepost.text_project_timeframe')}}</h3>-->
                           <?php /* <div class="input-bx d-se">
                              <div class="row">
                                 <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                       <div class="col-sm-12 col-md-12 col-lg-12">
                                          <div class="title-input">{{trans('client/projects/updatepost.text_project_start_date')}}</div>
                                       </div>
                                       <div class="col-sm-12 col-md-12 col-lg-12">
                                          <div class="input-datepickr input-bx input-lsit">
                                             <input data-rule-required="true" class="datepicker-start box-de" placeholder="{{trans('client/projects/updatepost.entry_project_start_date')}}" id="start_date" name="start_date" type="text" data-show-preview="false" value="{{$arr_info['project_start_date']}}">
                                             <i class="fa fa-calendar" id="first_date"></i>
                                             <span class='error'>{{ $errors->first('start_date') }}</span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                       <div class="col-sm-12 col-md-12 col-lg-12">
                                          <div class="title-input">{{trans('client/projects/updatepost.text_project_end_date')}}</div>
                                       </div>
                                       <div class="col-sm-12 col-md-12 col-lg-12">
                                          <div class="input-datepickr input-bx input-lsit">
                                             <input data-rule-required="true" class="datepicker-start box-de" placeholder="{{trans('client/projects/updatepost.entry_project_end_date')}}" id="end_date" name="end_date" type="text" data-show-preview="false" value="{{$arr_info['project_end_date']}}">
                                             <i class="fa fa-calendar" id="last_date"></i>
                                             <span class='error'>{{ $errors->first('end_date') }}</span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div> */ ?>
                           <div class="clr"></div>
                        </div>
                        
                        <div class="clr"></div>

                        {{--  Project cost section --}}
                        <div class="col-sm-12 col-md-6 col-lg-6">
                           <div class="user-box">
                              <div class="p-control-label">{{trans('client/projects/updatepost.text_project_duration')}}</div>
                              <div class="input-bx">
                                 <div class="input-bx"><input data-rule-required="true" type="text" name="project_duration" id="project_duration" class="input-lsit beginningSpace_restrict char_restrict" placeholder="{{trans('client/projects/updatepost.entry_project_duration')}}" value="{{$arr_info['project_expected_duration']}}">
                                    <span class='error'>{{ $errors->first('project_duration') }}</span>
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                        <div class="col-sm-12 col-md-6 col-lg-6">
                           <div class="user-box radio-wrapper">
                              <div class="p-control-label">{{trans('client/projects/updatepost.text_project_budget')}}</div>
                              <div class="input-bx">
                                 <div class="radio_area regi">
                                    <input type="radio" class="css-checkbox" id="radio_fixed" value="1" name="price_method" 
                                    @if(isset($arr_info["project_pricing_method"]) && $arr_info['project_pricing_method'] == "1"){{ 'checked=""' }} @endif   
                                    ><label class="css-label radGroup1" for="radio_fixed">{{trans('client/projects/updatepost.text_pay_fixed_price')}} </label>
                                 </div>
                                 <div class="radio_area regi">
                                    <input type="radio" class="css-checkbox" id="radio_hourly" value="2" name="price_method"
                                    @if(isset($arr_info["project_pricing_method"]) && $arr_info['project_pricing_method'] == "2"){{ 'checked=""' }} @endif   
                                    >
                                    <label class="css-label radGroup1" for="radio_hourly">{{trans('client/projects/updatepost.text_hourly_rate')}}</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                       
                        <div class="clr"></div>
                        <div class="col-sm-12 col-md-6 col-lg-6">
                           <div class="user-box">
                              <div class="p-control-label"> {{trans('client/projects/updatepost.text_project_currency')}}</div>
                              <div class="droup-select">
                                 <select name="project_currency" id="project_currency" class="droup mrns tp-margn" data-rule-required="true">
                                    @if(isset($arr_project_currency) && sizeof($arr_project_currency)>0)
                                    <option value="">{{trans('client/projects/updatepost.text_select_currency')}}</option>
                                    @foreach($arr_project_currency as $key => $currency)
                                    <option value="{{$key}}"
                                    @if(trim($arr_info['project_currency']) == trim($key))
                                    selected="" 
                                    @endif
                                    >{{$currency}}</option>
                                    @endforeach
                                    @endif
                                 </select>
                                 <span class='error'>{{ $errors->first('project_currency') }}</span>
                              </div>
                           </div>
                        </div>
                        <?php 
                           $style_one = "";
                           $style_two = "";
                           if($arr_info['is_custom_price'] == '1')
                           {     
                              $style_one = "display:none;";
                              $style_two = "display:none;";
                           }
                           else if($arr_info['project_pricing_method'] == '1')
                           {
                              $style_one = 'display:block;';
                           }
                           else if($arr_info['project_pricing_method'] == '2')
                           {
                              $style_two = 'display:block;';
                           }
                        ?>
                        <div class="col-sm-12 col-md-6 col-lg-6" id="section_fixed_price"   style="{{ $style_one }}">
                           <div class="user-box">
                              <div class="p-control-label">{{trans('client/projects/updatepost.text_project_cost')}}</div>
                              <div class="droup-select">
                                 <select name="fixed_rate" id="project_cost" class="droup mrns tp-margn"  onchange="set_custom_rates('1')" data-rule-required="true">
                                    @if(isset($aar_fixed_rates) && sizeof($aar_fixed_rates)>0)
                                       <option value="">{{trans('client/projects/updatepost.text_select_project_cost')}}</option>
                                       @foreach($aar_fixed_rates as $key => $fix_rate)
                                          <option value="{{$key}}"
                                             @if(trim($arr_info['project_cost']) == trim($key))
                                             selected="selected" 
                                             @endif
                                          >{{$fix_rate}}</option>
                                       @endforeach
                                    @endif
                                       <option value="CUSTOM_RATE"
                                       @if($arr_info['is_custom_price'] == '1')
                                       selected="selected" 
                                       @endif
                                    >{{trans('client/projects/updatepost.text_customize_budget')}}</option>
                                 </select>
                                 <span class='error'>{{ $errors->first('hourly_rate') }}</span>
                                 <span class='error'>{{ $errors->first('fixed_rate') }}</span>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-12 col-md-8 col-lg-6"  id="section_hourly_rate"  style="{{ $style_two }}" >
                           <div class="input-bx d-se">
                              <div class="title-input">{{trans('client/projects/updatepost.text_hourly_title')}}</div>
                              <div class="droup-select input-bx">
                                 <select name="hourly_rate" id="hourly_rate" class="droup mrns tp-margn" onchange="set_custom_rates('2')">
                                    <option value="">Select hourly rate</option>
                                    @if(isset($aar_hourly_rates) && sizeof($aar_hourly_rates)>0)
                                    @foreach($aar_hourly_rates as $key => $fix_rate)
                                    <option value="{{$key}}"
                                    @if($arr_info['project_cost']==$key)
                                    selected="" 
                                    @endif
                                    >{{$fix_rate}} / {{trans('client/projects/updatepost.text_per_hour')}}
                                    </option>
                                    @endforeach
                                    @endif
                                    <option value="CUSTOM_RATE" 
                                    @if($arr_info['is_custom_price'] == '1')
                                    selected="" 
                                    @endif
                                    >{{trans('client/projects/updatepost.text_customize_budget')}}</option>
                                 </select>
                                 <span class='error'>{{ $errors->first('hourly_rate') }}</span>
                                 <span class='error'>{{ $errors->first('fixed_rate') }}</span>
                              </div>
                           </div>
                        </div>
                        {{-- custom rates function --}}
                        <?php 
                           $custom_rate = $arr_info['project_cost'];
                              
                           if(isset($arr_info['is_custom_price']) && $arr_info['is_custom_price'] =="0")
                           {
                             $custom_rate = "";
                           }
                        ?>
                        <div class="col-sm-12 col-md-8 col-lg-4" id="custom_rate" 
                        @if($arr_info['is_custom_price'] == '1')
                        style="display: block;"
                        @else
                        style="display: none"
                        @endif
                        >
                        <div class="input-bx d-se">
                           <div class="title-input"> {{trans('client/projects/updatepost.text_customize_budget')}}</div>
                           <div class="cust-budget input-bx">
                              <input name="custom_rate" data-rule-required="true" value="{{ $custom_rate }}" data-rule-number="true" data-rule-min="0" type="text" class="input-lsit" data-rule-max="25000" data-rule-min="1"></input>   
                              <span class='error'>{{ $errors->first('custom_rate') }}</span>
                           </div>
                        </div>
                     </div>
                     {{--  project cost section --}}
                     <div class="clr"></div>
                      <div class="col-sm-12 col-md-6 col-lg-6">
                          <div class="row">
                             <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="title-input">{{trans('client/projects/post.text_bid_closing_date')}}</div>
                             </div>
                             <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="input-datepickr input-bx input-lsit">
                                   <input data-rule-required="true" class="datepicker-start box-de" placeholder="{{trans('client/projects/post.text_bid_closing_date')}}" id="bid_closing_date" name="bid_closing_date" type="text" data-show-preview="false" value="{{$arr_info['bid_closing_date']}}">
                                   <i class="fa fa-calendar" id="last_date"></i>
                                   <span class='error'>{{ $errors->first('bid_closing_date') }}</span>
                                </div>
                             </div>
                          </div>
                      </div>
                     <div class="clr"></div>

                     <!--
                     <div class="col-sm-12 col-md-8 col-lg-6">
                        <div class="input-bx">
                           <h3>{{trans('client/projects/updatepost.text_handle_by')}}</h3>
                           <div class="input-bx">
                              <div class="radio_area regi">
                                 <input type="radio" class="css-checkbox" id="radio3" value="1" name="handle_by" @if($arr_info['project_handle_by']=='1') checked="checked" @endif>
                                 <label class="css-label radGroup1" for="radio3">{{trans('client/projects/updatepost.text_myself')}}</label>
                              </div>
                              <div class="radio_area regi">
                                 <input type="radio" class="css-checkbox" id="radio4" value="2" name="handle_by" @if($arr_info['project_handle_by']=='2') checked="checked" @endif)>
                                 <label class="css-label radGroup1" for="radio4">{{trans('client/projects/updatepost.text_project_manager')}}</label>
                              </div>
                           </div>
                        </div>
                     </div>
                     -->


                      <div class="col-sm-12 col-md-8 col-lg-6">
                        <div class="input-bx">
                           <h3>{{trans('client/projects/updatepost.text_handle_by')}}</h3>
                           <div class="input-bx">
                              <div class="radio_area regi">
                                 <input type="radio" class="css-checkbox" id="radio3"  checked="checked">
                                 <label class="css-label radGroup1" for="radio3">
                                 @if($arr_info['project_handle_by']=='1')
                                    {{trans('client/projects/updatepost.text_myself')}}
                                 @else
                                    {{trans('client/projects/updatepost.text_project_manager')}}
                                 @endif
                                 </label>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="clr"></div>
                     <div class="col-sm-12 col-md-12 col-lg-12">
                        {{-- <div class="terms">{{trans('client/projects/updatepost.text_terms')}} <span>{{trans('client/projects/updatepost.text_terms1')}} </span>@if(!empty($arr_static_terms['page_slug']))<a href="{{url('/public')}}/info/{{$arr_static_terms['page_slug']}}">@else <a href="javascript:void(0);"> @endif {{trans('client/projects/updatepost.text_terms2')}}</a> {{trans('client/projects/updatepost.text_terms3')}} @if(!empty($arr_static_privacy['page_slug'])) <a href="{{url('/public')}}/info/{{$arr_static_privacy['page_slug']}}"> @else <a href="javascript:void(0);"> @endif {{trans('client/projects/updatepost.text_terms4')}}</a>.</div> --}}
                        <br>
                        <div class="right-side"><button type="submit" class="normal-btn pull-right">{{trans('client/projects/updatepost.text_update_job')}}</button></div>
                     </div>
                  </div>
              </form>
          </div>
       </div>
     </div>
   </div>
</div>
</div>
</div>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/additional-methods.min.js"></script>
<!--date picker start here-->
<link rel="stylesheet" type="text/css"  href="{{url('/public')}}/front/css/jquery-ui.css"/>
<script src="{{url('/public')}}/front/js/jquery-ui.js" type="text/javascript"></script>
<script> 
   $(document).ready(function(){
   $("#bid_closing_date" ).datepicker({
        dateFormat: 'yy-mm-dd',
        //'startDate': new Date(),
        minDate: 'today',
   }) 
   $("#start_date" ).datepicker({
      dateFormat: 'yy-mm-dd',
      'startDate': new Date(),
      minDate: 'today',
   }).on('change',function(e)
   {
    var date2 = $('#start_date').datepicker('getDate');
    date2.setDate(date2.getDate() + 1);
    //sets minDate to dt1 date + 1
    $('#end_date').datepicker('option', 'minDate', date2);
   
     var start_date = $("#start_date").val();
     var end_date = $("#end_date").val();
     updateProjectDuration(start_date,end_date);
   });
   
   $('#first_date').click(function ()
   {
      $('#first_date').datepicker("show");
   });
   
   $( "#end_date" ).datepicker({
      dateFormat: 'yy-mm-dd',
      'startDate': new Date(),
      minDate: 'today',
   }).on('change',function(e)
   { 
       var start_date = $("#start_date").val();
       var end_date = $("#end_date").val();
   
       updateProjectDuration(start_date,end_date);
   });
   
   });
      
</script>
<script>
   function updateProjectDuration(start_date,end_date)
   {  
   
      start_date = new Date(start_date);
      end_date = new Date(end_date);
   
    if(start_date==false || end_date==false)
    {
      return false;
    }
    project_duration = dateDiffInDays(start_date,end_date);
      //console.log(project_duration);return false;
    $("#project_duration").val(project_duration);
   }
   
   function dateDiffInDays(a, b) 
   {
   
    var _MS_PER_DAY = 1000 * 60 * 60 * 24;
    // Discard the time and time-zone information.
    var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
   
    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
   } 
</script>
<script type="text/javascript">
   $("#form-post_projects").validate({
         errorElement: 'span',
         });
   
   //<!--file upload js script-->  
function browseImage()
{
  $("#project_attachment").trigger('click');
}
 
function removeBrowsedImage()
{
   if(!confirm('Are you sure ? Do you want to delete project attachment ?'))
   {
      return false;
   }

   var project_id = $('#project_id').val();

   if(project_id != "")
   {


      $('#project_attachment_name').val("");
      $("#btn_remove_image").hide();
      $("#project_attachment").val("");

      $.ajax({
         url:"{{url('/public')}}"+"/client/delete_project_attachment",
         data :{'project_id':project_id}, 
         success:function (response)
         {
            if(typeof response.status != "undefined" && response.status == "SUCCESS_PROJECT")
            {
               $('#attachment_dwn').hide();
               $('#msg_project_attchment').html(response.msg).show();
            }
         }
      });  
   }
}

   
   $(document).ready(function()
   {
    // This is the simple bit of jquery to duplicate the hidden field to subfile
    $('#project_attachment').change(function()
    {
      if($(this).val().length>0)
      {
       $("#btn_remove_image").show();
    }
   
    $('#project_attachment_name').val($(this).val());
    });
   
   $(".getcat").change(function()
   {  
      var id=$(this).val();
      var site_url ="{{url('/')}}";
      var dataString = 'id='+ id;
      var token = $('input[name="_token"]').val();  
      $.ajax
      ({
        type: "POST",
        url: site_url+'/client/projects/subcatdata'+"?_token="+token,
        data: dataString,
        cache: false,
        success: function(html)
        {
          $(".subcategory").html(html);
        } 
      });   
    });
   
   /* $('#radio1').click(function(){
       
         $(".hourly").show();
     });
    $('#radio2').click(function(){
       
          $(".hourly").hide();
     });*/
   
   });
   
   $(document).ready( function () {
      $('#radio_hourly').click(function()
      {
           $("#section_hourly_rate").show();
           $("#section_fixed_price").hide();
       });
      $('#radio_fixed').click(function()
      {
            $("#section_hourly_rate").hide();
            $("#section_fixed_price").show();
       });
      
      if (jQuery('#radio_hourly').prop('checked')) 
      {
        $("#section_hourly_rate").show();
         $("#section_fixed_price").hide();
      }
      else if (jQuery('#radio_fixed').prop('checked'))
      {
          $("#section_hourly_rate").hide();
          $("#section_fixed_price").show();
      } 
        
      /* Initializing the multiple select box for skills */       
      $('#project_skills').select2({
         maximumSelectionLength: 5
      });
   
   });
   
   /* function for setting custom  input box rates */
   
   function set_custom_rates(rate_type)
   {
    if(rate_type == '1')
    {
      ref = "project_cost";
    }
   
    if(rate_type == '2')
    {
      ref = "hourly_rate";
    }
   
    if($('#'+ref).val() == 'CUSTOM_RATE')
    {
      $('#custom_rate').show();
    }
    else
    {
      $('#custom_rate').hide();
      $('input[name="custom_rate"]').prop('value','');  
    } 
   }
   

   function textCounter(field,maxlimit)
   {
      var countfield = 0;
      if ( field.value.length > maxlimit ) 
      {
        field.value = field.value.substring( 0, maxlimit );
        return false;
      } 
      else 
      {
         countfield = maxlimit - field.value.length;
         var message = 'You have left '+countfield+' characters for project description.';
         jQuery('#project_description_msg').html(message);
         return true;
      }
   }


</script>
<script type="text/javascript">
   /*$(document).ready(function() {
    var brand = document.getElementById('logo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
              
            };
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);
    });
       
});*/

</script> 
<script type="text/javascript">
function Upload() {
    $('.note').css('color','black');
    //Get reference of FileUpload.
    $('.img-preview').attr('src', ""); 
    var logo_id = document.getElementById("logo-id");
    //Check whether the file is valid Image.
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+()$");
    if (regex.test(logo_id.value.toLowerCase())) {
        //Check whether HTML5 is supported.
        if (typeof (logo_id.files) != "undefined") {
            var file_extension = logo_id.value.substring(logo_id.value.lastIndexOf('.')+1, logo_id.length); 
            var size = logo_id.files[0].size;
            if(size > 2000000){
               $('.note').css('color','red');
               $('.note').html("{{trans('client/projects/post.text_project_validsizeupload')}}");
               document.getElementById("logo-id").value = "";
               return false;
            }

            if(file_extension != 'jpg' && 
               file_extension != 'jpeg' &&
               file_extension != 'gif' &&
               file_extension != 'png' &&
               file_extension != 'xlsx' &&
               file_extension != 'pdf' &&
               file_extension != 'docx' &&
               file_extension != 'doc' &&
               file_extension != 'txt' &&
               file_extension != 'odt'){
               $('.note').css('color','red');
               $('.note').html("{{trans('client/projects/post.text_project_validupload')}}");
               document.getElementById("logo-id").value = "";
               return false;
            } 
            //Initiate the FileReader object.
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(logo_id.files[0]);
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();
                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;
                var size  = this.size;
                //Validate the File Height and Width.
                if(file_extension == 'xlsx' ||
                 file_extension == 'pdf' ||
                 file_extension == 'docx' ||
                 file_extension == 'doc' ||
                 file_extension == 'txt' ||
                 file_extension == 'odt'){
                    $('.img-preview').attr('src', site_url+'/front/images/file_formats/'+file_extension+'.png'); 
                }
                image.onload = function (e) {
                    var height = this.height;
                    var width  = this.width;
                    $('.note').html("{{trans('client/projects/post.text_project_validupload')}}");
                    $('.note').css('color','#646464');
                    if(file_extension == 'jpg' || 
                       file_extension == 'jpeg' ||
                       file_extension == 'gif' ||
                       file_extension == 'png'){
                            $('.img-preview').attr('src', image.src);
                       }  
                    return true;
                };
            }
        } else {
            $('.note').css('color','red');
            $('.note').html("This browser does not support HTML5.");
            document.getElementById("logo-id").value = "";
            return false;
        }
    } else {
        $('.note').css('color','red');
        $('.note').html("{{trans('client/projects/post.text_project_validupload')}}");
        document.getElementById("logo-id").value = "";
        return false;
    }
}
</script>
@stop