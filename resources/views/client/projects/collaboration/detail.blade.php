@extends('client.layout.master') @section('main_content')
<div class="col-sm-7 col-md-8 col-lg-9">
    <div class="coll-listing-section">
        <div class="contest-titile">
            <div class="sm-logo d-inlin"><img src="{{url('/public/front/images/small-logo.png')}}" class="img-responsive"></div>
            <span class="d-inlin">Collaboration details</span>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-10 col-lg-10">
                <h2><a href="{{url('/client/projects/details/')}}/{{base64_encode($project_id)}}" style="color:#8e8e8e;">{{isset($project_attachment['project_details']['project_name'])?$project_attachment['project_details']['project_name']:'-'}}</a></h2>
            </div>
            <div class="col-sm-12 col-md-2 col-lg-2">
                 <a href="{{url('/client/projects/details/')}}/{{base64_encode($project_id)}}" style="margin: 23px 0 0;" class="back-btn pull-right"><i class="fa fa-reply-all"></i> {{trans('client/contest/common.text_back')}}</a>
            </div>
        </div>
    </div>
    <!-- gallery hear -->
    @if(isset($project_attachment['attachment']) && $project_attachment['attachment'] != "")
     @php 
        $attachment             = isset($project_attachment['attachment'])?$project_attachment['attachment']:"";
        $get_attatchment_format = get_attatchment_format($attachment);
        $sidebar_information    = sidebar_information(isset($project_attachment['expert_user_id'])?$project_attachment['expert_user_id']:'0');
     @endphp
     <div class="contest-slider project-slider">
        <h4 class="entry-title">File #{{isset($project_attachment['attchment_no'])?$project_attachment['attchment_no']:"-"}}
        </h4>
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
                @if(isset($get_attatchment_format) &&
                            $get_attatchment_format == 'doc'  ||
                            $get_attatchment_format == 'docx' ||
                            $get_attatchment_format == 'odt'  ||
                            $get_attatchment_format == 'pdf'  ||
                            $get_attatchment_format == 'txt'  ||
                            $get_attatchment_format == 'xlsx')
                    <div class="carousel-inner">
                        <div class="item active">
                            @if(file_exists('public/uploads/front/project_attachments/'.$project_attachment['attachment']))
                            <img src="{{url('/public')}}/front/images/file_formats/{{$get_attatchment_format}}.png" alt="Los Angeles" style="width:100%;height: 400px;">
                            @else
                            <img src="{{url('/public')}}/front/images/file_formats/image.png"  class="img-responsive" alt=""/>
                            @endif
                        </div>
                    </div>
                @else
                    <div class="carousel-inner">
                        <div class="item active">
                            @if(file_exists('public/uploads/front/project_attachments/'.$project_attachment['attachment']))
                            <img src="{{url('/uploads/front/project_attachments/')}}/{{$project_attachment['attachment']}}" class="img-responsive">
                            @else
                            <img src="{{url('/public')}}/front/images/archexpertdefault/default-arch.jpg"  class="img-responsive" alt=""/>
                            @endif
                        </div>
                    </div>
                @endif 
                <!-- Left and right controls -->
                @if(isset($previous) && $previous!=0)
                <a class="left carousel-control" style="z-index: 99;" href="{{url('/client/projects/collaboration/details')}}/{{isset($previous)?base64_encode($previous):'0'}}/{{base64_encode($project_id)}}">
                  <span class="sr-only">Previous</span>
                </a>
                @endif
                @if(isset($next) && $next!=0)
                <a class="right carousel-control" style="z-index: 99;" href="{{url('/client/projects/collaboration/details')}}/{{isset($next)?base64_encode($next):'0'}}/{{base64_encode($project_id)}}">
                  <span class="sr-only">Next</span>
                </a>
                @endif
              <div class="layout-details gallery-bottam">
                <div class="layout-details-right">
                    <div class="coll-person d-inlin">
                        <div class="collaboration-img">
                            @if(isset($sidebar_information['user_details']['profile_image']) && $sidebar_information['user_details']['profile_image']!=null && file_exists($profile_img_base_path.$sidebar_information['user_details']['profile_image']))
                             <img src="{{$profile_img_public_path.$sidebar_information['user_details']['profile_image']}}" class="img-responsive" alt=""/>
                            @else
                              <img src="{{$profile_img_public_path.'default_profile_image.png'}}" class="img-responsive" alt=""/>
                            @endif

                            @if(isset($sidebar_information['last_login_duration']) && $sidebar_information['last_login_duration']=='Active')
                              <div class="online-status"> <img src="{{url('/public/front/images/active.png')}}"   class=""></div>
                            @else
                              <div class="online-status"> <img src="{{url('/public/front/images/deactive.png')}}" class=""></div>
                            @endif
                        </div>
                        <div class="coll-name">{{isset($sidebar_information['user_details']['first_name'])?$sidebar_information['user_details']['first_name'].'  '.str_limit($sidebar_information['user_details']['last_name'],1,'.'):""}} 
                          <a href="javascript:void(0)" style="margin-top:5px;" class="applozic-launcher" data-mck-id="{{env('applozicUserIdPrefix')}}{{$sidebar_information['user_details']['user_id']}}" data-mck-name="{{isset($sidebar_information['user_details']['first_name'])?$sidebar_information['user_details']['first_name']:'Unknown user'}} 
                    @if(isset($sidebar_information['user_details']['last_name']) && $sidebar_information['user_details']['last_name'] !="") @php echo substr($sidebar_information['user_details']['last_name'],0,1).'.'; @endphp @endif" ><i class="fa fa-comment-o"></i></a>
                          <br>
                          
                          @if($sidebar_information['last_login_duration'] == 'Active')
                            <span class="flag" title="{{isset($sidebar_information['expert_timezone'])?$sidebar_information['expert_timezone']:'-'}}">
                                <span class="flag-image"> 
                                    @if(isset($sidebar_information['user_country_flag'])) 
                                      @php 
                                         echo $sidebar_information['user_country_flag']; 
                                      @endphp 
                                    @elseif(isset($sidebar_information['user_country'])) 
                                      @php 
                                        echo $sidebar_information['user_country']; 
                                      @endphp 
                                    @else 
                                      @php 
                                        echo '-'; 
                                      @endphp 
                                    @endif 
                                </span>
                                {{isset($sidebar_information['expert_timezone_without_date'])?$sidebar_information['expert_timezone_without_date']:'-'}}
                            </span>
                          @else
                            <span class="" title="{{$sidebar_information['last_login_full_duration']}}">
                              <span class="flag-image"> 
                                  @if(isset($sidebar_information['user_country_flag'])) 
                                    @php 
                                       echo $sidebar_information['user_country_flag']; 
                                    @endphp 
                                  @elseif(isset($sidebar_information['user_country'])) 
                                    @php 
                                      echo $sidebar_information['user_country']; 
                                    @endphp 
                                  @else 
                                    @php 
                                      echo '-'; 
                                    @endphp 
                                  @endif 
                              </span>
                              <a style="color:#4D4D4D;font-size: 14px;text-transform:none;cursor:text;">
                                 {{$sidebar_information['last_login_duration']}}
                              </a>
                            </span>
                          @endif

                        </div>
                    </div>

                    

                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="design-title">
            <p style="display: inline;" title="{{isset($project_attachment['attachment'])?$project_attachment['attachment']:'Unknown'}}">{{isset($project_attachment['attachment'])?$project_attachment['attachment']:'Unknown'}}</p>
            <a  style="font-size:1.5em;color:#2d2d2d"  style="display: inline;" class="pull-right" download href="{{url('/public/uploads/front/project_attachments/')}}/{{$project_attachment['attachment']}}"><i class="fa fa-cloud-download"></i></a>
        </div>
        @if(isset($project_attachment['is_own_work']) && $project_attachment['is_own_work'] == 0)
        <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span>
        {{isset($sidebar_information['user_details']['first_name'])?$sidebar_information['user_details']['first_name'].'  '.str_limit($sidebar_information['user_details']['last_name'],1,'.'):""}} : 
        {{ trans('contets_listing/listing.text_all_work_was_done_by_me_or_our_company') }}
        @elseif(isset($project_attachment['is_own_work']) && $project_attachment['is_own_work'] == 1)
        <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span>
        {{isset($sidebar_information['user_details']['first_name'])?$sidebar_information['user_details']['first_name'].'  '.str_limit($sidebar_information['user_details']['last_name'],1,'.'):""}} : 
        {{ trans('contets_listing/listing.text_the_work_was_partially_done_by_me_or_our_company') }}
        @else
        <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span>
        {{isset($sidebar_information['user_details']['first_name'])?$sidebar_information['user_details']['first_name'].'  '.str_limit($sidebar_information['user_details']['last_name'],1,'.'):""}} : 
        Not Available
        @endif

        <div class="clearfix"></div>
        <div class="design-title">
            {{isset($project_attachment['description'])?$project_attachment['description']:"-"}}
        </div>
        </div>
    @else
        <div class="search-grey-bx">
          <div class="no-record" >
             {{trans('expert/projects/awarded_projects.text_sorry_no_record_found')}}
          </div>
        </div> 
    @endif   
</div>
@stop 