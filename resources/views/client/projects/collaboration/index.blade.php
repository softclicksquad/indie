@extends('client.layout.master')                
@section('main_content')
<?php $user = Sentinel::check();?>
    <div class="col-sm-7 col-md-8 col-lg-9">
        @include('front.layout._operation_status')
        <div class="coll-listing-section project-coll">
            <div class="contest-titile">
                <div class="sm-logo d-inlin"><img src="{{url('/public/front/images/small-logo.png')}}" class="img-responsive"></div>
                <span class="d-inlin">Collaboration</span>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <h2><a href="{{url('/client/projects/details/')}}/{{base64_encode($project_id)}}" style="color:#8e8e8e;">{{isset($project_main_attachment['project_name'])?$project_main_attachment['project_name']:'-'}}</a></h2>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                     <a href="{{url('/client/projects/details/')}}/{{base64_encode($project_id)}}" style="margin: 23px 0 0;" class="back-btn pull-right"><i class="fa fa-reply-all"></i> {{trans('client/contest/common.text_back')}}</a>
                     @if(isset($project_attachment['data']) && sizeof($project_attachment['data']) >0 || isset($project_main_attachment['project_attachment']) && $project_main_attachment['project_attachment'] != "")
                     <a href="{{url('/client/projects/collaboration/make-zip/')}}/{{base64_encode($project_id)}}" style="margin: 23px 23px 0;" class="back-btn pull-right download-zip"><i class="fa fa-file-archive-o"></i> {{trans("new_translations.text_download_all_files")}}</a>
                     @endif
                </div>
            </div>
            <div class="row">
                <!-- project main attachment from project table -->
                @if(isset($project_main_attachment['project_attachment']) && $project_main_attachment['project_attachment'] != "")
                    <?php 
                        $attachment             = isset($project_main_attachment['project_attachment'])?$project_main_attachment['project_attachment']:"";
                        $get_attatchment_format = get_attatchment_format($attachment);
                    ?>
                    @if(isset($get_attatchment_format) &&
                        $get_attatchment_format == 'doc'  ||
                        $get_attatchment_format == 'docx' ||
                        $get_attatchment_format == 'odt'  ||
                        $get_attatchment_format == 'pdf'  ||
                        $get_attatchment_format == 'txt'  ||
                        $get_attatchment_format == 'xlsx')
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 coll-list">
                            <div class="coll-list-block">
                                <div class="coll-img p-relative">
                                    @if(file_exists('public/uploads/front/postprojects/'.$project_main_attachment['project_attachment']))
                                    <img src="{{url('/public')}}/front/images/file_formats/{{$get_attatchment_format}}.png" class="img-responsive">
                                    @else
                                    <img src="{{url('/public')}}/front/images/file_formats/image.png"  class="img-responsive" alt=""/>
                                    @endif
                                    <div class="coll-details">
                                        <div class="coll-person">
                                            <div class="coll-name"><!-- File #1 --></div>
                                        </div>
                                        <!-- <div class="avg-rating">
                                            <ul>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                            </ul>
                                        </div> -->
                                    </div>
                                    <div class="list-hover">
                                        <a style="font-size:2em;" download href="{{url('/public')}}{{config('app.project.img_path.project_attachment')}}{{$project_main_attachment['project_attachment']}}" class="view-button"><i class="fa fa-cloud-download"></i></a>
                                    </div>
                                </div>
                                <div class="design-title">
                                    <p>Main attachment</p>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 coll-list">
                                <div class="coll-list-block">
                                    <div class="coll-img p-relative">
                                        @if(file_exists('public/uploads/front/postprojects/'.$project_main_attachment['project_attachment']))
                                        <img src="{{url('/public')}}{{config('app.project.img_path.project_attachment')}}{{$project_main_attachment['project_attachment']}}" class="img-responsive">
                                        @else
                                        <img src="{{url('/public')}}/front/images/archexpertdefault/default-arch.jpg"  class="img-responsive" alt=""/>
                                        @endif
                                        <div class="coll-details">
                                            <div class="coll-person">
                                                <div class="coll-name"><!-- File #2 --></div>
                                            </div>
                                            <!-- <div class="avg-rating">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </div> -->
                                        </div>
                                        <div class="list-hover">
                                            <a style="font-size:2em;" download href="{{url('/public')}}{{config('app.project.img_path.project_attachment')}}{{$project_main_attachment['project_attachment']}}" class="view-button"><i class="fa fa-cloud-download"></i></a>
                                        </div>
                                    </div>
                                    <div class="design-title">
                                        <p>Main attachment</p>
                                    </div>
                                </div>
                        </div>
                    @endif
                    <div class="clearfix"></div>
                @endif
                <!-- end project main attachment from project table -->
                @if(isset($project_attachment['data']) && sizeof($project_attachment['data']) >0)
                    @foreach($project_attachment['data'] as $key => $attch)
                        @php 
                            $attachment             = isset($attch['attachment'])?$attch['attachment']:"";
                            $get_attatchment_format = get_attatchment_format($attachment);
                        @endphp
                        @if(isset($get_attatchment_format) &&
                            $get_attatchment_format == 'doc'  ||
                            $get_attatchment_format == 'docx' ||
                            $get_attatchment_format == 'odt'  ||
                            $get_attatchment_format == 'pdf'  ||
                            $get_attatchment_format == 'txt'  ||
                            $get_attatchment_format == 'xlsx')
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 coll-list">
                                <div class="coll-list-block">
                                    <div class="coll-img p-relative">
                                        @if(file_exists('public/uploads/front/project_attachments/'.$attch['attachment']))
                                        <img src="{{url('/public')}}/front/images/file_formats/{{$get_attatchment_format}}.png" class="img-responsive">
                                        @else
                                        <img src="{{url('/public')}}/front/images/file_formats/image.png"  class="img-responsive" alt=""/>
                                        @endif
                                        <div class="coll-details">
                                            <div class="coll-person">
                                                <div class="coll-name">File #{{isset($attch['attchment_no'])?$attch['attchment_no']:'-'}}</div>
                                            </div>
                                            <!-- <div class="avg-rating">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </div> -->
                                        </div>
                                        <div class="list-hover">
                                            <a href="{{url('/client/projects/collaboration/details')}}/{{isset($attch['id'])?base64_encode($attch['id']):'0'}}/{{base64_encode($project_id)}}" class="view-button">View</a>
                                        </div>
                                    </div>
                                    <div class="design-title">
                                        <p style="display: inline;" title="{{isset($attch['attachment'])?$attch['attachment']:'Unknown'}}">{{isset($attch['attachment'])?str_limit($attch['attachment'],14):'Unknown'}}</p>
                                        <a  style="font-size:1.5em;color:#2d2d2d"  style="display: inline;" class="pull-right" download href="{{url('/public/uploads/front/project_attachments/')}}/{{$attch['attachment']}}"><i class="fa fa-cloud-download"></i></a>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 coll-list">
                                    <div class="coll-list-block">
                                        <div class="coll-img p-relative">
                                            @if(file_exists('public/uploads/front/project_attachments/'.$attch['attachment']))
                                            <img src="{{url('/public/uploads/front/project_attachments/')}}/{{$attch['attachment']}}" class="img-responsive">
                                            @else
                                            <img src="{{url('/public')}}/front/images/archexpertdefault/default-arch.jpg"  class="img-responsive" alt=""/>
                                            @endif
                                            <div class="coll-details">
                                                <div class="coll-person">
                                                    <div class="coll-name">File #{{isset($attch['attchment_no'])?$attch['attchment_no']:'-'}}</div>
                                                </div>
                                                <!-- <div class="avg-rating">
                                                    <ul>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div> -->
                                            </div>
                                            <div class="list-hover">
                                                <a href="{{url('/client/projects/collaboration/details')}}/{{isset($attch['id'])?base64_encode($attch['id']):'0'}}/{{base64_encode($project_id)}}" class="view-button">View</a>
                                            </div>
                                        </div>
                                        <div class="design-title" >
                                            <p style="display: inline;" title="{{isset($attch['attachment'])?$attch['attachment']:'Unknown'}}">{{isset($attch['attachment'])?str_limit($attch['attachment'],14):'Unknown'}}</p>
                                            <a  style="font-size:1.5em;color:#2d2d2d;"  style="display: inline;" class="pull-right" download href="{{url('/public/uploads/front/project_attachments/')}}/{{$attch['attachment']}}"><i class="fa fa-cloud-download"></i></a>
                                        </div>
                                    </div>
                                </div>
                        @endif
                    @endforeach
                @endif
            </div>
            @if(isset($project_main_attachment['project_attachment']) && $project_main_attachment['project_attachment'] == "" && empty($project_attachment['data']) && sizeof($project_attachment['data']) <= 0)
                <div class="search-grey-bx">
                  <div class="no-record" >
                     {{trans('expert/projects/awarded_projects.text_sorry_no_record_found')}}
                  </div>
                </div> 
            @endif
            <!-- Pagination -->
            @include('front.common.pagination_view', ['paginator' => $arr_pagination])
            <!-- ends -->
        </div>
    </div>
<script type="text/javascript">
$(document).ready(function(){
   $('.download-zip').click(function(){
     setTimeout(function(){
        hideProcessingOverlay();
     },500);
   });
});
</script>    
@stop