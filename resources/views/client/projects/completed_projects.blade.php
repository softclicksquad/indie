@extends('client.layout.master')                
@section('main_content')
<div class="col-sm-7 col-md-8 col-lg-9">

   
     
          <div class="row">
        <div class="col-lg-8">
          @include('front.layout._operation_status')
           <div class="head_grn">{{trans('client/projects/completed_projects.text_completed_project_title')}}</div>
          </div>
           <div class="col-lg-4">
          <div class="droup-select">
          <select class="droup mrns tp-margn" onchange="sortByHandledBy(this)">
            <option value="">-- Sort By --</option>
            <option @if(isset($sort_by) && $sort_by == 'asc') selected="" @endif value="asc">Ascending</option>
            <option @if(isset($sort_by) && $sort_by == 'desc') selected="" @endif value="desc">Descending</option>
           {{--  <option @if(isset($sort_by) && $sort_by == 'is_urgent') selected="" @endif value="is_urgent">Urgent</option>
            <option @if(isset($sort_by) && $sort_by == 'public_type') selected="" @endif value="public_type">Public</option>
            <option @if(isset($sort_by) && $sort_by == 'private_type') selected="" @endif value="private_type">Private</option>
            <option @if(isset($sort_by) && $sort_by == 'is_nda') selected="" @endif value="is_nda">NDA</option> --}}
          </select>
          </div>
        </div>
          </div>
        <div class="clearfix"></div>
        @if(isset($arr_completed_projects['data']) && sizeof($arr_completed_projects['data'])>0)
        @foreach($arr_completed_projects['data'] as $proRec)
   
            <div class="white-block-bg big-space hover">
              <div class="project-left-side">
                  <div class="project-title">
                      <div class="search-freelancer-user-head">
                   <a href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}">
                   {{$proRec['project_name']}}
                   </a>
                          </div>
       
                     <div class="search-freelancer-user-location">
                          <span class="gavel-icon">
                            <img src="{{url('/public')}}/front/images/calendar.png" alt="" /></span> 
                            <span class="dur-txt">  {{trans('client/projects/completed_projects.text_est_time')}}: {{$proRec['project_expected_duration']}}</span>
                          @if(isset($proRec['edited_at']) && $proRec['edited_at']!=NULL)
                            <span class="freelancer-user-line">|</span>
                          @endif
                              
                      </div>
                    
                    @if(isset($proRec['edited_at']) && $proRec['edited_at']!=NULL)
                        @php $edited_time_ago = time_ago($proRec['edited_at']); @endphp
                        <div class="search-freelancer-user-location">
                            <span class="gavel-icon"><img src="{{url('/public')}}/front/images/clock.png" alt="" /></span>
                            <span class="dur-txt">Last edited : {{$edited_time_ago}}</span>
                        </div>
                    @endif

                  <div class="more-project-dec">{{str_limit($proRec['project_description'],160)}}</div>
                    @if(isset($proRec['project_reviews']) && count($proRec['project_reviews']) > 0) 
                      <div class="more-project-dec">
                       @foreach($proRec['project_reviews'] as $key => $review )
                        <?php
                           $user = Sentinel::check();
                           if($review['from_user_id'] == $user->id){
                             if($user->inRole('client')){
                               $title = trans('client/projects/completed_projects.text_your_rating');
                             }
                           }
                           elseif($review['from_user_id'] == $proRec['expert_user_id'])
                           { 
                              $title = "";
                              $chk_user = Sentinel::findById($review['from_user_id']); 
                              if(isset($chk_user) && $chk_user != FALSE)
                              {
                                  if(isset($chk_user->expert_details->first_name) && isset($chk_user->expert_details->last_name)){  
                                     $title = $chk_user->expert_details->first_name.' '.substr($chk_user->expert_details->last_name,0,1);
                                  }else{
                                   $title = "N/A"; 
                                  }
                              }
                           }
                            $text_hour = '';
                            if(isset($proRec['project_pricing_method']) && $proRec['project_pricing_method'] == '2')
                            {
                                $text_hour = trans('common/_top_project_details.text_hour');
                            }
                        ?>   

                        @if(isset($proRec['project_reviews']) && sizeof($proRec['project_reviews']) == 2)
              					  <div class="stars-main-container">
                          @if(isset($title))
                              <div class="title-stars-block">{{$title or "N/A"}} : </div>
                  					  <div class="rating-list list-rating-block">
                  						  <span class="stars">{{$review['rating']}}</span>
                  					  </div>
              					  @endif
              					  </div>
                        @elseif($review['from_user_id'] == $user->id)
                          <div class="stars-main-container">
                          @if(isset($title))
                              <div class="title-stars-block">{{$title or "N/A"}} : </div>
                              <div class="rating-list list-rating-block">
                                <span class="stars">{{$review['rating']}}</span>
                              </div>
                          @endif
                          </div>
                        @endif

                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                     @endforeach
                  </div>
               @else
                  <div class="more-project-dec">
                     {{-- Client Review : 
                     No Review Yet 
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                  
                     Expert Review :
                     No Review Yet --}}
                     {{ trans('new_translations.rating')}} : {{ trans('new_translations.no_rating_yet')}}
                  </div>
               @endif
            </div>

              <div class="category-new">
                <img src="{{url('/public')}}/front/images/tag-2.png" alt="" /> {{$proRec['category_details']['category_title'] or 'NA'}}
                &nbsp;&nbsp;
                <img src="{{url('/public')}}/front/images/tag-2.png" alt="" /> {{isset($proRec['sub_category_details']['subcategory_title'])?$proRec['sub_category_details']['subcategory_title']:'NA'}}
              </div>
              <div class="clearfix"></div>
                
                <!--<div class="col-sm-12 col-md-1 col-lg-1 skills-de-new"><span class="colrs">{{trans('client/projects/completed_projects.text_skills')}}:</span></div>-->
                
                @if(isset($proRec['project_skills'])  && count($proRec['project_skills']) > 0)
                  <div class="skils-project">
                       <img src="{{url('/public')}}/front/images/tag.png" alt="" />
                     <ul>
                        @foreach($proRec['project_skills'] as $key=>$skills) 
                        @if(isset($skills['skill_data']['skill_name']) &&  $skills['skill_data']['skill_name'] != "")
                        <li >{{ str_limit($skills['skill_data']['skill_name'],25) }}</li>
                        @endif
                        @endforeach
                        
                     </ul>
                  </div>
               @endif    
             </div>
                     <div class="search-project-listing-right contest-right">
                            <div class="search-project-right-price">
                                 {{ isset($proRec['project_currency']) && $proRec['project_currency']!=''?$proRec['project_currency']:'$'}} {{isset($proRec['project_bid_info']['bid_cost'])?number_format($proRec['project_bid_info']['bid_cost']):'0'}}{{$text_hour}} 
                            </div>
          </div>
             <div class="invite-expert-btns">
                 <a class="black-border-btn" href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}">{{--  class="view_btn hidden-xs hidden-sm hidden-md"> --}} {{trans('client/projects/completed_projects.text_view')}} </a>
                  <a class="black-border-btn" href="{{ $module_url_path }}/milestones/{{ base64_encode($proRec['id'])}}"  >
                  {{-- <i class="fa fa-money" aria-hidden="true"></i> --}}{{trans('client/projects/completed_projects.text_milestones')}} 
                  {{-- {{trans('client/projects/completed_projects.text_view')}} --}} 
                  </a>
             </div>
                
              
           <div class="clearfix"></div>
         </div>
        
         @endforeach
         @else
            <div class="search-grey-bx">
                <div class="no-record" >
                   {{trans('expert/projects/awarded_projects.text_sorry_no_record_found')}}
                </div>
            </div>
         @endif
    
      <!-- Paination Links -->
      @include('front.common.pagination_view', ['paginator' => $arr_completed_projects])
      <!-- Paination Links -->
   

</div>
<script type="text/javascript">
  function sortByHandledBy(ref){  
      var sort_by = $(ref).val();
      if(sort_by != ""){
        window.location.href="{{ url('client/projects/completed') }}"+"?sort_by="+sort_by;
      }
      else{
        window.location.href="{{ url('client/projects/completed') }}";
      }
      return true;
    }
</script>
@stop