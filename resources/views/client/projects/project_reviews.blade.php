@extends('client.layout.master')                
@section('main_content')
<link rel="stylesheet" href="{{url('/public')}}/assets/rating-master/fontawesome-stars.css">
<script type="text/javascript" src="{{url('/public')}}/assets/rating-master/jquery.barrating.min.js"></script>
<!-- <div class="middle-container"> -->
<div class="container">
   <!-- <br/> -->
   <div class="row">
      <div class="col-sm-7 col-md-8 col-lg-9">
         <!-- Including top view of project details - Just pass project information array to the view -->
         @include('front.common._top_project_details',['projectInfo'=>$projectInfo])
         <!-- Ends -->  
         <?php $user = Sentinel::check();?>
         {{-- Review Display--}}
         @if(isset($arr_review_details) && sizeof($arr_review_details)>0 && $projectInfo['project_status'] == '3' ) 
         @foreach($arr_review_details as $review)
         <?php
            $user = Sentinel::check();
            
            if($review['from_user_id'] == $user->id)
            {
              
              $profile_image = isset($review['from_user_info']['profile_image']) ?$review['from_user_info']['profile_image']:"default_profile_image.png";
              $first_name  = isset($review['from_user_info']['first_name']) ?$review['from_user_info']['first_name']:"";
              $last_name  = isset($review['from_user_info']['last_name']) ?$review['from_user_info']['last_name']:"";
              $name = $first_name.' '.$last_name;
            
              $title = "Your Review";
            }
            else
            {
              if($user->inRole('client'))
              {
                $title = "Expert's Review";
              }
              else if($user->inRole('expert'))
              {
                $title = "Client's Review";
              }
            
              $profile_image = isset($review['from_user_info']['profile_image']) ?$review['from_user_info']['profile_image']:"default_profile_image.png";
              $first_name  = isset($review['from_user_info']['first_name']) ?$review['from_user_info']['first_name']:"";
              $last_name  = isset($review['from_user_info']['last_name']) ?$review['from_user_info']['last_name']:"";
              $name = $first_name.' '.$last_name;
            }
            
            ?>
         <div class="search-grey-bx">
            <div class="row">
               <div class="col-sm-12 col-md-8 col-lg-9">
                  <div class="going-profile-detail">
                     <div class="going-pro" style="display:inline-block;">
                        <img src="{{url('/public')}}/uploads/front/profile/{{$profile_image or ''}}" alt="pic"/>
                     </div>
                     <div class="going-pro-content">
                        <div class="profile-name" style="margin-bottom: 10px; display:inline-block;color:#c42881;">{{ $title }}</div>
                        <div style="color:#494949;" class="sub-project-dec">{{ $name or 'Unknown user'}}</div>
                        <div class="more-project-dec" style="height:auto;">
                           {{ $review['review'] }}
                           <div class="clr"></div>
                           <br/>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-sm-12 col-md-4 col-lg-3  ">
                  <div class="rating-profile1 br-left" style="display: inherit; margin-top: 18px;">
                     <div class="rating_profile">
                        <div class="rating-title1">{{trans('client/projects/project_reviews.text_review_rate')}} <span>:</span> </div>
                        <div class="rate-t">
                           <div class="rating-list">
                              @for($i=0;$i<$review['rating'];$i++)
                              <img src="{{url('/public')}}/front/images/star1.png" alt="" />
                              @endfor
                           </div>
                           ( {{ $review['rating'] or '0' }} )
                        </div>
                     </div>
                     <div class="rating_profile">
                        <div class="rating-title1">{{trans('client/projects/project_reviews.text_date')}} <span>:</span> </div>
                        <div class="rating-list"> {{ date('d M Y' ,strtotime($review['created_at']))}}</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         @endforeach                       
         @endif
         {{--  Review Display Ends --}}
         @if(isset($is_review_exists) && $is_review_exists==0 && isset($projectInfo['project_status']) && $projectInfo['project_status']==3 )
         <div class="search-grey-bx">
            <form id="frm_add_review" method="post"  action="{{url('/') }}/client/review/add">
               {{ csrf_field() }}
               <div class="subm-text">{{trans('client/projects/project_reviews.text_title')}}</div>
               <div class="row">
                  <input type="hidden" readonly="" name="project_id" value="{{isset($projectInfo['id'])?base64_encode($projectInfo['id']):''}}" ></input> 
                  <input type="hidden" readonly="" name="expert_user_id" value="{{isset($projectInfo['expert_user_id'])?base64_encode($projectInfo['expert_user_id']):''}}" ></input>   
                  <div class="col-sm-12 col-md-12 col-lg-12">
                     <div class="user-bx">
                       <div class="frm-nm">{{trans('client/projects/project_reviews.text_rating_type_one')}}<i style="color: red;">*</i></div>
                        <select  class="review-rating-starts" name="rating_type_one">
                           <option value="1">1</option>
                           <option value="2">2</option>
                           <option value="3">3</option>
                           <option value="4">4</option>
                           <option value="5">5</option>
                        </select>
                        <span class="error">{{ $errors->first('rating_type_one') }}</span>
                         <div class="frm-nm">{{trans('client/projects/project_reviews.text_rating_type_two')}}<i style="color: red;">*</i></div>
                        <select  class="review-rating-starts" name="rating_type_two">
                           <option value="1">1</option>
                           <option value="2">2</option>
                           <option value="3">3</option>
                           <option value="4">4</option>
                           <option value="5">5</option>
                        </select>
                        <span class="error">{{ $errors->first('rating_type_two') }}</span>

                         <div class="frm-nm">{{trans('client/projects/project_reviews.text_rating_type_three')}}<i style="color: red;">*</i></div>
                        <select  class="review-rating-starts" name="rating_type_three">
                           <option value="1">1</option>
                           <option value="2">2</option>
                           <option value="3">3</option>
                           <option value="4">4</option>
                           <option value="5">5</option>
                        </select>
                        <span class="error">{{ $errors->first('rating_type_three') }}</span>

                          <div class="frm-nm">{{trans('client/projects/project_reviews.text_rating_type_four')}}<i style="color: red;">*</i></div>
                        <select  class="review-rating-starts" name="rating_type_four">
                           <option value="1">1</option>
                           <option value="2">2</option>
                           <option value="3">3</option>
                           <option value="4">4</option>
                           <option value="5">5</option>
                        </select>
                        <span class="error">{{ $errors->first('rating_type_four') }}</span>

                        <div class="frm-nm">{{trans('client/projects/project_reviews.text_rating_type_five')}}<i style="color: red;">*</i></div>
                        <select  class="review-rating-starts" name="rating_type_five">
                           <option value="1">1</option>
                           <option value="2">2</option>
                           <option value="3">3</option>
                           <option value="4">4</option>
                           <option value="5">5</option>
                        </select>
                        <span class="error">{{ $errors->first('rating_type_five') }}</span>
                     </div>
                  </div>
                  <div class="col-sm-12 col-md-12 col-lg-12">
                     <div class="user-bx">
                        <div class="frm-nm">{{trans('client/projects/project_reviews.text_comment')}}<i style="color: red;">*</i></div>
                        <textarea style="max-width:100%;height:125px"  name="review" data-rule-required="true" class="det-in" placeholder="{{trans('client/projects/project_reviews.placeholder_comment')}}"></textarea>
                        <span class="error">{{ $errors->first('review') }}</span>
                     </div>
                  </div>
                  <div class="clr"></div>
                  <br/>
                  <div class="col-sm-2 col-md-2 col-lg-3">
                     <button type="submit" class="det-sub-btn" style="float:none;">{{trans('client/projects/project_reviews.text_add')}}</button>
                  </div>
               </div>
            </form>
         </div>
         @endif
      </div>
   </div>
</div>
<script type="text/javascript">
   $('#frm_add_review').validate();
   
   
    $(function() {
          $('.review-rating-starts').barrating({
          theme: 'fontawesome-stars'
           });
        }); 
   
</script>
@stop