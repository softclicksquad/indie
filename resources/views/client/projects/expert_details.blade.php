@extends('client.layout.master')                
@section('main_content')
<div class="col-sm-7 col-md-8 col-lg-9">  

    {{-- Includeing expert Details view --}}
      @include('front.common._expert_details',['arr_expert_details'=> isset($arr_project_bid_info['role_info'])? $arr_project_bid_info['role_info']:[],'user_details'=> isset($arr_project_bid_info['expert_details']) ? $arr_project_bid_info['expert_details'] : [] ]) 
    {{-- Ends --}}
    <br/>
       {{-- Bid Information --}}
       @if(isset($arr_project_bid_info) && count($arr_project_bid_info) > 0 )
            <div class="search-grey-bx projt-detls">
            <div class="row">
               <div class="col-sm-12 col-md-8 col-lg-8">
                  <div class="going-profile-detail">
                     <div class="going-pro-content" style="margin-left: 0px;">
                        <div class="profile-name" style="display:inline-block;color:#2d2d2d;">{{ trans('new_translations.bid_proposal')}}</div>
                        <div style="margin-bottom: 12px;"></div>
                        <div class="more-project-dec" style="height:auto;">
                          {{isset($arr_project_bid_info['bid_description'])?$arr_project_bid_info['bid_description']:''}}
                        <div class="clr"></div>
                        <br/>
                        </div>
                     </div>
                  </div>
                  @if(isset($arr_project_bid_info['bid_attachment']) && trim($arr_project_bid_info['bid_attachment']) != "")
                 <div class="user-box" style="max-width: 300px; width: 100%;">
                 <div class="p-control-label"><i class="fa fa-paperclip"></i> {{ trans('expert/projects/add_project_bid.text_attachment') }}</div>
                   <div class="input-name">
                      <div class="upload-block">
                        <div class="input-group ">
                            <input type="text" class="form-control file-caption  kv-fileinput-caption" id="profile_image_name" disabled="disabled" value="{{$arr_project_bid_info['bid_attachment'] or ''}}" >
                          
                            <div class="btn btn-primary btn-file btn-gry">
                              <a title="Download Previous Proposal" download href="{{$bid_attachment_public_path}}{{$arr_project_bid_info['bid_attachment']}}" >
                                <span style="color: #0E81C2;"><i class="fa fa-download"></i></span>
                             </a>    
                            </div>
                         </div>
                      </div>
                      <div id="msg_bid_attach" style="color: red;font-size: 12px;font-weight:600;display: none;"></div>
                      <div class="user-box hidden-lg">&nbsp;</div>
                      <div class="note_browse"></div>
                   </div>
                </div> 
             @endif  
            </div>
               <div class="col-sm-12 col-md-4 col-lg-4">
                  <div class="rating-profile1 br-left" style="margin-top: 0px; padding-left: 10px;">
                     <div title="Bid Cost" class="projrct-prce1" >
                     @if(isset($arr_project_bid_info['project_details']['project_currency']) && $arr_project_bid_info['project_details']['project_currency']=="$")
                     <span>
                     <img src="{{url('/public')}}/front/images/doller-img.png" alt="img"/> 
                     </span>{{$arr_project_bid_info['project_details']['project_currency']}}
                     {{isset($arr_project_bid_info['bid_cost'])?$arr_project_bid_info['bid_cost']:0}}
                     @else
                     <span>
                     <img src="{{url('/public')}}/front/images/doller-img-new-1.png" alt="img"/> 
                     </span>{{$arr_project_bid_info['project_details']['project_currency']}}
                     {{isset($arr_project_bid_info['bid_cost'])?$arr_project_bid_info['bid_cost']:0}}
                     @endif
                     </div>
                     <div title="Project Duration" class="projrct-prce1"><span><img src="{{url('/public')}}/front/images/bid-time.png" alt="img"/> </span> {{isset($arr_project_bid_info['bid_estimated_duration'])?$arr_project_bid_info['bid_estimated_duration']:0}}&nbsp;{{ trans('new_translations.days')}}
                     </div>
                  </div>
               </div>
            </div>
         </div>
         {{-- Show rejection reason --}}
         @if(isset($arr_project_bid_info['rejection_reason']) && $arr_project_bid_info['rejection_reason'] != "")
          <div class="search-grey-bx">
            <div class="row">
               <div class="col-sm-12 col-md-8 col-lg-12">
                  <div class="going-profile-detail">
                     <div class="going-pro-content">
                        <div class="profile-name" style="display:inline-block;color:#c42881;">{{ trans('new_translations.project_rejection_reason')}}</div>
                        <div style="margin-bottom: 12px;"></div>
                        <div class="more-project-dec" style="height:auto;">
                          {{$arr_project_bid_info['rejection_reason'] or ''}}
                        <div class="clr"></div>
                        <br/>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
          </div>
         @endif
         {{-- ends --}}
      @endif
    {{-- Bid Information Ends --}}
</div>
@stop