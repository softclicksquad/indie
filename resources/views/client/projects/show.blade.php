@extends('client.layout.master')                
@section('main_content')
<div class="middle-container">
   <div class="container">
      <br/>
      <div class="row">
         <div class="col-sm-7 col-md-8 col-lg-9">
            <div class="search-grey-bx">
               <div class="ongonig-project-section">
                  <div class="dispute-head">{{$projectInfo['project_name']}} {{-- <a href="#"><img src="{{url('/public')}}/front/images/mange.png" alt=""/> <span class="hidden-xs hidden-sm hidden-md">Manage</span></a> --}}</div>
                  <div class="row">
                     <div class="col-sm-7 col-md-7 col-lg-7">
                        <div class="pro-title"> <span style="width:165px;">{{trans('client/projects/project_reviews.text_company_name')}}</span>{{$projectInfo['client_info']['company_name']}}</div>
                        <div class="pro-title"><span style="width:165px;">{{trans('client/projects/project_reviews.text_email_id')}}</span>{{$projectInfo['client_details']['email']}}</div>
                     </div>
                     <div class="col-sm-5 col-md-5 col-lg-5">
                        <div class="pro-title"><span style="width:165px;">{{trans('client/projects/project_reviews.text_category')}} </span>{{$projectInfo['category_details']['category_title']}} </div>
                        {{--  
                        <div class="pro-title"><span style="width:165px;">Sub Category: </span>Architects</div>
                        --}}
                     </div>
                  </div>
                  <div class="det-divider"></div>
                  <div class="project-title">
                     <div class="pro-title">&nbsp;</div>
                     <div class="more-project-dec" style="height:auto;">{{$projectInfo['project_description']}}
                     </div>
                  </div>
                  <div class="clr"></div>
                  <br/>
                  <div class="project-list mile-list">
                     <ul>
                        <li><span class="hidden-xs"><img src="{{url('/public')}}/front/images/bid-time.png" alt="time icon"/> </span>{{$projectInfo['project_expected_duration']}}</li>
                        <li><span class="hidden-xs"><img src="{{url('/public')}}/front/images/calander.png" alt="time icon"/>&nbsp{{date('d M Y',strtotime($projectInfo['project_start_date']))}} </span></li>
                        <li style="background:none;"><span class="projrct-prce"><img class="hidden-xs hidden-sm" src="{{url('/public')}}/front/images/payment.png" alt="time icon"/>  ${{$projectInfo['project_cost']}}</span></li>
                     </ul>
                  </div>
                  <div class="clr"></div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@stop