@extends('client.layout.master')                
@section('main_content')
<div class="col-sm-7 col-md-8 col-lg-9">
   <div class="right_side_section payment-section">
      <div class="head_grn">{{isset($page_title)?$page_title:''}}</div>
      @include('front.layout._operation_status')
      <div class="ongonig-project-section">
         <div class="dispute-head"  style="padding-top: 0px;">
            {{isset($arr_project['project_name'])?$arr_project['project_name']:''}}
         </div>
         <span><i class="fa fa-calendar" aria-hidden="true"></i></span> {{isset($arr_project['created_at'])?date('d M Y',strtotime($arr_project['created_at'])):''}}
         <div class="det-divider"></div>
         <div class="project-title">
         </div>
         <div class="clr"></div>
             @php $total_cost = 0; @endphp
             @if(isset($commission_cost))
               @php $total_cost += $commission_cost; @endphp
               <div class="project-list pro-list-ul">
                  <ul>
                     <li style="background:none;">
                          <span class="projrct-prce"> {{ isset($project_currency_code)?$project_currency_code:'' }}
                            {{isset($commission_cost)?number_format($commission_cost,2):'0'}} <span style="color:#737373;">(<i>{{trans('milestones/payment_methods.text_project_manager_payment_note')}}</i>)</span>
                          </span>
                     </li>
                  </ul>
               </div>
               <div class="clr"></div>
               <br/>
             @endif

             <div class="project-list pro-list-ul">
                <ul>
                   <li style="background:none;">
                        <span class="projrct-prce"> <b>Total :</b> {{ isset($project_currency_code)?$project_currency_code:'' }}
                          {{isset($total_cost)?number_format($total_cost,2):'0'}}
                        </span>
                   </li>
                </ul>
             </div>
            <div class="clr"></div>
            <br/>
            <form id="validate-form-payment" name="validate-form-payment" method="POST" action="{{url('/payment/project_manager_paynow')}}">
                {{ csrf_field() }}
                <input type="hidden" name="payment_obj_id" id="payment_obj_id" value=" {{isset($arr_project['id'])?$arr_project['id']:'0'}}">
                <input type="hidden" name="transaction_type" id="transaction_type" value="5">
                <input type="hidden" name="project_services_cost" id="project_services_cost" value="{{isset($total_cost)?number_format($total_cost,2):'0'}}">
                <!-- Payment Methods --> 
                  <div class="paypa_radio">
                         @php 
                          // $wallet_amount= isset($arr_data_bal['balance'][0])?$arr_data_bal['balance'][0]:0;
                          // $wallet_balance = isset($arr_data_bal['balance'][0])?$arr_data_bal['balance'][0]:0;
                          // $wallet_currency = isset($arr_data_bal['currency'][0])?$arr_data_bal['currency'][0]:0;
                         @endphp

                         <div class="radio_area regi" onclick="javascript: return setOption('wallet');">
                            <input type="radio" name="payment_method" id="radio_wallet" class="css-checkbox" checked="" value="3">
                            <label for="radio_wallet" class="css-label radGroup1">
                              <img style="width: 292px;height: 57px;margin-top:-20px;" src="{{url('/public')}}/front/images/archexpertdefault/arc-hexpert-wallet-logo.png" alt="wallet payment method"/> 
                              <i> ( {{trans('common/wallet/text.text_money_balance')}} : {{ isset($wallet_amount)? $wallet_amount:'0'}} {{ isset($project_currency_code)?$project_currency_code:'' }} ) </i>
                              <a href=""><i class="fa fa-refresh" aria-hidden="true"></i></a>
                            </label>
                         </div> 
                         <div class="clearfix"><hr style="width: 100%;margin-left: 2px;"></div>
                     <!-- End Wallet -->
                     <span class='error' id="payment_method_err">{{$errors->first('payment_method')}}</span>
                  </div>
                <!-- Payment Methods -->
                <br/>
                <div class="clr"></div>
                
                <div class="pay-amt-details-main">
                    @if( (isset($wallet_amount) && isset($total_payment_amount)) && ($total_payment_amount > $wallet_amount))
                      <div class="pay-amt-details">
                          <div class="total-amt-txt-section">
                              Total Prize: 
                          </div>
                          <div class="total-amt-value-section">
                              {{ isset($project_currency_code)?$project_currency_code:'' }} {{ isset($total_payment_amount) ? number_format($total_payment_amount,2) : '0.0' }}
                          </div>
                      </div>
                      <div class="pay-amt-details">
                          <div class="total-amt-txt-section">
                              Wallet Balance: 
                          </div>
                          <div class="total-amt-value-section">
                              {{ isset($project_currency_code)?$project_currency_code:'' }}  {{ isset($wallet_amount) ? number_format($wallet_amount,2) : '0.0' }}
                          </div>
                      </div>
                      <div class="pay-amt-details net-total-section">
                          <div class="total-amt-txt-section">
                              Sub Total: 
                          </div>
                          <div class="total-amt-value-section">
                              {{ isset($project_currency_code)?$project_currency_code:'' }} {{ isset($sub_total_payment_amount) ? number_format($sub_total_payment_amount,2) : '0.0' }}
                          </div>
                      </div>
                      <div class="pay-amt-details">
                          <div class="total-amt-txt-section">
                              Service Charges: 
                          </div>
                          <div class="total-amt-value-section">
                              {{ isset($project_currency_code)?$project_currency_code:'' }} {{ isset($service_charge) ? number_format($service_charge,2) : '0.0' }}
                          </div>
                      </div>

                      <div class="pay-amt-details net-total-section">
                          <div class="total-amt-txt-section">
                              Total: 
                          </div>
                          <div class="total-amt-value-section">
                              {{ isset($project_currency_code)?$project_currency_code:'' }} {{ isset($total_pay_amount) ? number_format($total_pay_amount,2) : '0.0' }} 
                          </div>
                      </div>
                    @else
                      <div class="pay-amt-details">
                        <div class="total-amt-txt-section">
                            Total: 
                        </div>
                        <div class="total-amt-value-section">
                            {{ isset($project_currency_code)?$project_currency_code:'' }} {{ isset($total_pay_amount) ? number_format($total_pay_amount,2) : '0.0' }}
                        </div>
                      </div>
                    @endif
                </div>

                <br/>
                <div class="clr"></div>
                <div class="right-side">
                   <button class="normal-btn pull-right" value="submit" id="btn-form-payment-submit">{{trans('milestones/payment_methods.text_processed')}}</button>
                </div>
            </form>
         <div class="clr"></div>
      </div>
   </div>
</div>
<script type="text/javascript" src="{{url('/public')}}/assets/payment/jquery.payment.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript">
   
   $('#btn-form-payment-submit').click(function (){
      return true;
   });

</script>
<script type="text/javascript">
  (function (global, $) {
    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";
        global.setTimeout(function () 
        {
            global.location.href += "!";
        }, 50);
    };
    global.onhashchange = function () {
        if (global.location.hash != _hash) {
            global.location.hash = _hash;
        }
    };
    global.onload = function () 
    {
        noBackPlease();
        // disables backspace on page except on input fields and textarea..
        $(document.body).keydown(function (e) {
            var elm = e.target.nodeName.toLowerCase();
            if (e.which == 8 && (elm !== 'input' && elm  !== 'textarea')) 
            {
                e.preventDefault();
            }
            // stopping event bubbling up the DOM tree..
            e.stopPropagation();
        });
    };
  })(window, jQuery || window.jQuery);
</script>
@stop