@extends('client.layout.master')             
@section('main_content')
<div class="col-sm-7 col-md-8 col-lg-9">
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12">
      <div class="right_side_section proect-listing dispute-projects">
         @include('front.layout._operation_status')
         <div class="head_grn">{{trans('client/projects/dispute.text_projects')}}</div>

         @if(isset($arr_dispute_projects['data']) && sizeof($arr_dispute_projects['data'])>0)
         @foreach($arr_dispute_projects['data'] as $proRec)
         <div class="white-block-bg-no-padding">
        <div class="white-block-bg big-space hover">
             <div class="search-project-listing-left">
                 <div class="project-title">
                   {{-- <a href="{{url('/public')}}/client/projects/details/{{ base64_encode($proRec['id'])}}"> --}}
                    <h3>{{$proRec['project_name']}}</h3></a>
                   {{--    --}}
                   <div class="sub-project-dec"><p><i class="fa fa-calendar-o"></i>
 {{trans('client/projects/dispute.text_project_duration')}}:&nbsp;{{$proRec['project_expected_duration']}}</p></div>
                   <div class="more-project-dec">{{str_limit($proRec['project_description'],350)}}</div>
                </div>
                <!--<div class="col-sm-12 col-md-1 col-lg-1 skills-de-new">
                    <span class="colrs">{{trans('client/projects/ongoing_projects.text_skills')}}:</span>
                </div>-->
               <div class="skill-de-content">
                  <div class="skils-project">
                     <ul>
                        @if(isset($proRec['project_skills'])  && count($proRec['project_skills']) > 0)
                        @foreach($proRec['project_skills'] as $key=>$skills) 
                        @if(isset($skills['skill_data']['skill_name']) &&  $skills['skill_data']['skill_name'] != "")
                        <li style="font-size:12px; " >{{ str_limit($skills['skill_data']['skill_name'],18) }}</li>
                        @endif
                        @endforeach
                        @endif    
                     </ul>
                  </div>
               </div>
                {{-- Block For go to Dispute --}}
                  <?php 
                     $user = Sentinel::check();
                  ?>
                   @if($proRec['project_status'] == "4" && isset($user))    
                      @if($user->inRole('client'))

                        @if(isset($proRec['dispute']) && count($proRec['dispute']) > 0)
                          <?php  $btn_name =/* 'View ' ;*/(trans('client/projects/dispute.btn_view'));?> 
                        @else
                          <?php  $btn_name = /*'Add Dispute';*/(trans('client/projects/dispute.btn_dispute')); ?> 
                        @endif    

                       <a href="{{url('/')}}/client/dispute/{{base64_encode($proRec['id'])}}"  class="black-btn" >{{$btn_name or ''}}</a>

                      @endif
                   @endif

                {{-- Dispute Ends --}}
             </div>
           
            <div class="clearfix"></div>
         </div>
        </div>
        
         @endforeach
         @else
         <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;">
            <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
               <div class="search-content-block">
                  <div class="search-head" style="color:rgba(45, 45, 45, 0.8);">
                     {{trans('client/projects/dispute.text_no_record_found')}}
                  </div>
               </div>
            </td>
         </tr>
         @endif
      </div>
      <!-- Paination Links -->
      @include('front.common.pagination_view', ['paginator' => $arr_pagination])
      <!-- Paination Links -->
   </div>
		</div>
</div>
@stop