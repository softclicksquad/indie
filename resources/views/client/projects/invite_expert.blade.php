<!-- Modal -->
@php 
/*$arr_experts = get_experts();*/
@endphp 

<style type="text/css">
  
  .rating-box{
    background-color: rgb(211, 84, 0);   
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: .25em; 
  }

</style>

 <div class="modal fade invite-member-modal" id="invite_member" role="dialog">
    <div class="modal-dialog modal-lg">
       <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
       <div class="modal-content">
          <div class="modal-body">            
             <h2>{{trans('client/projects/invite_experts.text_invite_expert')}}</h2>
             <div class="invite-member-section">
                <div class="invite-form" style="padding: 0px 31px 0px;">
                  <div class="user-box">
                    <div class="txt-edit"> {{trans('client/projects/invite_experts.text_experts_showed_in_a_select_box_based_on_selected_project_br_category_skills')}} </div>
                  </div>
                </div>
                <div class="member-strip"></div>
             </div>
             <form action="{{url('/')}}/client/projects/invite_experts" method="post" id="invite-member-form">
               {{csrf_field()}}
               <input type="hidden" name="project-id" id="project-id" value="">
               <div class="invite-form">
                    <div class="user-box">
                       <div class="input-name member-search-wrapper">
                          <div class="droup-select code-select">
                              <select class="selectpicker experts" data-live-search="true" name="experts" id="experts" data-rule-required="true" data-show-subtext="true">
                                <option value="">--{{trans('client/projects/invite_experts.text_select')}}--</option>
                                 
                              </select>
                              <span class="error err" id="err_experts"></span>
                          </div>
                          <!-- <span class="member-search"><img src="{{url('/public/front/images/search-black.png')}}" class="img-responsive"/></span> -->
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="input-name">
                          <input type="hidden" class="clint-input" readonly style="cursor:no-drop;" data-rule-required="true" name="expert-email" id="expert-email" placeholder="{{trans('client/projects/invite_experts.text_email_address')}}">
                       </div>
                    </div>
                    <div class="user-box">
                       <div class="txt-edit"> {{trans('client/projects/post.text_project_subdescription')}}</div>
                       <span class="txt-edit" id="project_description_msg">
                        {{trans('client/projects/post.text_project_limit_description')}}
                       </span>
                       <div class="input-name">
                          <textarea value="hello, you have been invited to bid on project, please check details" class="client-taxtarea" rows="7" data-rule-required="true" data-rule-maxlength="1000" data-txt_gramm_id="21e619fb-536d-f8c4-4166-76a46ac5edce" onkeyup="javascript: return textCounter(this,1000);" id="invite-message" name="invite-message" placeholder="{{trans('client/projects/invite_experts.text_add_a_message')}} ">hello, you have been invited to bid on project, please check details</textarea>
                          <span class="error err" id="err_invite_message"></span>
                       </div>
                    </div>
                    <button type="submit" id="invite-expert" class="black-btn">{{trans('client/projects/invite_experts.text_invite')}}</button>
               </div>
             <form>
          </div>
       </div>
    </div>
 </div>


<script type="text/javascript" src="{{url('/public')}}/front/js/bootstrap-select.min.js"></script>
<link href="{{url('/public')}}/front/css/bootstrap-select.min.css" type="text/css" rel="stylesheet" /> 
<!--model popup end here--> 
<script type="text/javascript">
   $(document).ready(function(){
      $("#invite-member-form").validate();

      $('#invite-expert').click(function(){
          var experts      = $('#experts').val();
          var message      = $('#invite-message').val();
          var flag         = 0;
          $('.err').html('');
          if(experts == ""){
            $('#err_experts').html("{{ trans('common/footer.text_this_field_is_required') }}");
            flag = 1;
          } 
          if(message == ""){
            $('#err_invite_message').html("{{ trans('common/footer.text_this_field_is_required') }}");
            flag = 1;
          } 
          if(flag == 1){
            return false;
          }
          else{
            return true;
          }
     });

     $('.invite-expert').click(function(){
        var module_url_path = '{{$module_url_path}}';
        $('.err').html('');
        var project_id = $(this).data('project-id');
        $('#project-id').val(project_id);
        var token = "<?php echo csrf_token(); ?>";
        $.ajax({
          type:"post",
          url:'{{ $module_url_path }}/get_experts',
          data:{project_id: project_id,_token:token},
          success:function(result){
            if(result != false){
                console.log(result);
                $('#experts').find('option').remove().end().append(result);
                $('.selectpicker').selectpicker('refresh'); 
            }
          }
        });
      });

      $('#experts').change(function(){
        var user_id    = $(this).val();
        var user_email = $('option:selected', this).attr('email');
        var user_src   = $('option:selected', this).attr('src');
        var name       = $('option:selected', this).attr('name');
        var html       = "";
        if(user_id != "")
        {
          html = '<div class="member-info">'+
                     '<div class="member-img"><img src="'+user_src+'" class="img-responsive"/></div>'+
                     '<div class="member-details">'+
                         '<b>'+name+'</b>'+
                         /*'<p>'+user_email+'</p>'+*/
                     '</div>'+
                 '</div>'+
                 '<div class="remove-btn-block">'+
                     /*'<button class="remove-btn">Remove</button>'+*/
                 '</div>';
          $('.member-strip').html(html);
          $('#expert-email').val(user_email);
          $('#invite-message').html('Hello '+name+', you have been invited to bid on a project. Please check details.');
        }
        else
        {
          $('#invite-message').html('');
          $('.member-strip').html('');
          $('#expert-email').val('');
        }
     });
     $('.close-invite-expert').click(function(){
          $('#invite-message').html('');
          $('.member-strip').html('');
          $('#expert-email').val('');
          $('#experts').val('').trigger('change');
          $('#invite-message').val('');
          $('#project-url').val('');
     });
   }); 
  function textCounter(field,maxlimit){
    var countfield = 0;
    if ( field.value.length > maxlimit ){
       field.value = field.value.substring( 0, maxlimit );
       return false;
    } else {
        countfield = maxlimit - field.value.length;
        var message = '{{trans('client/projects/invite_experts.text_you_have_left')}} <b>'+countfield+'</b> {{trans('client/projects/invite_experts.text_characters_for_description')}}';
        jQuery('#project_description_msg').html(message);
        return true;
    }
  }
</script>   