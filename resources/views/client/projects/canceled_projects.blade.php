@extends('client.layout.master')             
@section('main_content')
<div class="col-sm-7 col-md-8 col-lg-9">
      <div class="right_side_section proect-listing">
         @include('front.layout._operation_status')
         <div class="head_grn">{{trans('client/projects/canceled_projects.text_canceled_project_title')}}</div>
         @if(isset($arr_canceled_projects['data']) && sizeof($arr_canceled_projects['data'])>0)
         @foreach($arr_canceled_projects['data'] as $proRec)
         <div class="white-block-bg-no-padding">
         <div class="white-block-bg big-space hover">
             <div class="project-left-side">
                 <div class="search-freelancer-user-head">
                       <a href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}">
                        {{$proRec['project_name']}}
                       </a>
                   </div>
                     
                      <div class="search-freelancer-user-location">
                            <span class="gavel-icon">
                              <img src="{{url('/public')}}/front/images/calendar.png" alt="" /></span> 
                              <span class="dur-txt"> {{trans('client/projects/canceled_projects.text_est_time')}}: {{$proRec['project_expected_duration']}}</span> 
                            @if(isset($proRec['edited_at']) && $proRec['edited_at']!=NULL)
                              <span class="freelancer-user-line">|</span>
                            @endif
                        </div>
                      
                      @if(isset($proRec['edited_at']) && $proRec['edited_at']!=NULL)
                          @php $edited_time_ago = time_ago($proRec['edited_at']); @endphp
                          <div class="search-freelancer-user-location">
                              <span class="gavel-icon"><img src="{{url('/public')}}/front/images/clock.png" alt="" /></span>
                              <span class="dur-txt">Last edited : {{$edited_time_ago}}</span>
                          </div>
                      @endif
                     
                       <div class="more-project-dec">{{str_limit($proRec['project_description'],160)}}</div>
                       
                        <div class="category-new">
                          <img src="{{url('/public')}}/front/images/tag-2.png" alt="" /> {{$proRec['category_details']['category_title'] or 'NA'}}
                          
                          &nbsp;&nbsp;
                          <img src="{{url('/public')}}/front/images/tag-2.png" alt="" /> {{isset($proRec['sub_category_details']['subcategory_title'])?$proRec['sub_category_details']['subcategory_title']:'NA'}}
                        </div>
                        <div class="clearfix"></div>
                
                <!--<div class="col-sm-12 col-md-1 col-lg-1 skills-de-new"> <span class="colrs">{{trans('client/projects/completed_projects.text_skills')}}:</span></div>-->
                
                @if(isset($proRec['project_skills'])  && count($proRec['project_skills']) > 0)
                  <div class="skils-project">
                      <img src="{{url('/public')}}/front/images/tag.png" alt="" />
                     <ul>
                        @foreach($proRec['project_skills'] as $key=>$skills) 
                           @if(isset($skills['skill_data']['skill_name']) &&  $skills['skill_data']['skill_name'] != "")
                              <li>{{ str_limit($skills['skill_data']['skill_name'],25) }}</li>
                           @endif
                        @endforeach
                     </ul>
                  </div>
                @endif   
               
             </div>
             <?php
              $projects_cost = explode('-',$proRec['project_cost']);
              if( (isset($projects_cost[0]) && $projects_cost[0]!='') && (isset($projects_cost[1]) && $projects_cost[1]!=''))
              {
                  $projects_cost = number_format(floor($projects_cost[0])).'-'.number_format(floor($projects_cost[1]));
              }
              elseif($projects_cost[0]!='')
              {
                  $projects_cost = number_format(floor($projects_cost[0]));
              }
             ?>
             <div class="search-project-listing-right contest-right">
                            <div class="search-project-right-price">
                               {{ isset($proRec['project_currency']) && $proRec['project_currency']!=''?$proRec['project_currency']:'$'}}
                                {{isset($projects_cost)?$projects_cost:'0'}} 
                                  </div>
                              
                     </div>
             <div class="invite-expert-btns">
                  <a class="black-border-btn" href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}">               
                       {{trans('client/projects/canceled_projects.text_view')}} 
                  </a>
                  {{-- <a class="black-border-btn" href="{{ $module_url_path }}/milestones/{{ base64_encode($proRec['id'])}}"  >
                        {{trans('client/projects/canceled_projects.text_milestones')}} 
                  </a> --}}
                  <a class="black-border-btn" href="{{ $module_url_path }}/post/{{ base64_encode($proRec['id'])}}"  >
                       {{trans('client/projects/canceled_projects.text_post_again')}} 
                  </a>

                  <a onclick="confirmDelete(this)" data-delete-id="{{base64_encode($proRec['id'])}}">
                    <button class="black-border-btn">{{trans('client/projects/canceled_projects.text_delete')}} </button>
                  </a>

             </div>
            
            <div class="clearfix"></div>
         </div>
         </div>
         @endforeach
         @else
            <div class="search-grey-bx">
                <div class="no-record" >
                   {{trans('expert/projects/awarded_projects.text_sorry_no_record_found')}}
                </div>
            </div>
         @endif
   </div>
<!-- Paination Links -->
@include('front.common.pagination_view', ['paginator' => $arr_pagination])
<!-- Paination Links -->
</div>

<script type="text/javascript">
function confirmDelete(ref) {
  alertify.confirm("Are you sure? You want to delete project ?", function (e) {
      if (e) {
          var delete_id = $(ref).attr('data-delete-id');
          window.location.href="{{ $module_url_path }}/delete_project/"+delete_id;
          return true;
      } 
      else 
      {
          return false;
      }
  });
}
</script>
@stop