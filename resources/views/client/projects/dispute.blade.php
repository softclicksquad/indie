
@extends('client.layout.master')                
@section('main_content')
<link rel="stylesheet" href="{{url('/public')}}/assets/rating-master/fontawesome-stars.css">
<script type="text/javascript" src="{{url('/public')}}/assets/rating-master/jquery.barrating.min.js"></script>
<!-- <div class="middle-container"> -->
<div class="container">
   <!-- <br/> -->

   <div class="row">
      <div class="col-sm-7 col-md-8 col-lg-9">
         <!-- Including top view of project details - Just pass project information array to the view -->
         @include('front.common._top_project_details',['projectInfo'=>$projectInfo])
         <!-- Ends -->  
         @if(isset($arr_dispute_details) && sizeof($arr_dispute_details)>0 && $projectInfo['project_status'] == '4' ) 
         <?php
            $profile_image = isset($arr_dispute_details['user_who_added_dispute']['profile_image']) ?$arr_dispute_details['user_who_added_dispute']['profile_image']:"";
            $first_name    = isset($arr_dispute_details['user_who_added_dispute']['first_name']) ?$arr_dispute_details['user_who_added_dispute']['first_name']:"";
            $last_name     = isset($arr_dispute_details['user_who_added_dispute']['last_name']) ?substr($arr_dispute_details['user_who_added_dispute']['last_name'], 0,1).'.':"";
            $name          = $first_name.' '.$last_name;
           
            //$name = isset($arr_dispute_details['user_who_added_dispute']['user_details']['user_name'])?$arr_dispute_details['user_who_added_dispute']['user_details']['user_name']:'';
            $headline = trans('client/projects/dispute.text_dispute_details');
            ?>
         <div class="search-grey-bx">
            <div class="row">
               <div class="col-sm-12 col-md-8 col-lg-12">
                  <div class="going-profile-detail">
                     <div class="going-pro-content">
                        <div class="profile-name" style="margin-bottom: 10px; display:inline-block;color:#2d2d2d;">{{ $headline }} 
                           <span style="color:#494949;font-size: 14px;" class="sub-project-dec">
                            (&nbsp;{{ trans('client/projects/dispute.text_added_by') }}
                             &nbsp;{{ $name or 'Unknown user'}} )
                           </span>
                        </div>
                        <div style="color:#0E81C2;margin-bottom: 15px;margin-top: 5px;font-size: 14px;" class="project_name"><b>{{  $arr_dispute_details['title'] or ''}}</b></div>
                        <div class="more-project-dec" style="height:auto;">
                           {{ $arr_dispute_details['description']  or ''}}
                           <div class="clr"></div>
                           <br/>
                        </div>
                     </div>
                  </div>
               </div>

               

            @endif
        
            @if(isset($is_dispute_exists) && $is_dispute_exists == '0' && isset($projectInfo['project_status']) && $projectInfo['project_status'] == '4'  )
              <div class="search-grey-bx">
                 <form id="frm_add_dispute" method="POST" action="{{url('/') }}/client/add_dispute">
                    {{ csrf_field() }}
                      <div class="subm-text" style="margin-bottom: 15px; display:inline-block;color:#2d2d2d;">{{ trans('client/projects/dispute.text_dispute') }}</div>
                    <div class="row">
                       <input type="hidden" readonly="" name="project_id" value="{{isset($projectInfo['id'])?base64_encode($projectInfo['id']):''}}" ></input> 
                       <input type="hidden" readonly="" name="expert_user_id" value="{{isset($projectInfo['expert_user_id'])?base64_encode($projectInfo['expert_user_id']):''}}" ></input>   
                    
                        <div class="col-sm-12 col-md-12 col-lg-12">
                           <div class="user-bx">
                              
                                 <input type="text" style="box-shadow:none;" data-rule-required="true" id="title" name="title" value="{{ old('title') }}" class="clint-input error" placeholder="{{ trans('client/projects/dispute.text_headline') }}" onkeyup="return chk_validation(this);"/>
                                 <span class="error">{{ $errors->first('title') }}</span>
                              
                           </div>
                        </div>
                        <div class="clr"></div>
                        <br/>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                           <div class="user-bx">
                               <span class="txt-edit" id="project_description_msg">
                                  {{trans('client/projects/dispute.text_project_limit_description')}}
                              </span>
                              <textarea class="clint-input error" cols=""  id="description" data-rule-required="true" name="description" style="box-shadow:none;height:120px;" placeholder="{{ trans('client/projects/dispute.text_comment') }}" rows="3"  data-rule-maxlength="1000" data-gramm="" data-txt_gramm_id="21e619fb-536d-f8c4-4166-76a46ac5edce" onkeyup="javascript: return textCounter(this,1000);">{{ old('description') }}</textarea>
                              <span class="error">{{ $errors->first('description') }}</span>
                            
                           </div>
                           <br>
                           <div class="user-bx">
                            <div class="terms">
                              <div class="check-box">
                                  <p>
                                   <input id="filled-in-box" class="filled-in" type="checkbox" name="check_terms" data-rule-required="true">
                                   <label for="filled-in-box"></label>
                                  </p>

                              </div>
                              <div class="temsp-d">
                                <?php
                                 if(isset($arr_static_terms['page_slug']))
                                    $link_term = url('/info/'.$arr_static_terms['page_slug']);
                                 else
                                    $link_term = 'javascript: void(0)';
                                 if (isset($arr_static_privacy['page_slug'])) 
                                    $link_policy = url('/info/'.$arr_static_privacy['page_slug']);
                                 else
                                    $link_policy = 'javascript: void(0)';
                                 ?>
                                 {!! trans('client/projects/post.text_terms_final',array('link_term'=>$link_term,'link_policy'=>$link_policy)) !!}
                              </div>
                               
                           </div>
                           <label for="check_terms" class="error"></label>
                           </div>
                           <button  type="button"  class="black-btn pull-right" onclick="javascript:return confirm_dispute(this);">{{ trans('client/projects/dispute.text_dispute') }}</button>
                           <div class="clr"></div>
                        </div>

                    </div>
                 </form>
              </div>
           @endif

          </div>
        </div>
        {{--  ADmin Comments Block Starts --}}
        @if(isset($arr_dispute_details['admin_comments']) &&  $arr_dispute_details['admin_comments'] != "" )
         <div class="search-grey-bx">
            <div class="row">
               <div class="col-sm-12 col-md-8 col-lg-12">
                  <div class="going-profile-detail">
                     <div class="going-pro-content">
                        <div class="profile-name" style="margin-bottom: 10px; display:inline-block;color:#2d2d2d;">{{ trans('client/projects/dispute.text_added_by1') }}
                           <span style="color:#494949;font-size: 14px;" class="sub-project-dec">
                            (&nbsp;{{ trans('client/projects/dispute.text_added_by_admin') }} )
                           </span>
                        </div>
                        <div class="more-project-dec" style="height:auto;">
                           {{ $arr_dispute_details['admin_comments']  or ''}}
                           <div class="clr"></div>
                           <br/>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
          </div>
          @endif
          {{--  ADmin Comments Block Ends --}}

      </div>
   </div>
</div>
<script type="text/javascript">
$('#frm_add_dispute').validate();
// Function for special characters not accepts.
function chk_validation(ref){
   var yourInput = $(ref).val();
    re = /[`~!@#$%^&*()_|+\-=?;:".<>\{\}\[\]\\\/]/gi;
    var isSplChar = re.test(yourInput);
    if(isSplChar)
    {
      var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
      $(ref).val(no_spl_char);
    }
}
function confirm_dispute(){
  if($('#frm_add_dispute').valid() == true) {
     alertify.confirm("Are you sure? You really want to add dispute ?", function (e) {
          if (e) {
              $('#frm_add_dispute').submit();
              return true;
          } else {
              return false;
          }
      });
  }  
}

$("#frm_add_dispute").validate({
         errorElement: 'span',
         errorPlacement: function (error, element) 
         {
            if(element.attr("name") == 'check_terms')
            {  
               error.appendTo($("#_err_term"));  
            }
            else
            {
               error.insertAfter(element);
            }
         }
   });

function textCounter(field, maxlimit) {
        var countfield = 0;
        if (field.value.length > maxlimit) {
            field.value = field.value.substring(0, maxlimit);
            return false;
        } else {
            countfield = maxlimit - field.value.length;
            var message = '{{trans('client/projects/dispute.text_you_have_left')}} <b>' + countfield + '</b> {{trans('client/projects/dispute.text_characters_for_description')}}';
            jQuery('#project_description_msg').html(message);
            return true;
        }
    }
</script>
@stop