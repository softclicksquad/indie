@extends('client.layout.master')
@section('main_content')
<div class="col-sm-7 col-md-8 col-lg-9">
    <div class="row">  
        <div class="col-lg-8">
            @include('front.layout._operation_status')
            <div class="head_grn">{{trans('client/projects/awarded_projects.text_awarded_title')}}</div>
        </div>
        <div class="col-lg-4">
            <div class="droup-select">
                <select class="droup mrns tp-margn" onchange="sortByHandledBy(this)">
                    <option value="">-- Sort By --</option>
                    <option @if(isset($sort_by) && $sort_by=='asc' ) selected="" @endif value="asc">Ascending</option>
                    <option @if(isset($sort_by) && $sort_by=='desc' ) selected="" @endif value="desc">Descending</option>
                    <option @if(isset($sort_by) && $sort_by=='is_urgent' ) selected="" @endif value="is_urgent">Urgent</option>
                    <option @if(isset($sort_by) && $sort_by=='public_type' ) selected="" @endif value="public_type">Public</option>
                    <option @if(isset($sort_by) && $sort_by=='private_type' ) selected="" @endif value="private_type">Private</option>
                    <option @if(isset($sort_by) && $sort_by=='is_nda' ) selected="" @endif value="is_nda">NDA</option>
                </select>
            </div>
        </div>

        <div class="clearfix"></div>
        @if(isset($arr_awarded_projects['data']) && sizeof($arr_awarded_projects['data'])>0)
            @foreach($arr_awarded_projects['data'] as $awardedRec)
            <div class="col-lg-12">
                <div class="white-block-bg-no-padding">
                    <div class="white-block-bg big-space hover">
                        <div class="project-left-side">
                            <div class="search-freelancer-user-head">
                                <a href="{{ $module_url_path }}/details/{{ base64_encode($awardedRec['id'])}}">
                                    {{$awardedRec['project_name']}}
                                </a>
                            </div>
                            <div class="more-project-dec">{{str_limit($awardedRec['project_description'],340)}}</div>
                            <div class="category-new">
                                <img src="{{url('/public')}}/front/images/tag-2.png" alt="" /> {{$awardedRec['category_details']['category_title'] or 'NA'}}

                                &nbsp;&nbsp;
                                <img src="{{url('/public')}}/front/images/tag-2.png" alt="" /> {{isset($awardedRec['sub_category_details']['subcategory_title'])?$awardedRec['sub_category_details']['subcategory_title']:'NA'}}

                            </div>
                            <div class="clearfix"></div>

                            @if(isset($awardedRec['project_skills']) && count($awardedRec['project_skills']) > 0)
                                <div class="skils-project">
                                    <img src="{{url('/public')}}/front/images/tag.png" alt="" />
                                    <ul>
                                        @if(isset($awardedRec['project_skills']) && count($awardedRec['project_skills']) > 0)
                                            @foreach($awardedRec['project_skills'] as $key=>$skills)
                                                @if(isset($skills['skill_data']['skill_name']) && $skills['skill_data']['skill_name'] != "")
                                                <li>{{ str_limit($skills['skill_data']['skill_name'],25) }}</li>
                                                @endif
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            @endif
                        </div>

                        <div class="search-project-listing-right contest-right">
                            <div class="search-project-right-price">
                                <?php
                                $pricing_method = $text_hour = "";
                                if(isset($awardedRec['project_pricing_method']) && $awardedRec['project_pricing_method'] == '1')
                                {
                                    $pricing_method = trans('project_listing/listing.text_budget');
                                }
                                else if(isset($awardedRec['project_pricing_method']) && $awardedRec['project_pricing_method'] == '2')
                                {
                                    $pricing_method = trans('project_listing/listing.text_hourly_rate'); 
                                     $text_hour = trans('common/_top_project_details.text_hour');
                                }

                                $projects_cost = explode('-',$awardedRec['project_cost']);
                                if( (isset($projects_cost[0]) && $projects_cost[0]!='') && (isset($projects_cost[1]) && $projects_cost[1]!=''))
                                {
                                    $projects_cost = number_format(floor($projects_cost[0])).'-'.number_format(floor($projects_cost[1]));
                                }
                                elseif($projects_cost[0]!='')
                                {
                                    $projects_cost = number_format(floor($projects_cost[0]));
                                }
                                ?>
                                {{isset($awardedRec['project_currency'])?$awardedRec['project_currency']:'$'}} {{isset($projects_cost)?$projects_cost:'0'}}{{$text_hour}}
                                <span>{{$pricing_method or ''}}</span>
                            </div>
                        </div>
                        <div class="invite-expert-btns">
                            <a class="black-btn" href="{{ $module_url_path }}/details/{{ base64_encode($awardedRec['id'])}}">{{-- class="view_btn hidden-xs hidden-sm hidden-md"> --}}
                                {{trans('client/projects/awarded_projects.text_view')}}
                            </a>

                            <?php 
                            $current_time = date('Y-m-d H:i:s');
                            $award_request_date_time = isset($awardedRec['award_request_date_time'])?$awardedRec['award_request_date_time']:'';
                            $new_date_time = date('Y-m-d H:i:s',strtotime('+48 hour',strtotime($award_request_date_time)));
                            ?>
                            @if($current_time > $new_date_time)
                                <a onclick="confirmDelete(this)" style="cursor: pointer;" data-project-id="{{ isset($awardedRec['id'])?base64_encode($awardedRec['id']):'' }}" class="black-border-btn">CANCEL</a>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            @endforeach
        @else
            <div class="search-grey-bx">
                <div class="no-record">
                    {{trans('expert/projects/awarded_projects.text_sorry_no_record_found')}}
                </div>
            </div>
        @endif
    </div>

    <!-- Paination Links -->
    @include('front.common.pagination_view', ['paginator' => $arr_pagination])
    <!-- Paination Links -->
</div>
<script type="text/javascript">
    function sortByHandledBy(ref) {
        var sort_by = $(ref).val();
        if (sort_by != "") {
            window.location.href = "{{ url('client/projects/awarded') }}" + "?sort_by=" + sort_by;
        } else {
            window.location.href = "{{ url('client/projects/awarded') }}";
        }
        return true;
    }

    function confirmDelete(ref) {
        alertify.confirm("Are you sure? You want to cancel project ?", function(e) {
            var project_id = $(ref).attr('data-project-id');
            if (e) {
                window.location.href = "{{ $module_url_path }}" + "/cancel_project/" + project_id;
                return true;
            } else {
                return false;
            }
        });
    }
</script>
@stop