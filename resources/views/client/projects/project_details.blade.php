@extends('client.layout.master')                
@section('main_content')
<style type="text/css">
    table { border-collapse: collapse; }
    td, th { padding: 0.5rem; text-align: left; }
    td{border-top: 0px !important;}
    .min-block-time {
        display: block;
        text-align: center;
        vertical-align: middle;
    }
    .time-remaining-head {
        display: block;
        text-align: center;
        vertical-align: middle;
        font-size: 16px;
        color: #223750;
        font-family: 'ralewaysemibold';
        margin-bottom: 5px;
    }
    .count-min-block {
        font-size: 26px;
        color: #228B22;
        font-family: 'ralewaysemibold';
        line-height: 26px;
    }
    .min-label-block {
        font-size: 12px;
        color: #5f5f5f;
        text-transform: uppercase;
    }
    .not-approved-btn {
        background: #2d2d2d;
        border: 1px solid #2d2d2d;
        background: #2d2d2d none repeat scroll 0 0;
        border: 1px solid #2d2d2d;
        border-radius: 3px;
        color: #fff;
        height: 32px;
        max-width: 167px;
        width: 100%;
    }
</style>  
<link rel="stylesheet" href="{{url('/public')}}/assets/rating-master/fontawesome-stars.css">
<script type="text/javascript" src="{{url('/public')}}/assets/rating-master/jquery.barrating.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" integrity="sha256-Z8TW+REiUm9zSQMGZH4bfZi52VJgMqETCbPFlGRB1P8=" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.js" integrity="sha256-ZvMf9li0M5GGriGUEKn1g6lLwnj5u+ENqCbLM5ItjQ0=" crossorigin="anonymous"></script>
<?php
    $user     = Sentinel::check();
    
    if ($user->inRole('project_manager')){
        $folder = "project_manager";  
    }elseif ($user->inRole('client')){
        $folder = "client"; 
    }elseif ($user->inRole('expert')){
        $folder = "expert"; 
    }
?>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
            @php
            $diffInSeconds = '0';

            if(isset($request_time) && $request_time!=''){      
                $expired_time = strtotime(date('Y-m-d H:i:s',strtotime('+2day', strtotime($request_time))));
                $current_time = strtotime(date('Y-m-d H:i:s'));
                $diffInSeconds = $expired_time - $current_time;

                if($diffInSeconds <= 0){
                    $diffInSeconds = '0';
                }
            }
            @endphp

            <?php 
            if(isset($arr_bids_data['data'])){
                foreach($arr_bids_data['data'] as $bidkey => $bids_data){
                    if(isset($projectInfo['project_status']) && $projectInfo['project_status'] == '2' &&  isset($arr_bids_data['data'][$bidkey]['bid_status']) && $arr_bids_data['data'][$bidkey]['bid_status'] == '4'){
            ?>
                        <div class="alert alert-info alert-dismissible">
                            <img style="height:21px; margin-top:-4px;" src="{{url('/public/front/images/information-icon-th.png')}}"> 
                            {{trans('controller_translations.success_project_request_to_expert_sent_successfully')}}  {{trans('controller_translations.success_project_request_to_expert_sent_successfully_1')}} Remaining time: <span id="days_left_timer"></span>
                        </div>
            <?php
                        break;
                    }
                }
            }
            ?> 

            <script type="text/javascript">
                var time        = '<?php echo $diffInSeconds; ?>';
                if(time < 0) {  time = 0; }

                var c           = time;
                var t;
                var days    = parseInt( c / 86400 ) % 365;
                var hours   = parseInt( c / 3600 ) % 24;
                var minutes = parseInt( c / 60 ) % 60;
                var seconds = c % 60;

                var d = ""; 
                var h = ""; 
                var m = "";
                var s = ""; 

                if(days     !=  ""  &&  days     !="0"){  d  =  (days+'d')     +  " ";  }
                if(hours    !=  ""  &&  hours    !="0"){  h  =  (hours+'h')    +  " ";  }
                if(minutes  !=  ""  &&  minutes  !="0"){  m  =  (minutes+'m')  +  " ";  }
                if(seconds  !=  ""  &&  seconds  !="0"){  s  =  (seconds+'s')  +  " ";  }

                if(d >= '2d' && (h>'0h' || m>'0m' || s>'0s')){
                    var result = "EXPIRED"
                }
                else if(d != "" && h == ""){
                    var result    = d;
                }
                else if(d != "" && h !=""){
                    var result    = d + h;
                }
                else if(h != "" && m !=""){
                    var result    = h + m;
                }
                else if(m !=""){
                    var result    = m;
                }
                else if(s !=""){
                    var result    = s;
                }

                if(result == "NaN:NaN:NaN:NaN"){ 
                    $('#days_left_timer').html('EXPIRED');
                    $('#days_left_timer').css('color','red');
                }
                else if(result == 'EXPIRED'){
                    $('#days_left_timer').html(result);
                    $('#days_left_timer').css('color','red');
                }
                else{
                    $('#days_left_timer').html(result);
                    $('#days_left_timer').css('font-weight','bold');
                }

                if(c == 0){
                    $('#days_left_timer').html('EXPIRED');
                    $('#days_left_timer').css('color','red');
                }
                c = c - 1;
            </script>
            
            @include('front.common._top_project_details',['projectInfo'=>$projectInfo])
            <?php
            if(isset($projectInfo['project_requests'][0]['expert_user_id'])){  
                $expert_who_accepted_project = isset($projectInfo['project_requests'][0]['expert_user_id'])?$projectInfo['project_requests'][0]['expert_user_id']:'';
            }
            ?>

            @if(isset($arr_bids_data['data'])  && count($arr_bids_data['data'])>0)
            <?php
            $is_bid_awarded = 0;
            foreach ($arr_bids_data['data'] as $key => $bid_value){
                if(isset($bid_value['bid_status']) && $bid_value['bid_status'] == '4'){
                    $is_bid_awarded = 1;
                    break;   
                }             
            }
            ?>
                
                <div class="new-head-bid">Received Bids</div>
               <div class="sort-received-bids">
                <div class="droup-select">
                    <select class="droup mrns tp-margn" onchange="sortByHandledBy(this)">
                        <option value="">-- Sort By --</option>
                        <option @if(isset($sort_by) && $sort_by=='rating' ) selected="" @endif value="rating">Rating</option>
                        <option @if(isset($sort_by) && $sort_by=='price_ascending' ) selected="" @endif value="price_ascending">Price Ascending</option>
                        <option @if(isset($sort_by) && $sort_by=='price_descending' ) selected="" @endif value="price_descending">Price Descending</option>
                        
                        <option @if(isset($sort_by) && $sort_by=='date_ascending' ) selected="" @endif value="date_ascending">Date Ascending</option>
                        <option @if(isset($sort_by) && $sort_by=='date_descending' ) selected="" @endif value="date_descending">Date Descending</option>

                        <option @if(isset($sort_by) && $sort_by=='shortlist_ascending' ) selected="" @endif value="shortlist_ascending">Short List Ascending</option>
                        <option @if(isset($sort_by) && $sort_by=='shortlist_descending' ) selected="" @endif value="shortlist_descending">Short List Descending</option>

                        <option @if(isset($sort_by) && $sort_by=='reputation' ) selected="" @endif value="reputation">Reputation</option>
                    </select>
                </div>
            </div>  
            <div class="clearfix"></div>
            @foreach($arr_bids_data['data'] as $key => $project_bid)
            <?php
            $arr_profile_data = [];  
            if(isset($project_bid['expert_user_id']) && $project_bid['expert_user_id'] != ""){
                $arr_profile_data = sidebar_information($project_bid['expert_user_id']);

            }
            ?>  

            @if(isset($expert_who_accepted_project))
                @if( $expert_who_accepted_project == $project_bid['expert_user_id'] )
                    
                    <div class="search-grey-bx ongoing-div">                              
                                <div class="going-profile-detail">
                                    <div class="listin-pro-image">
                                        @if(isset($project_bid['user_details']['is_online']) && $project_bid['user_details']['is_online']!='' && $project_bid['user_details']['is_online']=='1')
                                        <div class="user-online-round"></div>
                                        @else
                                        <div class="user-offline-round"></div>
                                        @endif

                                        @if(isset($project_bid['role_info']['profile_image']) && $project_bid['role_info']['profile_image'] != "" && file_exists('public/uploads/front/profile/'.$project_bid['role_info']['profile_image']) ) 
                                        <div class="going-pro">
                                            <img src="{{ url('/public/uploads/front/profile/').'/'.$project_bid['role_info']['profile_image']}}" alt="pic"/>
                                        </div>
                                        @else
                                        <div class="going-pro">
                                            <img src="{{  url('/public/uploads/front/profile/default_profile_image.png')}}" alt="pic"/>
                                        </div>
                                        @endif
                                    </div>
                                    <div class="going-pro-content">
                                        <div class="profile-name">
                                            <a class="profile-name-section-block" href="{{url('/client/projects/expert_details/'.base64_encode($project_bid['project_id']).'/'.base64_encode($project_bid['expert_user_id']))}}" >
                                                <!-- {{$project_bid['user_details']['user_name'] or ''}} -->
                                                {{isset($project_bid['user_details']['role_info']['first_name'])?$project_bid['user_details']['role_info']['first_name']:'Unknown user'}} 
                                                @if(isset($project_bid['user_details']['role_info']['last_name']) && $project_bid['user_details']['role_info']['last_name'] !="") @php echo substr($project_bid['user_details']['role_info']['last_name'],0,1).'.'; @endphp @endif
                                                <!-- ({{$project_bid['user_details']['email'] or ''}}) -->

                                            </a>
                                            &nbsp;
                                            @if(isset($project_bid['user_details']['kyc_verified']) && $project_bid['user_details']['kyc_verified']!='' && $project_bid['user_details']['kyc_verified'] == '1')          
                                            <span class="verfy-new-arrow">
                                                <a href="#" data-toggle="tooltip" title="" data-original-title="Identity Verified"><img src="{{ url('/') }}/public/front/images/verifild.png" alt=""> </a>
                                            </span>
                                            @endif
                                        </div>
                                      
                                        @if($arr_profile_data['last_login_duration'] == 'Active')
                                        <span title="{{isset($arr_profile_data['expert_timezone'])?$arr_profile_data['expert_timezone']:'Not Specify'}}">
                                            <span style="color:#4D4D4D;font-size: 14px;text-transform:none;cursor:text;">
                                                <span class="flag-image"> 
                                                    @if(isset($arr_profile_data['user_country_flag'])) 
                                                    @php 
                                                    echo $arr_profile_data['user_country_flag']; 
                                                    @endphp 
                                                    @elseif(isset($arr_profile_data['user_country'])) 
                                                    @php 
                                                    echo $arr_profile_data['user_country']; 
                                                    @endphp 
                                                    @else 
                                                    @php 
                                                    echo 'Not Specify'; 
                                                    @endphp 
                                                    @endif 
                                                </span>
                                                {{-- trans('common/_expert_details.text_local_time') --}}  {{isset($arr_profile_data['expert_timezone_without_date'])?$arr_profile_data['expert_timezone_without_date']:'Not Specify'}}
                                            </span>
                                        </span>
                                        @else
                                        <span class="flag-image"> 
                                            @if(isset($arr_profile_data['user_country_flag'])) 
                                            @php 
                                            echo $arr_profile_data['user_country_flag']; 
                                            @endphp 
                                            @elseif(isset($arr_profile_data['user_country'])) 
                                            @php 
                                            echo $arr_profile_data['user_country']; 
                                            @endphp 
                                            @else 
                                            @php 
                                            echo 'Not Specify'; 
                                            @endphp 
                                            @endif 
                                        </span>                            
                                       <span class="date-info">
                                            <span class="clock-image"><img src="{{url('/public')}}/front/images/clock.png" alt="new"/> </span> {{ isset($arr_profile_data['last_login_duration'])?$arr_profile_data['last_login_duration']:'' }}  
                                        </span>                       
                                        @endif
                                        
                                     

                                        <div class="designation" title="{{isset($project_bid['user_details']['role_info']['profession_details']['profession_title'])?$project_bid['user_details']['role_info']['profession_details']['profession_title']:'Not specified'}}">
                                            {{isset($project_bid['user_details']['role_info']['profession_details']['profession_title'])?$project_bid['user_details']['role_info']['profession_details']['profession_title']:'Not specified'}}
                                        </div>
                                        <div class="clearfix"></div>
                                          </div>
                                        <div class="received-bids-bx">
                                        <div class="more-project-dec">
                                            {{str_limit($project_bid['bid_description'],300)}}
                                               </div>
                                        
                                           
                                            <div class="category-new-all">
                                              <img src="{{url('/public')}}/front/images/tag-2.png" alt="" />
                                            <ul> <!-- skill_id -->
                                                @if(isset($project_bid['expert_details']['expert_categories']) && sizeof($project_bid['expert_details']['expert_categories'])>0)
                                                @foreach($project_bid['expert_details']['expert_categories'] as $category)
                                                @php
                                                $search_category_title = "";
                                                $obj_category = \App\Models\CategoriesModel::where('id',$category['category_id'])->first();   
                                                if($obj_category)
                                                {
                                                    $search_category_title =  $obj_category->category_title;  
                                                }
                                                @endphp
                                                <li title="{{$search_category_title}}">{{$search_category_title}}</li>     
                                                @endforeach   
                                                @endif
                                            </ul>
                                        </div>
                                        @if(isset($project_bid['expert_details']['expert_skills']) && sizeof($project_bid['expert_details']['expert_skills'])>0)
                                        <div class="skils-project">
                                             <img src="{{url('/public')}}/front/images/tag.png" alt="" />
                                            <ul> <!-- skill_id -->
                                                @if(isset($project_bid['expert_details']['expert_skills']) && sizeof($project_bid['expert_details']['expert_skills'])>0)
                                                @foreach($project_bid['expert_details']['expert_skills'] as $skill)
                                                @php
                                                $search_skill_name = "";
                                                $obj_skill = \App\Models\SkillsModel::where('id',$skill['skill_id'])->first();   
                                                if($obj_skill){
                                                    $search_skill_name =  $obj_skill->skill_name;  
                                                }
                                                @endphp
                                                <li title="{{$search_skill_name}}">{{$search_skill_name}}</li> 
                                                @endforeach   
                                                @endif
                                            </ul>
                                        </div>
                                        @endif
                                           
                                            <div class="invite-expert-btns">
                                                @if(isset($project_bid['user_details']['role_info']['first_name']) && $project_bid['user_details']['role_info']['first_name'] !="" )  
                                                @if(isset($projectInfo['project_status']) && $projectInfo['project_status'] == '5') 
                                                <button disabled="" style="background: graytext;border: graytext;" class="msg_btn" >{{trans('client/projects/project_details.message')}}</button>
                                                @else
                                                <a href="{{url('/client/twilio-chat/projectbid')}}/{{base64_encode($project_bid['id'])}}" class="applozic-launcher">
                                                    <button class="black-btn" >{{trans('client/projects/project_details.message')}}</button>
                                                </a>
                                                @endif
                                                @if(isset($projectInfo['project_status']) && $projectInfo['project_status'] == '4') 
                                                <a class="black-border-btn" href="{{url('/')}}/client/projects/milestones/{{base64_encode($project_bid['project_id'])}}">
                                                    {{trans('client/projects/ongoing_projects.text_create_milestone')}} 
                                                </a>
                                                @endif
                                                @else
                                                <div class="alert alert-info col-sm-12 col-md-12 col-lg-12">
                                                    <img style="height:21px; margin-top:-4px;" src="{{url('/public')}}/front/images/information-icon-th.png"> 
                                                    {{trans('controller_translations.user_is_no_more')}}
                                                </div>  
                                                @endif  
                                            </div>
                                     
                                    </div>
                                  
                                </div>
                           
                                <?php
                                  $text_hour = '';
                                  if(isset($projectInfo['project_pricing_method']) && $projectInfo['project_pricing_method'] == '2'){
                                    $text_hour = trans('common/_top_project_details.text_hour');
                                  }
                               ?>
                                <div class="rating-profile profile-rating-block">
                                    <div class="projrct-prce1">
                                        

                                        @if(isset($projectInfo['project_currency']) && $projectInfo['project_currency']=="$")
                                            <span>
                                                <img src="{{url('/public')}}/front/images/doller-img.png" alt="img"/> 
                                            </span> <span><!-- {{$project_bid['bid_cost']}} -->{{$projectInfo['project_currency']}}
                                            {{isset($project_bid['bid_cost'])?number_format($project_bid['bid_cost']):0}}{{$text_hour}}</span>
                                        @else
                                        <span>
                                            </span> <span>{{$projectInfo['project_currency']}}
                                            {{isset($project_bid['bid_cost'])?number_format($project_bid['bid_cost']):0}}{{$text_hour}}</span>
                                        @endif
                                    </div>
                                    <div class="rating_profile project-details-rating-profile">
                                        <div class="rate-t">
                                            <div class="rating-title1">
                                                {{trans('client/projects/project_details.duration')}} : 
                                                <span class="rating-value-span">{{$project_bid['bid_estimated_duration']}}</span>
                                            </div>           
                                            <div class="rating-title1">
                                                {{trans('client/projects/project_details.rating')}} :
                                                <span class="rating-value-span">
                                                    <span class="rating-list"><span class="rating-list"><span class="stars">{{isset($arr_profile_data['average_rating'])? $arr_profile_data['average_rating']:'0'  }}</span></span></span>
                                                    @if(isset($arr_profile_data['average_rating']) && $arr_profile_data['average_rating'] != "" )   
                                                    <span> ( {{ number_format(floatval($arr_profile_data['average_rating']), 1, '.', '')  }} )</span>
                                                    @else
                                                    <span>( '0.0' )</span>
                                                    @endif   
                                                    {{-- (&nbsp;{{ $arr_profile_data['average_rating'] or '0' }}&nbsp;) --}}
                                                </span>
                                            </div>           
                                        </div>
                                        <div class="rating_profile">
                                            <div class="rating-title1">
                                                {{trans('client/projects/project_details.review')}} : 
                                                <span class="star-rat rating-value-span"><span>{{ $arr_profile_data['review_count'] or '0' }}</span></span>
                                            </div>         
                                        </div>
                                        <div class="rating_profile">
                                            <div class="rating-title1">
                                                {{trans('client/projects/project_details.completion_rate')}} :
                                                <span class="star-rat rating-value-span">
                                                    <span>{{  isset($arr_profile_data['completion_rate']) ? round((float)$arr_profile_data['completion_rate'], 2):0 }}&nbsp;%</span>
                                                </span>
                                            </div>         
                                        </div>
                                        <div class="rating_profile">
                                            <div class="rating-title1">
                                                {{trans('client/projects/project_details.reputation')}} : 
                                                <span class="star-rat rating-value-span">
                                                    <span>{{  isset($arr_profile_data['reputation']) ? round((float)$arr_profile_data['completion_rate'], 2):0 }}&nbsp;%</span>
                                                </span>
                                            </div>         
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                           
                       
                   
                @endif
            @else    
                <div class="search-grey-bx     awd-div ">    
                            <div class="going-profile-detail">
                                <div class="listin-pro-image">
                                    @if(isset($project_bid['user_details']['is_online']) && $project_bid['user_details']['is_online']!='' && $project_bid['user_details']['is_online']=='1')
                                    <div class="user-online-round"></div>
                                    @else
                                    <div class="user-offline-round"></div>
                                    @endif


                                    @if(isset($project_bid['role_info']['profile_image']) && $project_bid['role_info']['profile_image'] != ""  && file_exists('public/uploads/front/profile/'.$project_bid['role_info']['profile_image']) ) 
                                    <div class="going-pro">
                                        <img src="{{ url('/public/uploads/front/profile/').'/'.$project_bid['role_info']['profile_image']}}" alt="pic"/>
                                    </div>
                                    @else
                                    <div class="going-pro">
                                        <img src="{{  url('/public/uploads/front/profile/default_profile_image.png')}}" alt="pic"/>

                                    </div>
                                    @endif
                                </div>
                                <div class="going-pro-content">
                                    <div class="profile-name" >
                                        <a class="profile-name-section-block" href="{{url('/client/projects/expert_details/'.base64_encode($project_bid['project_id']).'/'.base64_encode($project_bid['expert_user_id']))}}" 
                                            <!-- {{$project_bid['user_details']['user_name'] or ''}} -->
                                            {{isset($project_bid['user_details']['role_info']['first_name'])?$project_bid['user_details']['role_info']['first_name']:'Unknown user'}} 
                                            @if(isset($project_bid['user_details']['role_info']['last_name']) && $project_bid['user_details']['role_info']['last_name'] !="") @php echo substr($project_bid['user_details']['role_info']['last_name'],0,1).'.'; @endphp @endif
                                            <!-- ({{$project_bid['user_details']['email'] or ''}}) -->
                                        </a>
                                        &nbsp;
                                        @if(isset($project_bid['user_details']['kyc_verified']) && $project_bid['user_details']['kyc_verified']!='' && $project_bid['user_details']['kyc_verified'] == '1')          
                                        <span class="verfy-new-arrow">
                                            <a href="#" data-toggle="tooltip" title="" data-original-title="Identity Verified"><img src="{{ url('/') }}/public/front/images/verifild.png" alt=""> </a>
                                        </span>
                                        @endif
                                    </div>
                                    

                                    @if($arr_profile_data['last_login_duration'] == 'Active')
                                        <span title="{{isset($arr_profile_data['expert_timezone'])?$arr_profile_data['expert_timezone']:'Not Specify'}}">
                                            <span style="color:#4D4D4D;font-size: 14px;text-transform:none;cursor:text;">
                                                <span class="flag-image"> 
                                                    @if(isset($arr_profile_data['user_country_flag'])) 
                                                        @php 
                                                        echo $arr_profile_data['user_country_flag']; 
                                                        @endphp 
                                                    @elseif(isset($arr_profile_data['user_country'])) 
                                                        @php 
                                                            echo $arr_profile_data['user_country']; 
                                                        @endphp 
                                                    @else 
                                                        @php 
                                                        echo 'Not Specify'; 
                                                        @endphp 
                                                    @endif 
                                                </span>
                                            {{-- trans('common/_expert_details.text_local_time') --}}  {{isset($arr_profile_data['expert_timezone_without_date'])?$arr_profile_data['expert_timezone_without_date']:''}}</span>
                                        </span>
                                    @else
                                        <span class="flag-image"> 
                                            @if(isset($arr_profile_data['user_country_flag'])) 
                                                @php 
                                                echo $arr_profile_data['user_country_flag']; 
                                                @endphp 
                                            @elseif(isset($arr_profile_data['user_country'])) 
                                                @php 
                                                echo $arr_profile_data['user_country']; 
                                                @endphp 
                                            @else 
                                                @php 
                                                echo 'Not Specify'; 
                                                @endphp 
                                            @endif 
                                        </span>   
                                        <span class="date-info">
                                            <span class="clock-image"><img src="{{url('/public')}}/front/images/clock.png" alt="new"/> </span> {{ isset($arr_profile_data['last_login_duration'])?$arr_profile_data['last_login_duration']:'Not Specify' }}  
                                        </span>
                                    @endif
                                  



                                    <div class="designation" title="{{isset($project_bid['user_details']['role_info']['profession_details']['profession_title'])?$project_bid['user_details']['role_info']['profession_details']['profession_title']:'Not specified'}}">
                                        {{isset($project_bid['user_details']['role_info']['profession_details']['profession_title'])?$project_bid['user_details']['role_info']['profession_details']['profession_title']:'Not specified'}}
                                    </div>
                                    <div class="clearfix"></div>

                                 {{--    <div class="designation" title="{{isset($project_bid['user_details']['role_info']['profession_details']['profession_title'])?$project_bid['user_details']['role_info']['profession_details']['profession_title']:'Not specified'}}">
                                        {{isset($project_bid['user_details']['role_info']['profession_details']['profession_title'])?$project_bid['user_details']['role_info']['profession_details']['profession_title']:'Not specified'}}
                                    </div>
                                    <div class="clearfix"></div> --}}
                                    
                                     </div>
                                    <div class="received-bids-bx">


                                    
                                         <div class="more-project-dec">
                                        {{str_limit($project_bid['bid_description'],260)}} @if(strlen($project_bid['bid_description']) > 260)...@endif
                                              
                                        @php
                                        $timeFirst      = strtotime(date('Y-m-d H:i:s'));
                                        $timeSecond     = strtotime($projectInfo['bid_closing_date']);
                                        $diffInSeconds = $timeSecond - $timeFirst;
                                        if($diffInSeconds <= 0){
                                            $diffInSeconds = '0';
                                        }
                                        @endphp
                                        {{--@if($diffInSeconds>0)--}}
                                       
                                    </div>  
                                       <div class="category-new-all">
                                       <img src="{{url('/public')}}/front/images/tag-2.png" alt="" />
                                        <ul> <!-- skill_id -->
                                            @if(isset($project_bid['expert_details']['expert_categories']) && sizeof($project_bid['expert_details']['expert_categories'])>0)
                                            @foreach($project_bid['expert_details']['expert_categories'] as $category)
                                            @php
                                            $search_category_title = "";
                                            $obj_category = \App\Models\CategoriesModel::where('id',$category['category_id'])->first();   
                                            if($obj_category)
                                            {
                                                $search_category_title =  $obj_category->category_title;  
                                            }
                                            @endphp
                                            <li title="{{$search_category_title}}">{{$search_category_title}}</li>     
                                            @endforeach   
                                            @endif
                                        </ul>
                                    </div> 
                                    @if(isset($project_bid['expert_details']['expert_skills']) && sizeof($project_bid['expert_details']['expert_skills'])>0)                                 
                               <div class="skils-project">
                                <img src="{{url('/public')}}/front/images/tag.png" alt="" />
                                <ul>
                                    @if(isset($project_bid['expert_details']['expert_skills']) && sizeof($project_bid['expert_details']['expert_skills'])>0)
                                     @foreach($project_bid['expert_details']['expert_skills'] as $skill)
                                            @php
                                            $search_skill_name = "";
                                            $obj_skill = \App\Models\SkillsModel::where('id',$skill['skill_id'])->first();   
                                            if($obj_skill){
                                                $search_skill_name =  $obj_skill->skill_name;  
                                            }
                                            @endphp
                                    <li>{{$search_skill_name}}</li>
                                    @endforeach
                                    @endif
                                   
                                </ul>
                            </div>
                            @endif
                                   
                                    <div class="user-box">
                                            @if(isset($project_bid['bid_attachment']) && trim($project_bid['bid_attachment']) != "" && file_exists('public/uploads/front/bids/'.$project_bid['bid_attachment']))
                                            <div class="input-name">
                                                <div class="upload-block">
                                                    <div class="">
                                                        <!-- <input type="text" class="form-control file-caption  kv-fileinput-caption" id="profile_image_name" disabled="disabled" value="{{$project_bid['bid_attachment'] or ''}}" > -->
                                                        <a title="Download Previous Proposal" download href="{{url('/public')}}/uploads/front/bids/{{$project_bid['bid_attachment']}}" >
                                                            <div class="p-control-label"><i class="fa fa-paperclip"></i> {{ trans('expert/projects/add_project_bid.text_attachment') }} : <span style="color: #0E81C2;"><i class="fa fa-download"></i></span></div>
                                                        </a>    
                                                    </div>
                                                </div>
                                                <div id="msg_bid_attach" style="color: red;font-size: 12px;font-weight:600;display: none;"></div>
                                                <div class="user-box hidden-lg">&nbsp;</div>
                                                <div class="note_browse"></div>
                                            </div>
                                            @endif
                                            @if(isset($project_bid['nda_aggreement_pdf']) && trim($project_bid['nda_aggreement_pdf']) != "" && file_exists('public/uploads/front/nda-agreement/'.$project_bid['nda_aggreement_pdf']))
                                            <div class="input-name">
                                                <div class="upload-block">
                                                    <div class="">
                                                        <a title="Download Previous Proposal" download href="{{url('/public/uploads/front/nda-agreement/')}}/{{$project_bid['nda_aggreement_pdf']}}" >
                                                            <div class="p-control-label"><i class="fa fa-paperclip"></i> NDA agreement : <span style="color: #0E81C2;"><i class="fa fa-download"></i></span></div>
                                                        </a>    
                                                    </div>
                                                </div>
                                                <div id="msg_bid_attach" style="color: red;font-size: 12px;font-weight:600;display: none;"></div>
                                                <div class="user-box hidden-lg">&nbsp;</div>
                                                <div class="note_browse"></div>
                                            </div>
                                            @endif  
                                        </div> 
                                    <div class="invite-expert-btns">
                                            @if(isset($project_bid['user_details']['role_info']['first_name']) && $project_bid['user_details']['role_info']['first_name'] !="" )
                                                @if(isset($project_bid['bid_status']) &&  $project_bid['bid_status'] == "4")  
                                                    @if(isset($projectInfo['project_status']) && $projectInfo['project_status']!='5')
                                                        <button type="button" class="release-btn btn-release-block" >{{trans('client/projects/project_details.awarded')}}
                                                        </button>
                                                    @endif
                                                        <a href="{{url('/client/twilio-chat/projectbid')}}/{{base64_encode($project_bid['id'])}}" class="applozic-launcher">
                                                            <button class="black-btn" >{{trans('client/projects/project_details.message')}}</button>
                                                        </a>

                                                        @if(isset($award_request) && $award_request=='expired')
                                                            <button onclick="cancel_award(this);" project-id="{{base64_encode($projectInfo['id'])}}" expert-id="{{base64_encode($project_bid['expert_user_id'])}}" type="button" class="red-btn">{{trans('client/projects/project_details.cancel_award')}}</button>
                                                        @endif
                                                @elseif(isset($project_bid['bid_status']) &&  $project_bid['bid_status'] == "3")
                                                    {{-- IF bid status is 3 then Project is rejected  By expert --}}
                                                    <a href="javascript:void(0)" >
                                                        <button type="button"  class="black-btn" style="cursor: default;">{{trans('client/projects/project_details.rejected')}}</button>
                                                    </a>
                                                    <div class="prj-bid-reject"><span>{{ trans('new_translations.project_rejection_reason')}}:</span> {{isset($project_bid['rejection_reason'])?$project_bid['rejection_reason']:''}}</div>
                                                @else
                                                    @if($is_bid_awarded == 1)
                                                        <a href="javascript:void(0)" >
                                                            <button type="button" style="background: graytext;border: graytext; cursor:no-drop;" class="awd_btn">{{trans('client/projects/project_details.award')}}</button>
                                                        </a>
                                                    @else
                                                        @if(isset($projectInfo['project_status']) && $projectInfo['project_status']!='5')
                                                            <a onclick="confirmAward(this);" expert-user-id="{{base64_encode($project_bid['expert_user_id'])}}" project-id="{{base64_encode($project_bid['project_id'])}}">
                                                            <button type="button" class="blue_btn">{{trans('client/projects/project_details.award')}}</button>
                                                            </a>
                                                        @endif
                                                    @endif
                                                        <a href="{{url('/client/twilio-chat/projectbid')}}/{{base64_encode($project_bid['id'])}}" class="applozic-launcher">
                                                            <button class="black-btn" >{{trans('client/projects/project_details.message')}}</button>
                                                        </a>
                                                @endif  
                                                @else
                                                    <div class="alert alert-info">
                                                        <img style="height:21px; margin-top:-4px;" src="{{url('/public')}}/front/images/information-icon-th.png"> 
                                                        {{trans('controller_translations.user_is_no_more')}}
                                                    </div>
                                                @endif
                                        </div>
                                    <div class="clearfix"></div>
                                  
                                </div>
                               
                            </div>
                        
                                <?php
                                 $text_hour1 = '';
                                  if(isset($projectInfo['project_pricing_method']) && $projectInfo['project_pricing_method'] == '2'){
                                    $text_hour1 = trans('common/_top_project_details.text_hour');
                                  }
                               ?>
                            <div class="rating-profile profile-rating-block">

                                @if(isset($project_bid['is_shortlist']) && $project_bid['is_shortlist'] !='' && $project_bid['is_shortlist'] == '0')
                                    <a onclick="addShortListed(this);" projectBid-id="{{ isset($project_bid['id'])?base64_encode($project_bid['id']):'' }}" href="javascript:void(0);">Add to shortlist</a>

                                    
                                    <a title="{{ isset($project_bid['shortlist_desc'])?$project_bid['shortlist_desc']:'' }}" onclick="openForm(this)" style="cursor: pointer;" data-bid-id="{{ isset($project_bid['id'])?base64_encode($project_bid['id']):'' }}"><i class="fa fa-info-circle"></i></a>
                                @else
                                    <a onclick="removeShortListed(this);" projectBid-id="{{ isset($project_bid['id'])?base64_encode($project_bid['id']):'' }}" href="javascript:void(0);">Shortlisted</a>
                                    
                                    <a title="{{ isset($project_bid['shortlist_desc'])?$project_bid['shortlist_desc']:'' }}" onclick="openForm(this)" style="cursor: pointer;" data-bid-id="{{ isset($project_bid['id'])?base64_encode($project_bid['id']):'' }}"><i class="fa fa-info-circle"></i></a>

                                @endif

                                <div class="projrct-prce1">

                                    @if(isset($projectInfo['project_currency']) && $projectInfo['project_currency']=="$")
                                        <span>{{$projectInfo['project_currency_code']}}
                                        {{isset($project_bid['bid_cost'])?number_format($project_bid['bid_cost']):0}}{{$text_hour1}}</span>
                                    @else
                                        <span>{{$projectInfo['project_currency_code']}}
                                        {{isset($project_bid['bid_cost'])?number_format($project_bid['bid_cost']):0}}{{$text_hour1}}</span>
                                    @endif
                                </div>
                                <div class="rating_profile project-details-rating-profile">
                                    <div class="rate-t">
                                        <div class="rating-title1">
                                            {{trans('client/projects/project_details.duration')}} : 
                                            <span class="rating-value-span">{{$project_bid['bid_estimated_duration']}}</span>
                                        </div>           
                                        <div class="rating-title1">
                                            {{trans('client/projects/project_details.rating')}} :
                                            <span class="rating-value-span">
                                                <span class="rating-list"><span class="rating-list"><span class="stars">{{isset($arr_profile_data['average_rating'])? $arr_profile_data['average_rating']:'0'  }}</span></span></span>
                                                @if(isset($arr_profile_data['average_rating']) && $arr_profile_data['average_rating'] != "" )   
                                                <span> ( {{ number_format(floatval($arr_profile_data['average_rating']), 1, '.', '')  }} )</span>
                                                @else
                                                <span>( '0.0' )</span>
                                                @endif   
                                                {{-- (&nbsp;{{ $arr_profile_data['average_rating'] or '0' }}&nbsp;) --}}
                                            </span>
                                        </div>           
                                    </div>
                                    <div class="rating_profile">
                                        <div class="rating-title1">
                                            {{trans('client/projects/project_details.review')}} : 
                                            <span class="star-rat rating-value-span"><span>{{ $arr_profile_data['review_count'] or '0' }}</span></span>
                                        </div>         
                                    </div>
                                    <div class="rating_profile">
                                        <div class="rating-title1">
                                            {{trans('client/projects/project_details.completion_rate')}} :
                                            <span class="star-rat rating-value-span">
                                                <span>{{  isset($arr_profile_data['completion_rate']) ? round((float)$arr_profile_data['completion_rate'], 2):0 }}&nbsp;%</span>
                                            </span>
                                        </div>         
                                    </div>
                                    <div class="rating_profile">
                                        <div class="rating-title1">
                                            {{trans('client/projects/project_details.reputation')}} : 
                                            <span class="star-rat rating-value-span">
                                                <span>{{  isset($arr_profile_data['reputation']) ? round((float)$arr_profile_data['completion_rate'], 2):0 }}&nbsp;%</span>
                                            </span>
                                        </div>         
                                    </div>
                                </div>
                            </div>
                      <div class="clearfix"></div>
                    
                </div>
            @endif
            @endforeach
        @else 
            @if($projectInfo['project_status'] != 5)
                <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;">
                    <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                        <div class="search-grey-bx">
                            <div class="search-head" style="color:rgba(45, 45, 45, 0.8);">
                                @if(isset($projectInfo['project_handle_by']) && $projectInfo['project_handle_by'] == '2' )
                                <div class="search-head" style="color:rgba(45, 45, 45, 0.8);"> 
                                    {{trans('client/projects/project_details.recruiter_take_care_experts')}}
                                </div>  
                                @else
                                {{trans('client/projects/project_details.currently_no_bids_available')}}
                                @endif

                            </div>
                        </div>
                    </td>
                </tr>
            @endif   
        @endif

        @if(isset($projectInfo['project_status']) && $projectInfo['project_status'] == '3')
            <?php $user = Sentinel::check();?>
        @if(isset($arr_review_details) && sizeof($arr_review_details)>0   ) 
        @foreach($arr_review_details as $review)

        <?php
        $user = Sentinel::check();
        if($review['from_user_id'] == $user->id){
            $profile_image = isset($review['from_user_info']['profile_image']) ?$review['from_user_info']['profile_image']:"default_profile_image.png";
            $name          = ""; 
            $first_name    = isset($review['from_user_info']['first_name']) ?$review['from_user_info']['first_name']:"";
            $last_name     = isset($review['from_user_info']['last_name']) ?substr($review['from_user_info']['last_name'], 0,1).'.':"";
            $name         = $first_name.' '.$last_name;

            $title = trans('client/projects/completed_projects.text_your_rating');
        }
        else
        {
            if($user->inRole('client'))
            {
                $title = trans('client/projects/completed_projects.text_expert_rating');
            } else if($user->inRole('expert'))
            {
                $title = trans('client/projects/completed_projects.text_client_rating');
            }

            if(isset($review['from_user_info']['profile_image']) && $review['from_user_info']['profile_image'] != "" && file_exists('public/uploads/front/profile/'.$review['from_user_info']['profile_image']) )
            {
                $profile_image = isset($review['from_user_info']['profile_image']) ?$review['from_user_info']['profile_image']:"default_profile_image.png";
            } 
            else
            {
                $profile_image = "default_profile_image.png";
            }

            $name          = ""; 
            $first_name    = isset($project_bid['user_details']['role_info']['first_name']) ?$project_bid['user_details']['role_info']['first_name']:"";
            $last_name     = isset($project_bid['user_details']['role_info']['last_name']) ?substr($project_bid['user_details']['role_info']['last_name'], 0,1).'.':"";
            if($first_name =="" && $last_name == ""){
                $name          = 'Unknown user';
            }else{
                $name          = $first_name.' '.$last_name;
            }
        }
        ?>


        @if(isset($is_review_exists) && $is_review_exists!=0 )
        <div class="search-grey-bx">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <div class="going-profile-detail">
                        <div class="listin-pro-image">
                            <div class="going-pro" style="display:inline-block;">
                                <img src="{{url('/public')}}/uploads/front/profile/{{$profile_image or ''}}" alt="pic"/>
                            </div>
                        </div>
                        <div class="going-pro-content">
                            <div class="profile-name">{{ $title }}&nbsp;&nbsp;
                                <span style="color:#494949;font-size: 14px;" class="sub-project-dec" > 
                                    (&nbsp;{{ $name or 'Unknown user'}}&nbsp;)
                                </span>
                            </div>
                            <div class="more-project-dec" style="height:auto;">
                                {{ $review['review'] }}
                                <div class="clr"></div>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 br-left">
                    <div class="rating-profile1" style="display: inherit; margin-top: 18px;">
                        <div class="rating_profile">
                            <div class="rating-title1">{{trans('client/projects/project_reviews.text_review_rate')}}  <span>:</span> 
                            </div>
                            <div class="rate-t">
                                <div class="rating-list">
                                    <span class="stars">{{isset($review['rating'])? $review['rating']:'0'  }}</span>
                                </div>
                                @if(isset($review['rating']) && $review['rating'] != "" )   
                                ( {{ number_format(floatval($review['rating']), 1, '.', '')  }} )
                                @else
                                ( '0.0' )
                                @endif
                            </div>
                        </div>
                        <div class="rating_profile">
                            <div class="rating-title1">{{trans('client/projects/project_reviews.text_date')}}<span>:</span> </div>
                            <div class="rating-list"> {{ date('d M Y' ,strtotime($review['created_at']))}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        @endforeach                       
        @endif

        @if(isset($is_review_exists) && $is_review_exists==0 )
        <div class="search-grey-bx">
            <form id="frm_add_review" method="post"  action="{{url('/') }}/client/review/add">
                {{ csrf_field() }}
                <div class="subm-text">{{trans('client/projects/project_reviews.text_title')}}</div>
                <div class="row">
                    <input type="hidden" readonly="" name="project_id" value="{{isset($projectInfo['id'])?base64_encode($projectInfo['id']):''}}" />
                    <input type="hidden" readonly="" name="expert_user_id" value="{{isset($projectInfo['expert_user_id'])?base64_encode($projectInfo['expert_user_id']):''}}" />
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="user-bx user-frandlys">
                            <div class="frm-nm">{{trans('client/projects/project_reviews.text_rating_type_one')}}<i style="color: red;">*</i></div>
                            <select  class="review-rating-starts" name="rating_type_one">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            <span class="error">{{ $errors->first('rating_type_one') }}</span>
                            <div class="frm-nm">{{trans('client/projects/project_reviews.text_rating_type_two')}}<i style="color: red;">*</i></div>
                            <select  class="review-rating-starts" name="rating_type_two">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            <span class="error">{{ $errors->first('rating_type_two') }}</span>
                            <div class="frm-nm">{{trans('client/projects/project_reviews.text_rating_type_three')}}<i style="color: red;">*</i></div>
                            <select  class="review-rating-starts" name="rating_type_three">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            <span class="error">{{ $errors->first('rating_type_three') }}</span>
                            <div class="frm-nm">{{trans('client/projects/project_reviews.text_rating_type_four')}}<i style="color: red;">*</i></div>
                            <select  class="review-rating-starts" name="rating_type_four">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            <span class="error">{{ $errors->first('rating_type_four') }}</span>
                            <div class="frm-nm">{{trans('client/projects/project_reviews.text_rating_type_five')}}<i style="color: red;">*</i></div>
                            <select  class="review-rating-starts" name="rating_type_five">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            <span class="error">{{ $errors->first('rating_type_five') }}</span>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="user-bx user-frandlys">
                            <div class="frm-nm">{{trans('client/projects/project_reviews.text_comment')}}<i style="color: red;">*</i></div>
                            <textarea style="max-width:100%;height:125px"  name="review" id="comment" data-rule-required="true" class="det-in" placeholder="{{trans('client/projects/project_reviews.placeholder_comment')}}"></textarea>
                            <span class="error">{{ $errors->first('review') }}</span>
                        </div>
                    </div>
                    <div class="clr"></div>
                    <br/>
                    <div class="col-sm-2 col-md-2 col-lg-3">
                        <button type="submit" class="det-sub-btn"  onclick="showLoader()" style="float:none;">{{trans('client/projects/project_reviews.text_add')}}</button>
                    </div>
                </div>
            </form>
        </div>

        @endif
        @endif

        @if(isset($expert_who_accepted_project))
        @else
        @include('front.common.pagination_view', ['paginator' => $arr_pagination])
        @endif
</div>

        @if(!empty($arr_milestone['data']))
        <div class="milestone-listing">
            @if(isset($arr_milestone['data']) && count($arr_milestone['data']) > 0)
            <div class="new-head-bid">{{trans('milestones/project_milestones.text_title')}}</div>
        

            <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;"> 
                <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                   
                    
                        <!-- Wallet Details -->
                        @if(isset($mangopay_wallet_details) && $mangopay_wallet_details !="" && !empty($mangopay_wallet_details))
                        <div style="padding:0px; margin-bottom: 15px;">
                            <div class="dash-user-details" >
                                <div class="title"><i class="fa fa-money" aria-hidden="true"></i> {{trans('common/wallet/text.text_project_wallet')}} </div>
                                <div class="user-details-section">
                                    <table class="table" style="margin-bottom:15px">
                                        <tbody>     
                                            <tr>
                                                <td>
                                                    <span class="card-id"> {{ isset($mangopay_wallet_details->Id)? $mangopay_wallet_details->Id:''}} </span>
                                                </td>
                                                <td>
                                                    <div class="description">{{isset($mangopay_wallet_details->Description)? $mangopay_wallet_details->Description:''}}</div>
                                                </td>
                                                <td style="font-size: 17px;width: 120px;">
                                                    <div class="high-text-up">
                                                        <span>
                                                            {{ isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:''}}
                                                        </span>
                                                        <span class="mp-amount">
                                                            {{ isset($mangopay_wallet_details->Balance->Amount)? $mangopay_wallet_details->Balance->Amount/100:'0'}}
                                                        </span>
                                                    </div>
                                                    <div class="small-text-down" style="font-size: 10px;">
                                                        {{trans('common/wallet/text.text_money_balance')}}
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @endif  
                        <!-- End Wallet Details -->
                   
                </td>
            </tr>
            @foreach($arr_milestone['data'] as $key=>$milestone)
            <div class="search-grey-bx white-wrapper">
                <div class="milestone-details">
                    @if(empty($milestone['milestone_release_details']['status']) || isset($milestone['milestone_release_details']['status']) && $milestone['milestone_release_details']['status'] != '2' && $milestone['status'] != '0')
                    <div class="min-block-time">
                        <div id='mins{{$milestone["id"]}}' class="count-min-block">
                            <span id='timer{{$milestone["id"]}}'></span>
                        </div>
                    </div>
                    @endif
                    <h3><span class="milestone-batch">Milestone {{ isset($milestone['milestone_no']) ?  $milestone['milestone_no']:"0" }}</span> &nbsp;&nbsp; {{ isset($milestone['title']) ? $milestone['title']:"" }}</h3>
                    <div class="project-list mile-list">
                        <ul>
                            <li><span class="hidden-xs hidden-sm"><i class="fa fa-calendar" aria-hidden="true"></i></span>{{ date('d M Y' , strtotime($milestone['created_at']))}}</li>
                            @if(empty($milestone['milestone_release_details']['status']) || isset($milestone['milestone_release_details']['status']) && count($milestone['milestone_refund_details']) <= 0 || $milestone['milestone_refund_details'] == 'null')
                            @if(empty($milestone['milestone_release_details']['status']) || isset($milestone['milestone_release_details']['status']) &&  $milestone['milestone_release_details']['status'] != '2' || $milestone['milestone_release_details']['status'] == 'null')
                            @if(isset($milestone['milestone_due_date']) && $milestone['milestone_due_date'] != "" && $milestone['milestone_due_date'] != '0000-00-00 00:00:00' && $milestone['status'] != '0')
                            <?php 
                            $timeFirst             = strtotime(date('Y-m-d H:i:s'));
                            $timeSecond            = strtotime($milestone['milestone_due_date']);
                            $differenceInSeconds   = $timeSecond - $timeFirst;
                            if($differenceInSeconds <= 0){
                                $differenceInSeconds = '0';
                            }
                            ?>
                            <script type="text/javascript">
                                var time        = '<?php echo $differenceInSeconds; ?>';
                                var milestoneid = '<?php echo $milestone["id"]; ?>';
                                if(time < 0) {  time = 0; }
                                var c           = time; 
                                var t;
                                var days    = parseInt( c / 86400 ) % 365;
                                var hours   = parseInt( c / 3600 ) % 24;
                                var minutes = parseInt( c / 60 ) % 60;
                                var seconds = c % 60;
                                var d = ""; 
                                var h = ""; 
                                var m = "";
                                var s = ""; 

                                if(days     !=  ""  &&  days     !="0"){  d  =  (days+'d')     +  " ";  }
                                if(hours    !=  ""  &&  hours    !="0"){  h  =  (hours+'h')    +  " ";  }
                                if(minutes  !=  ""  &&  minutes  !="0"){  m  =  (minutes+'m')  +  " ";  }
                                if(seconds  !=  ""  &&  seconds  !="0"){  s  =  (seconds+'s')  +  " ";  }

                                if(d != "" && h !=""){
                                    var result    = d + h;
                                }else if(h != "" && m !=""){
                                    var result    = h + m;
                                }else if(m !=""){
                                    var result    = m;
                                }else if(s !=""){
                                    var result    = s;
                                }
                                if(result == "NaN:NaN:NaN:NaN"){ 
                                    $('#timer'+milestoneid).html('{{trans('client/projects/ongoing_projects.text_next_expire')}}');
                                    $('#mins'+milestoneid).addClass('expired');
                                    $('#mins'+milestoneid).removeClass('timer');
                                } else {
                                    $('#timer'+milestoneid).html(result);
                                    $('#mins'+milestoneid).addClass('timer');
                                    $('#mins'+milestoneid).removeClass('expired');
                                }
                                if(c == 0 ){
                                    $('#timer'+milestoneid).html('{{trans('client/projects/ongoing_projects.text_next_expire')}}');
                                    $('#mins'+milestoneid).addClass('expired');
                                    $('#mins'+milestoneid).removeClass('timer');
                                }
                                c = c - 1;
                            </script>
                            @endif
                            @endif
                            <li><span class="hidden-xs hidden-sm"><i class="fa fa-calendar" aria-hidden="true"></i></span>{{trans('client/projects/project_details.entry_milestone_due_date')}} : {{ date('d M Y' , strtotime($milestone['milestone_due_date']))}}</li>
                            @endif

                            <li style="background:none;">
                                <span class="projrct-prce">
                                    @if(isset($milestone_project_currency) && $milestone_project_currency=="$")
                                    <i class="hidden-xs hidden-sm fa fa-money" aria-hidden="true"></i>  {{$milestone_project_currency}}&nbsp;{{ isset($milestone['cost'])? $milestone['cost']: '0' }}
                                    @else
                                    <i class="hidden-xs hidden-sm fa fa-money" aria-hidden="true"></i> {{'$'}}&nbsp;{{ isset($milestone['cost'])? $milestone['cost']: '0' }}
                                    @endif
                                </span>
                            </li>
                        </ul>
                    </div>
                    <div class="more-project-dec" style="margin-bottom: 10px;">{{ str_limit(isset($milestone['description'])? $milestone['description']: "", 350) }}</div>
                    @if(isset($milestone['milestone_refund_details']) && count($milestone['milestone_refund_details']) > 0)
                    @if($user->inRole('expert'))
                    <a href="javascript:void(0)">
                        <button class="awd_btn new-btn-class" style="cursor: default;">Aborted by client</button>
                    </a>
                    <div class="clerfix"></div>
                    <small><i>Client requested for refund to ArchExperts</i></small>
                    @elseif($user->inRole('client'))
                    <a href="javascript:void(0)">
                        @if($milestone['milestone_refund_details']['is_refunded'] == 'no')
                        <button class="awd_btn new-btn-class green-new-btn">Refund requested</button>
                        <button class="awd_btn new-btn-class request-refund-btn" 
                        href="#refund_request" 
                        data-keyboard="false" 
                        data-backdrop="static" 
                        data-toggle="modal" 
                        id="refund-request-model" 
                        data-milestone="{{isset($milestone['milestone_no']) ?  $milestone['milestone_no']:'0'}}" 
                        data-expert_id="{{isset($milestone['expert_user_id']) ?  $milestone['expert_user_id']:'0'}}" 
                        data-milestone-cost="{{isset($milestone['cost']) ?  $milestone['cost']:'0'}}" 
                        data-project-id="{{isset($milestone['project_id'])?$milestone['project_id']:'0'}}" 
                        data-currency-code="{{ isset($milestone['project_details']['project_currency_code']) ? $milestone['project_details']['project_currency_code'] : 'USD' }}"
                        data-milestone-id="{{isset($milestone['id'])?$milestone['id']:'0'}}"><i class="fa fa-repeat" aria-hidden="true"></i> Resend</button>
                        @else
                        <button class="awd_btn new-btn-class green-new-btn"> Refunded</button>
                        @endif
                    </a>
                    @endif  
                    @else
                    @if(isset($milestone['transaction_details']) && isset($milestone['transaction_details']['payment_status']) && ($milestone['transaction_details']['payment_status']==1 || $milestone['transaction_details']['payment_status']==2) )
                    @if($user->inRole('expert'))
                    @if(isset($milestone['milestone_release_details']['status']) && $milestone['milestone_release_details']['status'] == '0')
                    <a href="javascript:void(0)">
                        <button class="black-btn" style="cursor: default;background-color:#08a23b;">{{trans('milestones/project_milestones.text_requested')}} </button>
                    </a>
                    @elseif(isset($milestone['milestone_release_details']['status']) && $milestone['milestone_release_details']['status'] == '1')
                    <a href="javascript:void(0)">
                        <button class="black-border-btn" style="cursor: default;">{{trans('milestones/project_milestones.text_approved')}} </button>
                    </a>
                    @elseif(isset($milestone['milestone_release_details']['status']) && $milestone['milestone_release_details']['status'] == '2')
                    <a href="javascript:void(0);">
                        <button class="black-btn"  style="cursor: default; background-color: #28B463;"> {{trans('milestones/project_milestones.text_paid')}} </button>
                    </a>
                    <a href="{{$module_url_path}}/invoice/{{base64_encode($milestone['id'])}}">
                        <button class="black-border-btn">{{trans('milestones/project_milestones.text_invoice')}} </button>
                    </a>
                    @elseif(isset($milestone['project_details']['project_status']) && $milestone['project_details']['project_status'] != '5')
                    <a onclick="confirmRelease(this)" data-release-id="{{base64_encode($milestone['id'])}}">
                        <button class="black-btn">{{trans('milestones/project_milestones.text_release_request')}}</button>
                    </a>
                    @endif
                    @endif 
                    @else
                    @if($user->inRole('client') && isset($milestone['project_details']['project_status']) && $milestone['project_details']['project_status']!='5')
                    <span><b>{{trans('milestones/project_milestones.text_unsuccessful_payment')}}</b></span>
                  <br>
                    <a href="{{ $module_url_path }}/milestones/delete/{{base64_encode($milestone['id'])}}">
                        <button class="release-btn-red">{{trans('milestones/project_milestones.text_delete')}} </button>
                    </a>
                    @endif  
                    @endif
                    @if(isset($milestone['milestone_release_details']) && $milestone['milestone_release_details'] != NULL)    
                    @if($user->inRole('client'))
                    @if(isset($milestone['milestone_release_details']['status']) && $milestone['milestone_release_details']['status'] == '0')  
                    <span >{{trans('milestones/project_milestones.text_approve_request_received_by_expert')}}</span>
                    <br/>
                    <br/>
                    <a onclick="confirmApprove(this)" data-currency"{{$projectInfo['project_currency_code']}}" data-release-details-id="{{base64_encode($milestone['milestone_release_details']['id'])}}">
                        <button class="awd_btn new-btn-class in-progress-btn"><font color="white">{{trans('milestones/project_milestones.text_approve')}}</font></button>
                    </a>  

                    <a onclick="confirmNotApprove(this)" data-release-details-id="{{base64_encode($milestone['milestone_release_details']['id'])}}">
                        <button class="not-approved-btn">{{trans('milestones/project_milestones.text_reject_request')}}</button>
                    </a> 



                    @elseif(isset($milestone['milestone_release_details']['status']) && $milestone['milestone_release_details']['status'] == '1')  
                    <a href="javascript:void(0)">
                        <button class="awd_btn new-btn-class" style="cursor: default;">{{trans('milestones/project_milestones.text_approved')}} </button>
                    </a>
                    @elseif(isset($milestone['milestone_release_details']['status']) && $milestone['milestone_release_details']['status'] == '2')  
                    <a href="javascript:void(0)">
                        <button class="black-btn active-green new-btn-class"><font color="white">  {{trans('milestones/project_milestones.text_paid')}}</font> </button>
                    </a>
                    @endif
                    @endif
                    @elseif($user->inRole('client') && (isset($milestone['transaction_details']) && isset($milestone['transaction_details']['payment_status']) && ($milestone['transaction_details']['payment_status']==1 || $milestone['transaction_details']['payment_status']==2) ))
                    <a href="javascript:void(0);">
                        <button class="awd_btn new-btn-class in-progress-btn">
                            <font color="white">{{trans('milestones/project_milestones.text_inprogress')}} </font></button>
                        </a>
                        <a href="#refund_request" 
                        data-keyboard="false" 
                        data-backdrop="static" 
                        data-toggle="modal" 
                        id="refund-request-model" 
                        data-milestone="{{isset($milestone['milestone_no']) ?  $milestone['milestone_no']:'0'}}" 
                        data-expert_id="{{isset($milestone['expert_user_id']) ?  $milestone['expert_user_id']:'0'}}"
                        data-milestone-cost="{{isset($milestone['cost']) ?  $milestone['cost']:'0'}}" 
                        data-project-id="{{isset($milestone['project_id'])?$milestone['project_id']:'0'}}" 
                        data-currency-code="{{ isset($milestone['project_details']['project_currency_code']) ? $milestone['project_details']['project_currency_code'] : 'USD' }}"
                        data-milestone-id="{{isset($milestone['id'])?$milestone['id']:'0'}}">
                        <button class="awd_btn new-btn-class request-refund-btn">
                            <font color="white"> {{trans('milestones/project_milestones.text_refund_request')}}</font></button>
                        </a>
                        @endif
                        @endif      
                    </div>
                </div>
                @endforeach
            </div> 
            @else
            <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;"> 
                <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                    <div class="new-head-bid">{{trans('milestones/project_milestones.text_title')}}</div>
                    <div class="search-grey-bx">
                        <div class="milestone-title">
                            <a href="{{ $module_url_path }}/details/{{ $enc_project_id }}"><span>{{isset($milestone_project_name)?''.$milestone_project_name.'':''}}</span></a>
                        </div>
                        <br/>
                        <div class="clearfix"></div> 
                        @if(isset($mangopay_wallet_details) && $mangopay_wallet_details !="" && !empty($mangopay_wallet_details))
                        <div class="col-sm-12 col-md-6 col-lg-6" style="padding:0px; margin-bottom: 15px;">
                            <div class="dash-user-details">
                                <div class="title"><i class="fa fa-money" aria-hidden="true"></i> Wallet </div>
                                <div class="user-details-section">
                                    <table class="table" style="margin-bottom:15px">
                                        <tbody>     
                                            <tr>
                                                <td>
                                                    <span class="card-id"> {{ isset($mangopay_wallet_details->Id)? $mangopay_wallet_details->Id:''}} </span>
                                                </td>
                                                <td>
                                                    <div class="description" style="font-size: 11px;">{{isset($mangopay_wallet_details->Description)? $mangopay_wallet_details->Description:''}}</div>
                                                </td>
                                                <td style="font-size: 17px;width: 120px;">
                                                    <div class="high-text-up">
                                                        <span>
                                                            {{ isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:''}}
                                                        </span>
                                                        <span class="mp-amount">
                                                            {{ isset($mangopay_wallet_details->Balance->Amount)? $mangopay_wallet_details->Balance->Amount/100:'0'}}
                                                        </span>
                                                        
                                                    </div>
                                                    <div class="small-text-down" style="font-size: 10px;">
                                                        money balance
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @endif  
                        <br/>
                        <div class="clearfix"></div>
                        <div class="alert alert-info">
                            <img style="height:21px; margin-top:-4px;" src="{{url('/public')}}/front/images/information-icon-th.png"> 
                            {{trans('milestones/project_milestones.text_no_record_found')}}
                        </div> 
                    </div>
                </td>
            </tr>
            @endif
            
            @include('front.common.pagination_view', ['paginator' => $arr_milestone_pagination])
            @endif

            @if(isset($project_main_attachment['project_attachment']) && $project_main_attachment['project_attachment'] != "" && !empty($project_attachment['data']) && sizeof($project_attachment['data']) > 0)
            <div class="search-grey-bx white-wrapper">
                <div class="coll-listing-section project-coll">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <h2><i class="fa fa-object-group"></i>Collaboration Space</h2>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-6">                        
                            @if(isset($project_attachment['data']) && sizeof($project_attachment['data']) >0 || isset($project_main_attachment['project_attachment']) && $project_main_attachment['project_attachment'] != "")
                            <a href="{{url('/client/projects/collaboration/make-zip/')}}/{{base64_encode($project_id)}}" style="margin: 23px 23px 0;" class="back-btn pull-right download-zip"><i class="fa fa-file-archive-o"></i> {{trans("new_translations.text_download_all_files")}}</a>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        @if(isset($project_main_attachment['project_attachment']) && $project_main_attachment['project_attachment'] != "")
                        <?php 
                        $attachment             = isset($project_main_attachment['project_attachment'])?$project_main_attachment['project_attachment']:"";
                        $get_attatchment_format = get_attatchment_format($attachment);
                        ?>

                        @if(isset($get_attatchment_format) &&
                            $get_attatchment_format == 'doc'  ||
                            $get_attatchment_format == 'docx' ||
                            $get_attatchment_format == 'odt'  ||
                            $get_attatchment_format == 'pdf'  ||
                            $get_attatchment_format == 'txt'  ||
                            $get_attatchment_format == 'zip'  ||
                            $get_attatchment_format == 'xlsx')
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 coll-list">
                                <div class="coll-list-block">
                                    <div class="coll-img p-relative">
                                        @if(file_exists('public/uploads/front/postprojects/'.$project_main_attachment['project_attachment']))
                                        <img src="{{url('/public')}}/front/images/file_formats/{{$get_attatchment_format}}.png" class="img-responsive">
                                        @else
                                        <img src="{{url('/public')}}/front/images/file_formats/image.png"  class="img-responsive" alt=""/>
                                        @endif

                                        <div class="coll-details">
                                            <div class="coll-person">
                                                <div class="coll-name"></div>
                                            </div>

                                        </div>
                                        <div class="list-hover">
                                            <a style="font-size:2em;" download href="{{url('/public')}}{{config('app.project.img_path.project_attachment')}}{{$project_main_attachment['project_attachment']}}" class="view-button"><i class="fa fa-cloud-download"></i></a>
                                        </div>
                                    </div>
                                    <div class="design-title">
                                        <p>Main attachment</p>
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 coll-list">
                                <div class="coll-list-block">
                                    <div class="coll-img p-relative">

                                        @if(file_exists('public/uploads/front/postprojects/'.$project_main_attachment['project_attachment']))
                                        <img src="{{url('/public')}}{{config('app.project.img_path.project_attachment')}}{{$project_main_attachment['project_attachment']}}" class="img-responsive">
                                        @else
                                        <img src="{{url('/public')}}/front/images/archexpertdefault/default-arch.jpg"  class="img-responsive" alt=""/>
                                        @endif
                                        <div class="coll-details">
                                            <div class="coll-person">
                                                <div class="coll-name"></div>
                                            </div>

                                        </div>
                                        <div class="list-hover">
                                            <a target="_blank" href="{{url('/public')}}{{config('app.project.img_path.project_attachment')}}{{$project_main_attachment['project_attachment']}}" class="view-button">View</a>
                                        </div>

                                    </div>
                                    <div class="design-title">
                                        <p style="display: inline;" title="{{isset($project_main_attachment['project_attachment'])?$project_main_attachment['project_attachment']:'Unknown'}}">{{isset($project_main_attachment['project_attachment'])?str_limit($project_main_attachment['project_attachment'],14):'Unknown'}}</p>
                                        <a  style="font-size:1.5em;color:#2d2d2d"  style="display: inline;" class="pull-right" download href="{{url('/public/uploads/front/postprojects/')}}/{{$project_main_attachment['project_attachment']}}"><i class="fa fa-cloud-download"></i></a>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="clearfix"></div>
                            @endif

                            @if(isset($project_attachment['data']) && sizeof($project_attachment['data']) >0)
                            @foreach($project_attachment['data'] as $key => $attch)
                            @if(isset($attch['attachment']) && $attch['attachment']!= '' && file_exists('public/uploads/front/project_attachments/'.$attch['attachment']))
                            @php 
                            $attachment             = isset($attch['attachment'])?$attch['attachment']:"";
                            $get_attatchment_format = get_attatchment_format($attachment);
                            @endphp

                            @if(isset($get_attatchment_format) &&
                                $get_attatchment_format == 'doc'  ||
                                $get_attatchment_format == 'docx' ||
                                $get_attatchment_format == 'odt'  ||
                                $get_attatchment_format == 'pdf'  ||
                                $get_attatchment_format == 'txt'  ||
                                $get_attatchment_format == 'zip'  ||
                                $get_attatchment_format == 'xlsx')
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 coll-list">
                                    <div class="coll-list-block">
                                        <div class="coll-img p-relative">                                          
                                            @if(file_exists('public/uploads/front/project_attachments/'.$attch['attachment']))
                                            <img src="{{url('/public')}}/front/images/file_formats/{{$get_attatchment_format}}.png" class="img-responsive">
                                            @else
                                            <img src="{{url('/public')}}/front/images/file_formats/image.png"  class="img-responsive" alt=""/>
                                            @endif
                                            <div class="coll-details">
                                                <div class="coll-person">
                                                    <div class="coll-name">File #{{isset($attch['attchment_no'])?$attch['attchment_no']:'-'}}</div>
                                                </div>

                                            </div>
                                            <div class="list-hover">
                                                <a href="{{url('/client/projects/collaboration/details')}}/{{isset($attch['id'])?base64_encode($attch['id']):'0'}}/{{base64_encode($project_id)}}" class="view-button">View</a>
                                            </div>
                                        </div>
                                        <div class="design-title">
                                            <p style="display: inline;" title="{{isset($attch['attachment'])?$attch['attachment']:'Unknown'}}">{{isset($attch['attachment'])?str_limit($attch['attachment'],14):'Unknown'}}</p>
                                            <a  style="font-size:1.5em;color:#2d2d2d"  style="display: inline;" class="pull-right" download href="{{url('/public/uploads/front/project_attachments/')}}/{{$attch['attachment']}}"><i class="fa fa-cloud-download"></i></a>
                                        </div>
                                    </div>
                                </div>
                                @else
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 coll-list">
                                    <div class="coll-list-block">
                                        <div class="coll-img p-relative">
                                            @if(file_exists('public/uploads/front/project_attachments/'.$attch['attachment']))
                                            <img src="{{url('/public/uploads/front/project_attachments/')}}/{{$attch['attachment']}}" class="img-responsive">
                                            @else
                                            <img src="{{url('/public')}}/front/images/archexpertdefault/default-arch.jpg"  class="img-responsive" alt=""/>
                                            @endif
                                            <div class="coll-details">
                                                <div class="coll-person">
                                                    <div class="coll-name">File #{{isset($attch['attchment_no'])?$attch['attchment_no']:'-'}}</div>
                                                </div>

                                            </div>
                                            <div class="list-hover">
                                                <a href="{{url('/client/projects/collaboration/details')}}/{{isset($attch['id'])?base64_encode($attch['id']):'0'}}/{{base64_encode($project_id)}}" class="view-button">View</a>
                                            </div>
                                        </div>
                                        <div class="design-title" >
                                            <p style="display: inline;" title="{{isset($attch['attachment'])?$attch['attachment']:'Unknown'}}">{{isset($attch['attachment'])?str_limit($attch['attachment'],14):'Unknown'}}</p>
                                            <a  style="font-size:1.5em;color:#2d2d2d;"  style="display: inline;" class="pull-right" download href="{{url('/public/uploads/front/project_attachments/')}}/{{$attch['attachment']}}"><i class="fa fa-cloud-download"></i></a>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @endif  
                                @endforeach
                                @endif
                            </div>

                            @if(isset($project_main_attachment['project_attachment']) && $project_main_attachment['project_attachment'] == "" && empty($project_attachment['data']) && sizeof($project_attachment['data']) <= 0)
                            <div class="search-grey-bx">
                                <div class="no-record" >
                                    {{trans('expert/projects/awarded_projects.text_sorry_no_record_found')}}
                                </div>
                            </div> 
                            @endif
                            {{-- @include('front.common.pagination_view', ['paginator' => $arr_collaboration_pagination]) --}}
                        </div>
                    </div>
                    @endif
                </div>
            </div>
              </div>
                </div>
        
    

    <script type="text/javascript">

        function sortByHandledBy(ref) {
            var sort_by = $(ref).val();

            if (sort_by != "") {
                window.location.href = "{{ url('client/projects/details') }}/{{base64_encode($project_id)}}" + "?sort_by=" + sort_by;
            } else {
                window.location.href = "{{ url('client/projects/details') }}/{{base64_encode($project_id)}}";
            }
            return true;
        }

    </script>

    <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

    @include('client.projects.invite_expert') 

    @include('client.projects.shortlist_info')

    <script type="text/javascript">
        $('#frm_add_milestone').validate();
        $('#frm_add_review').validate();
        $(function() {
            $('.review-rating-starts').barrating({
                theme: 'fontawesome-stars'
            });
        }); 

        function chk_validation(ref){
            var yourInput = $(ref).val();
            re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
            var isSplChar = re.test(yourInput);
            if(isSplChar){
                var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
                $(ref).val(no_spl_char);
            }
        }
        function confirmAward(ref) {
            // alertify.confirm("Are you sure? You want to award project to this expert ?", function (e) {
            //     if (e) {
            //         var project_id     = $(ref).attr('project-id');
            //         var expert_user_id = $(ref).attr('expert-user-id');
            //         window.location.href="{{$module_url_path}}/request/"+expert_user_id+"/"+project_id;
            //         return true;
            //     } else {
            //         return false;
            //     }
            // });
            swal({
                      title: "Are you sure? You want to award project to this expert ?",
                      text: "",
                      icon: "warning",
                      
                      dangerMode: true,
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, I am sure!',
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                     },
                     function(isConfirm){

                       if (isConfirm){
                          var project_id     = $(ref).attr('project-id');
                          var expert_user_id = $(ref).attr('expert-user-id');
                          window.location.href="{{$module_url_path}}/request/"+expert_user_id+"/"+project_id;
                            return true;

                        } else {
                            swal.close();
                          return false;
                        }
                });
        }

        function cancel_award(ref)
        {
            // alertify.confirm("Are you sure? You want to cancel award request ?", function (e) {
            //     if (e) {
            //         var project_id     = $(ref).attr('project-id');
            //         var expert_id = $(ref).attr('expert-id');
            //         window.location.href="{{$module_url_path}}/cancel_award/"+project_id+"/"+expert_id;
            //         return true;
            //     } else {
            //         return false;
            //     }
            // });
            swal({
                      title: "Are you sure? You want to cancel award request ?",
                      text: "",
                      icon: "warning",
                      
                      dangerMode: true,
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, I am sure!',
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                     },
                     function(isConfirm){

                       if (isConfirm){
                          var project_id     = $(ref).attr('project-id');
                          var expert_id = $(ref).attr('expert-id');
                          window.location.href="{{$module_url_path}}/cancel_award/"+project_id+"/"+expert_id;
                            return true;

                        } else {
                            swal.close();
                          return false;
                        }
                });
        }


        function showLoader(){  
            var comment = $('#comment').val();
            if($.trim(comment)!=="")
            {
                showProcessingOverlay();
            }
        }

        function addShortListed(ref) {
            swal({
                      title: "Are you sure? You want to add this expert in shortlist?",
                      text: "",
                      icon: "warning",
                      dangerMode: true,
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, I am sure!',
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                     },
                     function(isConfirm){
                       if (isConfirm){
                          var project_bid_id     = $(ref).attr('projectBid-id');
                          window.location.href="{{$module_url_path}}/add_shortlist/"+project_bid_id;
                            return true;

                        } else {
                            swal.close();
                          return false;
                        }
                });
        }

        function removeShortListed(ref) {
            swal({
                      title: "Are you sure? You want to remove this expert in shortlist ?",
                      text: "",
                      icon: "warning",
                      dangerMode: true,
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, I am sure!',
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                     },
                     function(isConfirm){
                       if (isConfirm){
                          var project_bid_id     = $(ref).attr('projectBid-id');
                          window.location.href="{{$module_url_path}}/remove_shortlist/"+project_bid_id;
                            return true;

                        } else {
                            swal.close();
                          return false;
                        }
                });
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('.download-zip').click(function(){
                setTimeout(function(){
                    hideProcessingOverlay();
                },500);
            });
        });
    </script>  

    <div class="modal fade invite-member-modal" id="refund_request" role="dialog">
        <div class="modal-dialog modal-lg">
            <button type="button" class="close" data-dismiss="modal"><img src="{{url('/public')}}/front/images/close-icon-black.png" alt=""/></button>
            <div class="modal-content">
                <div class="modal-body">
                    <h2><img style="width: 292px;height: 57px;" src="{{url('/public/front/images/archexpertdefault/arc-hexpert-wallet-logo.png')}}" alt="mangopay"> </h2>
                    <div class="invite-member-section">
                        <center><h4>{{trans('common/wallet/text.text_send_refund_request')}}</h4></center>
                    </div>
                    <form action="{{ $module_url_path }}/refund_requests" method="post" id="refund-request">
                        {{csrf_field()}}
                        <div class="invite-form">
                            <span>{{trans('common/wallet/text.text_milestone')}}  : <span class="ref_milestone">  </span> </span>
                            <div class="clearfix"></div>
                            <span>{{trans('common/wallet/text.text_refund_amount')}}  : <span class="ref_milestone_cost">0</span> <span class="ref_milestone_currency">{{config('app.project_currency.$')}}</span></span>
                            <div class="clearfix"></div>
                            <hr>
                            <input type="hidden" name="ref_milestone_id" id="ref_milestone_id" value="0" placeholder="Milestone id">
                            <input type="hidden" name="ref_project_id" id="ref_project_id" value="0" Placeholder="Project id">
                            <input type="hidden" name="ref_expert_id" id="ref_expert_id" value="0" Placeholder="Expert id">
                            <div class="user-box">
                                <div class="input-name">
                                    <textarea type="text" class="clint-input" data-rule-required="true" name="refunc_request_reasn" id="refunc_request_reasn" placeholder="Enter refund reason"></textarea>
                                </div>
                            </div>
                            <button type="submit" id="send-refund-request" class="black-btn">{{trans('common/wallet/text.text_send')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $('#refund-request').validate();  
    </script>

    <link rel="stylesheet" type="text/css"  href="{{url('/public')}}/front/css/jquery-ui.css"/>

    <script src="{{url('/public')}}/front/js/jquery-ui.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#due_date" ).datepicker({
                dateFormat: 'yy-mm-dd',
                'startDate': new Date(),
                minDate: 'today',
            });
            $(document).on('click', '.add-btn', function (e) {
                var check_active = $(this).next('.money-drop').attr('class');
                if (check_active.indexOf("active") >= 0){
                    $(this).next('.money-drop').removeClass('active');
                } else {
                    $('.money-drop').removeClass('active');
                    $(this).next('.money-drop').addClass('active');
                }
            });
            $(document).on('click','#refund-request-model',function(){
                var project_id      = $(this).data('project-id');
                var milestone_id    = $(this).data('milestone-id');
                var expert_id       = $(this).data('expert_id');
                var milestone       = $(this).data('milestone');
                var milestone_cost  = $(this).data('milestone-cost');
                var currency_code  = $(this).data('currency-code');
                $('#ref_project_id').val(project_id);
                $('#ref_milestone_id').val(milestone_id);
                $('.ref_milestone').html(milestone);
                $('.ref_milestone_cost').html(milestone_cost);
                $('.ref_milestone_currency').html(currency_code);
                $('#ref_expert_id').val(expert_id);
            }); 
        });
    </script>

    <script type="text/javascript">
        function confirmRelease(ref) {
            // alertify.confirm("Are you sure? You want to send release request ?", function (e) {
            //     if (e) {
            //         var release_id = $(ref).attr('data-release-id');
            //         window.location.href="{{ $module_url_path }}/milestones/release/"+release_id;
            //         return true;
            //     } 
            //     else 
            //     {
            //         return false;
            //     }
            // });
            swal({
                          title: "Are you sure? You want to send release request ?",
                          text: "",
                          icon: "warning",
                          
                          dangerMode: true,
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes, I am sure!',
                            cancelButtonText: "No",
                            closeOnConfirm: false,
                            closeOnCancel: false
                         },
                         function(isConfirm){

                           if (isConfirm){
                              var release_id = $(ref).attr('data-release-id');
                              window.location.href="{{ $module_url_path }}/milestones/release/"+release_id;
                                return true;

                            } else {
                                swal.close();
                              return false;
                            }
                         });
        }

        function confirmApprove(ref) {
            swal({
                          title: "Are you sure? You want to approve and release milestone ?",
                          text: "",
                          icon: "warning",
                          
                          dangerMode: true,
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes, I am sure!',
                            cancelButtonText: "No",
                            closeOnConfirm: false,
                            closeOnCancel: false
                         },
                         function(isConfirm){

                           if (isConfirm){
                              var release_details_id = $(ref).attr('data-release-details-id');
                              var currency           = "{{$projectInfo['project_currency_code']}}";
                              window.location.href="{{ $module_url_path }}/milestones/approve/"+release_details_id+"/"+currency;
                                return true;

                            } else {
                                swal.close();
                              return false;
                            }
                         });
        }

        function confirmNotApprove(ref) {

            swal({
                          title: "Are you sure? You want to Not approved milestone request?",
                          text: "",
                          icon: "warning",
                          
                          dangerMode: true,
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes, I am sure!',
                            cancelButtonText: "No",
                            closeOnConfirm: false,
                            closeOnCancel: false
                         },
                         function(isConfirm){

                           if (isConfirm){
                              var release_details_id = $(ref).attr('data-release-details-id');
                              window.location.href="{{ $module_url_path }}/milestones/not_approve/"+release_details_id;
                                return true;

                            } else {
                                swal.close();
                              return false;
                            }
                         });



        }





    </script>  

@stop
