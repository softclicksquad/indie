@extends('client.layout.master')                
@section('main_content')
<div class="col-sm-7 col-md-8 col-lg-9">
   <div class="right_side_section payment-section">
      <div class="head_grn">{{isset($page_title)?$page_title:''}}</div>
      @include('front.layout._operation_status')
      <div class="ongonig-project-section">
         <div class="dispute-head"  style="padding-top: 0px;">
            {{isset($arr_project['project_name'])?$arr_project['project_name']:''}}
         </div>
         <span><i class="fa fa-calendar" aria-hidden="true"></i></span> {{isset($arr_project['created_at'])?date('d M Y',strtotime($arr_project['created_at'])):''}}
         <div class="det-divider"></div>
         <div class="project-title">
         </div>
         <div class="clr"></div>
             @php $total_cost = 0; @endphp
             @if(isset($commission_cost))
               @php $total_cost += $commission_cost; @endphp
               <div class="project-list pro-list-ul">
                  <ul>
                     <li style="background:none;">
                          <span class="projrct-prce"> {{isset($arr_project['project_currency_code'])?$arr_project['project_currency_code']:''}}
                            {{isset($commission_cost)?number_format($commission_cost,2):'0'}} <span style="color:#737373;">(<i>{{trans('milestones/payment_methods.text_recruiter_payment_note')}}</i>)</span>
                          </span>
                     </li>
                  </ul>
               </div>
               <div class="clr"></div>
               <br/>
             @endif
             @if(isset($private_project_cost))
               @php $total_cost += $private_project_cost; @endphp
               <div class="project-list pro-list-ul">
                  <ul>
                     <li style="background:none;">
                          <span class="projrct-prce"> {{isset($arr_project['project_currency_code'])?$arr_project['project_currency_code']:''}}
                            {{isset($private_project_cost)?number_format($private_project_cost,2):'0'}} <span style="color:#737373;">( <i> For Private project cost </i>)</span>
                          </span>
                     </li>
                  </ul>
               </div>
               <div class="clr"></div>
               <br/>
             @endif
             @if(isset($nda_project_cost))
               @php $total_cost += $nda_project_cost; @endphp
               <div class="project-list pro-list-ul">
                  <ul>
                     <li style="background:none;">
                          <span class="projrct-prce"> {{isset($arr_project['project_currency_code'])?$arr_project['project_currency_code']:''}}
                            {{isset($nda_project_cost)?number_format($nda_project_cost,2):'0'}} <span style="color:#737373;">(<i> For NDA project cost </i>)</span>
                          </span>
                     </li>
                  </ul>
               </div>
               <div class="clr"></div>
               <br/>
             @endif
             @if(isset($highlight_days_cost))
               @php $total_cost += $highlight_days_cost; @endphp
               <div class="project-list pro-list-ul">
                  <ul>
                     <li style="background:none;">
                          <span class="projrct-prce"> {{isset($arr_project['project_currency_code'])?$arr_project['project_currency_code']:''}}
                            {{isset($highlight_days_cost)?number_format($highlight_days_cost,2):'0'}} <span style="color:#737373;">(<i> For Highlight project cost </i>)</span>
                          </span>
                     </li>
                  </ul>
               </div>
               <div class="clr"></div>
               <br/>
             @endif
             @if(isset($project_type_urgent))
               @php $total_cost += $project_type_urgent; @endphp
               <div class="project-list pro-list-ul">
                  <ul>
                     <li style="background:none;">
                          <span class="projrct-prce"> {{isset($arr_project['project_currency_code'])?$arr_project['project_currency_code']:''}}
                            {{isset($project_type_urgent)?number_format($project_type_urgent,2):'0'}} <span style="color:#737373;">(<i> For urgent project cost </i>)</span>
                          </span>
                     </li>
                  </ul>
               </div>
               <div class="clr"></div>
               <br/>
             @endif
             <div class="project-list pro-list-ul">
                <ul>
                   <li style="background:none;">
                        <span class="projrct-prce"> <b>Total :</b> {{isset($arr_project['project_currency_code'])?$arr_project['project_currency_code']:''}}
                          {{isset($total_cost)?number_format($total_cost,2):'0'}}
                        </span>
                   </li>
                </ul>
             </div>
            <div class="clr"></div>
            <br/>

            <form id="validate-form-payment" name="validate-form-payment" method="POST" action="{{url('/payment/paynow')}}">
                {{ csrf_field() }}
                <input type="hidden" name="payment_obj_id" id="payment_obj_id" value=" {{isset($arr_project['id'])?$arr_project['id']:'0'}}">
                <input type="hidden" name="transaction_type" id="transaction_type" value="5">
                {{-- <input type="hidden" name="project_services_cost" id="project_services_cost" value="{{isset($total_pay_amount)?number_format($total_pay_amount,2):'0'}}"> --}}
                <input type="hidden" name="project_services_cost" id="project_services_cost" value="{{isset($total_cost)?number_format($total_cost,2):'0'}}">
                <input type="hidden" name="project_services_type" id="project_services_type" value="{{isset($services_string)?$services_string:''}}">
                <input type="hidden" name="is_payment_by_card" id="is_payment_by_card" value="0">
                <input type="hidden" name="card_currency" id="card_currency" value="">

                <!-- Payment Methods --> 
                   <div class="paypa_radio">
                    
                    @if(isset($mangopay_wallet_details) && count($mangopay_wallet_details)>0)
                    @foreach($mangopay_wallet_details as $key=>$value)
                     <!-- Wallet -->
                     
                     {{-- @if(isset($value->Balance->Currency) && $value->Balance->Currency==$contest_currency) --}}
                         <div class="radio_area regi" onclick="javascript: return setOption('wallet');">
                            <input type="radio" name="payment_method" id="radio_wallet{{$key}}" class="css-checkbox" checked="" data-amount={{number_format($value->Balance->Amount/100,2)}} value="3" data-currency="{{ isset($value->Balance->Currency)? $value->Balance->Currency:''}}">
                            <label for="radio_wallet{{$key}}" class="css-label radGroup1">
                              <img style="width: 292px;height: 57px;margin-top:-20px;" src="{{url('/public')}}/front/images/archexpertdefault/arc-hexpert-wallet-logo.png" alt="wallet payment method"/> 
                              <i> ( {{trans('common/wallet/text.text_money_balance')}} : {{ isset($value->Balance->Amount)? number_format($value->Balance->Amount/100,2):'0'}} {{ isset($value->Balance->Currency)? $value->Balance->Currency:''}}) </i>
                            </label>
                         </div> 
                         <div class="clearfix"><hr style="width: 100%;margin-left: 2px;"></div>
                     <!-- End Wallet -->
                     
                     @endforeach
                     @endif
                    
                    <input type="hidden" name="currency_code" id="currency_code" value="">

                    <span class='error' id="payment_method_err">{{$errors->first('payment_method')}}</span>
                  </div>
                <!-- Payment Methods -->

                @if( (isset($wdallet_amount) && isset($total_payment_amount)) && ($total_payment_amount > $wallet_amount) && isset($mangopay_cards_details) && count($mangopay_cards_details)>0 && !empty($mangopay_cards_details))
                    <div class="pay-by-cards" {{-- style="padding-left: 5%" --}}>
                      <span class="projrct-prce"> <span style="color:#737373;"><i>Your saved credit card</i></span></span>
                      <div class="clearfix"><hr style="width: 100%;margin-left: 2px;"></div>
                      @foreach($mangopay_cards_details as $card_key => $card_value)
                        @php
                          $card_alias = $expiration_date = '';
                          if(isset($card_value->Alias) && $card_value->Alias!='') {
                              $arr_card_alias = explode('XXXXXX',$card_value->Alias);
                              $card_alias = isset($arr_card_alias[1])  ? $arr_card_alias[1] : '';
                          }
                          if(isset($card_value->ExpirationDate) && $card_value->ExpirationDate!='') {
                              $expiration_date = implode('/', str_split($card_value->ExpirationDate, 2));
                          }
                        @endphp
                        <div class="radio_area regi">
                              <input type="radio" onclick="javascript: return setIsPaymentByCard(this);"
                                     name="card_id" 
                                     id="radio_card_{{$card_value->Id}}" 
                                     class="css-checkbox" 
                                     value="{{$card_value->Id}}"
                                     data-card-currency="{{$card_value->Currency}}"
                                     data-is-payment-by-card="1"
                                     >
                              <label for="radio_card_{{$card_value->Id}}" class="css-label radGroup1">
                                <i> {{ isset($card_value->CardProvider)? $card_value->CardProvider: '' }} Card, ends with {{ isset($card_alias)? $card_alias: '' }} , expiry date {{ isset($expiration_date)? $expiration_date: '' }} </i>
                              </label>
                        </div> 
                        <div class="clearfix"><hr style="width: 100%;margin-left: 2px;"></div>
                      @endforeach
                        <div class="radio_area regi">
                              <input type="radio" onclick="javascript: return setIsPaymentByCard(this);"
                                     name="card_id" 
                                     id="radio_card_skip" 
                                     class="css-checkbox" 
                                     value=""
                                     data-card-currency=""
                                     data-is-payment-by-card="0"
                                     checked="" 
                                     >
                              <label for="radio_card_skip" class="css-label radGroup1">
                                <i> Payment by a different card </i>
                              </label>
                        </div> 
                        <div class="clearfix"><hr style="width: 100%;margin-left: 2px;"></div>
                    </div>
                @endif

                <div class="pay-amt-details-main">
                    @if( (isset($wallet_amount) && isset($total_payment_amount)) && ($total_payment_amount > $wallet_amount))
                      <div class="pay-amt-details">
                          <div class="total-amt-txt-section">
                              Total Prize: 
                          </div>
                          <div class="total-amt-value-section">
                              {{ isset($arr_project['project_currency_code'])?$arr_project['project_currency_code']:'' }} {{ isset($total_payment_amount) ? number_format($total_payment_amount,2) : '0.0' }}
                          </div>
                      </div>
                      <div class="pay-amt-details">
                          <div class="total-amt-txt-section">
                              Wallet Balance: 
                          </div>
                          <div class="total-amt-value-section">
                              {{ isset($arr_project['project_currency_code'])?$arr_project['project_currency_code']:'' }}  {{ isset($wallet_amount) ? number_format($wallet_amount,2) : '0.0' }}
                          </div>
                      </div>
                      <div class="pay-amt-details net-total-section">
                          <div class="total-amt-txt-section">
                              Sub Total: 
                          </div>
                          <div class="total-amt-value-section">
                              {{ isset($arr_project['project_currency_code'])?$arr_project['project_currency_code']:'' }} {{ isset($sub_total_payment_amount) ? number_format($sub_total_payment_amount,2) : '0.0' }}
                          </div>
                      </div>
                      <div class="pay-amt-details">
                          <div class="total-amt-txt-section">
                              Service Charges: 
                          </div>
                          <div class="total-amt-value-section">
                              {{ isset($arr_project['project_currency_code'])?$arr_project['project_currency_code']:'' }} {{ isset($service_charge) ? number_format($service_charge,2) : '0.0' }}
                          </div>
                      </div>

                      <div class="pay-amt-details net-total-section">
                          <div class="total-amt-txt-section">
                              Total: 
                          </div>
                          <div class="total-amt-value-section">
                              {{ isset($arr_project['project_currency_code'])?$arr_project['project_currency_code']:'' }} {{ isset($total_pay_amount) ? number_format($total_pay_amount,2) : '0.0' }} 
                          </div>
                      </div>
                    @else
                      <div class="pay-amt-details net-total-section">
                        <div class="total-amt-txt-section">
                            Total: 
                        </div>
                        <div class="total-amt-value-section">
                            {{ isset($arr_project['project_currency_code'])?$arr_project['project_currency_code']:'' }} {{ isset($total_pay_amount) ? number_format($total_pay_amount,2) : '0.0' }} (Amount will be charged from the wallet)
                        </div>
                      </div>
                    @endif
                </div>

                <br/>
                <div class="clr"></div>
                <div class="right-side">
                   <button class="normal-btn pull-right" value="submit" id="btn-form-payment-submit">{{trans('milestones/payment_methods.text_processed')}}</button>
                </div>
            </form>
         <div class="clr"></div>
      </div>
   </div>
</div>
<script type="text/javascript" src="{{url('/public')}}/assets/payment/jquery.payment.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript">
   
   function setIsPaymentByCard(ref){
      $('#card_currency').val($(ref).attr('data-card-currency'));
      $('#is_payment_by_card').val($(ref).attr('data-is-payment-by-card'));
   }

   $(document).ready(function(){
       var is_stripe = jQuery('#radio_stripe').prop('checked');
       if (is_stripe==true){
         jQuery('#section-card-details').show();
       } else {
         jQuery('#section-card-details').hide();
       }
   });
   $('input[name=cardNumber]').payment('formatCardNumber');
   $('input[name=cardCVC]').payment('formatCardCVC');
   jQuery("#validate-form-payment").validate({
      errorElement: 'span',   
   });
   $('#btn-form-payment-submit').click(function (){

      var total_cost     = '{{$total_cost}}';
      var wallet_amount  = $("input[name='payment_method']:checked").attr('data-amount');
      var currency_code  = $("input[name='payment_method']:checked").attr('data-currency');
      $('#currency_code').val(currency_code);
      
      $('#payment_method_err').html('');

      var check_currency = $('#currency_code').val();
      if(check_currency=='' || check_currency==null)
      {
        $("#payment_method_err").html('Sorry, Please select payment method.');
        return false;
      }


      $('#payment_method_err').html('');
      if($('#radio_wallet').is(':checked')) {
       if(total_cost=='0' || total_cost=='0.0' || total_cost=='0.00'){
         $("#payment_method_err").html('Sorry, total cost should not 0USD.');
         return false;
       }
       if(parseFloat(total_cost)>parseFloat(wallet_amount)){
         $("#payment_method_err").html('Sorry, Your wallet amount is not suffeciant to make transaction.');
         return false;
       } 
      }
      /*jQuery('#btn-form-payment-submit').prop('disabled', true);*/
   });
   function setOption(opt){
       $('#payment_method_err').html('');
       if (opt=='stripe'){
         jQuery('#section-card-details').show();
         jQuery('#radio_stripe').prop('checked',true);
       } else if (opt=='paypal'){
         jQuery('#section-card-details').hide();
         jQuery('#radio_paypal').prop('checked',true);
       } else if (opt=='wallet'){
         jQuery('#section-card-details').hide();
         jQuery('#radio_wallet').prop('checked',true);
       }
   }
   $('#cardExpiryYear').keypress(function(eve) 
   {
      if(eve.which == 8 || eve.which == 190) 
      {
        return true;
      } else if (eve.which != 8 && eve.which != 0 && (eve.which < 48 || eve.which > 57)) {
       eve.preventDefault();
      }
      if($('#cardExpiryYear').val().length>3){
         eve.preventDefault();
      }
   });
   $('#cardExpiryMonth').keypress(function(eve) 
   {
      if(eve.which == 8 || eve.which == 190) {
        return true;
      } else if (eve.which != 8 && eve.which != 0 && (eve.which < 48 || eve.which > 57)) {
       eve.preventDefault();
      }
      if($('#cardExpiryMonth').val().length>1){
         eve.preventDefault();
      }
   });
</script>
<script type="text/javascript">
  (function (global, $) {
    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";
        global.setTimeout(function () 
        {
            global.location.href += "!";
        }, 50);
    };
    global.onhashchange = function () {
        if (global.location.hash != _hash) {
            global.location.hash = _hash;
        }
    };
    global.onload = function () 
    {
        noBackPlease();
        // disables backspace on page except on input fields and textarea..
        $(document.body).keydown(function (e) {
            var elm = e.target.nodeName.toLowerCase();
            if (e.which == 8 && (elm !== 'input' && elm  !== 'textarea')) 
            {
                e.preventDefault();
            }
            // stopping event bubbling up the DOM tree..
            e.stopPropagation();
        });
    };
  })(window, jQuery || window.jQuery);
</script>
@stop