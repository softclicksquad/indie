@extends('client.layout.master')                
@section('main_content')
<div class="col-sm-7 col-md-8 col-lg-9">
      <div class="col-sm-7 col-md-8 col-lg-12">
         <div class="right_side_section">
            @include('front.layout._operation_status')
            <div class="head_grn">{{trans('client/projects/open_projects.text_open_project_title')}}</div>
            @if(isset($arr_open_projects['data']) && sizeof($arr_open_projects['data'])>0)
            @foreach($arr_open_projects['data'] as $proRec)
            <div class="ongonig-project-section client-dash">
               <div class="project-title">
                  <a href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}">
                     <h3>{{$proRec['project_name']}}</h3>
                  </a>
                  <div class="sub-project-dec">{{trans('client/projects/open_projects.text_project_duration')}}:&nbsp;{{$proRec['project_expected_duration']}}
                     &nbsp;&nbsp;&nbsp;&nbsp;
                     {{trans('client/projects/open_projects.text_bids')}}:&nbsp;{{$proRec['bid_count'] or '0'}}</div>
                  
                  <div class="more-project-dec">{{str_limit($proRec['project_description'],340)}}</div>
               </div>
               <div class="row">
                  <div class="col-sm-12 col-md-1 col-lg-1 skills-de-new"><span class="colrs">{{trans('client/projects/open_projects.text_skills')}}:</span></div>
                  <div class="col-sm-12 col-md-9 col-lg-11 skill-de-content">
                     <div class="skils-project">
                        <ul>
                           @if(isset($proRec['project_skills'])  && count($proRec['project_skills']) > 0)
                           @foreach($proRec['project_skills'] as $key=>$skills) 
                           @if(isset($skills['skill_data']['skill_name']) &&  $skills['skill_data']['skill_name'] != "")
                           <li style="font-size:12px; " >{{ str_limit($skills['skill_data']['skill_name'],25) }}</li>
                           @endif
                           @endforeach
                           @endif    
                        </ul>
                     </div>
                  </div>

                  <div class="col-md-12" style="margin-top: 10px;">
                        
                      {{--   <a href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}" class="view_btn hidden-xs hidden-sm hidden-md">
                        <i class="fa fa-eye" aria-hidden="true"></i> {{trans('client/projects/open_projects.text_view')}} 
                        </a>
                        
                        <a onclick="confirmDelete(this)" data-project-id="{{ isset($proRec['id'])?base64_encode($proRec['id']):'' }}" >
                        <button class="view_btn pull-right" style="margin-right:10px; ">
                        Cancel 
                        </button>
                        </a> --}}

                        <a onclick="confirmDelete(this)" style="cursor: pointer;" data-project-id="{{ isset($proRec['id'])?base64_encode($proRec['id']):'' }}" class="view_btn hidden-xs hidden-sm hidden-md">
                           {{trans('client/projects/open_projects.text_cancel')}} 
                        </a>
                        
                        <a href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}" >
                        <button class="view_btn pull-right" style="margin-right:10px; ">
                        <i class="fa fa-eye" aria-hidden="true"></i>&nbsp;{{trans('client/projects/open_projects.text_view')}} 
                        </button>
                        </a>

                  </div>
               </div>
            </div>
            <hr/>
            @endforeach
            @else
            <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;">
               <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                  <div class="search-content-block">
                     <div class="search-head" style="color:rgba(45, 45, 45, 0.8);">
                        {{trans('client/projects/open_projects.text_no_record_found')}}
                     </div>
                  </div>
               </td>
            </tr>
            @endif
         </div>
         <!-- Paination Links -->
         @include('front.common.pagination_view', ['paginator' => $arr_pagination])
         <!-- Paination Links -->
      </div>
</div>

<script type="text/javascript">
   function confirmDelete(ref) {
      alertify.confirm("Are you sure? You want to cancel project ?", function (e) {
          var project_id = $(ref).attr('data-project-id');
          if (e) {
              window.location.href="{{ $module_url_path }}"+"/cancel_project/"+project_id;
              return true;
          } else {
              return false;
          }
      });
   }
</script>
@stop