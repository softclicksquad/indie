@extends('client.layout.master') @section('main_content')
<style type="text/css">
    .moretext {
        color: rgb(0, 0, 0);
        text-decoration: none;
        text-transform: capitalize;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" integrity="sha256-Z8TW+REiUm9zSQMGZH4bfZi52VJgMqETCbPFlGRB1P8=" crossorigin="anonymous" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.js" integrity="sha256-ZvMf9li0M5GGriGUEKn1g6lLwnj5u+ENqCbLM5ItjQ0=" crossorigin="anonymous"></script>
<div class="col-sm-7 col-md-8 col-lg-9">
    <div class="row">
        <div class="col-lg-8">
            @include('front.layout._operation_status')
            <div class="head_grn">{{ trans('common/client_sidebar.text_posted_projects') }}</div>
        </div>
        <div class="col-lg-4">
            <div class="droup-select">
                <select class="droup mrns tp-margn" onchange="sortByHandledBy(this)">
                    <option value="">-- Sort By --</option>
                    <option @if(isset($sort_by) && $sort_by=='asc' ) selected="" @endif value="asc">Start Date Ascending</option>
                    <option @if(isset($sort_by) && $sort_by=='desc' ) selected="" @endif value="desc">Start Date Descending</option>
                    <option @if(isset($sort_by) && $sort_by=='bids-asc' ) selected="" @endif value="bids-asc">Recevied Bids Ascending</option>
                    <option @if(isset($sort_by) && $sort_by=='bids-desc' ) selected="" @endif value="bids-desc">Recevied Bids Descending</option>
                    <option @if(isset($sort_by) && $sort_by=='budget-asc' ) selected="" @endif value="budget-asc">Budget Ascending</option>
                    <option @if(isset($sort_by) && $sort_by=='budget-desc' ) selected="" @endif value="budget-desc">Budget Descending</option>
                    <option @if(isset($sort_by) && $sort_by=='is_urgent' ) selected="" @endif value="is_urgent">Urgent</option>
                    {{-- <option @if(isset($sort_by) && $sort_by=='public_type' ) selected="" @endif value="public_type">Public</option> --}}
                    <option @if(isset($sort_by) && $sort_by=='private_type' ) selected="" @endif value="private_type">Private</option>
                    <option @if(isset($sort_by) && $sort_by=='is_nda' ) selected="" @endif value="is_nda">NDA</option>
                </select>
            </div>
        </div>
        <div class="clearfix"></div>

        @if(isset($diffInSeconds))
        @php $diffInSeconds = $diffInSeconds; @endphp
        @else
        @php $diffInSeconds ='0'; @endphp
        @endif

        @if(isset($posted_projects['data']) && sizeof($posted_projects['data'])>0)
        @foreach($posted_projects['data'] as $proRec)
        <div class="col-lg-12">
            <div class="white-block-bg-no-padding">
                @if(isset($proRec['highlight_end_date']) &&
                $proRec['highlight_end_date'] !="" &&
                $proRec['highlight_end_date'] !=null &&
                $proRec['highlight_end_date'] !="0000-00-00 00:00:00" &&
                date('Y-m-d', strtotime($proRec['highlight_end_date'])) >= date('Y-m-d') )
                <div class="white-block-bg big-space hover hover highlite-bx">
                    @else
                    <div class="white-block-bg big-space hover">
                        @endif
                        <div class="project-left-side">

                            
                            <div class="batches">
                                @if($proRec['project_handle_by'] == '2')
                                    <span class="featured">Recruiter</span>
                                @endif

                                @if(isset($proRec['highlight_end_date']) &&
                                $proRec['highlight_end_date'] !="" &&
                                $proRec['highlight_end_date'] !=null &&
                                $proRec['highlight_end_date'] !="0000-00-00 00:00:00" &&
                                date('Y-m-d', strtotime($proRec['highlight_end_date'])) >= date('Y-m-d') )
                                <!-- <span class="private-active">HIGHLIGHTED</span> -->
                                <span class="private hightlight">HIGHLIGHTED</span>
                                @endif
                                <!-- 0 = no-nda / 1 = nda -->
                                @if(isset($proRec['is_nda']) && $proRec['is_nda'] == 1)
                                <span class="private nda">NDA</span>
                                @endif

                                <!-- 0 = no-urgent / 1 = urgent -->
                                @if(isset($proRec['is_urgent']) && $proRec['is_urgent'] == 1)
                                <!-- <span class="private">Urgent</span> -->
                                @endif

                                <!-- 0 = public / 1 = privare -->
                                @if(isset($proRec['project_type']) && $proRec['project_type'] == 1)
                                <span class="private pri-div">Private</span>
                                @endif

                            </div>

                            @if(isset($proRec['urjent_heighlight_end_date']) &&
                            $proRec['urjent_heighlight_end_date'] !="" &&
                            $proRec['urjent_heighlight_end_date'] !=null &&
                            $proRec['urjent_heighlight_end_date'] !="0000-00-00 00:00:00" &&
                            date('Y-m-d H:i:s', strtotime($proRec['urjent_heighlight_end_date'])) >= date('Y-m-d H:i:s') )
                            <div class="promoted-label">URGENT</div>
                            @endif
                            <div class="search-freelancer-user-head">
                                @if($proRec['is_hire_process'] == 'YES')
                                <span class="pull-right"><i>{{trans('project_listing/listing.text_posted_by_hired_process')}}</i></span>
                                @endif
                                <a href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}"> {{$proRec['project_name'] or '-'}} </a>

                                <input type="hidden" id="is_nda_{{$proRec['id']}}" name="is_nda_{{$proRec['id']}}" value="{{$proRec['is_nda']}}">
                                <input type="hidden" id="is_urgent_{{$proRec['id']}}" name="is_urgent_{{$proRec['id']}}" value="{{$proRec['is_urgent']}}">
                                <input type="hidden" id="is_recruiter_{{$proRec['id']}}" name="is_recruiter_{{$proRec['id']}}" value="{{$proRec['is_recruiter']}}">

                                
                            </div>
                            <div class="search-freelancer-user-location">
                                <span class="gavel-icon">
                                        <img src="{{url('/public')}}/front/images/bid.png" alt="" />
                                </span> 
                                <span class="dur-txt"> {{trans('client/projects/postedprojects.text_bids')}}:&nbsp;
                                    @if(isset($proRec['bid_count']) && $proRec['bid_count']>0)
                                        <span style="color: green;">{{$proRec['bid_count'] or '0'}}</span>
                                    @else
                                        <span>{{$proRec['bid_count'] or '0'}}</span>
                                    @endif
                                </span> 
                                <span class="freelancer-user-line">|</span>
                            </div>

                            @php $postes_time_ago = time_ago($proRec['created_at']); @endphp
                            
                            <div class="search-freelancer-user-location"> 
                                <span class="gavel-icon"><img src="{{url('/public')}}/front/images/clock.png" alt="" /></span> 
                                <span class="dur-txt"> {{trans('project_listing/listing.text_project_posted_on')}}: {{$postes_time_ago}}</span> 
                                <span class="freelancer-user-line">|</span>
                            </div>

                            @if(isset($proRec['edited_at']) && $proRec['edited_at']!=NULL)
                                @php $edited_time_ago = time_ago($proRec['edited_at']); @endphp
                                <div class="search-freelancer-user-location">
                                    <span class="gavel-icon"><img src="{{url('/public')}}/front/images/clock.png" alt="" /></span>
                                    <span class="dur-txt">Last edited : {{$edited_time_ago}}</span>
                                    <span class="freelancer-user-line">|</span>
                                </div>
                            @endif

                            <div class="search-freelancer-user-location"> 
                                <span class="gavel-icon">
                                    <img src="{{url('/public')}}/front/images/calendar.png" alt="" />
                                </span> 
                                <span class="dur-txt"> Open for Bids: 
                                    <span id='bids_left_timer{{$proRec["id"]}}' style="margin-top:0px;">-</span>
                                </span>



                            </div>

                            <div class="clearfix"></div>
                            <div class="more-project-dec">{{str_limit($proRec['project_description'],170)}}
                                @if(strlen(trim($proRec['project_description'])) > 170)
                                <a class="moretext" href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}">
                                    {{trans('project_manager/projects/postedprojects.text_more')}}
                                </a>
                                @endif
                            </div>

                            <div class="category-new">
                                <img src="{{url('/public')}}/front/images/tag-2.png" alt="" /> {{$proRec['category_details']['category_title'] or 'NA'}}

                                &nbsp;&nbsp;
                                <img src="{{url('/public')}}/front/images/tag-2.png" alt="" /> {{isset($proRec['sub_category_details']['subcategory_title'])?$proRec['sub_category_details']['subcategory_title']:'NA'}}

                            </div>
                            <div class="clearfix"></div>
                            @if(isset($proRec['project_skills']) && count($proRec['project_skills']) > 0)
                            <div class="skils-project">
                                <img src="{{url('/public')}}/front/images/tag.png" alt="" />
                                <ul>
                                    @foreach($proRec['project_skills'] as $key=> $skills)
                                    @if(isset($skills['skill_data']['skill_name']) && $skills['skill_data']['skill_name'] != "")
                                    <!-- <a onclick="searchBySkill(this)" style="cursor: pointer;" data-skill-name="{{$skills['skill_data']['skill_name']}}" data-skill-id="{{ isset($skills['skill_id'])? base64_encode($skills['skill_id']):''}}" > -->


                                    <li>{{ str_limit($skills['skill_data']['skill_name'],25) }}
                                        @if($key!=count($proRec['project_skills'])-1)
                                        ,
                                        @endif
                                    </li>


                                    @endif
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                        </div>
                        <div class="search-project-listing-right contest-right">
                            <div class="search-project-right-price">
                                <?php
                                  $pricing_method = $text_hour = "";
                                  if(isset($proRec['project_pricing_method']) && $proRec['project_pricing_method'] == '1')
                                  {
                                    $pricing_method = trans('project_listing/listing.text_budget');
                                  }
                                  else if(isset($proRec['project_pricing_method']) && $proRec['project_pricing_method'] == '2')
                                  {
                                    $pricing_method = trans('project_listing/listing.text_hourly_rate'); 
                                    $text_hour = trans('common/_top_project_details.text_hour');
                                  }

                                    $projects_cost = explode('-',$proRec['project_cost']);
                                    if( (isset($projects_cost[0]) && $projects_cost[0]!='') && (isset($projects_cost[1]) && $projects_cost[1]!=''))
                                    {
                                        $projects_cost = number_format(floor($projects_cost[0])).'-'.number_format(floor($projects_cost[1]));
                                    }
                                    elseif($projects_cost[0]!='')
                                    {
                                        $projects_cost = number_format(floor($projects_cost[0]));
                                    }
                          
                                ?>
                                {{isset($proRec['project_currency'])?$proRec['project_currency']:'$'}} {{isset($projects_cost)?$projects_cost:'0'}}{{$text_hour}}
                                <span>{{$pricing_method or ''}}</span>
                            </div>
                            @if($proRec['is_hire_process'] == 'NO')

                            <div class="search-project-right-price">
                                {{-- <span id='bids_left_timer{{$proRec["id"]}}' style="margin-top:0px;">-</span><span id="status{{$proRec[" id"]}}"></span> --}}
                                <?php 

                                      $arr_diff_between_two_date = get_array_diff_between_two_date(date('Y-m-d H:i:s'),$proRec['bid_closing_date']);
                                      $str_days_diff = isset($arr_diff_between_two_date['str_days_diff']) ? $arr_diff_between_two_date['str_days_diff'] : '';
                                      
                                      $is_one_day_left = '0';
                                      if( isset($arr_diff_between_two_date['years']) &&
                                          isset($arr_diff_between_two_date['months']) &&
                                          isset($arr_diff_between_two_date['days']) &&
                                          ($arr_diff_between_two_date['years'] == 0) &&
                                          ($arr_diff_between_two_date['months'] == 0) &&
                                          ($arr_diff_between_two_date['days'] == 0)
                                        ){
                                        $is_one_day_left = '1';
                                      }
                                      $timeFirst      = strtotime(date('Y-m-d H:i:s'));
                                      $timeSecond     = strtotime($proRec['bid_closing_date']);
                                      $diffInSeconds = $timeSecond - $timeFirst;

                                      if($diffInSeconds <= 0)
                                      {
                                        $diffInSeconds = '0';
                                      }

                                  ?>
                                <script type="text/javascript">
                                    var projectid = '<?php echo $proRec["id"]; ?>';
                                    var str_days_diff = '{{ isset($str_days_diff) ? $str_days_diff : '' }}';
                                    var is_one_day_left = '{{ isset($is_one_day_left) ? $is_one_day_left : '0' }}';
                                    if (str_days_diff == '') {
                                        $('#status' + projectid).html('');
                                        $('#bids_left_timer' + projectid).html('{{trans('client/projects/postedprojects.text_closed')}}');
                                        $('#bids_left_timer' + projectid).css('color', 'red');

                                    } else {
                                        $('#status' + projectid).html('Open for Bids');
                                        $('#bids_left_timer' + projectid).html(str_days_diff);
                                        if(is_one_day_left == '1'){
                                            $('#bids_left_timer' + projectid).css({"color":"red"});
                                        } else {
                                            $('#bids_left_timer' + projectid).css({"color":"green"});
                                        }
                                    }                                    
                                </script>
                            </div>
                            @endif
                            <div class="clearfix"></div>

                        </div>
                        <div class="invite-expert-btns">
                            @if($proRec['is_hire_process'] == 'NO')
                            @if($diffInSeconds >'0')
                            @if(isset($proRec['project_bid_info']['bid_status']) && $proRec['project_bid_info']['bid_status']!='4' || empty($proRec['project_bid_info']['bid_status']))
                            @if($proRec['project_handle_by'] == '1')
                            <a href="#invite_member" data-keyboard="false" data-backdrop="static" data-project-id="{{ base64_encode($proRec['id'])}}" data-toggle="modal" class="black-btn invite-expert" id="btn{{$proRec['id']}}">{{trans('client/projects/invite_experts.text_invite_expert')}}</a>
                            @endif
                            @endif
                            @endif
                            @endif
                            {{-- @if($proRec['project_status'] == '1' && $proRec['is_hire_process'] == 'NO')
                                    <a href="{{$module_url_path}}/edit/{{ base64_encode($proRec['id'])}}" class="black-border-btn">Edit</a>
                            @endif --}}
                            @if(isset($proRec['bid_count']) && $proRec['bid_count'] >'0' && $diffInSeconds >'0')
                            <a href="{{ $module_url_path }}/details/{{ base64_encode($proRec['id'])}}" data-keyboard="false" data-backdrop="static" data-project-id="{{ base64_encode($proRec['id'])}}" data-toggle="modal" class="black-btn invite-expert">{{trans('client/projects/invite_experts.text_check_bids')}}</a>
                            @elseif($proRec['bid_count'] >'0' && $diffInSeconds <='0' ) <a href=" {{ $module_url_path }}/details/ {{ base64_encode($proRec['id'])}}" class="awd_btn new-btn-class" style="cursor: default;>
                                <font color="white">{{trans("new_translations.text_award_job")}}</font>
                                </a>

                                @endif

                                <input type="hidden" name="project_description" id="project_description_{{$proRec['id']}}" value="{{$proRec['project_description']}}">
                                <input type="hidden" name="project_title" id="project_title_{{$proRec['id']}}" value="{{$proRec['project_name']}}">

                                @if(isset($diffInSeconds) && $diffInSeconds>0)
                                @if(isset($proRec['bid_count']) && $proRec['bid_count'] > '0')
                                <a onclick="openForm(this)" style="cursor: pointer;" data-project-id="{{ isset($proRec['id'])?base64_encode($proRec['id']):'' }}" class="black-btn">Add More Details</a>
                                @else
                                {{-- <a onclick="openUpdateDescriptionForm(this,'{{$proRec['id']}}')" style="cursor: pointer;" data-project-id="{{ isset($proRec['id'])?base64_encode($proRec['id']):'' }}" class="black-btn">Update Description</a> --}}

                                <a href="{{$module_url_path}}/edit/{{ base64_encode($proRec['id'])}}" class="black-border-btn">Edit</a>
                                @endif
                                @endif

                                @if($diffInSeconds <= '0' && $proRec['bid_count']=='0' ) <a class="black-btn invite-expert" href="{{$module_url_path}}/post/{{isset($proRec['id'])?base64_encode($proRec['id']):''}}"> Post Again </a>
                                    @endif
                                    @if($proRec['is_hire_process'] == 'NO')<a onclick="confirmDelete(this)" style="cursor: pointer;" data-project-id="{{ isset($proRec['id'])?base64_encode($proRec['id']):'' }}" data-is-nda="{{$proRec['is_nda']}}" data-is-urgent="{{$proRec['is_urgent']}}" data-highlight-days="{{$proRec['highlight_days']}}" data-project-type="{{$proRec['project_type']}}" data-handle-by="{{$proRec['project_handle_by']}}" class="black-border-btn" s{{-- tyle="margin" --}}>Cancel</a>
                                    @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
         

            @endforeach
            @include('front.common.pagination_view', ['paginator' => $arr_pagination])
            @else
            <div class="col-lg-12">
                <div class="search-grey-bx">
                    <div class="no-record">
                        {{trans('expert/projects/awarded_projects.text_sorry_no_record_found')}}
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
    <!-- Modal include-->
    @include('client.projects.invite_expert')
    <!-- model popup end here-->
    @include('client.projects.additional_info')


    <script type="text/javascript">
        function confirmDelete(ref) {
            var is_nda = $(ref).attr('data-is-nda');
            var is_urgent = $(ref).attr('data-is-urgent');
            var project_type = $(ref).attr('data-project-type');
            var handle_by = $(ref).attr('data-handle-by');
            var highlight_days = $(ref).attr('data-highlight-days');
            // var project_id = $(ref).attr('data-project-id');
            if (is_nda == '1' || is_urgent == '1' || project_type == '1' || handle_by != '1' || highlight_days > '0') {
                swal({
                        title: "Are you sure? You want to cancel project ?",
                        text: "If you cancel this job, you will not receive any refund for services booked while posting this job!",
                        icon: "warning",

                        dangerMode: true,
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, I am sure!',
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm) {

                        if (isConfirm) {
                            var project_id = $(ref).attr('data-project-id');
                            window.location.href = "{{ $module_url_path }}" + "/cancel_project/" + project_id;
                            return true;

                        } else {
                            swal.close();
                            return false;
                        }
                    });
            } else {

                swal({
                        title: "Are you sure? You want to cancel project ?",
                        text: "If you cancel this job, you will not receive any refund for services booked while posting this job!",
                        icon: "warning",

                        dangerMode: true,
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, I am sure!',
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm) {

                        if (isConfirm) {
                            var project_id = $(ref).attr('data-project-id');
                            window.location.href = "{{ $module_url_path }}" + "/cancel_project/" + project_id;
                            return true;

                        } else {
                            swal.close();
                            return false;
                        }
                    });
            }
        }

        function sortByHandledBy(ref) {
            var sort_by = $(ref).val();
            if (sort_by != "") {
                window.location.href = "{{ url('client/projects/posted') }}" + "?sort_by=" + sort_by;
            } else {
                window.location.href = "{{ url('client/projects/posted') }}";
            }
            return true;
        }
    </script>

    <style type="text/css">
        .moretext {
            color: rgb(0, 0, 0);
            text-decoration: none;
            text-transform: capitalize;
        }

        /* Button used to open the contact form - fixed at the bottom of the page */
        .open-button {
            background-color: #555;
            color: white;
            padding: 16px 20px;
            border: none;
            cursor: pointer;
            opacity: 0.8;
            position: fixed;
            bottom: 23px;
            right: 28px;
            width: 280px;
        }

        /* The popup form - hidden by default */
        .form-popup {
            display: none;
            position: fixed;
            /*bottom: 0;*/
            /*right: 15px;*/
            overflow: hidden;
            border: 3px solid #f1f1f1;
            z-index: 9;
            top: 50%;
            left: 50%;
            margin-right: -50%;
            transform: translate(-50%, -50%)
        }

        /* Add styles to the form container */
        .form-container {
            max-width: 300px;
            padding: 10px;
            background-color: white;
            border-style: solid;
            border-width: thin;
            border-color: #2d2d2d;
            /*#a4a4a5, #ddddde*/
        }

        /* Full-width input fields */
        .form-container input[type=text],
        .form-container input[type=password] {
            width: 100%;
            padding: 15px;
            margin: 5px 0 22px 0;
            border: none;
            background: #f1f1f1;
        }


        /* When the inputs get focus, do something */
        .form-container input[type=text]:focus,
        .form-container input[type=password]:focus {
            background-color: #ddd;
            outline: none;
        }

        /* Set a style for the submit/login button */
        .form-container .btn {
            /*  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  width: 100%;
  margin-bottom:10px;
  opacity: 0.8;*/
            width: 99%;
        }

        /* Add a red background color to the cancel button */
        .form-container .cancel {
            width: 99%;
        }

        /* Add some hover effects to buttons */
        .form-container .btn:hover,
        .open-button:hover {
            opacity: 1;
        }

        .update-description {
            padding: 7px;
            resize: vertical;
            max-height: 370px;
        }

        .btn-space {
            margin-bottom: 4px;
        }
    </style>
    <div class="form-popup" id="update-info-form">
        <form class="form-container">
            <center>
                <h3>Update Description</h3>
            </center>
            Project Name : <label for="contestnm" id="project_title" style="margin-bottom: 10px;"><b>&nbsp;</b></label>
            <textarea data-rule-required="true" placeholder="Enter more details" id="update_description" name="update_description" class="update-description btn-space" rows="10" cols="35" style="margin-bottom: 10px;"></textarea>
            <span id="update_description_error" style="color: red;"></span>
            <button type="button" class="black-border-btn btn btn-space" id="sbt-update-info" value=""> <span id="info-sbt-btn-text">Update</span> Description</button>
            <input type="hidden" name="project_id" id="project_id">
            <button type="button" class="black-border-btn btn cancel btn-space" onclick="closeUpdateDescriptionForm()">Cancel</button>
        </form>
    </div>
    <script>
        $('.add-info-btn').click(function() {
            btn_id = $(this).attr("id");
            contest_id = $('.contest_id_' + btn_id).val();
            contest_nm = $('.contest_name_' + btn_id).val();
            contest_info = $('.contest_info_' + btn_id).val();
            if (contest_info != '') {
                $('#sbt-update-info').text('Update');
            }
            $('#sbt-update-info').attr('value', contest_id);
            $('#project_title').html(contest_nm);
            $('#update_description').val(contest_info);
        })

        $('#sbt-update-info').click(function() {

            $('#update_description_error').html('');
            btn_id = $(this).attr("id");
            project_id = $('#project_id').val();
            project_add_info = $('#update_description').val();
            token = '{{csrf_token()}}';

            if (project_add_info == '') {
                $('#update_description_error').html('This field is required');
            } else {
                $.ajax({
                    url: '{{$module_url_path}}/update_description',
                    type: "POST",
                    data: {
                        project_id: project_id,
                        _token: token,
                        project_add_info: project_add_info
                    },
                    success: function(res) {
                        //$('.cd-top').click();
                        location.reload();
                    }
                })
            }
        })

        function openUpdateDescriptionForm(ref, id) {

            var project_id = $(ref).attr('data-project-id');
            var project_description = $('#project_description_' + id).val();
            var project_title = $('#project_title_' + id).val();

            $('#project_id').val(project_id);
            $('#update_description').val(project_description);
            $('#project_title').html(project_title);
            document.getElementById("update-info-form").style.display = "block";
        }

        function closeUpdateDescriptionForm() {
            $('.update_description').val('');
            document.getElementById("update-info-form").style.display = "none";
        }
    </script>
    @stop