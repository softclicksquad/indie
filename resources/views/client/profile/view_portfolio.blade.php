@extends('front.layout.master')                
@section('main_content') 
<div class="middle-container">
   <div class="container">
      <br/>
      <?php 
         $sidebar_information= array();
         if (isset($arr_client['user_id'])){
            $sidebar_information = sidebar_information($arr_client['user_id']);
         }
      ?>
      <div class="row">
         <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="search-grey-bx">
               <div class="head_grn">{{ trans('expert/portfolio/portfolio.text_profile') }}</div>
               <div class="row">
                  <div class="col-sm-12 col-md-12 col-lg-12">
                     <div class="profil-man">
                        <div class="row">
                           <div class="col-sm-3 col-md-3 col-lg-3">
                              <div class="user-profile">
                                 <div class="profile-circle">

                                    @if(isset($user_auth_details['is_online']) && $user_auth_details['is_online']!='' && $user_auth_details['is_online'] == '1')
                                    <div class="user-online-round"></div>
                                    @else
                                    <div class="user-offline-round"></div>
                                    @endif

                                    <div class="edit-hover">
                                       <li><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></li>
                                    </div>
                                    @if(isset($profile_img_public_path) && isset($arr_client['profile_image']) && $arr_client['profile_image']!=NULL)
                                      <img src="{{$profile_img_public_path.$arr_client['profile_image']}}" alt=""/>
                                    @else
                                      <img src="{{$profile_img_public_path}}default_profile_image.png" alt="">
                                    @endif
                                 </div>
                              </div>
                              <div style="text-align:center;margin-bottom:15px;">
                              @if(isset($arr_client['created_at']) && $arr_client['created_at']!="")
                                 {{'Since '.date('M Y',strtotime($arr_client['created_at'])).' on'}} <br/> {{config('app.project.name')}}
                              @endif
                              </div>
                              <!-- <div style="text-align:center;margin-bottom:15px;" title="{{$sidebar_information['client_timezone']}}">
                                 {{ trans('common/_expert_details.text_local_time') }} : {{$sidebar_information['client_timezone_without_date']}}
                              </div> -->
                           </div>
                           <div class="col-sm-9 col-md-9 col-lg-9">
                              <div class="right-dashbord">
                                 <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-9 col-lg-12">
                                       <?php 
                                           $client_user_name = "---";
                                           $first_name       = isset($arr_client['first_name']) ?$arr_client['first_name']:"";
                                           $last_name        = isset($arr_client['last_name']) ?substr($arr_client['last_name'], 0,1).'.':"";
                                           $client_user_name = $first_name.' '.$last_name;
                                           /*if(isset($arr_client['user_id']) && $arr_client['user_id'] != "") {
                                             $user = Sentinel::findById($arr_client['user_id']);
                                             if(isset($user) && $user != FALSE)
                                             { 
                                               if(isset($user->user_name))
                                               {
                                                 $client_user_name = $user->user_name;
                                               }
                                             }
                                           }*/
                                       ?>
                                       <div class="head_grn">
                                         {{$client_user_name or ''}}
                                         
                                          @if(isset($user_auth_details['kyc_verified']) && $user_auth_details['kyc_verified'] == '1')
                                          <span class="verfy-new-arrow">
                                              <a href="#" data-toggle="tooltip" title="Identity Verified"><img src="{{url('/public')}}/front/images/verifild.png" alt=""> </a>
                                          </span>
                                          @endif

                                       </div>
                                       <div class="address-mod-pro">
										                   <div class="icon-block"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                                       <div class="add-conte-block">{{isset($arr_client['country_details']['country_name'])?$arr_client['country_details']['country_name']:''}}</div>
                                         <div class="clr"></div>
                                       </div>
                                       <!-- <div class="model-deceb-pro">
                                       <div class="icon-block"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                       <div class="add-conte-block">{{isset($arr_client['user_details']['email'])?$arr_client['user_details']['email']:''}}</div>
                                         <div class="clr"></div>
                                       </div> -->
                                       <div class="model-deceb-pro">
										                    <div class="icon-block"><i class="fa fa-info-circle" aria-hidden="true"></i></div>&nbsp;&nbsp;
                                           <div class="add-conte-block">{{ $arr_client['profile_summary']  or ''}}</div>
                                           <div class="clr"></div>
                                       </div>
                                       <?php
                                          $lang_str = "";
                                          $arr_lang = [];
                                          if(isset($arr_client['spoken_languages'])){
                                          $lang_str = $arr_client['spoken_languages'];
                                          $arr_lang = explode(',', $lang_str);
                                          }
                                       ?>
                                      <div class="model-deceb-pro">
    										                  <div class="icon-block"><i class="fa fa-graduation-cap" aria-hidden="true"></i></div>
    										                  <div class="add-conte-block">
                                           @if(count($arr_lang)>0)
                                              @foreach($arr_lang as $language){{ $language }}@if($language != array_last($arr_lang)),@endif @endforeach
                                           @endif
                                           </div>
                                           <div class="clr"></div>
                                      </div>
                                     
                                      <div class="model-deceb-pro">
                                          <div class="icon-block"><i class="fa fa-star" aria-hidden="true"></i></div>
                                          <div class="rate-t">
                                             <div class="rating-list"><span class="stars">{{isset($sidebar_information['average_rating'])?$sidebar_information['average_rating']:'0'}}</span></div>
                                             ({{isset($sidebar_information['average_rating'])?$sidebar_information['average_rating']:'0'}})
                                          </div>
                                          <div class="clr"></div>
                                       </div>
                                       <div class="model-deceb-pro">
                                            <div class="icon-block"><i  class="fa fa-clock-o" aria-hidden="true"></i></div>
                                               <span >                              
                                                  @if($sidebar_information['last_login_duration'] == 'Active')
                                                     <a style="color:#4D4D4D;text-transform:none;cursor:text;" title="{{isset($sidebar_information['client_timezone'])?$sidebar_information['client_timezone']:'Not specify'}}">
                                                        <span class="flag-image"> 
                                                            @if(isset($sidebar_information['user_country_flag'])) 
                                                              @php 
                                                                 echo $sidebar_information['user_country_flag']; 
                                                              @endphp 
                                                            @elseif(isset($sidebar_information['user_country'])) 
                                                              @php 
                                                                echo $sidebar_information['user_country']; 
                                                              @endphp 
                                                            @else 
                                                              @php 
                                                                echo 'Not Specify'; 
                                                              @endphp 
                                                            @endif 
                                                        </span>
                                                        {{-- trans('common/_expert_details.text_local_time') --}}  {{isset($sidebar_information['client_timezone_without_date'])?$sidebar_information['client_timezone_without_date']:'Not specify'}}
                                                     </a>
                                                  @else
                                                     <span class="flag-image"> 
                                                      @if(isset($sidebar_information['user_country_flag'])) 
                                                        @php 
                                                           echo $sidebar_information['user_country_flag']; 
                                                        @endphp 
                                                      @elseif(isset($sidebar_information['user_country'])) 
                                                        @php 
                                                          echo $sidebar_information['user_country']; 
                                                        @endphp 
                                                      @else 
                                                        @php 
                                                          echo 'Not Specify'; 
                                                        @endphp 
                                                      @endif 
                                                  </span>
                                                     <a style="color:#4D4D4D;text-transform:none;cursor:text;" title="{{isset($sidebar_information['last_login_duration'])?$sidebar_information['last_login_full_duration']:'Not specify'}}">
                                                        <span  title="{{$sidebar_information['last_login_full_duration']}}">{{isset($sidebar_information['last_login_duration'])?$sidebar_information['last_login_duration']:'Not specify'}}</span>  
                                                     </a>
                                                  @endif
                                               </span>
                                        <div class="clr"></div>
                                     </div>   
                                    </div>
                                 </div>
                                <hr/>

                                 <div class="clr"></div>
                                 {{--<div class="rating-bx">
                                    <div class="img-pro infg"><img src="{{url('/public')}}/front/images/project-inc.png" alt="" /></div>
                                    <div class="info-r-pro"><div align="center" >{{isset($sidebar_information['project_count'])?$sidebar_information['project_count']:'0'}}</div><span>{{ trans('expert/portfolio/portfolio.text_projects') }} </span></div>
                                 </div>
                                 --}}
                                 <div class="rating-bx">
                                     <div class="img-pro infg"><img src="{{url('/public')}}/front/images/project-inc.png" alt="" /></div>
                                     <div class="info-r-pro">
                                        <div align="center">{{isset($sidebar_information['completed_project_count'])?$sidebar_information['completed_project_count']:'0'}}</div>
                                        <span>{{ trans('common/expert_sidebar.text_completed_projects') }}</span>
                                     </div>
                                  </div>
                                  <div class="rating-bx">
                                     <div class="img-pro infg"><img src="{{url('/public')}}/front/images/project-inc.png" alt="" /></div>
                                     <div class="info-r-pro">
                                        <div align="center">{{isset($sidebar_information['ongoing_project_count'])?$sidebar_information['ongoing_project_count']:'0'}}</div>
                                        <span>{{ trans('common/expert_sidebar.text_ongoing_projects') }}</span>
                                     </div>
                                  </div>
                                 {{--<div class="rating-bx">
                                    <div class="img-pro infg"><img src="{{url('/public')}}/front/images/star-inc.png" alt="" /></div>
                                    <div class="info-r-pro">
                                    <div align="center" >{{isset($sidebar_information['completion_rate'])?$sidebar_information['completion_rate']:'0.0'}}% </div><span>{{ trans('expert/portfolio/portfolio.text_completion_rate') }}</span></div>
                                 </div>
                                 --}}
                                 <div class="rating-bx">
                                    <div class="img-pro infg"><img src="{{url('/public')}}/front/images/reviews-inc.png" alt="" /></div>
                                    <div class="info-r-pro"><div align="center" >{{isset($sidebar_information['review_count'])?$sidebar_information['review_count']:'0'}}</div><span>{{ trans('expert/portfolio/portfolio.text_review') }}</span></div>
                                 </div>
                                 <div class="rating-bx">
                                    <div class="img-pro infg"><img src="{{url('/public')}}/front/images/rating-inc.png" alt="" /></div>
                                    <div class="info-r-pro"><div align="center" >{{isset($sidebar_information['reputation'])?$sidebar_information['reputation']:'0.0'}}%</div> <span>{{ trans('expert/portfolio/portfolio.text_reputation') }}</span></div>
                                 </div>

                              </div>
                           </div>
                           <div class="clr"></div>
                        </div>
                        <div class="row listing-detadfs">
                           <div class="col-sm-12 col-md-12 col-lg-12">
                              <div class="main-tbs">
                                 <!-- Details tabs -->
                                 <ul class="nav nav-tabs">
                                    <li class="active"><a href="#reviews" aria-controls="reviews" data-toggle="tab">@if(isset($review_cnt)&& sizeof($review_cnt)>0) {{$review_cnt}} @else 0 @endif Reviews</a></li>
                                 </ul>
                                 <!-- Details panes -->
                                 <div class="tab-content">
                      
                                    <!--tab 2-->
                                   <div class="tab-pane" id="reviews">
                                       <div class="customer-review-port">

                                          @if(isset($arr_review['data']) && sizeof($arr_review['data'])>0)
                                          @foreach($arr_review['data'] as $review)


                                          <div class="media">
                                             <div class="media-left">
                                                <a href="#">
                                                @if(isset($profile_img_public_path) && isset($review['expert_details']['profile_image']) && $review['expert_details']['profile_image']!="")

                                                  @if(file_exists($profile_img_public_path.$review['expert_details']['profile_image']))
                                                     <img class="media-object img-circle" src="{{$profile_img_public_path.$review['expert_details']['profile_image']}}" alt="" />
                                                  @else
                                                      <img src="{{$profile_img_public_path}}default_profile_image.png" alt="">
                                                  @endif
                                                 
                                                @else
                                                  <img src="{{$profile_img_public_path}}default_profile_image.png" alt="">
                                                @endif
                                                </a>
                                             </div>



                                           <?php 

                                           $user_name = "---";  

                                           if(isset($review['expert_details']['user_id']) && $review['expert_details']['user_id'] != "")
                                           {
                                             $user = Sentinel::findById($review['expert_details']['user_id']);
                                             if(isset($user) && $user != FALSE)
                                             { 
                                               if(isset($user->user_name))
                                               {
                                                 $user_name = $user->user_name;
                                               }
                                             }
                                           }

                                           $expert_last_name = $expert_first_name = '';
                                           $expert_first_name = isset($review['expert_details']['first_name'])?$review['expert_details']['first_name']:'';
                                           $expert_last_name  = isset($review['expert_details']['last_name']) ?substr($review['expert_details']['last_name'], 0,1).'.':"";

                                          ?>

                                             <div class="media-body">
                                                <h5 class="media-heading">{{$expert_first_name}} {{$expert_last_name}}</h5><h3 style="color:#289BDD;padding: 5px 0;" class="media-heading">

                                                @if(isset($review['project_details']['id']) && isset($review['project_details']['project_name']))
                                                {{-- <a href="{{url('projects/details').'/'.base64_encode($review['project_details']['id'])}}"> --}}
                                                {{isset($review['project_details']['project_name'])?$review['project_details']['project_name']:''}}
                                                {{-- </a> --}}
                                                @endif

                                                </h3>

                                                {{isset($review['review'])?str_limit($review['review'],$limit = 200 ,$end='...'):''}}</h4>


                                                <div class="rating-list dpc-blc" style="padding: 5px 0px;">
                                                   @if(isset($review['rating']))  
                                                      <span class="stars">{{$review['rating']}}</span>
                                                   @endif
                                                   @if(isset($review['created_at']))
                                                   <?php $yrdata= strtotime($review['created_at']);?>
                                                   <p>{{date('d M Y', $yrdata)}}</p>
                                                   @endif
                                                </div>
                                             </div>
                                          </div>
                                          @endforeach
                                          @else
                                          <br/>
                                          <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;" align="center">
                                             <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
                                                <div class="search-content-block">
                                                   <div class="search-head" style="color:rgba(45, 45, 45, 0.8);" align="center">
                                                      {{ trans('expert/portfolio/portfolio.text_no_reviews_found') }}
                                                   </div>
                                                </div>
                                             </td>
                                          </tr>
                                          @endif
                                       </div>
                                       <div class="clearfix"></div>

                                        @if(isset($arr_review['data']) && count($arr_review['data'])>0)
                                           <?php 
                                             $arr_pagination = $arr_review_links;
                                           ?>
                                          <!--pagigation start here-->
                                             @include('project_manager.layout.pagination_view', ['paginator' => $arr_pagination])
                                          <!--pagiyation end here-->
                                         @endif
                                    </div>
                                    <!--tab 2-->
                                 </div>
                                 <!-- Details tabs End --> 
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="{{url('/public')}}/front/js/lightbox-plus-jquery.min.js"></script>
<script type="text/javascript">
   $( document ).ready(function() {
      $('#reviews').show();
   });
   /* Star rating demo */ 
   $.fn.stars = function(){
    return $(this).each(function() {
      //$(this).html($('<span />').width(Math.max(0, (Math.min(5, parseFloat($(this).html())))) * 20));
    });
   }
   $(function() {       
    $('span.stars').stars();
   });
</script>
@stop

