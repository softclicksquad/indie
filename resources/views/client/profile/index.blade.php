@extends('client.layout.master')                
@section('main_content')
<style type="text/css">
    .select2-offscreen[required],
    .select2-offscreen[required]:focus {width: auto !important;height: auto !important;}

    .fld-required {  color: #FF0000; }
    /* sign up popup start here*/
    .modal-dialog.congratulations-popup{width: 500px;}
    .check-img-block{height: 60px; width: 60px; border-radius: 50%; margin: 0 auto; border: 1px solid #63c65f; text-align: center; padding-top: 17px;}
    .congratulations-popup .modal-body{text-align: center;}
    .congratulations-popup .modal-body h4{font-size: 25px; font-weight: 600;}
    .congratulations-popup .modal-body .you-may-need-message{background: #e2eaea; padding: 15px; border: 1px solid #d2bbbf; color: #7d6c6e; text-transform: uppercase; border-radius: 5px; font-weight: 600; display: block; margin: 5px 0 20px;}
    .congratulations-popup .modal-body .you-may-need-message span{text-transform: none;}
    .congratulations-popup .modal-body p{margin: 15px 0;}
    .congratulations-popup .modal-body .continue-btn{background: #63c65f; text-transform: uppercase; border-color: #63c65f;}

    .modal-open .modal.fade .modal-dialog {
        -webkit-transform: translate(0, 50%);
        -ms-transform: translate(0, 50%);
        -o-transform: translate(0, 50%);
        transform: translate(0, 50%);
    }

</style>
<div class="col-sm-7 col-md-8 col-lg-9">
    <form action="{{ url($module_url_path) }}/update" method="POST" id="form-client_profile" name="form-client_profile" enctype="multipart/form-data" files ="true">
        {{ csrf_field() }}
        <div class="right_side_section white-wrapper">
            @include('front.layout._operation_status')
            <div class="head_grn">{{ trans('client/profile/profile.text_heading') }}</div>
            <div class="row">
                <div class="change-pwd-form profile-edit-section">

                    <?php   

                    $first_name = '';
                    if( isset($arr_client_details['first_name']) && $arr_client_details['first_name'] != "" )
                    {
                        $first_name = $arr_client_details['first_name'];                     
                    }
                    else if (old('first_name') != "")
                    {
                        $first_name = old('first_name');                     
                    }

                    $ongoing_project_count = client_ongoing_project_count($arr_client_details['user_id']);
                    $open_contest_count    = client_open_contest_count($arr_client_details['user_id']);

                    $arr_currency = config('app.project_currency');

                    $selected_currency = isset($arr_client_details['user_details']['currency_code'])?$arr_client_details['user_details']['currency_code']:'';
                    ?>

                    <input type="hidden" name="old_currency_code" value="{{$selected_currency}}">

                    <div class="col-sm-12  col-md-6 col-lg-6">
                        <div class="user-box">
                            <div class="p-control-label">Profile Image</div>
                            <div class="profile-circle">
                                <div class="client_image client-show-photo"> <input type="file" id="profile_image" class="file">
                                    @if(isset($profile_img_public_path) && isset($arr_client_details['profile_image']) && $arr_client_details['profile_image']!="" && file_exists('public/uploads/front/profile/'.$arr_client_details['profile_image']))
                                    <img id="set_image" style="cursor: default;" src="{{isset($arr_client_details['profile_image'])?$profile_img_public_path.$arr_client_details['profile_image']:''}}" />
                                    @else
                                    <img id="set_image" style="cursor: default;" src="{{$profile_img_public_path.'default_profile_image.png'}}" />
                                    @endif
                                    @if( 'profile' == \Request::segment(2) )
                                    <div class="client-photo__upload-icon" onclick="openCropModal()">
                                       <span class="client-show-icon icon-edit_white"><img src="{{url('/public')}}/front/images/edit-file.png" alt="" />
                                           <div class="edit-profile-name">{{trans('new_translations.edit')}}</div>
                                       </span>
                                   </div>
                                   @endif
                               </div>
                           </div>
                       </div>
                   </div>
                </div>
                <div class="clearfix"></div>

                <div class="col-sm-12  col-md-6 col-lg-6">
                    <div class="user-box">
                        <div class="p-control-label">{{ trans('client/profile/profile.text_first_name') }}<span>*</span></div>
                        <div class="input-name">
                            <input type="text" class="clint-input" placeholder="{{ trans('client/profile/profile.entry_first_name') }}" name="first_name" id="first_name" value="{{$first_name}}" data-rule-required="true" data-rule-maxlength="250" onkeyup="return chk_validation(this);"/>
                            <span class='error'>{{ $errors->first('first_name') }}</span>
                        </div>
                    </div>
                </div>

                <?php 
                $last_name = '';
                if( isset($arr_client_details['last_name']) && $arr_client_details['last_name'] != "" )
                {
                    $last_name = $arr_client_details['last_name'];                     
                }
                else if (old('last_name') != "")
                {
                    $last_name = old('last_name');                     
                }
                ?>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="user-box">
                        <div class="p-control-label">{{ trans('client/profile/profile.text_last_name') }}<span>*</span></div>
                        <div class="input-name"><input type="text" class="clint-input" placeholder="{{ trans('client/profile/profile.entry_last_name') }}" name="last_name" id="last_name" value="{{$last_name}}" data-rule-required="true" data-rule-maxlength="250" onkeyup="return chk_validation(this);"/>
                            <span class='error'>{{ $errors->first('last_name') }}</span>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="user-box">
                        <div class="p-control-label">{{ trans('client/profile/profile.text_email') }}<span>*</span></div>
                        <div class="input-name"><input type="email" class="clint-input" placeholder="{{ trans('client/profile/profile.entry_email') }}" name="email"  id="email" value="{{isset($arr_client_details['user_details']['email'])?$arr_client_details['user_details']['email']:''}}" data-rule-required="true" /></div>
                    </div>
                </div>
                <input type="hidden" name="old_email" value="{{isset($arr_client_details['user_details']['email'])?$arr_client_details['user_details']['email']:''}}">
                <?php 
                $phone_code = '';
                if( isset($arr_client_details['phone_code']) && $arr_client_details['phone_code'] != "" )
                {
                    $phone_code = $arr_client_details['phone_code'];                     
                }
                else if (old('phone_code') != "")
                {
                    $phone_code = old('phone_code');                     
                }

                $phone_number = '';
                if( isset($arr_client_details['phone_number']) && $arr_client_details['phone_number'] != "" )
                {
                    $phone_number = $arr_client_details['phone_number'];                     
                }
                else if (old('phone_number') != "")
                {
                    $phone_number = old('phone_number');                     
                }

                ?>

                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="user-box">
                        <div class="p-control-label">{{ trans('client/profile/profile.text_phone_number') }}<span>*</span></div>

                        <div class="row">
                            <span class='error'>{{ $errors->first('phone_code') }}</span> 
                            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                <div class="input-name">
                                    <div class="droup-select code-select">
                                        <select class="selectpicker droup" data-live-search="true" name="phone_code" id="phone_code" data-rule-required="true">
                                            <option data-countryCode="DZ" value="213"  @if(isset($phone_code) && $phone_code == '213') selected="selected" @endif>Algeria (+213)</option>
                                            <option data-countryCode="AD" value="376"  @if(isset($phone_code) && $phone_code == '376') selected="selected" @endif>Andorra (+376)</option>
                                            <option data-countryCode="AO" value="244"  @if(isset($phone_code) && $phone_code == '244') selected="selected" @endif>Angola (+244)</option>
                                            <option data-countryCode="AI" value="1264"  @if(isset($phone_code) && $phone_code == '1264') selected="selected" @endif>Anguilla (+1264)</option>
                                            <option data-countryCode="AG" value="1268"  @if(isset($phone_code) && $phone_code == '1268') selected="selected" @endif>Antigua &amp; Barbuda (+1268)</option>
                                            <option data-countryCode="AR" value="54"  @if(isset($phone_code) && $phone_code == '54') selected="selected" @endif>Argentina (+54)</option>
                                            <option data-countryCode="AM" value="374"  @if(isset($phone_code) && $phone_code == '374') selected="selected" @endif>Armenia (+374)</option>
                                            <option data-countryCode="AW" value="297"  @if(isset($phone_code) && $phone_code == '297') selected="selected" @endif>Aruba (+297)</option>
                                            <option data-countryCode="AU" value="61"  @if(isset($phone_code) && $phone_code == '61') selected="selected" @endif>Australia (+61)</option>
                                            <option data-countryCode="AT" value="43"  @if(isset($phone_code) && $phone_code == '43') selected="selected" @endif>Austria (+43)</option>
                                            <option data-countryCode="AZ" value="994"  @if(isset($phone_code) && $phone_code == '994') selected="selected" @endif>Azerbaijan (+994)</option>
                                            <option data-countryCode="BS" value="1242"  @if(isset($phone_code) && $phone_code == '1242') selected="selected" @endif>Bahamas (+1242)</option>
                                            <option data-countryCode="BH" value="973"  @if(isset($phone_code) && $phone_code == '973') selected="selected" @endif>Bahrain (+973)</option>
                                            <option data-countryCode="BD" value="880"  @if(isset($phone_code) && $phone_code == '880') selected="selected" @endif>Bangladesh (+880)</option>
                                            <option data-countryCode="BB" value="1246"  @if(isset($phone_code) && $phone_code == '1246') selected="selected" @endif>Barbados (+1246)</option>
                                            <option data-countryCode="BY" value="375"  @if(isset($phone_code) && $phone_code == '375') selected="selected" @endif>Belarus (+375)</option>
                                            <option data-countryCode="BE" value="32"  @if(isset($phone_code) && $phone_code == '32') selected="selected" @endif>Belgium (+32)</option>
                                            <option data-countryCode="BZ" value="501"  @if(isset($phone_code) && $phone_code == '501') selected="selected" @endif>Belize (+501)</option>
                                            <option data-countryCode="BJ" value="229"  @if(isset($phone_code) && $phone_code == '229') selected="selected" @endif>Benin (+229)</option>
                                            <option data-countryCode="BM" value="1441"  @if(isset($phone_code) && $phone_code == '1441') selected="selected" @endif>Bermuda (+1441)</option>
                                            <option data-countryCode="BT" value="975"  @if(isset($phone_code) && $phone_code == '975') selected="selected" @endif>Bhutan (+975)</option>
                                            <option data-countryCode="BO" value="591"  @if(isset($phone_code) && $phone_code == '591') selected="selected" @endif>Bolivia (+591)</option>
                                            <option data-countryCode="BA" value="387"  @if(isset($phone_code) && $phone_code == '387') selected="selected" @endif>Bosnia Herzegovina (+387)</option>
                                            <option data-countryCode="BW" value="267"  @if(isset($phone_code) && $phone_code == '267') selected="selected" @endif>Botswana (+267)</option>
                                            <option data-countryCode="BR" value="55"  @if(isset($phone_code) && $phone_code == '55') selected="selected" @endif>Brazil (+55)</option>
                                            <option data-countryCode="BN" value="673"  @if(isset($phone_code) && $phone_code == '673') selected="selected" @endif>Brunei (+673)</option>
                                            <option data-countryCode="BG" value="359"  @if(isset($phone_code) && $phone_code == '359') selected="selected" @endif>Bulgaria (+359)</option>
                                            <option data-countryCode="BF" value="226"  @if(isset($phone_code) && $phone_code == '226') selected="selected" @endif>Burkina Faso (+226)</option>
                                            <option data-countryCode="BI" value="257"  @if(isset($phone_code) && $phone_code == '257') selected="selected" @endif>Burundi (+257)</option>
                                            <option data-countryCode="KH" value="855"  @if(isset($phone_code) && $phone_code == '855') selected="selected" @endif>Cambodia (+855)</option>
                                            <option data-countryCode="CM" value="237"  @if(isset($phone_code) && $phone_code == '237') selected="selected" @endif>Cameroon (+237)</option>
                                            <option data-countryCode="CA" value="1"  @if(isset($phone_code) && $phone_code == '1') selected="selected" @endif>Canada (+1)</option>
                                            <option data-countryCode="CV" value="238"  @if(isset($phone_code) && $phone_code == '238') selected="selected" @endif>Cape Verde Islands (+238)</option>
                                            <option data-countryCode="KY" value="1345"  @if(isset($phone_code) && $phone_code == '1345') selected="selected" @endif>Cayman Islands (+1345)</option>
                                            <option data-countryCode="CF" value="236"  @if(isset($phone_code) && $phone_code == '236') selected="selected" @endif>Central African Republic (+236)</option>
                                            <option data-countryCode="CL" value="56"  @if(isset($phone_code) && $phone_code == '56') selected="selected" @endif>Chile (+56)</option>
                                            <option data-countryCode="CN" value="86"  @if(isset($phone_code) && $phone_code == '86') selected="selected" @endif>China (+86)</option>
                                            <option data-countryCode="CO" value="57"  @if(isset($phone_code) && $phone_code == '57') selected="selected" @endif>Colombia (+57)</option>
                                            <option data-countryCode="KM" value="269"  @if(isset($phone_code) && $phone_code == '269') selected="selected" @endif>Comoros (+269)</option>
                                            <option data-countryCode="CG" value="242"  @if(isset($phone_code) && $phone_code == '242') selected="selected" @endif>Congo (+242)</option>
                                            <option data-countryCode="CK" value="682"  @if(isset($phone_code) && $phone_code == '682') selected="selected" @endif>Cook Islands (+682)</option>
                                            <option data-countryCode="CR" value="506"  @if(isset($phone_code) && $phone_code == '506') selected="selected" @endif>Costa Rica (+506)</option>
                                            <option data-countryCode="HR" value="385"  @if(isset($phone_code) && $phone_code == '385') selected="selected" @endif>Croatia (+385)</option>
                                            <option data-countryCode="CU" value="53"  @if(isset($phone_code) && $phone_code == '53') selected="selected" @endif>Cuba (+53)</option>
                                            <option data-countryCode="CY" value="90392"  @if(isset($phone_code) && $phone_code == '90392') selected="selected" @endif>Cyprus North (+90392)</option>
                                            <option data-countryCode="CY" value="357"  @if(isset($phone_code) && $phone_code == '357') selected="selected" @endif>Cyprus South (+357)</option>
                                            <option data-countryCode="CZ" value="42"  @if(isset($phone_code) && $phone_code == '42') selected="selected" @endif>Czech Republic (+42)</option>
                                            <option data-countryCode="DK" value="45"  @if(isset($phone_code) && $phone_code == '45') selected="selected" @endif>Denmark (+45)</option>
                                            <option data-countryCode="DJ" value="253"  @if(isset($phone_code) && $phone_code == '253') selected="selected" @endif>Djibouti (+253)</option>
                                            <option data-countryCode="DM" value="1809"  @if(isset($phone_code) && $phone_code == '1809') selected="selected" @endif>Dominica (+1809)</option>
                                            <option data-countryCode="DO" value="1809"  @if(isset($phone_code) && $phone_code == '1809') selected="selected" @endif>Dominican Republic (+1809)</option>
                                            <option data-countryCode="EC" value="593"  @if(isset($phone_code) && $phone_code == '593') selected="selected" @endif>Ecuador (+593)</option>
                                            <option data-countryCode="EG" value="20"  @if(isset($phone_code) && $phone_code == '20') selected="selected" @endif>Egypt (+20)</option>
                                            <option data-countryCode="SV" value="503"  @if(isset($phone_code) && $phone_code == '503') selected="selected" @endif>El Salvador (+503)</option>
                                            <option data-countryCode="GQ" value="240"  @if(isset($phone_code) && $phone_code == '240') selected="selected" @endif>Equatorial Guinea (+240)</option>
                                            <option data-countryCode="ER" value="291"  @if(isset($phone_code) && $phone_code == '291') selected="selected" @endif>Eritrea (+291)</option>
                                            <option data-countryCode="EE" value="372"  @if(isset($phone_code) && $phone_code == '372') selected="selected" @endif>Estonia (+372)</option>
                                            <option data-countryCode="ET" value="251"  @if(isset($phone_code) && $phone_code == '251') selected="selected" @endif>Ethiopia (+251)</option>
                                            <option data-countryCode="FK" value="500"  @if(isset($phone_code) && $phone_code == '500') selected="selected" @endif>Falkland Islands (+500)</option>
                                            <option data-countryCode="FO" value="298"  @if(isset($phone_code) && $phone_code == '298') selected="selected" @endif>Faroe Islands (+298)</option>
                                            <option data-countryCode="FJ" value="679"  @if(isset($phone_code) && $phone_code == '679') selected="selected" @endif>Fiji (+679)</option>
                                            <option data-countryCode="FI" value="358"  @if(isset($phone_code) && $phone_code == '358') selected="selected" @endif>Finland (+358)</option>
                                            <option data-countryCode="FR" value="33"  @if(isset($phone_code) && $phone_code == '33') selected="selected" @endif>France (+33)</option>
                                            <option data-countryCode="GF" value="594"  @if(isset($phone_code) && $phone_code == '594') selected="selected" @endif>French Guiana (+594)</option>
                                            <option data-countryCode="PF" value="689"  @if(isset($phone_code) && $phone_code == '689') selected="selected" @endif>French Polynesia (+689)</option>
                                            <option data-countryCode="GA" value="241"  @if(isset($phone_code) && $phone_code == '241') selected="selected" @endif>Gabon (+241)</option>
                                            <option data-countryCode="GM" value="220"  @if(isset($phone_code) && $phone_code == '220') selected="selected" @endif>Gambia (+220)</option>
                                            <option data-countryCode="GE" value="7880"  @if(isset($phone_code) && $phone_code == '7880') selected="selected" @endif>Georgia (+7880)</option>
                                            <option data-countryCode="DE" value="49"  @if(isset($phone_code) && $phone_code == '49') selected="selected" @endif>Germany (+49)</option>
                                            <option data-countryCode="GH" value="233"  @if(isset($phone_code) && $phone_code == '233') selected="selected" @endif>Ghana (+233)</option>
                                            <option data-countryCode="GI" value="350"  @if(isset($phone_code) && $phone_code == '350') selected="selected" @endif>Gibraltar (+350)</option>
                                            <option data-countryCode="GR" value="30"  @if(isset($phone_code) && $phone_code == '30') selected="selected" @endif>Greece (+30)</option>
                                            <option data-countryCode="GL" value="299"  @if(isset($phone_code) && $phone_code == '299') selected="selected" @endif>Greenland (+299)</option>
                                            <option data-countryCode="GD" value="1473"  @if(isset($phone_code) && $phone_code == '1473') selected="selected" @endif>Grenada (+1473)</option>
                                            <option data-countryCode="GP" value="590"  @if(isset($phone_code) && $phone_code == '590') selected="selected" @endif>Guadeloupe (+590)</option>
                                            <option data-countryCode="GU" value="671"  @if(isset($phone_code) && $phone_code == '671') selected="selected" @endif>Guam (+671)</option>
                                            <option data-countryCode="GT" value="502"  @if(isset($phone_code) && $phone_code == '502') selected="selected" @endif>Guatemala (+502)</option>
                                            <option data-countryCode="GN" value="224"  @if(isset($phone_code) && $phone_code == '224') selected="selected" @endif>Guinea (+224)</option>
                                            <option data-countryCode="GW" value="245"  @if(isset($phone_code) && $phone_code == '245') selected="selected" @endif>Guinea - Bissau (+245)</option>
                                            <option data-countryCode="GY" value="592"  @if(isset($phone_code) && $phone_code == '592') selected="selected" @endif>Guyana (+592)</option>
                                            <option data-countryCode="HT" value="509"  @if(isset($phone_code) && $phone_code == '509') selected="selected" @endif>Haiti (+509)</option>
                                            <option data-countryCode="HN" value="504"  @if(isset($phone_code) && $phone_code == '504') selected="selected" @endif>Honduras (+504)</option>
                                            <option data-countryCode="HK" value="852"  @if(isset($phone_code) && $phone_code == '852') selected="selected" @endif>Hong Kong (+852)</option>
                                            <option data-countryCode="HU" value="36"  @if(isset($phone_code) && $phone_code == '36') selected="selected" @endif>Hungary (+36)</option>
                                            <option data-countryCode="IS" value="354"  @if(isset($phone_code) && $phone_code == '354') selected="selected" @endif>Iceland (+354)</option>
                                            <option data-countryCode="IN" value="91"  @if(isset($phone_code) && $phone_code == '91') selected="selected" @endif >India (+91)</option>
                                            <option data-countryCode="ID" value="62"  @if(isset($phone_code) && $phone_code == '62') selected="selected" @endif>Indonesia (+62)</option>
                                            <option data-countryCode="IR" value="98"  @if(isset($phone_code) && $phone_code == '98') selected="selected" @endif>Iran (+98)</option>
                                            <option data-countryCode="IQ" value="964"  @if(isset($phone_code) && $phone_code == '964') selected="selected" @endif>Iraq (+964)</option>
                                            <option data-countryCode="IE" value="353"  @if(isset($phone_code) && $phone_code == '353') selected="selected" @endif>Ireland (+353)</option>
                                            <option data-countryCode="IL" value="972"  @if(isset($phone_code) && $phone_code == '972') selected="selected" @endif>Israel (+972)</option>
                                            <option data-countryCode="IT" value="39"  @if(isset($phone_code) && $phone_code == '39') selected="selected" @endif>Italy (+39)</option>
                                            <option data-countryCode="JM" value="1876"  @if(isset($phone_code) && $phone_code == '1876') selected="selected" @endif>Jamaica (+1876)</option>
                                            <option data-countryCode="JP" value="81"  @if(isset($phone_code) && $phone_code == '81') selected="selected" @endif>Japan (+81)</option>
                                            <option data-countryCode="JO" value="962"  @if(isset($phone_code) && $phone_code == '962') selected="selected" @endif>Jordan (+962)</option>
                                            <option data-countryCode="KZ" value="7"  @if(isset($phone_code) && $phone_code == '7') selected="selected" @endif>Kazakhstan (+7)</option>
                                            <option data-countryCode="KE" value="254"  @if(isset($phone_code) && $phone_code == '254') selected="selected" @endif>Kenya (+254)</option>
                                            <option data-countryCode="KI" value="686"  @if(isset($phone_code) && $phone_code == '686') selected="selected" @endif>Kiribati (+686)</option>
                                            <option data-countryCode="KP" value="850"  @if(isset($phone_code) && $phone_code == '850') selected="selected" @endif>Korea North (+850)</option>
                                            <option data-countryCode="KR" value="82"  @if(isset($phone_code) && $phone_code == '82') selected="selected" @endif>Korea South (+82)</option>
                                            <option data-countryCode="KW" value="965"  @if(isset($phone_code) && $phone_code == '965') selected="selected" @endif>Kuwait (+965)</option>
                                            <option data-countryCode="KG" value="996"  @if(isset($phone_code) && $phone_code == '996') selected="selected" @endif>Kyrgyzstan (+996)</option>
                                            <option data-countryCode="LA" value="856"  @if(isset($phone_code) && $phone_code == '856') selected="selected" @endif>Laos (+856)</option>
                                            <option data-countryCode="LV" value="371"  @if(isset($phone_code) && $phone_code == '371') selected="selected" @endif>Latvia (+371)</option>
                                            <option data-countryCode="LB" value="961"  @if(isset($phone_code) && $phone_code == '961') selected="selected" @endif>Lebanon (+961)</option>
                                            <option data-countryCode="LS" value="266"  @if(isset($phone_code) && $phone_code == '266') selected="selected" @endif>Lesotho (+266)</option>
                                            <option data-countryCode="LR" value="231"  @if(isset($phone_code) && $phone_code == '231') selected="selected" @endif>Liberia (+231)</option>
                                            <option data-countryCode="LY" value="218"  @if(isset($phone_code) && $phone_code == '218') selected="selected" @endif>Libya (+218)</option>
                                            <option data-countryCode="LI" value="417"  @if(isset($phone_code) && $phone_code == '417') selected="selected" @endif>Liechtenstein (+417)</option>
                                            <option data-countryCode="LT" value="370"  @if(isset($phone_code) && $phone_code == '370') selected="selected" @endif>Lithuania (+370)</option>
                                            <option data-countryCode="LU" value="352"  @if(isset($phone_code) && $phone_code == '352') selected="selected" @endif>Luxembourg (+352)</option>
                                            <option data-countryCode="MO" value="853"  @if(isset($phone_code) && $phone_code == '853') selected="selected" @endif>Macao (+853)</option>
                                            <option data-countryCode="MK" value="389"  @if(isset($phone_code) && $phone_code == '389') selected="selected" @endif>Macedonia (+389)</option>
                                            <option data-countryCode="MG" value="261"  @if(isset($phone_code) && $phone_code == '261') selected="selected" @endif>Madagascar (+261)</option>
                                            <option data-countryCode="MW" value="265"  @if(isset($phone_code) && $phone_code == '265') selected="selected" @endif>Malawi (+265)</option>
                                            <option data-countryCode="MY" value="60"  @if(isset($phone_code) && $phone_code == '60') selected="selected" @endif>Malaysia (+60)</option>
                                            <option data-countryCode="MV" value="960"  @if(isset($phone_code) && $phone_code == '960') selected="selected" @endif>Maldives (+960)</option>
                                            <option data-countryCode="ML" value="223"  @if(isset($phone_code) && $phone_code == '223') selected="selected" @endif>Mali (+223)</option>
                                            <option data-countryCode="MT" value="356"  @if(isset($phone_code) && $phone_code == '356') selected="selected" @endif>Malta (+356)</option>
                                            <option data-countryCode="MH" value="692"  @if(isset($phone_code) && $phone_code == '692') selected="selected" @endif>Marshall Islands (+692)</option>
                                            <option data-countryCode="MQ" value="596"  @if(isset($phone_code) && $phone_code == '596') selected="selected" @endif>Martinique (+596)</option>
                                            <option data-countryCode="MR" value="222"  @if(isset($phone_code) && $phone_code == '222') selected="selected" @endif>Mauritania (+222)</option>
                                            <option data-countryCode="YT" value="269"  @if(isset($phone_code) && $phone_code == '269') selected="selected" @endif>Mayotte (+269)</option>
                                            <option data-countryCode="MX" value="52"  @if(isset($phone_code) && $phone_code == '52') selected="selected" @endif>Mexico (+52)</option>
                                            <option data-countryCode="FM" value="691"  @if(isset($phone_code) && $phone_code == '691') selected="selected" @endif>Micronesia (+691)</option>
                                            <option data-countryCode="MD" value="373"  @if(isset($phone_code) && $phone_code == '373') selected="selected" @endif>Moldova (+373)</option>
                                            <option data-countryCode="MC" value="377"  @if(isset($phone_code) && $phone_code == '377') selected="selected" @endif>Monaco (+377)</option>
                                            <option data-countryCode="MN" value="976"  @if(isset($phone_code) && $phone_code == '976') selected="selected" @endif>Mongolia (+976)</option>
                                            <option data-countryCode="MS" value="1664"  @if(isset($phone_code) && $phone_code == '1664') selected="selected" @endif>Montserrat (+1664)</option>
                                            <option data-countryCode="MA" value="212"  @if(isset($phone_code) && $phone_code == '212') selected="selected" @endif>Morocco (+212)</option>
                                            <option data-countryCode="MZ" value="258"  @if(isset($phone_code) && $phone_code == '258') selected="selected" @endif>Mozambique (+258)</option>
                                            <option data-countryCode="MN" value="95"  @if(isset($phone_code) && $phone_code == '95') selected="selected" @endif>Myanmar (+95)</option>
                                            <option data-countryCode="NA" value="264"  @if(isset($phone_code) && $phone_code == '264') selected="selected" @endif>Namibia (+264)</option>
                                            <option data-countryCode="NR" value="674"  @if(isset($phone_code) && $phone_code == '674') selected="selected" @endif>Nauru (+674)</option>
                                            <option data-countryCode="NP" value="977"  @if(isset($phone_code) && $phone_code == '977') selected="selected" @endif>Nepal (+977)</option>
                                            <option data-countryCode="NL" value="31"  @if(isset($phone_code) && $phone_code == '31') selected="selected" @endif>Netherlands (+31)</option>
                                            <option data-countryCode="NC" value="687"  @if(isset($phone_code) && $phone_code == '687') selected="selected" @endif>New Caledonia (+687)</option>
                                            <option data-countryCode="NZ" value="64"  @if(isset($phone_code) && $phone_code == '64') selected="selected" @endif>New Zealand (+64)</option>
                                            <option data-countryCode="NI" value="505"  @if(isset($phone_code) && $phone_code == '505') selected="selected" @endif>Nicaragua (+505)</option>
                                            <option data-countryCode="NE" value="227"  @if(isset($phone_code) && $phone_code == '227') selected="selected" @endif>Niger (+227)</option>
                                            <option data-countryCode="NG" value="234"  @if(isset($phone_code) && $phone_code == '234') selected="selected" @endif>Nigeria (+234)</option>
                                            <option data-countryCode="NU" value="683"  @if(isset($phone_code) && $phone_code == '683') selected="selected" @endif>Niue (+683)</option>
                                            <option data-countryCode="NF" value="672"  @if(isset($phone_code) && $phone_code == '672') selected="selected" @endif>Norfolk Islands (+672)</option>
                                            <option data-countryCode="NP" value="670"  @if(isset($phone_code) && $phone_code == '670') selected="selected" @endif>Northern Marianas (+670)</option>
                                            <option data-countryCode="NO" value="47"  @if(isset($phone_code) && $phone_code == '47') selected="selected" @endif>Norway (+47)</option>
                                            <option data-countryCode="OM" value="968"  @if(isset($phone_code) && $phone_code == '968') selected="selected" @endif>Oman (+968)</option>
                                            <option data-countryCode="PW" value="680"  @if(isset($phone_code) && $phone_code == '680') selected="selected" @endif>Palau (+680)</option>
                                            <option data-countryCode="PA" value="507"  @if(isset($phone_code) && $phone_code == '507') selected="selected" @endif>Panama (+507)</option>
                                            <option data-countryCode="PG" value="675"  @if(isset($phone_code) && $phone_code == '675') selected="selected" @endif>Papua New Guinea (+675)</option>
                                            <option data-countryCode="PY" value="595"  @if(isset($phone_code) && $phone_code == '595') selected="selected" @endif>Paraguay (+595)</option>
                                            <option data-countryCode="PE" value="51"  @if(isset($phone_code) && $phone_code == '51') selected="selected" @endif>Peru (+51)</option>
                                            <option data-countryCode="PH" value="63"  @if(isset($phone_code) && $phone_code == '63') selected="selected" @endif>Philippines (+63)</option>
                                            <option data-countryCode="PL" value="48"  @if(isset($phone_code) && $phone_code == '48') selected="selected" @endif>Poland (+48)</option>
                                            <option data-countryCode="PT" value="351"  @if(isset($phone_code) && $phone_code == '351') selected="selected" @endif>Portugal (+351)</option>
                                            <option data-countryCode="PR" value="1787"  @if(isset($phone_code) && $phone_code == '1787') selected="selected" @endif>Puerto Rico (+1787)</option>
                                            <option data-countryCode="QA" value="974"  @if(isset($phone_code) && $phone_code == '974') selected="selected" @endif>Qatar (+974)</option>
                                            <option data-countryCode="RE" value="262"  @if(isset($phone_code) && $phone_code == '262') selected="selected" @endif>Reunion (+262)</option>
                                            <option data-countryCode="RO" value="40"  @if(isset($phone_code) && $phone_code == '40') selected="selected" @endif>Romania (+40)</option>
                                            <option data-countryCode="RU" value="7"  @if(isset($phone_code) && $phone_code == '7') selected="selected" @endif>Russia (+7)</option>
                                            <option data-countryCode="RW" value="250"  @if(isset($phone_code) && $phone_code == '250') selected="selected" @endif>Rwanda (+250)</option>
                                            <option data-countryCode="SM" value="378"  @if(isset($phone_code) && $phone_code == '378') selected="selected" @endif>San Marino (+378)</option>
                                            <option data-countryCode="ST" value="239"  @if(isset($phone_code) && $phone_code == '239') selected="selected" @endif>Sao Tome &amp; Principe (+239)</option>
                                            <option data-countryCode="SA" value="966"  @if(isset($phone_code) && $phone_code == '966') selected="selected" @endif>Saudi Arabia (+966)</option>
                                            <option data-countryCode="SN" value="221"  @if(isset($phone_code) && $phone_code == '221') selected="selected" @endif>Senegal (+221)</option>
                                            <option data-countryCode="CS" value="381"  @if(isset($phone_code) && $phone_code == '381') selected="selected" @endif>Serbia (+381)</option>
                                            <option data-countryCode="SC" value="248"  @if(isset($phone_code) && $phone_code == '248') selected="selected" @endif>Seychelles (+248)</option>
                                            <option data-countryCode="SL" value="232"  @if(isset($phone_code) && $phone_code == '232') selected="selected" @endif>Sierra Leone (+232)</option>
                                            <option data-countryCode="SG" value="65"  @if(isset($phone_code) && $phone_code == '65') selected="selected" @endif>Singapore (+65)</option>
                                            <option data-countryCode="SK" value="421"  @if(isset($phone_code) && $phone_code == '421') selected="selected" @endif>Slovak Republic (+421)</option>
                                            <option data-countryCode="SI" value="386"  @if(isset($phone_code) && $phone_code == '386') selected="selected" @endif>Slovenia (+386)</option>
                                            <option data-countryCode="SB" value="677"  @if(isset($phone_code) && $phone_code == '677') selected="selected" @endif>Solomon Islands (+677)</option>
                                            <option data-countryCode="SO" value="252"  @if(isset($phone_code) && $phone_code == '252') selected="selected" @endif>Somalia (+252)</option>
                                            <option data-countryCode="ZA" value="27"  @if(isset($phone_code) && $phone_code == '27') selected="selected" @endif>South Africa (+27)</option>
                                            <option data-countryCode="ES" value="34"  @if(isset($phone_code) && $phone_code == '34') selected="selected" @endif>Spain (+34)</option>
                                            <option data-countryCode="LK" value="94"  @if(isset($phone_code) && $phone_code == '94') selected="selected" @endif>Sri Lanka (+94)</option>
                                            <option data-countryCode="SH" value="290"  @if(isset($phone_code) && $phone_code == '290') selected="selected" @endif>St. Helena (+290)</option>
                                            <option data-countryCode="KN" value="1869"  @if(isset($phone_code) && $phone_code == '1869') selected="selected" @endif>St. Kitts (+1869)</option>
                                            <option data-countryCode="SC" value="1758"  @if(isset($phone_code) && $phone_code == '1758') selected="selected" @endif>St. Lucia (+1758)</option>
                                            <option data-countryCode="SD" value="249"  @if(isset($phone_code) && $phone_code == '249') selected="selected" @endif>Sudan (+249)</option>
                                            <option data-countryCode="SR" value="597"  @if(isset($phone_code) && $phone_code == '597') selected="selected" @endif>Suriname (+597)</option>
                                            <option data-countryCode="SZ" value="268"  @if(isset($phone_code) && $phone_code == '268') selected="selected" @endif>Swaziland (+268)</option>
                                            <option data-countryCode="SE" value="46"  @if(isset($phone_code) && $phone_code == '46') selected="selected" @endif>Sweden (+46)</option>
                                            <option data-countryCode="CH" value="41"  @if(isset($phone_code) && $phone_code == '41') selected="selected" @endif>Switzerland (+41)</option>
                                            <option data-countryCode="SI" value="963"  @if(isset($phone_code) && $phone_code == '963') selected="selected" @endif>Syria (+963)</option>
                                            <option data-countryCode="TW" value="886"  @if(isset($phone_code) && $phone_code == '886') selected="selected" @endif>Taiwan (+886)</option>
                                            <option data-countryCode="TJ" value="7"  @if(isset($phone_code) && $phone_code == '7') selected="selected" @endif>Tajikstan (+7)</option>
                                            <option data-countryCode="TH" value="66"  @if(isset($phone_code) && $phone_code == '66') selected="selected" @endif>Thailand (+66)</option>
                                            <option data-countryCode="TG" value="228"  @if(isset($phone_code) && $phone_code == '228') selected="selected" @endif>Togo (+228)</option>
                                            <option data-countryCode="TO" value="676"  @if(isset($phone_code) && $phone_code == '676') selected="selected" @endif>Tonga (+676)</option>
                                            <option data-countryCode="TT" value="1868"  @if(isset($phone_code) && $phone_code == '1868') selected="selected" @endif>Trinidad &amp; Tobago (+1868)</option>
                                            <option data-countryCode="TN" value="216"  @if(isset($phone_code) && $phone_code == '216') selected="selected" @endif>Tunisia (+216)</option>
                                            <option data-countryCode="TR" value="90"  @if(isset($phone_code) && $phone_code == '90') selected="selected" @endif>Turkey (+90)</option>
                                            <option data-countryCode="TM" value="7"  @if(isset($phone_code) && $phone_code == '7') selected="selected" @endif>Turkmenistan (+7)</option>
                                            <option data-countryCode="TM" value="993"  @if(isset($phone_code) && $phone_code == '993') selected="selected" @endif>Turkmenistan (+993)</option>
                                            <option data-countryCode="TC" value="1649"  @if(isset($phone_code) && $phone_code == '1649') selected="selected" @endif>Turks &amp; Caicos Islands (+1649)</option>
                                            <option data-countryCode="TV" value="688"  @if(isset($phone_code) && $phone_code == '688') selected="selected" @endif>Tuvalu (+688)</option>
                                            <option data-countryCode="UG" value="256"  @if(isset($phone_code) && $phone_code == '256') selected="selected" @endif>Uganda (+256)</option>
                                            <!-- <option data-countryCode="GB" value="44"  @if(isset($phone_code) && $phone_code == '44') selected="selected" @endif>UK (+44)</option> -->
                                            <option data-countryCode="UA" value="380"  @if(isset($phone_code) && $phone_code == '380') selected="selected" @endif>Ukraine (+380)</option>
                                            <option data-countryCode="AE" value="971"  @if(isset($phone_code) && $phone_code == '971') selected="selected" @endif>United Arab Emirates (+971)</option>
                                            <option data-countryCode="US" value="001"  @if(isset($phone_code) && $phone_code == '001') selected="selected" @endif>USA (+001)</option>
                                            <option data-countryCode="UY" value="598"  @if(isset($phone_code) && $phone_code == '598') selected="selected" @endif>Uruguay (+598)</option>
                                            <!-- <option data-countryCode="US" value="1"  @if(isset($phone_code) && $phone_code == '1') selected="selected" @endif>USA (+1)</option> -->
                                            <option data-countryCode="UZ" value="7"  @if(isset($phone_code) && $phone_code == '7') selected="selected" @endif>Uzbekistan (+7)</option>
                                            <option data-countryCode="VU" value="678"  @if(isset($phone_code) && $phone_code == '678') selected="selected" @endif>Vanuatu (+678)</option>
                                            <option data-countryCode="VA" value="379"  @if(isset($phone_code) && $phone_code == '379') selected="selected" @endif>Vatican City (+379)</option>
                                            <option data-countryCode="VE" value="58"  @if(isset($phone_code) && $phone_code == '58') selected="selected" @endif>Venezuela (+58)</option>
                                            <option data-countryCode="VN" value="84"  @if(isset($phone_code) && $phone_code == '84') selected="selected" @endif>Vietnam (+84)</option>
                                            <option data-countryCode="VG" value="84"  @if(isset($phone_code) && $phone_code == '84') selected="selected" @endif>Virgin Islands - British (+1284)</option>
                                            <option data-countryCode="VI" value="84"  @if(isset($phone_code) && $phone_code == '84') selected="selected" @endif>Virgin Islands - US (+1340)</option>
                                            <option data-countryCode="WF" value="681"  @if(isset($phone_code) && $phone_code == '681') selected="selected" @endif>Wallis &amp; Futuna (+681)</option>
                                            <option data-countryCode="YE" value="969"  @if(isset($phone_code) && $phone_code == '969') selected="selected" @endif>Yemen (North)(+969)</option>
                                            <option data-countryCode="YE" value="967"  @if(isset($phone_code) && $phone_code == '967') selected="selected" @endif>Yemen (South)(+967)</option>
                                            <option data-countryCode="ZM" value="260"  @if(isset($phone_code) && $phone_code == '260') selected="selected" @endif>Zambia (+260)</option>
                                            <option data-countryCode="ZW" value="263"  @if(isset($phone_code) && $phone_code == '263') selected="selected" @endif>Zimbabwe (+263)</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                <div class="input-name">
                                    <input type="text" class="clint-input" placeholder="{{ trans('client/profile/profile.entry_phone_number') }}" name="phone_number" id="phone_number" value="{{$phone_number}}" data-rule-required="true" data-rule-maxlength="17" data-rule-minlength="10" data-rule-number="true">
                                </div>
                            </div>
                        </div>

                        <span class='error'>{{ $errors->first('phone_number') }}</span>

                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="col-sm-12  col-md-6 col-lg-6">
                    <div class="user-box">
                        <div class="p-control-label">{{ trans('client/profile/profile.text_user_name') }}<span></span></div>
                        <div class="input-name">
                            <input autocomplete="off" type="text" class="clint-input" name="user_name" placeholder="{{ trans('client/profile/profile.text_user_name') }}" id="user_name" value="{{isset($user_name_string)?$user_name_string:''}}" readonly />
                        </div>
                        <span id="err_user_name" class='error'></span>
                    </div>
                </div>
                <?php 
                $country = '';
                    if( isset($arr_client_details['country']) && $arr_client_details['country'] != "" )
                    {
                        $country = $arr_client_details['country'];                     
                    }
                    else if (old('country') != "")
                    {
                        $country = old('country');                     
                    }
                ?>

                <div class="col-sm-12  col-md-6 col-lg-6">
                    <div class="user-box">
                        <div class="p-control-label">{{ trans('client/profile/profile.text_country') }}<span>*</span></div>
                        <div class="input-name">
                            <div class="droup-select">
                                <select class="droup" data-rule-required="true" name="country" id="country_client" onchange="return loadStates(this);">
                                    <option value="">{{ trans('client/profile/profile.entry_country') }}</option>
                                        @if(isset($arr_countries) && sizeof($arr_countries)>0)
                                        @foreach($arr_countries as $countries)
                                        <option value="{{isset($countries['id'])?$countries['id']:""}}" 
                                        @if($countries['id']==$country)
                                        selected=""
                                        @endif
                                        >
                                        {{isset($countries['country_name'])?$countries['country_name']:"" }}
                                    </option>
                                    @endforeach
                                    @endif
                                </select>
                                <span class='error'>{{ $errors->first('country') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-sm-12  col-md-6 col-lg-6">
                    <div class="user-box">
                        <div class="p-control-label">{{ trans('client/profile/profile.text_state') }}</div>
                        <div class="input-name">
                            <div class="droup-select">
                                <select class="droup" name="state" id="state_client" onchange="javascript: return loadCities(this);">
                                    <option value="">{{ trans('client/profile/profile.entry_state') }}</option>
                                        @if(isset($arr_client_details['country_details']['states']) && sizeof($arr_client_details['country_details']['states'])>0)
                                        @foreach($arr_client_details['country_details']['states'] as $states)
                                        <option value="{{isset($states['id'])?$states['id']:""}}"
                                        @if($states['id']==$arr_client_details['state'])
                                        selected="true" 
                                        @endif
                                        >
                                        {{isset($states['state_name'])?$states['state_name']:"" }}
                                    </option>
                                    @endforeach
                                    @endif
                                </select>
                                <span class='error'>{{ $errors->first('state') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12  col-md-6 col-lg-6">
                    <div class="user-box">
                        <div class="p-control-label">{{ trans('client/profile/profile.text_city') }}</div>
                        <div class="droup-select">
                            <select class="droup" name="city" id="city_client">
                                <option value="">{{ trans('client/profile/profile.entry_city') }}</option>
                                    @if(isset($arr_client_details['state_details']['cities']) && sizeof($arr_client_details['state_details']['cities'])>0)
                                    @foreach($arr_client_details['state_details']['cities'] as $cities)
                                    <option value="{{isset($cities['id'])?$cities['id']:""}}" 
                                    @if($cities['id']==$arr_client_details['city'])
                                    selected="true" 
                                    @endif
                                    >
                                    {{isset($cities['city_name'])?$cities['city_name']:"" }}
                                </option>
                                @endforeach
                                @endif
                            </select>
                            <span class='error'>{{ $errors->first('city') }}</span>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <?php 
                    $zip = '';
                    if( isset($arr_client_details['zip']) && $arr_client_details['zip'] != "" )
                    {
                        $zip = $arr_client_details['zip'];                     
                    }
                    else if (old('zip') != "")
                    {
                        $zip = old('zip');                     
                    }
                ?>

                <div class="col-sm-12  col-md-6 col-lg-6">
                    <div class="user-box">
                        <div class="p-control-label">{{ trans('client/profile/profile.text_postal_code') }}<span>*</span></div>
                        <div class="input-name"><input type="text" class="clint-input" placeholder="{{ trans('client/profile/profile.entry_postal_code') }}" name="zip" id="zip" value="{{$zip}}" data-rule-required="true" data-rule-minlength="4" data-rule-maxlength="17" onkeyup="return chk_validation(this);"/>
                            <span class='error'>{{ $errors->first('zip') }}</span>                                                            
                        </div>
                    </div>
                </div>

                <?php 
                    $vat_number = '';
                    if( isset($arr_client_details['vat_number']) && $arr_client_details['vat_number'] != "" )
                    {
                        $vat_number = $arr_client_details['vat_number'];                     
                    }
                    else if (old('vat_number') != "")
                    {
                        $vat_number = old('vat_number');                     
                    }
                ?>
                @if(isset($arr_client_details['user_type']) && $arr_client_details['user_type'] == 'business')
                    <div class="col-sm-12  col-md-6 col-lg-6">
                        <div class="user-box">
                            <div class="p-control-label">Vat No./ Tax Id / GST No.<span>*</span></div>
                            <div class="input-name"><input type="text" class="clint-input" placeholder="{{ trans('client/profile/profile.entry_vat_number') }}" name="vat_number" id="vat_number" value="{{$vat_number}}" data-rule-required="true" data-rule-maxlength="20" onkeyup="return chk_validation(this);"/>
                                <span class='error'>{{ $errors->first('vat_number') }}</span>                                                            
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                @endif


                <?php 
                    $address = '';
                    if( isset($arr_client_details['address']) && $arr_client_details['address'] != "" )
                    {
                        $address = $arr_client_details['address'];                     
                    }
                    else if (old('address') != "")
                    {
                        $address = old('address');                     
                    }
                ?>
                <div class="col-sm-12  col-md-6 col-lg-6">
                    <div class="user-box">
                        <div class="p-control-label">{{ trans('client/profile/profile.text_spoken_languages') }}<span>*</span></div>
                        <div class="input-name select_multiple">
                            <div class="droup-select ">
                                <select class="droup" name="spoken_languages[]" data-rule-required="true" id="spoken_languages" multiple="multiple" >
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12  col-md-6 col-lg-6">
                    <div class="user-box">
                        <div class="p-control-label">{{ trans('client/profile/profile.text_time_zone') }}<span>*</span></div>
                        <div class="input-name">
                            <div class="droup-select">
                                <select class="droup" name="timezone" id="timezone"  data-rule-required="true">
                                    <option value="">{{ trans('client/profile/profile.entry_time_zone') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <?php 
                    $company_name = '';
                    if( isset($arr_client_details['company_name']) && $arr_client_details['company_name'] != "" )
                    {
                        $company_name = $arr_client_details['company_name'];                     
                    }
                    else if (old('company_name') != "")
                    {
                        $company_name = old('company_name');                     
                    }
                ?>
                @if(isset($arr_client_details['user_type']) && $arr_client_details['user_type'] == 'business')
                <div class="col-sm-12  col-md-6 col-lg-6">
                    <div class="user-box">
                        <div class="p-control-label">{{ trans('client/profile/profile.text_compny_name') }}<span>*</span></div>
                        <div class="input-name"><input type="text" class="clint-input" placeholder="{{ trans('client/profile/profile.entry_compny_name') }}" data-rule-required="true" name="company_name" id="company_name" value="{{$company_name}}"/>                                                            </div>
                    </div>
                </div>
                @endif


                <?php 
                    $owner_name = '';
                    if( isset($arr_client_details['owner_name']) && $arr_client_details['owner_name'] != "" )
                    {
                        $owner_name = $arr_client_details['owner_name'];                     
                    }
                    else if (old('owner_name') != "")
                    {
                        $owner_name = old('owner_name');                     
                    }
                ?>

                @if(isset($arr_client_details['user_type']) && $arr_client_details['user_type'] == 'business')
                    <div class="col-sm-12  col-md-6 col-lg-6">
                        <div class="user-box">
                            <div class="p-control-label">{{ trans('client/profile/profile.text_owner_name') }}</div>
                            <div class="input-name"><input type="text" class="clint-input" placeholder="{{ trans('client/profile/profile.entry_owner_name') }}" name="owner_name" id="owner_name" value="{{$owner_name}}" onkeyup="return chk_validation(this);"/>
                            </div>
                        </div>
                    </div>
                @endif

                <?php 
                    $website = '';
                    if( isset($arr_client_details['website']) && $arr_client_details['website'] != "" )
                    {
                        $website = $arr_client_details['website'];                     
                    }
                    else if (old('website') != "")
                    {
                        $website = old('website');                     
                    }
                ?>

                @if(isset($arr_client_details['user_type']) && $arr_client_details['user_type'] == 'business')
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="user-box hidden-sm">
                        <div class="p-control-label">{{ trans('client/profile/profile.text_website') }}</div>
                        <div class="input-name">
                            <input type="text" class="clint-input" placeholder="{{ trans('client/profile/profile.entry_website') }}" name="website" id="website" value="{{$website}}" data-rule-url=""/>                                                            
                        </div>
                    </div>  
                </div>  
                @endif


                <div class="clearfix"></div>

                <?php 
                    $profile_summary = '';
                    if( isset($arr_client_details['profile_summary']) && $arr_client_details['profile_summary'] != "" )
                    {
                        $profile_summary = $arr_client_details['profile_summary'];                     
                    }
                    else if (old('profile_summary') != "")
                    {
                        $profile_summary = old('profile_summary');                     
                    }
                ?>
                <input type="hidden" name="user_type" value="{{$arr_client_details['user_type'] or ''}}"> 
                <input type="hidden" name="currency_symbol" id="currency_symbol"> 

                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="user-box">
                        <div class="p-control-label">{{ trans('client/profile/profile.text_about_you') }}</div>
                        <div class="input-name"><textarea style="padding: 5px;" rows="" cols="" type="text" class="client-taxtarea" placeholder="{{ trans('client/profile/profile.entry_about_you') }}" name="profile_summary" id="profile_summary"  data-rule-maxlength="500">{{$profile_summary}}</textarea> </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12"><button id="cupdate" class="normal-btn pull-right" type="submit">{{ trans('client/profile/profile.text_update') }}</button>
                </div>
            </div>
        </div>
    </form>


    <div class="right_side_section min-height change-password-profile-seciton white-wrapper">
        <div class="head_grn">{{ trans('client/profile/change_password.text_heading') }}</div>      
        <form action="{{url('/client/update_password')}}" method="POST" id="form-change-password" name="form-change-password">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-lg-12">
                    <div class="change-pwd-form">
                        <div class="user-box">
                            <div class="p-control-label">{{ trans('client/profile/change_password.text_current_password') }}:</div>
                            <div class="input-name"><input type="password" placeholder="{{ trans('client/profile/change_password.entry_current_password') }}" class="clint-input" name="current_password" id="current_password" data-rule-required='true'></div>
                            <span class='error'>{{ $errors->first('current_password') }}</span>
                        </div>
                        <div class="user-box">
                            <div class="p-control-label">{{ trans('client/profile/change_password.text_new_password') }}:</div>
                            <div class="input-name"><input type="password" placeholder="{{ trans('client/profile/change_password.entry_new_password') }}" class="clint-input" name="password" id="password"></div>
                            <span class='error'>{{ $errors->first('password') }}</span>
                        </div>
                        <div class="user-box">
                            <div class="p-control-label">{{ trans('client/profile/change_password.text_confirm_password') }}:</div>
                            <div class="input-name"><input type="password" placeholder="{{ trans('client/profile/change_password.entry_confirm_password') }}" class="clint-input" name="confirm_password" id="confirm_password" data-rule-required='true' data-rule-equalto="#password"></div>
                            <span class='error'>{{ $errors->first('confirm_password') }}</span>
                        </div>
                        <button class="normal-btn pull-right" type="submit">{{ trans('client/profile/change_password.text_update') }}</button>
                    </div>
                </div>
                <div class="col-lg-3">&nbsp;</div>
            </div>
        </form>
    </div>



<div id="country_blocked" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog congratulations-popup">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 id="alert-modal-title" class="modal-title text-center">Oops!</h4>
            </div>
            <div class="modal-body">
                <div id="alert-modal-body" class="modal-body">
                    <span class="you-may-need-message">We currently are not able to process any payments for persons living in the selected country. We are very sorry! Please check our website from time to time. As changes in regulations happen we would be very happy to welcome you to our website soon again.</span>
                </div>
            </div>                
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

</br></br>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $.validator.addMethod("pwcheck",
            function(value, element) {
                return /^^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/.test(value);
            });
    });

    $("#form-change-password").validate({
        errorElement: 'span',
        rules: {
            current_password: 
            {
                required: true,
                pwcheck: true,
                minlength: 8,
            },
            password: 
            {
                required: true,
                pwcheck: true,
                minlength: 8,

            },
            confirm_password: 
            {
                required: true,
                pwcheck: true,
                minlength: 8,

            },
        },
        @if(isset($selected_lang) && App::isLocale('de'))
        messages: 
        {
            current_password:
            {
                required: "Dieses Feld ist ein Pflichtfeld..",
                pwcheck: "Das Passwort muss in Großbuchstaben haben, Kleinschreibung und eine Ziffer",
                minlength:"Passwort erforderlich mindestens 8 Zeichen.",

            },
            password:
            {
                required: "Dieses Feld ist ein Pflichtfeld.",
                pwcheck: "Das Passwort muss in Großbuchstaben haben, Kleinschreibung und eine Ziffer.",
                minlength:"Passwort erforderlich mindestens 8 Zeichen.",

            },
            confirm_password: 
            {
                required: "Dieses Feld ist ein Pflichtfeld.",
                pwcheck: "Das Passwort muss in Großbuchstaben haben, Kleinschreibung und eine Ziffer.",
                minlength:"Passwort erforderlich mindestens 8 Zeichen.",
            },
        }
        @else

        messages: 
        {
            current_password:
            {
                required: "This field is required.",
                pwcheck: "Password must have uppercase lowercase and one digit.",
                minlength:"Password required minumum 8 characters.",

            },
            password:
            {
                required: "This field is required.",
                pwcheck: "Password must have uppercase,lowercase and one digit.",
                minlength:"Password required minumum 8 characters.",

            },
            confirm_password: 
            {
                required: "This field is required.",
                pwcheck: "Password must have uppercase,lowercase and one digit.",
                minlength:"Password required minumum 8 characters.",
            }
        }

        @endif
    });


</script>

<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/timezones/dist/timezones.full.js"></script>
<link href="{{url('/public')}}/assets/select2/dist/css/select2.min.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="{{url('/public')}}/assets/select2/dist/js/select2.full.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/languages/all_langs.js"></script>
<!--file upload-->
<script type="text/javascript" src="{{url('/public')}}/front/js/bootstrap-fileupload.js"></script>
<link href="{{url('/public')}}/front/css/bootstrap-fileupload.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="{{url('/public')}}/front/js/bootstrap-select.min.js"></script>
<link href="{{url('/public')}}/front/css/bootstrap-select.min.css" type="text/css" rel="stylesheet" />

<script type="text/javascript">

    function change_currency(ref)
    {
        var wallet_bal = '{{$wallet_bal or '0'}}';
        var ongoing_project_count = '{{$ongoing_project_count or '0'}}';
        var open_contest_count = '{{$open_contest_count or '0'}}';
        var currency_symbol = $('option:selected',ref).attr('data-symbol');
        var selected_currency = '{{$selected_currency or ''}}';
        $('#currency_symbol').val(currency_symbol);

/* if(ongoing_project_count != '0' || open_contest_count!='0')
{
alertify.alert('Please complete or Canceled all project or Contest.');
$(ref).val(selected_currency);
return;                 
}
else if(wallet_bal > '0')
{
alertify.alert('Please payout wallet amount first.');
$(ref).val(selected_currency);
return;    
}*/


if(wallet_bal > '0')
{
    alertify.alert('Please payout wallet amount first.');
    $(ref).val(selected_currency);
    return;    
}
else if(ongoing_project_count != '0' || open_contest_count!='0')
{
    alertify.alert('Please complete or Canceled all project or Contest.');
    $(ref).val(selected_currency);
    return;                 
}
}

$("#form-client_profile").validate({
    errorElement: 'span',
    errorPlacement: function (error, element) 
    {
        if(element.attr("name") == 'phone_code')
        {
            error.insertAfter($("#phone_number"));
        }
        else if(element.attr("id") == 'spoken_languages')
        {
            error.insertAfter($(".select_multiple"));  
        }
        else
        {
            error.insertAfter(element);
        }
    },
    groups: {
        phone_number: "phone_code phone_number"
    },
    rules: {

        website: {
            url: true,
        }
    },
    messages: {
        website:'Please enter a valid URL. Example. http://www.xyz.com or https://www.xyz.com',
    }

});




//times js init
$('#timezone').timezones();

setTimezone();
//set spoken languages pass id of select box
set_languages('spoken_languages');

<?php
$spk_lang = array();
if (isset($arr_client_details['spoken_languages']) && $arr_client_details['spoken_languages']!="") 
{
    $spk_lang = explode(",", $arr_client_details['spoken_languages']);
//print_r($arr_client_details['spoken_languages']);
}
?>


jQuery("#spoken_languages").select2({
    placeholder: "Select Languages",  
});

function set_languages(control_id) 
{
    if (control_id) 
    {
        var all_lang = get_languages();

        if(all_lang)
        {
            if(typeof(all_lang) == "object")
            {  
                var option = '<option value="">Select languages</option>'; 

                <?php
                if (isset($spk_lang) && sizeof($spk_lang)>0) 
                {
                    foreach ($spk_lang as $spklang) 
                    {

                        ?>
                        var currnt_lang = '<?php echo $spklang;?>';

                        for(lang in all_lang)
                        {
                            if (currnt_lang==all_lang[lang].name) 
                            {
                             option+='<option selected value="'+all_lang[lang].name+'">'+all_lang[lang].name+'</option>';
                         }
                         else
                         {
                             option+='<option value="'+all_lang[lang].name+'">'+all_lang[lang].name+'</option>';
                         }
                     }

                     <?php    }
                 }
                 else
                 { 
                    ?>
                    for(lang in all_lang)
                    {
                        option+='<option value="'+all_lang[lang].name+'">'+all_lang[lang].name+'</option>';
                    }

                    <?php  
                }
                ?>


                jQuery('select[id="'+control_id+'"]').html(option);
            }
        }
    }
}


function setTimezone() 
{

    var selected_timezone = "{{isset($arr_client_details['timezone'])?$arr_client_details['timezone']:''}}";

    if (selected_timezone && selected_timezone!="") 
    {
        $("#timezone").val(selected_timezone);
    }

}


var arr_blocked_country = '{{ json_encode($arr_mangopay_blocked_countries) }}';

function loadStates(ref)   
{
    var selected_country = jQuery(ref).val();

    if(selected_country && selected_country!="" && selected_country!=0)
    {

        if($.inArray( selected_country.toString(), arr_blocked_country ) != -1){
            $('#country_blocked').modal('toggle');
            $(ref).val('');
            return false;
        }

        jQuery('select[id="state_client"]').find('option').remove().end().append('<option value="">Select State</option>').val('');
        jQuery('select[id="city_client"]').find('option').remove().end().append('<option value="">Select City</option>').val('');

        jQuery.ajax({
            url:'{{url("/")}}/locations/get_states/'+btoa(selected_country),
            type:'GET',
            data:'flag=true',
            dataType:'json',
            beforeSend:function()
            {
                jQuery('select[id="state_client"]').attr('readonly','readonly');
            },
            success:function(response)
            {
                if(response.status=="success")
                {
                    jQuery('select[id="state_client"]').removeAttr('readonly');

                    if(typeof(response.states) == "object")
                    {
                        var option = '<option value="">Select State</option>'; 
                        jQuery(response.states).each(function(index,state)
                        {
                            option+='<option value="'+state.id+'">'+state.state_name+'</option>';
                        });

                        jQuery('select[id="state_client"]').html(option);
                    }

                }
                return false;
            }    
        });
    }
}

function loadCities(ref)   
{
    var selected_state = jQuery(ref).val();

    if(selected_state && selected_state!="" && selected_state!=0)
    {

        jQuery('select[id="city_client"]').find('option').remove().end().append('<option value="">Select City</option>').val('');

        jQuery.ajax({
            url:'{{url("/")}}/locations/get_cities/'+btoa(selected_state),
            type:'GET',
            data:'flag=true',
            dataType:'json',
            beforeSend:function()
            {
                jQuery('select[id="city_client"]').attr('readonly','readonly');
            },
            success:function(response)
            {
                if(response.status=="success")
                {
                    jQuery('select[id="city_client"]').removeAttr('readonly');

                    if(typeof(response.cities) == "object")
                    {
                        var option = '<option value="">Select City</option>'; 
                        jQuery(response.cities).each(function(index,city)
                        {
                            option+='<option value="'+city.id+'">'+city.city_name+'</option>';
                        });

                        jQuery('select[id="city_client"]').html(option);
                    }

                }
                return false;
            }    
        });
    }
}

$('#phone_number').keypress(function(eve) 
{
    if(eve.which == 8 || eve.which == 190) 
    {
        return true;
    }
    else if ((eve.which != 46 || $(this).val().indexOf('.') != -1) && (eve.which < 48 || eve.which > 57) || (eve.which == 46 && $(this).caret().start == 0) ) 
    {
        eve.preventDefault();
    }

    /*this part is when left part of number is deleted and leaves a . in the leftmost position. For example, 33.25, then 33 is deleted*/

    $('#phone_number').keyup(function(eve) 
    {
        if($(this).val().indexOf('.') == 0) {    $(this).val($(this).val().substring(1));
        }
    });
});

/* Function for special characters not accepts.*/
function chk_validation(ref)
{
    var yourInput = $(ref).val();
    re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
    var isSplChar = re.test(yourInput);
    if(isSplChar)
    {
        var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
        $(ref).val(no_spl_char);
    }
}


$('#cupdate').click(function(){
    var user_name = $('#user_name').val();
    if(user_name != ""){
var chk_username_availablity =  chk_user_name_availablity(user_name); //"Taclient"
if(chk_username_availablity != 'Available' ){
    return false;
}
}
});

function chk_user_name_availablity(ref)
{
    $( "#form-client_profile" ).validate().element( "#user_name" );
    $('#err_user_name').hide();
    $('#err_user_name').html('');
    $('for[user_name]').html('');
    $('#user_name').css('border','1px solid red');
    var yourInput = ref;
    var site_url  = "<?php echo url('/'); ?>";
    var token     = "<?php echo csrf_token(); ?>";
    var return_val = "";
    if(yourInput != "" && yourInput.length >= '3' && yourInput.length <= '32'){
        $.ajax({
            type:"post",
            async:false,
            url:site_url+'/chk_username_availabilty',
            data:{username:yourInput, _token:token},
            success:function(result){
                return_val = result;
                $('#err_user_name').show();
                if(result != 'Available'){
                    $('#user_name').css('border','1px solid red');
                    $('#err_user_name').html(result);
                    $('for[user_name]').html(result);
                    $('#err_user_name').css('color','red');
                } else {
                    $('#user_name').css('border','1px solid green');
//$('#err_user_name').html(result);
//$('for[user_name]').html(result);
$('#err_user_name').css('color','green');
}
}
});  
        return return_val;
    }
}
</script>
@stop