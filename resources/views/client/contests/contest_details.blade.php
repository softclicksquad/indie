 @extends('client.layout.master')
  @section('main_content')

    <div class="search-project-listing-left">
        <div class="search-freelancer-user-head">
            <!-- <a href="{{ $module_url_path }}/details/{{ base64_encode($contestRec['id'])}}"> {{$contestRec['contest_title'] or '-'}} </a> -->
            <a href="{{ $module_url_path }}/details/{{ base64_encode($contestRec['id'])}}"> {{$contestRec['contest_title'] or '-'}} </a>
        </div>
        <div class="search-freelancer-user-location">
            <span class="gavel-icon"><i class="fa fa-gavel"></i></span> {{isset($contestRec['contest_entry'])?count($contestRec['contest_entry']):'0'}} {{ trans('contets_listing/listing.text_total_entries') }} <span class="freelancer-user-line">|</span>
        </div>
        @php $postes_time_ago = time_ago($contestRec['created_at']); @endphp
        <div class="search-freelancer-user-location"><i class="fa fa-clock-o"></i> {{ trans('contets_listing/listing.text_posted') }} : {{$postes_time_ago}}</div>
        <div class="clearfix"></div>
        <div class="search-project-phara">{{str_limit($contestRec['contest_description'],100)}} 
             @if(strlen(trim($contestRec['contest_description'])) > 100)
             <a class="moretext" href="{{ $module_url_path }}/details/{{ base64_encode($contestRec['id'])}}">
             {{trans('project_manager/projects/postedprojects.text_more')}}
             </a>
             @endif
        </div>
        @if(isset($contestRec['contest_skills'])  && count($contestRec['contest_skills']) > 0)
         @foreach($contestRec['contest_skills'] as $key=> $skills)
          @if(isset($skills['skill_data']['skill_name']) &&  $skills['skill_data']['skill_name'] != "")
            <li style="font-size:12px;" class="search-project-skils">{{ str_limit($skills['skill_data']['skill_name'],18) }}</li>
          @endif
         @endforeach
        @endif    
    </div>
    @endsection