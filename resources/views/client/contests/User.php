<?php 
class User { 
    function __construct() {}
    function get_users($conn) { 
        $sql       = "SELECT users.id,users.name,users.email,users.mobile_number,users.gender,users.created,users.uuid,college.college_name FROM users JOIN  college ON users.college = college.id";
        return $run_query = mysqli_query($conn,$sql); 
    }
    function get_user($conn, $id) { 
        $sql       = "SELECT * FROM `users` WHERE id = ".$id;
        $run_query = mysqli_query($conn,$sql);
        if($run_query){
	        $row = mysqli_fetch_assoc($run_query);
    	    return $row;
        }else{
        	return array();
        }
    }
    function login($conn, array $data) {

        /*if(isset($_SESSION['logged_in'])){
            throw new Exception( 'Please login admin panel in another browser, Because of current browser handling front-end user login.' );
        }*/

        $_SESSION['admin_logged_in'] = false;
        if( !empty( $data ) ){
            // Trim all the incoming data:
            $trimmed_data = array_map('trim', $data);
            
            // escape variables for security
            $email    = mysqli_real_escape_string( $conn,  $trimmed_data['email'] );
            $password = mysqli_real_escape_string( $conn,  $trimmed_data['password'] );
                
            if((!$email) || (!$password) ) {
                throw new Exception( 'LOGIN_FIELDS_MISSING' );
            }
            $password = md5( $password );
            $query  = "SELECT id, name, email, mobile_number, gender, created  FROM tbl_admin_users where email = '$email' and password = '$password' ";
            $result = mysqli_query($conn, $query);
            $data   = mysqli_fetch_assoc($result);
            $count  = mysqli_num_rows($result);
            mysqli_close($conn);
            if( $count == 1){
                $_SESSION = $data;
                $_SESSION['admin_logged_in'] = true;
                return true;
            }else{
                throw new Exception( 'LOGIN_FAIL' );
            }
        } else{
            throw new Exception( 'LOGIN_FIELDS_MISSING' );
        }
    }
    /**
     * This handle sign out process
    */
    public function logout()
    {
        session_unset();
        session_destroy();
        session_start();
        $_SESSION['success'] = 'LOGOUT_SUCCESS';
        header('Location: '.BASE_URL.'index.php');exit;
    }
} 