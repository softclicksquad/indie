
@extends('client.layout.master') @section('main_content')
<style type="text/css">
.moretext{
    color: rgb(0, 0, 0);
    text-decoration: none;
    text-transform: capitalize;
}
</style>

<div class="col-sm-7 col-md-8 col-lg-9">
    <div class="coll-listing-section">
        <div class="contest-titile">
            <div class="sm-logo d-inlin"><i style="font-size: 1.5em;" class="fa fa-trophy"></i></div>
            <span class="d-inlin">{{ trans('contets_listing/listing.text_contest')}}</span>
        </div>
        @php
          $is_contest_cancelled = false;
          if(isset($arr_contest_details['contest_status']) && isset($arr_contest_details['is_active']) && $arr_contest_details['contest_status'] == '0' && $arr_contest_details['is_active'] == '2'){
            $is_contest_cancelled = true;
          }
        @endphp
         @if(isset($arr_contest_details['contest_end_date']) && $arr_contest_details['contest_end_date'] >= date('Y-m-d') && $arr_contest_details['winner_choose'] == 'NO' && $is_contest_cancelled == false)
            <a onclick="confirmDelete(this)" style="cursor: pointer;float: right;" data-contest-id="{{ isset($arr_contest_details['id'])?base64_encode($arr_contest_details['id']):'' }}" class="black-border-btn">Cancel</a>
            <div class="clearfix"></div>
        @endif
        <br>
        <br>
        
        @include('front.common._top_contest_details')

        
        @if($arr_contest_details['is_active']!='2')
        <div class="search-grey-bx">
          <div class="clearfix"></div>
          <!-- Wallet Details -->
          @if(isset($mangopay_wallet_details) && $mangopay_wallet_details !="" && !empty($mangopay_wallet_details))
            <div style="padding:0px; margin-bottom: 15px;">
                <div class="dash-user-details" style="width: 20%;">
                    <div class="title" style="text-align:center;">Prize for Winner
                        <span>
                            {{ isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:''}}
                        </span>
                        <span>
                            {{ isset($arr_contest_details['contest_price'])?$arr_contest_details['contest_price']:0}}
                        </span>
                    </div>
                    {{-- <div>
                        <table class="table">
                            <tbody>     
                                <tr>
                                  <td>
                                    <div class="high-text-up">
                                          <span>
                                              {{ isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:''}}
                                          </span>
                                          <span style="text-align:center;">
                                              {{ isset($arr_contest_details['contest_price'])?$arr_contest_details['contest_price']:0}}
                                          </span>
                                      </div>
                                  </td>
                                    <td>
                                        <span class="card-id"> {{ isset($mangopay_wallet_details->Id)? $mangopay_wallet_details->Id:''}} </span>
                                    </td>
                                    <td>
                                        <div class="description" style="font-size: 11px;">{{isset($mangopay_wallet_details->Description)? $mangopay_wallet_details->Description:''}}</div>
                                    </td>
                                    <td style="font-size: 17px;width: 120px;">
                                        <div class="high-text-up">
                                            <span>
                                                {{ isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:''}}
                                            </span>
                                            <span class="mp-amount">
                                                {{ isset($arr_contest_details['contest_price'])?$arr_contest_details['contest_price']:0}}
                                            </span>
                                        </div>
                                        <div class="small-text-down" style="font-size: 10px;">
                                            {{trans('common/wallet/text.text_money_balance')}}
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div> --}}
                </div>
            </div>
          @endif  
          <!-- End Wallet Details -->
        </div>
        @endif


        @if(isset($arr_contest_entry_data['data']) && sizeof($arr_contest_entry_data['data'])>0)
         <div class="contest-bottom-list">
         <div class="row">  
                @php  $i = 1; @endphp
                @foreach($arr_contest_entry_data['data'] as $key=>$entry)
                @php
                $sidebar_information      = sidebar_information($entry['expert_id']);
                $profile_img_public_path  = url('/public').config('app.project.img_path.profile_image');
                $profile_img_base_path    = base_path().'/public'.config('app.project.img_path.profile_image');
                $timeline_img_public_path = url('/public').config('app.project.img_path.cover_image');
                $timeline_img_base_path   = base_path().'/public'.config('app.project.img_path.cover_image');
                @endphp
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 coll-list">
                    <div class="coll-list-block">
                        @if($entry['is_winner'] == 'YES')
                          <span class="contest-winner"><img style="cursor: default;height: 40px;" src="{{url('/public')}}/front/images/archexpertdefault/contest-winner.png"></span>   
                        @elseif($entry['sealed_entry'] == '1')
                          <span class="sealed-entry">Sealed</span>   
                        @endif
                        <div class="coll-img p-relative">
                          <!-- Image --> 
                                @if(isset($entry['contest_entry_files']))
                                    <!-- entry fist image -->

                                    <?php 
                                        $higest_rating_file = '';

                                        if(isset($entry['contest_entry_files_highest_rating']) && count($entry['contest_entry_files_highest_rating'])>0)
                                        {
                                            $higest_rating_file = $entry['contest_entry_files_highest_rating'][0]['file_no'];
                                        }
                                     ?>
                                      @foreach($entry['contest_entry_files'] as $imgkey => $img)
                                        @php $explod = explode('.',$entry['contest_entry_files'][$imgkey]['image']);@endphp 
                                        @if(isset($explod[1]) && $explod[1] == 'jpg'  ||
                                                                 $explod[1] == 'jpeg' ||
                                                                 $explod[1] == 'JPG'  ||
                                                                 $explod[1] == 'png'  ||
                                                                 $explod[1] == 'PNG'  ||
                                                                 $explod[1] == 'JPEG' 
                                           )
                                            @if($entry['contest_entry_files'][$imgkey]['file_no'] == $higest_rating_file)
                                              @if(isset($entry['contest_entry_files'][$imgkey]['image']) && $entry['contest_entry_files'][$imgkey]['image']!=null && file_exists('public/uploads/front/postcontest/send_entry/'.$entry['contest_entry_files'][$imgkey]['image']))
                                                <img src="{{url('/public/uploads/front/postcontest/send_entry/'.$entry['contest_entry_files'][$imgkey]['image'])}}" class="img-responsive" alt=""/>
                                              @else
                                                <img src="{{$timeline_img_public_path.'/resize_cache/default-371x250.jpg'}}" class="img-responsive" alt=""/>
                                              @endif
                                            @elseif(isset($entry['contest_entry_files'][$imgkey]['image']) && $entry['contest_entry_files'][$imgkey]['image']!=null && file_exists('public/uploads/front/postcontest/send_entry/'.$entry['contest_entry_files'][$imgkey]['image']))
                                                <img src="{{url('/public/uploads/front/postcontest/send_entry/'.$entry['contest_entry_files'][$imgkey]['image'])}}" class="img-responsive" alt=""/>
                                              @else
                                                <img src="{{$timeline_img_public_path.'/resize_cache/default-371x250.jpg'}}" class="img-responsive" alt=""/>
                                            @endif
                                        @endif
                                      @endforeach  
                                    <!-- end entry first image --> 

                                @endif                     
                          <!-- Image -->
                          <div class="design-title">
                            <div class="id-block">{{trans('client/contest/common.text_entry')}} {{isset($entry['entry_id'])?'#'.$entry['entry_id']:"-"}}</div>
                            <div class="avg-rating">
                                  <ul>
                                      <span class="stars">{{isset($entry['contest_entry_files_highest_rating'][0]['rating'])?$entry['contest_entry_files_highest_rating'][0]['rating']:0}}</span>
                                  </ul>
                             </div>
                          </div>
                        <div class="list-hover">
                          @if($is_contest_cancelled == false)
                            <a href="{{$module_url_path}}/show_contest_entry_details/{{base64_encode($entry['id'])}}" class="view-button">{{ trans('contets_listing/listing.text_entry_details') }}</a>
                          @else
                            <a href="javascript:void(0);" class="view-button"><i class="fa fa-lock"></i></a>

                          @endif
                        </div>
                        </div>
                        <div class="coll-details">
                                <div class="coll-person">
                                    <div class="collaboration-img">
                                        @if(isset($sidebar_information['user_details']['profile_image']) && $sidebar_information['user_details']['profile_image']!=null && file_exists($profile_img_base_path.$sidebar_information['user_details']['profile_image']))
                                         <img src="{{$profile_img_public_path.$sidebar_information['user_details']['profile_image']}}" class="img-responsive" alt=""/>
                                        @else
                                          <img src="{{$profile_img_public_path.'default_profile_image.png'}}" class="img-responsive" alt=""/>
                                        @endif
                                        
                                        @if(isset($arr_contest_details['user_details']['is_online']) && $arr_contest_details['user_details']['is_online']!='' && $arr_contest_details['user_details']['is_online']=='1')
                                          <div class="online-status"> <img src="{{url('/public/front/images/active.png')}}"   class=""></div>
                                        @else
                                          <div class="online-status"> <img src="{{url('/public/front/images/deactive.png')}}" class=""></div>
                                        @endif
                                    </div>
                                    <div class="coll-name">
                                        <h5>{{isset($sidebar_information['user_details']['first_name'])?$sidebar_information['user_details']['first_name'].'  '.str_limit($sidebar_information['user_details']['last_name'],1,'.'):""}}

                                           @if(isset($arr_contest_details['user_details']['kyc_verified']) && $arr_contest_details['user_details']['kyc_verified'] == '1')
                                          <span class="verfy-new-arrow">
                                              <a href="#" data-toggle="tooltip" title="Identity Verified"><img src="{{url('/public/front/images/verifild.png')}}" alt=""> </a>
                                          </span>
                                        @endif
                                        </h5>

                                        @if($sidebar_information['last_login_duration'] == 'Active')
                                               <span class="flag" title="{{isset($sidebar_information['expert_timezone'])?$sidebar_information['expert_timezone']:'-'}}">
                                                  <span class="flag-image"> 
                                                      @if(isset($sidebar_information['user_country_flag'])) 
                                                        @php 
                                                           echo $sidebar_information['user_country_flag']; 
                                                        @endphp 
                                                      @elseif(isset($sidebar_information['user_country'])) 
                                                        @php 
                                                          echo $sidebar_information['user_country']; 
                                                        @endphp 
                                                      @else 
                                                        @php 
                                                          echo '-'; 
                                                        @endphp 
                                                      @endif 
                                                  </span>
                                                 
                                                    <span class="during-title">
                                                    {{isset($sidebar_information['expert_timezone_without_date'])?$sidebar_information['expert_timezone_without_date']:'-'}}
                                                </span>
                                               </span>
                                        @else
                                             <span class="" title="{{$sidebar_information['last_login_full_duration']}}">
                                                <span class="flag-image"> 
                                                    @if(isset($sidebar_information['user_country_flag'])) 
                                                      @php 
                                                         echo $sidebar_information['user_country_flag']; 
                                                      @endphp 
                                                    @elseif(isset($sidebar_information['user_country'])) 
                                                      @php 
                                                        echo $sidebar_information['user_country']; 
                                                      @endphp 
                                                    @else 
                                                      @php 
                                                        echo '-'; 
                                                      @endphp 
                                                    @endif 
                                                </span>
                                                <span class="during-title">
                                                   {{$sidebar_information['last_login_duration']}}
                                                </span>
                                             </span>
                                        @endif

                                       

                                    </div>
                                </div>
                                <div class="clearfix"></div>
                        </div>
                       </div>
                  </div>
                @endforeach
         </div>
         </div>
         </div>
        @else
          <tr style="background-color: #ffffff; border: 1px solid #cccccc; display: block; margin-bottom: 20px;" align="center">
            <td colspan="2" style="width: 480px; padding: 14px 7px 14px 20px; border-top: none;">
               <div class="search-content-block">
                  <div class="no-record">
                        {{ trans('contets_listing/listing.text_entry_will_be_follow_soon')}}...
                     </div>                                                        
                 </div>
             </td>
          </tr>
        @endif
    </div>

<script type="text/javascript">
    function confirmDelete(ref) {
        alertify.confirm("Are you sure? You want to cancel contest ?", function(e) {
            if (e) {
                var contest_id = $(ref).attr('data-contest-id');
                window.location.href = "{{ $module_url_path }}" + "/delete_contest/" + contest_id;
                return true;
            } else {
                return false;
            }
        });
    }
</script>

<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script> 
@stop