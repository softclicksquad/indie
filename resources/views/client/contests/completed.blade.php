@extends('client.layout.master') @section('main_content')
<style type="text/css">
.moretext{
    color: rgb(0, 0, 0);
    text-decoration: none;
    text-transform: capitalize;
}
</style>
<div class="col-sm-7 col-md-9 col-lg-9">
  <div class="row">
    <div class="col-lg-12">
       @include('front.layout._operation_status')
       <div class="head_grn">Completed Contest</div>
    </div> 
    @if(isset($arr_completed_contests['data']) && sizeof($arr_completed_contests['data'])>0)
    @foreach($arr_completed_contests['data'] as $contestRec)
    <div class="col-lg-12">
        <div class="white-block-bg-no-padding">
            <div class="white-block-bg big-space hover">
                <div class="search-project-listing-left">
                    <div class="search-freelancer-user-head">
                        <!-- <a href="{{ $module_url_path }}/details/{{ base64_encode($contestRec['id'])}}"> {{$contestRec['contest_title'] or '-'}} </a> -->
                        <a href="{{ $module_url_path }}/details/{{ base64_encode($contestRec['id'])}}"> {{$contestRec['contest_title'] or '-'}} </a>
                    </div>
                    <div class="search-freelancer-user-location">
                        <span class="gavel-icon"><i class="fa fa-gavel"></i></span> {{isset($contestRec['contest_entry'])?count($contestRec['contest_entry']):'0'}} {{ trans('contets_listing/listing.text_total_entries') }} <span class="freelancer-user-line">|</span>
                    </div>
                    @php $postes_time_ago = time_ago($contestRec['created_at']); @endphp
                    <div class="search-freelancer-user-location">
                      <i class="fa fa-clock-o"></i> {{ trans('contets_listing/listing.text_posted') }} : {{$postes_time_ago}}
                      @if(isset($contestRec['edited_at']) && $contestRec['edited_at']!=NULL)
                        <span class="freelancer-user-line">|</span>
                      @endif
                    </div>
                    
                    @if(isset($contestRec['edited_at']) && $contestRec['edited_at']!=NULL)
                        @php $edited_time_ago = time_ago($contestRec['edited_at']); @endphp
                        <div class="search-freelancer-user-location">
                            <i class="fa fa-clock-o"></i>
                            <span class="dur-txt">Last edited : {{$edited_time_ago}}</span>
                        </div>
                    @endif

                    <div class="clearfix"></div>
                    <div class="search-project-phara">{{str_limit($contestRec['contest_description'],100)}} 
                         @if(strlen(trim($contestRec['contest_description'])) > 100)
                         <a class="moretext" href="{{ $module_url_path }}/details/{{ base64_encode($contestRec['id'])}}">
                         {{trans('project_manager/projects/postedprojects.text_more')}}
                         </a>
                         @endif
                    </div>
                    @if(isset($contestRec['contest_skills'])  && count($contestRec['contest_skills']) > 0)
                     @foreach($contestRec['contest_skills'] as $key=> $skills)
                      @if(isset($skills['skill_data']['skill_name']) &&  $skills['skill_data']['skill_name'] != "")
                        <li style="font-size:12px;" class="search-project-skils">{{ str_limit($skills['skill_data']['skill_name'],18) }}</li>
                      @endif
                     @endforeach
                    @endif    
                </div>
                <div class="search-project-listing-right contest-right">
                    <div class="search-project-right-price">
                        {{isset($contestRec['contest_currency'])?$contestRec['contest_currency']:'$'}} {{isset($contestRec['contest_price'])?number_format($contestRec['contest_price']):'0'}}
                        <span>{{trans('client/contest/post.text_prize')}}</span>
                    </div>
                    <div class="search-project-right-price">
                        @if($contestRec['winner_choose'] == 'YES')
                            <span><img style="cursor: default;height: 40px;" src="{{url('/public')}}/front/images/archexpertdefault/contest-winner.png"></span>   
                        @endif      
                    </div>
                    <div class="clearfix"></div>
                    <div class="invite-expert-btns" style="padding-top: 21px;">
                            <a class="black-border-btn" style="padding: 0px 11px;width: 100px;font-size: 16px;" href="{{ $module_url_path }}/details/{{ base64_encode($contestRec['id'])}}">{{ trans('contets_listing/listing.text_entry_details') }}</a>
                            <a onclick="confirmDelete(this)" style="cursor: pointer;" data-contest-id="{{ isset($contestRec['id'])?base64_encode($contestRec['id']):'' }}" class="black-border-btn"><i class="fa fa-trash-o"></i></a> 
                          
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    @endforeach
    @include('front.common.pagination_view', ['paginator' => $arr_pagination]) 
    @else
    <div class="col-lg-12">
        <div class="search-grey-bx">
            <div class="no-record" >
               {{trans('expert/projects/awarded_projects.text_sorry_no_record_found')}}
            </div>
        </div>
    </div>
    @endif
  </div>
</div>
<script type="text/javascript">
    function confirmDelete(ref) {
      alertify.confirm("Are you sure? You want to cancel contest ?", function (e) {
          if (e) {
              var contest_id = $(ref).attr('data-contest-id');
              window.location.href="{{ $module_url_path }}"+"/delete_contest/"+contest_id;
              return true;
          } else {
              return false;
          }
      });
    }
</script>   
@stop