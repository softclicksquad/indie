@extends('client.layout.master') @section('main_content')
<div class="col-sm-7 col-md-9 col-lg-9 background_page">
  <div class="row">
    <div class="col-lg-12">
     @include('front.layout._operation_status')
     <div class="head_grn">Expired Contest</div>
   </div> 
   @if(isset($arr_expired_contests['data']) && sizeof($arr_expired_contests['data'])>0)
   @foreach($arr_expired_contests['data'] as $contestRec)
   @if($contestRec['winner_choose'] == "NO")
   <div class="col-lg-12">
    <div class="white-block-bg-no-padding">
      <div class="white-block-bg big-space hover">
        <div class="search-project-listing-left">
          <input type="hidden" class=" contest_id_{{$contestRec['id']}}" value="{{$contestRec['id'] or '0'}}">
          <input type="hidden" class=" contest_name_{{$contestRec['id']}}" value="{{$contestRec['contest_title'] or "N/A"}}">
          <input type="hidden" class=" contest_info_{{$contestRec['id']}}" value="{{$contestRec['contest_additional_info'] or ''}}">
          <div class="search-freelancer-user-head">
            <!-- <a href="{{ $module_url_path }}/details/{{ base64_encode($contestRec['id'])}}"> {{$contestRec['contest_title'] or '-'}} </a> -->
            <a href="{{ $module_url_path }}/details/{{ base64_encode($contestRec['id'])}}"> {{$contestRec['contest_title'] or '-'}} </a>
          </div>
          <div class="search-freelancer-user-location">
            <span class="gavel-icon"><i class="fa fa-gavel"></i></span> {{isset($contestRec['contest_entry'])?count($contestRec['contest_entry']):'0'}} {{ trans('contets_listing/listing.text_total_entries') }} <span class="freelancer-user-line">|</span>
          </div>
          @php $postes_time_ago = time_ago($contestRec['created_at']); @endphp
          <div class="search-freelancer-user-location">
            <i class="fa fa-clock-o"></i> {{ trans('contets_listing/listing.text_posted') }} : {{$postes_time_ago}}
            @if(isset($contestRec['edited_at']) && $contestRec['edited_at']!=NULL)
              <span class="freelancer-user-line">|</span>
            @endif
          </div>

          @if(isset($contestRec['edited_at']) && $contestRec['edited_at']!=NULL)
              @php $edited_time_ago = time_ago($contestRec['edited_at']); @endphp
              <div class="search-freelancer-user-location">
                  <i class="fa fa-clock-o"></i>
                  <span class="dur-txt">Last edited : {{$edited_time_ago}}</span>
              </div>
          @endif

          <div class="clearfix"></div>
          <div class="search-project-phara">{{str_limit($contestRec['contest_description'],100)}} 
           @if(strlen(trim($contestRec['contest_description'])) > 100)
           <a class="moretext" href="{{ $module_url_path }}/details/{{ base64_encode($contestRec['id'])}}">
             {{trans('project_manager/projects/postedprojects.text_more')}}
           </a>
           @endif
         </div>
         @if(isset($contestRec['contest_skills'])  && count($contestRec['contest_skills']) > 0)
         @foreach($contestRec['contest_skills'] as $key=> $skills)
         @if(isset($skills['skill_data']['skill_name']) &&  $skills['skill_data']['skill_name'] != "")
         <li style="font-size:12px;" class="search-project-skils">{{ str_limit($skills['skill_data']['skill_name'],18) }}</li>
         @endif
         @endforeach
         @endif    
       </div>
       <div class="search-project-listing-right contest-right">
        <div class="search-project-right-price">
          {{isset($contestRec['contest_currency'])?$contestRec['contest_currency']:'$'}} {{isset($contestRec['contest_price'])?number_format($contestRec['contest_price']):'0'}}
          <span>{{trans('client/contest/post.text_prize')}}</span>
        </div>
        <div class="search-project-right-price">
          @if($contestRec['winner_choose'] == 'YES')
          <span><img style="cursor: default;height: 40px;" src="{{url('/public')}}/front/images/archexpertdefault/contest-winner.png"></span>   
          @else
          <span id='days_left{{$contestRec["id"]}}' style="margin-top:0px;">{{trans('client/projects/ongoing_projects.text_next_expire')}} </span><span>{{trans('client/contest/posted.text_time_left')}}</span>
          <?php 
          $timeFirst      = strtotime(date('Y-m-d H:i:s'));
          $timeSecond     = strtotime($contestRec['contest_end_date']);
          $diffInSeconds  = $timeSecond - $timeFirst;
          if($diffInSeconds <= 0){
            $diffInSeconds = '0';
          }
          ?>
          <script type="text/javascript">
            var time        = '<?php echo $diffInSeconds; ?>';
            var contestid   = '<?php echo $contestRec["id"]; ?>';
            if(time < 0) {  time = 0; }
            var c           = time; // in seconds
            var t;
            var days    = parseInt( c / 86400 ) % 365;
            var hours   = parseInt( c / 3600 ) % 24;
            var minutes = parseInt( c / 60 ) % 60;
            var seconds = c % 60;
            var d = ""; 
            var h = ""; 
            var m = "";
            var s = ""; 
            if(days     !=  ""  &&  days     !="0"){  d  =  (days+'d')     +  " ";  }
            if(hours    !=  ""  &&  hours    !="0"){  h  =  (hours+'h')    +  " ";  }
            if(minutes  !=  ""  &&  minutes  !="0"){  m  =  (minutes+'m')  +  " ";  }
            if(seconds  !=  ""  &&  seconds  !="0"){  s  =  (seconds+'s')  +  " ";  }
            
            if(d != "" && h !=""){
              var result    = d + h;
            }else if(h != "" && m !=""){
              var result    = h + m;
            }else if(m !=""){
              var result    = m;
            }else if(s !=""){
              var result    = s;
            }
            if(result == "NaN:NaN:NaN:NaN"){ 
              $('#days_left'+contestid).html('{{trans('client/projects/ongoing_projects.text_next_expire')}}');
              $('#days_left'+contestid).css('color','red');
            } else {
              $('#days_left'+contestid).html(result);
              $('#days_left'+contestid).css('font-size','16px');
              $('#days_left'+contestid).css('color','green');
              $('#days_left'+contestid).css('font-weight','bold');
            }
            if(c == 0 ){
              $('#days_left'+contestid).html('{{trans('client/projects/ongoing_projects.text_next_expire')}}');
              $('#days_left'+contestid).css('color','red');
            }
            c = c - 1;
          </script> 
          @endif      
        </div>
        <div class="clearfix"></div>
        <div class="invite-expert-btns" style="padding-top: 21px;">
          {{-- @if(isset($contestRec['contest_end_date']) && $contestRec['contest_end_date'] >= date('Y-m-d')) --}}
            {{-- @if(empty($contestRec['contest_entry']) && count($contestRec['contest_entry']) == 0 ) --}}
              <a class="black-border-btn" style="padding: 0px 11px;width: 100px;font-size: 16px;" href="{{ $module_url_path }}/details/{{ base64_encode($contestRec['id'])}}">{{ trans('contets_listing/listing.text_entry_details') }}</a>
              <a class="black-border-btn" style="padding: 0px 11px;width: 100px;font-size: 16px;" href="{{ $module_url_path }}/post?id={{ base64_encode($contestRec['id'])}}">Repost</a>
              {{-- <a href="{{$module_url_path}}/edit/{{ base64_encode($contestRec['id'])}}" class="black-border-btn"><i class="fa fa-edit"></i></a>  --}}
              <a onclick="confirmDelete(this)" style="cursor: pointer;" data-contest-id="{{ isset($contestRec['id'])?base64_encode($contestRec['id']):'' }}" class="black-border-btn"><i class="fa fa-trash-o"></i></a> 
            {{-- @else
              <a class="black-border-btn" style="padding: 0px 11px;width: 190px;font-size: 16px;" href="{{ $module_url_path }}/details/{{ base64_encode($contestRec['id'])}}">{{ trans('contets_listing/listing.text_entry_details') }}</a>
            @endif --}}
            {{-- @if(count($contestRec['contest_entry']) == 1) --}}
            @if($diffInSeconds > 0)
              @if($contestRec['contest_additional_info'] == '')
                {{-- <a class="black-border-btn add-info-btn" style="padding: 0px 11px;width: 190px;font-size: 16px;"  onclick="openForm()" id="{{$contestRec['id']}}"><font size="1">Add Addition Information</font></a> --}}
              @else
                {{-- <a class="black-border-btn add-info-btn" style="padding: 0px 11px;width: 190px;font-size: 16px;"  onclick="openForm()" id="{{$contestRec['id']}}"><font size="1">Edit Addition Information</font></a> --}}
              @endif
            @endif
          {{-- @endif --}}

        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</div>
@endif
@endforeach
@include('front.common.pagination_view', ['paginator' => $arr_pagination]) 
@else
<div class="col-lg-12">
  <div class="search-grey-bx">
    <div class="no-record" >
     {{trans('expert/projects/awarded_projects.text_sorry_no_record_found')}}
   </div>
 </div>
</div>
@endif
</div>
</div>
@include('client.contests.additional_info')
<script type="text/javascript">
  function confirmDelete(ref) {
    alertify.confirm("Are you sure? You want to cancel contest", function (e) {
        if (e) {
            var contest_id = $(ref).attr('data-contest-id');
            window.location.href="{{ $module_url_path }}"+"/delete_contest/"+contest_id;
            return true;
        } else {
            return false;
        }
    });
  }
</script>   
@stop