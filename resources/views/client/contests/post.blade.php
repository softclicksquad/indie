@extends('front.layout.auth')                
@section('main_content')

<div class="gray-wrapper">
    <div class="container">
        <div class="post-contest">
            <div class="contest-title">
                <h2>{{trans('client/contest/post.text_contest')}}</h2>
                <p>{{trans('client/contest/post.text_contest_post_msg')}}</p>
            </div>
            <form action="{{ url($module_url_path) }}/create" method="POST" class="form-post-contests" enctype="multipart/form-data" files ="true" id="post_contest">
              {{ csrf_field() }}
              <div class="white-wrapper">
                @include('front.layout._operation_status')
                 <div class="row">
                     <div class="col-lg-6">
                         <div class="user-box">
                           <div class="p-control-label">{{trans('client/contest/post.text_what_do_you_need')}}</div>
                           <div class="input-name">
                              <div class="droup-select">
                                  <select class="droup getcat mrns tp-margn" data-rule-required="true" name="category" id="category">
                                      <option value="">{{trans('client/projects/post.text_select_category')}}</option>
                                      @if(isset($arr_categories) && sizeof($arr_categories)>0)
                                      @foreach($arr_categories as $categories)
                                      <option value="{{$categories['id']}}" 
                                      @if(old('category')==$categories['id'] || isset($arr_repost_contest['category_id']) && $arr_repost_contest['category_id']==$categories['id']) selected="" @endif>
                                      @if(isset($categories['category_title'])){{$categories['category_title']}}@endif</option>
                                      @endforeach
                                      @endif
                                  </select>
                                  <span class='error'>{{ $errors->first('category') }}</span>
                              </div>
                           </div>
                        </div>
                    </div>

                      <div class="col-lg-6">
                        <div class="user-box">
                          <div class="p-control-label">Subcategory</div>
                            <div class="droup-select">
                              <select class="droup subcategory mrns tp-margn" data-rule-required="true" name="sub_category" id="sub_category">
                              </select>
                              <span class='error'>{{ $errors->first('sub_category') }}</span>
                            </div>
                        </div>
                      </div>

                     <div class="col-lg-12">
                        <div class="user-box character-input">
                              <div class="p-control-label">{{trans('client/contest/post.text_title')}}</div>
                              <input name="contest_name" value="{{ isset($arr_repost_contest['contest_title']) ? $arr_repost_contest['contest_title'] : old('contest_name') }}" id="contest_name" class="input-lsit beginningSpace_restrict" placeholder="{{trans('client/contest/post.text_contest_title_placeholder')}}" data-rule-required="true" type="text">
                              <span class='error'>{{ $errors->first('contest_name') }}</span>
                        </div>
                     </div>
                     <div class="col-lg-12">
                        <div class="user-box">
                              <div class="p-control-label">{{trans('client/contest/post.text_contest_description')}}</div>
                              <h6 class="txt-edit" id="contest_description_msg">
                                {{trans('client/projects/post.text_project_limit_description')}}
                              </h6>
                              <textarea id="cont_description" name="cont_description" data-rule-required="true" data-txt_gramm_id="21e619fb-536d-f8c4-4166-76a46ac5edce" data-rule-maxlength="1000" rows="10" cols="30" onkeyup="javascript: return textCounter(this,1000);" rows="7" cols="" type="text" class="text-area beginningSpace_restrict" placeholder="{{trans('client/contest/post.text_contest_description_placeholder')}}">{{ isset($arr_repost_contest['contest_description']) ? $arr_repost_contest['contest_description'] : old('cont_description') }}</textarea> 
                              <span class='error'>{{ $errors->first('cont_description') }}</span>
                        </div>
                     </div>
                     @php $skills_arr        = []; @endphp
                     @if(isset($arr_repost_contest['contest_skills']))
                        @foreach($arr_repost_contest['contest_skills'] as $contest_skills)
                          @php $skills_arr[] = $contest_skills['skill_id']; @endphp
                        @endforeach
                     @endif
                     <div class="col-lg-12">
                       <div class="user-box">
                         <div class="p-control-label">{{trans('client/projects/ongoing_projects.text_skills')}} 
                         <span class="small-label">(Optional)</span></div>
                         <div class="input-name">
                          <div class="droup-select multiselect-block">
                            <select name="contest_skills[]" id="contest_skills" class="droup" multiple="multiple">
                             <option value="">{{trans('client/projects/post.text_select_skills_needed')}}</option>
                             @if(isset($arr_skills) && sizeof($arr_skills)>0)
                             @foreach($arr_skills as $skills)
                             @if(isset($skills['skill_name']))
                             <option value="{{$skills['id']}}" @if(old('contest_skills')==$skills['id'] || in_array($skills['id'] , $skills_arr)) selected=""  @endif>{{ $skills['skill_name']}}</option>
                             @endif
                             @endforeach
                             @endif
                           </select>
                           <span class='error' id="skill_name_error">{{ $errors->first('contest_skills') }}</span>
                         </div>
                         <div class="clr"></div>
                         <div class="err_contest_skills">
                         </div>
                       </div>
                     </div>
                   </div>
                   <!-- <div class="col-lg-12">
                    <div class="user-box">
                      <div class="p-control-label">{{trans('client/contest/post.text_contest_additonal_information')}}</div>
                      <h6 class="txt-edit" id="cont_additional_info_msg">
                        {{trans('client/projects/post.text_project_limit_addtional_info')}}
                      </h6>
                      <textarea id="cont_additional_info" name="cont_additional_info" value="{{old('cont_additional_info')}}" data-rule-required="false" data-rule-maxlength="1000" rows="10" cols="30" onkeyup="javascript: return textCounterForAddInfo(this,1000);" rows="7" cols="" type="text" class="text-area beginningSpace_restrict" placeholder="{{trans('client/contest/post.text_contest_addtional_info_placeholder')}}">{{old('cont_additional_info')}}</textarea> 
                      <span class='error'>{{ $errors->first('cont_additional_info') }}</span>
                    </div>
                  </div> -->
                </div>
     
                <div class="">
                 <div class="row">
                   <div class="col-lg-6">
                             <div class="user-box">
                                   <div class="p-control-label">{{trans('client/contest/post.text_prize')}} <span class="small-label">( <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span> {{trans('client/contest/post.text_prize_msg')}})</span></div>
                                   <div class="prize-box">
                                       <div class="droup-select" style="width: 153px; z-index: 9;">
                                             <select name="contest_currency" id="contest_currency" class="droup mrns tp-margn" data-rule-required="true" onchange="return set_minimum_currency_value1(this)">
                                                @if(isset($arr_contest_currency) && sizeof($arr_contest_currency)>0)
                                                <option value="">{{trans('client/projects/post.text_select_currency')}}</option>
                                                  @foreach($arr_contest_currency as $key => $currency)
                                                    <?php
                                                        $selected = '';
                                                        if( isset($arr_repost_contest['contest_currency']) && $arr_repost_contest['contest_currency'] == $currency['currency_code'] ){
                                                          $selected = 'selected';
                                                        } else if((isset($arr_data_bal['currency'][0]) && $arr_data_bal['currency'][0] == $currency['currency_code'])){
                                                          $selected = 'selected';
                                                        }
                                                    ?>
                                                    <option value="{{$currency['currency_code']}}" {{ $selected }}>
                                                    {{$currency['currency_code']}} ( {{ isset($currency['description'])?$currency['description']:'' }} )
                                                    </option>
                                                  @endforeach
                                                @endif
                                             </select>
                                             <span class='error'><!-- {{ $errors->first('contest_currency') }} --></span>
                                       </div>
                                       <input type="text" id="contest_price" value="{{isset($arr_repost_contest['contest_price']) ? $arr_repost_contest['contest_price'] : old('contest_price' )}}" name="contest_price" class="clint-input char_restrict space_restrict" value="0" data-rule-required="true" placeholder="{{trans('client/contest/post.text_contest_price_placeholder')}}">
                                       <span class='contest_price_message' style="color:red;"></span>
                                       <span class='error'>{{ $errors->first('contest_price') }}</span>
                                       <span class="contest-price-info">The higher the prize, the more highly qualified experts will participate in the contest and you will receive more entries.</span>
                                   </div>
                             </div>
                         </div> 

                         @php
                          $created_at       = isset($arr_repost_contest['created_at']) ? date('Y-m-d',strtotime($arr_repost_contest['created_at'])) : '';
                          $contest_end_date = isset($arr_repost_contest['contest_end_date']) ? date('Y-m-d',strtotime($arr_repost_contest['contest_end_date'])) : '';
                          $array_diff       = get_array_diff_between_two_date($created_at,$contest_end_date);
                        @endphp

                         <div class="col-lg-6">
                             <div class="user-box character-input">
                                   <div class="p-control-label">{{trans('client/contest/post.text_contest_end_date')}}</div>
                                   <div class="calender-input droup-select">

                                   {{-- <input id="contest_end_date" name="contest_end_date" value="{{old('contest_end_date')}}" type="text" class="clint-input cal-date" placeholder="{{trans('client/contest/post.text_contest_date_placeholder')}}" data-rule-required="true"> --}}
                                   <select name="contest_end_date" id="contest_end_date" class="droup mrns tp-margn" data-rule-required="true" >
                                     <option value="">{{trans('client/contest/post.text_contest_date_placeholder')}}</option>
                                     <option @if(isset($array_diff['days']) && $array_diff['days'] == 7) selected="" @endif value="7">7 Days</option>
                                     <option @if(isset($array_diff['days']) && $array_diff['days'] == 14) selected="" @endif value="14">14 Days</option>
                                     <option @if(isset($array_diff['months']) && $array_diff['months'] == 1) selected="" @endif value="30">30 Days</option>

                                  </select>

                                   </div>
                                   <span class='error'>{{ $errors->first('contest_end_date') }}</span>
                             </div>
                         </div>
                     </div>
                 </div>
                 <div class="row">
                  <div class="col-lg-12">
                    <div class="user-box character-input">
                      <div class="p-control-label">{{trans('client/contest/post.text_contest_attachments')}}</div>
                              <!-- <div class="attachment-block">
                                  <div class="update-pic-btns">
                                      <button href="#" class="up-btn"><i class="fa fa-cloud-upload"></i> {{trans('client/contest/post.text_contest_upload')}}</button>
                                      <input id="logo-id" onchange="Upload()" name="contest_attachment"  type="file" class="attachment_upload">
                                  </div>
                                  <div class="clearfix"></div>
                                  <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span> <div class="note">{{trans('client/contest/post.text_contest_attachments_msg')}}</div>
                                  <div class="valide_upload"></div>
                                  <div class="clearfix"></div>
                                  <div class="file-img"><img src="" class="img-responsive img-preview" alt=""/></div>
                              </div> -->

                              {{-- <div class="photo-gallrey-section">
                                  <div class="change-pass-main margi-less">
                                      <div class="add-busine-multi">
                                          <span data-multiupload="5">
                                              <span data-multiupload-holder id="data-multiupload-holder"></span>
                                              <span class="upload-photo">
                                                  <img src="{{url('/public')}}/front/images/plus-img.png" class="img-responsive" alt="" />
                                                  <input data-multiupload-src class="upload_pic_btn" type="file" multiple="" name="project_documents[]" id="file">
                                                  <span data-multiupload-fileinputs></span>
                                              </span>
                                          </span>
                                          <div class="clerfix"></div>
                                      </div>
                                      <div class="clearfix"></div>
                                      <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span> {{trans('client/contest/post.text_contest_attachments_msg')}}
                                  </div>
                              </div> --}}

                        <div class="picture-div new-picture" style="" id="pictures_div">
                          <div class="update-pro-img-main"> 
                              <div class="lab_img" id="lab_1">
                                 <div class="col-sm-12 col-lg-12 col-lg-12" style="float:right;">                                       
                                    <span>
                                    <a href="javascript:void(0);" id='remove_project' class="remove_project" style="display:none;" >
                                    <span class="glyphicon glyphicon-minus-sign" style="font-size: 20px;"></span>
                                    </a>
                                    </span>
                                 </div>
                                 <div class="" id="add_lab_div">
                                    <div class="add_pht upload-pic loc_add_pht" id="div_blank" onclick="return addpictures(this)"  style="height: 120px;width: 120px; float: left;"> 
                                      <img src="{{url('/public')}}/front/images/plus-img.png" alt="user pic"  style="width:100%;height:100%;" /></div>
                                    <div class="show_photos" id="show_photos" style="width: auto; display: initial;float: none;"></div>
                                    <div id="div_hidden_photo_list" class="div_hidden_photo_list">
                                       <input type="file" name="contest_attachments[]" id="contest_attachments" class="contest_attachments" style="display:none" multiple="multiple" />
                                    </div>
                                 </div>
                              </div>
                           </div>                       
                              <input type="hidden" name="file_name_lab" id="file_name_lab"  >
                               
                          <div class="clearfix"></div>                     
                          <div class="error-red" id="err_other_image"></div>                                          
                        </div>
                        <br>
                        <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span> {{trans('client/contest/post.text_contest_attachments_msg')}}
                    </div>
                  </div>
                  <div class="clr"></div>

                  @if((isset($arr_client_details['first_name']) && $arr_client_details['first_name'] == "") || (isset($arr_client_details['last_name']) && $arr_client_details['last_name'] == ""))
                    @if(isset($arr_client_details['user_details']['mp_wallet_created']) && $arr_client_details['user_details']['mp_wallet_created']!='Yes')
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <h3>Profile Details</h3>
                            @php
                                $first_name = '';
                                if( isset($arr_client_details['first_name']) && $arr_client_details['first_name'] != "" )
                                {
                                    $first_name = $arr_client_details['first_name'];                     
                                }
                                else if (old('first_name') != "")
                                {
                                    $first_name = old('first_name');                     
                                }
                                $last_name = '';
                                if( isset($arr_client_details['last_name']) && $arr_client_details['last_name'] != "" )
                                {
                                    $last_name = $arr_client_details['last_name'];                     
                                }
                                else if (old('last_name') != "")
                                {
                                    $last_name = old('last_name');                     
                                }

                                $vat_number = '';
                                if( isset($arr_client_details['vat_number']) && $arr_client_details['vat_number'] != "" )
                                {
                                    $vat_number = $arr_client_details['vat_number'];                     
                                }
                                else if (old('vat_number') != "")
                                {
                                    $vat_number = old('vat_number');                     
                                }
                            @endphp
                            <div class="col-sm-12  col-md-6 col-lg-6">
                                <div class="user-box">
                                    <div class="p-control-label">{{ trans('client/profile/profile.text_first_name') }}<span>*</span></div>
                                    <div class="input-name">
                                        <input type="text" class="clint-input" placeholder="{{ trans('client/profile/profile.entry_first_name') }}" name="first_name" id="first_name" value="{{$first_name}}" data-rule-required="true" data-rule-maxlength="250" onkeyup="return chk_validation(this);"/>
                                        <span class='error'>{{ $errors->first('first_name') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="user-box">
                                    <div class="p-control-label">{{ trans('client/profile/profile.text_last_name') }}<span>*</span></div>
                                    <div class="input-name"><input type="text" class="clint-input" placeholder="{{ trans('client/profile/profile.entry_last_name') }}" name="last_name" id="last_name" value="{{$last_name}}" data-rule-required="true" data-rule-maxlength="250" onkeyup="return chk_validation(this);"/>
                                        <span class='error'>{{ $errors->first('last_name') }}</span>
                                    </div>
                                </div>
                            </div>

                            @if(isset($arr_client_details['user_type']) && $arr_client_details['user_type'] == 'business')
                                <div class="col-sm-12  col-md-6 col-lg-6">
                                    <div class="user-box">
                                        <div class="p-control-label">Vat No./ Tax Id / GST No.(If applicable)<span></span></div>
                                        <div class="input-name"><input type="text" class="clint-input" placeholder="{{ trans('client/profile/profile.entry_vat_number') }}" name="vat_number" id="vat_number" value="{{$vat_number}}" data-rule-required="true" data-rule-maxlength="20" onkeyup="return chk_validation(this);"/>
                                            <span class='error'>{{ $errors->first('vat_number') }}</span>                                                            
                                        </div>
                                    </div>
                                </div>
                            @endif

                        </div>
                        <div class="clr"></div>
                    @endif
                  @endif
          
                  <div class="col-lg-12">&nbsp;</div>
                  <div class="clearfix"></div>
                  <div class="col-lg-12">
                    <button type="submit" class="black-btn" id="post-a-contest">
                      {{trans('client/contest/post.text_post_a_contest')}}
                    </button>
                  </div>
                 </div>
              </div>
            </form>
        </div>
    </div>
</div>
<!-- custom scrollbars plugin -->
<link href="{{url('/public')}}/front/css/select2.min.css" rel="stylesheet"/>
<script src="{{url('/public')}}/front/js/select2.full.js"></script>
<link rel="stylesheet" type="text/css"  href="{{url('/public')}}/front/css/jquery-ui.css"/>
<script src="{{url('/public')}}/front/js/jquery-ui.js" type="text/javascript"></script>

<script type="text/javascript">
  function check_profile_fill_or_not()
  {
     var token     = "<?php echo csrf_token(); ?>";
     var redirect_url = "{{url('/client/profile')}}";
     var url="{{url('/check_profile_filled')}}?type='contest'";
     $.ajax({
        type: 'GET',
        contentType: 'application/x-www-form-urlencoded',
        url:url,
        headers: {
        'X-CSRF-TOKEN': token
        },
        success: function(response) {
          console.log(response);
          if(response=='success')
          {
            $('#post_contest').submit();
          }
          else
          {
            window.open(redirect_url, "_blank");
          }
        }
      });  
  }
   $(".form-post-contests").validate({
         errorElement: 'span',
         errorPlacement: function (error, element) 
         {
            if(element.attr("name") == 'check_terms')
            {  
               error.appendTo($("#_err_term"));  
            }
            else if(element.attr("id") == 'contest_skills')
            {
               error.appendTo($(".err_contest_skills"));  
            }
            else
            {
               error.insertAfter(element);
            }
         }
   });

    $(".getcat").change(function()
    {  
      var id=$(this).val();
      var site_url ="{{url('/')}}";
      var dataString = 'id='+ id;
      var token = $('input[name="_token"]').val();  
      $.ajax
      ({
          type: "POST",
          url: site_url+'/client/contest/subcatdata'+"?_token="+token,
          data: dataString,
          cache: false,
          success: function(html)
          {
           $(".subcategory").html(html);
          } 
      });   
    });

    var category_id = '{{isset($arr_repost_contest['category_id']) ? $arr_repost_contest['category_id'] : ''}}';
    var sub_category_id = '{{isset($arr_repost_contest['sub_category_id']) ? $arr_repost_contest['sub_category_id'] : ''}}';
    if(category_id!=''){
      var site_url ="{{url('/')}}";
        var dataString = 'id='+ category_id;
        var token = '{{csrf_token()}}';  
        $.ajax
        ({
            type: "POST",
            url: site_url+'/client/contest/subcatdata'+"?_token="+token+'&sub_category_id='+sub_category_id,
            data: dataString,
            cache: false,
            success: function(html)
            {
             $(".subcategory").html(html);
            } 
        });
    }

</script>
<script type="text/javascript">
/*date picker*/    
$(function() {
  /*$("#contest_end_date").datepicker({ minDate: -0, maxDate: "+1M +10D",dateFormat: 'yy-mm-dd' });   */
  $("#contest_end_date").datepicker({ minDate: +1, maxDate: "+1M",dateFormat: 'yy-mm-dd' });   
});
</script>
<!--new profile image upload demo script start-->
<script type="text/javascript">
function Upload() {
    $('.valide_upload').css('color','black');
    //Get reference of FileUpload.
    $('.img-preview').attr('src', ""); 
    var logo_id = document.getElementById("logo-id");
    //Check whether the file is valid Image.
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+()$");
    if (regex.test(logo_id.value.toLowerCase())) {
        //Check whether HTML5 is supported.
        if (typeof (logo_id.files) != "undefined") {
            var file_extension = logo_id.value.substring(logo_id.value.lastIndexOf('.')+1, logo_id.length); 
            var size = logo_id.files[0].size;
            if(size > 25000000){
               $('.valide_upload').css('color','red');
               $("[for=logo-id]").html('');
               $('.valide_upload').html("{{trans('client/projects/post.text_project_validsizeupload')}}");
               document.getElementById("logo-id").value = "";
               return false;
            }
            if(file_extension != 'jpg' && 
               file_extension != 'jpeg' &&
               file_extension != 'gif' &&
               file_extension != 'png' &&
               file_extension != 'xlsx' &&
               file_extension != 'pdf' &&
               file_extension != 'docx' &&
               file_extension != 'doc' &&
               file_extension != 'txt' &&
               file_extension != 'odt'){
               $('.valide_upload').css('color','red');
               $("[for=logo-id]").html('');
               $('.valide_upload').html("{{trans('client/projects/post.text_project_validupload')}}");
               document.getElementById("logo-id").value = "";
               return false;
            } 
            //Initiate the FileReader object.
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(logo_id.files[0]);
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();
                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;
                //Validate the File Height and Width.
                if(file_extension == 'xlsx' ||
                 file_extension == 'pdf' ||
                 file_extension == 'docx' ||
                 file_extension == 'doc' ||
                 file_extension == 'txt' ||
                 file_extension == 'odt'){
                    $('.img-preview').attr('src', site_url+'/front/images/file_formats/'+file_extension+'.png'); 
                }
                image.onload = function (e) {
                    var height = this.height;
                    var width  = this.width;
                    $('.note').css('color','#646464');
                    if(file_extension == 'jpg' || 
                       file_extension == 'jpeg' ||
                       file_extension == 'gif' ||
                       file_extension == 'png'){
                            $('.img-preview').attr('src', image.src);
                       }  
                    return true;
                };
            }
        } else {
            $('.valide_upload').css('color','red');
            $("[for=logo-id]").html('');
            $('.valide_upload').html("This browser does not support HTML5.");
            document.getElementById("logo-id").value = "";
            return false;
        }
    } else {
        $('.valide_upload').css('color','red');
        $("[for=logo-id]").html('');
        $('.valide_upload').html("{{trans('client/projects/post.text_project_validupload')}}");
        document.getElementById("logo-id").value = "";
        return false;
    }
}

</script>  
<script type="text/javascript">
function textCounter(field,maxlimit){
   var countfield = 0;
   if ( field.value.length > maxlimit ) {
     field.value = field.value.substring( 0, maxlimit );
     return false;
   } else {
      countfield = maxlimit - field.value.length;
      var message = '{{trans('client/projects/invite_experts.text_you_have_left')}} <b>'+countfield+'</b> {{trans('client/projects/invite_experts.text_characters_for_description')}}';
      jQuery('#contest_description_msg').html(message);
      return true;
   }
}
function textCounterForAddInfo(field,maxlimit){
   var countfield = 0;
   if ( field.value.length > maxlimit ) {
     field.value = field.value.substring( 0, maxlimit );
     return false;
   } else {
      countfield = maxlimit - field.value.length;
      var message = '{{trans('client/projects/invite_experts.text_you_have_left')}} <b>'+countfield+'</b> {{trans('client/projects/invite_experts.text_characters_for_additional_info')}}';
      jQuery('#cont_additional_info_msg').html(message);
      return true;
   }
}
/* Initializing multiple skills data */
$('#contest_skills').select2({
     maximumSelectionLength: 5,
     placeholder: '{{trans('client/projects/ongoing_projects.text_skills')}}',
     allowClear: true
});


</script>
<!--new profile image upload demo script end-->     

<script type="text/javascript">    
    
  var arr_job_document_formats = {!! json_encode($arr_job_document_formats) !!};

    function addpictures(ref)
    { 
        var new_images = $("input[name='contest_attachments[]']").map(function() {
               return $(this).val();
           }).get();

        var count = new_images.length;
        
        if(count > 10)
        {
            swal('U can upload maximum 10 files');
            return false;
        }            

        $('#err_other_image').html('');
        var image_id = $(ref).closest('.lab_img').attr('id');
        var length = $('.lab_img').length;
        var view_photo_cnt = jQuery('#'+image_id).find('.photo_view').length                                
        jQuery('#'+image_id).last().find( ".div_hidden_photo_list" ).last().find( "input[name='contest_attachments[]']:last" ).click(); 
        jQuery('#'+image_id).last().find( ".div_hidden_photo_list" ).last().find( "input[name='contest_attachments[]']:last" ).change(function()
        { 
            var files      = this.files;
            var exist_file = $('#file_name_lab').val();
            
            
            if(exist_file == files[0]['name']) 
            { 
                return false; 
            }
            else 
            {
                $('#file_name_lab').val(files[0]['name']);
                for (var i=0, l=files.length; i<l; i++) 
                {
                    var max_size = 5000000;
                    var current_size = files[i].size;
                    if (max_size>=current_size) 
                    {
                        var file = files[i];

                        var prjct_id = image_id.split('_');
                        jQuery('#'+image_id).find('#image'+prjct_id[1]+'_'+(view_photo_cnt+1)).attr('value',files[i]['name']);
                        var img, reader, xhr;
                        img = document.createElement("img");
                        reader = new FileReader();
                        img = new Image();      

                        var ext      =   files[i]['name'].split('.').pop();  

                        if ($.inArray(ext, arr_job_document_formats) == -1)
                        {                                          
                            swal('File type not allowed');
                            return false;
                        }
                        else
                        {
                            img.onload = function()
                            {
                                                 
                            }                            
                        }

                        reader.onload = (function (theImg) 
                        {      
                            if(ext == 'docx' || ext == 'doc' || ext == 'pdf' || ext == 'zip' || ext == 'mp3' || ext == 'odt' || ext == 'txt' || ext == 'xlsx' || ext == 'aif' || ext == 'cda' || ext == 'mid' || ext == 'mp3' || ext == 'mpa' || ext == 'ogg' || ext == 'wma' || ext == 'wpl' || ext == '7z' || ext == 'arj' || ext == 'deb' ||
                              ext == 'pkg' || ext == 'rar' || ext == 'rpm' || ext == 'csv' || ext == 'dat' || ext == 'db' || ext == 'log' || ext == 'sql' || ext == 'xml' || ext == 'tar' || ext == 'apk' || ext == 'bat' || ext == 'bmp' || ext == 'gif' || ext == 'ico' || ext == 'ps' || ext == 'psd' || ext == 'svg' || ext == 'tif' || ext == 'asp' || ext == 'php' || ext == 'css' || ext == 'htm' || ext == 'html' || ext == 'js' || ext == 'jsp' || ext == '3g2' || ext == '3gp' || ext == 'avi' || ext == 'flv' || ext == 'h264' || ext == 'm4v' || ext == 'mkv' || ext == 'mov' || ext == 'mp4' || ext == 'mpg' || ext == 'mpeg' || ext == 'rm' || ext == 'swf' || ext == 'wmv')
                            {                          
                                var image_src = '{{url('/public')}}/front/images/file_formats/'+ext+'.png';      
                                return function (evt){                                       
                                    theImg.src = evt.target.result;                                
                                    var html = "<div class='photo_view2' onclick='remove_this(this);' style='width:120px;height:120%;position:relative;display: inline-block;'><img src="+ image_src +" class='add_pht' id='add_pht upload-pic' style='float: left; padding: 0px ! important; margin:0' width='120' height='120'><div class='overlay2'><span class='plus2'><i class='fa fa-trash-o' aria-hidden='true'></i></span></div></div>";
                                    jQuery('#'+image_id).last().find('.show_photos').append(html);
                                    jQuery('#'+image_id).last().find('.div_hidden_photo_list').append('<input type="file" name="contest_attachments[]" id="contest_attachments" class="contest_attachments" style="display:none" multiple="multiple" />');   
                                    countImages();          
                                    $('#file_name_lab').val('');
                                    };
                            }
                            else if(ext == 'jpg' || ext == 'png' || ext == 'jpeg' || ext == 'gif' || ext == 'JPG' || ext == 'PNG' || ext == 'JPEG' || ext == 'GIF')
                            { 
                                return function (evt){                                       
                                    theImg.src = evt.target.result;                                
                                    var html = "<div class='photo_view2' onclick='remove_this(this);' style='width:120px;height:120%;position:relative;display: inline-block;'><img src="+ evt.target.result +" class='add_pht' id='add_pht upload-pic' style='float: left; padding: 0px ! important; margin:0' width='120' height='120'><div class='overlay2'><span class='plus2'><i class='fa fa-trash-o' aria-hidden='true'></i></span></div></div>";
                                    jQuery('#'+image_id).last().find('.show_photos').append(html);
                                    jQuery('#'+image_id).last().find('.div_hidden_photo_list').append('<input type="file" name="contest_attachments[]" id="contest_attachments" class="contest_attachments" style="display:none" multiple="multiple" />');   
                                    countImages();          
                                    $('#file_name_lab').val('');
                                    };
                            }
                            else
                            {
                                var image_src = '{{url('/public')}}/uploads/front/default/default.jpg';  
                                return function (evt){   
                                    theImg.src = evt.target.result;                                
                                    var html = "<div class='photo_view2' onclick='remove_this(this);' style='width:120px;height:120%;position:relative;display: inline-block;'><img src="+ image_src +" class='add_pht' id='add_pht upload-pic' style='float: left; padding: 0px ! important; margin:0' width='120' height='120'><div class='overlay2'><span class='plus2'><i class='fa fa-trash-o' aria-hidden='true'></i></span></div></div>";
                                    jQuery('#'+image_id).last().find('.show_photos').append(html);
                                    jQuery('#'+image_id).last().find('.div_hidden_photo_list').append('<input type="file" name="contest_attachments[]" id="contest_attachments" class="contest_attachments" style="display:none" multiple="multiple" />');   
                                    countImages();          
                                    $('#file_name_lab').val('');
                                    };
                            } 

                        }(img));
                        reader.readAsDataURL(file);                        
                    }
                    else
                    {
                        swal('File size should be less than 5MB');
                        return false;
                    }                    
                };
            }        
        });          
   } 

   function remove_this(elm)
    {
       var this_index = jQuery(elm).index();
       jQuery('.lab_img').find(".div_hidden_photo_list").find("input").eq(this_index).remove();
       jQuery(elm).remove();
       countImages();
    }  
  
    function countImages(){

        // var imagecount = $(".photo_view2").length;
        // if(imagecount>5){
        //     $('#err_other_image').html(err_max_images);
        // }else{
        //     $('#err_other_image').html('');
        // }
    }

</script>
<script type="text/javascript">
  $(document).ready(function(){
      set_minimum_currency_value1();
  })

  function set_minimum_currency_value1()
  {
    var token     = "<?php echo csrf_token(); ?>";
    var currency = $("#contest_currency option:selected").val();
    var url="{{url('/get_minimum_charge')}}";
    $.ajax({
        type: 'post',
        contentType: 'application/x-www-form-urlencoded',
        
        data : { contest_currency : currency},
        url:url,
        headers: {
        'X-CSRF-TOKEN': token
        },
        success: function(response) {
          if(response.msg=='success')
          {
            $('#contest_price').attr({"min" : response.min_amount});
            $('#contest_price').val(response.min_amount);
            $('.contest_price_message').html('Minimum '+currency+ ' '+response.min_amount+ ' is must.' );

          }
        }
      });
    //alert(currency);
  }
</script>
@stop