@extends('client.layout.master')
@section('main_content') 
<style type="text/css">
.br-theme-fontawesome-stars .br-widget a {font-size: 27px !important;}
.webwing-gallery .prod-carousel.mini-slider .prod-wrapper-outer .prod-item:before{content: none}
</style>
@php 
  if(isset($arr_contest_entry_data['contest_entry_files'][0]['file_no']))
  { 
    $first_file = $arr_contest_entry_data['contest_entry_files'][0]['file_no']; 
  } 
  else 
  { 
    $first_file = 0; 
  } 

  @endphp

<div class="col-sm-7 col-md-9 col-lg-9">
  @include('front.layout._operation_status')
    <div class="coll-listing-section">
        <div class="contest-titile">
            <div class="sm-logo d-inlin"><i style="font-size: 1.5em;" class="fa fa-trophy"></i></div>
            <span class="d-inlin">{{ trans('contets_listing/listing.text_contest')}}</span>
        </div>
        <a href="{{ url()->previous() }}" class="back-btn"><i class="fa fa-reply-all"></i> {{trans('client/contest/common.text_back')}}</a>
        <div class="clearfix"></div>
    </div>

    <div class="contest-main-title">
      @if(\Session::has('active_file'))
          @php 
            $active_file = \Session::get('active_file'); 
          @endphp
          <script type="text/javascript">var active_file =  {{$active_file}}; setTimeout(function(){ $('[file-index='+active_file+']').click(); },500);</script>
      @endif
      {{isset($arr_contest_entry_data['contest_details']['contest_title'])?$arr_contest_entry_data['contest_details']['contest_title']:""}}

      @if(isset($arr_contest_entry_data['is_winner']) && $arr_contest_entry_data['contest_details']['winner_choose'])
          @if($arr_contest_entry_data['is_winner'] == 'YES')
          <span class="pull-right"><img style="cursor: default;height: 40px;" src="{{url('/public')}}/front/images/archexpertdefault/contest-winner.png">{{trans('client/contest/common.text_winner')}}</span>   
          @elseif($arr_contest_entry_data['contest_details']['winner_choose'] == 'NO' && $arr_contest_entry_data['is_winner'] == 'NO')
            @if($arr_contest_entry_data['contest_details']['contest_end_date'] > date('Y-m-d'))
              <span class="pull-right"><a style="font-size: 15px;" onclick="confirmwinner('{{base64_encode($arr_contest_entry_data['contest_details']['id'])}}','{{base64_encode($arr_contest_entry_data['id'])}}')" href="javascript:void(0);" class="black-btn">{{trans('client/contest/common.text_choose_a_winner')}}</a></sapn>
            @else
              <span class="pull-right"><small><i style="color:#9e9e9e;font-family: cursive;">{{trans('client/contest/common.text_expired')}}...</i></small></span>
            @endif
          @endif
      @endif  
    </div>
    <!-- gallery hear -->
    @php
    $timeline_img_public_path = url('/public').config('app.project.img_path.cover_image');
    $timeline_img_base_path   = base_path().'/public'.config('app.project.img_path.cover_image');
 
    @endphp
    <div class="contest-slider">
    <h4 class="entry-title">{{trans('client/contest/common.text_entry')}} {{isset($arr_contest_entry_data['entry_id'])?'#'.$arr_contest_entry_data['entry_id']:"-"}}<span class="sm-entrt-txt">{{isset($arr_contest_entry_data['entry_number'])?'#'.$arr_contest_entry_data['entry_number']:"-"}}</span></h4>

    <div id="example1" class="webwing-gallery img350">
        <div class="prod-carousel">
          @if(isset($arr_contest_entry_data['contest_entry_files']) && sizeof($arr_contest_entry_data['contest_entry_files'])>0)
          <?php  $i = 1; ?>    
            @foreach($arr_contest_entry_data['contest_entry_files'] as $files)

              @if(isset($files['is_admin_deleted']) && $files['is_admin_deleted'] == '1')
                <img @if($i == 1) style="box-shadow: 12px 14px 11px #888888" @endif src="{{get_resized_image_path('temp_file','temp','100','100','Deleted By Admin')}}"  data-medium-img="{{get_resized_image_path('temp_file','temp','100','100','Deleted By Admin')}}" class="entry_files star{{$files['file_rating']['rating']}} filesrindex{{$i}}" file-index="{{$files['file_no']}}" image-id="{{$files['id']}}" data-big-img="{{get_resized_image_path('temp_file','temp','100','100','Deleted By Admin')}}" data-title="{{get_resized_image_path('temp_file','temp','100','100','Deleted By Admin')}}" title="{{trans('client/contest/common.text_file')}} #{{$files['file_no']}}" alt="" >
              @elseif(isset($files['image']) && $files['image']!=null && file_exists($contest_send_entry_base_file_path.$files['image']))
                <img @if($i == 1) style="box-shadow: 12px 14px 11px #888888" @endif src="{{url('/public/uploads/front/postcontest/send_entry/'.$files['image']) }}"  data-medium-img="{{url('/public/uploads/front/postcontest/send_entry/'.$files['image']) }}" class="entry_files star{{$files['file_rating']['rating']}} filesrindex{{$i}}" file-index="{{$files['file_no']}}" image-id="{{$files['id']}}" data-big-img="{{url('/public/uploads/front/postcontest/send_entry/'.$files['image']) }}" data-title="{{trans('client/contest/common.text_file')}} #{{$files['file_no']}}" title="{{trans('client/contest/common.text_file')}} #{{$files['file_no']}}" alt="" attr-work-done = "{{isset($files['is_own_work'])?$files['is_own_work']:"-"}}">
              @else
                <!-- <img src="{{url('/front/cover_image/default_cover_photo.jpg')}}" data-medium-img="{{url('/front/cover_image/default_cover_photo.jpg')}}" data-big-img="{{url('/front/cover_image/default_cover_photo.jpg')}}" data-title="default_cover_photo.jpg" alt=""> -->
              @endif
           <?php $i++; ?>
          @endforeach
          @endif
        </div>
        @php
        $sidebar_information      = sidebar_information($arr_contest_entry_data['expert_id']);
        $profile_img_public_path  = url('/public').config('app.project.img_path.profile_image');
        $profile_img_base_path    = base_path().'/public'.config('app.project.img_path.profile_image');
        $timeline_img_public_path = url('/public').config('app.project.img_path.cover_image');
        $timeline_img_base_path   = base_path().'/public'.config('app.project.img_path.cover_image');
        @endphp
        <div class="layout-details for-color-chg">
            <div class="layout-details-left">
                <h5>{{isset($arr_contest_entry_data['title'])?str_limit($arr_contest_entry_data['title'],30,'...'):""}}</h5>

                @if($arr_contest_entry_data['is_winner'] == 'NO')
                <form action="{{url('/client/contest/store-rating')}}" id="file-rtng-form" name="file-rtng-form" method="POST">
                  {{csrf_field()}}
                  <input type="hidden" placeholder="Contest id" name="rt_contest_id" id="rt_contest_id" value="{{isset($arr_contest_entry_data['contest_id'])?$arr_contest_entry_data['contest_id']:"0"}}">
                  <input type="hidden" placeholder="Contest entry id" name="rt_contest_entry_id" id="rt_contest_entry_id" value="{{isset($arr_contest_entry_data['id'])?$arr_contest_entry_data['id']:"0"}}">
                  <input type="hidden" placeholder="File no" name="rt_file_no" id="rt_file_no" value="{{isset($arr_contest_entry_data['contest_entry_files'][0]['file_no'])?$arr_contest_entry_data['contest_entry_files'][0]['file_no']:'0'}}"> 
                  <input type="hidden" placeholder="Image Id" name="image_id" id="image_id" value="{{isset($arr_contest_entry_data['contest_entry_files'][0]['id'])?$arr_contest_entry_data['contest_entry_files'][0]['id']:'0'}}">

                    <div class="row">
                      <div class="col-sm-12 col-md-6 col-lg-3">
                        <select  class="review-rating-starts" id="file_rating" name="file_rating" data-rule-required="true">
                           <option value="1" @if(isset($arr_contest_entry_data['contest_entry_files_rating'][0]['rating']) && $arr_contest_entry_data['contest_entry_files_rating'][0]['rating'] == 1) selected @endif >1</option>
                           <option value="2" @if(isset($arr_contest_entry_data['contest_entry_files_rating'][0]['rating']) && $arr_contest_entry_data['contest_entry_files_rating'][0]['rating'] == 2) selected @endif>2</option>
                           <option value="3" @if(isset($arr_contest_entry_data['contest_entry_files_rating'][0]['rating']) && $arr_contest_entry_data['contest_entry_files_rating'][0]['rating'] == 3) selected @endif>3</option>
                           <option value="4" @if(isset($arr_contest_entry_data['contest_entry_files_rating'][0]['rating']) && $arr_contest_entry_data['contest_entry_files_rating'][0]['rating'] == 4) selected @endif>4</option>
                           <option value="5" @if(isset($arr_contest_entry_data['contest_entry_files_rating'][0]['rating']) && $arr_contest_entry_data['contest_entry_files_rating'][0]['rating'] == 5) selected @endif>5</option>
                        </select>
                        <span class="error" id="err_rating"></span>
                      </div>
                    </div>
                </form>
                @endif  

                <div class="avg-rating">
                    <ul style="display: none;">
                      @php $first_file_rating = ""; @endphp 
                      @if(isset($arr_contest_entry_data['contest_entry_files']) && sizeof($arr_contest_entry_data['contest_entry_files'])>0)
                        @php $existed_ratings = [];  @endphp
                        @foreach($arr_contest_entry_data['contest_entry_files'] as $fkey => $files)
                            @foreach($arr_contest_entry_data['contest_entry_files_rating'] as $rkey => $rating)
                               @if($rating['file_no'] == $files['file_no'])
                                   @if($fkey == 0 && $first_file_rating == '')
                                     @php $first_file_rating = $rating['rating']; @endphp 
                                   @endif   
                                   <span @if($fkey == 0) style="display:block;" @else style="display:none;" @endif class="stars file-rtn file-rtn{{$files['file_no']}}" rting="{{$rating['rating']}}" style="margin-left: -3px;">{{$rating['rating']}}</span>
                                   @php $existed_ratings[] = $rating['file_no'];  @endphp
                               @endif   
                            @endforeach
                            @if(!in_array($files['file_no'],$existed_ratings))
                                @if($fkey == 0 && $first_file_rating == '')
                                 @php $first_file_rating = 0; @endphp 
                                @endif
                            <span @if($fkey == 0) style="display:block;" @else style="display:none;" @endif class="stars file-rtn file-rtn{{$files['file_no']}}" rting="0" style="margin-left: -3px;">0</span>
                            @endif
                        @endforeach
                      @endif  
                    </ul>
                </div>
            </div>
            @if($arr_contest_entry_data['sealed_entry'] == '1')
              <span class="sealed-entry">Sealed</span>   
            @endif
            
            <div class="layout-details-right">
                <div class="coll-person d-inlin">
                    <?php

                    if(isset($profile_img_public_path) && isset($sidebar_information['user_details']['profile_image']) && $sidebar_information['user_details']['profile_image']!="" && file_exists('public/uploads/front/profile/'.$sidebar_information['user_details']['profile_image']))
                    {
                      $profile_image = isset($sidebar_information['user_details']['profile_image'])?$profile_img_public_path.$sidebar_information['user_details']['profile_image']:'';
                    }
                    else
                    {
                      $profile_image = $profile_img_public_path.'default_profile_image.png';
                    }
                     
                    ?>


                    @if(isset($arr_contest_entry_data['expert_details']['user_details']['is_online']) && $arr_contest_entry_data['expert_details']['user_details']['is_online']!='' && $arr_contest_entry_data['expert_details']['user_details']['is_online']=='1')
                      <div class="collaboration-img">
                          <img src="{{$profile_image}}" class="img-responsive" alt=""/>
                          <div class="online-status green"></div>
                      </div>
                    @else
                      <div class="collaboration-img">
                          <img src="{{$profile_image}}" class="img-responsive" alt=""/>
                          <div class="online-status grey"></div>
                      </div>
                    @endif


                    <div class="coll-name">
                    <h5>
                      <a title="Expert" href="{{url('/experts/portfolio/'.base64_encode(isset($arr_contest_entry_data['expert_id'])?$arr_contest_entry_data['expert_id']:'0'))}}">{{isset($sidebar_information['user_details']['first_name'])?$sidebar_information['user_details']['first_name'].'  '.str_limit($sidebar_information['user_details']['last_name'],1,'.'):""}}</a>
                    </h5>
                      @if($sidebar_information['last_login_duration'] == 'Active')
                             <span class="flag" title="{{isset($sidebar_information['expert_timezone'])?$sidebar_information['expert_timezone']:'-'}}">
                                <span class="flag-image"> 
                                    @if(isset($sidebar_information['user_country_flag'])) 
                                      @php 
                                         echo $sidebar_information['user_country_flag']; 
                                      @endphp 
                                    @elseif(isset($sidebar_information['user_country'])) 
                                      @php 
                                        echo $sidebar_information['user_country']; 
                                      @endphp 
                                    @else 
                                      @php 
                                        echo '-'; 
                                      @endphp 
                                    @endif 
                                </span>
                                {{isset($sidebar_information['expert_timezone_without_date'])?$sidebar_information['expert_timezone_without_date']:'-'}}
                             </span>
                          @else
                             <span class="flag" title="{{$sidebar_information['last_login_full_duration']}}">
                                <span class="flag-image"> 
                                    @if(isset($sidebar_information['user_country_flag'])) 
                                      @php 
                                         echo $sidebar_information['user_country_flag']; 
                                      @endphp 
                                    @elseif(isset($sidebar_information['user_country'])) 
                                      @php 
                                        echo $sidebar_information['user_country']; 
                                      @endphp 
                                    @else 
                                      @php 
                                        echo '-'; 
                                      @endphp 
                                    @endif 
                                </span>
                                {{$sidebar_information['last_login_duration'] or ''}}
                             </span>
                          @endif
                    </div>
                </div>
                <div class="chat d-inlin">
                  @if( isset($arr_contest_entry_data['id']))
                    <a href="{{ url('client/twilio-chat/contest/') . '/' . base64_encode($arr_contest_entry_data['id']) }}" style="margin-top:5px;" class="applozic-launcher" ><i class="fa fa-comment-o"></i></a>
                  @endif
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    @if(isset($arr_contest_entry_data['contest_entry_files']) && sizeof($arr_contest_entry_data['contest_entry_files'])>0)
    @foreach($arr_contest_entry_data['contest_entry_files'] as $fkey => $files)
      @if($fkey == 0)
        @if(isset($files['is_own_work']) && $files['is_own_work'] == 0)
          <div style="padding-bottom: 11px; padding-top: 6px;">
            <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span>
            {{isset($sidebar_information['user_details']['first_name'])?$sidebar_information['user_details']['first_name'].'  '.str_limit($sidebar_information['user_details']['last_name'],1,'.'):""}} : 
            <span class="text_wrk_done">{{ trans('contets_listing/listing.text_all_work_was_done_by_me_or_our_company') }}</span> 
          </div>
        @elseif(isset($files['is_own_work']) && $files['is_own_work'] == 1)
          <div style="padding-bottom: 11px; padding-top: 6px;">
            <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span>
            {{isset($sidebar_information['user_details']['first_name'])?$sidebar_information['user_details']['first_name'].'  '.str_limit($sidebar_information['user_details']['last_name'],1,'.'):""}} : 
            <span class="text_wrk_done">{{ trans('contets_listing/listing.text_the_work_was_partially_done_by_me_or_our_company') }}</span>
          </div>
        @else
          <div style="padding-bottom: 11px; padding-top: 6px;">
            {{isset($sidebar_information['user_details']['first_name'])?$sidebar_information['user_details']['first_name'].'  '.str_limit($sidebar_information['user_details']['last_name'],1,'.'):""}} : 
            <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span>
            <span class="text_wrk_done">Not available</span>
          </div>
        @endif  
      @endif  
    @endforeach
  @endif
  <div class="clearfix"></div>
    @if($arr_contest_entry_data['is_winner'] == 'YES')  
        <div class="comment-section">
            <span><i class="fa fa-file-archive-o" aria-hidden="true"></i> {{trans('client/contest/common.text_uploaded_original_files')}} (Zip) : <span>{{isset($arr_contest_entry_data['contest_entry_original_files'])?count($arr_contest_entry_data['contest_entry_original_files']):0}}</span> / 3</span></span>
            <div class="clearfix"></div>
            <hr>
            <div class="clearfix"></div>
            <table class="table">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">{{trans('client/contest/common.text_file')}} Name</th>
                    <th scope="col">{{trans('client/contest/common.text_uploaded')}}</th>
                    <th scope="col">{{trans('client/contest/common.text_action')}}</th>
                  </tr>
                </thead>
                <tbody>
                  @if(isset($arr_contest_entry_data['contest_entry_original_files']) && count($arr_contest_entry_data['contest_entry_original_files']) > 0)
                    @foreach($arr_contest_entry_data['contest_entry_original_files'] as $oufkey => $original_files)
                      @if(isset($original_files['file_name']) && $original_files['file_name'] != "" && file_exists('public/uploads/front/postcontest/send_entry/original_files/'.$original_files['file_name']))
                      <tr>
                        <td>{{$original_files['original_file_name'] or 'na'}}</td>
                        <td>{{time_ago($original_files['created_at'])}}</td>
                        <td>
                             <a href="{{url('/public/uploads/front/postcontest/send_entry/original_files/'.$original_files['file_name'])}}" download><i class="fa fa-file-archive-o" aria-hidden="true"></i> Download</a>
                        </td>
                      </tr>
                      @endif
                    @endforeach
                  @else
                  <tr>
                    <td scope="row" colspan="3" align="center">{{trans('client/contest/common.text_no_files_uploaded_yet')}}....</td>
                  </tr>
                  @endif
                </tbody>
              </table>
        </div>
    @endif
    <div class="clearfix"></div>
    <h4>{{isset($arr_contest_entry_data['title'])?$arr_contest_entry_data['title']:""}}</h4>
    <div class="slider-details">
    {{isset($arr_contest_entry_data['description'])?$arr_contest_entry_data['description']:""}}
    </div>
    </div>
    <!-- gallery hear End -->
    <div class="comment-section">
        <a href="#add_comment" data-toggle="modal" id="add-cmt-btn" class="black-btn add_comment">{{trans('client/contest/common.text_add_comment_on')}} <span class="file_index">{{trans('client/contest/common.text_file')}} #{{$first_file}}</span></a> 
        <div class="comment-block-section">
            <h4>{{trans('client/contest/common.text_comments')}} <span class="file_index">{{trans('client/contest/common.text_file')}} #{{$first_file}}</span> (0)</h4>
            <div class="comment-block">{{trans('client/contest/common.text_no_comment_on')}} <span class="file_index">{{trans('client/contest/common.text_file')}} #{{$first_file}}</span></div>
        </div>
    </div>
</div>
<!-- add comment Modal start-->
 <div class="modal fade add-comment-modal" id="add_comment" role="dialog">
    <div class="modal-dialog ">
       <div class="modal-content">
          <div class="modal-body">
            <h2 class="file_index"><span class="file_index">{{trans('client/contest/common.text_file')}} #{{$first_file}}</span></h2>
            <form action="{{url('/client/contest/store-comment')}}" id="file-cmnt-form" name="file-cmnt-form" method="POST">
                {{csrf_field()}}
                <input type="hidden" placeholder="Contest id" name="contest_id" id="contest_id" value="{{isset($arr_contest_entry_data['contest_id'])?$arr_contest_entry_data['contest_id']:"0"}}">
                <input type="hidden" placeholder="Contest entry id" name="contest_entry_id" id="contest_entry_id" value="{{isset($arr_contest_entry_data['id'])?$arr_contest_entry_data['id']:"0"}}">
                <input type="hidden" placeholder="File no" name="file_no" id="file_no" value="{{isset($arr_contest_entry_data['contest_entry_files'][0]['file_no'])?$arr_contest_entry_data['contest_entry_files'][0]['file_no']:'0'}}">
                <div class="form-group">
                    <label>{{trans('client/contest/common.text_add_commnet')}} <span style="color:red">*</span></label>
                    <textarea rows="5" name="file_cmnt" id="file_cmnt" data-rule-required="true" placeholder="{{trans('client/contest/common.text_description')}}"></textarea>
                    <span class="error" id="err_file_cmnt"></span>
                </div>
                <div class="modal-btns">
                    <button type="button" id="cancel-cmnt" class="black-border-btn inline-btn" data-dismiss="modal">{{trans('client/contest/common.text_cancel')}}</button>
                    <button type="submit" id="submit-cmnt" class="black-btn inline-btn">{{trans('client/contest/common.text_submit')}}</button>
                </div>
            </form>
          </div>
       </div>
    </div>
 </div>
<!--add comment Modal end here-->      
<link rel="stylesheet" href="{{url('/public')}}/assets/rating-master/fontawesome-stars.css">
<script type="text/javascript" src="{{url('/public')}}/assets/rating-master/jquery.barrating.min.js"></script>
<link href="{{url('/public')}}/front/css/gallery.css" rel="stylesheet" />
<script src="{{url('/public')}}/front/js/gallery.min.js"></script>

<script type="text/javascript">

  $(document).ready(function() {

  $('#file_rating').on('change', function()
  {
    file_no = $('#rt_file_no').val();
    image_id = $('#image_id').val();
    var token = csrf_token;
    var rating = this.value;
    var contest_id        = $('#contest_id').val();
    var contest_entry_id  = $('#contest_entry_id').val();
    var enc_contest_entry_id = "{{base64_encode($arr_contest_entry_data['id'])}}"
    $.ajax({
          'url':site_url+'/client/contest/store_rating',
          'method':"POST",
          'data':{file_no:file_no,image_id:image_id,contest_id:contest_id,contest_entry_id:contest_entry_id,rating:rating,_token:token},
          success:function(res)
          {
              if(res.status == 'success')
              {
                //window.location.href = "{{url('/public')}}/client/contest/show_contest_entry_details/"+enc_contest_entry_id;
                //location.reload();   
              }
          }
    }) 

  });

});
</script>

<script type="text/javascript">
    $("#file-rtng-form").validate();
    $("#file-cmnt-form").validate();
    $(document).ready(function() {
        $(function() {
             $('.review-rating-starts').barrating({
              theme: 'fontawesome-stars',
              allowEmpty: true,
              emptyValue: 0
             });
        });
        setTimeout(function(){
            var first_file_rating = '{{$first_file_rating}}';
            if(first_file_rating == 0 || first_file_rating == ''){
               $('#file_rating').val('');
               $("[data-rating-value]").attr('class','');
            }else{
               $('#file_rating').val(first_file_rating);
               $('.review-rating-starts').barrating('set', first_file_rating);
            }
        },200);
        // call ajax for first
        var token = csrf_token;
        /* loader start */
        $(".comment-block-section").css('opacity','0.5');
        /* loader start */
        var file_no           = '{{isset($arr_contest_entry_data['contest_entry_files'][0]['file_no'])?$arr_contest_entry_data['contest_entry_files'][0]['file_no']:'0'}}';
        var contest_id        = $('#contest_id').val();
        var contest_entry_id  = $('#contest_entry_id').val();
        $.ajax({
              'url':site_url+'/client/contest/get-comments',
              'method':"POST",
              'data':{file_no:file_no,contest_id:contest_id,contest_entry_id:contest_entry_id,_token:token},
              success:function(result){
                $('.comment-block-section').html(result);
                $(".comment-block-section").css('opacity','5');
              }
        }) 
        $('#example1').webwingGallery({
            openGalleryStyle: 'transform',
            changeMediumStyle: true
        });
        $('.entry_files').click(function(){
           var file_work_done_value = $(this).attr("attr-work-done");
           console.log(file_work_done_value);
           if(file_work_done_value != null){
              $('.note_wrk_done').show();  
              if(file_work_done_value == 0){
                $(".text_wrk_done").html("{{ trans('contets_listing/listing.text_all_work_was_done_by_me_or_our_company') }}");
              } else if(file_work_done_value == 1) {
               $(".text_wrk_done").html("{{ trans('contets_listing/listing.text_the_work_was_partially_done_by_me_or_our_company') }}"); 
              } else {
               $(".text_wrk_done").html("Not Available");  
              }
           }
           var file_no           = $(this).attr('file-index');
           var image_id          = $(this).attr('image-id');
           var contest_id        = $('#contest_id').val();
           var contest_entry_id  = $('#contest_entry_id').val();
           $('#file_no').val(file_no); 
           $('#rt_file_no').val(file_no); 
           $('#image_id').val(image_id); 

           $('.file_index').html('{{trans('client/contest/common.text_file')}}'+'#'+file_no);
           $('.entry_files').removeAttr('style');
           $('.file-rtn').hide();
           $('.file-rtn'+file_no).show();
           var file_rating = $('.file-rtn'+file_no).attr('rting');
           //$('#file_rating').trigger('change');
           $('#file_rating option[value='+file_rating+']').attr('selected','selected');
           if(file_rating == 0 || file_rating == ''){
             $('#file_rating').val('');
             $("[data-rating-value]").attr('class','');
           }else{
             $('#file_rating').val(file_rating);
             $('.review-rating-starts').barrating('set', file_rating);
           }
           $(this).attr('style','box-shadow:12px 14px 11px #888888');
           // call ajax for comment
           var token = csrf_token;
           /* loader start */
           //$("#add-cmt-btn").hide();
           $(".comment-block-section").css('opacity','0.5');
           /* loader start */
           $.ajax({
              'url':site_url+'/client/contest/get-comments',
              'method':"POST",
              'data':{file_no:file_no,contest_id:contest_id,contest_entry_id:contest_entry_id,_token:token},
              success:function(result){
                $('.comment-block-section').html(result);
                $(".comment-block-section").css('opacity','5');
              }
           })
        });
        $('.add_comment').click(function(){
           var file_no        = $('#file_no').val();
           if(file_no == ''){file_no = '{{isset($arr_contest_entry_data['contest_entry_files'][0]['file_no'])?$arr_contest_entry_data['contest_entry_files'][0]['file_no']:'0'}}';}
           $('#file_no').val(file_no);
        });
        $('#cancel-cmnt').click(function(){
           $('#err_file_cmnt').val('');
           $('#file_cmnt').val('');
        });
        $(document).on('click','.shwmore',function(){
           var nextshow  = $(this).attr('shw');
           var total_cnt = $('#cmt_cnt').html();
           $('.show'+nextshow).show();
           $('.shwmore').attr('shw',(parseInt(nextshow)*2));
           if(parseInt(nextshow) >= parseInt(total_cnt)){$('.shwmore').hide();}
        });
        $(document).on('click','#store-rtng',function(){  
           var file_rating        = $('#file_rating').val();
           if(file_rating == null){
            $('#err_rating').html('{{trans('client/contest/common.text_please_select_at_least_one_rating')}}...');
            return false;
           } else{
            $("#file-rtng-form").submit();
           }
        });
    });
    function confirmwinner(contest_id,contest_entry_id) {
        var confirm_message = '{{trans('client/contest/common.text_are_you_sure_do_you_want_to_winner_this_entry')}}';
        alertify.confirm(confirm_message, function (e) {
            if (e) {
                showProcessingOverlay();
                window.location.href="{{ url('/client/contest/choose-winner') }}"+'/'+contest_id+'/'+contest_entry_id;
                return true;
            } else {
                return false;
            }
        });
    }
</script>

<script>
    $(document).ready(function(){
        if ($('.entry_files').hasClass('star1')) {
            $('.star1').parent('.prod-item').addClass('star1');
        }              
        if ($('.entry_files').hasClass('star2')) {
            $('.star2').parent('.prod-item').addClass('star2');
        }      
        if ($('.entry_files').hasClass('star3')) {
            $('.star3').parent('.prod-item').addClass('star3');
        }      
        if ($('.entry_files').hasClass('star4')) {
            $('.star4').parent('.prod-item').addClass('star4');
        }      
        if ($('.entry_files').hasClass('star5')) {
            $('.star5').parent('.prod-item').addClass('star5');
        }  
    });         
</script>


@stop