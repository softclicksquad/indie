@extends('client.layout.master') @section('main_content')
<style type="text/css">
    .moretext {
        color: rgb(0, 0, 0);
        text-decoration: none;
        text-transform: capitalize;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" integrity="sha256-Z8TW+REiUm9zSQMGZH4bfZi52VJgMqETCbPFlGRB1P8=" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.js" integrity="sha256-ZvMf9li0M5GGriGUEKn1g6lLwnj5u+ENqCbLM5ItjQ0=" crossorigin="anonymous"></script>
<div class="col-sm-7 col-md-9 col-lg-9">
    <div class="row">
        <div class="col-lg-12">
            @include('front.layout._operation_status')
            <div class="head_grn">{{trans('client/contest/post.text_posted_contest')}}</div>
        </div>

        @if(isset($arr_open_contests['data']) && sizeof($arr_open_contests['data'])>0)
        @foreach($arr_open_contests['data'] as $contestRec)
        <div class="col-lg-12">
            <div class="white-block-bg-no-padding sssssssssss">
                <div class="white-block-bg big-space hover">
                    <div class="project-left-side">
                        <input type="hidden" class=" contest_id_{{$contestRec['id']}}" value="{{$contestRec['id'] or '0'}}">
                        <input type="hidden" class=" contest_name_{{$contestRec['id']}}" value="{{$contestRec['contest_title'] or "N/A"}}">
                        <input type="hidden" class=" contest_info_{{$contestRec['id']}}" value="{{$contestRec['contest_additional_info'] or ''}}">
                        <div class="search-freelancer-user-head">
                            <!-- <a href="{{ $module_url_path }}/details/{{ base64_encode($contestRec['id'])}}"> {{$contestRec['contest_title'] or '-'}} </a> -->
                            <a href="{{ $module_url_path }}/details/{{ base64_encode($contestRec['id'])}}"> {{$contestRec['contest_title'] or '-'}} </a>
                        </div>
                        <div class="search-freelancer-user-location">
                            <span class="gavel-icon"><img src="{{url('/public')}}/front/images/money.png" alt="" /></span>
                            <span class="dur-txt"> {{isset($contestRec['contest_entry'])?count($contestRec['contest_entry']):'0'}} {{ trans('contets_listing/listing.text_total_entries') }}</span><span class="freelancer-user-line">|</span>
                        </div>
                        @php $postes_time_ago = time_ago($contestRec['created_at']); @endphp

                        <div class="search-freelancer-user-location">
                            <span class="gavel-icon"><img src="{{url('/public')}}/front/images/clock.png" alt="" /></span>
                            <span class="dur-txt">{{ trans('contets_listing/listing.text_posted') }} : {{$postes_time_ago}}</span>
                            @if(isset($contestRec['edited_at']) && $contestRec['edited_at']!=NULL)
                                <span class="freelancer-user-line">|</span>
                            @endif
                        </div>
                        
                        @if(isset($contestRec['edited_at']) && $contestRec['edited_at']!=NULL)
                            @php $edited_time_ago = time_ago($contestRec['edited_at']); @endphp
                            <div class="search-freelancer-user-location">
                                <span class="gavel-icon"><img src="{{url('/public')}}/front/images/clock.png" alt="" /></span>
                                <span class="dur-txt">Last edited : {{$edited_time_ago}}</span>
                            </div>
                        @endif

                        <div class="clearfix"></div>
                        <div class="more-project-dec">{{str_limit($contestRec['contest_description'],160)}}
                            @if(strlen(trim($contestRec['contest_description'])) > 160)
                            <a class="moretext" href="{{ $module_url_path }}/details/{{ base64_encode($contestRec['id'])}}">
                                {{trans('project_manager/projects/postedprojects.text_more')}}
                            </a>
                            @endif
                        </div>

                        <div class="category-new"><img src="{{url('/public')}}/front/images/tag-2.png" alt="" /> {{$contestRec['category_details']['category_title'] or 'NA'}}</div>

                        <div class="category-new"><img src="{{url('/public')}}/front/images/tag-2.png" alt="" /> {{isset($contestRec['sub_category_details']['subcategory_title'])?$contestRec['sub_category_details']['subcategory_title']:'NA'}} </div>
                        
                        <div class="clearfix"></div>


                        @if(isset($contestRec['contest_skills']) && count($contestRec['contest_skills']) > 0)
                        @foreach($contestRec['contest_skills'] as $key=> $skills)
                        @if(isset($skills['skill_data']['skill_name']) && $skills['skill_data']['skill_name'] != "")
                       

                        <div class="skils-project">
                            <img src="{{url('/public')}}/front/images/tag.png" alt="" />
                            <ul>
                                <li>{{ str_limit($skills['skill_data']['skill_name'],25) }}</li>
                            </ul>
                        </div>

                        @endif
                        @endforeach
                        @endif
                    </div>

                    <div class="search-project-listing-right contest-right">
                        <div class="search-project-right-price">
                            {{isset($contestRec['contest_currency'])?$contestRec['contest_currency']:'$'}} {{isset($contestRec['contest_price'])?number_format($contestRec['contest_price']):'0'}}
                            <span>{{trans('client/contest/post.text_prize')}}</span>
                        </div>
                        <div class="search-project-right-price">
                            @if($contestRec['winner_choose'] == 'YES')
                                <span><img style="cursor: default;height: 40px;" src="{{url('/public')}}/front/images/archexpertdefault/contest-winner.png"></span>
                            @else
                                <span id='days_left{{$contestRec["id"]}}' style="margin-top:0px;">{{trans('client/projects/ongoing_projects.text_next_expire')}} </span><span>{{trans('client/contest/posted.text_time_left')}}</span>
                            @endif
                            
                            <?php 

                                $arr_diff_between_two_date = get_array_diff_between_two_date(date('Y-m-d H:i:s'),$contestRec['contest_end_date']);
                                $str_days_diff = isset($arr_diff_between_two_date['str_days_diff']) ? $arr_diff_between_two_date['str_days_diff'] : '';

                            ?>

                            <script type="text/javascript">
                                var contestid = '<?php echo $contestRec["id"]; ?>';
                                var str_days_diff = '{{ isset($str_days_diff) ? $str_days_diff : '' }}';

                                if (str_days_diff == '') {
                                    $('#days_left' + contestid).html('{{trans('client/projects/ongoing_projects.text_next_expire')}}');
                                    $('#days_left' + contestid).css('color', 'red');
                                } else {
                                    $('#days_left' + contestid).html(str_days_diff);
                                    $('#days_left' + contestid).css('font-size', '16px');
                                    $('#days_left' + contestid).css('color', 'green');
                                    $('#days_left' + contestid).css('font-weight', 'bold');
                                }
                            </script>

                        </div>
                        <div class="clearfix"></div>
                     
                    </div>
                       <div class="invite-expert-btns">
                            @if(isset($contestRec['contest_end_date']) && $contestRec['contest_end_date'] >= date('Y-m-d'))
                                @if(empty($contestRec['contest_entry']) && count($contestRec['contest_entry']) == 0 )
                                    <a class="black-border-btn" href="{{ $module_url_path }}/details/{{ base64_encode($contestRec['id'])}}">{{ trans('contets_listing/listing.text_entry_details') }}</a>
                                    <a href="{{$module_url_path}}/edit/{{ base64_encode($contestRec['id'])}}" class="black-border-btn">Edit</a>
                                @else
                                    <a class="black-border-btn new-btn-change-add" href="{{ $module_url_path }}/details/{{ base64_encode($contestRec['id'])}}">{{ trans('contets_listing/listing.text_entry_details') }}</a>
                                @endif
                            @endif

                            @if(isset($contestRec['contest_entry']) && !empty($contestRec['contest_entry']))
                                @if($str_days_diff != '')
                                    @if($contestRec['contest_additional_info'] == '')
                                        <a class="black-border-btn new-btn-change-add" onclick="openForm(this)" data-id="{{$contestRec['id']}}" id="{{$contestRec['id']}}">Add more details</a>
                                    @else
                                        <a class="black-border-btn new-btn-change-add" onclick="openForm(this)" data-id="{{$contestRec['id']}}" id="{{$contestRec['id']}}">Edit details</a>
                                    @endif
                                @endif
                            @endif
                            @if(isset($contestRec['contest_end_date']) && $contestRec['contest_end_date'] >= date('Y-m-d') && $contestRec['winner_choose'] == 'NO')
                                <a onclick="confirmDelete(this)" style="cursor: pointer;" data-contest-id="{{ isset($contestRec['id'])?base64_encode($contestRec['id']):'' }}" class="black-border-btn">Cancel</a>
                            @endif
                        </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        @endforeach
        @include('front.common.pagination_view', ['paginator' => $arr_pagination])
        @else
        <div class="col-lg-12">
            <div class="search-grey-bx">
                <div class="no-record">
                    {{trans('expert/projects/awarded_projects.text_sorry_no_record_found')}}
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@include('client.contests.additional_info')
<script type="text/javascript">
    function confirmDelete(ref) {
        // alertify.confirm("Are you sure? You want to cancel contest ?", function(e) {
        //     if (e) {
        //         var contest_id = $(ref).attr('data-contest-id');
        //         window.location.href = "{{ $module_url_path }}" + "/delete_contest/" + contest_id;
        //         return true;
        //     } else {
        //         return false;
        //     }
        // });

                    swal({
                         title: "Are you sure? You want to cancel contest ?",
                          text: "",
                          icon: "warning",
                          dangerMode: true,
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes, I am sure!',
                            cancelButtonText: "No",
                            closeOnConfirm: false,
                            closeOnCancel: false
                         },
                         function(isConfirm){

                           if (isConfirm){
                             var contest_id = $(ref).attr('data-contest-id');
                             window.location.href = "{{ $module_url_path }}" + "/delete_contest/" + contest_id;
                                return true;

                            } else {
                                swal.close();
                              return false;
                            }
                         });

    }
</script>
@stop