@extends('client.layout.master')                
@section('main_content')
<div class="col-sm-7 col-md-9 col-lg-9">
   <div class="right_side_section payment-section">
      <div class="head_grn">{{isset($page_title)?$page_title:''}}</div>
      @include('front.layout._operation_status')
      <div class="ongonig-project-section">
         <div class="dispute-head"  style="padding-top: 0px;">
            {{isset($arr_contest['contest_title'])?$arr_contest['contest_title']:''}}
         </div>
         <span><i class="fa fa-calendar" aria-hidden="true"></i></span> {{isset($arr_contest['created_at'])?date('d M Y',strtotime($arr_contest['created_at'])):''}}
         <div class="det-divider"></div>
         <div class="project-title">
         </div>
         <div class="clr"></div>
             @php $total_cost = 0; @endphp
             @if(isset($contest_price))
              @php $total_cost += $contest_price; @endphp
               <div class="project-list pro-list-ul">
                  <ul>
                     <li style="background:none;">
                          <span class="projrct-prce"> {{ isset($arr_contest['contest_currency'])?$arr_contest['contest_currency']:'' }}
                            {{isset($contest_price)?number_format($contest_price,2):'0'}} <span style="color:#737373;">(<i>Contest price</i> )</span>
                          </span>
                     </li>
                  </ul>
               </div>
               <div class="clr"></div>
               <br/>
             @endif
             @if(isset($service_fee))
              @php $total_cost += $service_fee; @endphp
               <div class="project-list pro-list-ul">
                  <ul>
                     <li style="background:none;">
                          <span class="projrct-prce"> {{ isset($arr_contest['contest_currency'])?$arr_contest['contest_currency']:'' }}
                            {{isset($service_fee)?number_format($service_fee,2):'0'}} <span style="color:#737373;">(<i>Contest service fee</i>  @if(isset($service_fee_type) && $service_fee_type == 'prcentage')) ({{$percentage_cost}}%) @endif</span>
                          </span>
                     </li>
                  </ul>
               </div>
               <div class="clr"></div>
               <br/>
             @endif
             
             <div class="project-list pro-list-ul">
                <ul>
                   <li style="background:none;">
                        <span class="projrct-prce"> <b>Total :</b> {{ isset($arr_contest['contest_currency'])?$arr_contest['contest_currency']:'' }}
                          {{isset($total_cost)?number_format($total_cost,2):'0'}}
                        </span>
                   </li>
                </ul>
             </div>
            <div class="clr"></div>
            <br/>
            <form id="validate-form-payment" name="validate-form-payment" method="POST" action="{{url('/payment/paynow')}}">
                {{ csrf_field() }}
                <input type="hidden" name="payment_obj_id"       id="payment_obj_id" value=" {{isset($arr_contest['id'])?$arr_contest['id']:'0'}}">
                <input type="hidden" name="contest_entry_id"     id="contest_entry_id" value=" {{isset($contest_entry_id)?$contest_entry_id:'0'}}">
                <input type="hidden" name="transaction_type"     id="transaction_type" value="7">
                <input type="hidden" name="contest_services_fee" id="contest_services_fee" value="{{isset($service_fee)?number_format($service_fee,2):'0'}}">
                <input type="hidden" name="contest_price"        id="contest_price" value="{{isset($contest_price)?number_format($contest_price,2):'0'}}">
                <input type="hidden" name="total_cost"           id="total_cost" value="{{isset($total_cost)?number_format($total_cost,2):'0'}}">
                <!-- Payment Methods --> 
                  {{-- <div class="paypa_radio">
                     <!-- Wallet -->
                         @php $wallet_amount= 0; @endphp
                         @if(isset($mangopay_wallet_details->Balance->Amount) && $mangopay_wallet_details->Balance->Amount!='')
                           @php $wallet_amount= $mangopay_wallet_details->Balance->Amount/100; @endphp
                         @endif
                         <div class="radio_area regi" onclick="javascript: return setOption('wallet');">
                            <input type="radio" name="payment_method" id="radio_wallet" class="css-checkbox" checked="" value="3">
                            <label for="radio_wallet" class="css-label radGroup1">
                              <img style="width: 292px;height: 57px;margin-top:-20px;" src="{{url('/public')}}/front/images/archexpertdefault/arc-hexpert-wallet-logo.png" alt="wallet payment method"/> 
                              <i> ( {{trans('common/wallet/text.text_money_balance')}} : {{ isset($mangopay_wallet_details->Balance->Amount)? $mangopay_wallet_details->Balance->Amount/100:'0'}}{{ isset($mangopay_wallet_details->Balance->Currency)? $mangopay_wallet_details->Balance->Currency:''}}) </i>
                            </label>
                         </div> 
                         <div class="clearfix"><hr style="width: 100%;margin-left: 2px;"></div>
                     <!-- End Wallet -->
                     <span class='error' id="payment_method_err">{{$errors->first('payment_method')}}</span>
                  </div> --}}
                  <div class="paypa_radio">
                    @if(isset($mangopay_wallet_details) && count($mangopay_wallet_details)>0)
                    @foreach($mangopay_wallet_details as $key=>$value)
                     <!-- Wallet -->
                     @if(isset($value->Balance->Amount) && $value->Balance->Amount > 0)
                         <div class="radio_area regi" onclick="javascript: return setOption('wallet');">
                            <input type="radio" name="payment_method" id="radio_wallet{{$key}}" class="css-checkbox" checked="" data-amount={{number_format($value->Balance->Amount/100,2)}} value="3" data-currency="{{ isset($value->Balance->Currency)? $value->Balance->Currency:''}}">
                            <label for="radio_wallet{{$key}}" class="css-label radGroup1">
                              <img style="width: 292px;height: 57px;margin-top:-20px;" src="{{url('/public')}}/front/images/archexpertdefault/arc-hexpert-wallet-logo.png" alt="wallet payment method"/> 
                              <i> ( {{trans('common/wallet/text.text_money_balance')}} : {{ isset($value->Balance->Amount)? number_format($value->Balance->Amount/100,2):'0'}} {{ isset($value->Balance->Currency)? $value->Balance->Currency:''}}) </i>
                            </label>
                         </div> 
                         <div class="clearfix"><hr style="width: 100%;margin-left: 2px;"></div>
                     <!-- End Wallet -->
                     @endif
                     @endforeach
                     @endif
                            <input type="hidden" name="currency_code" id="currency_code" value="">

                     <span class='error' id="payment_method_err">{{$errors->first('payment_method')}}</span>
                  </div>
                <!-- Payment Methods -->
                <div class="clr"></div>
                <div class="right-side">
                   <button class="normal-btn pull-right" value="submit" id="btn-form-payment-submit">{{trans('milestones/payment_methods.text_processed')}}</button>
                </div>
            </form>
         <div class="clr"></div>
      </div>
   </div>
</div>
<script type="text/javascript" src="{{url('/public')}}/assets/payment/jquery.payment.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript">
   $('#btn-form-payment-submit').click(function (){
        var total_cost     = '{{$total_cost}}';
        var wallet_amount  = $("input[name='payment_method']:checked").attr('data-amount');
        var currency_code  = $("input[name='payment_method']:checked").attr('data-currency');
        $('#currency_code').val(currency_code);
        $('#payment_method_err').html('');

        var check_currency = $('#currency_code').val();
        if(check_currency=='' || check_currency==null)
        {
          $("#payment_method_err").html('Sorry, Please select payment method.');
          return false;
        }
        if($('#radio_wallet').is(':checked')) {
         if(total_cost=='0' || total_cost=='0.0' || total_cost=='0.00'){
           $("#payment_method_err").html('Sorry, total cost should not 0USD.');
           return false;
         }
         if(parseFloat(total_cost)>parseFloat(wallet_amount)){
           $("#payment_method_err").html('Sorry, Your wallet amount is not suffeciant to make transaction.');
           return false;
         } 
      }
   });
</script>
@stop