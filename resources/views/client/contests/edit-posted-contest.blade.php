@extends('front.layout.auth')                
@section('main_content')
<style type="text/css">
  .droup-select.disabled:before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    z-index: 99;
    cursor: not-allowed;
}
</style>

<div class="gray-wrapper">
    <div class="container">
        <div class="post-contest">
            <div class="contest-title">
                <h2>{{trans('client/contest/post.text_contest')}}</h2>
                <p>{{trans('client/contest/post.text_contest_post_msg')}}</p>
            </div>
            <form action="{{ url($module_url_path) }}/update" method="POST" class="form-post-contests" enctype="multipart/form-data" files ="true">
                {{ csrf_field() }}
                <input type="hidden" name="contest_id" id="contest_id" value="{{isset($arr_info['id'])?base64_encode($arr_info['id']):'0'}}"> 
                <div class="white-wrapper">
                    @include('front.layout._operation_status')
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="user-box">
                                <div class="p-control-label">{{trans('client/contest/post.text_what_do_you_need')}}</div>
                                <div class="input-name">
                                    <div class="droup-select disabled">
                                        <select class="droup getcat mrns tp-margn" data-rule-required="true" name="category" id="category" >
                                            <option value="">{{trans('client/projects/post.text_select_category')}}</option>
                                            @if(isset($arr_categories) && sizeof($arr_categories)>0)
                                            @foreach($arr_categories as $categories)
                                            @if($categories['id']==$arr_info['category_id'])
                                            <option value="{{$categories['id']}}" selected="selected">@if(isset($categories['category_title'])){{$categories['category_title']}}@endif</option>
                                            @else
                                            <option value="{{$categories['id']}}">@if(isset($categories['category_title'])){{$categories['category_title']}}@endif</option>
                                            @endif
                                            @endforeach
                                            @endif
                                        </select>
                                        <span class='error'>{{ $errors->first('category') }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <div class="user-box">
                                <div class="p-control-label">Subcategory</div>
                                <div class="droup-select disabled">
                                    <select class="droup subcategory mrns tp-margn" data-rule-required="true" name="sub_category" id="sub_category">

                                    </select>
                                    <span class='error'>{{ $errors->first('sub_category') }}</span>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-12">
                            <div class="user-box character-input">
                                <div class="p-control-label">{{trans('client/contest/post.text_title')}}</div>
                                <input name="contest_name" value="{{$arr_info['contest_title'] or ''}}" id="contest_name" class="input-lsit beginningSpace_restrict" placeholder="{{trans('client/contest/post.text_contest_title_placeholder')}}" data-rule-required="true" type="text">
                                <span class='error'>{{ $errors->first('contest_name') }}</span>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="user-box">
                                <div class="p-control-label">{{trans('client/contest/post.text_contest_description')}}</div>
                                <h6 class="txt-edit" id="contest_description_msg">
                                    {{trans('client/projects/post.text_project_limit_description')}}
                                </h6>
                                <textarea id="cont_description" name="cont_description" value="{{$arr_info['contest_description'] or ''}}" data-rule-required="true" data-txt_gramm_id="21e619fb-536d-f8c4-4166-76a46ac5edce" data-rule-maxlength="1000" rows="10" cols="30" onkeyup="javascript: return textCounter(this,1000);" rows="7" cols="" type="text" class="text-area beginningSpace_restrict" placeholder="{{trans('client/contest/post.text_contest_description_placeholder')}}">{{$arr_info['contest_description'] or ''}}</textarea> 
                                <span class='error'>{{ $errors->first('cont_description') }}</span>
                            </div>
                        </div>
<!-- <div class="col-lg-12">
<div class="user-box">
<div class="p-control-label">{{trans('client/contest/post.text_contest_additonal_information')}}</div>
<h6 class="txt-edit" id="contest_additional_info_msg">
{{trans('client/projects/post.text_project_limit_addtional_info')}}
</h6>
<textarea id="contest_additional_info" name="contest_additional_info" value="{{$arr_info['contest_additional_info'] or ''}}" data-rule-required="false" {{-- data-txt_gramm_id="21e619fb-536d-f8c4-4166-76a46ac5edce" --}} data-rule-maxlength="1000" rows="10" cols="30" onkeyup="javascript: return textCounterForAddInfo(this,1000);" rows="7" cols="" type="text" class="text-area beginningSpace_restrict" placeholder="{{trans('client/contest/post.text_contest_addtional_info_placeholder')}}">{{$arr_info['contest_additional_info'] or 'N/A'}}</textarea> 
<span class='error'>{{ $errors->first('cont_additional_info') }}</span>
</div>
</div> -->
@php $skills_arr        = []; @endphp
@if(isset($project_details['project_skills']))
@foreach($project_details['project_skills'] as $project_skills)
@php $skills_arr[] = $project_skills['skill_id'];  @endphp
@endforeach
@endif
@php $contest_has_skills  = []; @endphp
@if(isset($arr_info['contest_skills']) && count($arr_info['contest_skills'])>0)
@foreach($arr_info['contest_skills'] as $key => $contest_skills)
@php array_push($contest_has_skills, isset($contest_skills['skill_id']) ? $contest_skills['skill_id']:"");  @endphp          
@endforeach
@endif
<div class="col-lg-12">
    <div class="user-box">
        <div class="p-control-label">{{trans('client/projects/ongoing_projects.text_skills')}}</div>
        <div class="input-name">
            <div class="droup-select multiselect-block">
                <select name="contest_skills[]" id="contest_skills" class="droup" multiple="multiple">
                    <option value="">{{trans('client/projects/post.text_select_skills_needed')}}</option>
                    @if(isset($arr_skills) && sizeof($arr_skills)>0)
                    @foreach($arr_skills as $skills)
                    @if(isset($skills['skill_name']))
                    <option value="{{$skills['id']}}"  @if(in_array($skills['id'],$contest_has_skills)) selected="selected" @endif>{{ $skills['skill_name']}}</option>
                    @endif
                    @endforeach
                    @endif
                </select>
                <span class='error' id="skill_name_error">{{ $errors->first('contest_skills') }}</span>
            </div>
            <div class="clr"></div>
            <div class="err_contest_skills">
            </div>
        </div>
    </div>
</div>
</div>
<div class="">
    <div class="row">
        <div class="col-lg-6">
         <div class="user-box">
               <div class="p-control-label">{{trans('client/contest/post.text_prize')}} <span class="small-label">( <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span> {{trans('client/contest/post.text_prize_msg')}})</span></div>
               <div class="prize-box">
                   <div class="droup-select disabled" style="width: 153px; z-index: 9;">
                         <select name="contest_currency" id="contest_currency" class="droup mrns tp-margn " data-rule-required="true" onchange="return set_minimum_currency_value1(this)">
                            @if(isset($arr_contest_currency) && sizeof($arr_contest_currency)>0)
                            <option value="">{{trans('client/projects/post.text_select_currency')}}</option>
                              @foreach($arr_contest_currency as $key => $currency)
                                <option value="{{$currency['currency_code']}}" 
                             @if(trim($arr_info['contest_currency']) == trim($currency['currency_code']))
                            selected="selected" 
                            @endif>
                                {{ isset($currency['currency_code'])?$currency['currency_code']:'' }} ( {{ isset($currency['description'])?$currency['description']:'' }} )
                                </option>
                              @endforeach
                            @endif
                         </select>
                         <span class='error'><!-- {{ $errors->first('contest_currency') }} --></span>
                   </div>
                   <input type="text" id="contest_price" value="{{isset($arr_info['contest_price'])?$arr_info['contest_price']:''}}" name="contest_price" class="clint-input char_restrict space_restrict" data-rule-required="true" min={{$arr_info['contest_price']}}  placeholder="{{trans('client/contest/post.text_contest_price_placeholder')}}">
                   <span class='contest_price_message' style="color:red;"></span>
                   <span class='error'>{{ $errors->first('contest_price') }}</span>
                   
                   <span class="contest-price-info"> Increase your contest prize now and attract higher skilled experts. </span>
               </div>
         </div>
     </div>
        {{-- <div class="col-lg-6">
            <div class="user-box">
                <div class="p-control-label">{{trans('client/contest/post.text_prize')}} <span class="small-label">( <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span> {{trans('client/contest/post.text_prize_msg')}})</span></div>
                <div class="prize-box">
                    <div class="droup-select" style="width: 153px;">
                        <select name="contest_currency" id="contest_currency" class="droup mrns tp-margn" data-rule-required="true">
                            @if(isset($arr_contest_currency) && sizeof($arr_contest_currency)>0)
                            <option value="">{{trans('client/projects/post.text_select_currency')}}</option>
                            @foreach($arr_contest_currency as $key => $currency)
                            <option value="{{$key}}"
                            @if(trim($arr_info['contest_currency']) == trim($key))
                            selected="selected" 
                            @endif>
                            {{$currency}}
                        </option>
                        @endforeach
                        @endif
                    </select>
                    <span class='error'><!-- {{ $errors->first('contest_currency') }} --></span>
                </div>
                <input type="text" readonly="true" value="{{$arr_info['contest_price'] or ''}}" id="contest_price"  name="contest_price" class="clint-input char_restrict space_restrict" value="0" data-rule-required="true" data-rule-min="1" data-rule-max="5000" placeholder="{{trans('client/contest/post.text_contest_price_placeholder')}}" style="cursor: not-allowed;">
                <span class='error'>{{ $errors->first('contest_price') }}</span>
            </div>
        </div>
    </div> --}}
    <div class="col-lg-6">
        <div class="user-box character-input">
            <div class="p-control-label">{{trans('client/contest/post.text_contest_end_date')}}</div>
            {{-- <div class="calender-input">

                <input id="contest_end_date" name="contest_end_date" value="{{date('Y-m-d',strtotime($arr_info['contest_end_date']))}}" type="text" class="clint-input cal-date" placeholder="{{trans('client/contest/post.text_contest_date_placeholder')}}" data-rule-required="true">
            </div> --}}
            <div class="calender-input droup-select">
           {{-- <input id="contest_end_date" name="contest_end_date" value="{{old('contest_end_date')}}" type="text" class="clint-input cal-date" placeholder="{{trans('client/contest/post.text_contest_date_placeholder')}}" data-rule-required="true"> --}}
           @php
              $created_at       = isset($arr_info['created_at']) ? date('Y-m-d',strtotime($arr_info['created_at'])) : '';
              $contest_end_date = isset($arr_info['contest_end_date']) ? date('Y-m-d',strtotime($arr_info['contest_end_date'])) : '';
              $array_diff       = get_array_diff_between_two_date($created_at,$contest_end_date);
              if(isset($array_diff['months']) && $array_diff['months']!='' && $array_diff['months']=='1')
              {
                $array_diff['days'] = 30;
              }
              $contest_entry_count = isset($arr_info['contest_entry']) ? count($arr_info['contest_entry']) : 0;
           @endphp
           <input type="hidden" name="hidden_contest_end_date" value="{{ isset($array_diff['days']) ? $array_diff['days'] : '' }}">
           <select name="contest_end_date" id="contest_end_date" class="droup mrns tp-margn" >
             <option value="">{{trans('client/contest/post.text_contest_date_placeholder')}}</option>
             {{-- <option @if(isset($array_diff['days']) && $array_diff['days'] == 7) selected="" @endif value="7">7 Days</option>
             <option @if(isset($array_diff['days']) && $array_diff['days'] == 14) selected="" @endif value="14">14 Days</option>
             <option @if(isset($array_diff['months']) && $array_diff['months'] == 1) selected="" @endif value="30">30 Days</option> --}}
            <option 
                @if(isset($array_diff['days']) && $array_diff['days'] >= 7 && 7 <= $array_diff['days']) 
                    selected="" 
                @endif 
                @if(isset($contest_entry_count) && $contest_entry_count>0 && $array_diff['days']) && $array_diff['days']!='' && 7 <= $array_diff['days']) 
                    disabled 
                @endif value="7">7 Days</option>
            <option 
                @if(isset($array_diff['days']) && $array_diff['days'] >= 14 && 14 <= $array_diff['days']) 
                    selected="" 
                @endif 
                @if(isset($contest_entry_count) && $contest_entry_count>0 && $array_diff['days']) && $array_diff['days']!='' && 14 <= $array_diff['days']) 
                    disabled 
                @endif value="14">14 Days</option>
            <option 
                @if(isset($array_diff['days']) && $array_diff['days'] >= 30 && 30 <= $array_diff['days']) 
                    selected="" 
                @endif 
                @if(isset($contest_entry_count) && $contest_entry_count>0 && $array_diff['days']) && $array_diff['days']!='' && 30 <= $array_diff['days']) 
                    disabled 
                @endif value="30">30 Days</option>
          </select>

           </div>
            <span class='error'>{{ $errors->first('contest_end_date') }}</span>
        </div>
    </div>
</div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="user-box character-input">
            <div class="p-control-label">{{trans('client/contest/post.text_contest_attachments')}}</div>
            {{-- <div class="photo-gallrey-section">
                <div class="change-pass-main margi-less">
                    <div class="add-busine-multi">
                        <span data-multiupload="5">
                            <span data-multiupload-holder id="data-multiupload-holder"></span>
                            <span class="upload-photo">
                                <img src="{{url('/public')}}/front/images/plus-img.png" class="img-responsive" alt="" />
                                <input data-multiupload-src class="upload_pic_btn" type="file" multiple="" name="project_documents[]" id="file">
                                <span data-multiupload-fileinputs></span>
                            </span>
                        </span>
                        <div class="clerfix"></div>
                    </div>
                    <div class="clearfix"></div>
                    <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span> {{trans('client/contest/post.text_contest_attachments_msg')}}
                </div>
            </div> --}}

            <div class="picture-div new-picture" style="" id="pictures_div">
                <div class="update-pro-img-main"> 
                    <div class="lab_img" id="lab_1">
                       <div class="col-sm-12 col-lg-12 col-lg-12" style="float:right;">                                       
                          <span>
                          <a href="javascript:void(0);" id='remove_project' class="remove_project" style="display:none;" >
                          <span class="glyphicon glyphicon-minus-sign" style="font-size: 20px;"></span>
                          </a>
                          </span>
                       </div>
                       <div class="" id="add_lab_div">
                          <div class="add_pht upload-pic loc_add_pht" id="div_blank" onclick="return addpictures(this)"  style="height: 120px;width: 120px; float: left;"> 
                            <img src="{{url('/public')}}/front/images/plus-img.png" alt="user pic"  style="width:100%;height:100%;" /></div>
                          <div class="show_photos" id="show_photos" style="width: auto; display: initial;float: none;"></div>
                          <div id="div_hidden_photo_list" class="div_hidden_photo_list">
                             <input type="file" name="contest_attachments[]" id="contest_attachments" class="contest_attachments" style="display:none" multiple="multiple" />
                          </div>
                       </div>
                    </div>
                 </div>                       
                    <input type="hidden" name="file_name_lab" id="file_name_lab"  >
                     
                <div class="clearfix"></div>                     
                <div class="error-red" id="err_other_image"></div>                                          
            </div>
            <br>
            <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span> {{trans('client/contest/post.text_contest_attachments_msg')}}

            <div class="clearfix"></div>
            @if(isset($arr_info['contest_post_documents']) && count($arr_info['contest_post_documents'])>0)
                <div class="file-new-upload-path upload-new-doc"><br>
                    <div class="p-control-label">{{trans('client/contest/post.text_contest_old_attachments')}}</div>
                        @foreach($arr_info['contest_post_documents'] as $contest_documents)

                        @php
                            $id                        = isset($contest_documents['id'])?$contest_documents['id']:0;
                            $id_multiupload_img_remove = "multiupload_img_remove".$id."_";
                            $id_multiupload_file       = 'id_multiupload_img_file';
                            $img_key                   = $id+1;  
                        @endphp

                        <div class="file-img" id="{{$id_multiupload_file}}{{$id}}">
                            <span class="upload-close">
                                <a href="javascript:void(0)" data-primary="{{$id}}" data-id="{{$id}}"  class="delete-img" data-image={{$contest_documents['image_name'] or ''}} id="{{$id_multiupload_img_remove}}{{$img_key}}"><i class="fa fa-trash-o"></i></a>
                            </span>
                            @if(isset($contest_documents['image_name']) && $contest_documents['image_name']!='' && file_exists('public/uploads/front/postcontest/'.$contest_documents['image_name']))
                                @php $explode = explode('.',$contest_documents['image_name']); @endphp
                                @if(isset($explode[1]))                        
                                    @if($explode[1] == 'jpg'  || $explode[1] == 'jpeg' || $explode[1] == 'gif'  || $explode[1] == 'png')
                                        @php $attc_src = url('/public').'/uploads/front/postcontest/'.$contest_documents['image_name']; @endphp
                                    @elseif(isset($explode[1]) && $explode[1] !="" && file_exists(base_path().'/public/front/images/file_formats/'.$explode[1].'.png'))
                                        @php $attc_src = url('/public/front/images/file_formats/'.$explode[1].'.png'); @endphp
                                    @else
                                        @php $attc_src = url('/public/uploads/front/default/default.jpg'); @endphp
                                    @endif
                                @endif
                            @endif
                                <img src="{{isset($attc_src)?$attc_src:''}}" title="{{$contest_documents['image_original_name'] or ''}}" class="img-responsive" alt="attachment" />
                                <input type="hidden" name="contest_documents[]" value="{{isset($contest_documents['image_name'])?$contest_documents['image_name']:''}}">
                        </div>

                        @endforeach
                </div>                               
            @endif
            </div>
        </div>
        <div class="clr"></div>
        <div class="col-lg-12">&nbsp;</div>
        <div class="clearfix"></div>
        <div class="col-lg-12">
            <button type="submit" class="black-btn" id="post-a-contest">{{trans('client/contest/post.text_update_a_contest')}}</button>
        </div>
    </div>
</div>
</form>
</div>
</div>
</div>
<!-- custom scrollbars plugin -->
<link href="{{url('/public')}}/front/css/select2.min.css" rel="stylesheet"/>
<script src="{{url('/public')}}/front/js/select2.full.js"></script>
<link rel="stylesheet" type="text/css"  href="{{url('/public')}}/front/css/jquery-ui.css"/>
<script src="{{url('/public')}}/front/js/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript">
    $(".form-post-contests").validate({
        errorElement: 'span',
        errorPlacement: function (error, element) 
        {
            if(element.attr("id") == 'contest_skills')
            {
                error.appendTo($(".err_contest_skills"));  
            }
            else
            {
                error.insertAfter(element);
            }
        }
    });

    $(document).ready(function(){
        var id = $('#category').val();
        var site_url ="{{url('/')}}";
        var contest_id = "{{$arr_info['id']}}";
        var dataString = 'id='+ id + '&contest_id='+ contest_id;
        var token = $('input[name="_token"]').val();  
        $.ajax
        ({
            type: "POST",
            url: site_url+'/client/contest/subcatdata_selected'+"?_token="+token,
            data: dataString,
            cache: false,
            success: function(html)
            {
                $(".subcategory").html(html);
            } 
        });   
    });

    $(".getcat").change(function()
    {  
        var id=$(this).val();
        var site_url ="{{url('/')}}";
        var contest_id = "{{$arr_info['id']}}";
        var dataString = 'id='+ id + '&contest_id='+ contest_id;
        var token = $('input[name="_token"]').val();  
        $.ajax
        ({
            type: "POST",
            url: site_url+'/client/contest/subcatdata'+"?_token="+token,
            data: dataString,
            cache: false,
            success: function(html)
            {
                $(".subcategory").html(html);
            } 
        });   
    });
</script>
<script type="text/javascript">
    /*date picker*/    
    $(function() {
        /*$("#contest_end_date").datepicker({ minDate: -0, maxDate: "+1M +10D",dateFormat: 'yy-mm-dd' });*/   
        $("#contest_end_date").datepicker({ minDate: +1, maxDate: "+1M",dateFormat: 'yy-mm-dd' });   
    });
</script>
<!--new profile image upload demo script start-->
<script type="text/javascript">
    function Upload() {
        $('.valide_upload').html('');
//Get reference of FileUpload.
$('.img-preview').attr('src', ""); 
var logo_id = document.getElementById("logo-id");
//Check whether the file is valid Image.
var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+()$");
if (regex.test(logo_id.value.toLowerCase())) {
//Check whether HTML5 is supported.
if (typeof (logo_id.files) != "undefined") {
    var file_extension = logo_id.value.substring(logo_id.value.lastIndexOf('.')+1, logo_id.length); 
    var size = logo_id.files[0].size;
    if(size > 25000000){
        $('.valide_upload').css('color','red');
        $("[for=logo-id]").html('');
        $('.valide_upload').html("{{trans('client/projects/post.text_project_validsizeupload')}}");
        document.getElementById("logo-id").value = "";
        return false;
    }
    if(file_extension != 'jpg' && 
        file_extension != 'jpeg' &&
        file_extension != 'gif' &&
        file_extension != 'png' &&
        file_extension != 'xlsx' &&
        file_extension != 'pdf' &&
        file_extension != 'docx' &&
        file_extension != 'doc' &&
        file_extension != 'txt' &&
        file_extension != 'odt'){
        $('.valide_upload').css('color','red');
    $("[for=logo-id]").html('');
    $('.valide_upload').html("{{trans('client/projects/post.text_project_validupload')}}");
    document.getElementById("logo-id").value = "";
    return false;
} 
//Initiate the FileReader object.
var reader = new FileReader();
//Read the contents of Image File.
reader.readAsDataURL(logo_id.files[0]);
reader.onload = function (e) {
//Initiate the JavaScript Image object.
var image = new Image();
//Set the Base64 string return from FileReader as source.
image.src = e.target.result;
//Validate the File Height and Width.
if(file_extension == 'xlsx' ||
    file_extension == 'pdf' ||
    file_extension == 'docx' ||
    file_extension == 'doc' ||
    file_extension == 'txt' ||
    file_extension == 'odt'){
    $('.img-preview').attr('src', site_url+'/front/images/file_formats/'+file_extension+'.png'); 
}
image.onload = function (e) {
    var height = this.height;
    var width  = this.width;
    $('.note').css('color','#646464');
    $('.valide_upload').html('');
    if(file_extension == 'jpg' || 
        file_extension == 'jpeg' ||
        file_extension == 'gif' ||
        file_extension == 'png'){
        $('.img-preview').attr('src', image.src);
}  
return true;
};
}
} else {
    $('.valide_upload').css('color','red');
    $("[for=logo-id]").html('');
    $('.valide_upload').html("This browser does not support HTML5.");
    document.getElementById("logo-id").value = "";
    return false;
}
} else {
    $('.valide_upload').css('color','red');
    $("[for=logo-id]").html('');
    $('.valide_upload').html("{{trans('client/projects/post.text_project_validupload')}}");
    document.getElementById("logo-id").value = "";
    return false;
}
}
</script>  
<script type="text/javascript">
    function textCounter(field,maxlimit){
        var countfield = 0;
        if ( field.value.length > maxlimit ) {
            field.value = field.value.substring( 0, maxlimit );
            return false;
        } else {
            countfield = maxlimit - field.value.length;
            var message = '{{trans('client/projects/invite_experts.text_you_have_left')}} <b>'+countfield+'</b> {{trans('client/projects/invite_experts.text_characters_for_description')}}';
            jQuery('#contest_description_msg').html(message);
            return true;
        }
    }
    function textCounterForAddInfo(field,maxlimit){
        var countfield = 0;
        if ( field.value.length > maxlimit ) {
            field.value = field.value.substring( 0, maxlimit );
            return false;
        } else {
            countfield = maxlimit - field.value.length;
            var message = '{{trans('client/projects/invite_experts.text_you_have_left')}} <b>'+countfield+'</b> {{trans('client/projects/invite_experts.text_characters_for_additional_info')}}';
            jQuery('#contest_additional_info_msg').html(message);
            return true;
        }
    }
    /* Initializing multiple skills data */
    $('#contest_skills').select2({
        maximumSelectionLength: 5,
        placeholder: '{{trans('client/projects/ongoing_projects.text_skills')}}',
        allowClear: true
    });
</script>
<!--new profile image upload demo script end-->     

<script type="text/javascript">

    var arr_job_document_formats = {!! json_encode($arr_job_document_formats) !!};
    var image1 = $("input[name='contest_documents[]']").map(function() {
        return $(this).val();
    }).get();
    

    $('.delete-img').on('click',function()
    {
        var image_div_id        = $(this).attr('id');
        var id                  = $(this).attr('data-id');
        var image_id            = "id_multiupload_img_file"+id;
        var image_primary_id    = $(this).attr('data-primary');
        var image_name          = $(this).attr('data-image');

        swal({
            title: "Are you sure ?",
            text: 'Do you really want to delete this record ?',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#C1C1C1",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function(isConfirm)
        {
            if(isConfirm==true)
            {
                $.ajax({
                    url: "{{$module_url_path}}/delete-document",
                    type:'POST',
                    data:{image_primary_id:image_primary_id,_token:"{{csrf_token()}}" },
                    success:function(data)
                    {
                        if(data == 'success')
                        {  
                            $("#"+image_div_id).remove();
                            $("#"+image_id).remove();
                            image1.splice($.inArray(image_name, image1),1);
                        }
                    }
                });                    
            }
        });
    });

</script>

<script type="text/javascript">    
    
    function addpictures(ref)
    { 
        var uploaded_images = $("input[name='contest_documents[]']").map(function() {
               return $(this).val();
           }).get();

        var new_images = $("input[name='contest_attachments[]']").map(function() {
               return $(this).val();
           }).get();

        var uploaded_images_count = uploaded_images.length;
        var new_images_count = new_images.length;

        var count = uploaded_images_count + new_images_count;
        
        if(count > 10)
        {
            swal('U can upload maximum 10 files');
            return false;
        }       

        $('#err_other_image').html('');
        var image_id = $(ref).closest('.lab_img').attr('id');
        var length = $('.lab_img').length;
        var view_photo_cnt = jQuery('#'+image_id).find('.photo_view').length                                
        jQuery('#'+image_id).last().find( ".div_hidden_photo_list" ).last().find( "input[name='contest_attachments[]']:last" ).click(); 
        jQuery('#'+image_id).last().find( ".div_hidden_photo_list" ).last().find( "input[name='contest_attachments[]']:last" ).change(function()
        { 
            var files      = this.files;
            var exist_file = $('#file_name_lab').val();
                 
            if(exist_file == files[0]['name']) 
            { 
                return false; 
            }
            else 
            {
                $('#file_name_lab').val(files[0]['name']);
                for (var i=0, l=files.length; i<l; i++) 
                {
                    var max_size = 5000000;
                    var current_size = files[i].size;
                    if (max_size>=current_size) 
                    {
                        var file = files[i];

                        var prjct_id = image_id.split('_');
                        jQuery('#'+image_id).find('#image'+prjct_id[1]+'_'+(view_photo_cnt+1)).attr('value',files[i]['name']);
                        var img, reader, xhr;
                        img = document.createElement("img");
                        reader = new FileReader();
                        img = new Image();      

                        var ext      =   files[i]['name'].split('.').pop();  

                        if ($.inArray(ext, arr_job_document_formats) == -1)
                        {                                          
                            swal('File type not allowed');
                            return false;
                        }
                        else
                        {
                            img.onload = function()
                            {
                                                 
                            }                            
                        }

                        reader.onload = (function (theImg) 
                        {      
                            if(ext == 'docx' || ext == 'doc' || ext == 'pdf' || ext == 'zip' || ext == 'mp3' || ext == 'odt' || ext == 'txt' || ext == 'xlsx')
                            {                          
                                var image_src = '{{url('/public')}}/front/images/file_formats/'+ext+'.png';      
                                return function (evt){                                       
                                    theImg.src = evt.target.result;                                
                                    var html = "<div class='photo_view2' onclick='remove_this(this);' style='width:120px;height:120%;position:relative;display: inline-block;'><img src="+ image_src +" class='add_pht' id='add_pht upload-pic' style='float: left; padding: 0px ! important; margin:0' width='120' height='120'><div class='overlay2'><span class='plus2'><i class='fa fa-trash-o' aria-hidden='true'></i></span></div></div>";
                                    jQuery('#'+image_id).last().find('.show_photos').append(html);
                                    jQuery('#'+image_id).last().find('.div_hidden_photo_list').append('<input type="file" name="contest_attachments[]" id="contest_attachments" class="contest_attachments" style="display:none" multiple="multiple"/>');   
                                    countImages();          
                                    $('#file_name_lab').val('');
                                    };
                            }
                            else if(ext == 'jpg' || ext == 'png' || ext == 'jpeg' || ext == 'gif' || ext == 'JPG' || ext == 'PNG' || ext == 'JPEG' || ext == 'GIF')
                            { 
                                return function (evt){                                       
                                    theImg.src = evt.target.result;                                
                                    var html = "<div class='photo_view2' onclick='remove_this(this);' style='width:120px;height:120%;position:relative;display: inline-block;'><img src="+ evt.target.result +" class='add_pht' id='add_pht upload-pic' style='float: left; padding: 0px ! important; margin:0' width='120' height='120'><div class='overlay2'><span class='plus2'><i class='fa fa-trash-o' aria-hidden='true'></i></span></div></div>";
                                    jQuery('#'+image_id).last().find('.show_photos').append(html);
                                    jQuery('#'+image_id).last().find('.div_hidden_photo_list').append('<input type="file" name="contest_attachments[]" id="contest_attachments" class="contest_attachments" style="display:none" multiple="multiple" />');   
                                    countImages();          
                                    $('#file_name_lab').val('');
                                    };
                            }
                            else
                            {
                                var image_src = '{{url('/public')}}/uploads/front/default/default.jpg';  
                                return function (evt){   
                                    theImg.src = evt.target.result;                                
                                    var html = "<div class='photo_view2' onclick='remove_this(this);' style='width:120px;height:120%;position:relative;display: inline-block;'><img src="+ image_src +" class='add_pht' id='add_pht upload-pic' style='float: left; padding: 0px ! important; margin:0' width='120' height='120'><div class='overlay2'><span class='plus2'><i class='fa fa-trash-o' aria-hidden='true'></i></span></div></div>";
                                    jQuery('#'+image_id).last().find('.show_photos').append(html);
                                    jQuery('#'+image_id).last().find('.div_hidden_photo_list').append('<input type="file" name="contest_attachments[]" id="contest_attachments" class="contest_attachments" style="display:none" multiple="multiple" />');   
                                    countImages();          
                                    $('#file_name_lab').val('');
                                    };
                            } 

                        }(img));
                        reader.readAsDataURL(file);                        
                    }
                    else
                    {
                        swal('File size should be less than 5MB');
                        return false;
                    }                    
                };
            }        
        });          
   } 

   function remove_this(elm)
    {
       var this_index = jQuery(elm).index();
       jQuery('.lab_img').find(".div_hidden_photo_list").find("input").eq(this_index).remove();
       jQuery(elm).remove();
       countImages();
    }  
  
    function countImages(){

        var imagecount = $(".photo_view2").length;
        if(imagecount>5){
            $('#err_other_image').html(err_max_images);
        }else{
            $('#err_other_image').html('');
        }
    }

</script>
<script type="text/javascript">
  function set_minimum_currency_value1()
  {
    var token     = "<?php echo csrf_token(); ?>";
    var currency = $("#contest_currency option:selected").val();
    var url="{{url('/get_minimum_charge')}}";
    $.ajax({
        type: 'post',
        contentType: 'application/x-www-form-urlencoded',
        
        data : { contest_currency : currency},
        url:url,
        headers: {
        'X-CSRF-TOKEN': token
        },
        success: function(response) {
          if(response.msg=='success')
          {
            $('#contest_price').attr({"min" : response.min_amount});
            $('#contest_price').val(response.min_amount);
            $('.contest_price_message').html('Minimum '+currency+ ' '+response.min_amount+ ' is must.' );

          }
        }
      });
    //alert(currency);
  }
</script>
@stop