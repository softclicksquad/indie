@extends('client.layout.master')                
@section('main_content')
<div class="col-sm-7 col-md-9 col-lg-9">
   <div class="right_side_section payment-section">
      <div class="head_grn">{{isset($page_title)?$page_title:''}}</div>
      @include('front.layout._operation_status')
      <div class="ongonig-project-section">
         <div class="dispute-head"  style="padding-top: 0px;">
            {{isset($arr_contest['contest_title'])?$arr_contest['contest_title']:''}}
         </div>
         <span><i class="fa fa-calendar" aria-hidden="true"></i></span> {{isset($arr_contest['created_at'])?date('d M Y',strtotime($arr_contest['created_at'])):''}}
         <div class="det-divider"></div>
         <div class="project-title">
         </div>
         <div class="clr"></div>
             @php $total_cost = 0; @endphp
             @if(isset($arr_contest) && $arr_contest['contest_price']!='')
              <?php  $total_cost += (float) $arr_contest['contest_price']; ?>
                <div class="project-list pro-list-ul">
                  <ul>
                    <li style="background:none;">
                      <span class="projrct-prce"> <span style="color:#737373;"><i>Contest prize</i> =</span> {{ isset($arr_contest['contest_currency'])?$arr_contest['contest_currency']:'' }}

                        @if(isset($arr_contest['contest_currency']) && $arr_contest['contest_currency'] == 'JPY')
                          {{isset($arr_contest['contest_price'])?$arr_contest['contest_price']:'0'}}
                        @else
                          {{isset($arr_contest['contest_price'])?number_format($arr_contest['contest_price'],2):'0'}} 
                        @endif
                      </span>
                    </li>
                  </ul>
                </div>
                <div class="clr"></div>
                <br/>
             @endif

             {{-- @if(isset($service_fee))
              @php $total_cost += $service_fee; @endphp
               <div class="project-list pro-list-ul">
                  <ul>
                     <li style="background:none;">
                          <span class="projrct-prce"> {{ isset($arr_contest['contest_currency'])?$arr_contest['contest_currency']:'' }}
                            {{isset($service_fee)?number_format($service_fee,2):'0'}} <span style="color:#737373;">(<i>Contest service fee</i>  @if(isset($service_fee_type) && $service_fee_type == 'prcentage')) ({{$percentage_cost}}%) @endif</span>
                          </span>
                     </li>
                  </ul>
               </div>
               <div class="clr"></div>
               <br/>
             @endif --}}
             
             {{-- <div class="project-list pro-list-ul">
                <ul>
                   <li style="background:none;">
                        <span class="projrct-prce"> <b>Total :</b> {{ isset($arr_contest['contest_currency'])?$arr_contest['contest_currency']:'' }}
                          {{isset($total_cost)?number_format($total_cost,2):'0'}}
                        </span>
                   </li>
                </ul>
             </div> --}}
             
            <div class="clr"></div>
            <br/>
            <form id="validate-form-payment" name="validate-form-payment" method="POST" action="{{url('/payment/paynow')}}">
                {{ csrf_field() }}
                
                <!-- Payment Methods --> 
                  <div class="paypa_radio">
                    @if(isset($mangopay_wallet_details) && count($mangopay_wallet_details)>0)
                    @foreach($mangopay_wallet_details as $key=>$value)
                     <!-- Wallet -->
                     {{-- @if(isset($value->Balance->Amount) && $value->Balance->Amount > 0) --}}
                     @if(isset($value->Balance->Currency) && $value->Balance->Currency==$contest_currency)
                         <div class="radio_area regi" onclick="javascript: return setOption('wallet');">
                            <input type="radio" name="payment_method" id="radio_wallet{{$key}}" class="css-checkbox"  data-amount={{number_format($value->Balance->Amount/100,2)}} value="3" data-currency="{{ isset($value->Balance->Currency)? $value->Balance->Currency:''}}" checked="checked">
                            <label for="radio_wallet{{$key}}" class="css-label radGroup1">
                              <img style="width: 292px;height: 57px;margin-top:-20px;" src="{{url('/public')}}/front/images/archexpertdefault/arc-hexpert-wallet-logo.png" alt="wallet payment method"/> 
                              <i> ( {{trans('common/wallet/text.text_money_balance')}} : 
                                @if(isset($arr_contest['contest_currency']) && $arr_contest['contest_currency'] == 'JPY')
                                  {{ isset($value->Balance->Currency)? $value->Balance->Currency:''}} {{ isset($value->Balance->Amount)? $value->Balance->Amount/100:'0'}} ) 
                                @else
                                  {{ isset($value->Balance->Currency)? $value->Balance->Currency:''}} {{ isset($value->Balance->Amount)? number_format($value->Balance->Amount/100,2):'0'}} ) 
                                @endif
                              </i>
                            </label>
                         </div> 
                         <div class="clearfix"><hr style="width: 100%;margin-left: 2px;"></div>
                     <!-- End Wallet -->
                     @endif
                     @endforeach
                     @endif
                    <input type="hidden" name="currency_code" id="currency_code" value="">

                     <span class='error' id="payment_method_err">{{$errors->first('payment_method')}}</span>
                  </div>
                <!-- Payment Methods -->
                  @if( (isset($mangopay_wallet_details[0]->Balance->Amount)) && ($arr_contest['contest_price'] > $mangopay_wallet_details[0]->Balance->Amount/100) && isset($mangopay_cards_details) && count($mangopay_cards_details)>0 && !empty($mangopay_cards_details))
                    <div class="pay-by-cards" {{-- style="padding-left: 5%" --}}>
                      <span class="projrct-prce"> <span style="color:#737373;"><i>Your saved credit card</i></span></span>
                      <div class="clearfix"><hr style="width: 100%;margin-left: 2px;"></div>
                      @foreach($mangopay_cards_details as $card_key => $card_value)
                        @php
                          $card_alias = $expiration_date = '';
                          if(isset($card_value->Alias) && $card_value->Alias!='') {
                              $arr_card_alias = explode('XXXXXX',$card_value->Alias);
                              $card_alias = isset($arr_card_alias[1])  ? $arr_card_alias[1] : '';
                          }
                          if(isset($card_value->ExpirationDate) && $card_value->ExpirationDate!='') {
                              $expiration_date = implode('/', str_split($card_value->ExpirationDate, 2));
                          }
                        @endphp
                        <div class="radio_area regi">
                              <input type="radio" onclick="javascript: return setIsPaymentByCard(this);"
                                     name="card_id" 
                                     id="radio_card_{{$card_value->Id}}" 
                                     class="css-checkbox" 
                                     value="{{$card_value->Id}}"
                                     data-card-currency="{{$card_value->Currency}}"
                                     data-is-payment-by-card="1"
                                     >
                              <label for="radio_card_{{$card_value->Id}}" class="css-label radGroup1">
                                <i> {{ isset($card_value->CardProvider)? $card_value->CardProvider: '' }} Card, ends with {{ isset($card_alias)? $card_alias: '' }} , expiry date {{ isset($expiration_date)? $expiration_date: '' }} </i>
                              </label>
                        </div> 
                        <div class="clearfix"><hr style="width: 100%;margin-left: 2px;"></div>
                      @endforeach
                        <div class="radio_area regi">
                              <input type="radio" onclick="javascript: return setIsPaymentByCard(this);"
                                     name="card_id" 
                                     id="radio_card_skip" 
                                     class="css-checkbox" 
                                     value=""
                                     data-card-currency=""
                                     data-is-payment-by-card="0"
                                     checked="" 
                                     >
                              <label for="radio_card_skip" class="css-label radGroup1">
                                <i> Payment by a different card </i>
                              </label>
                        </div> 
                        <div class="clearfix"><hr style="width: 100%;margin-left: 2px;"></div>
                    </div>
                  @endif
                <div class="clr"></div>

                <div class="pay-amt-details-main">
                    @if($arr_contest['contest_price'] > $mangopay_wallet_details[0]->Balance->Amount/100)
                    <div class="pay-amt-details">
                        <div class="total-amt-txt-section">
                            Contest Prize: 
                        </div>
                        <div class="total-amt-value-section">
                            {{ isset($arr_contest['contest_currency'])?$arr_contest['contest_currency']:'' }}
                            @if(isset($arr_contest['contest_currency']) && $arr_contest['contest_currency'] == 'JPY')
                              {{isset($arr_contest['contest_price'])?$arr_contest['contest_price']:'0'}}
                            @else
                              {{isset($arr_contest['contest_price'])?number_format($arr_contest['contest_price'],2):'0'}} 
                            @endif
                        </div>
                    </div>
                    <div class="pay-amt-details">
                        <div class="total-amt-txt-section">
                            Wallet Balance: 
                        </div>
                        <div class="total-amt-value-section">
                            @if(isset($arr_contest['contest_currency']) && $arr_contest['contest_currency'] == 'JPY')
                                {{ isset($mangopay_wallet_details[0]->Balance->Amount) ? $mangopay_wallet_details[0]->Balance->Amount/100 : '0'}}
                            @else
                                {{ isset($mangopay_wallet_details[0]->Balance->Amount) ? number_format($mangopay_wallet_details[0]->Balance->Amount/100,2) : '0.0'}}
                            @endif
                        </div>
                    </div>
                    <div class="pay-amt-details net-total-section">
                        <div class="total-amt-txt-section">
                            Sub Total: 
                        </div>
                        <div class="total-amt-value-section">
                            {{ isset($arr_contest['contest_currency'])?$arr_contest['contest_currency']:'' }}
                            @if(isset($arr_contest['contest_currency']) && $arr_contest['contest_currency'] == 'JPY')
                              {{ isset($contest_price_minus_balance) ? $contest_price_minus_balance : '0'}}
                            @else
                              {{ isset($contest_price_minus_balance) ? number_format($contest_price_minus_balance,2) : '0.0'}} 
                            @endif
                        </div>
                    </div>
                    <div class="pay-amt-details">
                        <div class="total-amt-txt-section">
                            Service Charges: 
                        </div>
                        <div class="total-amt-value-section">
                            {{ isset($arr_contest['contest_currency'])?$arr_contest['contest_currency']:'' }} 
                            @if(isset($arr_contest['contest_currency']) && $arr_contest['contest_currency'] == 'JPY')
                              {{ isset($new_service_charge) ? $new_service_charge : '0'}}
                            @else
                              {{ isset($new_service_charge) ? number_format($new_service_charge,2) : '0.0'}} 
                            @endif
                        </div>
                    </div>

                    <div class="pay-amt-details net-total-section">
                        <div class="total-amt-txt-section">
                            Total: 
                        </div>
                        <div class="total-amt-value-section">
                            {{ isset($arr_contest['contest_currency'])?$arr_contest['contest_currency']:'' }} 

                            @if(isset($arr_contest['contest_currency']) && $arr_contest['contest_currency'] == 'JPY')
                              {{ (float) $contest_price_minus_balance + (float) $new_service_charge  }} 
                            @else
                              {{ number_format((float) $contest_price_minus_balance + (float) $new_service_charge,2)  }} 
                            @endif
                            
                        </div>
                    </div>
                    {{-- <input type="hidden" name="contest_price" id="contest_price" value="{{$contest_price_minus_balance}}">
                    <input type="hidden" name="total_cost" id="total_cost" value="{{ (float) $contest_price_minus_balance + (float) $new_service_charge }}"> --}}
                    @else
                      <div class="pay-amt-details net-total-section">
                        <div class="total-amt-txt-section">
                            Total: 
                        </div>
                        <div class="total-amt-value-section">
                            {{ isset($arr_contest['contest_currency'])?$arr_contest['contest_currency']:'' }} 
                            @if(isset($arr_contest['contest_currency']) && $arr_contest['contest_currency'] == 'JPY')
                              {{isset($total_cost)?$total_cost:'0'}}
                            @else
                              {{isset($total_cost)?number_format($total_cost,2):'0'}} 
                            @endif
                             (Amount will be charged from the wallet)
                        </div>
                    </div>
                    @endif
                </div>
                
                  <input type="hidden" name="contest_price" id="contest_price" value="{{isset($total_cost)?$total_cost:'0'}}">
                  <input type="hidden" name="total_cost" id="total_cost" value="{{isset($total_cost)?$total_cost:'0'}}">

                <input type="hidden" name="payment_obj_id" id="payment_obj_id" value=" {{isset($arr_contest['id'])?$arr_contest['id']:'0'}}">
                <input type="hidden" name="transaction_type" id="transaction_type" value="8">
                <input type="hidden" name="is_payment_by_card" id="is_payment_by_card" value="0">
                <input type="hidden" name="card_currency" id="card_currency" value="">
                
                <input type="hidden" name="client_user_id" id="client_user_id" value=" {{isset($arr_contest['client_user_id'])?$arr_contest['client_user_id']:'0'}}">


                <div class="right-side">
                   <button class="normal-btn pull-right" value="submit" id="btn-form-payment-submit">{{trans('milestones/payment_methods.text_processed')}}</button>
                </div>
            </form>
         <div class="clr"></div>
      </div>
   </div>
</div>
<script type="text/javascript" src="{{url('/public')}}/assets/payment/jquery.payment.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript">
   
   function setIsPaymentByCard(ref){
      $('#card_currency').val($(ref).attr('data-card-currency'));
      $('#is_payment_by_card').val($(ref).attr('data-is-payment-by-card'));
   }

   $('#btn-form-payment-submit').click(function (){
        var total_cost     = '{{$total_cost}}';
        var wallet_amount  = $("input[name='payment_method']:checked").attr('data-amount');
        var currency_code  = $("input[name='payment_method']:checked").attr('data-currency');
        $('#currency_code').val(currency_code);

        $('#payment_method_err').html('');
        var check_currency = $('#currency_code').val();
        if(check_currency=='' || check_currency==null)
        {
          $("#payment_method_err").html('Sorry, Please select payment method.');
          return false;
        }
        if($('#radio_wallet').is(':checked'))
        {
          if(total_cost=='0' || total_cost=='0.0' || total_cost=='0.00')
          {
            $("#payment_method_err").html('Sorry, total cost should not 0.');
            return false;
          }

          if(parseFloat(total_cost)>parseFloat(wallet_amount)){
            $("#payment_method_err").html('Sorry, Your wallet amount is not suffeciant to make transaction.');
            return false;
         } 
      }
   });
</script>
@stop