@extends('client.layout.master')
@section('main_content')
<style type="text/css">
.msg-btn-auto-height {   
   padding: 4px 5px; font-size:13px;
}
 .sort-by {
    margin-bottom: -29px;
    height: 30px;
    border: 1px solid #ddd;
    border-radius: 3px;
    width: 70px;
}
</style>
<div class="col-sm-7 col-md-8 col-lg-9">
    <div class="search-grey-bx white-wrapper">
       <div class="head_grn">{{ trans('client/transactions/packs.text_heading') }}</div>
       <div class="table-responsive">
          <form id="frm_show_cnt" class="form-horizontal show-entry-form" method="POST" action="{{url('client/transactions/show_cnt')}}">
                {{ csrf_field() }}                
                <div class="sort-by">
                    @php $show_cnt ='show_transaction_cnt';  @endphp
                    @include('front.common.show-cnt-selectbox')
                </div>                
          </form> 
          <table id="TaBle" class="theme-table invoice-table-s table" style="border: 1px solid rgb(239, 239, 239); margin-bottom:0;">
             <thead class="tras-client-tbl">
                <tr>
                   <th>{{ trans('client/transactions/packs.text_invoice_id') }}</th>
                   <th>{{ trans('client/transactions/packs.text_transaction_type') }}</th>
                   <th>{{ trans('client/transactions/packs.text_wallet_transaction_id') }}</th>
                   <th>{{ trans('client/transactions/packs.text_payment_amount') }}</th>
                   <th>{{ trans('client/transactions/packs.text_payment_method') }}</th>
                   <th>{{ trans('client/transactions/packs.text_Payment_status') }}</th>
                   <th>{{ trans('client/transactions/packs.text_Payment_date') }}</th>
                   <th>{{ trans('client/transactions/packs.text_payment_action') }}</th>
                </tr>
             </thead>
             <tbody>
                @if(isset($arr_transactions['data']) && sizeof($arr_transactions['data'])>0)
                @foreach($arr_transactions['data'] as $transaction)
                <tr>
                   <td>{{isset($transaction['invoice_id'])?$transaction['invoice_id']:''}}</td>
                   <td>
                     
                      @if(isset($transaction['transaction_type']) && $transaction['transaction_type']=='1')
                      {{ trans('client/transactions/packs.text_pay_subscription') }}
                      @elseif(isset($transaction['transaction_type']) && $transaction['transaction_type']=='2')
                      {{ trans('client/transactions/packs.text_pay_milestone') }}
                      @elseif(isset($transaction['transaction_type']) && $transaction['transaction_type']=='3')
                      {{ trans('client/transactions/packs.text_pay_release_milestone') }}
                      @elseif(isset($transaction['transaction_type']) && $transaction['transaction_type']=='5')
                      {{ trans('client/transactions/packs.text_pay_project_payment') }}
                      @elseif(isset($transaction['transaction_type']) && $transaction['transaction_type']=='6')
                      Contest sealed entry 
                      @elseif(isset($transaction['transaction_type']) && $transaction['transaction_type']=='7')
                      Contest payment
                      @endif
                   </td>
                   <td>
                      {{isset($transaction['WalletTransactionId'])?$transaction['WalletTransactionId']:''}}
                   </td>
                   <td>{{isset($transaction['currency_code'])?$transaction['currency_code']:''}}&nbsp;{{isset($transaction['paymen_amount'])?number_format($transaction['paymen_amount'],2):'0' }}
                   </td>
                   <td>
                      @if(isset($transaction['payment_method']) && $transaction['payment_method']=='1')Paypal
                      @elseif(isset($transaction['payment_method']) && $transaction['payment_method']=='2') Stripe
                      @elseif(isset($transaction['payment_method'])&& $transaction['payment_method']=='0') Free 
                      @elseif(isset($transaction['payment_method'])&& $transaction['payment_method']=='3') Wallet 
                      @endif
                   </td>
                   <td>
                      @if(isset($transaction['payment_status']) && $transaction['payment_status']=='0'){{ trans('client/transactions/packs.text_fail')}}
                      @elseif(isset($transaction['payment_status']) && $transaction['payment_status']=='1')
                         {{ trans('client/transactions/packs.text_paid')}}
                      @elseif(isset($transaction['payment_status']) && $transaction['payment_status']=='2')
                         {{ trans('client/transactions/packs.text_paid')}}
                      @elseif(isset($transaction['payment_status']) && $transaction['payment_status']=='3')
                         {{ trans('client/transactions/packs.text_fail')}}
                      @elseif(isset($transaction['payment_status']) && $transaction['payment_status']=='4')
                         Refunded
                      @endif
                   </td>
                   <td>
                   @if(isset($transaction['payment_date']) && $transaction['payment_date']!='0000-00-00 00:00:00')
                   {{isset($transaction['payment_date'])?date("d-M-Y", strtotime($transaction['payment_date'])):''}}

                   @else
                      --
                   @endif
                   </td>
                         <td><a href="{{url('/client/transactions/show/'.base64_encode($transaction['id'])) }}"><span class="msg_btn-lnew ht-tp msg-btn-auto-height">
                            <i class="fa fa-eye"></i>
                               {{ trans('client/transactions/packs.text_action_view')}}
                         </span>
                      </a>
                   </td>
                </tr>
                @endforeach
                @else
                @endif
             </tbody>
          </table>
       </div>
    </div>
<!-- Paination Links -->
 @include('front.common.pagination_view', ['paginator' => $arr_transactions])
 <!-- Paination Links -->
 </div> 
@stop

