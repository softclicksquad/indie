@extends('client.layout.master')                
@section('main_content')
{{-- <div class="middle-container">
   <div class="container">
      <div class="row"> --}}
         <div class="col-sm-7 col-md-8 col-lg-9">
            <div class="search-grey-bx">
               <div class="head_grn">{{ trans('client/transactions/packs.text_heading_details') }}</div>
               @if(isset($transaction) && sizeof($transaction)>0)
               <div class="row">
                  <div class="col-sm-12 col-md-6 col-lg-6 mrgt">
                     <div class="row">
                        <div class="form_group">
                           <div class="col-sm-12 col-md-8 col-lg-5">
                              <div class="card_holder">{{ trans('client/transactions/packs.text_transaction_type') }} : </div>
                           </div>
                           <div class="col-sm-12 col-md-4 col-lg-7">
                              <div class="fill_input">  
                                 @if(isset($transaction['transaction_type']) && $transaction['transaction_type']=='1')Subscription
                                 @elseif(isset($transaction['transaction_type']) && $transaction['transaction_type']=='2')
                                 Milestone
                                 @elseif(isset($transaction['transaction_type']) && $transaction['transaction_type']=='3')Release Milestones 
                                 @elseif(isset($transaction['transaction_type']) && $transaction['transaction_type']=='5')Project Payment
                                 @endif
                              </div>
                           </div>
                           <div class="clr"></div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="form_group">
                           <div class="col-sm-12 col-md-8 col-lg-5">
                              <div class="card_holder">{{ trans('client/transactions/packs.text_invoice_id') }} : </div>
                           </div>
                           <div class="col-sm-12 col-md-4 col-lg-7">
                              <div class="fill_input">{{isset($transaction['invoice_id'])?$transaction['invoice_id']:''}}</div>
                           </div>
                           <div class="clr"></div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="form_group">
                           <div class="col-sm-12 col-md-8 col-lg-5">
                              <div class="card_holder">{{ trans('client/transactions/packs.text_payment_amount') }} : </div>
                           </div>
                           <div class="col-sm-12 col-md-4 col-lg-7">
                              <div class="fill_input">{{isset($transaction['currency'])?$transaction['currency']:''}}&nbsp;{{isset($transaction['paymen_amount'])?number_format($transaction['paymen_amount'],2):'0' }}</div>
                           </div>
                           <div class="clr"></div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="form_group">
                           <div class="col-sm-12 col-md-8 col-lg-5">
                              <div class="card_holder">{{ trans('client/transactions/packs.text_payment_method') }} : </div>
                           </div>
                           <div class="col-sm-12 col-md-4 col-lg-7">
                              <div class="fill_input"> 
                                 @if(isset($transaction['payment_method']) && $transaction['payment_method']=='1')Paypal @endif
                                 @if(isset($transaction['payment_method']) && $transaction['payment_method']=='2') Stripe @endif
                                 @if(isset($transaction['payment_method'])&& $transaction['payment_method']=='0') Free @endif
                                 @if(isset($transaction['payment_method'])&& $transaction['payment_method']=='3') Wallet @endif
                              </div>
                           </div>
                           <div class="clr"></div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="form_group">
                           <div class="col-sm-12 col-md-8 col-lg-5">
                              <div class="card_holder">{{ trans('client/transactions/packs.text_wallet_transaction_id') }} : </div>
                           </div>
                           <div class="col-sm-12 col-md-4 col-lg-7">
                              <div class="fill_input"> 
                                 {{isset($transaction['WalletTransactionId'])?$transaction['WalletTransactionId']:''}}
                              </div>
                           </div>
                           <div class="clr"></div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="form_group">
                           <div class="col-sm-12 col-md-8 col-lg-5">
                              <div class="card_holder">{{ trans('client/transactions/packs.text_Payment_status') }} : </div>
                           </div>
                           <div class="col-sm-12 col-md-4 col-lg-7">
                              <div class="fill_input">   
                                 @if(isset($transaction['payment_status']) && $transaction['payment_status']=='0')Fail @endif
                                 @if(isset($transaction['payment_status']) && $transaction['payment_status']=='1')Paid @endif
                                 @if(isset($transaction['payment_status']) && $transaction['payment_status']=='2')Paid @endif
                                 @if(isset($transaction['payment_status']) && $transaction['payment_status']=='3')Fail @endif 
                                 @if(isset($transaction['payment_status']) && $transaction['payment_status']=='4')Refunded @endif 
                              </div>
                           </div>
                           <div class="clr"></div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-12 col-md-6 col-lg-6 mrgt">
                     <div class="row">
                        <div class="form_group">
                           <div class="col-sm-12 col-md-5 col-lg-5">
                              <div class="card_holder">{{trans('client/transactions/packs.text_Payment_date') }} : </div>
                           </div>
                           <div class="col-sm-12 col-md-7 col-lg-7">
                              <div class="fill_input">{{isset($transaction['payment_date'])&&$transaction['payment_date']!='0000-00-00 00:00:00'?date("d-M-Y", strtotime($transaction['payment_date'])):'-'}}</div>
                           </div>
                           <div class="clr"></div>
                        </div>
                     </div>
                  </div>
               </div>
               @endif
            </div>
         </div>
     {{--  </div>
   </div>
</div> --}}
@stop

