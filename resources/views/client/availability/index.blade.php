@extends('client.layout.master')                
@section('main_content')
<div class="col-sm-7 col-md-9 col-lg-9">
   <form action="{{url('/client/update_availability')}}" method="POST" id="form-client_availability" name="form-client_availability" enctype="multipart/form-data" files ="true">
      {{ csrf_field() }}
      <div class="right_side_section">
         @include('front.layout._operation_status')
         <div class="head_grn">{{ trans('client/availability/availability.text_availability') }}</div>
         <div class="row">
            <div class="change-pwd-form">
               <div class="col-sm-12 col-md-12 col-lg-6">
                  <div class="user-box">
                     <div class="input-name">
                        <div class="radio_area regi">
                           <input type="radio" name="user_availability" id="radio_available" 
                           @if(isset($arr_client_details['is_available']) && ($arr_client_details['is_available']==1))
                           checked="" 
                           @endif 
                           class="css-checkbox"  value="1">
                           <label for="radio_available" class="css-label radGroup1">{{ trans('client/availability/availability.text_available') }}</label>
                        </div>
                        <div class="radio_area regi">
                           <input type="radio" name="user_availability" id="radio_not_available" 
                           @if(isset($arr_client_details['is_available']) && ($arr_client_details['is_available']==0))
                           checked="" 
                           @endif  
                           class="css-checkbox" value="0">
                           <label for="radio_not_available" class="css-label radGroup1">{{ trans('client/availability/availability.text_unavailable')  }}</label>
                        </div>
                     </div>
                     <span class='error'>{{ $errors->first('user_availability') }}</span>
                  </div>
               </div>
               <div class="col-sm-12 col-md-12 col-lg-12"><button class="normal-btn pull-right" type="submit">{{ trans('client/availability/availability.text_update') }}</button></div>
            </div>
         </div>
      </div>
   </form>
</div>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{url('/public')}}/assets/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript">
   $("#form-client_availability").validate({
         errorElement: 'span',
         });
   
</script>
@stop

