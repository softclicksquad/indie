@extends('client.layout.master')
@section('main_content')
@php $user = Sentinel::check(); @endphp
@if($user)
@php
$sidebar_information = sidebar_information($user->id);
//$sidebar_information        = sidebar_information($user->id);
$profile_img_public_path = url('/public').config('app.project.img_path.profile_image');
@endphp
@endif
<div class="col-sm-7 col-md-9 col-lg-9">
    @include('front.layout._operation_status')
    <div class="dashboard-box">
        <div class="head_grn">{{ trans('dashboard/dashboard.welcome_to_dashboard') }}</div>
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="pink-bx">
                    <div class="client-details-section-main">
                        <div class="client-details-user-pic">
                             @if(isset($profile_img_public_path) && isset($sidebar_information['user_details']['profile_image']) && $sidebar_information['user_details']['profile_image']!="" && file_exists('public/uploads/front/profile/'.$sidebar_information['user_details']['profile_image']))
                                <img class="img-responsive" id="set_image" style="cursor: default;" src="{{isset($sidebar_information['user_details']['profile_image'])?$profile_img_public_path.$sidebar_information['user_details']['profile_image']:''}}" />
                                @else
                                <img id="set_image" style="cursor: default;" src="{{$profile_img_public_path.'default_profile_image.png'}}" />
                                @endif

                            {{-- <img src="{{url('/public')}}/front/images/profile-pic-img.png" class="img-responsive" alt="" /> --}}
                        </div>
                        <div class="client-details-user-details">
                            <div class="client-details-user-name">
                                {{isset($sidebar_information['user_details']['first_name'])?$sidebar_information['user_details']['first_name']:'Unknown user'}}
                            </div>
                            <div class="client-details-designation">
                                {{-- {{isset($sidebar_information['user_type'])?ucfirst($sidebar_information['user_type']):''}} {{'Client'}} --}}
                            </div>
                            
                            <div class="country-flag-time-seciton">
                                @if(isset($sidebar_information['user_country_flag']))
                                    @php
                                    echo $sidebar_information['user_country_flag'];
                                    @endphp
                                @elseif(isset($sidebar_information['user_country']))
                                    @php
                                    echo $sidebar_information['user_country'];
                                    @endphp
                                @else
                                    @php
                                    echo '';
                                    @endphp
                                @endif
                                 <span>{{isset($sidebar_information['client_timezone_without_date'])?$sidebar_information['client_timezone_without_date']:'Not specify'}}</span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="total-rating-spend-section">
                        <div class="average-rating-section">
                            <div class="ave-rating-bx">
                                <span class="ave-span"> {{isset($sidebar_information['average_rating'])?$sidebar_information['average_rating']:'0'}}</span>
                                <span class="ave-title">{{ trans('common/client_sidebar.text_rating') }}</span>
                            </div>
                            <div class="ave-star-bx">
                                <ul>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star grey"></i></li>
                                    <li><i class="fa fa-star grey"></i></li>
                                </ul>
                                <span class="star-count">{{isset($sidebar_information['review_count'])?$sidebar_information['review_count']:'0'}} Reviews</span>
                            </div>
                        </div>
                        <div class="total-spend-section">
                            <div class="total-money-count">{{isset($sidebar_information['total_project_cost'])?$sidebar_information['total_project_cost']:'0'}}</div>
                            <div class="totlae money-title">Total Spend</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="pink-bx">
                    <div class="project-awarded-section">
                        <h2>Projects Awarded</h2>
                        <div class="project-award-bx">
                            <div class="awarded-count">{{ last_30_days_award_count_client($user->id)}}</div>
                            <div class="awarded-days">Last 30 Days</div>
                        </div>
                        <div class="project-award-bx awad-line">
                            <div class="awarded-count">{{isset($sidebar_information['awarded_project_count'])?$sidebar_information['awarded_project_count']:0}}</div>
                            <div class="awarded-days">Lifetime</div>
                        </div>

                    </div>

                    <div class="project-awarded-section remve-mrg-awd">
                        <h2>Work in Progress</h2>
                        <div class="project-award-bx">
                            <div class="awarded-count">{{isset($sidebar_information['ongoing_project_count'])?$sidebar_information['ongoing_project_count']:'0'}}</div>
                            <div class="awarded-days">Ongoing Projects</div>
                        </div>
                        <div class="project-award-bx awad-line">
                            <div class="awarded-count">{{get_active_milestone_count_client($user->id)}}</div>
                            <div class="awarded-days">Active milestones</div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="">
                    {{-- <img src="{{url('/public')}}/front/images/reputation-img.png" class="img-responsive" alt="" />  --}}
                        <?php $Acheive = (int) isset($sidebar_information['reputation'])?$sidebar_information['reputation']:'0.0';
                              $remain  = 100-$Acheive; 
                         ?>

                      <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                        <script type="text/javascript">
                          google.charts.load("current", {packages:["corechart"]});
                          google.charts.setOnLoadCallback(drawChart);
                          function drawChart() {
                            var data = google.visualization.arrayToDataTable([
                              ['Task', 'Percent'],
                              ['',{{$Acheive}}],
                              ['',{{$remain}}],
                            ]);

                            var options = {
                              title: 'Reputation',
                              pieHole: 0.4,
                            };

                            var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
                            chart.draw(data, options);
                          }
                        </script>
                      </head>
                      <body>
                        <div id="donutchart" style="height: 500px;"></div>
                      </body>



                </div>            
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="pink-bx">
                    <div class="currency-section">
                        <div class="total-currecy-bx">
                            <h2>Currency</h2>
                            <h3>Total</h3>
                            <div class="clearfix"></div>
                        </div>
                        @if(isset($arr_data['balance']) && count($arr_data['balance'])>0)
                        @foreach($arr_data['balance'] as $key=>$value)
                        <div class="currecy-bx">
                            <div class="currecy-name"> {{isset($arr_data['currency'][$key])?$arr_data['currency'][$key]:''}}</div>
                            <div class="currecy-count">{{isset($value)?number_format($value,2):''}}</div>
                            <div class="clearfix"></div>
                        </div>
                        @endforeach
                        @endif

                        
                </div>
            </div>
               </div>

        </div>
    </div>
</div>
@stop