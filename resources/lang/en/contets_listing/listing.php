<?php 
return [
 'text_contest'                    => 'Contest',
 'text_applied_contest'            => 'Applied Contest',
 'text_contests'                   => 'Contests',
 'text_prize'                      => 'Prize',
 'text_posted'                     => 'Posted',
 'text_contest_prize'              => 'Contest Prize',
 'text_status'                     => 'Status',
 'text_open'                       => 'Open',
 'text_ending'                     => 'Ending',
 'text_ended'                      => 'Ended',
 'text_left'                       => 'Left',
 'text_total_entries'              => 'Total Entries',	
 'text_contest_details'            => 'Contest Details',	
 'text_attachments'                => 'Attachments',	
 'text_skills_required'            => 'Skills Required',	
 'text_addition_information'       => 'Additional Information',	
 'text_project_brief'              => 'Job Brief',	
 'text_no_data_found'              => 'No Data Found',	
 'text_be_the_first_to_send_entry' => 'Be the first to send an entry!',	
 'text_employer'                   => 'Contest Owner',	
 'text_projects_posted'            => 'Jobs Posted',	
 'text_total_spent'                => 'Total Spend',	
 'text_send_entry'                 => 'Send Entry',	
 'text_all_categories'             => 'All categories',		
 'text_search'                     => 'Search',
 'text_categories'                 => 'Categories',	

 'text_gross_amount_includes_VAT_or_GST_in_contest_owner_country' => 'Gross amount, includes VAT or GST in Contest Owner country',		

 'text_FAQ'                        => 'FAQ',		
 'text_how_to_start_contest'       => 'How to start Contest',		
 'text_sign_in'                    => 'Sign in',		
 'text_complete_your_profile'      => 'Complete your profile',		
 'text_click_on_start_contest'     => 'Click on start contest',	
 'text_entry_details'              => 'View',	
 'text_enter_title'         	   => 'Enter Title',	
 'text_enter_your_entry_details'   => 'Enter your entry details',	
 'text_attachment_instructions'    => 'Attachment Instructions',	

 'text_Provide_general_info_about_your_entry_e_g_what_you_can_deliver_and_when_why_you_think_you_can_do_this_contest_etc'  => 'Provide some information about your idea and its benefits. Share your thoughts with the Contest Owner and try your best to get them excited about your concept.',	
 
 'text_select_atleast_one_image_to_upload_Only_jpg_png_jpeg_images_allowed_with_max_size_1mb_You_can_upload_upto_5_images_at_a_time' => 'Select at least one image to upload. Only jpg, png, jpeg images allowed with max size 10mb. You can upload up to 5 images at a time.',	
 
 'text_title'											=> 'Title',	
 'text_note'											=> 'Note',	
 'text_cancel'          			                    => 'Cancel',	
 'text_send_entry'          		                    => 'Send Entry',	
 'text_you_can_upload_only_5_images_at_a_time'          => 'You can upload only 5 images at a time.',
 'text_dictMaxFilesExceeded'                            => 'Max Files Exceeded.',
 'text_image_is_too_large_please_upload_upto_1mb_image' => 'Image is too large please upload upto 5mb image',
 'text_drop_your_images_here_to_upload'          	    => 'Drop your images here to upload',
 'text_this_field_is_required'          				=> 'This field is required.',
 'text_your_entry_has_been_send_successfully'         	=> 'Your entry has been send successfully.',
 'text_your_entry_files_has_been_send_successfully' 	=> 'Your entry files has been send successfully.',
 'text_error_occure_while_send_entry'          			=> 'Error occure while send entry.',
 'text_error_occure_while_send_entry_files'          	=> 'Error occure while upload entry files',
 
 'text_please_login_as_expert_to_download_and_see_original_attachment' => 'Note : Please log in as an expert to view and download the original attachment.',

'text_upload'                                           => 'Upload',
'text_upload_more'                                      => 'Upload More',
'text_remaining_files_to_upload'                        => 'Remaining files to upload',
'text_entries_received'                                 => 'Entries Received',
'text_shortlisted_entries'                              => 'Shortlisted Entries',
'text_rejected_entries'                                 => 'Rejected Entries',
'text_entry_will_be_follow_soon'                        => 'Entries will follow soon',


'text_sealed_entry'                                     => 'Sealed entry',
'text_sealed_entry_note'                                => 'Choose this option if you want only the Contest Owner can view your entries. Your files will be hidden to public and other contest participants.',
'text_wallet_balance'                                   => 'Wallet Balance',
'text_sealed_entry_amount'                              => 'Sealed entry amount',
'text_you_will_be_charged'                              => 'You will be charged',
'text_from_your_wallet_for_sealing_your_listing'        => 'from your wallet for sealing your listing',

'text_all_work_was_done_by_me_or_our_company'           => 'All work was done by me/our company.',
'text_the_work_was_partially_done_by_me_or_our_company' => 'The work was partially done by me/our company.',



]
?>