<?php 
return [
	'text_title'              => 'Package Subscription',
	'text_heading'            => 'Package Subscription',
	'text_moth'               => 'Month',
	'text_mo'                 => '/mo',
	'text_billed_monthly'     => 'Billed monthly',
	'text_bids'               => 'Bids',
	'text_topup_bid'          => 'Per extra bid',
	'text_categories'         => 'Categories',
	'text_skills'             => 'Skills',
	'text_favorites_projects' => 'Favorites Jobs',
	'text_email'              => 'Communication via email',
	'text_chat'               => 'Communication via chat',
	'text_pay_now'            => 'Pay Now ',
	];
?>