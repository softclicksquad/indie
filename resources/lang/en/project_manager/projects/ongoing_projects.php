<?php 
return [
		'text_est_time'              => 'Job Duration',
		'text_days'                  => 'Days',
		'text_more'                  => 'more',
		'text_skills'                => 'Skills',
		'text_view'                  => 'View',
		'text_milestones'            => 'Milestones',
		'text_ongoing_project_title' => 'Ongoing Jobs',
		'text_no_record_found'       => 'No entries yet.',
	   ];
?>