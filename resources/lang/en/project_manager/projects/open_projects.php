<?php 
return [
		'text_est_time'           => 'Job Duration',
		'text_days'               => 'Days',
		'text_skills'             => 'Skills',
		'text_view'               => 'View',
		'text_open_project_title' => 'Open Jobs',
		'text_no_record_found'    => 'No entries yet.',
		'text_bids'               => 'Bids',
		'text_project_duration'   => 'Job Duration',
	   ];
?>