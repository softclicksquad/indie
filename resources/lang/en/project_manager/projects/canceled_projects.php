<?php 
return [
		'text_est_time'               => 'Job Duration',
		'text_days'                   => 'Days',
		'text_more'                   => 'more',
		'text_skills'                 => 'Skills',
		'text_canceled_project_title' => 'Canceled Jobs',
		'text_no_record_found'        => 'No entries yet.',
	   ];
?>