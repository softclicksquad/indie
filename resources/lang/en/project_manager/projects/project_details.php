<?php 
return [
			'company_name'                    =>'Company Name :',
			'email_id'                        =>'Email ID :',
			'category'                        =>'Category',
			'days'                            =>'Days',
			'more'                            =>'more',
			'suggest'                         =>'Suggest',
			'unsuggest'                       =>'Unsuggest',
			'message'                         =>'Message',
			'duration'                        =>'Duration',
			'rating'                          =>'Rating',
			'review'                          =>'Review',
			'completion_rate'                 =>'Completion Rate',
			'reputation'                      =>'Reputation',
			'view'                            =>'View',
			'suggested'                       =>'Suggested',
			'username'                        =>'Username',
			'rating'                          =>'Rating',
			'skills'                          =>'Skills',
			'rejected'                        =>'Rejected',
			'project_completed'               =>'Job Completed',
			'currently_no_bids_are_available' =>'Currently No Bids Are Available !!',
			'days'                            =>'Days'
	   ];

?>5