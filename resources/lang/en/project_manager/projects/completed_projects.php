<?php 
return [
		'text_est_time'                => 'Job Duration',
		'text_days'                    => 'Days',
		'text_more'                    => 'more',
		'text_skills'                  => 'Skills',
		'text_completed_project_title' => 'Completed Jobs',
		'text_no_record_found'         => 'No entries yet.',
	   ];
?>