<?php 
return [
	'text_title'             => 'Reset Password',
	'text_heading'           => 'Reset Password',
	'text_password'          => 'Password',
	'text_confirm_password'  => 'Confirm Password',
	'text_reset'             => 'Reset',
	'entry_password'         => 'Enter Password',
	'entry_confirm_password' => 'Enter Confirmed Password',
    ];
?>