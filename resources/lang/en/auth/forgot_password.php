<?php 
return [
	'text_title'        => 'Forgot Password',
	'text_heading'      => 'Forgot Password',
	'text_email'        => 'Email address',
	'text_backto_login' => 'Back to login',
	'text_reset'        => 'Reset',
	'text_send'         => 'Send',
	'entry_email'       => 'Enter Email address',
    ];

?>