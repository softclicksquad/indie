<?php 
return [
	'text_login'           => 'Login',
	'text_email'           => 'Email Address ',
	'text_password'        => 'Password',
	'text_remember_me'     => 'Remember me',
	'text_forgot_password' => 'Forgot Password?',
	'text_dont_account'    => 'Don\'t have an account?',
	'text_register_here'   => 'Register here',
	'entry_email'          => 'Enter Email Address',
	'entry_password'       => 'Enter Password',
    ];
?>