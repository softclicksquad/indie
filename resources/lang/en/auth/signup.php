<?php 
return [
	'text_title'                                          => 'Sign up',
	'text_signup'                                         => 'Sign up',
	'text_heading'                                        => 'Sign up Client/Expert',
	'text_sign_up_client'                                 => 'Sign up Client',
	'text_sign_up_expert'                                 => 'Sign up expert',
	'text_already_account'                                => 'Already have an account?',
	'text_login_here'                                     => 'Log in here',
	'text_first_name'                                     => 'First Name',
	'text_last_name'                                      => 'Last Name',
	'text_user_name'                                      => 'User Name',
	'text_compny_name'                                    => 'Company Name (optional)',	
	'text_country'                                        => 'Country',
	'text_email'                                          => 'Email Address',
	'text_password'                                       => 'Password',
	'text_confirm_password'                               => 'Confirmed Password',
	'text_signup_as'                                      => 'Sign up as',
	'text_client'                                         => 'Client',
	'text_expert'                                         => 'Expert',
	'text_profile_image'                                  => 'Profile image (optional)',
	'text_browse'                                         => 'Browse',
	'text_home'                                           => 'Home',
	'text_lets_start_with'                                => 'Let\'s start with',
	'text_tell_us_what_youre_looking_for'                 => 'Tell us what you\'re looking for?',
	'text_i_want_to_hire_a_expert'                        => 'I want to hire a expert',
	'text_find_collaborate_with_and_pay_an_expert'        => 'Find, collaborate with, and pay an expert.',
	'text_hire'                                           => 'Hire',
	'text_or'                                             => 'OR',
	'text_i_m_looking_for_home_projets'                   => 'I\'m looking for home jobs',
	'text_find_freelance_projects_and_grow_your_business' => 'Find freelance jobs and grow your business.',
	'text_pick_one_or_more_of_the_proposed'               => 'Pick one or more of the proposed',
	'text_work'                                           => 'Work',
	'entry_first_name'                                    => 'Enter First Name',
	'entry_last_name'                                     => 'Enter Last Name',
	'entry_user_name'                                     => 'Enter User Name',
	'entry_compny_name'                                   => 'Enter Company Name',
	'entry_country'                                       => 'Select Country',
	'entry_email'                                         => 'Enter Email Address',
	'entry_password'                                      => 'Enter Password',
	'entry_confirm_password'                              => 'Enter Confirmed Password',
	"Congratulations"                                     => "Congratulations",
	'you_may_need_to_check_your_spam'                     => 'You may need to check your spam',
	'or'                                                  => 'or',
	'junk_folder'                                         => 'junk folder',
	'it_may_land_in_there'                                => 'it may land in there',
	'text_user_type'                                      => 'User Type',
	'text_user_type_select'                               => 'Select user type',
	'text_user_type_public'                               => 'I am a Business Client',
	'text_user_type_private'                              => 'I am an Individual',

    'text_i_am_a_client'                                  => 'I am a Client',
    'text_need_to_get_work_done'                          => 'Need to get work done online',
    'text_find_an_expertcolloborate_approve_amp_pay'      => 'Find an expert, collaborate, approve & pay',
    'text_hire_an_expert'                                 => 'Hire an Expert', 

    'text_i_am_an_expert'                                 => 'I am an Expert',
    'text_looking_for_freelance_project'                  => 'Looking for freelance jobs',
    'text_find_freelance_project_and_grow_your_business'  => 'Find freelance online work and grow your Business',
    'text_work'                                           => 'Work',
    ];
?>