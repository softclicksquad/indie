<?php 
return [
	'text_title'             => 'Change Password',
	'text_heading'           => 'Change your password',
	'text_current_password'  => 'Current Password',
	'text_new_password'      => 'New Password',
	'text_confirm_password'  => 'Confirm Password',
	'text_update'            => 'Update',
	'entry_current_password' => 'Enter Current Password',
	'entry_new_password'     => 'Enter New Password',
	'entry_confirm_password' => 'Enter Confirm Password',
	];

?>