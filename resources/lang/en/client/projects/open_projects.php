<?php 
return [
		'text_edit'                    => 'Edit',
		'text_est_time'                => 'Job Duration',
		'text_days'                    => 'Days',
		'text_more'                    => 'more',
		'text_skills'                  => 'Skills',
		'text_view'                    => 'View',
		'text_ongoing_project_title'   => 'Ongoing Jobs',
		'text_completed_project_title' => 'Completed Jobs',
		'text_canceled_project_title'  => 'Canceled Jobs',
		'text_project_duration'        => 'Job Duration',
		'text_open_project_title'      => 'Open Jobs',
		'text_no_record_found'         => 'No entries yet.',
		'text_bids'                    => 'Bids',
		'text_cancel'                  => 'Cancel',
	   ];
?>