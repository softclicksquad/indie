<?php 
return [
		'text_title'                  => 'Canceled Jobs',
		'text_heading'                => 'Canceled Jobs',
		'text_edit'                   => 'Edit',
		'text_est_time'               => 'Job Duration',
		'text_days'                   => 'Days',
		'text_more'                   => 'more',
		'text_skills'                 => 'Skills',
		'text_view'                   => 'View',
		'text_canceled_project_title' => 'Canceled Jobs',
		'text_update'                 => 'Update',
		'text_milestones'             => 'Milestones',
		'text_no_record_found'        => 'No entries yet.',
		'text_post_again'             => 'Post Again',
		'text_delete'                 => 'Delete',
	   ];
?>