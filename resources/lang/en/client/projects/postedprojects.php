<?php 
return [
		'text_title'            => 'Posted Jobs',
		'text_heading'          => 'Posted Jobs',
		'text_edit'             => 'Edit',
		'text_project_duration' => 'Job Duration',
		'text_est_time'         => 'Job Duration',
		'text_days'             => 'Days',
		'text_more'             => 'more',
		'text_skills'           => 'Skills',
		'text_edit'             => 'Edit',
		'text_no_record_found'  => 'No entries yet.',
		'text_cancel'           => 'Cancel',
		'text_handle_by'        => 'Handled By',
		'text_project_manager'  => 'Job Manager',
		'text_client'           => 'Client',
		'text_select'           => 'Select',
		'text_sort_by'          => 'Sort by',
		'text_bids'             => 'Bids',
		'text_posted_projects'  => 'Posted Jobs',
		'text_closed'           => 'CLOSED',
	   ];
?>