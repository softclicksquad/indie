<?php 
return [
	'text_title'             => 'Add Review',
	'text_review_rate'       => 'Rating',
	'text_add'               => 'Add',
	'text_review'            => 'Review',
	'text_date'              => 'Date',
	'text_comment'           => 'Comment',
	'text_category'          => 'Category:',
	'text_company_name'      => 'Company Name :',
	'text_email_id'          => 'Email ID :',
	'text_rating_type_one'   => 'Professional Appearance',
	'text_rating_type_two'   => 'Communication Quality',
	'text_rating_type_three' => 'Professional Expertise',
	'text_rating_type_four'  => 'Quality Of Work',
	'text_rating_type_five'  => 'Would hire again',
	'placeholder_comment'    => 'Please leave some comment about your experience with this contractor.',
	];
?>