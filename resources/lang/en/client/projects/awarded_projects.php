<?php 
return [
		'text_title'                  => 'Awarded Jobs',
		'text_heading'                => 'Awarded Jobs',
		'text_edit'                   => 'Edit',
		'text_est_time'               => 'Job Duration',
		'text_days'                   => 'Days',
		'text_more'                   => 'more',
		'text_skills'                 => 'Skills',
		'text_view'                   => 'View',
		'text_awarded_title'          => 'Awarded Jobs',
		'text_canceled_project_title' => 'Canceled Jobs',
		'text_applied_project_title'  => 'Applied Jobs',
		'text_update'                 => 'Update',
		'text_milestones'             => 'Milestones',
		'text_no_record_found'        => 'No entries yet.',
		'text_accepted_on'            => 'Accepted On:',
		'text_create_milestone'       => 'Create Milestone',
		'text_message_chat'           => 'Chat',
	   ];
?>