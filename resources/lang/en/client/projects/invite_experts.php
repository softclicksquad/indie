<?php 
return [
	'text_invite_expert'                                                                  => 'Invite',
	'text_experts_showed_in_a_select_box_based_on_selected_project_br_category_skills'    => 'Experts showed in a select box based on selected jobs category & skills',
	'text_select'                                                                         => 'Select',
	'text_invite'                                                                         => 'Invite',
	'text_email_address'                                                                  => 'Email Address',
	'text_add_a_message'                                                                  => 'Add A message',
	'text_you_have_left'                                                                  => 'You have left', 
	'text_characters_for_description'                                                     => 'characters for description.', 
	'text_characters_for_additional_info'                                                 => 'characters for addtional information.', 
	'text_check_bids'=>'Check Bids',
	'text_job_managed_by_manager' => 'Job managed by manager',
	'text_job_managed_by_recruiter' => 'Job managed by recruiter',
	'text_bid_already_placed' => 'Bid already placed',
	];
?>