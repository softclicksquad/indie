<?php 
return [
		'text_edit'                    =>  'Edit',
		'text_project_duration'        =>  'Job Duration',
		'text_est_time'                =>  'Job Duration',
		'text_days'                    =>  'Days',
		'text_more'                    =>  'more',
		'text_skills'                  =>  'Skills',
		'text_edit'                    =>  'Edit',
		'text_view'                    =>  'View',
		'text_milestones'              =>  'Milestones',
		'text_completed_project_title' =>  'Completed Jobs',
		'text_no_record_found'         =>  'No entries yet.',
		'text_your_rating'             =>  'Your Rating',
		'text_client_rating'           =>  'Client\'s Rating',
		'text_expert_rating'           =>  'Expert\'s Rating',
	   ];
?>