<?php 
return [
		'text_dispute'          => 'Add Dispute',
		'text_added_by'         => 'Added By',
		'text_dispute_details'  => 'Dispute Details',
		'text_comment'          => 'Comment...',
		'text_headline'         => 'Headline',
		'text_projects'         => 'Jobs',
		'text_no_record_found'  => 'No entries yet.',
		'text_project_duration' => 'Job Duration',
		'text_days'             => 'Days',
		"btn_dispute"           => "Add Dispute",
		"btn_view"              => "View",
		"text_added_by1"        => "Admin Comments",
		"text_added_by_admin"   => "Added by ".config('app.project.name')." admin.",
		"text_you_have_left"    => "You have left",
		"text_characters_for_description" => "characters for comment.",
		"text_project_limit_description" => "Maximum 1000 character of comment.",
		];
?>