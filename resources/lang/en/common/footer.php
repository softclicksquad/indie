<?php 
return [
		'thank_you_for_signup_newsletter'      => 'Please check your email, to confirm newsletter.',
		'problem_occured_in_signup_newsletter' => 'Problem occured in signup newsletter.',
		'text_already_subscribed'              => 'You have already subscribed to our newsletter.',
		'text_company_info'                    => 'Company Info',
		'text_browse_top_skills'               => 'Browse Top Skills',
		'text_connect_with_us'                 => 'Connect with Us',
		'text_contact_us'                      => 'Contact Us',
		'text_newsletter'                      => 'Subscribe for newsletter',
		'text_your_email'                      => 'Your email address',
		'text_follow_us'                       => 'Follow with us',
		'text_copyright'                       => "<span>Copyright </span> &copy; ".date('Y')." by <span>". config('app.project.name') ."</span>  All Rights Reserved.",
		'text_this_field_is_required'          => "This field is required.",
    ];
?>