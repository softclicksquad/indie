<?php 
return [
	"your_tickets" => "Your Tickets",
	"add_support_ticket" => "Add Support Ticket",
	"department" => "Department",
	"select_department_category" => "Select Department Category",
	"has_replied_to_support_ticket" => " has replied to support ticket ",
	"has_created_support_ticket" => " has created support ticket ",
	"has_reopened_support_ticket" => " has re-opened support ticket ",
	"priority" => "Priority",
	"select_priority" => "Select Priority",
	"urgent" => "Urgent",
	"normal" => "Normal",
];