<?php 
return [
	'text_title'                                   => 'Home',
	'text_heading'                                 => 'Home',
	'text_browse'                                  => 'Jobs',                  // Browse to Jobs
	'text_expert'                                  => 'Experts',
	'text_how_it_works'                            => 'How it works',
	'text_sign_up'                                 => 'Sign up',
	'text_login'                                   => 'Login',
	'text_post_job'                                => 'Post Job',
	'text_post_a_job'                              => 'Post a Job',
	'text_my_profile'                              => 'My profile',
	'text_my_wallet'                               => 'My wallet',
	'text_logout'                                  => 'Logout',
	'text_delete_account'                          => 'Delete Account',
	'text_dashboard'                               => 'Dashboard',
	'text_change_password'                         => 'Change Password',
	'text_messages'                                => 'Chats',
	'text_you_have'                                => 'You Have',
	'text_new_messages'                            => 'New Messages',
	'text_no_notifications'                        => 'No Notifications',
	'text_browse_categories'                       => 'Jobs by Categories',       // Browse Categories  to Jobs Categories
	'text_browse_categories_for_contest'           => 'Contests by Categories',       
	'text_browse_expertby_categories'              => 'Experts by Categories',       // Experts Categories  to Jobs Categories
	'text_no_categories'                           => 'No Categories Available',
	'text_browse_skills'                           => 'Jobs by Skills',             // Browse Skills to Jobs Skills
	'text_no_skills'                               => 'No Skills Available',
	'text_browse_jobs'                             => 'All Jobs',
	'text_browse_contest'                          => 'All Contests',
	'text_browse_experts'                          => 'All Experts',
	'text_browse_experts_skills'                   => 'Experts by Skills',
	'text_name'                                    => 'Name',
	'text_last_login'                              => 'Last Login',
	'text_active'                                  => 'Active',
	'text_jobs_matching_my_skills_and_categories'  => 'Jobs matching my profile',
	'text_contest_matching_my_skills_and_categories'  => 'Contests matching my profile',
    ];
?>