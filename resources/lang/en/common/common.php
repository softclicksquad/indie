<?php

return [
	'text_profession'=>'Profession',
	'text_categories'=>'Categories',
	'subject'=>'Subject',
	'message'=>'Message',
	'attachment'=>'Attachment',
	'browse'=>'Browse',
	'submit'=>'Submit',
	'support_ticket'=>'Support Ticket',
	'closed'=>'Closed',
];

?>