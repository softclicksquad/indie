<?php 
return [
	'text_expert_details'   => 'Expert Details',
	'text_first_name'       => 'First Name',
	'text_last_name'        => 'Last Name',
	'text_address'          => 'Address',
	'text_phone_number'     => 'Phone Number',
	'text_spoken_languages' => 'Spoken Languages',
	'text_rating'           => 'Rating',
	'text_projects'         => 'Jobs',
	'text_completion_rate'  => 'Completion Rate',
	'text_review'           => 'Review',
	'text_earned'           => 'Earned',
	'text_reputation'       => 'Reputation',
	'text_about_me'         => 'About Me',
	'text_username'         => 'Username',
	'text_location'         => 'Location',
	'text_local_timezone'   => 'Local Timezone',	
	'text_local_time'       => 'Local Time',
	'text_education'	    => 'Education',
	'text_experience'       => 'Experience',
	'text_certificates'     => 'Certificates',
	];
?>