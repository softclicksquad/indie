<?php 
return [
	'text_card_details'                 => 'Card Details',
	'text_card_number'                  => 'Card Number',
	'text_card_expiration_month_year'   => 'Card Expiration Month Year',
	'text_card_cv_code'                 => 'Card CV Code',
	'text_processed'                    => 'Pay now',
	'entry_card_number'                 => 'Enter Card Number',
	'entry_mm'                          => 'MM',
	'entry_yyyy'                        => 'YYYY',
	'entry_cv_code'                     => 'Enter CV Code',
	'text_project_manager_commision'    => 'Job manager commission',
	'text_month'                        => 'month',
	'text_year'                         => 'year',
	'text_project_manager_payment_note' => 'Cost for project manager',
	'text_recruiter_payment_note' => 'Cost for recruitment done by your recruiter',
	];
?>