<?php 
return [
	'text_title'                              => 'Job Milestones',
	'text_release'                            => 'Release',
	'text_paid'                               => 'Paid',
	'text_invoice'                            => 'Invoice',
	'text_update'                             => 'Update',
	'text_release'                            => 'Release',
	'text_pay'                                => 'Pay',
	'text_pending'                            => 'Pending',
	'text_release_request'                    => 'Send release request',
	'text_delete'                             => 'Delete',
	'text_released'                           => 'Released',
	'text_approved'                           => 'Approved',
	'text_delete'                             => 'Delete',
	'text_requested'                          => 'Requested',
	'text_approve'                            => 'Approve & Release',
	'text_approve_request_received_by_expert' => 'Please check expert\'s request and approve milestone payment.',
	'text_unsuccessful_payment'               => 'This milestone payment is unsuccessful, you can delete this milestone.',
	/*'text_no_record_found'                  => 'Sorry no record found !!',*/
	'text_no_record_found_1'                    => "Please create at least one milestone. The total of all milestones created may be up to",
	'text_no_record_found_2'                    => ", based on expert’s proposal.",
	'text_inprogress'                         => 'In Progress',
	'text_refund_request'                     => 'Request a Refund',
	'text_reject_request'                     => 'Reject Request',
	'text_work_in_progress'                   => 'Work in Progress',
	];

	

	
?>