<?php 
return [
    'text_chat'                    => 'Chat',
    'text_no_conversations'        => 'No Conversations',
    'text_refresh'                 => 'Refresh',
    'text_sorry_no_messages_found' => 'Sorry no messages found !!',
    'text_messages'                => 'Messages',
    'text_type_your_message_here'  => 'Please enter your message',
    "text_no_message"              => "No messages",
    "text_available"               => "Available",
    "text_month"                   => "Month",
    "text_second"                  => "Second",
    "text_minute"                  => "Minute",
    "text_hour"                    => "Hour",
    "text_day"                     => "Day",
    "text_week"                    => "Week",
    "text_year"                    => "Year",
    ];
?>