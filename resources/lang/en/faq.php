<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Faq Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'text_faq'             => 'FAQ',
    'text_faq_for_client'  => 'FAQ for Clients',
    'text_faq_for_expert'  => 'FAQ for ArchExperts',
    'text_search'          => 'Search',
];
