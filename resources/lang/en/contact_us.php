<?php 
return [
	'text_home'                                                                          => 'Home',
	'text_send_us_an_email'                                                              => 'Send us an Email',
	'text_name'                                                                          => 'Name',
	'text_email'                                                                         => 'Email',
	'text_phone'                                                                         => 'Phone',
	'text_subject'                                                                       => 'Subject',
	'text_message'                                                                       => 'Message',
	'text_send_message'                                                                  => 'Send Message',
	'text_our_location'                                                                  => 'Our Location',
	'text_contact_us'                                                                    => 'Contact Us',
	'text_send_a'                                                                        => 'Send a',
	'text_your_email_address_will_not_be_published_required_fields_are_marked'           => 'Your email address will not be published. Required fields are marked.',
    'text_enter_name'                                                                    => 'Enter Name',
    'text_enter_email'                                                                   => 'Enter Email',
    'text_enter_phone'                                                                   => 'Enter Phone',
    'text_enter_subject'                                                                 => 'Enter Subject',
    'text_enter_message'                                                                 => 'Enter Message',

    'text_report_a_problem'                                                              => 'Report a problem',
    'text_select'                                                                        => 'Select',
    'text_website_security'                                                              => 'Website Security',
    'text_there_is_a_bug_on_the_website'                                                 => 'There is a bug on the website',
    'text_expert_report'                                                                 => 'Expert report',
    'text_there_is_a_suspicious_activity'                                                => 'There is a suspicious activity',
    'text_other'                                                                         => 'Other',
    'text_optional'                                                                      => 'Optional',
    
	];
?>