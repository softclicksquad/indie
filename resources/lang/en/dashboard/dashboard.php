<?php 
return [
	'ongoing_projects'     => 'Ongoing Jobs',
	'completed_projects'   => 'Completed Jobs',
	'canceled_projects'    => 'Canceled Jobs',
	'awarded_projects'     => 'Awarded Jobs',
	'applied_projects'     => 'Bids on Jobs',
	'review_ratings'       => 'Review Ratings',
	'new_messages'         => 'New Messages',
	'open_projects'        => 'Open Jobs',
	'posted_projects'      => 'Posted Jobs',
	'milestone_release'    => 'Milestone Release Requests',
	'welcome_to_dashboard' => 'Welcome to Dashboard',
	'no_reviews_yet'       => 'No Reviews Yet',
	'of_5'                 => 'of 5.0',
	'text_my_contests'	   => 'My Contests',
    ];
?>