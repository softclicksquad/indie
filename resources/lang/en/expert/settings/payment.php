<?php 
return [
		'text_title'                   => 'Payment Settings',
		'text_heading'                 => 'Payment Settings',
		'text_paypal_client_id'        => 'Paypal Client ID',
		'text_paypal_secret_key'       => 'Paypal Secret Key',
		'text_stripe_secret_key'       => 'Stripe Secret Key',
		'text_stripe_publishable_key'  => 'Stripe Publishable Key',
		'text_update'                  => 'Update',
		'entry_paypal_client_id'       => 'Enter Paypal Client ID',
		'entry_paypal_secret_key'      => 'Enter Paypal Secret Key',
		'entry_stripe_secret_key'      => 'Enter Stripe Secret Key',
		'entry_stripe_publishable_key' => 'Enter Stripe Publishable Key',
		'text_paypal_email_id'         => 'Paypal Email ID',
		'entry_paypal_emial_id'        => 'Enter Paypal Email ID',
		"fill_your_payment_details"    => "Fill your payment details then only you are able to get paid for milestones payment , you have to only enter your paypal account email id.",
	];
?>