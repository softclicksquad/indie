<?php 
return [
		'text_edit'               => 'Edit',
		'text_est_time'           => 'Job Duration',
		'text_days'               => 'Days',
		'text_more'               => 'more',
		'text_skills'             => 'Skills',
		'text_view'               => 'View',
		'text_open_project_title' => 'Open Jobs',
		'text_no_record_found'    => 'No entries yet.',
	   ];
?>