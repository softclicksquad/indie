<?php 
return [
		'text_edit'                        => 'Edit',
		'text_est_time'                    => 'Job Duration',
		'text_days'                        => 'Days',
		'text_more'                        => 'more',
		'text_skills'                      => 'Skills',
		'text_view'                        => 'View',
		'text_ongoing_project_title'       => 'Ongoing Jobs',
		'text_update'                      => 'Update',
		'text_milestones'                  => 'Milestones',
		'text_no_record_found'             => 'No entries yet.',
		'text_sorry_no_record_found'       => 'No entries yet.',
		'text_accepted_on'                 => 'Accepted On',
		'text_no_milestones'               => 'No Milestones',
		'text_message_chat'                => 'Chat',
		'text_next_milestone'              => 'Next Milestone',
		'text_next_expiremilestone'        => 'Expired Milestones',
		'text_next_expire'                 => 'Expired',
		'project_delivery_remaining_days'  => 'Job delivery remaining days',
		'project_delivery_remaining_time'  => 'Job delivery remaining time',
	   ];
?>