<?php 
return [
		'text_title'                   => 'Bookmarks',
		'text_heading'                 => 'Bookmarks',
		'text_edit'                    => 'Edit',
		'text_est_time'                => 'Job Duration',
		'text_project_duration'        => 'Job Duration',
		'text_days'                    => 'Days',
		'text_more'                    => 'more',
		'text_skills'                  => 'Skills',
		'text_view'                    => 'View',
		'text_favourite_project_title' => 'Bookmarks',
		'text_sorry_no_record_found'   => 'No entries yet.',
		'confirm_add_to_favourite'     => 'Do you want to add this job to your favourites?',
		'confirm_remove_to_favourite'  => 'Do you want to remove this job from your favourites?',
	  ];
?>