<?php 
return [
	'text_title'                  => 'Post Job',
	'text_heading'                => 'Post Job',
	'text_category_title'         => 'Select Job Category of Work',
	'text_cat_subtitle'           => 'It\'s free to list your Job. We\'re not an agency and we don\'t charge a commission.',
	'text_project_details'        => 'Job Details',
	'text_project_skills'         => 'Job Skills',
	'text_project_description'    => 'Job description',
	'text_project_subdescription' => 'Include as much information as you can (e.g. audition and shoot dates) to receive more relevant applications.',
	'text_project_upload'         => 'Uploade File &amp; Attachment(Optional)',
	'text_project_timeframe'      => 'Job Timeframe',
	'text_project_start_date'     => 'Job Start Date',
	'text_project_duration'       => 'Expected Job Duration(Days)',
	'text_project_cost'           => 'Job Cost',
	'text_project_budget'         => 'Set Job Budget',
	'text_hourly_rate'            => 'Set an Hourly Rate',
	'text_hourly_title'           => 'Hourly Rate',
	'text_fixed_price'            => 'Fixed Price',
	'text_pay_fixed_price'        => 'Pay a Fixed Price',
	'text_handle_by'              => 'Handle By',
	'text_myself'                 => 'Myself',
	'text_project_manager'        => 'Job Manager',
	'text_post_job'               => 'Post Job',
	'text_terms'                  => 'You confirm that you have read and agree with',
	'text_terms1'                 => 'ArchExperts',
	'text_terms2'                 => 'Terms and Conditions',
	'text_terms3'                 => 'and',
	'text_terms4'                 => 'Privacy Policy',
	'entry_project_details'       => 'Enter Job Details',
	'entry_project_skills'        => 'Select Job Skills',
	'entry_project_description'   => 'Enter Job description',
	'entry_project_timeframe'     => 'Enter Job Timeframe',
	'entry_project_start_date'    => 'Select Start Date',
	'entry_project_end_date'      => 'Select End Date',
	'entry_project_duration'      => 'Your expected duration in days to complete job.',
	'entry_project_cost'          => 'Enter Job Cost',
	'entry_hourly_title'          => 'Enter Hourly Rate',
	'entry_fixed_price'           => 'Enter Fixed Price',
	];
?>