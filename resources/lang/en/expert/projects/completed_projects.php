<?php 
return [
		'text_edit'                    => 'Edit',
		'text_est_time'                => 'Job Duration',
		'text_days'                    => 'Days',
		'text_more'                    => 'more',
		'text_skills'                  => 'Skills',
		'text_view'                    => 'View',
		'text_completed_project_title' => 'Completed Jobs',
		'text_milestones'              => 'Milestones',
		'text_no_record_found'         => 'No entries yet.',
		'text_sorry_no_record_found'   => 'No entries yet.',
		'text_your_rating'             => ' Your Rating',
	   ];
?>