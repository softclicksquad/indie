<?php 
return [
		'text_added_by'         => 'Added By',
		'text_dispute'          => 'Dispute',
		'text_comment'          => 'Comment...',
		'text_headline'         => 'Headline',
		'text_projects'         => 'Jobs',
		'text_no_record_found'  => 'No Records Found.',
		'text_project_duration' => 'Job Duration',
		'text_days'             => 'Days',
		'text_dispute'          => 'Dispute',
		"btn_dispute"           => "Add Dispute",
		"btn_view"              => "View",	
		"text_you_have_left"    => "You have left",
		"text_characters_for_description" => "characters for comment.",
		"text_project_limit_description" => "Maximum 1000 character of comment.",
	   ];

?>