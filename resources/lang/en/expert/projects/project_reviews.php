<?php 
return [
	'text_title'                         => 'Add Review',
	'text_review_rate'                   => 'Review Rate',
	'text_add'                           => 'Add',
	'text_no_reviews_found'              => 'No reviews found !!!',
	'text_date'                          => 'Date',
	'text_rating'                        => 'Rating',
	'text_rating_type_one'               => 'Detail level of job description',
	'text_rating_type_two'               => 'Communication Quality',
	'text_rating_type_three'             => 'Collaboration in the project',
	'text_rating_type_four'              => 'Payment Speed',
	'text_rating_type_five'              => 'Would love to work for the client again',
	'text_review'                        => 'Review',
	'text_comment'                       => 'Comment',
	"send_delete_bid_attachment_request" => "Are you sure ? Do you want to delete bid attachment?",
	'placeholder_comment'    			 => 'Please leave some comment about your experience with that client.',
	];
?>