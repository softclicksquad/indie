<?php 
return [
		'text_title'                 => 'Posted Jobs',
		'text_heading'               => 'Posted Jobs',
		'text_edit'                  => 'Edit',
		'text_est_time'              => 'Job Duration',
		'text_days'                  => 'Days',
		'text_more'                  => 'more',
		'text_skills'                => 'Skills',
		'text_view'                  => 'View',
		'text_ongoing_project_title' => 'Ongoing Jobs',
		'text_awarded_project_title' => 'Awarded Jobs',
		'text_sorry_no_record_found' => 'No entries yet.',
		'text_accept'                => 'Accept',
		'text_reject'                => 'Reject',
		'text_awarded_on'            => 'Awarded On',
		"confirm_accept_project"     => "Are you sure? Do you want to accept this job?",
	   ];
?>