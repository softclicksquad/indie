<?php 
return [
		'text_est_time'               => 'Job Duration',
		'text_skills'                 => 'Skills',
		'text_view'                   => 'View',
		'text_days'                   => 'Days',
		'text_canceled_project_title' => 'Canceled Jobs',
		'text_milestones'             => 'Milestones',
		'text_no_record_found'        => 'No entries yet.',
	   ];
?>