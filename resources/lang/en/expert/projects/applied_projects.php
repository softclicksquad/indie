<?php 
return [
		'text_title'                 => 'Posted Jobs',
		'text_heading'               => 'Posted Jobs',
		'text_edit'                  => 'Edit',
		'text_est_time'              => 'Job Duration',
		'text_days'                  => 'Days',
		'text_more'                  => 'more',
		'text_skills'                => 'Skills',
		'text_view'                  => 'View',
		'text_ongoing_project_title' => 'Ongoing Jobs',
		'text_applied_project_title' => 'Bids on Jobs',
		'text_update'                => 'Update',
		'text_rejected'              => 'Rejected',
		'text_sorry_no_record_found' => 'No entries yet.',
		'text_bid_date'              => 'Bid Date',
		'text_bid_awarded'           => 'Awarded',
	   ];
?>