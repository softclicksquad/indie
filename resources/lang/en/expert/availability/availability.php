<?php 
return [
			'text_availability' => 'Your Availability',
			'text_available'    => 'Available',
			'text_unavailable'  => 'Not Available',
			'text_update'       => 'Update',
	];

?>