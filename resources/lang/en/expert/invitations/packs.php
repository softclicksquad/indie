<?php 
return [
	'text_title'                                         => 'Invitations',
	'text_heading'                                       => 'Invitations',
	'text_heading_details'                               => 'Invitation Details',
	'text_sorry_no_records_found'                        => 'Sorry no record found !!',
	'text_activated'                                     => 'Activated',
	'text_action'                                        => 'Action',
	"text_view"                                          => "View",
	"text_project_name"                                  => "Job name",
	"text_unique_id"                                     => "Invitation Number",
	"text_client"                                        => "Client",
	"text_notification"                                  => "Notification",
	"text_invite_date"                                   => "Invite date",
	"text_expired_on"                                    => "Expires in",
	"text_message"                                       => "Message",
	"text_Url"                                           => "Url",
	"text_expected_reasons"                              => "Expected Reasons",
	"text_may_be_project_have_awarded_to_someone_else"   => "It may be that the job has been awarded to someone else.",
	"text_may_be_project_has_canceled"                   => "The job may have been canceled.",
];
?>