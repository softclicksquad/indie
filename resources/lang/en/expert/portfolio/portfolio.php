<?php 
return [
	'text_title'             => 'Expert Portfolio',
	'text_heading'           => 'Expert Portfolio',
	'text_upload'            => 'Upload',
	'text_no_images_found'   => 'Sorry portfolio images are not available !!',
	'text_reputation'        => 'Reputation',
	'text_review'            => 'Reviews',
	'text_completion_rate'   => 'Completion Rate',
	'text_profile'           => 'Profile',
	'text_projects'          => 'Jobs',
	'text_portfolio'         => 'Portfolio',
	'text_no_reviews_found'  => 'No reviews found !!!',
	'text_since_on'          => 'Since :reg_year on ',
	'text_won_contests'    	 => 'Won Contests',
	'text_about_me'       	 => 'About me',
	'text_skills'        	 => 'Skills',
	'text_languages'       	 => 'Languages',
	'text_education'       	 => 'Education',
	'text_certification'     => 'Certifications',
	'text_experience'        => 'Experience',
	];
?>