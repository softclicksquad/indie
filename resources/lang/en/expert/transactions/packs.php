<?php 
return [
	'text_title'                  => 'Transactions',
	'text_heading'                => 'Transactions',
	'text_heading_details'        => 'Transaction Details',
	'text_sorry_no_records_found' => 'Sorry no record found !!',
	'text_moth'                   => 'Month',
	'text_mo'                     => '/mo',
	'text_invoice_id'             => 'Invoice Id',
	'text_transaction_type'       => 'Transaction Type',
	'text_wallet_transaction_id'  => 'Wallet Transaction Id',
	'text_payment_amount'         => 'Amount',
	'text_payment_method'         => 'Method',
	'text_Payment_status'         => 'Status',
	'text_Payment_date'           => 'Date',
	'text_email'                  => 'Communication via email',
	'text_chat'                   => 'Communication via chat',
	'text_pay_now'                => 'Get started',
	'text_activated'              => 'Activated',
	'text_action'                 => 'Action',
	"text_view"                   => "View",
];
?>