<?php 
return [
 'text_search_country'                    => 'Select Location',
 'placeholder_search_experts'             => 'Search Expert',
 'text_search'                            => 'SEARCH',
 'text_projects'                          => 'PROJECTS',
 'text_view_portfolio'                    => 'View Portfolio',
 'text_all_expert'                        => 'All Experts',
 'text_were_found_based_on_your_criteria' => 'were found based on your criteria',
 'text_sorry_no_records_found'            => 'Sorry no record found !!',
 'text_all'                               => 'All',
 'text_search_result'                     => 'Search Result',
 'text_recently_active'                   => 'Recently active',
 'text_ago'                               => 'ago',
 
 'text_select_rating'                     => 'Select Rating',
 'text_advanced'                          => 'Advanced',
 'text_hourly_rate'                       => 'Hourly Rate',
 'text_reset_filter'                      => 'Reset Filter',
 'text_showing'                           => 'Showing',
 'text_show'                              => 'Show',
 'text_results'                           => 'results',
 'text_projects_not_matching_experts_skills_or_categories_please_try_another_one'  => 'We didn\'t find any of your Posted Jobs matching with a profile of this expert.',
]
?>