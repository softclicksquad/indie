<?php 
return [
	'text_title'                                         => 'Einladungen',
	'text_heading'                                       => 'Einladungen',
	'text_heading_details'                               => 'Einladungsdetails',
	'text_sorry_no_records_found'                        => 'Entschuldigung kein Eintrag gefunden !!',
	'text_activated'                                     => 'Aktiviert',
	'text_action'                                        => 'Aktion',
	"text_view"                                          => "Aussicht",
	"text_project_name"                                  => "Projektname",
	"text_unique_id"                                     => "Einladungsnummer",
	"text_client"                                        => "Klient",
	"text_notification"                                  => "Benachrichtigung",
	"text_invite_date"                                   => "Datum einladen",
	"text_expired_on"                                    => "Verfällt in",
	"text_message"                                       => "Botschaft",
	"text_Url"                                           => "URL",
	"text_expected_reasons"                              => "Erwartete Gründe",
	"text_may_be_project_have_awarded_to_someone_else"   => "Es kann sein, dass der Auftrag an eine andere Person vergeben wurde.",
	"text_may_be_project_has_canceled"                   => "Der Auftrag wurde möglicherweise abgebrochen.",
];
?>