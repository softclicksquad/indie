<?php 
 return [ 
 "text_title" => "Paket Abonnementhistorie",
 "text_heading" => "Abonnementhistorie",
 "text_heading_details" => "Abonnementhistorie - Details",
 "text_sorry_no_records_found" => "Sorry, keine Datensätze gefunden !!",
 "text_moth" => "Monat",
 "text_mo" => "/mtl.",
 "text_invoice_id" => "Rechnungs-ID",
 "text_expiry_date" => "Enddatum",
 "text_start_date" => "Startdatum",
 "text_payment_status" => "Zahlungsstatus",
 "text_pack_name" => "Paketname",
 "text_pack_price" => "Paketpreis",
 "text_pack_validity" => "Gültigkeit",
 "text_no_of_bids" => "Anzahl Gebote",
 "text_top_up_bid_price" => "Preis pro zusätzliches Gebot",
 "text_no_of_cats" => "Anzahl der Kategorien",
 "text_no_of_skills" => "Anzahl der Fachkenntnisse",
 "text_favorites_projects" => "Lesezeichen",
 "text_email" => "Kommunikation per E-Mail",
 "text_chat" => "Kommunikation per Chat",
 "text_pay_now" => "Jetzt loslegen",
 "text_activated" => "Aktiviert",
 "text_action" => "Aktion",
 'text_fail'=>'Fehlgeschlagen',
 'text_paid'=>'Bezahlt',
 'text_no'=>'Nein',
 'text_yes'=>'Ja',
];