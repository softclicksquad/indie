<?php 
 return [ 
 "text_title" => "Passwort ändern",
 "text_heading" => "Ändere Dein Passwort",
 "text_current_password" => "Aktuelles Passwort",
 "text_new_password" => "Neues Passwort",
 "text_confirm_password" => "Bestätige neues Passwort",
 "text_update" => "Aktualisieren",
 "entry_current_password" => "Trage aktuelles Passwort ein",
 "entry_new_password" => "Trage neues Passwort ein",
 "entry_confirm_password" => "Bestätige neues Passwort",
];