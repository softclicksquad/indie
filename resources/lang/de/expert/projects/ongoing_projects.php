<?php 
 return [ 
 "text_edit"                        => "Bearbeiten",
 "text_est_time"                    => "Geschätzte Dauer",
 "text_days"                        => "Tage",
 "text_more"                        => "mehr",
 "text_skills"                      => "Fachkenntnisse",
 "text_view"                        => "Anzeigen",
 "text_ongoing_project_title"       => "Laufende Projekte",
 "text_update"                      => "Aktualisieren",
 "text_milestones"                  => "Meilensteine",
 "text_no_record_found"             => "Noch keine Einträge.",
 'text_accepted_on'                 => 'Angenommen am',
 'text_no_milestones'               => 'Noch Keine Meilensteine',
 'text_message_chat'                => 'Plaudern',
 'text_sorry_no_record_found'       => 'Noch keine Einträge.',
 'text_next_milestone'              => 'Volgende mijlpaal',
 'text_next_expiremilestone'        => 'Verlopen mijlpalen',
 'text_next_expire'                 => 'Niet meer geldig',
 'project_delivery_remaining_days'  => 'Resterende dagen projectlevering',
 'project_delivery_remaining_time'  => 'Resterende tijd projectlevering',

];