<?php 
return [
		'text_title' => 'Lesezeichen',
		'text_heading' => 'Lesezeichen',
        'text_edit' => 'Bearbeiten',
		'text_est_time' => 'Projektdauer',
		'text_project_duration' => 'Projektdauer',
		'text_days' => 'Tage',
		'text_more' => 'Mehr',
		'text_skills' => 'Fähigkeiten',
		'text_view' => 'Anzeigen',
		'text_favourite_project_title'=>'Lesezeichen',
		'text_sorry_no_record_found'=>'Noch keine Einträge.',

		'confirm_add_to_favourite' => 'Möchtest Du das Projekt zu Deinen Favoriten hinzufügen?',
		'confirm_remove_to_favourite' => 'Möchten Sie dieses Projekt aus Ihren Favoriten entfernen?',
		
	  ];

?>