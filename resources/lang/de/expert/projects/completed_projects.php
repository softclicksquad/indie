<?php 
 return [ 
 "text_edit" => "Bearbeiten",
 "text_est_time" => "Geschätzte Dauer",
 "text_days" => "Tage",
 "text_more" => "mehr",
 "text_skills" => "Fachkenntnisse",
 "text_view" => "Anzeigen",
 "text_completed_project_title" => "Fertiggestellte Projekte",
 "text_milestones" => "Meilensteine",
 "text_no_record_found" => "Noch keine Einträge.",

 'text_your_rating'=>' Deine Bewertung',
 'text_sorry_no_record_found' =>'Noch keine Einträge.',
];