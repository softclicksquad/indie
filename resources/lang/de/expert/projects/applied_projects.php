<?php 
 return [ 
 "text_title" => "Veröffentlichte Projekte",
 "text_heading" => "Veröffentlichte Projekte",
 "text_edit" => "Bearbeiten",
 "text_est_time" => "Geschätzte Dauer",
 "text_days" => "Tage",
 "text_more" => "mehr",
 "text_skills" => "Fachkenntnisse",
 "text_view" => "Anzeigen",
 "text_ongoing_project_title" => "Laufende Projekte",
 "text_applied_project_title" => "Meine Gebote",
 "text_update" => "Aktualisieren",
 "text_sorry_no_record_found" => "Noch keine Einträge.",
 /*'text_bid_date'=>'Bieten Datum',*/
 'text_bid_date'=>'Geboten am',
 "text_rejected" => 'Abgelehnt',
 'text_bid_awarded'=>'Vergbene',
];