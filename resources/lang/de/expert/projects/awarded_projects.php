<?php 
 return [ 
 "text_title" => "Veröffentlichte Projekte",
 "text_heading" => "Veröffentlichte Projekte",
 "text_edit" => "Bearbeiten",
 "text_est_time" => "Geschätzte Dauer",
 "text_days" => "Tage",
 "text_more" => "mehr",
 "text_skills" => "Fachkenntnisse",
 "text_view" => "Anzeigen",
 "text_ongoing_project_title" => "Laufende Projekte",
 "text_awarded_project_title" => "Vergebene Projekte",
 "text_sorry_no_record_found" => "Noch keine Einträge.",
 "text_accept" => "Akzeptieren",
 "text_reject" => "Ablehnen",
 "confirm_accept_project" => "Möchtest Du den Projektauftrag annehmen?",
 "text_awarded_on"=>"Vergeben am",
];