<?php 
 return [ 
 "text_est_time" => "Geschätzte Dauer",
 "text_skills" => "Fachkenntnisse",
 "text_view" => "Anzeigen",
 "text_days" => "Tage",
 "text_canceled_project_title" => "Stornierte Projekte",
 "text_milestones" => "Meilensteine",
 "text_no_record_found" => "Noch keine Einträge.",
];