<?php 
 return [ 
 "text_title"                         => "Rezension hinzufügen",
 "text_review_rate"                   => "Rezensionsbewertung",
 "text_add"                           => "Hinzufügen",
 "text_no_reviews_found"              => "Keine Rezensionen vorhanden",
 "text_date"                          => "Datum",
 "text_rating"                        => "Bewertung",
 "text_review"                        => "Rezensionen",
 "text_comment"                       => "Kommentar",
 "send_project_completion_request"    => "Möchtest Du wirklich einen Projektabschluss beim Auftraggeber beantragen?",
 "send_delete_bid_attachment_request" => "Bist du sicher ? Möchten Sie Gebotszusatz löschen?",
 "placeholder_comment"                => "Bitte hinterlassen Sie einen Kommentar zu Ihren Erfahrungen mit diesem Kunden.",
 'text_rating_type_one'               => 'Detailtiefe der Jobbeschreibung',
 'text_rating_type_two'               => 'Qualität der Kommunikation',
 'text_rating_type_three'             => 'Zusammenarbeit im Projekt',
 'text_rating_type_four'              => 'Geschwindigkeit der Bezahlung',
 'text_rating_type_five'              => 'Würde für den Kunden gerne wieder arbeiten',
];