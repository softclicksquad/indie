<?php 
 return [ 
 "text_added_by" => "Hinzugefügt durch",
 "text_dispute" => "Problem Melden",
 "text_comment" => "Kommentar...",
 "text_headline" => "Überschrift",
 "text_projects" => "Projekte",
 "text_no_record_found" => "Noch keine Einträge.",
 "text_project_duration" => "Projektdauer",
 "text_days" => "Tage",
 "btn_dispute" => "Problem Melden",
 "btn_view" => "Anzeigen",
	"text_you_have_left"    => "You have left",
	"text_characters_for_description" => "characters for comment.",
	"text_project_limit_description" => "Maximum 1000 character of comment.",
];