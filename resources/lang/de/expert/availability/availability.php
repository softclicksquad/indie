<?php 
 return [ 
 "text_availability" => "Deine Verfügbarkeit",
 "text_available" => "Verfügbar",
 "text_unavailable" => "Nicht verfügbar",
 "text_update" => "Aktualisieren",
];