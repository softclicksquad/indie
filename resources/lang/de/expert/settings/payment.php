<?php 
 return [ 
 "text_title" => "Zahlungseinstellungen",
 "text_heading" => "Zahlungseinstellungen",
 "text_paypal_client_id" => "PayPal Kunden ID",
 "text_paypal_secret_key" => "PayPal Geheimschlüssel",
 "text_stripe_secret_key" => "Stripe Geheimschlüssel",
 "text_stripe_publishable_key" => "Stripe öffentlicher Schlüssel",
 "text_update" => "Aktualisieren",
 "entry_paypal_client_id" => "Trage Deine Paypal Kunden ID ein",
 "entry_paypal_secret_key" => "Trage Deinen Paypal Geheimschlüssel ein",
 "entry_stripe_secret_key" => "Trage Deinen Stripe Geheimschlüssel ein",
 "entry_stripe_publishable_key" => "Trage Deinen öffentlichen Stripe Schlüssel ein",

 'text_paypal_email_id' => 'Paypal E-Mail-ID',
 'entry_paypal_emial_id' => 'Trage hier Deine Paypal-Email-Adresse ein',

 "fill_your_payment_details" => "Trage Deine Paypal-Email-Adresse ein, um Zahlungen für die fertiggestellten und vom Auftraggeber freigegebenen Meilensteine zu erhalten.",
];