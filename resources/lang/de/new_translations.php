<?php 

return [
"info" => "Info", 
"upload_correct_size_image_for_portfoilo" => "Bitte lade das Portfoliobild in der Größe von mindestens 720x480 Pixels (BxH) hoch. Falls Du ein Bild in der Größe über 1080x720 Pixels hochladen wirst, wird es auf die Größe 1080x720 Pixel (BxH) komprimiert.", 
"generate_invoice" => "Erstelle Rechnung", 
"from" => "Von", 
"to" => "An", 
"title" => "Titel", 
"cost" => "Kosten", 
"invoice" => "Rechnung", 
"project_name" => "Projektname", 
"date" => "Datum", 
"milestone_name" => "Meilensteinname", 
"milestone_cost" => "Meilensteinkosten", 
"including_vat" => "inklusive MwSt.", 
"vat" => "Mehrwertsteuer", 
"logo" => "Logo", 
"create_invoice" => "Erstellen", 
"projects" => "Projekte", 
"fill_your_payment_details" => "Fülle Deine Zahlungdetails aus, denn nur so Du wirst im Stande sein, für Meilensteine Zahlungen zu erhalten. Es reicht, nur Deine Paypal-Konto E-Mail-Adresse einzutragen.", 
"fill_your_payment" => "Fülle Deine Zahlungdetails aus, denn nur so Du wirst im Stande sein, für Meilensteine Zahlungen zu erhalten.", 
"bid_proposal" => "Gebot", 
"estimated_duration" => "geschätzte Dauer", 
"days" => "Tage", 
"bid_cost" => "Gebotskosten", 
"bid_description" => "Gebotsbeschreibung", 
"view" => "Anzeigen", 
"project_description" => "Projektbeschreibung", 
"more" => "mehr", 
"this_field_is_required" => "Dieses Feld ist ein Pflichtfeld.", 
"enter_comments" => "Trage Deinen Kommentar ein.", 
"submit" => "Senden", 
"rating" => "Bewertung", 
"no_rating_yet" => "Noch keine Bewertung", 
"upload_your_work" => "Lade Deine Arbeit hoch", 
"browse" => "Durchsuchen", 
"you_can_add_upto_25_files" => "Du kannst bis zu 25 Dateien pro Projekt hochladen.", 
"sr_no" => "SN:", 
"document_name" => "Dokumentenname", 
"download" => "Herunterladen", 
"download_project_attchment" => "Projektanhang herunterladen", 
"lifetime" => "Unbegrenzt", 
"subscription" => "Abonnement", 
"milestone" => "Meilenstein", 
"release_milestones" => "Meilensteine freigeben", 
"topup_bid" => "Zusatzgebot", 
"paypal" => "Paypal", 
"stripe" => "Stripe", 
"free" => "Frei", 
"fail" => "fehlgeschlagen", 
"paid" => "Bezahlt", 
"sign_up_as_a_experts" => "Als Experte anmelden", 
"sign_up_as_a_client" => "Als Auftraggeber anmelden", 
"already_have_an_account" => "Du hast bereits ein Konto?", 
"log_in_here" => "Hier einloggen", 
"first_name" => "Vorname", 
"last_name" => "Nachname", 
"phone_number" => "Telefonummer", 
"country" => "Land", 
"select_country" => "Wähle Land", 
"state" => "Bundesland", 
"city" => "Stadt", 
"zip_code" => "Postleitzahl", 
"address" => "Strasse + Hausnummer", 
"vat_number" => "Umsatzsteuer-ID", 
"email_address" => "E-Mail Adresse", 
"password" => "Passwort", 
"confirmed_password" => "Bestätige Passwort", 
"profile_image" => "Profilbild", 
"sign_up" => "Anmelden", 
"select_city" => "Wähle Stadt", 
"select_state" => "Wähle Bundesland", 
"optional_details" => "Optionale Details", 
"company_name" => "Firmenname", 
"ceo_owner_name" => "Geschäftsführer/Inhaber", 
"company_size" => "Firmengröße", 
"founding_year" => "Gründungsjahr", 
"select_year" => "Wähle Jahr", 
"enter_first_name" => "Trage Deinen Vornamen ein", 
"enter_last_name" => "Trage Deinen Nachnamen ein", 
"enter_phone_number" => "Trage Deine Telefonnummer ein", 
"enter_state" => "Trage Budesland ein", 
"enter_city" => "Trage Stadt ein", 
"enter_zip_code" => "Trage die Postleitzahl ein", 
"enter_address" => "Trage Strasse + Hausnummer ein", 
"enter_vat_number" => "Trage die Umsatzsteuer-ID ein", 
"enter_email_address" => "Trage die E-Mail-Adresse ein", 
"enter_password" => "Trage Dein Passwort ein", 
"enter_confirmed_password" => "Trage Dein bestätigtes Passwort ein", 
"enter_company_name" => "Trage den Firmennamen ein", 
"enter_ceo_owner_name" => "Trage den Geschäftsführer/Inhaber ein", 
"enter_company_size" => "Trage die Firmengröße ein", 
"upload_profile_picture" => "Profilbild hochladen", 
"crop_image" => "Bild zuschneiden", 
"rotate_left" => "Nach links drehen", 
"deg" => "Grad", 
"rotate_right" => "Nach rechts drehen", 
"set_profile_picture" => "Profilbild einstellen", 
"attachment" => "Anhang", 
"go_to_website" => "Zur Website", 
'link_go_to_profile'      => 'Zum Profil',
"you_have_subscribed" => "Sie haben abonniert", 
"newsletter_successfully" => "Newsletteranmeldung erfolgreich", 
"home" => "Home", 
"why_choose_us" => "Why <br> Kies ons", 
"how_it_works" => "Hoe het werkt", 
"post_your_project" => "Veröffentliche ein Projekt",

"register_with_virtual_home" => "<br/>Registriere Dich bei ArchExperts und veröffentliche ein Projekt.",
"concepts_and_post_your_project" => "Concepts und veröffentliche ein Projekt", 
"requirements" => "Anforderungen", 

'desc_select_an_expert'              	=> '<br/>Du bekommst Angebote von vielen Experten.<br/> Vergleiche sie, suche den passenden Experten für Dich aus und vergebe Dein Projekt.',

"select_an_expert" => "Wähle einen Experten", 
"we_will_select_the_best_suited" => "Wir werden für Dein Projekt die am besten", 
"experts_to_work_on_your_project" => "passenden Experten auswählen.", 
"pick_one_or_more" => "Wähle einen oder mehr von den vorgeschlagenen", 
"experts" => "Experten aus.", 

"work_on_the_project" => "Arbeite im Projekt mit", 
'desc_work_on_project' => '<br/> Stimme Dich zu wichtigen Details wie Projektumfang, Dauer und Kosten mit dem Experten ab.<br/> Bleibe für Rückfragen offen, nm ein bestmögliches Ergebnis zu erhalten.',

"agree_on_the_project" => "Stimme Dich bezüglich des Projektumfangs, Projektdauer,", 
"cost_and_get_started" => "Kosten und starte Dein Projekt.", 


"pay_only_when_satisfied" => "Bezahle nur dann, wenn Du zufrieden bist", 

'desc_pay_only_satisfied' => 'Die Projektgebühr muss am Anfang des Projekts bezahlt werden.Dein Experte wird jedoch erst bezahlt,<br/>nachdem Du bestätigst hast,dass das Projekt fertiggestellt worden ist.',

"pay_the_project_fee" => "Die Projektgebühr muss am Anfang des Projekts bezahlt werden. Dein Experte wird jedoch erst bezahlt,", 
"once_you_notify_project" => "nachdem Du bestätigst hast, dass das Projekt", 
"completion" => "fertiggestellt worden ist.", 


"registered_freelancers" => "Registrierte Experten", 
"worth_of_work_done_annually" => "werk gedaan jaarlijks", 
"jobs_posted_annually" => "Veröffentlichte Projekte im Jahr",
"registered_clients" => "Registrierte Auftraggeber", 
"we_are_currently_down_for_maintenance" => "Wir führen gerade Wartungsarbeiten durhc und sind bald wieder online.", 
"laravel" => "What does it mean ???", 
"something_wrong_here" => "Hier ist etwas schief gelaufen.", 
"we_can_t_find_the_page" => "Wir können die gesuchte Seite nicht finden. Navigiere einfach zu unserer Website zurück.", 
"be_right_back" => "Bin gleich wieder da.", 
"edit" => "Bearbeiten", 
"employer" => "Auftraggeber", 
"admin_comments" => "Adminitstrator-Kommentare", 
"select_category" => "Wähle Kategorie", 
"download_attachment" => "Anhang herunterladen", 
"project_rejection_reason" => "Grund für Projektablehnung", 


"if_you_choose_myself" => "Wenn Du die Option: Betreut durch „mich\" wählst, entscheidest Du Dich, Dein Projekt selbst zu betreuen. Das Projekt wird kostenlos veröffentlicht", 


"if_you_choose_project_manager" => "Wenn Du \"Projekt Manager\" wählst, wird Dein veröffentlichtes Projekt zuerst  von einem unserer Mitarbeiter überprüft und einem unserer Projekt Manager zugewiesen. Der Projekt Manager unterstützt Dich bei der Rekrutierung eines passenden Experten. Nachdem Du eine Entscheidung getroffen hast und Dein Projekt an einen Experten vergeben hast, wird Dich unser Projekt Manager auch während der Projektumsetzung unterstützen und bei Bedarf anleiten.<br/><br/>Für die beiden Services wird jeweils eine zusätzliche Service-Gebühr erhoben:<br/>- Rekrutierung: einmalig :initial_cost EUR/USD (abhängig von der gewählten Währung), sofort fällig<br/>- Projektunterstützung durch Projekt Manager: 10% (vom gesetzten Meilenstein), fällt während des Projektes an.", 


"as_initial_cost_for_project" => "Die Gebühr wird immer vorher ausgewiesen.", 
"project_attachments" => "Projektanhang", 

'reject_project_bid_title' => 'Bitte teile uns mit, warum Du das Projekt ablehnst ?',
'text_download_all_files'                 => 'Alle Dateien herunterladen (ZIP)',
'text_you_have_already_sent_an_entry'	  => 'Sie haben bereits einen Eintrag gesendet.',
'text_award_job' =>	'Auszeichnung Job',
"text_upload_more_entries"				  => 'Laden Sie weitere Einträge hoch',
];