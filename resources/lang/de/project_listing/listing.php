<?php 
 return [ 
 "placeholder_search_jobs" => "Jobs suchen",
 "text_search" => "Suchen",
 "text_all_project" => "Alle Projekte",
 "text_category" => "Kategorie",
 "text_were_found_based_on_your_criteria" => "Projekte wurden anhand Deiner Suchkriterien gefunden",
 "text_sorry_no_records_found" => "Sorry, keine Datensätze gefunden",
 "text_all" => "Alle",
 "text_search_result" => "Suchergebnis",
 "text_bid" => "Gebot",
 "text_bided" => "Geboten",

 'text_project_duration' => 'Projektdauer',
 'text_days' => 'Tage',
 'text_budget' => 'Budget',
 'text_hourly_rate' => 'Stundensatz',
 'text_bids' => 'Gebote',	
 'text_apply' => 'Ein Gebot abgeben',	
 'text_applied' => 'Gebot abgegeben',

 'text_sort_by' => 'Ordne nach:',
 'text_select' => '---Wählen---',

 'text_price_high_to_low'=>'Preis absteigend',
 'text_price_low_to_high'=>'Preis aufsteigend',
 'text_bid_high_to_low'=>'Gebotsanzahl absteigend',
 'text_bid_low_to_high'=>'Gebotsanzahl aufsteigend',
 'text_duration_high_to_low'=>'Dauer absteigend',
 'text_duration_low_to_high'=>'Dauer aufsteigend',

 'text_project_posted_on' =>'Gesendet',
 'text_posted_by_hired_process' =>'Direkt von mir eingestellt',
 'text_project_rejected' =>'Abgelehnt',

 'text_filter'                            => 'Filter',
 'text_keywords'                          => 'Schlüsselwörter',
 'text_fixed_rate'                        => 'Fester Zinssatz',
 'text_project_due_on'                    => 'Due date',
 'text_Provide_general_info_about_your_project_file' => 'Stellen Sie dem Client einige Informationen zu der Arbeit bereit, die Sie auf den Server hochladen möchten.',
 'text_select_atleast_one_image_to_upload_Only_jpg_png_jpeg_images_allowed_with_max_size_1mb_You_can_upload_upto_5_images_at_a_time' => 'Wählen Sie mindestens eine Datei zum Hochladen aus. Max. Größe 5 MB. Sie können bis zu 5 Bilder gleichzeitig hochladen.',
 'text_drop_your_images_here_to_upload'=> 'Ziehe die Dateien hier hin, um sie hochzuladen.',

];