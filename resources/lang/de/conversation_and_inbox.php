<?php 
 return [ 
 "text_chat" => "Chat",
 "text_no_conversations" => "Keine Unterhaltungen",
 "text_refresh" => "Aktualisieren",
 "text_sorry_no_messages_found" => "Sorry, keine Nachrichten gefunden",
 "text_messages" => "Nachrichten",/*verfügbar*/
 "text_type_your_message_here" => "Nachricht hier eintippen",

  "text_no_message"  	=> "Keine Nachrichten",
  "text_available"   	=> "Verfügbar",
  "text_month"      	=> "Monat",
  "text_second"     	=> "Sekunde",
  "text_minute"     	=> "Minute",
  "text_hour"       	=> "Stunde",
  "text_day"    		=> "Tag",
  "text_week"   		=> "Woche",
  "text_year"   		=> "Jahr",
];