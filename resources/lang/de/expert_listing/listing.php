<?php 
 return [ 
 "text_search_country" => "Wähle Land",
 "placeholder_search_experts" => "Suche nach Experten",
 "text_search" => "SUCHEN",
 "text_projects" => "PROJEKTE",
 "text_view_portfolio" => "Portfolio",
 "text_all_expert" => "Alle Experten",
 "text_were_found_based_on_your_criteria" => "wurden anhand Deiner Suchkriterien gefunden",
 "text_sorry_no_records_found" => "Sorry, keine Datensätze gefunden !!",
 "text_all" => "Alle Projekte",
 "text_search_result" => "Ergebnisse suchen",
 'text_recently_active'             => 'Onlangs actief',
 'text_ago'             => 'geleden',

 'text_select_rating'                     => 'Bewertung auswählen',
 'text_advanced'                          => 'Erweitert',
 'text_hourly_rate'                       => 'Stundensatz',
 'text_reset_filter'                      => 'Filter zurücksetzen',
 'text_showing'                           => 'Anzeigen',
 'text_show'                              => 'Show',
 'text_results'                           => 'Ergebnisse',
 'text_projects_not_matching_experts_skills_or_categories_please_try_another_one'  => 'Wir haben keine Ihrer veröffentlichten Projekte gefunden, die einem Profil dieses Experten entsprechen.',
];