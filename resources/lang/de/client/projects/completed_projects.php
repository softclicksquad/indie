<?php 
 return [ 
 "text_edit" => "Bearbeiten",
 "text_project_duration" => "Projektdauer",
 "text_est_time" => "Geschätzte Dauer",
 "text_days" => "Tage",
 "text_more" => "mehr",
 "text_skills" => "Fachkenntnisse",
 "text_view" => "Anzeigen",
 "text_milestones" => "Meilensteine",
 "text_completed_project_title" => "Fertiggestellte Projekte",
 "text_no_record_found" => "Noch keine Einträge.",

 'text_your_rating' =>'Deine Bewertung',
 'text_client_rating' =>'Kunden Bewertung',
 'text_expert_rating' =>'Bewertung vom Experten',
];