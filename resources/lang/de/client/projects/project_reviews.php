<?php 
 return [ 
 "text_title"             => "Bewertung hinzufügen",
 "text_review_rate"       => "Bewertung",
 "text_add"               => "Hinzufügen",
 "text_review"            => "Rezensionen",
 "text_date"              => "Datum",
 "text_comment"           => "Kommentar",
 "text_category"          => "Kategorie",
 "text_company_name"      => "Firmenname",
 "text_email_id"          => "E-Mail ID",
 "placeholder_comment"    => "Bitte hinterlassen Sie einen Kommentar zu Ihren Erfahrungen mit diesem Auftragnehmer",
 "text_rating_type_one"   => "Professionelles Auftreten",
 "text_rating_type_two"   => "Qualität der Kommunikation",
 "text_rating_type_three" => "Fachkompetenz",
 "text_rating_type_four"  => "Arbeitsqualität",
 "text_rating_type_five"  => "Würde den Experten wieder beauftragen",
];