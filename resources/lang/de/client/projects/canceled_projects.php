<?php 
return [
		'text_title' => 'Stornierte Projekte',
		'text_heading' => 'Stornierte Projekte',
        'text_edit' => 'Bearbeiten',
		'text_est_time' => 'Projektdauer',
		'text_days' => 'Tage',
		'text_more' => 'Mehr',
		'text_skills' => 'Fähigkeiten',
		'text_view' => 'Anzeigen',
		'text_canceled_project_title'=>'Stornierte Projekte',
		'text_update'=>'Aktualisieren',
		'text_milestones'=>'Meilensteine',
		'text_no_record_found'=>'Noch keine Einträge.',
		'text_post_again'=>'Post opnieuw',
		'text_delete'                 => 'Delete',
	   ];

?>