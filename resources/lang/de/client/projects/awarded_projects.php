<?php 
 return [ 
 "text_title" => "Vergebene Projekte",
 "text_heading" => "Vergebene Projekte",
 "text_edit" => "Bearbeiten",
 "text_est_time" => "Geschätzte Dauer",
 "text_days" => "Tage",
 "text_more" => "mehr",
 "text_skills" => "Fachkenntnisse",
 "text_view" => "Anzeigen",
 "text_awarded_title" => "Vergebene Projekte",
 "text_canceled_project_title" => "Stornierte Projekte",
 "text_applied_project_title" => "Gebote",
 "text_update" => "Aktualisieren",
 "text_milestones" => "Meilensteine",
 "text_no_record_found" => "Noch keine Einträge.",
 'text_accepted_on' => 'Angenommen am:',
 'text_create_milestone' => 'Meilenstein erstellen',
 'text_message_chat' => 'Nachricht',
];