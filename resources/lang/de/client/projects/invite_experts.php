<?php 
return [
	'text_invite_expert'                                                                  => 'Einladen',
	'text_experts_showed_in_a_select_box_based_on_selected_project_br_category_skills'    => 'Experten zeigten in einem Auswahlfeld basierend auf dem ausgewählten Projekt Kategorie & Fähigkeiten',
	'text_select'                                                                         => 'Wählen',
	'text_invite'                                                                         => 'Einladen',
	'text_email_address'                                                                  => 'E-Mail-Addresse',
	'text_add_a_message'                                                                  => 'Füge eine Nachricht hinzu',
	'text_you_have_left'                                                                  => 'Du bist gegangen', 
	'text_characters_for_description'                                                     => 'Zeichen zur Beschreibung.', 
	'text_characters_for_additional_info'                                                 => 'Zeichen für zusätzliche Informationen.', 
	'text_check_bids'=>'Gebote überprüfen',
	'text_job_managed_by_manager' => 'Verwaltet vom Manager',
	'text_job_managed_by_recruiter' => 'Job managed by recruiter',
	'text_bid_already_placed' => 'Bid already placed',
	];
?>