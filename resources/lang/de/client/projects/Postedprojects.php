<?php 
 return [ 
 "text_title" => "Veröffentlichte Projekte",
 "text_heading" => "Veröffentlichte Projekte",
 "text_edit" => "Bearbeiten",
 "text_est_time" => "Geschätzte Dauer",
 "text_days" => "Tage",
 "text_more" => "mehr",
 "text_skills" => "Fachkenntnisse",
 "text_view" => "Anzeigen",
 "text_canceled_project_title" => "Stornierte Projekte",
 "text_update" => "Aktualisieren",
 "text_milestones" => "Meilensteine",
 "text_no_record_found" => "Noch keine Einträge.",
];