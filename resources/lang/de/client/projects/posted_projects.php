<?php 
 return [ 
 "text_title" => "Veröffentlichte Projekte",
 "text_heading" => "Veröffentlichte Projekte",
 "text_edit" => "Bearbeiten",
 "text_project_duration" => "Projektdauer",
 "text_est_time" => "Geschätzte Dauer",
 "text_days" => "Tage",
 "text_more" => "mehr",
 "text_skills" => "Fachkenntnisse",
 "text_no_record_found" => "Noch keine Einträge.",
];