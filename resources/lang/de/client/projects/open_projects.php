<?php 
 return [ 
 "text_edit" => "Bearbeiten",
 "text_est_time" => "Geschätzte Dauer",
 "text_days" => "Tage",
 "text_more" => "mehr",
 "text_skills" => "Fachkenntnisse",
 "text_view" => "Anzeigen",
 "text_ongoing_project_title" => "Laufende Projekte",
 "text_completed_project_title" => "Abgeschlossene Projekte",
 "text_canceled_project_title" => "Stornierte Projekte",
 "text_open_project_title" => "Offene Projekte",
 "text_no_record_found" => "Noch keine Einträge.",
];