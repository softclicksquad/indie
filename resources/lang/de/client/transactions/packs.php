<?php 
 return [ 
 "text_title" => "Transaktionen",
 "text_heading" => "Transaktionen",
 "text_heading_details" => "Transaktionsdetails",
 "text_sorry_no_records_found" => "Sorry, keine Datensätze gefunden !!",
 "text_moth" => "Monat",
 "text_mo" => "/mtl.",
 "text_invoice_id" => "Rechnungs-ID",
 "text_transaction_type" => "Transaktionstyp",
 'text_wallet_transaction_id'  => 'Brieftaschen-Transaktions-ID',
 "text_payment_amount" => "Betrag",
 "text_payment_method" => "Methode",
 "text_Payment_status" => "Status",
 "text_Payment_date" => "Datum",
 "text_email" => "Kommunikation via E-Mail",
 "text_chat" => "Kommunikation via Chat",
 "text_pay_now" => "Jetzt loslegen",
 "text_activated" => "Aktiviert",

 'text_payment_action'=>'Aktion',
 'text_action_view'=>'Anzeigen',

  'text_fail'=>'Fehlgeschlagen',
  'text_paid'=>'Bezahlt',

  'text_pay_subscription' =>'Subscription',
  'text_pay_milestone' =>'Meilenstein',
  'text_pay_release_milestone' =>'Release Milestones',
  'text_pay_project_payment' =>'Servicegebühr',
];