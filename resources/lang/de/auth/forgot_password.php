<?php 
 return [ 
 "text_heading" => "Passwort vergessen",
 "text_email" => "E-Mail Adresse",
 "text_backto_login" => "Zurück zum Einloggen",
 "entry_email" => "Trage Deine E-Mail-Adresse ein",
];