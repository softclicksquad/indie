<?php 
 return [ 
 "text_title" => "Passwort zurücksetzen",
 "text_heading" => "Passwort zurücksetzen",
 "text_password" => "Passwort ",
 "text_confirm_password" => "Passwort bestätigen",
 "text_reset" => "Zurücksetzen",
 "entry_password" => "Trage Dein Passwort ein",
 "entry_confirm_password" => "Bestätige Dein Passwort",
];