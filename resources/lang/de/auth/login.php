<?php 
 return [ 
 "text_login" => "Einloggen",
 "text_email" => "E-mailadres",
 "text_password" => "Passwort",
 "text_remember_me" => "Anmeldedaten merken",
 "text_forgot_password" => "Passwort vergessen?",
 "text_dont_account" => "Du hast noch keinen Account?",
 "text_register_here" => "Registriere Dich hier",
 "entry_email" => "Vul email adres in",
 "entry_password" => "Trage Dein Passwort ein",
];