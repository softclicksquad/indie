<?php 
 return [ 
 "password" => "Das passwort muss mindestens 6 Zeichen lang sein und übereinstimmen.",
 "reset" => "Dein Passwort wurde zurückgesetzt.",
 "sent" => "Wir haben Dir einen Link für die Zurücksetzung Deines Passworts geschickt.",
 "token" => "Dieses Passwort-Rücksetzungs-Token ist ungültig.",
 "user" => "Wir konnten keinen Benutzer mit dieser E-Mail Adresse finden.",
];