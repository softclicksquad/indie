<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Faq Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'text_faq'             => 'FAQ',
    'text_faq_for_client'  => 'Veelgestelde vragen voor klanten',
    'text_faq_for_expert'  => 'Veelgestelde vragen voor ArchExperts',
    'text_search'          => 'Zoeken',
];
