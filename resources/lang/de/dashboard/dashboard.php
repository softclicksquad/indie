<?php 
 return [ 
 "ongoing_projects" => "Laufende Projekte",
 "completed_projects" => "Fertiggestellte Projekte",
 "canceled_projects" => "Stornierte Projekte",
 "awarded_projects" => "Vergebene Projekte",
 "applied_projects" => "Abgegebene Gebote",
 "review_ratings" => "Bewertung",
 "new_messages" => "Neue Nachrichten",
 "open_projects" => "Offene Projekte",
 "posted_projects" => "Veröffentlichte Projekte",
  'milestone_release' => 'Anfragen für MeilensteinFreigabe',
 "welcome_to_dashboard" => "Willkommen im Cockpit",
 "no_reviews_yet" => "Noch keine Rezensionen",
 "of_5" => "von 5.0",
 'text_my_contests'=>'Meine Wettbewerbe',
];