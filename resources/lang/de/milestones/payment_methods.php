<?php 
 return [ 
 "text_card_details" => "Kartendetails",
 "text_card_number" => "Kartennummer",
 "text_card_expiration_month_year" => "Kartenablaufdatum",
 "text_card_cv_code" => "Kartenprüfnummer (CVC Code)",
 //"text_processed" => "Bezahlen",
 "text_processed" => "Nu betalen",
 "entry_card_number" => "Bitte die Kartennummer eingeben",
 "entry_mm" => "MM",
 "entry_yyyy" => "YYYY",
 "entry_cv_code" => "Bitte den CV Code eingeben",
 'text_project_manager_commision' => 'Servicegebühr für Projekt Manager',

 'text_project_manager_payment_note' => 'Cost for project manager',
 'text_recruiter_payment_note' => 'Cost for recruitment done by your recruiter',
];