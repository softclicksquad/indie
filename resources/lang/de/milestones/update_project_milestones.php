<?php 
 return [ 
 "text_update" => "Aktualisieren",
 "text_title" => "Titel",
 "text_milestones" => "Meilensteine",
 "text_amount" => "Menge",
 "text_description" => "Beschreibung",
 "text_back" => "Zurück",
];