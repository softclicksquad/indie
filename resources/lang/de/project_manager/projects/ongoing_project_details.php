<?php 
return [
			'expert_details'=>'Expert Details',
			'name'=>'Name :',
			'address'=>'Address :',
			'zipcode'=>'Zipcode :',
			'phone_number'=>'Phone Number :',
			'company_name'=>'Company Name :',
			'vat_number'=>'Vat Number :',
			'spoken_languages'=>'Spoken Languages :',

			'submit_proposal'=>'Submit Proposal',
			'total_cost'=>'Total Cost',
			'estimated_duration'=>'Estimated Duration',
			'enter_your_proposal_details'=>'Enter your Proposal details',
			'download_proposal'=>'Download Proposal',
	   ];

?>