<?php 
 return [ 
 "text_est_time" => "Geschätzte Dauer",
 "text_days" => "Tage",
 "text_more" => "mehr",
 "text_skills" => "Fachkenntnisse",
 "text_view" => "Anzeigen",
 "text_milestones" => "Meilensteine",
 "text_ongoing_project_title" => "Laufende Projekte",
 "text_no_record_found" => "Noch keine Einträge.",
];