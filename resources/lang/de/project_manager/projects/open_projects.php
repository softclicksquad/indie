<?php 
 return [ 
 "text_est_time" => "Geschätzte Dauer",
 "text_days" => "Tage",
 "text_skills" => "Fachkenntnisse",
 "text_view" => "Anzeigen",
 "text_open_project_title" => "Offene Projekte",
 "text_no_record_found" => "Noch keine Einträge.",
];