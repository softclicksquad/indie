<?php 
 return [ 
 "text_est_time" => "Geschätzte Dauer",
 "text_days" => "Tage",
 "text_more" => "mehr",
 "text_skills" => "Fachkenntnisse",
 "text_canceled_project_title" => "Stornierte Projekte",
 "text_no_record_found" => "Noch keine Einträge.",
];