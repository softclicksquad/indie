<?php 
 return [ 
 "company_name" => "Firmenname (optional)",
 "email_id" => "Email-ID",
 "category" => "Kategorie",
 "days" => "Tage",
 "more" => "mehr",
 "suggest" => "Vorschlagen",
 "unsuggest" => "Nicht mehr vorschlagen",
 "message" => "Nachricht",
 "duration" => "Dauer",
 "rating" => "Bewertung",
 "review" => "Rezensionen",
 "completion_rate" => "Fertigstellungsquote",
 "reputation" => "Reputation",
 "view" => "Anzeigen",
 "suggested" => "Vorgeschlagen",
 "username" => "Benutzername",
 "skills" => "Fachkenntnisse",
 "project_completed" => "Projekt abgeschlossen",
 "currently_no_bids_are_available" => "Bis dato keine Gebote eingegangen",
 "days" => "Tage"
];