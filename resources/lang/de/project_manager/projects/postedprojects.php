<?php 
return [
		'text_title' => 'Posted Jobs',
		'text_heading' => 'Posted Jobs',
        'text_edit' => 'Edit',
		'text_est_time' => 'Job Duration',
		'text_days' => 'Days',
		'text_more' => 'more',
		'text_skills' => 'Skills',
		'text_ongoing_project_title'=>'Ongoing Jobs',
		'text_completed_project_title'=>'Completed Jobs',
	    'text_canceled_project_title'=>'Canceled Jobs',

		'text_open_project_title'=>'Open Jobs',
		'text_no_record_found'=>'Noch keine Einträge.',


	   ];

?>