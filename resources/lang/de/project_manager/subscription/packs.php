<?php 
 return [ 
 "text_title" => "Abopaket",
 "text_heading" => "Abopaket",
 "text_moth" => "Monat",
 "text_mo" => "/mtl.",
 "text_billed_monthly" => "monatliche Abrechnung",
 "text_bids" => "Gebote",
 "text_topup_bid" => "pro extra Gebot",
 "text_categories" => "Kategorien",
 "text_skills" => "Fachkenntnisse",
 "text_favorites_projects" => "Lesezeichen",
 "text_email" => "Kommunkation per E-Mail",
 "text_chat" => "Kommunkation per Chat",
 "text_pay_now" => "Jetzt bezahlen",
];