<?php 
 return [ 
 "text_title" => "Passwort ändern",
 "text_heading" => "Bitte ändere Dein Passwort.",
 "text_current_password" => "Aktuelles Passwort",
 "text_new_password" => "Neues Passwort",
 "text_confirm_password" => "Bestätige Passwort",
 "text_update" => "Aktualisieren",
 "entry_current_password" => "Gebe das aktuelle Passwort ein",
 "entry_new_password" => "Geben neues Passwort ein",
 "entry_confirm_password" => "Gebe das bestätigte Passwort ein.",
];