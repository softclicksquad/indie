<?php 
 return 
 [ 
	"text_title"                                      => "Home",
	"text_heading"                                    => "Home",
	"text_browse"                                     => "Jobs",                 // Durchsuchen to Jobs
	"text_expert"                                     => "Experten",
	"text_how_it_works"                               => "Wie es funktioniert",
	"text_sign_up"                                    => "Anmelden",
	"text_login"                                      => "Einloggen",
	'text_delete_account'                             => 'Account verwijderen',
	"text_post_job"                                   => "Post Job",
	"text_post_a_job"                                 => "Veröffentlichen Sie einen Job",
	"text_my_profile"                                 => "Mein Profil",
	"text_my_wallet"                                  => "Mein Geldbeutel",
	"text_logout"                                     => "Ausloggen",
	'text_dashboard'                                  => 'Cockpit',
	'text_change_password'                            => 'Passwort ändern',
	'text_messages'                                   => 'Plaudern',
 	'text_you_have'                                   => 'Du hast',
	'text_new_messages'                               => 'neue Nachrichten',
	'text_no_notifications'                           => 'Keine Benachrichtigungen',
	'text_browse_categories'                          => 'Banen op categorieën',              // Jobs nach Kategorien to  Banencategorieën
	'text_browse_categories_for_contest'              => 'Wettbewerbe nach Kategorien',           
	'text_browse_expertby_categories'                 => 'Experten nach Kategorien',       // Experts Categories  to Jobs Categories
	'text_browse_jobs'                                => 'Alle banen',
	'text_browse_contest'                             => 'Alle banen',
	'text_browse_skills'                              => 'Banen door vaardigheden',            // Jobs nach Fachkenntnissen to Banen vaardigheden
	'text_no_categories'                              => 'Keine Kategorien verfügbar',
	'text_no_skills'                                  => 'Keine Fähigkeiten vorhanden',
	'text_browse_experts'                             => 'Alle Experten',
	'text_browse_experts_skills'                      => 'Experten nach Fähigkeiten',
	'text_name'                                       => 'Naam',
	'text_last_login'                                 => 'Laatste aanmelding',
	'text_active'                                     => 'Actief',
	'text_jobs_matching_my_skills_and_categories'     => 'Jobs, die meinem Profil entsprechen',
	'text_contest_matching_my_skills_and_categories'  => 'Wettbewerbe, die meinem Profil entsprechen',
];