<?php

return [
	'text_profession'=>'Beruf',
	'text_categories'=>'Kategorien',
	'subject'=>'Gegenstand',
	'message'=>'Botschaft',
	'attachment'=>'Anhang',
	'browse'=>'Durchsuche',
	'submit'=>'einreichen',
	'support_ticket'=>'Unterstüzungsticket',
	'closed'=>'Geschlossen',
];

?>