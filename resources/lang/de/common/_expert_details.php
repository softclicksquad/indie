<?php 
 return [ 
 "text_expert_details" => "Experteninfos",
 "text_first_name" => "Vorname",
 "text_last_name" => "Nachname",
 "text_address" => "Strasse und Hausnummer",
 "text_phone_number" => "Telefonnummer",
 "text_spoken_languages" => "Sprachkenntnisse",
 "text_rating" => "Bewertung",
 "text_projects" => "Projekte",
 "text_completion_rate" => "Fertigstellungsquote",
 "text_review" => "Rezensionen",
 "text_earned" => "Einnahmen",

 "text_reputation" => "Reputation",
 'text_about_me' => 'Über mich',
 'text_username' => 'Benutzername',
 'text_location' => 'Standort',
 'text_local_timezone' => 'Lokale Zeitzone',
 'text_local_time' => 'Lokale tijd',
];