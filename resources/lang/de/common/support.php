<?php 
return [
 	"your_tickets" => "Ihre Tickets",
 	"add_support_ticket" => "Support Ticket hinzufügen",
 	"department" => "Abteilung",
 	"select_department_category" => "Abteilungskategorie auswählen",
 	"has_replied_to_support_ticket" => " hat auf das Support-Ticket geantwortet ",
 	"has_created_support_ticket" => " hat ein Support Ticket erstellt ",
 	"has_reopened_support_ticket" => " hat das Support-Ticket wieder geöffnet ",
 	"priority" => "Priorität",
 	"select_priority" => "Priorität auswählen",
 	"urgent" => "Dringend",
 	"normal" => "Normal",
];