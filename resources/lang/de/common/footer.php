<?php 
 return [ 
 "thank_you_for_signup_newsletter" => "Die Newsletteranmeldung war erfolgreich. Du erhälst gleich eine E-Mail, mit der Bitte, Deine Anmeldung zu bestätigen.",
 
 "problem_occured_in_signup_newsletter" => "Es is ein Problem bei der Anmeldung zum Newsletter aufgetreten.",
 "text_company_info" => "Information zur Firma",
 "text_browse_top_skills" => "Durchsuche TOP Fachkenntnisse",
 "text_connect_with_us" => "Verbinde Dich mit uns",
 "text_contact_us" => "Kontaktiere uns",
 "text_newsletter" => "Abboniere unsere Newsletter",
 "text_your_email" => "Deine E-Mail Adresse",
 "text_follow_us" => "Folge uns",
 "text_copyright" => " <span> Copyright </span>  &copy; ".date('Y')." by <span>, ". config('app.project.name') ."</span>, Alle Rechte vorbehalten.",
 'text_this_field_is_required'          => "Dieses Feld wird benötigt.",
];