                
<?php $__env->startSection('main_content'); ?>
<script src="<?php echo e(url('/public')); ?>/front/js/jquery.flexisel.js"></script>
<link href="<?php echo e(url('/public')); ?>/front/css/style.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo e(url('/public')); ?>/assets/jquery-ui/jquery-ui.min.css">
<script src="<?php echo e(url('/public')); ?>/assets/jquery-ui/jquery-ui.min.js"></script>
     
<div class="home-page">
<div class="home-banner">
	<!--banner slider start-->
	 <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- banner -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img src="<?php echo e(url('/public')); ?>/front/images/home-banner.jpg" alt="" />
				</div>
				<!-- <div class="item">
					<img src="<?php echo e(url('/public')); ?>/front/images/home-banner.jpg" alt="" />
				</div>
				<div class="item">
					<img src="<?php echo e(url('/public')); ?>/front/images/home-banner.jpg" alt="Flower" />
				</div> -->
			</div>
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<!-- <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li> -->
			</ol>
		<!-- end  banner -->	
		<h2><?php echo trans('common/home.text_where_will'); ?></h2>
	</div>
	<!--banner slider end-->
	<div class="banner-content">
		<div class="container">
			<h2><?php echo trans('common/home.text_where_will'); ?></h2>
			<h4>- <?php echo trans('common/home.text_quickly_find'); ?> -</h4>
			<form action="<?php echo e(url('/')); ?>/experts" id="frm_search" method="any">   
			
				<div class="search-bar">
					<input type="text" name="search" id="search-box" placeholder="Search architects, interior designers, visualizers and engineers" onkeyup="return chk_validation(this);"/>
					<input type="hidden" name="search_profession_id">
					<button class="search-btn bg-img show_loader" value="" type="submit">&nbsp;</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="offers-section">
	<div class="container">
		<?php /* <h2><?php echo trans('common/home.text_work_with'); ?></h2>
		<h5><?php echo trans('common/home.text_work_with2'); ?></h5> */ ?>
		<h2><?php echo trans('common/home.text_work_category'); ?></h2>
        <?php if(isset($arr_categories) && sizeof($arr_categories)>0): ?>
            <?php  $catcnt = 1;  ?>
            <?php foreach($arr_categories as $category): ?>
                <?php if($catcnt <= 4){?>
                <div class="offer-block-warpper">
					<a class="offer-block" href="<?php echo e(url('/experts/search/all/result?country=&expert_search_term=&category='.base64_encode($category['id']))); ?>">
					    <!--<a class="offer-block" href="<?php echo e(url('/projects/'.base64_encode($category['id']))); ?>">-->
						<div class="">
				          <?php if(isset($category['category_image']) && file_exists('public/uploads/admin/categories_images/'.$category['category_image'])){ ?>
	                        <img src="<?php echo e(url('/public/'.config('app.project.img_path.category_image').$category['category_image'])); ?>">
	                      <?php } else { ?> 
	                        <img src="<?php echo e(url('/public/front/images/not-found.png')); ?>" style="height:70px;width:70px">
	                      <?php } ?>
	                    </div>
						<span class="brd-box">&nbsp;</span>
						<?php if(isset($category['category_title']) && strlen(($category['category_title'])) >20 ): ?> <?php  $cat_dots='...';  ?> <?php else: ?> <?php  $cat_dots = '';  ?> <?php endif; ?>
                        <p title="<?php echo e(isset($category['category_title'])?$category['category_title']:''); ?>"><?php echo e(isset($category['category_title'])?substr($category['category_title'],0,20).$cat_dots:''); ?></p>
					</a>
				</div>
				<?php } ?>
			<?php  $catcnt++;  ?>
            <?php endforeach; ?>
        <?php endif; ?>
        <?php if(isset($arr_categories) && sizeof($arr_categories)>9): ?>
		<div class="offer-block-warpper">
			<a class="offer-block" href="<?php echo e(url('/categories')); ?>">
				<div class="bg-img offer-icon8"></div>
				<span class="brd-box">&nbsp;</span>
				<p><?php echo trans('common/home.text_more_categories'); ?></p>
			</a>
		</div>
		<?php endif; ?>
	</div>
</div>
<div class="how-it-work-section">
	<div class="container">
		<h2><b><?php echo trans('common/home.text_how_it_works'); ?></h2>
		<div class="row">
			<div class="col-sm-6 col-md-3 col-lg-3">
				<div class="how-it-block">
					<div class="how-it-circle bg-img how-it-icon1">&nbsp;</div>
					<h4><?php echo trans('common/home.text_posta_job'); ?></h4>
					<p><?php echo trans('common/home.text_posta_job_desc'); ?></p>
					<div class="arrow-block bg-img"></div>
				</div>
			</div>
			<div class="col-sm-6 col-md-3 col-lg-3">
				<div class="how-it-block">
					<div class="how-it-circle bg-img how-it-icon2">&nbsp;</div>
					<h4><?php echo trans('common/home.text_find_your_expert_and_award_project'); ?></h4>
					<p><?php echo trans('common/home.text_find_your_expert_and_award_project_des'); ?></p>
					<div class="arrow-block bg-img"></div>
				</div>
			</div>
			<div class="col-sm-6 col-md-3 col-lg-3">
				<div class="how-it-block">
					<div class="how-it-circle bg-img how-it-icon3">&nbsp;</div>
					<h4><?php echo trans('common/home.text_create_milestone_and_make_deposit'); ?></h4>
					<p><?php echo trans('common/home.text_create_milestone_and_make_deposit_desc'); ?></p>
					<div class="arrow-block bg-img"></div>
				</div>
			</div>
			<div class="col-sm-6 col-md-3 col-lg-3">
				<div class="how-it-block">
					<div class="how-it-circle bg-img how-it-icon4">&nbsp;</div>
					<h4><?php echo trans('common/home.text_collaboration_approve_pay'); ?></h4>
					<p><?php echo trans('common/home.text_collaboration_approve_pay_des'); ?></p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="professional-expert-section">
	<div class="container">
		<div class="row">
			<div class="col-lg-4"></div>
			<div class="col-lg-8">
				<div class="expert-content">
				<h2><?php echo trans('common/home.text_why_hire_archexperts'); ?></h2>
					<div class="row">
						<div class="col-sm-12 col-md-3 col-lg-3">
							<div class="expert-block text-center">
								<div class="bg-img circle-icon"></div>
								<p><?php echo trans('common/home.text_why_hire_archexperts_desc1'); ?></p>
							</div>
							<div class="expert-block text-center">
								<div class="bg-img circle-icon"></div>
								<p><?php echo trans('common/home.text_why_hire_archexperts_desc2'); ?></p>
							</div>
						</div>
						<div class="col-sm-12 col-md-6 col-lg-6">
							<div class="oval-block text-center">
								<img src="<?php echo e(url('/public')); ?>/front/images/oval-shape.png" class="img-responsive" alt=""/>
								<p><sup><i class="fa fa-quote-left"></i></sup> <?php echo trans('common/home.text_why_hire_archexperts_desc3'); ?> <sup><i class="fa fa-quote-right"></i></sup><span class="brd-box">&nbsp;</span></p>
							</div>
						</div>
						<div class="col-sm-12 col-md-3 col-lg-3">
							<div class="expert-block text-center">
								<div class="bg-img circle-icon"></div>
								<p><?php echo trans('common/home.text_why_hire_archexperts_desc4'); ?></p>
							</div>
							<div class="expert-block text-center">
								<div class="bg-img circle-icon"></div>
								<p><?php echo trans('common/home.text_why_hire_archexperts_desc5'); ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="feature-block-start">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-6 col-lg-6">
				<div class="left-block">
					<h2><?php echo trans('common/home.text_enjoy_your_time_with_us'); ?></h2>
					<p><?php echo trans('common/home.text_enjoy_your_time_with_us_des'); ?></p>
				</div>
			</div>
			<div class="col-sm-12 col-md-6 col-lg-6">
				<div class="right-block">
					<ul>
						<li>
							<div class="left-icon3 bg-img"></div>
							<div class="feature-content">
								<span><?php echo trans('common/home.text_payment_protection'); ?></span>
								<p><?php echo trans('common/home.text_payment_protection_desc'); ?></p>
							</div>
						</li>
						<li>
							<div class="left-icon2 bg-img"></div>
							<div class="feature-content">
								<span><?php echo trans('common/home.text_dispute_resolution'); ?></span>
								<p><?php echo trans('common/home.text_dispute_resolution_desc'); ?></p>
							</div>
						</li>
						<?php /* <li>
							
							<div class="left-icon1 bg-img"></div>
							<div class="feature-content">
								<span><?php echo trans('common/home.text_work_diary'); ?></span>
								<p><?php echo trans('common/home.text_work_diary_desc'); ?></p>
							</div>
						</li> */ ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
// Function for special characters not accepts.
function chk_validation(ref){
   var yourInput = $(ref).val();
    re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
    var isSplChar = re.test(yourInput);
    if(isSplChar){
      var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
      $(ref).val(no_spl_char);
    }
}
$(document).ready( function () {
   $("input[name='search']").autocomplete({
      minLength:0,
      source:"<?php echo e(url('/autocomplete')); ?>",
      select:function(event,ui)
      {
        $("input[name='search']").val(ui.item.label);
        $("input[name='search_profession_id']").val(ui.item.id);
      },
      response: function (event, ui)
      {
      }
   });
   $('#search-box').keyup(function()
   {
      var search_box_val = $(this).val();
      if(search_box_val != ""){
      	$('#frm_search').attr('action','<?php echo e(url('/')); ?>/experts/search/all/result');
      } else {
      	$('#frm_search').attr('action','<?php echo e(url('/')); ?>/experts');
      }
   });
   $('#search-box').blur(function()
   {
      var search_box_val = $(this).val();
      if(search_box_val != ""){
      	$('#frm_search').attr('action','<?php echo e(url('/')); ?>/experts/search/all/result');
      } else {
      	$('#frm_search').attr('action','<?php echo e(url('/')); ?>/experts');
      }
   });
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>