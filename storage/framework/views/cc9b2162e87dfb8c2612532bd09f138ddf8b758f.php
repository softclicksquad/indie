<?php /*test demo for cropping image */ ?>
<link rel="stylesheet" href="<?php echo e(url('/public')); ?>/assets/plugins/crop-avatar/css/cropper.min.css">
<link rel="stylesheet" href="<?php echo e(url('/public')); ?>/assets/plugins/crop-avatar/css/main.css">
<style>
	.clr{clear: both;}
</style>

       <div  id="crop-avatar" >
        <div style="display: none;" id="crop_image_modal" class="avatar-view profile-img">
         <?php echo e(trans('new_translations.upload_profile_picture')); ?>

            <img src="<?php echo e(url('/public')); ?>/uploads/front/profile/no_image.gif" width="271" alt="Avatar">
        </div>


         <!-- Cropping modal -->
          <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <form class="avatar-form" action="<?php echo e(url('/')); ?>/upload_cropped_image" enctype="multipart/form-data" method="post">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title crop_modal_title" id="avatar-modal-label" ><?php echo e(trans('new_translations.crop_image')); ?></h4>
                  </div>

                  <?php echo e(csrf_field()); ?>


                  <div class="modal-body">

                   <!-- Upload image and data -->

                      <div class="avatar-upload"  >
                        <input type="hidden" class="avatar-src" name="avatar_src">
                        <input type="hidden" class="avatar-data" name="avatar_data">
                        <label for="avatarInput"><?php echo e(trans('new_translations.browse')); ?></label>
                        <input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
                      </div>

                      <span id='image_err' style="color: red;"></span>
                    
                      <input type="hidden" name="upload_portfolio" id="upload_portfolio" readonly="" value="" ></input>

                    <div class="avatar-body" id="avatar_body">
                      <!-- Crop and preview -->
                      <div class="row">
                        <div class="col-md-9">
                          <div class="avatar-wrapper"></div>
                        </div>
                        <div class="col-md-3">
                          <div class="avatar-preview preview-lg"></div>
                          <div class="avatar-preview preview-md"></div>
                          <div class="avatar-preview preview-sm"></div>
                        </div>
                      </div>

                      <div class="row avatar-btns">
                        <div class="col-md-9">
                          <div class="btn-group">
                            <button type="button" class="btn btn-primary" data-method="rotate" data-option="-90" title="Rotate -90 degrees"><?php echo e(trans('new_translations.rotate_left')); ?></button>
                            <?php /* <button type="button" class="btn btn-primary" data-method="rotate" data-option="-15">-15deg</button> */ ?>
                            <button type="button" class="btn btn-primary" data-method="rotate" data-option="-30">-30<?php echo e(trans('new_translations.deg')); ?></button>
                            <button type="button" class="btn btn-primary in-mobi-block" data-method="rotate" data-option="-45">-45<?php echo e(trans('new_translations.deg')); ?></button>
                            <button type="button" class="btn btn-primary radious-block-new" data-method="rotate" data-option="90" title="Rotate 90 degrees"><?php echo e(trans('new_translations.rotate_right')); ?></button>
                            <?php /* <button type="button" class="btn btn-primary" data-method="rotate" data-option="15">15deg</button> */ ?>
                            <button type="button" class="btn btn-primary" data-method="rotate" data-option="30">30<?php echo e(trans('new_translations.deg')); ?></button>
                            <button type="button" class="btn btn-primary" data-method="rotate" data-option="45">45<?php echo e(trans('new_translations.deg')); ?></button>
							            <div class="clr"></div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <button type="submit" class="btn btn-primary btn-block avatar-save" id="chk_image_file" ><?php echo e(trans('new_translations.set_profile_picture')); ?></button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div> -->
                </form>
              </div>
            </div>
          </div><!-- /.modal -->
       </div>
       <!-- Loading state -->
<div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>

<script src="<?php echo e(url('/public')); ?>/assets/plugins/crop-avatar/js/cropper.min.js"></script>
<script src="<?php echo e(url('/public')); ?>/assets/plugins/crop-avatar/js/main.js"></script>

<script type="text/javascript">

 function openCropModal(data = false)
 {  
    if(data == "PORTFOLIO")
    {
      $("#crop_image_modal").trigger('click');
      $("#avatarInput").val("");
      $("#chk_image_file").html('Upload Portfolio Image');
      $(".crop_modal_title").html('Crop Portfolio Image');
      $("#upload_portfolio").val('YES');
    }
    else
    {
      $("#crop_image_modal").trigger('click');
      $("#avatarInput").val("");
      $(".crop_modal_title").html('Crop Profile Image');
    }
 }

 $('#chk_image_file').on('click' ,function () 
 {
    var ext1 = $('#avatarInput').val().split('.').pop().toLowerCase();
            
    if($.inArray(ext1, ['gif','png','jpg','jpeg']) == -1) 
    { 
      $("#image_err").fadeIn();
      $("#image_err" ).html("Please select image file.!");
      $("#image_err").fadeOut(7000);   
      return false;
    }
 });  


 	$(document).ready(function(){
 		// This is the simple bit of jquery to duplicate the hidden field to subfile
 		$('#pdffile').change(function(){
			$('#subfile').val($(this).val());
		});

		// This bit of jquery will show the actual file input box
		$('#showHidden').click(function(){
			$('#pdffile').css('visibilty','visible');
		});
        
        // This is the simple bit of jquery to duplicate the hidden field to subfile
 		$('#pdffile1').change(function(){
			$('#subfile1').val($(this).val());
		});

		// This bit of jquery will show the actual file input box
		$('#showHidden1').click(function(){
			$('#pdffile1').css('visibilty','visible');
		});
 	});

</script>

<?php /*  Demo ends */ ?>