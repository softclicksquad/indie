<!-- Modal -->
 <div class="modal fade signup-landing-modal" id="signup_modal" role="dialog">
     <div class="modal-dialog modal-lg">
        <div class="modal-content">
           <button type="button" class="close" data-dismiss="modal"><img class="img-responsive" src="<?php echo e(url('/public')); ?>/front/images/close-icon.png" alt="Close"/> </button>
           <div class="modal-body">
              <div class="signup-client-block">
                  <h2><?php echo e(trans('auth/signup.text_i_am_a_client')); ?></h2>
                  <h5><?php echo e(trans('auth/signup.text_need_to_get_work_done')); ?></h5>
                  <p><?php echo e(trans('auth/signup.text_find_an_expertcolloborate_approve_amp_pay')); ?></p>
                  <a href="<?php echo e(url('/signup/client')); ?>" class="black-border-btn"><span></span> <?php echo e(trans('auth/signup.text_hire_an_expert')); ?></a>
              </div>
              <div class="signup-client-block signup-expert-block">
                  <h2><?php echo e(trans('auth/signup.text_i_am_an_expert')); ?></h2>
                  <h5><?php echo e(trans('auth/signup.text_looking_for_freelance_project')); ?></h5>
                  <p><?php echo e(trans('auth/signup.text_find_freelance_project_and_grow_your_business')); ?></p>
                  <a href="<?php echo e(url('/signup/expert')); ?>" class="black-border-btn"><span></span> <?php echo e(trans('auth/signup.text_work')); ?></a>
              </div>
           </div>
        </div>
     </div>
 </div>
<!--model popup end here--> 