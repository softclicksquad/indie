<!-- HEader -->        
<?php echo $__env->make('front.layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>    
        
<!-- BEGIN Sidebar -->

<!-- END Sidebar -->

<!-- BEGIN Content -->

    <?php echo $__env->yieldContent('main_content'); ?>

    <!-- END Main Content -->

<!-- Footer -->      
<?php echo $__env->make('front.layout.dynamic_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>    

<?php echo $__env->make('front.layout.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>    
                
              