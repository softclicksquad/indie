<?php 

namespace App\Common\Services;
use Crypt;
use Illuminate\Http\Request;

use Session;
use Redirect;
use Sentinel;


use App\Models\SubscriptionPacksModel;
use App\Models\TransactionsModel;
use App\Models\SubscriptionUsersModel;
use App\Models\ProjectsBidsModel;
use App\Models\ProjectsBidsTopupModel;
use App\Models\CurrencyModel;

use App\Common\Services\WalletService;
use App\Models\NotificationsModel;
use App\Common\Services\MailService;


use App\Common\Services\PaymentService;
use App\Common\Services\PaypalService;
use App\Common\Services\StripeService;
use Lang;
class BidTopupService
{
	public function __construct()
	{
		 if(!Session::has('locale')){
           Session::put('locale', \Config::get('app.locale'));
         }
         app()->setLocale(Session::get('locale'));
         view()->share('selected_lang',\Session::get('locale'));
 		 $this->PaypalService = FALSE;
		 $this->StripeService = FALSE;
		 if(! $user = Sentinel::check()){
	        return redirect('/login');
	     }

     	 $this->user_id = $user->id;
		 $this->SubscriptionPacksModel 	 	     = new SubscriptionPacksModel();
		 $this->TransactionsModel		 	     = new TransactionsModel();
		 $this->SubscriptionUsersModel   	     = new SubscriptionUsersModel();
		 $this->ProjectsBidsModel   	   	     = new ProjectsBidsModel();
		 $this->ProjectsBidsTopupModel   	   	 = new ProjectsBidsTopupModel();
		 $this->PaymentService		 	         = new PaymentService();
		 $this->WalletService		 	         = new WalletService();
		 $this->NotificationsModel		 	     = new NotificationsModel();
		 $this->MailService		 	             = new MailService();
		 $this->CurrencyModel   	             = new CurrencyModel();

		 $admin_payment_settings = $this->PaymentService->get_admin_payment_settings();
		 if (isset($admin_payment_settings) && sizeof($admin_payment_settings)>0) 
		 {
		 	if (isset($admin_payment_settings['paypal_client_id']) && isset($admin_payment_settings['paypal_secret_key']) && isset($admin_payment_settings['paypal_payment_mode'])) 
		 	{
		 		$this->PaypalService = new PaypalService($admin_payment_settings['paypal_client_id'],$admin_payment_settings['paypal_secret_key'],$admin_payment_settings['paypal_payment_mode']);
		 	}
		 	if (isset($admin_payment_settings['stripe_secret_key']))
		 	{
		 		$this->StripeService = new StripeService($admin_payment_settings['stripe_secret_key']);
		 	}
		 }
		 if(isset($user)){
	        $logged_user                 = $user->toArray();  
	        if($logged_user['mp_wallet_created'] == 'Yes'){
	          $this->mp_user_id          = $logged_user['mp_user_id'];
	          $this->mp_wallet_id        = $logged_user['mp_wallet_id'];
	          $this->mp_wallet_created   = $logged_user['mp_wallet_created'];
	        } else {
		        $this->mp_user_id          = '';
		        $this->mp_wallet_id        = '';
		        $this->mp_wallet_created   = 'No';
		    }
	     } else {
	        $this->mp_user_id          = '';
	        $this->mp_wallet_id        = '';
	        $this->mp_wallet_created   = 'No';
	     }

	     if($user->inRole('admin'))
	     {
	      	$this->payment_cancel_url = url(config('app.project.admin_panel_slug')."/payment/cancel");
	      	$this->payment_success_url = url(config('app.project.admin_panel_slug').'/payment/success');
	     }
	     else
	     {
	      	$this->payment_cancel_url = url('/').'/payment/cancel';
	      	$this->payment_success_url = url('/').'/payment/success';
	     }
	}
	
	public function payment($arr_payment_details)
	{
		// dd('bid topup service',$arr_payment_details);
		$arr_user_subscription    = array();
		$arr_transaction          = array();
		$arr_topup_bid            = array();
		$invoice_id               = "";
		$obj_subscription_details = FALSE;

		if (isset($arr_payment_details['payment_obj_id']) && isset($arr_payment_details['payment_method'])) 
		{
			//dd('hereeee');
			/*chk_already_bid_purchase */
				// $chk_alreadybidnpurchase = $this->ProjectsBidsTopupModel
				//                                      ->where('user_subscription_id',$arr_payment_details['payment_obj_id'])
				//                                      ->where('expert_user_id',$this->user_id)
				//                                      ->limit('1')
				//                                      //->where('is_used','0')
				//                                      ->orderBy('id','DESC')
				//                                      ->first();
				// dd($chk_alreadybidnpurchase->toArray());
	   //          if(isset($chk_alreadybidnpurchase) && $chk_alreadybidnpurchase != null){
	   //          	$transaction_done = $chk_alreadybidnpurchase->toArray();
	   //              if(isset($transaction_done['is_used']) && $transaction_done['is_used'] == '0'){
		  //               Session::flash('payment_error_msg',trans('controller_translations.this_transaction_already_done'));	
				//         return redirect()->back();
	   //              }
	   //          }
            /* chk_already_bid_purchase */

			/*this is pack id in subscription $arr_payment_details['payment_obj_id'];*/
			/* Payment method :1-paypal 2-Stripe $arr_payment_details['payment_method'];*/
			$obj_subscription_details =  $this->SubscriptionUsersModel->where('id',$arr_payment_details['payment_obj_id'])->first();
			//dd($obj_subscription_details);
			if ($obj_subscription_details!=FALSE) 
			{
				$arr_user_subscription = $obj_subscription_details->toArray();

				if (sizeof($arr_user_subscription)>0) 
				{
					if (isset($arr_user_subscription['topup_bid_price'])) 
					{
					  	$invoice_id	= $this->_generate_invoice_id();

					  	/*First make transaction  */ 
					  	$arr_transaction['user_id']          = $this->user_id;
					  	$arr_transaction['invoice_id']       = $invoice_id;
					  	/*transaction_type is type for transactions 1-Subscription 2-Milestone 3-Release Milestones 4- topup bid*/
					  	$arr_transaction['transaction_type'] = 4;
					  	// $arr_transaction['paymen_amount']    = $arr_user_subscription['topup_bid_price']*5;
					  	$arr_transaction['paymen_amount']    = $arr_user_subscription['topup_bid_price'];
					  	$arr_transaction['payment_method']   = $arr_payment_details['payment_method'];
					  	$arr_transaction['payment_date']     = date('c');

					  	/* get currency for transaction */
					  	$arr_currency = setCurrencyForTransaction();

					  	$arr_transaction['currency']      = isset($arr_currency['currency']) ? $arr_currency['currency'] : '$';
					  	$arr_transaction['currency_code'] = isset($arr_currency['currency_code']) ? $arr_currency['currency_code'] : 'USD';

					  	$transaction = $this->TransactionsModel->create($arr_transaction);
					  	$transaction = $this->TransactionsModel->create($arr_transaction);
					  	$transaction = $this->TransactionsModel->create($arr_transaction);
					  	$transaction = $this->TransactionsModel->create($arr_transaction);
					  	$transaction = $this->TransactionsModel->create($arr_transaction);

					  	if ($transaction) 
					  	{
						  	/*insert record in vhc_projects_bids_topup for invoice and topup maping*/
						  	$arr_topup_bid['expert_user_id']       = $this->user_id;
						  	$arr_topup_bid['user_subscription_id'] = $arr_user_subscription['id'];
						  	$arr_topup_bid['invoice_id']           = $invoice_id;
						  	$arr_topup_bid['amount']               = $arr_user_subscription['topup_bid_price'];
						  	$arr_topup_bid['payment_status']       = 0;
						  	$arr_topup_bid['is_used']              = 0;
							$topup = $this->ProjectsBidsTopupModel->create($arr_topup_bid);
							$topup = $this->ProjectsBidsTopupModel->create($arr_topup_bid);
							$topup = $this->ProjectsBidsTopupModel->create($arr_topup_bid);
							$topup = $this->ProjectsBidsTopupModel->create($arr_topup_bid);
							$topup = $this->ProjectsBidsTopupModel->create($arr_topup_bid);
							if ($topup) 
							{
								/*Now redirect to payment gateway $arr_payment_details[payment_method] is Paypal and Stripe  1-Paypal 2-Stripe */
							  	if (isset($arr_payment_details['payment_method']) && $arr_payment_details['payment_method']==1) 
							  	{
							  		//Paypal
							  		if(!$this->PaypalService==FALSE)
									{
									 	$this->PaypalService->postPayment($invoice_id);
									}
							  	}
							  	elseif (isset($arr_payment_details['payment_method']) && $arr_payment_details['payment_method']==2) 
							  	{
							  		 if(!$this->StripeService==FALSE)
									 {
									 	if (isset($arr_payment_details['cardNumber']) && isset($arr_payment_details['cardExpiryMonth']) && isset($arr_payment_details['cardExpiryYear'])) 
									 	{
									 		$card_details = array('number' => $arr_payment_details['cardNumber'], 'exp_month' => $arr_payment_details['cardExpiryMonth'], 'exp_year' => $arr_payment_details['cardExpiryYear']);
									 		$this->StripeService->postPayment($invoice_id,$card_details);
									 	}
									 }
							  	}
							  	elseif (isset($arr_payment_details['payment_method']) && $arr_payment_details['payment_method']==3)  // wallet
				  	            {
				  	            	// dd('hereeeee');
				  	            	$admin_data  = get_user_wallet_details('1',$arr_transaction['currency_code']);
				  	            	$mp_details  = get_user_wallet_details($this->user_id,$arr_transaction['currency_code']);
				  	            	//dd($admin_data,$mp_details);
				  	            	if(isset($admin_data) && count($admin_data)>0)
				  	            	{
					  	            	// check client wallet balance
							            $mangopay_wallet_details = [];
							            $get_wallet_details      = $this->WalletService->get_wallet_details($mp_details['mp_wallet_id']);
							            //dd($get_wallet_details);
							            if(isset($get_wallet_details) && $get_wallet_details!='false' || $get_wallet_details != false){
							              $mangopay_wallet_details = $get_wallet_details;
							            }
							            $wallet_amount= 0;
							            if(isset($mangopay_wallet_details->Balance->Amount) && $mangopay_wallet_details->Balance->Amount != ""){
							               $wallet_amount= $mangopay_wallet_details->Balance->Amount/100;
							            }
							           
							            $string_amount = str_replace(",","",$arr_transaction['paymen_amount']);


							            $amount_int    = (float) $string_amount;
							            //dd($amount_int,$wallet_amount);
	                                    if($amount_int > $wallet_amount)
	                                    {

	                                    	if(isset($mp_details) && count($mp_details)>0)
						                    {
						                      $transaction_inp['mp_user_id']      = isset($mp_details['mp_user_id'])?$mp_details['mp_user_id']:'';
						                      $transaction_inp['mp_wallet_id']    = isset($mp_details['mp_wallet_id'])?$mp_details['mp_wallet_id']:'';
						                      $transaction_inp['ReturnURL']       = url('/expert/bids/purchase/return?invoice_id='.$invoice_id.'&amount_int='.$amount_int.'&currency_code='.$arr_transaction['currency_code'].'&debited_UserId='.$mp_details['mp_user_id'].'&credited_UserId='.$admin_data['mp_user_id'].'&debited_walletId='.$mp_details['mp_wallet_id'].'&credited_walletId='.$admin_data['mp_wallet_id'].'&user_id='.$admin_data['user_id']);
						                      $transaction_inp['currency_code']   = $arr_transaction['currency_code'];
						                      
						                    }
						                    $amount             = (float) $amount_int- (float)$wallet_amount;
						                    //dd($arr_payment_details['currency_code']);
						                    $obj_data = $this->CurrencyModel->with('deposite')
						                                                    ->where('currency_code',$arr_transaction['currency_code'])
						                                                    ->first();
						                    //dd($obj_data);
						                    if($obj_data)
						                    {
						                        $arr_data = $obj_data->toArray();
						                        $min_amount = isset($arr_data['deposite']['min_amount'])?$arr_data['deposite']['min_amount']:'';
						                        $max_amount = isset($arr_data['deposite']['max_amount'])?$arr_data['deposite']['max_amount']:'';
						                        $min_charge = isset($arr_data['deposite']['min_amount_charge'])?$arr_data['deposite']['min_amount_charge']:'';
						                        $max_charge = isset($arr_data['deposite']['max_amount_charge'])?$arr_data['deposite']['max_amount_charge']:'';
						                        
						                        $service_charge_payin = 0;	
						                        // if($amount>$min_amount)
						                        // {
						                        //     $service_charge_payin = (float)$amount * (float)$max_charge/100;
						                        // }
						                        // else
						                        // {
						                        //     $service_charge_payin =  (float) $min_charge;
						                        // }
						                        $total_amount = (float)$amount + (float)$service_charge_payin;
						                        
						                    }

						                    $transaction_inp['amount']          = $total_amount;
						                    $transaction_inp['fee']             = (int)$service_charge_payin;
						                    $mp_add_money_in_wallet             = $this->WalletService->payInWallet($transaction_inp);
						                    
						                    if(isset($mp_add_money_in_wallet->Status) && $mp_add_money_in_wallet->Status == 'CREATED')
						                    {   
						                        Redirect::to($mp_add_money_in_wallet->ExecutionDetails->RedirectURL)->send();
						                    } 
						                    else 
						                    {
						                      
						                      if(isset($mp_add_money_in_wallet) && $mp_add_money_in_wallet == false){
						                        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
						                        return redirect()->back();
						                      }else{
						                        Session::flash('error',$mp_add_money_in_wallet);
						                        return redirect()->back();
						                      }
						                    }

							              // Session::flash('payment_error_msg',trans('controller_translations.your_wallet_amount_is_not_suffeciant_to_make_transaction'));
							              // return redirect()->back();
							            }
							            else 
							            {
					                        $transaction_inp['tag']                      = $arr_transaction['invoice_id'].'-Top up bid fee';
					                        $transaction_inp['debited_UserId']           = $mp_details['mp_user_id'];           // expert user id
					                        $transaction_inp['credited_UserId']          = $admin_data['mp_user_id'];   // archexpert admin user id
					                        $transaction_inp['total_pay']                = $amount_int;                 // services amount 
					                        $transaction_inp['debited_walletId']         = $mp_details['mp_wallet_id'];
					                        $transaction_inp['credited_walletId']        = $admin_data['mp_wallet_id']; // archexpert admin wallet id
					                        $transaction_inp['currency_code']            = $arr_transaction['currency_code']; // archexpert admin wallet id
				 	                        $transaction_inp['cost_website_commission']  = '0';
									  		$pay_fees        = $this->WalletService->walletTransfer($transaction_inp);
									  		$project         = [];
									  		if(isset($pay_fees->Status) && $pay_fees->Status == 'SUCCEEDED'){
									  		   // update topup details
										  		   $update_topup = $this->ProjectsBidsTopupModel->where('invoice_id',$arr_transaction['invoice_id'])->update(['payment_status'=>'1']);
				                               // end update topup details
				                               
				                               // update transaction details
					                               $update_transaction   = [];
					                               $update_transaction['WalletTransactionId']    = isset($pay_fees->Id) ? $pay_fees->Id : '0';
					                               $update_transaction['payment_status']         = 2; // paid
					                               $update_transaction['response_data']          = json_encode($pay_fees); // paid
						  	                       $updatetransaction = $this->TransactionsModel->where('invoice_id',$arr_transaction['invoice_id'])->update($update_transaction); 
						  	                   // end update transaction details    

						  	                   // send notification to admin
						  	                      $arr_admin_data                         =  [];
									              $arr_admin_data['user_id']              =  $admin_data['id'];
									              $arr_admin_data['user_type']            = '1';
									              $arr_admin_data['url']                  = 'admin/wallet/archexpert';
									              $arr_admin_data['project_id']           = '';
									              $arr_admin_data['notification_text_en'] = $arr_transaction['invoice_id'].'-'.Lang::get('controller_translations.project_service_fee_paid',[],'en','en').' '.$pay_fees->Id;
									              $arr_admin_data['notification_text_de'] = $arr_transaction['invoice_id'].'-'.Lang::get('controller_translations.project_service_fee_paid',[],'de','en').' '.$pay_fees->Id;
									              $this->NotificationsModel->create($arr_admin_data); 
						  	                   // end send notification to admin    

					                           Session::flash('payment_success_msg',trans('controller_translations.msg_payment_transaction_for_extra_bid_successfully'));
											   Session::flash('payment_return_url','/projects');
										       $this->MailService->TopupBidMail($arr_transaction['invoice_id']);
										       Redirect::to($this->payment_success_url)->send();
									  		} else {
									  		   if(isset($pay_fees->ResultMessage)){
									  		   Session::flash('payment_error_msg',$pay_fees->ResultMessage);	
									  		   } else {
			                                     if(gettype($pay_fees) == 'string'){
			                                     	Session::flash('payment_error_msg',$pay_fees);
			                                     }
									  		   }
					                           return redirect()->back();
									  		}
							            }
				  	            	}else {
			                            if(isset($admin_data)){
				                            // send notification to admin
							  	              $arr_admin_data                         =  [];
								              $arr_admin_data['user_id']              =  $admin_data['id'];
								              $arr_admin_data['user_type']            = '1';
								              $arr_admin_data['url']                  = 'admin/wallet/archexpert';
								              $arr_admin_data['project_id']           = '';
								              $arr_admin_data['notification_text_en'] = Lang::get('controller_translations.create_wallet',[],'en','en');
								              $arr_admin_data['notification_text_de'] = Lang::get('controller_translations.create_wallet',[],'de','en');
								              $this->NotificationsModel->create($arr_admin_data);      
							  	            // end send notification to admin 	
			                            }
							  		    Session::flash('payment_error_msg',trans('controller_translations.admin_dont_have_wallet_created_yet'));	
			                            return redirect()->back(); 
							  		}
				  	            }
							}
						}
					}
				}
			}
		}
		
	}
   private function _generate_invoice_id()
   {
   		$secure = TRUE;    
        $bytes = openssl_random_pseudo_bytes(3, $secure);
        $order_token = 'INV'.date('Ymd').strtoupper(bin2hex($bytes));
        return $order_token;
   }
}
?>