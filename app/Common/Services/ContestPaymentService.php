<?php 

namespace App\Common\Services;
use Crypt;
use Illuminate\Http\Request;

use Session;
use Redirect;
use Sentinel;

use App\Models\TransactionsModel;
use App\Models\MilestonesModel;
use App\Models\ContestModel;
use App\Models\SiteSettingModel;
use App\Models\ContestEntryModel;  
use App\Models\UserModel;  
use App\Models\CurrencyModel;  


use App\Common\Services\PaymentService;
use App\Common\Services\PaypalService;
use App\Common\Services\StripeService;
use App\Common\Services\WalletService;
use App\Common\Services\MailService;
use App\Models\NotificationsModel;
use App\Models\UserWalletModel;
use Lang;
class ContestPaymentService
{
	public function __construct()
	{
		 if(!Session::has('locale'))
         {
           Session::put('locale', \Config::get('app.locale'));
         }
         app()->setLocale(Session::get('locale'));
         view()->share('selected_lang',\Session::get('locale'));
		
		 $this->PaypalService = FALSE;
		 $this->StripeService = FALSE;
		 if(! $user = Sentinel::check()) 
	     {
	        return redirect('/login');
	     }
      	 $this->user_id             = $user->id;
		 $this->TransactionsModel 	= new TransactionsModel();
		 $this->MilestonesModel		= new MilestonesModel();
		 $this->ContestModel 	    = new ContestModel();
		 $this->SiteSettingModel 	= new SiteSettingModel();
		 $this->PaymentService		= new PaymentService();
		 $this->WalletService		= new WalletService();
		 $this->MailService		    = new MailService();
		 $this->NotificationsModel	= new NotificationsModel();
		 $this->ContestEntryModel	= new ContestEntryModel();
		 $this->CurrencyModel	    = new CurrencyModel;
		 $this->UserModel			= new UserModel();
		 $this->UserWalletModel  	= new UserWalletModel();
		 $admin_payment_settings    = $this->PaymentService->get_admin_payment_settings();
		 
		$this->payment_cancel_url  = url('/').'/payment/cancel';
      	$this->payment_success_url = url('/').'/payment/success';
		$this->posted_contest_url  = url('/client/contest/posted');

	  	if(isset($user)){
	        $logged_user                 = $user->toArray();  
	        if($logged_user['mp_wallet_created'] == 'Yes'){
	          $this->mp_user_id          = $logged_user['mp_user_id'];
	          $this->mp_wallet_id        = $logged_user['mp_wallet_id'];
	          $this->mp_wallet_created   = $logged_user['mp_wallet_created'];
	        } else {
		        $this->mp_user_id          = '';
		        $this->mp_wallet_id        = '';
		        $this->mp_wallet_created   = 'No';
		     }
	    } else {
	        $this->mp_user_id          = '';
	        $this->mp_wallet_id        = '';
	        $this->mp_wallet_created   = 'No';
	     }
	}

	public function payment($arr_payment_details)
	{
		//dd($arr_payment_details);
		// $arr_payment_details = [];	
		$is_winner_choosen = "NO";
		if(empty($arr_payment_details)){
			Session::flash('error',trans('controller_translations.text_sorry_somthing_goes_wrong_please_try_again'));
			return redirect()->back();
		}
		$is_entry_has_winner =$this->ContestEntryModel->where('id',base64_decode($arr_payment_details['contest_entry_id']))->select('is_winner')->first(); 
		if(isset($is_entry_has_winner) && $is_entry_has_winner !=null){
			$is_winner_choosen = isset($is_entry_has_winner->is_winner)?$is_entry_has_winner->is_winner:"";		
		}

		if(isset($is_winner_choosen) && $is_winner_choosen == "YES"){
			Session::flash('error',"Sorry you cannot proceed because winner is already choosen for this contest.");
			return redirect()->back();
		}

		$is_contest_has_winner =$this->ContestModel->where('id',base64_decode($arr_payment_details['payment_obj_id']))->select('winner_choose')->first(); 
		if(isset($is_contest_has_winner) && $is_contest_has_winner !=null){
			$is_winner_choosen = isset($is_contest_has_winner->is_winner)?$is_contest_has_winner->is_winner:"";		
		}
		if(isset($is_winner_choosen) && $is_winner_choosen == "YES"){
			Session::flash('error',"Sorry you cannot proceed because winner is already choosen for this contest.");
			return redirect()->back();
		}

		$arr_transaction    = array();
		$obj_site_settings  = FALSE;
		$invoice_id         = "";

        $winner_details     = []; 
        $winner_expert      = [];
        $get_winner_details = $this->ContestEntryModel->with(['expert_details'])->where('id',base64_decode($arr_payment_details['contest_entry_id']))->first();
        if($get_winner_details){
        	$winner_details = $get_winner_details->toArray();
        	if(isset($winner_details['expert_details']['user_details']) && $winner_details['expert_details']['user_details'] != ""){
            $winner_expert  = $winner_details['expert_details']['user_details'];
        	}
        }
		if (isset($arr_payment_details['payment_obj_id']) && isset($arr_payment_details['payment_method'])) 
		{
			/*$arr_payment_details['payment_obj_id'] is project id in this service;*/
			/* Payment method :1-paypal 2-Stripe $arr_payment_details['payment_method']; */
		  	$invoice_id	= $this->_generate_invoice_id();
		  	/*First make transaction  */ 
		  	$arr_transaction['user_id']          = $this->user_id;
		  	$arr_transaction['invoice_id']       = $invoice_id;
		  	/*transaction_type is type for transactions 1-Subscription 2-Milestone 3-Release Milestones 5- Project Payment */
		  	$arr_transaction['transaction_type'] = 7;
		  	$arr_transaction['paymen_amount']    = $arr_payment_details['contest_price'];
		  	$arr_transaction['payment_method']   = $arr_payment_details['payment_method'];
		  	$arr_transaction['payment_date']     = date('c');
		  	/* get currency for transaction using helper function */
		  	//$arr_currency = setCurrencyForTransaction($arr_payment_details['payment_obj_id']);
		  	//dd($arr_currency);
		  	$arr_transaction['currency']         = isset($arr_payment_details['currency'])?
		  											     $arr_payment_details['currency']:$arr_payment_details['currency_code'];
		  	$arr_transaction['currency_code']    = isset($arr_payment_details['currency_code'])?
		  											     $arr_payment_details['currency_code']:'';
		  	$transaction = $this->TransactionsModel->create($arr_transaction);
		  	if($transaction) 
		  	{
		  		$obj_contest_data = $this->ContestModel->where('id',$arr_payment_details['payment_obj_id'])->first();
		  		$updated_at = isset($obj_contest_data->updated_at) ? $obj_contest_data->updated_at : NULL;
		  		/*update invoice_id  in project tabel*/
				$update_status = $this->ContestModel->where('id',$arr_payment_details['payment_obj_id'])->update(['invoice_id' =>$invoice_id,'edited_at'=>date('Y-m-d H:i:s'),'updated_at' => $updated_at]);
				if ($update_status) 
				{
					if (isset($arr_payment_details['payment_method']) && $arr_payment_details['payment_method']==3)  // wallet
				  	{
                        // Service fee payment
                        $expert_data                  = $winner_expert;
                        if(isset($expert_data) && isset($expert_data['mp_wallet_created']) && $expert_data['mp_wallet_created'] =='Yes'){
                    	    // check client wallet balance
				            $mangopay_wallet_details = [];
				            $client_mp_details = $this->UserWalletModel->where('user_id',$this->user_id)
														->where('currency_code',$arr_payment_details['currency_code'])
									                    ->first();
				            if($client_mp_details)
				            {
				              	$mp_details         = $client_mp_details->toArray();
				            }
				            //dd($mp_details);


				            $get_wallet_details      = $this->WalletService->get_wallet_details($mp_details['mp_wallet_id']);
				            if(isset($get_wallet_details) && $get_wallet_details!='false' || $get_wallet_details != false){
				              $mangopay_wallet_details = $get_wallet_details;
				            }
				            $wallet_amount= 0;
				            if(isset($mangopay_wallet_details->Balance->Amount) && $mangopay_wallet_details->Balance->Amount != ""){
				               $wallet_amount= $mangopay_wallet_details->Balance->Amount/100;
				            }

				            /*$string_amount = str_replace(",","",$arr_transaction['paymen_amount']);
				            $amount_int    = (int) $string_amount;*/

				            $string_amount = str_replace(",","",$arr_transaction['paymen_amount']);
				            $amount_int    = $string_amount;
				            //dd($expert_data,$amount_int,$wallet_amount);
				            //dd($amount_int);
				            $obj_data = $this->CurrencyModel->with('deposite')
                                        ->where('currency_code',$arr_payment_details['currency_code'])
                                        ->first();
					        if($obj_data)
					        {
						          $arr_data = $obj_data->toArray();
						          $min_amount = isset($arr_data['deposite']['min_amount'])?$arr_data['deposite']['min_amount']:'';
						          //$max_amount = isset($arr_data['deposite']['max_amount'])?$arr_data['deposite']['max_amount']:'';
						          $min_charge = isset($arr_data['deposite']['min_amount_charge'])?$arr_data['deposite']['min_amount_charge']:'';
						          $max_charge = isset($arr_data['deposite']['max_amount_charge'])?$arr_data['deposite']['max_amount_charge']:'';
						          
						          if($amount_int>$min_amount)
						          {
						              $service_charge = (float)$amount_int * (float)$max_charge/100;
						          }
						          else
						          {
						              $service_charge =  (float) $min_charge;
						          }
						          //$total_amount = (float)$amount + (float)$service_charge;
					      	}


				            $expert_mp_details = $this->UserWalletModel->where('user_id',$expert_data['id'])
														->where('currency_code',$arr_payment_details['currency_code'])
									                    ->first();
				            if($expert_mp_details)
				            {
				              	$arr_expert_mp_details         = $expert_mp_details->toArray();
				            }

				            //dd($arr_expert_mp_details['mp_user_id'],$arr_expert_mp_details['mp_wallet_id']);

				            if($amount_int > $wallet_amount){
				              Session::flash('payment_error_msg',trans('controller_translations.your_wallet_amount_is_not_suffeciant_to_make_transaction'));
				             return redirect()->back();	
				            } else {
		                        $transaction_inp['tag']                      = $arr_transaction['invoice_id'].'-Contest Winner Payment';
		                        $transaction_inp['debited_UserId']           = $mp_details['mp_user_id'];// client user id
		                        $transaction_inp['credited_UserId']          = $arr_expert_mp_details['mp_user_id'];// archexpert expert user id
		                        $transaction_inp['total_pay']                = $amount_int;                 // services amount 
		                        $transaction_inp['debited_walletId']         = (string)$mp_details['mp_wallet_id']; // client wallet id
		                        $transaction_inp['credited_walletId']        = (string)$arr_expert_mp_details['mp_wallet_id']; // archexpert expert wallet id
	 	                        $transaction_inp['cost_website_commission']  = $service_charge;
	 	                        $transaction_inp['currency_code']            = isset($arr_payment_details['currency_code'])?
		  											                                 $arr_payment_details['currency_code']:'';

						  		$pay_fees        = $this->WalletService->walletTransfer($transaction_inp);
						  		$project         = [];
						  		if(isset($pay_fees->Status) && $pay_fees->Status == 'SUCCEEDED')
						  		{  
						  		   // update project details
							  		   $get_project = $this->ContestModel->where('invoice_id',$arr_transaction['invoice_id'])->first(['id']);
		                               if(isset($get_project)){
		                               	 $project = $get_project->toArray();
		                               }
			                           $update = $this->ContestEntryModel->where('id',base64_decode($arr_payment_details['contest_entry_id']))->update(['is_winner'=>'YES']);
								       if($update){
								            $this->ContestModel->where('id',$arr_payment_details['payment_obj_id'])->update(['winner_choose'=>'YES','contest_status'=>'2']);
								            // update transaction details
				                               $update_transaction   = [];
				                               $update_transaction['WalletTransactionId']    = isset($pay_fees->Id) ? $pay_fees->Id : '0';
				                               $update_transaction['payment_status']         = 2; // paid
				                               $update_transaction['response_data']          = json_encode($pay_fees); // paid
					  	                       $updatetransaction = $this->TransactionsModel->where('invoice_id',$arr_transaction['invoice_id'])->update($update_transaction); 
					  	                   // end update transaction details    

					  	                   // send notification to expert
					  	                      $arr_expert_data                         =  [];
								              $arr_expert_data['user_id']              =  $expert_data['id'];
								              $arr_expert_data['user_type']            = '3';
								              $arr_expert_data['url']                  = 'expert/wallet/dashboard';
								              $arr_expert_data['project_id']           = '';
								              $arr_expert_data['notification_text_en'] = $arr_transaction['invoice_id'].'-'.Lang::get('controller_translations.contest_price_paid',[],'en','en').' '.$pay_fees->Id;
								              $arr_expert_data['notification_text_de'] = $arr_transaction['invoice_id'].'-'.Lang::get('controller_translations.contest_price_paid',[],'de','en').' '.$pay_fees->Id;
								              $this->NotificationsModel->create($arr_expert_data); 
						  	                   // end send notification to expert    
					                           Session::flash('payment_success_msg',trans('client/contest/common.text_congratualtion_you_have_choose_winner_for_this_contest'));
											   Session::flash('payment_return_url','/client/contest/show_contest_entry_details/'.$arr_payment_details['contest_entry_id']);
										       //$this->MailService->ProjectPaymentMail($arr_transaction['invoice_id']);

											  /* send notification to expert */
		                                      $arr_noti_data                         =  [];
		                                      $arr_noti_data['user_id']              =  isset($expert_data['id'])?$expert_data['id']:'';
		                                      $arr_noti_data['user_type']            = '3';
		                                      $arr_noti_data['url']                  = 'expert/contest/show_contest_entry_details/'.$arr_payment_details['contest_entry_id'];
		                                      $arr_noti_data['project_id']           = '';
		                                      $arr_noti_data['notification_text_en'] = Lang::get('controller_translations.text_Congratulations_you_becomes_the_winner_of_contest',[],'en','en');
		                                      $arr_noti_data['notification_text_de'] = Lang::get('controller_translations.text_Congratulations_you_becomes_the_winner_of_contest',[],'de','en');
		                                      $this->NotificationsModel->create($arr_noti_data);
		                                      /* send notification to expert */


									       Redirect::to($this->payment_success_url)->send();
								       } else {
								            Session::flash("error",trans('controller_translations.error_unauthorized_action'));
								            return redirect()->back();
								       }	                               // end update project details
						  		} else {
						  		   if(isset($pay_fees->ResultMessage)){
						  		   Session::flash('payment_error_msg',$pay_fees->ResultMessage);	
						  		   } else {
                                     if(gettype($pay_fees) == 'string'){
                                     	Session::flash('payment_error_msg',$pay_fees);
                                     }
						  		   }
		                          return redirect()->back(); 
						  		}
				            }
                        }else {
                            if(isset($expert_data)){
	                            // send notification to expert
				  	              $arr_expert_data                         =  [];
					              $arr_expert_data['user_id']              =  $expert_data['id'];
					              $arr_expert_data['user_type']            = '1';
					              $arr_expert_data['url']                  = 'expert/wallet/dashboard';
					              $arr_expert_data['project_id']           = '';
					              $arr_expert_data['notification_text_en'] = Lang::get('controller_translations.create_wallet',[],'en','en');
					              $arr_expert_data['notification_text_de'] = Lang::get('controller_translations.create_wallet',[],'de','en');
					              $this->NotificationsModel->create($arr_expert_data);      
				  	            // end send notification to expert 	
                            }
				  		    Session::flash('payment_error_msg',trans('controller_translations.admin_dont_have_wallet_created_yet'));	
                           return redirect()->back(); 
				  		}
				  	}
				}
			}
		}
	}

	public function contest_payment($arr_payment_details)
	{
		$payment_obj_id = isset($arr_payment_details['payment_obj_id']) ? base64_encode(trim($arr_payment_details['payment_obj_id'])) : '';
		$redirect_back_url = url('/').'/client/dashboard';
        if($payment_obj_id!=''){
            $redirect_back_url = url('/').'/client/contest/contest_payment/'.$payment_obj_id;
        }

		$is_winner_choosen = "NO";
		if(empty($arr_payment_details))
		{
			Session::flash('error',trans('controller_translations.text_sorry_somthing_goes_wrong_please_try_again'));
			return redirect($redirect_back_url);
		}

		$is_payment_by_card = isset($arr_payment_details['is_payment_by_card']) ? $arr_payment_details['is_payment_by_card'] : '';
		$card_id            = isset($arr_payment_details['card_id']) ? $arr_payment_details['card_id'] : '';
		$card_currency      = isset($arr_payment_details['card_currency']) ? $arr_payment_details['card_currency'] : 'USD';

		if($is_payment_by_card == '1' && $card_id == '') {
			Session::flash('error','Invalid Card identifier.');
			return redirect($redirect_back_url);
		}

		$arr_transaction    = array();
		$obj_site_settings  = FALSE;
		$invoice_id         = "";

        $winner_details     = []; 
        $winner_expert      = [];

        $obj_contest = $this->ContestModel->where('id',$arr_payment_details['payment_obj_id'])->first();
	    if($obj_contest == false)
	    {
	        Session::flash('error',trans('controller_translations.text_sorry_somthing_goes_wrong_please_try_again'));
			return redirect($redirect_back_url);
	    }

	    $arr_contest_details = $obj_contest->toArray();

      	$amount        = $arr_contest_details['contest_price'];
	    $currency_code = $arr_contest_details['contest_currency'];

	    $arr_service_charge = get_service_charge($amount,$currency_code);

	    $service_charge = isset($arr_service_charge['service_charge']) ? $arr_service_charge['service_charge'] : 0; 
	    $service_cost   = isset($arr_service_charge['total_amount']) ? $arr_service_charge['total_amount'] : 0;

	    $arr_payment_details['service_cost'] = $service_cost;

      	if (isset($arr_payment_details['payment_obj_id']) && isset($arr_payment_details['payment_method'])) 
		{
			$client_mp_details = $this->UserWalletModel->where('user_id',$arr_payment_details['client_user_id'])
														->where('currency_code',$arr_payment_details['currency_code'])
									                    ->first();
			//dd($client_mp_details);
            if($client_mp_details)
            {
              	$mp_details         = $client_mp_details->toArray();
		  		$invoice_id			= $this->_generate_invoice_id();
		  		$admin_invoice_id	= $this->_generate_invoice_id();
		  		
              	if(isset($mp_details['mp_user_id']) && $mp_details['mp_user_id'] !="")
              	{	
					$project_name  = $arr_payment_details['payment_obj_id'].'-'.$arr_contest_details['contest_title']; 
                	$mangopay_wallet_details = [];
		            
		            $get_wallet_details      = $this->WalletService->get_wallet_details($mp_details['mp_wallet_id']);

		            if(isset($get_wallet_details) && $get_wallet_details!='false' || $get_wallet_details != false)
		            {
		              	$mangopay_wallet_details = $get_wallet_details;
		            }

		            $wallet_amount= 0;
		            if(isset($mangopay_wallet_details->Balance->Amount) && $mangopay_wallet_details->Balance->Amount != "")
		            {
		               (float) $wallet_amount = $mangopay_wallet_details->Balance->Amount/100;
		            }

		            if((float)$arr_contest_details['contest_price'] > (float)$wallet_amount)
		            {
		            	$contest_price_minus_balance = (float) $arr_contest_details['contest_price'] - (float)$wallet_amount;
						  
					    $arr_new_service_charge =  get_service_charge($contest_price_minus_balance,$currency_code);

					    $service_charge_payin = isset($arr_new_service_charge['service_charge']) ? $arr_new_service_charge['service_charge'] : 0;

		            	$arr_data = [];
			            $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)
			                                                ->where('currency_code',$mp_details['currency_code'])
			                                                ->first();
	      
					    if($obj_data)
					    {
					        $arr_data = $obj_data->toArray();
					    }


	            		if(isset($arr_data) && count($arr_data)>0)
					    {
					      	$transaction_inp['mp_user_id']      = isset($arr_data['mp_user_id'])?$arr_data['mp_user_id']:'';
					        $transaction_inp['mp_wallet_id']    = isset($arr_data['mp_wallet_id'])?$arr_data['mp_wallet_id']:'';
					        $transaction_inp['currency_code']   = $mp_details['currency_code'];
					        $transaction_inp['ReturnURL']       = url('/client/contest/details/'.$arr_payment_details['payment_obj_id'].'/API?mp_user_id='.$arr_data['mp_user_id'].'&mp_wallet_id='.$arr_data['mp_wallet_id'].'&currency_code='.$mp_details['currency_code'].'&payment_obj_id='.$arr_payment_details['payment_obj_id'].'&service_cost='.$arr_contest_details['contest_price'].'&project_name='.$project_name.'&admin_charge='.$service_charge_payin.'&is_payment_by_card='.$is_payment_by_card.'&card_id='.$card_id);
					    }
					    
    				    $amount = isset($arr_new_service_charge['total_amount']) ? $arr_new_service_charge['total_amount'] :0;;
					    $amount = round($amount,2);
						$transaction_inp['amount'] = $amount;

						if($is_payment_by_card == '1') {
						    
						    $transaction_inp['card_id']       = $card_id;
						    $transaction_inp['card_currency'] = $card_currency;
						    $arr_payin_wallet_by_card = $this->WalletService->payInWalletByCard($transaction_inp);
						    // dd($arr_payin_wallet_by_card);
						    if(isset($arr_payin_wallet_by_card['status']) && $arr_payin_wallet_by_card['status'] == 'error') {
					          	$msg = isset($arr_payin_wallet_by_card['msg']) ? $arr_payin_wallet_by_card['msg'] : 'Something went, wrong, Unable to process payment, Please try again.';
					          	Session::flash('error',$msg);  
					          	return redirect($redirect_back_url);
					        }
                                    
                            if(isset($arr_payin_wallet_by_card['data']['Status']) && $arr_payin_wallet_by_card['data']['Status'] == 'CREATED')
                            {
                                Redirect::to($arr_payin_wallet_by_card['data']['ExecutionDetails']->SecureModeRedirectURL)->send();
                            }
                            else if(isset($arr_payin_wallet_by_card['data']['Status']) && $arr_payin_wallet_by_card['data']['Status'] == 'SUCCEEDED')
                            {   
                                $store_mp_wallet_id = $this->ContestModel->where('id',$arr_payment_details['payment_obj_id'])->update($arr_project_data); 
	                  	
			                  	$arr_project_data                           = [];
			                  	$arr_project_data['mp_contest_wallet_id']   = isset($AppWallet->Id)?$AppWallet->Id:""; 
			                 

		                        $transaction_inp['tag']                      = $invoice_id.'-Contest Service Fee';
			                    $transaction_inp['debited_UserId']           = $mp_details['mp_user_id'];
								$transaction_inp['credited_UserId']          = $mp_details['mp_user_id']; 
			                    $transaction_inp['total_pay']                = $arr_payment_details['contest_price'];              
			                    $transaction_inp['debited_walletId']         = (string)$mp_details['mp_wallet_id']; 
			                    $transaction_inp['credited_walletId']        = (string)$arr_project_data['mp_contest_wallet_id']; 
			                    $transaction_inp['currency_code']            = $contest_currency; 
		 	                    $transaction_inp['cost_website_commission']  = isset($arr_payment_details['contest_services_fee'])?$arr_payment_details['contest_services_fee']:'0';

							  	$pay_fees        	   = $this->WalletService->walletTransfer($transaction_inp);

		 	                    /*$arr_admin_details = get_user_wallet_details('1',$contest_currency);  
		 	                    if($arr_admin_details)
		 	                    {
		 	                    	$transaction_admin_inp['tag']                      = $admin_invoice_id.'-Contest Service Fee';
			                    	$transaction_admin_inp['debited_UserId']           = $mp_details['mp_user_id'];           
			                    	$transaction_admin_inp['credited_UserId']          = $arr_admin_details['mp_user_id'];
			                    	$transaction_admin_inp['total_pay']                = $service_charge;              
			                    	$transaction_admin_inp['debited_walletId']         = (string)$mp_details['mp_wallet_id']; 
			                    	$transaction_admin_inp['credited_walletId']        = (string)$arr_admin_details['mp_wallet_id']; 
		 	                    	$transaction_admin_inp['cost_website_commission']  = isset($arr_payment_details['contest_services_fee'])?
		 	                    															   $arr_payment_details['contest_services_fee']:'0';

							  		$pay_admin_fees        = $this->WalletService->walletTransfer($transaction_admin_inp);
		 	                    }*/

							  	if(isset($pay_fees->Status) && $pay_fees->Status == 'SUCCEEDED')
								{
									$obj_contest_data = $this->ContestModel->where('id',$arr_payment_details['payment_obj_id'])->first();
							  		$updated_at = isset($obj_contest_data->updated_at) ? $obj_contest_data->updated_at : NULL;
									
									$store_mp_wallet_id = $this->ContestModel->where('id',$arr_payment_details['payment_obj_id'])->update(['invoice_id'=>$invoice_id,'payment_status'=>'1','edited_at'=>NULL,'updated_at'=>$updated_at]);

		        					Session::flash("success",trans('controller_translations.contest_posted_successfully'));
		        					Redirect::to($this->payment_success_url)->send();
			                    }
			                    else
			                    {
			                    	Session::flash('error','Error while creating contest.');
		                			return redirect($redirect_back_url);	
			                    }
                            } 
                            else 
                            {
                                Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
                                return redirect($redirect_back_url);
                            }

						} else {

							$mp_add_money_in_wallet             = $this->WalletService->payInWallet($transaction_inp);

							/*$arr_admin_details = get_user_wallet_details('1',$currency_code);  
					        if($arr_admin_details)
					        {
					            $transaction_admin_inp['tag']                      = $admin_invoice_id.'-Contest Service Fee';
					            $transaction_admin_inp['debited_UserId']           = $mp_details['mp_user_id'];           
					            $transaction_admin_inp['credited_UserId']          = $arr_admin_details['mp_user_id'];
					            $transaction_admin_inp['total_pay']                = $service_charge_payin;            
					            $transaction_admin_inp['debited_walletId']         = (string)$mp_details['mp_wallet_id']; 
					            $transaction_admin_inp['credited_walletId']        = (string)$arr_admin_details['mp_wallet_id']; 
					            $transaction_admin_inp['cost_website_commission']  = '0';
					           	$transaction_admin_inp['currency_code']            = $currency_code;
					            $pay_admin_fees        = $this->WalletService->walletTransfer($transaction_admin_inp);
					        }*/

							if(isset($mp_add_money_in_wallet->Status) && $mp_add_money_in_wallet->Status == 'CREATED')
							{   
							  	Redirect::to($mp_add_money_in_wallet->ExecutionDetails->RedirectURL)->send();
							} 
							elseif(isset($mp_add_money_in_wallet->Status) && $mp_add_money_in_wallet->Status == 'FAILED')
							{
								Session::flash('error',$mp_add_money_in_wallet->ResultMessage);
							    return redirect($redirect_back_url);
							}
							else 
							{
							  	//dd('Not Createddd');
							    if(isset($mp_add_money_in_wallet) && $mp_add_money_in_wallet == false){
							      Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
							      return redirect($redirect_back_url);
								}else{
							      Session::flash('error',$mp_add_money_in_wallet);
							      return redirect($redirect_back_url);
								}
							}

						}
		            }

                	// create a project wallet against client mp account
                	$contest_currency = isset($mp_details['currency_code'])?$mp_details['currency_code']:'USD'; 
                	//dd($contest_currency);
                	$AppWallet     = $this->WalletService->create_wallet_currency_wise($mp_details['mp_user_id'],$project_name.' contest wallet',$contest_currency);

                	//dd($arr_payment_details['service_cost'],$AppWallet);
	                if($AppWallet)
	                {
	                  	
	                  	$arr_project_data                           = [];
	                  	$arr_project_data['mp_contest_wallet_id']   = isset($AppWallet->Id)?$AppWallet->Id:""; 
	                 
	                    $store_mp_wallet_id = $this->ContestModel->where('id',$arr_payment_details['payment_obj_id'])->update($arr_project_data); 

                        $transaction_inp['tag']                      = $invoice_id.'-Contest Service Fee';
	                    $transaction_inp['debited_UserId']           = $mp_details['mp_user_id'];
						$transaction_inp['credited_UserId']          = $mp_details['mp_user_id']; 
	                    $transaction_inp['total_pay']                = $arr_payment_details['contest_price'];              
	                    $transaction_inp['debited_walletId']         = (string)$mp_details['mp_wallet_id']; 
	                    $transaction_inp['credited_walletId']        = (string)$arr_project_data['mp_contest_wallet_id']; 
	                    $transaction_inp['currency_code']            = $contest_currency; 
 	                    $transaction_inp['cost_website_commission']  = isset($arr_payment_details['contest_services_fee'])?$arr_payment_details['contest_services_fee']:'0';

					  	$pay_fees        	   = $this->WalletService->walletTransfer($transaction_inp);

 	                    /*$arr_admin_details = get_user_wallet_details('1',$contest_currency);  
 	                    if($arr_admin_details)
 	                    {
 	                    	$transaction_admin_inp['tag']                      = $admin_invoice_id.'-Contest Service Fee';
	                    	$transaction_admin_inp['debited_UserId']           = $mp_details['mp_user_id'];           
	                    	$transaction_admin_inp['credited_UserId']          = $arr_admin_details['mp_user_id'];
	                    	$transaction_admin_inp['total_pay']                = $service_charge;              
	                    	$transaction_admin_inp['debited_walletId']         = (string)$mp_details['mp_wallet_id']; 
	                    	$transaction_admin_inp['credited_walletId']        = (string)$arr_admin_details['mp_wallet_id']; 
 	                    	$transaction_admin_inp['cost_website_commission']  = isset($arr_payment_details['contest_services_fee'])?
 	                    															   $arr_payment_details['contest_services_fee']:'0';

					  		$pay_admin_fees        = $this->WalletService->walletTransfer($transaction_admin_inp);
 	                    }*/

					  	if(isset($pay_fees->Status) && $pay_fees->Status == 'SUCCEEDED')
						{
							$obj_contest_data = $this->ContestModel->where('id',$arr_payment_details['payment_obj_id'])->first();
							$updated_at = isset($obj_contest_data->updated_at) ? $obj_contest_data->updated_at : NULL;

							$store_mp_wallet_id = $this->ContestModel->where('id',$arr_payment_details['payment_obj_id'])->update(['invoice_id'=>$invoice_id,'payment_status'=>'1','edited_at'=>date('Y-m-d H:i:s'),'updated_at'=>$updated_at]);

        					Session::flash("success",trans('controller_translations.contest_posted_successfully'));
        					Redirect::to($this->payment_success_url)->send();
	                    }
	                    else
	                    {
	                    	Session::flash('error','Error while creating contest.');
                			return redirect($redirect_back_url);	
	                    }
	                }
	                else
	                {
	                	Session::flash('error','Error while creating contest.');
                		return redirect($redirect_back_url);
	                }
              	} 
              	else
              	{
                	Session::flash('error','Error while creating contest.');
                	return redirect($redirect_back_url);
              	}
            }
            else
            {
              	Session::flash('error','Error while creating contest.');
              	return redirect($redirect_back_url);
            }
		}
		else
		{
			Session::flash('error','Error while creating contest.');
            return redirect($redirect_back_url);
		}
	}


	public function update_contest_payment($arr_payment_details)
	{
		$total_cost     = isset($arr_payment_details['total_cost']) ? $arr_payment_details['total_cost'] : '';
		$payment_obj_id = isset($arr_payment_details['payment_obj_id']) ? base64_encode(trim($arr_payment_details['payment_obj_id'])) : '';
		$redirect_back_url = url('/').'/client/dashboard';
        if($payment_obj_id!=''){
            $redirect_back_url = url('/').'/client/contest/update_contest_payment/'.$payment_obj_id.'/'.$total_cost;
        }
     
		$is_winner_choosen = "NO";
		if(empty($arr_payment_details))
		{
			Session::flash('error',trans('controller_translations.text_sorry_somthing_goes_wrong_please_try_again'));
			return redirect($redirect_back_url);
		}

		$is_payment_by_card = isset($arr_payment_details['is_payment_by_card']) ? $arr_payment_details['is_payment_by_card'] : '';
		$card_id            = isset($arr_payment_details['card_id']) ? $arr_payment_details['card_id'] : '';
		$card_currency      = isset($arr_payment_details['card_currency']) ? $arr_payment_details['card_currency'] : 'USD';

		if($is_payment_by_card == '1' && $card_id == '') {
			Session::flash('error','Invalid Card identifier.');
			return redirect($redirect_back_url);
		}

		$arr_transaction    = array();
		$obj_site_settings  = FALSE;
		$invoice_id         = "";

        $winner_details     = []; 
        $winner_expert      = [];

        $amount             = $arr_payment_details['total_cost'];
        $currency_code      = $arr_payment_details['currency_code'];

        $arr_service_charge = get_service_charge($amount,$currency_code);

	    $service_charge = isset($arr_service_charge['service_charge']) ? $arr_service_charge['service_charge'] : 0; 
	    $service_cost   = isset($arr_service_charge['total_amount']) ? $arr_service_charge['total_amount'] : 0;
		
		$arr_payment_details['service_cost'] = (float)$service_charge+ (float) $arr_payment_details['total_cost'];
      	
      	//$apply_double_service_charge = (float)$service_charge + (float)$service_charge+ (float) $arr_payment_details['total_cost'];
		if (isset($arr_payment_details['payment_obj_id']) && isset($arr_payment_details['payment_method'])) 
		{
			$arr_contest_details = [];
			$contest_details = $this->ContestModel->where('id',$arr_payment_details['payment_obj_id'])->first();
			if($contest_details)
			{
				$arr_contest_details = $contest_details->toArray();
			}
			//dd($arr_contest_details['mp_contest_wallet_id']);
			$client_mp_details = $this->UserWalletModel->where('user_id',$arr_payment_details['client_user_id'])
														->where('currency_code',$arr_payment_details['currency_code'])
									                    ->first();
			//dd($client_mp_details);
            if($client_mp_details)
            {
              	$mp_details         = $client_mp_details->toArray();
		  		$invoice_id			= $this->_generate_invoice_id();
		  		$admin_invoice_id	= $this->_generate_invoice_id();
		  		//dd($mp_details);
              	if(isset($mp_details['mp_user_id']) && $mp_details['mp_user_id'] !="")
              	{	
					$project_name  = $arr_payment_details['payment_obj_id'].'-'.$arr_contest_details['contest_title']; 
                	$mangopay_wallet_details = [];
		            $get_wallet_details      = $this->WalletService->get_wallet_details($mp_details['mp_wallet_id']);

		            if(isset($get_wallet_details) && $get_wallet_details!='false' || $get_wallet_details != false)
		            {
		              $mangopay_wallet_details = $get_wallet_details;
		            }

		            $wallet_amount= 0;
		            if(isset($mangopay_wallet_details->Balance->Amount) && $mangopay_wallet_details->Balance->Amount != "")
		            {
		               (float) $wallet_amount= $mangopay_wallet_details->Balance->Amount/100;
		            }

		            if((float)$arr_payment_details['total_cost'] > (float)$wallet_amount)
		            {
		            	$contest_price_minus_balance = (float) $arr_payment_details['total_cost'] - (float)$wallet_amount;
						  
					    $arr_new_service_charge =  get_service_charge($contest_price_minus_balance,$currency_code);

					    $service_charge_payin = isset($arr_new_service_charge['service_charge']) ? $arr_new_service_charge['service_charge'] : 0;

		            	  $arr_data = [];
			              $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)
			                                                ->where('currency_code',$mp_details['currency_code'])
			                                                ->first();
	      
					      if($obj_data)
					      {
					        $arr_data = $obj_data->toArray();
					      }

	            		  //dd($arr_data);
					      if(isset($arr_data) && count($arr_data)>0)
					      {
					      	$transaction_inp['mp_user_id']      = isset($arr_data['mp_user_id'])?$arr_data['mp_user_id']:'';
					        $transaction_inp['mp_wallet_id']    = isset($arr_data['mp_wallet_id'])?$arr_data['mp_wallet_id']:'';
					        $transaction_inp['currency_code']   = $mp_details['currency_code'];

					        $transaction_inp['ReturnURL']       = url('/client/contest/details/'.$arr_payment_details['payment_obj_id'].'/API?mp_user_id='.$arr_data['mp_user_id'].'&mp_wallet_id='.$arr_data['mp_wallet_id'].'&currency_code='.$mp_details['currency_code'].'&payment_obj_id='.$arr_payment_details['payment_obj_id'].'&service_cost='.$arr_payment_details['total_cost'].'&project_name='.$project_name.'&admin_charge='.$service_charge_payin.'&is_contest_update=1');
					      }

					      $amount = isset($arr_new_service_charge['total_amount']) ? $arr_new_service_charge['total_amount'] :0;;
					      $amount = round($amount,2);

						  $transaction_inp['amount']= $amount;

						  if($is_payment_by_card == '1') {

						  		$transaction_inp['card_id']       = $card_id;
							    $transaction_inp['card_currency'] = $card_currency;
							    $arr_payin_wallet_by_card = $this->WalletService->payInWalletByCard($transaction_inp);

							    if(isset($arr_payin_wallet_by_card['status']) && $arr_payin_wallet_by_card['status'] == 'error') {
						          	$msg = isset($arr_payin_wallet_by_card['msg']) ? $arr_payin_wallet_by_card['msg'] : 'Something went, wrong, Unable to process payment, Please try again.';
						          	Session::flash('error',$msg);  
						          	return redirect($redirect_back_url);
						        }
	                                    
	                            if(isset($arr_payin_wallet_by_card['data']['Status']) && $arr_payin_wallet_by_card['data']['Status'] == 'CREATED')
	                            {
	                                Redirect::to($arr_payin_wallet_by_card['data']['ExecutionDetails']->SecureModeRedirectURL)->send();
	                            }
	                            else if(isset($arr_payin_wallet_by_card['data']['Status']) && $arr_payin_wallet_by_card['data']['Status'] == 'SUCCEEDED')
	                            {   
				                	$contest_currency = isset($mp_details['currency_code'])?$mp_details['currency_code']:'USD'; 

			                        $transaction_inp['tag']                      = $invoice_id.'-Contest Service Fee';
				                    $transaction_inp['debited_UserId']           = $mp_details['mp_user_id'];
				                    $transaction_inp['credited_UserId']          = $mp_details['mp_user_id']; 
				                    $transaction_inp['total_pay']                = $arr_payment_details['contest_price'];              
				                    $transaction_inp['debited_walletId']         = (string)$mp_details['mp_wallet_id']; 
				                    $transaction_inp['credited_walletId']        = (string)$arr_contest_details['mp_contest_wallet_id']; 
				                    $transaction_inp['currency_code']            = $contest_currency; 
			 	                    $transaction_inp['cost_website_commission']  = isset($arr_payment_details['contest_services_fee'])?$arr_payment_details['contest_services_fee']:'0';

								  	$pay_fees        	   = $this->WalletService->walletTransfer($transaction_inp);

								  	if(isset($pay_fees->Status) && $pay_fees->Status == 'SUCCEEDED')
									{
										$obj_contest_data = $this->ContestModel->where('id',$arr_payment_details['payment_obj_id'])->first();
										$updated_at = isset($obj_contest_data->updated_at) ? $obj_contest_data->updated_at : NULL;

										$store_mp_wallet_id = $this->ContestModel->where('id',$arr_payment_details['payment_obj_id'])->update(['invoice_id'=>$invoice_id,'payment_status'=>'1','edited_at'=>date('Y-m-d H:i:s'),'updated_at'=>$$updated_at]);

			        					Session::flash("success",trans('controller_translations.contest_posted_successfully'));
			        					return Redirect::to($this->payment_success_url)->send();
				                    }
				                    else
				                    {
				                    	Session::flash('error','Error while creating contest.');
			                			return redirect()->back();	
				                    }

	                            } 
	                            else 
	                            {
	                                Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
	                                return redirect($redirect_back_url);
	                            }
						  }
						  else {

						  		//dd($transaction_inp,$wallet_amount,$service_charge_payin);
								$mp_add_money_in_wallet             = $this->WalletService->payInWallet($transaction_inp);

								/*$arr_admin_details = get_user_wallet_details('1',$currency_code);  
						        if($arr_admin_details)
						        {
						              $transaction_admin_inp['tag']                      = $admin_invoice_id.'-Contest Service Fee';
						              $transaction_admin_inp['debited_UserId']           = $mp_details['mp_user_id'];           
						              $transaction_admin_inp['credited_UserId']          = $arr_admin_details['mp_user_id'];
						              $transaction_admin_inp['total_pay']                = $service_charge_payin;            
						              $transaction_admin_inp['debited_walletId']         = (string)$mp_details['mp_wallet_id']; 
						              $transaction_admin_inp['credited_walletId']        = (string)$arr_admin_details['mp_wallet_id']; 
						              $transaction_admin_inp['cost_website_commission']  = '0';
						              $transaction_admin_inp['currency_code']            = $currency_code;
						              $pay_admin_fees        = $this->WalletService->walletTransfer($transaction_admin_inp);
						        }*/
								//dd($mp_add_money_in_wallet);
								if(isset($mp_add_money_in_wallet->Status) && $mp_add_money_in_wallet->Status == 'CREATED')
								{   
								  	return Redirect::to($mp_add_money_in_wallet->ExecutionDetails->RedirectURL)->send();
								} 
								elseif(isset($mp_add_money_in_wallet->Status) && $mp_add_money_in_wallet->Status == 'FAILED')
								{
									Session::flash('error',$mp_add_money_in_wallet->ResultMessage);
								    return redirect()->back();
								}
								else 
								{
								  	//dd('Not Createddd');
								    if(isset($mp_add_money_in_wallet) && $mp_add_money_in_wallet == false){
								      Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
								      return redirect()->back();
								    }else{
								      Session::flash('error',$mp_add_money_in_wallet);
								      return redirect()->back();
								    }
								}
						  }
		            }

		            //dd('outtttt');
                	// create a project wallet against client mp account
                	$contest_currency = isset($mp_details['currency_code'])?$mp_details['currency_code']:'USD'; 
                	//dd($contest_currency);
                	//$AppWallet     = $this->WalletService->create_wallet_currency_wise($mp_details['mp_user_id'],$project_name.' contest wallet',$contest_currency);

                	//dd($arr_payment_details['service_cost'],$AppWallet);
	                if(isset($arr_contest_details['mp_contest_wallet_id']))
	                {
	                  	// $arr_project_data                           = [];
	                  	// $arr_project_data['mp_contest_wallet_id']   = isset($AppWallet->Id)?$AppWallet->Id:""; 
	                 
	                   //  $store_mp_wallet_id = $this->ContestModel->where('id',$arr_payment_details['payment_obj_id'])->update($arr_project_data); 

                        $transaction_inp['tag']                      = $invoice_id.'-Contest Service Fee';
	                    $transaction_inp['debited_UserId']           = $mp_details['mp_user_id'];

	                    $transaction_inp['credited_UserId']          = $mp_details['mp_user_id']; 

	                    $transaction_inp['total_pay']                = (float)$arr_payment_details['total_cost'];              
	                    $transaction_inp['debited_walletId']         = (string)$mp_details['mp_wallet_id']; 

	                    $transaction_inp['credited_walletId']        = (string)$arr_contest_details['mp_contest_wallet_id']; 
	                    $transaction_inp['currency_code']            = $contest_currency; 

 	                    $transaction_inp['cost_website_commission']  = isset($arr_payment_details['contest_services_fee'])?$arr_payment_details['contest_services_fee']:'0';

					  	$pay_fees        	   = $this->WalletService->walletTransfer($transaction_inp);

 	                    /*$arr_admin_details = get_user_wallet_details('1',$contest_currency);  
 	                    if($arr_admin_details)
 	                    {
 	                    	$transaction_admin_inp['tag']                      = $admin_invoice_id.'-Contest Service Fee';
	                    	$transaction_admin_inp['debited_UserId']           = $mp_details['mp_user_id'];           
	                    	$transaction_admin_inp['credited_UserId']          = $arr_admin_details['mp_user_id'];
	                    	$transaction_admin_inp['total_pay']                = $service_charge;              
	                    	$transaction_admin_inp['debited_walletId']         = (string)$mp_details['mp_wallet_id']; 
	                    	$transaction_admin_inp['credited_walletId']        = (string)$arr_admin_details['mp_wallet_id']; 
 	                    	$transaction_admin_inp['cost_website_commission']  = isset($arr_payment_details['contest_services_fee'])?
 	                    															   $arr_payment_details['contest_services_fee']:'0';

					  		$pay_admin_fees        = $this->WalletService->walletTransfer($transaction_admin_inp);
 	                    }*/

					  	if(isset($pay_fees->Status) && $pay_fees->Status == 'SUCCEEDED')
						{
							$contest_price = 0;
							if(isset($arr_contest_details['contest_price'])){
								$contest_price = $arr_contest_details['contest_price'];
							}
                			$contest_price = $contest_price + round($total_cost,2);

                			$obj_contest_data = $this->ContestModel->where('id',$arr_payment_details['payment_obj_id'])->first();
							$updated_at = isset($obj_contest_data->updated_at) ? $obj_contest_data->updated_at : NULL;

							$store_mp_wallet_id = $this->ContestModel->where('id',$arr_payment_details['payment_obj_id'])->update(['invoice_id'=>$invoice_id,'payment_status'=>'1','contest_price'=>$contest_price,'edited_at'=>date('Y-m-d H:i:s'),'updated_at' => $updated_at]);
							Session::forget("error");
        					Session::flash("success",trans('controller_translations.contest_posted_successfully'));
        					return Redirect::to($this->posted_contest_url)->send();
	                    }
	                    else
	                    {
	                    	Session::flash('error','Error while creating contest.');
                			return redirect()->back();	
	                    }
	                }
	                else
	                {
	                	Session::flash('error','Error while creating contest.');
                		return redirect()->back();
	                }
              	} 
              	else
              	{
                	Session::flash('error','Error while creating contest.');
                	return redirect()->back();
              	}
            }
            else
            {
              	Session::flash('error','Error while creating contest.');
              	return redirect()->back();
            }
		}
		else
		{
			Session::flash('error','Error while creating contest.');
            return redirect()->back();
		}
	}

	public function send_to_redirect_url($url)
	{
		//dd($url);
		return redirect($url);
	}

    private function _generate_invoice_id()
    {
   		$secure      = TRUE;    
        $bytes       = openssl_random_pseudo_bytes(3, $secure);
        $order_token = 'INV'.date('Ymd').strtoupper(bin2hex($bytes));
        return $order_token;
    }
}	
?>