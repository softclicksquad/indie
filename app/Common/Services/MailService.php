<?php
namespace App\Common\Services;
use Illuminate\Http\Request;
use Session;
use Redirect;
use Mail;
use App\Models\SubscriptionUsersModel; 
use App\Models\MilestonesModel;
use App\Models\UserModel; 
use App\Models\MilestoneReleaseModel; 
use App\Models\ProjectsBidsTopupModel;
use App\Models\ProjectpostModel;
use App\Models\NotificationsModel;
use App\Models\EmailTemplateModel;

use Lang;

class MailService
{

	/*
		Auther : Sagar sainkar
		Comment: service for all payment releatd mails like subscription , milestone and milestone release and topup bid and post project payment
	*/


	public function __construct()
	{


		if(!Session::has('locale'))
        {
           Session::put('locale', \Config::get('app.locale'));
        }
        app()->setLocale(Session::get('locale'));
        view()->share('selected_lang',\Session::get('locale'));
        
		 $this->SubscriptionUsersModel 	= new SubscriptionUsersModel();
		 $this->MilestonesModel    		= new MilestonesModel();
		 $this->MilestoneReleaseModel 	= new MilestoneReleaseModel();
		 $this->ProjectsBidsTopupModel  = new ProjectsBidsTopupModel();
		 $this->ProjectpostModel	 	= new ProjectpostModel();
		 $this->UserModel 				= new UserModel();
		 $this->NotificationsModel 		= new NotificationsModel();
		 $this->EmailTemplateModel 		= new EmailTemplateModel();

	}

	public function get_email_template($email_template_id = false)
	{
		$arr_template_data = [];

		$obj_email_template = EmailTemplateModel::where('id',$email_template_id)->first();

		if($obj_email_template)
		{
			$arr_template_data = $obj_email_template->toArray();
		}

		return $arr_template_data;
	}

	public function send_mail($arr_email,$content)
	{

        $from_email     = isset($arr_email['from_mail']) ? $arr_email['from_mail'] : '';
        $project_name   = config('app.project.name');

        try{
                $send_mail = Mail::send(array(), array(),function($message) use($arr_email,$content,$from_email,$project_name)
            	{
            		$message->from($from_email,$project_name);
            		$message->to($arr_email['email_id'],isset($arr_email['name'])?$arr_email['name']:'')
            		->subject($arr_email['subject'])
            		->setBody($content, 'text/html');
            	});

                
            if(count(Mail::failures()) > 0){
                    return false;
                }
                else
                {
                  return true;
                }
            }    

        catch(\Exception $e){
            dd($e);
        }


    	return $send_mail;
    }

	public function subscriptionMail($invoice_id)
	{
		if ($invoice_id) 
		{
			$arr_subscription = array();
			$arr_user_details = array();

    		$obj_subscription = $this->SubscriptionUsersModel->where('invoice_id',$invoice_id)->with(['transaction_details'])->first();

            if($obj_subscription)
            {
             	$arr_subscription = $obj_subscription->toArray();

             	if (isset($arr_subscription['user_id'])) 
             	{
             		$arr_user_details = $this->getExpertDetails($arr_subscription['user_id']);
             	}

             	$email_to = isset($arr_user_details['email'])?$arr_user_details['email']:'';
	            $data['name'] = isset($arr_user_details['expert_details']['first_name'])?ucfirst($arr_user_details['expert_details']['first_name']):'';
	            $data['email_to'] = isset($arr_user_details['email'])?$arr_user_details['email']:'';
	            $data['pack_name'] = isset($arr_subscription['pack_name'])?$arr_subscription['pack_name']:'';
	            $data['pack_price'] = isset($arr_subscription['pack_price'])?'$'.number_format($arr_subscription['pack_price'],2):'';

            	$project_name = config('app.project.name');
            	$mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';

            	if ($email_to!="")
            	{
            		
	                try{
		                Mail::send('front.email.subscription', $data, function ($message) use ($email_to,$mail_form,$project_name) 
		            	{
		                      $message->from($mail_form, $project_name);
		                      $message->subject($project_name.' : Membership Subscription.');
		                      $message->to($email_to);
		                });
		            }
		            catch(\Exception $e){
		            Session::Flash('error'   , 'E-Mail not sent');
		            }
            	}
            	
         	}
		}
		
	}


	public function getExpertDetails($user_id)
	{
		$arr_user = array();

		$obj_user = $this->UserModel->where('id',$user_id)->first();
		if ($obj_user && $obj_user!=FALSE) 
		{
			$obj_user->load(['expert_details']);
			$arr_user = $obj_user->toArray();
			return $arr_user;
		}

		return false;
	}

	public function send_user_registration_verification_email($arr_data = array())
    {
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']      = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            $arr_email['name']          = isset($arr_data['name'])?$arr_data['name']:'';
            
            $arr_email['confirmation_link'] = isset($arr_data['confirmation_link'])?$arr_data['confirmation_link']:'';

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('1');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';
                $content = str_replace("##USERNAME##",$arr_data['name'], $content);
                $content = str_replace("##CONFIRMLINK##",$arr_data['confirmation_link'], $content);
                
                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']= isset($subject)? $subject:'';

                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
         
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_user_forget_password_email($arr_data = array())
    {
    	//dd($arr_data);
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
        	//dd('here');
            $arr_email['email_id']      = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            $arr_email['name']          = isset($arr_data['name'])?$arr_data['name']:'';
            
            $arr_email['reminder_url'] = isset($arr_data['reminder_url'])?$arr_data['reminder_url']:'';

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('3');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';
                $content = str_replace("##USERNAME##",$arr_data['name'], $content);
                $content = str_replace("##REPLY##",$arr_data['reminder_url'], $content);
                
                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']= isset($subject)? $subject:'';

                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
         		
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_applozic_fallback_email($arr_data = array())
    {
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']      = isset($arr_data['to_email'])?$arr_data['to_email']:'';
            $arr_email['name']          = isset($arr_data['name'])?$arr_data['name']:'';
            $arr_email['url']           = isset($arr_data['url'])?$arr_data['url']:'';

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('4');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';
                $content = str_replace("##USERNAME##",$arr_data['to'], $content);
                $content = str_replace("##FROMUSER##",$arr_data['from'], $content);
                $content = str_replace("##HOMEURL##",$arr_data['url'], $content);
                
                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']= isset($subject)? $subject:'';

                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
         		
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_expiry_subscription_notification_email($arr_data = array())
	{
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']      = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            $arr_email['expert_name']   = isset($arr_data['expert_name'])?$arr_data['expert_name']:'';
            $arr_email['url']           = isset($arr_data['url'])?$arr_data['url']:'';

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('5');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';
                
                $content = str_replace("##EXPERT_NAME##",$arr_data['expert_name'], $content);
                $content = str_replace("##EXPIRE_DATE##",$arr_data['expire_date'], $content);
                $content = str_replace("##PACK_NAME##",$arr_data['pack_name'], $content);
                
                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']= isset($subject)? $subject:'';

                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
         		
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_expiry_subscription_warning_balance_email($arr_data = array())
    {
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']      = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            $arr_email['expert_name']   = isset($arr_data['expert_name'])?$arr_data['expert_name']:'';
            $arr_email['url']           = isset($arr_data['url'])?$arr_data['url']:'';

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('37');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';
                
                $content = str_replace("##EXPERT_NAME##",$arr_data['expert_name'], $content);
                $content = str_replace("##EXPIRE_DATE##",$arr_data['expire_date'], $content);
                $content = str_replace("##PACK_NAME##",$arr_data['pack_name'], $content);
                
                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']= isset($subject)? $subject:'';

                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }    


    public function send_change_email_confirmation_email($arr_data = array())
    {
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']      = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            $arr_email['name']   = isset($arr_data['name'])?$arr_data['name']:'';
            $arr_email['url']           = isset($arr_data['url'])?$arr_data['url']:'';

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('6');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';
                
                $content = str_replace("##USERNAME##",$arr_data['name'], $content);
                $content = str_replace("##CONFIRMATION_LINK##",$arr_data['confirmation_link'], $content);
                
                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']= isset($subject)? $subject:'';

                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
         		
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }


    public function send_dispute_added_by_client_to_admin_email($arr_data = array())
    {
    	$arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('7');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';
                
                $content = str_replace("##ADMIN_NAME##",$arr_data['admin_name'], $content);
                $content = str_replace("##CLIENT_NAME##",$arr_data['client_username'], $content);
                $content = str_replace("##EXPERT_NAME##",$arr_data['expert_name'], $content);
                $content = str_replace("##PROJECT_NAME##",$arr_data['project_name'], $content);
                $content = str_replace("##LOGIN_URL##",$arr_data['admin_login_url'], $content);
                
                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']= isset($subject)? $subject:'';

                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
         		
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_dispute_added_by_client_to_expert_email($arr_data = array())
    {
    	$arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('8');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';
                
                $content = str_replace("##EXPERT_NAME##",$arr_data['expert_username'], $content);
                $content = str_replace("##CLIENT_NAME##",$arr_data['client_username'], $content);
                $content = str_replace("##PROJECT_NAME##",$arr_data['project_name'], $content);
                $content = str_replace("##LOGIN_URL##",$arr_data['login_url'], $content);
                
                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']= isset($subject)? $subject:'';

                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
         		
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }


    public function send_dispute_added_by_expert_to_admin_email($arr_data = array())
    {
    	$arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('9');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';
                
                

                $content = str_replace("##ADMIN_NAME##",$arr_data['admin_name'], $content);
                $content = str_replace("##EXPERT_NAME##",$arr_data['expert_name'], $content);
                $content = str_replace("##CLIENT_NAME##",$arr_data['client_name'], $content);
                $content = str_replace("##PROJECT_NAME##",$arr_data['project_name'], $content);
                $content = str_replace("##ADMIN_LOGIN_URL##",$arr_data['admin_login_url'], $content);

                
                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']= isset($subject)? $subject:'';

                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
         		
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_dispute_added_by_expert_to_client_email($arr_data = array())
    {
    	$arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('10');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';

                $content = str_replace("##ADMIN_NAME##",$arr_data['admin_name'], $content);
                $content = str_replace("##EXPERT_USERNAME##",$arr_data['expert_username'], $content);
                $content = str_replace("##CLIENT_NAME##",$arr_data['client_name'], $content);
                $content = str_replace("##PROJECT_NAME##",$arr_data['project_name'], $content);
                $content = str_replace("##LOGIN_URL##",$arr_data['login_url'], $content);

                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_expire_contest_email($arr_data=array())
    {
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('11');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';

                $content = str_replace("##USERNAME##",$arr_data['name'], $content);
                $content = str_replace("##CONTEST_TITLE##",$arr_data['contest_title'], $content);
                

                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_expire_contest_email_for_client($arr_data=array())
    {
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('36');

            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';

                $content = str_replace("##USERNAME##",$arr_data['name'], $content);
                $content = str_replace("##CONTEST_TITLE##",$arr_data['contest_title'], $content);
                $content = str_replace("##CONTEST_START_DATE##",$arr_data['start_date'], $content);
                $content = str_replace("##CONTEST_END_DATE##",$arr_data['end_date'], $content);

                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_file_upload_contest_email($arr_data=array())
    {
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('12');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';

                $content = str_replace("##USERNAME##",$arr_data['name'], $content);
                $content = str_replace("##CONTEST_TITLE##",$arr_data['contest_title'], $content);
                

                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_file_upload_project_email($arr_data=array())
    {
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('13');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';

                $content = str_replace("##USERNAME##",$arr_data['name'], $content);
                $content = str_replace("##PROJECT_NAME##",$arr_data['project_name'], $content);
                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_invite_expert_email($arr_data=array())
    {
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('14');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';

                $content = str_replace("##EXPERT_NAME##",$arr_data['expert_name'], $content);
                $content = str_replace("##UNIQUE_ID##",$arr_data['unique_id'], $content);
                $content = str_replace("##INVITE_MESSAGE##",$arr_data['invite_message'], $content);
                $content = str_replace("##LINK##",$arr_data['url'], $content);

                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }
    
    public function send_milestone_payment_email($arr_data=array())
    {
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('17');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';

                $content = str_replace("##USERNAME##",$arr_data['expert_name'], $content);
                $content = str_replace("##PROJECT_TITLE##",$arr_data['project_title'], $content);
                $content = str_replace("##MILESTONE_TITLE##",$arr_data['milestone_title'], $content);
                $content = str_replace("##CURRENCY##",$arr_data['currency'], $content);
                $content = str_replace("##FINAL_AMOUNT##",$arr_data['final_amount'], $content);
                $content = str_replace("##PAYMENT_STATUS##",$arr_data['payment_status'], $content);
                $content = str_replace("##LOGIN_URL##",$arr_data['login_url'], $content);

                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_milestone_payment_informing_mail_to_expert_email($arr_data=array())
    {
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('18');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';
                $content = str_replace("##EXPERT_NAME##",$arr_data['expert_name'], $content);
                $content = str_replace("##PROJECT_TITLE##",$arr_data['project_title'], $content);
                $content = str_replace("##MILESTONE_TITLE##",$arr_data['milestone_title'], $content);
                $content = str_replace("##CURRENCY##",$arr_data['currency'], $content);
                $content = str_replace("##FINAL_AMOUNT##",$arr_data['final_amount'], $content);
                $content = str_replace("##PAYMENT_STATUS##",$arr_data['payment_status'], $content);
                $content = str_replace("##LOGIN_URL##",$arr_data['login_url'], $content);

                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_milestone_refund_by_client_to_admin_email($arr_data=array())
    {
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('19');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';


                $content = str_replace("##ADMIN_NAME##",$arr_data['admin_name'], $content);
                $content = str_replace("##PROJECT_NAME##",$arr_data['project_name'], $content);
                $content = str_replace("##CLIENT_NAME##",$arr_data['client_name'], $content);
                $content = str_replace("##EXPERT_NAME##",$arr_data['expert_name'], $content);
                $content = str_replace("##MILESTONE_TITLE##",$arr_data['milestone_title'], $content);
                $content = str_replace("##MILESTONE_AMOUNT##",$arr_data['milestone_amount'], $content);
                $content = str_replace("##LOGIN_URL##",$arr_data['login_url'], $content);

                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_milestone_release_payment_to_expert_email($arr_data=array())
    {
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('20');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';
                $content = str_replace("##USERNAME##",$arr_data['name'], $content);
                $content = str_replace("##PROJECT_TITLE##",$arr_data['project_title'], $content);
                $content = str_replace("##MILESTONE_TITLE##",$arr_data['milestone_title'], $content);
                $content = str_replace("##MILESTONE_PRICE##",$arr_data['milestone_price'], $content);
                $content = str_replace("##COST_WEBSITE_COMMISSION##",$arr_data['cost_website_commission'], $content);
                $content = str_replace("##COST_TO_EXPERT##",$arr_data['cost_to_expert'], $content);
                $content = str_replace("##PAYMENT_STATUS##",$arr_data['payment_status'], $content);
                $content = str_replace("##LOGIN_URL##",$arr_data['login_url'], $content);
                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_milestone_release_request_by_expert_to_client_email($arr_data=array())
    {
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('21');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';

                $content = str_replace("##CLIENT_NAME##",$arr_data['client_name'], $content);
                $content = str_replace("##PROJECT_NAME##",$arr_data['project_name'], $content);
                $content = str_replace("##EXPERT_USERNAME##",$arr_data['expert_username'], $content);
                $content = str_replace("##MILESTONE_TITLE##",$arr_data['milestone_title'], $content);
                $content = str_replace("##PROJECT_CURRENCY##",$arr_data['project_currency'], $content);
                $content = str_replace("##MILESTONE_AMOUNT##",$arr_data['milestone_amount'], $content);
               
                $content = str_replace("##LOGIN_URL##",$arr_data['login_url'], $content);
                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_milestone_release_requested_rejected_by_client_email($arr_data=array())
    {
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('22');
            //dd($arr_email_template);           
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';
                $content = str_replace("##EXPERT_NAME##",$arr_data['expert_name'], $content);
                $content = str_replace("##PROJECT_NAME##",$arr_data['project_name'], $content);
                $content = str_replace("##CLIENT_USERNAME##",$arr_data['client_username'], $content);
                $content = str_replace("##MILESTONE_TITLE##",$arr_data['milestone_title'], $content);
                $content = str_replace("##PROJECT_CURRENCY##",$arr_data['project_currency'], $content);
                $content = str_replace("##MILESTONE_AMOUNT##",$arr_data['milestone_amount'], $content);
                $content = str_replace("##LOGIN_URL##",$arr_data['login_url'], $content);
                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                dd($send_mail);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_new_contact_enquiry_email($arr_data=array())
    {
        //dd($arr_data);
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('23');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';

                $content = str_replace("##USERNAME##",$arr_data['name'], $content);
                $content = str_replace("##EMAIL##",$arr_data['email'], $content);
                $content = str_replace("##CONTACT_NUMBER##",$arr_data['contact_number'], $content);
                $content = str_replace("##ENQUIRY_SUBJECT##",$arr_data['enquiry_subject'], $content);
                $content = str_replace("##ENQUIRY_MESSAGE##",$arr_data['enquiry_message'], $content);
                
                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }
    
    public function send_new_project_awarded_to_expert_email($arr_data=array())
    {
        //dd($arr_data);
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('24');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';

                $content = str_replace("##EXPERT_NAME##",$arr_data['expert_name'], $content);
                $content = str_replace("##PROJECT_NAME##",$arr_data['project_name'], $content);
                $content = str_replace("##CLIENT_USERNAME##",$arr_data['client_username'], $content);
                $content = str_replace("##PROJECT_DESCRIPTION##",$arr_data['project_description'], $content);
                $content = str_replace("##LOGIN_URL##",$arr_data['login_url'], $content);

                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_new_project_bid_information_mail_to_client_email($arr_data=array())
    {
        //dd($arr_data);
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('25');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';

                $content = str_replace("##CLIENT_NAME##",$arr_data['client_name'], $content);
                $content = str_replace("##PROJECT_NAME##",$arr_data['project_name'], $content);
                $content = str_replace("##EXPERT_USERNAME##",$arr_data['expert_username'], $content);
                $content = str_replace("##PROJECT_CURRENCY##",$arr_data['project_currency'], $content);
                $content = str_replace("##LOGIN_URL##",$arr_data['login_url'], $content);               
                $content = str_replace("##BID_COST##",$arr_data['bid_cost'], $content);
                $content = str_replace("##BID_ESTIMATED_DURATION##",$arr_data['bid_estimated_duration'], $content);
                $content = str_replace("##BID_DESCRIPTION##",$arr_data['bid_description'], $content);

                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_new_project_bid_information_mail_to_project_manager_email($arr_data=array())
    {
        //dd($arr_data);
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('26');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';

                $content = str_replace("##PROJECT_MANAGER_NAME##",$arr_data['project_manager_name'], $content);
                $content = str_replace("##PROJECT_NAME##",$arr_data['project_name'], $content);
                $content = str_replace("##EXPERT_USERNAME##",$arr_data['expert_username'], $content);
                $content = str_replace("##PROJECT_CURRENCY##",$arr_data['project_currency'], $content);
                $content = str_replace("##LOGIN_URL##",$arr_data['login_url'], $content);               
                $content = str_replace("##BID_COST##",$arr_data['bid_cost'], $content);
                $content = str_replace("##BID_ESTIMATED_DURATION##",$arr_data['bid_estimated_duration'], $content);
                $content = str_replace("##BID_DESCRIPTION##",$arr_data['bid_description'], $content);

                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }   

    public function send_newsletter_subscribe_email($arr_data=array())
    {
        //dd($arr_data);
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';
            

            $email_subject  = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('27');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';

                $content = str_replace("##USERNAME##",$arr_data['username'], $content);
                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_project_accepted_by_expert_mail_to_client_email($arr_data=array())
    {
        //dd($arr_data);
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';

            $email_subject      = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('28');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';
                $content = str_replace("##CLIENT_NAME##",$arr_data['client_name'], $content);
                $content = str_replace("##PROJECT_NAME##",$arr_data['project_name'], $content);
                $content = str_replace("##PROJECT_DESCRIPTION##",$arr_data['project_description'], $content);
                $content = str_replace("##EXPERT_USERNAME##",$arr_data['expert_username'], $content);
                $content = str_replace("##LOGIN_URL##",$arr_data['login_url'], $content);
                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_project_assigned_to_project_manager_email($arr_data=array())
    {
        //dd($arr_data);
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';

            $email_subject      = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('29');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';

                $content = str_replace("##PROJECT_MANAGER_NAME##",$arr_data['project_manager_name'], $content);
                $content = str_replace("##PROJECT_NAME##",$arr_data['project_name'], $content);
                $content = str_replace("##PROJECT_DESCRIPTION##",$arr_data['project_description'], $content);
                $content = str_replace("##LOGIN_URL##",$arr_data['login_url'], $content);
                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_project_assigned_to_project_manager_mail_to_client_email($arr_data=array())
    {
        //dd($arr_data);
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';

            $email_subject      = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('30');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';
                $content = str_replace("##CLIENT_NAME##",$arr_data['client_name'], $content);
                $content = str_replace("##PROJECT_NAME##",$arr_data['project_name'], $content);
                $content = str_replace("##PROJECT_DESCRIPTION##",$arr_data['project_description'], $content);
                $content = str_replace("##PROJECT_MANAGER_USERNAME##",$arr_data['project_manager_username'], $content);
                $content = str_replace("##LOGIN_URL##",$arr_data['login_url'], $content);
                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_project_assigned_to_project_recruiter_email($arr_data=array())
    {
        //dd($arr_data);
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';

            $email_subject      = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('32');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';

                $content = str_replace("##PROJECT_MANAGER_NAME##",$arr_data['project_manager_name'], $content);
                $content = str_replace("##PROJECT_NAME##",$arr_data['project_name'], $content);
                $content = str_replace("##PROJECT_DESCRIPTION##",$arr_data['project_description'], $content);
                $content = str_replace("##LOGIN_URL##",$arr_data['login_url'], $content);
                
                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_project_assigned_to_project_recruiter_mail_to_client_email($arr_data=array())
    {
        //dd($arr_data);
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';

            $email_subject      = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('33');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';

                $content = str_replace("##CLIENT_NAME##",$arr_data['project_manager_name'], $content);
                $content = str_replace("##PROJECT_NAME##",$arr_data['project_name'], $content);
                $content = str_replace("##PROJECT_DESCRIPTION##",$arr_data['project_description'], $content);
                $content = str_replace("##PROJECT_MANAGER_USERNAME##",$arr_data['project_description'], $content);
                $content = str_replace("##LOGIN_URL##",$arr_data['login_url'], $content);
                
                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }

    public function send_project_bid_suggested_mail_to_client_email($arr_data=array())
    {
        //dd($arr_data);
        $arr_email = [];
        if(isset($arr_data['email_id']))
        {
            $arr_email['email_id']  = isset($arr_data['email_id'])?$arr_data['email_id']:'';

            $email_subject      = config('app.project.name').' : '.'Welcome to '.config('app.project.name');

            $arr_email_template = [];

            $arr_email_template = $this->get_email_template('34');
                        
            if($arr_email_template)
            {           
                $subject = isset($arr_email_template['template_subject']) ? $arr_email_template['template_subject'] : '';
                $content = isset($arr_email_template['template_html']) ? $arr_email_template['template_html'] : '';

                $content = str_replace("##CLIENT_NAME##",$arr_data['client_name'], $content);
                $content = str_replace("##PROJECT_NAME##",$arr_data['project_name'], $content);
                $content = str_replace("##PROJECT_DESCRIPTION##",$arr_data['project_description'], $content);
                $content = str_replace("##PROJECT_MANAGER_USERNAME##",$arr_data['project_manager_username'], $content);
                $content = str_replace("##LOGIN_URL##",$arr_data['login_url'], $content);
                
                $content = view('email.general', compact('content'))->render();
                $content = html_entity_decode($content);

                $arr_email['subject']   = isset($subject)? $subject:'';
                $arr_email['from_mail'] =  isset($arr_email_template['template_from_mail']) ? $arr_email_template['template_from_mail'] : '';
                $send_mail = $this->send_mail($arr_email,$content);
                return $send_mail;
            }
            return FALSE;   
        }
        return false;
    }


	public function MilestonePaymentMail($invoice_id)
	{
		if ($invoice_id) 
		{
			$arr_milestone = array();
			$arr_user_details = array();

    		$obj_milestone = $this->MilestonesModel->where('invoice_id',$invoice_id)->with(['transaction_details','project_details'=>	function ($query) {
    								$query->select('id','project_name');
                                   }])->first();

            if($obj_milestone)
            {
             	$arr_milestone = $obj_milestone->toArray();

             	/* get client details */
             	if (isset($arr_milestone['client_user_id'])) 
             	{
             		$arr_user_details = $this->getClientDetails($arr_milestone['client_user_id']);
             	}

             	$email_to_client = isset($arr_user_details['email'])?$arr_user_details['email']:'';

	            $data['name'] = isset($arr_user_details['client_details']['first_name'])?ucfirst($arr_user_details['client_details']['first_name']):'';

	            $data['email_to_client'] = isset($arr_user_details['email'])?$arr_user_details['email']:'';

	            /* client details */

	            /* Create Notification for client */      
	            if(isset($arr_milestone['project_id']) && $arr_milestone['project_id'] != "")
	            {
	                $arr_client_data =  [];

			        $arr_client_data['user_id']    = $arr_user_details['client_details']['user_id'];
			        $arr_client_data['user_type']  = '2';
			        $arr_client_data['url']        = 'client/projects/milestones/'.base64_encode($arr_milestone['project_id']);
			        $arr_client_data['project_id'] = $arr_milestone['project_id'];
			        
			        /*$arr_client_data['notification_text'] =  trans('controller_translations.milestone_created_successfully');*/
			        $arr_client_data['notification_text_en'] =  Lang::get('controller_translations.milestone_created_successfully',[],'en','en');
			        $arr_client_data['notification_text_de'] =  Lang::get('controller_translations.milestone_created_successfully',[],'de','en');

			        $this->NotificationsModel->create($arr_client_data);  
		        }

	            /* get expert details */

             	if (isset($arr_milestone['expert_user_id'])) {
             		$arr_expert_details = $this->getExpertDetails($arr_milestone['expert_user_id']);
             	}

             	/* Create Notification for expert */      

	            if( isset($arr_milestone['project_id']) && $arr_milestone['project_id'] != "")
	            {
	                $arr_expert_data =  [];

			        $arr_expert_data['user_id']    = $arr_expert_details['expert_details']['user_id'];
			        $arr_expert_data['user_type']  = '3';
			        $arr_expert_data['url']        = 'expert/projects/milestones/'.base64_encode($arr_milestone['project_id']);
			        $arr_expert_data['project_id'] = $arr_milestone['project_id'];
			        
			        /*$arr_expert_data['notification_text'] =  trans('controller_translations.milestone_payment_done_by_client');*/
			        
			        $arr_expert_data['notification_text_en'] = Lang::get('controller_translations.milestone_payment_done_by_client',[],'en','en');
			        $arr_expert_data['notification_text_de'] = Lang::get('controller_translations.milestone_payment_done_by_client',[],'de','en');
			        
			        $this->NotificationsModel->create($arr_expert_data);  
		        }


            	$email_to_expert = isset($arr_expert_details['email'])?$arr_expert_details['email']:'';

	            $data['expert_name'] = isset($arr_expert_details['expert_details']['first_name'])?ucfirst($arr_expert_details['expert_details']['first_name']):'';

	            $data['email_to_expert'] = isset($arr_expert_details['email'])?$arr_expert_details['email']:'';

	            /* expert information */

	            $data['project_title'] 		= isset($arr_milestone['project_details']['project_name'])?$arr_milestone['project_details']['project_name']:'';

	            $data['milestone_title'] 	= isset($arr_milestone['title'])?$arr_milestone['title']:'';

	            $data['milestone_price']    = isset($arr_milestone['cost'])?number_format($arr_milestone['cost'],2):'';

	            $data['cost_from_client']   = isset($arr_milestone['cost_from_client'])?'$'.number_format($arr_milestone['cost_from_client'],2):'0';

	            $data['project_manager_commission'] = isset($arr_milestone['cost_project_manager_commission'])?number_format($arr_milestone['cost_project_manager_commission'],2):'0';

	            $data['payment_status'] = isset($arr_milestone['transaction_details']['payment_status'])?$arr_milestone['transaction_details']['payment_status']:'';

	            $data['currency'] = isset($arr_milestone['transaction_details']['currency'])?$arr_milestone['transaction_details']['currency']:'';


	            $data['login_url']   = url('/redirection?redirect=MILESTONE&id='.base64_encode($arr_milestone['project_id']));

            	$project_name = config('app.project.name');
                $final_variable_string ='';
            	$mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
                if(isset($cost_from_client) && isset($milestone_price) && isset($project_manager_commission))
                {
                  if($project_manager_commission<1)
                  {
                     $final_variable_string.= isset($milestone_price)?$milestone_price:'0';
                  }
                  else
                  {
                        $final_variable_string.= isset($cost_from_client)?$cost_from_client:'0';

                        $final_variable_string.= (isset($milestone_price)?$milestone_price:'0' + isset($project_manager_commission)?$project_manager_commission:'0'.'Project Manager Commission');
                  }
                }
                if(isset($payment_status) && $payment_status==0){
                 $payment_status ='Pending';
                }
                elseif(isset($payment_status) && $payment_status==1){
                    $payment_status='Paid';
                }
                elseif(isset($payment_status) && $payment_status==2){
                 $payment_status='Paid';
                }
                elseif(isset($payment_status) && $payment_status==3){
                 $payment_status='Lost';
                }

                $data['final_amount']   = $final_variable_string;
                $data['payment_status'] = $payment_status;

            	if ($email_to_client!="")
            	{
            		
	                try{
                            $mail_status = $this->send_milestone_payment_email($data);
                         //Mail::send('front.email.milestone_payment', $data, function ($message) use ($email_to_client,$mail_form,$project_name) 
            		      //{
		                  // $message->from($mail_form, $project_name);
		                  // $message->subject($project_name.' : Milestone Payment');
		                  // $message->to($email_to_client); 
		                  //});
		            }
		            catch(\Exception $e){
		            Session::Flash('error'   , 'E-Mail not sent');
		            }
            	}

            	/* Milestone payemnt has been done by client send informing mail to email. */

            	$data['website_url'] = url('/').'/login';
            	
            	if ($email_to_expert!="") 
            	{
            		$mail_status = $this->send_milestone_payment_informing_mail_to_expert_email($data);
              //       Mail::send('front.email.milestone_payment_informing_mail_to_expert', $data, function ($message) use ($email_to_expert,$mail_form,$project_name) 
	            	// {
	             //          $message->from($mail_form, $project_name);
	             //          $message->subject($project_name.' : Milestone Payment Done By Client.');
	             //          $message->to($email_to_expert);
	             //    });
            	}	
            	
         	}
		}
		
	}

	public function MilestoneReleasePaymentMail($invoice_id)
	{
		if ($invoice_id) 
		{
			$arr_milestone_request = array();
			$arr_user_details = array();

    		$obj_milestone = $this->MilestoneReleaseModel->where('invoice_id',$invoice_id)->with(['transaction_details','milestone_details','project_details'=>	function ($query) {
    								$query->select('id','project_name');
                                   }])->first();


            if($obj_milestone)
            {
             	$arr_milestone_request = $obj_milestone->toArray();

             	if (isset($arr_milestone_request['expert_user_id'])) 
             	{
             		$arr_user_details = $this->getExpertDetails($arr_milestone_request['expert_user_id']);
             	}

             	$currency = isset($arr_milestone_request['transaction_details']['currency'])?$arr_milestone_request['transaction_details']['currency']:'';

             	$email_to = isset($arr_user_details['email'])?$arr_user_details['email']:'';
                $data['email_id'] = $email_to;
	            $data['name'] = isset($arr_user_details['expert_details']['first_name'])?ucfirst($arr_user_details['expert_details']['first_name']):'';
	            $data['email_to'] = isset($arr_user_details['email'])?$arr_user_details['email']:'';

	            $data['project_title'] = isset($arr_milestone_request['project_details']['project_name'])?$arr_milestone_request['project_details']['project_name']:'';

	            $data['milestone_title'] = isset($arr_milestone_request['milestone_details']['title'])?$arr_milestone_request['milestone_details']['title']:'';

	            $data['milestone_price'] = isset($arr_milestone_request['milestone_details']['cost'])? $currency.' '.number_format($arr_milestone_request['milestone_details']['cost'],2):'';

	            $data['cost_website_commission'] = isset($arr_milestone_request['milestone_details']['cost_website_commission'])? $currency.' '.number_format($arr_milestone_request['milestone_details']['cost_website_commission'],2):'';

	            $data['cost_to_expert'] = isset($arr_milestone_request['milestone_details']['cost_to_expert'])? $currency.' '.number_format($arr_milestone_request['milestone_details']['cost_to_expert'],2):'';

	            
            	$project_name = config('app.project.name');
            	$mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';

            	if(isset($arr_milestone_request['project_details']['id']) &&  $arr_milestone_request['project_details']['id'] != "")
            	{
            		$data['login_url']   = url('/redirection?redirect=MILESTONE&id='.base64_encode($arr_milestone_request['project_details']['id']));
            	}
            	else
            	{
            		$data['login_url']   = url('login');
            	}
                $payment_status = isset($arr_milestone_request['transaction_details']['payment_status'])?
                                        $arr_milestone_request['transaction_details']['payment_status']:'';

                if(isset($payment_status) && $payment_status==0){
                 $payment_status= 'Pending';
                }
                elseif(isset($payment_status) && $payment_status==1){
                 $payment_status='Paid';
                }
                elseif(isset($payment_status) && $payment_status==2){
                 $payment_status='Paid';
                }
                elseif(isset($payment_status) && $payment_status==3){
                 $payment_status='Lost';
                }
                $data['payment_status'] = isset($payment_status)?$payment_status:'';

            	if ($email_to!="")
            	{
            		
	                try{
                          $mail_status = $this->send_milestone_release_payment_to_expert_email($data);

    		             //    Mail::send('front.email.milestone_release_payment_to_expert', $data, function ($message) use ($email_to,$mail_form,$project_name) 
    		            	// {
    		             //          $message->from($mail_form, $project_name);
    		             //          $message->subject($project_name.' : Milestone Released.');
    		             //          $message->to($email_to);
    		             //    });
		            }
		            catch(\Exception $e){
		            Session::Flash('error'   , 'E-Mail not sent');
		            }
            	}
            	
         	}
		}
		
	}

	public function TopupBidMail($invoice_id)
	{
		if ($invoice_id) 
		{
			$arr_topup_bid = array();
			$arr_user_details = array();

    		$obj_topup_bid = $this->ProjectsBidsTopupModel->where('invoice_id',$invoice_id)->first();

            if($obj_topup_bid)
            {
             	$arr_topup_bid = $obj_topup_bid->toArray();

             	if (isset($arr_topup_bid['expert_user_id'])) 
             	{
             		$arr_user_details = $this->getExpertDetails($arr_topup_bid['expert_user_id']);
             	}

             	$email_to = isset($arr_user_details['email'])?$arr_user_details['email']:'';
	            $data['name'] = isset($arr_user_details['expert_details']['first_name'])?ucfirst($arr_user_details['expert_details']['first_name']):'';
	            $data['email_to'] = isset($arr_user_details['email'])?$arr_user_details['email']:'';

	            $data['topup_cost'] = isset($arr_topup_bid['amount'])?'$'.number_format($arr_topup_bid['amount'],2):'';

            	$project_name = config('app.project.name');
            	$mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';

            	if ($email_to!="")
            	{
            		
	                try{
		                Mail::send('front.email.topup_bid_payment', $data, function ($message) use ($email_to,$mail_form,$project_name) 
		            	{
		                      $message->from($mail_form, $project_name);
		                      $message->subject($project_name.' : Topup Bid Purchase.');
		                      $message->to($email_to);
		                });
		            }
		            catch(\Exception $e){
		            Session::Flash('error'   , 'E-Mail not sent');
		            }
            	}
            	
         	}
		}
		
	}

	public function ProjectPaymentMail($invoice_id)
	{

		if ($invoice_id) 
		{
			$arr_project = array();
			$arr_user_details = array();

    		$obj_project = $this->ProjectpostModel->where('invoice_id',$invoice_id)->with(['transaction_details'])->first();

            if($obj_project)
            {
             	$arr_project = $obj_project->toArray();

             	if (isset($arr_project['client_user_id'])) 
             	{
             		$arr_user_details = $this->getClientDetails($arr_project['client_user_id']);
             	}

             	$email_to               = isset($arr_user_details['email'])?$arr_user_details['email']:'';
	            $data['name']           = isset($arr_user_details['client_details']['first_name'])?ucfirst($arr_user_details['client_details']['first_name']):'';
	            $data['email_to']       = isset($arr_user_details['email'])?$arr_user_details['email']:'';
	            $data['project_title']  = isset($arr_project['project_name'])?$arr_project['project_name']:'';
	            $data['payment_cost']   = isset($arr_project['transaction_details']['paymen_amount'])?number_format($arr_project['transaction_details']['paymen_amount'],2):'';
	            $data['payment_status'] = isset($arr_project['transaction_details']['payment_status'])?$arr_project['transaction_details']['payment_status']:'';
            	$project_name           = config('app.project.name');
            	$mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';

            	$data['login_url']      = url('/login');

            	if ($email_to!="")
            	{
	                try{
		                Mail::send('front.email.project_payment', $data, function ($message) use ($email_to,$mail_form,$project_name) 
		            	{
		                      $message->from($mail_form, $project_name);
		                      $message->subject($project_name.' : Initial Project Payment.');
		                      $message->to($email_to);
		                });
		            }
		            catch(\Exception $e){
		            Session::Flash('error'   , 'E-Mail not sent');
		            }
            	}
            	
         	}
		}
		
	}

	public function getClientDetails($user_id)
	{
		$arr_user = array();

		$obj_user = $this->UserModel->where('id',$user_id)->first();
		if ($obj_user && $obj_user!=FALSE) 
		{
			$obj_user->load(['client_details']);
			$arr_user = $obj_user->toArray();
			return $arr_user;
		}

		return false;
	}


	/* Send mail to client that project is suggested by project manager */

	public function email_to_client_on_bid_suggestion_by_recruiter($project_id)
	{
	  $project_name = config('app.project.name');
      //$mail_form    = get_site_email_address();   /* getting email address of admin from helper functions */
	  $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
      $obj_project_info = $this->ProjectpostModel->where('id',$project_id)
                                                  ->with(['client_info'=> function ($query) {
                                                          $query->select('user_id','first_name','last_name');
                                                        },
                                                        'client_details'=>function ($query_nxt) {
                                                          $query_nxt->select('id','email','user_name');
                                                        }])
                                                  ->with(['project_recruiter_info'=> function ($query_nxt_1) {
                                                          $query_nxt_1->select('user_id','first_name','last_name');
                                                        },
                                                        'project_recruiter_details'=>function ($query_nxt_2) {
                                                          $query_nxt_2->select('id','email','user_name');
                                                        }
                                                        ])
                                                  ->first(['id','project_name','client_user_id','expert_user_id','project_description','project_recruiter_user_id']);

      $data = [];

      if($obj_project_info)
      {
        $arr_project_info = $obj_project_info->toArray();

        /* get client name */
        $client_first_name = isset($arr_project_info['client_info']['first_name'])?$arr_project_info['client_info']['first_name']:'';
        
        $client_name = $client_first_name;

        $project_manager_first_name = isset($arr_project_info['project_recruiter_info']['first_name'])?$arr_project_info['project_recruiter_info']['first_name']:'';

        $project_manager_name = $project_manager_first_name;

        $project_manager_username = isset($arr_project_info['project_recruiter_details']['user_name'])?$arr_project_info['project_recruiter_details']['user_name']:'';

        $data['project_name']         = isset($arr_project_info['project_name'])?$arr_project_info['project_name']:'';
        $data['project_description']  = isset($arr_project_info['project_description'])?$arr_project_info['project_description']:'';
        
        $data['client_name']         		  = $client_name;
        $data['project_manager_name']         = $project_manager_name;
        $data['project_manager_username']     = $project_manager_username;
        $data['login_url']                    = url('/redirection?redirect=PROJECT&id='.base64_encode($project_id));
        $data['email_id']            		  = isset($arr_project_info['client_details']['email'])? 
                                                      $arr_project_info['client_details']['email']:'';
                                                      
      }

      $email_to = isset($arr_project_info['client_details']['email'])? $arr_project_info['client_details']['email']:'';

      if($email_to!= "" && isset($arr_project_info['project_recruiter_user_id']) && $arr_project_info['project_recruiter_user_id'] != "" )
      {
        
        try{
            $mail_status = $this->send_project_bid_suggested_mail_to_client_email($data);
         //    Mail::send('front.email.project_bid_suggested_mail_to_client', $data, function ($message) use ($email_to,$mail_form,$project_name) 
        	// {
         //      $message->from($mail_form, $project_name);
         //      $message->subject($project_name.': Project Bid Suggested By Recruiter.');
         //      $message->to($email_to);
         //    });
        }
        catch(\Exception $e)
        {
        	Session::Flash('error','E-Mail not sent');
        }
      }

	}

	/* 
	Comments : Send project completion request mail to client.
	Auther   : Nayan S. 
	*/

	public function project_completion_request($project_id)
	{
		$arr_project_data = [];
		$arr_project_data = $this->get_all_users_details($project_id);

		$data['project_name']         = isset($arr_project_data['project_name'])?$arr_project_data['project_name']:'';
        $data['project_description']  = isset($arr_project_data['project_description'])?$arr_project_data['project_description']:'';

        $data['client_name']         	= isset($arr_project_data['client_first_name'])?$arr_project_data['client_first_name']:'';
        $data['client_username']        = isset($arr_project_data['client_username'])?$arr_project_data['client_username']:'';

        $data['expert_username']        = isset($arr_project_data['expert_username'])?$arr_project_data['expert_username']:'';
        $data['login_url']           	= url('/redirection?redirect=PROJECT&id='.base64_encode($project_id));

        $data['email_to']           	= isset($arr_project_data['client_email'])? $arr_project_data['client_email']:'';
        $data['subject']           		= 'Project Completion Request By Expert';
        $data['email_view_name']        = 'project_completion_request_mail_to_client';
      		
        $this->send_dynamic_mail($data);
        return TRUE;
	}

	/* 
	Comments : Project completed by client / Admin.
	Auther   : Nayan S. 
	*/

	public function project_completed($project_id , $completed_by_admin = FALSE)
	{
		$arr_project_data = [];
		$arr_project_data = $this->get_all_users_details($project_id);

		$data['project_name']         		= isset($arr_project_data['project_name'])?$arr_project_data['project_name']:'';
        $data['project_description']  		= isset($arr_project_data['project_description'])?$arr_project_data['project_description']:'';
		$data['client_name']         		= isset($arr_project_data['client_first_name'])?$arr_project_data['client_first_name']:'';
		$data['expert_name']         		= isset($arr_project_data['expert_first_name'])?$arr_project_data['expert_first_name']:'';
		$data['project_manager_name']   	= isset($arr_project_data['project_manager_first_name'])?$arr_project_data['project_manager_first_name']:'';
        $data['client_username']        	= isset($arr_project_data['client_username'])?$arr_project_data['client_username']:'';
		$data['expert_username']        	= isset($arr_project_data['expert_username'])?$arr_project_data['expert_username']:'';
        $data['project_manager_username'] 	= isset($arr_project_data['project_manager_username'])?$arr_project_data['project_manager_username']:'';
        $data['project_handle_by'] 	        = isset($arr_project_data['project_handle_by'])?$arr_project_data['project_handle_by']:'';

        $data['login_url']  = url('/redirection?redirect=PROJECT&id='.base64_encode($project_id));

      	$completed_by = 'Client';

      	if($completed_by_admin == TRUE )
      	{
      		$completed_by = 'Admin';
      	}		

      	$data['subject']           		= 'Project Completion Request By '.$completed_by;
	    $data['email_view_name']        = 'project_completed';
	    $data['completed_by']           = $completed_by;

        $client_email           	= isset($arr_project_data['client_email'])? $arr_project_data['client_email']:'';
        $expert_email           	= isset($arr_project_data['expert_email'])? $arr_project_data['expert_email']:'';
        $project_manager_email      = isset($arr_project_data['project_manager_email'])? $arr_project_data['project_manager_email']:'';
        
        /* Mail to client */
        if($client_email != "")
        {
	        $data['email_to']           	= $client_email;
	        $data['user_first_name']        = $data['client_name'];
	        $this->call_dynamic_mail_function($data);
        }

        /* mail to expert */

        if($expert_email != "")
        {
	        $data['email_to']           	= $expert_email;
	        $data['user_first_name']        = $data['expert_name'];
	        $this->call_dynamic_mail_function($data);
        }

        /* mail to project manager */

        if($project_manager_email != "")
        {	
        	$data['email_to']           	= $project_manager_email;
        	$data['user_first_name']        = $data['project_manager_name'];
	        $this->call_dynamic_mail_function($data);
        }

        /* mail to Admin */
        $arr_admin_email = get_admin_email_address();

        if(isset($arr_admin_email['email']) && $arr_admin_email['email'] != "")
        {
        	$data['login_url']  = url('/redirection?redirect=COMPLETED&id='.base64_encode($project_id));

	        $data['email_to']           	= $arr_admin_email['email'];
	        $data['user_first_name']        = 'Admin';
	        $this->call_dynamic_mail_function($data);
        }

        return TRUE;
	}

	/* 
	Comments : Send mail for review to client.
	Auther   : Nayan S. 
	*/

	public function send_review_mail_to_client($arr_data)
	{
		$this->send_review_mail($arr_data ,'CLIENT');
		return TRUE;		
	}		
	/* 
	Comments : Send mail for review to expert.
	Auther   : Nayan S. 
	*/

	public function send_review_mail_to_expert($arr_data)
	{
		$this->send_review_mail($arr_data ,'EXPERT');
        return TRUE;	
	}

	/* 
	Comments : Send review mail.
	Auther   : Nayan S.
	*/

	public function send_review_mail($arr_data , $mail_to = FALSE )
	{
		if(isset($arr_data['project_id']) && $arr_data['project_id'] != "")
		{
			$project_id = base64_decode($arr_data['project_id']);

			$arr_project_data = [];
			$arr_project_data = $this->get_all_users_details($project_id);

			$data['project_name']         	= isset($arr_project_data['project_name'])?$arr_project_data['project_name']:'';
	        $data['login_url']           	= url('/redirection?redirect=PROJECT&id='.base64_encode($project_id));

			$data['time_management_rating'] = isset($arr_data['rating_type_one'])?intval($arr_data['rating_type_one']):0;
			$data['communication_rating']   = isset($arr_data['rating_type_two'])?intval($arr_data['rating_type_two']):0;
			$data['understanding_rating']   = isset($arr_data['rating_type_three'])?intval($arr_data['rating_type_three']):0;
			$data['responsibility_rating']  = isset($arr_data['rating_type_four'])?intval($arr_data['rating_type_four']):0;
			$data['support_rating']         = isset($arr_data['rating_type_five'])?intval($arr_data['rating_type_five']):0;
			
			$all_rating =  $arr_data['rating_type_one'] +  $arr_data['rating_type_two'] +  $arr_data['rating_type_three'] +  $arr_data['rating_type_four'] + $arr_data['rating_type_five'];

            $avg_rating = (float) ($all_rating/5);

			$data['avg_project_rating']         = $avg_rating;			

			if($mail_to == "EXPERT")
			{
				$data['expert_name']        	= isset($arr_project_data['expert_first_name'])?$arr_project_data['expert_first_name']:'';
	            $data['client_username']    	= isset($arr_project_data['client_username'])?$arr_project_data['client_username']:'';
		        $data['email_to']           	= isset($arr_project_data['expert_email'])? $arr_project_data['expert_email']:'';
		        $data['subject']           		= 'Project Review By Client';
		        $data['email_view_name']        = 'project_review_by_client_mail_to_expert';
	      		$this->call_dynamic_mail_function($data);
			}
	      	
	      	if($mail_to == "CLIENT")
			{
				$data['client_name']         	= isset($arr_project_data['client_first_name'])?$arr_project_data['client_first_name']:'';
	            $data['expert_username']        = isset($arr_project_data['expert_username'])?$arr_project_data['expert_username']:'';
		        $data['email_to']           	= isset($arr_project_data['client_email'])? $arr_project_data['client_email']:'';
		        $data['subject']           		= 'Project Review By Expert';
		        $data['email_view_name']        = 'project_review_by_expert_mail_to_client';
	      		$this->call_dynamic_mail_function($data);
			}

	    }

	    return TRUE;
	}


	/* 
	Comments : Function to send multiple mails in one function.
	Auther   : Nayan S. 
	*/

	public function call_dynamic_mail_function($data)
	{
		$this->send_dynamic_mail($data);
		return TRUE;
	}

	/* 
	Comments : Function to send mail by passing variable.
	Auther   : Nayan S. 
	*/

	public function send_dynamic_mail($data)
	{
		$project_name = config('app.project.name');
        //$mail_form    = get_site_email_address();   /* getting email address of admin from helper functions */
		$mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
        $email_to 	  		= $data['email_to'];
        $subject 	  		= $data['subject'];
		$email_view_name  	= $data['email_view_name'];
		
		if($email_to!= "" && $email_view_name != "" && $subject != "" )
        {
        	
            try{
	            Mail::send('front.email.'.$email_view_name, $data, function ($message) use ($email_to,$mail_form,$project_name,$subject) 
	        	{
	              $message->from($mail_form, $project_name);
	              $message->subject($project_name.': '.$subject);
	              $message->to($email_to);
	              //$message->to('nayans@webwingtechnologies.com');
	            });
	        }
	        catch(\Exception $e){
	        Session::Flash('error'   , 'E-Mail not sent');
	        }
        }	
		return TRUE;
	}

	/* 
	Comments : Function to get client expert and project manager details from project id
	Auther   : Nayan S. 
	*/

	public function get_all_users_details($project_id)
	{
		$arr_project =  $arr_project_data = [];

	    $obj_project_info = $this->ProjectpostModel->where('id',$project_id)
		                                          ->with(['client_info'=> function ($query) {
		                                                  $query->select('user_id','first_name','last_name');
		                                                },
		                                                'client_details'=>function ($query_nxt) {
		                                                  $query_nxt->select('id','email','user_name');
		                                                }])
		                                          ->with(['expert_info'=> function ($query_nxt_1) {
		                                                  $query_nxt_1->select('user_id','first_name','last_name');
		                                                },
		                                                'expert_details'=>function ($query_nxt_2) {
		                                                  $query_nxt_2->select('id','email','user_name');
		                                                }
		                                                ])
		                                          ->with(['project_manager_info'=> function ($query_nxt_1) {
		                                                  $query_nxt_1->select('user_id','first_name','last_name');
		                                                },
		                                                'project_manager_details'=>function ($query_nxt_2) {
		                                                  $query_nxt_2->select('id','email','user_name');
		                                                }
		                                                ])
		                                          ->first();

		if($obj_project_info)
		{
			$arr_project = $obj_project_info->toArray();	
			$arr_project_data['project_info_found'] = 'YES';
		} 
		else
		{
			$arr_project_data['project_info_found'] = 'NO';
		}                                         

		$arr_project_data['project_name'] = isset($arr_project['project_name']) ? $arr_project['project_name'] : '';

		$arr_project_data['project_description'] = isset($arr_project['project_description']) ? $arr_project['project_description'] : '';

		$arr_project_data['project_status'] 	 = isset($arr_project['project_status']) ? $arr_project['project_status'] : ''; 

		$arr_project_data['project_handle_by'] 	 = isset($arr_project['project_handle_by']) ? $arr_project['project_handle_by'] : ''; 

		$arr_project_data['client_first_name'] 	= isset($arr_project['client_info']['first_name']) ? $arr_project['client_info']['first_name'] : ''; 

		$arr_project_data['client_last_name'] 	= isset($arr_project['client_info']['last_name']) ? $arr_project['client_info']['last_name'] : ''; 

		$arr_project_data['client_username'] 	= isset($arr_project['client_details']['user_name']) ? $arr_project['client_details']['user_name'] : ''; 

		$arr_project_data['client_email'] 		= isset($arr_project['client_details']['email']) ? $arr_project['client_details']['email'] : ''; 


		$arr_project_data['expert_first_name'] 	= isset($arr_project['expert_info']['first_name']) ? $arr_project['expert_info']['first_name'] : ''; 

		$arr_project_data['expert_last_name'] 	= isset($arr_project['expert_info']['last_name']) ? $arr_project['expert_info']['last_name'] : ''; 

		$arr_project_data['expert_username'] 	= isset($arr_project['expert_details']['user_name']) ? $arr_project['expert_details']['user_name'] : ''; 

		$arr_project_data['expert_email'] 		= isset($arr_project['expert_details']['email']) ? $arr_project['expert_details']['email'] : ''; 


		$arr_project_data['project_manager_first_name'] = isset($arr_project['project_manager_info']['first_name']) ? $arr_project['project_manager_info']['first_name'] : ''; 

		$arr_project_data['project_manager_last_name'] 	= isset($arr_project['project_manager_info']['last_name']) ? $arr_project['project_manager_info']['last_name'] : ''; 

		$arr_project_data['project_manager_username'] 	= isset($arr_project['project_manager_details']['user_name']) ? $arr_project['project_manager_details']['user_name'] : ''; 

		$arr_project_data['project_manager_email'] 		= isset($arr_project['project_manager_details']['email']) ? $arr_project['project_manager_details']['email'] : ''; 

		return $arr_project_data;

	}
}

?>