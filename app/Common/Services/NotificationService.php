<?php 
namespace App\Common\Services;

use Illuminate\Http\Request;

use App\Models\ProjectpostModel; 
use App\Models\NotificationsModel;
use App\Models\UserModel;
use App\Models\ProjectMessagingThreadsModel;

use Cmgmyr\Messenger\Models\Thread as MessangerThreadModel;
use Cmgmyr\Messenger\Models\Message as MessangerMessageModel;
use Cmgmyr\Messenger\Models\Participant as MessangerParticipantModel;


use Messagable;
use Session;
use Sentinel;

class NotificationService
{
	public function __construct(
									ProjectpostModel $property_post,
									MessangerMessageModel $messanger_message,
                  					MessangerParticipantModel $messanger_participant,
                  					MessangerThreadModel $messanger_thread,
                                    ProjectMessagingThreadsModel $project_messaging_threads,
									NotificationsModel $notifications,
									UserModel $user
									
								)
	{


		if(!Session::has('locale'))
        {
           Session::put('locale', \Config::get('app.locale'));
        }
        app()->setLocale(Session::get('locale'));
        view()->share('selected_lang',\Session::get('locale'));

        
		$this->ProjectpostModel 	= $property_post;
		$this->NotificationsModel 	= $notifications;
		$this->UserModel 			= $user;
        $this->MessangerMessageModel        = $messanger_message;
     	$this->MessangerParticipantModel    = $messanger_participant;
      	$this->MessangerThreadModel         = $messanger_thread;
      	$this->ProjectMessagingThreadsModel = $project_messaging_threads;


		$user = Sentinel::check();

		if(!$user)
		{
			//return redirect()->back();
			return redirect('/login');
		}

		$this->user_id = $user->id;

	}

	/*	
		@Comment : This Function gives the information about Notifications for a logged in user.
		@Auther  : Nayan S.
	*/

	public function show_notifications($is_inbox_page = FALSE)
	{	
		/*
		$url = "https://evatr.bff-online.de/evatrRPC?UstId_1=DE111111125&UstId_2=&Firmenname=&Ort=&PLZ=&Strasse=&Druck=";
		$curl = curl_init();
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, array(
		    CURLOPT_RETURNTRANSFER => 1,
		    CURLOPT_URL => $url,
		    //CURLOPT_USERAGENT => 'Codular Sample cURL Request'
		));
		// Send the request & save response to $resp
		$resp = curl_exec($curl);
		// Close request to clear up some resources
		curl_close($curl);
       */

		/*
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,"https://evatr.bff-online.de/evatrRPC?");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,
		            "UstId_1=DE111111125&UstId_2=AB1234567890&Firmenname=
		& Site = zip = road = pressure =");

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$resp = curl_exec ($ch);

		curl_close ($ch);

		$xml = simplexml_load_string($resp);
		$json = json_encode($xml);
		$array = json_decode($json,TRUE);
		dd($resp);
		*/

		/*	
		$client = new XMLRPCClient("http://my.server.com/XMLRPC");
  		print var_export($client->myRpcMethod(0));
  		$client->close();*/
		/*

		if($vatValidation->check('DE', 'DE113298780')) 
		{
			echo '<h1>valid one!</h1>';
			echo 'denomination: ' . $vatValidation->getDenomination(). '<br/>';
			echo 'name: ' . $vatValidation->getName(). '<br/>';
			echo 'address: ' . $vatValidation->getAddress(). '<br/>';
		} else {
			echo '<h1>Invalid VAT</h1>';
		}
		*/

		$user = Sentinel::check();
		$arr_notification = array();
		if($user)
		{
		
		/* for counting number of unread messages by the user */
		$obj_threads = false;
		/*$obj_threads = $this->MessangerParticipantModel->where('user_id',$this->user_id)
													   //->limit(5)
													   ->get(['id','thread_id','user_id','last_read']);*/
		//dd($obj_threads);
		$arr_threads = $arr_msgs_details = $tmp_notifications = []; 

		$total_unread_msgs = 0;

		if($obj_threads)
		{
			$arr_threads = $obj_threads->toArray();
		}

		if(count($arr_threads) > 0)
		{
			foreach ($arr_threads as $key => $thread) 
			{
				if(array_has($thread ,'thread_id') && array_has($thread ,'last_read') && array_has($thread ,'user_id') )
				{
				    $tmp_obj_messages = $this->MessangerMessageModel->where('thread_id','=',$thread['thread_id'])
				  							 ->where('user_id','<>',$thread['user_id'])
				  							 ->where('created_at','>',$thread['last_read'])->limit(5)->get();

					if($tmp_obj_messages)
					{	
						$tmp_arr_messages = $tmp_obj_messages->groupBy('user_id')->toArray();
						if(count($tmp_arr_messages) > 0 )
						{
							foreach ($tmp_arr_messages as $key =>  $value)  
							{	
								$tmp_arr_msgs_details = [];
								$tmp_arr_msgs_details['user_id']         = $key;
								$tmp_arr_msgs_details['thread_id']       = $thread['thread_id'];
								$tmp_arr_msgs_details['unread_messages'] = count($value);
								//$tmp_arr_msgs_details['user_info'] = isset($value[0]['role_info'][0]) ? $value[0]['role_info'][0]:[];
								array_push($arr_msgs_details, $tmp_arr_msgs_details);
							}
						} 
					}	
				}
			}


			if(count($arr_msgs_details) > 0 )
			{

				foreach ($arr_msgs_details as $key => $msg) 
				{	
					if(isset($msg['unread_messages']) && $msg['unread_messages'] != "")
					{
						$total_unread_msgs += (int) $msg['unread_messages']; 
					}
				}
			}
		}
		/* ends */

		$arr_notification['unread_messages'] 	 = $total_unread_msgs;
		$arr_notification['thread_info_of_msgs'] = $arr_msgs_details;

		/* Notifications From the notifications table */
		$obj_notifications = $this->NotificationsModel->where('user_id','=',$this->user_id)
													  ->where('is_seen','=','0')
													  // ->limit(5)	
													  ->get();

		if($obj_notifications)
		{
			$tmp_notifications = $obj_notifications->toArray();
		}
		/* ends */

		$arr_notification['notifications'] = $tmp_notifications;

		/* sort notifications by date DESC order */
		$_sortBy = [];

        foreach ($arr_notification['notifications'] as $key => $row)
        {
            $_sortBy[$key] = $row['created_at'];
        }

        array_multisort($_sortBy,SORT_DESC,$arr_notification['notifications']);

		return $arr_notification;

		}

		return $arr_notification;
	}
}

?>