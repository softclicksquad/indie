<?php 

namespace App\Common\Services;
use Crypt;
use Illuminate\Http\Request;

use Session;
use Redirect;
use Sentinel;

use App\Models\TransactionsModel;
use App\Models\MilestonesModel;
use App\Models\MilestoneReleaseModel;

use App\Common\Services\PaymentService;
use App\Common\Services\PaypalService;
use App\Common\Services\StripeService;

use App\Models\ExpertsModel;

class MilestoneReleaseService
{
	public function __construct($expert_id)
	{

		if(!Session::has('locale'))
        {
           Session::put('locale', \Config::get('app.locale'));
        }
        app()->setLocale(Session::get('locale'));
        view()->share('selected_lang',\Session::get('locale'));
		
		 $this->PaypalService = FALSE;
		 $this->StripeService = FALSE;
		 $this->expert_paypal_id = FALSE;

		  if(! $user = Sentinel::check()) 
	      {
	        return redirect('/login');
	      }

      	 $this->user_id = $user->id;
		 $this->TransactionsModel 	  = new TransactionsModel();
		 $this->MilestonesModel		  = new MilestonesModel();
		 $this->MilestoneReleaseModel = new MilestoneReleaseModel();
		 $this->PaymentService		  = new PaymentService();
		 $this->ExpertsModel 		  = new ExpertsModel();

		 $admin_payment_settings = $this->PaymentService->get_admin_payment_settings();

		 if (isset($admin_payment_settings) && sizeof($admin_payment_settings)>0) 
		 {
		 	if (isset($admin_payment_settings['paypal_client_id']) && isset($admin_payment_settings['paypal_secret_key']) && isset($admin_payment_settings['paypal_payment_mode'])) 
		 	{
		 			$this->PaypalService = new PaypalService($admin_payment_settings['paypal_client_id'],$admin_payment_settings['paypal_secret_key'],$admin_payment_settings['paypal_payment_mode']);
		 	}

		 	if (isset($admin_payment_settings['stripe_secret_key']))
		 	{
		 		$this->StripeService = new StripeService($admin_payment_settings['stripe_secret_key']);
		 	}
		 }

		 $obj_expert_paypal = $this->ExpertsModel->where('user_id',$expert_id)->first(['paypal_email']);
		 if ($obj_expert_paypal && isset($obj_expert_paypal->paypal_email)) 
		 {
		 	$this->expert_paypal_id = $obj_expert_paypal->paypal_email;
		 }
	}
	public function payment($arr_payment_details)
	{
		$arr_milestone_details = array();
		$arr_transaction = array();
		$arr_user_subcription = array();
		$invoice_id = "";

		if (isset($arr_payment_details['payment_obj_id']) && isset($arr_payment_details['payment_method'])) 
		{

			/*$arr_payment_details['payment_obj_id'] is Milestone id in this service;*/
			/* Payment method :1-paypal 2-Stripe $arr_payment_details['payment_method'];*/
			
			$obj_milestone = $this->MilestonesModel->where('id',$arr_payment_details['payment_obj_id'])->first();

			if ($obj_milestone!=FALSE) 
			{
				$arr_milestone_details = $obj_milestone->toArray();

				if (sizeof($arr_milestone_details)>0) 
				{
					if (isset($arr_milestone_details['cost_to_expert'])  && isset($arr_milestone_details['project_id']) ) 
					{
					  	$invoice_id	= $this->_generate_invoice_id();

					  	/*First make transaction  */ 
					  	$arr_transaction['user_id'] = $this->user_id;
					  	$arr_transaction['invoice_id'] = $invoice_id;
					  	/*transaction_type is type for transactions 1-Subscription 2-Milestone 3-Release Milestones */
					  	$arr_transaction['transaction_type'] = 3;
					  	$arr_transaction['paymen_amount'] = $arr_milestone_details['cost_to_expert'];
					  	$arr_transaction['payment_method'] = $arr_payment_details['payment_method'];
					  	$arr_transaction['payment_date'] = date('c');

					  	/* get currency for transaction */
					  	$arr_currency = setCurrencyForTransaction($arr_milestone_details['project_id']);

					  	$arr_transaction['currency']      = isset($arr_currency['currency']) ? $arr_currency['currency'] : '$';
					  	$arr_transaction['currency_code'] = isset($arr_currency['currency_code']) ? $arr_currency['currency_code'] : 'USD';
					  	$transaction = $this->TransactionsModel->create($arr_transaction);
					  	if ($transaction) 
					  	{ 	
					  		/*update invoice_id  in milestone release request*/
					  		/*here we have considerd one release request for one milestone so updated on milestone id*/
							$update_status = $this->MilestoneReleaseModel->where('milestone_id',$arr_payment_details['payment_obj_id'])->update(['invoice_id' =>$invoice_id]);
							if ($update_status) 
							{
								/*Now redirect to payment gateway $arr_payment_details[payment_method] is Paypal and Stripe  1-Paypal 2-Stripe */
							  	if (isset($arr_payment_details['payment_method']) && $arr_payment_details['payment_method']==1)
							  	{
							  		 //Paypal
							  		 if(!$this->PaypalService==FALSE && !$this->expert_paypal_id==FALSE)
									 {
									 	//$this->PaypalService->postPayment($invoice_id);
									 	$this->PaypalService->postPayout($invoice_id,$this->expert_paypal_id);
									 }
							  		
							  	}
							}

						}

					}
				}
			}
		}
		
	}



   private function _generate_invoice_id()
   {
   		$secure = TRUE;    
        $bytes = openssl_random_pseudo_bytes(3, $secure);
        $order_token = 'INV'.date('Ymd').strtoupper(bin2hex($bytes));
        return $order_token;
   }
  
}	
?>