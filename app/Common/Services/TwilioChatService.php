<?php 
namespace App\Common\Services;

use Twilio\Rest\Client;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;
use Twilio\Jwt\Grants\SyncGrant;
use Twilio\Jwt\Grants\ChatGrant;

class TwilioChatService
{
	public function __construct()
	{
		/*$this->TWILIO_ACCOUNT_SID       = env('TWILIO_ACCOUNT_SID');
		$this->TWILIO_AUTH_TOKEN       = env('TWILIO_AUTH_TOKEN');
		$this->TWILIO_API_KEY	        = env('TWILIO_API_KEY');
		$this->TWILIO_API_SECRET        = env('TWILIO_API_SECRET');
		$this->TWILIO_CHAT_SERVICE_SID  = env('TWILIO_CHAT_SERVICE_SID');*/

		$this->TWILIO_ACCOUNT_SID      = 'AC93b0980b8fab9de10e27fc89791183f5';
		$this->TWILIO_AUTH_TOKEN       = 'df069bcebb7376181ec54449cbf78052';
		$this->TWILIO_API_KEY          = 'SK812e635b9a6d5e7c37064d87155c2a30';
		$this->TWILIO_API_SECRET       = 'IjtRGitySbAUhcZreR57Ne1x88CQqZoV';
		$this->TWILIO_CHAT_SERVICE_SID = 'IS4cebaef994fd43e98add440959a48929';
		
	}

	public function get_twilio_user_details($arr_data){
		if(isset($arr_data['identity']) == false){
			return NULL;
		}

		try {
				$twilio = new Client($this->TWILIO_ACCOUNT_SID, $this->TWILIO_AUTH_TOKEN);

				return $twilio->chat->v2->services($this->TWILIO_CHAT_SERVICE_SID)
			                            ->users($arr_data['identity'])
			                            ->fetch();
		} catch (\Exception $e) {
			if( $e->getCode() == 20404 ) {
				return $this->create_new_user($arr_data);
			}
			return NULL;
		}		
	}

	public function create_new_user( $arr_data ) {
		try {
				$twilio = new Client($this->TWILIO_ACCOUNT_SID, $this->TWILIO_AUTH_TOKEN);

				return $twilio->chat->v2->services($this->TWILIO_CHAT_SERVICE_SID)
			                            ->users
			                            ->create($arr_data['identity']);
		} catch (\Exception $e) {
			return NULL;
		}
	}

	public function get_channel_member_resource($channel_resource_sid,$identity){

		try {
				$twilio = new Client($this->TWILIO_ACCOUNT_SID, $this->TWILIO_AUTH_TOKEN);

				return $twilio->chat->v2->services($this->TWILIO_CHAT_SERVICE_SID)
							               ->channels($channel_resource_sid)
				                           ->members($identity)
				                           ->fetch();
		} catch (\Exception $e) {
			
			if( $e->getCode() == 20404 ) {
				return $this->create_channel_member_resource($channel_resource_sid,$identity);
			}
			return NULL;
		}		
	}

	public function create_channel_member_resource( $channel_resource_sid,$identity ) {
		try {
				$twilio = new Client($this->TWILIO_ACCOUNT_SID, $this->TWILIO_AUTH_TOKEN);

				return $twilio->chat->v2->services($this->TWILIO_CHAT_SERVICE_SID)
			                            ->channels($channel_resource_sid)
										->members
			                            ->create($identity);
		} catch (\Exception $e) {
			return NULL;
		}
	}

	public function genrate_access_token( $identity ) {

		try {

		    $token = new AccessToken(
				    $this->TWILIO_ACCOUNT_SID,
				    $this->TWILIO_API_KEY,
				    $this->TWILIO_API_SECRET,
				    3600,
				    $identity
				);

		    // Grant access to Video
			$grant = new VideoGrant();
			$token->addGrant($grant);

			// Grant access to Sync
			$syncGrant = new SyncGrant();
			if (empty($this->TWILIO_SYNC_SERVICE_SID)) {
			    $syncGrant->setServiceSid('default');
			} else  {
			    $syncGrant->setServiceSid($this->TWILIO_SYNC_SERVICE_SID);
			}  
			$token->addGrant($syncGrant);

			// Grant access to Chat
			if (!empty($this->TWILIO_CHAT_SERVICE_SID)) {
			    $chatGrant = new ChatGrant();
			    $chatGrant->setServiceSid($this->TWILIO_CHAT_SERVICE_SID);
			    $token->addGrant($chatGrant);
			}

			return $token->toJWT();
		} catch (\Exception $e) {
			return NULL;
		}
	}

	public function get_channel_resource_details_by_sid( $channel_sid ) {

		if($channel_sid == NULL){
			return NULL;
		}

		try {
				$twilio = new Client($this->TWILIO_ACCOUNT_SID, $this->TWILIO_AUTH_TOKEN);

				return $twilio->chat->v2->services($this->TWILIO_CHAT_SERVICE_SID)
			                            ->channels($channel_sid)
			                            ->fetch();

		} catch (\Exception $e) {
			return NULL;
		}
	}


	public function get_channel_resource_details( $arr_channel_details ) {

		$channel_id = NULL;
		$friendly_name = '';
		if(isset($arr_channel_details['is_project_chat']) && $arr_channel_details['is_project_chat'] == '1'){
			$channel_id    = isset($arr_channel_details['project_channel_name']) ? $arr_channel_details['project_channel_name'] : '';
			$friendly_name = isset($arr_channel_details['project_name']) ? $arr_channel_details['project_name'] : '';
		} else if(isset($arr_channel_details['is_contest_chat']) && $arr_channel_details['is_contest_chat'] == '1'){
			$channel_id    = isset($arr_channel_details['contest_channel_name']) ? $arr_channel_details['contest_channel_name'] : '';
			$friendly_name = isset($arr_channel_details['contest_title']) ? $arr_channel_details['contest_title'] : '';
		}

		if($channel_id == NULL){
			return NULL;
		}

		try {
				$twilio = new Client($this->TWILIO_ACCOUNT_SID, $this->TWILIO_AUTH_TOKEN);

				/*$twilio->chat->v2->services($this->TWILIO_CHAT_SERVICE_SID)
			                            ->channels($channel_id)
			                            ->update([ 'friendlyName' => $friendly_name, 'attributes' => json_encode($arr_channel_details) ]);*/

				return $twilio->chat->v2->services($this->TWILIO_CHAT_SERVICE_SID)
			                            ->channels($channel_id)
			                            ->fetch();

		} catch (\Exception $e) {
			if( $e->getCode() == 20404 ) {
				return $this->create_channel_resource($arr_channel_details);
			}
			return NULL;
		}
	}
	
	public function create_channel_resource( $arr_channel_details ) {

		$channel_id = NULL;
		$friendly_name = '';
		if(isset($arr_channel_details['is_project_chat']) && $arr_channel_details['is_project_chat'] == '1'){
			$channel_id    = isset($arr_channel_details['project_channel_name']) ? $arr_channel_details['project_channel_name'] : '';
			$friendly_name = isset($arr_channel_details['project_name']) ? $arr_channel_details['project_name'] : '';
		} else if(isset($arr_channel_details['is_contest_chat']) && $arr_channel_details['is_contest_chat'] == '1'){
			$channel_id    = isset($arr_channel_details['contest_channel_name']) ? $arr_channel_details['contest_channel_name'] : '';
			$friendly_name = isset($arr_channel_details['contest_title']) ? $arr_channel_details['contest_title'] : '';
		}

		if($channel_id == NULL){
			return NULL;
		}

		try {
				$twilio = new Client($this->TWILIO_ACCOUNT_SID, $this->TWILIO_AUTH_TOKEN);

				return $twilio->chat->v2->services($this->TWILIO_CHAT_SERVICE_SID)
			                            ->channels
			                            ->create([ 'uniqueName' => $channel_id, 'friendlyName' => $friendly_name, 'type' => 'private', 'attributes' => json_encode($arr_channel_details) ]);

		} catch (\Exception $e) {
			return NULL;
		}
	}

	public function update_channel_resource( $arr_channel_details ) {

		$channel_id = NULL;
		if(isset($arr_channel_details['is_project_chat']) && $arr_channel_details['is_project_chat'] == '1'){
			$channel_id    = isset($arr_channel_details['project_channel_name']) ? $arr_channel_details['project_channel_name'] : '';
		} else if(isset($arr_channel_details['is_contest_chat']) && $arr_channel_details['is_contest_chat'] == '1'){
			$channel_id    = isset($arr_channel_details['contest_channel_name']) ? $arr_channel_details['contest_channel_name'] : '';
		}

		if($channel_id == NULL){
			return NULL;
		}

		try {
				$twilio = new Client($this->TWILIO_ACCOUNT_SID, $this->TWILIO_AUTH_TOKEN);

				$twilio->chat->v2->services($this->TWILIO_CHAT_SERVICE_SID)
                            				->channels($channel_id)
			                            	->update([ 'type' => 'private', 'attributes' => json_encode($arr_channel_details) ]);
		} catch (\Exception $e) {
			return NULL;
		}
	}

	public function get_video_room_resource_details( $uniqueName ) {

		if($uniqueName == NULL){
			return NULL;
		}

		try {
				$twilio = new Client($this->TWILIO_ACCOUNT_SID, $this->TWILIO_AUTH_TOKEN);

				return $twilio->video->v1->rooms($uniqueName)
			                            ->fetch();


		} catch (\Exception $e) {
			if( $e->getCode() == 20404 ) {
				return $this->create_video_room_resource($uniqueName);
			}
			return NULL;
		}
	}
	
	public function create_video_room_resource( $uniqueName ) {

		if($uniqueName == NULL){
			return NULL;
		}

		try {
				$twilio = new Client($this->TWILIO_ACCOUNT_SID, $this->TWILIO_AUTH_TOKEN);

				return $twilio->video->v1->rooms
                          ->create([
                                       "recordParticipantsOnConnect" => true,
                                       "statusCallback" => url('/twilio-video-callback'),
                                       "type" => "group",
                                       "uniqueName" => $uniqueName
                                   ]
                          );


		} catch (\Exception $e) {
			return NULL;
		}
	}

	public function fetch_message_list( $channel_resource_sid ) {

		try {
				$twilio = new Client($this->TWILIO_ACCOUNT_SID, $this->TWILIO_AUTH_TOKEN);

				return $twilio->chat->v2->services($this->TWILIO_CHAT_SERVICE_SID)
			                            ->channels($channel_resource_sid)
			                            ->messages
			                            ->read();

		} catch (\Exception $e) {
			return NULL;
		}
	}

	public function fetch_channels_list() {

		try {
				$twilio = new Client($this->TWILIO_ACCOUNT_SID, $this->TWILIO_AUTH_TOKEN);

				return $twilio->chat->v2->services($this->TWILIO_CHAT_SERVICE_SID)
										->channels
			                            ->read();
		} catch (\Exception $e) {
			return NULL;
		}
	}

}

?>