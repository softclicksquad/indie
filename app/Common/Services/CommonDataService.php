<?php
namespace App\Common\Services;
use Scrypt;
use DB;
use Mail;
class CommonDataService
{
    public function __construct(){$this->connection         = DB::connection('mysql');}
    /*------------------------------------------------------------------------------
    | Encrypt value
    --------------------------------------------------------------------------------*/
    public function encrypt($value)
    {
        //$encrypted   = encrypt($value);          // laravel Crypt
        //$encrypted   = sha1($value);             // sha1
        $encrypted     = base64_encode($value);    // base64_
        return $encrypted;
    }
    /*------------------------------------------------------------------------------
    | Decrypt value
    --------------------------------------------------------------------------------*/
    public function decrypt($value)
    {
        //$decrypted   = decrypt($value);          // laravel Crypt
        $decrypted     = base64_decode($value);    // base64_
        return $decrypted;
    }

    /*------------------------------------------------------------------------------
    | ip_info 
    echo ip_info("173.252.110.27", "Country"); // United States
    echo ip_info("173.252.110.27", "Country Code"); // US
    echo ip_info("173.252.110.27", "State"); // California
    echo ip_info("173.252.110.27", "City"); // Menlo Park
    echo ip_info("173.252.110.27", "Address"); // Menlo Park, California, United States
    print_r(ip_info("173.252.110.27", "Location")); // Array ( [city] => Menlo Park [state] => California [country] => United States [country_code] => US [continent] => North America [continent_code] => NA )
    --------------------------------------------------------------------------------*/
    function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) 
    {
        //$ip = '81.169.181.179';
        $output = NULL;
        if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) 
        {
            $ip = $_SERVER["REMOTE_ADDR"];
            if ($deep_detect) {
                if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
        }
        $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
        $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
        $continents = array(
            "AF" => "Africa",
            "AN" => "Antarctica",
            "AS" => "Asia",
            "EU" => "Europe",
            "OC" => "Australia (Oceania)",
            "NA" => "North America",
            "SA" => "South America"
        );
        if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
            $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
            if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                switch ($purpose) {
                    case "location":
                        $output                = array(
                        "city"                 => @$ipdat->geoplugin_city,
                        "state"                => @$ipdat->geoplugin_regionName,
                        "country"              => @$ipdat->geoplugin_countryName,
                        "country_code"         => @$ipdat->geoplugin_countryCode,
                        "continent"            => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                        "continent_code"       => @$ipdat->geoplugin_continentCode,
                        "currency_code"        => @$ipdat->geoplugin_currencyCode,
                        "currency_symbol"      => @$ipdat->geoplugin_currencySymbol,
                        "currency_symbol_UTF8" => @$ipdat->geoplugin_currencySymbol_UTF8,
                        "currency_converter"   => @$ipdat->geoplugin_currencyConverter,
                        "inEU"                 => @$ipdat->geoplugin_inEU
                        );
                        break;
                    case "address":
                        $address = array($ipdat->geoplugin_countryName);
                        if (@strlen($ipdat->geoplugin_regionName) >= 1)
                            $address[] = $ipdat->geoplugin_regionName;
                        if (@strlen($ipdat->geoplugin_city) >= 1)
                            $address[] = $ipdat->geoplugin_city;
                        $output = implode(", ", array_reverse($address));
                        break;
                    case "city":
                        $output = @$ipdat->geoplugin_city;
                        break;
                    case "state":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "region":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "country":
                        $output = @$ipdat->geoplugin_countryName;
                        break;
                    case "countrycode":
                        $output = @$ipdat->geoplugin_countryCode;
                        break;
                }
            }
        }
        return $output;
    }
    
}
?>