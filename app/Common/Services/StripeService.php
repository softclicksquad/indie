<?php 

namespace App\Common\Services;
use Illuminate\Http\Request;

use \Stripe\Stripe as Stripe;
use \Stripe\Charge as Charge;

use Validator;
use Session;
use Input;
use DB;
use Auth;
use Mail;
use Redirect;
use Sentinel;

use App\Models\TransactionsModel;
use App\Models\SubscriptionUsersModel;
use App\Models\MilestonesModel;
use App\Models\MilestoneReleaseModel;
use App\Models\ProjectsBidsTopupModel;
use App\Models\ProjectpostModel;

use App\Common\Services\MailService;

use App\Models\ExpertsCategoriesModel;
use App\Models\ExpertsSkillModel;
use App\Models\FavouriteProjectsModel;


class StripeService
{
	
	private $stripe;

    public function __construct($secret_key)    
    {

    	if(!Session::has('locale'))
        {
           Session::put('locale', \Config::get('app.locale'));
        }
        app()->setLocale(Session::get('locale'));
        view()->share('selected_lang',\Session::get('locale'));
    	
        $this->stripe = Stripe::setApiKey($secret_key);

      if(! $user = Sentinel::check()) 
      {
        return redirect('/');
      }
      $this->user_id = $user->id;

      if($user->inRole('admin'))
      {
      	$this->payment_cancel_url = url(config('app.project.admin_panel_slug')."/payment/cancel");
      	$this->payment_success_url = url(config('app.project.admin_panel_slug').'/payment/success');
      }
      else
      {
      	$this->payment_cancel_url = url('/').'/payment/cancel';
      	$this->payment_success_url = url('/').'/payment/success';
      }

      $this->TransactionsModel= new TransactionsModel;
      $this->SubscriptionUsersModel= new SubscriptionUsersModel;
      $this->MilestonesModel= new MilestonesModel;
      $this->MilestoneReleaseModel= new MilestoneReleaseModel;
      $this->ProjectsBidsTopupModel= new ProjectsBidsTopupModel;
      $this->ProjectpostModel = new ProjectpostModel();

      $this->ExpertsCategoriesModel = new ExpertsCategoriesModel();
      $this->ExpertsSkillModel = new ExpertsSkillModel();
      $this->FavouriteProjectsModel = new FavouriteProjectsModel();
      
      $this->MailService = new MailService;
    }


    public function postPayment($invoice_id=null,$cc_details=null)
	{
		$total_cost = 0;
		$responce_invoice_id = false;
		

		if ($invoice_id!=null && $cc_details!=null) 
		{
				
			$transaction_details = $this->TransactionsModel->where('invoice_id',$invoice_id)->first();
	  	
		  	if ($transaction_details) 
		  	{
		  		$transaction_details = $transaction_details->toArray();
		  	}

		  	if (isset($transaction_details) && sizeof($transaction_details)>0 && isset($transaction_details['paymen_amount']) && isset($transaction_details['currency_code'] ) )   
		  	{
		  		$total_cost = round($transaction_details['paymen_amount'] * 100);
		  		

		  		try {
				  	// Use Stripe's library to make requests...

					       $charge_responce  =   Charge::create(array(
					       "card"            =>  $cc_details,
					       "amount"          =>  $total_cost,
					       "currency"        =>  $transaction_details['currency_code'], 								
					       "metadata"        =>  array("invoice_id"   =>  $invoice_id)
					       ));

					if(isset($charge_responce) && sizeof($charge_responce)>0)
				    {
				    	$arr_transaction = array();
				    	if(isset($charge_responce['status']) && $charge_responce['status']=="succeeded")
				    	{
				    		$payment_status = "1";
				    	}
						elseif(isset($charge_responce['status']) && $charge_responce['status']=="pending")
			            {   
			                $payment_status = "0";
			            }
			            else
			            {
			            	$payment_status = "0";	
			            }

			            $responce_invoice_id = $charge_responce->metadata['invoice_id'];

			            $arr_transaction['payment_txn_id']  = $charge_responce->id;
			            $arr_transaction['payment_status']  = $payment_status;
			            $arr_transaction['payment_date']    = date('c');
			            $arr_transaction['response_data']   = json_encode($charge_responce);
			            if ($responce_invoice_id!=false) 
			            {
			            	$obj_transactions = $this->TransactionsModel->where('invoice_id',$responce_invoice_id)->first();

			            	if ($obj_transactions) 
			            	{
			            		$update_status = $obj_transactions->update($arr_transaction);

					            if ($update_status && $update_status!=false) 
					            {
					            	// this payment is for subscription
					            	if (isset($obj_transactions->transaction_type) && $obj_transactions->transaction_type==1) 
					            	{
					            		if (isset($payment_status) && $payment_status==1)  
					            		{
					            			//update all previous subscription status to 0 and then create new subscription
			              					$subscription_update = $this->SubscriptionUsersModel->where('user_id',$this->user_id)->where('is_active',1)->update(['is_active'=>0]);	

			              					$subscription_update_paid = $this->SubscriptionUsersModel->where('user_id',$this->user_id)->where('invoice_id',$responce_invoice_id)->update(['is_active'=>1]);	

			              					$this->remove_subscription_dependency($this->user_id);
											
											Session::flash('payment_success_msg',trans('controller_translations.msg_payment_transaction_for_subscription_is_successfully'));

						            		Session::flash('payment_return_url','/expert/profile');

			              					$this->MailService->subscriptionMail($responce_invoice_id);
					            		}
					            		
					            	}
					            	elseif (isset($obj_transactions->transaction_type) && $obj_transactions->transaction_type==2) 
					            	{
					            		if (isset($payment_status) && $payment_status==1) 
						            	{
						            		$update_milstone = $this->MilestonesModel->where('invoice_id',$responce_invoice_id)->update(['status'=>'1']);

						            		$message = trans('controller_translations.msg_payment_transaction_for_milestone_successfully');

						            		Session::flash('payment_success_msg',$message);

						            		Session::flash('payment_return_url','/client/projects/ongoing');

						            		$this->MailService->MilestonePaymentMail($responce_invoice_id);
						            	}
		            		
		            					
					            	}
					            	elseif (isset($obj_transactions->transaction_type) && $obj_transactions->transaction_type==3) 
					            	{
					            		if (isset($payment_status) && $payment_status==1) 
					            		{
					            			$update_milstone = $this->MilestoneReleaseModel->where('invoice_id',$responce_invoice_id)->update(['status'=>'2']);
					            			$this->MailService->MilestoneReleasePaymentMail($responce_invoice_id);	
					            		}
					            	}
					            	elseif (isset($obj_transactions->transaction_type) && $obj_transactions->transaction_type==4) 
		            				{
					            		if (isset($payment_status) && $payment_status==1) 
					            		{
					            			$update_topup = $this->ProjectsBidsTopupModel->where('invoice_id',$responce_invoice_id)->update(['payment_status'=>'1']);
					            			
					            				Session::flash('payment_success_msg',trans('controller_translations.msg_payment_transaction_for_extra_bid_successfully'));

						        				Session::flash('payment_return_url','/projects');


					            			$this->MailService->TopupBidMail($responce_invoice_id);	
					            		}
					            		
					            	}
					            	elseif (isset($obj_transactions->transaction_type) && $obj_transactions->transaction_type==5) 
		            				{
					            		if (isset($payment_status) && $payment_status==1) 
					            		{
					            			$update_project = $this->ProjectpostModel->where('invoice_id',$responce_invoice_id)->update(['project_status'=>'1']);

					            			Session::flash('payment_success_msg',trans('controller_translations.msg_payment_transaction_for_post_project_is_successfully'));
						            		Session::flash('payment_return_url','/client/projects/posted');

					            			$this->MailService->ProjectPaymentMail($responce_invoice_id);
					            		}
					            		
					            	}

						            Redirect::to($this->payment_success_url)->send();
					            }
				        	}
			        	}
				    }

				} 
				catch(\Stripe\Error\Card $e) 
				{
				  Redirect::to($this->payment_cancel_url)->send();
				} catch (\Stripe\Error\RateLimit $e) {
				  // Too many requests made to the API too quickly
					Redirect::to($this->payment_cancel_url)->send();
				} catch (\Stripe\Error\InvalidRequest $e) {
				  // Invalid parameters were supplied to Stripe's API
					Redirect::to($this->payment_cancel_url)->send();
				} catch (\Stripe\Error\Authentication $e) {
				  // Authentication with Stripe's API failed
				  // (maybe you changed API keys recently)
					Redirect::to($this->payment_cancel_url)->send();
				} catch (\Stripe\Error\ApiConnection $e) {
				  // Network communication with Stripe failed
					Redirect::to($this->payment_cancel_url)->send();
				} catch (\Stripe\Error\Base $e) {
				  // Display a very generic error to the user, and maybe send
				  // yourself an email
					Redirect::to($this->payment_cancel_url)->send();
				} catch (Exception $e) {
				  // Something else happened, completely unrelated to Stripe
					Redirect::to($this->payment_cancel_url)->send();
				}
		  	}
		}
		Redirect::to($this->payment_cancel_url)->send();
	}
	

	 /*
      Auther: sagar sainkar
      Comment : this is function for remove category , skills , and favourite project for subscription
  */
   public function remove_subscription_dependency($expert_user_id=null)
   {
     if (isset($expert_user_id) && $expert_user_id!=null) 
     {
       $delete_category  = $this->ExpertsCategoriesModel->where('expert_user_id',$expert_user_id)->delete();
       $delete_skills    = $this->ExpertsSkillModel->where('expert_user_id',$expert_user_id)->delete();
       $delete_favourite = $this->FavouriteProjectsModel->where('expert_user_id',$expert_user_id)->delete();
     }
   }
}?>