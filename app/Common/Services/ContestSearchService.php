<?php 
namespace App\Common\Services;

use Illuminate\Http\Request;

use App\Models\ContestModel; 
use App\Models\SkillsModel;
use App\Models\ContestSkillsModel;
use App\Models\ProjectsBidsModel;
use Illuminate\Pagination\Paginator;

use DB;
use Session;

class ContestSearchService
{
	public function __construct(ContestModel $contest_post,
								SkillsModel $skill_model,
								ContestSkillsModel $project_skills,
								ProjectsBidsModel $project_bids	
								)
	{
		if(!Session::has('locale'))
        {
           Session::put('locale', \Config::get('app.locale'));
        }
        app()->setLocale(Session::get('locale'));
        view()->share('selected_lang',\Session::get('locale'));
        
		$this->ContestModel 		= $contest_post;
		$this->SkillsModel 			= $skill_model;
		$this->ContestSkillsModel 	= $project_skills;
		$this->ProjectsBidsModel 	= $project_bids;
	}

	public function make_filer_search( $arr_data = [] )
	{
		$search_keys   = [];
    $search_result = '';

    if($arr_data!='')
    {
        $search_result = $this->ContestModel->with(['category_details','sub_category_details'])
                                            ->where('is_active','!=','2')
                                            ->where('contest_end_date','>',date('Y-m-d'))
                                            ->where('payment_status','1');

    /* Category search */
        if(isset($arr_data['cat']) && $arr_data['cat']!='' && sizeof($arr_data['cat'])>0)
        { 
          // foreach($arr_data['cat'] as $key=> $enc_category_id)
          // {
          //  $category_id[$key] = base64_decode($enc_category_id);
          // }
      //$category = $arr_data['cat'];
            $search_result =  $search_result->where('category_id',base64_decode($arr_data['cat']));
        }

   //      if(isset($arr_data['category']) && sizeof($arr_data['category'])>0)
   //      {  
   //       foreach($arr_data['category'] as $key=> $enc_category_id)
   //       {
   //         $category_id[$key] = base64_decode($enc_category_id);
   //       }

      // $category = $arr_data['category'];
   //          $search_result =$search_result->whereIn('category_id',$category_id);
              
   //      }

        if(isset($arr_data['subcategory']) && $arr_data['subcategory']!='' && sizeof($arr_data['subcategory'])>0)
        { 
          foreach($arr_data['subcategory'] as $key=> $enc_sub_category_id)
          {
            $sub_category_id[$key] = base64_decode($enc_sub_category_id);
          }

      $category = $arr_data['subcategory'];
            $search_result =$search_result->whereIn('sub_category_id',$sub_category_id);
              
        }

        /* skills search */
        if(isset($arr_data['skills']) && $arr_data['skills'] !=""){
      $skills = $arr_data['skills'];
            $search_result =$search_result->where('contests.contest_status','0')
            ->where(function ($q) use($skills) 
      {
          $q->whereHas('contest_skills.skill_data.skill_search', function ($query) use($skills) 
        {
          if (isset($skills) && sizeof($skills)){
            foreach ($skills as $key => $skill){
              if($key == 0){
                $query->where('skill_id',base64_decode($skill)); 
              } else {              
                $query->orWhere('skill_id',base64_decode($skill)); 
              }
            }
          }
        });
      }); 
        }
        /* end skills search */

        if(isset($arr_data['min_contest_amt']) && $arr_data['min_contest_amt']!=null && isset($arr_data['max_contest_amt']) && $arr_data['max_contest_amt']!=null)
        {
          $search_result =$search_result->whereBetween('usd_contest_price',[$arr_data['min_contest_amt'],$arr_data['max_contest_amt']]);
        } 
      
        if(Session::has('show_contest_listing_cnt')) { $pagi_cnt = Session::get('show_contest_listing_cnt'); }else { $pagi_cnt = config('app.project.pagi_cnt'); }
        $search_result->with(['skill_details','contest_skills.skill_data','contest_entry']);
                                       if(isset($arr_data['showedby']) && $arr_data['showedby'] == 'ending-soon'){
                                       $search_result->where('contest_status','0');
                                       $search_result->orderBy('contest_end_date','ASC');
                                       } else if(isset($arr_data['showedby']) && $arr_data['showedby'] == 'recently-posted'){
                                       $search_result->where('contest_status','0');
                                       $search_result->orderBy('created_at','DESC');
                                       } 
        $search_result =               $search_result->paginate($pagi_cnt);
    }
        /* end Category search */
		return $search_result;
	}
	/*
	Auther   : Nayan S.
	Comments : function to return contests according to skills matched from browse skills section 
	*/
	public function search_with_skill_id($skill_id)
	{
		$obj_data = FALSE;
		$obj_data = $this->ContestModel->join('clients', function($join)
                        {
                            $join->on('clients.user_id', '=', 'contests.client_user_id');
                        })
                       ->select('contests.*','clients.user_id')
                       ->where('contests.project_status','2')
					   ->whereHas('project_skills.skill_data', function($q) use($skill_id)
						   {
						   	$q->where('id',$skill_id);
						   })	
					   ->with(['project_skills.skill_data','skill_details','client_details','category_details'])->paginate(config('app.project.pagi_cnt'));
		return $obj_data;
	}	
}

?>