<?php 

namespace App\Common\Services;
use Crypt;
use Illuminate\Http\Request;

use Session;
use Redirect;
use Sentinel;

use App\Models\SubscriptionPacksModel;
use App\Models\TransactionsModel;
use App\Models\SubscriptionUsersModel;
use App\Models\ProjectsBidsModel;
use App\Models\ExpertsCategoriesModel;
use App\Models\ProjectpostModel;
use App\Models\ExpertsSkillModel;
use App\Models\ProjectSkillsModel;
use App\Models\ProjectsBidsTopupModel;
use App\Models\FavouriteProjectsModel;
use App\Models\ContestModel;
use App\Models\ContestSkillsModel;
use App\Models\NotificationsModel;
use App\Models\CurrencyModel;

use App\Common\Services\MailService;


use App\Common\Services\PaymentService;
use App\Common\Services\PaypalService;
use App\Common\Services\StripeService;
use App\Common\Services\WalletService;

use Lang;
class SubscriptionService
{
	public function __construct()
	{

        if(!Session::has('locale'))
        {
           Session::put('locale', \Config::get('app.locale'));
        }
        
        app()->setLocale(Session::get('locale'));
        view()->share('selected_lang',\Session::get('locale'));
		    $this->PaypalService = FALSE;
		    $this->StripeService = FALSE;
  		  if(! $user = Sentinel::check()) {
  	        return redirect('/login');
  	    }

        $this->user_id                = $user->id;
        $this->SubscriptionPacksModel = new SubscriptionPacksModel();
        $this->TransactionsModel      = new TransactionsModel();
        $this->SubscriptionUsersModel = new SubscriptionUsersModel();
        $this->ProjectsBidsModel      = new ProjectsBidsModel();
        $this->ExpertsCategoriesModel = new ExpertsCategoriesModel();
        $this->ProjectpostModel       = new ProjectpostModel();
        $this->ExpertsSkillModel      = new ExpertsSkillModel();
        $this->ProjectSkillsModel     = new ProjectSkillsModel();
        $this->ProjectsBidsTopupModel = new ProjectsBidsTopupModel();
        $this->FavouriteProjectsModel = new FavouriteProjectsModel();
        $this->ContestModel           = new ContestModel();
        $this->ContestSkillsModel     = new ContestSkillsModel();
        $this->WalletService          = new WalletService();
        $this->NotificationsModel     = new NotificationsModel();
        $this->PaymentService         = new PaymentService();
        $this->MailService            = new MailService();
        $this->CurrencyModel          = new CurrencyModel();
  		
        $admin_payment_settings          = $this->PaymentService->get_admin_payment_settings();

  		if (isset($admin_payment_settings) && sizeof($admin_payment_settings)>0) {
  		 	if (isset($admin_payment_settings['paypal_client_id']) && isset($admin_payment_settings['paypal_secret_key']) && isset($admin_payment_settings['paypal_payment_mode'])) {
  		 		$this->PaypalService = new PaypalService($admin_payment_settings['paypal_client_id'],$admin_payment_settings['paypal_secret_key'],$admin_payment_settings['paypal_payment_mode']);
  		 	}
  		 	if (isset($admin_payment_settings['stripe_secret_key'])){
  		 		$this->StripeService = new StripeService($admin_payment_settings['stripe_secret_key']);
  		 	}
  		}
        
        if(isset($user)){
            $logged_user                 = $user->toArray();  
            if($logged_user['mp_wallet_created'] == 'Yes'){
                $this->mp_user_id          = $logged_user['mp_user_id'];
                $this->mp_wallet_id        = $logged_user['mp_wallet_id'];
                $this->mp_wallet_created   = $logged_user['mp_wallet_created'];
            } else {
                $this->mp_user_id          = '';
                $this->mp_wallet_id        = '';
                $this->mp_wallet_created   = 'No';
            }
        } else {
            $this->mp_user_id          = '';
            $this->mp_wallet_id        = '';
            $this->mp_wallet_created   = 'No';
        }
        $this->payment_cancel_url = url('/').'/payment/cancel';
        $this->payment_success_url = url('/').'/payment/success';
	}
	public function payment($arr_payment_details)
	{
        // dd($arr_payment_details);
        $arr_pack_details     = array();
        $service_charge_payin = 0;
        $arr_transaction      = array();
        $arr_user_subcription = array();
        $invoice_id           = "";

		if (isset($arr_payment_details['payment_obj_id']) && isset($arr_payment_details['payment_method'])) 
        {
            /*chk_already_bid_purchase */
            $chk_alreadytransaction = $this->SubscriptionUsersModel
                                       ->where('user_id',$this->user_id)
                                       //->where('subscription_pack_id',$arr_payment_details['payment_obj_id'])
                                       ->where('expiry_at','>=',date('Y-m-d H:i:s'))
                                       ->with(['transaction_details'])
                                       ->limit('1')
                                       ->orderBy('id','DESC')
                                       ->first();
            if(isset($chk_alreadytransaction) && $chk_alreadytransaction != null){
                $transaction_done = $chk_alreadytransaction->toArray();
                if(isset($transaction_done['transaction_details']['payment_status']) && 
                   $transaction_done['transaction_details']['payment_status'] == '2' &&
                   $arr_payment_details['payment_obj_id'] == $transaction_done['subscription_pack_id']){
                   Session::flash('payment_error_msg',trans('controller_translations.this_transaction_already_done')); 
                   return redirect()->back();
                }
            }
            
            /* chk_already_bid_purchase */

            /*this is pack id in subscription $arr_payment_details['payment_obj_id'];*/
			/* Payment method :1-paypal 2-Stripe 3-Wallet $arr_payment_details['payment_method'];*/
            
            $obj_pack_details =  $this->SubscriptionPacksModel->where('id',$arr_payment_details['payment_obj_id'])->first();
            
			if($obj_pack_details!=FALSE)
            {
				$arr_pack_details = $obj_pack_details->toArray();
                //dd($arr_payment_details);
				if (sizeof($arr_pack_details)>0)
                {
					if (isset($arr_pack_details['pack_price']))
                    {
                        $invoice_id = $this->_generate_invoice_id();
                        /*First make transaction  */ 
                        $arr_transaction['user_id']          = $this->user_id;
                        $arr_transaction['invoice_id']       = $invoice_id;
                        /*transaction_type is type for transactions 1-Subscription 2-Milestone 3-Release Milestones */
                        $arr_transaction['transaction_type'] = 1;
                        $arr_transaction['paymen_amount']    = $arr_payment_details['total_cost'];
                        $arr_transaction['payment_method']   = $arr_payment_details['payment_method'];
                        $arr_transaction['payment_date']     = date('c');
                        /* get currency for transaction */
                        $arr_currency = setCurrencyForTransaction();
                        $arr_transaction['currency']      = isset($arr_currency['currency']) ? $arr_currency['currency'] : '$';
                        $arr_transaction['currency_code'] = isset($arr_currency['currency_code']) ? $arr_currency['currency_code'] : 'USD';

                        $transaction  = $this->TransactionsModel->create($arr_transaction);
                        
                        if ($transaction) 
                        {
                            /*insert record in user_subcription for selected pack and invoice details*/
                            $arr_user_subcription['user_id']                      = $this->user_id;
                            $arr_user_subcription['subscription_pack_currency']   = $arr_payment_details['currency_code'];
                            $arr_user_subcription['subscription_pack_id']         = isset($arr_pack_details['id'])?$arr_pack_details['id']:'';
                            $arr_user_subcription['invoice_id']                   = $invoice_id;
                            
                            if(isset($arr_payment_details['type']) && $arr_payment_details['type']=='MONTHLY')
                            {
                              $arr_user_subcription['expiry_at']                    = date('Y-m-d',strtotime("+30 days"));
                            }
                            else
                            {
                              $arr_user_subcription['expiry_at']                    = date('Y-m-d',strtotime("+365 days"));  
                            }
                            
                            $arr_user_subcription['pack_name']                    = isset($arr_pack_details['pack_name'])?$arr_pack_details['pack_name']:'';
                            $arr_user_subcription['pack_price']                   = isset($arr_pack_details['pack_price'])?$arr_pack_details['pack_price']:'';
                            $arr_user_subcription['validity']                     = isset($arr_pack_details['validity'])?$arr_pack_details['validity']:'';
                            $arr_user_subcription['number_of_bids']               = isset($arr_pack_details['number_of_bids'])?$arr_pack_details['number_of_bids']:'0';
                            $arr_user_subcription['topup_bid_price']              = isset($arr_pack_details['topup_bid_price'])?$arr_pack_details['topup_bid_price']:'0';
                            $arr_user_subcription['number_of_categories']         = isset($arr_pack_details['number_of_categories'])?$arr_pack_details['number_of_categories']:'0';
                            $arr_user_subcription['number_of_subcategories']         = isset($arr_pack_details['number_of_subcategories'])?$arr_pack_details['number_of_subcategories']:'0';
                            $arr_user_subcription['number_of_skills']             = isset($arr_pack_details['number_of_skills'])?$arr_pack_details['number_of_skills']:'0';
                            $arr_user_subcription['number_of_favorites_projects'] = isset($arr_pack_details['number_of_favorites_projects'])?$arr_pack_details['number_of_favorites_projects']:'0';
                            $arr_user_subcription['website_commision']            = isset($arr_pack_details['website_commision'])?$arr_pack_details['website_commision']:'0';
                            $arr_user_subcription['allow_email']                  = isset($arr_pack_details['allow_email'])?$arr_pack_details['allow_email']:'0';
                            $arr_user_subcription['allow_chat']                   = isset($arr_pack_details['allow_chat'])?$arr_pack_details['allow_chat']:'0';
                            $arr_user_subcription['is_active']                    = 0;
              
                            $subscription = $this->SubscriptionUsersModel->create($arr_user_subcription);
                            
                            if ($subscription) 
                            {
                                //dd('hereeeeee',$arr_payment_details);
						  	    /*Now redirect to payment gateway $arr_payment_details[payment_method] is Paypal and Stripe  1-Paypal 2-Stripe */
						  	    if (isset($arr_payment_details['payment_method']) && $arr_payment_details['payment_method']==1) 
                                {
						  		    //Paypal
						  	        if(!$this->PaypalService==FALSE){
  								 	    $this->PaypalService->postPayment($invoice_id);
  								    }
						  	    } 
                                elseif (isset($arr_payment_details['payment_method']) && $arr_payment_details['payment_method']==2)
                                {
						  		    if(!$this->StripeService==FALSE){
  								 	    if (isset($arr_payment_details['cardNumber']) && isset($arr_payment_details['cardExpiryMonth']) && isset($arr_payment_details['cardExpiryYear'])) 
                                        {
     								 		$card_details = array('number' => $arr_payment_details['cardNumber'], 'exp_month' => $arr_payment_details['cardExpiryMonth'], 'exp_year' => $arr_payment_details['cardExpiryYear']);
  	 							 		    $this->StripeService->postPayment($invoice_id,$card_details);
  								 	    }
								    }
						  	    } 
                                else if (isset($arr_payment_details['payment_method']) && $arr_payment_details['payment_method']==3) 
                                {
                                    $admin_data = get_user_wallet_details('1',$arr_payment_details['currency_code']);
                                    $user_data  = get_user_wallet_details($this->user_id,$arr_payment_details['currency_code']);
                                    //dd($admin_data,$user_data);
                                    if(isset($admin_data) && count($admin_data)>1)
                                    {
                                        // check client wallet balance
                                        $mangopay_wallet_details = [];
                                        $get_wallet_details      = $this->WalletService->get_wallet_details($user_data['mp_wallet_id']);
                                        if(isset($get_wallet_details) && $get_wallet_details!='false' || $get_wallet_details != false){
                                            $mangopay_wallet_details = $get_wallet_details;
                                        }
                                        $wallet_amount= 0;
                                        if(isset($mangopay_wallet_details->Balance->Amount) && $mangopay_wallet_details->Balance->Amount != ""){
                                            $wallet_amount= $mangopay_wallet_details->Balance->Amount/100;
                                        }
                                        
                                        $string_amount = str_replace(",","",$arr_transaction['paymen_amount']);
                                        $amount_int    = (float) $string_amount;
                                        //dd($amount_int,$wallet_amount);
                                        if($amount_int > $wallet_amount)
                                        {
                                            if(isset($user_data) && count($user_data)>0)
                                            {
                                              $transaction_inp['mp_user_id']      = isset($user_data['mp_user_id'])?$user_data['mp_user_id']:'';
                                              $transaction_inp['mp_wallet_id']    = isset($user_data['mp_wallet_id'])?$user_data['mp_wallet_id']:'';
                                              $transaction_inp['ReturnURL']       = url('/expert/subscription/payment/return/return?invoice_id='.$invoice_id.'&amount_int='.$amount_int.'&currency_code='.$arr_payment_details['currency_code'].'&payment_obj_id='.$arr_payment_details['payment_obj_id']);
                                              $transaction_inp['currency_code']   = $arr_payment_details['currency_code'];
                                              
                                            }
                                            $amount             = (float) $amount_int - (float)$wallet_amount;

                                            // $obj_data = $this->CurrencyModel->with('deposite')
                                            //                                 ->where('currency_code',$arr_payment_details['currency_code'])
                                            //                                 ->first();
                                            // if($obj_data)
                                            // {
                                            //     $arr_data = $obj_data->toArray();
                                            //     $min_amount = isset($arr_data['deposite']['min_amount'])?$arr_data['deposite']['min_amount']:'';
                                            //     $max_amount = isset($arr_data['deposite']['max_amount'])?$arr_data['deposite']['max_amount']:'';
                                            //     $min_charge = isset($arr_data['deposite']['min_amount_charge'])?$arr_data['deposite']['min_amount_charge']:'';
                                            //     $max_charge = isset($arr_data['deposite']['max_amount_charge'])?$arr_data['deposite']['max_amount_charge']:'';
                                                
                                            //     if($amount>$min_amount)
                                            //     {
                                            //         $service_charge_payin = (float)$amount * (float)$max_charge/100;
                                            //     }
                                            //     else
                                            //     {
                                            //         $service_charge_payin =  (float) $min_charge;
                                            //     }
                                            //     //$total_amount = (float)$total_amount - (float)$wallet_amount;
                                            // }
                                            
                                            $total_amount = (float)$amount;

                                            $transaction_inp['amount']          = $total_amount;
                                            $transaction_inp['fee']             = 0;//(int)$service_charge_payin;

                                            // dd($transaction_inp);
                                            $mp_add_money_in_wallet             = $this->WalletService->payInWallet($transaction_inp);
                                            //dd($mp_add_money_in_wallet);
                                            if(isset($mp_add_money_in_wallet->Status) && $mp_add_money_in_wallet->Status == 'CREATED')
                                            {   
                                                Redirect::to($mp_add_money_in_wallet->ExecutionDetails->RedirectURL)->send();
                                            } 
                                            else 
                                            {
                                              
                                              if(isset($mp_add_money_in_wallet) && $mp_add_money_in_wallet == false){
                                                Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
                                                return redirect()->back();
                                              }else{
                                                Session::flash('error',$mp_add_money_in_wallet);
                                                return redirect()->back();
                                              }
                                            }
                                        }
                                        else 
                                        {
                                            $transaction_inp['tag']                      = $arr_transaction['invoice_id'].'- Subscription fee';
                                            $transaction_inp['credited_UserId']          = $admin_data['mp_user_id'];   // archexpert admin user id
                                            $transaction_inp['credited_walletId']        = (string)$admin_data['mp_wallet_id']; 
                                            $transaction_inp['total_pay']                = $amount_int;                 // services amount 

                                            $transaction_inp['debited_UserId']           = $user_data['mp_user_id']; // expert user id
                                            $transaction_inp['debited_walletId']         = (string)$user_data['mp_wallet_id']; // expert wallet id
                                            $transaction_inp['currency_code']            = $arr_payment_details['currency_code']; // expert wallet id
                                            $transaction_inp['cost_website_commission']  = '0';
                                            //dd($transaction_inp);
                                            $pay_fees        = $this->WalletService->walletTransfer($transaction_inp);
                                            $project         = [];
                                            if(isset($pay_fees->Status) && $pay_fees->Status == 'SUCCEEDED')
                                            {

                                                // update transaction details
                                                $update_transaction   = [];
                                                $update_transaction['WalletTransactionId']    = isset($pay_fees->Id) ? $pay_fees->Id : '0';
                                                $update_transaction['payment_status']         = 2; // paid
                                                $update_transaction['response_data']          = json_encode($pay_fees); // paid
                                                $updatetransaction = $this->TransactionsModel->where('invoice_id',$arr_transaction['invoice_id'])->update($update_transaction); 
                                                // end update transaction details    

                                                // send notification to admin
                                                $arr_admin_data                         =  [];
                                                $arr_admin_data['user_id']              =  $admin_data['id'];
                                                $arr_admin_data['user_type']            = '1';
                                                $arr_admin_data['url']                  = 'admin/wallet/archexpert';
                                                $arr_admin_data['project_id']           = '';
                                                $arr_admin_data['notification_text_en'] = $arr_transaction['invoice_id'].'-'.Lang::get('controller_translations.subscription_fee_paid',[],'en','en').' '.$pay_fees->Id;
                                                $arr_admin_data['notification_text_de'] = $arr_transaction['invoice_id'].'-'.Lang::get('controller_translations.subscription_fee_paid',[],'de','en').' '.$pay_fees->Id;
                                                $this->NotificationsModel->create($arr_admin_data); 
                                                // end send notification to admin    

                                                $subscription_update = $this->SubscriptionUsersModel->where('user_id',$this->user_id)->where('is_active',1)->update(['is_active'=>0]);  
                                                $subscription_update_paid = $this->SubscriptionUsersModel->where('user_id',$this->user_id)->where('invoice_id',$arr_transaction['invoice_id'])->update(['is_active'=>1]); 
                                                if($arr_payment_details['payment_obj_id']=='1'){
                                                $this->remove_subscription_dependency($this->user_id);
                                                }

                                                Session::flash('payment_success_msg',trans('controller_translations.msg_payment_transaction_for_subscription_is_successfully'));
                                                Session::flash('payment_return_url','/expert/profile');
                                                $this->MailService->subscriptionMail($arr_transaction['invoice_id']);
                                                Redirect::to($this->payment_success_url)->send();
                                            } 
                                            else 
                                            {
                                              if(isset($pay_fees->ResultMessage))
                                              {
                                                  Session::flash('payment_error_msg',$pay_fees->ResultMessage);  
                                              }
                                              else 
                                              {
                                                if(gettype($pay_fees) == 'string')
                                                {
                                                    Session::flash('payment_error_msg',$pay_fees);
                                                }
                                              }
                                              return redirect()->back();
                                            }
                                        }
                                    }
                                    else 
                                    {
                                        if(isset($admin_data))
                                        {
                                            // send notification to admin
                                            $arr_admin_data                         =  [];
                                            $arr_admin_data['user_id']              =  $admin_data['id'];
                                            $arr_admin_data['user_type']            = '1';
                                            $arr_admin_data['url']                  = 'admin/wallet/archexpert';
                                            $arr_admin_data['project_id']           = '';
                                            $arr_admin_data['notification_text_en'] = Lang::get('controller_translations.create_wallet',[],'en','en');
                                            $arr_admin_data['notification_text_de'] = Lang::get('controller_translations.create_wallet',[],'de','en');
                                            $this->NotificationsModel->create($arr_admin_data);      
                                            // end send notification to admin   
                                        }
                                        Session::flash('payment_error_msg',trans('controller_translations.admin_dont_have_wallet_created_yet'));  
                                        return redirect()->back(); 
                                    }
                                }
                                else if (isset($arr_payment_details['payment_method']) && $arr_payment_details['payment_method']==4) 
                                {
                                    $user_data  = get_user_wallet_details($this->user_id,$arr_payment_details['currency_code']);
                                    
                                    $string_amount = str_replace(",","",$arr_transaction['paymen_amount']);
                                    $amount_int    = (int) $string_amount;
                                    
                                    if(isset($user_data) && count($user_data)>0)
                                    {
                                        $transaction_inp['mp_user_id']      = isset($user_data['mp_user_id'])?$user_data['mp_user_id']:'';
                                        $transaction_inp['mp_wallet_id']    = isset($user_data['mp_wallet_id'])?$user_data['mp_wallet_id']:'';
                                        $transaction_inp['ReturnURL']       = url('/expert/subscription/payment/return/return?invoice_id='.$invoice_id.'&amount_int='.$amount_int.'&currency_code='.$arr_payment_details['currency_code'].'&payment_obj_id='.$arr_payment_details['payment_obj_id']).'&is_card_payment=1&card_id='.$arr_payment_details['card_id'];
                                        $transaction_inp['currency_code']   = $arr_payment_details['currency_code'];
                                    }

                                    $amount = (float) $amount_int;

                                    // $obj_data = $this->CurrencyModel->with('deposite')
                                    //                                 ->where('currency_code',$arr_payment_details['currency_code'])
                                    //                                 ->first();
                                    // if($obj_data)
                                    // {
                                    //     $arr_data = $obj_data->toArray();
                                    //     $min_amount = isset($arr_data['deposite']['min_amount'])?$arr_data['deposite']['min_amount']:'';
                                    //     $max_amount = isset($arr_data['deposite']['max_amount'])?$arr_data['deposite']['max_amount']:'';
                                    //     $min_charge = isset($arr_data['deposite']['min_amount_charge'])?$arr_data['deposite']['min_amount_charge']:'';
                                    //     $max_charge = isset($arr_data['deposite']['max_amount_charge'])?$arr_data['deposite']['max_amount_charge']:'';
                                        
                                    //     if($amount>$min_amount)
                                    //     {
                                    //         $service_charge_payin = (float)$amount * (float)$max_charge/100;
                                    //     }
                                    //     else
                                    //     {
                                    //         $service_charge_payin =  (float) $min_charge;
                                    //     }
                                    //     //$total_amount = (float)$total_amount - (float)$wallet_amount;
                                    // }
                                    
                                    $total_amount = (float)$amount;

                                    $transaction_inp['amount']          = ceil($total_amount);
                                    $transaction_inp['fee']             = 0;//(int)$service_charge_payin;

                                    $transaction_inp['card_id']          = $arr_payment_details['card_id'];
                                    $transaction_inp['card_fingerprint'] = $arr_payment_details['card_fingerprint'];
 
                                    $arr_payin_wallet_by_card = $this->WalletService->payInWalletByCard($transaction_inp);
                            
                                    if(isset($arr_payin_wallet_by_card['status']) && $arr_payin_wallet_by_card['status'] == 'error') {
                                        $msg = isset($arr_payin_wallet_by_card['msg']) ? $arr_payin_wallet_by_card['msg'] : 'Something went, wrong, Unable to process payment, Please try again.';
                                        Session::flash('error',$msg);  
                                        return redirect()->back();
                                    }

                                    if(isset($arr_payin_wallet_by_card['data']->Status) && $arr_payin_wallet_by_card['data']->Status == 'CREATED')
                                    {   
                                        Redirect::to($arr_payin_wallet_by_card['data']->ExecutionDetails->SecureModeRedirectURL)->send();
                                    }
                                    else if(isset($arr_payin_wallet_by_card['data']->Status) && $arr_payin_wallet_by_card['data']->Status == 'SUCCEEDED')
                                    {   
                                        $admin_data   = get_user_wallet_details('1',$arr_payment_details['currency_code']);

                                        $transaction_inp['tag']                      = $arr_transaction['invoice_id'].'- Subscription fee';
                                        $transaction_inp['credited_UserId']          = $admin_data['mp_user_id'];   // archexpert admin user id
                                        $transaction_inp['credited_walletId']        = (string)$admin_data['mp_wallet_id']; 
                                        $transaction_inp['total_pay']                = $amount_int;                 // services amount 

                                        $transaction_inp['debited_UserId']           = $user_data['mp_user_id']; // expert user id
                                        $transaction_inp['debited_walletId']         = (string)$user_data['mp_wallet_id']; // expert wallet id
                                        $transaction_inp['currency_code']            = $arr_payment_details['currency_code']; // expert wallet id
                                        $transaction_inp['cost_website_commission']  = '0';
                                        //dd($transaction_inp);
                                        $pay_fees        = $this->WalletService->walletTransfer($transaction_inp);
                                        $project         = [];
                                        
                                        if(isset($pay_fees->Status) && $pay_fees->Status == 'SUCCEEDED')
                                        {
                                            // update transaction details
                                            $update_transaction   = [];
                                            $update_transaction['WalletTransactionId']    = isset($pay_fees->Id) ? $pay_fees->Id : '0';
                                            $update_transaction['payment_status']         = 2; // paid
                                            $update_transaction['response_data']          = json_encode($pay_fees); // paid
                                            $updatetransaction = $this->TransactionsModel->where('invoice_id',$arr_transaction['invoice_id'])->update($update_transaction); 
                                            // end update transaction details    

                                            // send notification to admin
                                            $arr_admin_data                         =  [];
                                            $arr_admin_data['user_id']              =  $admin_data['id'];
                                            $arr_admin_data['user_type']            = '1';
                                            $arr_admin_data['url']                  = 'admin/wallet/archexpert';
                                            $arr_admin_data['project_id']           = '';
                                            $arr_admin_data['notification_text_en'] = $arr_transaction['invoice_id'].'-'.Lang::get('controller_translations.subscription_fee_paid',[],'en','en').' '.$pay_fees->Id;
                                            $arr_admin_data['notification_text_de'] = $arr_transaction['invoice_id'].'-'.Lang::get('controller_translations.subscription_fee_paid',[],'de','en').' '.$pay_fees->Id;
                                            $this->NotificationsModel->create($arr_admin_data); 
                                            // end send notification to admin    

                                            $subscription_update = $this->SubscriptionUsersModel->where('user_id',$this->user_id)->where('is_active',1)->update(['is_active'=>0]);  
                                            $subscription_update_paid = $this->SubscriptionUsersModel->where('user_id',$this->user_id)->where('invoice_id',$arr_transaction['invoice_id'])->update(['is_active'=>1]); 
                                            if($arr_payment_details['payment_obj_id']=='1'){
                                            $this->remove_subscription_dependency($this->user_id);
                                            }

                                            Session::flash('payment_success_msg',trans('controller_translations.msg_payment_transaction_for_subscription_is_successfully'));
                                            Session::flash('payment_return_url','/expert/profile');
                                            $this->MailService->subscriptionMail($arr_transaction['invoice_id']);
                                            Redirect::to($this->payment_success_url)->send();
                                        } 
                                        else 
                                        {
                                            if(isset($pay_fees->ResultMessage))
                                            {
                                                Session::flash('payment_error_msg',$pay_fees->ResultMessage);  
                                            }
                                            else 
                                            {
                                                if(gettype($pay_fees) == 'string')
                                                {
                                                    Session::flash('payment_error_msg',$pay_fees);
                                                }
                                            }
                                            return redirect()->back();
                                        }
                                    } 
                                    else 
                                    {
                                        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
                                        return redirect()->back();
                                    }
                                }
							}
						}
					}
				}
			}
		}
	}
  private function _generate_invoice_id(){
   		$secure = TRUE;    
        $bytes = openssl_random_pseudo_bytes(3, $secure);
        $order_token = 'INV'.date('Ymd').strtoupper(bin2hex($bytes));
        return $order_token;
  }
  public function remove_subscription_dependency($expert_user_id=null)
   {
     if (isset($expert_user_id) && $expert_user_id!=null) 
     {
       $delete_category  = $this->ExpertsCategoriesModel->where('expert_user_id',$expert_user_id)->delete();
       $delete_skills    = $this->ExpertsSkillModel->where('expert_user_id',$expert_user_id)->delete();
       $delete_favourite = $this->FavouriteProjectsModel->where('expert_user_id',$expert_user_id)->delete();
     }
   }

    /*
    | Comments 	: Validate Subscription plan before making a bid.
    | Auther 	: Nayan S.
    */

    public function validate_subscription($project_id)
    {
    	$arr_subscription = $result = [];
        $is_starter_plan  = 0;

        /* Chekcking if the subsription olan is starter plan */
        $obj_subscription_initial = $this->SubscriptionUsersModel->where('user_id',$this->user_id)->where('is_active','1')->orderBy('id','DESC')->first();

        if($obj_subscription_initial)
        {
            $arr_tmp_subscription = $obj_subscription_initial->toArray();

            if(isset($arr_tmp_subscription['subscription_pack_id'])  && $arr_tmp_subscription['subscription_pack_id'] == '1')
            {
                $is_starter_plan = 1;
            }
        }

        /* If subscription plan is not a stater */
        if($is_starter_plan == 0)
        {
            $obj_subscription = $this->SubscriptionUsersModel->where('is_active','1')
                                                                ->where('user_id',$this->user_id)
                                                                ->whereRaw('DATE_FORMAT(expiry_at,"%Y-%m-%d") >= DATE_FORMAT(NOW(),"%Y-%m-%d") AND DATE_FORMAT(created_at,"%Y-%m-%d") <= DATE_FORMAT(NOW(),"%Y-%m-%d")')
                                                                ->orderBy('id','DESC')
                                                                ->with(['transaction_details'])
                                                                ->whereHas('transaction_details',function ($query){
                                                                    $query->whereIN('payment_status',array('1','2'));
                                                                })->first();
        } 
        else
        {
            $obj_subscription = $this->SubscriptionUsersModel->where('is_active','1')
                                                                ->where('user_id',$this->user_id)
                                                                ->orderBy('id','DESC')
                                                                ->with(['transaction_details'])
                                                                ->whereHas('transaction_details',function ($query){
                                                                    $query->whereIN('payment_status',array('1','2'));
                                                                })->first();
        }

        if ($obj_subscription)
        {
            $arr_subscription = $obj_subscription->toArray();

            if(count($arr_subscription)>0){ 
            /* counting bids done in perticular subscription plan */
            if($is_starter_plan == 0) {

                $bids_count = $this->ProjectsBidsModel->where('expert_user_id',$this->user_id)
                                                    ->whereRaw('DATE_FORMAT(created_at,"%Y-%m-%d") BETWEEN DATE_FORMAT("'.$arr_subscription['created_at'].'","%Y-%m-%d") AND DATE_FORMAT("'.$arr_subscription['expiry_at'].'","%Y-%m-%d")')
                                                    ->count();
            } else {
                $bids_count = $this->ProjectsBidsModel->where('expert_user_id',$this->user_id)
                                                    ->orderBy('id','DESC')
                                                    ->where('created_at','>=',$arr_subscription['created_at'])
                                                    ->count();
            }

            if( $bids_count >= $arr_subscription['number_of_bids']) {
                $topup_bid = $this->ProjectsBidsTopupModel->where('is_used','0')->where('payment_status','1')
                                ->where('expert_user_id',$this->user_id)
                                ->whereHas('transaction_details',function ($query)
                                 {
                                   $query->whereIN('payment_status',['1','2']);
                                 })
                                ->count();
              if ($topup_bid==0){
                $result['status'] = "ERROR";
                $result['msg']    = trans('controller_translations.msg_bid_limit_excced'); 
                /*Changes by Sagar Sainkar:
                for topup bid fucntionality rediect form here to purchase one extra bid*/
                return redirect('/expert/bids/purchase')->send();
              }
            }   
            /* checking that expert category matches project category */
            $arr_expert_cat = [];
            $obj_expert_cat = $this->ExpertsCategoriesModel->where('expert_user_id',$this->user_id)->get();
            if($obj_expert_cat){
              $arr_tmp_cat    = $obj_expert_cat->toArray(); 
              foreach ($arr_tmp_cat as $key => $category){
                array_push($arr_expert_cat,$category['category_id']);
              }
            }
            if(count($arr_expert_cat)>0){
              $obj_project_category = $this->ProjectpostModel->where('id',$project_id)
                                                             ->whereIn('category_id',$arr_expert_cat)
                                                             ->first();
              if(!$obj_project_category){ 
                //dd('1');
                  $result['status'] = "ERROR";
                  //$result['msg']  = trans('controller_translations.msg_category_does_not_matches'); 
                  $result['msg']    = trans('controller_translations.msg_skill_category_did_not_match',array('subscription_link'=>url('/expert/subscription')));
              }
            }
            /* checking with expert skills */
            
          } else {
            //dd('4');
              $result['status'] = "ERROR";
              //$result['msg']  = trans('controller_translations.msg_not_have_a_valid_subscription_plan'); 
              $result['msg']    = trans('controller_translations.msg_skill_category_did_not_match',array('subscription_link'=>url('/expert/subscription')));
          }
        }  else {
          //dd('5');
            $result['status'] = "ERROR";
            //$result['msg']  = trans('controller_translations.msg_not_have_a_valid_subscription_plan');
            $result['msg']    = trans('controller_translations.msg_skill_category_did_not_match',array('subscription_link'=>url('/expert/subscription')));
        }
        return $result;
      }
     /*
    | Comments  : Validate Subscription plan before making a bid.
    | Auther  : Nayan S.
    */
    public function validate_favourite_projects($project_id)
    {
      $arr_subscription = $result = [];
      $is_starter_plan  = 0;
      $favourites_count = 0;

      /* Chekcking if the subsription olan is starter plan */
      
      $obj_subscription_initial = $this->SubscriptionUsersModel
                                       ->where('user_id',$this->user_id)
                                       ->where('is_active','1')
                                       ->orderBy('id','DESC')
                                       ->first();

      if($obj_subscription_initial)
      {
        $arr_tmp_subscription = $obj_subscription_initial->toArray();
        
        if(isset($arr_tmp_subscription['subscription_pack_id'])  && $arr_tmp_subscription['subscription_pack_id'] == '1')
        {
          $is_starter_plan = 1;
        }
      }
      
      /* If subscription plan is not a stater */

      if($is_starter_plan == 0)
      { 
        $obj_subscription = $this->SubscriptionUsersModel
                                 ->where('is_active','1')
                                 ->where('user_id',$this->user_id)
                                 ->whereRaw('DATE_FORMAT(expiry_at,"%Y-%m-%d") >= DATE_FORMAT(NOW(),"%Y-%m-%d") AND DATE_FORMAT(created_at,"%Y-%m-%d") <= DATE_FORMAT(NOW(),"%Y-%m-%d")')
                                 ->orderBy('id','DESC')
                                 ->with(['transaction_details'])
                                 ->whereHas('transaction_details',function ($query)
                                  {
                                      $query->whereIN('payment_status',array('1','2'));
                                  })->first();
      } 
      else
      {
        $obj_subscription = $this->SubscriptionUsersModel
                                 ->where('is_active','1')
                                 ->where('user_id',$this->user_id)
                                 ->orderBy('id','DESC')
                                 ->with(['transaction_details'])
                                 ->whereHas('transaction_details',function ($query)
                                  {
                                      $query->whereIN('payment_status',array('1','2'));
                                  })->first();
      }

        if ($obj_subscription) 
        {
          $arr_subscription = $obj_subscription->toArray();

          if(count($arr_subscription)>0)
          { 
            /* counting favourites done in perticular subscription plan */
            
            if($is_starter_plan == 0)
            {
              $favourites_count = $this->FavouriteProjectsModel->where('expert_user_id',$this->user_id)
                                                    ->whereRaw('DATE_FORMAT(created_at,"%Y-%m-%d") BETWEEN DATE_FORMAT("'.$arr_subscription['created_at'].'","%Y-%m-%d") AND DATE_FORMAT("'.$arr_subscription['expiry_at'].'","%Y-%m-%d")')
                                                    ->count();
            }
            else 
            {
              $favourites_count = $this->FavouriteProjectsModel->where('expert_user_id',$this->user_id)
                                                    ->orderBy('id','DESC')
                                                    ->where('created_at','>=',$arr_subscription['created_at'])
                                                    ->count();
            }
            if( $favourites_count >= $arr_subscription['number_of_favorites_projects'])
            {
                $result['status'] = "ERROR";
                $result['msg']    = trans('controller_translations.msg_already_added_projects_to_your_favourites');
                return $result;
            }   
          }
          else
          {
              $result['status'] = "ERROR";
              $result['msg'] = trans('controller_translations.msg_not_have_a_valid_subscription_plan'); 
          }
        }
        else
        {
            $result['status'] = "ERROR";
            $result['msg'] = trans('controller_translations.msg_not_have_a_valid_subscription_plan');
        }
        return $result;
    }
    public function validate_contest_critearea($contest_id)
    {
      if(! $user = Sentinel::check()) {
          return redirect('/login');
      }
      $arr_expert_cat      = [];
      $is_category_matched = FALSE;    
      $is_skill_matched    = TRUE;
      $obj_expert_cat = $this->ExpertsCategoriesModel->where('expert_user_id',$this->user_id)->get();
      if($obj_expert_cat){
        $arr_tmp_cat    = $obj_expert_cat->toArray(); 
        foreach ($arr_tmp_cat as $key => $category) {
          array_push($arr_expert_cat,$category['category_id']);
        }
      }
      if(count($arr_expert_cat)>0){
        $obj_contest_category = $this->ContestModel->where('id',$contest_id)
                                                       ->whereIn('category_id',$arr_expert_cat)
                                                       ->first();
        if($obj_contest_category != null){ 
            $is_category_matched = TRUE;
        }
      }

      /* checking with expert skills */
      $arr_expert_skills = [];
      $obj_expert_skills = $this->ExpertsSkillModel->where('expert_user_id',$this->user_id)->get();
      if($obj_expert_skills){
        $arr_tmp_skills    = $obj_expert_skills->toArray(); 
        foreach ($arr_tmp_skills as $key => $skills){
          array_push($arr_expert_skills,$skills['skill_id']);
        }
      }
      $arr_contest_skills = [];
      $obj_project_skills = $this->ContestSkillsModel->where('contest_id',$contest_id)
                                                     ->get();
      if($obj_project_skills){
        $arr_tmp_project_skills    = $obj_project_skills->toArray(); 
        foreach ($arr_tmp_project_skills as $key => $skills) {
          array_push($arr_contest_skills,$skills['skill_id']);
        }
      }
      if(count($arr_expert_skills) > 0 && count($arr_contest_skills) > 0)
      { 
          /* Checking that at least one of experts skill matches with the projects skills*/  
          foreach ($arr_expert_skills as $key => $skill) {
             if(in_array($skill,$arr_contest_skills)){
                $is_skill_matched = TRUE;
             } 
          }
      } 
      
      if($is_skill_matched == TRUE && $is_category_matched == TRUE){
      return true;
      } else {
      return false;
      }

    }
}
?>