<?php 

namespace App\Common\Services;
use Crypt;
use Illuminate\Http\Request;
use App\Models\PaymentSettingsAdminModel;
use App\Models\PaymentSettingsModule;
use Session;
use Redirect;

class PaymentService
{
	public function __construct()
	{
		 $this->PaymentSettingsAdminModel = new PaymentSettingsAdminModel();
		 $this->PaymentSettingsModule = new PaymentSettingsModule();
		 
	}
	public function _encrypt($arr_data = array())
	{
		if(isset($arr_data)&& is_array($arr_data) && $arr_data!="" && sizeof($arr_data)>0)
		{
			$encoded_version  = json_encode($arr_data);
			$encrypted_version = Crypt::encrypt($encoded_version);
			return $encrypted_version;
		}
		else
		{
			return false;
		}
		
	}

	public function _decrypt($encrypted_data="")
	{
		$arr_data = array();
		if(isset($encrypted_data) && $encrypted_data!="")
		{
			$decrypted_version = Crypt::decrypt($encrypted_data);	
			$arr_data  = json_decode($decrypted_version,true);
			return $arr_data;
		}
		else
		{
			return false;
		}
	}
		
	public function get_admin_payment_settings()
	{

		$payment_data =array();
		$obj_payment_settings =  $this->PaymentSettingsAdminModel->where('id',1)->first();

		if(isset($obj_payment_settings) && sizeof($obj_payment_settings)>0)
		{

			$arr_payment_settings  = $obj_payment_settings->toArray();
			
			if(isset($arr_payment_settings) && is_array($arr_payment_settings) && sizeof($arr_payment_settings)>0)
			{
				if (isset($arr_payment_settings['payment_details']) && $arr_payment_settings['payment_details']!="" && $arr_payment_settings['payment_details']!=NULL)
				{
					$payment_data  = $this->_decrypt($arr_payment_settings['payment_details']);	
				}
				
				return $payment_data;	
			}
			else
			{
				return false;	
			}
		}
		else
		{
			return false;
		}
	}

	public function get_user_payment_settings($user_id=false)
	{
		$payment_data =array();

		if (isset($user_id) && $user_id!=false) 
		{	
			$obj_payment_settings =  $this->PaymentSettingsModule->where('user_id',$user_id)->first();

			if(isset($obj_payment_settings) && sizeof($obj_payment_settings)>0)
			{

				$arr_payment_settings  = $obj_payment_settings->toArray();
				
				if(isset($arr_payment_settings) && is_array($arr_payment_settings) && sizeof($arr_payment_settings)>0)
				{
					if (isset($arr_payment_settings['payment_details']) && $arr_payment_settings['payment_details']!="" && $arr_payment_settings['payment_details']!=NULL)
					{
						$payment_data  = $this->_decrypt($arr_payment_settings['payment_details']);
						
						return $payment_data;	
					}
				}
			}
		}
	
		return false;
	}
}
?>