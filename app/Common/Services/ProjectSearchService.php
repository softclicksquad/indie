<?php 
namespace App\Common\Services;
use Illuminate\Http\Request;
use App\Models\ProjectpostModel; 
use App\Models\SkillsModel;
use App\Models\ProjectSkillsModel;
use App\Models\ProjectsBidsModel;
use Illuminate\Pagination\Paginator;
use DB;
use Session;
use Sentinel;

class ProjectSearchService
{
	public function __construct(ProjectpostModel $property_post,
								SkillsModel $skill_model,
								ProjectSkillsModel $project_skills,
								ProjectsBidsModel $project_bids	
								)
	{
		if(!Session::has('locale'))
        {
           Session::put('locale', \Config::get('app.locale'));
        }
        app()->setLocale(Session::get('locale'));
        view()->share('selected_lang',\Session::get('locale'));
		$this->ProjectpostModel 	= $property_post;
		$this->SkillsModel 			= $skill_model;
		$this->ProjectSkillsModel 	= $project_skills;
		$this->ProjectsBidsModel 	= $project_bids;
	}
	public function make_filer_search( $arr_data = [] )
	{
		//dd($arr_data);
		$search_result = '';
		$search_keys   = [];
		$is_job_matching = 0;
		$is_job_matching = isset($arr_data['is_job_matching'])?$arr_data['is_job_matching']:0;
		/*if(Sentinel::check() == false){
		$search_result = $this->ProjectpostModel->where('project_type','0');
		} else {*/
		$search_result = $this->ProjectpostModel->whereDate('bid_closing_date','>',date('Y-m-d h:i:s'));
		/*}*/
		/* new Seaching code */
        if(empty($arr_data))
        {
        	return false;
        }        
        //dd($arr_data);
		$search_term     = isset($arr_data['search'])?trim($arr_data['search']):'';
		$full_text       = isset($arr_data['search'])?$arr_data['search']:'';
		if($search_term != "")
		{
       	  	$search_keys = explode(" ", $search_term);
		}



		if(isset($arr_data['category']) && $arr_data['category']!='')
		{
			/* category search */
			$arr_category = [];
			if( isset($arr_data['category']) && is_array($arr_data['category']))
			{
				foreach ($arr_data['category'] as $category_key => $category_value) 
				{
					$arr_category[] = isset($category_value) ? base64_decode($category_value):'';
				}
			}
			else if(isset($arr_data['category']))
			{
				$arr_category[] = base64_decode($arr_data['category']);
			}

	        if(isset($arr_category) && count($arr_category)>0)
	        {
	          $search_result = $search_result->whereIn('category_id',$arr_category);
	        }

	       
		}
		elseif($is_job_matching == '1')
		{
			$search_result = '';
		}

		/*Search by Subcategories*/ 
		//dd($arr_data['subcategory']);
		 if(isset($arr_data['subcdategory']) && $arr_data['subcategory'] !="" && $search_result!=''){
			$subcategory = $arr_data['subcategory'];
            $search_result =$search_result->where('projects.project_status','2')
            ->where(function ($q) use($subcategory) 
			{
			    $q->whereHas('sub_category_details.subcategory_search', function ($query) use($subcategory) 
				{
					if (isset($subcategory) && count($subcategory)){
						foreach ($subcategory as $key => $subcat){
							if($key == 0)
							{
								$query->where('subcategories_id',base64_decode($subcat)); 
							} 
							else 
							{							
								$query->orWhere('subcategories_id',base64_decode($subcat)); 
							}
						}
					}
				});
			});	
        }
        
        /* skills search */
        /*if(isset($arr_data['skills']) && $arr_data['skills'] !="" && $search_result!=''){
			$skills = $arr_data['skills'];
            $search_result =$search_result->where('projects.project_status','2')
            ->where(function ($q) use($skills) 
			{
			    $q->whereHas('project_skills.skill_data.skill_search', function ($query) use($skills) 
				{
					if (isset($skills) && sizeof($skills)){
						foreach ($skills as $key => $skill){
							if($key == 0)
							{
								$query->where('skill_id',base64_decode($skill)); 
							} 
							else 
							{							
								$query->orWhere('skill_id',base64_decode($skill)); 
							}
						}
					}
				});
			});	
        }*/
        //dd($search_result->get()->toArray());
        /* end skills search */
        /* project features search */
        if(isset($arr_data['project']) && $arr_data['project'] !="" && $search_result!=''){
            $project       = $arr_data['project'];

            $search_result = $search_result->where('projects.project_status','2')->where('projects.is_hire_process','NO')->where('projects.bid_closing_date','>',date('Y-m-d'))
            ->where(function ($q) use($project) 
			{
				if(isset($project) && sizeof($project))
				{
					if(in_array('featured', $project) &&
	                   in_array('private', $project) &&
	                   in_array('nda', $project) 	
	                  ){
	                  	$q->where('projects.highlight_end_date','>',date('Y-m-d H:i:s'));
						$q->where('projects.highlight_days','!=','0000-00-00 00:00:00');
						$q->orWhere('projects.project_type','1');
					    $q->orWhere('projects.is_nda','1');
	                } else 
	                  if(in_array('featured', $project) &&
	                     in_array('private', $project)
	                  ){
	                	$q->where('projects.highlight_end_date','>',date('Y-m-d H:i:s'));
						$q->where('projects.highlight_days','!=','0000-00-00 00:00:00');
						$q->orWhere('projects.project_type','1');
	                } else 
	                  if(in_array('featured', $project) &&
	                     in_array('nda', $project)
	                  ){
	                	$q->where('projects.highlight_end_date','>',date('Y-m-d H:i:s'));
						$q->where('projects.highlight_days','!=','0000-00-00 00:00:00');
						$q->orWhere('projects.is_nda','1');
	                } else 
	                  if(in_array('private', $project) &&
	                     in_array('nda', $project)
	                  ){
	                	$q->orWhere('projects.project_type','1');
						$q->orWhere('projects.is_nda','1');
	                } 
	                else {
	                    if(in_array('featured', $project))
	                    {
							$q->where('projects.highlight_end_date','>',date('Y-m-d H:i:s'));
							$q->where('projects.highlight_days','!=','0000-00-00 00:00:00');
					    }
					    if(in_array('private', $project)){
							$q->where('projects.project_type','1');
				     	}
						if(in_array('nda', $project)){
							$q->where('projects.is_nda','1');
						}
						if(in_array('urgent', $project)){
							$q->where('projects.is_urgent','1');
						}
	                }
				}
			});
        }
        /* end features search */
        /* fixed price search */
        if(isset($arr_data['min_fixed_amt']) && $arr_data['min_fixed_amt']>0 && isset($arr_data['max_fixed_amt']) && $arr_data['max_fixed_amt']>0 && $search_result!=''){

			//$fixed_rate = $arr_data['fixed_rate'];
			//dd($arr_data);
            $search_result =$search_result->where('projects.project_status','2')->where('projects.is_hire_process','NO')->where('projects.bid_closing_date','>',date('Y-m-d'))
            ->where('project_pricing_method','1');
   //          ->where(function ($q) use($fixed_rate) 
			// {
			//     if (isset($fixed_rate) && sizeof($fixed_rate)){
			// 	    foreach ($fixed_rate as $key => $fixedrate){
			// 			if($key == 0){
			// 				if (strpos($fixedrate, '-') !== false) {
			// 				    $explode = explode('-', $fixedrate);
			// 				    if(isset($explode[0]) && $explode[0] !="" && isset($explode[1]) && $explode[1] !=""){
			// 					    //$q->where('projects.min_project_cost','>=',intval($explode[0])); 
			// 				        //$q->where('projects.max_project_cost','<=',intval($explode[1]));
			// 				        $q->whereBetween('projects.max_project_cost', [$explode[0], $explode[1]]);
			// 				    }
			// 				} else if(isset($fixedrate) && $fixedrate !="" && strpos($fixedrate, '-') !== true){
			// 			    $q->where('projects.min_project_cost','>=',$fixedrate); 
			// 			    }
			// 			} else {
			// 				if (strpos($fixedrate, '-') !== false) {
			// 				    $explode = explode('-', $fixedrate);
			// 				    if(isset($explode[0]) && $explode[0] !="" && isset($explode[1]) && $explode[1] !=""){
			// 				        //$q->orWhere('projects.min_project_cost','>=',intval($explode[0]));
			// 				        $q->orWhere('projects.max_project_cost','<=',intval($explode[1])); 
			// 				    }
			// 				} else if(isset($fixedrate) && $fixedrate !="" && strpos($fixedrate, '-') !== true){
			// 			    $q->orWhere('projects.min_project_cost','>=',$fixedrate); 
			// 			    }	
			// 			}
			// 		}
			// 	}
			// });	
        }

        /* end fixed price search */
        /* hourly price search */
        if(isset($arr_data['min_project_amt']) && $arr_data['min_project_amt']>0 && isset($arr_data['max_project_amt']) && $arr_data['max_project_amt'] >0 && $search_result!=''){
			//$hourly_rate = $arr_data['hourly_rate'];
            $search_result =$search_result->where('projects.project_status','2')
            ->where('project_pricing_method','2')	
            ->where('projects.is_hire_process','NO')
            ->where('projects.bid_closing_date','>',date('Y-m-d'));
   //          ->where(function ($q) use($hourly_rate) 
			// {
			//     if (isset($hourly_rate) && sizeof($hourly_rate)){
			// 		foreach ($hourly_rate as $key => $hourlyrate){
			// 			$q->where('project_pricing_method',2); 
			// 			if($key == 0){
			// 				if (strpos($hourlyrate, '-') !== false) {
			// 				    $explode = explode('-', $hourlyrate);
			// 				    if(isset($explode[0]) && $explode[0] !="" && isset($explode[1]) && $explode[1] !=""){
			// 					    //$q->where('projects.min_project_cost','>=',intval($explode[0])); 
			// 				        //$q->where('projects.max_project_cost','<=',intval($explode[1]));
			// 				        $q->whereBetween('projects.max_project_cost', [$explode[0], $explode[1]]);
			// 				    }
			// 				} else if(isset($hourlyrate) && $hourlyrate !="" && strpos($hourlyrate, '-') !== true){
			// 			    $q->where('projects.min_project_cost','>=',$hourlyrate); 
			// 			    }
			// 			} else {
			// 				if (strpos($hourlyrate, '-') !== false) {
			// 				    $explode = explode('-', $hourlyrate);
			// 				    if(isset($explode[0]) && $explode[0] !="" && isset($explode[1]) && $explode[1] !=""){
			// 				        //$q->orWhere('projects.min_project_cost','>=',intval($explode[0])); 
			// 				        $q->orWhere('projects.max_project_cost','<=',intval($explode[1])); 
			// 				    }
			// 				} else if(isset($hourlyrate) && $hourlyrate !="" && strpos($hourlyrate, '-') !== true){
			// 			    $q->orWhere('projects.min_project_cost','>=',$hourlyrate); 
			// 			    }	
			// 			}
			// 		}
			//     }
			// });	
        }

        /* end hourly price search */
        if($search_result!='')
        {
        	$search_result =$search_result
		                ->where('projects.project_status','2')->where('projects.is_hire_process','NO')->where('projects.bid_closing_date','>',date('Y-m-d'))
                        ->where(function ($q) use ($search_keys,$full_text) 
						{ 
						    if (isset($search_keys) && sizeof($search_keys) > 0 ) 
							{
								foreach ($search_keys as $key => $for_search) 
								{	
									if($key == 0)
									{
										$q->where('project_name','like','%'.$full_text.'%');
									    //$q->orWhere('project_name','like','%'.$for_search.'%'); 
									}
									else
									{
										//$q->orWhere('project_name','like','%'.$for_search.'%'); 
									}
					       			//$q->orWhere('project_description','like','%'.$for_search.'%');
								}
							}
					   })->groupBy('projects.id');
        if(sizeof($search_keys)> 0)
        {	
            $search_result =$search_result->where('projects.project_status','2')->where('projects.is_hire_process','NO')->where('projects.bid_closing_date','>',date('Y-m-d'))
            ->orWhere(function ($q) use($search_keys) 
			{
				$q->whereHas('project_skills.skill_data.skill_search', function ($query) use($search_keys) 
					{
						if (isset($search_keys) && sizeof($search_keys)) 
						{
							foreach ($search_keys as $key => $for_search) 
							{
								if($key == 0)
								{
									$query->where('skill_name','like','%'.$for_search.'%'); 
								}
								else
								{
									$query->orWhere('skill_name','like','%'.$for_search.'%');	
								}
							}
						}
					})
				->with( ['project_skills.skill_data.skill_search' => function ($query) use($search_keys) 
					{
						if (isset($search_keys) && sizeof($search_keys)) 
						{
							foreach ($search_keys as $key => $for_search) 
							{	
								if($key == 0)
								{
									$query->where('skill_name','like','%'.$for_search.'%'); 
								}
								else
								{
									$query->orWhere('skill_name','like','%'.$for_search.'%');	
								}
							}
						}
					}]);
			});
            $search_result = $search_result
                ->where('projects.project_status','2')->where('projects.is_hire_process','NO')->where('projects.bid_closing_date','>',date('Y-m-d'))
                ->orWhere(function ($q) use($search_keys) {
				$q->whereHas('category_details.category_traslation', 
				function ($query_four) use ($search_keys) 
		       	{
		       		if (isset($search_keys) && sizeof($search_keys)) 
					{
						foreach ($search_keys as $key => $for_search) 
						{
							if($key == 0)
							{
								$query_four->where('category_title','like','%'.$for_search.'%');
							}
							else
							{
								$query_four->orWhere('category_title','like','%'.$for_search.'%');
							}
						}
					}
		       	})->with(['category_details.category_search' => 
				function ($query_four) use ($search_keys) 
		       	{
		       		if (isset($search_keys) && sizeof($search_keys)) 
					{
						foreach ($search_keys as $key => $for_search) 
						{
							if($key == 0)
							{
								$query_four->where('category_title','like','%'.$for_search.'%');
							}
							else
							{
								$query_four->orWhere('category_title','like','%'.$for_search.'%');
							}
						}
					}
		       	}]);
			});
		}

		$search_result =   $search_result->where('projects.project_status','=','2')
		                   ->where('projects.is_hire_process','NO')
		                   ->where('projects.bid_closing_date','>',date('Y-m-d'))
                           ->join('clients', function($join)
                            {
                                $join->on('clients.user_id', '=', 'projects.client_user_id');
                            })
                           ->select('projects.*','clients.user_id')
                           ->groupBy('projects.id')
                           ->orderBy('projects.created_at','DESC');
        }

		return $search_result;
	}
	/*
	Auther   : Nayan S.
	Comments : function to return projects according to skills matched from browse skills section 
	*/
	public function search_with_skill_id($skill_id)
	{
		$obj_data = FALSE;
		$obj_data = $this->ProjectpostModel->join('clients', function($join)
                        {
                            $join->on('clients.user_id', '=', 'projects.client_user_id');
                        })
                       ->select('projects.*','clients.user_id')
                       ->where('projects.project_status','2')
                       ->where('projects.is_hire_process','NO');
                        /*if(Sentinel::check() == false){
                       	$obj_data->where('projects.project_type','0');
                        }*/
                        $obj_data = $obj_data->where('projects.bid_closing_date','>',date('Y-m-d'))
					   ->whereHas('project_skills.skill_data', function($q) use($skill_id)
						{
						  $q->where('id',$skill_id);
						})	
					   ->with(['project_skills.skill_data','skill_details','client_details','category_details'])->paginate(config('app.project.pagi_cnt'));
		return $obj_data;
	}	
}
?>