<?php 

namespace App\Common\Services;
use Illuminate\Http\Request;

//menthod paypal
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;


use PayPal\Api\Invoice;

//billing method
use PayPal\Api\ChargeModel; 
use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\PaymentDefinition; 
use PayPal\Api\Plan;
use PayPal\Api\Agreement;

//billing update
use PayPal\Api\Patch; 
use PayPal\Api\PatchRequest; 
use PayPal\Common\PayPalModel;

//billing paypal
use PayPal\Api\ShippingAddress;

//billing CreditCard
use PayPal\Api\CreditCard; 
use PayPal\Api\FundingInstrument;

use Validator;
use Session;
use Input;
use DB;
use Mail;
use Redirect;
use Sentinel;

use App\Models\TransactionsModel;
use App\Models\SubscriptionUsersModel;
use App\Models\MilestonesModel;
use App\Models\MilestoneReleaseModel;
use App\Models\ProjectsBidsTopupModel;
use App\Models\ProjectpostModel;

use App\Models\ExpertsCategoriesModel;
use App\Models\ExpertsSkillModel;
use App\Models\FavouriteProjectsModel;

use App\Models\ExpertsModel;

use App\Common\Services\MailService;




/*Fron Here new added : 16-08-2016*/

use PayPal\Api\Payout;

class PaypalService
{
	private $_api_context;

    public function __construct($client_id,$secret_key,$paypal_mode)    
    {

    	if(!Session::has('locale'))
        {
           Session::put('locale', \Config::get('app.locale'));
        }
        app()->setLocale(Session::get('locale'));
        view()->share('selected_lang',\Session::get('locale'));
    	
        // setup PayPal api context
        $paypal_conf = config('paypal');
       
        //$this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));

        if (isset($paypal_mode) && $paypal_mode!="") 
        {
        	if (isset($paypal_conf['settings']['mode'])) 
        	{
        		if ($paypal_mode==2) 
        		{
        				$paypal_conf['settings']['mode'] = 'live';
        		}	
        		else if($paypal_mode==1)
        		{
        			$paypal_conf['settings']['mode'] = 'sandbox';
        		}
        	}
        }

        $this->_api_context = new ApiContext(new OAuthTokenCredential($client_id, $secret_key));
        $this->_api_context->setConfig($paypal_conf['settings']);

        if(!$user = Sentinel::check())
        {
         	Redirect::to('/login')->send();
        }
        $this->user_id = $user->id;

      if($user->inRole('admin'))
      {
      	$this->payment_cancel_url = url(config('app.project.admin_panel_slug')."/payment/cancel");
      	$this->payment_success_url = url(config('app.project.admin_panel_slug').'/payment/success');
      }
      else
      {
      	$this->payment_cancel_url = url('/').'/payment/cancel';
      	$this->payment_success_url = url('/').'/payment/success';
      }

      //Models
      $this->TransactionsModel= new TransactionsModel;
      $this->SubscriptionUsersModel= new SubscriptionUsersModel;
      $this->MilestonesModel = new MilestonesModel;
      $this->MilestoneReleaseModel = new MilestoneReleaseModel;
      $this->ProjectsBidsTopupModel = new ProjectsBidsTopupModel;
      $this->ProjectpostModel = new ProjectpostModel;

      $this->ExpertsCategoriesModel = new ExpertsCategoriesModel();
      $this->ExpertsSkillModel = new ExpertsSkillModel();
      $this->FavouriteProjectsModel = new FavouriteProjectsModel();
      
      
      $this->MailService = new MailService;
      
      $this->payment_redirect_url = url('/payment/charge');

    }


    // payment method in paypal 
    public function postPayment($invoice_id)
	{
		$payer = new Payer();
	    $payer->setPaymentMethod('paypal');

	  	$invoice = new Invoice();

	  	$transaction_details = $this->TransactionsModel->where('invoice_id',$invoice_id)->first();
	  	
	  	if ($transaction_details) 
	  	{
	  		$transaction_details = $transaction_details->toArray();
	  	}

		//dd($transaction_details);

	  	if (isset($transaction_details) && sizeof($transaction_details)>0 && isset($transaction_details['paymen_amount']) && isset($transaction_details['currency_code'])) 
	  	{


	  		$item1 = new Item();
	  		if (isset($transaction_details['transaction_type']) && $transaction_details['transaction_type']==1) 
	  		{
	  			$item1->setName('Subscription') // item name
	  				  ->setCurrency($transaction_details['currency_code'])				         
		              ->setQuantity(1) // quentity
		         	  ->setSku("321321")
		         	  ->setPrice($transaction_details['paymen_amount']); // unit price
	  		}
	  		elseif (isset($transaction_details['transaction_type']) && $transaction_details['transaction_type']==2) 
	  		{
	  			$item1->setName('Milestone') // item name
	  				  ->setCurrency($transaction_details['currency_code'])				         
		              ->setQuantity(1) // quentity
		         	  ->setSku("321321")
		         	  ->setPrice($transaction_details['paymen_amount']); // unit price
	  		}
	  		elseif (isset($transaction_details['transaction_type']) && $transaction_details['transaction_type']==3) 
	  		{
	  			$this->demo_payout();


	  			$item1->setName('Release Milestone') // item name
	  				  ->setCurrency($transaction_details['currency_code'])				         
		              ->setQuantity(1) // quentity
		         	  ->setSku("321321")
		         	  ->setPrice($transaction_details['paymen_amount']); // unit price
	  		}
	  		elseif (isset($transaction_details['transaction_type']) && $transaction_details['transaction_type']==4) 
	  		{
	  			$item1->setName('Bid Topup') // item name
	  				  ->setCurrency($transaction_details['currency_code'])				         
		              ->setQuantity(1) // quentity
		         	  ->setSku("321321")
		         	  ->setPrice($transaction_details['paymen_amount']); // unit price
	  		}
			elseif (isset($transaction_details['transaction_type']) && $transaction_details['transaction_type']==5) 
	  		{
	  			$item1->setName('Project Payment') // item name
	  				  ->setCurrency($transaction_details['currency_code'])				         
		              ->setQuantity(1) // quentity
		         	  ->setSku("321321")
		         	  ->setPrice($transaction_details['paymen_amount']); // unit price
	  		}
		         
			
			//add iteam to item list
			$itemList = new ItemList();
			$itemList->setItems(array($item1));

			$details = new Details();
			$details->setSubtotal($transaction_details['paymen_amount']);

  			$amount = new Amount();
    		$amount->setCurrency($transaction_details['currency_code'])
        	       ->setTotal($transaction_details['paymen_amount'])
        	       ->setDetails($details);

        	$invoice_number = $invoice_id;
	        $transaction = new Transaction();
	    	$transaction->setAmount($amount)
	        			->setItemList($itemList)
	        			->setDescription($invoice_number);
	        				  
	        			

	        
		    $redirect_urls = new RedirectUrls();
		    $redirect_urls->setReturnUrl($this->payment_redirect_url)
		        		  ->setCancelUrl($this->payment_redirect_url);

		    $payment = new Payment();
		    $payment->setIntent('sale')
		        	->setPayer($payer)
		        	->setRedirectUrls($redirect_urls)
		        	->setTransactions(array($transaction));      			
	  	}

	   

	    try {
		        $payment->create($this->_api_context);
	    } catch (\PayPal\Exception\PayPalConnectionException $ex) {
	        if (\Config::get('app.debug')) {
	        	
	        	Redirect::to($this->payment_cancel_url)->send();
	        	/*echo $ex->getData();
	            echo "Exception: " . $ex->getMessage() . PHP_EOL;
	            $err_data = json_decode($ex->getData(), true);
	            exit;*/
	        } else {
	        	Redirect::to($this->payment_cancel_url)->send();
	            //die('Some error occur, sorry for inconvenient');
	        }
	    }
	    catch (\Exception $ex) 
	    {
	    	Redirect::to($this->payment_cancel_url)->send();
	    }


	    foreach($payment->getLinks() as $link) {
	        if($link->getRel() == 'approval_url')
	         {
	            $redirect_url = $link->getHref();
	            break;
	        }
	    }

	    // add payment ID to session
	    Session::put('paypal_payment_id', $payment->getId());
	    //set trasaction type
	    if (isset($transaction_details['transaction_type'])) 
  		{
  			Session::put('transaction_type', $transaction_details['transaction_type']);

  			/* transaction type is release request then ger expert and set in session for charge_handler  in payment controller (for this payment we have to get experts paypal details)*/
  			if ($transaction_details['transaction_type']==3) 
  			{
  				$expert_id = $this->get_expert_form_release_request($invoice_id);
  				Session::put('expert_id_for_paypal', $expert_id);
  				
  			}
  		}


	    if(isset($redirect_url)) 
	    {   // redirect to paypal
	        //return Redirect::away($redirect_url);
            Redirect::to($redirect_url)->send();
	    }

	    Redirect::to($this->payment_cancel_url)->send();
	    /*return Redirect::route('original.route')
	        ->with('error', 'Unknown error occurred');*/
	}


	public function getPaymentStatus($payment_id,$payer_id)
	{
		$response_result = array();
		$arr_transaction = array();
		$payment_status = "0";
		$update_status = false;

		if ($payer_id!="" && $payment_id!="") 
		{

			try 
			{

				$payment = Payment::get($payment_id, $this->_api_context);
			    $execution = new PaymentExecution();	    
			    $execution->setPayerId($payer_id);
			    //Execute the payment
			    $result = $payment->execute($execution, $this->_api_context);

			    if($result)
			    {
			    	$response_result = $result->toArray();
			    }	
		        
	    	} 
	    	catch (\Exception $ex) 
	    	{
	    		Redirect::to($this->payment_cancel_url)->send();
	    	}
		    //dd($response_result);
		}
		else
		{
			Redirect::to($this->payment_cancel_url)->send();
		}
	    
		

	    if(isset($response_result) && sizeof($response_result)>0)
	    {
	    	
	    	if($response_result['state']=="approved")
	    	{
	    		$payment_status = "1";
	    	}
			elseif($response_result['state']=="pending")
            {   
                $payment_status = "0";
            }
            elseif($response_result['state']=="captured")
            {   
                $payment_status = "2";
            }
            elseif($response_result['state']=="loss")
            {
                $payment_status = "3";  
            }

            $arr_transaction['payment_txn_id']   = $response_result['id'];
            $arr_transaction['payment_status']   = $payment_status;
            $arr_transaction['payment_date']     = date('c');
            $arr_transaction['response_data']    = json_encode($response_result);
            if (isset($response_result['transactions'][0]['description'])) 
            {
            	$invoice_id = $response_result['transactions'][0]['description'];

            	$obj_transactions = $this->TransactionsModel->where('invoice_id',$invoice_id)->first();

            	if ($obj_transactions) 
            	{
            		$update_status = $obj_transactions->update($arr_transaction);

		            if ($update_status && $update_status!=false) 
		            {
		            	// this payment is for subscription
		            	if (isset($obj_transactions->transaction_type) && $obj_transactions->transaction_type==1) 
		            	{
		            		if (isset($payment_status) && $payment_status==1 || isset($payment_status) && $payment_status==2)  
		            		{
		            			//update all previous subscription status to 0 and then create new subscription
              					$subscription_update = $this->SubscriptionUsersModel->where('user_id',$this->user_id)->where('is_active',1)->update(['is_active'=>0]);	

              					$subscription_update_paid = $this->SubscriptionUsersModel->where('user_id',$this->user_id)->where('invoice_id',$invoice_id)->update(['is_active'=>1]);	

              					$this->remove_subscription_dependency($this->user_id);

              					Session::flash('payment_success_msg',trans('controller_translations.msg_payment_transaction_for_subscription_is_successfully'));
						        Session::flash('payment_return_url','/expert/profile');

              					$this->MailService->subscriptionMail($invoice_id);
		            		}
		            		
		            	}
		            	// this payment is for milestone
		            	elseif (isset($obj_transactions->transaction_type) && $obj_transactions->transaction_type==2) 
		            	{
		            		if (isset($payment_status) && $payment_status==1 || isset($payment_status) && $payment_status==2) 
		            		{
		            			$update_milstone = $this->MilestonesModel->where('invoice_id',$invoice_id)->update(['status'=>'1']);

		            			Session::flash('payment_success_msg',trans('controller_translations.msg_payment_transaction_for_milestone_successfully'));

						        Session::flash('payment_return_url','/client/projects/ongoing');

		            			$this->MailService->MilestonePaymentMail($invoice_id);	
		            		}
		            		
		            	}
		            	// this payment is for milestone release
		            	elseif (isset($obj_transactions->transaction_type) && $obj_transactions->transaction_type==3) 
		            	{
		            		if (isset($payment_status) && $payment_status==1 || isset($payment_status) && $payment_status==2) 
		            		{
		            			$update_milstone = $this->MilestoneReleaseModel->where('invoice_id',$invoice_id)->update(['status'=>'2']);
		            			$this->MailService->MilestoneReleasePaymentMail($invoice_id);
		            		}
		            		
		            	}
		            	elseif (isset($obj_transactions->transaction_type) && $obj_transactions->transaction_type==4) 
		            	{
		            		if (isset($payment_status) && $payment_status==1 || isset($payment_status) && $payment_status==2) 
		            		{
		            			$update_topup = $this->ProjectsBidsTopupModel->where('invoice_id',$invoice_id)->update(['payment_status'=>'1']);

		            			Session::flash('payment_success_msg',trans('controller_translations.msg_payment_transaction_for_extra_bid_successfully'));

						        Session::flash('payment_return_url','/projects');

		            			$this->MailService->TopupBidMail($invoice_id);	
		            		}
		            		
		            	}
		            	elseif (isset($obj_transactions->transaction_type) && $obj_transactions->transaction_type==5) 
		            	{
		            		if (isset($payment_status) && $payment_status==1 || isset($payment_status) && $payment_status==2) 
		            		{
		            			$update_project = $this->ProjectpostModel->where('invoice_id',$invoice_id)->update(['project_status'=>'1']);
		            			
		            			Session::flash('payment_success_msg',trans('controller_translations.msg_payment_transaction_for_post_project_is_successfully'));

						        Session::flash('payment_return_url','/client/projects/posted');
		            			$this->MailService->ProjectPaymentMail($invoice_id);
		            		}
		            	}

		            	/*Session::flash('success','You have made successfull transaction for your membership subscription !');*/
			           Redirect::to($this->payment_success_url)->send();
		            }
	        	}
        	}
	    }

	    Redirect::to($this->payment_cancel_url)->send();

	}


	public function get_expert_form_release_request($invoice_id=null)
	{
		if ($invoice_id!=null)
		{
			$obj_milestone_request = $this->MilestoneReleaseModel->where('invoice_id',$invoice_id)->first(['expert_user_id']);
			if ($obj_milestone_request!=FALSE && isset($obj_milestone_request->expert_user_id)) 
			{
				return $obj_milestone_request->expert_user_id;
			}	
		}

		return 0;
	}

	 /*
      Auther: sagar sainkar
      Comment : this is function for remove category , skills , and favourite project for subscription
  */
   public function remove_subscription_dependency($expert_user_id=null)
   {
     if (isset($expert_user_id) && $expert_user_id!=null) 
     {
       $delete_category  = $this->ExpertsCategoriesModel->where('expert_user_id',$expert_user_id)->delete();
       $delete_skills    = $this->ExpertsSkillModel->where('expert_user_id',$expert_user_id)->delete();
       $delete_favourite = $this->FavouriteProjectsModel->where('expert_user_id',$expert_user_id)->delete();
     }
   }


   public function postPayout($invoice_id,$expert_paypal_email)
   {
   		$payment_status = 0;

   		$transaction_details = $this->TransactionsModel->where('invoice_id',$invoice_id)->first();
	  	
	  	if ($transaction_details) 
	  	{
	  		$transaction_details = $transaction_details->toArray();
	  	}


	  	if (isset($transaction_details) && sizeof($transaction_details)>0 && isset($transaction_details['paymen_amount']) && isset($expert_paypal_email) && $expert_paypal_email!=FALSE) 
	  	{

	   		$payouts = new \PayPal\Api\Payout();
	   		$senderBatchHeader = new \PayPal\Api\PayoutSenderBatchHeader();

	   		$senderBatchHeader->setSenderBatchId(uniqid())->setEmailSubject("New Milestone Released");
	   		$senderItem = new \PayPal\Api\PayoutItem();


	   		$senderItem->setRecipientType('Email')
	   					->setNote('Your milestone payment')
	   					->setReceiver($expert_paypal_email)
	   					->setSenderItemId($transaction_details['invoice_id'])
	   					->setAmount(new \PayPal\Api\Currency('{
	                        "value":"'.$transaction_details['paymen_amount'].'",
	                        "currency":"USD"
	                    	}')
	                    );

	   		$payouts->setSenderBatchHeader($senderBatchHeader) ->addItem($senderItem);
	   		$request = clone $payouts;

	   		try 
	   		{ 

	   			$payment_responce = $payouts->createSynchronous($this->_api_context);

		   			if (isset($payment_responce) && $payment_responce!=FALSE) 
		   			{
	   					$response_result = $payment_responce->toArray();

			   			if(isset($response_result) && sizeof($response_result)>0)
					    {
					    	if(isset($response_result['items'][0]['transaction_status']) && $response_result['items'][0]['transaction_status']=="SUCCESS")
					    	{
					    		$payment_status = 1;
					    	}
							else
				            {   
				                $payment_status = 0;
				            }
				            
				            //dd($response_result['items'][0]['transaction_status']);

				            $arr_transaction['payment_txn_id']   = isset($response_result['items'][0]['transaction_id'])?$response_result['items'][0]['transaction_id']:'';
				            $arr_transaction['payment_status']   = $payment_status;
				            $arr_transaction['payment_date']     = date('c');
				            $arr_transaction['response_data']    = json_encode($response_result);
				            if (isset($response_result['items'][0]['payout_item']['sender_item_id'])) 
				            {
				            	$invoice_id = $response_result['items'][0]['payout_item']['sender_item_id'];

				            	$obj_transactions = $this->TransactionsModel->where('invoice_id',$invoice_id)->first();

				            	if ($obj_transactions) 
				            	{
				            		$update_status = $obj_transactions->update($arr_transaction);

						            if ($update_status && $update_status!=false) 
						            {
						            	if(!isset($response_result['items'][0]['errors']))
				    					{
							            	// this payment is for milestone release
							            	if (isset($obj_transactions->transaction_type) && $obj_transactions->transaction_type==3) 
							            	{
							            		if (isset($payment_status) && $payment_status==1) 
							            		{
							            			$update_milstone = $this->MilestoneReleaseModel->where('invoice_id',$invoice_id)->update(['status'=>'2']);
							            			$this->MailService->MilestoneReleasePaymentMail($invoice_id);

							            			Redirect::to($this->payment_success_url)->send();
							            		}
							            	}
							            	/*Session::flash('success','You have made successfull transaction for your membership subscription !');*/
						            	}
					        		}
				        		}
				        	}

				    	}
	   				}

	   				Redirect::to($this->payment_cancel_url)->send();

	   		} 
	   		catch (\Exception $ex) 
	   		{
	   			//dd($ex);
	   			Redirect::to($this->payment_cancel_url)->send();
	   		}

	   		Redirect::to($this->payment_cancel_url)->send();

   		}
   		Redirect::to($this->payment_cancel_url)->send();
   		
   }
	
}
?>