<?php 

namespace App\Common\Services;
use Crypt;
use Illuminate\Http\Request;

use Session;
use Redirect;
use Sentinel;

use App\Models\TransactionsModel;
use App\Models\MilestonesModel;
use App\Models\ProjectpostModel;
use App\Models\SiteSettingModel;
use App\Models\UserWalletModel;
use App\Models\CurrencyModel;


use App\Common\Services\PaymentService;
use App\Common\Services\PaypalService;
use App\Common\Services\StripeService;
use App\Common\Services\WalletService;
use App\Common\Services\MailService;
use App\Models\NotificationsModel;
use Lang;
class oldProjectPaymentService
{
	public function __construct()
	{
		 if(!Session::has('locale'))
         {
           Session::put('locale', \Config::get('app.locale'));
         }
         app()->setLocale(Session::get('locale'));
         view()->share('selected_lang',\Session::get('locale'));
		
		 $this->PaypalService = FALSE;
		 $this->StripeService = FALSE;
		 if(! $user = Sentinel::check()) 
	     {
	        return redirect('/login');
	     }
      	 $this->user_id = $user->id;
		 $this->TransactionsModel 	= new TransactionsModel();
		 $this->MilestonesModel		= new MilestonesModel();
		 $this->ProjectpostModel 	= new ProjectpostModel();
		 $this->SiteSettingModel 	= new SiteSettingModel();
		 $this->PaymentService		= new PaymentService();
		 $this->WalletService		= new WalletService();
		 $this->MailService		    = new MailService();
		 $this->UserWalletModel     = new UserWalletModel();
		 $this->NotificationsModel	= new NotificationsModel();
		 $this->CurrencyModel       = new CurrencyModel;
		 $admin_payment_settings    = $this->PaymentService->get_admin_payment_settings();
		 if (isset($admin_payment_settings) && sizeof($admin_payment_settings)>0) 
		 {
		 	if (isset($admin_payment_settings['paypal_client_id']) && isset($admin_payment_settings['paypal_secret_key']) && isset($admin_payment_settings['paypal_payment_mode'])) 
		 	{
		 			$this->PaypalService = new PaypalService($admin_payment_settings['paypal_client_id'],$admin_payment_settings['paypal_secret_key'],$admin_payment_settings['paypal_payment_mode']);
		 	}
		 	if (isset($admin_payment_settings['stripe_secret_key']))
		 	{
		 		$this->StripeService = new StripeService($admin_payment_settings['stripe_secret_key']);
		 	}
		 }
		 $this->payment_cancel_url  				= url('/').'/payment/cancel';
      	 $this->payment_success_url 				= url('/').'/payment/success';
    
	  	 if(isset($user)){
	        $logged_user                 = $user->toArray();  
	        if($logged_user['mp_wallet_created'] == 'Yes'){
	          $this->mp_user_id          = $logged_user['mp_user_id'];
	          $this->mp_wallet_id        = $logged_user['mp_wallet_id'];
	          $this->mp_wallet_created   = $logged_user['mp_wallet_created'];
	        } else {
		        $this->mp_user_id          = '';
		        $this->mp_wallet_id        = '';
		        $this->mp_wallet_created   = 'No';
		    }
	     } else {
	        $this->mp_user_id          = '';
	        $this->mp_wallet_id        = '';
	        $this->mp_wallet_created   = 'No';
	     }
	}

	public function payment($arr_payment_details)
	{
		//dd('newwwww',$arr_payment_details);
		$arr_transaction   = array();
		$obj_site_settings = FALSE;
		$invoice_id        = "";
		if (isset($arr_payment_details['payment_obj_id']) && isset($arr_payment_details['payment_method'])) 
		{
			$paid_services = isset($arr_payment_details['project_services_type'])?$arr_payment_details['project_services_type']:'';

			/* Payment method :1-paypal 2-Stripe $arr_payment_details['payment_method']; */
		  	$invoice_id	= $this->_generate_invoice_id();
		  	/*First make transaction  */ 
		  	$arr_transaction['user_id']          = $this->user_id;
		  	$arr_transaction['invoice_id']       = $invoice_id;
		  	/*transaction_type is type for transactions 1-Subscription 2-Milestone 3-Release Milestones 5- Project Payment */
		  	$arr_transaction['transaction_type'] = 5;
		  	$arr_transaction['paymen_amount']    = $arr_payment_details['project_services_cost'];
		  	$arr_transaction['payment_method']   = $arr_payment_details['payment_method'];
		  	$arr_transaction['payment_date']     = date('c');
		  	/* get currency for transaction using helper function */
		  	//$arr_currency = setCurrencyForTransaction($arr_payment_details['payment_obj_id']);
		  	//$arr_transaction['currency']         = isset($arr_currency['currency']) ? $arr_currency['currency'] : '$';
		  	$arr_transaction['currency_code']    = isset($arr_payment_details['currency_code'])?
		  												 $arr_payment_details['currency_code'] : 'USD';
		  	$transaction = $this->TransactionsModel->create($arr_transaction);
		  	if($transaction) 
		  	{ 	
		  		/*update invoice_id  in project tabel*/
				$update_status = $this->ProjectpostModel->where('id',$arr_payment_details['payment_obj_id'])->update(['invoice_id' =>$invoice_id]);

				if($update_status) 
				{
					/* Now redirect to payment gateway $arr_payment_details[payment_method] is Paypal and Stripe  1-Paypal 2-Stripe 3 wallet */
				  	if (isset($arr_payment_details['payment_method']) && $arr_payment_details['payment_method']==1) // paypal 
				  	{
				  		if(!$this->PaypalService==FALSE)
						{
						 	$this->PaypalService->postPayment($invoice_id);
						}
				  	}
				  	else if(isset($arr_payment_details['payment_method']) && $arr_payment_details['payment_method']==2) // stripe 
				  	{
				  		if(!$this->StripeService==FALSE)
						{
						 	if (isset($arr_payment_details['cardNumber']) && isset($arr_payment_details['cardExpiryMonth']) && isset($arr_payment_details['cardExpiryYear'])) 
						 	{
						 		$card_details = array('number' => $arr_payment_details['cardNumber'], 'exp_month' => $arr_payment_details['cardExpiryMonth'], 'exp_year' => $arr_payment_details['cardExpiryYear']);
						 		$this->StripeService->postPayment($invoice_id,$card_details);
						 	}
						}
				  	}
				  	else if (isset($arr_payment_details['payment_method']) && $arr_payment_details['payment_method']==3)  // wallet
				  	{
				  		// dd('hereee');
                        $admin_data  = get_admin_email_address();
                        //dd($admin_data);
                        if(isset($admin_data) && isset($admin_data['mp_wallet_created']) && $admin_data['mp_wallet_created'] =='Yes')
                        {
                    	    // check client wallet balance
				            $mangopay_wallet_details = [];

				            $mp_details = get_user_wallet_details($this->user_id,$arr_payment_details['currency_code']);

				            $get_wallet_details = $this->WalletService->get_wallet_details($mp_details['mp_wallet_id']);

				            if(isset($get_wallet_details) && $get_wallet_details!='false' || $get_wallet_details != false)
				            {

				              $mangopay_wallet_details = $get_wallet_details;
				            }

				            $wallet_amount= 0;
				            if(isset($mangopay_wallet_details->Balance->Amount) && $mangopay_wallet_details->Balance->Amount != "")
				            {
				               $wallet_amount= $mangopay_wallet_details->Balance->Amount/100;
				            }
				            $string_amount = str_replace(",","",$arr_transaction['paymen_amount']);
				            $amount_int    = (int) $string_amount;
				            
			              	$admin_mp_details  = get_user_wallet_details('1',$arr_payment_details['currency_code']);
				            //dd($amount_int,$wallet_amount);
				            if($amount_int > $wallet_amount)
				            {

				                    if(isset($mp_details) && count($mp_details)>0)
				                    {
				                      $transaction_inp['mp_user_id']      = isset($mp_details['mp_user_id'])?$mp_details['mp_user_id']:'';
				                      $transaction_inp['mp_wallet_id']    = isset($mp_details['mp_wallet_id'])?$mp_details['mp_wallet_id']:'';
				                      $transaction_inp['ReturnURL']       = url('/client/projects/payment/return/return?invoice_id='.$invoice_id.'&amount_int='.$amount_int.'&currency_code='.$arr_payment_details['currency_code'].'&paid_services='.$paid_services.'&user_id='.$admin_data['id']);
				                      $transaction_inp['currency_code']   = $arr_payment_details['currency_code'];
				                      
				                    }
				                    $amount             = (float) $amount_int- (float)$wallet_amount;

				                    // $obj_data = $this->CurrencyModel->with('deposite')
				                    //                                 ->where('currency_code',$arr_payment_details['currency_code'])
				                    //                                 ->first();
				                    // if($obj_data)
				                    // {
				                    //     $arr_data = $obj_data->toArray();
				                    //     $min_amount = isset($arr_data['deposite']['min_amount'])?$arr_data['deposite']['min_amount']:'';
				                    //     $max_amount = isset($arr_data['deposite']['max_amount'])?$arr_data['deposite']['max_amount']:'';
				                    //     $min_charge = isset($arr_data['deposite']['min_amount_charge'])?$arr_data['deposite']['min_amount_charge']:'';
				                    //     $max_charge = isset($arr_data['deposite']['max_amount_charge'])?$arr_data['deposite']['max_amount_charge']:'';
				                        
				                    //     if($amount>$min_amount)
				                    //     {
				                    //         $service_charge_payin = (float)$amount * (float)$max_charge/100;
				                    //     }
				                    //     else
				                    //     {
				                    //         $service_charge_payin =  (float) $min_charge;
				                    //     }
				                    //     $total_amount = (float)$amount + (float)$service_charge_payin;
				                    //     //$total_amount = (float)$total_amount - (float)$wallet_amount;
				                    // }

				                    $transaction_inp['amount']          = $amount;//$total_amount;
				                    $transaction_inp['fee']             = 0;//(int)$service_charge_payin;

				                    //dd($transaction_inp);
				                    $mp_add_money_in_wallet             = $this->WalletService->payInWallet($transaction_inp);
				                    //dd($mp_add_money_in_wallet);
				                    if(isset($mp_add_money_in_wallet->Status) && $mp_add_money_in_wallet->Status == 'CREATED')
				                    {   
				                        Redirect::to($mp_add_money_in_wallet->ExecutionDetails->RedirectURL)->send();
				                    } 
				                    else 
				                    {
				                      
				                      if(isset($mp_add_money_in_wallet) && $mp_add_money_in_wallet == false){
				                        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
				                        return redirect()->back();
				                      }else{
				                        Session::flash('error',$mp_add_money_in_wallet);
				                        return redirect()->back();
				                      }
				                    }
				            }
				            else
				            {
		                        $transaction_inp['tag']                      = $arr_transaction['invoice_id'].'- Job Service fee';
		                        $transaction_inp['debited_UserId']           = $mp_details['mp_user_id']; // client user id
		                        $transaction_inp['debited_walletId']         = (string)$mp_details['mp_wallet_id'];

		                        $transaction_inp['credited_UserId']          = $admin_mp_details['mp_user_id'];
		                        $transaction_inp['credited_walletId']        = (string)$admin_mp_details['mp_wallet_id'];
		                        $transaction_inp['total_pay']                = $amount_int;
		                        $transaction_inp['currency_code']            = $arr_payment_details['currency_code'];

	 	                        $transaction_inp['cost_website_commission']  = '0';

						  		$pay_fees        = $this->WalletService->walletTransfer($transaction_inp);
						  		$project         = [];

						  		if(isset($pay_fees->Status) && $pay_fees->Status == 'SUCCEEDED')
						  		{
						  		   	// update project details
						  		   	$get_project = $this->ProjectpostModel->where('invoice_id',$arr_transaction['invoice_id'])->first(['id','project_handle_by','paid_services']);
	                               	if(isset($get_project))
	                               	{
	                               		$project = $get_project->toArray();

		                               	if(isset($paid_services) && $paid_services!='')
		                               	{
		                               		$new_string = $project['paid_services'].','.$paid_services;
		                               		$this->ProjectpostModel->where('id',$project['id'])->update(['paid_services'=>$new_string]);
		                               	}
		                            }

	                               	if(isset($project['project_handle_by']) && $project['project_handle_by']=="2")
	                               	{
		                           		$update_project = $this->ProjectpostModel->where('invoice_id',$arr_transaction['invoice_id'])->update(['project_status'=>'1']);
	                               	} 
	                               	else if(isset($project['project_handle_by']) && $project['project_handle_by']=="1")
	                               	{
		                           		$update_project = $this->ProjectpostModel->where('invoice_id',$arr_transaction['invoice_id'])->update(['project_status'=>'2']);
	                               	}
	                               	// end update project details
	                               
	                               	// update transaction details
		                            $update_transaction   = [];
		                            $update_transaction['WalletTransactionId']    = isset($pay_fees->Id) ? $pay_fees->Id : '0';
		                            $update_transaction['payment_status']         = 2; // paid
		                            $update_transaction['response_data']          = json_encode($pay_fees); // paid
			  	                    $updatetransaction = $this->TransactionsModel->where('invoice_id',$arr_transaction['invoice_id'])->update($update_transaction); 
			  	                   	// end update transaction details    

			  	                   	// send notification to admin
			  	                    $arr_admin_data                         =  [];
						            $arr_admin_data['user_id']              =  $admin_data['id'];
						            $arr_admin_data['user_type']            = '1';
						            $arr_admin_data['url']                  = 'admin/wallet/archexpert';
						            $arr_admin_data['project_id']           = '';
						            $arr_admin_data['notification_text_en'] = $arr_transaction['invoice_id'].'-'.Lang::get('controller_translations.project_service_fee_paid',[],'en','en').' '.$pay_fees->Id;
						            $arr_admin_data['notification_text_de'] = $arr_transaction['invoice_id'].'-'.Lang::get('controller_translations.project_service_fee_paid',[],'de','en').' '.$pay_fees->Id;
						            $this->NotificationsModel->create($arr_admin_data); 
			  	                   	// end send notification to admin    

		                           	Session::flash('payment_success_msg',trans('controller_translations.msg_payment_transaction_for_post_project_is_successfully'));
								   	Session::flash('payment_return_url','/client/projects/posted');
							       	$this->MailService->ProjectPaymentMail($arr_transaction['invoice_id']);
							       	Redirect::to($this->payment_success_url)->send();
						  		} 
						  		else
						  		{
						  		   if(isset($pay_fees->ResultMessage)){
						  		   Session::flash('payment_error_msg',$pay_fees->ResultMessage);	
						  		   } else {
                                     if(gettype($pay_fees) == 'string'){
                                     	Session::flash('payment_error_msg',$pay_fees);
                                     }
						  		   }
		                           return redirect()->back();
						  		}
				            }
                        }else {
                            if(isset($admin_data)){
	                            // send notification to admin
				  	              $arr_admin_data                         =  [];
					              $arr_admin_data['user_id']              =  $admin_data['id'];
					              $arr_admin_data['user_type']            = '1';
					              $arr_admin_data['url']                  = 'admin/wallet/archexpert';
					              $arr_admin_data['project_id']           = '';
					              $arr_admin_data['notification_text_en'] = Lang::get('controller_translations.create_wallet',[],'en','en');
					              $arr_admin_data['notification_text_de'] = Lang::get('controller_translations.create_wallet',[],'de','en');
					              $this->NotificationsModel->create($arr_admin_data);      
				  	            // end send notification to admin 	
                            }
				  		    Session::flash('payment_error_msg',trans('controller_translations.admin_dont_have_wallet_created_yet'));	
                            return redirect()->back(); 
				  		}
				  	}
				}
			}
		}
	}

	public function project_manager_payment($arr_payment_details)
	{
		$arr_transaction   = array();
		$obj_site_settings = FALSE;
		$invoice_id        = "";
		if (isset($arr_payment_details['payment_obj_id']) && isset($arr_payment_details['payment_method'])) 
		{
			/*chk_already_transaction_done */
			$chk_already_transaction_done = $this->ProjectpostModel->where('id',$arr_payment_details['payment_obj_id'])->first(['invoice_id']);
            if(isset($chk_already_transaction_done) && $chk_already_transaction_done !="null"){
            	$transaction_done = $chk_already_transaction_done->toArray();
                if(isset($transaction_done['invoice_id']) && $transaction_done['invoice_id'] != "" || $transaction_done['invoice_id'] != null){
	                Session::flash('payment_error_msg',trans('controller_translations.this_transaction_already_done'));	
			        return redirect()->back();
                }
            }
            /* chk_already_transaction_done */

			/*$arr_payment_details['payment_obj_id'] is project id in this service;*/
			/* Payment method :1-paypal 2-Stripe $arr_payment_details['payment_method']; */
		  	$invoice_id	= $this->_generate_invoice_id();
		  	/*First make transaction  */ 
		  	$arr_transaction['user_id']          = $this->user_id;
		  	$arr_transaction['invoice_id']       = $invoice_id;
		  	/*transaction_type is type for transactions 1-Subscription 2-Milestone 3-Release Milestones 5- Project Payment */
		  	$arr_transaction['transaction_type'] = 5;
		  	$arr_transaction['paymen_amount']    = $arr_payment_details['project_services_cost'];
		  	$arr_transaction['payment_method']   = $arr_payment_details['payment_method'];
		  	$arr_transaction['payment_date']     = date('c');
		  	/* get currency for transaction using helper function */
		  	$arr_currency = setCurrencyForTransaction($arr_payment_details['payment_obj_id']);
		  	$arr_transaction['currency']         = isset($arr_currency['currency']) ? $arr_currency['currency'] : '$';
		  	$arr_transaction['currency_code']    = isset($arr_currency['currency_code']) ? $arr_currency['currency_code'] : 'USD';
		  	$transaction = $this->TransactionsModel->create($arr_transaction);
		  	if($transaction) 
		  	{ 	
		  		/*update invoice_id  in project tabel*/
				$update_status = $this->ProjectpostModel->where('id',$arr_payment_details['payment_obj_id'])->update(['invoice_id' =>$invoice_id]);
				if ($update_status) 
				{
					/* Now redirect to payment gateway $arr_payment_details[payment_method] is Paypal and Stripe  1-Paypal 2-Stripe 3 wallet */
				  	if (isset($arr_payment_details['payment_method']) && $arr_payment_details['payment_method']==1) // paypal 
				  	{
				  		if(!$this->PaypalService==FALSE)
						{
						 	$this->PaypalService->postPayment($invoice_id);
						}
				  	}
				  	elseif(isset($arr_payment_details['payment_method']) && $arr_payment_details['payment_method']==2) // stripe 
				  	{
				  		if(!$this->StripeService==FALSE)
						{
						 	if (isset($arr_payment_details['cardNumber']) && isset($arr_payment_details['cardExpiryMonth']) && isset($arr_payment_details['cardExpiryYear'])) 
						 	{
						 		$card_details = array('number' => $arr_payment_details['cardNumber'], 'exp_month' => $arr_payment_details['cardExpiryMonth'], 'exp_year' => $arr_payment_details['cardExpiryYear']);
						 		$this->StripeService->postPayment($invoice_id,$card_details);
						 	}
						}
				  	}
				  	elseif (isset($arr_payment_details['payment_method']) && $arr_payment_details['payment_method']==3)  // wallet
				  	{
                        $admin_data                  = get_admin_email_address();
                        if(isset($admin_data) && isset($admin_data['mp_wallet_created']) && $admin_data['mp_wallet_created'] =='Yes'){
                    	    // check client wallet balance
				            $mangopay_wallet_details = [];
				            $get_wallet_details = $this->WalletService->get_wallet_details($this->mp_wallet_id);
				            if(isset($get_wallet_details) && $get_wallet_details!='false' || $get_wallet_details != false){
				              $mangopay_wallet_details = $get_wallet_details;
				            }
				            $wallet_amount= 0;
				            if(isset($mangopay_wallet_details->Balance->Amount) && $mangopay_wallet_details->Balance->Amount != ""){
				               $wallet_amount= $mangopay_wallet_details->Balance->Amount/100;
				            }
				            $string_amount = str_replace(",","",$arr_transaction['paymen_amount']);
				            $amount_int    = (int) $string_amount;

				            if($amount_int > $wallet_amount){
				              Session::flash('payment_error_msg',trans('controller_translations.your_wallet_amount_is_not_suffeciant_to_make_transaction'));
				              return redirect()->back();
				            } 
				            else 
				            {
		                        $transaction_inp['tag']                      = $arr_transaction['invoice_id'].'- Job project manager fee';
		                        $transaction_inp['debited_UserId']           = $this->mp_user_id;           // client user id
		                        $transaction_inp['credited_UserId']          = $admin_data['mp_user_id'];   // archexpert admin user id
		                        $transaction_inp['total_pay']                = $amount_int;                 // services amount 
		                        $transaction_inp['debited_walletId']         = (string)$this->mp_wallet_id; // client wallet id
		                        $transaction_inp['credited_walletId']        = (string)$admin_data['mp_wallet_id']; // archexpert admin wallet id
	 	                        $transaction_inp['cost_website_commission']  = '0';
						  		$pay_fees        = $this->WalletService->walletTransfer($transaction_inp);
						  		$project         = [];
						  		if(isset($pay_fees->Status) && $pay_fees->Status == 'SUCCEEDED')
						  		{
						  		   
						  		   // update project details
							  		   $this->ProjectpostModel->where('invoice_id',$arr_transaction['invoice_id'])->update(['project_handle_by_pm'=>'1']);
	                               // end update project details
	                               
	                               // update transaction details
		                               $update_transaction   = [];
		                               $update_transaction['WalletTransactionId']    = isset($pay_fees->Id) ? $pay_fees->Id : '0';
		                               $update_transaction['payment_status']         = 2; // paid
		                               $update_transaction['response_data']          = json_encode($pay_fees); // paid
			  	                       $updatetransaction = $this->TransactionsModel->where('invoice_id',$arr_transaction['invoice_id'])->update($update_transaction); 
			  	                   // end update transaction details    

			  	                   // send notification to admin
			  	                      $arr_admin_data                         =  [];
						              $arr_admin_data['user_id']              =  $admin_data['id'];
						              $arr_admin_data['user_type']            = '1';
						              $arr_admin_data['url']                  = 'admin/wallet/archexpert';
						              $arr_admin_data['project_id']           = '';
						              $arr_admin_data['notification_text_en'] = $arr_transaction['invoice_id'].'-'.Lang::get('controller_translations.project_manager_fee_paid',[],'en','en').' '.$pay_fees->Id;
						              $arr_admin_data['notification_text_de'] = $arr_transaction['invoice_id'].'-'.Lang::get('controller_translations.project_manager_fee_paid',[],'de','en').' '.$pay_fees->Id;
						              $this->NotificationsModel->create($arr_admin_data); 
			  	                    // end send notification to admin    

						            //Admin notification for assign project manager
						            $arr_data['user_id']              =  $admin_data['id'];
									$arr_data['url']                  = "admin/projects/posted";
									$arr_data['notification_text_en'] = Lang::get('controller_translations.assign_project_manager',[],'en','en');
									$arr_data['notification_text_de'] = Lang::get('controller_translations.assign_project_manager',[],'de','en');
									$arr_data['project_id'] = $arr_payment_details['payment_obj_id'];
									$this->NotificationsModel->create($arr_data);
									//End of Admin notification for assign project manager

		                           	Session::flash('payment_success_msg',trans('controller_translations.msg_payment_transaction_for_assign_project_manager'));

									Session::flash('payment_return_url','/client/projects/ongoing');

									$this->MailService->ProjectPaymentMail($arr_transaction['invoice_id']);
									
									Redirect::to($this->payment_success_url)->send();
						  		} 
						  		else 
						  		{
						  		   if(isset($pay_fees->ResultMessage)){
						  		   Session::flash('payment_error_msg',$pay_fees->ResultMessage);	
						  		   } 
						  		   else 
						  		   {
                                     if(gettype($pay_fees) == 'string'){
                                     	Session::flash('payment_error_msg',$pay_fees);
                                     }
						  		   }
		                           return redirect()->back();
						  		}
				            }
                        }
                        else 
                        {
                            if(isset($admin_data))
                            {
	                            // send notification to admin
				  	              $arr_admin_data                         =  [];
					              $arr_admin_data['user_id']              =  $admin_data['id'];
					              $arr_admin_data['user_type']            = '1';
					              $arr_admin_data['url']                  = 'admin/wallet/archexpert';
					              $arr_admin_data['project_id']           = '';
					              $arr_admin_data['notification_text_en'] = Lang::get('controller_translations.create_wallet',[],'en','en');
					              $arr_admin_data['notification_text_de'] = Lang::get('controller_translations.create_wallet',[],'de','en');
					              $this->NotificationsModel->create($arr_admin_data);      
				  	            // end send notification to admin 	
                            }
				  		    Session::flash('payment_error_msg',trans('controller_translations.admin_dont_have_wallet_created_yet'));	
                            return redirect()->back(); 
				  		}
				  	}
				}
			}
		}
	}

    private function _generate_invoice_id()
    {
   		$secure      = TRUE;    
        $bytes       = openssl_random_pseudo_bytes(3, $secure);
        $order_token = 'INV'.date('Ymd').strtoupper(bin2hex($bytes));
        return $order_token;
    }
}	
?>