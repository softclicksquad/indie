<?php
namespace App\Common\Services;
require_once base_path('vendor/mangopay/php-sdk-v2/MangoPay/Autoloader.php');
require_once base_path('vendor/mangopay/php-sdk-v2/demos/workflow/inc/mockStorage.php');
use App\Models\UserModel;
use App\Models\UserWalletModel;

use Mail;
use Exception;
use Session;

class WalletService
{
	public function __construct(){
		$this->UserWalletModel = new UserWalletModel;
		$this->UserModel       = new UserModel;

	}
	//-------------------------------------------------------------------------------------------
	// Method used: MangoPay PayIn
	// PARAMETERS: 	User details
	// user creation (UserNatural)
	// card registration (CardRegistration)
	// save cards (Client side)
	// -------------------------------------------------------------------------------------------
    

    // Admin ( Client ) services
    public function get_client_details()
    { // owner
		try{
			// create instance of MangoPayApi
			$mangoPayApi = new \MangoPay\MangoPayApi();
			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
			
			$ClientDetails = $mangoPayApi->Clients->Get();
			if(isset($ClientDetails) && count($ClientDetails)>0){
				return $ClientDetails;
			}else{
				return false;
			}
		}
		catch(\Exception $e){
			return false;
		}
	}

	public function card_preregistration($preregData=NULL)
	{
		// dd($preregData,'sudarshan');
		try
		{
			// create instance of MangoPayApi
			$mangoPayApi = new \MangoPay\MangoPayApi();
			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
			$cardRegister = new \MangoPay\CardRegistration();

			// register card
			$cardRegister->UserId   = $preregData['user_id'];		//$createdUser->Id;
			$cardRegister->Currency = 'EUR';//$preregData['currency_code'];//config('app.project.Currency'); 	//'EUR';
			$cardRegister->CardType = $preregData['card_type'];

			$createdCardRegister = $mangoPayApi->CardRegistrations->Create($cardRegister);
		    //dd($createdCardRegister);
			$data['cardRegistrationURL']   = $createdCardRegister->CardRegistrationURL;
			$data['preregistrationData']   = $createdCardRegister->PreregistrationData;
			$data['accessKey']             = $createdCardRegister->AccessKey;
			$data['clientId']              = $mangoPayApi->Config->ClientId;
			$data['baseURL']               = config('app.project.MangoPay.URL');
			$data['cardPreregistrationId'] = $createdCardRegister->Id;
			$data['cardType']              = $cardRegister->CardType;
			$data['status']                = $createdCardRegister->Status;

			return $data;
		}
		catch(MangoPay\Libraries\ResponseException $e){
			//dd($e);
			// handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
			$message = "Exception : ".$e->GetCode()."| Message: ".$e->GetMessage()."| Error(s) ".$e->GetErrorDetails();
			return $message;
		} catch(MangoPay\Libraries\Exception $e) {
			// handle/log the exception $e->GetMessage()
			//dd($e);
			$message = "Message: ".$e->GetMessage();
		}catch(\Exception $e){
			//dd($e);
			$message = $e->getMessage();
			return $message;
		}
	}


	public function card_registration($preregData=NULL)
	{
		// dd($preregData,'sudarshan');
		try
		{
			// create instance of MangoPayApi
			$mangoPayApi = new \MangoPay\MangoPayApi();
			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
			$cardRegister = new \MangoPay\CardRegistration();

			// register card
			$cardRegister->UserId   = $preregData['user_id'];		//$createdUser->Id;
			$cardRegister->Currency = 'EUR';//$preregData['currency_code'];//config('app.project.Currency'); 	//'EUR';
			$cardRegister->CardType = $preregData['card_type'];

			$createdCardRegister = $mangoPayApi->CardRegistrations->Create($cardRegister);
		    //dd($createdCardRegister);
			$data['cardRegistrationURL']   = $createdCardRegister->CardRegistrationURL;
			$data['preregistrationData']   = $createdCardRegister->PreregistrationData;
			$data['accessKey']             = $createdCardRegister->AccessKey;
			$data['clientId']              = $mangoPayApi->Config->ClientId;
			$data['baseURL']               = config('app.project.MangoPay.URL');
			$data['cardPreregistrationId'] = $createdCardRegister->Id;
			$data['cardType']              = $cardRegister->CardType;
			$data['status']                = $createdCardRegister->Status;

			return $data;
		}
		catch(MangoPay\Libraries\ResponseException $e){
			//dd($e);
			// handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
			$message = "Exception : ".$e->GetCode()."| Message: ".$e->GetMessage()."| Error(s) ".$e->GetErrorDetails();
			return $message;
		} catch(MangoPay\Libraries\Exception $e) {
			// handle/log the exception $e->GetMessage()
			//dd($e);
			$message = "Message: ".$e->GetMessage();
		}catch(\Exception $e){
			//dd($e);
			$message = $e->getMessage();
			return $message;
		}
	}

	public function card_preregistration_data($data=NULL)
	{
		try {

				$mangoPayApi = new \MangoPay\MangoPayApi();
				$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
				$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
				$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
				$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL'); //$data['cardRegistrationURL'];
                

                $cardRegister = $mangoPayApi->CardRegistrations->Get($data['cardRegisterId']);
			    
			    $cardRegister->RegistrationData =$data['data'];
			    $updatedCardRegister = $mangoPayApi->CardRegistrations->Update($cardRegister);

			    $data['UserId']                = $updatedCardRegister->UserId;
				$data['CardType']              = $updatedCardRegister->CardType;
				$data['AccessKey']             = $updatedCardRegister->AccessKey;
				$data['CardId']                = $updatedCardRegister->CardId;
				$data['Status']                = $updatedCardRegister->Status;
				$data['Id']                    = $updatedCardRegister->Id;
			    return $data;
				// $mangoPayApi = new \MangoPay\MangoPayApi();
				// $mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
				// $mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
				// $mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
				// $mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
				// $cardRegister = new \MangoPay\CardRegistration();
			 //    // update register card with registration data from Payline service
			 //    $cardRegister = $mangoPayApi->CardRegistrations->Get($data['cardRegisterId']);
			 //    $cardRegister->RegistrationData = isset($data['data'])?'data='.$data['data']:'';
			 //    return $updatedCardRegister = $mangoPayApi->CardRegistrations->Update($cardRegister);

			    // if ($updatedCardRegister->Status != \MangoPay\CardRegistrationStatus::Validated || !isset($updatedCardRegister->CardId))
			    //     die('<div style="color:red;">Cannot create card. Payment has not been created.<div>');

			    // // get created virtual card object
			    // $card = $mangoPayApi->Cards->Get($updatedCardRegister->CardId);

			    // // create temporary wallet for user
			    // $wallet = new \MangoPay\Wallet();
			    // $wallet->Owners = array( $updatedCardRegister->UserId );
			    // $wallet->Currency = 'EUR';
			    // $wallet->Description = 'Temporary wallet for payment demo';
			    // $createdWallet = $mangoPayApi->Wallets->Create($wallet);

			    // // create pay-in CARD DIRECT
			    // $payIn = new \MangoPay\PayIn();
			    // $payIn->CreditedWalletId = $createdWallet->Id;
			    // $payIn->AuthorId = $updatedCardRegister->UserId;
			    // $payIn->DebitedFunds = new \MangoPay\Money();
			    // $payIn->DebitedFunds->Amount = $_SESSION['amount'];
			    // $payIn->DebitedFunds->Currency = $_SESSION['currency'];
			    // $payIn->Fees = new \MangoPay\Money();
			    // $payIn->Fees->Amount = 0;
			    // $payIn->Fees->Currency = $_SESSION['currency'];

			    // // payment type as CARD
			    // $payIn->PaymentDetails = new \MangoPay\PayInPaymentDetailsCard();
			    // $payIn->PaymentDetails->CardType = $card->CardType;
			    // $payIn->PaymentDetails->CardId = $card->Id;

			    // // execution type as DIRECT
			    // $payIn->ExecutionDetails = new \MangoPay\PayInExecutionDetailsDirect();
			    // $payIn->ExecutionDetails->SecureModeReturnURL = 'http://test.com';

			    // // create Pay-In
			    // return $createdPayIn = $mangoPayApi->PayIns->Create($payIn);

			    // // if created Pay-in object has status SUCCEEDED it's mean that all is fine
			    // if ($createdPayIn->Status == \MangoPay\PayInStatus::Succeeded) {
			    //     print '<div style="color:green;">'.
			    //                 'Pay-In has been created successfully. '
			    //                 .'Pay-In Id = ' . $createdPayIn->Id 
			    //                 . ', Wallet Id = ' . $createdWallet->Id 
			    //             . '</div>';
			    // }
			    // else {
			    //     // if created Pay-in object has status different than SUCCEEDED 
			    //     // that occurred error and display error message
			    //     print '<div style="color:red;">'.
			    //                 'Pay-In has been created with status: ' 
			    //                 . $createdPayIn->Status . ' (result code: '
			    //                 . $createdPayIn->ResultCode . ')'
			    //             .'</div>';
			    // }

			} catch (\MangoPay\Libraries\ResponseException $e) {
			   return $e->GetMessage();
			}
	}

	public function get_cards_list($UserId=NULL)
	{
		try
		{
			// create instance of MangoPayApi
			$mangoPayApi = new \MangoPay\MangoPayApi();
			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL'); //$data['cardRegistrationURL'];
			
			if($UserId != NULL)
			{
				$CardsList = $mangoPayApi->Users->GetCards($UserId);
				if(isset($CardsList) && count($CardsList)>0)
				{
					return $CardsList;
				}else
				{
					return false;
				}
			}else
			{
				return false;
			}
		}
		catch(\Exception $e){
			$message = $e->getMessage();
			return false;
		}
	}

	// public function card_registration_success($data=NULL)
	// {
	// 	try {
	// 			$mangoPayApi = new \MangoPay\MangoPayApi();
	// 			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
	// 			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
	// 			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
	// 			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
 //                $CardRegistration = new \MangoPay\CardRegistration();
 //                $CardRegistration->Id = '75979326';
 //                $CardRegistration->RegistrationData = 'data=VLTIjgpf1ag15dmwRyhmOV3LlhlR4aUCu2wL82eh9Vk9ruccUQ-bCxqGrLufRsKE87kxa9zg6WPUciBaTsj7uceAQNOLCPPTEqWZiRt1-OlmLKqE5z-TeAVx2UEXAl-t';

 //                $Result = $mangoPayApi->CardRegistrations->Update($CardRegistration);
 //                dd($Result);

 //            } catch(MangoPay\Libraries\ResponseException $e) {
 //            	dd($e);
 //            // handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()

 //            } catch(MangoPay\Libraries\Exception $e) {
 //            	dd($e);
 //            // handle/log the exception $e->GetMessage()

 //            }
	// }

	public function get_client_wallet_details() 
	{ // owner
		try{
			// create instance of MangoPayApi
			$mangoPayApi = new \MangoPay\MangoPayApi();
			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
			$ClientWalletDetails = $mangoPayApi->Clients->GetWallets();
			if(isset($ClientWalletDetails) && count($ClientWalletDetails)>0){
				return $ClientWalletDetails;
			}else{
				return false;
			}
		}
		catch(\Exception $e){
			return false;
		}
	}
	// eND Admin ( Client ) services 

	/*Create natural or Individual user*/
	public function create_user($user_data = NULL)
	{
		try{
			if ($user_data != NULL) {
				// create instance of MangoPayApi
				$mangoPayApi                          = new \MangoPay\MangoPayApi();
				$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
				$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
				$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
				$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
				$User = new \MangoPay\UserNatural();

				// create user for payment
				$User->Email               = $user_data['Email'];
				$User->FirstName           = $user_data['FirstName'];
				$User->LastName            = $user_data['LastName'];
				$User->Birthday            = time();
				$User->Nationality         = $user_data['Nationality'];
				$User->CountryOfResidence  = $user_data['CountryOfResidence'];

				$createdUser               = $mangoPayApi->Users->Create($User);


				$arr_data['createdUserId'] = $createdUser->Id;
				$Wallet_name = 'User Wallet';
				if(isset($user_data['Wallet_name']) && $user_data['Wallet_name'] !=""){
					$Wallet_name = $user_data['Wallet_name'];
				}
				$currency_code = $user_data['currency_code'];

				//$AppWallet = $this->create_wallet($createdUser->Id,$Wallet_name);
				$AppWallet = $this->create_wallet_currency_wise($createdUser->Id,$Wallet_name,$currency_code);

				$arr_data['AppWalletId']   = $AppWallet->Id;
				if (isset($createdUser) && count($createdUser)>0 && isset($AppWallet) && count($AppWallet)>0) {
					return $arr_data;
				}else{
					return false;
				}
			}else{
				return false;
			}
		} catch(\Exception $e) {
			$excp_message = "Exception : ".$e->GetCode()."| Message: ".$e->GetMessage();
            try{
			        Mail::raw($excp_message, function($message){
					    $message->from('no-reply@archexperts.com', config('app.project.name'));
					    $message->to('webwingt@gmail.com');
					   	$message->subject('Mangopay : Error');
					});
			} catch(\Exception $e) {
				return false;
			}
			return false;
		}
	}

	public function create_user_with_all_currency_wallet($user_data = NULL,$arr_currency=NULL,$user_id)
	{
		try{
			if ($user_data != NULL) {
				// create instance of MangoPayApi
				$mangoPayApi                          = new \MangoPay\MangoPayApi();
				$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
				$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
				$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
				$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
				$User = new \MangoPay\UserNatural();

				// create user for payment
				$User->Email               = $user_data['Email'];
				$User->FirstName           = $user_data['FirstName'];
				$User->LastName            = $user_data['LastName'];
				$User->Birthday            = time();
				$User->Nationality         = $user_data['Nationality'];
				$User->CountryOfResidence  = $user_data['CountryOfResidence'];

				$createdUser               = $mangoPayApi->Users->Create($User);


				$arr_data['createdUserId'] = $createdUser->Id;
				$Wallet_name = 'User Wallet';
				if(isset($user_data['Wallet_name']) && $user_data['Wallet_name'] !=""){
					$Wallet_name = $user_data['Wallet_name'];
				}
				//dd($arr_currency);
				foreach ($arr_currency as $key => $value) 
				{
					$currency_code = $value['currency_code'];
					$AppWallet = $this->create_wallet_currency_wise($createdUser->Id,$Wallet_name,$currency_code);
					$arr_data1['mp_wallet_created'] = "Yes";
                	$update = $this->UserModel->where('id',$user_id)->update($arr_data1);
					$arr_data2['mp_user_id']        = $createdUser->Id;
	                $arr_data2['mp_wallet_id']      = $AppWallet->Id;
	                $arr_data2['user_id']           = $user_id;
	                $arr_data2['currency_code']     = $currency_code;
	                $this->UserWalletModel->create($arr_data2);
	                
				}
				//$AppWallet = $this->create_wallet($createdUser->Id,$Wallet_name);
				if (isset($createdUser) && count($createdUser)>0 && isset($AppWallet) && count($AppWallet)>0) {
					return $arr_data;
				}else{
					return false;
				}
			}else{
				return false;
			}
		} catch(\Exception $e) {
			$excp_message = "Exception : ".$e->GetCode()."| Message: ".$e->GetMessage();
            try{
			        Mail::raw($excp_message, function($message){
					    $message->from('no-reply@archexperts.com', config('app.project.name'));
					    $message->to('webwingt@gmail.com');
					   	$message->subject('Mangopay : Error');
					});
			} catch(\Exception $e) {
				return false;
			}
			return false;
		}
	}

	/*Create legal user or Professional user*/
	public function create_legal_user($user_data = NULL,$arr_currency=NULL,$user_id)
	{
		try{
			if ($user_data != NULL) {
				// create instance of MangoPayApi
				$mangoPayApi                          = new \MangoPay\MangoPayApi();
				$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
				$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
				$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
				$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
				$User = new \MangoPay\UserLegal();

				// create user for payment
				$User->LegalPersonType                       = "BUSINESS";
				$User->Name                                  = $user_data['company_name'];  //company name
				$User->LegalRepresentativeBirthday           = time();
				$User->LegalRepresentativeCountryOfResidence = $user_data['CountryOfResidence'];
				$User->LegalRepresentativeNationality        = $user_data['Nationality'];
				$User->Email                                 = $user_data['Email'];
				$User->LegalRepresentativeFirstName          = $user_data['FirstName'];
				$User->LegalRepresentativeLastName           = $user_data['LastName'];

				$createdUser               = $mangoPayApi->Users->Create($User);

				$arr_data['createdUserId'] = $createdUser->Id;
				$Wallet_name = 'User Wallet';
				if(isset($user_data['Wallet_name']) && $user_data['Wallet_name'] !="")
				{
					$Wallet_name = $user_data['Wallet_name'];
				}

				foreach ($arr_currency as $key => $value) 
				{
					$currency_code = $value['currency_code'];
					$AppWallet = $this->create_wallet_currency_wise($createdUser->Id,$Wallet_name,$currency_code);
					$arr_data1['mp_wallet_created'] = "Yes";
                	$update = $this->UserModel->where('id',$user_id)->update($arr_data1);
					$arr_data2['mp_user_id']        = $createdUser->Id;
	                $arr_data2['mp_wallet_id']      = $AppWallet->Id;
	                $arr_data2['user_id']           = $user_id;
	                $arr_data2['currency_code']     = $currency_code;
	                $this->UserWalletModel->create($arr_data2);
	                
				}

				//$currency_code = $user_data['currency_code'];
				//$AppWallet     = $this->create_wallet_currency_wise($createdUser->Id,$Wallet_name,$currency_code);
				
				//$AppWallet = $this->create_wallet($createdUser->Id,$Wallet_name);

				//$arr_data['AppWalletId']   = $AppWallet->Id;
				if (isset($createdUser) && count($createdUser)>0 && isset($AppWallet) && count($AppWallet)>0) 
				{
					return $arr_data;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		} catch(\Exception $e) {
			$excp_message = "Exception : ".$e->GetCode()."| Message: ".$e->GetMessage();
            try{
			        Mail::raw($excp_message, function($message){
					    $message->from('no-reply@archexperts.com', config('app.project.name'));
					    $message->to('webwingt@gmail.com');
					   	$message->subject('Mangopay : Error');
					});
			} catch(\Exception $e) {
				return false;
			}
			return false;
		}
	}

	public function create_ubo($UserId=NULL) // Create ubo declaration
	{
		try
		{
			if($UserId!='')
			{

				$mangoPayApi = new \MangoPay\MangoPayApi();
				$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
				$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
				$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
				$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');

				$UboDeclaration = new \MangoPay\UboDeclaration();
				$Result = $mangoPayApi->UboDeclarations->Create($UserId, $UboDeclaration);
				// $UboDeclaration = new \MangoPay\UboDeclaration();
				// $Result = $mangoPayApi->UboDeclaration->Create($UserId, $UboDeclaration);
				//dd($Result);
				if(isset($Result->Status) && $Result->Status == 'CREATED')
				{
					return $Result;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
			
		} catch(Exception $e) {
			//dd('hereeee',$e);
			return false;

		// handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()

		} 
	}

	public function get_ubo($UserId=NULL) // Get list for ubo declaration
	{
		try{
			// create instance of MangoPayApi
			$mangoPayApi = new \MangoPay\MangoPayApi();
			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
			//get the doc
			$UboDeclaration = new \MangoPay\UboDeclaration();

			$mangopay_ubo_details = $mangoPayApi->UboDeclaration->GetAll($UserId);

			if($UserId != NULL)
			{
				if(isset($mangopay_ubo_details) && count($mangopay_ubo_details)>0)
				{
					return $mangopay_ubo_details;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		catch(\Exception $e){
			return false;
		}
	}

	public function add_UBO($arr_ubo = NULL) // Add user in particular UBO declaration.
	{
		try{
			if($arr_ubo != null)
			{
				// create instance of MangoPayApi
				$mangoPayApi                          = new \MangoPay\MangoPayApi();
				$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
				$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
				$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
				$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');


				$UserId = $arr_ubo['UserId'];
				$UboDeclarationId = $arr_ubo['ubo_declaration_id'];

				$UboDeclaration = new \MangoPay\UboDeclaration();

				$UboDeclaration->FirstName             = $arr_ubo['FirstName'];
				$UboDeclaration->LastName              = $arr_ubo['LastName'];
				$UboDeclaration->Address               = new \MangoPay\Address();
				$UboDeclaration->Address->AddressLine1 = $arr_ubo['Address'];
				$UboDeclaration->Address->AddressLine2 = $arr_ubo['Address1'];
				$UboDeclaration->Address->City         = $arr_ubo['City'];
				$UboDeclaration->Address->Region       = $arr_ubo['Region'];
				$UboDeclaration->Address->PostalCode   = $arr_ubo['PostalCode'];
				$UboDeclaration->Address->Country      = $arr_ubo['Country'];
				$UboDeclaration->Nationality           = $arr_ubo['Nationality'];
				$UboDeclaration->Birthday              = strtotime($arr_ubo['Birthday']);
				$UboDeclaration->Birthplace            = new \MangoPay\Birthplace();
				$UboDeclaration->Birthplace->City      = $arr_ubo['Birthplace'];
				$UboDeclaration->Birthplace->Country   = $arr_ubo['Birthplace_Country'];

				$mangoPayApi->UboDeclarations->CreateUbo($UserId,$UboDeclarationId,$UboDeclaration);

				return true;
			}
			else
			{
				return false;
            }
		}catch(\MangoPay\Libraries\ResponseException $e)
		{
			return false;
		} catch(\MangoPay\Libraries\Exception $e) 
		{
			return false;
		}catch(\Exception $e){
			return false;
		}
	}

	public function submit_ubo_declaration($UserId= NULL,$uboDeclarationId= NULL)
	{
		try
		{
			if($UserId!='' && $uboDeclarationId!='')
			{

				$mangoPayApi = new \MangoPay\MangoPayApi();

				$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
				$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
				$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
				$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');

				$UboDeclaration = new \MangoPay\UboDeclaration();

				$Result = $mangoPayApi->UboDeclarations->SubmitForValidation($UserId, $uboDeclarationId);

				if(isset($Result->Status) && $Result->Status == 'VALIDATION_ASKED')
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
			
		} catch(Exception $e) {
			return 0;
		// handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()

		} 
	}

	/*Update individual user*/
    public function update_user($user_data = NULL)
	{
		try
		{
			if ($user_data != NULL) 
			{
				$mangoPayApi                          = new \MangoPay\MangoPayApi();
				$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
				$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
				$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
				$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');

				$UserNatural = new \MangoPay\UserNatural();
				$UserNatural->Tag = "custom meta";
				$UserNatural->Email = isset($user_data['email'])?$user_data['email']:'';

				$UserNatural->FirstName = isset($user_data['first_name'])?$user_data['first_name']:'';
				$UserNatural->LastName = isset($user_data['last_name'])?$user_data['last_name']:'';
				
				$UserNatural->Nationality = isset($user_data['nationality'])?$user_data['nationality']:'';
				$UserNatural->CountryOfResidence = isset($user_data['residence'])?$user_data['residence']:'';

				$UserNatural->Id = isset($user_data['mp_user_id'])?$user_data['mp_user_id']:'';

				//$Result = $mangoPayApi->Users->Update($UserNatural);

				$updatedUser = $mangoPayApi->Users->Update($UserNatural);

				if (isset($updatedUser) && count($updatedUser) > 0 ) 
				{
					return true;
				}
				else
				{
					return false;
				}
			}else
			{
				return false;
			}
		} catch(\Exception $e) {
			$excp_message = "Exception : ".$e->GetCode()."| Message: ".$e->GetMessage();
			try{
			       Mail::raw($excp_message, function($message){
						    $message->from('no-reply@archexperts.com', config('app.project.name'));
						    $message->to('webwingt@gmail.com');
						   	$message->subject('Mangopay : Error');
						});
			} catch(\Exception $e) {
				return false;
			}
			return false;
		}
	}

	/*Update Professional or Business user*/
	public function update_legal_user($user_data = NULL)
	{
		try
		{
			if ($user_data != NULL) 
			{
				$mangoPayApi                          = new \MangoPay\MangoPayApi();
				$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
				$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
				$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
				$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');

				$UserLegal = new \MangoPay\UserLegal();
				$UserLegal->Tag = "custom meta";
				$UserLegal->LegalRepresentativeEmail = isset($user_data['email'])?$user_data['email']:'';

				$UserLegal->LegalRepresentativeFirstName = isset($user_data['first_name'])?$user_data['first_name']:'';
				$UserLegal->LegalRepresentativeLastName = isset($user_data['last_name'])?$user_data['last_name']:'';

				$UserLegal->Id = isset($user_data['mp_user_id'])?$user_data['mp_user_id']:'';


				$UserLegal->LegalRepresentativeNationality = isset($user_data['nationality'])?$user_data['nationality']:'';
				$UserLegal->LegalRepresentativeCountryOfResidence = isset($user_data['residence'])?$user_data['residence']:'';

				//$Result = $mangoPayApi->Users->Update($UserLegal);

				$updatedUser = $mangoPayApi->Users->Update($UserLegal);

				if (isset($updatedUser) && count($updatedUser) > 0 ) 
				{
					return true;
				}else
				{
					return false;
				}
			}else
			{
				return false;
			}
		} catch(\Exception $e) {
			$excp_message = "Exception : ".$e->GetCode()."| Message: ".$e->GetMessage();
			try{
			       Mail::raw($excp_message, function($message){
						    $message->from('no-reply@archexperts.com', config('app.project.name'));
						    $message->to('webwingt@gmail.com');
						   	$message->subject('Mangopay : Error');
						});
			} catch(\Exception $e) {
				return false;
			}
			return false;
		}
	}

	public function create_wallet($UserId=null,$Description = null)
	{
		if(isset($Description) && $Description!=""){
			$Description = $Description;
		} else {
            $Description = '';
		}
		try{
			// create instance of MangoPayApi
			$mangoPayApi = new \MangoPay\MangoPayApi();
			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');

			if($UserId != null){
				$wallet              = new \MangoPay\Wallet();
				$wallet->Owners      = array($UserId);
				$wallet->Currency    = config('app.project_currency.$');
				$wallet->Description = $Description;
				return $mangoPayApi->Wallets->Create($wallet);
			}else{
				return false;
            }
		}catch(\Exception $e){
			return false;
		}
	}

	public function create_wallet_currency_wise($UserId=null,$Description = null,$Currency = null)
	{
		if(isset($Description) && $Description!=""){
			$Description = $Description;
		} else {
            $Description = '';
		}
		try{
			// create instance of MangoPayApi
			$mangoPayApi = new \MangoPay\MangoPayApi();
			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
			if($UserId != null){
				$wallet              = new \MangoPay\Wallet();
				$wallet->Owners      = array($UserId);
				$wallet->Currency    = $Currency;
				$wallet->Description = $Description;
				//$arrr = $mangoPayApi->Wallets->Create($wallet);
				return $mangoPayApi->Wallets->Create($wallet);
			}else{
				return false;
            }
		}catch(\Exception $e){
			return false;
		}

		// if(isset($Description) && $Description!="")
		// {
		//      $Description = $Description;
		// }
		// else 
		// {
  		//     $Description = '';
		// }
		
		// try
		// {
		// 	// create instance of MangoPayApi
		// 	$mangoPayApi = new \MangoPay\MangoPayApi();
		// 	$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
		// 	$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
		// 	$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
		// 	$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');

		// 	if($UserId != null)
		// 	{
		// 		$wallet              = new \MangoPay\Wallet();
		// 		$wallet->Owners      = array($UserId);
		// 		//$wallet->Currency    = config('app.project_currency.$');
		// 		$wallet->Currency    = $Currency;
		// 		$wallet->Description = $Description;
		// 		return $mangoPayApi->Wallets->Create($wallet);
		// 	}
		// 	else
		// 	{
		// 		return false;
  		//  }
		// }
		// catch(\Exception $e)
		// {
		// 	return false;
		// }
	}

	public function createBankAccount($transaction_inp=null)
	{
		try{
			if($transaction_inp != null){
				// create instance of MangoPayApi
				$mangoPayApi                          = new \MangoPay\MangoPayApi();
				$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
				$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
				$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
				$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
				$account = new \MangoPay\BankAccount();
				$account->OwnerName = $transaction_inp['FirstName'] . ' ' . $transaction_inp['LastName'];
				$Address = new \MangoPay\Address();
				$Address->AddressLine1 	= $transaction_inp['Address'];
				$Address->City    		= $transaction_inp['City'];
				$Address->Region 		= $transaction_inp['Region'];
				$Address->PostalCode 	= $transaction_inp['PostalCode'];
				$Address->Country 		= $transaction_inp['Country'];
				$account->OwnerAddress  = $Address;
				if($transaction_inp['bank_type']        == 'IBAN'){
				$account->Type                          = "IBAN";
				$account->Details                       = new \MangoPay\BankAccountDetailsIBAN();
				$account->Details->IBAN                 = $transaction_inp['IBAN']; // FR7618829754160173622224154
				$account->Details->BIC                  = $transaction_inp['BIC'];  // CMBRFR2BCME
				} else if($transaction_inp['bank_type'] == 'GB'){
				$account->Type                          = "GB";
				$account->Details                       = new \MangoPay\BankAccountDetailsGB();
				$account->Details->SortCode             = $transaction_inp['gbSortCode']; // 400515
				$account->Details->AccountNumber        = $transaction_inp['gbAccountNumber']; //11696419
				} else if($transaction_inp['bank_type'] == 'US'){
				$account->Type                          = "US";
				$account->Details                       = new \MangoPay\BankAccountDetailsUS();
				$account->Details->AccountNumber        = $transaction_inp['usAccountNumber']; //11696419
				$account->Details->DepositAccountType   = $transaction_inp['usDepositAccountType']; //071000288
				$account->Details->ABA                  = $transaction_inp['usABA']; //CHECKING
				} else if($transaction_inp['bank_type'] == 'CA'){
				$account->Type                          = "CA";
				$account->Details                       = new \MangoPay\BankAccountDetailsCA();
				$account->Details->AccountNumber        = $transaction_inp['caAccountNumber']; //11696419
				$account->Details->BranchCode           = $transaction_inp['caBranchCode']; //00152
				$account->Details->BankName             = $transaction_inp['caBankName']; // The Big Bank
				$account->Details->InstitutionNumber    = $transaction_inp['caInstitutionNumber']; //614
				} else if($transaction_inp['bank_type'] == 'OTHER'){
				$account->Type                          = "OTHER";
				$account->Details                       = new \MangoPay\BankAccountDetailsOTHER();
				$account->Details->AccountNumber        = $transaction_inp['otherAccountNumber']; //11696419
				$account->Details->Country              = $transaction_inp['Country']; //IN
				$account->Details->BIC                  = $transaction_inp['otherBIC']; //CRLYFRPP
			    }
				/*return */$mangoPayApi->Users->CreateBankAccount($transaction_inp['UserId'], $account);
				return true;
			}else{
				return false;
            }
		}catch(\MangoPay\Libraries\ResponseException $e){
			// handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
			return "Exception : ".$e->GetCode()."| Message: ".$e->GetMessage()."| Error(s) ".$e->GetErrorDetails();
		} catch(\MangoPay\Libraries\Exception $e) {
			// handle/log the exception $e->GetMessage()
			return "Message: ".$e->GetMessage();
		}catch(\Exception $e){
			return $e->getMessage();
		}
	}
	
	public function walletTransfer($transaction_inp=null)
	{	
		$is_contest_refund_cron = '0';
		if(isset($transaction_inp['is_contest_refund_cron']) && $transaction_inp['is_contest_refund_cron'] == '1') {
			$is_contest_refund_cron = '1';
		}

		Session::forget('arr_data_bal');
        Session::forget('get_mangopay_wallet_details_expert');
        Session::forget('mangopay_transaction_details_expert');
        Session::forget('get_mangopay_wallet_details_client');
        Session::forget('mangopay_transaction_details_client');
		//dd($transaction_inp);
		try{
			// create instance of MangoPayApi
			$mangoPayApi = new \MangoPay\MangoPayApi();
			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');

			$total_pay               = isset($transaction_inp['total_pay']) ? $transaction_inp['total_pay'] : 0;
			$cost_website_commission = isset($transaction_inp['cost_website_commission']) ? $transaction_inp['cost_website_commission'] : 0;

			// for JPY currency we don't need to multiple by 100
			if($transaction_inp['currency_code']!='JPY'){
				$total_pay               = $total_pay*100;
				$cost_website_commission = $cost_website_commission*100;
			}

            if($transaction_inp != null){
		        $transfer 					        = new \MangoPay\Transfer();
		        $transfer->Tag 				        = $transaction_inp['tag'];					// config('app.project.name') Order orderId
		        $transfer->AuthorId 		        = $transaction_inp['debited_UserId'];		// Wallet owner UserId (user)
		        $transfer->CreditedUserId       	= $transaction_inp['credited_UserId'];		// Credited UserId (Geeker)

		        $transfer->DebitedFunds 			= new \MangoPay\Money();			        // Money to transfer without commission
		        $transfer->DebitedFunds->Currency 	= $transaction_inp['currency_code']; //config('app.project_currency.$');
		        $transfer->DebitedFunds->Amount 	= $total_pay;

		        $transfer->Fees 			        = new \MangoPay\Money();			        // MangoPay commission fees
		        $transfer->Fees->Currency 	        = $transaction_inp['currency_code'];
		        $transfer->Fees->Amount 	        = $cost_website_commission;

		        $transfer->DebitedWalletId 	        = $transaction_inp['debited_walletId'];		// Debited walletId (User)
		        $transfer->CreditedWalletId         = $transaction_inp['credited_walletId'];	// Credited walletId (Geeker)
		        return $mangoPayApi->Transfers->Create($transfer);
			}else{
				return false;
            }
		}catch(MangoPay\Libraries\ResponseException $e){
			if($is_contest_refund_cron == '1'){
				return $e;
			}
			//dd($e);
			// handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
			return "Exception : ".$e->GetCode()."| Message: ".$e->GetMessage()."| Error(s) ".$e->GetErrorDetails();
		} catch(MangoPay\Libraries\Exception $e) {
			if($is_contest_refund_cron == '1'){
				return $e;
			}
			return "Message: ".$e->GetMessage();
		}catch(\Exception $e){
			if($is_contest_refund_cron == '1'){
				return $e;
			}
			return $e->getMessage();
		}
	}

	public function payOutBankWire($transaction_inp=null)
	{
		Session::forget('arr_data_bal');
        Session::forget('get_mangopay_wallet_details_expert');
        Session::forget('mangopay_transaction_details_expert');
        Session::forget('get_mangopay_wallet_details_client');
        Session::forget('mangopay_transaction_details_client');
		try{
			// create instance of MangoPayApi
			$mangoPayApi = new \MangoPay\MangoPayApi();
			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');

            // check kyc complete
            $check_kyc_complete = $this->get_kyc_status($transaction_inp['UserId']);
            if($check_kyc_complete == 'not-completed'){
            	return 'Your Kyc service not completed, Please complete your Kyc first.';
            }

            $total_pay         = isset($transaction_inp['total_pay']) ? $transaction_inp['total_pay'] : 0;
			$commission_amount = isset($transaction_inp['commission_amount']) ? $transaction_inp['commission_amount'] : 0;

			// for JPY currency we don't need to multiple by 100
			if(config('app.project_currency.$')!='JPY'){
				$total_pay         = $total_pay*100;
				$commission_amount = $commission_amount*100;
			}

			if($transaction_inp != null){
		        $payOut 				= new \MangoPay\PayOut();
	            $payOut->Tag 			= $transaction_inp['tag'];						        // comment
	            $payOut->AuthorId 		= $transaction_inp['UserId'];			                // Wallet owner Id
	            $payOut->CreditedUserId = $transaction_inp['UserId'];

	            $payOut->DebitedFunds = new \MangoPay\Money();
	            $payOut->DebitedFunds->Currency = config('app.project_currency.$');
	            $payOut->DebitedFunds->Amount   = $total_pay;			// order payment to transfer

	            $payOut->Fees = new \MangoPay\Money();
	            $payOut->Fees->Currency = config('app.project_currency.$');
	            $payOut->Fees->Amount   = $commission_amount;					// We already deducted while transfer

	            $payOut->DebitedWalletId 	  = $transaction_inp['walletId'];				    // Geeker wallet id debited from
	            $payOut->MeanOfPaymentDetails = new \MangoPay\PayOutPaymentDetailsBankWire();
	            $payOut->MeanOfPaymentDetails->BankAccountId = $transaction_inp['bankId'];	    // Credit bank acc Id
	            $payOut->MeanOfPaymentDetails->BankWireRef   = $transaction_inp['tag'];			// Detail to appear on bank statement

	            return $mangoPayApi->PayOuts->Create($payOut);
			}else{
				return false;
            }
		}catch(MangoPay\Libraries\ResponseException $e){
			// handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
			return "Exception : ".$e->GetCode()."| Message: ".$e->GetMessage()."| Error(s) ".$e->GetErrorDetails();
		} catch(MangoPay\Libraries\Exception $e) {
			// handle/log the exception $e->GetMessage()
			return "Message: ".$e->GetMessage();
		}catch(\Exception $e){
			return $e->getMessage();
		}
	}
	
	public function get_user_details($UserId=NULL)
	{
		try{
			// create instance of MangoPayApi
			$mangoPayApi = new \MangoPay\MangoPayApi();
			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
            //$mangoPayApi->OAuthTokenManager->RegisterCustomStorageStrategy(new \MangoPay\DemoWorkflow\MockStorageStrategy());
			if($UserId != NULL){
				$UserDetails = $mangoPayApi->Users->Get($UserId);
				if(isset($UserDetails) && count($UserDetails)>0){
					return $UserDetails;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
		catch(\Exception $e){
			return false;
		}
	} 
	
	public function get_wallet_details($WalletId=NULL)
	{
		// dd('Sudarshan',$WalletId);
		try{
			// create instance of MangoPayApi
			//dd($WalletId);
			$mangoPayApi = new \MangoPay\MangoPayApi();
			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
			$mangopay_wallet_details = $mangoPayApi->Wallets->Get($WalletId);
			// dump('Sudarshan',$mangopay_wallet_details);
			if($WalletId != NULL || $WalletId!=''){
				if(isset($mangopay_wallet_details) && count($mangopay_wallet_details)>0)
				if(isset($mangopay_wallet_details))
				{
					return $mangopay_wallet_details;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		catch(\Exception $e){
			// dd($e);
			return false;
		}
	}
	
	public function payInWalletByCard($transaction_inp=null)
	{
		// $data = $this->updateCardCurrency($transaction_inp);
		
		// dd($data);

		$arr_result = [];
		$arr_result['status'] = 'error';
		$arr_result['msg']    = 'Something went, wrong, Unable to process payment, Please try again.';
		$arr_result['data']   = [];

		try{
    		if($transaction_inp != null)
    		{
				$amount        = isset($transaction_inp['amount']) ? $transaction_inp['amount'] : 0;
				$fee           = isset($transaction_inp['fee']) ? $transaction_inp['fee'] : 0;
				$card_currency = isset($transaction_inp['card_currency']) ? $transaction_inp['card_currency'] : 'USD';

				// for JPY currency we don't need to multiple by 100
				if($transaction_inp['currency_code']!='JPY'){
					$amount = $amount*100;
					$fee    = $fee*100;
				}

    			$mangoPayApi = new \MangoPay\MangoPayApi();
                $mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
				$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
				$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
				$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');  
                
				$PayIn                            = new \MangoPay\PayIn();
				$PayIn->CreditedWalletId          = $transaction_inp['mp_wallet_id'];
				$PayIn->AuthorId                  = $transaction_inp['mp_user_id'];
				$PayIn->DebitedFunds              = new \MangoPay\Money();
				$PayIn->DebitedFunds->Currency    = $transaction_inp['currency_code'];
				$PayIn->DebitedFunds->Amount      = $amount;				
				$PayIn->Fees                      = new \MangoPay\Money();
				$PayIn->Fees->Currency            = $transaction_inp['currency_code'];
				$PayIn->Fees->Amount              = $fee;
				

				$PayIn->PaymentDetails                = new \MangoPay\PayInPaymentDetailsCard();
				$PayIn->PaymentDetails->CardId        = $transaction_inp['card_id'];
				
				$PayIn->Culture = "EN";
				$PayIn->ExecutionDetails              = new \MangoPay\PayInExecutionDetailsDirect();
				// $PayIn->ExecutionDetails->SecureModeReturnURL = url('/test');
				// $PayIn->ExecutionDetails->SecureModeReturnURL   = $transaction_inp['ReturnURL'];
				$PayIn->SecureModeReturnURL   = $transaction_inp['ReturnURL'];

				//dd($PayIn,$mangoPayApi);
				$obj_result = $mangoPayApi->PayIns->Create($PayIn);  

				if(isset($obj_result->Status) && ($obj_result->Status == 'SUCCEEDED' || $obj_result->Status == 'CREATED')) {
					$arr_result['status'] = 'success';
					$arr_result['msg']    = isset($obj_result->ResultMessage) ? $obj_result->ResultMessage : '';
					$arr_result['data']   = (array) $obj_result;
					return $arr_result;
				}
				
				$ResultCode    = isset($obj_result->ResultCode) ? $obj_result->ResultCode : '';
				$ResultMessage = isset($obj_result->ResultMessage) ? $obj_result->ResultMessage : '';
				
				$arr_result['msg']    = $ResultCode.':'.$ResultMessage;
				$arr_result['data']   = (array) $obj_result;
				return $arr_result;

    		}
		} catch (MangoPay\Libraries\ResponseException $e){
			$arr_result['msg'] = $e->GetCode().' : '.$e->GetMessage();
			return $arr_result;
		} catch(MangoPay\Libraries\Exception $e) {
			$arr_result['msg'] = $e->GetCode().' : '.$e->GetMessage();
			return $arr_result;
		}catch(\Exception $e){
			$arr_result['msg'] = $e->GetCode().' : '.$e->GetMessage();
			return $arr_result;
		}
		return $arr_result;
	}

	public function updateCardCurrency($transaction_inp)
	{
		$arr_result = [];
		$arr_result['status'] = 'error';
		$arr_result['msg']    = 'Something went, wrong, Unable to process payment, Please try again.';
		$arr_result['data']   = [];

		try{
    		if($transaction_inp != null)
    		{
    			$mangoPayApi = new \MangoPay\MangoPayApi();
				$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
				$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
				$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
				$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');

				$CardRegistrationId = 83971369;
				// DD($CardRegistrationId);
				$Card = $mangoPayApi->Cards->Get($CardRegistrationId);

				$CardRegistration = $mangoPayApi->CardRegistrations->Get($CardRegistrationId);
				
				// dd($CardRegistration);

                $CardRegistration = new \MangoPay\CardRegistration();

                dd($Card,$CardRegistration);

				$CardRegistration->Id = 83971369;
				$CardRegistration->Currency = $transaction_inp['currency_code'];
				$CardRegistration->RegistrationData = $transaction_inp['currency_code']. ' Update for payment';

				dd($CardRegistration,$mangoPayApi->CardRegistrations);

				$obj_result = $mangoPayApi->CardRegistrations->Update($CardRegistration);
				dd($obj_result);
				
				$obj_result = $mangoPayApi->PayIns->Create($PayIn);  

				if(isset($obj_result->Status) && ($obj_result->Status == 'SUCCEEDED' || $obj_result->Status == 'CREATED')) {
					$arr_result['status'] = 'success';
					$arr_result['msg']    = isset($obj_result->ResultMessage) ? $obj_result->ResultMessage : '';
					$arr_result['data']   = (array) $obj_result;
					return $arr_result;
				}
				
				$ResultCode    = isset($obj_result->ResultCode) ? $obj_result->ResultCode : '';
				$ResultMessage = isset($obj_result->ResultMessage) ? $obj_result->ResultMessage : '';
				
				$arr_result['msg']    = $ResultCode.':'.$ResultMessage;
				$arr_result['data']   = (array) $obj_result;
				return $arr_result;

    		}
		} catch (MangoPay\Libraries\ResponseException $e){
			dd($e,1);
			$arr_result['msg'] = $e->GetCode().' : '.$e->GetMessage();
			return $arr_result;
		} catch(MangoPay\Libraries\Exception $e) {
			dd($e,2);
			$arr_result['msg'] = $e->GetCode().' : '.$e->GetMessage();
			return $arr_result;
		}catch(\Exception $e){
			dd($e,3);
			$arr_result['msg'] = $e->GetCode().' : '.$e->GetMessage();
			return $arr_result;
		}
		return $arr_result;
	}

	public function payInWallet($transaction_inp=null)
	{
	  	Session::forget('arr_data_bal');
      	Session::forget('get_mangopay_wallet_details_expert');
      	Session::forget('mangopay_transaction_details_expert');
      	Session::forget('get_mangopay_wallet_details_client');
      	Session::forget('mangopay_transaction_details_client');
		try{
    		if($transaction_inp != null)
    		{
    			$amount = isset($transaction_inp['amount']) ? $transaction_inp['amount'] : 0;
				$fee    = isset($transaction_inp['fee']) ? $transaction_inp['fee'] : 0;

				// for JPY currency we don't need to multiple by 100
				if($transaction_inp['currency_code']!='JPY'){
					$amount = $amount*100;
					$fee    = $fee*100;
				}

    			$mangoPayApi = new \MangoPay\MangoPayApi();
                $mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
				$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
				$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
				$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');  
                $PayIn 								  = new \MangoPay\PayIn();
				$PayIn->CreditedWalletId              = $transaction_inp['mp_wallet_id'];
				$PayIn->AuthorId                      = $transaction_inp['mp_user_id'];
				$PayIn->PaymentType                   = "CARD";
				$PayIn->PaymentDetails                = new \MangoPay\PayInPaymentDetailsCard();
				$PayIn->PaymentDetails->CardType      = "CB_VISA_MASTERCARD";
				$PayIn->DebitedFunds                  = new \MangoPay\Money();
				$PayIn->DebitedFunds->Currency        = $transaction_inp['currency_code'];
				$PayIn->DebitedFunds->Amount          = $amount;
				$PayIn->Fees                          = new \MangoPay\Money();
				$PayIn->Fees->Currency                = $transaction_inp['currency_code'];
				$PayIn->Fees->Amount                  = $fee;
				$PayIn->ExecutionType                 = "WEB";
				$PayIn->ExecutionDetails              = new \MangoPay\PayInExecutionDetailsWeb();
				$PayIn->ExecutionDetails->ReturnURL   = $transaction_inp['ReturnURL'];
				$PayIn->ExecutionDetails->Culture     = "EN";
				$result = $mangoPayApi->PayIns->Create($PayIn);  
                return $result;
    		}else{
    			return false;
            }
		}catch(MangoPay\Libraries\ResponseException $e){
			// handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
			return "Exception : ".$e->GetCode()."| Message: ".$e->GetMessage()."| Error(s) ".$e->GetErrorDetails();
		} catch(MangoPay\Libraries\Exception $e) {
			// handle/log the exception $e->GetMessage()
			return "Message: ".$e->GetMessage();
		}catch(\Exception $e){
			return $e->getMessage();
		}
	}

	public function viewPayIn($transaction_id=null)
	{
		$arr_result = [];
		$arr_result['status'] = 'error';
		$arr_result['msg']    = 'Something went, wrong, Unable to process payment, Please try again.';
		$arr_result['data']   = [];

		try{
    		if($transaction_id != null)
    		{
    			$mangoPayApi = new \MangoPay\MangoPayApi();
                $mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
				$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
				$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
				$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL'); 

				$obj_result = $mangoPayApi->PayIns->Get($transaction_id);
				if(isset($obj_result->Status) && $obj_result->Status == 'SUCCEEDED') {
					$arr_result['status'] = 'success';
					$arr_result['msg']    = isset($obj_result->ResultMessage) ? $obj_result->ResultMessage : '';
					$arr_result['data']   = (array) $obj_result;
					return $arr_result;
				}
				
				$ResultCode    = isset($obj_result->ResultCode) ? $obj_result->ResultCode : '';
				$ResultMessage = isset($obj_result->ResultMessage) ? $obj_result->ResultMessage : '';
				
				$arr_result['msg']    = $ResultCode.':'.$ResultMessage;
				$arr_result['data']   = (array) $obj_result;
				return $arr_result;
    		}
		} catch (MangoPay\Libraries\ResponseException $e){
			$arr_result['msg'] = $e->GetCode().':'.$e->GetMessage();
			return $arr_result;
		} catch(MangoPay\Libraries\Exception $e) {
			$arr_result['msg'] = $e->GetCode().':'.$e->GetMessage();
			return $arr_result;
		}catch(\Exception $e){
			$arr_result['msg'] = $e->GetCode().':'.$e->GetMessage();
			return $arr_result;
		}
		return $arr_result;
	}


	
	public function create_card_registration($transaction_inp=null)
	{
		try {
				$mangoPayApi = new \MangoPay\MangoPayApi();
				$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
				$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
				$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
				$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');

				$CardRegistration = new \MangoPay\CardRegistration();
				$CardRegistration->UserId   = $transaction_inp['user_id'];
				$CardRegistration->Currency = $transaction_inp['currency'];
				$CardRegistration->CardType = $transaction_inp['card_type'];
				$Result = $mangoPayApi->CardRegistrations->Create($CardRegistration);
				return $Result;
			}catch(MangoPay\Libraries\ResponseException $e){
				// handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
				return "Exception : ".$e->GetCode()."| Message: ".$e->GetMessage()."| Error(s) ".$e->GetErrorDetails();

				} catch(MangoPay\Libraries\Exception $e) {
				// handle/log the exception $e->GetMessage()
					return "Message: ".$e->GetMessage();

				}
	}

	public function submit_kyc_docs($transaction_inp=null)
	{
		try{

			// create instance of MangoPayApi
			$mangoPayApi = new \MangoPay\MangoPayApi();
			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');

            $transaction_inp['KycDocument'] = $transaction_inp['KycDocument'];

			//create the doc
			$KycDocument          = new \MangoPay\KycDocument();
			$KycDocument->Type    = $transaction_inp['KycDocumentType'];
			$KycDocument->Status  = "CREATED";
            $KycDocument->File    = $transaction_inp['KycDocument'];
          
			$result             = $mangoPayApi->Users->CreateKycDocument($transaction_inp['mp_user_id'], $KycDocument);
			//dd('Sudarshan',$result);
			$KycDocumentId      = $result->Id;

			//add a page to this doc
			$result2            = $mangoPayApi->Users->CreateKycPageFromFile($transaction_inp['mp_user_id'], $KycDocumentId, $transaction_inp['KycDocumentPath']);
			//submit the doc for validation

			$KycDocument->Id      = $KycDocumentId;
			$KycDocument->Status  = "VALIDATION_ASKED";
			$result3  = $mangoPayApi->Users->UpdateKycDocument($transaction_inp['mp_user_id'], $KycDocument);
			//dd($result3);
            if(isset($result3) && count($result3)>0)
            {
				return true;
			}
			else
			{
				return false;
			}
		}
		catch(\Exception $e){
			//dd($e);
			return false;
		}
	}
	public function get_kyc_details($UserId=NULL)
	{
		try{
			// create instance of MangoPayApi
			$mangoPayApi = new \MangoPay\MangoPayApi();
			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
			//get the doc
			$KycDocument = new \MangoPay\KycDocument();
			$mangopay_kyc_details = $mangoPayApi->Users->GetKycDocuments($UserId);
            if($UserId != NULL){
				if(isset($mangopay_kyc_details) && count($mangopay_kyc_details)>0){
					return $mangopay_kyc_details;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
		catch(\Exception $e){
			return false;
		}
	}
	public function get_kyc_status($UserId=NULL)
	{
		try{
			// create instance of MangoPayApi
			$mangoPayApi = new \MangoPay\MangoPayApi();
			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
			$mangopay_user_kyc_status = $mangoPayApi->Users->GetKycDocuments($UserId);
            if($UserId != NULL){
				if(isset($mangopay_user_kyc_status) && $mangopay_user_kyc_status != ""){
				    foreach ($mangopay_user_kyc_status as $kyckey => $kyc) {
			    	 	if($kyc->Status == 'VALIDATED'){
			    	       return 'completed'; 		
			    	 	}
				    }	 
					return 'not-completed';
				}else{
					return 'not-completed';
				}
			}else{
				return 'not-completed';
			}
		}
		catch(\Exception $e){
			return 'not-completed';
		}
	}
	public function get_banks_details($UserId=NULL)
	{
		try{
			// create instance of MangoPayApi
			$mangoPayApi = new \MangoPay\MangoPayApi();
			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
			//get the doc
			$KycDocument = new \MangoPay\KycDocument();
			$mangopay_bank_details = $mangoPayApi->Users->GetBankAccounts($UserId);
            if($UserId != NULL){
				if(isset($mangopay_bank_details) && count($mangopay_bank_details)>0){
					return $mangopay_bank_details;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
		catch(\Exception $e){
			return false;
		}
	}
	public function get_transaction_details($UserId=NULL,$page=NULL)
	{
		try{
			// create instance of MangoPayApi
			$mangoPayApi = new \MangoPay\MangoPayApi();
			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
			$pagination  = new \MangoPay\Pagination($page,config('app.project.pagi_cnt')); // get 1st page, 10 items per page
			$mangopay_transactions_details        = $mangoPayApi->Users->GetTransactions($UserId,$pagination);
            if($UserId != NULL){
				if(isset($mangopay_transactions_details) && count($mangopay_transactions_details)>0){
					return $mangopay_transactions_details;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
		catch(\Exception $e){
			return false;
		}
	}
	public function get_wallet_transaction_details($wallet_id=NULL,$page=NULL)
	{
		try{
			// create instance of MangoPayApi
			$mangoPayApi = new \MangoPay\MangoPayApi();
			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
			$pagination  = new \MangoPay\Pagination($page,config('app.project.pagi_cnt')); // get 1st page, 10 items per page
			$wallet_transactions_details          = $mangoPayApi->Wallets->GetTransactions($wallet_id,$pagination);
            if($wallet_id != NULL){
				if(isset($wallet_transactions_details) && count($wallet_transactions_details)>0){
					return $wallet_transactions_details;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
		catch(\Exception $e){
			return false;
		}
	}
	public function get_transaction_pagi_links_count($UserId=NULL)
	{
		try{
			// create instance of MangoPayApi
			$mangoPayApi = new \MangoPay\MangoPayApi();
			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
			$pagination                           = new \MangoPay\Pagination(1,config('app.project.pagi_cnt')); // get 1st page, 10 items per page
			$mangopay_transactions_details        = $mangoPayApi->Users->GetTransactions($UserId,$pagination);
            //dd($pagination);
            if($UserId != NULL){
				if(isset($mangopay_transactions_details) && count($mangopay_transactions_details)>0){
					$pagi_cnt = 1;
					$pagi     = ($pagination->TotalItems/10);
					$explode  = explode('.', $pagi);
                    if(isset($explode[1]) && $explode[1] >= 0){
                      $pagi_cnt =  $explode[0]+1;
                    } else{
                      $pagi_cnt = $explode[0];
                    }

                    if($pagi_cnt == 0){
                    	$pagi_cnt = 1;
                    }
					return $pagi_cnt;
				}else{
					return 1;
				}
			}else{
				return 1;
			}
		}
		catch(\Exception $e){
			return 1;
		}
	}
	public function get_wallet_transaction_pagi_links_count($WalletId=NULL)
	{
		try{
			// create instance of MangoPayApi
			$mangoPayApi = new \MangoPay\MangoPayApi();
			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
			$pagination                           = new \MangoPay\Pagination(1,config('app.project.pagi_cnt')); // get 1st page, 10 items per page
			$mangopay_wallet_transactions_details = $mangoPayApi->Wallets->GetTransactions($WalletId,$pagination);
            //dd($pagination);
            if($WalletId != NULL){
				if(isset($mangopay_wallet_transactions_details) && count($mangopay_wallet_transactions_details)>0){
					$pagi_cnt = 1;
					$pagi     = ($pagination->TotalItems/10);
					$explode  = explode('.', $pagi);
                    if(isset($explode[1]) && $explode[1] >= 0){
                      $pagi_cnt =  $explode[0]+1;
                    } else{
                      $pagi_cnt = $explode[0];
                    }

                    if($pagi_cnt == 0){
                    	$pagi_cnt = 1;
                    }
					return $pagi_cnt;
				}else{
					return 1;
				}
			}else{
				return 1;
			}
		}
		catch(\Exception $e){
			return 1;
		}
	}
	public function payinRefund($transaction_inp=null)
	{
		try{

			// create instance of MangoPayApi
			$mangoPayApi = new \MangoPay\MangoPayApi();
			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
			$PayInId                              = $transaction_inp['payinId'];
			$Refund                               = new \MangoPay\Refund();
			$Refund->AuthorId                     = $transaction_inp['authorId'];
			$Refund->DebitedFunds                 = new \MangoPay\Money();
			$Refund->DebitedFunds->Currency       = config('app.project_currency.$');
			$Refund->DebitedFunds->Amount         = $transaction_inp['act_refund_amount']*100;
			$Refund->Fees                         = new \MangoPay\Money();
			$Refund->Fees->Currency               = config('app.project_currency.$');
			$Refund->Fees->Amount                 = 0;
			$result                               = $mangoPayApi->PayIns->CreateRefund($PayInId, $Refund);
			return $result;
		} catch(MangoPay\Libraries\Exception $e) {
			// handle/log the exception $e->GetMessage()
			return "Message: ".$e->GetMessage();
		}catch(\Exception $e){
			return $e->getMessage();
		}
	}
	public function transferRefund($transaction_inp=null)
	{
		try{
			// create instance of MangoPayApi
			$mangoPayApi = new \MangoPay\MangoPayApi();
			$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
			$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
			$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
			$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');
            if($transaction_inp != null){
				$TransferId                     = $transaction_inp['TransactionId'];
				$Refund                         = new \MangoPay\Refund();
				$Refund->AuthorId               = $transaction_inp['AuthorId'];
				$Refund->DebitedFunds           = new \MangoPay\Money();
				$Refund->DebitedFunds->Currency = config('app.project_currency.$');
				$Refund->DebitedFunds->Amount   = $transaction_inp['refund_amount']*100;//Note that partial Refunds for Transfers are not possible
				$Refund->Fees                   = new \MangoPay\Money();
				$Refund->Fees->Currency         = config('app.project_currency.$');
				$Refund->Fees->Amount           = 0;
				return $result                  = $mangoPayApi->Transfers->CreateRefund($TransferId, $Refund);
			}else{
				return false;
            }
		}catch(MangoPay\Libraries\ResponseException $e){
			// handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
			return "Exception : ".$e->GetCode()."| Message: ".$e->GetMessage()."| Error(s) ".$e->GetErrorDetails();
		} catch(MangoPay\Libraries\Exception $e) {
			// handle/log the exception $e->GetMessage()
			return "Message: ".$e->GetMessage();
		}catch(\Exception $e){
			return $e->getMessage();
		}
	}

	public function create_mandate($bankId=NULL)
	{
		try
		{

				$mangoPayApi = new \MangoPay\MangoPayApi();

				$mangoPayApi->Config->ClientId        = config('app.project.MangoPay.ClientId');
				$mangoPayApi->Config->ClientPassword  = config('app.project.MangoPay.ClientPassword');
				$mangoPayApi->Config->TemporaryFolder = config('app.project.MangoPay.TemporaryFolder');
				$mangoPayApi->Config->BaseUrl         = config('app.project.MangoPay.URL');

				$Mandate = new \MangoPay\Mandate();
				$Mandate->Tag = "custom meta";
				$Mandate->BankAccountId = "66968875";
				$Mandate->Culture = "EN";
				$Mandate->ReturnURL = "http://www.my-site.com/returnURL/";

				$Result = $mangoPayApi->Mandates->Create($Mandate);
				DD($Result);
				/*if(isset($Result->Status) && $Result->Status == 'CREATED')
				{
					return $Result;
				}
				else
				{
					return false;
				}*/
			
			/*else
			{
				return false;
			}*/
			
		} catch(Exception $e) {
			DD($e);

		} 
	}


} //  end class 
