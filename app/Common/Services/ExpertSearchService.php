<?php 

namespace App\Common\Services;
use Illuminate\Http\Request;

use App\Models\ExpertsModel;
use App\Models\SkillsModel;
use App\Models\ExpertsSkillModel; 
use App\Models\ReviewsModel;

use Session;

class ExpertSearchService
{
	public function __construct(
									ExpertsModel $expert,
									SkillsModel $skill_model,
								    ExpertsSkillModel $expert_skills	
								)
	{
		if(!Session::has('locale'))
        {
           Session::put('locale', \Config::get('app.locale'));
        }
        app()->setLocale(Session::get('locale'));
        view()->share('selected_lang',\Session::get('locale'));
        
		$this->ExpertsModel = $expert;
		$this->SkillsModel = $skill_model;
		$this->ExpertsSkillModel = $expert_skills;
	}

	public function make_filer_search( $arr_data = [] )
	{
		//dd($arr_data);
		$search_result = array();
		$tmp_expert_skill = $arr_experts =  $arr_skill_result = $arr_skill_ids =  []; 

		$arr_search_term  = [];
		$arr_search_term  = explode(' ',isset($arr_data['expert_search_term'])? $arr_data['expert_search_term'] : "");
		$search_term      = trim(isset($arr_data['expert_search_term'])? $arr_data['expert_search_term'] : "");

		$search_result = $this->ExpertsModel
		                      ->with(['user_details','country_details','city_details','expert_skills','profession_details','expert_categories'])
		                      ->whereHas('user_details',function ($query)
					        	{
					          		$query->where('is_active',1);
					          		$query->where('is_available',1);
					        	});

		                      if(array_has($arr_data,'search_profession_id') && $arr_data['search_profession_id'] != "")
		                      {
		                      	$search_result->where('profession',$arr_data['search_profession_id']);
		                      }

		if($search_result)
		{
            if(array_has($arr_data,'category') && $arr_data['category'] != "")
            {
            	$category      = base64_decode($arr_data['category']);
            	$search_result = $search_result->join('experts_categories','experts_categories.expert_user_id','=','experts.user_id')
            	                               ->where('experts_categories.category_id',$category);
            }

            if(array_has($arr_data,'subcategory') && $arr_data['subcategory'] != "")
            {
            	$subcategory      =$arr_data['subcategory'];
            	
            	$search_result = $search_result->join('experts_categories_subcategories','experts_categories_subcategories.expert_user_id','=','experts.user_id')
            	                               ->where('experts_categories_subcategories.subcategory_id',$subcategory);
            }

            if(array_has($arr_data,'profession') && $arr_data['profession'] != "")
            {
            	$profession      = base64_decode($arr_data['profession']);
            	$search_result = $search_result->where('experts.profession',$profession);
            }

            if(array_has($arr_data,'lang') && $arr_data['lang'] != "")
            {
            	$lang          = $arr_data['lang'];
            	$search_result = $search_result->join('experts_spoken_languages','experts_spoken_languages.expert_user_id','=','experts.user_id')
            	                               ->where('experts_spoken_languages.language',$lang);
            }

            if(!empty($arr_data['h_rate_max_val']))
            {
                $search_result = $search_result->whereBetween('experts.hourly_rate', array($arr_data['h_rate_min_val'], $arr_data['h_rate_max_val']));
            }

            if(!empty($arr_data['rating']))
            {
            	  $explode = explode('-', $arr_data['rating']);
                  $average_rating = $search_result->join('reviews', 'experts.user_id', '=', 'reviews.to_user_id')
						            ->groupBy('experts.id')
						            ->havingRaw('AVG(rating) >= ?', [$explode[0]])
						            //->havingRaw('AVG(rating) <= ?', [$explode[1]])
						            ->avg('rating');
            }

			/* ends */
			/* if country is not selected */

			if(array_has($arr_data,'expert_search_term') && strlen(trim($arr_data['expert_search_term']))>0  && ( isset($arr_data['country']) &&  trim($arr_data['country']) == "" ) )
            {	
            	if(count($arr_experts) > 0 )
				{
					$search_result  = $search_result->where(function ($query_two) use($arr_experts) 
									  				{
														$query_two->orWhereIn('user_id',$arr_experts);
													});
					/* here we need or berfore where */
					if(count($arr_search_term) > 0)
					{
						foreach ($arr_search_term as $key => $term) 
						{
							if(trim($term) != "")
							{	
								$search_result = $search_result->Where('first_name','like','%'.trim($term).'%')
				            							       ->with(['user_details'])->whereHas('user_details',function ($query)
														        {
														          $query->where('is_active',1);
														          $query->where('is_available',1);
														        });
							}
						}
						$search_result = $search_result->groupBy('experts.id');
					}
					else
					{
						$search_result = $search_result->Where('first_name','like','%'.$search_term.'%')
    							        ->orWhere( function ($q) use($search_term)
         							       {
         							       		$q->whereHas('expert_spoken_languages', function ($q1) use ($search_term) 
         							       		{
         							       			$q1->orWhere('language','like','%'.trim($search_term).'%');	
         							       		});

         							       		$q->with(['expert_spoken_languages'=> function ($q1) use ($search_term) 
         							       		{
             							       			$q1->orWhere('language','like','%'.trim($search_term).'%');	
             							       	}]);
        							       })
    							        ->groupBy('experts.id')->with(['user_details'])->whereHas('user_details',function ($query)
									        {
									          $query->where('is_active',1);
									          $query->where('is_available',1);
									        });
					}
				}
				else
				{
					/* here we need dont need or berfore where  so removing it */
					if(count($arr_search_term) > 1 )
					{
						foreach ($arr_search_term as $key => $term) 
						{
							if(trim($term) != "")
							{	
								$search_result = $search_result->where('first_name', 'LIKE', "%".trim($term)."%")
                							       			   ->with(['user_details'])->whereHas('user_details',function ($query)
														        {
														          $query->where('is_active',1);
														          $query->where('is_available',1);
														        });
							}
						}
						$search_result = $search_result->groupBy('experts.id');
					}
					else
					{
						$search_result = $search_result->where('first_name','LIKE',"%".$search_term."%")
                							       	   ->groupBy('experts.id')->with(['user_details'])->whereHas('user_details',function ($query)
												        {
												          $query->where('is_active',1);
												          $query->where('is_available',1);
												        });
					}
				}
	        }
	        /* ends */
	        /* if only country is selceted */
	        if(array_has($arr_data,'country') && strlen(trim($arr_data['country']))>0 && ( isset($arr_data['expert_search_term']) &&  trim($arr_data['expert_search_term']) == "" ) )
            {
                $search_result = $search_result->where('country',$arr_data['country']) 
                							   ->groupBy('experts.id');    
	        }
	        /* ends */

	        /* If country and keyword both  are present */
	        if(array_has($arr_data,'country') && strlen(trim($arr_data['country']))>0)
            {
            	if(array_has($arr_data,'expert_search_term') && strlen(trim($arr_data['expert_search_term']))>0)
                {		
                    if(count($arr_experts) > 0 )
					{
						/* formatting query properly*/
						$search_result =$search_result->where(function ($query_three) use ($arr_data,$arr_experts,$arr_search_term)  
	                                                  	{
	                                                  		$query_three->where('country',$arr_data['country']);
	                                                  		$query_three->where(function ($query_four) use($arr_data,$arr_experts,$arr_search_term) 
	                                                  		{
	                                                  			if(count($arr_search_term) > 1 )
																{
																	foreach ($arr_search_term as $key => $term) 
																	{
																		if(trim($term) != "")
																		{	
																			$query_four->orwhere('first_name','like','%'.trim($term).'%');
																		}
																	}
																	$query_four->orWhereIn('user_id',$arr_experts);
	                                                  			}
	                                                  			else
	                                                  			{
		                                                  			$query_four->orwhere('first_name','like','%'.$arr_data['expert_search_term'].'%'); 
		                							     			$query_four->orWhereIn('user_id',$arr_experts);
	                                                  			}	
	                                                  		});
	                                                  	})->groupBy('experts.id'); 
	              	}
					else
					{
		                if(count($arr_search_term) > 1 )
						{
							$search_result = $search_result->where('country',$arr_data['country']);	
							$search_result = $search_result->where(function ($query_five) use($arr_search_term)
											{
												foreach ($arr_search_term as $key => $term) 
												{
													if(trim($term) != "")
													{	
														$query_five->orWhere('first_name','like','%'.trim($term).'%');
						                			}
												}
											});	

							$search_result = $search_result->groupBy('experts.id');
						}
						else
						{
							$search_result = $search_result->where('country',$arr_data['country'])
														  ->where(function ($query_six) use($search_term)
														  {
				                                               $query_six->where('first_name','like','%'.$search_term.'%');
														  });	
		                	$search_result =$search_result->groupBy('experts.id'); 
						} 							   
					}
                }
	        }
	        /* ends */
		}
		return $search_result;
	}
} // end service
?>