<?php
namespace App\Events;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class sendEmailOnPostProject extends Event
{
    use SerializesModels;
    /**
     * Create a new event instance.
     *
     * @return void
    */
    /* declaring project_id */

    public $project_id;
    public function __construct($project_id){
        $this->project_id   = $project_id;    
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */

    public function broadcastOn()
    {
       return [];
    }
}
