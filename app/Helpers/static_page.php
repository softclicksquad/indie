<?php

use App\Models\StaticPageModel;



   /*
    | Comment: This function will return CMS page of how it works
    | auther : Bharat K
    */
    
	function get_static_page()
	{
      $obj_static_page =  StaticPageModel::where('page_slug','how-it-works')->where('is_active',"1")->first(['is_active']);
      
      $arr_static_page = array();
      if($obj_static_page)
      {
        $arr_static_page = $obj_static_page->toArray();
      }
      return $arr_static_page;
    }
?>