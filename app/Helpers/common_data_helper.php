<?php

use App\Models\SiteSettingModel;
use App\Models\UserModel;
use App\Models\ProjectpostModel;
use App\Models\ClientsModel;
use App\Models\ExpertsModel;
use App\Models\ExpertCategorySubCategoryModel;
use App\Models\ReviewsModel;
use App\Models\ProjectRequestsModel;
use App\Models\ContestModel;
use App\Models\CurrencyConversionModel;
use App\Models\SupportTicketModel;
use App\Models\BlogCategoriesModel;
use App\Models\UserWalletModel;
use App\Models\CurrencyModel;
use App\Models\SubscriptionUsersModel;
use App\Models\CountryModel;
//use Sentinel;
/*
Comments : Function to get site email id
Auther   : Nayan S.
*/
function get_user_email_id($expert_id)
{
    //dd($expert_id);
    $email  = '';
    $obj_user = UserModel::select('id','email')
                              ->where('id',$expert_id)
                              ->first();
    if($obj_user)
    {
        $email = isset($obj_user->email)?$obj_user->email:'';
    }
    return $email; 
}

function check_default_currency_set($user_id)
{

    $obj_user = UserModel::where('id',$user_id)
                              ->first();
    //dd($obj_user);
    if($obj_user)
    {
        $currency_code = isset($obj_user->currency_code)?$obj_user->currency_code:null;
        if($currency_code!=null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    return false;
}

function get_default_currency($user_id)
{
    $currency_code = 'USD';
    $obj_user      = UserModel::where('id',$user_id)
                              ->first();
    //dd($obj_user);
    if($obj_user)
    {
        $currency_code = isset($obj_user->currency_code)?$obj_user->currency_code:'USD';
        return $currency_code;
        
    }
    return $currency_code;
}

function is_downgrade_requested($pack_id)
{
    $user = \Sentinel::check();
    
    if($user)
    {
        $obj_data  = SubscriptionUsersModel::where('user_id',$user->id)->where('subscription_pack_id',$pack_id)->first();
        //dd($obj_data->toArray());
        if($obj_data->is_downgrade_request=='1')
        {
            return 'Requested';
        }
        else
        {
            return 'Downgrade';
        }
    }
    return 'Downgrade';
}

function check_requested_msg($pack_id)
{
    $user = \Sentinel::check();
    
    if($user)
    {
        $obj_data  = SubscriptionUsersModel::where('user_id',$user->id)->where('subscription_pack_id',$pack_id)->orderBy('id','desc')->first();
        if($obj_data->is_downgrade_request=='1')
        {
            $expiry_at = get_added_on_date(isset($obj_data->expiry_at)?$obj_data->expiry_at:'');

            return 'Your plan will valid till '.$expiry_at.' and will downgrade automatically';
        }
        else
        {
            return '';
        }
    }
    return '';
}

function get_service_charge($amount,$currency_code)
{

    $obj_data = CurrencyModel::with('deposite')
                                    ->where('currency_code',$currency_code)
                                    ->first();
    if($obj_data)
    {
        $arr_data = $obj_data->toArray();

        $min_amount = isset($arr_data['deposite']['min_amount'])?floatval($arr_data['deposite']['min_amount']):0;
        $max_amount = isset($arr_data['deposite']['max_amount'])?floatval($arr_data['deposite']['max_amount']):0;

        $min_charge = isset($arr_data['deposite']['min_amount_charge'])?str_replace(',', '.', $arr_data['deposite']['min_amount_charge']):0;
        $max_charge = isset($arr_data['deposite']['max_amount_charge'])?str_replace(',', '.', $arr_data['deposite']['max_amount_charge']):0;
        
        if($amount>$min_amount)
        {
            $service_charge = (float)$amount * (float)$max_charge/100;
        }
        else
        {
            $service_charge =  (float) $min_charge;
            //$service_charge =  (float) $amount + (float) $additional_charge;
        }
        
        if($currency_code == 'JPY'){
            $service_charge = round($service_charge);
        }

        $total_amount = (float)$amount + (float)$service_charge;

        if($currency_code == 'JPY'){
            $total_amount = round($total_amount);
        }

        $data['service_charge'] = $service_charge;
        $data['total_amount']   = $total_amount;
        return $data;
    }  
}

function get_site_email_address()
{
    $obj_email  = SiteSettingModel::first();
    $site_email = "";
    if($obj_email)
    {
        if(isset($obj_email->site_email_address) && $obj_email->site_email_address != "" ) 
        {
            $site_email = $obj_email->site_email_address;       
        }
    }
    return $site_email;
}
/*
Comments : Function to get admin info
Auther   : Nayan S.
*/
function get_admin_email_address()
{       
    $admin_data = [];

    $obj_user = UserModel::where('id','1')->first();
    if($obj_user)
    {
        $admin_data = $obj_user->toArray();       
    }

    return $admin_data;
}

function get_user_wallet_details($id,$currency)
{       
    $admin_data = [];

    $obj_user = UserWalletModel::where('user_id',$id)->where('currency_code',$currency)
                                    ->first();
    if($obj_user)
    {
        $admin_data = $obj_user->toArray();       
    }

    return $admin_data;
}

function filter_valid_projects($arr_project = [])
{
    $arr_filtered = [];
    if(sizeof($arr_project)>0)
    {
        $arr_filtered = array_filter($arr_project,function($data)
        {

            if($data['project_handle_by']==2)
            {

                return isset($data['transaction_details']['payment_status']) &&
                    ($data['transaction_details']['payment_status']==1 || $data['transaction_details']['payment_status']==2);
            }
            else
            {
                return true;
            }
            return false;
        });
    }
    return $arr_filtered;
}
function humanTiming($time)
{
    $time = time() - $time; // to get the time since that moment
    $time = ($time<1) ? 1 : $time;
    $tokens = array (
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second'
    );
    
    foreach ($tokens as $unit => $text) {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
    }
}
/* 
    Comments : Sets Currency code of transaction if project id is present.
    Auther   : Nayan S.
    @param   : project id  
*/
function setCurrencyForTransaction($id = FALSE) 
{
    $currency      = '$';
    $currency_code = 'USD';
    $arr_data      = [];

    if($id != FALSE)
    {
        $obj_project = ProjectpostModel::where('id','=',$id)->first(['id','project_name','project_currency','project_currency_code']);
        
        if(isset($obj_project->project_currency) && $obj_project->project_currency != "" && isset($obj_project->project_currency_code) && $obj_project->project_currency_code != "" )
        {
            $currency       =  $obj_project->project_currency;
            $currency_code  =  $obj_project->project_currency_code;
        }
    }

    $arr_data['currency']      = $currency;
    $arr_data['currency_code'] = $currency_code;

    return $arr_data;
} 

function time_ago($datetime = FALSE){

    $datetime1  = new DateTime();
    $datetime2  = new DateTime($datetime);
    $interval   = $datetime1->diff($datetime2);

    $ago        = '0 second';
    $year       = $interval->format('%y');
    $month      = $interval->format('%m');
    $day        = $interval->format('%d');
    $hours      = $interval->format('%h');
    $minutes    = $interval->format('%i');
    $second     = $interval->format('%s');
    $y = $mon = $d = $h = $m = $s= "";

    if($year > 1){
        if(isset($year) && $year != "0" ) {  
            $y = $year.' years ';    
        }
    } else {
       if(isset($year) && $year != "0") {  
            $y = $year.' year ';    
        }
    }

    if($month > 1){
       if(isset($month) && $month != "0"){  
            $mon = $month.' months ';   
        }
    } else {
       if(isset($month) && $month != "0"){  
            $mon  =  $month.' month ';   
        }
    }

    if($day > 1){
       if(isset($day) && $day != "0"){  
            $d = $day.' days ';     
        }
    } else {
       if(isset($day) && $day != "0"){  
            $d = $day.' day ';     
        }
    }

    if($hours > 1){
       if(isset($hours) && $hours != "0"){  
            $h = $hours.' hours ';   
        }
    } else {
       if(isset($hours) && $hours != "0"){  
            $h = $hours.' hour ';   
        }
    }

    if($minutes > 1){
       if(isset($minutes) && $minutes !=  "0"){ 
            $m = $minutes.' minutes ';   
        }
    } else {
       if(isset($minutes) && $minutes !=  "0"){  
            $m = $minutes.' minute ';   
        }
    }

    if($second > 1){
       if(isset($second) && $second != "0"){  
            $s = $second.' seconds ';  
        }
    } else {
       if(isset($second) && $second != "0"){  
            $s = $second.' second ';  
        }
    }  

    if($y != ""){
        $ago   = $y.' ago';
    }else if($mon != ""){
        $ago   = $mon.' ago';
    }else if($d != ""){
        $ago   = $d.' ago';
    }else if($h != ""){
        $ago   = $h.' ago';
    }else if($m != ""){
        $ago   = $m.' ago';
    }else{
        $ago   = $s.' ago';
    }
    return str_replace('  ', ' ', $ago);
}


function str_time_ago($interval = FALSE){

    if(isset($interval) == false){
        return '';
    }
    
    if( isset($interval) && $interval instanceof \DateInterval == false ) {
        return '';
    }

    $ago        = '0 second';
    $year       = $interval->format('%y');
    $month      = $interval->format('%m');
    $day        = $interval->format('%d');
    $hours      = $interval->format('%h');
    $minutes    = $interval->format('%i');
    $second     = $interval->format('%s');
    $y = $mon = $d = $h = $m = $s= "";

    if($year > 1){
        if(isset($year) && $year != "0" ) {  
            $y = $year.' y ';    
        }
    } else {
       if(isset($year) && $year != "0") {  
            $y = $year.' y ';    
        }
    }

    if($month > 1){
       if(isset($month) && $month != "0"){  
            $mon = $month.' mon ';   
        }
    } else {
       if(isset($month) && $month != "0"){  
            $mon  =  $month.' mon ';   
        }
    }

    if($day > 1){
       if(isset($day) && $day != "0"){  
            $d = $day.' d ';     
        }
    } else {
       if(isset($day) && $day != "0"){  
            $d = $day.' d ';     
        }
    }

    if($hours > 1){
       if(isset($hours) && $hours != "0"){  
            $h = $hours.' h ';   
        }
    } else {
       if(isset($hours) && $hours != "0"){  
            $h = $hours.' h ';   
        }
    }

    if($minutes > 1){
       if(isset($minutes) && $minutes !=  "0"){ 
            $m = $minutes.' min ';   
        }
    } else {
       if(isset($minutes) && $minutes !=  "0"){  
            $m = $minutes.' min ';   
        }
    }

    if($second > 1){
       if(isset($second) && $second != "0"){  
            $s = $second.' s ';  
        }
    } else {
       if(isset($second) && $second != "0"){  
            $s = $second.' s ';  
        }
    }  

    if($y != ""){
        $ago   = $y.' ago';
    }else if($mon != ""){
        $ago   = $mon.' ago';
    }else if($d != ""){
        $ago   = $d.' ago';
    }else if($h != ""){
        $ago   = $h.' ago';
    }else if($m != ""){
        $ago   = $m.' ago';
    }else{
        $ago   = $s.' ago';
    }
    return str_replace('  ', ' ', $ago);
}

function get_attatchment_format($file_name=null)
{
    $file_extension = "";
    if(isset($file_name) && $file_name!=null)
    {
        $file_explode   = explode('.',$file_name);
        $file_extension = isset($file_explode[1])?$file_explode[1]:"";
    }
   
    return $file_extension;
}

function find_key_value_in_array($array, $key, $val)
{
    foreach ($array as $item)
    {
        if (is_array($item) && find_key_value_in_array($item, $key, $val)) return true;

        if (isset($item[$key]) && $item[$key] == $val) return true;
    }

    return false;
}

function find_key_value_in_all_array($array, $key, $val)
{
    $flag = 0;
    foreach ($array as $item)
    {
        if (is_array($item) && find_key_value_in_all_array($item, $key, $val)){
            $flag = 1;
        }else{
            $flag = 0;
        }

        if (isset($item[$key]) && $item[$key] == $val){
            $flag = 1;
        }else{
            $flag = 0;
        }
    }
    if($flag == 1){
        return true;
    }
    return false;
}

function get_client_timezone($user_id = false)
{
    $timezone = '';
    if($user_id!=false)
    {
        $obj_timezone = ClientsModel::select('timezone')->where('user_id',$user_id)->first();
        $timezone = isset($obj_timezone->timezone)?$obj_timezone->timezone:'';
        return $timezone;
    }

    return $timezone;
}

function get_expert_timezone($user_id = false)
{
    $timezone = '';
    if($user_id!=false)
    {
        $obj_timezone = ExpertsModel::select('timezone')->where('user_id',$user_id)->first();
        $timezone = isset($obj_timezone->timezone)?$obj_timezone->timezone:'';
        return $timezone;
    }

    return $timezone;
    
}

function convert_current_utc_time_to_current_timezone_time($timezone = false){

    if($timezone!=false){
        $date = new \DateTime("now", new DateTimeZone($timezone) );
        if(isset($date)){
            return date("g:i A",strtotime($date->format('Y-m-d H:i:s')));
        }
    }
    return '';   
}

function convert_twillo_chat_datetime_to_user_timezone_datetime($date_time = false, $timezone_required = false, $current_timezone = 'UTC')
{
    if($timezone_required == false) {
        $timezone_required = config('app.timezone');
    }

    if($date_time == false || $timezone_required == false ){
        return '';
    }

    $system_timezone = date_default_timezone_get();
    $local_timezone = $current_timezone;
    date_default_timezone_set($local_timezone);
    $local = date("Y-m-d h:i:s A");
 
    date_default_timezone_set("GMT");
    $gmt = date("Y-m-d h:i:s A");
 
    $require_timezone = $timezone_required;
    date_default_timezone_set($require_timezone);
    $required = date("Y-m-d h:i:s A");
 
    date_default_timezone_set($system_timezone);

    $diff1 = (strtotime($gmt) - strtotime($local));
    $diff2 = (strtotime($required) - strtotime($gmt));

    $date = new DateTime($date_time);
    $date->modify("+$diff1 seconds");
    $date->modify("+$diff2 seconds");
    $timestamp = $date->format("D, d M h:i A");
    return $timestamp;
}

function convert_bid_closing_time_to_current_timezone_time($timezone = false,$bid_closing_date_time = false)
{
    if($timezone!=false){
        $date = new DateTime($bid_closing_date_time);
        if(isset($date)){
            $date->setTimezone(new DateTimeZone($timezone));
            return $date->format('Y-m-d H:i:s');
            //return date("Y-m-d H:i:s",strtotime($date->format('Y-m-d H:i:s')));
        }
    }
    return '';  
}

function get_selected_subcategories($value)
{   
    $obj_subcategories = ExpertCategorySubCategoryModel::where(['category_id'=>$value])
                                            ->with(['subcategory_traslation'])
                                            ->get();
      if($obj_subcategories != FALSE)
      {
           $arr_subcategories = $obj_subcategories->toArray();
           return $arr_subcategories;
      }
}

function check_both_review_available($proj_id)
{
    $review_count = 0;
    $review_count = ReviewsModel::where('project_id',$proj_id)->count();
    return $review_count;
}

function project_award_request($expert_id,$project_id)
{
    $project_award_request = 0;

    if($expert_id=='' || $project_id=='')
    {
        return $project_award_request;
    }
    else
    {
        $project_award_request = ProjectRequestsModel::where('project_id',$project_id)
                                                     ->where('expert_user_id',$expert_id)
                                                     ->where('is_accepted','0')
                                                     ->count();
        return $project_award_request;
    }
    return $project_award_request;    
}

function client_ongoing_project_count($client_id)
{
    $ongoing_project_count = 0;
    $ongoing_project_count = ProjectpostModel::where('client_user_id','=',$client_id)
                                          ->where('project_status','=','4')
                                          ->count();
    return $ongoing_project_count;
}

function client_open_contest_count($client_id)
{   
    $open_contest_count = 0;
    $open_contest_count = ContestModel::where('client_user_id','=',$client_id)
                                        ->where('contest_status','=','0')
                                        ->where('payment_status','=','1')
                                        ->count();

    return $open_contest_count;
}




function currency_conversion_api($from_currency, $to_currency)
{
    //dd($from_currency,$to_currency);
    try 
    {
        $CurrencyConversionAPIKey = 'pr_c08bcf2549ca4448972787e789aa7865';//config('app.project.CurrencyConversionAPIKey');
        $from_Currency            = urlencode($from_currency);
        $to_Currency              = urlencode($to_currency);

        $query                    = "{$from_Currency}_{$to_Currency}";
        $url = "https://prepaid.currconv.com/api/v7/convert?q={$query}&compact=ultra&apiKey={$CurrencyConversionAPIKey}";

        $json = file_get_contents($url);
        $obj  = json_decode($json, true);
        if(isset($obj[$query]))
        {
             return floatval($obj[$query]);
        }
        return false;
    } catch(\Exception $e) {
        return false;
    }
}

function old_currency_conversion_api($from_currency, $to_currency)
{
    //dd($from_currency,$to_currency);
    try 
    {
        $CurrencyConversionAPIKey = '9fbb3df01a3fe1753b62';//config('app.project.CurrencyConversionAPIKey');
        $from_Currency            = urlencode($from_currency);
        $to_Currency              = urlencode($to_currency);
        $query                    = "{$from_Currency}_{$to_Currency}";
        $conversion_api_url       = trim("https://free.currencyconverterapi.com/api/v7/convert?q={$query}&compact=ultra&apiKey={$CurrencyConversionAPIKey}");
        $json                     = file_get_contents($conversion_api_url);
        $obj                      = json_decode($json);
        $val                      = floatval($obj->$query);
        return $val;
    } catch(\Exception $e) {
        return $e;
    }
}

function currencyConverterAPI($from_Currency, $to_Currency, $amount)
{
    $data = [];
    $conversion_rate = 1;

    if ($from_Currency == null && $to_Currency == null && $amount == null) {
        return 0;
    } else {
        if ($from_Currency != $to_Currency) {

            $conversion_rates_obj = CurrencyConversionModel::select('from_currency_id', 'to_currency_id', 'conversion_rate')
                                    ->with(['from_currency_detail' =>function($q_fc) {
                                        $q_fc->select('id', 'currency_code');
                                    }, 'to_currency_detail' => function($q_tc) {
                                        $q_tc->select('id', 'currency_code');
                                    }])
                                    ->whereHas('to_currency_detail', function($q_if_tc) use ($to_Currency) {
                                        $q_if_tc->where('currency_code', $to_Currency);
                                    })
                                    ->whereHas('from_currency_detail', function($q_if_fc) use ($from_Currency) {
                                        $q_if_fc->where('currency_code', $from_Currency);
                                    })
                                    ->first();

            if($conversion_rates_obj) {
                $conversion_rates_arr = $conversion_rates_obj->toArray();
            }
            
            if (isset($conversion_rates_arr) && count($conversion_rates_arr) > 0) {
                $conversion_rate = $conversion_rates_arr['conversion_rate'];
            }
        }
        //dd($conversion_rate);
        return floatval($conversion_rate) * floatval($amount);
    }
}

function time_elapsed_string($datetime, $full = false)
{
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function get_tickets_count($type=null, $user_id=null)
{
    $user = Sentinel::check();

    $role = '';

    if($user){
        $role = get_user_role($user->id);
    }

    $obj_ticket = SupportTicketModel::whereHas('get_user_details', function(){})
                                        ->whereHas('get_category', function($q)use($user, $role){
                                            if($role == 'subadmin'){
                                                $q->where('id', $user->support_category_id);
                                            }
                                        })
                                        ->where('thread_id',null);

    if($user_id){
        $obj_ticket = $obj_ticket->where('user_id', $user_id);
    }

    if($type){
        $obj_ticket = $obj_ticket->where('status', $type);
    }
    $obj_ticket = $obj_ticket->count();

    return $obj_ticket;
}

function get_blog_categories()
{
    $arr_categories = [];
    $obj_categories = BlogCategoriesModel::where('is_active', '1')->orderBy('name')->get(['id', 'name', 'slug']);

    if($obj_categories)
    {
        $arr_categories = $obj_categories->toArray();
    }
    return $arr_categories;
}

function filter_data($input='')
{
    $str = trim($input);
    $str = strip_tags($str);
    return $str;
}

function base64ToImage($base64_string, $output_file) 
{
    $file = fopen($output_file, "wb");

    $data = explode(',', $base64_string);

    $data = isset($data[1])?$data[1]:'';

    fwrite($file, base64_decode($data));
    //dd($base64_string, $output_file,base64_decode($data[1]));
    fclose($file);

    return $output_file;
}


function get_added_on_date($created_date)
{
    $date ='';
    if($created_date!="" || $created_date!="0000-00-00 00:00:00")
    {
        $date = date('d-M-Y',strtotime($created_date));
    }
    return $date;
}

function get_phone_code_by_country_code($country_code){

    $arr_country_code = [ "DZ" => "213", "AD" => "376", "AO" => "244", "AI" => "1264", "AG" => "1268", "AR" => "54", "AM" => "374", "AW" => "297", "AU" => "61", "AT" => "43", "AZ" => "994", "BS" => "1242", "BH" => "973", "BD" => "880", "BB" => "1246", "BY" => "375", "BE" => "32", "BZ" => "501", "BJ" => "229", "BM" => "1441", "BT" => "975", "BO" => "591", "BA" => "387", "BW" => "267", "BR" => "55", "BN" => "673", "BG" => "359", "BF" => "226", "BI" => "257", "KH" => "855", "CM" => "237", "CA" => "1", "CV" => "238", "KY" => "1345", "CF" => "236", "CL" => "56", "CN" => "86", "CO" => "57", "KM" => "269", "CG" => "242", "CK" => "682", "CR" => "506", "HR" => "385", "CU" => "53", "CY" => "90392", "CY" => "357", "CZ" => "42", "DK" => "45", "DJ" => "253", "DM" => "1809", "DO" => "1809", "EC" => "593", "EG" => "20", "SV" => "503", "GQ" => "240", "ER" => "291", "EE" => "372", "ET" => "251", "FK" => "500", "FO" => "298", "FJ" => "679", "FI" => "358", "FR" => "33", "GF" => "594", "PF" => "689", "GA" => "241", "GM" => "220", "GE" => "7880", "DE" => "49", "GH" => "233", "GI" => "350", "GR" => "30", "GL" => "299", "GD" => "1473", "GP" => "590", "GU" => "671", "GT" => "502", "GN" => "224", "GW" => "245", "GY" => "592", "HT" => "509", "HN" => "504", "HK" => "852", "HU" => "36", "IS" => "354", "IN" => "91", "ID" => "62", "IR" => "98", "IQ" => "964", "IE" => "353", "IL" => "972", "IT" => "39", "JM" => "1876", "JP" => "81", "JO" => "962", "KZ" => "7","KE" => "254", "KI" => "686", "KP" => "850", "KR" => "82", "KW" => "965", "KG" => "996", "LA" => "856", "LV" => "371", "LB" => "961", "LS" => "266", "LR" => "231", "LY" => "218", "LI" => "417", "LT" => "370", "LU" => "352", "MO" => "853", "MK" => "389", "MG" => "261", "MW" => "265", "MY" => "60", "MV" => "960", "ML" => "223", "MT" => "356", "MH" => "692", "MQ" => "596", "MR" => "222", "YT" => "269","MX" => "52", "FM" => "691", "MD" => "373", "MC" => "377", "MN" => "976", "MS" => "1664", "MA" => "212", "MZ" => "258", "MN" => "95", "NA" => "264", "NR" => "674", "NP" => "977", "NL" => "31", "NC" => "687", "NZ" => "64","NI" => "505", "NE" => "227","NG" => "234", "NU" => "683","NF" => "672", "NP" => "670", "NO" => "47", "OM" => "968", "PW" => "680","PA" => "507", "PG" => "675", "PY" => "595", "PE" => "51", "PH" => "63", "PL" => "48", "PT" => "351", "PR" => "1787", "QA" => "974", "RE" => "262", "RO" => "40", "RU" => "7","RW" => "250","SM" => "378","ST" => "239","SA" => "966","SN" => "221","CS" => "381","SC" => "248","SL" => "232","SG" => "65", "SK" => "421","SI" => "386","SB" => "677","SO" => "252","ZA" => "27","ES" => "34","LK" => "94","SH" => "290","KN" => "1869","SC" => "1758","SD" => "249","SR" => "597","SZ" => "268","SE" => "46","CH" => "41","SI" => "963","TW" => "886","TJ" => "7","TH" => "66","TG" => "228","TO" => "676","TT" => "1868","TN" => "216","TR" => "90","TM" => "7","TM" => "993","TC" => "1649","TV" => "688","UG" => "256","GB" => "44","UA" => "380","AE" => "971","US" => "001","UY" => "598","US" => "1","UZ" => "7","VU" => "678","VA" => "379","VE" => "58","VN" => "84","VG" => "84","VI" => "84","WF" => "681","YE" => "969","YE" => "967", "ZM" => "260", "ZW" => "263"];
    if(isset($arr_country_code[$country_code])) {
        return $arr_country_code[$country_code];
    }
    return '';

}

function get_mangopay_blocked_countries_id()
{
    $arr_blocked_countries = $arr_ids = [];
    $obj_blocked_countries = CountryModel::select('id')->where('is_mangopay_blocked',1)->get();
    if($obj_blocked_countries != FALSE)
    {
        $arr_blocked_countries = $obj_blocked_countries->toArray();
    }

    if(isset($arr_blocked_countries) && count($arr_blocked_countries)>0)
    {
        foreach ($arr_blocked_countries as $key => $value) 
        {
            $arr_ids[] = $value['id'];
        }
    }

    return $arr_ids;
}

function get_currency_description($currency_code)
{
    $currency_description = '';
    $obj_currency = CurrencyModel::where('currency_code',$currency_code)->first();

    $currency_description = isset($obj_currency->description)?$obj_currency->description:'';

    return $currency_description;
}

function get_array_diff_between_two_date($start_date,$end_date)
{
    $arr_result =
                    [
                        'years'          => '',
                        'months'         => '',
                        'days'           => '',
                        'hours'          => '',
                        'minutes'        => '',
                        'seconds'        => '',
                        'str_days_diff' => '',
                    ];

    if($start_date!='' && $end_date!='') 
    {
        $start_date = strtotime($start_date);
        $end_date   = strtotime($end_date);
        
        if(($end_date - $start_date) <= 0) {
            return $arr_result;
        }
        // Formulate the Difference between two dates
        $diff = abs($end_date - $start_date);

        // To get the year divide the resultant date into
        // total seconds in a year (365*60*60*24)
        $years = floor($diff / (365*60*60*24));

        // To get the month, subtract it with years and
        // divide the resultant date into
        // total seconds in a month (30*60*60*24)
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

        // To get the day, subtract it with years and
        // months and divide the resultant date into
        // total seconds in a days (60*60*24)
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

        // To get the hour, subtract it with years,
        // months & seconds and divide the resultant
        // date into total seconds in a hours (60*60)
        $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));

        // To get the minutes, subtract it with years,
        // months, seconds and hours and divide the
        // resultant date into total seconds i.e. 60
        $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);

        // To get the minutes, subtract it with years,
        // months, seconds, hours and minutes
        $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));

        $str_days_diff = '';

        $years   = intval($years);
        $months  = intval($months);
        $days    = intval($days);
        $hours   = intval($hours);
        $minutes = intval($minutes);
        $seconds = intval($seconds);

        $arr_tmp = [];

        if( $days!=0 || $hours!=0 ) {

            if( $days!=0 ) {
                $arr_tmp[] = $days .'d';    
            }

            if( $hours!=0 ) {
                $arr_tmp[] = $hours .'h';    
            }

            if($days == 0 && $minutes!=0) {
                $arr_tmp[] = $minutes .'m';
            }
        } else if( $hours!=0 || $minutes!=0 ) {
            
            if( $hours!=0 ) {
                $arr_tmp[] = $hours .'h';    
            }
            
            if( $minutes!=0 ) {
                $arr_tmp[] = $minutes .'m';    
            }

            if($hours == 0 && $seconds!=0) {
                $arr_tmp[] = $seconds .'s';
            }
        } else if( $minutes!=0 || $seconds!=0 ) {
            
            if( $minutes!=0 ) {
                $arr_tmp[] = $minutes .'m';    
            }
            
            if( $seconds!=0 ) {
                $arr_tmp[] = $seconds .'s';    
            }
        }
        
        if(count($arr_tmp)>0) {
            $str_days_diff = implode(' ', $arr_tmp);
        }

        $arr_result =
                        [
                            'years'          => $years,
                            'months'         => $months,
                            'days'           => $days,
                            'hours'          => $hours,
                            'minutes'        => $minutes,
                            'seconds'        => $seconds,
                            'str_days_diff'  => $str_days_diff
                        ];
    }

    return $arr_result;
}

function project_converted_currency()
{
    $arr_currency = $arr_temp_curr = [];
    $obj_temp_curr = CurrencyModel::select('currency','currency_code')->where('is_active','=','1')->get();
    if($obj_temp_curr)
    {
        $arr_temp_curr = $obj_temp_curr->toArray();
    }

    $currency = $currency_code = '';
    foreach ($arr_temp_curr as $curr_key => $curr_value) 
    {
        $currency =  isset($curr_value['currency'])?$curr_value['currency']:'';
        $currency_code = isset($curr_value['currency_code'])?$curr_value['currency_code']:'';
        $arr_currency[$currency_code] = $currency;
    }

    return $arr_currency;
}

function project_currency()
{
    $arr_currency = $arr_temp_curr = [];
    $obj_temp_curr = CurrencyModel::select('currency','currency_code')->where('is_active','=','1')->get();
    if($obj_temp_curr)
    {
        $arr_temp_curr = $obj_temp_curr->toArray();
    }

    $currency = $currency_code = '';
    foreach ($arr_temp_curr as $curr_key => $curr_value) 
    {
        $currency =  isset($curr_value['currency'])?$curr_value['currency']:'';
        $currency_code = isset($curr_value['currency_code'])?$curr_value['currency_code']:'';
        $arr_currency[$currency] = $currency_code;
    }

    return $arr_currency;
}

?>