<?php

function get_default_logo_image($height=10,$width=10,$txtsize=10, $txt='Image Not Found')
{
   ini_set('memory_limit', '600M' );
   $CACHE_DIR             = 'resize_cache/';
   $CACHE_DIR_BASE_PATH   = base_path().'/uploads/'.$CACHE_DIR;
   $CACHE_DIR_PUBLIC_PATH = url('/').'/uploads/'.$CACHE_DIR;
   $image_file = 'logomissing.jpg';
   $extension  = get_extension($image_file);
   $expected_resize_image_name = generate_resized_image_name($image_file,$width,$height,$extension);
   $real_dir = base_path().'/front/images/';

   if(!image_exists($CACHE_DIR_BASE_PATH.$expected_resize_image_name))
   {
    /* Create Cache Dir */
    $parent_dir =  dirname($real_dir.$expected_resize_image_name);
    @mkdir($CACHE_DIR_BASE_PATH,0777);
    $real_path   = $real_dir.$image_file;   
    $status = Image::make( $real_path )->fit( $width, $height , function ($constraint){
        $constraint->upsize();
    })->save( $CACHE_DIR_BASE_PATH.$expected_resize_image_name );
}
return $CACHE_DIR_PUBLIC_PATH.$expected_resize_image_name;
}


function get_blog_formatted_date($date = '')
{
    $formatted_date = '';

    if($date != '')
    {
        $formatted_date   = date('d F Y',strtotime($date));
    }
    return $formatted_date;
}

function encrypt_data($value='')
{
    $output = '';
    $secret_key     = '741852963';
    $secret_iv      = '789456123';
    $output         = false;
    $encrypt_method = "AES-256-CBC";
    $key            = hash( 'sha256', $secret_key );
    $iv             = substr( hash( 'sha256', $secret_iv ), 0, 16 );
    $output         = base64_encode( openssl_encrypt( $value, $encrypt_method, $key, 0, $iv ));
    return $output;
}

function decrypt_data($value='')
{
    $output = '';
    $secret_key     = '741852963';
    $secret_iv      = '789456123';
    $output         = false;
    $encrypt_method = "AES-256-CBC";
    $key            = hash( 'sha256', $secret_key );
    $iv             = substr( hash( 'sha256', $secret_iv ), 0, 16 );
    $output         = openssl_decrypt( base64_decode( $value ), $encrypt_method, $key, 0, $iv );
    return $output;
}

function get_comment_date_time($date = '')
{
    $formatted_date = '';

    if($date != '')
    {
        $formatted_date = date('F d, Y - h:i a',strtotime($date));
    }
    return $formatted_date;
}

/*Usage:  get_resized_image_path('d3f5be31e24a194366da81e5ad76a8328e1159df.jpeg','/uploads/cities/',250,260);*/
function get_resized_image_path($image_file = FALSE,$dir=FALSE,$height=250,$width=250,$fallback_text="Default Image"){
    ini_set('memory_limit','600M');
    $CACHE_DIR = 'resize_cache/';
    $dir       = rtrim($dir,"/");
    $dir       = ltrim($dir,"/");
    $base_path = rtrim(base_path(),"/") ;
    $real_dir  = $base_path."/public/".$dir."/";
    $extension = get_extension($image_file);
    if($image_file == FALSE || $dir == FALSE){
         return "https://placeholdit.imgix.net/~text?txtsize=15&txt=".$fallback_text."&w=".$width."&h=".$height;
    }
    /* Check if File Exists */
    if(!image_exists($real_dir.$image_file)){
        return "https://placeholdit.imgix.net/~text?txtsize=15&txt=".$fallback_text."&w=".$width."&h=".$height;
    }
    /* Check if Given file is image*/
    if(!is_valid_image($real_dir.$image_file)){
        return "https://placeholdit.imgix.net/~text?txtsize=33&txt=No+Image&w=".$width."&h=".$height;
    }
    /* Generate Expected Resized Image Name */
    $expected_resize_image_name = generate_resized_image_name($image_file,$width,$height,$extension);
    if(!image_exists($real_dir.$CACHE_DIR.$expected_resize_image_name)){
        /* Create Cache Dir */
        $parent_dir =  dirname($real_dir.$expected_resize_image_name);
        $resize_cache_dir  = $parent_dir."/".$CACHE_DIR;
        @mkdir($resize_cache_dir,0777);
        $real_path   = $real_dir.$image_file;   
        /*  Image::make( $real_path )->resize( $width, $height )->save( $resize_cache_dir.'/'.$expected_resize_image_name ); */
        $status = Image::make( $real_path )
                        ->resize($width, $height, function ($constraint) {
                            $constraint->aspectRatio();
                        })
                        ->resizeCanvas($width, $height, 'center', false, array(255, 255, 255, 0))
                        ->save( $resize_cache_dir.'/'.$expected_resize_image_name );

    }
    return url('/public')."/".$dir."/".$CACHE_DIR.$expected_resize_image_name;
}
function get_extension($image_file){
    $arr_part = array();
    $arr_part = explode('.', $image_file);
    return end($arr_part);
}
function is_valid_image($image_real_path){
    return getimagesize($image_real_path);
}
function image_exists($image_real_path){
    if (!file_exists($image_real_path) || !is_readable($image_real_path)){
        return FALSE;
    } 
    return TRUE;
}
function generate_resized_image_name($file_name,$width,$height,$extension){
    return substr($file_name, 0, strrpos($file_name, '.')) . '-' . $width . 'x' . $height . '.' . $extension;
}
function add_watermark($path,$imageName,$text=FALSE) {
    $watermark  = \Image::make(public_path('front/images/watermark/virtual-logo.png'))->opacity(20);
    $img        = \Image::make(public_path($path.$imageName));
    //#1
    $watermarkSize = $img->width() - 20; //size of the image minus 20 margins
    //#2
    $watermarkSize = $img->width() / 2; //half of the image size
    //#3
    $resizePercentage = 50;//70% less then an actual image (play with this value)
    $watermarkSize = round($img->width() * ((100 - $resizePercentage) / 100), 2); //watermark will be $resizePercentage less then the actual width of the image
    // resize watermark width keep height auto
    $watermark->resize($watermarkSize, null, function ($constraint) {
    $constraint->aspectRatio();
    });
    //insert resized watermark to image center aligned //top-left , top-right , center , bottom-left , bottom-right
    //$img->insert($watermark, 'top-left', 10, 10); 
    //$img->insert($watermark, 'top-right', 10, 10);
    $img->insert($watermark, 'center', 10, 10); 
    //$img->insert($watermark, 'bottom-left', 10, 10); 
    //$img->insert($watermark, 'bottom-right', 10, 10); 
    // add text 
    //$img->text('Uploded by : Tushar ahire', 10, 10)->opacity(30);

    if(isset($text) && $text != ""){
        $img->text($text, 0, 0, function($font) {  
              $font->file(public_path('front/fonts/roboto-light-webfont.ttf'));  
              $font->size(14);  
              $font->color('#e1e1e1');  
              $font->align('left');  
              $font->valign('top');  
              $font->angle(0);  
        });
    }

    //save new image
    $img->save(public_path($path.$imageName));
    if($img){
        return true;
    }
    else
    {
        return false;
    }
}


function get_default_image($height=10,$width=10,$txtsize=10, $txt='Image Not Found')
{
    return "https://assets.imgix.net/~text?txtsize=".$txtsize."&txt=".$txt."&w=".$width."&h=".$height.'&txtalign=center,middle&bg=7d7d7d&txtclr=fff&border-radius=5';
}

function get_image_upload_note($for='',$height=10,$width=10, $size=2)
{
    $txt = '';
    if(isset($for) && $for!=false && $for=='admin_profile')
    {
       $txt = '<span>Allowed only jpg | jpeg | png  '."<br>".'Please upload image Height and Width greater than or equal to '.$height.' X '.$width.' with maximum size '.$size.'MB for best result.</span>';
   }
   elseif(isset($for) && $for!=false && $for=='advertisement')
   {
       $txt = 'Allowed only jpg | jpeg | png | bmp image.'.'<br/>'.'You can drag image to re-postion or use slider to zoom-in or zoom-out image.'.'<br/>'.'For best result upload image Height and Width greater than or equal to '.$height.' X '.$width.' for best result.';
   }

   return $txt;
}
?>