<?php

use App\Models\CountryModel; 
use App\Models\StateModel; 
use App\Models\CityModel;


   /*
    | Comment: This function will return array of countries
    | auther : Sagar Sainkar
    */
    
	function get_countries()
	{
		$arr_countries = array();		
		$obj_countries = CountryModel::get();

		if (isset($obj_countries) && $obj_countries!=FALSE) 
		{
			$arr_countries = $obj_countries->toArray();
		}

		return $arr_countries;
	}


 	/*
    | get_states : Ajax fuction to get all states for specific country
    | auther :Sagar Sainkar
    | 
    */ 

    function get_states($country_id=null)
    {
    	$response_data = array();
    	if ($country_id) 
    	{
    		$country_id = base64_decode($country_id);

    		$obj_states = StateModel::where('country_id',$country_id)->orderBy('state_name', 'asc')->get();

	        if($obj_states != FALSE)
	        {
	            $response_data['states'] = $obj_states->toArray();
	            $response_data['status'] = 'success';
	            $response_data['msg'] = 'States found.';
	        }
	        else
	        {
	        	$response_data['states'] = null;
	            $response_data['status'] = 'error';
	            $response_data['msg'] = 'No state found for this country';
	        }
    	}
    	else
    	{
    		$response_data['states'] = null;
	        $response_data['status'] = 'error';
	        $response_data['msg'] = 'No state found for this country';
    	}

    	echo json_encode($response_data);
	    exit();
    }

    /*
    | get_states : Ajax fuction to get all cities for specific states
    | auther :Sagar Sainkar
    | 
    */ 

    function get_cities($state_id=null)
    {
    	$response_data = array();
    	if ($state_id) 
    	{
    		$state_id = base64_decode($state_id);

    		$obj_states = CityModel::where('state_id',$state_id)->orderBy('city_name', 'asc')->get();

	        if($obj_states != FALSE)
	        {
	            $response_data['cities'] = $obj_states->toArray();
	            $response_data['status'] = 'success';
	            $response_data['msg'] = 'States found.';
	        }
	        else
	        {
	        	$response_data['cities'] = null;
	            $response_data['status'] = 'error';
	            $response_data['msg'] = 'No city found for this state';
	        }
    	}
    	else
    	{
    		$response_data['cities'] = null;
	        $response_data['status'] = 'error';
	        $response_data['msg'] = 'No city found for this state';
    	}

    	echo json_encode($response_data);
	    exit();
    }

    /*
    Comments : Get country name from id .
    Auther   : Nayan S.
    */

    function get_country_name($country_id)
    {
        $country = "";
        $obj_country = CountryModel::where('id',$country_id)->first();

        if($obj_country)
        {
            $country = $obj_country->country_name;
        }
        return $country;
    }
   
    /*
    Comments : Get City name from id.
    Auther   : Nayan S.
    */

    function get_city_name($city_id)
    {
        $city = "";
        $obj_city = CityModel::where('id',$city_id)->first();

        if($obj_city)
        {
            $city = $obj_city->city_name;
        }
        return $city;
    }


?>