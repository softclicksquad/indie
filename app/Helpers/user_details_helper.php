<?php
use App\Models\UserModel; 
use App\Models\ClientsModel; 
use App\Models\ExpertsModel;
use App\Models\ProjectManagerModel;
use App\Models\RecruiterModel;
use App\Models\SubscriptionUsersModel;
use App\Models\ProjectpostModel;
use App\Models\ReviewsModel;
use App\Models\ProjectsBidsModel;
use App\Models\MilestonesModel;
use App\Models\MilestoneReleaseModel;
use App\Models\InviteProjectModel;
use App\Models\ContestModel;
use App\Models\ContestEntryModel;
use App\Models\ProjectRequestsModel;
use App\Models\UserWalletModel;

use App\Common\Services\WalletService;
    /*
    | Comment: This function will return details of user
    | auther : Ashwini K
    */
	function get_user_details()
	{
        $arr_details = array();
        $user        = Sentinel::check();
         if($user){
            if($user->inRole('client')){
              $obj_details = ClientsModel::where('user_id',$user->id)->with(['user_details','review_details','project_details'])->first(['id','user_id','first_name','last_name','profile_image']);
            }
            else if($user->inRole('expert')){
              $obj_details = ExpertsModel::where('user_id',$user->id)->with(['user_details','review_details','project_details'])->first(['id','user_id','first_name','last_name','profile_image']);
            }
            else{
              $obj_details = ProjectManagerModel::where('user_id',$user->id)->with('user_details')->first(['id','user_id','first_name','last_name','profile_image']);
            }
         }
		if (isset($obj_details) && $obj_details!=FALSE) {
			$arr_details = $obj_details->toArray();
		}
		return $arr_details;
	}
    /*this funtion gives username */
    function get_username($id)
    {
        $user_name = "NA";
        $chk_user = Sentinel::findById($id); 
        if(isset($chk_user) && $chk_user != FALSE){
            if(isset($chk_user->user_name)){  
                $user_name = $chk_user->user_name;
            }
        }
        return  $user_name;
    }

    function get_user_email($id)
    {
        $email = "";
        $chk_user = Sentinel::findById($id); 
        if(isset($chk_user) && $chk_user != FALSE){
            if(isset($chk_user->email)){  
                $email = $chk_user->email;
            }
        }
        return  $email;
    }

    function get_user_role($user_id)
    {
      $role_slug = "";
      if($user_id != null){
        $obj_role  = Sentinel::findById($user_id)->roles()->first();
        }      
    
      if(isset($obj_role) && $obj_role != null){
        $role_slug = $obj_role->slug; 
      }
      return $role_slug;
    }

    function sidebar_information($user_id)
    {
        $user_role    = get_user_role($user_id);    
        $sidebar_data = array();
        if( $user_role == "expert" ){
            $obj_subscription        = FALSE;
            $obj_user                = FALSE;
            $obj_expert_details      = FALSE;
            $project_count           = 0;
            $review_count            = 0;
            $average_rating          = 0;
            $completion_rate         = 0.0;
            $reputation              = 0.0;
            $bid_count               = 0;
            $total_project_cost      = 0;
            $completed_project_count = 0;

            // Get expert current subscription
            $obj_subscription = SubscriptionUsersModel::where('user_id',$user_id)
                                                        ->where('is_active','1')
                                                        ->orderBy('id','DESC')
                                                        ->limit(1)
                                                        ->with(['transaction_details'])
                                                        ->whereHas('transaction_details',function ($query)
                                                        {
                                                            $query->whereIN('payment_status',array('1','2'));
                                                        })->first(['pack_name','subscription_pack_id']);

             if($obj_subscription != FALSE && isset($obj_subscription->pack_name)){
                $sidebar_data['pack_name'] = $obj_subscription->pack_name;
                $sidebar_data['subscription_pack_id'] = $obj_subscription->subscription_pack_id;
             }

             /*get expert avg rating count*/ 
             $average_rating = ReviewsModel::where('to_user_id',$user_id)->avg('rating');
             $sidebar_data['average_rating'] = number_format($average_rating,1);

             /*get expert all projects count*/ 
             $project_count                            = ProjectpostModel::where('expert_user_id',$user_id)->where('project_status','!=','0')->count();
             $sidebar_data['project_count']            = $project_count;
             $applied_project_count                    = ProjectsBidsModel::with(['project_details.skill_details','project_details.category_details','project_details.project_skills.skill_data'])
                                                         ->where('expert_user_id',$user_id)
                                                         ->whereHas('project_details',function ($query) 
                                                         { 
                                                           $query->where('project_status','=',2);
                                                           $query->where('is_hire_process','<>','YES');
                                                         })->count();
             $sidebar_data['applied_project_count']    = $applied_project_count;
             

             $awarded_project_count                    = ProjectRequestsModel::where('expert_user_id',$user_id)
                                                        ->where('is_accepted','0')
                                                        ->with('project_details','project_details.skill_details','project_details.project_skills.skill_data','project_details.client_details')
                                                        ->with(['project_details.project_bid_info' => function ($q) use($user_id) {
                                                            $q->where('expert_user_id','=' ,$user_id);
                                                        }])
                                                        ->count();
             $sidebar_data['awarded_project_count']     = $awarded_project_count;


             $ongoing_project_count                     = ProjectpostModel::where('expert_user_id',$user_id)->where('project_status','=','4')->count();
             $sidebar_data['ongoing_project_count']     = $ongoing_project_count;

             $completed_project_count                   = ProjectpostModel::where('expert_user_id',$user_id)->where('project_status','=','3')->count();
             $sidebar_data['completed_project_count']   = $completed_project_count;

             $canceled_project_count                    = ProjectpostModel::where('expert_user_id',$user_id)->where('project_status','=','5')->count();
             $sidebar_data['canceled_project_count']    = $canceled_project_count;

             $invitation_project_count                  = InviteProjectModel::where('expert_user_id',$user_id)->where('expire_on','>',date('Y-m-d H:i:s'))/*->where('is_read','NO')*/->count();
             $sidebar_data['invitation_project_count']  = $invitation_project_count;

             /*get total complted project count  and calculate completion rate*/
             if ($completed_project_count>0 && $project_count>0){
                 $completion_rate = ($completed_project_count/$project_count)*100;
             }
             $sidebar_data['completion_rate'] = number_format($completion_rate,0);

             /*get expert review count*/ 
             $review_count = ReviewsModel::where('to_user_id',$user_id)->count();
             $sidebar_data['review_count'] = $review_count;
              

             $contest = ContestEntryModel::where('expert_id',$user_id);
             $contest_applied = $contest->count();
             $sidebar_data['contest_applied_count'] = $contest_applied;
             
             $contest->where('is_winner','YES');
             $contest_win_count = $contest->count();
             $sidebar_data['contest_win_count'] = $contest_win_count;

             /*calculate reputation for expert with number of bids he added and number project he get*/ 
             $bid_count = ProjectsBidsModel::where('expert_user_id',$user_id)->count();
             if ($bid_count>0 && $project_count>0) 
             {
                 $reputation = ($project_count/$bid_count)*100;
             }
             $sidebar_data['reputation'] = number_format($reputation,0);
             
             /*get user availablity */
             $obj_user = UserModel::where('id',$user_id)->first(['is_available','last_login','last_logout']);
             if ($obj_user!=FALSE && isset($obj_user->last_logout)) 
             {
                $sidebar_data['last_login'] = $obj_user->last_logout;    
             }
             else{
                $sidebar_data['last_login'] = '0000-00-00 00:00:00';  
             }
             /* expert login duration*/
                    $sidebar_data['last_login_duration'] = '0 s';
                    
                    $sidebar_data['last_login_full_duration'] = '0 seconds';
                    if($sidebar_data['last_login'] != "0000-00-00 00:00:00"){
                    $datetime1  = new DateTime();
                    $datetime2  = new DateTime($sidebar_data['last_login']);
                    $interval   = $datetime1->diff($datetime2);

                    $year       = $interval->format('%y');
                    $month      = $interval->format('%m');
                    $day        = $interval->format('%d');
                    $hours      = $interval->format('%h');
                    $minutes    = $interval->format('%i');
                    $second     = $interval->format('%s');
                    $y = $mon = $d = $h = $m = $s= "";
                    if($year > 1){
                       if(isset($year)      &&  $year    !=  "0"){  $y    =  $year.' years ';    }
                    } else {
                       if(isset($year)      &&  $year    !=  "0"){  $y    =  $year.' year ';    }
                    }
                    if($month > 1){
                       if(isset($month)     &&  $month   !=  "0"){  $mon  =  $month.' months ';   }
                    } else {
                       if(isset($month)     &&  $month   !=  "0"){  $mon  =  $month.' month ';   }
                    }
                    if($day > 1){
                       if(isset($day)       &&  $day     !=  "0"){  $d    =  $day.' days ';     }
                    } else {
                       if(isset($day)       &&  $day     !=  "0"){  $d    =  $day.' day ';     }
                    }
                    if($hours > 1){
                       if(isset($hours)     &&  $hours   !=  "0"){  $h    =  $hours.' hours ';   }
                    } else {
                       if(isset($hours)     &&  $hours   !=  "0"){  $h    =  $hours.' hour ';   }
                    }
                    if($minutes > 1){
                       if(isset($minutes)   &&  $minutes !=  "0"){  $m    =  $minutes.' minutes ';   }
                    } else {
                       if(isset($minutes)   &&  $minutes !=  "0"){  $m    =  $minutes.' minute ';   }
                    }
                    if($second > 1){
                       if(isset($second)    &&  $second  !=  "0"){  $s    =  $second.' seconds ';  }
                    } else {
                       if(isset($second)    &&  $second  !=  "0"){  $s    =  $second.' second ';  }
                    }  
                    //$last_login_duration   = $y.$mon.$d.$h.$m.$s;
                    /*if($y != "" && $mon !=""){
                    $last_login_duration   = $y.$mon;
                    }else if($mon != "" && $d !=""){
                    $last_login_duration   = $mon.$d;
                    }else if($d != "" && $h !=""){
                    $last_login_duration   = $d.$h;
                    }else if($h != "" && $m !=""){
                    $last_login_duration   = $h.$m;
                    }else if($m != "" && $s !=""){
                    $last_login_duration   = $m.$s;
                    }else{
                    $last_login_duration   = $s;
                    }*/

                    if($y != ""){
                    $last_login_duration   = $y.' ago';
                    }else if($mon != ""){
                    $last_login_duration   = $mon.' ago';
                    }else if($d != ""){
                    $last_login_duration   = $d.' ago';
                    }else if($h != ""){
                    $last_login_duration   = $h.' ago';
                    }else if($m != ""){
                    $last_login_duration   = $m.' ago';
                    }else{
                    $last_login_duration   = $s.' ago';
                    }

                    $sidebar_data['last_login_duration']      = $last_login_duration;
                    $sidebar_data['last_login_full_duration'] = trans('expert_listing/listing.text_recently_active').' : ( '.$y.$mon.$d.$h.$m.$s.' ) '.trans('expert_listing/listing.text_ago');
                    }else
                    {
                        $sidebar_data['last_login_duration']      = trans('common/header.text_active'); 
                        $sidebar_data['last_login_full_duration'] = trans('common/header.text_active'); 
                    }
             /* end expert login duration*/
             /* expert last login*/
             if(Session::has('last_login') && Session::get('last_login') != "0000-00-00 00:00:00"){
                $sidebar_data['last_login']          = date("g:i A",strtotime(Session::get('last_login'))).",".date('j M y',strtotime(Session::get('last_login')));
             }
             else {
                $sidebar_data['last_login']          = date("g:i A",strtotime(date('Y-m-d H:i:s'))).",".date('j M y',strtotime(date('Y-m-d H:i:s')));
             }
             // last_login
             $sidebar_data['tbl_last_login'] = isset($obj_user->last_login) ? $obj_user->last_login : '';

             /* end expert last login*/

             if ($obj_user!=FALSE && isset($obj_user->is_available)){
                $sidebar_data['is_available'] = $obj_user->is_available;    
             }

             $obj_expert_details = ExpertsModel::where('user_id',$user_id)->with('expert_spoken_languages','country_details','expert_skills','expert_categories','expert_subcategories')->first(['id','user_id','first_name','last_name','profile_image','timezone','country','cover_image']);
             $sidebar_data['ex_skills']       = '';
             $sidebar_data['ex_category_arr'] = '';
             $ex_skills                       = '';
             $ex_category                       = '';
             $ex_subcategory                       = '';
             $ex_category_arr                 = '&category=';
             $ex_cat_arr                      = '&cat[]=';
             $ex_subcategory_arr              = '&subcategory=';
             $ex_subcat_arr                   = '&subcat[]=';
             $arr_category_ids = [];
             $arr_sub_category_ids = [];
             $arr_skills_ids = [];

             if ($obj_expert_details!=FALSE) 
             {
                 $sidebar_data['user_details']  =$obj_expert_details->toArray();
                 foreach ($sidebar_data['user_details']['expert_skills'] as $key => $skill) 
                 {
                    $arr_skills_ids[] = base64_encode($skill['skill_id']);
                    $ex_skills .= '&skills%5B%5D='.base64_encode($skill['skill_id']);
                 }

                 // foreach ($sidebar_data['user_details']['expert_categories'] as $key => $category) 
                 // {
                 //      if($key == 0)
                 //      {
                 //        $ex_category_arr .= base64_encode($category['category_id']);
                 //        $ex_cat_arr .= base64_encode($category['category_id']);
                 //      }
                 // }



                foreach ($sidebar_data['user_details']['expert_categories'] as $category_key => $category) 
                {
                    $arr_category_ids[] = base64_encode($category['category_id']);
                    $ex_category .= '&category%5B%5D='.base64_encode($category['category_id']);
                }

                $ex_category_arr = $ex_category;
                $ex_cat_arr  = $ex_category;


                foreach ($sidebar_data['user_details']['expert_subcategories'] as $subcategory_key => $subcategory) 
                {
                    $arr_sub_category_ids[] = base64_encode($subcategory['subcategory_id']);
                    $ex_subcategory .= '&subcategory%5B%5D='.base64_encode($subcategory['subcategory_id']);
                }

                $ex_subcategory_arr = $ex_subcategory;
                $ex_subcat_arr = $ex_subcategory;

                $max_fixed_amt = ProjectpostModel::where('project_pricing_method','1')
                                                    ->where('project_status','2')
                                                    ->whereDate('projects.bid_closing_date','>',date('Y-m-d h:i:s'))
                                                    ->where('projects.is_hire_process','NO')
                                                    ->max('usd_max_project_cost');
                                                    //->max('max_project_cost');

                $min_fixed_amt = ProjectpostModel::where('project_pricing_method','1')
                                                        ->where('project_status','2')
                                                        ->whereDate('projects.bid_closing_date','>',date('Y-m-d h:i:s'))
                                                        ->where('projects.is_hire_process','NO')
                                                        ->min('usd_min_project_cost');
                                                        //->min('min_project_cost');

                $max_project_amt   = ProjectpostModel::where('project_pricing_method','2')
                                                            ->where('project_status','2')
                                                            ->whereDate('projects.bid_closing_date','>',date('Y-m-d h:i:s'))
                                                            ->where('projects.is_hire_process','NO')
                                                            ->max('usd_max_project_cost');
                                                            //->max('max_project_cost');

                $min_project_amt   = ProjectpostModel::where('project_pricing_method','2')
                                                            ->where('project_status','2')
                                                            ->whereDate('projects.bid_closing_date','>',date('Y-m-d h:i:s'))
                                                            ->where('projects.is_hire_process','NO')
                                                            ->min('usd_min_project_cost');
                                                            //->min('min_project_cost');



                
                 $sidebar_data['max_fixed_amt']      = $max_fixed_amt;
                 $sidebar_data['min_fixed_amt']      = $min_fixed_amt;
                 $sidebar_data['max_project_amt']    = $max_project_amt;
                 $sidebar_data['min_project_amt']    = $min_project_amt;

                 $sidebar_data['ex_skills']          = $ex_skills;
                 $sidebar_data['ex_category_arr']    = $ex_category_arr;
                 $sidebar_data['ex_cat_arr']         = $ex_cat_arr;
                 $sidebar_data['ex_subcategory_arr'] = $ex_subcategory_arr;
                 $sidebar_data['ex_subcat_arr']      = $ex_subcat_arr;
                 $sidebar_data['arr_category_ids']     = $arr_category_ids;
                 $sidebar_data['arr_sub_category_ids'] = $arr_sub_category_ids;
                 $sidebar_data['arr_skills_ids']       = $arr_skills_ids;

             }
             if(isset($sidebar_data['user_details']['timezone']) && $sidebar_data['user_details']['timezone'] != null){
                $date = new DateTime("now", new DateTimeZone($sidebar_data['user_details']['timezone']) );
                $user_country = "";
                $user_flag    = "";
                $user_country_flag = "";
                
                /* check country */
                if(isset($sidebar_data['user_details']['country_details']['country_name']) && $sidebar_data['user_details']['country_details']['country_name'] !=""){
                    $user_country = $sidebar_data['user_details']['country_details']['country_name'].',';
                }
                /* check flag */
                if(isset($sidebar_data['user_details']['country_details']['country_code']) && $sidebar_data['user_details']['country_details']['country_code'] !=""){
                    $flag = substr(strtolower($sidebar_data['user_details']['country_details']['country_code']), 0,2);
                    
                    if(is_file('public/front/images/flags/'.$flag.'.png')){               
                        $user_flag = $flag.'.png';
                    }

                    //dd($user_flag);
                }
                /* set country or flag */
                if(isset($user_flag) && $user_flag != ""){
                   $user_country_flag = '<img src="'.url('/public').'/front/images/flags/'.$user_flag.'">';
                }
                // else
                // {
                //    $user_country_flag = '<img src="'.url('/').'/front/images/flags/id.png">'; 
                // }

                $sidebar_data['user_country_flag']           = $user_country_flag;
                $sidebar_data['user_country']                = $user_country;
                $sidebar_data['expert_timezone']             = $user_country.date("g:i A",strtotime($date->format('Y-m-d H:i:s'))).",".date('j M y',strtotime($date->format('Y-m-d H:i:s')));
                $sidebar_data['expert_timezone_without_date']= date("g:i A",strtotime($date->format('Y-m-d H:i:s')));
             }
            /*get total earn cost by milestone for expert in current year*/
            /*$total_project_cost = MilestonesModel::where('expert_user_id',$user_id)->whereYear('created_at', '=', date('Y'))->sum('cost');*/
            $total_project_cost = MilestonesModel::where('expert_user_id',$user_id)->whereHas('transaction_details',function ($query)
            {
                $query->whereIN('payment_status',array('1','2'));
               
            })->sum('cost_to_expert');

            if ($total_project_cost) 
            {
                if($total_project_cost >= '1000' && $total_project_cost <= '1999'){
                      $sidebar_data['total_project_cost'] = '1k+'; 
                  } else if($total_project_cost >= '2000' && $total_project_cost <= '4999'){
                      $sidebar_data['total_project_cost'] = '2k+'; 
                  } else if($total_project_cost >= '5000' && $total_project_cost <= '9999'){
                      $sidebar_data['total_project_cost'] = '5k+'; 
                  } else if($total_project_cost >= '10000' && $total_project_cost <= '19999'){
                      $sidebar_data['total_project_cost'] = '10k+'; 
                  } else if($total_project_cost >= '20000' && $total_project_cost <= '49999'){
                      $sidebar_data['total_project_cost'] = '20k+'; 
                  } else if($total_project_cost >= '50000' && $total_project_cost <= '99999'){
                      $sidebar_data['total_project_cost'] = '50k+'; 
                  } else if($total_project_cost >= '100000'){
                      $sidebar_data['total_project_cost'] = '100k+'; 
                  } else {
                      $sidebar_data['total_project_cost']= number_format($total_project_cost);  
                  }
            }
        }
        elseif ( $user_role == "client" ) 
        {
            $obj_user = FALSE;
            $obj_client_details = FALSE;
            $project_count = 0;
            $review_count = 0;
            $average_rating =0;
            $completion_rate = 0.0;
            $reputation =0.0;
            $project_except_cancle_count =0;
            $total_project_cost = 0;

           
             /*get client avg rating count*/ 
             $average_rating = ReviewsModel::where('to_user_id',$user_id)->avg('rating');
             $sidebar_data['average_rating'] = number_format($average_rating,1);

             /*get client all projects count*/ 
             $project_count                           = ProjectpostModel::where('client_user_id',$user_id)->where('project_status','!=','0')->count();
             $sidebar_data['project_count']           = $project_count;

             $posted_project_count                    = ProjectpostModel::where('client_user_id',$user_id)->where(function ($q) 
                                                                        {
                                                                          $q->where('project_status','=','1');
                                                                          $q->orWhere('project_status','=','2');

                                                                        })
                                                                        ->where('is_hire_process','<>','YES')
                                                                        ->count();
             $sidebar_data['posted_project_count']    = $posted_project_count;

             $awarded_project_count                   = ProjectpostModel::where('client_user_id',$user_id)
                                                                        ->where('project_status','=','2')
                                                                        ->whereHas('project_bid_info',function ($query)
                                                                        {
                                                                          $query->where('bid_status',4);
                                                                        })
                                                                        ->count();
             $sidebar_data['awarded_project_count']   = $awarded_project_count;

             $ongoing_project_count                   = ProjectpostModel::where('client_user_id',$user_id)->where('project_status','=','4')->count();
             $sidebar_data['ongoing_project_count']   = $ongoing_project_count;

             $completed_project_count                 = ProjectpostModel::where('client_user_id',$user_id)->where('project_status','=','3')->count();
             $sidebar_data['completed_project_count'] = $completed_project_count;

             $canceled_project_count                  = ProjectpostModel::where('client_user_id',$user_id)->where('project_status','=','5')->count();
             $sidebar_data['canceled_project_count']  = $canceled_project_count;
             /*end client all projects count*/ 
             /*get total complted project count  and calculate completion rate*/
             if ($completed_project_count>0 && $project_count>0) 
             {
                 $completion_rate = ($completed_project_count/$project_count)*100;
             }
             $sidebar_data['completion_rate'] = number_format($completion_rate,0);

             /*get client review count*/ 
             $review_count = ReviewsModel::where('to_user_id',$user_id)->count();
             $sidebar_data['review_count'] = $review_count;

             /*calculate reputation for client with number of bids he added and number project he get*/ 
             $project_except_cancle_count = ProjectpostModel::where('client_user_id',$user_id)->whereNotIN('project_status',array('5','0'))->count();

             if ($project_except_cancle_count>0 && $project_count>0) 
             {
                 $reputation = ($project_except_cancle_count/$project_count)*100;
             }
             $sidebar_data['reputation'] = number_format($reputation,0);

             $contest_posted = ContestModel::where('client_user_id',$user_id)
                                            ->where('is_active','0')
                                            ->where('payment_status','=','1')
                                            ->whereDoesntHave('contest_entry',function($q){})
                                            ->count();

             $contest_ongoing = ContestModel::where('client_user_id',$user_id)
                                            ->where('is_active','0')
                                            ->where('payment_status','=','1')
                                            ->whereHas('contest_entry',function($q){})
                                            ->count();
             
             $sidebar_data['contest_posted_count'] = $contest_posted + $contest_ongoing;

             $sidebar_data['contest_and_project_posted_count'] = ($sidebar_data['contest_posted_count'] + $sidebar_data['posted_project_count']);

             /*get user availablity */
             $obj_user = UserModel::where('id',$user_id)->first(['is_available','last_login','last_logout']);

             if ($obj_user!=FALSE && isset($obj_user->last_logout)) 
             {
                $sidebar_data['last_login'] = $obj_user->last_logout;    
             }
             else{
                $sidebar_data['last_login'] = '0000-00-00 00:00:00';  
             }

             /* client login duration*/
                    $sidebar_data['last_login_duration'] = '0 s';
                    
                    $sidebar_data['last_login_full_duration'] = '0 seconds';
                    if($sidebar_data['last_login'] != "0000-00-00 00:00:00"){
                    $datetime1  = new DateTime();
                    $datetime2  = new DateTime($sidebar_data['last_login']);
                    $interval   = $datetime1->diff($datetime2);

                    $year       = $interval->format('%y');
                    $month      = $interval->format('%m');
                    $day        = $interval->format('%d');
                    $hours      = $interval->format('%h');
                    $minutes    = $interval->format('%i');
                    $second     = $interval->format('%s');
                    $y = $mon = $d = $h = $m = $s= "";
                    if($year > 1){
                       if(isset($year)      &&  $year    !=  "0"){  $y    =  $year.' years ';    }
                    } else {
                       if(isset($year)      &&  $year    !=  "0"){  $y    =  $year.' year ';    }
                    }
                    if($month > 1){
                       if(isset($month)     &&  $month   !=  "0"){  $mon  =  $month.' months ';   }
                    } else {
                       if(isset($month)     &&  $month   !=  "0"){  $mon  =  $month.' month ';   }
                    }
                    if($day > 1){
                       if(isset($day)       &&  $day     !=  "0"){  $d    =  $day.' days ';     }
                    } else {
                       if(isset($day)       &&  $day     !=  "0"){  $d    =  $day.' day ';     }
                    }
                    if($hours > 1){
                       if(isset($hours)     &&  $hours   !=  "0"){  $h    =  $hours.' hours ';   }
                    } else {
                       if(isset($hours)     &&  $hours   !=  "0"){  $h    =  $hours.' hour ';   }
                    }
                    if($minutes > 1){
                       if(isset($minutes)   &&  $minutes !=  "0"){  $m    =  $minutes.' minutes ';   }
                    } else {
                       if(isset($minutes)   &&  $minutes !=  "0"){  $m    =  $minutes.' minute ';   }
                    }
                    if($second > 1){
                       if(isset($second)    &&  $second  !=  "0"){  $s    =  $second.' seconds ';  }
                    } else {
                       if(isset($second)    &&  $second  !=  "0"){  $s    =  $second.' second ';  }
                    }  

                    //$last_login_duration   = $y.$mon.$d.$h.$m.$s;
                    
                    /*if($y != "" && $mon !=""){
                    $last_login_duration   = $y.$mon;
                    }else if($mon != "" && $d !=""){
                    $last_login_duration   = $mon.$d;
                    }else if($d != "" && $h !=""){
                    $last_login_duration   = $d.$h;
                    }else if($h != "" && $m !=""){
                    $last_login_duration   = $h.$m;
                    }else if($m != "" && $s !=""){
                    $last_login_duration   = $m.$s;
                    }else{
                    $last_login_duration   = $s;
                    }*/

                    if($y != ""){
                    $last_login_duration   = $y.' ago';
                    }else if($mon != ""){
                    $last_login_duration   = $mon.' ago';
                    }else if($d != ""){
                    $last_login_duration   = $d.' ago';
                    }else if($h != ""){
                    $last_login_duration   = $h.' ago';
                    }else if($m != ""){
                    $last_login_duration   = $m.' ago';
                    }else{
                    $last_login_duration   = $s.' ago';
                    }
                    $sidebar_data['last_login_duration']      = $last_login_duration;
                    $sidebar_data['last_login_full_duration'] = trans('expert_listing/listing.text_recently_active').' : ( '.$y.$mon.$d.$h.$m.$s.' ) '.trans('expert_listing/listing.text_ago');
                    }
                    else
                    {
                        $sidebar_data['last_login_duration']      = trans('common/header.text_active'); 
                        $sidebar_data['last_login_full_duration'] = trans('common/header.text_active'); 
                    }
             /* end client login duration*/

             /* client last login*/
             if(Session::has('last_login') && Session::get('last_login') != "0000-00-00 00:00:00"){
                $sidebar_data['last_login']          = date("g:i A",strtotime(Session::get('last_login'))).",".date('j M y',strtotime(Session::get('last_login')));
             }
             else {
                $sidebar_data['last_login']          = date("g:i A",strtotime(date('Y-m-d H:i:s'))).",".date('j M y',strtotime(date('Y-m-d H:i:s')));
             }
             /* end client last login*/
             if ($obj_user!=FALSE && isset($obj_user->is_available)){
                $sidebar_data['is_available'] = $obj_user->is_available;    
             }
             $obj_client_details = ClientsModel::where('user_id',$user_id)->with('country_details')->first(['id','user_id','first_name','last_name','profile_image','timezone','user_type','country']);
             if ($obj_client_details!=FALSE){
                 $sidebar_data['user_details']=$obj_client_details->toArray();
             }
             $sidebar_data['ex_skills']        = '';
             $sidebar_data['ex_category_arr']  = '';
             $sidebar_data['ex_subcategory_arr']  = '';
             $sidebar_data['ex_cat_arr']       = '';
             $sidebar_data['ex_subcat_arr']       = '';
             
             if(isset($sidebar_data['user_details']['timezone']) && $sidebar_data['user_details']['timezone'] != null){
                $date = new DateTime("now", new DateTimeZone($sidebar_data['user_details']['timezone']) );
                $user_country = "";
                $user_flag    = "";
                $user_country_flag = "";
                
                /* check country */
                if(isset($sidebar_data['user_details']['country_details']['country_name']) && $sidebar_data['user_details']['country_details']['country_name'] !=""){
                    $user_country = $sidebar_data['user_details']['country_details']['country_name'].',';
                }
                /* check flag */
                if(isset($sidebar_data['user_details']['country_details']['country_code']) && $sidebar_data['user_details']['country_details']['country_code'] !=""){
                    $flag = substr(strtolower($sidebar_data['user_details']['country_details']['country_code']), 0,2);
                    if(is_file('public/front/images/flags/'.$flag.'.png')){               
                        $user_flag = $flag.'.png';
                    }
                }
                /* set country or flag */
                if(isset($user_flag) && $user_flag != ""){
                   $user_country_flag = '<img src="'.url('/public').'/front/images/flags/'.$user_flag.'">';
                } 

                $sidebar_data['user_country_flag']           = $user_country_flag;
                $sidebar_data['user_country']                = $user_country;
                $sidebar_data['client_timezone']             = $user_country.date("g:i A",strtotime($date->format('Y-m-d H:i:s'))).",".date('j M y',strtotime($date->format('Y-m-d H:i:s')));
                $sidebar_data['client_timezone_without_date']= date("g:i A",strtotime($date->format('Y-m-d H:i:s')));
             }

             
             $sidebar_data['user_type'] = "";
             if(isset($sidebar_data['user_details']['user_type']) && $sidebar_data['user_details']['user_type'] != ''){
                $sidebar_data['user_type'] = $sidebar_data['user_details']['user_type'];
             }
             /*get total spend cost by milestone for client*/
             $total_project_cost = MilestonesModel::where('client_user_id',$user_id)->whereHas('transaction_details',function ($query)
             {
                $query->whereIN('payment_status',array('1','2'));
             })->sum('cost_from_client');

             if ($total_project_cost) 
             {
                  if($total_project_cost >= '1000' && $total_project_cost <= '1999'){
                      $sidebar_data['total_project_cost'] = '1k+'; 
                  } else if($total_project_cost >= '2000' && $total_project_cost <= '4999'){
                      $sidebar_data['total_project_cost'] = '2k+'; 
                  } else if($total_project_cost >= '5000' && $total_project_cost <= '9999'){
                      $sidebar_data['total_project_cost'] = '5k+'; 
                  } else if($total_project_cost >= '10000' && $total_project_cost <= '19999'){
                      $sidebar_data['total_project_cost'] = '10k+'; 
                  } else if($total_project_cost >= '20000' && $total_project_cost <= '49999'){
                      $sidebar_data['total_project_cost'] = '20k+'; 
                  } else if($total_project_cost >= '50000' && $total_project_cost <= '99999'){
                      $sidebar_data['total_project_cost'] = '50k+'; 
                  } else if($total_project_cost >= '100000'){
                      $sidebar_data['total_project_cost'] = '100k+'; 
                  } else {
                      $sidebar_data['total_project_cost']= number_format($total_project_cost);  
                  }
             }
        }
        elseif ($user_role == "project_manager" ) 
        {
            $obj_user = FALSE;
            $obj_manager_details = FALSE;
            $project_count = 0;

            /*get manager all projects count*/ 
             $project_count = ProjectpostModel::where('project_manager_user_id',$user_id)->where('project_status','!=','0')->count();
             $sidebar_data['project_count'] = $project_count;
           
             $open_project_count                      = ProjectpostModel::where('project_manager_user_id',$user_id)->where('project_status','=','2')->count();           
             $sidebar_data['open_project_count']      = isset($open_project_count)?$open_project_count:'0';

             $ongoing_project_count                   = ProjectpostModel::where('project_manager_user_id',$user_id)->where('project_status','=','4')->count();
             $sidebar_data['ongoing_project_count']   = isset($ongoing_project_count)?$ongoing_project_count:'0';

             $completed_project_count                 = ProjectpostModel::where('project_manager_user_id',$user_id)->where('project_status','=','3')->count();
             $sidebar_data['completed_project_count'] = isset($completed_project_count)?$completed_project_count:'0';

             $canceled_project_count                  = ProjectpostModel::where('project_manager_user_id',$user_id)->where('project_status','=','5')->count();
             $sidebar_data['canceled_project_count']  = isset($canceled_project_count)?$canceled_project_count:'0';

             $awarded_project_count                   = ProjectpostModel::where('project_manager_user_id',$user_id)
                                                                          ->Where('project_status','=','2')
                                                                          ->whereHas('project_bid_info',function ($query)
                                                                               {
                                                                                 $query->where('bid_status',4);
                                                                               })
                                                                            ->count();
             $sidebar_data['awarded_project_count']  = isset($awarded_project_count)?$awarded_project_count:'0';

             /*get user availablity */
             $obj_user = UserModel::where('id',$user_id)->first(['is_available','last_login','last_logout']);
             if ($obj_user!=FALSE && isset($obj_user->last_logout)) 
             {
                $sidebar_data['last_login'] = $obj_user->last_logout;    
             }
             else{
                $sidebar_data['last_login'] = '0000-00-00 00:00:00';  
             }
             /* manager login duration*/
                    $sidebar_data['last_login_duration'] = '0 s';
                    
                    $sidebar_data['last_login_full_duration'] = '0 seconds';
                    if($sidebar_data['last_login'] != "0000-00-00 00:00:00"){
                    $datetime1  = new DateTime();
                    $datetime2  = new DateTime($sidebar_data['last_login']);
                    $interval   = $datetime1->diff($datetime2);

                    $year       = $interval->format('%y');
                    $month      = $interval->format('%m');
                    $day        = $interval->format('%d');
                    $hours      = $interval->format('%h');
                    $minutes    = $interval->format('%i');
                    $second     = $interval->format('%s');
                    $y = $mon = $d = $h = $m = $s= "";
                    if($year > 1){
                       if(isset($year)      &&  $year    !=  "0"){  $y    =  $year.' years ';    }
                    } else {
                       if(isset($year)      &&  $year    !=  "0"){  $y    =  $year.' year ';    }
                    }
                    if($month > 1){
                       if(isset($month)     &&  $month   !=  "0"){  $mon  =  $month.' months ';   }
                    } else {
                       if(isset($month)     &&  $month   !=  "0"){  $mon  =  $month.' month ';   }
                    }
                    if($day > 1){
                       if(isset($day)       &&  $day     !=  "0"){  $d    =  $day.' days ';     }
                    } else {
                       if(isset($day)       &&  $day     !=  "0"){  $d    =  $day.' day ';     }
                    }
                    if($hours > 1){
                       if(isset($hours)     &&  $hours   !=  "0"){  $h    =  $hours.' hours ';   }
                    } else {
                       if(isset($hours)     &&  $hours   !=  "0"){  $h    =  $hours.' hour ';   }
                    }
                    if($minutes > 1){
                       if(isset($minutes)   &&  $minutes !=  "0"){  $m    =  $minutes.' minutes ';   }
                    } else {
                       if(isset($minutes)   &&  $minutes !=  "0"){  $m    =  $minutes.' minute ';   }
                    }
                    if($second > 1){
                       if(isset($second)    &&  $second  !=  "0"){  $s    =  $second.' seconds ';  }
                    } else {
                       if(isset($second)    &&  $second  !=  "0"){  $s    =  $second.' second ';  }
                    }  

                    //$last_login_duration   = $y.$mon.$d.$h.$m.$s;
                    
                    /*if($y != "" && $mon !=""){
                    $last_login_duration   = $y.$mon;
                    }else if($mon != "" && $d !=""){
                    $last_login_duration   = $mon.$d;
                    }else if($d != "" && $h !=""){
                    $last_login_duration   = $d.$h;
                    }else if($h != "" && $m !=""){
                    $last_login_duration   = $h.$m;
                    }else if($m != "" && $s !=""){
                    $last_login_duration   = $m.$s;
                    }else{
                    $last_login_duration   = $s;
                    }*/

                    if($y != ""){
                    $last_login_duration   = $y.' ago';
                    }else if($mon != ""){
                    $last_login_duration   = $mon.' ago';
                    }else if($d != ""){
                    $last_login_duration   = $d.' ago';
                    }else if($h != ""){
                    $last_login_duration   = $h.' ago';
                    }else if($m != ""){
                    $last_login_duration   = $m.' ago';
                    }else{
                    $last_login_duration   = $s.' ago';
                    }

                    $sidebar_data['last_login_duration'] = $last_login_duration;
                    $sidebar_data['last_login_full_duration'] = trans('expert_listing/listing.text_recently_active').' : ( '.$y.$mon.$d.$h.$m.$s.' ) '.trans('expert_listing/listing.text_ago');
                    }else
                    {
                        $sidebar_data['last_login_duration']      = trans('common/header.text_active'); 
                        $sidebar_data['last_login_full_duration'] = trans('common/header.text_active'); 
                    }
             /* end manager login duration*/

             /* manager last login*/
             if(Session::has('last_login') && Session::get('last_login') != "0000-00-00 00:00:00"){
                $sidebar_data['last_login']          = date("g:i A",strtotime(Session::get('last_login'))).",".date('j M y',strtotime(Session::get('last_login')));
             }
             else {
                $sidebar_data['last_login']          = date("g:i A",strtotime(date('Y-m-d H:i:s'))).",".date('j M y',strtotime(date('Y-m-d H:i:s')));
             }
             /* end manager last login*/
             
             if ($obj_user!=FALSE && isset($obj_user->is_available)) 
             {
                $sidebar_data['is_available'] = $obj_user->is_available;    
             }

              $obj_manager_details = ProjectManagerModel::where('user_id',$user_id)->first(['id','user_id','first_name','last_name','profile_image']);
             if ($obj_manager_details!=FALSE) 
             {
                 $sidebar_data['user_details']=$obj_manager_details->toArray();
             }
             $sidebar_data['ex_skills']          = '';
             $sidebar_data['ex_category_arr']    = '';
             $sidebar_data['ex_subcategory_arr'] = '';
             $sidebar_data['ex_cat_arr']         = '';
             $sidebar_data['ex_subcat_arr']      = '';
             
        }
        elseif ($user_role == "recruiter" ) 
        {
            $obj_user = FALSE;
            $obj_manager_details = FALSE;
            $project_count = 0;

            /*get manager all projects count*/ 
             $project_count = ProjectpostModel::where('project_recruiter_user_id',$user_id)->where('project_status','!=','0')->count();
             $sidebar_data['project_count'] = $project_count;
           
             $open_project_count                      = ProjectpostModel::where('project_recruiter_user_id',$user_id)->where('project_status','=','2')->count();           
             $sidebar_data['open_project_count']      = isset($open_project_count)?$open_project_count:'0';

             $completed_project_count                 = ProjectpostModel::where('project_recruiter_user_id',$user_id)->where('project_status','=','4')->count();
             $sidebar_data['completed_project_count'] = isset($completed_project_count)?$completed_project_count:'0';

             $awarded_project_count                   = ProjectpostModel::where('project_recruiter_user_id',$user_id)
                                                                          ->Where('project_status','=','2')
                                                                          ->whereHas('project_bid_info',function ($query)
                                                                               {
                                                                                 $query->where('bid_status',4);
                                                                               })
                                                                            ->count();
             $sidebar_data['awarded_project_count']  = isset($awarded_project_count)?$awarded_project_count:'0';

             /*get user availablity */
             $obj_user = UserModel::where('id',$user_id)->first(['is_available','last_login','last_logout']);
             if ($obj_user!=FALSE && isset($obj_user->last_logout)) 
             {
                $sidebar_data['last_login'] = $obj_user->last_logout;    
             }
             else{
                $sidebar_data['last_login'] = '0000-00-00 00:00:00';  
             }
             /* manager login duration*/
                    $sidebar_data['last_login_duration'] = '0 s';
                    
                    $sidebar_data['last_login_full_duration'] = '0 seconds';
                    if($sidebar_data['last_login'] != "0000-00-00 00:00:00"){
                    $datetime1  = new DateTime();
                    $datetime2  = new DateTime($sidebar_data['last_login']);
                    $interval   = $datetime1->diff($datetime2);

                    $year       = $interval->format('%y');
                    $month      = $interval->format('%m');
                    $day        = $interval->format('%d');
                    $hours      = $interval->format('%h');
                    $minutes    = $interval->format('%i');
                    $second     = $interval->format('%s');
                    $y = $mon = $d = $h = $m = $s= "";
                    if($year > 1){
                       if(isset($year)      &&  $year    !=  "0"){  $y    =  $year.' years ';    }
                    } else {
                       if(isset($year)      &&  $year    !=  "0"){  $y    =  $year.' year ';    }
                    }
                    if($month > 1){
                       if(isset($month)     &&  $month   !=  "0"){  $mon  =  $month.' months ';   }
                    } else {
                       if(isset($month)     &&  $month   !=  "0"){  $mon  =  $month.' month ';   }
                    }
                    if($day > 1){
                       if(isset($day)       &&  $day     !=  "0"){  $d    =  $day.' days ';     }
                    } else {
                       if(isset($day)       &&  $day     !=  "0"){  $d    =  $day.' day ';     }
                    }
                    if($hours > 1){
                       if(isset($hours)     &&  $hours   !=  "0"){  $h    =  $hours.' hours ';   }
                    } else {
                       if(isset($hours)     &&  $hours   !=  "0"){  $h    =  $hours.' hour ';   }
                    }
                    if($minutes > 1){
                       if(isset($minutes)   &&  $minutes !=  "0"){  $m    =  $minutes.' minutes ';   }
                    } else {
                       if(isset($minutes)   &&  $minutes !=  "0"){  $m    =  $minutes.' minute ';   }
                    }
                    if($second > 1){
                       if(isset($second)    &&  $second  !=  "0"){  $s    =  $second.' seconds ';  }
                    } else {
                       if(isset($second)    &&  $second  !=  "0"){  $s    =  $second.' second ';  }
                    }  

                    //$last_login_duration   = $y.$mon.$d.$h.$m.$s;
                    
                    /*if($y != "" && $mon !=""){
                    $last_login_duration   = $y.$mon;
                    }else if($mon != "" && $d !=""){
                    $last_login_duration   = $mon.$d;
                    }else if($d != "" && $h !=""){
                    $last_login_duration   = $d.$h;
                    }else if($h != "" && $m !=""){
                    $last_login_duration   = $h.$m;
                    }else if($m != "" && $s !=""){
                    $last_login_duration   = $m.$s;
                    }else{
                    $last_login_duration   = $s;
                    }*/

                    if($y != ""){
                    $last_login_duration   = $y.' ago';
                    }else if($mon != ""){
                    $last_login_duration   = $mon.' ago';
                    }else if($d != ""){
                    $last_login_duration   = $d.' ago';
                    }else if($h != ""){
                    $last_login_duration   = $h.' ago';
                    }else if($m != ""){
                    $last_login_duration   = $m.' ago';
                    }else{
                    $last_login_duration   = $s.' ago';
                    }

                    $sidebar_data['last_login_duration'] = $last_login_duration;
                    $sidebar_data['last_login_full_duration'] = trans('expert_listing/listing.text_recently_active').' : ( '.$y.$mon.$d.$h.$m.$s.' ) '.trans('expert_listing/listing.text_ago');
                    }else
                    {
                        $sidebar_data['last_login_duration']      = trans('common/header.text_active'); 
                        $sidebar_data['last_login_full_duration'] = trans('common/header.text_active'); 
                    }
             /* end manager login duration*/

             /* manager last login*/
             if(Session::has('last_login') && Session::get('last_login') != "0000-00-00 00:00:00"){
                $sidebar_data['last_login']          = date("g:i A",strtotime(Session::get('last_login'))).",".date('j M y',strtotime(Session::get('last_login')));
             }
             else {
                $sidebar_data['last_login']          = date("g:i A",strtotime(date('Y-m-d H:i:s'))).",".date('j M y',strtotime(date('Y-m-d H:i:s')));
             }
             /* end manager last login*/
             
             if ($obj_user!=FALSE && isset($obj_user->is_available)) 
             {
                $sidebar_data['is_available'] = $obj_user->is_available;    
             }

              $obj_manager_details = RecruiterModel::where('user_id',$user_id)->first(['id','user_id','first_name','last_name','profile_image']);
             if ($obj_manager_details!=FALSE) 
             {
                 $sidebar_data['user_details']=$obj_manager_details->toArray();
             }
             $sidebar_data['ex_skills']          = '';
             $sidebar_data['ex_category_arr']    = '';
             $sidebar_data['ex_subcategory_arr'] = '';
             $sidebar_data['ex_cat_arr']         = '';
             $sidebar_data['ex_subcat_arr']      = '';
             
        }

        return $sidebar_data;
    }

    function get_subscription_plan_details($user_id)
    {
        $arr_data = [];
        $obj_subscription = SubscriptionUsersModel::where('user_id',$user_id)
                                                        ->where('is_active','1')
                                                        ->orderBy('id','DESC')
                                                        ->limit(1)
                                                        ->with(['subscription_pack_details.subscription_user_details'])
                                                        ->first();

             if($obj_subscription != FALSE && isset($obj_subscription->pack_name)){
                $arr_data = $obj_subscription->toArray();
             }
        return $arr_data;
    }
    
    function last_30_days_award_count($user_id)
    {
        $date = \Carbon\Carbon::today()->subDays(30);
        $awarded_project_count = ProjectRequestsModel::where('expert_user_id',$user_id)
                                                        ->where('is_accepted','0')
                                                        ->where('created_at', '>=', date($date))
                                                        ->with(['project_details.project_bid_info' => function ($q) use($user_id) {
                                                            $q->where('expert_user_id','=' ,$user_id);
                                                        }])
                                                        ->count();
        return $awarded_project_count;
    }


    
    function last_30_days_award_count_client($user_id)
    {
        $date = \Carbon\Carbon::today()->subDays(30);
        $awarded_project_count = ProjectRequestsModel::where('client_user_id',$user_id)
                                                        ->where('is_accepted','0')
                                                        ->where('created_at', '>=', date($date))
                                                        ->with(['project_details.project_bid_info' => function ($q) use($user_id) {
                                                            $q->where('client_user_id','=' ,$user_id);
                                                        }])
                                                        ->count();
        return $awarded_project_count;
    }

    function get_active_milestone_count($user_id)
    {
       $milestone_count = MilestonesModel::where('expert_user_id',$user_id)
                                           ->where('status','1')
                                           ->where('is_milestone','1')
                                           ->count(); 
       return $milestone_count;
    }

    function get_active_milestone_count_client($user_id)
    {
       $milestone_count = MilestonesModel::where('client_user_id',$user_id)
                                           ->where('status','1')
                                           ->where('is_milestone','1')
                                           ->count(); 
       return $milestone_count;
    }

    function get_message_user_class_by_role($user_id=null)
    {
        if ($user_id!=null) 
        {
            $user = Sentinel::findById($user_id);

            if ($user) 
            {
                $user_role = $user->roles()->first();

                if ($user_role) 
                {
                    if (isset($user_role->slug) && $user_role->slug!='') 
                    {

                        if ($user_role->slug=='client') 
                        {
                            return 'msg-role-client';    
                        }
                        elseif ($user_role->slug=='expert') 
                        {
                            return 'msg-role-expert';    
                        }
                        elseif ($user_role->slug=='project_manager') 
                        {
                            return 'msg-role-project-manager';    
                        }
                    }
                }
            }
        }
        return 'default';
    }

    function get_experts()
    {
        $arr_experts = []; 
        $obj_experts = ExpertsModel::with(['user_details','country_details','city_details','expert_skills','profession_details','expert_categories'])->whereHas('user_details',function ($query){
        $query->where('is_active',1);
        $query->where('is_available',1);
        })->get();
        if($obj_experts != FALSE){
        $arr_experts        = $obj_experts->toArray();
        }  
        return $arr_experts;  
    }

    function get_expert($expert_id)
    {
        $arr_experts = []; 
        $obj_experts = ExpertsModel::where('user_id',$expert_id)->with(['user_details','country_details','city_details','expert_skills','profession_details','expert_categories'])->whereHas('user_details',function ($query){
        $query->where('is_active',1);
        $query->where('is_available',1);
        })->first();
        if($obj_experts != FALSE){
        $arr_experts        = $obj_experts->toArray();
        }  
        return $arr_experts;  
    }

    function get_projects($client_user_id=FALSE)
    {
        $arr_project = []; 
        $obj_post_project = ProjectpostModel::where('client_user_id',$client_user_id)
        ->where(function ($q) 
        {
          $q->where('project_status','=','1');
          $q->where('projects.is_hire_process','NO');
          $q->where('projects.bid_closing_date','>',date('Y-m-d'));
          $q->orWhere('project_status','=','2');
        }) 
        ->with('skill_details','project_skills.skill_data')
        ->get();
        if($obj_post_project != FALSE){
        $arr_project        = $obj_post_project->toArray();
        }  
        return $arr_project;  
    }

    function get_logged_user_wallet_details($curr)
    {
        $WalletService = new WalletService;
        $user = Sentinel::check();
        $wallet_details = $arr_data = [];
        if(isset($user) && $user != null){
            $logged_user                 = $user->toArray();
            $obj_data = UserWalletModel::where('user_id',$logged_user['id'])->where('currency_code',$curr)->first();
      
            if($obj_data)
            {
               $arr_data = $obj_data->toArray();
            } 
            //dd($arr_data);
            if($logged_user['mp_wallet_created'] == 'Yes')
            {
                $mp_wallet_id = isset($arr_data['mp_wallet_id'])?$arr_data['mp_wallet_id']:'';
                $get_mangopay_wallet_details[] = $WalletService->get_wallet_details($mp_wallet_id);
                  if(isset($get_mangopay_wallet_details)){
                    $wallet_details = $get_mangopay_wallet_details;
                  }
            }
        }
        return  $wallet_details;
    }

    function get_user_all_wallet_details()
    {
        $WalletService = new WalletService;
        $user = Sentinel::check();
        $wallet_details = [];
        if(isset($user) && $user != null){
            $logged_user                 = $user->toArray();
            $obj_data = UserWalletModel::where('user_id',$logged_user['id'])->get();
      
            if($obj_data)
            {
               $arr_data = $obj_data->toArray();
            } 
            if($logged_user['mp_wallet_created'] == 'Yes')
            {
              foreach ($arr_data as $key => $value) {
                  $get_mangopay_wallet_details[] = $WalletService->get_wallet_details($value['mp_wallet_id']);
              }
              if(isset($get_mangopay_wallet_details)){
                $wallet_details = $get_mangopay_wallet_details;
              }
            }
        }
        return  $wallet_details;
    }

    /*-----3-Complet,5.Canceled---*/
    function check_project_status()
    {
        $user = Sentinel::check();
        $user_id = isset($user->id)?$user->id:0;

        $obj_project = $this->ProjectpostModel->where('project_status','<>','3')
                                              ->where('project_status','<>','5')
                                              ->count();

        dd($obj_project);
    }


?>