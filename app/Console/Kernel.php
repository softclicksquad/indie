<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
            // Commands\Inspire::class,
            Commands\ExpireSubscription::class,
            Commands\ExpirySubscriptionNotification::class,
            Commands\ExpireHighlightedProject::class,
            Commands\ExpireContest::class,
            Commands\WinnerChoose::class,
            Commands\RefundContestAmount::class,
            Commands\StoreCurrencyConversion::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->command('expire:subscription')->everyMinute();
        $schedule->command('expirysubscription:notification')->everyMinute();
        $schedule->command('expire:highlightproject')->everyMinute();
        $schedule->command('expire:contest')->everyMinute();
        $schedule->command('winner:choose')->everyMinute();
        $schedule->command('refund:contest_amount')->everyMinute();
        $schedule->command('store_currency_conversion:schedule')->twiceDaily();

    }
}
