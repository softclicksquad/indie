<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\UserModel;
use App\Models\ContestModel;
use App\Models\ContestEntryModel; 
use App\Models\NotificationsModel; 
use App\Models\ContestPricingModel;
use App\Models\UserWalletModel;

use App\Common\Services\MailService;
use App\Common\Services\WalletService;

use Mail;
use Session;

class WinnerChoose extends Command {
    /*
     *
     * The name and signature of the console command.
     *
     * @var string
    */
    protected $signature = 'winner:choose';
    /**
     * The console command description.
     *
     * @var string
    */
    protected $description = 'Winner Choose';
    /**
     * Create a new command instance.
     *
     * @return void
    */
    public function __construct(){
        parent::__construct();
        $this->log_cron_url            = "https://betteruptime.com/api/v1/heartbeat/x7BGFrwHh4VSRViqEJ9MuXbL";
        $this->UserModel            = new UserModel();
        $this->ContestModel         = new ContestModel();
        $this->ContestEntryModel    = new ContestEntryModel();
        $this->NotificationsModel   = new NotificationsModel();
        $this->ContestPricingModel  = new ContestPricingModel();
        $this->MailService          = new MailService;
        $this->WalletService        = new WalletService;
        $this->UserWalletModel      = new UserWalletModel;

    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arr_contest= [];
        $date = date('Y-m-d H:i:s');

        $obj_all_contest = $this->ContestModel/*->select('id','contest_end_date','contest_title')*/
                                              ->where('contest_end_date', '<', $date)
                                              ->with('user_details')
                                              ->where('is_active','!=', '2')
                                              ->where('is_expire_notification_sent','0')
                                              ->orderBy('id','Desc')
                                              ->get();

        if($obj_all_contest)
        {
            $arr_contest = $obj_all_contest->toArray();
        }

        if(isset($arr_contest) && sizeof($arr_contest)>0)
        {
            foreach ($arr_contest as $key => $value) 
            {
                $invoice_id = $this->_generate_invoice_id();

                $email_data['user_id'] = isset($value['client_user_id'])?$value['client_user_id']:'';
                $email_data['name']    = isset($value['user_details']['role_info']['first_name'])?$value['user_details']['role_info']['first_name']:'';
                $email_data['name']    .= isset($value['user_details']['role_info']['last_name'])?$value['user_details']['role_info']['last_name']:'';
                
                $start_date = isset($value['created_at']) ? date('Y-m-d H:i:s',strtotime($value['created_at'])) : '';
                $created_at_time = isset($value['created_at']) ? date('H:i:s',strtotime($value['created_at'])) : '';
                $end_date = isset($value['contest_end_date']) ? date('Y-m-d',strtotime($value['contest_end_date'])) : '';
                
                $end_date = $end_date. ' ' .$created_at_time;

                $email_data['start_date'] = $start_date;
                $email_data['end_date']   = $end_date;

                $email_data['email_id'] = $email_id = get_user_email_id($value['client_user_id']);

                $email_data['contest_title'] = isset($value['contest_title'])?$value['contest_title']:'';

                $mail_status = $this->MailService->send_expire_contest_email_for_client($email_data);

                $arr_users = [];
                $contest_id = isset($value['id'])?$value['id']:'0';

                $obj_users = $this->ContestEntryModel->select('id','contest_id','expert_id')
                                                        ->with('expert_details','contest_details')
                                                        ->where('contest_id',$contest_id)
                                                        ->get();
                if($obj_users)
                {
                    $arr_users = $obj_users->toArray();
                }

                shuffle($arr_users);

                if(isset($arr_users) && sizeof($arr_users)>0)
                {
                   foreach ($arr_users as $winner_key => $data) 
                   {
                     //dd($data);
                      if($winner_key==0)
                      {
                        if($this->ContestModel->where('id',$contest_id)->where('is_expire_notification_sent','0')->count() > 0)
                        {
                            $mp_details = get_user_wallet_details($value['client_user_id'],$data['contest_details']['contest_currency']);

                            $winner_wallet = get_user_wallet_details($data['expert_id'],$data['contest_details']['contest_currency']);
                            

                            if($mp_details)
                            {
                                //$mp_details = $client_mp_details->toArray();

                                $invoice_id         = $this->_generate_invoice_id();
                                $admin_invoice_id   = $this->_generate_invoice_id();

                                if(isset($mp_details['mp_user_id']) && $mp_details['mp_user_id'] !="")
                                {
                                    $project_name  = $contest_id.'-'.$value['contest_title'];

                                    $mangopay_wallet_details = [];
                                    $get_wallet_details      = $this->WalletService->get_wallet_details($value['mp_contest_wallet_id']);
                                    
                                    if(isset($get_wallet_details) && $get_wallet_details!='false' || $get_wallet_details != false){
                                        $mangopay_wallet_details = $get_wallet_details;
                                    }

                                    $wallet_amount = 0;

                                    if(isset($mangopay_wallet_details->Balance->Amount) && $mangopay_wallet_details->Balance->Amount != "")
                                    {
                                        $wallet_amount = $mangopay_wallet_details->Balance->Amount/100;
                                    }
                                    //dd($wallet_amount);

                                    $transaction_admin_inp['tag']                      = ' Contest Winner transfer';
                                    $transaction_admin_inp['debited_UserId']           = $mp_details['mp_user_id'];           
                                    $transaction_admin_inp['credited_UserId']          = $winner_wallet['mp_user_id'];
                                    $transaction_admin_inp['total_pay']                = $wallet_amount;              
                                    $transaction_admin_inp['debited_walletId']         = (string)$value['mp_contest_wallet_id']; 
                                    $transaction_admin_inp['credited_walletId']        = (string)$winner_wallet['mp_wallet_id']; 
                                    $transaction_admin_inp['currency_code']            = $data['contest_details']['contest_currency']; 
                                    $transaction_admin_inp['cost_website_commission']  = 0; 

                                    $pay_winner      = $this->WalletService->walletTransfer($transaction_admin_inp);

                                    if(isset($pay_winner->Status) && $pay_winner->Status == 'SUCCEEDED')
                                    {
                                        $update = $this->ContestEntryModel->where('id',$data['id'])->update(['is_winner'=>'YES']);
                                        if($update){
                                        $this->ContestModel->where('id',$value['id'])->update(['winner_choose'=>'YES','contest_status'=>'2']);
                                        }
                                    }
                                }
                            }
                        }
                      }
                   }
                    
                }

                $this->ContestModel
                            ->where('id',$contest_id)
                            ->update(['is_expire_notification_sent'=>'1']);

            }
        }

        $this->log_betteruptime_site();

        dump("Yes its done");
    }

    private function log_betteruptime_site(){

        $ch = curl_init();
        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $this->log_cron_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // grab URL and pass it to the browser
        $response = curl_exec($ch);
        // close cURL resource, and free up system resources
        curl_close($ch);

    }

    private function _generate_invoice_id()
    {
        $secure      = TRUE;    
        $bytes       = openssl_random_pseudo_bytes(3, $secure);
        $order_token = 'INV'.date('Ymd').strtoupper(bin2hex($bytes));
        return $order_token;
    }
    
}
