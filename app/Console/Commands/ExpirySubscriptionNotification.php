<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\SubscriptionPacksModel;
use App\Models\SubscriptionUsersModel;
use App\Common\Services\MailService;
use App\Common\Services\WalletService;
use Mail;
use URL;
use DB;

class ExpirySubscriptionNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expirysubscription:notification';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscription expiry notification';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(MailService $MailService,
                                WalletService $WalletService){
        parent::__construct();
        $this->log_cron_url     = "https://betteruptime.com/api/v1/heartbeat/yCVkpUAW8VpaKJbQWdXUtGmA";
        $this->SubscriptionPacksModel = new SubscriptionPacksModel();  
        $this->SubscriptionUsersModel = new SubscriptionUsersModel(); 
        $this->MailService            = $MailService;
        $this->WalletService          = $WalletService;
    }
    /**
     * Execute the console command.
     *
     * @return mixed
    */
    public function handle()
    {   
        $before_one_day   = date('Y-m-d',strtotime("+ 1 day"));
        $before_three_day = date('Y-m-d',strtotime("+ 3 day"));
        $arr_subscription = $data = array();
        $arr_exp_dates    = array();
        $arr_exp_dates[0] = $before_one_day;
        $arr_exp_dates[1] = $before_three_day;
        $current_date     = date('Y-m-d');
        //dd($arr_exp_dates);
        //$obj_subscription = $this->SubscriptionUsersModel->whereDate('expiry_at','>=',$current_date)->with(['user_details'])->get(['id','user_id','expiry_at','pack_name']);
        $obj_subscription = $this->SubscriptionUsersModel->where('is_active','1')->whereIn('expiry_at',$arr_exp_dates)->with(['user_details'])->orderBy('id','Desc')->get(['id','user_id','expiry_at','pack_name','subscription_pack_currency','subscription_pack_id']);
        if($obj_subscription && $obj_subscription!=FALSE){
          $arr_subscription = $obj_subscription->toArray();
        }

        $project_name = config('app.project.name');
        //$mail_form    = get_site_email_address(); 
        $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
        
        if(isset($arr_subscription) && sizeof($arr_subscription)>0){
            foreach ($arr_subscription as $key => $subscription) {
                $arr_user =get_user_wallet_details($subscription['user_id'],$subscription['subscription_pack_currency']);
                //dd($arr_user);
                if(!empty($arr_user))
                {
                    $mp_wallet_id = $arr_user['mp_wallet_id'];
                    $mp_user_id   = $arr_user['mp_user_id'];

                    $get_mangopay_wallet_details = $this->WalletService->get_wallet_details($arr_user['mp_wallet_id']);
                    $balance_amount = $get_mangopay_wallet_details->Balance->Amount/100;


                    $obj_pack_details = $this->SubscriptionPacksModel->where('id',$subscription['subscription_pack_id'])
                                                                     ->first();
                    if($obj_pack_details)
                    {
                        $pack_price = $obj_pack_details->pack_price; 
                    }
                    //dd($balance_amount,$pack_price);
                    //dd($subscription['user_details']['email']);
                    if($balance_amount < $pack_price)
                    {
                        $expire_date = date('d M Y', strtotime($subscription['expiry_at']));
                        $expert_first_name = isset($subscription['user_details']['role_info']['first_name'])?$subscription['user_details']['role_info']['first_name']:'';
                        $expert_name         = $expert_first_name;
                        $data['expert_name'] = $expert_name;
                        $data['pack_name']   = $subscription['pack_name'];
                        $data['expire_date'] = $expire_date;
                        $data['email_id']    = isset($subscription['user_details']['email'])? 
                                                     $subscription['user_details']['email']:'';
                        $email_to = isset($subscription['user_details']['email'])? $subscription['user_details']['email']:'';
                        if($email_to!= ""){
                            try{
                                $mail_status = $this->MailService->send_expiry_subscription_warning_balance_email($data);
                            }
                            catch(\Exception $e){
                            dump('error E-Mail not sent');
                            }
                        }

                    }

                }
                $expire_date = date('d M Y', strtotime($subscription['expiry_at']));
                $expert_first_name = isset($subscription['user_details']['role_info']['first_name'])?$subscription['user_details']['role_info']['first_name']:'';
                $expert_name         = $expert_first_name;
                $data['expert_name'] = $expert_name;
                $data['pack_name']   = $subscription['pack_name'];
                $data['expire_date'] = $expire_date;
                $data['email_id']    = isset($subscription['user_details']['email'])? $subscription['user_details']['email']:'';
                $email_to = isset($subscription['user_details']['email'])? $subscription['user_details']['email']:'';
                if($email_to!= ""){
                    try{
                        $mail_status = $this->MailService->send_expiry_subscription_notification_email($data);
                        // Mail::send('front.email.expiry_subscription_notification', $data, function ($message) use ($email_to,$mail_form,$project_name) {
                        //     $message->from($mail_form, $project_name);
                        //     $message->subject($project_name.':Subscription Expiry.');
                        //     $message->to($email_to);
                        // });
                    }
                    catch(\Exception $e){
                    dump('error E-Mail not sent');
                    }
                }
            }
        }

        $this->log_betteruptime_site();
    }

    private function log_betteruptime_site(){

        $ch = curl_init();
        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $this->log_cron_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // grab URL and pass it to the browser
        $response = curl_exec($ch);
        // close cURL resource, and free up system resources
        curl_close($ch);

   }

} // end class
