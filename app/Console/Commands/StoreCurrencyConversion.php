<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\CurrencyModel;
use App\Models\CurrencyConversionModel;
use App\Models\ContestModel;
use App\Models\ProjectpostModel;

class StoreCurrencyConversion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
     protected $signature = 'store_currency_conversion:schedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $base_currency              = 'EUR';
    protected $base_amount                = '1.00';
    protected $arr_currency               = [];
    protected $arr_remote_currency_rates  = [];
    protected $arr_all_currency_rates     = [];
    protected $arr_db_currency_conversion = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->log_cron_url            = "https://betteruptime.com/api/v1/heartbeat/RPDi6VpBq4mUY6TvRDGBgsru";
        $this->api_key                 = '4141b3a2ef4b03a55dda18e662ef601e';
        $this->live_mode               = 'yes';
        $this->is_vatsense_api         = 'no';
        $this->CurrencyModel 		   = new CurrencyModel();
        $this->CurrencyConversionModel = new CurrencyConversionModel();
        $this->ContestModel            = new ContestModel();
        $this->ProjectpostModel        = new ProjectpostModel();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->arr_currency               = $this->get_currency_details();
    	$this->arr_remote_currency_rates  = $this->get_remote_currency_rates();
        $this->arr_all_currency_rates     = $this->build_all_currency_rates();
        $this->arr_db_currency_conversion = $this->pull_db_currency_conversion_details();
        
		// update the currency conversion in database 
        if(isset($this->arr_db_currency_conversion) && count($this->arr_db_currency_conversion)>0){
        	foreach ($this->arr_db_currency_conversion as $currency_key => $arr_data) {
        		if(isset($arr_data) && is_array($arr_data) && count($arr_data)>0){
        			foreach ($arr_data as $key => $value) {
        				if(isset($value['id']) && isset($this->arr_all_currency_rates[$value['from_currency_code']][$value['to_currency_code']])){
        					$this->CurrencyConversionModel
                                    	->where('id',$value['id'])
                                    	->update(['conversion_rate' => $this->arr_all_currency_rates[$value['from_currency_code']][$value['to_currency_code']]]);
        				}
        			}
        		}
        	}
        }

        $this->update_contest_pricing_details();
        $this->update_project_pricing_details();

        $this->log_betteruptime_site();
        
        dump('currency conversion updated successfully.');
    }

    private function update_contest_pricing_details(){

        $arr_db_currency_conversion = $this->pull_db_currency_conversion_details();
        
        $obj_open_contests = $this->ContestModel
                                              ->where('is_active','!=','2')
                                              ->whereDate('contest_end_date','>',date('Y-m-d H:i:s'))
                                              ->where('payment_status','1')
                                              ->orderBy('created_at','DESC')
                                              ->get();

        if(isset($obj_open_contests) && count($obj_open_contests)>0){
            foreach ($obj_open_contests as $key => $obj_contest) {
                $contest_price    = isset($obj_contest->contest_price) ? $obj_contest->contest_price : 0;
                $contest_currency = isset($obj_contest->contest_currency) ? $obj_contest->contest_currency : '';

                $arr_currency_conversion = isset($arr_db_currency_conversion[$contest_currency]) ? $arr_db_currency_conversion[$contest_currency] : [];
                if(count($arr_currency_conversion)>0){
                    $arr_currency_conversion = $this->rekey_array('to_currency_code',$arr_currency_conversion);
                    $arr_currency_conversion = isset($arr_currency_conversion['USD']) ? $arr_currency_conversion['USD'] : [];
                }

                $usd_contest_price = $contest_price;
                if($contest_currency != 'USD')
                {
                    $conversion_rate = isset($arr_currency_conversion['conversion_rate']) ? $arr_currency_conversion['conversion_rate'] : 0;
                    $usd_contest_price = floor(floatval($conversion_rate) * floatval($usd_contest_price));
                }
                $obj_contest->usd_contest_price = $usd_contest_price;
                $obj_contest->save();
            }
        }
        return true;
    }

    private function update_project_pricing_details(){

        $arr_db_currency_conversion = $this->pull_db_currency_conversion_details();
        
        $obj_open_projects = $this->ProjectpostModel->where('projects.project_status','=','2')
                                                ->whereDate('projects.bid_closing_date','>',date('Y-m-d h:i:s'))
                                                ->where('projects.is_hire_process','NO')
                                                ->get();

        if(isset($obj_open_projects) && count($obj_open_projects)>0){
            foreach ($obj_open_projects as $key => $obj_project) {
                
                $min_project_cost    = isset($obj_project->min_project_cost) ? $obj_project->min_project_cost : 0;
                $max_project_cost    = isset($obj_project->max_project_cost) ? $obj_project->max_project_cost : 0;
                $project_currency_code = isset($obj_project->project_currency_code) ? $obj_project->project_currency_code : '';

                $arr_currency_conversion = isset($arr_db_currency_conversion[$project_currency_code]) ? $arr_db_currency_conversion[$project_currency_code] : [];
                if(count($arr_currency_conversion)>0){
                    $arr_currency_conversion = $this->rekey_array('to_currency_code',$arr_currency_conversion);
                    $arr_currency_conversion = isset($arr_currency_conversion['USD']) ? $arr_currency_conversion['USD'] : [];
                }

                $usd_min_project_cost = $min_project_cost;
                $usd_max_project_cost = $max_project_cost;
                if($project_currency_code != 'USD')
                {
                    $conversion_rate      = isset($arr_currency_conversion['conversion_rate']) ? $arr_currency_conversion['conversion_rate'] : 0;
                    $usd_min_project_cost = floor(floatval($conversion_rate) * floatval($usd_min_project_cost));
                    $usd_max_project_cost = floor(floatval($conversion_rate) * floatval($usd_max_project_cost));
                }
                
                $obj_project->usd_min_project_cost = $usd_min_project_cost;
                $obj_project->usd_max_project_cost = $usd_max_project_cost;
                $obj_project->save();
            }
        }
        return true;
    }

    private function log_betteruptime_site(){

        $ch = curl_init();
        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $this->log_cron_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // grab URL and pass it to the browser
        $response = curl_exec($ch);
        // close cURL resource, and free up system resources
        curl_close($ch);

    }

    private function get_currency_details(){

    	$arr_currency = [];
    	$obj_currency = $this->CurrencyModel
    								->select('id','currency_code')
    								->where('currency_code','!=',$this->base_currency)
    								->where('is_active','1')
    								->get();
    	if($obj_currency){
			$arr_currency = $obj_currency->toArray();
    	}
    	if(isset($arr_currency) && count($arr_currency)>0) {
    		return array_keys($this->rekey_array('currency_code',$arr_currency));
    	}
    	return $arr_currency;
    }

    private function get_remote_currency_rates(){

    	$arr_remote_currency_rates = [];
        if($this->is_vatsense_api == 'no') {
            return $this->get_europa_bank_currency_conversion_rate();
        }

    	if(isset($this->arr_currency) && count($this->arr_currency)>0){
			foreach ($this->arr_currency as $key => $value) {
				if($this->live_mode == 'yes') {
		    		$arr_response = $this->get_vatsense_currency_conversion_rate($value);
		    		if(isset($arr_response['rate'])){
		    			$arr_remote_currency_rates[$value] = $arr_response['rate'];
		    		}
		    	} elseif($this->live_mode == 'no') {
		    		$arr_remote_currency_rates[$value] = 1.4065;
		    	}
			}
		}
    	return $arr_remote_currency_rates;
    }

    private function build_all_currency_rates() {
    	$arr_all_currency_rates = [];
    	if(isset($this->arr_remote_currency_rates) && count($this->arr_remote_currency_rates)>0){
    		// add base_currency_rate_into global array
    		$arr_all_currency_rates[$this->base_currency] = $this->arr_remote_currency_rates;
    		foreach ($this->arr_remote_currency_rates as $currency_key => $currency_value) {
    			$arr_tmp_remote_currency_rates = $this->arr_remote_currency_rates;
                //dd($arr_tmp_remote_currency_rates);

                // calculate the base currency rate for the base currency == EUR 
                $base_currency_rate = 0;
                if(isset($arr_tmp_remote_currency_rates[$currency_key]) && $arr_tmp_remote_currency_rates[$currency_key]>0){
                    
                    foreach ($arr_tmp_remote_currency_rates as $tmp_currency_key => $tmp_currency_value) {
                        if($tmp_currency_key!=$currency_key){
                            $current_currency_value = 0;
                            if($tmp_currency_value>0){
                                $current_currency_value = round($tmp_currency_value/$arr_tmp_remote_currency_rates[$currency_key],5);
                            }
                            $arr_tmp_remote_currency_rates[$tmp_currency_key] = $current_currency_value;
                        }
                    }    
                    $base_currency_rate = round(1/$arr_tmp_remote_currency_rates[$currency_key],5);
                }
    			$arr_tmp_remote_currency_rates[$this->base_currency] = $base_currency_rate;
    			unset($arr_tmp_remote_currency_rates[$currency_key]);
    			$arr_all_currency_rates[$currency_key]  = $arr_tmp_remote_currency_rates; 
    		}
    	}
    	return $arr_all_currency_rates;
    }

    private function pull_db_currency_conversion_details() {

    	$arr_currency_conversion = [];
    	$obj_currency_conversion = $this->CurrencyConversionModel
    											->select('id', 'from_currency_id', 'to_currency_id', 'conversion_rate')
    											->with(['from_currency_detail', 'to_currency_detail'])
    											->get();
    	if($obj_currency_conversion){
    		$arr_currency_conversion = $obj_currency_conversion->toArray();
    	}

    	if(isset($arr_currency_conversion) && count($arr_currency_conversion)>0){
    		foreach ($arr_currency_conversion as $key => $value) 
    		{
    			$arr_currency_conversion[$key]['from_currency_code'] = isset($value['from_currency_detail']['currency_code']) ? $value['from_currency_detail']['currency_code'] : '';
    			$arr_currency_conversion[$key]['to_currency_code']   = isset($value['to_currency_detail']['currency_code']) ? $value['to_currency_detail']['currency_code'] : '';
    			unset($arr_currency_conversion[$key]['from_currency_detail']);
    			unset($arr_currency_conversion[$key]['to_currency_detail']);
    		}
    	}
    	return $this->rekey_array('from_currency_code',$arr_currency_conversion,true);
    }

    private function get_europa_bank_currency_conversion_rate(){
        
        $arr_response = [];
        $XML=simplexml_load_file("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");
        foreach($XML->Cube->Cube->Cube as $rate){
            if(in_array((string)$rate["currency"], $this->arr_currency) == true){
                $arr_response[(string)$rate["currency"]] = floatval($rate["rate"]);        
            }
        }
        return $arr_response;
    }

    private function get_vatsense_currency_conversion_rate($currency){
    	
    	$arr_response = [];
		$endpoint = 'https://api.vatsense.com/1.0/currency?from='.$this->base_currency.'&to='.$currency.'&amount='.$this->base_amount;
		
		// init curl
		$ch = curl_init();

		// set curl options
		curl_setopt($ch, CURLOPT_URL, $endpoint);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_USERPWD, 'user:'.$this->api_key);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);

		$response = curl_exec($ch);
		curl_close($ch);

		$response = '{"success": true,"code": 200,"data": {"object": "conversion","from": "USD","to": "GBP","amount": 39.99,"converted": 28.43,"rate": 1.4065}}';

		$arr_response = json_decode($response,true);
		if(isset($arr_response['data']) && count($arr_response['data'])>0){
			return $arr_response['data'];
		}
		return [];
    }

    private function rekey_array($str_key,$arr_data,$is_multiple_array_with_same_key = false) {
		$str_key = trim($str_key);
		if(!isset($arr_data) || !is_array($arr_data) || count($arr_data)<=0){
			return $arr_data;
		}
		$arr_rekey_data = [];
		foreach ($arr_data as $key => $value) {
			$str_key_value = isset($value[$str_key]) ? trim($value[$str_key]) : '';
			if($str_key_value!='') {
				if($is_multiple_array_with_same_key){
					$arr_rekey_data[$str_key_value][] = $value;		
				} else {
					$arr_rekey_data[$str_key_value] = $value;
				}
			}
		}
		return $arr_rekey_data;
	}

   	/*$arr_insert = [];
	foreach ($this->arr_currency as $key => $value) {
		foreach ($this->arr_currency as $inner_key => $inner_value) {
			if($key!=$inner_key) {
				$arr_tmp = 
							[
								'from_currency_id' => $value['id'],
								'to_currency_id'   => $inner_value['id'],
								'conversion_rate'  => 1,
							];
				array_push($arr_insert, $arr_tmp);
			}
		}
	}
	$this->CurrencyConversionModel->insert($arr_insert);*/


}
