<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\ContestModel;
use App\Models\NotificationsModel;
use App\Common\Services\WalletService;

class RefundContestAmount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refund:contest_amount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This job will refund the contest amount if there are no entries avaliable.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->ContestModel         = new ContestModel();
        $this->WalletService        = new WalletService;
        $this->NotificationsModel   = new NotificationsModel;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arr_contest= [];
        $date = date('Y-m-d H:i:s');

        $obj_all_contest = $this->ContestModel
                                      ->with('contest_entry')
                                      ->whereDoesntHave('contest_entry',function($query){
                                      })
                                      ->where('contest_end_date', '<', $date)
                                      ->where('is_active','!=','2')
                                      ->where('payment_status','1')
                                      ->where('is_refunded','0')
                                      ->where('failed_refund_retry_count','<=',5)
                                      ->orderBy('id','DESC')
                                      ->get();

        if($obj_all_contest)
        {
            $arr_contest = $obj_all_contest->toArray();
        }
        if(isset($arr_contest) && count($arr_contest)>0) {
            foreach ($arr_contest as $key => $contest) {

                $contest_id                  = isset($contest['id'])?$contest['id']:'';
                $contest_title               = isset($contest['contest_title']) ? $contest['contest_title'] : '';
                $client_user_id              = isset($contest['client_user_id'])?$contest['client_user_id']:'';
                $currency_code               = isset($contest['contest_currency'])?$contest['contest_currency']:'';
                $contest_price               = isset($contest['contest_price'])?$contest['contest_price']:'';
                $mp_contest_wallet_id        = isset($contest['mp_contest_wallet_id'])?$contest['mp_contest_wallet_id']:'';
                $failed_refund_retry_count   = isset($contest['failed_refund_retry_count'])?$contest['failed_refund_retry_count']:0;

                $arr_client_wallet  = get_user_wallet_details($client_user_id,$currency_code);
                $obj_contest_wallet = $this->WalletService->get_wallet_details($mp_contest_wallet_id);
                
                if($obj_contest_wallet == false) {
                    continue;
                }

                $amount = $obj_contest_wallet->Balance->Amount/100;
                
                $transaction_admin_inp = [];
                $transaction_admin_inp['tag']                      = ' Contest price return to client';
                $transaction_admin_inp['debited_UserId']           = $arr_client_wallet['mp_user_id'];
                $transaction_admin_inp['credited_UserId']          = $arr_client_wallet['mp_user_id'];
                $transaction_admin_inp['total_pay']                = $amount;              
                $transaction_admin_inp['debited_walletId']         = (string)$mp_contest_wallet_id ;
                $transaction_admin_inp['credited_walletId']        = (string)$arr_client_wallet['mp_wallet_id']; 
                $transaction_admin_inp['currency_code']            = $currency_code; 
                $transaction_admin_inp['cost_website_commission']  = 0; 
                $transaction_admin_inp['is_contest_refund_cron']   = '1'; 
                
                $obj_payment_status = $this->WalletService->walletTransfer($transaction_admin_inp);

                if(isset($obj_payment_status->Status) && $obj_payment_status->Status == 'SUCCEEDED') {
                    
                    /* send notification to client */
                    $arr_noti_data                         =  [];
                    $arr_noti_data['user_id']              =  $client_user_id;
                    $arr_noti_data['user_type']            = '2';
                    $arr_noti_data['url']                  = 'client/contest/expired';
                    $arr_noti_data['project_id']           = '';
                    $arr_noti_data['notification_text_en'] = $contest_title . ' - Contest Amount refunded to your wallet.';
                    $arr_noti_data['notification_text_de'] = $contest_title . ' - Contest Amount refunded to your wallet.';
                    $this->NotificationsModel->create($arr_noti_data);
                    /* send notification to client */

                    $this->ContestModel
                                ->where('id','=',$contest_id)
                                ->update([
                                            'is_active'=>'2',
                                            'is_refunded'=>'1',
                                            'refund_payment_response'=> isset($obj_payment_status) ? json_encode($obj_payment_status) : NULL,
                                          ]);    
                } else {
                    
                    $failed_refund_retry_count = $failed_refund_retry_count + 1;  

                    $arr_payment_gateway_response_details = [];
                    if(isset($contest['refund_payment_response'])){
                        $arr_tmp_response = json_decode($contest['refund_payment_response'],true);
                        if(isset($arr_tmp_response) && is_array($arr_tmp_response)){
                            array_push($arr_payment_gateway_response_details,$arr_tmp_response);
                        }
                    }     
                    array_push($arr_payment_gateway_response_details,$obj_payment_status);
                    $this->ContestModel
                                ->where('id','=',$contest_id)
                                ->update([
                                            'refund_payment_response'=> isset($arr_payment_gateway_response_details) ? json_encode($arr_payment_gateway_response_details) : NULL,
                                            'failed_refund_retry_count'=> $failed_refund_retry_count,
                                          ]);
                }
            }
            dump('Cron job Executed successfully.');
        } else {
            dump('No expired contest avaliable.');
        }
    }
}
