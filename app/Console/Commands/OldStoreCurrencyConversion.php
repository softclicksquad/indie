<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\CurrencyConversionModel;

class OldStoreCurrencyConversion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
     protected $signature = 'store_currency_conversion:schedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->CurrencyConversionModel       = new CurrencyConversionModel();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currency_conversion_obj = $this->CurrencyConversionModel->select('id', 'from_currency_id', 'to_currency_id', 'conversion_rate')->with(['from_currency_detail', 'to_currency_detail'])->get();

        if (isset($currency_conversion_obj) && count($currency_conversion_obj)) {
            
            $currency_conversion_arr = $currency_conversion_obj->toArray();

            foreach ($currency_conversion_arr AS $currency_conversion_data) 
            {
                $from_currency_code = $currency_conversion_data['from_currency_detail']['currency_code'];
                $to_currency_code   = $currency_conversion_data['to_currency_detail']['currency_code'];
                if($from_currency_code!="" && $to_currency_code!= "")
                {
                    $conversion_rate = currency_conversion_api($from_currency_code, $to_currency_code);
                    if($conversion_rate!=false)
                    {
                        $this->CurrencyConversionModel->where('from_currency_id', $currency_conversion_data['from_currency_id'])
                                    ->where('to_currency_id',  $currency_conversion_data['to_currency_id'])
                                    ->update(['conversion_rate' => $conversion_rate]);
                    }
                    
                }
            }
            dump('Success');
        }
    }
}
