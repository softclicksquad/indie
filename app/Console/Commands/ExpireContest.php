<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ContestEntryModel; 
use App\Models\ContestModel; 
use App\Models\NotificationsModel; 
use Mail;
use Session;

class ExpireContest extends Command {
    /*
     *
     * The name and signature of the console command.
     *
     * @var string
    */
    protected $signature = 'expire:contest';
    /**
     * The console command description.
     *
     * @var string
    */
    protected $description = 'contest expire';
    /**
     * Create a new command instance.
     *
     * @return void
    */
    public function __construct(){
        parent::__construct();
        $this->log_cron_url     = "https://betteruptime.com/api/v1/heartbeat/9ZVB5nx6gNTYbEq1MLFNdvBy";
        $this->ContestModel       = new ContestModel();
        $this->ContestEntryModel  = new ContestEntryModel();
        $this->NotificationsModel = new NotificationsModel();

    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
         
      $arr_contest= [];
      $date = date('Y-m-d H:i:s');

      $obj_all_contest = $this->ContestModel->select('id','contest_end_date','contest_title','is_notification_sent')
                                           ->whereRaw('contest_end_date<="'.$date.'"')
                                           ->where('is_notification_sent','0')
                                           ->orderBy('id','DESC')
                                           ->get();
      if($obj_all_contest)
      {
         $arr_contest = $obj_all_contest->toArray();
      }
      if(isset($arr_contest) && sizeof($arr_contest)>0)
      {
         foreach ($arr_contest as $key => $value) 
         {
            //dd($value);
            $arr_users = [];
            $contest_id = isset($value['id'])?$value['id']:'0';
            $obj_users = $this->ContestEntryModel->select('id','contest_id','expert_id')
                                                  ->with('expert_details')
                                                 ->where('contest_id',$contest_id)
                                                 ->get();
            if($obj_users)
            {
              $arr_users = $obj_users->toArray();
            } 
            if(isset($arr_users) && sizeof($arr_users)>0)
            {
              foreach ($arr_users as $key1 => $data) 
              {    
                  $arr_data['user_id']              = $data['expert_id'];
                  $arr_data['user_type']            = '3';
                  $arr_data['project_id']           = '0';
                  $arr_data['notification_text_en'] = 'Contest is expired';
                  $arr_data['notification_text_de'] = 'Contest is expired';
                  $arr_data['url']                  = 'expert/contest/show_contest_entry_details/'.base64_encode($contest_id);
                  $arr_data['is_seen']              = '0';
                  //$notify                         = $this->NotificationsModel->create($arr_data);


                  $data1['user_id'] = isset($data['expert_id'])?$data['expert_id']:'';
                  $data1['name']    = isset($data['expert_details']['first_name'])?
                                           $data['expert_details']['first_name']:'';
                                           
                  $data1['email_id'] = $email_id = get_user_email_id($data['expert_id']);

                  $data1['contest_title'] = isset($value['contest_title'])?$value['contest_title']:'';
                  $mail_form = 'no-reply@archexperts.com';
                  $project_name = config('app.project.name');
                  //dd($data1);
                  try
                  {
                      Mail::send('front.email.expire_contest', $data1, function ($message) use ($email_id,$mail_form,$project_name) {
                                $message->from($mail_form, $project_name);
                                $message->subject($project_name.':Expire Contest');
                                $message->to($email_id);
                                //$message->to('yesahotug@atnextmail.com');

                      });
                  }
                  catch(\Exception $e)
                  {
                    dump($e->getMessage());
                  }

                  $arr_update['is_notification_sent'] = '1';
                  $obj_users = $this->ContestModel->where('id',$contest_id)
                                                       ->update($arr_update);
                                  
              }
            }
         }
      }

      $this->log_betteruptime_site();
   }

   private function log_betteruptime_site(){

        $ch = curl_init();
        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $this->log_cron_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // grab URL and pass it to the browser
        $response = curl_exec($ch);
        // close cURL resource, and free up system resources
        curl_close($ch);

   }
}
