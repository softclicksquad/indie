<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ProjectpostModel; 

class ExpireHighlightedProject extends Command {
    /*
     *
     * The name and signature of the console command.
     *
     * @var string
    */
    protected $signature = 'expire:highlightproject';
    /**
     * The console command description.
     *
     * @var string
    */
    protected $description = 'Highlighted project expire';
    /**
     * Create a new command instance.
     *
     * @return void
    */
    public function __construct(){
        parent::__construct();
        
        $this->log_cron_url     = "https://betteruptime.com/api/v1/heartbeat/fcb7m1FmbjxEMrxiTBcu7Ara";
        $this->ProjectpostModel = new ProjectpostModel();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        $this->expire_urjent_project();
        dump('expire_urjent_project succssfully executed');
        $this->expire_feature_project();
        dump('expire_feature_project succssfully executed');

        $this->log_betteruptime_site();
    }

    private function log_betteruptime_site(){

        $ch = curl_init();
        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $this->log_cron_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // grab URL and pass it to the browser
        $response = curl_exec($ch);
        // close cURL resource, and free up system resources
        curl_close($ch);

    }

    public function expire_urjent_project()
    {
      $obj_projects = $this->ProjectpostModel
                           ->where('projects.urjent_heighlight_end_date','<=',date('Y-m-d H:i:s'))
                           ->where('projects.urjent_heighlight_end_date','!=','0000-00-00 00:00:00')
                           ->update(['urjent_heighlight_end_date'=>'0000-00-00 00:00:00','is_urgent'=>'0']);
      if($obj_projects){
        return true;
      }
      return false;
    }
    public function expire_feature_project()
    {
      $obj_projects = $this->ProjectpostModel
                           ->where('projects.highlight_end_date','<=',date('Y-m-d H:i:s'))
                           ->where('projects.highlight_end_date','!=','0000-00-00 00:00:00')
                           ->update(['highlight_end_date'=>'0000-00-00 00:00:00']);
      if($obj_projects){
        return true;
      }
      return false;
    }
}
