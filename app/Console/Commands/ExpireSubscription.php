<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\SubscriptionPacksModel;
use App\Models\SubscriptionUsersModel;
use App\Models\TransactionsModel;
use App\Models\ExpertsCategoriesModel;
use App\Models\ExpertsSkillModel;
use App\Models\FavouriteProjectsModel;
use App\Models\UserModel;
use App\Models\UserWalletModel;
use App\Models\NotificationsModel;
use App\Common\Services\WalletService;
use App\Common\Services\SubscriptionService;
use App\Common\Services\MailService;
use Mail;
use Lang;

class ExpireSubscription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expire:subscription';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscription pack expire';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(WalletService $WalletService,
                                SubscriptionService $SubscriptionService,
                                MailService $MailService)
    {
        parent::__construct();
        $this->log_cron_url     = "https://betteruptime.com/api/v1/heartbeat/4eEgAVa5mWNXHMZ6aomsKAWU";
        $this->SubscriptionPacksModel = new SubscriptionPacksModel();  
        $this->SubscriptionUsersModel = new SubscriptionUsersModel(); 
        $this->TransactionsModel      = new TransactionsModel();  

        $this->ExpertsCategoriesModel = new ExpertsCategoriesModel();
        $this->ExpertsSkillModel      = new ExpertsSkillModel();
        $this->FavouriteProjectsModel = new FavouriteProjectsModel();
        $this->UserModel              = new UserModel();
        $this->UserWalletModel        = new UserWalletModel();
        $this->NotificationsModel     = new NotificationsModel();
        $this->WalletService          = $WalletService;
        $this->SubscriptionService    = $SubscriptionService;
        $this->MailService            = $MailService;
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $current_date     = date('Y-m-d h:i:s');
 
        $arr_subscription = array();
        $obj_subscription = $this->SubscriptionUsersModel->where('is_active','1')
                                                         ->whereRaw('Date(expiry_at)<="'.$current_date.'"')
                                                         ->where('expire_email_send','0')
                                                         ->with('user_details')
                                                         ->get(['id','user_id','expiry_at','expire_email_send','subscription_pack_currency','subscription_pack_id','is_downgrade_request','downgrade_pack_id','is_active']);
        if($obj_subscription && $obj_subscription!=FALSE)
        {
          $arr_subscription = $obj_subscription->toArray();
        }
        if(isset($arr_subscription) && sizeof($arr_subscription)>0)
        {
            foreach ($arr_subscription as $key => $subscription) 
            {
                 //dd($subscription);
                $arr_user = [];
                $arr_user = get_user_wallet_details($subscription['user_id'],$subscription['subscription_pack_currency']);
                if($arr_user)
                {
                  //$arr_user =  $obj_user->toArray();
                  //dd($arr_user);
                  if(isset($arr_user) && count($arr_user)>0)
                  {
                      $subscription_type = '';
                      $mp_wallet_id = $arr_user['mp_wallet_id'];
                      $mp_user_id   = $arr_user['mp_user_id'];

                      $get_mangopay_wallet_details = $this->WalletService->get_wallet_details($arr_user['mp_wallet_id']);
                      $balance_amount = $get_mangopay_wallet_details->Balance->Amount/100;


                      $obj_pack_details =  $this->SubscriptionPacksModel->where('id',$subscription['subscription_pack_id'])
                                                                        ->first();
                      if($obj_pack_details)
                      {
                        $pack_price = $obj_pack_details->pack_price; 
                        $subscription_type = $obj_pack_details->type;
                      }
                      //dd($obj_pack_details->type);
                      //dd($balance_amount,$pack_price);
                      if($balance_amount > $pack_price)
                      {

                        $arr =[];
                        $arr['user_id']         = $subscription['user_id'];
                        if($subscription['is_downgrade_request']=='1')
                        {
                          $arr['pack_id']       = $subscription['downgrade_pack_id'];
                        }
                        else
                        {
                          $arr['pack_id']       = $subscription['subscription_pack_id'];
                        }
                        $arr['type']            = $subscription_type;
                        $arr['mp_wallet_id']    = $mp_wallet_id;
                        $arr['mp_user_id']      = $mp_user_id;
                        $arr['payment_obj_id']  = $subscription['subscription_pack_id'];
                        $arr['transaction_type']= '1';
                        $arr['total_cost']      = $pack_price;
                        $arr['payment_method']  = '3';
                        $arr['currency_code']   = $subscription['subscription_pack_currency'];
                        $purchase_plan_automatic = $this->purchase_plan_automatic($arr);
                      }
                      else
                      {

                        //dd($subscription['user_details']['currency_code']);
                        $arr_user = get_user_wallet_details($subscription['user_id'],$subscription['user_details']['currency_code']);
                        if(!empty($arr_user))
                        {
                          $mp_wallet_id = $arr_user['mp_wallet_id'];
                          $mp_user_id   = $arr_user['mp_user_id'];

                          $get_mangopay_wallet_details = $this->WalletService->get_wallet_details($arr_user['mp_wallet_id']);
                          $balance_amount   = $get_mangopay_wallet_details->Balance->Amount/100;
                          $obj_pack_details =  $this->SubscriptionPacksModel
                                                    ->where('id',$subscription['subscription_pack_id'])
                                                    ->first();
                          if($obj_pack_details)
                          {
                            $pack_price = $obj_pack_details->pack_price; 
                          }

                          if($balance_amount > $pack_price)
                          {

                            $arr =[];
                            $arr['user_id']         = $subscription['user_id'];
                            if($subscription['is_downgrade_request']=='1')
                            {
                              $arr['pack_id']       = $subscription['downgrade_pack_id'];
                            }
                            else
                            {
                              $arr['pack_id']       = $subscription['subscription_pack_id'];
                            }
                            $arr['type']            = $subscription_type;
                            $arr['mp_wallet_id']    = $mp_wallet_id;
                            $arr['mp_user_id']      = $mp_user_id;
                            $arr['payment_obj_id']  = $subscription['subscription_pack_id'];
                            $arr['transaction_type']= '1';
                            $arr['total_cost']      = $pack_price;
                            $arr['payment_method']  = '3';
                            $arr['currency_code']   = $subscription['subscription_pack_currency'];
                            $purchase_plan_automatic = $this->purchase_plan_automatic($arr);
                          }
                          else
                          {
                            $this->free_subscription($subscription['user_id'],'1');
                          }

                        }
                        else
                        {
                          $this->free_subscription($subscription['user_id'],'1');
                        }

                      }
                  }

                }
                else
                {
                  $this->free_subscription($subscription['user_id'],'1');
                }
            }
        }

        $this->log_betteruptime_site();
    }

    private function log_betteruptime_site(){

        $ch = curl_init();
        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $this->log_cron_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // grab URL and pass it to the browser
        $response = curl_exec($ch);
        // close cURL resource, and free up system resources
        curl_close($ch);

   }
   
    public function purchase_plan_automatic($arr)
    {
      // dd($arr);
      $arr_pack_details = array();
      $arr_user_subcription = array();
      $user_id = $arr['user_id'];
      $pack_id = $arr['pack_id'];
      $type    = $arr['type'];
      if(isset($arr) && sizeof($arr)>0) 
      {
        $obj_pack_details =  $this->SubscriptionPacksModel->where('id',$arr['pack_id'])->first();
        //dd($obj_pack_details);
        if($obj_pack_details!=FALSE) 
        {
            $arr_pack_details = $obj_pack_details->toArray();
            if(sizeof($arr_pack_details)>0) 
            {
              if(isset($arr_pack_details['pack_price']) && $arr_pack_details['pack_price']!=0) 
              {
                  $invoice_id = $this->_generate_invoice_id();
                  /*First make transaction  */ 
                  $arr_transaction['user_id']    = $user_id;
                  $arr_transaction['invoice_id'] = $invoice_id;
                  /*transaction_type is type for transactions 1-Subscription*/
                  $arr_transaction['transaction_type'] = 1;
                  $arr_transaction['payment_method']   = 0;
                  $arr_transaction['payment_status']   = 1;
                  $arr_transaction['payment_date']     = date('c');
                  $arr_transaction['paymen_amount']    = $arr_pack_details['pack_price'];
                  $arr_transaction['payment_date']     = date('c');
                  
                  $transaction = $this->TransactionsModel->create($arr_transaction);

                  if($transaction) 
                  {
                  
                    /*insert record in user_subcription for selected pack and invoice details*/
                  $arr_user_subcription['user_id'] = $user_id;
                  $arr_user_subcription['subscription_pack_id'] = isset($arr_pack_details['id'])?$arr_pack_details['id']:'';
                  $arr_user_subcription['invoice_id'] = $invoice_id;
                  if($type=='MONTHLY'){
                    $arr_user_subcription['expiry_at'] = date('Y-m-d',strtotime("+30 days"));
                  }
                  if($type=='YEARLY')
                  {
                    $arr_user_subcription['expiry_at'] = date('Y-m-d',strtotime("+365 days"));
                  }
                  $arr_user_subcription['pack_name'] = isset($arr_pack_details['pack_name'])?$arr_pack_details['pack_name']:'';
                  $arr_user_subcription['pack_price'] = isset($arr_pack_details['pack_price'])?$arr_pack_details['pack_price']:'';
                  $arr_user_subcription['validity'] = isset($arr_pack_details['validity'])?$arr_pack_details['validity']:'';
                  $arr_user_subcription['number_of_bids'] = isset($arr_pack_details['number_of_bids'])?$arr_pack_details['number_of_bids']:'0';
                  $arr_user_subcription['topup_bid_price'] = isset($arr_pack_details['topup_bid_price'])?$arr_pack_details['topup_bid_price']:'0';
                  $arr_user_subcription['number_of_categories'] = isset($arr_pack_details['number_of_categories'])?$arr_pack_details['number_of_categories']:'0';
                  $arr_user_subcription['number_of_subcategories'] = isset($arr_pack_details['number_of_subcategories'])?$arr_pack_details['number_of_subcategories']:'0';
                  $arr_user_subcription['number_of_skills'] = isset($arr_pack_details['number_of_skills'])?$arr_pack_details['number_of_skills']:'0';
                  $arr_user_subcription['number_of_favorites_projects'] = isset($arr_pack_details['number_of_favorites_projects'])?$arr_pack_details['number_of_favorites_projects']:'0';

                  $arr_user_subcription['website_commision'] = isset($arr_pack_details['website_commision'])?$arr_pack_details['website_commision']:'0';
                  
                  $arr_user_subcription['allow_email'] = isset($arr_pack_details['allow_email'])?$arr_pack_details['allow_email']:'0';
                  $arr_user_subcription['allow_chat'] = isset($arr_pack_details['allow_chat'])?$arr_pack_details['allow_chat']:'0';
                  $arr_user_subcription['is_active'] = 1;
                  $arr_user_subcription['subscription_pack_currency'] = $arr['currency_code'];

                  //update all previous subscription status to 0 and then create new subscription
                  $subscription_update = $this->SubscriptionUsersModel->where('user_id',$user_id)->where('is_active',1)->update(['is_active'=>0]);
                  

                  $subscription = $this->SubscriptionUsersModel->create($arr_user_subcription);
                  
                  /*delete category and skills of expert*/
                  if($pack_id=='1')
                  {
                    $this->remove_subscription_dependency($user_id);
                  }
                  $admin_data = get_user_wallet_details('1',$arr['currency_code']);
                  if(isset($admin_data) && count($admin_data)>1)
                  {
                      $string_amount = str_replace(",","",$arr['total_cost']);
                      $amount_int    = (int) $string_amount;
                      
                      $transaction_inp['tag']                      = $arr_transaction['invoice_id'].'- Subscription fee';
                      $transaction_inp['credited_UserId']          = $admin_data['mp_user_id'];   // archexpert admin user id
                      $transaction_inp['credited_walletId']        = (string)$admin_data['mp_wallet_id']; 
                      $transaction_inp['total_pay']                = $amount_int;                 // services amount 

                      $transaction_inp['debited_UserId']           = $arr['mp_user_id']; // expert user id
                      $transaction_inp['debited_walletId']         = (string)$arr['mp_wallet_id']; // expert wallet id
                      $transaction_inp['currency_code']            = $arr['currency_code']; // expert wallet id
                      $transaction_inp['cost_website_commission']  = '0';
                      $pay_fees        = $this->WalletService->walletTransfer($transaction_inp);
                      $project         = [];
                      if(isset($pay_fees->Status) && $pay_fees->Status == 'SUCCEEDED')
                      {
                          $update_transaction   = [];
                          $update_transaction['WalletTransactionId']    = isset($pay_fees->Id) ? $pay_fees->Id : '0';
                          $update_transaction['payment_status']         = 2; // paid
                          $update_transaction['response_data']          = json_encode($pay_fees); // paid
                          $updatetransaction = $this->TransactionsModel->where('invoice_id',$arr_transaction['invoice_id'])->update($update_transaction); 
                          // end update transaction details    

                          // send notification to admin
                          $arr_admin_data                         =  [];
                          $arr_admin_data['user_id']              =  $admin_data['id'];
                          $arr_admin_data['user_type']            = '1';
                          $arr_admin_data['url']                  = 'admin/wallet/archexpert';
                          $arr_admin_data['project_id']           = '';
                          $arr_admin_data['notification_text_en'] = $arr_transaction['invoice_id'].'-'.Lang::get('controller_translations.subscription_fee_paid',[],'en','en').' '.$pay_fees->Id;
                          $arr_admin_data['notification_text_de'] = $arr_transaction['invoice_id'].'-'.Lang::get('controller_translations.subscription_fee_paid',[],'de','en').' '.$pay_fees->Id;
                          $this->NotificationsModel->create($arr_admin_data); 
                          // end send notification to admin    

                          $subscription_update = $this->SubscriptionUsersModel->where('user_id',$user_id)->where('is_active',1)->update(['is_active'=>0]);  
                          $subscription_update_paid = $this->SubscriptionUsersModel->where('user_id',$user_id)->where('invoice_id',$arr_transaction['invoice_id'])->update(['is_active'=>1]); 
                          if($arr['payment_obj_id']=='1'){
                          $this->remove_subscription_dependency($user_id);
                          }
                          $this->MailService->subscriptionMail($arr_transaction['invoice_id']);
                          $this->SubscriptionUsersModel->where('user_id',$user_id)->update(['expire_email_send'=>1]);
                          return true;
                      }
                  }
                  return false;
              }
            }
          }
        }
      }
      return false;
    }

    public function free_subscription($user_id,$pack_id)
    {
      $arr_pack_details = array();
      $arr_user_subcription = array();

      if (isset($user_id) && $user_id!=null) 
      {
        $obj_pack_details =  $this->SubscriptionPacksModel->where('id',$pack_id)->first();

        if ($obj_pack_details!=FALSE) 
        {
            $arr_pack_details = $obj_pack_details->toArray();
          
            if (sizeof($arr_pack_details)>0) 
            {
              if (isset($arr_pack_details['pack_price']) && $arr_pack_details['pack_price']==0) 
              {
                  $invoice_id = $this->_generate_invoice_id();

                  /*First make transaction  */ 
                  $arr_transaction['user_id'] = $user_id;
                  $arr_transaction['invoice_id'] = $invoice_id;
                  /*transaction_type is type for transactions 1-Subscription*/
                  $arr_transaction['transaction_type'] = 1;
                  $arr_transaction['payment_method']   = 0;
                  $arr_transaction['payment_status']   = 1;
                  $arr_transaction['payment_date']     = date('c');
                  $arr_transaction['paymen_amount']    = $arr_pack_details['pack_price'];
                  $arr_transaction['payment_date']     = date('c');

                  /* get currency for transaction */
                  /*$arr_currency = setCurrencyForTransaction();

                  $arr_transaction['currency']      = isset($arr_currency['currency']) ? $arr_currency['currency'] : '$';
                  $arr_transaction['currency_code'] = isset($arr_currency['currency_code']) ? $arr_currency['currency_code'] : 'USD';*/
                  
                  
                  $transaction = $this->TransactionsModel->create($arr_transaction);

                  if ($transaction) 
                  {
                  
                    /*insert record in user_subcription for selected pack and invoice details*/
                  $arr_user_subcription['user_id'] = $user_id;
                  $arr_user_subcription['subscription_pack_id'] = isset($arr_pack_details['id'])?$arr_pack_details['id']:'';
                  $arr_user_subcription['invoice_id'] = $invoice_id;
                  $arr_user_subcription['expiry_at'] = null;
                  $arr_user_subcription['pack_name'] = isset($arr_pack_details['pack_name'])?$arr_pack_details['pack_name']:'';
                  $arr_user_subcription['pack_price'] = isset($arr_pack_details['pack_price'])?$arr_pack_details['pack_price']:'';
                  $arr_user_subcription['validity'] = isset($arr_pack_details['validity'])?$arr_pack_details['validity']:'';
                  $arr_user_subcription['number_of_bids'] = isset($arr_pack_details['number_of_bids'])?$arr_pack_details['number_of_bids']:'0';
                  $arr_user_subcription['topup_bid_price'] = isset($arr_pack_details['topup_bid_price'])?$arr_pack_details['topup_bid_price']:'0';
                  $arr_user_subcription['number_of_categories'] = isset($arr_pack_details['number_of_categories'])?$arr_pack_details['number_of_categories']:'0';
                  $arr_user_subcription['number_of_subcategories'] = isset($arr_pack_details['number_of_subcategories'])?$arr_pack_details['number_of_subcategories']:'0';
                  $arr_user_subcription['number_of_skills'] = isset($arr_pack_details['number_of_skills'])?$arr_pack_details['number_of_skills']:'0';
                  $arr_user_subcription['number_of_favorites_projects'] = isset($arr_pack_details['number_of_favorites_projects'])?$arr_pack_details['number_of_favorites_projects']:'0';

                  $arr_user_subcription['website_commision'] = isset($arr_pack_details['website_commision'])?$arr_pack_details['website_commision']:'0';
                  
                  $arr_user_subcription['allow_email'] = isset($arr_pack_details['allow_email'])?$arr_pack_details['allow_email']:'0';
                  $arr_user_subcription['allow_chat'] = isset($arr_pack_details['allow_chat'])?$arr_pack_details['allow_chat']:'0';
                  $arr_user_subcription['is_active'] = 1;
                  $arr_user_subcription['subscription_pack_currency'] = 'USD';
                  //update all previous subscription status to 0 and then create new subscription
                  $subscription_update = $this->SubscriptionUsersModel->where('user_id',$user_id)->where('is_active',1)->update(['is_active'=>0]);
                  

                  $subscription = $this->SubscriptionUsersModel->create($arr_user_subcription);
                  
                  /*delete category and skills of expert*/
                  if($pack_id=='1')
                  {
                    $this->remove_subscription_dependency($user_id);
                  }

                  $obj_data = $this->UserModel->select('first_name','email')->where('id',$user_id)->first();
                  if($obj_data)
                  {
                    $arr_data = $obj_data->toArray();
                    $first_name   = isset($arr_data['first_name']) && $arr_data['first_name']!=''?$arr_data['first_name']:'Sir/Mam';
                    $email_to     = $arr_data['email'];
                    $mail_form    = 'no-reply@archexperts.com';
                    $project_name = '';
                    
                    $content= "Hello '".$first_name."', we appreciate your trust in our service, 
                              but it is a pity you have not renewed your paid subscription. 
                              However you still can upgrade the plan or you can enjoy a free membership. 
                              If so, just have a quick look at your profile and select your favorite categories, 
                              subcategories and skills that you can cover. 
                              By the way, you can massively increase your chances of getting a job by upgrading your subscription plan now.";

                    $content = view('email.general', compact('content'))->render();
                    $content = html_entity_decode($content);
                    $data = [];
                      try
                      {
                          Mail::send('email.general', $data, function ($message) use ($email_to,$mail_form,$project_name,$content) 
                          {
                                $message->from($mail_form, $project_name);
                                $message->subject($project_name.' : Archexpert subscription plan');
                                $message->setBody($content, 'text/html');
                                $message->to($email_to); 
                          });
                      }
                      catch(\Exception $e){
                      }

                      //$data_update['expire_email_send'] = '1';
                      $this->SubscriptionUsersModel->where('user_id',$user_id)->update(['expire_email_send'=>1]);
                  }
                  return true;
              }
            }
          }
        }
      }

      return false;
    }



   private function _generate_invoice_id()
   {
        $secure = TRUE;    
        $bytes = openssl_random_pseudo_bytes(3, $secure);
        $order_token = 'INV'.date('Ymd').strtoupper(bin2hex($bytes));
        return $order_token;
   }

   /*
      Auther: sagar sainkar
      Comment : this is function for remove category , skills , and favourite project for subscription
  */
   public function remove_subscription_dependency($expert_user_id=null)
   {
     if (isset($expert_user_id) && $expert_user_id!=null) 
     {
       $delete_category  = $this->ExpertsCategoriesModel->where('expert_user_id',$expert_user_id)->delete();
       $delete_skills    = $this->ExpertsSkillModel->where('expert_user_id',$expert_user_id)->delete();
       $delete_favourite = $this->FavouriteProjectsModel->where('expert_user_id',$expert_user_id)->delete();
     }
   }

}
