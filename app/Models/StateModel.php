<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StateModel extends Model
{
    protected $table = 'state';
    protected $primaryKey = 'id';
    protected $fillable = ['state_name', 'is_active','country_id'];
    

    public function country_details()
    {
        return $this->belongsTo('App\Models\CountryModel','country_id','id');
    }    

    public function cities()
    {
        return $this->hasMany('App\Models\CityModel','state_id','id');
    }
}
