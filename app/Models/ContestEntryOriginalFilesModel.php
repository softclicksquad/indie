<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContestEntryOriginalFilesModel extends Model
{
    protected $table = 'contests_entry_original_files';
    protected $primaryKey = 'id';
    protected $fillable = ['contest_id','contest_entry_id', 'file_name','created_at','updated_at','original_file_name'];
}
