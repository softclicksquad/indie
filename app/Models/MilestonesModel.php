<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class MilestonesModel extends Model
{
    protected $table      = "milestones";
    protected $primaryKey = "id";

    protected $fillable   = [
    							'project_id',
    							'client_user_id',
                                'expert_user_id',
    							'created_by',
    							'title',
    							'cost',
                                'cost_project_manager_commission',
                                'cost_from_client',
                                'cost_website_commission',
                                'cost_to_expert',
                                'project_manager_commission',
                                'website_commission',
    							'description',
                                'milestone_no',
                                'milestone_due_date',
    							'invoice_id',
    							'status',
                                'AuthorId',
                                'TransactionId',
                                'mp_transaction_status',
                                'mp_transaction_type',
                                'is_milestone'
    						];

   public $appends = ['role_info'];

    public function user_details()
    {
        return $this->belongsTo('App\Models\UserModel','client_user_id','id');
    }                          
    /* Relation with expert details with Expert informanation*/
    public function client_details()
    {
        return $this->belongsTo('App\Models\ClientsModel','client_user_id','user_id');
    }
    public function expert_details()
    {
        return $this->belongsTo('App\Models\ExpertsModel','expert_user_id','user_id');
    }
    public function expert_currency()
    {
        return $this->belongsTo('App\Models\UserWalletModel','expert_user_id','user_id');
    }                        
    public function project_details()
    {
        return $this->belongsTo('App\Models\ProjectpostModel','project_id','id');
    }
    public function transaction_details()
    {
        return $this->hasOne('App\Models\TransactionsModel','invoice_id','invoice_id');
    }
    public function milestone_release_details()
    {
        return $this->hasOne('App\Models\MilestoneReleaseModel','milestone_id','id');
    }
    public function expert_subscription()
    {
        return $this->hasOne('App\Models\SubscriptionUsersModel','user_id','expert_user_id');
    }
    public function milestone_refund_details()
    {
        return $this->hasOne('App\Models\MilestoneRefundRequests','milestone_id','id');
    }
    public function getRoleInfoAttribute()
    {
        $user = $this->user_details;
        if(isset($user) && $user->inRole('expert'))
        {
            $expert = app(\App\Models\ExpertsModel::class);
            $expert = $expert->where('user_id',$user->id)->first();
            $arr_expert = [];
            if($expert)
            {
                $arr_expert = $expert->toArray();
            }
            return $arr_expert;
        }
        elseif(isset($user) && $this->user_details->inRole('client'))
        {
            $client = app(\App\Models\ClientsModel::class);
            $client = $client->where('user_id',$user->id)->first();
            $arr_client = [];
            if($client)
            {
                $arr_client = $client->toArray();
            }
            return $arr_client;

        }
        return false;
    }             
}
