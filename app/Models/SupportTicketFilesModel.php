<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupportTicketFilesModel extends Model
{
    protected $table = 'support_ticket_files';

    protected $fillable = [ 'support_ticket_id',
    						'thread_id',
    						'file',
    						'type',
    								];
}
