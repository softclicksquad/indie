<?php

namespace App\Models;

use \Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model as Eloquent;
class FAQModel extends Eloquent
{
	use Translatable;
    protected $table              = 'faq';
    public $translationModel 	  = 'App\Models\FAQTranslationModel';
    public $translationForeignKey = 'question_id';
    public $translatedAttributes  = ['question_name','question_ans','locale'];
    protected $fillable           = ['question_slug','faq_type','is_active','created_at'];


    public function page_traslation()
    {
        return $this->hasMany('App\Models\FAQTranslationModel','question_id','id');
    }
    protected static function boot() {
        parent::boot();

        static::deleting(function($page) 
        { 
             $page->page_traslation()->delete();
        });
    }					
}
