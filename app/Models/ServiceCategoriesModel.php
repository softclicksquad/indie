<?php

namespace App\Models;

use \Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class ServiceCategoriesModel extends Model
{
    use Translatable;
    protected $table = 'service_categories';

    /* Translatable Config */
    public $translationModel 	  = 'App\Models\ServiceCategoriesTranslationModel';
    public $translationForeignKey = 'service_categories_id';
    public $translatedAttributes  = ['category_title','meta_tag','description'];
    protected $fillable 		  = ['category_slug','is_active','category_image'];

    public function page_traslation()
    {
        return $this->hasMany('App\Models\ServiceCategoriesTranslationModel','service_categories_id','id');
    }

    
    protected static function boot() {
        parent::boot();

        static::deleting(function($page) 
        { 
             $page->page_traslation()->delete();
        });
    }
}
