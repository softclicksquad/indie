<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class MilestoneRefundRequests extends Model {
    protected $primaryKey = "id";
    protected $table      = "milestone_refund_requests";
    protected $fillable   = ['project_id','milestone_id','is_refunded','Refund_reason','InitialTransactionId','AuthorId','created_at','updated_at'];

    public function project_details(){
        return $this->belongsTo('App\Models\ProjectpostModel','project_id','id')->with(['client_details']);
    }
    public function milestone_details(){
        return $this->belongsTo('App\Models\MilestonesModel','milestone_id','id');
    }
} // end module
