<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectManagerModel extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table      = "project_manager";
    protected $primaryKey = "id";
    protected $fillable   = [	'user_id',
    							'first_name',
    							'last_name',
    							'address',
                                'phone_code',
    							'phone',
    							'profile_image',
                                'country',
                                'state',
                                'city',
                                'zip'
    						];


    public function user_details()
    {
    	return $this->BelongsTo('App\Models\UserModel','user_id','id');
    }

    public function country_details()
    {
        return $this->belongsTo('App\Models\CountryModel','country','id');
    }
    public function state_details()
    {
        return $this->belongsTo('App\Models\StateModel','state','id');
    }
    public function city_details()
    {
        return $this->belongsTo('App\Models\CityModel','city','id');
    }

}
