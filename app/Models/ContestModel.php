<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ContestModel extends Model
{
    protected $table      = 'contests';
    protected $primaryKey = 'id';
    protected $fillable   = ['category_id',
    'sub_category_id',
    'client_user_id',  
    'contest_title',
    'contest_description',
    'contest_additional_info',
    'contest_price',
    'contest_currency',
    'contest_end_date',
    'contest_attachment',
    'contest_status',
    'winner_choose',
    'is_active',
    'is_notification_sent',
    'updated_at',
    'created_at',
    'invoice_id',
    'mp_contest_wallet_id',
    'payment_status',
    'usd_contest_price',
    'edited_at',
    'is_refunded',
    'refund_payment_response',
    'failed_refund_retry_count'
];

/* Relation with post contest details with skill informanation*/
public function skill_details(){
    return $this->belongsTo('App\Models\SkillsModel','skill_id','id');
}             
/* To get all contest skills */
public function contest_skills(){
    return $this->hasMany('App\Models\ContestSkillsModel','contest_id','id');
} 
public function category_details(){
    return $this->hasOne('App\Models\CategoriesModel','id','category_id');
}
public function sub_category_details(){
    return $this->hasOne('App\Models\SubCategoriesModel','id','sub_category_id');
}
public function client_user_details(){
    return $this->hasOne('App\Models\ClientsModel','user_id','client_user_id');
}
public function user_details(){
    return $this->hasOne('App\Models\UserModel','id','client_user_id');
}
public function contest_entry(){
    return $this->hasMany('App\Models\ContestEntryModel','contest_id','id');   
}
public function contest_post_documents(){
    return $this->hasMany('App\Models\ContestPostDocumentsModel','contest_id','id');   
}
} // end php
