<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ProjectsBidsTopupModel extends Model
{
    protected $table = 'projects_bids_topup';

    protected $fillable   = [   'expert_user_id',
                                'user_subscription_id',
                                'invoice_id',
                                'amount',
                                'payment_status',
                                'is_used'
                            ];

    public function transaction_details()
    {
        return $this->hasOne('App\Models\TransactionsModel','invoice_id','invoice_id');
    }

}
