<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CountryModel extends Model
{
    protected $table = 'country';
    protected $primaryKey = 'id';
    protected $fillable = ['country_code','country_name','is_active','is_mangopay_blocked'];

   	public function states()
    {
        return $this->hasMany('App\Models\StateModel','country_id','id');
    }
    public function country_details()
    {
        return $this->hasMany('App\Models\ExpertsModel','country','id');
    }
}
