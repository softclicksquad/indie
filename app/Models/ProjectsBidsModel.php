<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ProjectsBidsModel extends Model
{
    protected $table = 'projects_bids';

    protected $fillable   = [    'project_id',
                                'expert_user_id',
                                'bid_cost',
                                'bid_estimated_duration',
                                'bid_description',
                                'bid_attachment',
                                'bid_status',
                                'rejection_reason',
                                'is_suggested',
                                'nda_aggreement_accept',
                                'nda_aggreement_pdf',
                                'hour_per_week',
                                'is_hour_custom_rate',
                                'is_shortlist',
                                'shortlist_desc',
                                'is_chat_initiated',
                                'is_admin_chat_initiated'
                            ];
   
   public $appends = ['role_info'];

   public function project_details()
   {
        return $this->belongsTo('App\Models\ProjectpostModel','project_id','id');
   } 

   
    public function user_details()
    {
        return $this->belongsTo('App\Models\UserModel','expert_user_id','id');
    }                          

     /* Relation with expert details with Expert informanation*/
    public function expert_details()
    {
        return $this->belongsTo('App\Models\ExpertsModel','expert_user_id','user_id');
    }

    public function review_details()
    {
        return $this->hasMany('App\Models\ReviewsModel','to_user_id','expert_user_id');
    }

    /*public function bid_attachments()
    {
        return $this->hasMany('App\Models\ProjectAttchmentModel','bid_id','id');
    }*/

    public function getRoleInfoAttribute()
    {
        $user = $this->user_details;
        if(isset($user) && $user->inRole('expert'))
        {
            $expert = app(\App\Models\ExpertsModel::class);
            $expert = $expert->where('user_id',$user->id)->first();
            $arr_expert = [];
            if($expert)
            {
                $arr_expert = $expert->toArray();
            }
            return $arr_expert;
        }
        elseif(isset($user) && $this->user_details->inRole('client'))
        {
            $client = app(\App\Models\ClientsModel::class);
            $client = $client->where('user_id',$user->id)->first();
            $arr_client = [];
            if($client)
            {
                $arr_client = $client->toArray();
            }
            return $arr_client;

        }
        return false;
    }                      



}
