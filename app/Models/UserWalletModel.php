<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Users\EloquentUser as CartalystUser;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cmgmyr\Messenger\Traits\Messagable;
use Cache;

class UserWalletModel extends CartalystUser 
{
    use SoftDeletes;
    use Messagable;
    
    // protected $dates = ['deleted_at'];
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_wallet';
    
   /* protected $hidden = ['password'];*/

    protected $primaryKey = "id";

    protected $fillable   = [	
    							'user_id',
                                'currency_code',
    							'mp_user_id',
    							'mp_wallet_id'
    						];

    public function user_details()
    {
        return $this->hasOne('App\Models\UserModel','user_id','id');
    }

    // public function client_details()
    // {
    //     return $this->hasOne('App\Models\ClientsModel','user_id','id');
    // }

    
 
}