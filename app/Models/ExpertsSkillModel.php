<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExpertsSkillModel extends Model
{
    protected $table      = "experts_skills";
    protected $primaryKey = "id";
    protected $fillable   = [	'expert_user_id',
    							'skill_id',
    						];

   public function skills()
   {
        return $this->hasOne('App\Models\SkillsModel','id','skill_id');
   }
 						
}
