<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SealedEntryRatesModel extends Model
{
    use SoftDeletes;

    protected $table    = 'sealed_entry_rates';
    protected $fillable = ['id','quantity','price','is_active'];
}
