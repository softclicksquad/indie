<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExpertPortfolioModel extends Model
{
     //use SoftDeletes;
/*    protected $dates = ['updated_at'];
*/  protected $table      = "experts_portfolio";
    protected $primaryKey = "id";
    protected $fillable   = ['expert_user_id',
    						 'file_name',
    						];

    
    public function expert_details()
    {
        return $this->belongsTo('App\Models\ExpertsModel','expert_user_id','user_id');
    }   						
}
