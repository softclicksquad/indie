<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CityModel extends Model
{
    protected $table = 'city';
    protected $primaryKey = 'id';
    protected $fillable = ['city_name', 'state_id','is_active'];
    
    public function state_details()
    {
        return $this->belongsTo('App\Models\StateModel','state_id','id');
    }    
}
