<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContestPostDocumentsModel extends Model
{
    protected $table      = 'contest_post_documents';
   protected $fillable    =	[ 'contest_id',
   							  'image_name',
   							  'image_original_name'
   							];	
}
