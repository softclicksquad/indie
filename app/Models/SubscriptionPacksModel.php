<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriptionPacksModel extends Model
{
    protected $table      = "subscription_packs";
    protected $primaryKey = "id";

    protected $fillable   = [
    							'pack_name',
    							'pack_price',
    							'validity',
    							'number_of_bids',
    							'topup_bid_price',
    							'number_of_categories',
                                'number_of_subcategories',
    							'number_of_skills',
    							'number_of_favorites_projects',
                                'website_commision',
    							'allow_email',
    							'allow_chat',
    							'is_default',
    							'is_active',
                                'type',
                                'number_of_payouts',
                                'off',
                                'pack_price_eur',
                                'pack_price_gbp',
                                'pack_price_pln',
                                'pack_price_chf',
                                'pack_price_nok',
                                'pack_price_sek',
                                'pack_price_dkk',
                                'pack_price_cad',
                                'pack_price_zar',
                                'pack_price_aud',
                                'pack_price_hkd',
                                'pack_price_jpy',
                                'pack_price_czk'
    						];

    public function subscription_user_details()
    {
        return $this->belongsTo('App\Models\SubscriptionUsersModel','id','subscription_pack_id')->limit(1)->orderBy('id','DESC');
    }                        
} // end class
