<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExpertsModel extends Model
{
    use SoftDeletes;

    protected $dates      = ['deleted_at'];
    protected $table      = "experts";
    protected $primaryKey = "id";    

    protected $fillable   = [	'user_id',
                                'user_type',
    							'first_name',
    							'last_name',
                                'phone_code',
    							'phone_number',
    							'country',
    							'state',
    							'city',
    							'zip',
    							'address',
                                'latitude',
                                'longitude',
                                'profession',
    							'company_name',
    							'vat_number',
                                'spoken_languages',
                                'vat_number',
                                'timezone',
                                'about_me',
                                'hourly_rate',
                                'education_id',
    							'profile_image',
                                'show_profile_image_name',
                                'show_cover_image_name',
                                'cover_image',
                                'paypal_email',
    						];
    public function subscription_details()
    {
        return $this->belongsTo('App\Models\SubscriptionUsersModel','user_id','user_id')->where('is_active','1');
    }                        

    public function user_details()
    {
        return $this->belongsTo('App\Models\UserModel','user_id','id');
    }
    public function country_details()
    {
        return $this->belongsTo('App\Models\CountryModel','country','id');
    }
    public function state_details()
    {
        return $this->belongsTo('App\Models\StateModel','state','id');
    }
    public function city_details()
    {
        return $this->belongsTo('App\Models\CityModel','city','id');
    }
    public function expert_skills()
    {
        return $this->hasMany('App\Models\ExpertsSkillModel','expert_user_id','user_id');
    }

    public function expert_categories()
    {
        return $this->hasMany('App\Models\ExpertsCategoriesModel','expert_user_id','user_id');
    }

    public function expert_subcategories()
    {
        return $this->hasMany('App\Models\ExpertCategorySubCategoryModel','expert_user_id','user_id');
    }

    public function review_details()
    {
        return $this->hasMany('App\Models\ReviewsModel','to_user_id','user_id');
    }

    public function project_details()
    {
       return $this->hasMany('App\Models\ProjectpostModel','expert_user_id','user_id');
    }

    public function expert_portfolio()
    {
        return $this->hasMany('App\Models\ExpertPortfolioModel','expert_user_id','user_id');
    }
        
    public function expert_spoken_languages()
    {
        return $this->hasMany('App\Models\ExpertsSpokenLanguagesModel','expert_user_id','user_id');
    }    
    public function profession_details()
    {
        return $this->belongsTo('App\Models\ProfessionModel','profession','id');
    }

    public function education_details()
    {
        return $this->belongsTo('App\Models\EducationModel','education_id','id');
    }

    public function expert_work_experience()
    {
        return $this->hasMany('App\Models\ExpertWorkExperienceModel','expert_user_id','user_id');
    }

    public function expert_certification_details()
    {
        return $this->hasMany('App\Models\ExpertsCertificationDetailsModel','expert_user_id','user_id');
    }
    

}
