<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class SubCategoriesTranslationModel extends Model
{
    protected $table='subcategories_translation';
    public $timestamps = false;
    protected $fillable = ['subcategories_id','subcategory_title','locale'];
}