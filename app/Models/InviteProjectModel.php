<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InviteProjectModel extends Model
{
    protected $table      = 'invite_to_project';
    protected $primaryKey = 'id';
    protected $fillable   = ['project_id', 'client_user_id','expert_user_id','expert_email_id','message','notification_id','is_read','create_at','expire_on','updated_at','unique_id'];

    public function notification_details()
    {
        return $this->belongsTo('App\Models\NotificationsModel','notification_id','id');
    }
    public function project_details()
    {
        return $this->belongsTo('App\Models\ProjectpostModel','project_id','id');
    }  
    public function client_details()
    {
        return $this->belongsTo('App\Models\ClientsModel','client_user_id','user_id');
    } 
} // end model


