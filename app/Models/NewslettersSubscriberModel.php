<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewslettersSubscriberModel extends Model
{
    protected $table      = "newsletters_subscriber";
    protected $primaryKey = "id";

    protected $fillable   = [	'subscriber_email',
    							'is_active',
    						];
}
