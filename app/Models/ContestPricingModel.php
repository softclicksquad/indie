<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class ContestPricingModel extends Model
{
    protected $PrimaryKey = 'id';
    protected $table      = 'contest_pricing';
    protected $fillables  = ['id','pricing_for','slug','price','price_type','status','created_at','updated_at'];
}
