<?php

namespace App\Models;

use \Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class SubCategoriesModel extends Model
{
    use Translatable;
    protected $table = 'subcategories';

    /* Translatable Config */
    public $translationModel 	  = 'App\Models\SubCategoriesTranslationModel';
    public $translationForeignKey = 'subcategories_id';
    public $translatedAttributes  = ['subcategory_title'];
    protected $fillable 		  = ['category_id','subcategory_slug','is_active'];

    public function subcategory_traslation()
    {
        return $this->hasMany('App\Models\SubCategoriesTranslationModel','subcategories_id','id');
    }

    public function category_details()
    {
        return $this->belongsTo('App\Models\CategoriesModel','category_id','id');
    }

    public function subcategory_search()
    {
        $locale =  \App::getLocale();    
        return $this->hasMany('App\Models\SubCategoriesTranslationModel','subcategories_id','id')->where('locale','=',$locale);
    }
    
    protected static function boot() {
        parent::boot();

        static::deleting(function($obj) 
        { 
             $obj->subcategory_traslation()->delete();
        });
    }
}
