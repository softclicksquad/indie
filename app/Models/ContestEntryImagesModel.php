<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContestEntryImagesModel extends Model
{
    protected $table = 'contests_entry_images';
    protected $primaryKey = 'id';
    protected $fillable = ['contest_entry_id',
    						'image',
    						'file_no',
    						'is_own_work',
    						'is_admin_deleted'
    					];

   	public function contest_details()
    {
        return $this->hasOne('App\Models\ContestModel','id','contest_entry_id');
    }

    public function file_rating()
    {
        return $this->hasOne('App\Models\ContestEntryImagesRatingModel','image_id','id');
    }
}
