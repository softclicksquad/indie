<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ProjectRequestsModel extends Model
{
    protected $table = 'project_requests';

    protected $fillable   = [   'project_id',
                                'expert_user_id',
                                'client_user_id',
                                'is_accepted',
                                
                            ];

   public function project_details()
   {
        return $this->belongsTo('App\Models\ProjectpostModel','project_id','id');
   }

}
