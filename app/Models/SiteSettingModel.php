<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;


class SiteSettingModel extends Eloquent
{
    
    protected $table      = "site_settings";
    protected $primaryKey = "site_settting_id";

    protected $fillable   = [	
    							'site_name',
    							'site_email_address',
    							'site_contact_number',
    							'site_address',
    							'meta_desc',
    							'meta_keyword',
    							'fb_url',
    							'linkedin_url',
    							'twitter_url',
    							'google_plus_url',
                                'mailchimp_api_key',
                                'mailchimp_list_id',
                                'wesite_project_commission',
                                'website_pm_initial_cost',
                                'site_status',
                                'wesite_project_manager_commission',
                                'nda_price',
                                'urgent_price',
                                'private_price',
                                'recruiter_price',
                                'project_cost',
                                'default_currency',
                                'default_currency_code'
    						];
}
