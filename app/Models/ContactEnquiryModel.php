<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactEnquiryModel extends Model
{
    protected $table = 'contact_enquiry';
    protected $primaryKey = 'id';
    protected $fillable = ['name','contact_number','enquiry_subject','email','enquiry_message','reply_message','reply_status','is_report_a_problem','report_a_problem','user_id','reply_date_time','enquiry_indentifier'];
}
