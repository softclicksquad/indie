<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Users\EloquentUser as CartalystUser;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cmgmyr\Messenger\Traits\Messagable;
use Cache;

class UserModel extends CartalystUser 
{
    use SoftDeletes;
    use Messagable;
    
    // protected $dates = ['deleted_at'];
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    
   /* protected $hidden = ['password'];*/

    protected $primaryKey = "id";

    protected $fillable   = [	
    							'email',
                                'user_name',
    							'password',
    							'permissions',
    							'last_login',
    							'first_name',
    							'last_name',
                                'is_active',
                                'is_available',
    							'token',
                                'ip_address',
                                'last_logout',
                                'mp_user_id',
                                'mp_wallet_id',
                                'mp_wallet_created',
                                'kyc_verified',
                                'is_email_changed',
                                'is_email_verified',
                                'currency_code',
                                'currency_symbol',
                                'support_category_id',
                                'deleted_at'
    						];


    public $appends = ['role_info','is_online'];  

    
    public function getIsOnlineAttribute()
    {
        return Cache::has('user-is-online-'.$this->id);
    }

    public function expert_details()
    {
        return $this->hasOne('App\Models\ExpertsModel','user_id','id');
    }

    public function client_details()
    {
        return $this->hasOne('App\Models\ClientsModel','user_id','id');
    }

    public function getRoleInfoAttribute()
    {
        if($this->inRole('expert'))
        {
            $expert = app(\App\Models\ExpertsModel::class);
            $expert = $expert->where('user_id',$this->id)->with(['country_details.states','state_details.cities','city_details','expert_skills','profession_details'])->first();
            $arr_expert = [];
            if($expert)
            {
                $arr_expert = $expert->toArray();
            }
            return $arr_expert;
        }
        elseif($this->inRole('client'))
        {
            $client = app(\App\Models\ClientsModel::class);
            $client = $client->where('user_id',$this->id)->with(['country_details.states','state_details.cities','city_details'])->first();
            $arr_client = [];
            if($client)
            {
                $arr_client = $client->toArray();
            }
            return $arr_client;

        }
        elseif($this->inRole('admin') || $this->inRole('subadmin'))
        {
            $admin = app(\App\Models\AdminProfileModel::class);
            $admin = $admin->where('user_id',$this->id)->first();
            $arr_admin = [];
            if($admin)
            {
                $arr_admin = $admin->toArray();
            }
            return $arr_admin;
        }
        elseif($this->inRole('project_manager'))
        {
            $project_manager = app(\App\Models\ProjectManagerModel::class);
            $project_manager = $project_manager->where('user_id',$this->id)->with(['country_details.states','state_details.cities','city_details'])->first();
            $arr_project_manager = [];
            if($project_manager)
            {
                $arr_project_manager = $project_manager->toArray();
            }
            return $arr_project_manager;
        }
        elseif($this->inRole('recruiter'))
        {
            $recruiter = app(\App\Models\RecruiterModel::class);
            $recruiter = $recruiter->where('user_id',$this->id)->with(['country_details.states','state_details.cities','city_details'])->first();
            $arr_recruiter = [];
            if($recruiter)
            {
                $arr_recruiter = $recruiter->toArray();
            }
            return $arr_recruiter;
        }
    }
 
}