<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoriesTranslationModel extends Model
{
    protected $table='categories_translation';
   
    public $timestamps = false;
    protected $fillable = ['categories_id','category_title','locale'];
}
