<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurrencyJobPostBudgetModel extends Model
{
    protected $table = 'currency_job_post_budget';
    protected $fillable = [
                                'currency_id',
                                'tiny_from',
                                'tiny_to',
                                'small_from',
                                'small_to',
                                'medium_from',
                                'medium_to',
                                'large_from',
                                'large_to',
                                'big_from',
                                'big_to',
                                'jumbo_from',
                                'hourly_price',
     						];

}
