<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailSentModel extends Model
{
    protected $table      = 'email_sent';
    protected $primaryKey = 'id';
    protected $fillable   = ['mail_from','mail_to','subject','message','is_sent','unsent_reasone','created_at','updated_at','unique_id'];
}
