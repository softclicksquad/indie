<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class MilestoneReleaseModel extends Model
{
    protected $table      = "milestone_release";
    protected $primaryKey = "id";

    protected $fillable   = [
                                'project_id',
                                'milestone_id',
                                'client_user_id',
                                'expert_user_id',
                                'invoice_id',
                                'status',
                            ];


    public function milestone_details()
    {
        return $this->belongsTo('App\Models\MilestonesModel','milestone_id','id');
    }                        
    
    public function project_details()
    {
        return $this->belongsTo('App\Models\ProjectpostModel','project_id','id');
    }

    public function transaction_details()
    {
        return $this->hasOne('App\Models\TransactionsModel','invoice_id','invoice_id');
    }

    public function client_details()
    {
        return $this->hasOne('App\Models\UserModel','id','client_user_id');
    }

    public function expert_details()
    {
        return $this->hasOne('App\Models\UserModel','id','expert_user_id');
    }

}
