<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ProjectAttchmentModel extends Model
{
    protected $table 		= 'project_attachment';
    protected $primaryKey 	= 'id';
    protected $fillable 	= ['project_id','expert_user_id','attachment','attchment_no','description','is_own_work'];
    
    public function project_details(){
        return $this->belongsTo('App\Models\ProjectpostModel','project_id','id');
    }   

}
