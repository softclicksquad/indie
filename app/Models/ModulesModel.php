<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModulesModel extends Model
{
    protected $table = "modules";
    protected $fillable = ['title',
    					   'slug',
    					   'is_active'];
}
