<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SealedEntriesTransactionsModel extends Model
{
    protected $table      = 'sealed_entries_transactions';
    protected $primaryKey = 'id';

    protected $fillable   = [
    							'user_id',
    							'sealed_entry_rates_id',
    							'invoice_id',
                                'currency',
                                'currency_code',
                                'transaction_type',
    							'payment_txn_id',
    							'payment_amount',
    							'actual_amount',
    							'sealed_entries_quantity',
    							'payment_method',
    							'payment_status',
    							'payment_date',
                                'wallet_transaction_id',
    							'response_data',
    						];
}
