<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceCategoriesTranslationModel extends Model
{
    protected $table='service_categories_translation';
   
    public $timestamps = false;
    protected $fillable = ['id','service_categories_id','category_title','meta_tag','description','locale'];
}
