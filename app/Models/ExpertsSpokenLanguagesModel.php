<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExpertsSpokenLanguagesModel extends Model
{
    protected $table 		= 'experts_spoken_languages';
    protected $primaryKey 	= 'id';
    protected $fillable 	= ['expert_user_id','language'];
    public function expert_spoken_language()
    {
        return $this->hasMany('App\Models\ExpertsModel','user_id','expert_user_id');
    }
} // class 
