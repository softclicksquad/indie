<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationsModel extends Model
{
    protected $table = "notifications";
     protected $primaryKey = 'id';
    protected $fillable = ['user_id','user_type','project_id','notification_text_en','notification_text_de','url','is_seen'];

}
