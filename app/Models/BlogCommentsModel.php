<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogCommentsModel extends Model
{
	protected $table    = 'blog_comments';
	protected $fillable = [
		'blog_id',
		'comment_by',
		'user_name',
		'user_email',
		'comment'
	];

	public function user()
	{
		return $this->belongsTo('App\Models\UserModel', 'comment_by','id');
	}
}
