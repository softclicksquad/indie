<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectPriorityPackages extends Model
{
    protected $table       = 'project_priority_packages';
    protected $primaryKey  = 'id';
    protected $fillable    = ['highlited_days',
    						  'USD_prize',
    						  'EUR_prize',
    						  'GBP_prize',
    						  'PLN_prize',
    						  'CHF_prize',
    						  'NOK_prize',
    						  'SEK_prize',
    						  'DKK_prize',
    						  'CAD_prize',
    						  'ZAR_prize',
    						  'AUD_prize',
    						  'HKD_prize',
    						  'CZK_prize',
    						  'JPY_prize',
    						  'status',
    						  'created_at',
    						  'updated_at'];
}
