<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentSettingsAdminModel extends Model
{
    protected $table      = "payment_settings_admin";
    protected $primaryKey = "id";

    protected $fillable   = [
    							'admin_user_id',
    							'payment_details',
    						];
}
