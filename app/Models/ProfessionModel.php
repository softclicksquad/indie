<?php

namespace App\Models;

use \Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class ProfessionModel extends Model
{
    use Translatable;
    protected $table = 'profession';

    /* Translatable Config */
    public $translationModel 	  = 'App\Models\ProfessionTranslationModel';
    public $translationForeignKey = 'profession_id';
    public $translatedAttributes  = ['profession_title'];
    protected $fillable 		  = ['profession_slug','is_active','profession_image'];

    public function profession_traslation()
    {
        return $this->hasMany('App\Models\ProfessionTranslationModel','profession_id','id');
    }

    public function profession_search()
    {
        $locale =  \App::getLocale();    
        return $this->hasMany('App\Models\ProfessionTranslationModel','profession_id','id')->where('locale','=',$locale);
    }

    public function expert_profession()
    {
        return $this->hasMany('App\Models\ExpertsModel','profession','id');
    }
    
    protected static function boot() {
        parent::boot();

        static::deleting(function($obj) 
        { 
             $obj->profession_traslation()->delete();
        });
    }
}
