<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectPostDocumentsModel extends Model
{
    protected $table      = 'project_post_documents';
   protected $fillable    =	[ 'project_id',
   							  'image_name',
   							  'image_original_name'
   							];	
}
