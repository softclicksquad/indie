<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;


class SupportCategoryModel extends Model
{
	use SoftDeletes;

    protected $table = 'support_category';

    protected $fillable = ['title'];

    public function get_tickets()
    {
    	return $this->hasMany('App\Models\SupportTicketModel','cat_id', 'id');
    }
}