<?php

namespace App\Models;

use \Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class SkillsModel extends Model
{
    use Translatable;
    protected $table = 'skills';

    /*Translatable Config*/
    public $translationModel 	  = 'App\Models\SkillsTranslationModel';
    public $translationForeignKey = 'skill_id';
    public $translatedAttributes  = ['skill_name'];
    protected $fillable 		  = ['skill_slug','is_active'];

    public function skill_traslation()
    {
        return $this->hasMany('App\Models\SkillsTranslationModel','skill_id','id');
    }

     /* used for search */
    public function skill_search()
    {
        $locale =  \App::getLocale();    
        return $this->hasMany('App\Models\SkillsTranslationModel','skill_id','id')->where('locale','=',$locale);
    }
    
    
    protected static function boot() 
    {
        parent::boot();

        static::deleting(function($obj) 
        { 
             $obj->skill_traslation()->delete();
        });
    }
}
