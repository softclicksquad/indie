<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentsMasterModel extends Model
{
    protected $table      =   'document_master';
   protected $fillable    =	[ 'user_type',
   							  'country',
   							  'translation',
   							  'id_proof',
   							  'registration_proof',
   							  'articles_of_association',
   							  'shareholder_declaration'
   							];	
}
