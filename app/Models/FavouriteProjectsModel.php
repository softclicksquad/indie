<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FavouriteProjectsModel extends Model
{
    protected $table    = "favourite_projects";

    protected $fillable = ['expert_user_id','project_id'];

    /* Relation with post project details */

    public function project_details()
    {
        return $this->belongsTo('App\Models\ProjectpostModel','project_id','id');
    }

}
