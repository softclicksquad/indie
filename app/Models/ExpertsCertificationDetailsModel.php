<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExpertsCertificationDetailsModel extends Model
{
    protected $table = 'experts_certification_details';

    protected $fillable = ['expert_user_id',
						   'certification_name',
						   'completion_year',
						   'expire_year'
						  ];
}
