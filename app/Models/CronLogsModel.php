<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CronLogsModel extends Model
{
    protected $table 		= 'cron_logs';
    protected $primaryKey 	= 'id';
    protected $fillable 	= [ 'corn_type',
    							'user_id',
    							'subscription_users_id',
    							'title',
    							'description'
    						  ];
}
