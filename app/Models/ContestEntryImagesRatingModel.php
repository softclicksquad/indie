<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContestEntryImagesRatingModel extends Model
{
    protected $table      = 'contest_entry_file_rating';
    protected $primaryKey = 'id';
    protected $fillable   = ['image_id','contest_id','contest_entry_id','comment_user_id','file_no','rating','created_at','updated_at'];

    public function rating_user()
    {
        return $this->hasMany('App\Models\UserModel','id','comment_user_id');
    }
}
