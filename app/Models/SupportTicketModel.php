<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupportTicketModel extends Model
{
    protected $table = 'support_ticket';

    protected $fillable = [ 'user_id',
                            'ticket_number',
                            'title',
    						'cat_id',
                            'subject',
    						'description',
                            'thread_id',
    						'priority',
                            'status',
                            'last_reply_by'
               			];

    public function get_user_details()
    {
        return $this->hasOne('App\Models\UserModel','id','user_id');
    }

    public function get_category()
    {
    	return $this->hasOne('App\Models\SupportCategoryModel','id','cat_id');
    }

    public function get_files()
    {
    	return $this->hasMany('App\Models\SupportTicketFilesModel','support_ticket_id','id');
    }

    public function get_admin_details()
    {
        return $this->hasOne('App\Models\UserModel','id','admin_id');
    }

    public function get_admin_profile_details()
    {
        return $this->hasOne('App\Models\AdminProfileModel','user_id','admin_id');
    }
}
