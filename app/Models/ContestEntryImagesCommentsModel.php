<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContestEntryImagesCommentsModel extends Model
{
    protected $table      = 'contest_comment';
    protected $primaryKey = 'id';
    protected $fillable   = ['contest_id', 'contest_entry_id','comment_user_id','file_no','parent_id','comment','created_at','updated_at'];

    public function comment_user()
    {
        return $this->hasMany('App\Models\UserModel','id','comment_user_id');
    }
}
