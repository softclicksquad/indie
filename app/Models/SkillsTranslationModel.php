<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SkillsTranslationModel extends Model
{
     protected $table='skills_translation';
   
    public $timestamps = false;
    protected $fillable = ['skill_id','skill_name','locale'];
}
