<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DepositeCurrencyModel extends Model
{
    protected $table = 'deposite_currency';
    protected $fillable = [
     							'currency_id',
     							'min_amount',
     							'max_amount',
     							'min_amount_charge',
     							'max_amount_charge'
     						];



    public function currency()
    {
        return $this->hasOne('App\Models\CurrencyModel','id','currency_id');
    }
}
