<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

use Sentinel;

class ReviewsModel extends Model
{
    protected $table      = "reviews";
    protected $primaryKey = "id";

    protected $fillable   = [
                                'to_user_id',
                                'from_user_id',
                                'project_id',
                                'review',
                                'rating',

                            ];

  

    public function user_details()
    {
        return $this->belongsTo('App\Models\UserModel','from_user_id','id');
    }                          

    //Relation with expert details with Expert informanation
    public function client_details()
    {
        return $this->belongsTo('App\Models\ClientsModel','from_user_id','user_id');
    }

    public function expert_details()
    {
        return $this->belongsTo('App\Models\ExpertsModel','from_user_id','user_id');
    }                           

     public function project_details()
    {
        return $this->belongsTo('App\Models\ProjectpostModel','project_id','id');
    }
    
    /*-------------------------------------
    Auther : Nayan S.
    --------------------------------------*/                            
    
    public $appends = ['from_user_info','to_user_info'];  

    public function getFromUserInfoAttribute()
    {
        $result = $this->get_users_info($this->from_user_id);
        return $result;
    }

    public function getToUserInfoAttribute()
    {
        $result = $this->get_users_info($this->to_user_id);
        return $result;
    }

    public function get_users_info($user_id)
    {   
        $user_role = $this->get_user_role($user_id);

        if( $user_role == "client" )
        {
            $model = app(\App\Models\ClientsModel::class);
        } 
        else if( $user_role == "expert" )
        {
            $model = app(\App\Models\ExpertsModel::class);
        } 
        else if( $user_role == "project_manager" )
        {
            $model = app(\App\Models\ProjectManagerModel::class);
        }

        $obj_user = $model->where('user_id','=',$user_id)->first();
       
        $arr_user = [];

        if($obj_user)
        {
            $arr_user = $obj_user->toArray();
        }
        return $arr_user;
    }

    public function get_user_role($user_id) 
    {
      $obj_role  = Sentinel::findById($user_id)->roles()->first();
      
      $role_slug = "";

      if($obj_role)
      {
        $role_slug = $obj_role->slug; 
      }

      return $role_slug;
    }   

    /*------- Ends -----------*/
         
}
