<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfessionTranslationModel extends Model
{
    protected $table='profession_translation';
   
    public $timestamps = false;
    protected $fillable = ['profession_id','profession_title','locale'];
}
