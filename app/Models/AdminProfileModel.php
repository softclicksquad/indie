<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminProfileModel extends Model
{
    protected $table      = "admin_profile";
    protected $primaryKey = "id";

    protected $fillable   = [	
    							'id',
    							'user_id',
    							'name',
    							'contact_email',
    							'contact_number',
    							'fax',
    							'address',
    							'image',
                                'file_attachment',
    						];

    public function user_details()
    {
        return $this->BelongsTo('App\Models\UserModel','user_id','id');
    }
}
