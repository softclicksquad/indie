<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContestSkillsModel extends Model
{
    protected $table      = "contest_skills";
    protected $primaryKey = "id";
    protected $fillable   = [	
                                'contest_id',
    							'skill_id',
    						];
	/* To get all project skills */
    public function skill_data(){
        return $this->belongsTo('App\Models\SkillsModel','skill_id','id');
    } 	    		
    				
} // end class