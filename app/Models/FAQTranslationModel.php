<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class FAQTranslationModel extends Model
{
	
    protected $table='faq_translation';
   
    public $timestamps = false;
    protected $fillable = ['id','question_name','question_ans','question_id','locale'];

}
