<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class StaticPageTranslationModel extends Model
{
	
    protected $table='static_pages_translation';
   
    public $timestamps = false;
    protected $fillable = ['id','page_name','page_title','page_desc','static_page_id','locale','meta_keyword','meta_title','meta_desc'];

}
