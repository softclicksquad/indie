<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientsModel extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table      = "clients";
    protected $primaryKey = "id";
    protected $fillable   = [	'user_id',
                                'user_type',
    							'first_name',
    							'last_name',
                                'phone_code',
    							'phone_number',
    							'country',
    							'state',
    							'city',
    							'zip',
    							'address',
    							'company_name',
    							'owner_name',
    							'company_size',
                                'occupation',
    							'founding_year',
    							'vat_number',
                                'spoken_languages',
                                'show_profile_image_name',
                                'timezone',
    							'profile_image',
                                'website',
                                'profile_summary'
    						];
    public function user_details()
    {
        return $this->belongsTo('App\Models\UserModel','user_id','id');
    }
    public function country_details()
    {
        return $this->belongsTo('App\Models\CountryModel','country','id');
    }
    public function state_details()
    {
        return $this->belongsTo('App\Models\StateModel','state','id');
    }
    public function city_details()
    {
        return $this->belongsTo('App\Models\CityModel','city','id');
    }
    public function review_details()
    {
        return $this->hasMany('App\Models\ReviewsModel','to_user_id','user_id');
    }
    public function project_details()
    {
       return $this->hasMany('App\Models\ProjectpostModel','client_user_id','user_id');
    }

    
}
