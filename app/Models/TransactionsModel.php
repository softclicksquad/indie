<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionsModel extends Model
{
    protected $table      = "transactions";
    protected $primaryKey = "id";

    protected $fillable   = [
    							'user_id',
    							'invoice_id',
                                'currency',
                                'currency_code',
                                'transaction_type',
    							'payment_txn_id',
    							'paymen_amount',
    							'payment_method',
    							'payment_status',
    							'payment_date',
                                'WalletTransactionId',
    							'response_data',
    						];

    public $appends = ['role_info'];                        

    public function user_details()
    {
        return $this->belongsTo('App\Models\UserModel','user_id','id');
    }                          

    public function getRoleInfoAttribute()
    {
        $user = $this->user_details;
        
        if($user->inRole('expert'))
        {
            $expert = app(\App\Models\ExpertsModel::class);
            $expert = $expert->where('user_id',$user->id)->first();
            $arr_expert = [];
            if($expert)
            {
                $arr_expert = $expert->toArray();
            }
            return $arr_expert;
        }
        elseif($this->user_details->inRole('client'))
        {
            $client = app(\App\Models\ClientsModel::class);
            $client = $client->where('user_id',$user->id)->first();
            $arr_client = [];
            if($client)
            {
                $arr_client = $client->toArray();
            }
            return $arr_client;

        }
        elseif($this->user_details->inRole('admin'))
        {
            $admin = app(\App\Models\AdminProfileModel::class);
            $admin = $admin->where('user_id',$user->id)->first();
            $arr_admin = [];
            if($admin)
            {
                $arr_admin = $admin->toArray();
            }
            return $arr_admin;

        }
    }
}
