<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectSkillsModel extends Model
{
    protected $table      = "project_skills";
    protected $primaryKey = "id";
    protected $fillable   = [	'project_id',
    							'skill_id',
    						];

	/* To get all project skills */
    
    public function skill_data()
    {
        return $this->belongsTo('App\Models\SkillsModel','skill_id','id');
    } 	    						

   

}