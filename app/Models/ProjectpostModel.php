<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\SkillsModel;
use App\Models\UserModel;
use App\Models\CategoriesModel;
use App\Models\ClientsModel;
use App\Models\ProjectsBidsModel;

use Sentinel;

class ProjectpostModel extends Model
{
   use SoftDeletes;
   protected $dates      = ['deleted_at'];
   protected $table      = 'projects';
   protected $fillable   = [    'category_id',
                                'sub_category_id',
                                'client_user_id',
                                'expert_user_id',
                                'project_name',
                                'project_attachment',
                                'project_description',
                                'project_additional_info',
                                'project_start_date',
                                'project_end_date',
                                'bid_closing_date',
                                'project_expected_duration',
                                'project_currency',
                                'project_currency_code',
                                'project_cost',
                                'min_project_cost',
                                'max_project_cost',
                                'price_method',
                                'project_name',
                                'project_handle_by',
                                'invoice_id',
                                'project_skills',
                                'project_hourly_rate',
                                'project_fixed_price',
                                'project_status',
                                'project_type',
                                'is_nda',
                                'is_urgent',
                                'urjent_heighlight_end_date',
                                'highlight_days',
                                'highlight_start_date',
                                'highlight_end_date',
                                'project_pricing_method',
                                'has_completion_request',
                                'is_hire_process',
                                'mp_job_wallet_id',
                                'award_request_date_time',
                                'project_handle_by_pm',
                                'recruiter_assign_date',
                                'is_custom_price',
                                'paid_services',
                                'is_highlight_edited',
                                'usd_min_project_cost',
                                'usd_max_project_cost',
                                'edited_at',
                                'hour_per_week',
                                'is_custom_hour_per_week'
                            ];

    public $appends = ['is_bid_exists','bid_count','is_project_rejected_by_expert'];
    

    /* Relation with post project details with skill informanation*/
    public function skill_details()
    {
        return $this->belongsTo('App\Models\SkillsModel','project_skills','id');
    }
    
    /* Relation with post projects details with client informanation*/
    public function client_details()
    {
        return $this->belongsTo('App\Models\UserModel','client_user_id','id');
    }

    /* To get all project skills */
    public function project_skills()
    {
        return $this->hasMany('App\Models\ProjectSkillsModel','project_id','id');
    }   
    
    /* Relation with post projects details with Expert informanation*/
    public function expert_details()
    {
        return $this->belongsTo('App\Models\UserModel','expert_user_id','id');
    }

    /* Relation with post projects details with expert informanation*/
    public function expert_info()
    {
        return $this->belongsTo('App\Models\ExpertsModel','expert_user_id','user_id');
    }

    /* Relation with post projects details with Expert informanation*/
    public function project_requests()
    {
        return $this->hasMany('App\Models\ProjectRequestsModel','project_id','id');
    }

    /* Relation with post projects details with client informanation*/
    public function client_info()
    {
        return $this->belongsTo('App\Models\ClientsModel','client_user_id','user_id');

    }
    /* Relation with post project details with category informanation*/
    public function category_details()
    {
        return $this->belongsTo('App\Models\CategoriesModel','category_id','id');
    }
    public function sub_category_details()
    {
        return $this->belongsTo('App\Models\SubCategoriesModel','sub_category_id','id');
    }
    /* Relation with  project manager details to post project informanation*/
    public function project_manager_info()
    {
        return $this->belongsTo('App\Models\ProjectManagerModel','project_manager_user_id','user_id');
    }
    public function project_recruiter_info()
    {
        return $this->belongsTo('App\Models\RecruiterModel','project_recruiter_user_id','user_id');
    }

    /* Relation with post projects details with Project manager informanation*/
    public function project_manager_details()
    {
        return $this->belongsTo('App\Models\UserModel','project_manager_user_id','id');
    }
    public function project_recruiter_details()
    {
        return $this->belongsTo('App\Models\UserModel','project_recruiter_user_id','id');
    }

    public function project_bid_info()
    {
        return $this->hasOne('App\Models\ProjectsBidsModel','project_id','id');
    }

    public function project_bids_infos()
    {
        return $this->hasMany('App\Models\ProjectsBidsModel','project_id','id');
    }
    
    public function project_bid_count()
    {
        return $this->hasMany('App\Models\ProjectsBidsModel','project_id','id')->selectRaw('project_id, count(*) as count')->groupBy('project_id')->orderBy('count','DESC');
    }

    public function project_attachment()
    {
        return $this->hasMany('App\Models\ProjectAttchmentModel','project_id','id');
    }
    public function experts_project_attachment()
    {
        return $this->hasMany('App\Models\ProjectAttchmentModel','project_id','id');
    }

    public function project_post_documents()
    {
        return $this->hasMany('App\Models\ProjectPostDocumentsModel','project_id','id');
    }

    public function milestone_refund_details()
    {
        return $this->hasOne('App\Models\MilestoneRefundRequests','project_id','id');
    }
    public function getBidCountAttribute()
    {
        $count_project_bid = 0;

        $obj_project_bid = app(\App\Models\ProjectsBidsModel::class);

        if($this->project_handle_by == '1')
        {
            $count_project_bid = $obj_project_bid->where('project_id',$this->id)->count();
        } 
        else if($this->project_handle_by == '2')
        {
            $count_project_bid = $obj_project_bid->where('project_id',$this->id)->where('is_suggested','1')->count();
        }
        return $count_project_bid;        
    }
    /* Function to get atribute of bid exists or not */
    public function  getIsBidExistsAttribute()
    {
        $user = Sentinel::check();
        if( $user != FALSE && $user->inRole('expert'))
        {   
            $obj_project_bid   = app(\App\Models\ProjectsBidsModel::class);
            $count_project_bid = $obj_project_bid->where('expert_user_id',$user->id)
                                                 ->where('project_id',$this->id)     
                                                 ->count();
            if($count_project_bid >= 1){
                return '1';
            }    
        }
        return '0';        
    }
    public function  getIsProjectRejectedByExpertAttribute()
    {
        $user = Sentinel::check();
        if( $user != FALSE && $user->inRole('expert'))
        {   
            $obj_project_bid   = app(\App\Models\ProjectsBidsModel::class);
            $count_project_bid = $obj_project_bid->where('expert_user_id',$user->id)
                                                 ->where('project_id',$this->id)
                                                 ->where('bid_status','3')     
                                                 ->count();
            if($count_project_bid >= 1)
            {
                return '1';
            }    
        }
        return '0';        
    }

    public function review_details()
    {
        return $this->hasMany('App\Models\ReviewsModel','project_id','id')->where('to_user_id',$this->client_user_id);
    }
    /* for getting project which has milestones */
    public function project_milestones()
    {
        return $this->hasMany('App\Models\MilestonesModel','project_id','id');               
    }
    /* Shows project review use at admin side */
    public function project_reviews()
    {
        return $this->hasMany('App\Models\ReviewsModel','project_id','id');    
    }
    /* Belong to favourite project */
    public function favourite_project()
    {
        return $this->hasMany('App\Models\FavouriteProjectsModel','project_id','id');    
    }
    public function transaction_details()
    {
        return $this->hasOne('App\Models\TransactionsModel','invoice_id','invoice_id');
    }
    public function dispute()
    {
        return $this->hasOne('App\Models\DisputeModel','project_id','id');
    }
    public function milestone_release_details()
    {
        return $this->hasMany('App\Models\MilestoneReleaseModel','project_id','id');
    }
    public function scopeValidProject($query)
    {
        return $query->whereNotIn('project_status',['0'])
                     ->whereHas('transaction_details',function ($transaction)
                {
                    $transaction->whereIn('payment_status',['1','2']);
                });
    }
}
