<?php

namespace App\Models;

use \Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class CategoriesModel extends Model
{
    use Translatable;
    protected $table = 'categories';

    /* Translatable Config */
    public $translationModel      = 'App\Models\CategoriesTranslationModel';
    public $translationForeignKey = 'categories_id';
    public $translatedAttributes  = ['category_title'];
    protected $fillable           = ['category_slug','is_active','category_image'];

    public function category_traslation()
    {
        return $this->hasMany('App\Models\CategoriesTranslationModel','categories_id','id');
    }

    public function expert_categories()
    {
        return $this->hasMany('App\Models\ExpertsCategoriesModel','category_id','id');
    }

    public function category_search()
    {
        $locale =  \App::getLocale();    
        return $this->hasMany('App\Models\CategoriesTranslationModel','categories_id','id')->where('locale','=',$locale);
    }

    public function is_sub_category_exist()
    {
        return $this->hasMany('App\Models\SubCategoriesModel','category_id','id');
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($obj) 
        { 
             $obj->category_traslation()->delete();
        });
    }
}
