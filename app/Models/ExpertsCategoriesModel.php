<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExpertsCategoriesModel extends Model
{
    protected $table      = "experts_categories";
    protected $primaryKey = "id";
    protected $fillable   = [
    							'expert_user_id',
    							'category_id',
    						];

	public function categories()
	{
		return $this->hasOne('App\Models\CategoriesModel','id','category_id');
	}    		
    /*public function category_traslation()
    {
        return $this->hasOne('App\Models\CategoriesTranslationModel','id','category_id');
    }		*/		
    public function experts_categories_subcategories_details()
    {
        return $this->hasMany('App\Models\ExpertCategorySubCategoryModel','experts_categories_id','id');
    }
    
    public function is_sub_category_exist()
    {
        return $this->hasMany('App\Models\SubCategoriesModel','category_id','category_id');
    }                           
}
