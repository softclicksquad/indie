<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogViewModel extends Model
{
    protected $table    = 'blog_view';
    protected $fillable = ['blog_id', 'unique_id'];
}
