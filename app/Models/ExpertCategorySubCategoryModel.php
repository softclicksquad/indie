<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExpertCategorySubCategoryModel extends Model
{
    protected $table      = "experts_categories_subcategories";
    protected $primaryKey = "id";
    protected $fillable   = [
                             'expert_user_id',
    						 'experts_categories_id',
    						 'subcategory_id'
    						];

    public function subcategory_traslation()
    {
        return $this->hasMany('App\Models\SubCategoriesTranslationModel','subcategories_id','subcategory_id');
    }
    						
}
