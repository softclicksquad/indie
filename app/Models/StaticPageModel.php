<?php

namespace App\Models;

use \Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model as Eloquent;


class StaticPageModel extends Eloquent
{
	
	use Translatable;
    protected $table = 'static_pages';

       /* Translatable Config */
    public $translationModel 	  = 'App\Models\StaticPageTranslationModel';
    public $translationForeignKey = 'static_page_id';
    public $translatedAttributes  = ['page_name','page_title','page_desc','meta_title','meta_desc','locale','meta_keyword'];
    protected $fillable 		  = ['page_slug','is_active'];

    public function page_traslation()
    {
        return $this->hasMany('App\Models\StaticPageTranslationModel','static_page_id','id');
    }

    
    protected static function boot() {
        parent::boot();

        static::deleting(function($page) 
        { 
             $page->page_traslation()->delete();
        });
    }

}