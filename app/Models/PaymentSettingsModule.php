<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentSettingsModule extends Model
{
 	protected $table      = "payment_settings";
    protected $primaryKey = "id";

    protected $fillable   = [
    							'user_id',
    							'payment_details',
    						];
}
