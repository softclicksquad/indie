<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogsModel extends Model
{
    use SoftDeletes;

    protected $table    = 'blogs';
    protected $fillable = ['category_id','name','slug','image','description','tags','created_by','updated_by','is_active'];
    public $appends     = ['view_count','comment_count'];

    public function category()
	{
		return $this->belongsTo('App\Models\BlogCategoriesModel','category_id','id');
	}

	public function comments()
	{
		return $this->hasMany('App\Models\BlogCommentsModel','blog_id','id');
	}

	public function getViewCountAttribute()
    {
        $count = 0;
        $count = app(\App\Models\BlogViewModel::class);
        $count = $count->where('blog_id','=',$this->id)->count();
                                   
        return $count;
    }

    public function getCommentCountAttribute()
    {
        $count = 0;
        $count = app(\App\Models\BlogCommentsModel::class);
        $count = $count->where('blog_id','=',$this->id)->count();
                                   
        return $count;
    }


}
