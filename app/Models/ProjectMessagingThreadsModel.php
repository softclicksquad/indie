<?php

namespace App\Models;

/*use Cmgmyr\Messenger\Models\Message as MessangerMessageModel;*/

use Illuminate\Database\Eloquent\Model;

class ProjectMessagingThreadsModel extends Model
{
    protected $table      = "project_messaging_threads";

    protected $primaryKey = "id";
    
    protected $fillable   = [	'project_id',
    							'client-expert',
                                'client-project_manager',
                                'project_manager-expert',
                                'client_user_id',
                                'expert_user_id',
                                'project_manager_user_id',
                                'message_thread_id'
                        	];

  public function messages()
  {
    return $this->hasMany('Cmgmyr\Messenger\Models\Message','thread_id','message_thread_id');
  }                             

  public function chat_participants()
  {
    return $this->hasMany('Cmgmyr\Messenger\Models\Participant','thread_id','message_thread_id');
  }

  public function project_data()
  {
    return $this->belongsTo('App\Models\ProjectpostModel','project_id','id');
  }
}