<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewslettersModel extends Model
{
    protected $table      = "newsletters";
    protected $primaryKey = "id";

    protected $fillable   = [
    							'title',
    							'subject',
    							'news_message',
    							'is_active',
    						];
}
