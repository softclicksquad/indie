<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogCategoriesModel extends Model
{
    protected $table    = 'blog_categories';
	protected $fillable = ['name','icon','slug','created_by','updated_by', 'is_active','created_at','updated_at'];
}
