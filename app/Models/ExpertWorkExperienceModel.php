<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExpertWorkExperienceModel extends Model
{
    protected $table = "experts_work_experience";

    protected $fillable = ['expert_user_id',
						   'designation',
						   'company_name',
						   'from_year',
						   'to_year'];
}
