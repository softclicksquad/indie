<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContestEntryModel extends Model
{
    protected $table      = 'contests_entry';
    protected $primaryKey = 'id';
    protected $fillable   = [
                            'entry_number',
                            'entry_id',
                            'contest_id',
                            'client_user_id', 
                            'expert_id',
                            'is_own_work',
                            'title',
                            'description',
                            'sealed_entry',
                            'is_winner',
                            'sealed_entry_invoice_id',
                            'is_chat_initiated',
                            'is_admin_chat_initiated'
                         ];
    public function contest_details()
    {
        return $this->hasOne('App\Models\ContestModel','id','contest_id')->with(['skill_details','contest_skills.skill_data']);
    }
    public function contest_entry_files()
    {
        return $this->hasMany('App\Models\ContestEntryImagesModel','contest_entry_id','id')/*->where('is_admin_deleted','0')*/->orderBy('id','DESC');
    }
    public function contest_entry_files_comments()
    {
        return $this->hasMany('App\Models\ContestEntryImagesCommentsModel','contest_entry_id','id')->orderBy('file_no')->with('comment_user');
    }
    public function contest_entry_files_rating()
    {
        return $this->hasMany('App\Models\ContestEntryImagesRatingModel','contest_entry_id','id')->orderBy('file_no')->with('rating_user');
    }
    public function contest_entry_files_highest_rating()
    {
        return $this->hasMany('App\Models\ContestEntryImagesRatingModel','contest_entry_id','id')->orderBy('rating','DESC');
    }
    public function contest_entry_original_files()
    {
        return $this->hasMany('App\Models\ContestEntryOriginalFilesModel','contest_entry_id','id')->orderBy('id','DESC');;
    }
    public function client_user_details(){
        return $this->hasOne('App\Models\ClientsModel','user_id','client_user_id')->with(['user_details']);
    }
    public function expert_details(){
        return $this->hasOne('App\Models\ExpertsModel','user_id','expert_id')->with(['user_details']);
    }
} // end class
