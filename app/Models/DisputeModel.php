<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DisputeModel extends Model
{
	use SoftDeletes;
    
    protected $table 	  = "dispute";

    protected $primaryKey = 'id';

    protected $fillable  = [
    							'project_id',
    							'client_user_id',
    							'expert_user_id',
    							'added_by',
    							'title',
								'description',
                                'status',
                                'admin_comments'
    						];

    public function experts_details()
    {   
        return $this->belongsTo('App\Models\ExpertsModel','expert_user_id','user_id');
    }

    public function clients_details()
    {
        return $this->belongsTo('App\Models\ClientsModel','client_user_id','user_id');   
    }
    
    public function project_details()
    {
        return $this->belongsTo('App\Models\ProjectpostModel','project_id','id');   
    }

    /*
        Commets : Function to get details who added dispute.
        Auther  : Nayan S.
    */                        

    protected $appends = ['user_who_added_dispute'];

    public function getUserWhoAddedDisputeAttribute()
    {
        if($this->added_by  == '1')
        {
            $model = app(\App\Models\ClientsModel::class);
            $user_id = $this->client_user_id;
        } 
        else if($this->added_by  == '2')
        {   
            $model = app(\App\Models\ExpertsModel::class);
            $user_id = $this->expert_user_id;
        }
        
        $arr_user = [];

        if($model)
        {
            $obj_user = $model->where('user_id','=',$user_id)->with('user_details')->first();
            
            if($obj_user)
            {
                $arr_user = $obj_user->toArray();
            }
        }

        return $arr_user;
    }  

    /* Ends */                      

}
