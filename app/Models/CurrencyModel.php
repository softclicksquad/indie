<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurrencyModel extends Model
{
    protected $table = 'currency';
    protected $fillable = [
     							'currency',
     							'currency_code',
                                'description',
     							'is_active'
     						];


    public function deposite()
    {
    	return $this->hasOne('App\Models\DepositeCurrencyModel','currency_id','id');
    }

    public function job_post_budget()
    {
        return $this->hasOne('App\Models\CurrencyJobPostBudgetModel','currency_id','id');
    }
}
