<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriptionUsersModel extends Model
{
    protected $table      = "subscription_users";
    protected $primaryKey = "id";

    protected $fillable   = [
    							'user_id',
    							'subscription_pack_id',
                                'subscription_pack_currency',
    							'invoice_id',
    							'expiry_at',
    							'pack_name',
    							'pack_price',
    							'validity',
    							'number_of_bids',
    							'topup_bid_price',
    							'number_of_categories',
                                'number_of_subcategories',
    							'number_of_skills',
    							'number_of_favorites_projects',
                                'website_commision',
    							'allow_email',
                                'is_downgrade_request',
                                'downgrade_pack_id',
    							'allow_chat',
                                'is_active'
    						];

    public function transaction_details()
    {
        return $this->hasOne('App\Models\TransactionsModel','invoice_id','invoice_id');
    }
    public function user_details()
    {
        return $this->belongsTo('App\Models\UserModel','user_id','id');
    }
    public function subscription_pack_details()
    {
        return $this->belongsTo('App\Models\SubscriptionPacksModel','subscription_pack_id','id');
    }

}
