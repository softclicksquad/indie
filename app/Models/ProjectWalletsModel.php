<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ProjectWalletsModel extends Model
{
    protected $table = 'projects_wallets';

    public $PrimaryKey  = 'id';
    protected $fillable = ['id','project_id','client_user_id','expert_user_id','project_mp_wallet_id','created_at','updated_at'];
}
