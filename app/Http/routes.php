<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::any('/store_currency_conversion', function () {
	\Artisan::call('store_currency_conversion:schedule');
});

Route::get('expire_highlightproject', function () {
	//\Artisan::call('optimize');
	\Artisan::call('expire:highlightproject');
});

Route::get('expire_contest', function () {
	\Artisan::call('expire:contest');
});

Route::get('winner_choose', function () {
	\Artisan::call('winner:choose');
});

Route::get('expire_subscription', function () {
	\Artisan::call('expire:subscription');
});

Route::get('expire_subscription_notification', function () {
	\Artisan::call('expirysubscription:notification');
});

Route::get('refund_contest_amount', function () {
	\Artisan::call('refund:contest_amount');
});

Route::get('cache-clear', function () {
	//\Artisan::call('optimize');
	\Artisan::call('config:clear');
	\Artisan::call('config:cache');
	\Artisan::call('cache:clear');
	\Artisan::call('route:clear');
	\Artisan::call('view:clear');
	//\Artisan::call('logs:clear');
	exec('composer dump-autoload');
	\Cache::flush();
	\Artisan::call('clear-compiled');
	dd("Cache cleared!");
});

Route::group(array('prefix' => 'blog'), function () 
{
	$module_controller = "Front\BlogController@";
	Route::get('/',['as'=>'index', 'uses'=> $module_controller.'index']);
	Route::get('/details/{slug}',['as'=> 'index', 'uses'=> $module_controller.'details']);
	Route::post('/submit-comment',['as'=> 'index', 'uses'=> $module_controller.'submit_comment']);
	Route::get('/{tags?}',['as'=> 'index', 'uses'=> $module_controller.'index']);
});


Route::any('test-chat',function(){
	return view('front.test-chat');
});

/* fronts applozic Routes Starts */
Route::group(array('prefix' => '/applozic'), function(){
	Route::any('fallback-emails',['as'=>'fallback_emails','uses'=>'Front\CronController@applozic_fallback_emails']);
});


//Admin section Routes

$admin_path = config('app.project.admin_panel_slug');
/* Set Language */	
Route::get('/set_lang/{locale}','Front\HomeController@set_lang');

/* Admin Routes Group :: Main group all route of admin will in this Group */
Route::group(['prefix' => $admin_path,'middleware' =>'admin'], function () 
{
	/* Admin Auth Routes Starts */
	Route::get('/',['as'=>'admin_auth_login','uses'=>'Admin\AuthController@login']);
	Route::get('/login',['as'=>'admin_auth_login','uses'=>'Admin\AuthController@login']);
	Route::post('process_login',['as'=>'admin_auth_process_login','uses'=>'Admin\AuthController@process_login']);	
	Route::get('change_password',['as'=>'admin_auth_change_password','uses'=>'Admin\AuthController@change_password']);	
	Route::post('update_password',['as'=>'admin_auth_change_password','uses'=>'Admin\AuthController@update_password']);



	Route::post('forgot_password',['as'=>'admin_auth_change_password','uses'=>'Admin\AuthController@forgot_password']);
	Route::get('reset_password/{token}',['as'=>'admin_auth_change_password','uses'=>'Admin\AuthController@reset_password']);
	Route::post('reset_password/{token}/{enc_id}',['as'=>'admin_auth_change_password','uses'=>'Admin\AuthController@update_reset_password']);
	
	/* Dashboard */
	Route::get('/dashboard',['as'=>'admin_auth_dashboard','uses'=>'Admin\DashboardController@index']);
	Route::get('/logout',['as'=>'admin_auth_logout','uses'=>'Admin\AuthController@logout']);

/*-------------------------------Only Admin Payment routes----------------------------------------*/

Route::group(array('prefix'=>'/payment'),function()
{
	Route::any('/paynow','Common\PaymentController@payment_handler');
	Route::any('/charge','Common\PaymentController@charge_handler');
	Route::group(['middleware' =>'front'], function () 
	{
		Route::any('/cancel','Common\PaymentController@transaction_cancel_admin');		
		Route::any('/success','Common\PaymentController@transaction_success_admin');
	});

});

/*----------------------------------End Payment----------------------------------------------*
 	Route::group(array('prefix' => '/sendmail'), function()
	{
		Route::get('/', ['as' => 'send_mail', 'uses' => 'Admin\SendmailController@index']);
		Route::post('/send', ['as' => 'send', 'uses' => 'Admin\SendmailController@send']);
	});

	/*---------------------- Admin Routes Starts-------------------------------*/

	include_once(app_path('Http/Routes/admin.php'));
	
	// for create user by sentilen and asdign role for user 
	// this is temperary remove after all registration and login fuctionality get complited
	Route::group(['prefix' => 'test'], function () 
	{
		Route::get('/create_role0',function()
		{	
			$role = Sentinel::getRoleRepository()->createModel()->create([
		    'name' => 'SubAdmin',
		    'slug' => 'subadmin',
			]);
		});
		Route::get('/assign_role',function()
		{
			$role = Sentinel::findRoleBySlug('subadmin');
			$user = Sentinel::findById(25); //assgning role to perticular user id statically.
			$user->roles()->attach($role);
		});

		Route::get('/create_user',function()
		{
			$credentials = [
			    'email'    => 'sagar.subadmin@webwing.com',
			    'password' => 'vhc@webwing',
			];
			$user = Sentinel::registerAndActivate($credentials);
		});
	});	  // testing route to create user by sentinel end here
});
/* Admin Routes Group end here */

/*-------------------------------Only Front Payment routes----------------------------------------*/
Route::group(array('prefix'=>'/payment'),function()
{
	Route::any('/paynow','Common\PaymentController@payment_handler');
	Route::any('/project_manager_paynow','Common\PaymentController@project_manager_payment_handler');
	Route::any('/charge','Common\PaymentController@charge_handler');
	Route::group(['middleware' =>'front'], function () 
	{
		Route::any('/cancel','Common\PaymentController@transaction_cancel');		
		Route::any('/success','Common\PaymentController@transaction_success');
	});

});

Route::any('/check_profile_filled',['as' => 'check_profile_filled' ,'uses' => 'Front\HomeController@check_profile_filled']);

Route::any('/get_service_charge',['as' => 'get_service_charge' ,'uses' => 'Front\HomeController@get_service_charge']);
Route::any('/get_wallet_balance',['as' => 'get_wallet_balance' ,'uses' => 'Front\HomeController@get_wallet_balance']);
Route::any('/get_minimum_charge',['as' => 'get_minimum_charge' ,'uses' => 'Front\HomeController@get_minimum_charge']);
/*----------------------------------End Payment----------------------------------------------*/
/* Front section routes */
/* Please add front side routes after this */
Route::group(['middleware' =>'front'], function () 
{
	//Home page 
	/*Route::get('/', function () {
	    return view('front.home.welcome');
	});*/

	Route::get('/',['as' => 'vhc_home_page' ,'uses' => 'Front\HomeController@index']);
	Route::get('',['as' => 'vhc_home_page' ,'uses' => 'Front\HomeController@index']);
	Route::get('categories',['categories' => 'vhc_categories' ,'uses' => 'Front\HomeController@more_categories']);
	
	/* front crons controller*/ 
	Route::get('expiry_subscription_notification',['as' => 'front_cron' ,'uses' => 'Front\CronController@expiry_subscription_notification']);
	Route::get('subscription_expire',['as' => 'expire_subscription' ,'uses' => 'Front\CronController@subscription_expire']);

	/* Frount user Auth Routes Starts */
	Route::get('signup',['as'=>'front_auth_login','uses'=>'Front\AuthController@signup']);

	/*route for signup client and expert with same name but differetnt method GET AND POST*/
	Route::get('signup/client',['as'=>'get_signup_client','uses'=>'Front\AuthController@registeration_client']);
	Route::get('signup/expert',['as'=>'get_signup_expert','uses'=>'Front\AuthController@registeration_expert']);
	Route::any('chk_username_availabilty',['as'=>'chk_username_availabilty','uses'=>'Front\AuthController@chk_username_availabilty']);	

	/*route for signup client and expert with same name but differetnt method GET AND POST*/
	Route::post('signup/client',['as'=>'post_signup_client','uses'=>'Front\AuthController@store_client']);
	Route::post('signup/expert',['as'=>'post_signup_expert','uses'=>'Front\AuthController@store_expert']);


	Route::get('confirm_email/{enc_user_id}/{token}',['as'=>'front_auth_signup_expert','uses'=>'Front\AuthController@confirm_email']);
	Route::any('delete_account',['as'=>'front_auth_delete_account','uses'=>'Front\AuthController@delete_account']);

	//Login and logout routes
	Route::get('login',['as'=>'front_auth_login','uses'=>'Front\AuthController@login']);
	Route::post('login',['as'=>'front_auth_process_login','uses'=>'Front\AuthController@validate_login']);
	Route::get('logout',['as'=>'front_auth_logout','uses'=>'Front\AuthController@logout']);


	//change password
	Route::get('change_password',['as'=>'front_auth_change_password','uses'=>'Front\AuthController@change_password']);	
	Route::post('update_password',['as'=>'front_auth_change_password','uses'=>'Front\AuthController@update_password']);

	Route::any('faq/{type?}',['as'=>'faq','uses'=>'Front\FAQController@index']);

	/*Upload cropped image */
	
	Route::post('upload_cropped_image',['as'=>'upload_cropped_image','uses'=>'Front\UploadCroppedImageController@upload_cropped_image']);	

	/* handle email redirection */
	Route::get('redirection',['as'=>'handle_email_redirection','uses'=>'Front\AuthController@handle_email_redirection']);	

	//forgot password

	Route::get('forgot_password',['as'=>'front_auth_change_password','uses'=>'Front\AuthController@show_forgot_password']);

	Route::post('process_forgot_password',['as'=>'front_auth_change_password','uses'=>'Front\AuthController@process_forgot_password']);

	Route::get('validate_reset_password/{enc_id}/{enc_reminder_code}',['as'=>'front_auth_change_password','uses'=>'Front\AuthController@validate_reset_password_link']);

	Route::post('reset_password',['as'=>'front_auth_change_password','uses'=>'Front\AuthController@reset_password']);


	/* Conversations */
	Route::get('conversation/{enc_project_id}/{enc_recipient_id}',	['as'=>'front_conversation','uses'=>'Front\MessagingController@show_conversations']);
	Route::post('message/send',					['as'=>'front_send_message','uses'=>'Front\MessagingController@send_message']);
	Route::get('message/update_last_read',		['as'=>'update_last_read','uses'=>'Front\MessagingController@update_last_read']);
	Route::get('/user_details/{enc_id}',		['as'=>'user_details','uses'=>'Front\MessagingController@user_details']);
	
	// Front home newsletter //signup_newsletter
	Route::post('signup_newsletter',['as'=>'front_newsletter','uses'=>'Front\NewsletterController@signup_newsletter']);

	Route::get('thank-you',['as'=>'thank_you_page','uses'=>'Front\HomeController@thank_you']);
	Route::get('subscription-plans',['as'=>'subscription_plans_page','uses'=>'Front\HomeController@subscription_plans']);

	Route::get('/info/{page_slug}','Front\HomeController@show_static_page');
	Route::get('/contact-us','Front\HomeController@contact_us');
	Route::post('/sendenquiry','Front\HomeController@store_contact_enquiry');
	
	Route::any('/get_wallet_details',['as' => 'get_wallet_details' ,'uses' => 'Front\HomeController@get_wallet_details']);
	
	/* fronts locations Routes Starts */
   	Route::group(array('prefix' => '/locations'), function()
	{
		Route::get('get_states/{enc_country_id}',['as'=>'front_get_states','uses'=>'Front\LocationsController@get_states']);
		Route::get('get_cities/{enc_state_id}',['as'=>'front_get_cities','uses'=>'Front\LocationsController@get_cities']);
	});

	/*---------------------- Client Routes Starts-------------------------------*/
	include_once(app_path('Http/Routes/client.php'));

	/*---------------------- Expert Routes Starts-------------------------------*/
	include_once(app_path('Http/Routes/expert.php'));

	/*---------------------- Project manager Routes-------------------------------*/
	include_once(app_path('Http/Routes/project_manager.php'));

	/*---------------------- Recruiter Routes-------------------------------*/
	include_once(app_path('Http/Routes/recruiter.php'));


	

	/*---------------------- Front ( Browse ) Project Listing -------------------------------*/
	// include_once(app_path('Http/Routes/project_listing.php'));

	/* fronts expert routes group starts */
	Route::group(array('prefix' => '/projects','middleware' =>'front'), function()
	{ 
		Route::get('/',					['as'=>'front_project_listing','uses'=>'Front\ProjectListingController@index']);
		Route::get('/{enc_id}',			['as'=>'front_show_category_wise_projects','uses'=>'Front\ProjectListingController@index']);
		Route::get('/details/{enc_id}',	['as'=>'show_project_details','uses'=>'Front\ProjectListingController@show_project_details']);
		Route::get('/details/make_zip/{enc_id}',	['as'=>'show_project_details/make_zip','uses'=>'Front\ProjectListingController@make_zip']);
		Route::get('/details/download_zip/{enc_id}',	['as'=>'show_project_details/download_zip','uses'=>'Front\ProjectListingController@download_zip']);
		/* Searching*/
		Route::any('/search/all/result',	['as'=>'search_project_details','uses'=>'Front\ProjectListingController@search_project']);

		/* Add / Remove favourite */
		Route::any('/add_or_remove_favourite',	['as'=>'search_project_details','uses'=>'Front\ProjectListingController@add_or_remove_favourite']);
		Route::any('/skills/{enc_id}',	['as'=>'browse_skills','uses'=>'Front\ProjectListingController@browse_skills']);					
	    
	    /* pagination */
		Route::any('show_cnt',['as'=>'show_cnt','uses'=>'Front\ProjectListingController@show_cnt']);

		//get experts
		Route::any('get_experts',  ['as'=>'get_experts',	'uses'=>'Front\Client\ProjectsController@get_experts']);
	});

	/* fronts contest routes group starts */
	Route::group(array('prefix' => '/contests','middleware' =>'front'), function()
	{
		Route::get('/',	['as'=>'front_contest_listing','uses'=>'Front\ContestListingController@index']);
		//Route::get('/{enc_id}',			['as'=>'front_show_category_wise_projects','uses'=>'Front\ContestListingController@index']);
		Route::get('/details/{enc_id}',	['as'=>'show_contest_details','uses'=>'Front\ContestListingController@show_contest_details']);

		Route::get('/details/make_zip/{enc_id}',	['as'=>'show_contest_details/make_zip','uses'=>'Front\ContestListingController@make_zip']);
		Route::get('/details/download_zip/{enc_id}',	['as'=>'show_contest_details/download_zip','uses'=>'Front\ContestListingController@download_zip']);

        Route::get('/show_contest_entry_details/{enc_id}',	['as'=>'show_contest_details','uses'=>'Front\ContestListingController@show_contest_entry_details']); 
        Route::get('/get-seal-entry-payment-details',	['as'=>'show_contest_details','uses'=>'Front\ContestListingController@get_seal_entry_payment_details']); 

		/* Searching*/
		//Route::any('/search/all/result',	['as'=>'search_project_details','uses'=>'Front\ContestListingController@search_project']);

		/* Add / Remove favourite */
		//Route::any('/add_or_remove_favourite',	['as'=>'search_project_details','uses'=>'Front\ContestListingController@add_or_remove_favourite']);
		//Route::any('/skills/{enc_id}',	['as'=>'browse_skills','uses'=>'Front\ContestListingController@browse_skills']);					
	    
	    /* pagination */
		Route::any('show_cnt',['as'=>'show_cnt','uses'=>'Front\ContestListingController@show_cnt']);
		Route::any('search',['as'=>'show_cnt','uses'=>'Front\ContestListingController@search']);
	});

	Route::any('search_project',	['as'=>'search_project','uses'=>'Front\ProjectListingController@project_search_for_autocomplete']);			
	Route::any('autocomplete',	['as'=>'autocomplete','uses'=>'Front\ProjectListingController@get_autocomplete_data']);				
	/*---------------------- Front ( Experts ) Expert Listing -------------------------------*/
	//include_once(app_path('Http/Routes/expert_listing.php'));
});