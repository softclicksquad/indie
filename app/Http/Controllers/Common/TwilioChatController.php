<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\UserModel;
use App\Models\ProjectsBidsModel;
use App\Models\ContestEntryModel;
use App\Models\NotificationsModel;

use App\Common\Services\TwilioChatService; 

use Session;

class TwilioChatController extends Controller
{
    public function __construct()
    {
        $this->UserModel          = new UserModel();
        $this->ProjectsBidsModel  = new ProjectsBidsModel();
        $this->ContestEntryModel  = new ContestEntryModel();
    	$this->NotificationsModel = new NotificationsModel();

    	$this->TwilioChatService  = new TwilioChatService();

    	$this->default_profile_image   = url('/public/uploads/front/profile/default_profile_image.png');
        $this->profile_img_base_path   = base_path().'/public'.config('app.project.img_path.profile_image');
        $this->profile_img_public_path = url('/public').config('app.project.img_path.profile_image');

      	$this->arr_view_data 	  = [];
      	$this->view_folder_path   = '/twilio-chat/';
      	$this->module_url_path    = url("/client/twilio-chat");
    }
    
    public function show_chat_list(){
        
        $obj_user = \Sentinel::check();
        if( $obj_user == false ) {
          return redirect('/login');
        }
        
        $user_type = '';
        if($obj_user->inRole('client')){
            $user_type = 'client';
        } else if($obj_user->inRole('expert')){
            $user_type = 'expert';
        }

        if($user_type == false) {
            Session::flash("error",'Invalid user type unable to process request, Please try again.');
            return redirect(url('/'. $user_type .'/dashboard'));
        }


        $this->module_url_path = url('/'. $user_type .'/twilio-chat');

        $login_user_id  = isset($obj_user->id) ? $obj_user->id : 0;

        $obj_channel_resources = $this->TwilioChatService->fetch_channels_list();
        
        // $this->update_channels_addition_details($obj_channel_resources);
        $arr_chat_list = $this->build_chat_list_data($obj_channel_resources,$login_user_id,$user_type);
        
        $this->arr_view_data['page_title']       = 'Chat List';
        $this->arr_view_data['login_user_id']    = $login_user_id;
        $this->arr_view_data['login_user_type']  = $user_type;
        $this->arr_view_data['arr_chat_list']    = $arr_chat_list;
        $this->arr_view_data['user_type']        = $user_type;
        $this->arr_view_data['module_url_path']  = $this->module_url_path;

        return view('twilio-chat.new_chat_ui',$this->arr_view_data);
    }

    public function project_bid_chat($project_bid_id){

        $project_bid_id = base64_decode( $project_bid_id );

    	$obj_user = \Sentinel::check();
    	if( $obj_user == false ) {
          return redirect('/login');
        }
        
        $user_type = '';
        if($obj_user->inRole('client')){
			$user_type = 'client';
        } else if($obj_user->inRole('expert')){
			$user_type = 'expert';
        }
        
        $this->module_url_path = url('/'. $user_type .'/twilio-chat');

        if($user_type == false) {
			Session::flash("error",'Invalid user type unable to process request, Please try again.');
    		return redirect(url('/'. $user_type .'/dashboard'));
    	}

        $arr_from_user = isset($obj_user) ? $obj_user->toArray() : [];
        
        $login_user_id  = isset($obj_user->id) ? $obj_user->id : 0;

        $login_user_timezone = config('app.timezone');
        if($user_type == 'client'){
            $login_user_timezone = get_client_timezone($login_user_id);
        } else if($user_type == 'expert'){
            $login_user_timezone = get_expert_timezone($login_user_id);
        }

        $first_name     = isset($arr_from_user['role_info']['first_name']) ? $arr_from_user['role_info']['first_name'] : '';
    	$last_name      = isset($arr_from_user['role_info']['last_name']) ? $arr_from_user['role_info']['last_name'] : '';
		$from_user_name = $first_name . ' ' . $last_name;

		$from_user_profile_image = $this->default_profile_image;
		$to_user_profile_image   = $this->default_profile_image;
        

        if( isset($arr_from_user['role_info']['profile_image']) && $arr_from_user['role_info']['profile_image']!='' && file_exists($this->profile_img_base_path.$arr_from_user['role_info']['profile_image'])){
            $from_user_profile_image = $this->profile_img_public_path.$arr_from_user['role_info']['profile_image'];
        }

		$obj_projects_bids = $this->ProjectsBidsModel
										->with('project_details.client_details')
    									->where('id',$project_bid_id)
    									->first();

        if($obj_projects_bids == false) {
			Session::flash("error",'Invalid bid details unable to process request, Please try again.');
    		return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
    	}
    	
    	$arr_projects_bid = $obj_projects_bids->toArray(); 

    	$project_id   = isset($arr_projects_bid['project_id']) ? $arr_projects_bid['project_id'] : '';

        $arr_channel_details                     = [];
        $arr_channel_details['project_id']       = $project_id;
        $arr_channel_details['project_name']     = isset($arr_projects_bid['project_details']['project_name']) ? $arr_projects_bid['project_details']['project_name'] : '';
        $arr_channel_details['project_bid_id']   = isset($arr_projects_bid['id']) ? $arr_projects_bid['id'] : '';
        $arr_channel_details['is_project_chat']  = '1';
        $arr_channel_details['client_id']        = isset($arr_projects_bid['project_details']['client_details']['id']) ? $arr_projects_bid['project_details']['client_details']['id'] : '';;
        $arr_channel_details['expert_id']        = isset($arr_projects_bid['expert_user_id']) ? $arr_projects_bid['expert_user_id'] : '';

        $project_channel_name = '';

    	if($user_type == 'client'){
			
			$to_first_name = isset($arr_projects_bid['role_info']['first_name']) ? $arr_projects_bid['role_info']['first_name'] : '';
	    	$to_last_name  = isset($arr_projects_bid['role_info']['last_name']) ? substr($arr_projects_bid['role_info']['last_name'], 0, 1) : '';
	    	$to_user_id    = isset($arr_projects_bid['expert_user_id']) ? $arr_projects_bid['expert_user_id'] : 0;
			$to_user_name  = $to_first_name . ' ' . $to_last_name;

			if( isset($arr_projects_bid['role_info']['profile_image']) && $arr_projects_bid['role_info']['profile_image']!='' && file_exists($this->profile_img_base_path.$arr_projects_bid['role_info']['profile_image'])){
                $to_user_profile_image = $this->profile_img_public_path.$arr_projects_bid['role_info']['profile_image'];
	         }

			$project_channel_name = '#' . $project_id .'-'.$project_bid_id .'-'.$login_user_id .'-'.$to_user_id; 
        
        } else if($user_type == 'expert'){
		
			$to_first_name = isset($arr_projects_bid['project_details']['client_details']['role_info']['first_name']) ? $arr_projects_bid['project_details']['client_details']['role_info']['first_name'] : '';
	    	$to_last_name  = isset($arr_projects_bid['project_details']['client_details']['role_info']['last_name']) ? substr($arr_projects_bid['project_details']['client_details']['role_info']['last_name'], 0, 1) : '';
	    	$to_user_id    = isset($arr_projects_bid['project_details']['client_user_id']) ? $arr_projects_bid['project_details']['client_user_id'] : 0;
			$to_user_name  = $to_first_name . ' ' . $to_last_name;
	
			if( isset($arr_projects_bid['project_details']['client_details']['role_info']['profile_image']) && $arr_projects_bid['project_details']['client_details']['role_info']['profile_image']!='' && file_exists($this->profile_img_base_path.$arr_projects_bid['project_details']['client_details']['role_info']['profile_image'])){
                $to_user_profile_image = $this->profile_img_public_path.$arr_projects_bid['project_details']['client_details']['role_info']['profile_image'];
		    }

			$project_channel_name = '#' . $project_id .'-'.$project_bid_id .'-'.$to_user_id .'-'.$login_user_id; 
        }
        
        $expert_first_name = isset($arr_projects_bid['role_info']['first_name']) ? $arr_projects_bid['role_info']['first_name'] : '';
        $expert_last_name  = isset($arr_projects_bid['role_info']['last_name']) ? substr($arr_projects_bid['role_info']['last_name'], 0, 1) : '';
        $expert_user_id    = isset($arr_projects_bid['expert_user_id']) ? $arr_projects_bid['expert_user_id'] : 0;
        $expert_user_name  = $expert_first_name . ' ' . $expert_last_name;        

        $client_first_name = isset($arr_projects_bid['project_details']['client_details']['role_info']['first_name']) ? $arr_projects_bid['project_details']['client_details']['role_info']['first_name'] : '';
        $client_last_name  = isset($arr_projects_bid['project_details']['client_details']['role_info']['last_name']) ? substr($arr_projects_bid['project_details']['client_details']['role_info']['last_name'], 0, 1) : '';
        $client_user_id    = isset($arr_projects_bid['project_details']['client_user_id']) ? $arr_projects_bid['project_details']['client_user_id'] : 0;
        $client_user_name  = $client_first_name . ' ' . $client_last_name;
        
        $client_chat_name = $expert_user_name;
        $expert_chat_name = $client_user_name;

        $arr_channel_details['project_channel_name'] = $project_channel_name;
        $arr_channel_details['client_chat_name'] = $client_chat_name;
        $arr_channel_details['expert_chat_name'] = $expert_chat_name;

        $obj_channel_resource = $this->TwilioChatService->get_channel_resource_details( $arr_channel_details );

        if($obj_channel_resource == NULL ) {
            Session::flash("error",'Failed to create channel resource, Please try again.');
            return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
        }

        $channel_resource_sid = isset($obj_channel_resource->sid) ? $obj_channel_resource->sid : '';

        $access_token = $this->TwilioChatService->genrate_access_token( $login_user_id );
        if($access_token == false ){
            Session::flash("error",'Failed to genrate access token, Please try again.');
            return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
        }

        if(isset($arr_projects_bid['is_chat_initiated']) && $arr_projects_bid['is_chat_initiated'] == 0 ) {
            $arr_user = [ 'identity' => $login_user_id, 'friendlyName' => $from_user_name ];
            $obj_from_user = $this->TwilioChatService->get_twilio_user_details( $arr_user );
            if($obj_from_user == false ){
                Session::flash("error",'Failed to genrate twilio user, Please try again.');
                return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
            }

            $arr_user = [ 'identity' => $to_user_id, 'friendlyName' => $to_user_name ];
            $obj_to_user = $this->TwilioChatService->get_twilio_user_details( $arr_user );
            if($obj_to_user == false ){
                Session::flash("error",'Failed to genrate twilio user, Please try again.');
                return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
            }

            //check and attach member to the channel resource
            $obj_from_member_resource = $this->TwilioChatService->get_channel_member_resource($channel_resource_sid,$login_user_id);    
            if($obj_from_member_resource == NULL ) {
                Session::flash("error",'Failed to create add user to channel resource, Please try again.');
                return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
            }

            $obj_to_member_resource = $this->TwilioChatService->get_channel_member_resource($channel_resource_sid,$to_user_id); 
            if($obj_to_member_resource == NULL ) {
                Session::flash("error",'Failed to create add user to channel resource, Please try again.');
                return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
            }
        }

        $client_profile_image = $this->default_profile_image;
        $expert_profile_image   = $this->default_profile_image;

        if( isset($arr_projects_bid['project_details']['client_details']['role_info']['profile_image']) && $arr_projects_bid['project_details']['client_details']['role_info']['profile_image']!='' && file_exists($this->profile_img_base_path.$arr_projects_bid['project_details']['client_details']['role_info']['profile_image'])){
            $client_profile_image = $this->profile_img_public_path.$arr_projects_bid['project_details']['client_details']['role_info']['profile_image'];
        }

        if( isset($arr_projects_bid['role_info']['profile_image']) && $arr_projects_bid['role_info']['profile_image']!='' && file_exists($this->profile_img_base_path.$arr_projects_bid['role_info']['profile_image'])){
            $expert_profile_image   = $this->profile_img_public_path.$arr_projects_bid['role_info']['profile_image'];
        }

        $arr_chat_user = 
                [
                    'expert_user_id'       => $expert_user_id,
                    'expert_user_name'     => $expert_user_name,
                    'expert_profile_image' => $expert_profile_image,
                    'client_user_id'       => $client_user_id,
                    'client_user_name'     => $client_user_name,
                    'client_profile_image' => $client_profile_image,
                    'admin_user_id'        => 1,
                    'admin_user_name'      => config('app.project.name') . ' Admin',
                    'admin_profile_image'  => url('/public/front/images/default_admin_image.jpg')
                ];

        $obj_message_list = false;
        if(isset($arr_projects_bid['is_chat_initiated']) && $arr_projects_bid['is_chat_initiated'] == 1 ) {
    		$obj_message_list = $this->TwilioChatService->fetch_message_list($channel_resource_sid);
            $obj_message_list = $this->rebuild_message_list($obj_message_list,$login_user_timezone,$arr_chat_user);
        }

        // notify expert regarding chat intialted process
        if($user_type == 'client' && isset($arr_projects_bid['is_chat_initiated']) && $arr_projects_bid['is_chat_initiated'] == 0 ) {
            $this->prepare_and_send_notification_to_expert( $arr_projects_bid, 'project' );
            $this->ProjectsBidsModel
                            ->where('id',$arr_projects_bid['id'])
                            ->update(['is_chat_initiated'=>'1']);
        }

        // load channel list and chat list
        $obj_channel_resources = $this->TwilioChatService->fetch_channels_list();
        //dd($obj_channel_resources);
        $arr_chat_list         = $this->build_chat_list_data($obj_channel_resources,$login_user_id,$user_type);

        $this->arr_view_data['page_title']              = 'Project Bid Chat';
        $this->arr_view_data['login_user_type']         = $user_type;
        $this->arr_view_data['from_user_id']            = $login_user_id;
        $this->arr_view_data['to_user_id']              = $to_user_id;
        $this->arr_view_data['to_user_name']            = $to_user_name;
        $this->arr_view_data['login_user_type']         = $user_type;
        $this->arr_view_data['project_channel_name']    = $project_channel_name;
        $this->arr_view_data['from_user_profile_image'] = $from_user_profile_image;
        $this->arr_view_data['to_user_profile_image']   = $to_user_profile_image;
        $this->arr_view_data['login_user_timezone']     = $login_user_timezone;

    	$this->arr_view_data['access_token']             = $access_token;
    	$this->arr_view_data['channel_resource_sid']     = $channel_resource_sid;
    	$this->arr_view_data['obj_channel_resource']     = $obj_channel_resource;
        $this->arr_view_data['obj_message_list']         = $obj_message_list;
    	$this->arr_view_data['arr_chat_list']            = $arr_chat_list;
        $this->arr_view_data['is_project_chat']          = '1';
        $this->arr_view_data['arr_data']                 = $arr_projects_bid;
        $this->arr_view_data['arr_chat_user']                 = $arr_chat_user;
        $this->arr_view_data['user_type']                 = $user_type;
    	
        $this->arr_view_data['module_url_path']  = $this->module_url_path;

        return view('twilio-chat.new_common_chat_ui',$this->arr_view_data);   
    }

    public function contest_chat($contest_entry_id){

        $contest_entry_id = base64_decode( $contest_entry_id );

        $obj_user = \Sentinel::check();
        if( $obj_user == false ) {
          return redirect('/login');
        }
        
        $user_type = '';
        if($obj_user->inRole('client')){
            $user_type = 'client';
        } else if($obj_user->inRole('expert')){
            $user_type = 'expert';
        }

        $this->module_url_path = url('/'. $user_type .'/twilio-chat');

        if($user_type == false) {
            Session::flash("error",'Invalid user type unable to process request, Please try again.');
            return redirect(url('/'. $user_type .'/dashboard'));
        }

        $arr_from_user = isset($obj_user) ? $obj_user->toArray() : [];
        
        $login_user_id  = isset($obj_user->id) ? $obj_user->id : 0;

        $login_user_timezone = config('app.timezone');
        if($user_type == 'client'){
            $login_user_timezone = get_client_timezone($login_user_id);
        } else if($user_type == 'expert'){
            $login_user_timezone = get_expert_timezone($login_user_id);
        }

        $first_name     = isset($arr_from_user['role_info']['first_name']) ? $arr_from_user['role_info']['first_name'] : '';
        $last_name      = isset($arr_from_user['role_info']['last_name']) ? $arr_from_user['role_info']['last_name'] : '';
        $from_user_name = $first_name . ' ' . $last_name;

        $from_user_profile_image = $this->default_profile_image;
        $to_user_profile_image   = $this->default_profile_image;
        
        if( isset($arr_from_user['role_info']['profile_image']) && $arr_from_user['role_info']['profile_image']!='' && file_exists($this->profile_img_base_path.$arr_from_user['role_info']['profile_image'])){
            $from_user_profile_image = $this->profile_img_public_path.$arr_from_user['role_info']['profile_image'];
        }

        $obj_contest_entry = $this->ContestEntryModel->where('id',$contest_entry_id)
                                                        ->with('contest_details','client_user_details','expert_details','expert_details.user_details')      
                                                        ->first();

        if($obj_contest_entry == false) {
            Session::flash("error",'Invalid contest entry details unable to process request, Please try again.');
            return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
        }
        
        $arr_contest_entry = $obj_contest_entry->toArray(); 

        $contest_id   = isset($arr_contest_entry['contest_id']) ? $arr_contest_entry['contest_id'] : '';

        $arr_channel_details                     = [];
        $arr_channel_details['contest_id']       = $contest_id;
        $arr_channel_details['contest_title']    = isset($arr_contest_entry['contest_details']['contest_title']) ? $arr_contest_entry['contest_details']['contest_title'] : '';
        $arr_channel_details['contest_entry_id'] = isset($arr_contest_entry['id']) ? $arr_contest_entry['id'] : '';
        $arr_channel_details['is_contest_chat']  = '1';
        $arr_channel_details['client_id']        = isset($arr_contest_entry['client_user_id']) ? $arr_contest_entry['client_user_id'] : '';;
        $arr_channel_details['expert_id']        = isset($arr_contest_entry['expert_id']) ? $arr_contest_entry['expert_id'] : '';
        
        $contest_channel_name = '';

        if($user_type == 'client'){
            
            $to_first_name = isset($arr_contest_entry['expert_details']['first_name']) ? $arr_contest_entry['expert_details']['first_name'] : '';
            $to_last_name  = isset($arr_contest_entry['expert_details']['last_name']) ? substr($arr_contest_entry['expert_details']['last_name'], 0, 1) : '';
            $to_user_id    = isset($arr_contest_entry['expert_id']) ? $arr_contest_entry['expert_id'] : 0;
            $to_user_name  = $to_first_name . ' ' . $to_last_name;

            if( isset($arr_contest_entry['expert_details']['profile_image']) && $arr_contest_entry['expert_details']['profile_image']!='' && file_exists($this->profile_img_base_path.$arr_contest_entry['expert_details']['profile_image'])){
                $to_user_profile_image = $this->profile_img_public_path.$arr_contest_entry['expert_details']['profile_image'];
            }

            $contest_channel_name = '#contest-' . $contest_id .'-'.$contest_entry_id .'-'.$login_user_id .'-'.$to_user_id; 
        
        } else if($user_type == 'expert'){
        
            $to_first_name = isset($arr_contest_entry['client_user_details']['first_name']) ? $arr_contest_entry['client_user_details']['first_name'] : '';
            $to_last_name  = isset($arr_contest_entry['client_user_details']['last_name']) ? substr($arr_contest_entry['client_user_details']['last_name'], 0, 1) : '';
            $to_user_id    = isset($arr_contest_entry['client_user_id']) ? $arr_contest_entry['client_user_id'] : 0;
            $to_user_name  = $to_first_name . ' ' . $to_last_name;
    
            if( isset($arr_contest_entry['client_user_details']['profile_image']) && $arr_contest_entry['client_user_details']['profile_image']!='' && file_exists($this->profile_img_base_path.$arr_contest_entry['client_user_details']['profile_image'])){
                $to_user_profile_image = $this->profile_img_public_path.$arr_contest_entry['client_user_details']['profile_image'];
            }

            $contest_channel_name = '#contest-' . $contest_id .'-'.$contest_entry_id .'-'.$to_user_id .'-'.$login_user_id; 
        }

        $expert_first_name = isset($arr_contest_entry['expert_details']['first_name']) ? $arr_contest_entry['expert_details']['first_name'] : '';
        $expert_last_name  = isset($arr_contest_entry['expert_details']['last_name']) ? substr($arr_contest_entry['expert_details']['last_name'], 0, 1) : '';
        $expert_user_id    = isset($arr_contest_entry['expert_id']) ? $arr_contest_entry['expert_id'] : 0;
        $expert_user_name  = $expert_first_name . ' ' . $expert_last_name;        

        $client_first_name = isset($arr_contest_entry['client_user_details']['first_name']) ? $arr_contest_entry['client_user_details']['first_name'] : '';
        $client_last_name  = isset($arr_contest_entry['client_user_details']['last_name']) ? substr($arr_contest_entry['client_user_details']['last_name'], 0, 1) : '';
        $client_user_id    = isset($arr_contest_entry['client_user_id']) ? $arr_contest_entry['client_user_id'] : 0;
        $client_user_name  = $client_first_name . ' ' . $client_last_name;
        
        $client_chat_name = $expert_user_name;
        $expert_chat_name = $client_user_name;

        $arr_channel_details['contest_channel_name'] = $contest_channel_name;
        $arr_channel_details['client_chat_name'] = $client_chat_name;
        $arr_channel_details['expert_chat_name'] = $expert_chat_name;

        $obj_channel_resource = $this->TwilioChatService->get_channel_resource_details( $arr_channel_details );
        if($obj_channel_resource == NULL ) {
            Session::flash("error",'Failed to create channel resource, Please try again.');
            return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
        }

        $channel_resource_sid = isset($obj_channel_resource->sid) ? $obj_channel_resource->sid : '';

        $access_token = $this->TwilioChatService->genrate_access_token( $login_user_id );
        if($access_token == false ){
            Session::flash("error",'Failed to genrate access token, Please try again.');
            return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
        }

        if(isset($arr_contest_entry['is_chat_initiated']) && $arr_contest_entry['is_chat_initiated'] == 0 ) {
            $arr_user = [ 'identity' => $login_user_id, 'friendlyName' => $from_user_name ];
            $obj_from_user = $this->TwilioChatService->get_twilio_user_details( $arr_user );
            if($obj_from_user == false ){
                Session::flash("error",'Failed to genrate twilio user, Please try again.');
                return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
            }

            $arr_user = [ 'identity' => $to_user_id, 'friendlyName' => $to_user_name ];
            $obj_to_user = $this->TwilioChatService->get_twilio_user_details( $arr_user );
            if($obj_to_user == false ){
                Session::flash("error",'Failed to genrate twilio user, Please try again.');
                return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
            }

            //check and attach member to the channel resource
            $obj_from_member_resource = $this->TwilioChatService->get_channel_member_resource($channel_resource_sid,$login_user_id);    
            if($obj_from_member_resource == NULL ) {
                Session::flash("error",'Failed to create add user to channel resource, Please try again.');
                return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
            }

            $obj_to_member_resource = $this->TwilioChatService->get_channel_member_resource($channel_resource_sid,$to_user_id); 
            if($obj_to_member_resource == NULL ) {
                Session::flash("error",'Failed to create add user to channel resource, Please try again.');
                return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
            }
        }
        
        $client_profile_image = $this->default_profile_image;
        $expert_profile_image   = $this->default_profile_image;
        if( isset($arr_contest_entry['expert_details']['profile_image']) && $arr_contest_entry['expert_details']['profile_image']!='' && file_exists($this->profile_img_base_path.$arr_contest_entry['expert_details']['profile_image'])){
            $expert_profile_image   = $this->profile_img_public_path.$arr_contest_entry['expert_details']['profile_image'];
        }

        if( isset($arr_contest_entry['client_user_details']['profile_image']) && $arr_contest_entry['client_user_details']['profile_image']!='' && file_exists($this->profile_img_base_path.$arr_contest_entry['client_user_details']['profile_image'])){
            $client_profile_image = $this->profile_img_public_path.$arr_contest_entry['client_user_details']['profile_image'];
        }

        $arr_chat_user = 
                [
                    'expert_user_id'       => $expert_user_id,
                    'expert_user_name'     => $expert_user_name,
                    'expert_profile_image' => $expert_profile_image,
                    'client_user_id'       => $client_user_id,
                    'client_user_name'     => $client_user_name,
                    'client_profile_image' => $client_profile_image,
                    'admin_user_id'        => 1,
                    'admin_user_name'      => config('app.project.name') . ' Admin',
                    'admin_profile_image'  => url('/public/front/images/default_admin_image.jpg')
                ];

        $obj_message_list = false;
        if(isset($arr_contest_entry['is_chat_initiated']) && $arr_contest_entry['is_chat_initiated'] == 1 ) {
            $obj_message_list = $this->TwilioChatService->fetch_message_list($channel_resource_sid);
            $obj_message_list = $this->rebuild_message_list($obj_message_list,$login_user_timezone,$arr_chat_user);
        }
        
        // notify expert regarding chat intialted process
        if($user_type == 'client' && isset($arr_contest_entry['is_chat_initiated']) && $arr_contest_entry['is_chat_initiated'] == 0 ) {
            $this->prepare_and_send_notification_to_expert( $arr_contest_entry, 'contest' );
            $this->ContestEntryModel
                            ->where('id',$arr_contest_entry['id'])
                            ->update(['is_chat_initiated'=>'1']);
        }

        // load channel list and chat list
        $obj_channel_resources = $this->TwilioChatService->fetch_channels_list();
        $arr_chat_list         = $this->build_chat_list_data($obj_channel_resources,$login_user_id,$user_type);

        $this->arr_view_data['page_title']              = 'Contest Bid Chat';
        $this->arr_view_data['login_user_type']         = $user_type;
        $this->arr_view_data['from_user_id']            = $login_user_id;
        $this->arr_view_data['to_user_id']              = $to_user_id;
        $this->arr_view_data['to_user_name']            = $to_user_name;
        $this->arr_view_data['login_user_type']         = $user_type;
        $this->arr_view_data['contest_channel_name']    = $contest_channel_name;
        $this->arr_view_data['from_user_profile_image'] = $from_user_profile_image;
        $this->arr_view_data['to_user_profile_image']   = $to_user_profile_image;
        $this->arr_view_data['login_user_timezone']     = $login_user_timezone;

        $this->arr_view_data['access_token']             = $access_token;
        $this->arr_view_data['channel_resource_sid']     = $channel_resource_sid;
        $this->arr_view_data['obj_channel_resource']     = $obj_channel_resource;
        $this->arr_view_data['obj_message_list']         = $obj_message_list;
        $this->arr_view_data['arr_chat_list']            = $arr_chat_list;
        $this->arr_view_data['is_contest_chat']          = '1';
        $this->arr_view_data['arr_data']                 = $arr_contest_entry;
        $this->arr_view_data['arr_chat_user']            = $arr_chat_user;
        $this->arr_view_data['user_type']                = $user_type;

        $this->arr_view_data['module_url_path']  = $this->module_url_path;

        return view('twilio-chat.new_common_chat_ui',$this->arr_view_data);   
    }

    public function refresh_token(){

    	$obj_user = \Sentinel::check();
    	if( $obj_user == false ) {
          	return response()->json(['status' => 'error','msg' => 'Login session expired,Please relogin']);
        }

        $login_user_id = isset($obj_user->id) ? $obj_user->id : 0;
        $access_token = $this->TwilioChatService->genrate_access_token( $login_user_id );
        if($access_token == false ){
        	return response()->json(['status' => 'error','msg' => 'Failed to genrate access token, Please try again.']);
        }
        return response()->json(['status' => 'success','access_token' => $access_token]);
    }

    public function update_channel(Request $request){
        
        $channel_sid = $request->input('channel_sid');
        if($channel_sid == ''){
            return response()->json(['status' => 'error','msg' => '']);
        }

        $obj_channel_resource = $this->TwilioChatService->get_channel_resource_details_by_sid( $channel_sid );
        if($obj_channel_resource == NULL ) {
            return response()->json(['status' => 'error','msg' => '']);
        }

        $arr_attributes =  isset($obj_channel_resource->attributes) ? json_decode($obj_channel_resource->attributes,true) : [];
        if(isset($arr_attributes) && count($arr_attributes)>0) {
            $this->TwilioChatService->update_channel_resource($arr_attributes);
        }

        return response()->json(['status' => 'success']);
    }

    public function request_admin(Request $request){
        
        $arr_rules                = array();
        $arr_rules['chat_type']   = "required";
        $arr_rules['channel_sid'] = "required";
        $arr_rules['request_id']  = "required";
        
        $validator = \Validator::make($request->all(),$arr_rules);
        if($validator->fails()){
            Session::flash("error",'Invalid request parameters provided unable to process request, Please try again.');
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $channel_sid = $request->input('channel_sid');
        $chat_type   = $request->input('chat_type');
        $request_id  = base64_decode($request->input('request_id'));

        $obj_channel_resource = $this->TwilioChatService->get_channel_resource_details_by_sid( $channel_sid );

        if($obj_channel_resource == NULL ) {
            Session::flash("error",'Invalid channel resource unable to process request, Please try again.');
            return redirect()->back();
        }
        
        $arr_notification_data = [];
        
        if( $chat_type == 'project') {
            $obj_projects_bids = $this->ProjectsBidsModel
                                        ->with('project_details.client_details')
                                        ->where('id',$request_id)
                                        ->first();

            if($obj_projects_bids == false) {
                Session::flash("error",'Invalid bid details unable to process request, Please try again.');
                return redirect()->back();
            }

            $arr_notification_data = $obj_projects_bids->toArray();
        }

        if( $chat_type == 'contest') {
            $obj_contest_entry = $this->ContestEntryModel->where('id',$request_id)
                                                        ->with('contest_details','client_user_details','expert_details','expert_details.user_details')      
                                                        ->first();

            if($obj_contest_entry == false) {
                Session::flash("error",'Invalid contest entry details unable to process request, Please try again.');
                return redirect()->back();
            }

            $arr_notification_data = $obj_contest_entry->toArray();    
        }

        $this->prepare_and_send_notification_to_admin( $arr_notification_data, 'project' );

        $arr_admin_user = [ 'identity' => 1, 'friendlyName' => config('app.project.name') . ' Admin' ];
        $obj_admin_user = $this->TwilioChatService->get_twilio_user_details( $arr_admin_user );
        
        if($obj_admin_user == false ){
            Session::flash("error",'Failed to genrate admin user, Please try again.');
            return redirect()->back();
        }

        //check and attach member to the channel resource
        $obj_member_resource = $this->TwilioChatService->get_channel_member_resource($channel_sid,1);    
        if($obj_member_resource == NULL ) {
            Session::flash("error",'Failed to create add user to channel resource, Please try again.');
            return redirect()->back();
        }

        if( $chat_type == 'project') {
            $this->prepare_and_send_notification_to_admin( $arr_notification_data, 'project' );
            $this->ProjectsBidsModel
                        ->where('id',$request_id)
                        ->update(['is_admin_chat_initiated'=>'1']);

            Session::flash("success",'Chat joining request successfully sent to '. config('app.project.name') . ' Admin.');
            return redirect()->back();
        }

        if( $chat_type == 'contest') {
            $this->prepare_and_send_notification_to_admin( $arr_notification_data, 'contest' );
            $this->ContestEntryModel
                        ->where('id',$request_id)
                        ->update(['is_admin_chat_initiated'=>'1']);

            Session::flash("success",'Chat joining request successfully sent to '. config('app.project.name') . ' Admin.');
            return redirect()->back();  
        }

        Session::flash("error",'Something went wrong unable to process request, Please try again.');
        return redirect()->back();
    }

    public function video_call(Request $request){

        $arr_rules                = array();
        $arr_rules['chat_type']   = "required";
        $arr_rules['request_id']  = "required";
        $arr_rules['unique_name'] = "required";

        $validator = \Validator::make($request->all(),$arr_rules);
        if($validator->fails()){
            Session::flash("error",'Invalid request parameters provided unable to process request, Please try again.');
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $chat_type    = $request->input('chat_type');
        $request_id   = base64_decode($request->input('request_id'));
        $unique_name  = $request->input('unique_name');

        $obj_user = \Sentinel::check();
        if( $obj_user == false ) {
          return redirect('/login');
        }
        
        $user_type = '';
        if($obj_user->inRole('client')){
            $user_type = 'client';
        } else if($obj_user->inRole('expert')){
            $user_type = 'expert';
        }
        
        $this->module_url_path = url('/'. $user_type .'/twilio-chat');

        if($user_type == false) {
            Session::flash("error",'Invalid user type unable to process request, Please try again.');
            return redirect(url('/'. $user_type .'/dashboard'));
        }

        $arr_from_user = isset($obj_user) ? $obj_user->toArray() : [];
        
        $first_name      = isset($arr_from_user['role_info']['first_name']) ? $arr_from_user['role_info']['first_name'] : '';
        $last_name       = isset($arr_data['role_info']['last_name']) ? substr($arr_data['role_info']['last_name'], 0, 1) : '';
        $login_user_name = $first_name.' '.$last_name;

        $login_user_id   = isset($obj_user->id) ? $obj_user->id : 0;
        
        $access_token = $this->TwilioChatService->genrate_access_token( $login_user_id );
        if($access_token == false ){
            Session::flash("error",'Failed to genrate access token, Please try again.');
            return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
        }

        $obj_video_room_resource = $this->TwilioChatService->get_video_room_resource_details( $unique_name );
        if($obj_video_room_resource == NULL ) {
            Session::flash("error",'Failed to create video room resource, Please try again.');
            return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
        }

        $arr_to_user_details = $this->get_video_call_to_user_details($request_id,$chat_type,$user_type);

        $this->arr_view_data['user_type']               = $chat_type;
        $this->arr_view_data['request_id']              = $request_id;
        $this->arr_view_data['module_url_path']         = $this->module_url_path;
        $this->arr_view_data['access_token']            = $access_token;
        $this->arr_view_data['unique_name']             = $unique_name;
        $this->arr_view_data['login_user_id']           = $login_user_id;
        $this->arr_view_data['login_user_name']         = $login_user_name;
        $this->arr_view_data['obj_video_room_resource'] = $obj_video_room_resource;
        $this->arr_view_data['arr_to_user_details']     = $arr_to_user_details;

        return view('twilio-chat.new_video_chat_ui',$this->arr_view_data);
    }

    public function get_video_call_to_user_details($request_id,$chat_type,$user_type){

        $arr_data = [];
        $to_user_id = 0;
        $to_user_name = '';

        if( $chat_type == 'project') {
            $obj_projects_bids = $this->ProjectsBidsModel
                                        ->with('project_details.client_details')
                                        ->where('id',$request_id)
                                        ->first();

            if($obj_projects_bids != false) {
                $arr_data = $obj_projects_bids->toArray();
            }
            if($user_type == 'client'){
            
                $to_first_name = isset($arr_data['role_info']['first_name']) ? $arr_data['role_info']['first_name'] : '';
                $to_last_name  = isset($arr_data['role_info']['last_name']) ? substr($arr_data['role_info']['last_name'], 0, 1) : '';
                $to_user_id    = isset($arr_data['expert_user_id']) ? $arr_data['expert_user_id'] : 0;
                $to_user_name  = $to_first_name . ' ' . $to_last_name;
            
            } else if($user_type == 'expert'){
            
                $to_first_name = isset($arr_data['project_details']['client_details']['role_info']['first_name']) ? $arr_data['project_details']['client_details']['role_info']['first_name'] : '';
                $to_last_name  = isset($arr_data['project_details']['client_details']['role_info']['last_name']) ? substr($arr_data['project_details']['client_details']['role_info']['last_name'], 0, 1) : '';
                $to_user_id    = isset($arr_data['project_details']['client_user_id']) ? $arr_data['project_details']['client_user_id'] : 0;
                $to_user_name  = $to_first_name . ' ' . $to_last_name;
            }
        }

        if( $chat_type == 'contest') {
            $obj_contest_entry = $this->ContestEntryModel->where('id',$request_id)
                                                        ->with('contest_details','client_user_details','expert_details','expert_details.user_details')      
                                                        ->first();

            if($obj_contest_entry != false) {
                $arr_data = $obj_contest_entry->toArray();    
            }
            if($user_type == 'client'){
                
                $to_first_name = isset($arr_data['expert_details']['first_name']) ? $arr_data['expert_details']['first_name'] : '';
                $to_last_name  = isset($arr_data['expert_details']['last_name']) ? substr($arr_data['expert_details']['last_name'], 0, 1) : '';
                $to_user_id    = isset($arr_data['expert_id']) ? $arr_data['expert_id'] : 0;
                $to_user_name  = $to_first_name . ' ' . $to_last_name;
            
            } else if($user_type == 'expert'){
            
                $to_first_name = isset($arr_data['client_user_details']['first_name']) ? $arr_data['client_user_details']['first_name'] : '';
                $to_last_name  = isset($arr_data['client_user_details']['last_name']) ? substr($arr_data['client_user_details']['last_name'], 0, 1) : '';
                $to_user_id    = isset($arr_data['client_user_id']) ? $arr_data['client_user_id'] : 0;
                $to_user_name  = $to_first_name . ' ' . $to_last_name;
            }
        }

        return [
                    'to_user_name'            => $to_user_name,
                    'to_user_id'              => $to_user_id,
                    'admin_user_id'           => 1,
                    'admin_user_name'         => config('app.project.name') . ' Admin',
                    'is_admin_chat_initiated' => isset($arr_data['is_admin_chat_initiated']) ? $arr_data['is_admin_chat_initiated'] : '0'
                ];
    }

    private function prepare_and_send_notification_to_expert( $arr_data, $type ) {

        $url = $name = $notification_text = $client_user_name = '';
        $user_id = $project_id = 0;

        if( $type == 'project'){

            $name = isset($arr_data['project_details']['project_name']) ? $arr_data['project_details']['project_name'] : '';
            $url = "expert/twilio-chat/projectbid/".base64_encode($arr_data['id']);
            $project_id = isset($arr_data['project_id']) ? $arr_data['project_id'] : 0;
            
            $client_first_name = isset($arr_data['project_details']['client_details']['role_info']['first_name']) ? $arr_data['project_details']['client_details']['role_info']['first_name'] : '';
            $client_last_name  = isset($arr_data['project_details']['client_details']['role_info']['last_name']) ? $arr_data['project_details']['client_details']['role_info']['last_name'] : '';
            $client_user_name  = $client_first_name . ' ' . $client_last_name;
            
            $user_id    = isset($arr_data['expert_user_id']) ? $arr_data['expert_user_id'] : 0;
            $notification_text = 'Chat intialted by ' . $client_user_name . ' for the Project of #' .$name;

        } elseif( $type == 'contest'){

            $name = isset($arr_data['contest_details']['contest_title']) ? $arr_data['contest_details']['contest_title'] : '';
            $url = "expert/twilio-chat/contest/".base64_encode($arr_data['id']);
            $project_id = isset($arr_data['contest_id']) ? $arr_data['contest_id'] : 0;
            
            $client_first_name = isset($arr_data['client_user_details']['first_name']) ? $arr_data['client_user_details']['first_name'] : '';
            $client_last_name  = isset($arr_data['client_user_details']['last_name']) ? $arr_data['client_user_details']['last_name'] : '';
            $client_user_name  = $client_first_name . ' ' . $client_last_name;
            
            $user_id    = isset($arr_data['expert_id']) ? $arr_data['expert_id'] : 0;
            $notification_text = 'Chat intialted by ' . $client_user_name . ' for the Contest of #' .$name;
        }

        /* Create Notification for expert*/
        $arr_data                         = [];
        $arr_data['user_id']              = $user_id;
        $arr_data['user_type']            = '3';
        $arr_data['url']                  = $url;
        $arr_data['notification_text_en'] = $notification_text;
        $arr_data['notification_text_de'] = $notification_text;
        $arr_data['project_id']           = $project_id;

        $this->NotificationsModel->create($arr_data); 
    }

    private function prepare_and_send_notification_to_admin( $arr_data, $type ) {

        if(isset($arr_data) && count($arr_data)>0){
            $url = $name = $notification_text = $client_user_name = '';
            $user_id = $project_id = 0;

            if( $type == 'project'){

                $name = isset($arr_data['project_details']['project_name']) ? $arr_data['project_details']['project_name'] : '';
                $url = "admin/twilio-chat/projectbid/".base64_encode($arr_data['id']);
                $project_id = isset($arr_data['project_id']) ? $arr_data['project_id'] : 0;
                
                $client_first_name = isset($arr_data['project_details']['client_details']['role_info']['first_name']) ? $arr_data['project_details']['client_details']['role_info']['first_name'] : '';
                $client_last_name  = isset($arr_data['project_details']['client_details']['role_info']['last_name']) ? $arr_data['project_details']['client_details']['role_info']['last_name'] : '';
                $client_user_name  = $client_first_name . ' ' . $client_last_name;
                
                $user_id    = isset($arr_data['expert_user_id']) ? $arr_data['expert_user_id'] : 0;
                $notification_text = 'Chat intialted by ' . $client_user_name . ' for the Project of #' .$name;

            } elseif( $type == 'contest'){

                $name = isset($arr_data['contest_details']['contest_title']) ? $arr_data['contest_details']['contest_title'] : '';
                $url = "admin/twilio-chat/contest/".base64_encode($arr_data['id']);
                $project_id = isset($arr_data['contest_id']) ? $arr_data['contest_id'] : 0;
                
                $client_first_name = isset($arr_data['client_user_details']['first_name']) ? $arr_data['client_user_details']['first_name'] : '';
                $client_last_name  = isset($arr_data['client_user_details']['last_name']) ? $arr_data['client_user_details']['last_name'] : '';
                $client_user_name  = $client_first_name . ' ' . $client_last_name;
                
                $user_id    = isset($arr_data['expert_id']) ? $arr_data['expert_id'] : 0;
                $notification_text = 'Chat intialted by ' . $client_user_name . ' for the Contest of #' .$name;
            }

            /* Create Notification for admin*/
            $arr_data                         = [];
            $arr_data['user_id']              = 1;
            $arr_data['user_type']            = '1';
            $arr_data['url']                  = $url;
            $arr_data['notification_text_en'] = $notification_text;
            $arr_data['notification_text_de'] = $notification_text;
            $arr_data['project_id']           = $project_id;
            
            $this->NotificationsModel->create($arr_data); 

            // send notification to all active sub-admins
            $obj_sub_admin = $this->UserModel
                                        ->whereHas('roles',function($q){
                                            return $q->where('slug','subadmin');
                                        })
                                        ->get();
            
            if(isset($obj_sub_admin) && count($obj_sub_admin)>0){
                foreach ($obj_sub_admin as $key => $value) {
                    $arr_data['user_id'] = isset($value->id) ? $value->id : 0;
                    $this->NotificationsModel->create($arr_data);
                }
            }
        }
        
        return true;
    }
    
    private function build_chat_list_data($obj_channel_resources,$login_user_id,$user_type){
        $arr_chat_list = [];
        if(isset($obj_channel_resources) && count($obj_channel_resources)>0) {
            foreach ($obj_channel_resources as $key => $obj_channel_resource) {
                $arr_attributes = json_decode($obj_channel_resource->attributes,true); 
                if( $obj_channel_resource->uniqueName != NULL && isset($arr_attributes) && is_array($arr_attributes) && count($arr_attributes)>0 ) {

                    $remote_user_id = 0;
                    if($user_type == 'client'){
                        $remote_user_id = isset($arr_attributes['client_id']) ? $arr_attributes['client_id'] : 0;
                    } else if($user_type == 'expert'){
                        $remote_user_id = isset($arr_attributes['expert_id']) ? $arr_attributes['expert_id'] : 0;
                    }

                    // made the list for project chat
                    if(isset($arr_attributes['is_project_chat']) && $arr_attributes['is_project_chat'] == '1' && $remote_user_id == $login_user_id ) {
                        
                        if( isset($arr_chat_list['project-'.$arr_attributes['project_id']]['id']) == false ) {
                            $arr_chat_list['project-'.$arr_attributes['project_id']]['id'] = isset($arr_attributes['project_id']) ? $arr_attributes['project_id'] : ''; 
                        }

                        $updated_date = '';
                        if(isset($obj_channel_resource->dateUpdated)){
                            $arrdateUpdated = json_decode(json_encode($obj_channel_resource->dateUpdated),true);
                            if(isset($arrdateUpdated['date'])){
                                $updated_date = date('Y-m-d H:i:s',strtotime($arrdateUpdated['date']));
                            }
                        }
                        
                        $arr_chat_list['project-'.$arr_attributes['project_id']]['group_name'] = isset($arr_attributes['project_name']) ? $arr_attributes['project_name'] : ''; 

                        if( isset($arr_chat_list['project-'.$arr_attributes['project_id']]['last_updated_date']) && $arr_chat_list['project-'.$arr_attributes['project_id']]['last_updated_date']!='' && strtotime($arr_chat_list['project-'.$arr_attributes['project_id']]['last_updated_date']) >= strtotime($updated_date) ) {
                            $arr_chat_list['project-'.$arr_attributes['project_id']]['last_updated_date'] = $arr_chat_list['project-'.$arr_attributes['project_id']]['last_updated_date'];
                        } else {
                            $arr_chat_list['project-'.$arr_attributes['project_id']]['last_updated_date'] = isset($updated_date) ? $updated_date : ''; 
                        }

                        $is_group_chat = isset($arr_attributes['is_group_chat']) ? $arr_attributes['is_group_chat'] : '0';
                        if( isset($arr_chat_list['project-'.$arr_attributes['project_id']]['is_group_chat']) && $arr_chat_list['project-'.$arr_attributes['project_id']]['is_group_chat']=='1' ) {
                            $arr_chat_list['project-'.$arr_attributes['project_id']]['is_group_chat'] = '1';
                            $arr_chat_list['project-'.$arr_attributes['project_id']]['group_chat_image'] = url('/public/front/images/default_group_chat.png');

                        } else {
                            $arr_chat_list['project-'.$arr_attributes['project_id']]['is_group_chat'] = $is_group_chat; 
                            $arr_chat_list['project-'.$arr_attributes['project_id']]['group_chat_image'] = url('/public/uploads/front/profile/default_profile_image.png');
                        }

                        if( isset($arr_chat_list['project-'.$arr_attributes['project_id']]['chat_list'][$arr_attributes['project_bid_id']]) == false ) {
                            
                            $arr_attributes['sid']           = isset($obj_channel_resource->sid) ? $obj_channel_resource->sid : '';
                            $arr_attributes['message_count'] = isset( $obj_channel_resource->messagesCount) ? $obj_channel_resource->messagesCount : '';
                            
                            $redirect_url = 'javascript:void(0);';
                            if(isset($user_type) && isset($arr_attributes['project_bid_id']) ){
                              $redirect_url = url('/') . '/' . $user_type . '/twilio-chat/projectbid/' . base64_encode($arr_attributes['project_bid_id']);
                            }

                            $chat_name = '';
                            if(isset($user_type) && $user_type == 'client') {
                                $chat_name = isset($arr_attributes['client_chat_name']) ? $arr_attributes['client_chat_name'] : '';
                            } else if(isset($user_type) && $user_type == 'expert') {
                                $chat_name = isset($arr_attributes['expert_chat_name']) ? $arr_attributes['expert_chat_name'] : '';
                            }

                            $group_chat_image = url('/public/uploads/front/profile/default_profile_image.png');
                            if( isset($arr_attributes['is_group_chat']) && $arr_attributes['is_group_chat'] == '1' ){
                                $chat_name = $chat_name .' | '. config('app.project.name'). ' Admin';
                                $group_chat_image = url('/public/front/images/default_group_chat.png');
                            }
                            
                            $arr_attributes['redirect_url']       = $redirect_url;
                            $arr_attributes['chat_name']          = $chat_name;
                            $arr_attributes['last_updated_date']  = $updated_date;
                            $arr_attributes['group_chat_image']   = $group_chat_image;
                            
                            $arr_chat_list['project-'.$arr_attributes['project_id']]['chat_list'][$arr_attributes['project_bid_id']] = $arr_attributes; 

                        }
                    }

                    // made the list for contest chat
                    if(isset($arr_attributes['is_contest_chat']) && $arr_attributes['is_contest_chat'] == '1' && $remote_user_id == $login_user_id ) {
                        
                        if( isset($arr_chat_list['contest-'.$arr_attributes['contest_id']]['id']) == false ) {
                            $arr_chat_list['contest-'.$arr_attributes['contest_id']]['id'] = isset($arr_attributes['contest_id']) ? $arr_attributes['contest_id'] : ''; 
                        }

                        $updated_date = '';
                        if(isset($obj_channel_resource->dateUpdated)){
                            $arrdateUpdated = json_decode(json_encode($obj_channel_resource->dateUpdated),true);
                            if(isset($arrdateUpdated['date'])){
                                $updated_date = date('Y-m-d H:i:s',strtotime($arrdateUpdated['date']));
                            }
                        }

                        $arr_chat_list['contest-'.$arr_attributes['contest_id']]['group_name'] = isset($arr_attributes['contest_title']) ? $arr_attributes['contest_title'] : '';                         
                        
                        if( isset($arr_chat_list['contest-'.$arr_attributes['contest_id']]['last_updated_date']) && $arr_chat_list['contest-'.$arr_attributes['contest_id']]['last_updated_date']!='' && strtotime($arr_chat_list['contest-'.$arr_attributes['contest_id']]['last_updated_date']) >= strtotime($updated_date) ) {
                            $arr_chat_list['contest-'.$arr_attributes['contest_id']]['last_updated_date'] = $arr_chat_list['contest-'.$arr_attributes['contest_id']]['last_updated_date'];
                        } else {
                            $arr_chat_list['contest-'.$arr_attributes['contest_id']]['last_updated_date'] = isset($updated_date) ? $updated_date : ''; 
                        }

                        $is_group_chat = isset($arr_attributes['is_group_chat']) ? $arr_attributes['is_group_chat'] : '0';
                        if( isset($arr_chat_list['contest-'.$arr_attributes['contest_id']]['is_group_chat']) && $arr_chat_list['contest-'.$arr_attributes['contest_id']]['is_group_chat']=='1' ) {
                            $arr_chat_list['contest-'.$arr_attributes['contest_id']]['is_group_chat'] = '1';
                        } else {
                            $arr_chat_list['contest-'.$arr_attributes['contest_id']]['is_group_chat'] = $is_group_chat; 
                        }

                        $arr_chat_list['contest-'.$arr_attributes['contest_id']]['group_chat_image'] = url('/public/front/images/default_group_chat.png');

                        if( isset($arr_chat_list['contest-'.$arr_attributes['contest_id']]['chat_list'][$arr_attributes['contest_entry_id']]) == false ) {
                            
                            $arr_attributes['sid']           = isset($obj_channel_resource->sid) ? $obj_channel_resource->sid : '';
                            $arr_attributes['message_count'] = isset( $obj_channel_resource->messagesCount) ? $obj_channel_resource->messagesCount : '';
                            
                            $redirect_url = 'javascript:void(0);';
                            if(isset($user_type) && isset($arr_attributes['contest_entry_id']) ){
                              $redirect_url = url('/') . '/' . $user_type . '/twilio-chat/contest/' . base64_encode($arr_attributes['contest_entry_id']);
                            }

                            $chat_name = '';
                            if(isset($user_type) && $user_type == 'client') {
                                $chat_name = isset($arr_attributes['client_chat_name']) ? $arr_attributes['client_chat_name'] : '';
                            } else if(isset($user_type) && $user_type == 'expert') {
                                $chat_name = isset($arr_attributes['expert_chat_name']) ? $arr_attributes['expert_chat_name'] : '';
                            }

                            $group_chat_image = url('/public/uploads/front/profile/default_profile_image.png');
                            if( isset($arr_attributes['is_group_chat']) && $arr_attributes['is_group_chat'] == '1' ){
                                $chat_name = $chat_name .' | '. config('app.project.name'). ' Admin';
                                $group_chat_image = url('/public/front/images/default_group_chat.png');
                            }

                            $arr_attributes['redirect_url'] = $redirect_url;
                            $arr_attributes['chat_name'] = $chat_name;
                            $arr_attributes['group_chat_image']  = $group_chat_image;

                            $arr_chat_list['contest-'.$arr_attributes['contest_id']]['chat_list'][$arr_attributes['contest_entry_id']] = $arr_attributes; 

                        }
                    }

                }
            }
        }
        
        if(isset($arr_chat_list) && count($arr_chat_list)>0){
            usort($arr_chat_list, function($a, $b) {
                $a_last_updated_date = isset($a['last_updated_date']) ? strtotime($a['last_updated_date']) : 0;
                $b_last_updated_date = isset($b['last_updated_date']) ? strtotime($b['last_updated_date']) : 0;
                return $a_last_updated_date < $b_last_updated_date;
            });
        }

        return $arr_chat_list;            
    }

    private function rebuild_message_list($obj_message_list,$login_user_timezone,$arr_chat_user) {
        if(isset($obj_message_list) && count($obj_message_list)>0 ) {

            $system_timezone   = date_default_timezone_get();
            $current_timezone = 'UTC';
            $local_timezone   = $current_timezone;

            date_default_timezone_set($local_timezone);
            $local = date("Y-m-d h:i:s A");
     
            date_default_timezone_set("GMT");
            $gmt = date("Y-m-d h:i:s A");
     
            $require_timezone = $login_user_timezone;
            date_default_timezone_set($require_timezone);
            $required = date("Y-m-d h:i:s A");
     
            foreach ($obj_message_list as $key => $obj_value) {

                $arrDateCreated = json_decode(json_encode($obj_value->dateCreated),true);
                if(isset($arrDateCreated['date']) && isset($login_user_timezone)){
                  
                    $diff1 = (strtotime($gmt) - strtotime($local));
                    $diff2 = (strtotime($required) - strtotime($gmt));

                    $datetime1  = new \DateTime();
                    $date       = new \DateTime($arrDateCreated['date']);

                    $date->modify("+$diff1 seconds");
                    $date->modify("+$diff2 seconds");
                    $timestamp = $date->format("D, d M h:i A");
                    
                    $interval   = $datetime1->diff($date);
                    $from_user_name = '';

                    $profile_image = url('/public/uploads/front/profile/default_profile_image.png');
                    if( isset($obj_value->from) && isset($arr_chat_user['client_user_id']) && $obj_value->from == $arr_chat_user['client_user_id'] ){
                        $from_user_name = isset($arr_chat_user['client_user_name']) ? $arr_chat_user['client_user_name'] : '';
                        $profile_image  = isset($arr_chat_user['client_profile_image']) ? $arr_chat_user['client_profile_image'] : url('/public/uploads/front/profile/default_profile_image.png'); 
                    } else if( isset($obj_value->from) && isset($arr_chat_user['expert_user_id']) && $obj_value->from == $arr_chat_user['expert_user_id'] ){
                        $from_user_name = isset($arr_chat_user['expert_user_name']) ? $arr_chat_user['expert_user_name'] : '';
                        $profile_image  = isset($arr_chat_user['expert_profile_image']) ? $arr_chat_user['expert_profile_image'] : url('/public/uploads/front/profile/default_profile_image.png');
                    }  else if( isset($obj_value->from) && isset($arr_chat_user['admin_user_id']) && $obj_value->from == $arr_chat_user['admin_user_id'] ){
                        $from_user_name = isset($arr_chat_user['admin_user_name']) ? $arr_chat_user['admin_user_name'] : '';
                        $profile_image  = isset($arr_chat_user['admin_profile_image']) ? $arr_chat_user['admin_profile_image'] : url('/public/uploads/front/profile/default_profile_image.png');
                    }

                    $obj_message_list[$key]->from_user_name = $from_user_name;
                    $obj_message_list[$key]->profile_image  = $profile_image;
                    $obj_message_list[$key]->formatted_time = $timestamp;
                    $obj_message_list[$key]->time_ago = str_time_ago($interval);
                }
            }

            date_default_timezone_set($system_timezone);
        }

        return $obj_message_list;
    }

    private function update_channels_addition_details($obj_channel_resources) {
        if(isset($obj_channel_resources)){
            foreach ($obj_channel_resources as $key => $value) {
                if($value->uniqueName == 'general' || $value->uniqueName == NULL ){
                    continue;
                }
                
                $arr_attributes = json_decode($value->attributes,true);
                if(isset($arr_attributes['is_project_chat']) && $arr_attributes['is_project_chat'] == '1') {
                    if(isset($arr_attributes['project_bid_id']) && isset($arr_attributes['project_id'])){

                        $obj_projects_bids = $this->ProjectsBidsModel
                                            ->with('project_details.client_details')
                                            ->where('id',$arr_attributes['project_bid_id'])
                                            ->where('project_id',$arr_attributes['project_id'])
                                            ->first();

                        if($obj_projects_bids == false) {
                            continue;
                        }

                        $arr_projects_bid = $obj_projects_bids->toArray();

                        $project_id   = isset($arr_projects_bid['project_id']) ? $arr_projects_bid['project_id'] : '';

                        $arr_channel_details                     = [];
                        $arr_channel_details['project_id']       = $project_id;
                        $arr_channel_details['project_name']     = isset($arr_projects_bid['project_details']['project_name']) ? $arr_projects_bid['project_details']['project_name'] : '';
                        $arr_channel_details['project_bid_id']   = isset($arr_projects_bid['id']) ? $arr_projects_bid['id'] : '';
                        $arr_channel_details['is_project_chat']  = '1';
                        $arr_channel_details['client_id']        = isset($arr_projects_bid['project_details']['client_details']['id']) ? $arr_projects_bid['project_details']['client_details']['id'] : '';;
                        $arr_channel_details['expert_id']        = isset($arr_projects_bid['expert_user_id']) ? $arr_projects_bid['expert_user_id'] : '';

                        $expert_first_name = isset($arr_projects_bid['role_info']['first_name']) ? $arr_projects_bid['role_info']['first_name'] : '';
                        $expert_last_name  = isset($arr_projects_bid['role_info']['last_name']) ? substr($arr_projects_bid['role_info']['last_name'], 0, 1) : '';
                        $expert_user_id    = isset($arr_projects_bid['expert_user_id']) ? $arr_projects_bid['expert_user_id'] : 0;
                        $expert_user_name  = $expert_first_name . ' ' . $expert_last_name;        

                        $client_first_name = isset($arr_projects_bid['project_details']['client_details']['role_info']['first_name']) ? $arr_projects_bid['project_details']['client_details']['role_info']['first_name'] : '';
                        $client_last_name  = isset($arr_projects_bid['project_details']['client_details']['role_info']['last_name']) ? substr($arr_projects_bid['project_details']['client_details']['role_info']['last_name'], 0, 1) : '';
                        $client_user_id    = isset($arr_projects_bid['project_details']['client_user_id']) ? $arr_projects_bid['project_details']['client_user_id'] : 0;
                        $client_user_name  = $client_first_name . ' ' . $client_last_name;
                        
                        $client_chat_name = $expert_user_name;
                        $expert_chat_name = $client_user_name;

                        $arr_channel_details['project_channel_name'] = $value->uniqueName;
                        $arr_channel_details['client_chat_name'] = $client_chat_name;
                        $arr_channel_details['expert_chat_name'] = $expert_chat_name;                    
       
                        $this->TwilioChatService->update_channel_resource($arr_channel_details);
                    }
                } 

                if(isset($arr_attributes['is_contest_chat']) && $arr_attributes['is_contest_chat'] == '1') {

                    if(isset($arr_attributes['contest_id']) && isset($arr_attributes['contest_entry_id'])){

                        $obj_contest_entry = $this->ContestEntryModel
                                                            ->where('id',$arr_attributes['contest_entry_id'])
                                                            ->where('contest_id',$arr_attributes['contest_id'])
                                                            ->with('contest_details','client_user_details','expert_details','expert_details.user_details')      
                                                            ->first();

                        if($obj_contest_entry == false) {
                            continue;
                        }
                        
                        $arr_contest_entry = $obj_contest_entry->toArray(); 

                        $contest_id   = isset($arr_contest_entry['contest_id']) ? $arr_contest_entry['contest_id'] : '';

                        $arr_channel_details                     = [];
                        $arr_channel_details['contest_id']       = $contest_id;
                        $arr_channel_details['contest_title']    = isset($arr_contest_entry['contest_details']['contest_title']) ? $arr_contest_entry['contest_details']['contest_title'] : '';
                        $arr_channel_details['contest_entry_id'] = isset($arr_contest_entry['id']) ? $arr_contest_entry['id'] : '';
                        $arr_channel_details['is_contest_chat']  = '1';
                        $arr_channel_details['client_id']        = isset($arr_contest_entry['client_user_id']) ? $arr_contest_entry['client_user_id'] : '';;
                        $arr_channel_details['expert_id']        = isset($arr_contest_entry['expert_id']) ? $arr_contest_entry['expert_id'] : '';
                        

                        $expert_first_name = isset($arr_contest_entry['expert_details']['first_name']) ? $arr_contest_entry['expert_details']['first_name'] : '';
                        $expert_last_name  = isset($arr_contest_entry['expert_details']['last_name']) ? substr($arr_contest_entry['expert_details']['last_name'], 0, 1) : '';
                        $expert_user_id    = isset($arr_contest_entry['expert_id']) ? $arr_contest_entry['expert_id'] : 0;
                        $expert_user_name  = $expert_first_name . ' ' . $expert_last_name;        

                        $client_first_name = isset($arr_contest_entry['client_user_details']['first_name']) ? $arr_contest_entry['client_user_details']['first_name'] : '';
                        $client_last_name  = isset($arr_contest_entry['client_user_details']['last_name']) ? substr($arr_contest_entry['client_user_details']['last_name'], 0, 1) : '';
                        $client_user_id    = isset($arr_contest_entry['project_details']['client_user_id']) ? $arr_contest_entry['project_details']['client_user_id'] : 0;
                        $client_user_name  = $client_first_name . ' ' . $client_last_name;
                        
                        $client_chat_name = $expert_user_name;
                        $expert_chat_name = $client_user_name;

                        $arr_channel_details['contest_channel_name'] = $value->uniqueName;
                        $arr_channel_details['client_chat_name'] = $client_chat_name;
                        $arr_channel_details['expert_chat_name'] = $expert_chat_name;

                        $this->TwilioChatService->update_channel_resource($arr_channel_details);
                    }
                }
            }
        }
    }
}
