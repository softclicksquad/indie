<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Session;
use Sentinel;
use CreditCard;
use Crypt;
use Validator;

/*Models*/
use App\Models\SubscriptionPacksModel; 
use App\Models\MilestonesModel; 

use App\Models\ProjectpostModel; 

/*Services*/
use App\Common\Services\PaymentService;
use App\Common\Services\PaypalService;
use App\Common\Services\SubscriptionService;
use App\Common\Services\MilestoneService;
use App\Common\Services\MilestoneReleaseService;
use App\Common\Services\BidTopupService;
use App\Common\Services\ProjectPaymentService;
use App\Common\Services\ContestPaymentService;


class PaymentController extends Controller
{
    
    public $arr_view_data;
    
    public function __construct(
    							SubscriptionPacksModel $subscription_packs,
    							MilestonesModel $milestones,
    							ProjectpostModel $post_project,
    							ContestPaymentService $ContestPaymentService
    							)
    { 
      $this->arr_view_data = array();
      if(! $user           = Sentinel::check()){
        return redirect('/');
      }

      $this->user_id = $user->id;
      if($user->inRole('admin')){
      	$this->payment_cancel_url = url(config('app.project.admin_panel_slug')."/payment/cancel");
      } else {
      	$this->payment_cancel_url = url('/').'/payment/cancel';
      }
      
      /*Initialize models*/
      $this->SubscriptionPacksModel = $subscription_packs;
      $this->MilestonesModel        = $milestones;
      $this->ProjectpostModel       = $post_project;
      $this->PaymentService		    = new PaymentService();
      $this->SubscriptionService    = new SubscriptionService();
      $this->MilestoneService 	    = new MilestoneService();
      $this->BidTopupService 	    = new BidTopupService();
      $this->ProjectPaymentService 	= new ProjectPaymentService();
      $this->ContestPaymentService 	= new ContestPaymentService();
      $this->PaypalService = FALSE;
    }

    public function payment_handler(Request $request)
    {
    	// dd($request->All())
    	$arr_rules                       = array();
    	$form_data                       = array();
    	$arr_cc_details                  = array();
        $status                          = FALSE;
        $form_data                       = $request->all(); 

        /*payment_obj_id is id for which user is going to pay like subscription pack or milestone */
        $arr_rules['payment_obj_id']     = "required";
        $arr_rules['payment_method']     = "required";
        /*transaction_type is type for which user making this transaction like subscription pack or milestone  */
		$arr_rules['transaction_type']   = "required";
		//$arr_rules['project_services_cost'] = "required";

		if (isset($form_data) && sizeof($form_data)>0 ) 
		{
			// if payment method is stripe then we have to validate card details
			if (isset($form_data['payment_method']) && $form_data['payment_method']==2) {
				$arr_rules['cardNumber']      = "required";
            	$arr_rules['cardCVC']         = "required";
            	$arr_rules['cardExpiryMonth'] = "required|min:2|max:2";
            	$arr_rules['cardExpiryYear']  = "required|min:4|max:4";	
			}
		}
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }
        
	    if(isset($form_data['payment_method']) && $form_data['payment_method']==2) 
	    { 
	    	/* Validate CC Details if payment mathod is Stripe*/
	        /* CC Number */
	        $cc_number_status = CreditCard::validCreditCard($request->input('cardNumber'));
	        if($cc_number_status['valid']==FALSE)
	        {
	            Session::flash('error',trans('controller_translations.error_invalid_credentials_please_try_again'));
	            return redirect()->back()->withInput($request->all());
	        }

	        $arr_cc_details['cardNumber'] = $request->input('cardNumber');
	        /* CVV Number */
	        $cc_security_number_status = CreditCard::validCvc($request->input('cardCVC'),$cc_number_status['type']);
	        if($cc_security_number_status==FALSE)
	        {
	            Session::flash('error',trans('controller_translations.error_invalid_credentials_please_try_again'));
	            return redirect()->back()->withInput($request->all());
	        }

	        $arr_cc_details['cardCVC'] = $request->input('cardCVC');
	        $cardExpiryMonth           = $request->input('cardExpiryMonth');
	        $cardExpiryYear            = $request->input('cardExpiryYear');

	        if (isset($cardExpiryMonth) && $cardExpiryMonth!="" && isset($cardExpiryYear) && $cardExpiryYear!="") 
	        {   
                $cc_expiry_year_status = CreditCard::validDate(trim($cardExpiryYear),trim($cardExpiryMonth));
                if($cc_expiry_year_status==FALSE)
                {
                    Session::flash('error',trans('controller_translations.error_invalid_credentials_please_try_again'));
                    return redirect()->back()->withInput($request->all());
                } 
                $arr_cc_details['cardExpMonth'] = trim($cardExpiryMonth); 
                $arr_cc_details['cardExpYear']  = trim($cardExpiryYear);
	        }
	        else
	        {
	            Session::flash('error',trans('controller_translations.error_invalid_credentials_please_try_again'));
	            return redirect()->back()->withInput($request->all());
	        }
	   	}
	   	/*end cards validations with CreditCard lib*/

	   	if (isset($form_data['transaction_type']) && $form_data['transaction_type']==1){
	   		// call payment method for subscription
	   		$this->SubscriptionService->payment($form_data);
	   	}
	   	elseif (isset($form_data['payment_method']) && $form_data['transaction_type']==2){
	   		//call payment method for milestone
	   		$this->MilestoneService->payment($form_data);
	   	}
	   	elseif (isset($form_data['expert_user_id']) && isset($form_data['payment_method']) && $form_data['transaction_type']==3){
	   		// paymnet for release milestone
	   		$this->MilestoneReleaseService = new MilestoneReleaseService($form_data['expert_user_id']);
	   		if ($this->MilestoneReleaseService){
	   			$this->MilestoneReleaseService->payment($form_data);	
	   		}
	   	}
	   	elseif (isset($form_data['payment_method']) && $form_data['transaction_type']==4){
	   		//call payment method for top-up bid service
	   		$this->BidTopupService->payment($form_data);
	   	}
	   	elseif (isset($form_data['payment_method']) && $form_data['transaction_type']==5) 
	   	{
	   		//call payment method for project payment service
	   		$this->ProjectPaymentService->payment($form_data);
	   		//dd('Hereeee',$request->all());
	   	}
	   	elseif (isset($form_data['payment_method']) && $form_data['transaction_type']==7) 
	   	{
	   		//call payment method for project payment service
	   		$this->ContestPaymentService->payment($form_data);
	   	}
	   	elseif(isset($form_data['payment_method']) && $form_data['transaction_type']==8) 
	   	{
	   		//call payment method for contest payment service & create contest wallet
	   		$obj_return_data = $this->ContestPaymentService->contest_payment($form_data);
			if( $obj_return_data instanceof \Illuminate\Http\RedirectResponse ) {
	            return redirect($obj_return_data->getTargetUrl());
	        }
	   	}
	   	elseif(isset($form_data['payment_method']) && $form_data['transaction_type']==9) 
	   	{
	   		//call payment method for contest payment service & create contest wallet
	   		$obj_return_data = $this->ContestPaymentService->update_contest_payment($form_data);
			if( $obj_return_data instanceof \Illuminate\Http\RedirectResponse ) {
	            return redirect($obj_return_data->getTargetUrl());
	        }

	   	}
 		return redirect($this->payment_cancel_url)->with('error', 'Payment failed 123');
    }

    public function project_manager_payment_handler(Request $request)
    {
    	$arr_rules                       = array();
    	$form_data                       = array();
    	$arr_cc_details                  = array();
        $status                          = FALSE;
        $form_data                       = $request->all(); 
        /*payment_obj_id is id for which user is going to pay like subscription pack or milestone */
        $arr_rules['payment_obj_id']     = "required";
        $arr_rules['payment_method']     = "required";
		$arr_rules['transaction_type']   = "required";
		//$arr_rules['project_services_cost'] = "required";

		if (isset($form_data) && sizeof($form_data)>0 ) 
		{
			// if payment method is stripe then we have to validate card details
			if (isset($form_data['payment_method']) && $form_data['payment_method']==2) {
				$arr_rules['cardNumber']      = "required";
            	$arr_rules['cardCVC']         = "required";
            	$arr_rules['cardExpiryMonth'] = "required|min:2|max:2";
            	$arr_rules['cardExpiryYear']  = "required|min:4|max:4";	
			}
		}
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

	    if(isset($form_data['payment_method']) && $form_data['payment_method']==2) 
	    { /* Validate CC Details if payment mathod is Stripe*/
	        /* CC Number */
	        $cc_number_status = CreditCard::validCreditCard($request->input('cardNumber'));
	        if($cc_number_status['valid']==FALSE){
	            Session::flash('error',trans('controller_translations.error_invalid_credentials_please_try_again'));
	            return redirect()->back()->withInput($request->all());
	        }
	        $arr_cc_details['cardNumber'] = $request->input('cardNumber');
	        /* CVV Number */
	        $cc_security_number_status = CreditCard::validCvc($request->input('cardCVC'),$cc_number_status['type']);
	        if($cc_security_number_status==FALSE){
	            Session::flash('error',trans('controller_translations.error_invalid_credentials_please_try_again'));
	            return redirect()->back()->withInput($request->all());
	        }
	        $arr_cc_details['cardCVC'] = $request->input('cardCVC');
	        $cardExpiryMonth           = $request->input('cardExpiryMonth');
	        $cardExpiryYear            = $request->input('cardExpiryYear');
	        if (isset($cardExpiryMonth) && $cardExpiryMonth!="" && isset($cardExpiryYear) && $cardExpiryYear!="") 
	        {   
                $cc_expiry_year_status = CreditCard::validDate(trim($cardExpiryYear),trim($cardExpiryMonth));
                if($cc_expiry_year_status==FALSE)
                {
                    Session::flash('error',trans('controller_translations.error_invalid_credentials_please_try_again'));
                    return redirect()->back()->withInput($request->all());
                } 
                $arr_cc_details['cardExpMonth'] = trim($cardExpiryMonth); 
                $arr_cc_details['cardExpYear']  = trim($cardExpiryYear);
	        }
	        else
	        {
	            Session::flash('error',trans('controller_translations.error_invalid_credentials_please_try_again'));
	            return redirect()->back()->withInput($request->all());
	        }
	   	}
	   	/*end cards validations with CreditCard lib*/

	   	if(isset($form_data['payment_method']) && $form_data['transaction_type']==5) 
	   	{
	   		//call payment method for project payment service
	   		$obj_return_data = $this->ProjectPaymentService->project_manager_payment($form_data);
			// dd(Session::all(),$obj_return_data);
	   		if( $obj_return_data instanceof \Illuminate\Http\RedirectResponse ) {
	            return redirect($obj_return_data->getTargetUrl());
	        }
	   	}

 		return redirect($this->payment_cancel_url)->with('error', 'Payment failed');
    }

    public function charge_handler(Request $request)
    {	
    	$payment_id="";
    	$payment_id           = Session::get('paypal_payment_id');
    	$transaction_type     = Session::get('transaction_type');
    	$expert_id_for_paypal = Session::get('expert_id_for_paypal');
    	
	    // clear the session payment ID
	    Session::forget('paypal_payment_id');
	    Session::forget('transaction_type');
	    Session::forget('expert_id_for_paypal');
	    if (empty($request->get('PayerID')) || empty($request->get('token'))){
	        return redirect($this->payment_cancel_url)->with('error', 'Payment failed');
	    }
	    if (!$transaction_type || !$payment_id){
	    	return redirect($this->payment_cancel_url)->with('error', 'Payment failed');
	    }
	    if (isset($transaction_type) && $transaction_type==1 || isset($transaction_type) && $transaction_type==2 || isset($transaction_type) && $transaction_type==4 || isset($transaction_type) && $transaction_type==5) 
	    {
	    	$admin_payment_settings = $this->PaymentService->get_admin_payment_settings();

			 if (isset($admin_payment_settings) && sizeof($admin_payment_settings)>0) 
			 {
			 	if (isset($admin_payment_settings['paypal_client_id']) && isset($admin_payment_settings['paypal_secret_key']) && isset($admin_payment_settings['paypal_payment_mode'])) 
			 	{
		 			$this->PaypalService = new PaypalService($admin_payment_settings['paypal_client_id'],$admin_payment_settings['paypal_secret_key'],$admin_payment_settings['paypal_payment_mode']);

		 			if($this->PaypalService==FALSE)
					{
					 	return redirect($this->payment_cancel_url);
					}
					else
					{
						return $this->PaypalService->getPaymentStatus($payment_id,$request->get('PayerID'));
					}
			 	}
			 }
	    } else if (isset($transaction_type) && $transaction_type==3 && isset($expert_id_for_paypal) && $expert_id_for_paypal!=0)  {
	    	 $expert_payment_settings = $this->PaymentService->get_user_payment_settings($expert_id_for_paypal);
			 if (isset($expert_payment_settings) && sizeof($expert_payment_settings)>0) 
			 {
			 	if (isset($expert_payment_settings['paypal_client_id']) && isset($expert_payment_settings['paypal_secret_key']) && isset($expert_payment_settings['paypal_payment_mode'])) 
			 	{
			 			$this->PaypalService = new PaypalService($expert_payment_settings['paypal_client_id'],$expert_payment_settings['paypal_secret_key'],$expert_payment_settings['paypal_payment_mode']);

		 			 if($this->PaypalService==FALSE)
					 {
					 	return redirect($this->payment_cancel_url);
					 }
					 else
					 {
					 	return $this->PaypalService->getPaymentStatus($payment_id,$request->get('PayerID'));
					 }
			 	}
			}
	    }
    	return redirect($this->payment_cancel_url);
    }

    public function transaction_cancel()
    {
      $this->arr_view_data['page_title'] = "Error";
      return view('front.common.cancel_page',$this->arr_view_data);
    }

    public function transaction_success()
    {
    	$this->arr_view_data['page_title'] = "Success";
    	return view('front.common.success_page',$this->arr_view_data);
    }

    public function transaction_cancel_admin()
    {
      $this->arr_view_data['page_title'] = "Payment Error";
      return view('admin.payment.cancel_page',$this->arr_view_data);
    }

    public function transaction_success_admin()
    {
    	$this->arr_view_data['page_title'] = "Payment Success";
    	return view('admin.payment.success_page',$this->arr_view_data);
    }
}