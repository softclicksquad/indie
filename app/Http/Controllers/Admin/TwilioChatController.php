<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ProjectsBidsModel;
use App\Models\ContestEntryModel;
use App\Models\NotificationsModel;

use App\Common\Services\TwilioChatService; 

use Session;


class TwilioChatController extends Controller
{
    public function __construct()
    {
        $this->ProjectsBidsModel  = new ProjectsBidsModel();
        $this->ContestEntryModel  = new ContestEntryModel();
    	$this->NotificationsModel = new NotificationsModel();

    	$this->TwilioChatService  = new TwilioChatService();

    	$this->default_profile_image   = url('/public/uploads/front/profile/default_profile_image.png');
        $this->profile_img_base_path   = base_path().'/public'.config('app.project.img_path.profile_image');
        $this->profile_img_public_path = url('/public').config('app.project.img_path.profile_image');

      	$this->arr_view_data 	  = [];
      	$this->view_folder_path   = '/twilio-chat/';
      	$this->module_url_path    = url("/admin/twilio-chat");
    }

    public function show_chat_list(){
        
        $obj_user = \Sentinel::check();
        if( $obj_user == false ) {
          return redirect('/login');
        }
        
        $user_type = 'admin';

        if($user_type == false) {
            Session::flash("error",'Invalid user type unable to process request, Please try again.');
            return redirect(url('/'. $user_type .'/dashboard'));
        }


        $this->module_url_path = url('/'. $user_type .'/twilio-chat');

        // $login_user_id  = isset($obj_user->id) ? $obj_user->id : 0;
        $login_user_id = 1;

        $obj_channel_resources = $this->TwilioChatService->fetch_channels_list();
        
        // $this->update_channels_addition_details($obj_channel_resources);
        $arr_chat_list = $this->build_chat_list_data($obj_channel_resources,$login_user_id,$user_type);

        $this->arr_view_data['page_title']       = 'Chat List';
        $this->arr_view_data['login_user_id']    = $login_user_id;
        $this->arr_view_data['login_user_type']  = $user_type;
        $this->arr_view_data['arr_chat_list']    = $arr_chat_list;
        $this->arr_view_data['module_url_path']  = $this->module_url_path;
        $this->arr_view_data['user_type']        = $user_type;

        return view('admin.twilio-chat.new_chat_ui',$this->arr_view_data);
    }

    public function project_bid_chat($project_bid_id){

    	$project_bid_id = base64_decode( $project_bid_id );
    	$obj_user = \Sentinel::check();
    	if( $obj_user == false ) {
          return redirect('admin/login');
        }
        
        $user_type = 'admin';
        
        $this->module_url_path = url('/'. $user_type .'/twilio-chat');

        if($user_type == false) {
			Session::flash("error",'Invalid user type unable to process request, Please try again.');
    		return redirect(url('/'. $user_type .'/dashboard'));
    	}
        
        // $login_user_id  = isset($obj_user->id) ? $obj_user->id : 0;
        $login_user_id = 1;
        $login_user_timezone = config('app.timezone');
        
		$obj_projects_bids = $this->ProjectsBidsModel
										->with('project_details.client_details')
    									->where('id',$project_bid_id)
    									->first();

        if($obj_projects_bids == false) {
			Session::flash("error",'Invalid bid details unable to process request, Please try again.');
    		return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
    	}

    	$arr_projects_bid = $obj_projects_bids->toArray(); 

		if(isset($arr_projects_bid['is_admin_chat_initiated']) && $arr_projects_bid['is_admin_chat_initiated'] == 0 ) {
			Session::flash("error",'Invalid chat details requested unable to process request, Please try again.');
    		return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
		}

    	$project_id   = isset($arr_projects_bid['project_id']) ? $arr_projects_bid['project_id'] : '';
    	$client_id    = isset($arr_projects_bid['project_details']['client_details']['id']) ? $arr_projects_bid['project_details']['client_details']['id'] : '';
    	$expert_id    = isset($arr_projects_bid['expert_user_id']) ? $arr_projects_bid['expert_user_id'] : '';

        $project_channel_name = '#' . $project_id .'-'.$project_bid_id .'-'.$client_id .'-'.$expert_id; 
        
        $obj_channel_resource = $this->TwilioChatService->get_channel_resource_details_by_sid( $project_channel_name );
       
        if($obj_channel_resource == NULL ) {
            Session::flash("error",'Failed to load channel resource, Please try again.');
            return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
        }

		$channel_resource_sid = isset($obj_channel_resource->sid) ? $obj_channel_resource->sid : '';

        $arr_attributes =  isset($obj_channel_resource->attributes) ? json_decode($obj_channel_resource->attributes,true) : [];
        if(isset($arr_attributes) && count($arr_attributes)>0) {
        	if(isset($arr_attributes['is_group_chat']) == false ){
        		$arr_attributes['is_group_chat'] = '1';
				$arr_attributes['admin_user_id'] = $login_user_id;
        	}
            $this->TwilioChatService->update_channel_resource($arr_attributes);
        	$obj_channel_resource = $this->TwilioChatService->get_channel_resource_details_by_sid( $channel_resource_sid );
	        if($obj_channel_resource == NULL ) {
	            Session::flash("error",'Failed to load channel resource, Please try again.');
	            return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
	        }
        }

        $access_token = $this->TwilioChatService->genrate_access_token( $login_user_id );
        if($access_token == false ){
            Session::flash("error",'Failed to genrate access token, Please try again.');
            return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
        }

        $client_profile_image = $this->default_profile_image;
        $expert_profile_image   = $this->default_profile_image;

        $expert_first_name = isset($arr_projects_bid['role_info']['first_name']) ? $arr_projects_bid['role_info']['first_name'] : '';
        $expert_last_name  = isset($arr_projects_bid['role_info']['last_name']) ? substr($arr_projects_bid['role_info']['last_name'], 0, 1) : '';
        $expert_user_id    = isset($arr_projects_bid['expert_user_id']) ? $arr_projects_bid['expert_user_id'] : 0;
        $expert_user_name  = $expert_first_name . ' ' . $expert_last_name;        

        $client_first_name = isset($arr_projects_bid['project_details']['client_details']['role_info']['first_name']) ? $arr_projects_bid['project_details']['client_details']['role_info']['first_name'] : '';
        $client_last_name  = isset($arr_projects_bid['project_details']['client_details']['role_info']['last_name']) ? substr($arr_projects_bid['project_details']['client_details']['role_info']['last_name'], 0, 1) : '';
        $client_user_id    = isset($arr_projects_bid['project_details']['client_user_id']) ? $arr_projects_bid['project_details']['client_user_id'] : 0;
        $client_user_name  = $client_first_name . ' ' . $client_last_name;

        if( isset($arr_projects_bid['project_details']['client_details']['role_info']['profile_image']) && $arr_projects_bid['project_details']['client_details']['role_info']['profile_image']!='' && file_exists($this->profile_img_base_path.$arr_projects_bid['project_details']['client_details']['role_info']['profile_image'])){
            $client_profile_image = $this->profile_img_public_path.$arr_projects_bid['project_details']['client_details']['role_info']['profile_image'];
        }

        if( isset($arr_projects_bid['role_info']['profile_image']) && $arr_projects_bid['role_info']['profile_image']!='' && file_exists($this->profile_img_base_path.$arr_projects_bid['role_info']['profile_image'])){
            $expert_profile_image   = $this->profile_img_public_path.$arr_projects_bid['role_info']['profile_image'];
        }

        $arr_chat_user = 
            [
                'expert_user_id'   => $expert_user_id,
                'expert_user_name' => $expert_user_name,
                'expert_profile_image' => $expert_profile_image,
                'client_user_id'   => $client_user_id,
                'client_user_name' => $client_user_name,
                'client_profile_image' => $client_profile_image,
                'admin_user_id'        => 1,
                'admin_user_name'      => config('app.project.name') . ' Admin',
                'admin_profile_image'  => url('/public/front/images/default_admin_image.jpg')
            ];

        $obj_message_list = $this->TwilioChatService->fetch_message_list($channel_resource_sid);
		$obj_message_list = $this->rebuild_message_list($obj_message_list,$login_user_timezone,$arr_chat_user);

        // load channel list and chat list
        $obj_channel_resources = $this->TwilioChatService->fetch_channels_list();
        
        $arr_chat_list         = $this->build_chat_list_data($obj_channel_resources,$login_user_id,$user_type);

        $this->arr_view_data['page_title']              = 'Project Bid Chat';
        $this->arr_view_data['login_user_type']         = $user_type;
        $this->arr_view_data['from_user_id']            = $login_user_id;
        $this->arr_view_data['login_user_type']         = $user_type;
        $this->arr_view_data['login_user_timezone']     = $login_user_timezone;

    	$this->arr_view_data['access_token']             = $access_token;
    	$this->arr_view_data['channel_resource_sid']     = $channel_resource_sid;
    	$this->arr_view_data['obj_channel_resource']     = $obj_channel_resource;
        $this->arr_view_data['obj_message_list']         = $obj_message_list;
    	$this->arr_view_data['arr_chat_list']            = $arr_chat_list;
        $this->arr_view_data['is_project_chat']          = '1';
        $this->arr_view_data['arr_data']                 = $arr_projects_bid;
        $this->arr_view_data['arr_chat_user']            = $arr_chat_user;

        $this->arr_view_data['module_url_path']  = $this->module_url_path;
        $this->arr_view_data['user_type']  = $user_type;

        return view('admin.twilio-chat.new_common_chat_ui',$this->arr_view_data);   
    }

    public function contest_chat($contest_entry_id){
        $contest_entry_id = base64_decode($contest_entry_id);
        
        $obj_user = \Sentinel::check();
        if( $obj_user == false ) {
          return redirect('admin/login');
        }
        
        $user_type = 'admin';
        /*if($obj_user->inRole('admin')){
            $user_type = 'admin';
        }*/
        
        $this->module_url_path = url('/'. $user_type .'/twilio-chat');

        if($user_type == false) {
            Session::flash("error",'Invalid user type unable to process request, Please try again.');
            return redirect(url('/'. $user_type .'/dashboard'));
        }

        $arr_from_user = isset($obj_user) ? $obj_user->toArray() : [];
        
        // $login_user_id  = isset($obj_user->id) ? $obj_user->id : 0;
        $login_user_id  = 1;
        $login_user_timezone = config('app.timezone');

        $obj_contest_entry = $this->ContestEntryModel->where('id',$contest_entry_id)
                                                        ->with('contest_details','client_user_details','expert_details','expert_details.user_details')      
                                                        ->first();

        if($obj_contest_entry == false) {
            Session::flash("error",'Invalid contest entry details unable to process request, Please try again.');
            return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
        }

        $arr_contest_entry = $obj_contest_entry->toArray(); 

        if(isset($arr_contest_entry['is_admin_chat_initiated']) && $arr_contest_entry['is_admin_chat_initiated'] == 0 ) {
            Session::flash("error",'Invalid chat details requested unable to process request, Please try again.');
            return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
        }

        $contest_id = isset($arr_contest_entry['contest_id']) ? $arr_contest_entry['contest_id'] : '';
        $client_id  = isset($arr_contest_entry['client_user_id']) ? $arr_contest_entry['client_user_id'] : '';;
        $expert_id  = isset($arr_contest_entry['expert_id']) ? $arr_contest_entry['expert_id'] : '';

        $contest_channel_name = '#contest-' . $contest_id .'-'.$contest_entry_id .'-'.$client_id .'-'.$expert_id; 

        $obj_channel_resource = $this->TwilioChatService->get_channel_resource_details_by_sid( $contest_channel_name );

        if($obj_channel_resource == NULL ) {
            Session::flash("error",'Failed to load channel resource, Please try again.');
            return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
        }

        $channel_resource_sid = isset($obj_channel_resource->sid) ? $obj_channel_resource->sid : '';

        $arr_attributes =  isset($obj_channel_resource->attributes) ? json_decode($obj_channel_resource->attributes,true) : [];
        if(isset($arr_attributes) && count($arr_attributes)>0) {
            if(isset($arr_attributes['is_group_chat']) == false ){
                $arr_attributes['is_group_chat'] = '1';
                $arr_attributes['admin_user_id'] = $login_user_id;
            }
            $this->TwilioChatService->update_channel_resource($arr_attributes);
            $obj_channel_resource = $this->TwilioChatService->get_channel_resource_details_by_sid( $channel_resource_sid );
            if($obj_channel_resource == NULL ) {
                Session::flash("error",'Failed to load channel resource, Please try again.');
                return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
            }
        }

        $access_token = $this->TwilioChatService->genrate_access_token( $login_user_id );
        if($access_token == false ){
            Session::flash("error",'Failed to genrate access token, Please try again.');
            return redirect(url('/'. $user_type .'/twilio-chat/chat_list'));
        }


        $expert_first_name = isset($arr_contest_entry['expert_details']['first_name']) ? $arr_contest_entry['expert_details']['first_name'] : '';
        $expert_last_name  = isset($arr_contest_entry['expert_details']['last_name']) ? substr($arr_contest_entry['expert_details']['last_name'], 0, 1) : '';
        $expert_user_id    = isset($arr_contest_entry['expert_id']) ? $arr_contest_entry['expert_id'] : 0;
        $expert_user_name  = $expert_first_name . ' ' . $expert_last_name;        

        $client_first_name = isset($arr_contest_entry['client_user_details']['first_name']) ? $arr_contest_entry['client_user_details']['first_name'] : '';
        $client_last_name  = isset($arr_contest_entry['client_user_details']['last_name']) ? substr($arr_contest_entry['client_user_details']['last_name'], 0, 1) : '';
        $client_user_id    = isset($arr_contest_entry['client_user_id']) ? $arr_contest_entry['client_user_id'] : 0;
        $client_user_name  = $client_first_name . ' ' . $client_last_name;

        $client_profile_image = $this->default_profile_image;
        $expert_profile_image   = $this->default_profile_image;
        if( isset($arr_contest_entry['expert_details']['profile_image']) && $arr_contest_entry['expert_details']['profile_image']!='' && file_exists($this->profile_img_base_path.$arr_contest_entry['expert_details']['profile_image'])){
            $expert_profile_image   = $this->profile_img_public_path.$arr_contest_entry['expert_details']['profile_image'];
        }

        if( isset($arr_contest_entry['client_user_details']['profile_image']) && $arr_contest_entry['client_user_details']['profile_image']!='' && file_exists($this->profile_img_base_path.$arr_contest_entry['client_user_details']['profile_image'])){
            $client_profile_image = $this->profile_img_public_path.$arr_contest_entry['client_user_details']['profile_image'];
        }

        $arr_chat_user = 
            [
                'expert_user_id'   => $expert_user_id,
                'expert_user_name' => $expert_user_name,
                'expert_profile_image' => $expert_profile_image,
                'client_user_id'   => $client_user_id,
                'client_user_name' => $client_user_name,
                'client_profile_image' => $client_profile_image,
                'admin_user_id'        => 1,
                'admin_user_name'      => config('app.project.name') . ' Admin',
                'admin_profile_image'  => url('/public/front/images/default_admin_image.jpg')
            ];
        
        $obj_message_list = $this->TwilioChatService->fetch_message_list($channel_resource_sid);
        $obj_message_list = $this->rebuild_message_list($obj_message_list,$login_user_timezone,$arr_chat_user);
        
        // load channel list and chat list
        $obj_channel_resources = $this->TwilioChatService->fetch_channels_list();
        
        $arr_chat_list         = $this->build_chat_list_data($obj_channel_resources,$login_user_id,$user_type);

        $this->arr_view_data['page_title']              = 'Contest Entry Chat';
        $this->arr_view_data['login_user_type']         = $user_type;
        $this->arr_view_data['from_user_id']            = $login_user_id;
        $this->arr_view_data['login_user_type']         = $user_type;
        $this->arr_view_data['login_user_timezone']     = $login_user_timezone;

        $this->arr_view_data['access_token']             = $access_token;
        $this->arr_view_data['channel_resource_sid']     = $channel_resource_sid;
        $this->arr_view_data['obj_channel_resource']     = $obj_channel_resource;
        $this->arr_view_data['obj_message_list']         = $obj_message_list;
        $this->arr_view_data['arr_chat_list']            = $arr_chat_list;
        $this->arr_view_data['is_contest_chat']          = '1';
        $this->arr_view_data['arr_data']                 = $arr_contest_entry;
        $this->arr_view_data['arr_chat_user']            = $arr_chat_user;

        $this->arr_view_data['module_url_path']  = $this->module_url_path;
        $this->arr_view_data['user_type']  = $user_type;

        return view('admin.twilio-chat.new_common_chat_ui',$this->arr_view_data);
           
    }

    private function rebuild_message_list($obj_message_list,$login_user_timezone,$arr_chat_user) {
        if(isset($obj_message_list) && count($obj_message_list)>0 ) {

            $system_timezone   = date_default_timezone_get();
            $current_timezone = 'UTC';
            $local_timezone   = $current_timezone;

            date_default_timezone_set($local_timezone);
            $local = date("Y-m-d h:i:s A");
     
            date_default_timezone_set("GMT");
            $gmt = date("Y-m-d h:i:s A");
     
            $require_timezone = $login_user_timezone;
            date_default_timezone_set($require_timezone);
            $required = date("Y-m-d h:i:s A");
     
            foreach ($obj_message_list as $key => $obj_value) {
                $arrDateCreated = json_decode(json_encode($obj_value->dateCreated),true);
                if(isset($arrDateCreated['date']) && isset($login_user_timezone)){
                  
                    $diff1 = (strtotime($gmt) - strtotime($local));
                    $diff2 = (strtotime($required) - strtotime($gmt));

                    $datetime1  = new \DateTime();
                    $date       = new \DateTime($arrDateCreated['date']);

                    $date->modify("+$diff1 seconds");
                    $date->modify("+$diff2 seconds");
                    $timestamp = $date->format("D, d M h:i A");
                    
                    $interval   = $datetime1->diff($date);

                    $from_user_name = '';
                    $profile_image = url('/public/uploads/front/profile/default_profile_image.png');
                    if( isset($obj_value->from) && isset($arr_chat_user['client_user_id']) && $obj_value->from == $arr_chat_user['client_user_id'] ){
                        $from_user_name = isset($arr_chat_user['client_user_name']) ? $arr_chat_user['client_user_name'] : '';
                        $profile_image  = isset($arr_chat_user['client_profile_image']) ? $arr_chat_user['client_profile_image'] : url('/public/uploads/front/profile/default_profile_image.png'); 
                    } else if( isset($obj_value->from) && isset($arr_chat_user['expert_user_id']) && $obj_value->from == $arr_chat_user['expert_user_id'] ){
                        $from_user_name = isset($arr_chat_user['expert_user_name']) ? $arr_chat_user['expert_user_name'] : '';
                        $profile_image  = isset($arr_chat_user['expert_profile_image']) ? $arr_chat_user['expert_profile_image'] : url('/public/uploads/front/profile/default_profile_image.png');
                    }  else if( isset($obj_value->from) && isset($arr_chat_user['admin_user_id']) && $obj_value->from == $arr_chat_user['admin_user_id'] ){
                        $from_user_name = isset($arr_chat_user['admin_user_name']) ? $arr_chat_user['admin_user_name'] : '';
                        $profile_image  = isset($arr_chat_user['admin_profile_image']) ? $arr_chat_user['admin_profile_image'] : url('/public/uploads/front/profile/default_profile_image.png');
                    }

                    $obj_message_list[$key]->from_user_name = $from_user_name;
                    $obj_message_list[$key]->profile_image  = $profile_image;
                    $obj_message_list[$key]->formatted_time = $timestamp;
                    $obj_message_list[$key]->time_ago = str_time_ago($interval);
                }
            }

            date_default_timezone_set($system_timezone);
        }

        return $obj_message_list;
    }

    private function build_chat_list_data($obj_channel_resources,$login_user_id,$user_type){
        $arr_chat_list = [];
        if(isset($obj_channel_resources) && count($obj_channel_resources)>0) {
            foreach ($obj_channel_resources as $key => $obj_channel_resource) {
                $arr_attributes = json_decode($obj_channel_resource->attributes,true); 
                if( $obj_channel_resource->uniqueName != NULL && isset($arr_attributes) && is_array($arr_attributes) && count($arr_attributes)>0 ) {

                    $remote_user_id = 0;
                    $remote_user_id = isset($arr_attributes['admin_user_id']) ? $arr_attributes['admin_user_id'] : 0;

                    if(isset($arr_attributes['is_group_chat']) && $arr_attributes['is_group_chat'] == '1' && $remote_user_id == $login_user_id ) {
                    
                        // made the list for project chat
                        if(isset($arr_attributes['is_project_chat']) && $arr_attributes['is_project_chat'] == '1') {
                            
                            if( isset($arr_chat_list['project-'.$arr_attributes['project_id']]['id']) == false ) {
                                $arr_chat_list['project-'.$arr_attributes['project_id']]['id'] = isset($arr_attributes['project_id']) ? $arr_attributes['project_id'] : ''; 
                            }

                            $updated_date = '';
                            if(isset($obj_channel_resource->dateUpdated)){
                                $arrdateUpdated = json_decode(json_encode($obj_channel_resource->dateUpdated),true);
                                if(isset($arrdateUpdated['date'])){
                                    $updated_date = date('Y-m-d H:i:s',strtotime($arrdateUpdated['date']));
                                }
                            }
                            
                            $arr_chat_list['project-'.$arr_attributes['project_id']]['group_name'] = isset($arr_attributes['project_name']) ? $arr_attributes['project_name'] : ''; 
                            $arr_chat_list['project-'.$arr_attributes['project_id']]['group_chat_image'] = url('/public/front/images/default_group_chat.png');

                            if( isset($arr_chat_list['project-'.$arr_attributes['project_id']]['last_updated_date']) && $arr_chat_list['project-'.$arr_attributes['project_id']]['last_updated_date']!='' && strtotime($arr_chat_list['project-'.$arr_attributes['project_id']]['last_updated_date']) >= strtotime($updated_date) ) {
                                $arr_chat_list['project-'.$arr_attributes['project_id']]['last_updated_date'] = $arr_chat_list['project-'.$arr_attributes['project_id']]['last_updated_date'];
                            } else {
                                $arr_chat_list['project-'.$arr_attributes['project_id']]['last_updated_date'] = isset($updated_date) ? $updated_date : ''; 
                            }

                            
                            if( isset($arr_chat_list['project-'.$arr_attributes['project_id']]['chat_list'][$arr_attributes['project_bid_id']]) == false ) {
                                
                                $arr_attributes['sid']           = isset($obj_channel_resource->sid) ? $obj_channel_resource->sid : '';
                                $arr_attributes['message_count'] = isset( $obj_channel_resource->messagesCount) ? $obj_channel_resource->messagesCount : '';
                                
                                $redirect_url = 'javascript:void(0);';
                                if(isset($user_type) && isset($arr_attributes['project_bid_id']) ){
                                  $redirect_url = url('/') . '/' . $user_type . '/twilio-chat/projectbid/' . base64_encode($arr_attributes['project_bid_id']);
                                }

                                $client_chat_name = isset($arr_attributes['client_chat_name']) ? $arr_attributes['client_chat_name'] : '';
                                $expert_chat_name = isset($arr_attributes['expert_chat_name']) ? $arr_attributes['expert_chat_name'] : '';
                                $chat_name = $client_chat_name . ' | '.$expert_chat_name;

                                $arr_attributes['redirect_url']      = $redirect_url;
                                $arr_attributes['chat_name']         = $chat_name;
                                $arr_attributes['last_updated_date'] = $updated_date;
                                $arr_attributes['group_chat_image'] = url('/public/front/images/default_group_chat.png');

                                $arr_chat_list['project-'.$arr_attributes['project_id']]['chat_list'][$arr_attributes['project_bid_id']] = $arr_attributes; 

                            }
                        }

                        // made the list for contest chat
                        if(isset($arr_attributes['is_contest_chat']) && $arr_attributes['is_contest_chat'] == '1') {
                            
                            if( isset($arr_chat_list['contest-'.$arr_attributes['contest_id']]['id']) == false ) {
                                $arr_chat_list['contest-'.$arr_attributes['contest_id']]['id'] = isset($arr_attributes['contest_id']) ? $arr_attributes['contest_id'] : ''; 
                            }

                            $updated_date = '';
                            if(isset($obj_channel_resource->dateUpdated)){
                                $arrdateUpdated = json_decode(json_encode($obj_channel_resource->dateUpdated),true);
                                if(isset($arrdateUpdated['date'])){
                                    $updated_date = date('Y-m-d H:i:s',strtotime($arrdateUpdated['date']));
                                }
                            }
                            $arr_chat_list['contest-'.$arr_attributes['contest_id']]['group_chat_image'] = url('/public/front/images/default_group_chat.png');
                            $arr_chat_list['contest-'.$arr_attributes['contest_id']]['group_name'] = isset($arr_attributes['contest_title']) ? $arr_attributes['contest_title'] : ''; 

                            if( isset($arr_chat_list['contest-'.$arr_attributes['contest_id']]['last_updated_date']) && $arr_chat_list['contest-'.$arr_attributes['contest_id']]['last_updated_date']!='' && strtotime($arr_chat_list['contest-'.$arr_attributes['contest_id']]['last_updated_date']) >= strtotime($updated_date) ) {
                                $arr_chat_list['contest-'.$arr_attributes['contest_id']]['last_updated_date'] = $arr_chat_list['contest-'.$arr_attributes['contest_id']]['last_updated_date'];
                            } else {
                                $arr_chat_list['contest-'.$arr_attributes['contest_id']]['last_updated_date'] = isset($updated_date) ? $updated_date : ''; 
                            }
         
                            if( isset($arr_chat_list['contest-'.$arr_attributes['contest_id']]['chat_list'][$arr_attributes['contest_entry_id']]) == false ) {
                                
                                $arr_attributes['sid']           = isset($obj_channel_resource->sid) ? $obj_channel_resource->sid : '';
                                $arr_attributes['message_count'] = isset( $obj_channel_resource->messagesCount) ? $obj_channel_resource->messagesCount : '';
                                
                                $redirect_url = 'javascript:void(0);';
                                if(isset($user_type) && isset($arr_attributes['contest_entry_id']) ){
                                  $redirect_url = url('/') . '/' . $user_type . '/twilio-chat/contest/' . base64_encode($arr_attributes['contest_entry_id']);
                                }

                                $client_chat_name = isset($arr_attributes['client_chat_name']) ? $arr_attributes['client_chat_name'] : '';
                                $expert_chat_name = isset($arr_attributes['expert_chat_name']) ? $arr_attributes['expert_chat_name'] : '';
                                $chat_name = $client_chat_name . ' | '.$expert_chat_name;

                                $arr_attributes['redirect_url'] = $redirect_url;
                                $arr_attributes['chat_name'] = $chat_name;
                                $arr_attributes['group_chat_image'] = url('/public/front/images/default_group_chat.png');

                                $arr_chat_list['contest-'.$arr_attributes['contest_id']]['chat_list'][$arr_attributes['contest_entry_id']] = $arr_attributes; 

                            }
                        }

                    }
                }
            }
        }
        
        if(isset($arr_chat_list) && count($arr_chat_list)>0){
            usort($arr_chat_list, function($a, $b) {
                $a_last_updated_date = isset($a['last_updated_date']) ? strtotime($a['last_updated_date']) : 0;
                $b_last_updated_date = isset($b['last_updated_date']) ? strtotime($b['last_updated_date']) : 0;
                return $a_last_updated_date < $b_last_updated_date;
            });
        }

        return $arr_chat_list;            
    }

}
