<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models\ProjectpostModel;
use App\Models\MilestonesModel;
use App\Models\ClientsModel;
use App\Models\MilestoneRefundRequests;

use Session;
use Validator;
use Sentinel;
class MilestoneRefundController extends Controller
{
   
    public function __construct(ProjectpostModel $projects,
                                MilestonesModel $milestones,
                                MilestoneRefundRequests $MilestoneRefundRequests,
                                ClientsModel $client)
    { 
       $this->ProjectpostModel         = $projects;
       $this->MilestonesModel          = $milestones;
       $this->ClientsModel             = $client;
       $this->MilestoneRefundRequests  = $MilestoneRefundRequests;
       $this->module_url_path          = url(config('app.project.admin_panel_slug')."/milestones/milestones-refunds");
    }
    /*
     Comments   : Show all milestones with refund requests.
     Author     : Tushar A.
    */
	  public function index() 
      { 
        $refund_requests                         = [];
        $this->arr_view_data['page_title']       = "Refund Requests";
        $this->arr_view_data['module_title']     = "Milestones";
        $this->arr_view_data['module_url_path']  = $this->module_url_path;
        $get_refund_requests                     = $this->MilestoneRefundRequests
                                                        ->with(['milestone_details','project_details'=>function($q){
                                                            $q->select('id','mp_job_wallet_id','client_user_id','project_name');
                                                        }]);
                                                        if(isset($_GET['mid']) && $_GET['mid']!=""){
                                                        $get_refund_requests->where('milestone_id',base64_decode($_GET['mid']));
                                                        }
                                                        $get_refund_requests->orderBy('id','DESC');
                                                        $get_refund_requests = $get_refund_requests->get();


        if(isset($get_refund_requests)){
          $refund_requests = $get_refund_requests->toArray();
        }
        $this->arr_view_data['refund_requests']  = $refund_requests;
        return view('admin.milestone_requests.milestones_refund_requests',$this->arr_view_data);
    }

} // 
