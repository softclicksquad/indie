<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\NotificationsModel;
use App\Models\SupportTicketModel;
use App\Models\SupportTicketFilesModel;
use App\Models\UserModel;

use DataTables;
use Validator;
use Sentinel;
use Session;
use Mail;
use DB;


class SupportTicketController extends Controller
{
	function __construct()
	{  	
		$this->arr_view_data                = [];
		$this->admin_panel_slug             = config('app.project.admin_panel_slug');
		$this->admin_url_path               = url(config('app.project.admin_panel_slug'));
		$this->module_url_path              = $this->admin_url_path."/support_ticket";
		$this->module_title                 = "Support Ticket";
		$this->module_view_folder			= "admin.support_ticket";
		$this->module_icon                  = "fa fa-ticket";
		$this->UserModel					= new UserModel();
		$this->ip_address                   = isset($_SERVER['REMOTE_ADDR'])?$_SERVER['REMOTE_ADDR']:false;  
		$this->BaseModel					= new SupportTicketModel();
		$this->NotificationsModel			= new NotificationsModel();
		$this->SupportTicketFilesModel		= new SupportTicketFilesModel();
		$this->admin_profile_image_base_img_path   = base_path().config('app.project.img_path.admin_profile_image');
		$this->admin_profile_image_public_img_path = url('/').config('app.project.img_path.admin_profile_image');
		$this->user_profile_base_img_path     = base_path().config('app.project.img_path.user_profile_image');
		$this->user_profile_public_img_path   = url('/').config('app.project.img_path.user_profile_image');
		$this->support_ticket_base_img_path   = base_path().config('app.project.img_path.support_ticket_files');
		$this->support_ticket_public_img_path = url('/').config('app.project.img_path.support_ticket_files');
		$this->file_format_base_img_path    = base_path().config('app.project.img_path.file_format_image');
		$this->file_format_public_img_path  = url('/').config('app.project.img_path.file_format_image');
	}

	public function index(Request $request)
	{
		$admin = Sentinel::check();

		$role = get_user_role($admin->id);

		$obj_ticket = $this->BaseModel->whereHas('get_user_details', function(){})
										->whereHas('get_category', function($q)use($role, $admin){
											if($role == 'subadmin'){
												$q->where('id', $admin->support_category_id);
											}
										})
										->with(['get_user_details','get_category'])
										->where('thread_id',null)
										->orderBy('id','DESC');		

		if($request->has('type') && $request->input('type') == 'closed' ){
			$obj_ticket = $obj_ticket->where('status', 'closed');
		}
		elseif($request->has('type') && $request->input('type') == 'resolved' ){
			$obj_ticket = $obj_ticket->where('status', 'resolved');
		}
		elseif($request->has('type') && $request->input('type') == 're-opened' ){
			$obj_ticket = $obj_ticket->where('status', 're_opened');
		}
		else{
			$obj_ticket = $obj_ticket->where('status', 'in_progress');
		}
		
		$obj_ticket = $obj_ticket->get();

		$arr_tickets = [];

        if($obj_ticket)
        {
            $arr_tickets = $obj_ticket->toArray();
        }

        $this->arr_view_data['arr_tickets'] = $arr_tickets;
		//dd($arr_project_manager);
        $this->arr_view_data['page_title'] = "Manage Support Tickets";
        $this->arr_view_data['module_title'] = "Support Ticket";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view($this->module_view_folder.'.index',$this->arr_view_data);
	}
	
	public function view($enc_id)
	{
		$arr_ticket 	= $arr_admin_details = [];
		$ticket_number  = base64_decode($enc_id);

		if(isset($ticket_number) && $ticket_number!="")
		{
			$obj_ticket = $this->BaseModel->with(['get_files'])
							   ->with(['get_user_details'])
							   ->where('ticket_number','=',$ticket_number)
							   ->orderBy('created_at','asc')
							   ->get();
		}

		if($obj_ticket)
		{
			$arr_ticket = $obj_ticket->toArray();
		}

		$obj_admin_details  = login_admin_details();

		if($obj_admin_details)
		{
			$arr_admin_details = $obj_admin_details->toArray();
		}
		
		$this->arr_view_data['arr_ticket']                   = $arr_ticket;
		$this->arr_view_data['ticket_number']                = $ticket_number;
		$this->arr_view_data['arr_admin_details']            = $arr_admin_details;
		$this->arr_view_data['parent_module_icon']           = "fa-home";
		$this->arr_view_data['parent_module_title']          = "Dashboard";
		$this->arr_view_data['parent_module_url']            = $this->admin_url_path.'/dashboard';
		$this->arr_view_data['module_title']                 = str_plural($this->module_title);
		$this->arr_view_data['module_icon']                  = $this->module_icon;
		$this->arr_view_data['module_url']                   = $this->module_url_path;
		$this->arr_view_data['admin_panel_slug']             = $this->admin_panel_slug;
		$this->arr_view_data['sub_module_title']             = 'View '.str_singular($this->module_title);
		$this->arr_view_data['sub_module_icon']              = 'fa fa-eye';
		$this->arr_view_data['module_url_path']              = $this->module_url_path;
		$this->arr_view_data['user_profile_base_img_path']   = $this->user_profile_base_img_path;
		$this->arr_view_data['user_profile_public_img_path'] = $this->user_profile_public_img_path;
		$this->arr_view_data['admin_profile_image_base_img_path']   = $this->admin_profile_image_base_img_path;
		$this->arr_view_data['admin_profile_image_public_img_path'] = $this->admin_profile_image_public_img_path;
		$this->arr_view_data['support_ticket_base_img_path']   = $this->support_ticket_base_img_path;
		$this->arr_view_data['support_ticket_public_img_path'] = $this->support_ticket_public_img_path;
		$this->arr_view_data['file_format_base_img_path']    = $this->file_format_base_img_path;
		$this->arr_view_data['file_format_public_img_path']  = $this->file_format_public_img_path;

		return view($this->module_view_folder.'.view',$this->arr_view_data);
	}

	public function reply($enc_id)
	{
		$arr_ticket = $arr_threads = [];

		$id = base64_decode($enc_id);

		if(isset($id) && $id!="")
		{
			$obj_ticket = $this->BaseModel->with(['get_files'])
							   ->with(['get_user_details', 'get_category'])
							   ->where('id','=',$id)
							   ->first();

			if($obj_ticket)
			{
				$arr_ticket = $obj_ticket->toArray();

				$obj_threads = $this->BaseModel->with(['get_files'])
								   ->with(['get_user_details','get_files'])
								   ->with(['get_admin_details', 'get_admin_profile_details'])
								   ->where('thread_id','=',$obj_ticket->id)
								   ->orderBy('created_at','asc')
								   ->get();
				if($obj_threads){
					$arr_threads = $obj_threads->toArray();
				}
			}
		}

		$admin_img_path = url('/').'/uploads/admin/profile/';
		$user_img_path = url('/').config('app.project.img_path.profile_image');

        $this->arr_view_data['page_title'] 			= "Reply Support Tickets";
        $this->arr_view_data['module_title'] 		= "Support Ticket";
        $this->arr_view_data['admin_img_path'] 		= $admin_img_path;
        $this->arr_view_data['user_img_path'] 		= $user_img_path;
        $this->arr_view_data['support_ticket_path']	= url('/').str_replace("public/","",config('app.project.img_path.support_ticket_files'));
        $this->arr_view_data['module_url_path'] 	= $this->module_url_path;

        $this->arr_view_data['arr_ticket'] 			= $arr_ticket;
        $this->arr_view_data['arr_threads'] 		= $arr_threads;

		return view($this->module_view_folder.'.view',$this->arr_view_data);
	}

	public function submit_reply(Request $request, $enc_id)
	{
		$id = base64_decode($enc_id);

		if(!is_numeric($id)){
			Session::flash('error','Invalid request');
          	return redirect()->back();
		}

		$obj_ticket = $this->BaseModel->with(['get_files'])
							   ->with(['get_user_details'])
							   ->where('id','=',$id)
							   ->first();

		if(!$obj_ticket){
			Session::flash('error','Invalid request');
          	return redirect()->back();
		}

		$arr_rules['reply_msg'] = "required";

        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
        	Session::flash('error','Please enter reply message');
          	return redirect()->back();
            //return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $admin = Sentinel::check();

		$support = new SupportTicketModel;

		//$support->user_id			= isset($admin->id) ? $admin->id : '';
		$support->admin_id			= $admin->id;
		$support->ticket_number		= $obj_ticket->ticket_number;	
		//$support->title				= $request->input('title', null);	
		$support->description		= $request->input('reply_msg', null);	
		$support->thread_id			= isset($obj_ticket->id) ? $obj_ticket->id : '';	

		$support_obj 		   		= $support->save();
		$last_inserted_id	 		= $support->id;
		$support->uploaded_file    	= $request->input('_token', null);

		if($request->hasFile('upload_file'))
		{
			if(count($request->file('upload_file')) > 0)
			{
				foreach($request->file('upload_file') as $file)
				{
					$file_extension = strtolower($file->getClientOriginalExtension());

					$filename = sha1(uniqid().uniqid()) . '.' . $file->getClientOriginalExtension();
					$path     = $this->support_ticket_base_img_path . $filename;

					$isUpload = $file->move($this->support_ticket_base_img_path , $filename);

					if($isUpload)
					{
						$arr_data_file['file']		 		= $filename;
						$arr_data_file['type'] 				= $file_extension;
						$arr_data_file['thread_id'] 		= isset($obj_ticket->id) ? $obj_ticket->id : '';	
						$arr_data_file['support_ticket_id'] = $last_inserted_id;	
					}

					$status = $this->SupportTicketFilesModel->create($arr_data_file);
				}
			}
		}

		if($support_obj)
		{
			$this->BaseModel->where('id', $obj_ticket->id)->update(['last_reply_by' => 'admin']);

			$username = $role = '';

            if(isset($obj_ticket->get_user_details->role_info) && !empty($obj_ticket->get_user_details->role_info)){
                if(isset($obj_ticket->get_user_details->role_info['first_name']) && $obj_ticket->get_user_details->role_info['first_name'] != ''){
                    $username .= $obj_ticket->get_user_details->role_info['first_name'].' ';
                }
                if(isset($obj_ticket->get_user_details->role_info['last_name']) && $obj_ticket->get_user_details->role_info['last_name'] != ''){
                    $username .= $obj_ticket->get_user_details->role_info['last_name'];
                }

                $role = get_user_role($obj_ticket->get_user_details->role_info['user_id']);
            }

			/* Notification to User */
			$arr_client_data['user_id']              = isset($obj_ticket->user_id)?$obj_ticket->user_id:'';
			$arr_client_data['user_type']            = $role;
			$arr_client_data['url']                  = $role.'/tickets';
			$arr_client_data['notification_text_en'] = "Admin replied to your ticket : ".$obj_ticket->ticket_number;
			$arr_client_data['notification_text_de'] = "Der Administrator hat auf Ihr Ticket geantwortet : ".$obj_ticket->ticket_number;
			$this->NotificationsModel->create($arr_client_data);
			/* Notification to User */

			/* Email to User */
			$arr_contact_enquiry['arr_user'] = $username;
			$arr_contact_enquiry['message_content'] = "Admin replied to you ticket. Ticket No : ".$obj_ticket->ticket_number;
			$arr_contact_enquiry['message_content'] .= "\n\n Please login and check your admin reply.";
			$to_email_id         = isset($obj_ticket->get_user_details) && isset($obj_ticket->get_user_details->email) ? $obj_ticket->get_user_details->email : '';
            $project_name        = config('app.project.name');
            $mail_subject        = "Admin replied to your ticket : ".$obj_ticket->ticket_number;
            $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';

            try{
                Mail::send('admin.email.support_ticket_mail', $arr_contact_enquiry, function ($message) use ($to_email_id,$mail_form,$project_name,$mail_subject) 
                {
                  $message->from($mail_form, $project_name);
                  $message->subject($project_name.' : '.$mail_subject);
                  $message->to($to_email_id);

                });
            }
            catch(\Exception $e){
            }

			Session::flash('success', 'Reply given successfully.');
			return redirect()->back();
		}

		Session::flash('error', 'Error while creating '.str_singular($this->module_title).'.');
		return redirect()->back();
	}

	public function resolve_ticket(Request $request,$enc_id)
	{
		$arr_rules      = $arr_data = array();
		$id 			= base64_decode($enc_id);

		$obj_ticket     = $this->BaseModel->where('id',$id)->first();

		if(!$obj_ticket)
		{
			Session::flash('error', 'Error while resolving '.str_singular($this->module_title).'.');
			return redirect()->back();
		}

		if($this->BaseModel->where('id',$id)->update(['status'=>'resolved']))
		{
			$username = $role = '';

            if(isset($obj_ticket->get_user_details->role_info) && !empty($obj_ticket->get_user_details->role_info)){
                if(isset($obj_ticket->get_user_details->role_info['first_name']) && $obj_ticket->get_user_details->role_info['first_name'] != ''){
                    $username .= $obj_ticket->get_user_details->role_info['first_name'].' ';
                }
                if(isset($obj_ticket->get_user_details->role_info['last_name']) && $obj_ticket->get_user_details->role_info['last_name'] != ''){
                    $username .= $obj_ticket->get_user_details->role_info['last_name'];
                }

                $role = get_user_role($obj_ticket->get_user_details->role_info['user_id']);
            }

			/* Notification to User */
			$arr_client_data['user_id']              = isset($obj_ticket->user_id)?$obj_ticket->user_id:'';
			$arr_client_data['user_type']            = $role;
			$arr_client_data['url']                  = $role.'/tickets';
			$arr_client_data['notification_text_en'] = "Ticket #".$obj_ticket->ticket_number.' resolved by admin.';
			$arr_client_data['notification_text_de'] = "Ticket #".$obj_ticket->ticket_number.' gelöst von admin';
			$this->NotificationsModel->create($arr_client_data);
			/* Notification to User */

			/* Email to User */
			$arr_contact_enquiry['arr_user'] = $username;
			$arr_contact_enquiry['message_content'] = "Ticket #".$obj_ticket->ticket_number.' has been resolved by admin';
			$arr_contact_enquiry['message_content'] .= "\n\n Please login and check your ticket.";
			$to_email_id         = isset($obj_ticket->get_user_details) && isset($obj_ticket->get_user_details->email) ? $obj_ticket->get_user_details->email : '';
            $project_name        = config('app.project.name');
            $mail_subject        = "Ticket resolved by Admin";
            $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';

            try{
                Mail::send('admin.email.support_ticket_mail', $arr_contact_enquiry, function ($message) use ($to_email_id,$mail_form,$project_name,$mail_subject) 
                {
                  $message->from($mail_form, $project_name);
                  $message->subject($project_name.' : '.$mail_subject);
                  $message->to($to_email_id);

                });
            }
            catch(\Exception $e){
            }

			Session::flash('success', 'Ticket resolved successfully.');
			return redirect($this->module_url_path);
		}else{
			Session::flash('error', 'Error while resolving '.str_singular($this->module_title).'.');
			return redirect()->back();
		}

	}
}