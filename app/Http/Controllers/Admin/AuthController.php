<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Validator;
use Sentinel;
use Session;
use Mail;
use App\Models\UserModel;

class AuthController extends Controller
{
    public $arr_view_data;
    public $admin_panel_slug;

     /*
    | Constructor : 
    | auther : Sagar Sainkar
    | Date : 
    | @return \Illuminate\Http\Response
    */

    public function __construct(UserModel $user_model)
    {
      $this->UserModel     = $user_model;
      $this->arr_view_data = [];
      $this->admin_panel_slug = config('app.project.admin_panel_slug');
    }

     /*
    | Login : load login page for admin
    | auther : Sagar Sainkar
    | Date : 
    | @return \Illuminate\Http\Response
    */

    public function login()
    {
    	$page_title = "Admin Login ";
      $this->arr_view_data['admin_panel_slug'] = $this->admin_panel_slug;
      $this->arr_view_data['page_title'] = "Login";
      
    	return view('admin.auth.login',$this->arr_view_data);
    }


    /*
    | process_login : validate admin credentials and login it into system
    | auther : Sagar Sainkar
    | Date : 06/05/2016
    | @return \Illuminate\Http\Response
    */
    public function process_login(Request $request)
    {
    	 $validator = Validator::make($request->all(), [
            'email' => 'required|max:255|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) 
        {
            return redirect(config('app.project.admin_panel_slug').'/login')
                        ->withErrors($validator)
                        ->withInput($request->all());
        }

    		$credentials = [
    		    'email'    => $request->input('email'),
    		    'password' => $request->input('password'),
    		];


        $user = Sentinel::findByCredentials($credentials); // check if user exists

      if($user)
      {
          if ($user->is_active==1) 
          {
            $check_authentication = Sentinel::authenticate($credentials);

           	if($check_authentication)
           	{
           		$user = Sentinel::check();

           		if($user->inRole('admin'))
           		{
                if(Session::has('DISPUTE'))
                {
                  return redirect(config('app.project.admin_panel_slug').'/dispute');
                }

                if(Session::has('COMPLETED'))
                {
                  return redirect(config('app.project.admin_panel_slug').'/projects/completed');
                }

                if(Session::has('MILESTONE_APPROVED') && Session::has('PROJECT_ID'))
                {
                  return redirect(config('app.project.admin_panel_slug').'/project_milestones/all/'.Session::get('PROJECT_ID'));
                }     
                
           			return redirect(config('app.project.admin_panel_slug').'/dashboard');
           		}
              else if($user->inRole('subadmin') && $user['permissions']!=null)
              {
                if(Session::has('DISPUTE'))
                {
                  return redirect(config('app.project.admin_panel_slug').'/dispute');
                }

                if(Session::has('COMPLETED'))
                {
                  return redirect(config('app.project.admin_panel_slug').'/projects/completed');
                }

                if(Session::has('MILESTONE_APPROVED') && Session::has('PROJECT_ID'))
                {
                  return redirect(config('app.project.admin_panel_slug').'/project_milestones/all/'.Session::get('PROJECT_ID'));
                }     
                
                return redirect(config('app.project.admin_panel_slug').'/dashboard');
              }
           		else
           		{
                Session::flash('error', trans('controller_translations.text_you_do_not_have_sufficient_privileges'));          
               
           		}
           	}
           	else
           	{
                Session::flash('error',trans('controller_translations.error_invalid_credentials_please_try_again'));
           	} 
          }
          else
          {
              Session::flash('error', trans('controller_translations.error_your_account_is_temporary_blocked_please_login_after_some_time'));
          } 
      }
      else
      {
          Session::flash('error',trans('controller_translations.error_invalid_credentials_please_try_again'));
      } 
      Sentinel::logout();
      return redirect()->back();
    }

  /*
    | Change pass : load view for change password
    | auther :Sagar Sainkar
    | Date : 
    | 
    */ 

    public function change_password()
    {

      $page_title = "Change Password";
      $this->arr_view_data['admin_panel_slug'] = $this->admin_panel_slug;
      $this->arr_view_data['page_title'] = "Change Password";

      return view('admin.auth.change_password',$this->arr_view_data);    
    }

    /*
    | description : validate and update admin password
    | auther :Sagar Sainkar
    | Date : 
    | 
    */ 

    public function update_password(Request $request)
    {
        $arr_rules = array();
        $arr_rules['current_password'] = "required";
        $arr_rules['password'] = "required|confirmed";
        
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $user = Sentinel::check();
        
        $credentials = array();
        $password = trim($request->input('current_password'));
        $credentials['email'] = $user->email;        
        $credentials['password'] = $password;

        if (Sentinel::validateCredentials($user,$credentials)) 
        { 
          $new_credentials = [];
          $new_credentials['password'] = $request->input('password');

          if(Sentinel::update($user,$new_credentials))
          {
            Session::flash('success', trans('controller_translations.success_password_changed_successfully'));
          }
          else
          {
            Session::flash('error', trans('controller_translations.error_problem_occured_while_changing_password'));
          }          
        } 
        else
        {
          Session::flash('error', trans('controller_translations.error_your_current_password_is_invalid'));          
        }       
        
        return redirect()->back(); 
    }

    /*
    | description : Logout admin from system
    | auther :Sagar Sainkar
    | Date : 
    | 
    */ 
    public function logout()
    {
      
      Sentinel::logout();
      return redirect(url($this->admin_panel_slug.'/login'));
    }

  
    /*
    | description : validate email and send forgot password link to admin
    | auther :Sagar Sainkar
    | Date : 
    | 
    */ 

    public function forgot_password(Request $request)
    {
        $arr_rules = array();
        $arr_rules['email'] = "required|email";
        
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }


        
        $credentials = [
            'login' => $request->input('email'),
            ];

        

        if (Sentinel::findByCredentials($credentials)) 
        { 

          $email_id = $request->input('email');
          $user = Sentinel::findByCredentials($credentials);

          if ($user->inRole('admin') || $user->inRole('subadmin')) 
          {

              $secure = TRUE;    
              $bytes = openssl_random_pseudo_bytes(3, $secure);
              $password_token = bin2hex($bytes);

              $user_update = array();
              $user_update['token']= $password_token;

              $update_user = $this->UserModel->where('id',$user->id)->where('email',$email_id)->update($user_update);

              if($update_user)
              {
                  
                  $project_name = config('app.project.name');
                  $data['welcome_text'] = "welcome to ".config('app.project.name');
                  $data['token'] = $password_token;
                  
                  //$mail_from    = isset($website_contact_email)?$website_contact_email:'support@archexperts.com';
                  $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
                  
                  try{
                      Mail::send('admin.email.password_reset_admin', $data, function ($message) use ($email_id,$project_name,$mail_from) {
                          $message->from($mail_from, $project_name);
                          $message->subject($project_name.': Reset Password');
                          $message->to($email_id);

                      });
                      Session::flash('success_fp', trans('controller_translations.success_password_reset_link_is_send_to_your_email_address'));
                  }
                  catch(\Exception $e){
                  Session::Flash('error'   ,trans('controller_translations.text_mail_not_sent'));
                  }
              }
              else
              {
                Session::flash('error_fp', trans('controller_translations.error_problem_occured_while_changing_password'));
              }          
            } 
            else
            {
              Session::flash('error_fp', trans('controller_translations.text_you_do_not_have_sufficient_privileges')); 
            } 
          } 
          else
          {
             Session::flash('error_fp',trans('controller_translations.error_invalid_credentials_please_try_again'));
          }      
        
        return redirect()->back(); 
    }


    /*
    | description : validate forgot password token and load reset password view
    | auther :Sagar Sainkar
    | Date : 
    | 
    */ 

    public function reset_password($token=null)
    {
        if ($token) 
        {
            $user_details = $this->UserModel->where('token',$token)->first();

            if (isset($user_details) && sizeof($user_details)>0) 
            {
               $user_details = $user_details->toArray();

               $page_title = "Reset Password";
               return view('admin.auth.reset_password', compact('user_details','page_title'));
            }
            else
            {
                Session::flash('error', trans('controller_translations.text_invalid_request'));
            }

        } 
        else
        {
            Session::flash('error', trans('controller_translations.text_invalid_request'));
        }

        return redirect(url($this->admin_panel_slug.'/login'));
    }

    /*
    | description : update new password for admin
    | auther :Sagar Sainkar
    | Date : 
    | 
    */ 
    public function update_reset_password($token=null,$enc_id=null,Request $request)
    {
        if ($token && $enc_id) 
        {

            $arr_rules = array();
            
            $arr_rules['password'] = "required|confirmed";
            $arr_rules['password_confirmation'] = "required";
            
            $validator = Validator::make($request->all(),$arr_rules);
            if($validator->fails())
            {
                return redirect()->back()->withErrors($validator)->withInput($request->all());
            }

            $user_id = base64_decode($enc_id);
            $user_details = $this->UserModel->where('token',$token)->where('id',$user_id)->first();

            if (isset($user_details) && sizeof($user_details)>0) 
            {
              $credentials = array();
              $new_credentials['password'] = $request->input('password');
              $user = Sentinel::findById($user_id);

              if($user)
              {
                  if(Sentinel::update($user,$new_credentials))
                  {
                    $update_token = $this->UserModel->where('id',$user_id)->update(['token'=>null]);
                    Session::flash('success', trans('controller_translations.success_your_password_has_been_successfully_reset'));
                  }
                  else
                  {
                    Session::flash('error', trans('controller_translations.error_problem_occured_while_reseting_password'));
                  }
              }
              else
              {
                Session::flash('error', trans('controller_translations.text_invalid_request'));
              }
                   
            }
            else
            {
                Session::flash('error', trans('controller_translations.text_invalid_request'));
            }

        } 
        else
        {
            Session::flash('error', trans('controller_translations.text_invalid_request'));
        }

        return redirect(url($this->admin_panel_slug.'/login'));
    }


}