<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\BlogCategoriesModel;
use DataTables;
use Validator;
use Session;
use Sentinel;

use App\Common\Traits\MultiActionTrait;

class BlogCategoriesController extends Controller
{
    
	//use MultiActionTrait;
	function __construct()
	{
		$this->arr_data                	= [];
		$this->admin_panel_slug        	= 'admin';
		$this->admin_url_path          	= url(config('app.project.admin_panel_slug'));
		$this->module_url_path         	= $this->admin_url_path.'/blogs/category';
		$this->module_title            	= "Blog Categories";
		$this->module_view_folder      	= "admin.blog_categories";
		$this->module_icon             	= "fa fa-cubes";
		$this->BaseModel               	= new BlogCategoriesModel();
		$this->auth                 	= Sentinel::check();
		$this->ip_address              	= isset($_SERVER['REMOTE_ADDR'])?$_SERVER['REMOTE_ADDR']:false;
		$this->category_icon_base_path 	= base_path().config('app.project.image_path.category_icon');
	}

	public function index()
	{
		$arr_blog_cats = [];

		$obj_blog_cats = $this->BaseModel;

		$obj_blog_cats = $obj_blog_cats->orderBy('id','DESC')->paginate(10);

		if($obj_blog_cats){
			$arr_blog_cats = $obj_blog_cats->toArray();
		}

		$this->arr_view_data['page_title']       	= 'Manage '.$this->module_title;
		$this->arr_view_data['module_title']     	= 'Manage '.$this->module_title;
		$this->arr_view_data['page_icon']        	= $this->module_icon;
		$this->arr_view_data['module_icon']      	= $this->module_icon;
		$this->arr_view_data['admin_panel_slug'] 	= $this->admin_panel_slug;
		$this->arr_view_data['module_url_path']  	= $this->module_url_path;
		$this->arr_view_data['arr_blog_cats']  		= $arr_blog_cats;

		return view($this->module_view_folder.'.index',$this->arr_view_data);
	}

	public function load_data(Request $request)
	{

		$search = $request->input('column_filter', null);

		$obj_request_data = $this->BaseModel;
		if(isset($search['q_title']) && $search['q_title']!='')
		{
			$search_term = $search['q_title'];
			$obj_request_data = $obj_request_data->where('name', 'like', '%'.$search_term.'%');
		}

		if(isset($search['q_status']) && $search['q_status']!='')
		{
			$search_term = $search['q_status'];
			$obj_request_data = $obj_request_data->where('is_active', $search_term);
		}

		$obj_request_data = $obj_request_data->orderBy('id','DESC');
		$json_result      = DataTables::of($obj_request_data)->make(true);
		$build_result     = $json_result->getData();

		if(isset($build_result->data) && sizeof($build_result->data)>0)
		{
			foreach ($build_result->data as $key => $data) 
			{
				$built_edit_href = $this->module_url_path.'/edit/'.base64_encode($data->id);
				$title      = isset($data->name)? $data->name:'NA';
				$created_at      = isset($data->created_at)? get_added_on_date($data->created_at):'NA';
				$action_btn      = '';
				$status_btn      = '';

				if($data->is_active != null && $data->is_active == "0")
				{   
					$status_btn = '<a class="btn btn-xs btn-danger" title="In-active" href="'.$this->module_url_path.'/unblock/'.base64_encode($data->id).'" 
					onclick="return confirm_action(this,event,\'Do you really want to activate this record ?\')" >Inactive</a>';
				}
				elseif($data->is_active != null && $data->is_active == "1")
				{
					$status_btn = '<a class="btn btn-xs btn-success" title="Active" href="'.$this->module_url_path.'/block/'.base64_encode($data->id).'" onclick="return confirm_action(this,event,\'Do you really want to inactivate this record ?\')" >Active</a>';
				}

				$action_btn = "<a class='btn btn-default btn-rounded show-tooltip' href='".$built_edit_href."' title='Edit' ><i class='fa fa-pencil-square-o' ></i></a>";

				$build_result->data[$key]->name      = $title;
				$build_result->data[$key]->id         = base64_encode($data->id);
				$build_result->data[$key]->created_at = $created_at;
				$build_result->data[$key]->action_btn = $action_btn;
				$build_result->data[$key]->status_btn = $status_btn;
			}
		}
		return response()->json($build_result);
	}

	public function create()
	{
		$this->arr_view_data['page_title']       = 'Create '.str_singular($this->module_title);
		$this->arr_view_data['page_icon']        = $this->module_icon;
		$this->arr_view_data['module_title']     = 'Manage '.$this->module_title;
		$this->arr_view_data['sub_module_title']     = 'Create '.$this->module_title;
		$this->arr_view_data['sub_module_icon']     = 'fa fa-plus';

		$this->arr_view_data['module_icon']      = $this->module_icon;
		$this->arr_view_data['admin_panel_slug'] = $this->admin_panel_slug;
		$this->arr_view_data['module_url_path']  = $this->module_url_path;

		return view($this->module_view_folder.'.create',$this->arr_view_data);
	}

	public function store(Request $request)
	{
		$arr_rules      = $arr_front_page = array();
		$status         = false;

		$arr_rules['name']      	   = "required";

		$validator = validator::make($request->all(),$arr_rules);

		if ($validator->fails()) 
		{
			return redirect()->back()->withErrors($validator)->withInput();
		}

		$name = $request->input('name', null);

		$arr_data['name']       = filter_data($name);
		$arr_data['slug']       = $this->create_slug($name);
		$arr_data['created_by'] = $this->auth->id;

		$dose_exist = $this->BaseModel->where('name', $arr_data['name'])->count();
		
		if($dose_exist>0)
		{
			Session::flash('error', ''.str_singular($this->module_title).' already exist in system.');
			return redirect($this->module_url_path);
		}

		$status = $this->BaseModel->create($arr_data);

		if($status)
		{
			Session::flash('success', str_singular($this->module_title).' created successfully.');
			return redirect($this->module_url_path);
		}
		Session::flash('error', 'Error while creating '.str_singular($this->module_title).'.');
		return redirect($this->module_url_path);
	}

	private function create_slug($string='')
	{
		$slug = '';
		if($string!='')
		{
			$slug  = str_slug($string);
			$count = $this->BaseModel->where('slug', 'like', '%'.$slug.'%')->count();
			if($count>0)
			{
				return $slug.'-'.$count;
			}
		}
		return $slug;
	}

	public function edit($enc_id='')
	{
		if($enc_id=='')
		{
			return redirect()->back();
		}

		$obj_data = $this->BaseModel->where('id', base64_decode($enc_id))->first(['name', 'icon']);
		$arr_data = [];

		if($obj_data)
		{
			$arr_data = $obj_data->toArray();
		}
		else
		{
			return redirect()->back();
		}

		$this->arr_view_data['page_title']              = 'Edit '.str_singular($this->module_title);
		$this->arr_view_data['page_icon']               = $this->module_icon;
		$this->arr_view_data['module_title']            = 'Manage '.$this->module_title;
		$this->arr_view_data['sub_module_title']        = 'Edit '.$this->module_title;
		$this->arr_view_data['sub_module_icon']         = 'fa fa-pencil-square-o';

		$this->arr_view_data['module_icon']             = $this->module_icon;
		$this->arr_view_data['category_icon_base_path'] = $this->category_icon_base_path;
		$this->arr_view_data['admin_panel_slug']        = $this->admin_panel_slug;
		$this->arr_view_data['module_url_path']         = $this->module_url_path;
		$this->arr_view_data['arr_data']                = $arr_data;
		$this->arr_view_data['enc_id']                  = $enc_id;

		return view($this->module_view_folder.'.edit',$this->arr_view_data);
	}

	public function update(Request $request, $enc_id='')
	{
		$arr_rules      = $arr_front_page = array();
		$status         = false;

		$arr_rules['name']      	   = "required";

		$validator = validator::make($request->all(),$arr_rules);

		if ($validator->fails()) 
		{
			return redirect()->back()->withErrors($validator)->withInput();
		}

		$name = $request->input('name', null);

		$arr_data['name']       = filter_data($name);
		// $arr_data['slug']       = $this->create_slug($name);
		$arr_data['updated_by'] = $this->auth->id;

		$dose_exist = $this->BaseModel->where('name', $arr_data['name'])->where('id','!=', base64_decode($enc_id))->count();
		
		if($dose_exist>0)
		{
			Session::flash('error', ''.str_singular($this->module_title).' already exist in system.');
			return redirect($this->module_url_path);
		}

		$status = $this->BaseModel->where('id', base64_decode($enc_id))->update($arr_data);

		if($status)
		{
			Session::flash('success', str_singular($this->module_title).' updated successfully.');
			return redirect($this->module_url_path);
		}
		Session::flash('error', 'Error while updating '.str_singular($this->module_title).'.');
		return redirect($this->module_url_path);
	}

	public function check_category(Request $request)
	{
		$name   = $request->input('name', null);
		$enc_id = $request->input('enc_id', null);

		$count = $this->BaseModel;

		if(isset($enc_id) && $enc_id!=null)
		{
			$count = $count->where('id', '!=', base64_decode($enc_id));
		}

		$count = $count->where('name', $name)->count();

		if($count>0)
		{
			return response()->json(false);
		}
		return response()->json(true);
	}

	function activate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while blog category activation.');
            return redirect()->back();
        }

        if($this->perform_activate(base64_decode($enc_id)))
        {
            Session::flash('success','Blog Category activated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while blog category activation.');
        }

        return redirect()->back();
    }

    public function deactivate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while blog category deactivation.');
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            Session::flash('success','Blog Category deactivated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while blog category deactivation.');
        }

        return redirect()->back();
    }

    public function perform_activate($id)
    {
        if ($id) 
        {
            $obj_blog = $this->BaseModel->where('id',$id)->first();
            if($obj_blog)
            {
                return $obj_blog->update(['is_active'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_deactivate($id)
    {
        if ($id) 
        {
            $obj_blog = $this->BaseModel->where('id',$id)->first();
            if($obj_blog)
            {
                return $obj_blog->update(['is_active'=>0]);
            }
        }

        return FALSE;
    }

    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            Session::flash('error','please select at least one record.');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            
            if($multi_action=="activate")
            {
            	$this->perform_activate(base64_decode($record_id));
               	Session::flash('success','Blog categoris activated successfully');
            }
            elseif($multi_action=="deactivate")
            {
            	$this->perform_deactivate(base64_decode($record_id));
               	Session::flash('success','Blog categoris blocked successfully.');
            }
           	/* elseif($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));
               Session::flash('success','City(s) deleted successfully.');
            } */
        }

        return redirect()->back();
    }

}
