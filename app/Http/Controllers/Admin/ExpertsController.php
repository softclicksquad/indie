<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models\ExpertsModel;
use App\Models\UserModel;
use App\Models\ProjectpostModel;
use App\Models\CountryModel;
use App\Models\SubscriptionUsersModel;
use Session;
use Validator;
use Sentinel;

class ExpertsController extends Controller
{
    /*
        Auther : Bharat Khairnar
        Comments: controller for manage clients
    */

    public function __construct(ProjectpostModel $projects,ExpertsModel $experts,UserModel $use_model,CountryModel $countries)
    {      
       $this->CountryModel = $countries;
       $this->ExpertsModel = $experts;
       $this->ProjectpostModel = $projects;
       $this->SubscriptionUsersModel = new SubscriptionUsersModel();
       $this->UserModel = $use_model;
       $this->module_url_path = url(config('app.project.admin_panel_slug')."/experts");
       $this->project_attachment_public_path = url('/').config('app.project.img_path.expert_project_attachment');
    }
    /*
        Auther : Bharat Khairnar
        Comments: display all clients
    */
	public function index()
    {
        $obj_expert_user = $this->ExpertsModel
                          ->with(['user_details','subscription_details'=>function($query){
                               $query->select('id','user_id','expiry_at','created_at'); 
                          }])->get();

        if($obj_expert_user != FALSE){
            $arr_expert_user = $obj_expert_user->toArray();
        }
        $this->arr_view_data['arr_expert_user'] = $arr_expert_user;
        $this->arr_view_data['page_title'] = "Manage Experts";
        $this->arr_view_data['module_title'] = "Expert Manage";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.experts.index',$this->arr_view_data);
    }

    public function deleted_expert()
    {
        $arr_deleted_expert = [];
        $deleted_obj_expert = $this->UserModel->with('expert_details')
                                    ->whereHas('roles',function($query){
                                        $query->where('slug','expert');
                                    })
                                    ->onlyTrashed()
                                    ->orderBy('deleted_at','desc')
                                    ->get();
        if($deleted_obj_expert)
        {
            $arr_deleted_expert = $deleted_obj_expert->toArray();
        }


        $this->arr_view_data['arr_deleted_expert'] = $arr_deleted_expert;
        $this->arr_view_data['page_title'] = "Manage Deleted Experts";
        $this->arr_view_data['module_title'] = "Deleted Experts";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.experts.deleted',$this->arr_view_data);
    }

    public function view_deleted_experts($enc_id)
    {
        $id = base64_decode($enc_id);
        $profile_img_path = url('/').'/uploads/front/profile/';

        $obj_expert = $this->ExpertsModel->where('user_id', $id)->with(['user_details','country_details.states','state_details.cities','city_details','expert_skills.skills','expert_categories.categories'])->first();

        $arr_expert = array();

        if($obj_expert)
        {
           $arr_expert = $obj_expert->toArray();
        }

        $arr_countries = array();

        $obj_countries = $this->CountryModel->get();

        if($obj_countries != FALSE)
        {
            $arr_countries = $obj_countries->toArray();
        }

        $this->arr_view_data['arr_countries'] = $arr_countries;
        $this->arr_view_data['enc_id'] = $enc_id;
        $this->arr_view_data['profile_img_path'] = $profile_img_path;
        $this->arr_view_data['arr_expert'] = $arr_expert;
        $this->arr_view_data['page_title'] = "View Expert";
        $this->arr_view_data['module_title'] = "Mange deleted experts";
        $this->arr_view_data['module_url_path'] = $this->module_url_path.'/deleted';
        return view('admin.experts.deleted_view',$this->arr_view_data);  
    }


    /*  
        Auther : Sagar Sainkar
        Comments: display view for Add new project_manager
    */
    public function create()
    {
        $this->arr_view_data['page_title'] = "Create Expert";
        $this->arr_view_data['module_title'] = "Expert";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.experts.create',$this->arr_view_data);
    }
    /*  
        Auther : Sagar Sainkar
        Comments: Add and store project_manager details
    */
    public function store(Request $request)
    {
        $form_data = array();
        $form_data = $request->all();
        $arr_rules['email'] = "required|email|unique:users,email";
        $arr_rules['password'] = "required|min:8";
        $arr_rules['first_name'] = "required|max:255";
        $arr_rules['last_name'] = "required|max:255";
        $arr_rules['phone'] = "required|max:12";
        $arr_rules['address'] = "required|max:255";
        
        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
             return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = $request->all();
        $arr_data = array();
        $arr_project_manager = array();

        $credentials = ['email' => $request->input('email')];
		$user = Sentinel::findByCredentials($credentials);

		if ($user) 
		{
			Session::flash('error','This email is already present.');
			return redirect()->back();
		}

		$arr_data['email'] = $form_data['email'];
        $arr_data['password'] = $form_data['password'];
        $arr_data['is_active'] = 0;

        $obj_project_manager = Sentinel::registerAndActivate($arr_data);

        if ($obj_project_manager) 
        {
        	//assign role to user
        	$role = Sentinel::findRoleBySlug('project_manager');
			$obj_project_manager->roles()->attach($role);

        	$arr_project_manager['user_id'] = $obj_project_manager->id;
        	$arr_project_manager['first_name'] = ucfirst($form_data['first_name']);
        	$arr_project_manager['last_name'] = ucfirst($form_data['last_name']);
        	$arr_project_manager['address'] = $form_data['address'];
        	$arr_project_manager['phone'] = $form_data['phone'];

        	$status = $this->ProjectManagerModel->create($arr_project_manager);

        	if($status)
	        {
	        	Session::flash('success','Project Manager created successfully.');
	        } 
	        else
	        {
	            Session::flash('error','Problem occured, while creating project manager.');
	        }
        }

        return redirect()->back();
    }


    /*  
        Auther : Bharat Khairnar
        Comments: display view for edit clients
    */

    public function edit($enc_id)
    {
        $id = base64_decode($enc_id);
        $profile_img_path = url('/').'/uploads/front/profile/';

        

        $obj_expert = $this->ExpertsModel->where('id', $id)->with(['user_details','country_details.states','state_details.cities','city_details','expert_skills.skills','expert_categories.categories'])->first();

        $arr_expert = array();

        if($obj_expert)
        {
           $arr_expert = $obj_expert->toArray();
        }

        $arr_countries = array();

        $obj_countries = $this->CountryModel->get();

        if($obj_countries != FALSE)
        {
            $arr_countries = $obj_countries->toArray();
        }

        $this->arr_view_data['arr_countries'] = $arr_countries;
        $this->arr_view_data['enc_id'] = $enc_id;
        $this->arr_view_data['profile_img_path'] = $profile_img_path;
        $this->arr_view_data['arr_expert'] = $arr_expert;
        $this->arr_view_data['page_title'] = "Edit Expert";
        $this->arr_view_data['module_title'] = "Expert Manage";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.experts.edit',$this->arr_view_data);  

    }


    /*  
        Auther : Bharat Khairnar
        Comments: update client details
    */
    public function update(Request $request, $enc_id)
    {
        $expert_id = base64_decode($enc_id);
        $arr_rules = array();
        $status = FALSE;
 
        $arr_rules['first_name'] 	= "required|max:255";
        $arr_rules['last_name'] 	= "required|max:255";
        $arr_rules['phone_code']    = "required|min:2";
        $arr_rules['phone_number'] 	= "required|max:12|min:10";
        $arr_rules['country']      	= "required";
        $arr_rules['state']        	= "required";
        $arr_rules['city']         	= "required";
        $arr_rules['zip_code']     	= "required|max:10";
        $arr_rules['address']      	= "required|max:255";
        
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = array();
        $form_data = $request->all(); 

        $obj_expert = $this->ExpertsModel->where('id',$expert_id)->first();

        if($obj_expert && sizeof($obj_expert) > 0)
        { 	
        	 $arr_expert = array();
             $arr_expert['first_name']    = ucfirst($form_data['first_name']);
	         $arr_expert['last_name']     = ucfirst($form_data['last_name']);
             $arr_expert['phone_code']    = $form_data['phone_code'];
	         $arr_expert['phone_number']  = $form_data['phone_number'];
	         $arr_expert['country']       = $form_data['country'];
	         $arr_expert['state']         = $form_data['state'];
	         $arr_expert['city']          = $form_data['city'];
	         $arr_expert['zip']           = $form_data['zip_code'];
	         $arr_expert['address']       = $form_data['address'];
	         $arr_expert['company_name']  = $form_data['company_name'];
	         $arr_expert['vat_number']    = $form_data['vat_number'];

        	$status = $obj_expert->update($arr_expert);
        }

        if ($status) 
        {
            Session::flash('success','Expert details updated successfully.');    
        }
        else
        {
            Session::flash('error','Error while updating expert details.');
        }
        
        return redirect()->back();
    }

    /*
    | Following Fuctions for active ,deactive and delete
    | auther :Bharat Khairnar    
    | 
    */ 

    public function activate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while expert activation.');
            return redirect()->back();
        }

        if($this->perform_activate(base64_decode($enc_id)))
        {
            Session::flash('success','Expert activated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while expert activation.');
        }

        return redirect()->back();
    }

    public function deactivate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
        	Session::flash('error','Problem occured while expert deactivation.');
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            Session::flash('success','Expert deactivated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while expert deactivation.');
        }

        return redirect()->back();
    }

    public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
        	Session::flash('error','Problem occured while expert deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Expert deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while expert deletion.');
        }

        return redirect()->back();
    }


    public function perform_activate($id)
    {
        if ($id) 
        {
            $client = $this->UserModel->where('id',$id)->first();
            if($client)
            {
                return $client->update(['is_active'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_deactivate($id)
    {
        if ($id) 
        {
            $client = $this->UserModel->where('id',$id)->first();
            if($client)
            {
                return $client->update(['is_active'=>0]);
            }
        }

        return FALSE;
    }

    public function perform_delete($id)
    {
        if ($id) 
        {	
        	$user= $this->UserModel->where('id',$id)->first();
            $expert= $this->ExpertsModel->where('user_id',$id)->first();

            if($user!=FALSE && $expert!=FALSE)
            {	
            	$delete_user = $user->delete();	
            	return $expert->delete();	
            }
        }
        return FALSE;
    }
   

     /* 
    | multi_action: Following Fuctions for active ,deactive and delete for multiple records
    | auther :Bharat Khairnar    
    | 
    */ 
    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Session::flash('success','Expert(s) deleted successfully.');
            } 
            elseif($multi_action=="activate")
            {
               $this->perform_activate(base64_decode($record_id)); 
               Session::flash('success','Expert(s) activated successfully');               
            }
            elseif($multi_action=="deactivate")
            {
               $this->perform_deactivate(base64_decode($record_id));    
               Session::flash('success','Expert(s) blocked successfully.');
            }
        }

        return redirect()->back();
    }

     /*
        Auther : Shankar Jamdhade
        Comments: display all projects
    */
    public function projects($expert_user_id)
    {
        if($expert_user_id=="")
        {
            return redirect()->back();
        }
        $expert_user_id=base64_decode($expert_user_id);
        $obj_expert_projects = $this->ProjectpostModel->with('skill_details','expert_details','category_details')->where('expert_user_id',$expert_user_id)->get();

        if($obj_expert_projects != FALSE)
        {
            $arr_expert_projects = $obj_expert_projects->toArray();
        }
        //dd($obj_client_projects);
        $this->arr_view_data['expert_user_id'] = base64_encode($expert_user_id);
        $this->arr_view_data['arr_expert_projects'] = $arr_expert_projects;
        $this->arr_view_data['page_title'] = "Manage Experts Project";
        $this->arr_view_data['module_title'] = "Project Manage";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.experts.all-projects',$this->arr_view_data);
    }
    /*  
        Auther : Shankar Jamdhade
        Comments: display view for projects details.
    */

    public function show($project_id)
    {
        $id = base64_decode($project_id);

        $obj_project_info = $this->ProjectpostModel->with('project_skills.skill_data','expert_details','category_details')->where('id',$id)->first();

        $arr_info = array();

        if($obj_project_info)
        {
           $arr_info = $obj_project_info->toArray();
        }

        //dd($arr_info);
        $this->arr_view_data['arr_info'] = $arr_info;
        $this->arr_view_data['page_title'] = "Projects Details";
        $this->arr_view_data['module_title'] = "Expert Manage";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        $this->arr_view_data['project_attachment_public_path'] = $this->project_attachment_public_path;


        return view('admin.experts.show',$this->arr_view_data);

    }
    /*  
        Auther : Bharat Khairnar
        Comments: Display current subscription of expert which is active.
    */
    public function subscription($subscription_id){
        if($subscription_id == null)
        {
            Session::flash("error",trans('controller_translations.text_sorry_somthing_goes_wrong_please_try_again'));
            return redirect()->back();
        }
      $id = base64_decode($subscription_id);
      $arr_current_exprt_sub = array();
      $obj_current_sub =   $this->SubscriptionUsersModel->where('id',$id)->where('is_active','1')->with(['user_details'])->first();
      if($obj_current_sub && $obj_current_sub!=FALSE){
         $arr_current_exprt_sub =  $obj_current_sub->toArray();
      }
      $this->arr_view_data['arr_info'] = $arr_current_exprt_sub;
      $this->arr_view_data['page_title'] = "Subscription Details";
      $this->arr_view_data['module_title'] = "Expert Subscription";
      $this->arr_view_data['module_url_path'] = $this->module_url_path;
      return view('admin.experts.subscription',$this->arr_view_data);
    }
} // end class
