<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Common\Services\LanguageService;  
use App\Models\CurrencyModel;
use App\Models\CurrencyJobPostBudgetModel;

use Validator;
use Session;
Use Sentinel;
use Excel;

class CurrencyController extends Controller
{
     /*
        Auther : Sagar Sainkar
        Comments: controller for currency (projec currency)        
    */
    public $CurrencyModel; 
    
    public function __construct(CurrencyModel $currency,LanguageService $langauge)
    {      
       $this->CurrencyModel                 = $currency;
       $this->CurrencyJobPostBudgetModel    = new CurrencyJobPostBudgetModel;
       $this->LanguageService               = $langauge;
       $this->module_url_path               = url(config('app.project.admin_panel_slug')."/currency");

       // $this->image_base_img_path   = public_path().config('app.project.img_path.category_image');
       // $this->image_public_img_path = url('/').config('app.project.img_path.category_image');
    }


    /*
        Auther : Sagar Sainkar
        Comments: display catergories
    */
    public function index()
    {

        // $arr_lang   =  $this->LanguageService->get_all_language();
        $arr_currency = [];
        $obj_currency = $this->CurrencyModel->get();

        if($obj_currency != FALSE)
        {
            $arr_currency = $obj_currency->toArray();
        }
        //dd($arr_currency);
        $this->arr_view_data['arr_currency'] = $arr_currency;

        $this->arr_view_data['page_title'] = "Manage Currency";
        $this->arr_view_data['module_title'] = "Currency";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.currency.index',$this->arr_view_data);
    }


    /*  
        Auther : Sagar Sainkar
        Comments: display view for Add new category
    */

    public function create()
    {

        $this->arr_view_data['arr_lang'] = $this->LanguageService->get_all_language();
        $this->arr_view_data['page_title'] = "Create Currency";
        $this->arr_view_data['module_title'] = "Currency";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.currency.create',$this->arr_view_data);
    }

    /*  
        Auther : Sagar Sainkar
        Comments: Add new category
    */
    public function store(Request $request)
    {
        $form_data = array();

        $form_data = $request->all();
        $arr_rules['currency']      = "required";
        $arr_rules['currency_code'] = "required";
        
        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
             return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        /* Check if already exists */
        $does_exists = $this->CurrencyModel->where('currency_code',$request->input('currency_code'))->count();
        
        if($does_exists)
        {
            Session::flash('error','Currency already exist');            
            return redirect()->back();
        }
    
        $form_data = $request->all();
        $arr_data  = array();
        
        $arr_data['currency_code'] = $form_data['currency_code'];
        $arr_data['currency']      = $form_data['currency'];

        $obj_currency    = $this->CurrencyModel->create($arr_data);
        if($obj_currency)
        {
            Session::flash('success','Currency created successfully');
        }
        else
        {
            Session::flash('error','Something went wrong');
        }
            return redirect()->back();   
    }


    /*  
        Auther : Sagar Sainkar
        Comments: display view for edit category
    */

    public function edit($enc_id)
    {
        $id = base64_decode($enc_id);

        $obj_currency = $this->CurrencyModel->where('id', $id)->first();

        $arr_currency = [];

        if($obj_currency)
        {
           $arr_currency = $obj_currency->toArray(); 
        }

        $this->arr_view_data['enc_id'] = $enc_id;
        $this->arr_view_data['arr_currency'] = $arr_currency;  

        $this->arr_view_data['page_title'] = "Edit Currency";
        $this->arr_view_data['module_title'] = "Currency";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.currency.edit',$this->arr_view_data);  

    }


    /*  
        Auther : Sagar Sainkar
        Comments: update category details
    */
    public function update(Request $request, $enc_id)
    {
        $currency_id = base64_decode($enc_id);
        $arr_rules = array();
        $status = FALSE;

        $arr_rules['currency']      = "required";
        $arr_rules['currency_code'] = "required";       
        
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = array();
        $form_data = $request->all(); 

        $arr_data['currency_code'] = $form_data['currency_code'];
        $arr_data['currency']      = $form_data['currency'];

        $obj_currency    = $this->CurrencyModel->where('id',$currency_id)->update($arr_data);
        if($obj_currency)
        {
            Session::flash('success','Currency Update successfully');
        }
        else
        {
            Session::flash('error','Something went wrong');
        }
            return redirect()->back(); 
        
        return redirect()->back();
    }

    /*
    | Following Fuctions for active ,deactive and delete
    | auther :Sagar Sainkar
    | 
    */ 

    public function activate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while Currency activation.');
            return redirect()->back();
        }

        if($this->perform_activate(base64_decode($enc_id)))
        {
            Session::flash('success','Category activated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while Currency activation.');
        }

        return redirect()->back();
    }

    public function deactivate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while Currency deactivation.');
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            Session::flash('success','Currency deactivated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while Currency deactivation.');
        }

        return redirect()->back();
    }

    public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while Currency deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Currency deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while Currency deletion.');
        }

        return redirect()->back();
    }


    public function perform_activate($id)
    {
        if ($id) 
        {
            $service_category = $this->CurrencyModel->where('id',$id)->first();
            if($service_category)
            {
                return $service_category->update(['is_active'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_deactivate($id)
    {
        if ($id) 
        {
            $service_category = $this->CurrencyModel->where('id',$id)->first();
            if($service_category)
            {
                return $service_category->update(['is_active'=>0]);
            }
        }

        return FALSE;
    }

    public function perform_delete($id)
    {
        if ($id) 
        {
            $service_category= $this->CurrencyModel->where('id',$id)->first();
            if($service_category)
            {
                return $service_category->delete();
            }
        }
        return FALSE;
    }
   

    /*
    | multi_action: Following Fuctions for active ,deactive and delete for multiple records
    | auther :Sagar Sainkar
    | Date : 
    | 
    */ 
    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        /*if ($multi_action=="export") 
        {
            $this->_export($request);
        }*/

        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Session::flash('success','Currency(s) deleted successfully.');
            } 
            elseif($multi_action=="activate")
            {
               $this->perform_activate(base64_decode($record_id)); 
               Session::flash('success','Currency(s) activated successfully');               
            }
            elseif($multi_action=="deactivate")
            {
               $this->perform_deactivate(base64_decode($record_id));    
               Session::flash('success','Currency(s) blocked successfully.');
            }

        }

        return redirect()->back();
    }

    public function arrange_locale_wise(array $arr_data)
    {
        if(sizeof($arr_data)>0)
        {
            foreach ($arr_data as $key => $data)
            {
                $arr_tmp = $data;
                unset($arr_data[$key]);

                $arr_data[$data['locale']] = $data;                    
            }

            return $arr_data;
        }
        else
        {
            return [];
        }
    }

    public function job_post_budget($enc_id)
    {
        $id = base64_decode($enc_id);

        $obj_currency = $this->CurrencyModel->where('id',$id)->with('job_post_budget')->first();

        if(!$obj_currency)
        {
            Session::flash('error','Invalid Request.');
            return redirect()->back();
        }

        $this->arr_view_data['page_title']      = "Job Post Budget";
        $this->arr_view_data['module_title']    = "Job Post Budget";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        $this->arr_view_data['arr_currency']    = $obj_currency->toArray();

        return view('admin.currency.job_post_budget',$this->arr_view_data);
    }

    public function store_job_post_budget($enc_id, Request $request)
    {
        $id = base64_decode($enc_id);

        $obj_currency = $this->CurrencyModel->where('id',$id)
                                            ->with('job_post_budget')
                                            ->first();

        if(!$obj_currency)
        {
            Session::flash('error','Invalid Request.');
            return redirect()->back();
        }

        $form_data = array();

        $form_data = $request->all();

        $arr_rules['tiny_from']     = 'required';
        $arr_rules['tiny_to']       = 'required';
        $arr_rules['small_from']    = 'required';
        $arr_rules['small_to']      = 'required';
        $arr_rules['medium_from']   = 'required';
        $arr_rules['medium_to']     = 'required';
        $arr_rules['large_from']    = 'required';
        $arr_rules['large_to']      = 'required';
        $arr_rules['big_from']      = 'required';
        $arr_rules['big_to']        = 'required';
        $arr_rules['jumbo_from']    = 'required';

        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $obj_job_post_budget = CurrencyJobPostBudgetModel::firstOrNew(['currency_id'=>$id]);

        $obj_job_post_budget->currency_id   = $id;
        $obj_job_post_budget->tiny_from     = $request->input('tiny_from');
        $obj_job_post_budget->tiny_to       = $request->input('tiny_to');
        $obj_job_post_budget->small_from    = $request->input('small_from');
        $obj_job_post_budget->small_to      = $request->input('small_to');
        $obj_job_post_budget->medium_from   = $request->input('medium_from');
        $obj_job_post_budget->medium_to     = $request->input('medium_to');
        $obj_job_post_budget->large_from    = $request->input('large_from');
        $obj_job_post_budget->large_to      = $request->input('large_to');
        $obj_job_post_budget->big_from      = $request->input('big_from');
        $obj_job_post_budget->big_to        = $request->input('big_to');
        $obj_job_post_budget->jumbo_from    = $request->input('jumbo_from');

        if($obj_job_post_budget->save())
        {
            Session::flash('success','Records has been updated successfully.');
            return redirect($this->module_url_path);
        }else{
            Session::flash('error','Error occured while updating records.');
            return redirect()->back();
        }

    }

    public function store_job_post_hourly_budget($enc_id, Request $request)
    {
        $id = base64_decode($enc_id);

        $obj_currency = $this->CurrencyModel->where('id',$id)
                                            ->with('job_post_budget')
                                            ->first();

        if(!$obj_currency)
        {
            Session::flash('error','Invalid Request.');
            return redirect()->back();
        }

        $form_data = array();

        $form_data = $request->all();

        $combined_array = array_combine($form_data['from'], $form_data['to']);

        $arr_rules['from']     = 'required|array|min:1';
        $arr_rules['to']       = 'required|array|min:1';

        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $obj_job_post_budget = CurrencyJobPostBudgetModel::firstOrNew(['currency_id'=>$id]);

        $obj_job_post_budget->currency_id   = $id;
        $obj_job_post_budget->hourly_price  = serialize($combined_array);

        if($obj_job_post_budget->save())
        {
            Session::flash('success','Records has been updated successfully.');
            return redirect($this->module_url_path);
        }else{
            Session::flash('error','Error occured while updating records.');
            return redirect()->back();
        }

    }

}