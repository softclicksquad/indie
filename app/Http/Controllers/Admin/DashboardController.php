<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Models\ProjectpostModel;
use App\Models\ExpertsModel;
use App\Models\ClientsModel;
use App\Models\ProjectManagerModel; 
use App\Models\SubscriptionPacksModel;  
use App\Models\AdminProfileModel;
use App\Models\CategoriesModel; 
use App\Models\MilestonesModel;
use App\Models\ContactEnquiryModel;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller
{
	public function __construct(  ProjectpostModel $projectpost,
                                ExpertsModel $experts,
                                ClientsModel $clients,
                                ProjectManagerModel $project_manager,
                                SubscriptionPacksModel $subscription_packs,
                                AdminProfileModel $admin_profile,
                                CategoriesModel $categories,
                                MilestonesModel $milestone,
                                ContactEnquiryModel $ContactEnquiryModel       
                                )
	{
        $this->ProjectpostModel       = $projectpost;
        $this->ExpertsModel           = $experts;
        $this->ClientsModel           = $clients;
        $this->ProjectManagerModel    = $project_manager;
        $this->SubscriptionPacksModel = $subscription_packs;
        $this->AdminProfileModel      = $admin_profile;
        $this->CategoriesModel        = $categories;
        $this->MilestonesModel        = $milestone;
        $this->ContactEnquiryModel    = $ContactEnquiryModel;
    		$this->arr_view_data          = [];
	}
  /*
  | Index : Display dashboard view
  | auther :Sagar Sainkar
  | Date : 06/05/2016
  | 
  */ 
  public function index()
  {
      $obj_projects             = $this->ProjectpostModel->where('project_status','<>','0')->get();
      $arr_projects             = [];
      $all_projects_count       = '0';
      $subadmin_count           = '0';
      $experts_count            = '0';
      $client_count             = '0';
      $project_manager_count    = '0';
      $subscription_packs_count = '0';
      $project_category_count   = '0';
      if($obj_projects){
          $arr_projects       = $obj_projects->groupBy('project_status')->toArray();
          $all_projects_count = $obj_projects->count();
      }
    
      $total_project_count = "0";
      $total_project_count = $obj_projects->count();
      $obj_projects_tmp    = ProjectpostModel::select('project_status', DB::raw('count(*) as total'))
                                           ->where('project_status','<>','0')
                                           ->where('deleted_at','=',null)
                                           ->groupBy('project_status')    
                                           ->get();
      /* Pie chart functionality start here*/
       $arr_data = $arr_proj = array();
       if($obj_projects_tmp)
       {
            $arr_proj = $obj_projects_tmp->toArray();
            foreach ($arr_proj as $key => $project) 
            {
                if($project['project_status']=="1")
                {
                  $project["perc"] = ($project["total"]/$total_project_count)*100;
                  $arr_data['posted'] = '{  y: '.number_format($project["perc"],2).', legendText: "Posted", exploded: true, label: "Posted" },';
                }
                else if($project['project_status']=="2")
                {
                   $project["perc"] = ($project["total"]/$total_project_count)*100;
                  $arr_data['open'] ='{  y: '.number_format($project["perc"],2).', legendText: "Open", exploded: true, label: "Open" },';
                }
                else if($project['project_status']=="3")  
                {
                   $project["perc"] = ($project["total"]/$total_project_count)*100;
                  $arr_data['completed'] = '{  y: '.number_format($project["perc"],2).', legendText: "Completed", exploded: true, label: "Completed" },';
                }
                else if($project['project_status']=="4")
                {
                   $project["perc"] = ($project["total"]/$total_project_count)*100;
                  $arr_data['ongoing'] = '{  y: '.number_format($project["perc"],2).', legendText: "Ongoing", exploded: true, label: "Ongoing" },';
                }
                else if($project['project_status']=="5")
                {
                   $project["perc"] = ($project["total"]/$total_project_count)*100;
                  $arr_data['canceled'] = '{  y: '.number_format($project["perc"],2).', legendText: "Cancelled", exploded: true, label: "Cancelled" }';
                }
            }
      }
      /* Pie chart functionality end here*/

      /* subdmin count */
      $subadmin_count           = $this->AdminProfileModel->whereNotIn('id', [1])->count();

      /* experts count */
      $experts_count            = $this->ExpertsModel->count();

      /* Clients count */
      $client_count             = $this->ClientsModel->count();
          
      /* Projct Manager count */
      $project_manager_count    = $this->ProjectManagerModel->count();

      /* Projct cat  count */
      $project_category_count   = $this->CategoriesModel->count();
      
      /* Projct Subscription count */
      $subscription_packs_count = $this->SubscriptionPacksModel->count();

      /* Contact enquiry count */
      $contact_enquiry_count    = $this->ContactEnquiryModel->where('reply_status','0')->count();
      
      /*Count of registered experts in month for each date. */
      $user_info =  $this->ExpertsModel
      ->whereMonth('created_at', '=', date('m'))
      ->get(['created_at'])
      ->groupBy(function($date)
      {
       // dd($date->created_at);
          return Carbon::parse($date->created_at)->format('Y-m-d'); // grouping by years
      });
      $arr_group_date = array();
      if($user_info)
      {
          $arr_group_date = $user_info->toArray();
         // dd($arr_group_date);
      }
      $arr_expert_data = $arr_temp = array();
      if(isset($arr_group_date) && sizeof($arr_group_date)>0)
      {
          foreach ($arr_group_date as $key => $arr_date) 
          {
              $arr_register_size = sizeof($arr_date);
              $arr_temp['expert_register_count'] = $arr_register_size;

              foreach ($arr_date as $key => $visitor) 
              {
                  $arr_temp['date'] =  date('d',strtotime($visitor['created_at']));
              }
              array_push($arr_expert_data, $arr_temp);
          }
      }
      // dd($arr_expert_data);
      /*Count of registered clients in month for each date. */

       $user_info =  $this->ClientsModel
      ->whereMonth('created_at', '=', date('m'))
      ->get(['created_at'])
      ->groupBy(function($date)
      {
          return Carbon::parse($date->created_at)->format('Y-m-d'); // grouping by years
      });
      $arr_group_date = array();
      if($user_info)
      {
        $arr_group_date = $user_info->toArray();
        // dd($arr_group_date);
      }
      $arr_client_data = $arr_temp = array();
      if(isset($arr_group_date) && sizeof($arr_group_date)>0)
      {
          foreach ($arr_group_date as $key => $arr_date) 
          {
              $arr_register_size = sizeof($arr_date);
              $arr_temp['client_register_count'] = $arr_register_size;

              foreach ($arr_date as $key => $visitor) 
              {
                  $arr_temp['date'] =  date('d',strtotime($visitor['created_at']));
              }
              array_push($arr_client_data, $arr_temp);

          }
      }
      /*Payment Section*/
      /*$total_milestone       = "";
      $arr_milestone         = $total_milestone_year = $total_milestone_month = [];
      $current_year          = date('Y');
      $current_month         = date('m');
      $total_milestone       = $this->MilestonesModel->sum('cost_from_client');
      $total_milestone_year  = $this->MilestonesModel
                                   ->whereYear('created_at','=',$current_year)
                                   ->sum('cost_from_client');
      $total_milestone_month = $this->MilestonesModel
                                   ->whereMonth('created_at','=',$current_month)
                                   ->sum('cost_from_client');*/
      /*$arr_milestone['total_milestone']                = $total_milestone;
      $arr_milestone['total_milestone_year']           = $total_milestone_year;
      $arr_milestone['total_milestone_month']          = $total_milestone_month;
      $this->arr_view_data['arr_milestone']            = $arr_milestone;*/
      $this->arr_view_data['page_title']               = "Dashboard";
      $this->arr_view_data['arr_projects']             = $arr_projects;
      $this->arr_view_data['all_projects_count']       = $all_projects_count;
      $this->arr_view_data['subadmin_count']           = $subadmin_count;
      $this->arr_view_data['experts_count']            = $experts_count;
      $this->arr_view_data['client_count']             = $client_count;
      $this->arr_view_data['project_manager_count']    = $project_manager_count;
      $this->arr_view_data['project_category_count']   = $project_category_count;
      $this->arr_view_data['subscription_packs_count'] = $subscription_packs_count;
      $this->arr_view_data['arr_expert_data']          = $arr_expert_data;
      $this->arr_view_data['arr_client_data']          = $arr_client_data;
      $this->arr_view_data['contact_enquiry_count']    = $contact_enquiry_count;
      $this->arr_view_data['arr_data']                 = $arr_data;
  	return view('admin.dashboard.index',$this->arr_view_data);
  }
} // 
