<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models\PaymentSettingsAdminModel;
use Session;
use Validator;
use Sentinel;
use App\Common\Services\PaymentService as PaymentService;

class PaymentSettingsController extends Controller
{
    public function __construct(PaymentSettingsAdminModel $admin_payment)
    {
        $this->PaymentSettingsAdminModel = $admin_payment;
        $this->arr_view_data = [];
        $this->admin_panel_slug = config('app.project.admin_panel_slug');
        $this->module_url_path  = url(config('app.project.admin_panel_slug')."/payment_settings");
        $this->PaymentService = new PaymentService();
    }


    public function index()
    {
    	
    	$page_title = "Payment Methods";
    	$payment_details = array();

    	$enc_obj_payment_details = $this->PaymentSettingsAdminModel->where('id',1)->first();

    	if ($enc_obj_payment_details!=FALSE) 
    	{
    		$enc_arr_payment_details = $enc_obj_payment_details->toArray();

    		if (isset($enc_arr_payment_details['payment_details']))
    		{
    			$payment_details = $this->PaymentService->get_admin_payment_settings();
    		}
    		
    	}

        $this->arr_view_data['payment_details'] = $payment_details;
        $this->arr_view_data['page_title'] = $page_title;
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

    	return view('admin.payment_settings.index',$this->arr_view_data);
    }


     /*
    | update : update payment settings details
    | auther :Sagar Sainkar
    | 
    */ 

    public function update(Request $request)
    {
        $arr_rules=array();
        $arr_rules['paypal_client_id'] = "required";
        $arr_rules['paypal_secret_key'] = "required";
        $arr_rules['paypal_payment_mode'] = "required";
        $arr_rules['stripe_secret_key'] = "required";
        $arr_rules['stripe_publishable_key'] = "required";
        
        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
             return back()->withErrors($validator)->withInput();
        }

         $form_data = array();
         $arr_data = array();
         $form_data = $request->all();
         $status = FALSE;

         $arr_data['paypal_client_id'] = $form_data['paypal_client_id'];
         $arr_data['paypal_secret_key'] = $form_data['paypal_secret_key'];
         $arr_data['paypal_payment_mode'] = $form_data['paypal_payment_mode'];
         $arr_data['stripe_secret_key'] = $form_data['stripe_secret_key'];
         $arr_data['stripe_publishable_key'] = $form_data['stripe_publishable_key'];

        $user = Sentinel::check();

        $payment_settings = $this->PaymentSettingsAdminModel->where('id',1)->first();
        if($payment_settings)
        {
        	$ecn_arr_data = $payment_details = $this->PaymentService->_encrypt($arr_data);
        	if ($ecn_arr_data && $ecn_arr_data!=false) 
        	{
        		$status = $this->PaymentSettingsAdminModel->where('id',1)->where('admin_user_id',$user->id)->update(['payment_details'=>$ecn_arr_data]);
        	}
            
        }

        if($status)
        {
            Session::flash("success","Payment settings Updated Successfullly.");
        }
        else
        {
            Session::flash("error","Error while updating payment settings.");  
        }

        return redirect()->back();
    }

}
