<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\CountryModel; 
use App\Models\StateModel; 

use Validator;
use Session;
use Input;
 
class StateController extends Controller
{

      /*
    | Constructor : creates instances of model class 
    |               & handles the admin authantication
    | auther : Sagar Sainkar
    | Date : 05/05/2016
    | @return \Illuminate\Http\Response
    */

    /*NOTE: all deletion methods and code is commented in controller and view because we doesnot have to give delete option to admin(if required please only make on delete fuctions in view and controller) */

    public function __construct(CountryModel $countries, StateModel $state)
    {
        $this->CountryModel     = $countries;
        $this->StateModel       = $state;

        $this->arr_view_data    = [];
        $this->module_url_path  = url(config('app.project.admin_panel_slug')."/states");
    } 


      /*
    | Index : Display listing of states
    | auther :Sagar Sainkar
    | Date : 05/05/2016
    | 
    */ 
 
    public function index()
    {
        
        $arr_states = array();

        $obj_state = $this->StateModel->orderBy('state_name', 'asc')->with(['country_details'])->paginate(config('app.project.pagi_cnt'));;

        if(isset($obj_state) && sizeof($obj_state)>0)
        {
            $arr_states = $obj_state->toArray();
        }

        $this->arr_view_data['page_title'] = "Manage States";
        $this->arr_view_data['module_title'] = "Manage States";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        $this->arr_view_data['arr_states'] = $arr_states;
        $this->arr_view_data['obj_state'] = $obj_state;

        return view('admin.state.index',$this->arr_view_data);
    }

    public function create()
    {
        $arr_countries = array();

        $obj_countries = $this->CountryModel->orderBy('country_name', 'asc')->where('is_active',1)->get();

        if($obj_countries != FALSE)
        {
            $arr_countries = $obj_countries->toArray();
        }

        $this->arr_view_data['arr_countries'] = $arr_countries;

        $this->arr_view_data['page_title'] = "Create State";
        $this->arr_view_data['module_title'] = "State";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.state.create',$this->arr_view_data);
    }



    /*
    | store() : store State details
    | auther : Sagar Sainkar
    | Date : 05/05/2016
    | @param  \Illuminate\Http\Request  $request
    | 
    */

    public function store(Request $request)
    {
        $form_data = array();

        
        $arr_rules['state_name'] = "required";
        $arr_rules['country'] = "required";
        
        $form_data = $request->all();

        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }   

        $duplication = $this->StateModel->where('state_name',$request->input('state_name'))->where('country_id',$form_data['country'])->count();

        if ($duplication) 
        {
          Session::flash('error', 'This state already exist');
          return redirect()->back()->withInput($request->all());
        }
        /* Insert into state Table */
        $arr_data = array();
        $arr_data['state_name'] =  $form_data['state_name'];
        $arr_data['country_id'] = $form_data['country'];
        $arr_data['is_active'] =  '1';

        $state = $this->StateModel->create($arr_data);


        if($state)      
        {
            Session::flash('success','State created successfully.');
        }
        else
        {
            Session::flash('error','Problem occured, while creating state.');
        }

        return redirect()->back();
    }


    /*
    | edit() : edit state details
    | auther : Sagar Sainkar
    | Date : 05/05/2016    
    | 
    */
    public function edit($enc_id)
    {

        $State_id = base64_decode($enc_id); 

        $obj_state = $this->StateModel->where('id', $State_id)->first();

        $arr_state = [];
        if($obj_state)
        {
           $arr_state = $obj_state->toArray();
        }

        $arr_countries = array();

        $obj_countries = $this->CountryModel->orderBy('country_name', 'asc')->get();

        if($obj_countries != FALSE)
        {
            $arr_countries = $obj_countries->toArray();
        }

        $this->arr_view_data['arr_countries'] = $arr_countries;


        $this->arr_view_data['enc_id'] = $enc_id;
        $this->arr_view_data['arr_state'] = $arr_state;  
        $this->arr_view_data['arr_countries'] = $arr_countries;
        $this->arr_view_data['page_title'] = "Edit State";
        $this->arr_view_data['module_title'] = "State";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.state.edit',$this->arr_view_data);    
    }


    /*
    | update() : update country details
    | auther : Sagar Sainkar
    | Date : 05/05/2016
    | @param  \Illuminate\Http\Request  $request
    | 
    */

    public function update(Request $request, $enc_id)
    {

        $State_id = base64_decode($enc_id);
        $arr_rules = array();
        
        $arr_rules['state_name'] = "required";
        $arr_rules['country'] = "required";
      
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return back()->withErrors($validator)->withInput();
        }

        $form_data = array();
        $form_data = $request->all();

        $duplication = $this->StateModel->where('state_name',$request->input('state_name'))->where('country_id',$form_data['country'])->whereNotIn('id',[$State_id])->count();

        if ($duplication) 
        {
          Session::flash('error', 'This state already exist');
          return redirect()->back()->withInput($request->all());
        }

        /* Retrieve Existing state */
        $state = $this->StateModel->where('id',$State_id)->first();

        if(!$state)
        {
            Session::flash('error','Problem occured while updating state.');
            return redirect()->back();   
        }
        
        $arr_data['state_name'] =  $form_data['state_name'];
        $arr_data['country_id'] = $form_data['country'];

        $state_instance = clone $state ;        
        $status_update = $state_instance->update($arr_data);

        if ($status_update)
        {
            Session::flash('success','State updated successfully.');    
        }

        return redirect()->back();
    }


    /*
    | Following Fuctions for active ,deactive and delete
    | auther :Sagar Sainkar
    | Date : 05/05/2016
    | 
    */ 

    public function activate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while state activation.');
            return redirect()->back();
        }

        if($this->perform_activate(base64_decode($enc_id)))
        {
            Session::flash('success','State activated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while state activation.');
        }

        return redirect()->back();
    }

    public function deactivate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while state deactivation.');
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            Session::flash('success','State deactivated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while state deactivation.');
        }

        return redirect()->back();
    }

    /*public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while state deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','State deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while state deletion.');
        }

        return redirect()->back();
    }*/


    public function perform_activate($id)
    {
        if ($id) 
        {
            $obj_state = $this->StateModel->where('id',$id)->first();
            if($obj_state)
            {
                return $obj_state->update(['is_active'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_deactivate($id)
    {
        if ($id) 
        {
            $obj_state = $this->StateModel->where('id',$id)->first();
            if($obj_state)
            {
                return $obj_state->update(['is_active'=>0]);
            }
        }

        return FALSE;
    }

    /*public function perform_delete($id)
    {
        if ($id) 
        {
            $obj_state= $this->StateModel->where('id',$id)->first();
            if($obj_state)
            {
                return $obj_state->delete();
            }
        }
        return FALSE;
    }*/
   

   /*
    | multi_action: Following Fuctions for active ,deactive and delete for multiple records
    | auther :Sagar Sainkar
    | Date : 05/05/2016
    | 
    */ 

    public function multi_action(Request $request)
    {

        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            Session::flash('error','please select at least one record.');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            
            if($multi_action=="activate")
            {
               $this->perform_activate(base64_decode($record_id));
               Session::flash('success','State(s) activated successfully');
            }
            elseif($multi_action=="deactivate")
            {
               $this->perform_deactivate(base64_decode($record_id));
               Session::flash('success','State(s) blocked successfully.');
            }
            /*elseif($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));
               Session::flash('success','State(s) deleted successfully.');
            } */
        }


        return redirect()->back();
    }


    public function search(Request $request)
    {

        $form_data = array();
        $form_data = $request->all();

        if (isset($form_data['search_text']) && $form_data['search_text']!="")
        {
            $arr_states = array();

            $obj_state = $this->StateModel->where('state_name', 'like','%'.$form_data['search_text'].'%')->with(['country_details'])->paginate(config('app.project.pagi_cnt'));;

            if(isset($obj_state) && sizeof($obj_state)>0)
            {
                $arr_states = $obj_state->toArray();
            }

            
            $this->arr_view_data['arr_states'] = $arr_states;
            $this->arr_view_data['obj_state'] = $obj_state;
        }
        else
        {
            $arr_states = array();
            $obj_state = $this->StateModel->with(['country_details'])->paginate(config('app.project.pagi_cnt'));;

            if(isset($obj_state) && sizeof($obj_state)>0)
            {
                $arr_states = $obj_state->toArray();
            }

            $this->arr_view_data['arr_states'] = $arr_states;
            $this->arr_view_data['obj_state'] = $obj_state;

        }

        $this->arr_view_data['page_title'] = "Manage States";
        $this->arr_view_data['module_title'] = "Manage States";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.state.index',$this->arr_view_data);

        //return back();

    }
    
}