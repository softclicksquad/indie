<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models\ProjectpostModel;
use App\Models\MilestonesModel;
use App\Models\ClientsModel;
use App\Models\SubscriptionUsersModel;

use Session;
use Validator;
use Sentinel;

class MilestoneRequestsController extends Controller
{
   
    public function __construct(
                                ProjectpostModel $projects,
                                MilestonesModel $milestones,
                                ClientsModel $client,
                                SubscriptionUsersModel $subscription_users)
    {      
       $this->ProjectpostModel  = $projects;
       $this->MilestonesModel   = $milestones;
       $this->ClientsModel      = $client;
       $this->SubscriptionUsersModel      = $subscription_users;

       $this->module_url_path   = url(config('app.project.admin_panel_slug')."/project_milestones");

    }


    /*
     Comments   : Show all projects with milestones.
     Author     : Nayan S.
    */
	public function show_all_projects_with_milestones()
    {
        $obj_project = $this->ProjectpostModel->whereHas('project_milestones',function($query) {})
                                              ->where('project_status','=','4')          
                                              ->with(['project_milestones'=> function ($query) 
                                                    {
                                                        $query->select('id','project_id','client_user_id','title','cost','description','status');
                                                    },
                                                    'client_info'=> function ($query_1) 
                                                    {   
                                                        $query_1->select('id','user_id','first_name','last_name');
                                                    },
                                                    'expert_info'=> function ($query_2) 
                                                    {   
                                                        $query_2->select('id','user_id','first_name','last_name');
                                                    }])
                                              ->get(['id','client_user_id','expert_user_id','project_name','project_start_date','project_end_date','project_cost']);           
        $arr_project_data = [];                        
        
        if($obj_project)
        {
            $arr_project_data = $obj_project->toArray();
        }                                                   
                
        $this->arr_view_data['page_title']       = "Project Milestones Requests";
        $this->arr_view_data['module_title']     = "Project Milestones";
        $this->arr_view_data['module_url_path']  = $this->module_url_path;
        $this->arr_view_data['arr_project_data'] = $arr_project_data;

        return view('admin.milestone_requests.manage_projects_with_milestones',$this->arr_view_data);
    }

    /*
     Comments   : Show all milestones of a project.
     Author     : Nayan S.
    */
    public function show_all_milestones($enc_id)
    {
        $milestone_id = base64_decode($enc_id);

        if($milestone_id == "")
        {
            return redirect()->back();
        }   
 
        $arr_milestones  = [];
        $obj_milestones  = $this->MilestonesModel->with(['project_details'=> function ($query) { 
                                                            $query->select('id','project_name','project_start_date','project_end_date','project_cost','project_currency','project_currency_code');
                                                         },
                                                         'user_details'=> function ($query_1) {
                                                            $query_1->select('id','user_name','email');
                                                         },
                                                         'expert_details', 
                                                         'transaction_details'=> function ($query_2) {
                                                            $query_2->select('id','payment_status','invoice_id','user_id');
                                                         },
                                                         'milestone_release_details',
                                                         'expert_subscription'=> function ($query_3) 
                                                         {
                                                            $query_3->select('id','user_id','subscription_pack_id','pack_name','website_commision');
                                                            $query_3->where('is_active','1');
                                                            $query_3->orderBy('id','DESC');   
                                                          }  
                                                        ])
                                                 ->where('project_id',$milestone_id)
                                                 ->orderBy('updated_at','desc')
                                                 ->get();

        if($obj_milestones != FALSE){
            $arr_milestones = $obj_milestones->toArray();
        }
        $this->arr_view_data['page_title']          = "Project Milestones Requests";
        $this->arr_view_data['module_title']        = "Milestones";
        $this->arr_view_data['module_url_path']     = $this->module_url_path;
        $this->arr_view_data['arr_milestones']      = $arr_milestones;
        return view('admin.milestone_requests.manage_project_milestones',$this->arr_view_data);
    }
    /*
     Comments   : Show single Milestone Information.
     Author     : Nayan S.
    */
    public function show_milestone($enc_id)
    {
        $milestone_id = base64_decode($enc_id);

        if($milestone_id == "")
        {
            return redirect()->back();
        } 

        $arr_milestones  = [];
        $obj_milestones  = $this->MilestonesModel->with('project_details','user_details','transaction_details','expert_details')
                                                 ->where('id',$milestone_id)
                                                 ->first();

        if($obj_milestones)
        {
           $arr_milestones = $obj_milestones->toArray();
        }
        $this->arr_view_data['arr_milestones']  = $arr_milestones;
        $this->arr_view_data['page_title']      = "Milestone Details";
        $this->arr_view_data['module_title']    = "Project Milestone";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.milestone_requests.show',$this->arr_view_data);
    }
     public function delete($enc_id = FALSE)
     {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while client deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Bid deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while bid deletion.');
        }

        return redirect()->back();
     }

    public function perform_delete($id)
    {
        if ($id) 
        {   
            $bid= $this->ProjectsBidsModel->where('id',$id)->first();

            if($bid!=FALSE)
            {   
                return $bid->delete();   
            }
        }
        return FALSE;
    }

   
    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Session::flash('success','Bids(s) deleted successfully.');
            } 
            
        }
        return redirect()->back();
    }


      /*
      Authe: Sagar Sainkar  
      comment: show payment methods for milestones
    */

    public function payment_methods($enc_id)
    {
      if ($enc_id) 
      {
          $arr_milestone = array();
          $milestone_id = base64_decode($enc_id);

          $obj_milestone = $this->MilestonesModel->where('id',$milestone_id)->with(['project_details'])->first();

          if($obj_milestone != FALSE)
          {
              $arr_milestone = $obj_milestone->toArray();
          }

          $this->arr_view_data['arr_milestone']   = $arr_milestone;
          $this->arr_view_data['page_title']      = "Milestone Payment";
          $this->arr_view_data['module_url_path'] = $this->module_url_path;

          return view('admin.payment.payment_methods',$this->arr_view_data);
      }

      return back();
    }

}
