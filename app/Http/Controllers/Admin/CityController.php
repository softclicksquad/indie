<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\CountryModel; 
use App\Models\StateModel; 
use App\Models\CityModel; 
use Validator;
use Session;
use Input;

class CityController extends Controller
{
    /*
    | Constructor : creates instances of model class 
    |               & handles the admin authantication
    | auther : Sagar Sainkar
    | Date : 06/05/2016
    | @return \Illuminate\Http\Response
    */

    /*NOTE: all deletion methods and code is commented in controller and view because we doesnot have to give delete option to admin(if required please only make on delete fuctions in view and controller) */

    public function __construct(CountryModel $countries, StateModel $state, CityModel $city)
    {
        $this->CountryModel     = $countries;
        $this->StateModel       = $state;
        $this->CityModel       = $city;

        $this->arr_view_data    = [];
        $this->module_url_path  = url(config('app.project.admin_panel_slug')."/cities");
    } 


      /*
    | Index : Display listing of cities
    | auther :Sagar Sainkar
    | Date : 06/05/2016
    | 
    */ 
 
    public function index()
    {
        
        $arr_cities = array();

        $obj_city = $this->CityModel->with(['state_details.country_details'])->paginate(config('app.project.pagi_cnt'));;

        if(isset($obj_city) && sizeof($obj_city)>0)
        {
            $arr_cities = $obj_city->toArray();
        }

        $this->arr_view_data['page_title'] = "Manage Cities";
        $this->arr_view_data['module_title'] = "Manage Cities";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        $this->arr_view_data['arr_cities'] = $arr_cities;
        $this->arr_view_data['obj_city'] = $obj_city;

        return view('admin.city.index',$this->arr_view_data);
    }

      /*
    | Create : create new cities
    | auther :Sagar Sainkar
    | Date : 06/05/2016
    | 
    */ 

    public function create()
    {
        $arr_countries = array();

        $obj_countries = $this->CountryModel->orderBy('country_name', 'asc')->where('is_active',1)->get();

        if($obj_countries != FALSE)
        {
            $arr_countries = $obj_countries->toArray();
        }

        $this->arr_view_data['arr_countries'] = $arr_countries;

        $this->arr_view_data['page_title'] = "Create city";
        $this->arr_view_data['module_title'] = "Cities";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.city.create',$this->arr_view_data);
    }



    /*
    | store() : store city details
    | auther : Sagar Sainkar
    | Date : 05/05/2016
    | @param  \Illuminate\Http\Request  $request
    | 
    */

    public function store(Request $request)
    {
        $form_data = array();

        $arr_rules['city_name'] = "required";
        $arr_rules['country'] = "required";
        $arr_rules['state'] = "required";
        
        $form_data = $request->all();

        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $duplication=$this->CityModel->where('city_name',$request->input('city_name'))->where('state_id',$form_data['state'])->count();

        if ($duplication) 
        {
          Session::flash('error','This city already exist');
          return redirect()->back()->withInput($request->all());
        }
        
        /* Insert into city Table */
        $arr_data = array();
        $arr_data['city_name'] =  $form_data['city_name'];
        $arr_data['state_id'] = $form_data['state'];
        $arr_data['is_active'] =  '1';

        $city = $this->CityModel->create($arr_data);


        if($city)      
        {
            Session::flash('success','City created successfully.');
        }
        else
        {
            Session::flash('error','Problem occured, while creating city.');
        }

        return redirect()->back();
    }


    /*
    | edit() : edit state details
    | auther : Sagar Sainkar
    | Date : 05/05/2016    
    | 
    */
    public function edit($enc_id)
    {

        $city_id = base64_decode($enc_id); 

        /* get city with its states details with country detials and country with its aviliable states (please reffer relations in country , state and city model for more details)*/

        $obj_city = $this->CityModel->where('id', $city_id)->with(['state_details.country_details.states'])->first();

        $arr_city = [];
        if($obj_city)
        {
           $arr_city = $obj_city->toArray();
        }

        $arr_countries = array();

        $obj_countries = $this->CountryModel->orderBy('country_name', 'asc')->get();

        if($obj_countries != FALSE)
        {
            $arr_countries = $obj_countries->toArray();
        }

        //dd($arr_city);
        $this->arr_view_data['enc_id'] = $enc_id;
        $this->arr_view_data['arr_city'] = $arr_city;  
        $this->arr_view_data['arr_countries'] = $arr_countries;
        $this->arr_view_data['page_title'] = "Edit City";
        $this->arr_view_data['module_title'] = "Cities";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.city.edit',$this->arr_view_data);    
    }


    /*
    | update() : update state details
    | auther : Sagar Sainkar
    | Date : 06/05/2016
    | @param  \Illuminate\Http\Request  $request
    | 
    */

    public function update(Request $request, $enc_id)
    {

        $city_id = base64_decode($enc_id);
        $arr_rules = array();
        
        $arr_rules['city_name'] = "required";
        $arr_rules['state'] = "required";
        $arr_rules['country'] = "required";
      
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return back()->withErrors($validator)->withInput();
        }

        $form_data = array();
        $form_data = $request->all();

        $duplication=$this->CityModel->where('city_name',$request->input('city_name'))->where('state_id',$form_data['state'])->whereNotIn('id',[$city_id])->count();

        if ($duplication) 
        {
          Session::flash('error','This city already exist');
          return redirect()->back()->withInput($request->all());
        }


        /* Retrieve Existing city */
        $city = $this->CityModel->where('id',$city_id)->first();

        if(!$city)
        {
            Session::flash('error','Problem occured while updating city.');
            return redirect()->back();   
        }
        
        $arr_data['city_name'] =  $form_data['city_name'];
        $arr_data['state_id'] = $form_data['state'];

        $city_instance = clone $city;
        $status_update = $city_instance->update($arr_data);

        if ($status_update)
        {
            Session::flash('success','City updated successfully.');    
        }

        return redirect()->back();
    }


  	/*
    | Following Fuctions for active ,deactive and delete
    | auther :Sagar Sainkar
    | Date : 06/05/2016
    | 
    */ 

    public function activate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while city activation.');
            return redirect()->back();
        }

        if($this->perform_activate(base64_decode($enc_id)))
        {
            Session::flash('success','City activated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while city activation.');
        }

        return redirect()->back();
    }

    public function deactivate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while city deactivation.');
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            Session::flash('success','City deactivated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while city deactivation.');
        }

        return redirect()->back();
    }

    /*public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while city deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','City deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while city deletion.');
        }

        return redirect()->back();
    }
*/

    public function perform_activate($id)
    {
        if ($id) 
        {
            $obj_city = $this->CityModel->where('id',$id)->first();
            if($obj_city)
            {
                return $obj_city->update(['is_active'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_deactivate($id)
    {
        if ($id) 
        {
            $obj_city = $this->CityModel->where('id',$id)->first();
            if($obj_city)
            {
                return $obj_city->update(['is_active'=>0]);
            }
        }

        return FALSE;
    }

    /*public function perform_delete($id)
    {
        if ($id) 
        {
            $obj_city= $this->CityModel->where('id',$id)->first();
            if($obj_city)
            {
                return $obj_city->delete();
            }
        }
        return FALSE;
    }*/
   

   /*
    | multi_action: Following Fuctions for active ,deactive and delete for multiple records
    | auther :Sagar Sainkar
    | Date : 06/05/2016
    | 
    */ 

    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            Session::flash('error','please select at least one record.');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            
            if($multi_action=="activate")
            {
               $this->perform_activate(base64_decode($record_id));
               Session::flash('success','City(s) activated successfully');
            }
            elseif($multi_action=="deactivate")
            {
               $this->perform_deactivate(base64_decode($record_id));
               Session::flash('success','City(s) blocked successfully.');
            }
           /* elseif($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));
               Session::flash('success','City(s) deleted successfully.');
            } */
        }

        return redirect()->back();
    }

    /*
    | get_states : Ajax fuction to get all states for specific country
    | auther :Sagar Sainkar
    | Date : 06/05/2016
    | 
    */ 

    public function get_states($country_id=null)
    {
    	$response_data = array();
    	if ($country_id) 
    	{
    		$country_id = base64_decode($country_id);

    		$obj_states = $this->StateModel->where('country_id',$country_id)->orderBy('state_name', 'asc')->where('is_active',1)->get();

	        if($obj_states != FALSE)
	        {
	            $response_data['states'] = $obj_states->toArray();
	            $response_data['status'] = 'success';
	            $response_data['msg'] = 'States found.';
	        }
	        else
	        {
	        	$response_data['states'] = null;
	            $response_data['status'] = 'error';
	            $response_data['msg'] = 'No state found for this country';
	        }
    	}
    	else
    	{
    		$response_data['states'] = null;
	        $response_data['status'] = 'error';
	        $response_data['msg'] = 'No state found for this country';
    	}

    	echo json_encode($response_data);
	    exit();
    }

    /*
    | search : search city by name with pagination
    | auther :Sagar Sainkar
    | Date : 06/05/2016
    */ 

    public function search(Request $request)
    {
        $form_data = array();
        $form_data = $request->all();

        if (isset($form_data['search_text']) && $form_data['search_text']!="")
        {
            $arr_cities = array();

            $obj_city = $this->CityModel->where('city_name', 'like','%'.$form_data['search_text'].'%')->with(['state_details.country_details'])->paginate(config('app.project.pagi_cnt'));;

            if(isset($obj_city) && sizeof($obj_city)>0)
            {
                $arr_cities = $obj_city->toArray();
            }

            
            $this->arr_view_data['arr_cities'] = $arr_cities;
        	$this->arr_view_data['obj_city'] = $obj_city;
        }
        else
        {
            $arr_cities = array();
            $obj_city = $this->CityModel->with(['state_details.country_details'])->paginate(config('app.project.pagi_cnt'));;

            if(isset($obj_city) && sizeof($obj_city)>0)
            {
                $arr_cities = $obj_city->toArray();
            }

            $this->arr_view_data['arr_cities'] = $arr_cities;
        	$this->arr_view_data['obj_city'] = $obj_city;

        }

        $this->arr_view_data['page_title'] = "Manage Cities";
        $this->arr_view_data['module_title'] = "Manage Cities";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.city.index',$this->arr_view_data);

    }

}