<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\BlogCommentsModel;
use DataTables;
use Validator;
use Session;
use Sentinel;

use App\Common\Traits\MultiActionTrait;

class BlogCommentsController extends Controller
{
	//use MultiActionTrait;
	function __construct()
	{
		$this->arr_data             = [];
		$this->admin_panel_slug     = 'admin';
		$this->admin_url_path       = url(config('app.project.admin_panel_slug'));
		$this->module_url_path      = $this->admin_url_path.'/blogs/comments';
		$this->module_title         = "Blog Comments";
		$this->module_view_folder   = "admin.blogs_comments";
		$this->module_icon          = "fa fa-comments";
		$this->BaseModel            = new BlogCommentsModel();
		$this->auth                 = Sentinel::check();
		$this->ip_address           = isset($_SERVER['REMOTE_ADDR'])?$_SERVER['REMOTE_ADDR']:false;
	}

	public function index($enc_id='')
	{
		$arr_comments = [];

		if($enc_id=='')
		{
			return redirect()->back();
		}

		$obj_comments = $this->BaseModel->where('blog_id', base64_decode($enc_id));

		$obj_comments = $obj_comments->orderBy('id','DESC')->paginate(10);

		if($obj_comments){
			$arr_comments = $obj_comments->toArray();
		}

		$this->arr_view_data['module_title']     	= 'Manage Blogs';
		$this->arr_view_data['page_title']       	= 'Manage '.$this->module_title;
		$this->arr_view_data['sub_module_title'] 	= 'Manage '.$this->module_title;
		$this->arr_view_data['page_icon']        	= $this->module_icon;
		$this->arr_view_data['module_icon']      	= 'fa fa-comment';
		$this->arr_view_data['sub_module_icon']  	= $this->module_icon;
		$this->arr_view_data['admin_panel_slug'] 	= $this->admin_panel_slug;
		$this->arr_view_data['enc_id']           	= $enc_id;
		$this->arr_view_data['module_url_path']  	= $this->admin_url_path.'/blogs/comments';
		$this->arr_view_data['arr_comments']  		= $arr_comments;

		return view($this->module_view_folder.'.index',$this->arr_view_data);
	}

	public function load_data(Request $request, $enc_id='')
	{

		$search = $request->input('column_filter', null);

		$obj_request_data = $this->BaseModel->where('blog_id', base64_decode($enc_id));

		if(isset($search['q_commnet']) && $search['q_commnet']!='')
		{
			$search_term      = $search['q_commnet'];
			$obj_request_data = $obj_request_data->where('comment', 'like', '%'.$search_term.'%');
		}

		if(isset($search['q_email']) && $search['q_email']!='')
		{
			$search_term      = $search['q_email'];
			$obj_request_data = $obj_request_data->where('user_email', 'like', '%'.$search_term.'%');
		}

		if(isset($search['q_comment_by']) && $search['q_comment_by']!='')
		{
			$search_term      = $search['q_comment_by'];
			$obj_request_data = $obj_request_data->where('user_name', 'like','%'.$search_term.'%');
		}

		$obj_request_data = $obj_request_data->orderBy('id','DESC');
		$json_result      = DataTables::of($obj_request_data)->make(true);
		$build_result     = $json_result->getData();

		if(isset($build_result->data) && sizeof($build_result->data)>0)
		{
			foreach ($build_result->data as $key => $data) 
			{

				$built_delete_href  = $this->module_url_path.'/delete/'.base64_encode($data->id);

				$user_name             = isset($data->user_name)? $data->user_name:'NA';
				$user_email       = isset($data->user_email)? $data->user_email:'NA';
				$comment       = isset($data->comment)? $data->comment:'NA';
				$created_at        = isset($data->created_at)? get_added_on_date($data->created_at):'NA';
				
				$action_btn      = '';
				$status_btn      = '';

				

				$delete_btn   = "<a class='btn btn-default btn-rounded show-tooltip' href='".$built_delete_href."' title='Delete' ";
				$delete_btn .= 'onclick="return confirm_action(this,event,\'Do you really want to delete this record ?\')"';
				$delete_btn .= "><i class='fa fa-trash' ></i></a>";

				$action_btn = $delete_btn;

				$build_result->data[$key]->id         = base64_encode($data->id);
				$build_result->data[$key]->user_name  = $user_name;
				$build_result->data[$key]->user_email = $user_email;
				$build_result->data[$key]->comment    = $comment;
				$build_result->data[$key]->created_at = $created_at;
				$build_result->data[$key]->action_btn = $action_btn;
			}
		}
		return response()->json($build_result);
	}


	public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while comment deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Comment deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while coment deletion.');
        }

        return redirect()->back();
    }

    public function perform_delete($id)
    {
        if ($id) 
        {
            $obj_blog= $this->BaseModel->where('id',$id)->first();

            if($obj_blog)
            {
                return $obj_blog->delete();
            }
        }
        return FALSE;
    }

}
