<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\ProjectpostModel;
use App\Models\ProjectsBidsModel;
use App\Models\ExpertsModel;
use Session;
use Validator;
use Sentinel;

class BidsController extends Controller
{
   
    public function __construct(ProjectpostModel $projects,ProjectsBidsModel $projectbids,ExpertsModel $expert)
    {      
       $this->ProjectpostModel  = $projects;
       $this->ProjectsBidsModel = $projectbids;
       $this->ExpertsModel      = $expert;
       $this->module_url_path   = url(config('app.project.admin_panel_slug')."/bids");
       $this->bid_attachment_public_path = url('/public').config('app.project.img_path.bid_attachment');


    }


	public function all($id='')
    {
        $proj_id=base64_decode($id);
        $arr_all_bids  =array();
        $obj_all_bids  = $this->ProjectsBidsModel->with('project_details','user_details')->where('project_id',$proj_id)->get();
        if($obj_all_bids != FALSE)
        {
            $arr_all_bids = $obj_all_bids->toArray();
        }
        // dd($arr_all_bids);
        $this->arr_view_data['arr_all_bids'] = $arr_all_bids;
        $this->arr_view_data['page_title'] = "Manage Project Bids";
        $this->arr_view_data['module_title'] = "Project Bids";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.project_bids.manage-project-bids',$this->arr_view_data);
    }
  
   
    public function show($bid_id)
    {
        $bid_id = base64_decode($bid_id);

        $obj_bids_info = $this->ProjectsBidsModel->with('project_details','user_details','project_details.expert_info.expert_spoken_languages')->where('id',$bid_id)->first();

        $arr_info = array();

        if($obj_bids_info)
        {
           $arr_info = $obj_bids_info->toArray();
        }
        
        $this->arr_view_data['arr_info']        = $arr_info;
        $this->arr_view_data['page_title']      = "Bid Details";
        $this->arr_view_data['module_title']    = "Project Bids";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        $this->arr_view_data['bid_attachment_public_path'] = $this->bid_attachment_public_path;

        return view('admin.project_bids.show',$this->arr_view_data);
       
    }
    

     public function delete($enc_id = FALSE)
     {
        if(!$enc_id)
        {
            Session::flash('error',trans('controller_translations.text_problem_occured_while_deleting_bid'));
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success',trans('controller_translations.text_Bid_deleted_successfully'));
        }
        else
        {
            Session::flash('error',trans('controller_translations.text_problem_occured_while_deleting_bid'));
        }

        return redirect()->back();
     }

    public function perform_delete($id)
    {
        if ($id) 
        {   
            $bid= $this->ProjectsBidsModel->where('id',$id)->first();

            if($bid!=FALSE)
            {   
                return $bid->delete();   
            }
        }
        return FALSE;
    }

   
    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error',trans('controller_translations.text_sorry_somthing_goes_wrong_please_try_again'));
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Session::flash('success',trans('controller_translations.text_Records_deleted_successfully'));
            } 
            
        }
        return redirect()->back();
    }

}