<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserModel;
use App\Models\SupportCategoryModel;

use Validator;
use Sentinel;
use Session;
use DataTables;
use DB;


class SupportCategoryController extends Controller
{
	function __construct()
	{
		$this->arr_view_data                = [];
		$this->admin_panel_slug             = config('app.project.admin_panel_slug');
		$this->admin_url_path               = url(config('app.project.admin_panel_slug'));
		$this->module_url_path              = $this->admin_url_path."/support_ticket/support_categories";
		$this->module_title                 = "Department Categories";
		$this->module_view_folder			= "admin.support_ticket.categories";
		$this->module_icon                  = "fa fa-ticket";
		$this->UserModel					= new UserModel();
		$this->ip_address                   = isset($_SERVER['REMOTE_ADDR'])?$_SERVER['REMOTE_ADDR']:false;  
		$this->BaseModel					= new SupportCategoryModel();
		$this->admin_profile_image_base_img_path   = base_path().config('app.project.img_path.admin_profile_image');
		$this->admin_profile_image_public_img_path = url('/').config('app.project.img_path.admin_profile_image');
		$this->user_profile_base_img_path     = base_path().config('app.project.img_path.user_profile_image');
		$this->user_profile_public_img_path   = url('/').config('app.project.img_path.user_profile_image');
		$this->support_ticket_base_img_path   = base_path().config('app.project.img_path.support_ticket_files');
		$this->support_ticket_public_img_path = url('/').config('app.project.img_path.support_ticket_files');
		$this->file_format_base_img_path    = base_path().config('app.project.img_path.file_format_image');
		$this->file_format_public_img_path  = url('/').config('app.project.img_path.file_format_image');

		$admin = Sentinel::check();

		$role = get_user_role($admin->id);

		if($role == 'subadmin'){
			Session::flash('error', 'You dont have permission to access module.');
			\Redirect::to($this->admin_url_path.'/dashboard')->send();
		}
	}

	public function index(Request $request)
	{
		$obj_cats = $this->BaseModel->with(['get_tickets'])->get();

		$arr_cats = [];

        if($obj_cats)
        {
            $arr_cats = $obj_cats->toArray();
        }

        $this->arr_view_data['arr_categories'] = $arr_cats;
        $this->arr_view_data['page_title'] = "Manage Department Categories";
        $this->arr_view_data['module_title'] = "Department Categories";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view($this->module_view_folder.'.index',$this->arr_view_data);
	}

	public function create(Request $request)
	{
        $this->arr_view_data['page_title'] = "Create Department Categories";
        $this->arr_view_data['module_title'] = "Department Categories";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view($this->module_view_folder.'.create',$this->arr_view_data);
	}

	public function store(Request $request)
	{
		$arr_rules['title'] = "required";

		$validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $arr_ins['title'] = $request->title;

        if($this->BaseModel->create($arr_ins))
        {
        	Session::flash('success','Record created successfuly.');
          	return redirect($this->module_url_path);
        }else{
        	Session::flash('error','Error while creating record.');
          	return redirect($this->module_url_path);
        }
	}

	public function update($enc_id, Request $request)
	{
		$id = base64_decode($enc_id);

		if(!is_numeric($id)){
			Session::flash('error','Invalid Request.');
          	return redirect()->back();
		}

		$obj_category = $this->BaseModel->where('id', $id)->first();

		if(!$obj_category){
			Session::flash('error','Invalid Request..');
          	return redirect()->back();
		}

		$arr_rules['title'] = "required";

		$validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $arr_ins['title'] = $request->title;

        if($this->BaseModel->where('id', $id)->update($arr_ins))
        {
        	Session::flash('success','Record updated successfuly.');
          	return redirect($this->module_url_path);
        }else{
        	Session::flash('error','Error while updating record.');
          	return redirect($this->module_url_path);
        }

	}

	public function edit($enc_id)
	{
		$id = base64_decode($enc_id);

		if(!is_numeric($id)){
			Session::flash('error','Invalid Request.');
          	return redirect()->back();
		}

		$obj_category = $this->BaseModel->where('id', $id)
										->first();

		if(!$obj_category){
			Session::flash('error','Invalid Request..');
          	return redirect()->back();
		}

		$arr_category = $obj_category->toArray();

        $this->arr_view_data['arr_category'] = $arr_category;
        $this->arr_view_data['page_title'] = "Edit Department Categories";
        $this->arr_view_data['module_title'] = "Department Categories";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view($this->module_view_folder.'.edit',$this->arr_view_data);
	}

	public function delete($enc_id)
	{
		$id = base64_decode($enc_id);

		if(!is_numeric($id)){
			Session::flash('error','Invalid Request.');
          	return redirect()->back();
		}

		$obj_category = $this->BaseModel->whereHas('get_tickets', function(){})
										->where('id', $id)
										->first();

		if($obj_category){
			Session::flash('error','Cannot delete category as it has tickets exists.');
          	return redirect()->back();
		}

		if($this->BaseModel->where('id', $id)->delete()){
			Session::flash('success','Record deleted successfully.');
          	return redirect()->back();
		}else{
			Session::flash('error','Error while deleting record.');
          	return redirect()->back();
		}
	}

}