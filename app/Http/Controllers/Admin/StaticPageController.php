<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Common\Services\LanguageService;  
use App\Models\StaticPageModel;  

use Validator;
use Session;
Use Sentinel;
 
class StaticPageController extends Controller
{

    /*
        Auther : Sagar Sainkar
        Date: 
        Comments: controller for static pages
    */

  
    public $StaticPageModel; 
    
    public function __construct(StaticPageModel $static_page,LanguageService $langauge)
    {      
       $this->StaticPageModel = $static_page;     
       $this->LanguageService = $langauge;

       $this->module_url_path = url(config('app.project.admin_panel_slug')."/static_pages");
    }

    /*
    | Index : Display listing of pages
    | auther :Sagar Sainkar
    | Date : 04/05/2016
    | 
    */
 
    public function index()
    {

        $arr_lang   =  $this->LanguageService->get_all_language();  

        $obj_static_page = $this->StaticPageModel->get();

        if($obj_static_page != FALSE)
        {
            $arr_static_page = $obj_static_page->toArray();
        }

        $this->arr_view_data['arr_static_page'] = $arr_static_page;

        $this->arr_view_data['page_title'] = "Manage CMS";
        $this->arr_view_data['module_title'] = "CMS";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
         
       return view('admin.static_page.index',$this->arr_view_data);
    }


    /*
    | Create : create new page
    | auther :Sagar Sainkar
    | Date : 04/05/2016
    | 
    */ 
    public function create()
    {

        $this->arr_view_data['arr_lang'] = $this->LanguageService->get_all_language();
        $this->arr_view_data['page_title'] = "Create CMS";
        $this->arr_view_data['module_title'] = "CMS";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.static_page.create',$this->arr_view_data);
    }



    /*
    | store() : store page details
    | auther : Sagar Sainkar
    | Date : 04/05/2016
    | @param  \Illuminate\Http\Request  $request
    | 
    */
    public function store(Request $request)
    {
        $form_data = array();

        $form_data = $request->all();
        
        $arr_rules['page_name_en']     = "required";  
        $arr_rules['page_title_en']     = "required";  
        $arr_rules['meta_keyword_en']   = "required"; 
        $arr_rules['meta_title_en']      = "required";  
        $arr_rules['meta_desc_en']      = "required";  
        $arr_rules['page_desc_en']      = "required";  

        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
             return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = $request->all();

        $arr_data = array();
        $arr_data['page_slug'] = str_slug($form_data['page_title_en']);
        $arr_data['is_active'] = 1;            

    
        $duplication=$this->StaticPageModel->where('page_slug',$arr_data['page_slug'])->count();

        if ($duplication) 
        {
          Session::flash('error', trans('This page already exist.') );
          return redirect()->back()->withInput($request->all());
        }

        
      
        
        $static_page    = $this->StaticPageModel->create($arr_data);

        $static_page_id = $static_page->id;

        /* Fetch All Languages*/
        $arr_lang =  $this->LanguageService->get_all_language();

        if(sizeof($arr_lang) > 0 )
        {
            foreach ($arr_lang as $lang) 
            {            
                $arr_data     = array();
                $page_name   = 'page_name_'.$lang['locale'];
                $page_title   = 'page_title_'.$lang['locale'];
                $meta_keyword = 'meta_keyword_'.$lang['locale'];
                $meta_title = 'meta_title_'.$lang['locale'];
                $meta_desc    = 'meta_desc_'.$lang['locale'];
                $page_desc    =  'page_desc_'.$lang['locale'];

                if( isset($form_data[$page_title]) && $form_data[$page_title] != '')
                { 
                    $translation = $static_page->translateOrNew($lang['locale']);

                    $translation->page_name       = ucfirst($form_data[$page_name]);
                    $translation->page_title      = ucfirst($form_data[$page_title]);
                    $translation->meta_keyword    = $form_data[$meta_keyword];
                    $translation->meta_title      = $form_data[$meta_title];
                    $translation->meta_desc       = $form_data[$meta_desc];
                    $translation->page_desc       = $form_data[$page_desc];
                    $translation->static_page_id  = $static_page_id;

                    $translation->save();

                    Session::flash('success','Page Created Successfully.');
                }

            }//foreach

        } //if
        else
        {
            Session::flash('error','Problem Occured, While Creating page.');
            
        }

        return redirect()->back();
    }

     /*
    | edit() : edit page details
    | auther : Sagar Sainkar
    | Date : 04/05/2016    
    | 
    */

    public function edit($enc_id)
    {
        $id = base64_decode($enc_id);

        $arr_lang = $this->LanguageService->get_all_language();      

        $obj_static_page = $this->StaticPageModel->where('id', $id)->with(['translations'])->first();

        $arr_static_page = [];

        if($obj_static_page)
        {
           $arr_static_page = $obj_static_page->toArray(); 
           /* Arrange Locale Wise */
           $arr_static_page['translations'] = $this->arrange_locale_wise($arr_static_page['translations']);
        }

        $this->arr_view_data['enc_id'] = $enc_id;
        $this->arr_view_data['arr_lang'] = $this->LanguageService->get_all_language();          
        $this->arr_view_data['arr_static_page'] = $arr_static_page;  
        $this->arr_view_data['page_title'] = "Edit CMS";
        $this->arr_view_data['module_title'] = "CMS";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.static_page.edit',$this->arr_view_data);  

    }

    /*
    | update() : update page details
    | auther : Sagar Sainkar
    | Date : 04/05/2016
    | @param  \Illuminate\Http\Request  $request
    | 
    */
    public function update(Request $request, $enc_id)
    {
        $page_id = base64_decode($enc_id);
        $arr_rules = array();
        $status = FALSE;

        $arr_rules['page_name_en']     = "required";  
        $arr_rules['page_title_en']     = "required";  
        $arr_rules['meta_keyword_en']   = "required"; 
        $arr_rules['meta_title_en']      = "required";  
        $arr_rules['meta_desc_en']      = "required";  
        $arr_rules['page_desc_en']      = "required";  

        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = array();
        $form_data = $request->all(); 

         /* Get All Active Languages */ 

        $duplication=$this->StaticPageModel->where('page_slug',str_slug($form_data['page_title_en']))->whereNotIn('id',[$page_id])->count();

        if ($duplication) 
        {
          Session::flash('error', trans('This page already exist.') );
          return redirect()->back()->withInput($request->all());
        }



  
        $arr_lang = $this->LanguageService->get_all_language();

        $pages = $this->StaticPageModel->where('id',$page_id)->first();

        if ($pages) 
        {
            $arr_data = array();
            $arr_data['page_slug'] = str_slug($form_data['page_title_en']);
            $static_page    = $pages->update($arr_data);
        }
        else
        {
            Session::flash('error','Error while updating page.');       
            return redirect()->back();
        }
        

         /* Insert Multi Lang Fields */

        if(sizeof($arr_lang) > 0)
        { 
            foreach($arr_lang as $i => $lang)
            {
                $translate_data_ary = array();
                $title = 'page_title_'.$lang['locale'];

                if(isset($form_data[$title]) && $form_data[$title]!="")
                {
                    /* Get Existing Language Entry */
                    $translation = $pages->getTranslation($lang['locale']);    
                    if($translation)
                    {
                        $translation->page_name       =  ucfirst($form_data['page_name_'.$lang['locale']]);
                        $translation->page_title      =  ucfirst($form_data['page_title_'.$lang['locale']]);
                        $translation->meta_title      =  $form_data['meta_title_'.$lang['locale']];
                        $translation->meta_keyword    =  $form_data['meta_keyword_'.$lang['locale']];
                        $translation->meta_desc       =  $form_data['meta_desc_'.$lang['locale']];
                        $translation->page_desc       =  $form_data['page_desc_'.$lang['locale']];

                        $status = $translation->save();                       
                    }  
                    else
                    {
                        /* Create New Language Entry  */
                        $translation     = $pages->getNewTranslation($lang['locale']);

                        $translation->static_page_id  =  $page_id;
                        $translation->page_name       =  ucfirst($form_data['page_name_'.$lang['locale']]);
                        $translation->page_title      =  ucfirst($form_data['page_title_'.$lang['locale']]);
                        $translation->meta_title      =  $form_data['meta_title_'.$lang['locale']];
                        $translation->meta_keyword    =  $form_data['meta_keyword_'.$lang['locale']];
                        $translation->meta_desc       =  $form_data['meta_desc_'.$lang['locale']];
                        $translation->page_desc       =  $form_data['page_desc_'.$lang['locale']];

                        $status = $translation->save();
                    } 
                }   
            }
            
        }

        if ($status) 
        {
            Session::flash('success','Page updated successfully.');    
        }
        else
        {
            Session::flash('error','Error while updating page.');       
        }
        
        
      
        return redirect()->back();
    }


    /*
    | Following Fuctions for active ,deactive and delete
    | auther :Sagar Sainkar
    | Date : 04/05/2016
    | 
    */ 
    public function activate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while static page activation.');
            return redirect()->back();
        }

        if($this->perform_activate(base64_decode($enc_id)))
        {
            Session::flash('success','Static page activated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while static page activation.');
        }

        return redirect()->back();
    }

    public function deactivate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while static page deactivation.');
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            Session::flash('success','Static page deactivated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while static page deactivation.');
        }

        return redirect()->back();
    }

    public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while static page deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Static page deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while static page deletion.');
        }

        return redirect()->back();
    }


    public function perform_activate($id)
    {
        if ($id) 
        {
            $static_page = $this->StaticPageModel->where('id',$id)->first();
            if($static_page)
            {
                return $static_page->update(['is_active'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_deactivate($id)
    {
        if ($id) 
        {
            $static_page = $this->StaticPageModel->where('id',$id)->first();
            if($static_page)
            {
                return $static_page->update(['is_active'=>0]);
            }
        }

        return FALSE;
    }

    public function perform_delete($id)
    {
        if ($id) 
        {
            $static_page= $this->StaticPageModel->where('id',$id)->first();
            if($static_page)
            {
                return $static_page->delete();
            }
        }
        return FALSE;
    }
   

   /*
    | multi_action: Following Fuctions for active ,deactive and delete for multiple records
    | auther :Sagar Sainkar
    | Date : 04/05/2016
    | 
    */ 

    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Session::flash('success','Static page(s) deleted successfully.');
            } 
            elseif($multi_action=="activate")
            {
               $this->perform_activate(base64_decode($record_id)); 
               Session::flash('success','Static page(s) activated successfully');               
            }
            elseif($multi_action=="deactivate")
            {
               $this->perform_deactivate(base64_decode($record_id));    
               Session::flash('success','Static page(s) blocked successfully.');
            }
        }

        return redirect()->back();
    }

    public function arrange_locale_wise(array $arr_data)
    {
        if(sizeof($arr_data)>0)
        {
            foreach ($arr_data as $key => $data) 
            {
                $arr_tmp = $data;
                unset($arr_data[$key]);

                $arr_data[$data['locale']] = $data;                    
            }

            return $arr_data;
        }
        else
        {
            return [];
        }
    }


}