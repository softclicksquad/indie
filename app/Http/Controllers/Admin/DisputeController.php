<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\DisputeModel;
use App\Models\ExpertsModel;
use App\Models\ClientsModel;
use App\Models\ProjectpostModel;


/*    START - Laravel messanger */

use App\Models\ProjectMessagingThreadsModel;
use Cmgmyr\Messenger\Models\Thread as MessangerThreadModel;
use Cmgmyr\Messenger\Models\Message as MessangerMessageModel;
use Cmgmyr\Messenger\Models\Participant as MessangerParticipantModel;

use Messagable;
/*    END - Laravel messanger */

use App\Http\Requests;

use Session;
use Validator;
use Sentinel;




class DisputeController extends Controller
{
    public function __construct(    
                                    DisputeModel $dispute,
                                    ExpertsModel $experts,
                                    ClientsModel $clients,
                                    ProjectMessagingThreadsModel $project_messaging_thread,
                                    MessangerMessageModel $messanger_thread ,
                                    ProjectpostModel $project_post   
                               )
    {      
        $this->DisputeModel         = $dispute;
        $this->ExpertsModel         = $experts;
        $this->ClientsModel         = $clients;
        $this->MessangerMessageModel         = $messanger_thread;
        $this->ProjectMessagingThreadsModel  = $project_messaging_thread;
        $this->ProjectpostModel     = $project_post;

        $this->BaseModel            = $this->DisputeModel;

        $this->module_title         = "Dispute";
        $this->arr_view_data        = [];
        $this->module_folder_path   = 'admin.dispute';    
        $this->module_url_path      = url(config('app.project.admin_panel_slug')."/dispute"); 
    }

    /*
        Comments  : Show Disputes.
        Author    : Nayan S.
    */
	
    public function index()
    {
        if(Session::has('DISPUTE') && Session::has('DISPUTE') == "DISPUTE" )
        {
            Session::forget('DISPUTE');   
        }

        $arr_data = [];

        $obj_data = $this->BaseModel->with('project_details')->get();

        if($obj_data)
        {
           $arr_data = $obj_data->toArray();   
        }

        $this->arr_view_data['page_title']      = 'View '.str_plural($this->module_title);
        $this->arr_view_data['module_title']    = str_plural($this->module_title);
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        $this->arr_view_data['arr_data']        = $arr_data;
 
        return view($this->module_folder_path.'.index',$this->arr_view_data);
    }

    /*
        Comments  : Show Disputes.
        Author    : Nayan S.
    */
    
    public function view($enc_id)
    {
        $project_id = base64_decode($enc_id);

        if($project_id == "")
        {
            return redirect()->back();
        }

        $arr_data = [];

        $obj_data = $this->BaseModel->where('project_id','=',$project_id)
                                    ->with('experts_details','clients_details','project_details')    
                                    ->first();

        if($obj_data)
        {
           $arr_data = $obj_data->toArray();   
        }

        $this->arr_view_data['page_title']      = 'View '.str_singular($this->module_title);
        $this->arr_view_data['module_title']    = 'View '.str_plural($this->module_title);
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        $this->arr_view_data['arr_data']        = $arr_data;
 
        return view($this->module_folder_path.'.view',$this->arr_view_data);
    }

    /*
        Comments  : Show Conversation Messages.
        Author    : Nayan S.
    */

    public function show_messages($enc_id,$is_manager_exists = FALSE)
    {   
        $project_id = base64_decode($enc_id);

        $arr_conversation = $arr_user = [];
        $expert_user_id   =  "";
        $client_user_id   =  "";
        $project_manager_user_id   =  "";

        if($project_id == "")
        {
            return redirect()->back();
        }

        $obj_users =  $this->BaseModel->where('project_id','=',$project_id)->with('project_details')->first();
          
        if($obj_users)
        {
            $expert_user_id = $obj_users->expert_user_id;
            $client_user_id = $obj_users->client_user_id;
            
            $arr_user = $obj_users->toArray();
            $project_name   =  isset($obj_users['project_details']['project_name']) ? $obj_users['project_details']['project_name']:''; 

        }
        else
        {
            Session::flash("error","There are no messages for this conversation."); 
            return redirect()->back();
        }   


        $obj_thread = $this->ProjectMessagingThreadsModel->where('project_id','=',$project_id)
                                                         ->where('client_user_id','=',$client_user_id);

        if($is_manager_exists == TRUE)
        {   
            $obj_project = $this->ProjectpostModel->where('id','=',$project_id)->first();
            
            if($obj_project)
            {
                if(isset($obj_project->project_manager_user_id) && $obj_project->project_manager_user_id != "" )   
                {
                    $project_manager_user_id = $obj_project->project_manager_user_id;
                }
            }
            
            if($project_manager_user_id != "")
            {
                $obj_thread = $obj_thread->where('project_manager_user_id','=',$project_manager_user_id);
            }
            else
            {
                Session::flash("error","There are no messages for this conversation."); 
                return redirect()->back();   
            }
        }   
        else if($expert_user_id != "")
        {
            $obj_thread = $obj_thread->where('expert_user_id','=',$expert_user_id);
        }
        else 
        {
            Session::flash("error","There are no messages for this conversation."); 
            return redirect()->back();   
        }                                                                                

        $obj_thread = $obj_thread->first();

        if($obj_thread)
        {
            $thread_id  = $obj_thread->message_thread_id; 

            /*----------------------------------------------
             Get conversation with project id
            ------------------------------------------------*/

            $threads = $this->MessangerMessageModel->with( 'user' )      
                                                   ->where(['thread_id' => $thread_id])
                                                   ->orderBy('id', 'ASC')
                                                   ->get();
      
            if($threads)
            {
                $arr_conversation = $threads->toArray();
            }
            
            /*-------Ends --------*/
        }
        
        $this->arr_view_data['page_title']      = 'View Messages';
        $this->arr_view_data['module_title']    = 'View '.str_plural($this->module_title);
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        $this->arr_view_data['arr_conversation']= $arr_conversation;
        $this->arr_view_data['project_name']    = $project_name;
        $this->arr_view_data['client_user_id']  = $client_user_id;
        $this->arr_view_data['expert_user_id']  = $expert_user_id;

        return view($this->module_folder_path.'.view_messages',$this->arr_view_data);
    }


    /*
        Comments  : Show Conversation Messages of project manager.
        Author    : Nayan S.
    */
        public function show_messages_for_project_manager($enc_id)
        {   
            return $this->show_messages($enc_id,$is_manager_exists = TRUE);
        }

    /*
        Comments  : Updates admin Comments to dispute .
        Author    : Nayan S.
    */

    public function add_admin_comments(Request $request)
    { 
        $arr_rules = [];
        $form_data = [];

        $arr_rules['dispute_id'] = 'required';
        $arr_rules['comments']   = 'required';

        $validator = Validator::make($request->all(),$arr_rules);

        $form_data = $request->all();
        
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($form_data);
        }

        $result = $this->BaseModel->where('id','=',base64_decode($form_data['dispute_id']))->update(['admin_comments'=>$form_data['comments']]);

        if($result)
        {
            Session::flash('success','Comments Saved Successfully.');
        } 
        else
        {
            Session::flash('error','Problem occured, while saving comments.');
        }
        
        return redirect()->back();
    }

}   
