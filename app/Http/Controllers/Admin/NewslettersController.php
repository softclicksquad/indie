<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models\NewslettersModel;
use App\Models\NewslettersSubscriberModel;
use Session;
use Validator;
use Sentinel;
use Mail;

class NewslettersController extends Controller
{
    /*
        Auther : Sagar Sainkar
        Comments: controller for manage newsletters
    */

    public function __construct(NewslettersModel $newsletters ,NewslettersSubscriberModel $subscriber)
    {      
       $this->NewslettersModel = $newsletters;
       $this->NewslettersSubscriberModel = $subscriber;
       $this->module_url_path = url(config('app.project.admin_panel_slug')."/newsletters");
    }


     /*
        Auther : Sagar Sainkar
        Comments: display all newsletters
    */
	public function index()
    {

        $obj_news = $this->NewslettersModel->get();

        if($obj_news != FALSE)
        {
            $arr_newsletters = $obj_news->toArray();
        }

        $this->arr_view_data['arr_newsletters'] = $arr_newsletters;

        $this->arr_view_data['page_title'] = "Manage Newsletters";
        $this->arr_view_data['module_title'] = "Newsletters";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
         
        return view('admin.newsletters.index',$this->arr_view_data);
    }


    /*  
        Auther : Sagar Sainkar
        Comments: display view for Add new Newsletters
    */

    public function create()
    {
        $this->arr_view_data['page_title'] = "Create Newsletters";
        $this->arr_view_data['module_title'] = "Newsletters";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.newsletters.create',$this->arr_view_data);
    }

    /*  
        Auther : Sagar Sainkar
        Comments: Add and store newsletter details
    */
    public function store(Request $request)
    {
        $form_data = array();

        $form_data = $request->all();
        
        $arr_rules['title'] = "required|max:255";
        $arr_rules['subject'] = "required|max:255";
        $arr_rules['news_message'] = "required";
        
        
        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
             return redirect()->back()->withErrors($validator)->withInput($request->all());
        }
    
        $form_data = $request->all();
        $arr_data = array();
        $arr_data['title'] = ucfirst($form_data['title']);
        $arr_data['subject'] = ucfirst($form_data['subject']);
        $arr_data['news_message'] = $form_data['news_message'];
        $arr_data['is_active'] = 1;

		$newsletter    = $this->NewslettersModel->create($arr_data);
        
        if($newsletter)
        {
        	Session::flash('success','Newsletter created successfully.');
        } 
        else
        {
            Session::flash('error','Problem occured, while creating newsletter.');
        }

        return redirect()->back();
    }


    /*  
        Auther : Sagar Sainkar
        Comments: display view for edit skill
    */

    public function edit($enc_id)
    {
        $id = base64_decode($enc_id);

        $obj_news = $this->NewslettersModel->where('id', $id)->first();

        $arr_newsletter = array();

        if($obj_news)
        {
           $arr_newsletter = $obj_news->toArray();
        }

        $this->arr_view_data['enc_id'] = $enc_id;
        $this->arr_view_data['arr_newsletter'] = $arr_newsletter;  
        $this->arr_view_data['page_title'] = "Edit Newsletter";
        $this->arr_view_data['module_title'] = "Newsletters";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.newsletters.edit',$this->arr_view_data);  

    }


    /*  
        Auther : Sagar Sainkar
        Comments: update skill details
    */
    public function update(Request $request, $enc_id)
    {
        $newsletter_id = base64_decode($enc_id);
        $arr_rules = array();
        $status = FALSE;

      	$arr_rules['title'] = "required|max:255";
        $arr_rules['subject'] = "required|max:255";
        $arr_rules['news_message'] = "required";
        
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = array();
        $form_data = $request->all(); 

        $obj_news = $this->NewslettersModel->where('id',$newsletter_id)->first();

        if($obj_news && sizeof($obj_news) > 0)
        { 	
        	$arr_data = array();
            $arr_data['title'] = ucfirst($form_data['title']);
        	$arr_data['subject'] = ucfirst($form_data['subject']);
        	$arr_data['news_message'] = $form_data['news_message'];

        	$status = $obj_news->update($arr_data);
        }

        if ($status) 
        {
            Session::flash('success','Newsletter updated successfully.');    
        }
        else
        {
            Session::flash('error','Error while updating newsletter.');       
        }
        
        return redirect()->back();
    }



    public function show($enc_id)
    {
        $id = base64_decode($enc_id);

        $obj_news = $this->NewslettersModel->where('id', $id)->first();

        $arr_newsletter = array();

        if($obj_news)
        {
           $arr_newsletter = $obj_news->toArray();
        }

        $this->arr_view_data['enc_id'] = $enc_id;
        $this->arr_view_data['arr_newsletter'] = $arr_newsletter;  
        $this->arr_view_data['page_title'] = "Edit Newsletter";
        $this->arr_view_data['module_title'] = "Newsletters";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.newsletters.show',$this->arr_view_data);  

    }

    /*
    | Following Fuctions for active ,deactive and delete
    | auther :Sagar Sainkar    
    | 
    */ 

    public function activate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while newsletter activation.');
            return redirect()->back();
        }

        if($this->perform_activate(base64_decode($enc_id)))
        {
            Session::flash('success','Newsletter activated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while newsletter activation.');
        }

        return redirect()->back();
    }

    public function deactivate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
        	Session::flash('error','Problem occured while newsletter deactivation.');
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            Session::flash('success','Newsletter deactivated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while newsletter deactivation.');
        }

        return redirect()->back();
    }

    public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
        	Session::flash('error','Problem occured while newsletter deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Newsletter deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while newsletter deletion.');
        }

        return redirect()->back();
    }


    public function perform_activate($id)
    {
        if ($id) 
        {
            $newsletter = $this->NewslettersModel->where('id',$id)->first();
            if($newsletter)
            {
                return $newsletter->update(['is_active'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_deactivate($id)
    {
        if ($id) 
        {
            $newsletter = $this->NewslettersModel->where('id',$id)->first();
            if($newsletter)
            {
                return $newsletter->update(['is_active'=>0]);
            }
        }

        return FALSE;
    }

    public function perform_delete($id)
    {
        if ($id) 
        {
            $newsletter= $this->NewslettersModel->where('id',$id)->first();
            if($newsletter)
            {
                return $newsletter->delete();
            }
        }
        return FALSE;
    }
   

     /*
    | multi_action: Following Fuctions for active ,deactive and delete for multiple records
    | auther :Sagar Sainkar    
    | 
    */ 
    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Session::flash('success','Newsletter(s) deleted successfully.');
            } 
            elseif($multi_action=="activate")
            {
               $this->perform_activate(base64_decode($record_id)); 
               Session::flash('success','Newsletter(s) activated successfully');               
            }
            elseif($multi_action=="deactivate")
            {
               $this->perform_deactivate(base64_decode($record_id));    
               Session::flash('success','Newsletter(s) blocked successfully.');
            }
        }

        return redirect()->back();
    }



    public function send()
    {


        $obj_news = $this->NewslettersModel->where('is_active',1)->get();

        if($obj_news != FALSE)
        {
            $arr_newsletters = $obj_news->toArray();
        }

        $this->arr_view_data['arr_newsletters'] = $arr_newsletters;

        $obj_subscriber = $this->NewslettersSubscriberModel->where('is_active',1)->get();

        if($obj_subscriber != FALSE)
        {
            $arr_subscriber = $obj_subscriber->toArray();
        }

        $this->arr_view_data['arr_subscriber'] = $arr_subscriber;

        $this->arr_view_data['page_title'] = "Send Newsletter";
        $this->arr_view_data['module_title'] = "Newsletters";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
         
        return view('admin.newsletters.newsletter_send',$this->arr_view_data);
        
    }   

    public function send_email(Request $request)
    {
        $arr_rules = array();

        $arr_rules['news_letter'] = "required";

        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $news_letter_id = base64_decode($request->input('news_letter'));
        $checked_record = $request->input('checked_record');

        
        if(isset($checked_record) && sizeof($checked_record)>0)
        {
            $newsletter_details = $this->NewslettersModel->where('id',$news_letter_id)->first();
            if (isset($newsletter_details) && $newsletter_details!=FALSE)
            {
                $newsletter_details = $newsletter_details->toArray();
                
                foreach ($checked_record as $key => $email_id) 
                {  
                  $project_name = config('app.project.name');
                  $mail_subject = isset($newsletter_details['subject'])?$newsletter_details['subject']:'';
                  //$mail_form = isset($website_contact_email)?$website_contact_email:'support@archexperts.com';
                  $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
                    try{
                        Mail::send('admin.email.newsletter', $newsletter_details, function ($message) use ($email_id,$mail_form,$project_name,$mail_subject) 
                        {
                          $message->from($mail_form, $project_name);
                          $message->subject($project_name.':'.$mail_subject);
                          $message->to($email_id);
                        }); 
                        Session::flash('success','Newsletter send successfully.');
                    }
                    catch(\Exception $e){
                    Session::Flash('error'   , 'E-Mail not sent');
                    }
                }
            }
        }
        else
        {
            Session::flash('error','Please select subscriber to send newsletter.');
            return redirect()->back();
        }

        return redirect()->back();
        
    }
}