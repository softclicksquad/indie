<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\BlogsModel;
use DataTables;
use Validator;
use Session;
use Sentinel;

use App\Common\Traits\MultiActionTrait;

class BlogsController extends Controller
{
	//use MultiActionTrait;
	function __construct()
	{
		$this->arr_data             = [];
		$this->admin_panel_slug     = 'admin';
		$this->admin_url_path       = url(config('app.project.admin_panel_slug'));
		$this->module_url_path      = $this->admin_url_path.'/blogs';
		$this->module_title         = "Blogs";
		$this->module_view_folder   = "admin.blogs";
		$this->module_icon          = "fa fa-comment";
		$this->BaseModel            = new BlogsModel();
		$this->auth                 = Sentinel::check();
		$this->ip_address           = isset($_SERVER['REMOTE_ADDR'])?$_SERVER['REMOTE_ADDR']:false;
		$this->blog_image_base_path = base_path().'/public'.config('app.project.img_path.blog_image');
		$this->blog_image_public_path = url('/').config('app.project.img_path.blog_image');
	}

	public function index(Request $request)
	{
		$arr_blogs = [];
		$obj_request_data = $this->BaseModel->with('comments')
											->orderBy('id','DESC');

		if($request->has('search_text') && $request->input('search_text') != ''){
			$obj_request_data = $obj_request_data->where('name', 'like', '%'.$request->input('search_text').'%');
		}

		$obj_request_data = $obj_request_data->paginate(10);

		if($obj_request_data){
			$arr_blogs = $obj_request_data->toArray();
		}

		$this->arr_view_data['page_title']       	= 'Manage '.$this->module_title;
		$this->arr_view_data['module_title']     	= 'Manage '.$this->module_title;
		$this->arr_view_data['page_icon']        	= $this->module_icon;
		$this->arr_view_data['module_icon']      	= $this->module_icon;
		$this->arr_view_data['admin_panel_slug'] 	= $this->admin_panel_slug;
		$this->arr_view_data['module_url_path']  	= $this->module_url_path;
		$this->arr_view_data['arr_blogs']  			= $arr_blogs;

		return view($this->module_view_folder.'.index',$this->arr_view_data);
	}

	public function load_data(Request $request)
	{
		$search = $request->input('column_filter', null);

		$obj_request_data = $this->BaseModel->with('comments');

		if(isset($search['q_name']) && $search['q_name']!='')
		{
			$search_term      = $search['q_name'];
			$obj_request_data = $obj_request_data->where('name', 'like', '%'.$search_term.'%');
		}

		if(isset($search['q_status']) && $search['q_status']!='')
		{
			$search_term      = $search['q_status'];
			$obj_request_data = $obj_request_data->where('is_active',$search_term);
		}

		$obj_request_data = $obj_request_data->orderBy('id','DESC');
		$json_result      = DataTables::of($obj_request_data)->make(true);
		$build_result     = $json_result->getData();

		if(isset($build_result->data) && sizeof($build_result->data)>0)
		{
			foreach ($build_result->data as $key => $data) 
			{
				$built_edit_href    = $this->module_url_path.'/edit/'.base64_encode($data->id);
				$built_view_href    = $this->module_url_path.'/view/'.base64_encode($data->id);
				$built_delete_href  = $this->module_url_path.'/delete/'.base64_encode($data->id);
				$built_comment_href = $this->module_url_path.'/comments/'.base64_encode($data->id);

				$title             = isset($data->name)? $data->name:'NA';
				$description       = isset($data->description)? $data->description:'NA';
				$created_at        = isset($data->created_at)? get_added_on_date($data->created_at):'NA';
				
				$action_btn      = '';
				$status_btn      = '';

				if($data->is_active != null && $data->is_active == "0")
				{   
					$status_btn = '<a class="btn btn-xs btn-danger" title="In-active" href="'.$this->module_url_path.'/unblock/'.base64_encode($data->id).'" 
					onclick="return confirm_action(this,event,\'Do you really want to activate this record ?\')" >Inactive</a>';
				}
				elseif($data->is_active != null && $data->is_active == "1")
				{
					$status_btn = '<a class="btn btn-xs btn-success" title="Active" href="'.$this->module_url_path.'/block/'.base64_encode($data->id).'" onclick="return confirm_action(this,event,\'Do you really want to inactivate this record ?\')" >Active</a>';
				}

				$edit_btn     = "<a class='btn btn-default btn-rounded show-tooltip' href='".$built_edit_href."' title='Edit' ><i class='fa fa-pencil-square-o' ></i></a>";

				$view_btn     = "<a class='btn btn-default btn-rounded show-tooltip' href='".$built_view_href."' title='View' ><i class='fa fa-eye' ></i></a>";
				$delete_btn   = "<a class='btn btn-default btn-rounded show-tooltip' href='".$built_delete_href."' title='Delete' ";
				$delete_btn .= 'onclick="return confirm_action(this,event,\'Do you really want to delete this record ?\')"';
				$delete_btn .= "><i class='fa fa-trash' ></i></a>";
				$comment_btn = '';
				if(isset($data->comments) && $data->comments!=null)
				{
					$comment_btn     = "<a class='btn btn-default btn-rounded show-tooltip' href='".$built_comment_href."' title='Comments' ><i class='fa fa-comments-o' ></i></a>";
				}


				$action_btn = $view_btn.' '.$edit_btn.' '.$comment_btn.' '.$delete_btn;

				if(isset($data->image))
				{
					$image_url =$this->blog_image_public_path.$data->image;
				}
				else
				{
					$image_url = get_default_image(46,80,10,'No Advertisement' );
				}

				$advertisement = '<img src="'.$image_url.'" style="width: 80px!important;">';

				$build_result->data[$key]->name          = $title;
				$build_result->data[$key]->id            = base64_encode($data->id);
				$build_result->data[$key]->created_at    = $created_at;
				$build_result->data[$key]->description   = str_limit($description,150);
				$build_result->data[$key]->action_btn    = $action_btn;
				$build_result->data[$key]->status_btn    = $status_btn;
				$build_result->data[$key]->advertisement = $advertisement;
			}
		}
		return response()->json($build_result);
	}

	public function create()
	{
		$this->arr_view_data['page_title']       = 'Create '.str_singular($this->module_title);
		$this->arr_view_data['page_icon']        = $this->module_icon;
		$this->arr_view_data['module_title']     = 'Manage '.$this->module_title;
		$this->arr_view_data['sub_module_title'] = 'Create '.$this->module_title;
		$this->arr_view_data['sub_module_icon']  = 'fa fa-plus';
		$this->arr_view_data['module_icon']      = $this->module_icon;
		$this->arr_view_data['admin_panel_slug'] = $this->admin_panel_slug;
		$this->arr_view_data['module_url_path']  = $this->module_url_path;

		return view($this->module_view_folder.'.create',$this->arr_view_data);
	}

	public function store(Request $request)
	{
		$arr_rules      = $arr_front_page = array();
		$status         = false;

		$arr_rules['name']         	= "required";
		$arr_rules['category']     	= "required";
		$arr_rules['blog_description'] 	= "required";
		$arr_rules['image-editor'] 	= "required";

		$validator = validator::make($request->all(),$arr_rules);

		if ($validator->fails()) 
		{
			return redirect()->back()->withErrors($validator)->withInput();
		}

		$name             = $request->input('name', null);
		$image_data       = $request->input('image-data', null);
		$category         = $request->input('category', null);
		$blog_description = $request->input('blog_description', null);
		$tags             = $request->input('tags', null);

		$arr_data['name']        = filter_data($name);
		$arr_data['description'] = $blog_description;
		$arr_data['tags'] = $tags;
		$arr_data['slug']        = $this->create_slug($name);
		$arr_data['category_id'] = base64_decode($category);
		$arr_data['created_by']  = $this->auth->id;
		
		if($image_data)
		{
			$encoded_image     = base64_decode($image_data);
			$filename          = sha1(uniqid().uniqid()) . '.' . 'png';
			$output_file       = $this->blog_image_base_path.$filename;
			$status            = base64ToImage($image_data, $output_file);
			$arr_data['image'] = $filename;
		}
		
		$status = $this->BaseModel->create($arr_data);

		if($status)
		{
			Session::flash('success', str_singular($this->module_title).' created successfully.');
			return redirect($this->module_url_path);
		}
		Session::flash('error', 'Error while creating '.str_singular($this->module_title).'.');
		return redirect($this->module_url_path);
	}

	private function create_slug($string='',$id=null)
	{
		$slug = '';
		if($string!='')
		{
			$slug  = str_slug($string);
			$count = $this->BaseModel;
			
			if($id!=null)
			{
				$count = $count->where('id','!=',$id);
			}
			$count = $count->where('slug','like','%'.$slug.'%')
			->count();
			if($count>0)
			{
				$slug  = $slug.'-'.($count+1);
			}
		}
		return $slug;
	}

	public function edit($enc_id='')
	{
		if($enc_id=='')
		{
			return redirect()->back();
		}

		$obj_data = $this->BaseModel->where('id', base64_decode($enc_id))->first();
		$arr_data = [];

		if($obj_data)
		{
			$arr_data = $obj_data->toArray();
		}
		else
		{
			return redirect()->back();
		}

		$this->arr_view_data['page_title']             = 'Edit '.str_singular($this->module_title);
		$this->arr_view_data['page_icon']              = $this->module_icon;
		$this->arr_view_data['module_title']           = 'Manage '.$this->module_title;
		$this->arr_view_data['sub_module_title']       = 'Edit '.$this->module_title;
		$this->arr_view_data['sub_module_icon']        = 'fa fa-pencil-square-o';
		$this->arr_view_data['module_icon']            = $this->module_icon;
		$this->arr_view_data['admin_panel_slug']       = $this->admin_panel_slug;
		$this->arr_view_data['module_url_path']        = $this->module_url_path;
		$this->arr_view_data['arr_data']               = $arr_data;
		$this->arr_view_data['enc_id']                 = $enc_id;
		$this->arr_view_data['blog_image_public_path'] = $this->blog_image_public_path;

		return view($this->module_view_folder.'.edit',$this->arr_view_data);
	}

	public function update(Request $request, $enc_id='')
	{
		$arr_rules = $arr_front_page = array();
		$status    = false;

		$arr_rules['name']         	     = "required";
		$arr_rules['category']     	     = "required";
		$arr_rules['blog_description'] 	 = "required";

		$validator = validator::make($request->all(),$arr_rules);

		if ($validator->fails()) 
		{
			return redirect()->back()->withErrors($validator)->withInput();
		}

		$name             = $request->input('name', null);
		$image_data       = $request->input('image-data', null);
		$category         = $request->input('category', null);
		$blog_description = $request->input('blog_description', null);
		$tags             = $request->input('tags', null);

		$arr_data['name']        = filter_data($name);
		$arr_data['description'] = $blog_description;
		$arr_data['tags']        = $tags;
		// $arr_data['slug']     = $this->create_slug($name);
		$arr_data['category_id'] = base64_decode($category);
		$arr_data['updated_by']  = $this->auth->id;
		$arr_data['slug']        = $this->create_slug($name,base64_decode($enc_id));

		if($image_data)
		{
			$encoded_image     = base64_decode($image_data);
			$filename          = sha1(uniqid().uniqid()) . '.' . 'png';
			$output_file       = $this->blog_image_base_path.$filename;
			$status            = base64ToImage($image_data, $output_file);
			$arr_data['image'] = $filename;
			$obj_data          = $this->BaseModel->where('id', base64_decode($enc_id))->first();
			if(file_exists($this->blog_image_base_path. $obj_data->image)){
				@unlink($this->blog_image_base_path. $obj_data->image);
			}
		}

		$status = $this->BaseModel->where('id', base64_decode($enc_id))->update($arr_data);

		if($status)
		{
			Session::flash('success', str_singular($this->module_title).' updated successfully.');
			return redirect($this->module_url_path);
		}

		Session::flash('error', 'Error while updating '.str_singular($this->module_title).'.');

		return redirect($this->module_url_path);
	}

	public function view($enc_id='')
	{
		if($enc_id=='')
		{
			return redirect()->back();
		}

		$obj_data = $this->BaseModel->with(['category'=>function($q){
			$q->select(['id','name']);
		},'comments'])->where('id', base64_decode($enc_id))->first(['id','tags','name','image','category_id','description']);
		$arr_data = [];

		if($obj_data)
		{
			$arr_data = $obj_data->toArray();
		}
		else
		{
			return redirect()->back();
		}


		$this->arr_view_data['page_title']       = 'View '.str_singular($this->module_title);
		$this->arr_view_data['page_icon']        = $this->module_icon;
		$this->arr_view_data['module_title']     = 'Manage '.$this->module_title;
		$this->arr_view_data['sub_module_title'] = 'View '.str_singular($this->module_title);
		$this->arr_view_data['sub_module_icon']  = 'fa fa-eye';
		$this->arr_view_data['module_icon']      = $this->module_icon;
		$this->arr_view_data['admin_panel_slug'] = $this->admin_panel_slug;
		$this->arr_view_data['module_url_path']  = $this->module_url_path;
		$this->arr_view_data['arr_data']         = $arr_data;
		$this->arr_view_data['blog_image_public_path'] = $this->blog_image_public_path;
		$this->arr_view_data['enc_id'] = $enc_id;

		return view($this->module_view_folder.'.view',$this->arr_view_data);
	}

	public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            Session::flash('error','please select at least one record.');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            
            if($multi_action=="activate")
            {
            	$this->perform_activate(base64_decode($record_id));
               	Session::flash('success','Blog(s) activated successfully');
            }
            elseif($multi_action=="deactivate")
            {
            	$this->perform_deactivate(base64_decode($record_id));
               	Session::flash('success','Blog(s) blocked successfully.');
            }
           	/* elseif($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));
               Session::flash('success','City(s) deleted successfully.');
            } */
        }

        return redirect()->back();
    }

    public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while blog deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Blog deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while blog deletion.');
        }

        return redirect()->back();
    }

    public function perform_activate($id)
    {
        if ($id) 
        {
            $obj_blog = $this->BaseModel->where('id',$id)->first();
            if($obj_blog)
            {
                return $obj_blog->update(['is_active'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_deactivate($id)
    {
        if ($id) 
        {
            $obj_blog = $this->BaseModel->where('id',$id)->first();
            if($obj_blog)
            {
                return $obj_blog->update(['is_active'=>0]);
            }
        }

        return FALSE;
    }

    public function perform_delete($id)
    {
        if ($id) 
        {
            $obj_blog= $this->BaseModel->where('id',$id)->first();

            if($obj_blog)
            {
                return $obj_blog->delete();
            }
        }
        return FALSE;
    }

    public function activate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while country activation.');
            return redirect()->back();
        }

        if($this->perform_activate(base64_decode($enc_id)))
        {
            Session::flash('success','Country activated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while country activation.');
        }

        return redirect()->back();
    }

    public function deactivate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while country deactivation.');
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            Session::flash('success','Country deactivated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while country deactivation.');
        }

        return redirect()->back();
    }

}
