<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Common\Services\LanguageService;  
use App\Models\ProfessionModel;  

use Validator;
use Session;
Use Sentinel;
use Excel;

class ProfessionController extends Controller
{
     /*
        Auther : Sagar Sainkar
        Comments: controller for professions (projec professions)        
    */
    public $ProfessionModel; 
    
    public function __construct(ProfessionModel $professions,LanguageService $langauge)
    {      
       $this->ProfessionModel = $professions;
       $this->LanguageService = $langauge;
       $this->module_url_path = url(config('app.project.admin_panel_slug')."/professions");

       $this->image_base_img_path   = public_path().config('app.project.img_path.profession_image');
       $this->image_public_img_path = url('/').config('app.project.img_path.profession_image');
    }


    /*
        Auther : Sagar Sainkar
        Comments: display catergories
    */
	public function index()
    {

        $arr_lang   =  $this->LanguageService->get_all_language();

        $obj_profession = $this->ProfessionModel->with(['translations'])->get();

        if($obj_profession != FALSE)
        {
            $arr_professions = $obj_profession->toArray();
        }

        $this->arr_view_data['arr_professions'] = $arr_professions;

        $this->arr_view_data['page_title'] = "Manage Expert Professions";
        $this->arr_view_data['module_title'] = "Expert Professions";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.professions.index',$this->arr_view_data);
    }


    /*  
        Auther : Sagar Sainkar
        Comments: display view for Add new profession
    */

    public function create()
    {
        $this->arr_view_data['arr_lang'] = $this->LanguageService->get_all_language();
        $this->arr_view_data['page_title'] = "Create Professions";
        $this->arr_view_data['module_title'] = "Professions";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.professions.create',$this->arr_view_data);
    }
    /*  
        Auther : Sagar Sainkar
        Comments: Add new profession
    */
    public function store(Request $request)
    {
        $form_data = array();

        $form_data = $request->all();
        $arr_rules['profession_title_en'] = "required";
        
        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
             return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        /* Check if skill already exists with given translation */
        $does_exists = $this->ProfessionModel->whereHas('translations',function($query) use($request)
             {
                  $query->where('locale', 'en')
                        ->where('profession_title',$request->input('profession_title_en'));
             })->count();
        
        if($does_exists)
        {
            Session::flash('error','Category already exists.');            
            return redirect()->back();
        }
    
        $form_data = $request->all();
        $arr_data  = array();


        /*  Image Upload */
        /*$is_new_file_uploaded = FALSE;
        if($request->hasFile('image')) 
        {   
            $image     = $request->input('image');
            $excel_file_name = $image;
            $fileExtension   = strtolower($request->file('image')->getClientOriginalExtension()); 
            $file_name       = sha1(uniqid().$excel_file_name.uniqid()).'.'.$fileExtension;
            $request->file('image')->move($this->image_base_img_path,$file_name); 
            $is_new_file_uploaded = TRUE;         
        }
        if($is_new_file_uploaded){
            if(isset($pre_image) && $pre_image != "" && file_exists($this->image_base_img_path.$pre_image) ){
                $unlink_path    = $this->image_base_img_path.$pre_image;
                @unlink($unlink_path);
            }
            $arr_data['profession_image']  = $file_name;
        }*/
        /* End Image Upload */ 
        $arr_data['profession_slug'] = str_slug($form_data['profession_title_en']);
        $arr_data['is_active'] = 1;
        
        $obj_profession    = $this->ProfessionModel->create($arr_data);

        $profession_id = $obj_profession->id;

        /* Fetch All Languages*/
        $arr_lang =  $this->LanguageService->get_all_language();

        if(sizeof($arr_lang) > 0 )
        {
            foreach ($arr_lang as $lang) 
            {            
                $arr_data     = array();
                $profession_title   = 'profession_title_'.$lang['locale'];
                
                if( isset($form_data[$profession_title]) && $form_data[$profession_title] != '')
                { 
                    $translation = $obj_profession->translateOrNew($lang['locale']);
                    $translation->profession_title       = ucfirst($form_data[$profession_title]);
                    $translation->profession_id  = $profession_id;
                    $translation->save();
                    Session::flash('success','Category created successfully.');
                }

            }//foreach

        } //if
        else
        {
            Session::flash('error','Problem occured, while creating profession.');
        }
        return redirect()->back();
    }
    /*  
        Auther : Sagar Sainkar
        Comments: display view for edit profession
    */
    public function edit($enc_id)
    {
        $id = base64_decode($enc_id);

        $arr_lang = $this->LanguageService->get_all_language();      

        $obj_profession = $this->ProfessionModel->where('id', $id)->with(['translations'])->first();

        $arr_professions = [];

        if($obj_profession)
        {
           $arr_professions = $obj_profession->toArray(); 
           /* Arrange Locale Wise */
           $arr_professions['translations'] = $this->arrange_locale_wise($arr_professions['translations']);
        }

        $this->arr_view_data['enc_id'] = $enc_id;
        $this->arr_view_data['arr_lang'] = $this->LanguageService->get_all_language();          
        $this->arr_view_data['arr_professions'] = $arr_professions;  

        $this->arr_view_data['page_title'] = "Edit Category";
        $this->arr_view_data['module_title'] = "Professions";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.professions.edit',$this->arr_view_data);  

    }


    /*  
        Auther : Sagar Sainkar
        Comments: update profession details
    */
    public function update(Request $request, $enc_id)
    {
        $profession_id = base64_decode($enc_id);
        $arr_rules = array();
        $status = FALSE;

      	$arr_rules['profession_title_en']     = "required";        
        
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = array();
        $form_data = $request->all(); 

         /* Get All Active Languages */ 
  
        $arr_lang = $this->LanguageService->get_all_language();

        $profession = $this->ProfessionModel->where('id',$profession_id)->first();

        
         /* Insert Multi Lang Fields */

        if(sizeof($arr_lang) > 0 && $profession)
        { 

            /*  Image Upload */
            /*$is_new_file_uploaded = FALSE;
            if($request->hasFile('image')) 
            {   
                $image     = $request->input('image');
                $excel_file_name = $image;
                $fileExtension   = strtolower($request->file('image')->getClientOriginalExtension()); 
                $file_name       = sha1(uniqid().$excel_file_name.uniqid()).'.'.$fileExtension;
                $request->file('image')->move($this->image_base_img_path,$file_name); 
                $is_new_file_uploaded = TRUE;         
            }
            if($is_new_file_uploaded){
                if($request->input('pre_image')!=null && $request->input('pre_image')!=""){
                    if(isset($pre_image) && $pre_image != "" && file_exists($this->image_base_img_path.$pre_image) ){
                        $unlink_path    = $this->image_base_img_path.$pre_image;
                        @unlink($unlink_path);
                    }
                }
                $arr_data['profession_image']  = $file_name;
            }*/
            /* End Image Upload */ 
        	$arr_data['profession_slug'] = str_slug($form_data['profession_title_en']);
        	$profession->update($arr_data);

            foreach($arr_lang as $i => $lang)
            {
                $translate_data_ary = array();
                $profession_title   = 'profession_title_'.$lang['locale'];

                if(isset($form_data[$profession_title]) && $form_data[$profession_title]!="")
                {
                    /* Get Existing Language Entry and update it */
                    $translation = $profession->getTranslation($lang['locale']);    
                    if($translation)
                    {

		                $translation->profession_title       =  ucfirst($form_data['profession_title_'.$lang['locale']]);
                        $status = $translation->save();                       
                    }  
                    else
                    {
                        /* Create New Language Entry  */
                        $translation     = $profession->getNewTranslation($lang['locale']);
                        $translation->profession_id  =  $profession_id;
                        $translation->profession_title       =  ucfirst($form_data['profession_title_'.$lang['locale']]);
                        $status = $translation->save();
                    } 
                }   
            }
            
        }

        if ($status) 
        {
            Session::flash('success','Category updated successfully.');    
        }
        else
        {
            Session::flash('error','Error while updating profession.');       
        }
        
        return redirect()->back();
    }

    /*
    | Following Fuctions for active ,deactive and delete
    | auther :Sagar Sainkar
    | 
    */ 

    public function activate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while profession activation.');
            return redirect()->back();
        }

        if($this->perform_activate(base64_decode($enc_id)))
        {
            Session::flash('success','Category activated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while profession activation.');
        }

        return redirect()->back();
    }

    public function deactivate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
        	Session::flash('error','Problem occured while profession deactivation.');
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            Session::flash('success','Category deactivated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while profession deactivation.');
        }

        return redirect()->back();
    }

    public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
        	Session::flash('error','Problem occured while profession deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Category deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while profession deletion.');
        }

        return redirect()->back();
    }


    public function perform_activate($id)
    {
        if ($id) 
        {
            $service_profession = $this->ProfessionModel->where('id',$id)->first();
            if($service_profession)
            {
                return $service_profession->update(['is_active'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_deactivate($id)
    {
        if ($id) 
        {
            $service_profession = $this->ProfessionModel->where('id',$id)->first();
            if($service_profession)
            {
                return $service_profession->update(['is_active'=>0]);
            }
        }

        return FALSE;
    }

    public function perform_delete($id)
    {
        if ($id) 
        {
            $service_profession= $this->ProfessionModel->where('id',$id)->first();
            if($service_profession)
            {
                return $service_profession->delete();
            }
        }
        return FALSE;
    }
   

    /*
    | multi_action: Following Fuctions for active ,deactive and delete for multiple records
    | auther :Sagar Sainkar
    | Date : 
    | 
    */ 
    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        /*if ($multi_action=="export") 
        {
            $this->_export($request);
        }*/

        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Session::flash('success','Category(s) deleted successfully.');
            } 
            elseif($multi_action=="activate")
            {
               $this->perform_activate(base64_decode($record_id)); 
               Session::flash('success','Category(s) activated successfully');               
            }
            elseif($multi_action=="deactivate")
            {
               $this->perform_deactivate(base64_decode($record_id));    
               Session::flash('success','Category(s) blocked successfully.');
            }

        }

        return redirect()->back();
    }

    public function arrange_locale_wise(array $arr_data)
    {
        if(sizeof($arr_data)>0)
        {
            foreach ($arr_data as $key => $data)
            {
                $arr_tmp = $data;
                unset($arr_data[$key]);

                $arr_data[$data['locale']] = $data;                    
            }

            return $arr_data;
        }
        else
        {
            return [];
        }
    }

    /* export: export professions and Generate CSV file */
    public function export_professions()
    {
        $obj_profession = FALSE;
        $arr_professions = array();
        $export_professions = array();

        $obj_profession = $this->ProfessionModel->with(['translations'])->get();

        if($obj_profession != FALSE)
        {
            $arr_professions = $obj_profession->toArray();

            if(isset($arr_professions) && sizeof($arr_professions)>0)
            {  
                foreach ($arr_professions as $key => $value) 
                {
                    $arr_professions[$key]['translations'] = $this->arrange_locale_wise($value['translations']);
                }
            }
            
        }

        if(isset($arr_professions) && sizeof($arr_professions)>0)
        {
            foreach ($arr_professions as $key => $profession) 
            {
               
               $export_professions[$key]['Category Title English']=isset($profession['translations']['en']['profession_title'])?$profession['translations']['en']['profession_title']:'';
               $export_professions[$key]['Category Title German']=isset($profession['translations']['de']['profession_title'])?$profession['translations']['de']['profession_title']:'';
               $export_professions[$key]['Category_slug']=isset($profession['profession_slug'])?$profession['profession_slug']:'';
               $export_professions[$key]['Status']=isset($profession['is_active']) && $profession['is_active']?'Active':'Inactive';
            }
        }

        $data = $export_professions;
        $type = 'CSV';

        return Excel::create('Professions', function($excel) use ($data) {

             // Set the title
            $excel->setTitle('Professions Backup');

            // Chain the setters
            $excel->setCreator('VirtualHomeConcept')
                    ->setCompany('VirtualHomeConcept');

            // Call them separately
            $excel->setDescription('Professions Backup');

            $excel->sheet('professions', function($sheet) use ($data)
            {
                $sheet->fromArray($data);

            });


        })->download($type);
    }
}