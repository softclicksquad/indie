<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ProjectPriorityPackages;
use App\Models\UserModel;

use Sentinel;
use Validator;
use Session;
use Lang;
use Mail;
class ProjectPriorityPacakge extends Controller
{
    public $arr_view_data;
    public function __construct(UserModel $UserModel,
                                ProjectPriorityPackages $ProjectPriorityPackages)
    {
      if(! $user = Sentinel::check()) {
        return redirect('/login');
      }
      $this->user                    = $user;
      $this->user_id                 = $user->id;
      $this->arr_view_data           = [];
      $this->UserModel               = $UserModel;
      $this->ProjectPriorityPackages = $ProjectPriorityPackages;
      $this->module_url_path         = url(config('app.project.admin_panel_slug')."/project-priority-pacakges");
    }
    public function index(){
      $packages            = [];
      $this->arr_view_data = [];
      $get_packages = $this->ProjectPriorityPackages
                           ->where('status','!=','2')
                           ->get();
      if(sizeof($get_packages)>0){
        $packages = $get_packages->toArray();
      }           

      $this->arr_view_data['packages']         = $packages;
      $this->arr_view_data['page_title']       = "Project priority packages";
      $this->arr_view_data['module_title']     = "Project priority packages";
      $this->arr_view_data['module_url_path']  = $this->module_url_path;
      return view('admin.priority_package.index',$this->arr_view_data);
    }
    public function create(){
      $this->arr_view_data = [];
      $this->arr_view_data['page_title']       = "Create";
      $this->arr_view_data['module_title']     = "Project priority packages";
      $this->arr_view_data['module_url_path']  = $this->module_url_path;
      return view('admin.priority_package.create',$this->arr_view_data);
    }
    public function store(Request $request){
        $form_data = $request->all();
        $arr_rules['highlight_days'] = "required";
        $arr_rules['USD_prize']          = "required";
        $arr_rules['EUR_prize']          = "required";
        $arr_rules['GBP_prize']          = "required";
        $arr_rules['PLN_prize']          = "required";
        $arr_rules['CHF_prize']          = "required";
        $arr_rules['NOK_prize']          = "required";
        $arr_rules['SEK_prize']          = "required";
        $arr_rules['DKK_prize']          = "required";
        $arr_rules['CAD_prize']          = "required";
        $arr_rules['ZAR_prize']          = "required";
        $arr_rules['AUD_prize']          = "required";
        $arr_rules['HKD_prize']          = "required";
        $arr_rules['CZK_prize']          = "required";
        $arr_rules['JPY_prize']          = "required";
        

        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails()) {
             return redirect()->back()->withErrors($validator)->withInput($request->all());
        }
        $store_data                     = [];
        
        $store_data['highlited_days']     = $request->input('highlight_days');
        $store_data['USD_prize']          = $request->input('USD_prize');
        $store_data['EUR_prize']          = $request->input('EUR_prize');
        $store_data['GBP_prize']          = $request->input('GBP_prize');
        $store_data['PLN_prize']          = $request->input('PLN_prize');
        $store_data['CHF_prize']          = $request->input('CHF_prize');
        $store_data['NOK_prize']          = $request->input('NOK_prize');
        $store_data['SEK_prize']          = $request->input('SEK_prize');
        $store_data['DKK_prize']          = $request->input('DKK_prize');
        $store_data['CAD_prize']          = $request->input('CAD_prize');
        $store_data['ZAR_prize']          = $request->input('ZAR_prize');
        $store_data['AUD_prize']          = $request->input('AUD_prize');
        $store_data['HKD_prize']          = $request->input('HKD_prize');
        $store_data['CZK_prize']          = $request->input('CZK_prize');
        $store_data['JPY_prize']          = $request->input('JPY_prize');
        // chke exist
        $get_exists = $this->ProjectPriorityPackages
                           ->where('highlited_days',$request->input('highlight_days'))
                           ->where('status','!=','2')
                           ->count();

        if($get_exists > 0){
          Session::flash('error','This package already exists.'); 
          return redirect('admin/project-priority-pacakges/manage');
        }                   
        // end chk exist
        $store = $this->ProjectPriorityPackages->insertGetId($store_data); 
        if($store){
          Session::flash('success','Package created successfully.');
        } else {
          Session::flash('error','Problem occured while package created.'); 
        }
        return redirect('admin/project-priority-pacakges/manage');
    }
    public function edit($enc_id)
    {
        $id = base64_decode($enc_id);
        $obj_pacakage    = $this->ProjectPriorityPackages->where('id', $id)->first();
        $arr_pacakage    = [];
        if($obj_pacakage) {
           $arr_pacakage = $obj_pacakage->toArray(); 
        }
        $this->arr_view_data['enc_id']          = $enc_id;
        $this->arr_view_data['pacakage']        = $arr_pacakage;
        $this->arr_view_data['page_title']      = "Edit Package";
        $this->arr_view_data['module_title']    = "Project priority packages";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.priority_package.edit',$this->arr_view_data);  
    }
    public function update(Request $request){
        $form_data = $request->all();
        //dd($form_data);
        $arr_rules['highlight_days']     = "required";
        $arr_rules['USD_prize']          = "required";
        $arr_rules['EUR_prize']          = "required";
        $arr_rules['GBP_prize']          = "required";
        $arr_rules['PLN_prize']          = "required";
        $arr_rules['CHF_prize']          = "required";
        $arr_rules['NOK_prize']          = "required";
        $arr_rules['SEK_prize']          = "required";
        $arr_rules['DKK_prize']          = "required";
        $arr_rules['CAD_prize']          = "required";
        $arr_rules['ZAR_prize']          = "required";
        $arr_rules['AUD_prize']          = "required";
        $arr_rules['HKD_prize']          = "required";
        $arr_rules['CZK_prize']          = "required";
        $arr_rules['JPY_prize']          = "required";
        $arr_rules['package_id']         = "required";
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails()) {
             return redirect()->back()->withErrors($validator)->withInput($request->all());
        }
        $update_data                     = [];
        $update_data['highlited_days']     = $request->input('highlight_days');
        $update_data['USD_prize']          = $request->input('USD_prize');
        $update_data['EUR_prize']          = $request->input('EUR_prize');
        $update_data['GBP_prize']          = $request->input('GBP_prize');
        $update_data['PLN_prize']          = $request->input('PLN_prize');
        $update_data['CHF_prize']          = $request->input('CHF_prize');
        $update_data['NOK_prize']          = $request->input('NOK_prize');
        $update_data['SEK_prize']          = $request->input('SEK_prize');
        $update_data['DKK_prize']          = $request->input('DKK_prize');
        $update_data['CAD_prize']          = $request->input('CAD_prize');
        $update_data['ZAR_prize']          = $request->input('ZAR_prize');
        $update_data['AUD_prize']          = $request->input('AUD_prize');
        $update_data['HKD_prize']          = $request->input('HKD_prize');
        $update_data['CZK_prize']          = $request->input('CZK_prize');
        $update_data['JPY_prize']          = $request->input('JPY_prize');

        // chke exist
        $get_exists = $this->ProjectPriorityPackages
                           ->where('highlited_days',$request->input('highlight_days'))
                           ->where('id','!=',$request->input('package_id'))
                           ->count();
        //dd($get_exists);
        if($get_exists > 0){
          Session::flash('error','This package already exists.'); 
          return redirect('admin/project-priority-pacakges/edit/'.base64_encode($request->input('package_id')));
        }                   
        // end chk exist
        $store = $this->ProjectPriorityPackages
                      ->where('id',$request->input('package_id'))
                      ->update($update_data); 
        if($store){
          Session::flash('success','Package updated successfully.');
        } else {
          Session::flash('error','Problem occured while package updated.'); 
        }
        return redirect('admin/project-priority-pacakges/manage');
    }
    public function activate($enc_id = FALSE)
    {
        if(!$enc_id){
            Session::flash('error','Problem occured while package activation.');
            return redirect()->back();
        }
        if($this->perform_activate(base64_decode($enc_id)))
        {
            Session::flash('success','Package activated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while package activation.');
        }
        return redirect()->back();
    }

    public function deactivate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
          Session::flash('error','Problem occured while package deactivation.');
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            Session::flash('success','Package deactivated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while package deactivation.');
        }
        return redirect()->back();
    }
    public function perform_activate($id)
    {
        if ($id) 
        {
            $obj_pacakge = $this->ProjectPriorityPackages->where('id',$id)->first();
            if($obj_pacakge)
            {
                return $obj_pacakge->update(['status'=>'0']);
            }
        }
        return FALSE;
    }

    public function perform_deactivate($id)
    {
        if ($id) 
        {
            $obj_pacakge = $this->ProjectPriorityPackages->where('id',$id)->first();
            if($obj_pacakge)
            {
                return $obj_pacakge->update(['status'=>'1']);
            }
        }
        return FALSE;
    }
    public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
          Session::flash('error','Problem occured while package deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Package deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while package deletion.');
        }

        return redirect()->back();
    }
    public function perform_delete($id)
    {
        if ($id) 
        {
            $package= $this->ProjectPriorityPackages->where('id',$id)->first();
            if($package)
            {
                return $package->update(['status'=>'2']);
            }
        }
        return FALSE;
    }
    public function multi_action(Request $request)
    {
        $arr_rules                   = array();
        $arr_rules['multi_action']   = "required";
        $arr_rules['checked_record'] = "required";
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action   = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Session::flash('success','Package(s) deleted successfully.');
            } 
            elseif($multi_action=="activate")
            {
               $this->perform_activate(base64_decode($record_id)); 
               Session::flash('success','Package(s) activated successfully');               
            }
            elseif($multi_action=="deactivate")
            {
               $this->perform_deactivate(base64_decode($record_id));    
               Session::flash('success','Package(s) blocked successfully.');
            }
        }
        return redirect()->back();
    }
} // end class