<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UserModel;

use App\Http\Requests;
use Session;
use Validator;
use Sentinel;
use Excel;
use DB;

class UserReportController extends Controller
{
    public function __construct(UserModel $user)
    {      
       $this->UserModel = $user;
       $this->module_url_path = url(config('app.project.admin_panel_slug')."/reports");
    }
    /*
     Profile: Display all user reports.
     Author: Bharat K
    */

	public function index()
    {
        
        $this->arr_view_data['page_title'] = "Manage User Reports";
        $this->arr_view_data['module_title'] = "Reports";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
 
        return view('admin.reports.index',$this->arr_view_data);
    }
     /*
     Profile: Filtering of all user reports.
     Author: Bharat K
    */
    public function filter(Request $request)
    {
        $page_title = 'User Reports';
        $arr_rules =array();
        $arr_rules['start_date'] = 'required';
        $arr_rules['end_date']   = 'required';

        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $form_data = $request->all();
        	//dd($form_data);
        $start_date  = $form_data['start_date'];
        $end_date    = $form_data['end_date'];
     
        $start_date = date('Y-m-d', strtotime($start_date));

        $end_date = date('Y-m-d', strtotime($end_date));
		
        $users  = $this->UserModel->whereBetween('created_at', array($start_date, $end_date))->whereHas('roles',function ($query)
        {
        	$query->whereNotIn('role_id',array('1','5'));
        })->get();

        $arr_user = array();

        if($users)
        {
        	$arr_user = $users->toArray();
        	//dd($arr_user);	
        }
        $this->arr_view_data['page_title'] 	= "Manage User Reports";
        $this->arr_view_data['module_title'] = "Reports";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        $this->arr_view_data['start_date']  = $start_date;
        $this->arr_view_data['end_date'] 	= $end_date;
        $this->arr_view_data['arr_user'] 	= $arr_user;

        return view('admin.reports.index',$this->arr_view_data);
	}
	 /*
     Profile: Display detail of user.
     Author: Bharat K
    */
	 public function show($id)
    {
    	if(isset($id) && $id!="")
    	{
    		$id = base64_decode($id);
    	}

    	$users  = $this->UserModel->where('id', $id)->whereHas('roles',function ($query)
        {
        	$query->whereNotIn('role_id',array('1','5'));
        })->first();

        $arr_user = array();

        if($users)
        {
        	$arr_user = $users->toArray();
        }
        //dd($arr_user);
        $this->arr_view_data['page_title'] 	= "User Report";
        $this->arr_view_data['module_title'] = "Reports";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        $this->arr_view_data['arr_user'] 	= $arr_user;
        
	  	return view('admin.reports.show',$this->arr_view_data);
    }	
    /* Profile: Export csv file of user report.
       Author: Bharat K*
       Generate CSV file */
    public function export(Request $request)
    {

        $arr_rules =array();
        $arr_rules['start_date'] = 'required';
        $arr_rules['end_date']   = 'required';

        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $start_date  = $request->input('start_date');
        $end_date    = $request->input('end_date');

       

        $start_date = date('Y-m-d', strtotime($start_date));
        $end_date = date('Y-m-d', strtotime($end_date));

        $users  = $this->UserModel->whereBetween('created_at', array($start_date, $end_date))->whereHas('roles',function ($query)
        {
            $query->whereNotIn('role_id',array('1','5'));
        })->get();

        $arr_user = $arr_user_data = array();

        if($users)
        {
            $arr_user = $users->toArray();
        }
         //dd($arr_user);
        if(isset($arr_user) && sizeof($arr_user)>0)
        {
            foreach ($arr_user as $key => $user) 
            {
                $arr_user_data[$key]['Name'] = $user['role_info']['first_name'].' '.$user['role_info']['last_name'];
                $arr_user_data[$key]['Email']      = $user['email'];
                $arr_user_data[$key]['User type']  = $user['roles'][0]['name'];
                $arr_user_data[$key]['Country']    = $user['role_info']['country_details']['country_name'];
                $arr_user_data[$key]['State']      = $user['role_info']['state_details']['state_name'];
                $arr_user_data[$key]['City']       = $user['role_info']['city_details']['city_name'];
                $arr_user_data[$key]['Address']    = str_replace(',', '', $user['role_info']['address']);
                $arr_user_data[$key]['Zip code']      = $user['role_info']['zip'];
                if(isset($user['is_active']) && $user['is_active']=="1")
                {
                    $status = "Active";
                }
                else
                {   
                    $status = "Block";
                }
                $arr_user_data[$key]['Status']  = $status;
                if(isset($user['is_available']) && $user['is_available']=="1")
                {
                    $status = "Yes";
                }
                else
                {   
                    $status = "No";
                }
                $arr_user_data[$key]['Available']  = $status;
                $arr_user_data[$key]['Last login']  = date('d M Y G:i',strtotime($user['last_login']));
                $arr_user_data[$key]['Created date']  = date('d M Y G:i',strtotime($user["created_at"]));
                
            }
        }
       // dd($arr_user_data);
        $data = $arr_user_data;
        $type = 'CSV';

        return Excel::create('User_report', function($excel) use ($data) {

             // Set the title
            $excel->setTitle('User Report');

            // Chain the setters
            $excel->setCreator('VirtualHomeConcept')
                    ->setCompany('VirtualHomeConcept');

            // Call them separately
            $excel->setDescription('User report details');

            $excel->sheet('mySheet', function($sheet) use ($data)
            {

                $sheet->fromArray($data);

            });

        })->download($type);
    }

}
