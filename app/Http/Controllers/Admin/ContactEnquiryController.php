<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ContactEnquiryModel;
use Validator;
use Session;
use Input;
use Mail;

class ContactEnquiryController extends Controller
{
    /*
        Auther : Sagar Sainkar
        Comments: controller for Contact Enquiries
    */
    public function __construct(ContactEnquiryModel $contact_enquiry)
    {      
       $this->ContactEnquiryModel = $contact_enquiry;
       $this->module_url_path     = url(config('app.project.admin_panel_slug')."/contact_inquiries");
    }
    /*
        Auther : Sagar Sainkar
        Comments: display all Contact Enquiries
    */
	public function index()
    {
        $obj_enquiry = $this->ContactEnquiryModel->get();
        if($obj_enquiry != FALSE)
        {
            $arr_enquiries = $obj_enquiry->toArray();
        }
        $this->arr_view_data['arr_enquiries']   = $arr_enquiries;
        $this->arr_view_data['page_title']      = "Manage Contact Inquiries";
        $this->arr_view_data['module_title']    = "Contact Inquiries";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.contact_enquiries.index',$this->arr_view_data);
    }
    /*  
        Auther : Sagar Sainkar
        Comments: display view for edit contact enquiry
    */
    public function show($enc_id)
    {
        $id = base64_decode($enc_id);
        $obj_enquiry = $this->ContactEnquiryModel->where('id', $id)->first();
        $arr_enquiry = array();
        if($obj_enquiry)
        {
           $arr_enquiry = $obj_enquiry->toArray();
        }

        $this->arr_view_data['enc_id'] = $enc_id;
        $this->arr_view_data['arr_enquiry'] = $arr_enquiry;
        $this->arr_view_data['page_title'] = "Manage Contact Inquiries";
        $this->arr_view_data['module_title'] = "Contact Inquiries";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.contact_enquiries.show',$this->arr_view_data);

    }
    /*  
        Auther : Sagar Sainkar
        Comments: reply to contact enquiry 
    */
    public function reply(Request $request, $enc_id)
    {
        $newsletter_id = base64_decode($enc_id);
        $arr_rules     = array();
        $status        = FALSE;

        $arr_rules['reply_message'] = "required";
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }
        $reply_message = $request->input('reply_message');

        $obj_enquiry = $this->ContactEnquiryModel->where('id',$newsletter_id)->first();

        if($obj_enquiry && sizeof($obj_enquiry) > 0 && $reply_message)
        { 	
        	$arr_data = array();
            $arr_data['reply_message']    = $reply_message;
            $arr_data['reply_status']     = 1;
            $arr_data['reply_date_time']  = date("Y-m-d H:i:s");;

        	$status = $obj_enquiry->update($arr_data);
            if ($status) 
            {
                $arr_contact_enquiry = $obj_enquiry->toArray();
                $to_email_id         = isset($arr_contact_enquiry['email'])?$arr_contact_enquiry['email']:'support@archexperts.com';
                $project_name        = config('app.project.name');
                $mail_subject        = isset($arr_contact_enquiry['enquiry_subject']) && $arr_contact_enquiry['enquiry_subject']!=""?$arr_contact_enquiry['enquiry_subject']:'Reply Contact Enquiry';
                //$mail_form           = isset($website_contact_email)?$website_contact_email:'support@archexperts.com';
                $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
                
                try{
                    Mail::send('admin.email.contact_enquiry_reply', $arr_contact_enquiry, function ($message) use ($to_email_id,$mail_form,$project_name,$mail_subject) 
                    {
                      $message->from($mail_form, $project_name);
                      $message->subject($project_name.' : '.$mail_subject);
                      $message->to($to_email_id);

                    });
                    Session::flash('success','Replied to contact inquiry successfully.');
                }
                catch(\Exception $e){
                Session::Flash('error'   , 'E-Mail not sent');
                }
            }
            else
            {
                Session::flash('error','Error while replying to contact inquiry.');
            }
        }
        else
        {
            Session::flash('error','Error while replying to contact inquiry.');
        }
        
        return redirect()->back();
    }



    /*
    | Following Fuctions for delete
    | auther :Sagar Sainkar    
    | 
    */ 

    public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
        	Session::flash('error','Problem occured while newsletter deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Contact inquiry deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while contact inquiry deletion.');
        }

        return redirect()->back();
    }



    public function perform_delete($id)
    {
        if ($id) 
        {
            $newsletter= $this->ContactEnquiryModel->where('id',$id)->first();
            if($newsletter)
            {
                return $newsletter->delete();
            }
        }
        return FALSE;
    }
   

     /*
    | multi_action: Following Fuctions for delete for multiple records
    | auther :Sagar Sainkar    
    | 
    */ 
    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Session::flash('success','Contact inquires deleted successfully.');
            } 
        }

        return redirect()->back();
    }
    
}
