<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models\ClientsModel;
use App\Models\UserModel;
use App\Models\ProjectpostModel;
use App\Models\CountryModel;
use Session;
use Validator;
use Sentinel;

class ClientsController extends Controller
{
    /*
        Auther : Bharat Khairnar
        Comments: controller for manage clients
    */

    public function __construct(ProjectpostModel $projects,ClientsModel $clients,UserModel $use_model,CountryModel $countries)
    {      
       $this->CountryModel                   = $countries;
       $this->ClientsModel                   = $clients;
       $this->ProjectpostModel               = $projects;
       $this->UserModel                      = $use_model;
       $this->module_url_path                = url(config('app.project.admin_panel_slug')."/clients");
       $this->project_attachment_public_path = url('/').config('app.project.img_path.project_attachment');
    }


     /*
        Auther : Bharat Khairnar
        Comments: display all clients
    */
	public function index()
    {
        $obj_client_user = $this->ClientsModel->with(['user_details'])->get();

        if($obj_client_user != FALSE)
        {
            $arr_client_user = $obj_client_user->toArray();
        }
        
        $this->arr_view_data['arr_client_user'] = $arr_client_user;
        $this->arr_view_data['page_title'] = "Manage Clients";
        $this->arr_view_data['module_title'] = "Client Manage";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.clients.index',$this->arr_view_data);
    }

    public function deleted_client()
    {

        $deleted_obj_client = $this->UserModel->with('client_details')
                                    ->whereHas('roles',function($query){
                                        $query->where('slug','client');
                                    })
                                    ->onlyTrashed()
                                    ->orderBy('deleted_at','desc')
                                    ->get();
        if($deleted_obj_client)
        {
            $arr_deleted_client = $deleted_obj_client->toArray();
        }

        $this->arr_view_data['arr_deleted_client'] = $arr_deleted_client;
        $this->arr_view_data['page_title'] = "Manage Deleted Clients";
        $this->arr_view_data['module_title'] = "Deleted Clients";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.clients.deleted',$this->arr_view_data);
    }

    public function view_deleted_clients($enc_id)
    {
        $id = base64_decode($enc_id);
        $profile_img_path = url('/').'/uploads/front/profile/';


        $obj_client = $this->ClientsModel->where('user_id', $id)->with(['user_details','country_details.states','state_details.cities','city_details'])->first();

        $arr_client = array();

        if($obj_client)
        {
           $arr_client = $obj_client->toArray();
        }

        $arr_countries = array();

        $obj_countries = $this->CountryModel->get();

        if($obj_countries != FALSE)
        {
            $arr_countries = $obj_countries->toArray();
        }
        $this->arr_view_data['arr_countries'] = $arr_countries;
        $this->arr_view_data['enc_id'] = $enc_id;
        $this->arr_view_data['profile_img_path'] = $profile_img_path;
        $this->arr_view_data['arr_client'] = $arr_client;  
        $this->arr_view_data['page_title'] = "View Deleted clients";
        $this->arr_view_data['module_title'] = "Manage Deleted Clients";
        $this->arr_view_data['module_url_path'] = $this->module_url_path.'/deleted';
        //dd($this->arr_view_data);
        return view('admin.clients.deleted_view',$this->arr_view_data);  
    }


    /*  
        Auther : Sagar Sainkar
        Comments: display view for Add new project_manager
    */

    public function create()
    {
        $this->arr_view_data['page_title'] = "Manage Project Manager";
        $this->arr_view_data['module_title'] = "Project Manager";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.project_manager.create',$this->arr_view_data);
    }

    /*  
        Auther : Sagar Sainkar
        Comments: Add and store project_manager details
    */
    public function store(Request $request)
    {
        $form_data = array();

        $form_data = $request->all();
        
        $arr_rules['email'] = "required|email|unique:users,email";
        $arr_rules['password'] = "required|min:8";
        $arr_rules['first_name'] = "required|max:255";
        $arr_rules['last_name'] = "required|max:255";
        $arr_rules['phone'] = "required|max:12";
        $arr_rules['address'] = "required|max:255";
        
        
        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
             return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = $request->all();
        $arr_data = array();
        $arr_project_manager = array();

        $credentials = ['email' => $request->input('email')];
		$user = Sentinel::findByCredentials($credentials);

		if ($user) 
		{
			Session::flash('error','This email is already present.');
			return redirect()->back();
		}

		$arr_data['email'] = $form_data['email'];
        $arr_data['password'] = $form_data['password'];
        $arr_data['is_active'] = 0;

        $obj_project_manager = Sentinel::registerAndActivate($arr_data);

        if ($obj_project_manager) 
        {
        	//assign role to user
        	$role = Sentinel::findRoleBySlug('project_manager');
			$obj_project_manager->roles()->attach($role);

        	$arr_project_manager['user_id'] = $obj_project_manager->id;
        	$arr_project_manager['first_name'] = ucfirst($form_data['first_name']);
        	$arr_project_manager['last_name'] = ucfirst($form_data['last_name']);
        	$arr_project_manager['address'] = $form_data['address'];
        	$arr_project_manager['phone'] = $form_data['phone'];

        	$status = $this->ProjectManagerModel->create($arr_project_manager);

        	if($status)
	        {
	        	Session::flash('success','Project Manager created successfully.');
	        } 
	        else
	        {
	            Session::flash('error','Problem occured, while creating project manager.');
	        }
        }

        return redirect()->back();
    }


    /*  
        Auther : Bharat Khairnar
        Comments: display view for edit clients
    */

    public function edit($enc_id)
    {
        $id = base64_decode($enc_id);
        $profile_img_path = url('/').'/uploads/front/profile/';


        $obj_client = $this->ClientsModel->where('id', $id)->with(['user_details','country_details.states','state_details.cities','city_details'])->first();

        $arr_client = array();

        if($obj_client)
        {
           $arr_client = $obj_client->toArray();
           ($arr_client);
        }

        $arr_countries = array();

        $obj_countries = $this->CountryModel->get();

        if($obj_countries != FALSE)
        {
            $arr_countries = $obj_countries->toArray();
        }

        $this->arr_view_data['arr_countries'] = $arr_countries;
        $this->arr_view_data['enc_id'] = $enc_id;
        $this->arr_view_data['profile_img_path'] = $profile_img_path;
        $this->arr_view_data['arr_client'] = $arr_client;  
        $this->arr_view_data['page_title'] = "Edit Client";
        $this->arr_view_data['module_title'] = "Client Manage";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        //dd($this->arr_view_data);
        return view('admin.clients.edit',$this->arr_view_data);  

    }


    /*  
        Auther : Bharat Khairnar
        Comments: update client details
    */
    public function update(Request $request, $enc_id)
    {
        $client_id = base64_decode($enc_id);
        $arr_rules = array();
        $status = FALSE;
 
        $arr_rules['first_name'] 	= "required|max:255";
        $arr_rules['last_name'] 	= "required|max:255";
        $arr_rules['phone_code']    = "required|min:2";
        $arr_rules['phone_number'] 	= "required|max:12|min:10";
        $arr_rules['country']      	= "required";
        $arr_rules['state']        	= "required";
        $arr_rules['city']         	= "required";
        $arr_rules['zip_code']     	= "required|max:10";
        $arr_rules['address']      	= "required|max:255";
        
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = array();
        $form_data = $request->all(); 

        $obj_client = $this->ClientsModel->where('id',$client_id)->first();

        if($obj_client && sizeof($obj_client) > 0)
        { 	
        	 $arr_client = array();
             $arr_client['first_name']    = ucfirst($form_data['first_name']);
	         $arr_client['last_name']     = ucfirst($form_data['last_name']);
             $arr_client['phone_code']    = $form_data['phone_code'];
	         $arr_client['phone_number']  = $form_data['phone_number'];
	         $arr_client['country']       = $form_data['country'];
	         $arr_client['state']         = $form_data['state'];
	         $arr_client['city']          = $form_data['city'];
	         $arr_client['zip']           = $form_data['zip_code'];
	         $arr_client['address']       = $form_data['address'];
	         $arr_client['company_name']  = $form_data['company_name'];
	         $arr_client['owner_name']    = $form_data['owner_name'];
	         $arr_client['company_size']  = $form_data['company_size'];
	         $arr_client['founding_year'] = $form_data['founding_year'];
	         $arr_client['vat_number']    = $form_data['vat_number'];

        	$status = $obj_client->update($arr_client);
        }

        if ($status) 
        {
            Session::flash('success','Client details updated successfully.');    
        }
        else
        {
            Session::flash('error','Error while updating client details.');
        }
        
        return redirect()->back();
    }

    /*
    | Following Fuctions for active ,deactive and delete
    | auther :Bharat Khairnar    
    | 
    */ 

    public function activate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while client activation.');
            return redirect()->back();
        }

        if($this->perform_activate(base64_decode($enc_id)))
        {
            Session::flash('success','Client activated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while client activation.');
        }

        return redirect()->back();
    }

    public function deactivate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
        	Session::flash('error','Problem occured while client deactivation.');
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            Session::flash('success','Client deactivated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while client deactivation.');
        }

        return redirect()->back();
    }

    public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
        	Session::flash('error','Problem occured while client deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Client deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while client deletion.');
        }

        return redirect()->back();
    }


    public function perform_activate($id)
    {
        if ($id) 
        {
            $client = $this->UserModel->where('id',$id)->first();
            if($client)
            {
                return $client->update(['is_active'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_deactivate($id)
    {
        if ($id) 
        {
            $client = $this->UserModel->where('id',$id)->first();
            if($client)
            {
                return $client->update(['is_active'=>0]);
            }
        }

        return FALSE;
    }

    public function perform_delete($id)
    {
        if ($id) 
        {	
        	$user= $this->UserModel->where('id',$id)->first();
            $client= $this->ClientsModel->where('user_id',$id)->first();

            if($user!=FALSE && $client!=FALSE)
            {	
            	$delete_user = $user->delete();	
            	return $client->delete();	
            }
        }
        return FALSE;
    }
   

     /*
    | multi_action: Following Fuctions for active ,deactive and delete for multiple records
    | auther :Bharat Khairnar    
    | 
    */ 
    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Session::flash('success','Client(s) deleted successfully.');
            } 
            elseif($multi_action=="activate")
            {
               $this->perform_activate(base64_decode($record_id)); 
               Session::flash('success','Client(s) activated successfully');               
            }
            elseif($multi_action=="deactivate")
            {
               $this->perform_deactivate(base64_decode($record_id));    
               Session::flash('success','Client(s) blocked successfully.');
            }
        }

        return redirect()->back();
    }

     /*
        Auther : Shankar Jamdhade
        Comments: display all projects
    */
    public function projects($client_user_id)
    {
        if($client_user_id=="")
        {
            return redirect()->back();
        }
        $client_user_id=base64_decode($client_user_id);
        $obj_client_projects = $this->ProjectpostModel->with('skill_details','client_details','category_details')
                                                      ->where('client_user_id',$client_user_id)
                                                      ->where('project_status','<>','0')
                                                      ->get();

        if($obj_client_projects != FALSE)
        {
            $obj_client_projects = $obj_client_projects->toArray();
        }
        //dd($obj_client_projects);
        $this->arr_view_data['client_user_id'] = base64_encode($client_user_id);
        $this->arr_view_data['obj_client_projects'] = $obj_client_projects;
        $this->arr_view_data['page_title'] = "Manage Clients Project";
        $this->arr_view_data['module_title'] = "Project Manage";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.clients.all-projects',$this->arr_view_data);
    }
    /*  
        Auther : Shankar Jamdhade
        Comments: display view for projects details.
    */

    public function show($project_id)
    {
        $id = base64_decode($project_id);

        $obj_project_info = $this->ProjectpostModel->with('project_skills.skill_data','client_details','category_details')->where('id',$id)->first();

        $arr_info = array();

        if($obj_project_info)
        {
           $arr_info = $obj_project_info->toArray();
        }

        //dd($arr_info);
        $this->arr_view_data['arr_info'] = $arr_info;
        $this->arr_view_data['page_title'] = "Projects Details";
        $this->arr_view_data['module_title'] = "Client Manage";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        $this->arr_view_data['project_attachment_public_path'] = $this->project_attachment_public_path;
        return view('admin.clients.show',$this->arr_view_data);
    }
    public function make_private($id)
    {
        if ($id) 
        {   
            $id = base64_decode($id);
            $client = $this->ClientsModel->where('user_id',$id)->first();
            if($client)
            {
                $this->ClientsModel->where('user_id',$id)->update(['user_type'=>'private']);
                Session::flash('success','Client user type change successfully as private.');
                return redirect()->back();
            }
            else{
                Session::flash('error','Something went wrong, Please try again later.');
                return redirect()->back();
            }
        }
    }
    public function make_business($id)
    {
        if ($id) 
        {
            $id = base64_decode($id);
            $client = $this->ClientsModel->where('user_id',$id)->first();
            if($client)
            {
                $this->ClientsModel->where('user_id',$id)->update(['user_type'=>'business']);
                Session::flash('success','Client user type change successfully as business.');
                return redirect()->back();
            }
            else{
                Session::flash('error','Something went wrong, Please try again later.');
                return redirect()->back();
            }
        }
    }
} //  end class