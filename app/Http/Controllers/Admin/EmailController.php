<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\ClientsModel;
use App\Models\ExpertsModel;
use App\Models\UserModel;
use App\Models\EmailSentModel;
use Session;
use Validator;
use Sentinel;
use Mail;

class EmailController extends Controller
{
    public function __construct(UserModel $user,ClientsModel $clients,EmailSentModel $EmailSentModel,ExpertsModel $ExpertsModel)
    {      
       $this->UserModel       = $user;
       $this->ClientsModel    = $clients;
       $this->EmailSentModel  = $EmailSentModel;
       $this->ExpertsModel    = $ExpertsModel;
       $this->module_url_path = url(config('app.project.admin_panel_slug')."/email");
    }


	public function index()
    {
        $this->arr_view_data['page_title']      = "Send Mail";
        $this->arr_view_data['module_title']    = "Send Mail";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.mail.index',$this->arr_view_data);
    }
    public function listing()
    {
        $sent_mails     = [];
        $get_sent_mails = $this->EmailSentModel->orderBy('id','DESC')->get();
        if(isset($get_sent_mails) && $get_sent_mails !=""){
            $sent_mails = $get_sent_mails->toArray();
        }
        $this->arr_view_data['page_title']      = "Sent listing";
        $this->arr_view_data['module_title']    = "Sent listing";
        $this->arr_view_data['arr_sent_mails']  = $sent_mails;
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.mail.listing',$this->arr_view_data);
    }
    public function details($sent_mail_id = FALSE)
    {
        $sent_mails_details     = [];
        $get_sent_mails = $this->EmailSentModel->where('id',base64_decode($sent_mail_id))->first();
        if(isset($get_sent_mails) && $get_sent_mails !=""){
            $sent_mails_details = $get_sent_mails->toArray();
        }
        $this->arr_view_data['page_title']         = "Sent Mail Details";
        $this->arr_view_data['module_title']       = "Sent Mail Details";
        $this->arr_view_data['sent_mails_details'] = $sent_mails_details;
        $this->arr_view_data['module_url_path']    = $this->module_url_path;
        return view('admin.mail.show',$this->arr_view_data);
    }
    public function send(Request $request)
    {
        $arr_rules = array();
        $arr_rules['message_to']   = "required|email";
        $arr_rules['subject']      = "required";
        $arr_rules['message']      = "required";

        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }


        // create unique indentifier
            $rs  = \DB::table('email_sent')->select('unique_id')->where('unique_id' ,'!=' , null)->where('unique_id' ,'!=' , '')->orderBy('id' , 'DESC')->LIMIT('1')->get();
            $res = $rs;
            if(count($res) > 0)
            {                 
                $max_mail_nbr = $res[0]->unique_id;
                // remove STU from that no.                
                $max_mail_nbr = substr($max_mail_nbr, 3);
                // get count of that no.
                $cnt = strlen($max_mail_nbr);
                // increment that no. by 1
                $max_mail_nbr = $max_mail_nbr + 1;                        
                // add no. of zero's to the no.                
                $max_mail_nbr = str_pad($max_mail_nbr, $cnt, '0', STR_PAD_LEFT);
                $max_mail_nbr = 'EML'.$max_mail_nbr;  
            }
            else
            {
                $max_mail_nbr = 'EML000000001';  
            }
        // create unique indentifier

        $form_data = $request->all();
        $arr_data = array();
        $arr_data['message_to']      = $form_data['message_to'];
        $arr_data['subject']         = $max_mail_nbr.'-'.$form_data['subject']; 
        $arr_data['message_content'] = $form_data['message']; 

        $obj_user = $this->UserModel->where('email',$arr_data['message_to'])->first(['user_name','id']);
        if($obj_user != FALSE)
        {
            $arr_user = $obj_user->toArray();
        }
        else
        {
          Session::flash('error','Sorry, user not found.');  
          return redirect()->back();
        }
        
        $arr_data['email_number'] = $max_mail_nbr;     

        $user_details = sidebar_information($arr_user['id']);
        $first_name = isset($user_details['user_details']['first_name'])?$user_details['user_details']['first_name']:'-';
        $last_name  = isset($user_details['user_details']['last_name'])?substr($user_details['user_details']['last_name'], 0,1).'.':'';

        $arr_data['arr_user'] = $first_name/*.' '.$last_name*/;
        $email_id             = isset($arr_data['message_to'])?$arr_data['message_to']:'';
        $project_name         = config('app.project.name');
        $mail_subject         = isset($arr_data['subject'])?$arr_data['subject']:'';
        //$mail_form            = isset($website_contact_email)?$website_contact_email:'support@archexperts.com';
        $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
        try{
                Mail::send('admin.email.mail_to_user', $arr_data, function ($message) use ($email_id,$mail_form,$project_name,$mail_subject) 
                {
                    $message->from($mail_form, $project_name);
                    $message->subject($project_name.':'.$mail_subject);
                    $message->to($email_id);

                });
                $sent_mail_data = [];
                $sent_mail_data['mail_from'] =$mail_form;
                $sent_mail_data['mail_to']   =$email_id;
                $sent_mail_data['subject']   =$project_name.':'.$mail_subject;
                $sent_mail_data['message']   =$arr_data['message_content'];
                $sent_mail_data['is_sent']   ='YES';
                $sent_mail_data['unique_id']   =$max_mail_nbr;
                $this->EmailSentModel->insertGetId($sent_mail_data);
                Session::flash('success','Message send successfully.');
            }
            catch(\Exception $e){
            $sent_mail_data = [];
            $sent_mail_data['mail_from'] =$mail_form;
            $sent_mail_data['mail_to']   =$email_id;
            $sent_mail_data['subject']   =$project_name.':'.$mail_subject;
            $sent_mail_data['message']   =$arr_data['message_content'];
            $sent_mail_data['is_sent']   ='NO';
            $sent_mail_data['unique_id'] =$max_mail_nbr;
            $sent_mail_data['unsent_reasone']   =$e->getMessage();
            $this->EmailSentModel->insertGetId($sent_mail_data);
            Session::Flash('error'   , 'E-Mail not sent');
            }
        return redirect()->back();
    }
    public function search_users($search)
    {
        $search_text = $search;    
        if ($search_text) 
        {
            $user_emails = $this->UserModel->select(['email'])
                            ->where('email', 'like', $search_text.'%')
                            ->get();
            if (isset($user_emails) && $user_emails!="") 
            {
                $user_emails = $user_emails->toArray();

                if (sizeof($user_emails) > 0) 
                {
                    $user_data = $user_emails;
                    
                }
                else
                {   
                    $user_data['msg']= "User Not Found";
                }
                
            }  
            echo json_encode($user_data);
            exit();  
        }
        else
        {
            $user_data['status'] = "error";
            $user_data['msg']    = "User Not Found";
            echo json_encode($user_data);
            exit();
        }
    }   
} // end class