<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models\AdminProfileModel;
use App\Models\UserModel;
use App\Models\ModulesModel;
use App\Models\SupportCategoryModel;

use Session;
use Validator;
use Sentinel;
use Response;

class SubAdminController extends Controller
{
    /*
        Auther : Sagar Sainkar
        Comments: controller for manage subadmin
    */

    public function __construct(AdminProfileModel $subadmin,UserModel $use_model,ModulesModel $modules_model)
    {      
       $this->AdminProfileModel     = $subadmin;
       $this->UserModel             = $use_model;
       $this->ModulesModel          = $modules_model;
       $this->SupportCategoryModel  = new SupportCategoryModel;
       $this->module_url_path       = url(config('app.project.admin_panel_slug')."/subadmins");
    }


     /*
        Auther : Sagar Sainkar
        Comments: display all subadmin
    */
	public function index()
    {

        $obj_subadmin = $this->AdminProfileModel->whereNotIn('id', [1])->with(['user_details'])->get();

        if($obj_subadmin != FALSE)
        {
            $arr_subadmin = $obj_subadmin->toArray();
        }

        $this->arr_view_data['arr_subadmin'] = $arr_subadmin;
		//dd($arr_subadmin);
        $this->arr_view_data['page_title'] = "Manage Subadmins";
        $this->arr_view_data['module_title'] = "Subadmins";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.subadmins.index',$this->arr_view_data);
    }


    /*  
        Auther : Sagar Sainkar
        Comments: display view for Add new subadmin
    */

    public function create()
    {
        $this->arr_view_data['page_title'] = "Create Subadmin";
        $this->arr_view_data['module_title'] = "Subadmins";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.subadmins.create',$this->arr_view_data);
    }

    /*  
        Auther : Sagar Sainkar
        Comments: Add and store subadmin details
    */
    public function store(Request $request)
    {
        $form_data = array();
        $form_data = $request->all();
        
        $arr_rules['email']          = "required|email|unique:users,email";
        $arr_rules['password']       = "required|min:8";
        $arr_rules['name']           = "required|max:255";
        $arr_rules['contact_number'] = "required|max:16";
        $arr_rules['fax']            = "required|max:12";
        $arr_rules['address']        = "required|max:255";        
        
        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
             return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = $request->all();
        $arr_data = array();
        $arr_subadmin = array();


        $credentials = ['email' => $request->input('email')];
		$user = Sentinel::findByCredentials($credentials);

		if ($user) 
		{
			Session::flash('error','This email is already present.');
			return redirect()->back();
		}

		$arr_data['email'] = $form_data['email'];
        $arr_data['password'] = $form_data['password'];
        $arr_data['is_active'] = 1;

        $obj_subadmin = Sentinel::registerAndActivate($arr_data);

        if ($obj_subadmin) 
        {
        	//assign role to user
        	$role = Sentinel::findRoleBySlug('subadmin');
			$obj_subadmin->roles()->attach($role);

            $arr_subadmin['user_id']        = $obj_subadmin->id;
            $arr_subadmin['name']           = ucfirst($form_data['name']);
            $arr_subadmin['contact_email']  = $form_data['email'];
            $arr_subadmin['contact_number'] = $form_data['contact_number'];
            $arr_subadmin['fax']            = $form_data['fax'];
            $arr_subadmin['address']        = $form_data['address'];
            $arr_subadmin['image']          = 'default-admin.png';
        	

        	$status = $this->AdminProfileModel->create($arr_subadmin);

        	if($status)
	        {
	        	Session::flash('success','Subadmin created successfully.');
	        } 
	        else
	        {
	            Session::flash('error','Problem occured, while creating subadmin.');
	        }
        }

        return redirect()->back();
    }


    /*  
        Auther : Sagar Sainkar
        Comments: display view for edit subadmin
    */

    public function edit($enc_id)
    {
        $id = base64_decode($enc_id);

        $obj_subadmin = $this->AdminProfileModel->where('id', $id)->with(['user_details'])->first();

        $arr_subadmin = array();

        if($obj_subadmin)
        {
           $arr_subadmin = $obj_subadmin->toArray();
        }

        $this->arr_view_data['enc_id'] = $enc_id;
        $this->arr_view_data['arr_subadmin'] = $arr_subadmin;  
        $this->arr_view_data['page_title'] = "Edit Subadmin";
        $this->arr_view_data['module_title'] = "Subadmins";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.subadmins.edit',$this->arr_view_data);
    }


    /*  
        Auther : Sagar Sainkar
        Comments: update subadmin details
    */
    public function update(Request $request, $enc_id)
    {
        $subadmin_id = base64_decode($enc_id);
        $arr_rules = array();
        $status = FALSE;

        $arr_rules['name']              = "required|max:255";
        $arr_rules['email']             = "required|email";
        $arr_rules['contact_number']    = "required|max:16";
        $arr_rules['fax']               = "required|max:12";
        $arr_rules['address']           = "required|max:255";
        
        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $user_id = base64_decode($request->input('user_id'));

        $user_exists = $this->UserModel->where('id','!=',$user_id)->where('email',$request->input('email'))->count();

        if ($user_exists>0) 
        {
            Session::flash("error","This email is already exists.");
            return back()->withInput($request->all());
        }

        $form_data = array();
        $form_data = $request->all(); 

        $obj_subadmin = $this->AdminProfileModel->where('id',$subadmin_id)->first();

        if($obj_subadmin && sizeof($obj_subadmin) > 0)
        { 	

        	$arr_subadmin = array();
            
        	$arr_subadmin['name'] = ucfirst($form_data['name']);
            $arr_subadmin['contact_email'] = $form_data['email'];
        	$arr_subadmin['contact_number'] = $form_data['contact_number'];
        	$arr_subadmin['fax'] = $form_data['fax'];
        	$arr_subadmin['address'] = $form_data['address'];

             /*file uploade code for laravel auther : sagar sainkar*/
             if ($request->hasFile('file_attachment')) 
             {  
                $img_valiator = Validator::make(array('file_attachment'=>$request->file('file_attachment')),array(
                                                    'file_attachment' => 'mimes:jpg,jpeg,gif,png,pdf,doc')); 

                if ($request->file('file_attachment')->isValid() && $img_valiator->passes())
                {

                    $file_attach = $form_data['file_attachment'];
                    
                    $imageExtension = $request->file('file_attachment')->getClientOriginalExtension();
                
                    $imageName = sha1(uniqid().$file_attach.uniqid()).'.'.$imageExtension;
                    
                    $arr_doctor_result =$this->AdminProfileModel->where('id',$subadmin_id)->first();
                    if($arr_doctor_result)
                    {
                        $arr_result     = $arr_doctor_result->toArray();
                        
                        if (isset($arr_result['file_attachment']) && $arr_result['file_attachment']!='') 
                        {
                            $file_exits = file_exists(base_path() .'/public/uploads/admin/subadmin_attachment/'.$arr_result['file_attachment']);
                            if ($file_exits) 
                            {
                                unlink(base_path() .'/public/uploads/admin/subadmin_attachment/'.$arr_result['file_attachment']);
                            }
                        }
                    }

                    $request->file('file_attachment')->move(
                        base_path() . '/public/uploads/admin/subadmin_attachment/', $imageName
                    );

                    $arr_subadmin['file_attachment'] = $imageName;  
                }
                else
                {
                     Session::flash("error","Please upload valid image.");
                     return back()->withErrors($validator)->withInput($request->all());
                }
            }
            /*file uploade code end here*/ 

        	$status = $obj_subadmin->update($arr_subadmin);

            if($user_id)
            {
                 $obj_user = $this->UserModel->where('id',$user_id)->update(['email'=> $form_data['email']]);
            }

        }

        if ($status) 
        {
            Session::flash('success','Subadmin details updated successfully.');    
        }
        else
        {
            Session::flash('error','Error while updating subadmin details.');
        }
        
        return redirect()->back();
    }

    /*
    | Following Fuctions for download attachment
    | auther :Bharat Khairnar    
    | 
    */
    public function attachment_download($enc_id)
    {
        $id = base64_decode($enc_id);

        $obj_subadmin = $this->AdminProfileModel->where('id', $id)->with(['user_details'])->first();

        $arr_subadmin = array();

        if($obj_subadmin)
        {
           $arr_subadmin = $obj_subadmin->toArray();
        }
        
        if(isset($arr_subadmin['file_attachment']) && $arr_subadmin['file_attachment']!="")
        {
            $filepath = base_path() . '/public/uploads/admin/subadmin_attachment/'.$arr_subadmin['file_attachment'];
            if(file_exists($filepath))
            {
                //ob_end_clean();
                return Response::download($filepath);
            }
        }
    }

    /*
    | Following Fuctions for active ,deactive and delete
    | auther :Sagar Sainkar    
    | 
    */ 

    public function activate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while subadmin activation.');
            return redirect()->back();
        }

        if($this->perform_activate(base64_decode($enc_id)))
        {
            Session::flash('success','Subadmin activated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while subadmin activation.');
        }

        return redirect()->back();
    }

    public function deactivate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
        	Session::flash('error','Problem occured while subadmin deactivation.');
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            Session::flash('success','Subadmin deactivated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while subadmin deactivation.');
        }

        return redirect()->back();
    }

    /*public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
        	Session::flash('error','Problem occured while subadmin deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Subadmin deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while subadmin deletion.');
        }

        return redirect()->back();
    }*/


    public function perform_activate($id)
    {
        if ($id) 
        {
            $obj_user = $this->UserModel->where('id',$id)->first();
            if($obj_user)
            {
                return $obj_user->update(['is_active'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_deactivate($id)
    {
        if ($id) 
        {
            $obj_user = $this->UserModel->where('id',$id)->first();
            if($obj_user)
            {
                return $obj_user->update(['is_active'=>0]);
            }
        }

        return FALSE;
    }

    /*public function perform_delete($id)
    {
        if ($id) 
        {	
        	$user= $this->UserModel->where('id',$id)->first();
            $obj_user= $this->AdminProfileModel->where('user_id',$id)->first();

            if($user!=FALSE && $obj_user!=FALSE)
            {	
            	$delete_user = $user->delete();	
            	return $obj_user->delete();	
            }
        }
        return FALSE;
    }*/
   

     /*
    | multi_action: Following Fuctions for active ,deactive and delete for multiple records
    | auther :Sagar Sainkar    
    | 
    */ 
    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               //$this->perform_delete(base64_decode($record_id));    
               //Session::flash('success','Subadmin(s) deleted successfully.');
               Session::flash('error','You can not delete the user');
            } 
            elseif($multi_action=="activate")
            {
               $this->perform_activate(base64_decode($record_id)); 
               Session::flash('success','Subadmin(s) activated successfully');               
            }
            elseif($multi_action=="deactivate")
            {
               $this->perform_deactivate(base64_decode($record_id));    
               Session::flash('success','Subadmin(s) blocked successfully.');
            }
        }

        return redirect()->back();
    }


    public function change_password($enc_id)
    {
        
        $this->arr_view_data['enc_id'] = $enc_id;
        $this->arr_view_data['page_title'] = "Change Password";
        $this->arr_view_data['module_title'] = "Subadmins";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.subadmins.change_password',$this->arr_view_data);
    }
    public function update_password(Request $request,$enc_id)
    { 

        $arr_rules = array();
        $user_id = base64_decode($enc_id);
        
        $arr_rules['password']         = 'required|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/';
        $arr_rules['confirm_password'] = 'required|same:password|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/';

        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $form_data = array();
        $form_data = $request->all();
        

        $user = Sentinel::findUserById($user_id);

        if ($user)
        {
        	$new_credentials = array();
        	$new_credentials['password'] = $request->input('password');

		    if(Sentinel::update($user,$new_credentials))
		    {
		        Session::flash('success', 'Password changed successfully.');
		    }
		    else
		    {
		        Session::flash('error', 'Problem occured, while changing password.');
		    }        	
        }
        else
	    {
	        Session::flash('error', 'Problem occured, while changing password.');
	    } 
        
        return redirect($this->module_url_path);
    }

    public function set_permissions($enc_id=false)
    {
        if($enc_id==false)
        {
            Session::flash('error','Something went wrong');
            return redirect()->back();
        }

        $arr_modules = $arr_user = $arr_permissions = $support_cats = [];

        $sub_admin_name = '';

        $sub_admin_id   = base64_decode($enc_id);

        $obj_user = $this->AdminProfileModel
                                        ->with('user_details')
                                        ->where('user_id',$sub_admin_id)
                                        ->first();
        if($obj_user){
            $arr_user = $obj_user->toArray();
            $sub_admin_name = $arr_user['name'];
            $sub_admin_support_cat = $arr_user['user_details']['support_category_id'];
            $arr_permissions = $arr_user['user_details']['permissions'];
        }

        $obj_modules = $this->ModulesModel->where('is_active','1')->get();

        if($obj_modules)
        {
            $arr_modules = $obj_modules->toArray();
        }

        $obj_support_cat = $this->SupportCategoryModel->get();

        if($obj_support_cat){
            $support_cats = $obj_support_cat->toArray();
        }

        $this->arr_view_data['page_title']      = "Manage Permissions";
        $this->arr_view_data['module_title']    = "Manage Sub Admins";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        $this->arr_view_data['arr_modules']     = $arr_modules;
        $this->arr_view_data['sub_admin_id']    = $enc_id;
        $this->arr_view_data['sub_admin_name']  = $sub_admin_name;
        $this->arr_view_data['arr_permissions'] = $arr_permissions;
        $this->arr_view_data['support_cat_id']  = $sub_admin_support_cat;
        $this->arr_view_data['support_cats']    = $support_cats;

        return view('admin.subadmins.set_permissions',$this->arr_view_data);
    }

    public function store_permissions(Request $request)
    {
        $arr_rules['permissions']         = 'required';

        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $permission_arr = [];
        $sub_admin_id = base64_decode($request->input('sub_admin_id'));

        $permissions    =   $request->input('permissions');

        if(isset($permissions) && sizeof($permissions)>0)
        {
            if(isset($permissions['moderator']) && sizeof($permissions['moderator'])>0)
            {
                if(array_key_exists('support_ticket', $permissions['moderator']) && $request->input('support_category') == ''){
                    Session::flash('error', 'Please select category for support ticket module.');
                    return redirect()->back();
                }else{
                    $permission_arr = $permissions['moderator'];
                }
            }
        }

        $update = $this->UserModel->where('id',$sub_admin_id)->first();
        $update->permissions = $permission_arr;

        if(array_key_exists('support_ticket', $permissions['moderator']) && $request->input('support_category') != ''){
            $update->support_category_id = $request->input('support_category');
        }

        $store = $update->save();

        if($store)
        {
            Session::flash('success', 'Permissions set successfully.');
            return redirect()->back();
        }
        else
        {
            Session::flash('error', 'Something went wrong.');
            return redirect()->back();
        }
    }
}
