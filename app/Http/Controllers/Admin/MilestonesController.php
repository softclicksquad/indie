<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\ProjectpostModel;
use App\Models\MilestonesModel;
use App\Models\ClientsModel;
use Session;
use Validator;
use Sentinel;

class MilestonesController extends Controller
{
   
    public function __construct(ProjectpostModel $projects,MilestonesModel $milestones,ClientsModel $client)
    {      
       $this->ProjectpostModel  = $projects;
       $this->MilestonesModel   = $milestones;
       $this->ClientsModel      = $client;
       $this->module_url_path   = url(config('app.project.admin_panel_slug')."/milestones");
    }

    /*
     Profile: load page of project milestones
     Author: Ashwini K
    */

	public function all($id)
    {
        $proj_id = "";    
        if($id!=FALSE && $id!="")
        {   
            $proj_id=base64_decode($id);
            $arr_all_milestones  =array();
            $obj_all_milestones  = $this->MilestonesModel->with('project_details','user_details','transaction_details')->where('project_id',$proj_id)->orderBy('updated_at','desc')->get();
            if($obj_all_milestones != FALSE)
            {
                $arr_all_milestones = $obj_all_milestones->toArray();
            }
        }

        $this->arr_view_data['page_title']          = "Manage Project Milestones";
        $this->arr_view_data['module_title']        = "Project Milestones";
        $this->arr_view_data['project_id']          = $proj_id;
        $this->arr_view_data['module_url_path']     = $this->module_url_path;
        $this->arr_view_data['arr_all_milestones']  = $arr_all_milestones;

        return view('admin.project_milestones.manage-project-milestones',$this->arr_view_data);
    }


    /*
     Profile: load page of project milestone details
     Author: Ashwini K
    */
   
    public function show($milestone_id)
    {
        if($milestone_id!=FALSE && $milestone_id!="")
        {    
            $milestone_id = base64_decode($milestone_id);

            $obj_milestone_info = $this->MilestonesModel->with('project_details','user_details','transaction_details')->where('id',$milestone_id)->first();

            $arr_info = array();

            if($obj_milestone_info)
            {

               $arr_info = $obj_milestone_info->toArray();
            }  
        } 
           
        $this->arr_view_data['arr_info']        = $arr_info;
        $this->arr_view_data['page_title']      = "Milestone Details";
        $this->arr_view_data['module_title']    = "Project Milestones";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.project_milestones.show',$this->arr_view_data);
    }


    

     public function delete($enc_id = FALSE)
     {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while client deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Bid deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while bid deletion.');
        }

        return redirect()->back();
     }

    public function perform_delete($id)
    {
        if ($id) 
        {   
            $bid= $this->ProjectsBidsModel->where('id',$id)->first();

            if($bid!=FALSE)
            {   
                return $bid->delete();   
            }
        }
        return FALSE;
    }

   
    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Session::flash('success','Bids(s) deleted successfully.');
            } 
            
        }
        return redirect()->back();
    }



}