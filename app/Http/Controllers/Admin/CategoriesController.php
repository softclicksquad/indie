<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Common\Services\LanguageService;  
use App\Models\CategoriesModel;  

use Validator;
use Session;
Use Sentinel;
use Excel;

class CategoriesController extends Controller
{
     /*
        Auther : Sagar Sainkar
        Comments: controller for categories (projec categories)        
    */
    public $CategoriesModel; 
    
    public function __construct(CategoriesModel $categories,LanguageService $langauge)
    {      
       $this->CategoriesModel = $categories;
       $this->LanguageService = $langauge;
       $this->module_url_path = url(config('app.project.admin_panel_slug')."/categories");

       $this->image_base_img_path   = public_path().config('app.project.img_path.category_image');
       $this->image_public_img_path = url('/').config('app.project.img_path.category_image');
    }


    /*
        Auther : Sagar Sainkar
        Comments: display catergories
    */
    public function index()
    {
        $arr_lang   =  $this->LanguageService->get_all_language();

        $obj_category = $this->CategoriesModel->with(['translations','is_sub_category_exist'])->get();

        if($obj_category != FALSE)
        {
            $arr_categories = $obj_category->toArray();
        }

        $this->arr_view_data['arr_categories'] = $arr_categories;

        $this->arr_view_data['page_title'] = "Manage Project Categories";
        $this->arr_view_data['module_title'] = "Project Categories";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.categories.index',$this->arr_view_data);
    }


    /*  
        Auther : Sagar Sainkar
        Comments: display view for Add new category
    */

    public function create()
    {

        $this->arr_view_data['arr_lang'] = $this->LanguageService->get_all_language();
        $this->arr_view_data['page_title'] = "Create Categories";
        $this->arr_view_data['module_title'] = "Categories";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.categories.create',$this->arr_view_data);
    }

    /*  
        Auther : Sagar Sainkar
        Comments: Add new category
    */
    public function store(Request $request)
    {
        $form_data = array();

        $form_data = $request->all();
        $arr_rules['category_title_en'] = "required";
        
        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
             return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        /* Check if skill already exists with given translation */
        $does_exists = $this->CategoriesModel->whereHas('translations',function($query) use($request)
             {
                  $query->where('locale', 'en')
                        ->where('category_title',$request->input('category_title_en'));
             })->count();
        
        if($does_exists)
        {
            Session::flash('error',trans('controller_translations.text_already_exist'));            
            return redirect()->back();
        }
    
        $form_data = $request->all();
        $arr_data  = array();

        /*  Image Upload */
        $is_new_file_uploaded = FALSE;
        if($request->hasFile('image')) 
        {   
            $image           = $request->input('image');
            $excel_file_name = $image;
            $fileExtension   = strtolower($request->file('image')->getClientOriginalExtension());
            $file_name       = sha1(uniqid().$excel_file_name.uniqid()).'.'.$fileExtension;
            $request->file('image')->move($this->image_base_img_path,$file_name); 
            $is_new_file_uploaded = TRUE;         
        }
        if($is_new_file_uploaded){
            /* Unlink */
            if(isset($pre_image) && $pre_image != "" && file_exists($this->image_base_img_path.$pre_image) ){
                $unlink_path    = $this->image_base_img_path.$pre_image;
                @unlink($unlink_path);
            }
            /* Unlink */
            $arr_data['category_image']  = $file_name;
        }
        /* End Image Upload */ 
        $arr_data['category_slug'] = str_slug($form_data['category_title_en']);
        $arr_data['is_active'] = 1;
        
        $obj_category    = $this->CategoriesModel->create($arr_data);

        $category_id = $obj_category->id;

        /* Fetch All Languages*/
        $arr_lang =  $this->LanguageService->get_all_language();

        if(sizeof($arr_lang) > 0 )
        {
            foreach ($arr_lang as $lang) 
            {            
                $arr_data     = array();
                $category_title   = 'category_title_'.$lang['locale'];
                
                if( isset($form_data[$category_title]) && $form_data[$category_title] != '')
                { 
                    $translation = $obj_category->translateOrNew($lang['locale']);
                    $translation->category_title       = ucfirst($form_data[$category_title]);
                    $translation->categories_id  = $category_id;
                    $translation->save();
                    Session::flash('success',trans('controller_translations.text_category_created_successfully'));
                }

            }//foreach

        } //if
        else
        {
            Session::flash('error',trans('controller_translations.text_sorry_somthing_goes_wrong_please_try_again'));
            
        }

        return redirect()->back();
    }


    /*  
        Auther : Sagar Sainkar
        Comments: display view for edit category
    */

    public function edit($enc_id)
    {
        $id = base64_decode($enc_id);

        $arr_lang = $this->LanguageService->get_all_language();      

        $obj_category = $this->CategoriesModel->where('id', $id)->with(['translations'])->first();

        $arr_categories = [];

        if($obj_category)
        {
           $arr_categories = $obj_category->toArray(); 
           /* Arrange Locale Wise */
           $arr_categories['translations'] = $this->arrange_locale_wise($arr_categories['translations']);
        }

        $this->arr_view_data['enc_id'] = $enc_id;
        $this->arr_view_data['arr_lang'] = $this->LanguageService->get_all_language();          
        $this->arr_view_data['arr_categories'] = $arr_categories;  

        $this->arr_view_data['page_title'] = "Edit Category";
        $this->arr_view_data['module_title'] = "Categories";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.categories.edit',$this->arr_view_data);  

    }


    /*  
        Auther : Sagar Sainkar
        Comments: update category details
    */
    public function update(Request $request, $enc_id)
    {
        $category_id = base64_decode($enc_id);
        $arr_rules = array();
        $status = FALSE;

        $arr_rules['category_title_en']     = "required";        
        
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = array();
        $form_data = $request->all(); 

         /* Get All Active Languages */ 
  
        $arr_lang = $this->LanguageService->get_all_language();

        $category = $this->CategoriesModel->where('id',$category_id)->first();

        
         /* Insert Multi Lang Fields */

        if(sizeof($arr_lang) > 0 && $category)
        { 

            /*  Image Upload */
            $is_new_file_uploaded = FALSE;
            if($request->hasFile('image')) 
            {   
                $image     = $request->input('image');
                $excel_file_name = $image;
                $fileExtension   = strtolower($request->file('image')->getClientOriginalExtension()); 
                $file_name       = sha1(uniqid().$excel_file_name.uniqid()).'.'.$fileExtension;
                $request->file('image')->move($this->image_base_img_path,$file_name); 
                $is_new_file_uploaded = TRUE;         
            }
            if($is_new_file_uploaded){
                /* Unlink */
                if($request->input('pre_image')!=null && $request->input('pre_image')!=""){
                    if(isset($pre_image) && $pre_image != "" && file_exists($this->image_base_img_path.$pre_image) ){
                        $unlink_path    = $this->image_base_img_path.$pre_image;
                        @unlink($unlink_path);
                    }
                }
                /* Unlink */
                $arr_data['category_image']  = $file_name;
            }
            /* End Image Upload */ 
            $arr_data['category_slug'] = str_slug($form_data['category_title_en']);
            $category->update($arr_data);

            foreach($arr_lang as $i => $lang)
            {
                $translate_data_ary = array();
                $category_title   = 'category_title_'.$lang['locale'];

                if(isset($form_data[$category_title]) && $form_data[$category_title]!="")
                {
                    /* Get Existing Language Entry and update it */
                    $translation = $category->getTranslation($lang['locale']);    
                    if($translation)
                    {

                        $translation->category_title       =  ucfirst($form_data['category_title_'.$lang['locale']]);
                        $status = $translation->save();                       
                    }  
                    else
                    {
                        /* Create New Language Entry  */
                        $translation     = $category->getNewTranslation($lang['locale']);
                        $translation->categories_id  =  $category_id;
                        $translation->category_title       =  ucfirst($form_data['category_title_'.$lang['locale']]);
                        $status = $translation->save();
                    } 
                }   
            }
            
        }

        if ($status) 
        {
            Session::flash('success',trans('controller_translations.text_category_updated_successfully'));    
        }
        else
        {
            Session::flash('error',trans('controller_translations.text_sorry_somthing_goes_wrong_please_try_again'));       
        }
        
        return redirect()->back();
    }

    /*
    | Following Fuctions for active ,deactive and delete
    | auther :Sagar Sainkar
    | 
    */ 

    public function activate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while category activation.');
            return redirect()->back();
        }

        if($this->perform_activate(base64_decode($enc_id)))
        {
            Session::flash('success','Category activated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while category activation.');
        }

        return redirect()->back();
    }

    public function deactivate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while category deactivation.');
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            Session::flash('success','Category deactivated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while category deactivation.');
        }

        return redirect()->back();
    }

    public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while category deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Category deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while category deletion.');
        }

        return redirect()->back();
    }


    public function perform_activate($id)
    {
        if ($id) 
        {
            $service_category = $this->CategoriesModel->where('id',$id)->first();
            if($service_category)
            {
                return $service_category->update(['is_active'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_deactivate($id)
    {
        if ($id) 
        {
            $service_category = $this->CategoriesModel->where('id',$id)->first();
            if($service_category)
            {
                return $service_category->update(['is_active'=>0]);
            }
        }

        return FALSE;
    }

    public function perform_delete($id)
    {
        if ($id) 
        {
            $service_category= $this->CategoriesModel->where('id',$id)->first();
            if($service_category)
            {
                return $service_category->delete();
            }
        }
        return FALSE;
    }
   

    /*
    | multi_action: Following Fuctions for active ,deactive and delete for multiple records
    | auther :Sagar Sainkar
    | Date : 
    | 
    */ 
    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        /*if ($multi_action=="export") 
        {
            $this->_export($request);
        }*/

        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Session::flash('success','Category(s) deleted successfully.');
            } 
            elseif($multi_action=="activate")
            {
               $this->perform_activate(base64_decode($record_id)); 
               Session::flash('success','Category(s) activated successfully');               
            }
            elseif($multi_action=="deactivate")
            {
               $this->perform_deactivate(base64_decode($record_id));    
               Session::flash('success','Category(s) blocked successfully.');
            }

        }

        return redirect()->back();
    }

    public function arrange_locale_wise(array $arr_data)
    {
        if(sizeof($arr_data)>0)
        {
            foreach ($arr_data as $key => $data)
            {
                $arr_tmp = $data;
                unset($arr_data[$key]);

                $arr_data[$data['locale']] = $data;                    
            }

            return $arr_data;
        }
        else
        {
            return [];
        }
    }


    /* export: export categories and Generate CSV file */

    public function export_categories()
    {
        $obj_category = FALSE;
        $arr_categories = array();
        $export_categories = array();

        $obj_category = $this->CategoriesModel->with(['translations'])->get();

        if($obj_category != FALSE)
        {
            $arr_categories = $obj_category->toArray();

            if(isset($arr_categories) && sizeof($arr_categories)>0)
            {  
                foreach ($arr_categories as $key => $value) 
                {
                    $arr_categories[$key]['translations'] = $this->arrange_locale_wise($value['translations']);
                }
            }
            
        }

        if(isset($arr_categories) && sizeof($arr_categories)>0)
        {
            foreach ($arr_categories as $key => $category) 
            {
               
               $export_categories[$key]['Category Title English']=isset($category['translations']['en']['category_title'])?$category['translations']['en']['category_title']:'';
               $export_categories[$key]['Category Title German']=isset($category['translations']['de']['category_title'])?$category['translations']['de']['category_title']:'';
               $export_categories[$key]['Category_slug']=isset($category['category_slug'])?$category['category_slug']:'';
               $export_categories[$key]['Status']=isset($category['is_active']) && $category['is_active']?'Active':'Inactive';
            }
        }

        $data = $export_categories;
        $type = 'CSV';

        return Excel::create('Categories', function($excel) use ($data) {

             // Set the title
            $excel->setTitle('Categories Backup');

            // Chain the setters
            $excel->setCreator('VirtualHomeConcept')
                    ->setCompany('VirtualHomeConcept');

            // Call them separately
            $excel->setDescription('Categories Backup');

            $excel->sheet('categories', function($sheet) use ($data)
            {
                $sheet->fromArray($data);

            });


        })->download($type);
    }
}