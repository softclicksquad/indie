<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\ProjectpostModel;
use App\Models\ReviewsModel;
use App\Models\ProjectManagerModel;
use App\Models\RecruiterModel;
use App\Models\NotificationsModel;
use App\Models\AdminProfileModel;

use App\Common\Services\MailService;

use Session;
use Validator;
use Sentinel;
use Mail;
use Lang;

class ProjectsController extends Controller
{
    /*
        Auther : Shankar Jamdhade
        Comments: controller for manage all projects
    */

    public function __construct(
                                ProjectpostModel $projects,
                                ProjectManagerModel $projectmanager,
                                RecruiterModel $recruiter,
                                ReviewsModel $review,
                                MailService $mail_service,
                                NotificationsModel $notifications,
                                AdminProfileModel  $adminprofilemodel
                                )
    {      
       $this->ProjectpostModel = $projects;
       $this->ProjectManagerModel = $projectmanager;
       $this->RecruiterModel      = $recruiter;
       $this->ReviewsModel = $review;
       $this->NotificationsModel = $notifications;
       $this->MailService = $mail_service;
       $this->AdminProfileModel  = $adminprofilemodel;

       $user = Sentinel::check();
       if(!$user)
       {
        return redirect()->back();
       }

       $this->user_id = $user->id;
       $this->module_url_path = url(config('app.project.admin_panel_slug')."/projects");


       $this->expert_project_attachment_public_path  = url('/').config('app.project.img_path.expert_project_attachment');
    }

    /*
    Auther : Shankar Jamdhade
    Comments: display all projects
    */

    public function all()
    {
        $arr_all_projects=array();
        $obj_all_projects = $this->ProjectpostModel->whereIn('project_status',['1','2','3','4','5'])->with('skill_details','client_details','category_details','transaction_details')->orderBy('id','desc')->get();

        if($obj_all_projects != FALSE)
        {
            $arr_all_projects = $obj_all_projects->toArray();
            $arr_all_projects = filter_valid_projects($arr_all_projects);
        }   


        $this->arr_view_data['arr_all_projects']    = $arr_all_projects;
        $this->arr_view_data['page_title']          = "Manage All Projects";
        $this->arr_view_data['module_title']        = "All Projects";
        $this->arr_view_data['module_url_path']     = $this->module_url_path;
        
        return view('admin.projects.manage-projects',$this->arr_view_data);
    }
    /*
        Auther : Shankar Jamdhade
        Comments:display New projects
    */

    /*project posted by client and need project manager*/
    
    public function posted_projects()
    {
        $arr_all_projects=array();

        $obj_all_projects = $this->ProjectpostModel->with('skill_details','client_details','category_details')->where('project_status','4')->where('project_handle_by_pm','1')->orderBy('id','DESC')->get();

        if($obj_all_projects != FALSE)
        {
            $arr_all_projects = $obj_all_projects->toArray();
        }
        
        $this->arr_view_data['arr_all_projects'] = $arr_all_projects;
        $this->arr_view_data['page_title']       = "Manage Assign Project Manager";
        $this->arr_view_data['module_title']     = "Posted Projects";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.projects.manage-projects',$this->arr_view_data);
    }

    public function assign_recruiter()
    {
        $arr_all_projects=array();
        $obj_all_projects = $this->ProjectpostModel->with('skill_details','client_details','category_details')->whereIn('project_status',['1','2'])->where('project_handle_by','2')->orderBy('id','DESC')->get();

        if($obj_all_projects != FALSE)
        {
            $arr_all_projects = $obj_all_projects->toArray();
        }

        $this->arr_view_data['arr_all_projects'] = $arr_all_projects;
        $this->arr_view_data['page_title']       = "Manage Assign Recruiter";
        $this->arr_view_data['module_title']     = "Posted Projects";
        $this->arr_view_data['module_url_path']  = $this->module_url_path;

        return view('admin.projects.manage-projects',$this->arr_view_data);   
    }

    public function open_projects()
    {
        $arr_all_projects=array();
        $obj_all_projects = $this->ProjectpostModel->with('skill_details','client_details','category_details')->where('project_status','2')->orderBy('id','DESC')->get();

        if($obj_all_projects != FALSE)
        {
            $arr_all_projects = $obj_all_projects->toArray();
        }

        /* Making Notifications Null */
       // $this->NotificationsModel->where('user_id','=',$this->user_id)->update(['is_seen'=>'1']);

        $this->arr_view_data['arr_all_projects'] = $arr_all_projects;
        $this->arr_view_data['page_title']  = "Manage Open Projects";
        $this->arr_view_data['module_title'] = "Open Projects";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.projects.manage-projects',$this->arr_view_data);
    }


    public function ongoing_projects()
    {
        $arr_all_projects=array();
        $obj_all_projects = $this->ProjectpostModel->with('skill_details','client_details','category_details')->where('project_status','4')->orderBy('id','DESC')->get();

        if($obj_all_projects != FALSE)
        {
            $arr_all_projects = $obj_all_projects->toArray();
        }

       
        /* Making Notifications Null */
       // $this->NotificationsModel->where('user_id','=',$this->user_id)->update(['is_seen'=>'1']);

        $this->arr_view_data['arr_all_projects'] = $arr_all_projects;
        $this->arr_view_data['page_title'] = "Manage Ongoing Projects";
        $this->arr_view_data['module_title'] = "Ongoing Projects";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.projects.manage-projects',$this->arr_view_data);
    }

    /*
        Auther : Shankar Jamdhade
        Comments:display closed projects
    */
    public function completed_project()
    {
        $arr_all_projects=array();
        $obj_all_projects = $this->ProjectpostModel->with('skill_details','client_details','category_details')->where('project_status','3')->orderBy('id','DESC')->get();

        if($obj_all_projects != FALSE)
        {
            $arr_all_projects = $obj_all_projects->toArray();
        }

        /*unset PROJECT_ID FROm Session */
        if(Session::has('COMPLETED'))
        {   
            $request->session()->forget('COMPLETED');
        }

        $this->arr_view_data['arr_all_projects'] = $arr_all_projects;
        $this->arr_view_data['page_title'] = "Manage Completed Projects";
        $this->arr_view_data['module_title'] = "Completed Projects";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.projects.manage-projects',$this->arr_view_data);
    }
    /*
        Auther : Shankar Jamdhade
        Comments:display New projects
    */
    public function canceled_project()
    {
        $arr_all_projects=array();
        $obj_all_projects = $this->ProjectpostModel->with('skill_details','client_details','category_details')->where('project_status','5')->orderBy('id','DESC')->get();

        if($obj_all_projects != FALSE)
        {
            $arr_all_projects = $obj_all_projects->toArray();
        }

        /* Making Notifications Null */
       // $this->NotificationsModel->where('user_id','=',$this->user_id)->update(['is_seen'=>'1']);

        $this->arr_view_data['arr_all_projects'] = $arr_all_projects;
        $this->arr_view_data['page_title']       = "Manage Canceled Projects";
        $this->arr_view_data['module_title']     = "Canceled Projects";
        $this->arr_view_data['module_url_path']  = $this->module_url_path;
        return view('admin.projects.manage-projects',$this->arr_view_data);
    }
    /*  
        Auther : Shankar Jamdhade
        Comments: display view for projects details.
    */

    public function show($project_id)
    {
        $arr_info      = [];
        $review_info   = [];
        $arr_manager   = [];
        $arr_recruiter = [];
        
        if($project_id!=FALSE && $project_id!="")
        {
            $id = base64_decode($project_id);

            $obj_project_info = $this->ProjectpostModel->with('skill_details','client_details','category_details','project_manager_info','experts_project_attachment','project_skills.skill_data','project_manager_details','project_recruiter_info','project_recruiter_details')->where('id',$id)->first();

            $obj_reviews      = $this->ReviewsModel->where('project_id',$id)->with(['client_details','expert_details'])->orderBy('created_at', 'DESC')->get();
            
            $arr_info    = array();

            if($obj_project_info)
            {
               $arr_info = $obj_project_info->toArray();
            }

            if($obj_reviews)
            {
               $review_info = $obj_reviews->toArray();
            }

            $manager = $this->ProjectManagerModel->whereHas('user_details',function ($query)
            {   
                $query->where('is_active',1);
                $query->where('is_available',1);

            })->get();

            if($manager!=FALSE)
            {
                $arr_manager=$manager->toArray();
            }

            $recruiter = $this->RecruiterModel->whereHas('user_details',function ($query)
            {   
                $query->where('is_active',1);
                $query->where('is_available',1);

            })->get();

            if($recruiter!=FALSE)
            {
                $arr_recruiter=$recruiter->toArray();
            }
      }

        $this->arr_view_data['arr_manager'] = $arr_manager;
        $this->arr_view_data['arr_recruiter'] = $arr_recruiter;
        $this->arr_view_data['arr_info']    = $arr_info;
        $this->arr_view_data['review_info'] = $review_info;

        $this->arr_view_data['page_title'] = "Projects Details";
        $this->arr_view_data['module_title'] = "New Projects";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        $this->arr_view_data['expert_project_attachment_public_path']  = $this->expert_project_attachment_public_path; 

        if(isset($arr_info) && isset($arr_info['project_handle_by']) && $arr_info['project_handle_by'] == '2')
        {
            return view('admin.projects.show_recruiter',$this->arr_view_data);
        }
        
        return view('admin.projects.show',$this->arr_view_data);

    }

    /*
      Comments: Show Reviews for completed Projects.
      Auther  : Nayan S.
    */

    public function project_reviews($enc_id)
    {
        $project_id = base64_decode($enc_id);

        $arr_project_reviews = [];
        
        $obj_project_reviews = $this->ProjectpostModel->where('project_status','3')
                                                      ->where('id',$project_id) 
                                                      ->whereHas('project_reviews', function () {})
                                                      ->with('project_reviews','project_reviews.user_details')
                                                      ->first(['id','project_name','project_status','client_user_id']);
                                                      
        if($obj_project_reviews != FALSE)
        {
            $arr_project_reviews = $obj_project_reviews->toArray();
        }

        $this->arr_view_data['arr_project_reviews'] = $arr_project_reviews;
        $this->arr_view_data['page_title']          = "View Project Reviews";
        $this->arr_view_data['module_url_path']     = $this->module_url_path;
        return view('admin.projects.project_reviews.index',$this->arr_view_data);
    }

    /*
      Comments: update Review for view project review.
      Auther  : Sagar Sahuji.
    */

    public function project_review_update(Request $request)
     {
        $review_id = base64_decode($request->input('review_id'));

        if($review_id == "")
        {
           return redirect()->back(); 
        }

        $arr_rules['message'] = "required";
        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
             return redirect()->back()->withErrors($validator)->withInput($request->all());
        }
        $review = $request->input('message');
        
        $arr_review=array();
        $arr_review['review']=$review;

        $result = $this->ReviewsModel->where('id','=',$review_id)->update($arr_review);     
        if($result)
        {   
            Session::flash('success','Project review updated successfully');
        }   
        else
        {
            Session::flash('error','Problem occured, while updating project review.');
        }                                               
        
        return redirect()->back();
     } 

    /*
      Comments: Show Review Details for Single Review.
      Auther  : Nayan S.
    */


      
    public function project_review_details($enc_id)
    {
        $review_id = base64_decode($enc_id);

        $arr_project_review = [];
        
        $obj_project_review  = $this->ReviewsModel->where('id',$review_id)
                                                ->with('user_details')
                                                ->with(['project_details'=> function ($query) {
                                                        $query->select('id','project_name');    
                                                    }])                
                                                ->first();
                                                      
        if($obj_project_review != FALSE)
        {
            $arr_project_review = $obj_project_review->toArray();
        }


        $this->arr_view_data['arr_project_review']  = $arr_project_review;
        $this->arr_view_data['page_title']          = "View Project Review";
        $this->arr_view_data['module_url_path']     = $this->module_url_path;
        return view('admin.projects.project_reviews.view',$this->arr_view_data);
    }

    ## By Shankar => Update status of Projects
    public function update_project_status(Request $request)
    {
        $project_id          = $request->input('project_id',0);
        $project_status      = $request->input('project_status',0);
        $project_manager     = $request->input('project_manager',0);
        $pre_project_manager = $request->input('pre_project_manager_id',0);
        $admin               = Sentinel::check();
        $adminmail           = $admin['email'];
        $adminid             = $admin['id'];

        if($admin->inRole('subadmin') && $project_status == 5)
        {
            Session::flash("error",trans('controller_translations.text_you_do_not_have_sufficient_privileges'));
            return redirect()->back();
        }

        if(isset($project_manager) && $project_manager!=0)
        {
            $status  = $this->ProjectpostModel->where('id',$project_id)->update(['project_status'=>$project_status,'project_manager_user_id'=>$project_manager]);
            
            if($status)
            {
                /* send notification to manager */
                $arr_data                         = [];
                $arr_data['user_id']              = $project_manager;
                $arr_data['project_id']           = $project_id;
                $arr_data['user_type']            = '4';
                $arr_data['url']                  = 'project_manager/projects/details/'.base64_encode($project_id);
                /*$arr_data['notification_text']  = "New Project Assigned.";*/
                $arr_data['notification_text_en'] = Lang::get('controller_translations.notification_new_project_assign',[],'en','en');
                $arr_data['notification_text_de'] = Lang::get('controller_translations.notification_new_project_assign',[],'de','en');
                $this->NotificationsModel->create($arr_data);
                /* end  notification to pre manager */
            }
        } else {
            $status  = $this->ProjectpostModel->where('id',$project_id)->update(['project_status'=>$project_status]);
        }


        if($status)
        {
            $obj_client_data = $this->ProjectpostModel->select("project_name","client_user_id","project_handle_by","project_manager_user_id","project_description")
                                                    ->where('id',$project_id)
                                                    ->with("project_manager_info.user_details","client_details")
                                                    ->first();
            if(isset($obj_client_data) & $obj_client_data!= null)
            {
                $client_data = [];
                $client_data = $obj_client_data->toArray();   
            }

            if($project_status == '5')
            {

                $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
                 //  //send notification to admin for payment refund
                 //  $arr_client_data                         = [];
                 //  $arr_client_data['user_id']              = $adminid;
                 //  $arr_client_data['user_type']            = '1';
                 //  $arr_client_data['url']                  = 'admin/projects/show/'.base64_encode($project_id);
                 //  $arr_client_data['project_id']           = $project_id;
                 //  $arr_client_data['notification_text_en'] = "You have received new notification for payment refund";
                 //  $arr_client_data['notification_text_de'] = "Sie haben eine neue Benachrichtigung zur Rückerstattung der Zahlung erhalten";
                 // $this->NotificationsModel->create($arr_client_data);
                 //  //end notification to admin for payment refund

                  // send mail notification to project manager for project cancellation

                //send mail notification to client for project cancellation

                $obj_admin = $this->AdminProfileModel->where('user_id',$this->user_id)->first();
                $admin_name = isset($obj_admin->name)?$obj_admin->name:'';


                  $project_name                     = isset($client_data['project_name'])?$client_data['project_name']:"NA";

                  $data                             = [];
                  $data['project_name']             = $project_name;
                  $data['cancelled_by']             = $admin_name;
                  $data['client_username']          = isset($client_data['client_details']['user_name'])?$client_data['client_details']['user_name']:"NA";
                  $data['user_first_name']          = isset($data['client_username'])?$data['client_username']:"";
                  $data['project_description']      = isset($client_data['project_description'])?$client_data['project_description']:"NA";
                  $data['project_handle_by']        = isset($client_data['project_handle_by'])?$client_data['project_handle_by']:"";
                  $data['project_manager_username'] = isset($client_data['project_manager_info']['first_name'])?$client_data['project_manager_info']['first_name']:"NA";
                  $email_to                         = isset($client_data['client_details']['email'])?$client_data['client_details']['email']:"NA";; 
                  //$mail_form                        = $adminmail;
                    $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
                    try{
                          Mail::send('front.email.project_cancellation', $data, function ($message) use ($email_to,$mail_form,$project_name) {
                              $message->from($mail_form, $project_name);
                              $message->subject($project_name.':Project Cancellation.');
                              $message->to($email_to);
                          });
                      }
                      catch(\Exception $e){
                      Session::Flash('error'   , 'E-Mail not sent');
                      }
                  // end mail notification to client for project cancellation
                if(isset($client_data['project_handle_by']) && $client_data['project_handle_by'] == 2) {
                  $data['user_first_name'] = $data['project_manager_username'];
                  $data['cancelled_by']    = $admin_name;
                  $email_to                = isset($client_data['project_manager_info']['user_details']['email'])?$client_data['project_manager_info']['user_details']['email']:"NA";
                    
                          try{
                              Mail::send('front.email.project_cancellation', $data, function ($message) use ($email_to,$mail_form,$project_name) {
                                  $message->from($mail_form, $project_name);
                                  $message->subject($project_name.':Project Cancellation.');
                                  $message->to($email_to);
                              });
                          }
                          catch(\Exception $e){
                          Session::Flash('error'   , 'E-Mail not sent');
                          }
                  // send mail notification to project manager for project cancellation

                 //send project cancelation notification to project manager
                  $arr_client_data['user_id']              = isset($client_data['project_manager_user_id'])?$client_data['project_manager_user_id']:'';
                  $arr_client_data['user_type']            = '4';
                  $arr_client_data['url']                  = 'project_manager/projects/details/'.base64_encode($project_id);
                  $arr_client_data['project_id']           = $project_id;
                  $arr_client_data['notification_text_en'] = "Project canceled by admin";
                  $arr_client_data['notification_text_de'] = "Projekt wurde vom Admin abgebrochen";
                  $this->NotificationsModel->create($arr_client_data);

                 //send project cancelation notification to client
                  $arr_client_data['user_id']              = isset($client_data['client_user_id'])?$client_data['client_user_id']:'';
                  $arr_client_data['user_type']            = '2';
                  $arr_client_data['url']                  = 'client/projects/details/'.base64_encode($project_id);
                  $arr_client_data['project_id']           = $project_id;
                  $arr_client_data['notification_text_en'] = "Project canceled by admin";
                  $arr_client_data['notification_text_de'] = "Projekt wurde vom Admin abgebrochen";
                 $this->NotificationsModel->create($arr_client_data);
                }    
            }

            /* Set Notification for Project Manager that new project is assigned to him only if project status is 2 */
            if($project_status == '2')
            {
                /* Send mail to Project manager */
                $project_name = config('app.project.name');
                //$mail_form    = get_site_email_address();   /* getting email address of admin from helper functions */
                $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
               
                $obj_project_info = $this->ProjectpostModel->where('id',$project_id)
                                                          ->with(['project_manager_info'=> function ($query) {
                                                                  $query->select('user_id','first_name','last_name');
                                                                },
                                                                'project_manager_details'=>function ($query_nxt) {
                                                                  $query_nxt->select('id','email','user_name');
                                                                }])
                                                          ->with(['client_info'=> function ($query) {
                                                                  $query->select('user_id','first_name','last_name');
                                                                },
                                                                'client_details'=>function ($query_nxt) {
                                                                  $query_nxt->select('id','email','user_name');
                                                                }])
                                                          ->first(['id','project_name','client_user_id','expert_user_id','project_manager_user_id','project_name','project_description']);

                $data = [];
                if($obj_project_info)
                {
                    $arr_project_info                 = $obj_project_info->toArray();
                    /* get client name */
                    $pm_first_name                    = isset($arr_project_info['project_manager_info']['first_name'])?$arr_project_info['project_manager_info']['first_name']:'';
                    $pm_last_name                     = isset($arr_project_info['project_manager_info']['last_name'])?$arr_project_info['project_manager_info']['last_name']:'';
                    /*$pm_name                        = $pm_first_name.' '.$pm_last_name;*/
                    $pm_name                          = $pm_first_name;
                    /*PM username*/
                    $pm_user_name                     = isset($arr_project_info['project_manager_details']['user_name'])?$arr_project_info['project_manager_details']['user_name']:'';
                    /* Getting admins information from helper function */
                    $admin_data                       = get_admin_email_address();
                    $data['project_name']             = isset($arr_project_info['project_name'])?$arr_project_info['project_name']:'';
                    $data['project_description']      = isset($arr_project_info['project_description'])?$arr_project_info['project_description']:'';
                    $data['project_manager_name']     = $pm_name;
                    $data['project_manager_username'] = $pm_user_name;
                    $data['admin_name']               = isset($admin_data['user_name'])?$admin_data['user_name']:'';
                    $data['login_url']                = url('/redirection?redirect=PROJECT&id='.base64_encode($project_id));
                    $data['email_id']                = isset($arr_project_info['project_manager_details']['email'])?
                    $arr_project_info['project_manager_details']['email']:'';
                    /* client information */
                    $client_name = isset($arr_project_info['client_info']['first_name'])?$arr_project_info['client_info']['first_name']:'';
                    $data['client_name'] = $client_name;
                }
                $email_to = isset($arr_project_info['project_manager_details']['email'])?$arr_project_info['project_manager_details']['email']:'';
                if($email_to!= "")
                {
                    try{
                        $mail_status = $this->MailService->send_project_assigned_to_project_manager_email($data);
                        // Mail::send('front.email.project_assigned_to_project_manager', $data, function ($message) use ($email_to,$mail_form,$project_name) {
                        //   $message->from($mail_form, $project_name);
                        //   $message->subject($project_name.': Project Manager Assigned.');
                        //   $message->to($email_to);
                        // });
                    }
                    catch(\Exception $e){
                        Session::Flash('error'   , 'E-Mail not sent');
                    }
                }
                /* Send email to client informing about project manager is assigned */
                $client_email = isset($arr_project_info['client_details']['email'])?$arr_project_info['client_details']['email']:'';

                if($client_email!= "")
                {
                    try{
                        $mail_status = $this->MailService->send_project_assigned_to_project_manager_mail_to_client_email($data);
                        // Mail::send('front.email.project_assigned_to_project_manager_mail_to_client', $data, function ($message) use ($client_email,$mail_form,$project_name) {
                        //       $message->from($mail_form, $project_name);
                        //       $message->subject($project_name.': Project Manager Assigned.');
                        //       $message->to($client_email);
                        // });
                    }
                    catch(\Exception $e){
                        Session::Flash('error'   , 'E-Mail not sent');
                    }
                }
              
              //if(isset($project_manager) && isset($pre_project_manager) && $pre_project_manager != 0 && $pre_project_manager != $project_manager){
                /* send notification to client or previous manager if admin change project manager for a project
                   
                /* send notification to client */
                $arr_data['user_id']              = isset($arr_project_info['client_user_id'])?$arr_project_info['client_user_id']:'0';
                $arr_data['user_type']            = '2';
                $arr_data['url']                  = 'client/projects/details/'.base64_encode($project_id);
                $arr_data['project_id']           = $project_id;
                $arr_data['notification_text_en'] = Lang::get('controller_translations.new_project_manager_assign_to_project_by_admin',[],'en','en');
                $arr_data['notification_text_de'] = Lang::get('controller_translations.new_project_manager_assign_to_project_by_admin',[],'de','en');
                $this->NotificationsModel->create($arr_data); 
                /* end  notification to client */

                /* send notification to manager */
                // $arr_data                         = [];
                // $arr_data['user_id']              = $project_manager;
                // $arr_data['project_id']           = $project_id;
                // $arr_data['user_type']            = '4';
                // $arr_data['url']                  = 'project_manager/projects/details/'.base64_encode($project_id);
                // $arr_data['notification_text']  = "New Project Assigned.";
                // $arr_data['notification_text_en'] = Lang::get('controller_translations.notification_new_project_assign',[],'en','en');
                // $arr_data['notification_text_de'] = Lang::get('controller_translations.notification_new_project_assign',[],'de','en');
                // $this->NotificationsModel->create($arr_data);
                /* end  notification to pre manager */

                /* end notification to client or previous manager if admin change project manager for a project */
              //}
            }

            /* Mail ends */
            /* ++++ Send Email / NOtifications for project completion ++++*/
            if($project_status == '3')
            {
                /* Create Notification for client */      
                $this->ProjectpostModel->where('id',$project_id)->update(['has_completion_request'=>3]);

                $arr_data =  [];
                /*$arr_data['notification_text'] = 'Project Completed By Admin' ;*/
                $arr_data['notification_text_en'] = Lang::get('controller_translations.notification_project_completed_by_admin',[],'en','en');
                $arr_data['notification_text_de'] = Lang::get('controller_translations.notification_project_completed_by_admin',[],'de','en');

                $arr_data['project_id'] = $project_id;

                if(isset($obj_project_info->client_user_id) && $obj_project_info->client_user_id != "")
                {
                $arr_data['user_id']    = $obj_project_info->client_user_id;
                $arr_data['user_type']  = '2';
                $arr_data['url']        = 'client/projects/details/'.base64_encode($project_id);
                $this->completed_project_notification($arr_data);
                }

                if(isset($obj_project_info->expert_user_id) && $obj_project_info->expert_user_id != "")
                {
                $arr_data['user_id']    = $obj_project_info->expert_user_id;
                $arr_data['user_type']  = '3';
                $arr_data['url']        = 'expert/projects/details/'.base64_encode($project_id);
                $this->completed_project_notification($arr_data);
                }

                if(isset($obj_project_info->project_manager_user_id) && $obj_project_info->project_manager_user_id != "")
                {
                $arr_data['user_id']    = $obj_project_info->project_manager_user_id;
                $arr_data['user_type']  = '4';
                $arr_data['url']        = 'project_manager/projects/details/'.base64_encode($project_id);
                $this->completed_project_notification($arr_data);
                }

                /* to admin */
                $arr_data['user_id']    = 1;
                $arr_data['user_type']  = '1';
                $arr_data['url']        = 'admin/projects/completed';

                $this->completed_project_notification($arr_data);

                /* Notificatin ends */

                /* Send mail to client */
                $completed_by_admin = TRUE;
                $this->MailService->project_completed($project_id ,$completed_by_admin);
            }
            /* ++++ Notifications for complete project ends +++*/            
            Session::flash('success','Project details updated successfully.');
        } 
        else
        {
            Session::flash('error','Problem occured, while updating project details.');
        }
        return redirect()->back();
    }

    public function update_recruiter_project_status(Request $request)
    {
        $project_id            = $request->input('project_id',0);
        $project_recruiter     = $request->input('project_recruiter',0);
        $pre_project_recruiter = $request->input('pre_project_recruiter_id',0);
        $admin                 = Sentinel::check();
        $adminmail             = $admin['email'];
        $adminid               = $admin['id'];

        if($admin->inRole('subadmin') && $project_status == 5)
        {
            Session::flash("error",trans('controller_translations.text_you_do_not_have_sufficient_privileges'));
            return redirect()->back();
        }

        if(isset($project_recruiter) && $project_recruiter!=0)
        {
            $status  = $this->ProjectpostModel->where('id',$project_id)->update(['project_recruiter_user_id'=>$project_recruiter,'project_status'=>'2','recruiter_assign_date'=>date('Y-m-d H:i:s')]);

            /* send notification to recruiter */
            $arr_data                         = [];
            $arr_data['user_id']              = $project_recruiter;
            $arr_data['project_id']           = $project_id;
            $arr_data['user_type']            = '4';
            $arr_data['url']                  = 'recruiter/projects/details/'.base64_encode($project_id);
            /*$arr_data['notification_text']  = "New Project Assigned.";*/
            $arr_data['notification_text_en'] = Lang::get('controller_translations.notification_new_project_assign',[],'en','en');
            $arr_data['notification_text_de'] = Lang::get('controller_translations.notification_new_project_assign',[],'de','en');
            $this->NotificationsModel->create($arr_data);
            /* end  notification to pre manager */
        }

        if($status)
        {
            $obj_client_data = $this->ProjectpostModel->select("project_name","client_user_id","project_handle_by","project_recruiter_user_id","project_description")
                                                    ->where('id',$project_id)
                                                    ->with("project_recruiter_info.user_details","client_details")
                                                    ->first();
            if(isset($obj_client_data) & $obj_client_data!= null){
             $client_data = [];
             $client_data = $obj_client_data->toArray();   
            }
                

               /* Send mail to Project manager */
               $project_name = config('app.project.name');
               //$mail_form    = get_site_email_address();   /* getting email address of admin from helper functions */
               $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
               
               $obj_project_info = $this->ProjectpostModel->where('id',$project_id)
                                                          ->with(['project_recruiter_info'=> function ($query) {
                                                                  $query->select('user_id','first_name','last_name');
                                                                },
                                                                'project_recruiter_details'=>function ($query_nxt) {
                                                                  $query_nxt->select('id','email','user_name');
                                                                }])
                                                          ->with(['client_info'=> function ($query) {
                                                                  $query->select('user_id','first_name','last_name');
                                                                },
                                                                'client_details'=>function ($query_nxt) {
                                                                  $query_nxt->select('id','email','user_name');
                                                                }])
                                                          ->first(['id','project_name','client_user_id','expert_user_id','project_recruiter_user_id','project_name','project_description']);

              $data = [];
              if($obj_project_info){
                $arr_project_info                 = $obj_project_info->toArray();

                /* get client name */
                $pm_first_name                    = isset($arr_project_info['project_recruiter_info']['first_name'])?$arr_project_info['project_recruiter_info']['first_name']:'';
                $pm_last_name                     = isset($arr_project_info['project_recruiter_info']['last_name'])?$arr_project_info['project_recruiter_info']['last_name']:'';
                /*$pm_name                        = $pm_first_name.' '.$pm_last_name;*/
                $pm_name                          = $pm_first_name;
                /*PM username*/
                $pm_user_name                     = isset($arr_project_info['project_recruiter_details']['user_name'])?$arr_project_info['project_recruiter_details']['user_name']:'';

                /* Getting admins information from helper function */
                $admin_data                       = get_admin_email_address();
                $data['project_name']             = isset($arr_project_info['project_name'])?$arr_project_info['project_name']:'';
                $data['project_description']      = isset($arr_project_info['project_description'])?$arr_project_info['project_description']:'';
                $data['project_manager_name']     = $pm_name;
                $data['project_manager_username'] = $pm_user_name;
                $data['admin_name']               = isset($admin_data['user_name'])?$admin_data['user_name']:'';
                $data['login_url']                = url('/redirection?redirect=PROJECT&id='.base64_encode($project_id));
                $data['email_id']                 = isset($arr_project_info['project_recruiter_details']['email'])?
                                                          $arr_project_info['project_recruiter_details']['email']:'';
                /* client information */
                $client_name = isset($arr_project_info['client_info']['first_name'])?$arr_project_info['client_info']['first_name']:'';
                $data['client_name'] = $client_name;
              }
              $email_to = isset($arr_project_info['project_recruiter_details']['email'])?$arr_project_info['project_recruiter_details']['email']:'';

              if($email_to!= "")
              {
                try{
                     $mail_status = $this->MailService->send_project_assigned_to_project_recruiter_email($data);
                    // Mail::send('front.email.project_assigned_to_project_recruiter', $data, function ($message) use ($email_to,$mail_form,$project_name) 
                    // {
                    //   $message->from($mail_form, $project_name);
                    //   $message->subject($project_name.': Recruiter Assigned.');
                    //   $message->to($email_to);
                    // });
                }
                catch(\Exception $e){
                Session::Flash('error','E-Mail not sent');
                }
              }
        
              /* Send email to client informing about project recruiter is assigned */
              $client_email = isset($arr_project_info['client_details']['email'])?$arr_project_info['client_details']['email']:'';
              if($client_email!= "")
              {
                try{
                     $mail_status = $this->MailService->send_project_assigned_to_project_recruiter_mail_to_client_email($data);
                    // Mail::send('front.email.project_assigned_to_project_recruiter_mail_to_client', $data, function ($message) use ($client_email,$mail_form,$project_name) {
                    //       $message->from($mail_form, $project_name);
                    //       $message->subject($project_name.': Recruiter Assigned.');
                    //       $message->to($client_email);
                    // });
                }
                catch(\Exception $e){
                Session::Flash('error', 'E-Mail not sent');
                }
              }
              
              //if(isset($project_manager) && isset($pre_project_manager) && $pre_project_manager != 0 && $pre_project_manager != $project_manager){
                /* send notification to client or previous manager if admin change project manager for a project
                   
                    /* send notification to client */
                        $arr_data['user_id']              = isset($arr_project_info['client_user_id'])?$arr_project_info['client_user_id']:'0';
                        $arr_data['user_type']            = '2';
                        $arr_data['url']                  = 'client/projects/details/'.base64_encode($project_id);
                        $arr_data['project_id']           = $project_id;
                        $arr_data['notification_text_en'] = Lang::get('controller_translations.new_project_recruiter_assign_to_project_by_admin',[],'en','en');
                        $arr_data['notification_text_de'] = Lang::get('controller_translations.new_project_recruiter_assign_to_project_by_admin',[],'de','en');
                        $this->NotificationsModel->create($arr_data); 
                    /* end  notification to client */

                    /* send notification to recruiter */
                        // $arr_data                         = [];
                        // $arr_data['user_id']              = $project_recruiter;
                        // $arr_data['project_id']           = $project_id;
                        // $arr_data['user_type']            = '4';
                        // $arr_data['url']                  = 'recruiter/projects/details/'.base64_encode($project_id);
                        // $arr_data['notification_text']  = "New Project Assigned.";
                        // $arr_data['notification_text_en'] = Lang::get('controller_translations.notification_new_project_assign',[],'en','en');
                        // $arr_data['notification_text_de'] = Lang::get('controller_translations.notification_new_project_assign',[],'de','en');
                        // $this->NotificationsModel->create($arr_data);
                    /* end  notification to pre manager */

                /* end notification to client or previous manager if admin change project manager for a project */
              //}
            /* Mail ends */

            Session::flash('success','Project details updated successfully.');
        } 
        else
        {
            Session::flash('error','Problem occured, while updating project details.');
        }
        return redirect()->back();
    }

    public function completed_project_notification($arr_data)
    {
      $this->NotificationsModel->create($arr_data);  
      return TRUE;
    }


     public function delete($enc_id = FALSE)
     {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while client deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Project deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while client deletion.');
        }

        return redirect()->back();
     }

    public function perform_delete($id)
    {
        if ($id) 
        {   
            /*$user= $this->UserModel->where('id',$id)->first();*/
            $project= $this->ProjectpostModel->where('id',$id)->first();

            if($project!=FALSE)
            {   
                /*$delete_user = $user->delete(); */
                return $project->delete();   
            }
        }
        return FALSE;
    }

    /*
    | multi_action: Following Fuctions for active ,deactive and delete for multiple records
    | auther :Bharat Khairnar    
    | 
    */ 
    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Session::flash('success','Projects(s) deleted successfully.');
            } 
            
        }
        return redirect()->back();
    }
      /*
      Auther : Bharat K.
    */
    public function notifications_seen($id)
    {   
      if($id && $id!="")
      {
        $notification_id =  base64_decode($id);
        $status = $this->NotificationsModel->where('id',$notification_id)->update(['is_seen'=>'1']);

        if($status)
        {
          $data['status'] = "success";
          echo json_encode($data);
        } 
        else
        {
          $data['status'] = "error";
          echo json_encode($data);
        }
      }  
    } 

}