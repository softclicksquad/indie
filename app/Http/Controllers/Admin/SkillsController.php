<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Common\Services\LanguageService;  
use App\Models\SkillsModel;  

use Validator;
use Session;
Use Sentinel;
use Excel;

class SkillsController extends Controller
{
    /*
        Auther : Sagar Sainkar
        Comments: controller for skills (project skill)        
    */

    public function __construct(SkillsModel $skill,LanguageService $langauge)
    {      
       $this->SkillsModel = $skill;
       $this->LanguageService = $langauge;
       $this->module_url_path = url(config('app.project.admin_panel_slug')."/skills");
    }

    /*
        Auther : Sagar Sainkar
        Comments: display skills
    */
	public function index()
    {

        $arr_lang   =  $this->LanguageService->get_all_language();  

        $obj_skills = $this->SkillsModel->orderBy('id','desc')->get();

        if($obj_skills != FALSE)
        {
            $arr_skills = $obj_skills->toArray();
        }

        $this->arr_view_data['arr_skills'] = $arr_skills;

        $this->arr_view_data['page_title'] = "Manage Skills";
        $this->arr_view_data['module_title'] = "Skills";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
         
        return view('admin.skills.index',$this->arr_view_data);
    }


    /*  
        Auther : Sagar Sainkar
        Comments: display view for Add new skill
    */

    public function create()
    {

        $this->arr_view_data['arr_lang'] = $this->LanguageService->get_all_language();
        $this->arr_view_data['page_title'] = "Create Skill";
        $this->arr_view_data['module_title'] = "Skills";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.skills.create',$this->arr_view_data);
    }

    /*  
        Auther : Sagar Sainkar
        Comments: Add new skill details
    */
    public function store(Request $request)
    {
        $form_data = array();

        $form_data = $request->all();
        
        //dd($form_data);
        $arr_rules['skill_name_en'] = "required";
        
        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
             return redirect()->back()->withErrors($validator)->withInput($request->all());
        }
    
        $form_data = $request->all();

        $arr_data = array();
        $arr_data['skill_slug'] = str_slug($form_data['skill_name_en']);
        $arr_data['is_active'] = 1;

        /* Check if skill already exists with given translation */
   		$does_exists = $this->SkillsModel->whereHas('translations',function($query) use($request)
             {
                  $query->where('locale', 'en')
                        ->where('skill_name',trim($request->input('skill_name_en')));
             })->count();
        
        if($does_exists)
        {
            Session::flash('error','Skill already exists.');            
            return redirect()->back();
        }
        

        $skill    = $this->SkillsModel->create($arr_data);

        $skill_id = $skill->id;

        /* Fetch All Languages*/
        $arr_lang =  $this->LanguageService->get_all_language();

        if(sizeof($arr_lang) > 0 && $skill_id)
        {
            foreach ($arr_lang as $lang) 
            {            
                $arr_data     = array();
                $skill_name   = 'skill_name_'.$lang['locale'];

                if( isset($form_data[$skill_name]) && $form_data[$skill_name] != '')
                { 
                    $translation = $skill->translateOrNew($lang['locale']);

                    $translation->skill_name       = ucfirst($form_data[$skill_name]);
                    $translation->skill_id       = $skill_id;
                    $translation->save();

                    Session::flash('success','Skill created successfully.');
                }

            }//foreach

        } //if
        else
        {
            Session::flash('error','Problem occured, while creating skill.');
            
        }

        return redirect()->back();
    }


    /*  
        Auther : Sagar Sainkar
        Comments: display view for edit skill
    */

    public function edit($enc_id)
    {
        $id = base64_decode($enc_id);

        $arr_lang = $this->LanguageService->get_all_language();      

        $obj_skill = $this->SkillsModel->where('id', $id)->with(['translations'])->first();

        $arr_skill = [];

        if($obj_skill)
        {
           $arr_skill = $obj_skill->toArray(); 
           /* Arrange Locale Wise */
           $arr_skill['translations'] = $this->arrange_locale_wise($arr_skill['translations']);
        }

        $this->arr_view_data['enc_id'] = $enc_id;
        $this->arr_view_data['arr_lang'] = $this->LanguageService->get_all_language();
        $this->arr_view_data['arr_skill'] = $arr_skill;
        $this->arr_view_data['page_title'] = "Edit Skill";
        $this->arr_view_data['module_title'] = "Skills";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.skills.edit',$this->arr_view_data);  

    }


    /*  
        Auther : Sagar Sainkar
        Comments: update skill details
    */
    public function update(Request $request, $enc_id)
    {
        $skill_id = base64_decode($enc_id);
        $arr_rules = array();
        $status = FALSE;

      	$arr_rules['skill_name_en']     = "required";        
        
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = array();
        $form_data = $request->all(); 

         /* Get All Active Languages */ 
        $arr_lang = $this->LanguageService->get_all_language();

        $obj_skill = $this->SkillsModel->where('id',$skill_id)->first();

        
         /* Insert Multi Lang Fields */

        if(sizeof($arr_lang) > 0 && $obj_skill)
        { 
            foreach($arr_lang as $i => $lang)
            {
                $translate_data_ary = array();
                $skill_name   = 'skill_name_'.$lang['locale'];

                if(isset($form_data[$skill_name]) && $form_data[$skill_name]!="")
                {
                	//update skill slug
                	$arr_data['skill_slug'] = str_slug($form_data['skill_name_en']);
        			$obj_skill->update($arr_data);

                    /* Get Existing Language Entry and update it */
                    $translation = $obj_skill->getTranslation($lang['locale']);    
                    if($translation)
                    {
                       	$translation->skill_name       =  ucfirst($form_data['skill_name_'.$lang['locale']]);
                        $status = $translation->save();
                    }  
                    else
                    {
                        /* Create New Language Entry  */
                        $translation     = $obj_skill->getNewTranslation($lang['locale']);
                        $translation->skill_id   =  $skill_id;
                        $translation->skill_name =  ucfirst($form_data['skill_name_'.$lang['locale']]);
                        $status = $translation->save();
                    } 
                }   
            }
            
        }

        if ($status) 
        {
            Session::flash('success','Skill updated successfully.');    
        }
        else
        {
            Session::flash('error','Error while updating skill.');       
        }
        
        return redirect()->back();
    }

    /*
    | Following Fuctions for active ,deactive and delete
    | auther :Sagar Sainkar    
    | 
    */ 

    public function activate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while skill activation.');
            return redirect()->back();
        }

        if($this->perform_activate(base64_decode($enc_id)))
        {
            Session::flash('success','Skill activated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while skill activation.');
        }

        return redirect()->back();
    }

    public function deactivate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
        	Session::flash('error','Problem occured while skill deactivation.');
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            Session::flash('success','Skill deactivated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while skill deactivation.');
        }

        return redirect()->back();
    }

    public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
        	Session::flash('error','Problem occured while skill deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Skill deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while skill deletion.');
        }

        return redirect()->back();
    }


    public function perform_activate($id)
    {
        if ($id) 
        {
            $skill = $this->SkillsModel->where('id',$id)->first();
            if($skill)
            {
                return $skill->update(['is_active'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_deactivate($id)
    {
        if ($id) 
        {
            $skill = $this->SkillsModel->where('id',$id)->first();
            if($skill)
            {
                return $skill->update(['is_active'=>0]);
            }
        }

        return FALSE;
    }

    public function perform_delete($id)
    {
        if ($id) 
        {
            $skill= $this->SkillsModel->where('id',$id)->first();
            if($skill)
            {
                return $skill->delete();
            }
        }
        return FALSE;
    }
   

     /*
    | multi_action: Following Fuctions for active ,deactive and delete for multiple records
    | auther :Sagar Sainkar    
    | 
    */ 
    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Session::flash('success','Skill(s) deleted successfully.');
            } 
            elseif($multi_action=="activate")
            {
               $this->perform_activate(base64_decode($record_id)); 
               Session::flash('success','Skill(s) activated successfully');               
            }
            elseif($multi_action=="deactivate")
            {
               $this->perform_deactivate(base64_decode($record_id));    
               Session::flash('success','Skill(s) blocked successfully.');
            }
        }

        return redirect()->back();
    }

    public function arrange_locale_wise(array $arr_data)
    {
        if(isset($arr_data) && sizeof($arr_data)>0)
        {
            foreach ($arr_data as $key => $data)
            {
                $arr_tmp = $data;
                unset($arr_data[$key]);
                $arr_data[$data['locale']] = $data;                    
            }

            return $arr_data;
        }
        else
        {
            return [];
        }
    }

     /* export: export skills and Generate CSV file */

    public function export_skills()
    {
        $obj_skills = FALSE;
        $arr_skills = array();
        $export_skills = array();

        $obj_skills = $this->SkillsModel->with(['translations'])->get();

        if($obj_skills != FALSE)
        {
            $arr_skills = $obj_skills->toArray();

            if(isset($arr_skills) && sizeof($arr_skills)>0)
            {  
                foreach ($arr_skills as $key => $value) 
                {
                    $arr_skills[$key]['translations'] = $this->arrange_locale_wise($value['translations']);
                }
            }
            
        }

        if(isset($arr_skills) && sizeof($arr_skills)>0)
        {
            foreach ($arr_skills as $key => $skill) 
            {
               
               $export_skills[$key]['Skill Name English']=isset($skill['translations']['en']['skill_name'])?$skill['translations']['en']['skill_name']:'';
               $export_skills[$key]['Skill Title German']=isset($skill['translations']['de']['skill_name'])?$skill['translations']['de']['skill_name']:'';
               $export_skills[$key]['Skill_slug']=isset($skill['skill_slug'])?$skill['skill_slug']:'';
               $export_skills[$key]['Status']=isset($skill['is_active']) && $skill['is_active']?'Active':'Inactive';
            }
        }

        $data = $export_skills;
        $type = 'CSV';

        return Excel::create('Skills', function($excel) use ($data) {

             // Set the title
            $excel->setTitle('Skills Backup');

            // Chain the setters
            $excel->setCreator('VirtualHomeConcept')
                    ->setCompany('VirtualHomeConcept');

            // Call them separately
            $excel->setDescription('Skills Backup');

            $excel->sheet('Skills', function($sheet) use ($data)
            {
                $sheet->fromArray($data);

            });


        })->download($type);
    }

}