<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Common\Services\LanguageService;  
use App\Models\CurrencyModel;  
use App\Models\DepositeCurrencyModel;  

use Validator;
use Session;
Use Sentinel;
use Excel;

class DepositeCurrencyController extends Controller
{
     
    public $CurrencyModel; 
    
    public function __construct(CurrencyModel $currency,LanguageService $langauge)
    {      
       $this->CurrencyModel = $currency;
       $this->LanguageService = $langauge;
       $this->DepositeCurrencyModel = new DepositeCurrencyModel;
       $this->module_url_path = url(config('app.project.admin_panel_slug')."/deposite_currency");

    }
    
    public function index()
    {

        $arr_currency = [];
        $obj_currency = $this->CurrencyModel->with('deposite')->where('is_active','1')->get();

        if($obj_currency != FALSE)
        {
            $arr_currency = $obj_currency->toArray();
        }
        //dd($arr_currency);
        $this->arr_view_data['arr_currency'] = $arr_currency;

        $this->arr_view_data['page_title'] = "Manage Deposite Currency";
        $this->arr_view_data['module_title'] = "Deposite Currency";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.deposite_currency.index',$this->arr_view_data);
    }

    public function store(Request $request)
    {
        $form_data = array();
        $form_data = $request->all();

        $arr_rules['currency_id']       = "required";
        $arr_rules['min_amount']        = "required";
        $arr_rules['max_amount']        = "required";
        $arr_rules['min_amount_charge'] = "required";
        $arr_rules['max_amount_charge'] = "required";

        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
             return redirect()->back()->withErrors($validator)->withInput($request->all());
        }
        
        $delete_old = DepositeCurrencyModel::truncate();
        $form_data = $request->all();
        foreach ($form_data['currency_id'] as $key => $value) 
        {

            $arr_data['currency_id']       = $form_data['currency_id'][$key];
            $arr_data['min_amount']        = $form_data['min_amount'][$key];
            $arr_data['max_amount']        = $form_data['max_amount'][$key];
            $arr_data['min_amount_charge'] = $form_data['min_amount_charge'][$key];
            $arr_data['max_amount_charge'] = $form_data['max_amount_charge'][$key];
            
            $obj_currency    = $this->DepositeCurrencyModel->create($arr_data);
        }

        if($obj_currency)
        {
            Session::flash('success','Deposite Currency created successfully');
        }
        else
        {
            Session::flash('error','Something went wrong');
        }
            return redirect()->back();   
    }
    
}