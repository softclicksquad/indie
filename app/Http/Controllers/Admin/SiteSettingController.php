<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\SiteSettingModel;
use Validator;
use Session;
use Input;
 
class SiteSettingController extends Controller
{
    private $SiteSettingModel; 
    
    public function __construct(SiteSettingModel $siteSetting)
    {
        $this->SiteSettingModel = $siteSetting;
        $this->arr_view_data    = [];
        $this->module_url_path  = url(config('app.project.admin_panel_slug')."/site_settings");
    }

    /*
    | Index : Display site settings
    | auther :Sagar Sainkar
    | Date : 
    */ 
    public function index()
    {
        $arr_site_settings = array();   

        $obj_site_settings =  $this->SiteSettingModel->where('site_settting_id',1)->first();

        if($obj_site_settings != FALSE)
        {
            $arr_site_settings = $obj_site_settings->toArray();    
        }

        $this->arr_view_data['arr_site_settings'] = $arr_site_settings;
        $this->arr_view_data['page_title']        = "Site Settings";
        $this->arr_view_data['module_title']      = "Site Settings";
        $this->arr_view_data['module_url_path']   = $this->module_url_path;        
        return view('admin.site_setting.index',$this->arr_view_data);
    }
 

     /*
    | update : update site settings
    | auther :Sagar Sainkar
    | Date : 
    */ 

    public function update(Request $request, $enc_id)
    {
        $id = base64_decode($enc_id);  

        $arr_rules = array();

        $arr_data['site_name']                          = "required";
        $arr_rules['site_email_address']                = "email|required";
        $arr_rules['site_contact_number']               = "required";
        $arr_rules['site_address']                      = "required";
        $arr_rules['fb_url']                            = "required|active_url";
        $arr_rules['twitter_url']                       = "required|active_url";
        $arr_rules['linkedin_url']                      = "required|active_url";
        $arr_rules['google_plus_url']                   = "required|active_url";
        $arr_rules['site_status']                       = "required";
        $arr_rules['mailchimp_api_key']                 = "required";
        $arr_rules['mailchimp_list_id']                 = "required";
        $arr_rules['wesite_project_manager_commission'] = "required|numeric|max:100";
        $arr_rules['website_pm_initial_cost']           = "required|numeric";
        $arr_rules['nda_price']                         = "required|numeric";
        $arr_rules['urgent_price']                      = "required|numeric";
        $arr_rules['private_price']                     = "required|numeric";
        $arr_rules['recruiter_price']                   = "required|numeric";
        $arr_rules['project_cost']                      = "required|numeric";
        $arr_rules['default_currency']                  = "required";
        $arr_rules['default_currency_code']             = "required";
        
        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {       
            return back()->withErrors($validator)->withInput();  
        }

        $form_data = array();
        $form_data = $request->all();  

        if (isset($form_data['website_pm_initial_cost']) && $form_data['website_pm_initial_cost']<1) 
        {
            Session::flash('error','Please check Website Project Manager Fix Price not less than one.'); 
            return back()->withErrors($validator)->withInput();  
        }

        $arr_data['site_name']                         = $form_data['site_name'];
        $arr_data['site_address']                      = $form_data['site_address'];
        $arr_data['site_contact_number']               = $form_data['site_contact_number'];
        $arr_data['meta_desc']                         = $form_data['meta_desc'];
        $arr_data['meta_keyword']                      = $form_data['meta_keyword'];
        $arr_data['site_email_address']                = $form_data['site_email_address'];
        $arr_data['fb_url']                            = $form_data['fb_url'];
        $arr_data['twitter_url']                       = $form_data['twitter_url'];
        $arr_data['linkedin_url']                      = $form_data['linkedin_url'];
        $arr_data['google_plus_url']                   = $form_data['google_plus_url'];
        $arr_data['site_status']                       = $form_data['site_status'];
        $arr_data['mailchimp_api_key']                 = $form_data['mailchimp_api_key'];
        $arr_data['mailchimp_list_id']                 = $form_data['mailchimp_list_id'];
        $arr_data['wesite_project_manager_commission'] = $form_data['wesite_project_manager_commission'];
        $arr_data['website_pm_initial_cost']           = $form_data['website_pm_initial_cost'];
        $arr_data['nda_price']                         = $form_data['nda_price'];
        $arr_data['urgent_price']                      = $form_data['urgent_price'];
        $arr_data['private_price']                     = $form_data['private_price'];
        $arr_data['recruiter_price']                   = $form_data['recruiter_price'];
        $arr_data['project_cost']                      = $form_data['project_cost'];
        $arr_data['default_currency']                  = $form_data['default_currency'];
        $arr_data['default_currency_code']             = $form_data['default_currency_code'];
        
        $update=$this->SiteSettingModel->where('site_settting_id',$id)->first()->update($arr_data);        
        
        if($update)
        {
            Session::flash('success','WebSite Setting Updated Successfully.'); 
        }
        else
        {
            Session::flash('error','Problem Occured, While Updating WebSite Setting.');  
        } 
      
        return back()->withInput();;
    }
 

}
