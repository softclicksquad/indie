<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ProjectpostModel;
use App\Models\MilestoneReleaseModel;
use App\Models\MilestoneRefundRequests;
use App\Common\Services\WalletService;
use App\Models\UserModel;
use App\Models\MilestonesModel;
use App\Models\TransactionsModel;
use App\Models\NotificationsModel;
use App\Models\CurrencyModel;
use App\Models\UserWalletModel;

use Sentinel;
use Validator;
use Session;
use Lang;
use Mail;
class WalletController extends Controller
{
    public $arr_view_data;
    public function __construct(  
                                ProjectpostModel $projectpost,
                                MilestoneReleaseModel  $milestone,
                                WalletService $WalletService, 
                                UserModel $UserModel,
                                MilestonesModel $MilestonesModel,
                                TransactionsModel $TransactionsModel,
                                MilestoneRefundRequests $MilestoneRefundRequests,
                                NotificationsModel $notifications
                               )
    {
      if(! $user = Sentinel::check()) {
        return redirect('/login');
      }
      $this->user                     = $user;
      $this->user_id                  = $user->id;
      $this->arr_view_data            = [];
      $this->ProjectpostModel         = $projectpost;
      $this->MilestoneReleaseModel    = $milestone;
      $this->WalletService            = $WalletService;
      $this->UserModel                = $UserModel;
      $this->MilestoneRefundRequests  = $MilestoneRefundRequests;
      $this->NotificationsModel       = $notifications;
      $this->TransactionsModel        = $TransactionsModel;
      $this->MilestonesModel          = $MilestonesModel;
      $this->CurrencyModel            = new CurrencyModel;
      $this->UserWalletModel          = new UserWalletModel;
      $this->module_url_path          = url(config('app.project.admin_panel_slug')."/wallet");

      if(isset($this->user)){
          $logged_user                 = $this->user->toArray();  
          if($logged_user['mp_wallet_created'] == 'Yes'){
            $this->mp_user_id          = $logged_user['mp_user_id'];
            $this->mp_wallet_id        = $logged_user['mp_wallet_id'];
            $this->mp_wallet_created   = $logged_user['mp_wallet_created'];
          } else {
              $this->mp_user_id          = '';
              $this->mp_wallet_id        = '';
              $this->mp_wallet_created   = 'No';
          }
        } else {
          $this->mp_user_id          = '';
          $this->mp_wallet_id        = '';
          $this->mp_wallet_created   = 'No';
        }
    }
    public function wallet(){
      dd('here');
    }
    public function archexpert_wallet()
    { // technical wallet
      $mango_user_detail                = [];
      $mangopay_wallet_details          = [];
      $mangopay_kyc_details             = [];
      $mangopay_bank_details            = [];
      $mangopay_transaction_details     = [];
      $get_transaction_pagi_links_count = 0;
      if(isset($this->user))
      {
        $logged_user                         = $this->user->toArray();  
        if($logged_user['mp_wallet_created'] == 'Yes'){
          
          
          $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)->get();
      
          if($obj_data)
          {
            $arr_data = $obj_data->toArray();
          }
          // dd($arr_data);
          $mango_data['mp_user_id']          = isset($arr_data[0]['mp_user_id'])?$arr_data[0]['mp_user_id']:'';
          //$mango_data['mp_wallet_id']        = $arr_data['mp_wallet_id'];
          $mango_data['mp_wallet_created']   = $logged_user['mp_wallet_created'];
          $mango_user_detail                 = $this->WalletService->get_user_details($mango_data['mp_user_id']);
          //dd($mango_user_detail);
          if(isset($mango_user_detail))
          {
            // get mangopay wallet balance
              foreach ($arr_data as $key => $value) {
                $mango_data['mp_wallet_id']= $value['mp_wallet_id'];
                $get_mangopay_wallet_details[] = $this->WalletService->get_wallet_details($mango_data['mp_wallet_id']);  
              }
              if(isset($get_mangopay_wallet_details)){
                $mangopay_wallet           = $get_mangopay_wallet_details;
                $mangopay_wallet_details   = $mangopay_wallet;
              }
            // end get mangopay wallet balance 

            // get mangopay kyc details
            $mangopay_kyc_details = $this->WalletService->get_kyc_details($mango_data['mp_user_id']);
            // end get mangopay kyc details

            // get mangopay bank details;
            $mangopay_bank_details = $this->WalletService->get_banks_details($mango_data['mp_user_id']);
            // end get mangopay bank details;

            // get mangopay transaction details;
            $get_transaction_pagi_links_count = 1;//$this->WalletService->get_wallet_transaction_pagi_links_count($mp_wallet_id);
            if(isset($_REQUEST['page_cnt']) && $_REQUEST['page_cnt'] !=""){
            foreach ($arr_data as $key => $value) {
              $mp_wallet_id = $value['mp_wallet_id'];
              $mangopay_transaction_details[] = $this->WalletService->get_wallet_transaction_details($mp_wallet_id,$_REQUEST['page_cnt']);
            }
            } else {
              foreach ($arr_data as $key => $value) {
              $mp_wallet_id= $value['mp_wallet_id'];
            $mangopay_transaction_details[]= $this->WalletService->get_wallet_transaction_details($mp_wallet_id,$get_transaction_pagi_links_count);
            }
            }
            // end get mangopay transaction details;            
            $this->arr_view_data['mp_wallet_created']        = 'Yes';  
          }    
        } else {
          $this->arr_view_data['mp_wallet_created']          = 'No'; 
        }
      }
      $this->arr_view_data['page_title']                              = 'Archexpert Wallet';
      $this->arr_view_data['mango_user_detail']                       = $mango_user_detail;
      $this->arr_view_data['mangopay_wallet_details']                 = $mangopay_wallet_details;
      $this->arr_view_data['mangopay_kyc_details']                    = $mangopay_kyc_details;
      $this->arr_view_data['mangopay_bank_details']                   = $mangopay_bank_details;
      $this->arr_view_data['mangopay_transaction_details']            = $mangopay_transaction_details;
      $this->arr_view_data['mangopay_transaction_pagi_links_count']   = $get_transaction_pagi_links_count;
      $this->arr_view_data['module_url_path'] = $this->module_url_path;
      //dd($this->arr_view_data);
      return view('admin/wallet/archexpert_wallet_dashboard',$this->arr_view_data);
    }
    public function fee_wallet(){
      $get_client_ewallet_details        = $this->WalletService->get_client_details();
      $get_client_wallet_details         = $this->WalletService->get_client_wallet_details();


      $this->arr_view_data['page_title']             = 'Fee Wallet';
      $this->arr_view_data['client_ewallet_details'] = $get_client_ewallet_details;
      $this->arr_view_data['client_wallet_details']  = $get_client_wallet_details;
      $this->arr_view_data['module_url_path']        = $this->module_url_path;
      return view('admin/wallet/fee_wallet_dashboard',$this->arr_view_data);
    }
    public function create_user(){
      if(isset($this->user)){
        $arr_currency = [];
        $obj_currency = $this->CurrencyModel->select('id','currency_code')->get();
        if($obj_currency)
        {
          $arr_currency = $obj_currency->toArray();
        }

        $logged_user  = $this->user->toArray();  
        $first_name   = "-";
        $last_name    = "-";
        $email        = "-";
        if(isset($logged_user['role_info']['first_name']) && $logged_user['role_info']['first_name'] !=""){
          $first_name = $logged_user['role_info']['first_name'];
        }
        if(isset($logged_user['role_info']['last_name']) && $logged_user['role_info']['last_name'] !=""){
          $last_name = $logged_user['role_info']['last_name'];
        }
        if(isset($logged_user['email']) && $logged_user['email'] !=""){
          $email = $logged_user['email'];
        }
        try{
        $timezoneinfo = file_get_contents('http://ip-api.com/json/' . $_SERVER['REMOTE_ADDR']);
        }catch(\Exception $e){
        $timezoneinfo = file_get_contents('http://ip-api.com/json');  
        } 
        $mp_user_data = [];
        //Create account in mangopay and create wallet Start.......
          $mp_user_data['FirstName']          = $first_name;
          $mp_user_data['LastName']           = $last_name;
          $mp_user_data['Email']              = $email;
          $mp_user_data['CountryOfResidence'] = isset($timezoneinfo->countryCode)?$timezoneinfo->countryCode:"IN";
          $mp_user_data['Nationality']        = isset($timezoneinfo->countryCode)?$timezoneinfo->countryCode:"IN";
          $mp_user_data['Wallet_name']        = 'Archexpert Wallet';
          // Register user on MangoPay as well

          $Mangopay_create_user               =  $this->WalletService->create_user_with_all_currency_wallet($mp_user_data,$arr_currency,$this->user_id);
          
          if(1)//($Mangopay_create_user)
          {
            // $arr_data['mp_user_id']           = isset($Mangopay_create_user['createdUserId'])?$Mangopay_create_user['createdUserId']:"";
            // $arr_data['mp_wallet_id']         = isset($Mangopay_create_user['AppWalletId'])?$Mangopay_create_user['AppWalletId']:"";
            // $arr_data['mp_wallet_created']    = "Yes";
            // $update_mangopya_details          = $this->UserModel->where('id',$this->user->id)->update($arr_data); 
            Session::flash('success','Archexpert fee wallet account has been created successfully');
            return redirect()->back();
          } else { 
            Session::flash('error','Something went wrong please try again later');
            return redirect()->back();
          } 
        //Mangopay Settings End.......  
      } else {
        Session::flash('error','Something went wrong please try again later');
        return redirect()->back();
      }
    }
    public function create_wallet_using_mp_owner_id($mp_owners_id = false){
      if($mp_owners_id == false){
        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
        return redirect()->back();
      }
      $desciption = config('app.project.name')." wallet";
      $AppWallet = $this->WalletService->create_wallet(base64_decode($mp_owners_id),$desciption);
      $arr_data['mp_wallet_id']      = isset($AppWallet->Id)?$AppWallet->Id:"";
      $arr_data['mp_wallet_created'] = "Yes";
      if($AppWallet){
        $update_mangopya_details = $this->UserModel->where('id',$this->user->id)->update($arr_data); 
        Session::flash('success',trans('common/wallet/text.text_archexpert_wallet_account_has_been_created_successfully'));
      } else {
        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
      }
      return redirect()->back();
    }
    public function add_bank(Request $request){
      $transaction_inp               = [];
      $bank_type    = $request->input('bank_type');

      if($bank_type == 'IBAN'){
        $validator  = Validator::make($request->all(),[
            'FirstName'             => 'required',
            'LastName'              => 'required',
            'Country'               => 'required',
            'City'                  => 'required',
            'Region'                => 'required',
            'Address'               => 'required',
            'PostalCode'            => 'required',
            'IBAN'                  => 'required',
            'BIC'                   => 'required'
        ]);
      } else if($bank_type == 'GB'){
        $validator = Validator::make($request->all(),[
            'FirstName'             => 'required',
            'LastName'              => 'required',
            'City'                  => 'required',
            'PostalCode'            => 'required',
            'Region'                => 'required',
            'Country'               => 'required',
            'gbSortCode'            => 'required',
            'Address'               => 'required',
            'gbAccountNumber'       => 'required'
        ]);
      } else if($bank_type == 'US'){
        $validator = Validator::make($request->all(),[
            'FirstName'             => 'required',
            'LastName'              => 'required',
            'City'                  => 'required',
            'PostalCode'            => 'required',
            'Region'                => 'required',
            'Country'               => 'required',
            'Address'               => 'required',
            'usAccountNumber'       => 'required',
            'usDepositAccountType'  => 'required',
            'usABA'                 => 'required'
        ]);
      } else if($bank_type == 'CA'){
        $validator = Validator::make($request->all(),[
            'FirstName'            => 'required',
            'LastName'             => 'required',
            'City'                 => 'required',
            'PostalCode'           => 'required',
            'Region'               => 'required',
            'Country'              => 'required',
            'Address'              => 'required',
            'caAccountNumber'      => 'required',
            'caBranchCode'         => 'required',
            'caBankName'           => 'required',
            'caInstitutionNumber'  => 'required'
        ]);
      } else if($bank_type == 'OTHER'){
        $validator = Validator::make($request->all(),[
            'FirstName'            => 'required',
            'LastName'             => 'required',
            'PostalCode'           => 'required',
            'Address'              => 'required',
            'Region'               => 'required',
            'City'                 => 'required',
            'Country'              => 'required',
            'otherAccountNumber'   => 'required',
            'otherBIC'             => 'required'
        ]);
      } 
      if($validator->fails()){
        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
        return redirect('/admin/wallet/archexpert');
      }
      $transaction_inp['UserId']     = $this->mp_user_id;
      $transaction_inp['FirstName']  = $request->input('FirstName');
      $transaction_inp['LastName']   = $request->input('LastName');
      $transaction_inp['Country']    = $request->input('Country');
      $transaction_inp['PostalCode'] = $request->input('PostalCode');
      $transaction_inp['City']       = $request->input('City');
      $transaction_inp['bank_type']  = $request->input('bank_type');
      $transaction_inp['Address']    = $request->input('Address');
      $transaction_inp['Region']     = $request->input('Region');

      if($bank_type == 'IBAN'){
        $transaction_inp['IBAN']                 = $request->input('IBAN');
        $transaction_inp['BIC']                  = $request->input('BIC');
      } else if($bank_type == 'GB'){
        $transaction_inp['gbSortCode']           = $request->input('gbSortCode');
        $transaction_inp['gbAccountNumber']      = $request->input('gbAccountNumber');       
      } else if($bank_type == 'US'){
        $transaction_inp['usAccountNumber']      = $request->input('usAccountNumber');
        $transaction_inp['usDepositAccountType'] = $request->input('usDepositAccountType');
        $transaction_inp['usABA']                = $request->input('usABA');       
      } else if($bank_type == 'CA'){
        $transaction_inp['caAccountNumber']      = $request->input('caAccountNumber');
        $transaction_inp['caBranchCode']         = $request->input('caBranchCode');
        $transaction_inp['caBankName']           = $request->input('caBankName');
        $transaction_inp['caInstitutionNumber']  = $request->input('caInstitutionNumber');       
      } else if($bank_type == 'OTHER'){
        $transaction_inp['otherAccountNumber']   = $request->input('otherAccountNumber');
        $transaction_inp['otherBIC']             = $request->input('otherBIC');     
      }
      $mp_add_bank = $this->WalletService->createBankAccount($transaction_inp);
      if(isset($mp_add_bank) && $mp_add_bank == 'true'){
        Session::flash('success',trans('common/wallet/text.text_bank_has_been_successfully_registered'));
        return redirect('/admin/wallet/archexpert');
      } else{
         if(isset($mp_add_bank) && $mp_add_bank != ""){
          Session::flash('error',$mp_add_bank);
         } else {
         Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
         }
         return redirect('/admin/wallet/archexpert');
      }
    }
    public function make_cashout_in_bank(Request $request){
        $validator = Validator::make($request->all(),[
          'act_cashout_amount' => 'required',
          'walletId'           => 'required',
          'bankId'             => 'required',
          'cashout_amount'     => 'required'
        ]);
        if($validator->fails()){
          Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
          return redirect('/admin/wallet/archexpert');
        }
        $act_cashout_amount = $request->input('act_cashout_amount');
        $walletId           = $request->input('walletId');
        $bankId             = $request->input('bankId');
        $tag                = $request->input('Tag',null);
        //  check wallet balance 
        $get_mangopay_wallet_details = $this->WalletService->get_wallet_details($walletId);
        if(isset($get_mangopay_wallet_details->Balance->Amount) && $get_mangopay_wallet_details->Balance->Amount !=""){
            $wallet_balance = $get_mangopay_wallet_details->Balance->Amount/100;
            if($act_cashout_amount > $wallet_balance) {
              Session::flash('error',trans('common/wallet/text.text_wallet_balance_is_not_sufficient'));
              return redirect('/admin/wallet/archexpert');
            } else {
              $transaction_inp                      = [];
              $transaction_inp['UserId']            = $this->mp_user_id;
              $transaction_inp['bankId']            = $bankId;
              $transaction_inp['tag']               = $tag;
              $transaction_inp['commission_amount'] = '0';
              $transaction_inp['walletId']          = $walletId;
              $transaction_inp['total_pay']         = $act_cashout_amount;
              $payOutBankWire  = $this->WalletService->payOutBankWire($transaction_inp);
              if(isset($payOutBankWire->Id) && $payOutBankWire->Id != ""){
                Session::flash('success', trans('common/wallet/text.text_your_cashout_has_been_successfully_done_You_can_check_your_transaction_status_using')." ( ".$payOutBankWire->Id." ) ".trans('common/wallet/text.text_transaction_id'));
                return redirect('/admin/wallet/archexpert');
              } else {
                Session::flash('error',$payOutBankWire);
                return redirect('/admin/wallet/archexpert');
              }
            }
        } else {
          Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
          return redirect('/admin/wallet/archexpert');
        }
    }
    public function upload_kyc_docs(Request $request){
      $transaction_inp                          = [];
      $validator = Validator::make($request->all(),[
          'kyc_upload_documents_natural_types'  => 'required',
          'Kyc_doc'                             => 'required'
      ]);
      if($validator->fails()){
        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
        return redirect('/admin/wallet/archexpert');
      }
      $transaction_inp['mp_user_id']       = $this->mp_user_id;
      $transaction_inp['KycDocumentType']  = $request->input('kyc_upload_documents_natural_types');
      $transaction_inp['KycDocument']      = base64_encode(file_get_contents($request->file('Kyc_doc')));
      $transaction_inp['KycDocumentPath']  = $request->file('Kyc_doc')->getrealPath();
      $transaction_inp['KycDocumentName']  = $request->file('Kyc_doc')->getClientOriginalName();
      $mp_upload_kyc_doc = $this->WalletService->submit_kyc_docs($transaction_inp);
      if(isset($mp_upload_kyc_doc) && $mp_upload_kyc_doc == true){
        Session::flash('success',trans('common/wallet/text.text_your_kyc_details_submited_successfully'));
        return redirect('/admin/wallet/archexpert');
      } else {
        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
        return redirect('/admin/wallet/archexpert');
      }
    }
    public function transfer_refund(Request $request) 
    { 
      $outputdata                         = '';
      $arr_rules                          = [];
      $arr_rules['AuthorId']              = "required";
      $arr_rules['refund_amount']         = "required";
      $arr_rules['TransactionId']         = "required";
      $arr_rules['refundid']              = "required";
      $arr_rules['milestoneid']           = "required";
      $arr_rules['invoice_id']            = "required";
      $arr_rules['project_id']            = "required";
      $arr_rules['client_user_id']        = "required";
      $validator = Validator::make($request->all(),$arr_rules);
      if($validator->fails()){
          $outputdata = '<div class="alert alert-danger alert-dismissible">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
               Something went wrong, Please try again later '.'<a href="" style="cursor:pointer;"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</a>  
          </div>';
         echo $outputdata;  
         exit;
      }
      $transaction_inp                          = [];
      $transaction_inp['AuthorId']              = $request->input('AuthorId');
      $transaction_inp['mp_transaction_status'] = $request->input('mp_transaction_status',null);
      $transaction_inp['refund_amount']         = $request->input('refund_amount');
      $transaction_inp['mp_transaction_type']   = $request->input('mp_transaction_type',null);
      $transaction_inp['TransactionId']         = $request->input('TransactionId');
      $transfer_refund = $this->WalletService->transferRefund($transaction_inp);
      if(isset($transfer_refund->ResultMessage) && $transfer_refund->ResultMessage != ""){
        $refund_data = [];
        $refund_data['InitialTransactionId']    = $transfer_refund->InitialTransactionId;          
        $refund_data['AuthorId']                = $transfer_refund->AuthorId;          
        if($transfer_refund->Status != 'FAILED'){
          $refund_data['is_refunded']           = 'yes';  
          $this->MilestonesModel->where('id',$request->input('milestoneid'))->update(['status'=>3,'is_milestone'=>3]); // refunded  
          $this->TransactionsModel->where('invoice_id',$request->input('invoice_id'))->update(['payment_status'=>4]); // refunded    
        }
        $this->MilestoneRefundRequests->where('id',$request->input('refundid'))->update($refund_data); 
        if($transfer_refund->Status == 'FAILED'){ 
          $outputdata = '<div class="alert alert-danger alert-dismissible">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
               '.$transfer_refund->ResultMessage.' '.'<a href="" style="cursor:pointer;"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</a>  
          </div>'; 
        } else {

          $obj_project_info = $this->ProjectpostModel->where('id',$request->input('project_id'))
                                    ->with(['client_info'=> function ($query) {
                                          $query->select('user_id','first_name','last_name');
                                        },
                                        'client_details'=>function ($query_nxt) {
                                          $query_nxt->select('id','email');
                                        }])
                                    ->with(['expert_info'=> function ($query_nxt_1) {
                                        $query_nxt_1->select('user_id','first_name','last_name');
                                    }])
                                   ->first(['id','project_name','client_user_id','expert_user_id']);

          $posted_project_name = isset($obj_project_info->project_name)?$obj_project_info->project_name:'';

          /* Create Notification for client */      
          $arr_data                         =  [];
          $arr_data['user_id']              =  $request->input('client_user_id');
          $arr_data['user_type']            =  '2';
          $arr_data['url']                  =  'client/projects/milestones/'.base64_encode($request->input('project_id'));
          $arr_data['project_id']           =  $request->input('project_id');
          $arr_data['notification_text_en'] =  $posted_project_name . ' - ' . Lang::get('common/wallet/text.text_milestone_refunded_successfully',[],'en','en');
          $arr_data['notification_text_de'] =  $posted_project_name . ' - ' . Lang::get('common/wallet/text.text_milestone_refunded_successfully',[],'de','en');
          $this->NotificationsModel->create($arr_data);  

          // send email to client
          $project_name     = config('app.project.name');
          $obj_milestone    = [];
          $obj_milestone    = $this->MilestonesModel->where('id',$request->input('milestoneid'))->first();
          if($obj_milestone){
             $obj_milestone = $obj_milestone->toArray(); 
          }
          //$mail_form        = get_site_email_address();   /* getting email address of admin from helper functions */
          $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
          
          $data                          = [];
          if($obj_project_info){
            $arr_project_info            = $obj_project_info->toArray();
            /* get client name */
            $client_first_name           = isset($arr_project_info['client_info']['first_name'])?$arr_project_info['client_info']['first_name']:'';
            $client_last_name            = isset($arr_project_info['client_info']['last_name'])?$arr_project_info['client_info']['last_name']:'';
            /*$client_name               = $client_first_name.' '.$client_last_name;*/
            $client_name                 = $client_first_name;
            $expert_first_name           = isset($arr_project_info['expert_info']['first_name'])?$arr_project_info['expert_info']['first_name']:'';
            $expert_last_name            = isset($arr_project_info['expert_info']['last_name'])?$arr_project_info['expert_info']['last_name']:'';
            $expert_name                 = $expert_first_name.' '.$expert_last_name;
            /* Getting admins information from helper function */
            $admin_data                  = get_admin_email_address();
            //$data['login_url']           = url('/redirection?redirect=MILESTONE_APPROVED&id='.base64_encode($obj_milestone->project_id)) ;
            $data['project_name']        = isset($arr_project_info['project_name'])?$arr_project_info['project_name']:'';
            $data['milestone_title']     = isset($obj_milestone['title'])?$obj_milestone['title']:'';
            $data['milestone_amount']    = isset($obj_milestone['cost'])?$obj_milestone['cost'].'USD':'';
            $data['client_name']         = $client_name;
            $data['expert_name']         = $expert_name;
            $data['admin_name']          = isset($admin_data['user_name'])?$admin_data['user_name']:'';
          }
          $email_to = isset($arr_project_info['client_details']['email'])?$arr_project_info['client_details']['email']:'';
          if($email_to!= ""){
            try{
                $sendmail = Mail::send('admin.email.milestone_refund_by_admin_to_client', $data, function ($message) use ($email_to,$mail_form,$project_name) {
                    $message->from($mail_form, $project_name);
                    $message->subject($project_name.': Milestone Refunded.');
                    $message->to($email_to);
                });
            } catch(\Exception $e){
            //Session::Flash('error'   , 'E-Mail not sent');
            }
          }
          $outputdata = '<div class="alert alert-success alert-dismissible">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
               '.$transfer_refund->ResultMessage.' '.'<a href="" style="cursor:pointer;"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</a>  
          </div>';
        }   
        echo $outputdata;
      }  else{
        $outputdata = '<div class="alert alert-danger alert-dismissible">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
               '.$transfer_refund.' '.'<a href="" style="cursor:pointer;"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</a>  
          </div>';
        echo $outputdata;  
      }
    }
} // end class