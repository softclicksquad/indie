<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\RecruiterModel;
use App\Models\UserModel;
use App\Models\ProjectpostModel;
use Session;
use Validator;
use Sentinel;

class RecruiterController extends Controller
{
    public function __construct(RecruiterModel $recruiter,UserModel $use_model,ProjectpostModel $project_post)
    {      
       $this->RecruiterModel = $recruiter;
       $this->ProjectpostModel = $project_post;
       $this->UserModel = $use_model;
       $this->module_url_path = url(config('app.project.admin_panel_slug')."/recruiter");
       $this->project_attachment_public_path = url('/').config('app.project.img_path.project_attachment');
    }


     /*
        Auther : Sagar Sainkar
        Comments: display all project_manager
    */
	public function index()
    {
        $obj_project_manager = $this->RecruiterModel->with(['user_details'])->get();

        if($obj_project_manager != FALSE)
        {
            $arr_project_manager = $obj_project_manager->toArray();
        }

        $this->arr_view_data['arr_project_manager'] = $arr_project_manager;
		//dd($arr_project_manager);
        $this->arr_view_data['page_title'] = "Manage Recruiter";
        $this->arr_view_data['module_title'] = "Recruiter";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.recruiter.index',$this->arr_view_data);
    }


    /*  
        Auther : Sagar Sainkar
        Comments: display view for Add new project_manager
    */

    public function create()
    {
        $this->arr_view_data['page_title']          = "Add Recruiter";
        $this->arr_view_data['module_title']        = "Recruiter";
        $this->arr_view_data['module_url_path']     = $this->module_url_path;

        return view('admin.recruiter.create',$this->arr_view_data);
    }

    /*  
        Auther : Sagar Sainkar
        Comments: Add and store project_manager details
    */
    public function store(Request $request)
    {
        $form_data = array();

        $form_data = $request->all();
        
        $arr_rules['email'] = "required|email|unique:users,email";
        $arr_rules['user_name'] = "required|unique:users,user_name";
        $arr_rules['password'] = "required|min:8";
        $arr_rules['first_name'] = "required|max:255";
        $arr_rules['last_name'] = "required|max:255";
        $arr_rules['phone'] = "required|min:6|max:16";
        $arr_rules['address'] = "required|max:255";        
        
        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
             return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = $request->all();
        $arr_data = array();
        $arr_project_manager = array();

        $credentials = ['user_name' => $request->input('user_name')];
        $user = Sentinel::findByCredentials($credentials);

        $username_exits = $this->UserModel->where('user_name',$request->input('user_name'))->count();
        if ($username_exits) 
        {
            Session::flash('error','This user name is already present.');
            return redirect()->back()->withInput($request->all());   
        }

        $credentials = ['email' => $request->input('email')];
		$user = Sentinel::findByCredentials($credentials);

		if ($user) 
		{
			Session::flash('error','This email is already present.');
			return redirect()->back()->withInput($request->all());
		}

		$arr_data['email'] = $form_data['email'];
        $arr_data['password'] = $form_data['password'];
        $arr_data['user_name'] = $form_data['user_name'];
        $arr_data['is_active'] = 0;

        $obj_project_manager = Sentinel::registerAndActivate($arr_data);

        if ($obj_project_manager) 
        {
        	//assign role to user
        	$role = Sentinel::findRoleBySlug('recruiter');
			$obj_project_manager->roles()->attach($role);

        	$arr_project_manager['user_id'] = $obj_project_manager->id;
        	$arr_project_manager['first_name'] = ucfirst($form_data['first_name']);
        	$arr_project_manager['last_name'] = ucfirst($form_data['last_name']);
        	$arr_project_manager['address'] = $form_data['address'];
        	$arr_project_manager['phone'] = $form_data['phone'];

        	$status = $this->RecruiterModel->create($arr_project_manager);

        	if($status)
	        {
	        	Session::flash('success','Recruiter created successfully.');
	        } 
	        else
	        {
	            Session::flash('error','Problem occured, while creating recruiter.');
	        }
        }

        return redirect()->back()->withInput($request->all());
    }


    /*  
        Auther : Sagar Sainkar
        Comments: display view for edit project_manager
    */

    public function edit($enc_id)
    {
        $id = base64_decode($enc_id);

        $onj_manager = $this->RecruiterModel->where('id', $id)->with(['user_details'])->first();

        $arr_project_manager = array();

        if($onj_manager)
        {
           $arr_project_manager = $onj_manager->toArray();
        }

        $this->arr_view_data['enc_id'] = $enc_id;
        $this->arr_view_data['arr_project_manager'] = $arr_project_manager;  
        $this->arr_view_data['page_title'] = "Edit Recruiter";
        $this->arr_view_data['module_title'] = "Recruiter";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.recruiter.edit',$this->arr_view_data);  

    }


    /*  
        Auther : Sagar Sainkar
        Comments: update project_manager details
    */
    public function update(Request $request, $enc_id)
    {
        $manager_id = base64_decode($enc_id);
        $arr_rules = array();
        $status = FALSE;
 
        $arr_rules['first_name'] = "required|max:255";
        $arr_rules['last_name'] = "required|max:255";
        $arr_rules['phone'] = "required|max:16";
        $arr_rules['phone_code'] = "required|min:2";
        $arr_rules['address'] = "required|max:255";
        
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = array();
        $form_data = $request->all(); 

        $obj_manager = $this->RecruiterModel->where('id',$manager_id)->first();

        if($obj_manager && sizeof($obj_manager) > 0)
        { 	
        	$arr_project_manager = array();
            $arr_project_manager['first_name'] = ucfirst($form_data['first_name']);
        	$arr_project_manager['last_name'] = ucfirst($form_data['last_name']);
        	$arr_project_manager['address'] = $form_data['address'];
        	$arr_project_manager['phone'] = $form_data['phone'];
            $arr_project_manager['phone_code'] = $form_data['phone_code'];
            
        	$status = $obj_manager->update($arr_project_manager);
        }

        if ($status) 
        {
            Session::flash('success','Recruiter details updated successfully.');    
        }
        else
        {
            Session::flash('error','Error while updating recruiter details.');
        }
        
        return redirect()->back();
    }
     /*  
        Auther : Bharat Khairnar
        Comments: List of projects who handled by recruiter
    */
    public function projects($enc_id)
    {
       $manager_user_id = base64_decode($enc_id);
      
       $obj_projects =  $this->ProjectpostModel->where('project_recruiter_user_id',$manager_user_id)->with(['skill_details','client_details','category_details'])->get();

       if($obj_projects)
       {
           $arr_projects = $obj_projects->toArray();
       }



       $this->arr_view_data['enc_id'] = $enc_id;
       $this->arr_view_data['arr_projects'] = $arr_projects;  
       $this->arr_view_data['page_title'] = "Manage Projects";
       $this->arr_view_data['module_title'] = "Recruiter";
       $this->arr_view_data['module_url_path'] = $this->module_url_path;

       return view('admin.recruiter.project_list',$this->arr_view_data);  
    }
     /*  
        Auther : Bharat Khairnar
        Comments: display view for projects details.
    */

    public function show($project_id)
    {
        $id = base64_decode($project_id);

        $obj_project_info = $this->ProjectpostModel->with('project_skills.skill_data','client_details','category_details','project_recruiter_info')->where('id',$id)->first();

        $arr_info = array();

        if($obj_project_info)
        {
           $arr_info = $obj_project_info->toArray();
        }
        //dd($arr_info);
        $this->arr_view_data['arr_info'] = $arr_info;
        $this->arr_view_data['page_title'] = "Project Details";
        $this->arr_view_data['module_title'] = "Recruiter";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        $this->arr_view_data['project_attachment_public_path'] = $this->project_attachment_public_path;

        return view('admin.recruiter.show',$this->arr_view_data);

    }

    /*
    | Following Fuctions for active ,deactive and delete
    | auther :Sagar Sainkar    
    | 
    */ 

    public function activate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while recruiter activation.');
            return redirect()->back();
        }

        if($this->perform_activate(base64_decode($enc_id)))
        {
            Session::flash('success','Recruiter activated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while recruiter activation.');
        }

        return redirect()->back();
    }

    public function deactivate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
        	Session::flash('error','Problem occured while recruiter deactivation.');
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            Session::flash('success','Recruiter deactivated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while recruiter deactivation.');
        }

        return redirect()->back();
    }

    public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
        	Session::flash('error','Problem occured while recruiter deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Recruiter deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while recruiter deletion.');
        }

        return redirect()->back();
    }


    public function perform_activate($id)
    {
        if ($id) 
        {
            $manager = $this->UserModel->where('id',$id)->first();
            if($manager)
            {
                return $manager->update(['is_active'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_deactivate($id)
    {
        if ($id) 
        {
            $manager = $this->UserModel->where('id',$id)->first();
            if($manager)
            {
                return $manager->update(['is_active'=>0]);
            }
        }

        return FALSE;
    }

    public function perform_delete($id)
    {
        if ($id) 
        {	
        	$user= $this->UserModel->where('id',$id)->first();
            $manager= $this->RecruiterModel->where('user_id',$id)->first();

            if($user!=FALSE && $manager!=FALSE)
            {	
            	$delete_user = $user->delete();	
            	return $manager->delete();	
            }
        }
        return FALSE;
    }
   

     /*
    | multi_action: Following Fuctions for active ,deactive and delete for multiple records
    | auther :Sagar Sainkar    
    | 
    */ 
    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Session::flash('success','Recruiter(s) deleted successfully.');
            } 
            elseif($multi_action=="activate")
            {
               $this->perform_activate(base64_decode($record_id)); 
               Session::flash('success','Recruiter(s) activated successfully');               
            }
            elseif($multi_action=="deactivate")
            {
               $this->perform_deactivate(base64_decode($record_id));    
               Session::flash('success','Recruiter(s) blocked successfully.');
            }
        }

        return redirect()->back();
    }

}