<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\SubscriptionPacksModel; 

use Validator;
use Session;

class SubscriptionPacksController extends Controller
{

	/*
    | auther :Sagar Sainkar
    | Comment: controller for manage subscription packs
    */ 

    public function __construct(SubscriptionPacksModel $subscription_packs)
    {
        $this->SubscriptionPacksModel = $subscription_packs;
        
        $this->arr_view_data = [];
        $this->module_url_path = url(config('app.project.admin_panel_slug')."/subscription_packs");
    }

	/*
    | auther :Sagar Sainkar
    | Comment: display and mange subscription_packs
    */ 

    public function index()
    {
        $arr_packs = array();

        $obj_packs = $this->SubscriptionPacksModel->get();

        if($obj_packs != FALSE)
        {
            $arr_packs = $obj_packs->toArray();
        }

        $this->arr_view_data['arr_packs'] = $arr_packs;
        $this->arr_view_data['page_title'] = "Manage Subscription Packs";
        $this->arr_view_data['module_title'] = "Subscription Packs";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.subscription_packs.index',$this->arr_view_data);
    }    


    /*
    | edit() : edit subscription_packs details
    | auther : Sagar Sainkar
    */
    public function edit($enc_id)
    {
    	$arr_pack = array();
        $pack_id = base64_decode($enc_id); 

	   	if ($pack_id) 
	   	{
	        $obj_pack = $this->SubscriptionPacksModel->where('id', $pack_id)->first();
	        if($obj_pack)
	        {
	           $arr_pack = $obj_pack->toArray();
	        }
	    }

        $this->arr_view_data['enc_id'] = $enc_id;
        $this->arr_view_data['arr_pack'] = $arr_pack;  
        $this->arr_view_data['page_title'] = "Edit Subscription Packs";
        $this->arr_view_data['module_title'] = "Subscription Packs";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.subscription_packs.edit',$this->arr_view_data);    
    }


    /*
    | update() : update subscription_packs details
    | auther : Sagar Sainkar
    */

    public function update(Request $request, $enc_id)
    {
        $pack_id = base64_decode($enc_id);
        $arr_rules = array();
        
        $arr_rules['pack_name']                    = "required|max:250";
        $arr_rules['number_of_bids']               = "required|numeric|min:0";
        $arr_rules['topup_bid_price']              = "required|numeric|min:0";
        $arr_rules['number_of_categories']         = "required|numeric|min:0";
        $arr_rules['number_of_subcategories']      = "required|numeric|min:0";
        $arr_rules['number_of_skills']             = "required|numeric|min:0";
        $arr_rules['number_of_favorites_projects'] = "required|numeric|min:0";
        $arr_rules['website_commision']            = "required|numeric|max:100";
        $arr_rules['off']                          = "required|numeric|max:100";
        $arr_rules['number_of_payouts']            = "required|numeric|min:0";

        $arr_rules['pack_price']                   = "required|numeric|min:0";
        $arr_rules['pack_price_eur']               = "required|numeric|min:0";
        $arr_rules['pack_price_gbp']               = "required|numeric|min:0";
        $arr_rules['pack_price_pln']               = "required|numeric|min:0";
        $arr_rules['pack_price_chf']               = "required|numeric|min:0";
        $arr_rules['pack_price_nok']               = "required|numeric|min:0";
        $arr_rules['pack_price_sek']               = "required|numeric|min:0";
        $arr_rules['pack_price_dkk']               = "required|numeric|min:0";
        $arr_rules['pack_price_cad']               = "required|numeric|min:0";
        $arr_rules['pack_price_zar']               = "required|numeric|min:0";
        $arr_rules['pack_price_aud']               = "required|numeric|min:0";
        $arr_rules['pack_price_hkd']               = "required|numeric|min:0";
        $arr_rules['pack_price_jpy']               = "required|numeric|min:0";
        $arr_rules['pack_price_czk']               = "required|numeric|min:0";

        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return back()->withErrors($validator)->withInput();
        }

        $form_data = array();
        $form_data = $request->all();  

        /* Retrieve Existing pack */
        $pack = $this->SubscriptionPacksModel->where('id',$pack_id)->first();

        if(!$pack)
        {
            Session::flash('error','Problem occured while updating subscription pack.');
            return redirect()->back();   
        }

        $arr_data['pack_name']                    = $form_data['pack_name'];
        $arr_data['number_of_bids']               = $form_data['number_of_bids'];
        $arr_data['topup_bid_price']              = $form_data['topup_bid_price'];
        $arr_data['number_of_categories']         = $form_data['number_of_categories'];
        $arr_data['number_of_subcategories']      = $form_data['number_of_subcategories'];
        $arr_data['number_of_skills']             = $form_data['number_of_skills'];
        $arr_data['number_of_favorites_projects'] = $form_data['number_of_favorites_projects'];
        $arr_data['website_commision']            = $form_data['website_commision'];
        $arr_data['off']                          = $form_data['off'];
        $arr_data['number_of_payouts']            = $form_data['number_of_payouts'];

        $arr_data['pack_price']                   = $form_data['pack_price'];
        $arr_data['pack_price_eur']               = $form_data['pack_price_eur'];
        $arr_data['pack_price_gbp']               = $form_data['pack_price_gbp'];
        $arr_data['pack_price_pln']               = $form_data['pack_price_pln'];
        $arr_data['pack_price_chf']               = $form_data['pack_price_chf'];
        $arr_data['pack_price_nok']               = $form_data['pack_price_nok'];
        $arr_data['pack_price_sek']               = $form_data['pack_price_sek'];
        $arr_data['pack_price_dkk']               = $form_data['pack_price_dkk'];
        $arr_data['pack_price_cad']               = $form_data['pack_price_cad'];
        $arr_data['pack_price_zar']               = $form_data['pack_price_zar'];
        $arr_data['pack_price_aud']               = $form_data['pack_price_aud'];
        $arr_data['pack_price_hkd']               = $form_data['pack_price_hkd'];
        $arr_data['pack_price_jpy']               = $form_data['pack_price_jpy'];
        $arr_data['pack_price_czk']               = $form_data['pack_price_czk'];
                
        $status_update = $pack->update($arr_data);

        if ($status_update) 
        {
            Session::flash('success','Subscription pack updated successfully.');    
        }

        return redirect()->back();
    }

    /*
    | Following Fuctions for active ,deactive and delete
    | auther :Sagar Sainkar
    | Date : 06/05/2016
    | 
    */ 



    public function activate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while subscription pack activation.');
            return redirect()->back();
        }

        if($this->perform_activate(base64_decode($enc_id)))
        {
            Session::flash('success','Subscription pack activated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while subscription pack activation.');
        }

        return redirect()->back();
    }

    public function deactivate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while subscription pack deactivation.');
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            Session::flash('success','Subscription pack deactivated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while subscription pack deactivation.');
        }

        return redirect()->back();
    }

  
  	public function set_default($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while make subscription pack default.');
            return redirect()->back();
        }

        if($this->perform_default(base64_decode($enc_id)))
        {
            Session::flash('success','Subscription pack set to default successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while make subscription pack default.');
        }

        return redirect()->back();
    }

    


    public function perform_default($id)
    {
        if ($id) 
        {
        	$other_packs = $this->SubscriptionPacksModel->whereNotIn('id', [$id])->update(['is_default'=>0]);

            $obj_pack = $this->SubscriptionPacksModel->where('id',$id)->first();
            if($obj_pack)
            {
                return $obj_pack->update(['is_default'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_activate($id)
    {
        if ($id) 
        {
            $obj_pack = $this->SubscriptionPacksModel->where('id',$id)->first();
            if($obj_pack)
            {
                return $obj_pack->update(['is_active'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_deactivate($id)
    {
        if ($id) 
        {
            $obj_pack = $this->SubscriptionPacksModel->where('id',$id)->first();
            if($obj_pack)
            {
                return $obj_pack->update(['is_active'=>0]);
            }
        }

        return FALSE;
    }

   
   

    /*
    | multi_action: Following Fuctions for active ,deactive and delete for multiple records
    | auther :Sagar Sainkar
    | 
    */ 
    
    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            
            if($multi_action=="activate")
            {
               $this->perform_activate(base64_decode($record_id)); 
               Session::flash('success','Subscription pack(s) activated successfully');               
            }
            elseif($multi_action=="deactivate")
            {
               $this->perform_deactivate(base64_decode($record_id));    
               Session::flash('success','Subscription pack(s) blocked successfully.');
            }
        }

        return redirect()->back();
    }
}
