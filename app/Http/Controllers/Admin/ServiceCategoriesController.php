<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Common\Services\LanguageService;  
use App\Models\ServiceCategoriesModel;  

use Validator;
use Session;
Use Sentinel;

class ServiceCategoriesController extends Controller
{
     /*
        Auther : Sagar Sainkar
        Date: 
        Comments: controller for sevice categories (projec categories)        
    */
    public $ServiceCategoriesModel; 
    
    public function __construct(ServiceCategoriesModel $service_categories,LanguageService $langauge)
    {      
       $this->ServiceCategoriesModel = $service_categories;
       $this->LanguageService = $langauge;
       $this->module_url_path = url(config('app.project.admin_panel_slug')."/service_categories");

       $this->category_base_path = base_path() . '/public/uploads/admin/service_categories/';
       $this->category_public_path = url('/').'/uploads/admin/service_categories/';
    }


    /*
        Auther : Sagar Sainkar
        Date: 
        Comments: display catergories
    */
	public function index()
    {

        $arr_lang   =  $this->LanguageService->get_all_language();  

        $obj_service_category = $this->ServiceCategoriesModel->get();

        if($obj_service_category != FALSE)
        {
            $arr_service_category = $obj_service_category->toArray();
        }

        $this->arr_view_data['arr_service_categories'] = $arr_service_category;

        $this->arr_view_data['page_title'] = "Manage Service Categories";
        $this->arr_view_data['module_title'] = "Service Categories";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
         
        return view('admin.service_categories.index',$this->arr_view_data);
    }


    /*  
        Auther : Sagar Sainkar
        Comments: display view for Add new category
    */

    public function create()
    {

        $this->arr_view_data['arr_lang'] = $this->LanguageService->get_all_language();
        $this->arr_view_data['page_title'] = "Create Service Categories";
        $this->arr_view_data['module_title'] = "Service Categories";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.service_categories.create',$this->arr_view_data);
    }

    /*  
        Auther : Sagar Sainkar
        Comments: Add new category
    */
    public function store(Request $request)
    {
        $form_data = array();

        $form_data = $request->all();
        
        $arr_rules['category_title_en']     = "required";        
        $arr_rules['meta_tag_en']      = "required";
        $arr_rules['description_en']      = "required";
        $arr_rules['category_image']      = "required|mimes:png,jpeg,gif";
        
        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
             return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        /* Check if skill already exists with given translation */
        $does_exists = $this->ServiceCategoriesModel->whereHas('translations',function($query) use($request)
             {
                  $query->where('locale', 'en')
                        ->where('category_title',$request->input('category_title_en'));
             })->count();
        
        if($does_exists)
        {
            Session::flash('error','Category already exists.');            
            return redirect()->back();
        }
    
        $form_data = $request->all();
        $arr_data = array();
        $arr_data['category_slug'] = str_slug($form_data['category_title_en']);
        $arr_data['is_active'] = 1;

         /*file uploade code for laravel auther : sagar sainkar*/
         if ($request->hasFile('category_image')) 
         {  
            $img_valiator = Validator::make(array('image'=>$request->file('category_image')),array(
                                                'image' => 'mimes:png,jpeg,gif')); 

            if ($request->file('category_image')->isValid() && $img_valiator->passes())
            {

                $temp_img_name = $form_data['category_image'];
                //dd($company_logo_name);
                $imageExtension = $request->file('category_image')->getClientOriginalExtension();

                $imageName = sha1(uniqid().$temp_img_name.uniqid()).'.'.$imageExtension;
                
         
                $request->file('category_image')->move($this->category_base_path, $imageName);

                $arr_data['category_image'] = $imageName;  
            }
            else
            {
                 Session::flash("error","Please upload valid image.");
                 return back()->withErrors($validator)->withInput($request->all());
            }
        }
        /*file uploade code end here*/ 

        
        $service_category    = $this->ServiceCategoriesModel->create($arr_data);

        $service_category_id = $service_category->id;

        /* Fetch All Languages*/
        $arr_lang =  $this->LanguageService->get_all_language();

        if(sizeof($arr_lang) > 0 )
        {
            foreach ($arr_lang as $lang) 
            {            
                $arr_data     = array();
                $category_title   = 'category_title_'.$lang['locale'];
                $meta_tag   = 'meta_tag_'.$lang['locale'];
                $description = 'description_'.$lang['locale'];

                if( isset($form_data[$category_title]) && $form_data[$category_title] != '')
                { 
                    $translation = $service_category->translateOrNew($lang['locale']);

                    $translation->category_title       = ucfirst($form_data[$category_title]);
                    $translation->meta_tag      = $form_data[$meta_tag];
                    $translation->description    = $form_data[$description];                    
                    $translation->service_categories_id  = $service_category_id;

                    $translation->save();

                    Session::flash('success','Category created successfully.');
                }

            }//foreach

        } //if
        else
        {
            Session::flash('error','Problem occured, while creating category.');
            
        }

        return redirect()->back();
    }


    /*  
        Auther : Sagar Sainkar
        Comments: display view for edit category
    */

    public function edit($enc_id)
    {
        $id = base64_decode($enc_id);

        $arr_lang = $this->LanguageService->get_all_language();      

        $obj_service_category = $this->ServiceCategoriesModel->where('id', $id)->with(['translations'])->first();

        $arr_service_category = [];

        if($obj_service_category)
        {
           $arr_service_category = $obj_service_category->toArray(); 
           /* Arrange Locale Wise */
           $arr_service_category['translations'] = $this->arrange_locale_wise($arr_service_category['translations']);
        }

        $this->arr_view_data['enc_id'] = $enc_id;
        $this->arr_view_data['arr_lang'] = $this->LanguageService->get_all_language();          
        $this->arr_view_data['arr_service_category'] = $arr_service_category;  

        $this->arr_view_data['page_title'] = "Edit Service Categories";
        $this->arr_view_data['module_title'] = "Service Categories";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        $this->arr_view_data['category_public_path'] = $this->category_public_path;
       

        return view('admin.service_categories.edit',$this->arr_view_data);  

    }


    /*  
        Auther : Sagar Sainkar
        Comments: update category details
    */
    public function update(Request $request, $enc_id)
    {
        $category_id = base64_decode($enc_id);
        $arr_rules = array();
        $status = FALSE;

      	$arr_rules['category_title_en']     = "required";        
        $arr_rules['meta_tag_en']      = "required";
        $arr_rules['description_en']      = "required";

        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = array();
        $form_data = $request->all(); 

         /* Get All Active Languages */ 
  
        $arr_lang = $this->LanguageService->get_all_language();

        $category = $this->ServiceCategoriesModel->where('id',$category_id)->first();

        /*file uploade code for laravel auther : sagar sainkar*/
         if($request->hasFile('category_image') && $form_data['category_image']!="") 
         {  
            $img_valiator = Validator::make(array('image'=>$request->file('category_image')),array(
                                                'image' => 'mimes:png,jpeg,gif')); 

            if ($request->file('category_image')->isValid() && $img_valiator->passes())
            {
                $arr_data = array();

                $temp_img_name = $form_data['category_image'];
                //dd($company_logo_name);
                $imageExtension = $request->file('category_image')->getClientOriginalExtension();

                $imageName = sha1(uniqid().$temp_img_name.uniqid()).'.'.$imageExtension;
                
         
                $request->file('category_image')->move($this->category_base_path, $imageName);

                $arr_data['category_image'] = $imageName;

                if ($category) 
                {
                    $category_array = $category->toArray();
                    if ($category_array && sizeof($category_array)>0) 
                    {
                        if (isset($category_array['category_image']) && $category_array['category_image']!=null) 
                        {
                            $file_exits = file_exists($this->category_base_path.$category_array['category_image']);

                            if ($file_exits) 
                            {
                                unlink($this->category_base_path.$category_array['category_image']);
                            }
                        }
                    }
                }

                $category->update($arr_data);
            }
            else
            {
                 Session::flash("error","Please upload valid image.");
                 return back()->withErrors($validator)->withInput($request->all());
            }
        }
        /*file uploade code end here*/ 

        
         /* Insert Multi Lang Fields */

        if(sizeof($arr_lang) > 0)
        { 
            foreach($arr_lang as $i => $lang)
            {
                $translate_data_ary = array();
                $category_title   = 'category_title_'.$lang['locale'];

                if(isset($form_data[$category_title]) && $form_data[$category_title]!="")
                {
                    /* Get Existing Language Entry and update it */
                    $translation = $category->getTranslation($lang['locale']);    
                    if($translation)
                    {

		                $translation->category_title       =  ucfirst($form_data['category_title_'.$lang['locale']]);
                        $translation->meta_tag      	   =  $form_data['meta_tag_'.$lang['locale']];
                        $translation->description          =  $form_data['description_'.$lang['locale']];                        
                        $status = $translation->save();                       
                    }  
                    else
                    {
                        /* Create New Language Entry  */
                        $translation     = $category->getNewTranslation($lang['locale']);

                        $translation->service_categories_id  =  $category_id;
                        $translation->category_title       =  ucfirst($form_data['category_title_'.$lang['locale']]);
                        $translation->meta_tag      	   =  $form_data['meta_tag_'.$lang['locale']];
                        $translation->description          =  $form_data['description_'.$lang['locale']];    

                        $status = $translation->save();
                    } 
                }   
            }
            
        }

        if ($status) 
        {
            Session::flash('success','Category updated successfully.');    
        }
        else
        {
            Session::flash('error','Error while updating category.');       
        }
        
        return redirect()->back();
    }

    /*
    | Following Fuctions for active ,deactive and delete
    | auther :Sagar Sainkar
    | 
    */ 

    public function activate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while category activation.');
            return redirect()->back();
        }

        if($this->perform_activate(base64_decode($enc_id)))
        {
            Session::flash('success','Category activated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while category activation.');
        }

        return redirect()->back();
    }

    public function deactivate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
        	Session::flash('error','Problem occured while category deactivation.');
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            Session::flash('success','Category deactivated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while category deactivation.');
        }

        return redirect()->back();
    }

    public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
        	Session::flash('error','Problem occured while category deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Category deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while category deletion.');
        }

        return redirect()->back();
    }


    public function perform_activate($id)
    {
        if ($id) 
        {
            $service_category = $this->ServiceCategoriesModel->where('id',$id)->first();
            if($service_category)
            {
                return $service_category->update(['is_active'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_deactivate($id)
    {
        if ($id) 
        {
            $service_category = $this->ServiceCategoriesModel->where('id',$id)->first();
            if($service_category)
            {
                return $service_category->update(['is_active'=>0]);
            }
        }

        return FALSE;
    }

    public function perform_delete($id)
    {
        if ($id) 
        {
            $service_category= $this->ServiceCategoriesModel->where('id',$id)->first();
            if($service_category)
            {
                return $service_category->delete();
            }
        }
        return FALSE;
    }
   

     /*
    | multi_action: Following Fuctions for active ,deactive and delete for multiple records
    | auther :Sagar Sainkar
    | Date : 
    | 
    */ 
    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Session::flash('success','Category(s) deleted successfully.');
            } 
            elseif($multi_action=="activate")
            {
               $this->perform_activate(base64_decode($record_id)); 
               Session::flash('success','Category(s) activated successfully');               
            }
            elseif($multi_action=="deactivate")
            {
               $this->perform_deactivate(base64_decode($record_id));    
               Session::flash('success','Category(s) blocked successfully.');
            }
        }

        return redirect()->back();
    }

    public function arrange_locale_wise(array $arr_data)
    {
        if(sizeof($arr_data)>0)
        {
            foreach ($arr_data as $key => $data)
            {
                $arr_tmp = $data;
                unset($arr_data[$key]);

                $arr_data[$data['locale']] = $data;                    
            }

            return $arr_data;
        }
        else
        {
            return [];
        }
    }


}