<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\TransactionsModel;
use Session;
use Validator;
use Sentinel;
use Excel;

class TransactionsController extends Controller
{
    
    public function __construct(TransactionsModel $transactions)
    {      
       $this->TransactionsModel = $transactions;
       $this->module_url_path = url(config('app.project.admin_panel_slug')."/transactions");
    }


    /*
     Profile: load page of trransaction listing
     Author: Ashwini K
    */

   	public function index()
    {
        $this->arr_view_data['page_title'] = "Manage Transacations";
        $this->arr_view_data['module_title'] = "Transacations";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.transactions.index',$this->arr_view_data);
    }
   
     /*
     Profile: load page of trransaction details
     Author: Ashwini K
    */

    public function show($transaction_id)
    {
        $id = base64_decode($transaction_id);

        $obj_transaction_info = $this->TransactionsModel->with('user_details')->where('id',$id)->first();

        $arr_info = array();

        if($obj_transaction_info)
        {
           $arr_info = $obj_transaction_info->toArray();
        }
        $this->arr_view_data['arr_info'] = $arr_info;
        $this->arr_view_data['page_title'] = "Transacation Details";
        $this->arr_view_data['module_title'] = "Transacation Details";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.transactions.show',$this->arr_view_data);

    }

    /*
      Profile: Select start and end date so display transactions information between that dates.
      Author: Bharat K.
    */    

    public function filter( Request $request )
    {
        $arr_rules =array();
        $arr_rules['start_date'] = 'required';
        $arr_rules['end_date']   = 'required';
       
        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
       
        $form_data = $request->all();
        $start_date  = $form_data['start_date'];
        $end_date    = $form_data['end_date'];
     
        $start_date = date('Y-m-d', strtotime($start_date));

        $end_date = date('Y-m-d', strtotime($end_date));
        
        $obj_all_transactions=$this->TransactionsModel->whereBetween('created_at', array($start_date, $end_date))->with('user_details')->orderBy('payment_date','desc')->get();

        if($obj_all_transactions != FALSE)
        {
           $arr_all_transactions = $obj_all_transactions->toArray();
        }

        $this->arr_view_data['arr_all_transactions'] = $arr_all_transactions;
        $this->arr_view_data['page_title'] = "Manage Transacations";
        $this->arr_view_data['module_title'] = "Transacations";
        $this->arr_view_data['start_date']  = $start_date;
        $this->arr_view_data['end_date']    = $end_date;
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.transactions.index',$this->arr_view_data);

    }
    /* Profile: Export csv file of transactions information.
       Author: Bharat K*
       Generate CSV file */
    public function export(Request $request)
    {

        $arr_rules =array();
        $arr_rules['start_date'] = 'required';
        $arr_rules['end_date']   = 'required';

        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $start_date  = $request->input('start_date');
        $end_date    = $request->input('end_date');

       

        $start_date = date('Y-m-d', strtotime($start_date));
        $end_date = date('Y-m-d', strtotime($end_date));

         $obj_all_transactions=$this->TransactionsModel->whereBetween('created_at', array($start_date, $end_date))->with('user_details')->orderBy('payment_date','desc')->get();

        $arr_all_transactions = $arr_user_data = array();
        if($obj_all_transactions != FALSE)
        {
            $arr_all_transactions = $obj_all_transactions->toArray();
        }

        if(isset($arr_all_transactions) && sizeof($arr_all_transactions)>0)
        {
            foreach ($arr_all_transactions as $key => $transaction) 
            {
                if(isset($transaction['role_info']['name']))
                {
                   $name = ucfirst($transaction['role_info']['name']);
                }
                else
                {
                    $name = ucfirst($transaction['role_info']['first_name']).' '.ucfirst($transaction['role_info']['last_name']);
                }

                $arr_user_data[$key]['Name'] = $name;

                $arr_user_data[$key]['Email'] = isset($transaction['user_details']['email'])?$transaction['user_details']['email']:'';
                if(isset($transaction['transaction_type']) && $transaction['transaction_type']=='1')
                {
                    $transaction_type = 'Subscription';
                } 
                elseif(isset($transaction['transaction_type']) && $transaction['transaction_type']=='2')
                {
                    $transaction_type = 'Milestone';
                }
                elseif( isset($transaction['transaction_type']) && $transaction['transaction_type']=='3')
                {
                    $transaction_type = 'Release Milestones';
                }
                elseif( isset($transaction['transaction_type']) && $transaction['transaction_type']=='4')
                {
                    $transaction_type = 'Topup Bid';
                }
               
               $arr_user_data[$key]['Transaction Type'] = $transaction_type;
               $arr_user_data[$key]['Payment Amount'] = isset($transaction['paymen_amount'])?$transaction['paymen_amount']:'0';

                if(isset($transaction['payment_method']) && $transaction['payment_method']=='1')
                {
                  $payment_method = 'Paypal';
                }
                elseif(isset($transaction['payment_method']) && $transaction['payment_method']=='2')
                {
                  $payment_method = 'Stripe';
                }
                else
                {
                  $payment_method = 'Free';
                }
                $arr_user_data[$key]['Payment Method'] = $payment_method;

                 if(isset($transaction['payment_status']) && $transaction['payment_status']=='0')
                 {
                     $payment_status = 'Pending';
                 }
                  elseif(isset($transaction['payment_status']) && $transaction['payment_status']=='1')
                  {
                    $payment_status = 'Paid';
                  }
                    
                  elseif(isset($transaction['payment_status']) && $transaction['payment_status']=='2')
                  {
                    $payment_status = 'Paid';
                  }
                    
                  elseif(isset($transaction['payment_status']) && $transaction['payment_status']=='3')
                  {
                    $payment_status = 'Loss';
                  }
                  else
                  {
                    $payment_status = 'Pending';  
                  }
                  $arr_user_data[$key]['Payment Status'] = $payment_status;

                 if(isset($transaction['payment_date']) && $transaction['payment_date']!='0000-00-00 00:00:00')
                 {
                     $arr_user_data[$key]['Payment Date'] = isset($transaction['payment_date'])?date("d-M-Y", strtotime($transaction['payment_date'])):'';
                 }
                 else
                 {
                    $arr_user_data[$key]['Payment Date'] = '-';
                 }
                
            }
        }
        $data = $arr_user_data;
        $type = 'CSV';

        return Excel::create('Transaction_report', function($excel) use ($data) {

             // Set the title
            $excel->setTitle('Transaction Report');

            // Chain the setters
            $excel->setCreator('VirtualHomeConcept')
                    ->setCompany('VirtualHomeConcept');

            // Call them separately
            $excel->setDescription('Transaction report details');

            $excel->sheet('mySheet', function($sheet) use ($data)
            {

                $sheet->fromArray($data);

            });

        })->download($type);
    }

}
