<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Common\Services\LanguageService;  
use App\Models\CategoriesModel;  
use App\Models\SubCategoriesModel;  

use Validator;
use Session;
Use Sentinel;

class SubCategoriesController extends Controller
{
    public $SubCategoriesModel;
    public function __construct(SubCategoriesModel $sub_category,CategoriesModel $category,LanguageService $langauge)
    {
        $this->SubCategoriesModel = $sub_category;
        $this->CategoriesModel = $category;
        $this->LanguageService = $langauge;
        $this->module_url_path = url(config('app.project.admin_panel_slug')."/subcategories");
    }

    public function index()
    {
        $arr_subcategories = array();

        $arr_lang   =  $this->LanguageService->get_all_language();
        $obj_subcategories = $this->SubCategoriesModel->with('category_details.translations')->get();

         if($obj_subcategories!=FALSE)
         {
            $arr_subcategories =  $obj_subcategories->toArray();
         }  

        $this->arr_view_data['arr_subcategories'] = $arr_subcategories;
        $this->arr_view_data['page_title'] = "Manage Subcategories";
        $this->arr_view_data['module_title'] = "Subcategories";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        
        return view('admin.subcategories.index',$this->arr_view_data);
    }

    public function create()
    {
        $arr_categories = array();

        $obj_categories = $this->CategoriesModel->where('is_active','=','1')->get();
        if ($obj_categories!=FALSE) 
        {
            $arr_categories = $obj_categories->toArray();
        }

        $this->arr_view_data['arr_lang'] = $this->LanguageService->get_all_language();
        $this->arr_view_data['arr_categories'] = $arr_categories;

        $this->arr_view_data['page_title'] = "Create Subcategories";
        $this->arr_view_data['module_title'] = "Subcategories";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.subcategories.create',$this->arr_view_data);
    }

    public function store(Request $request)
    {
        $form_data = array();

        $form_data = $request->all();
        $arr_rules['parent_category'] = "required";
        $arr_rules['subcategory_title_en'] = "required";
        
        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
             return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        /* Check if subcategory already exists with given translation */
        $slug = str_slug($form_data['subcategory_title_en']);
        $category_id = $form_data['parent_category'];

        $does_exists = $this->SubCategoriesModel->where('subcategory_slug','=',$slug)
                                                ->where('category_id','=',$category_id)
                                                ->count();
        
        if($does_exists)
        {
            Session::flash('error','Subcategory already exists.');            
            return redirect()->back();
        }


       /* $does_exists = $this->SubCategoriesModel->whereHas('translations',function($query) use($request)
        {
          $query->where('locale', 'en')
                ->where('subcategory_title',$request->input('subcategory_title_en'));
        })->count();
        
        if($does_exists)
        {
            Session::flash('error','Subcategory already exists.');            
            return redirect()->back();
        }*/
    
        $form_data = $request->all();
        $arr_data = array();
        $arr_data['category_id'] = $form_data['parent_category'];
        $arr_data['subcategory_slug'] = str_slug($form_data['subcategory_title_en']);
        $arr_data['is_active'] = 1;
        
        $obj_subcategory    = $this->SubCategoriesModel->create($arr_data);

        $subcategory_id = $obj_subcategory->id;

        /* Fetch All Languages*/
        $arr_lang =  $this->LanguageService->get_all_language();

        if(sizeof($arr_lang) > 0 )
        {
            foreach ($arr_lang as $lang) 
            {            
                $arr_data     = array();
                $subcategory_title   = 'subcategory_title_'.$lang['locale'];
                
                if( isset($form_data[$subcategory_title]) && $form_data[$subcategory_title] != '')
                { 
                    $translation = $obj_subcategory->translateOrNew($lang['locale']);
                    $translation->subcategory_title       = ucfirst($form_data[$subcategory_title]);
                    $translation->subcategories_id  = $subcategory_id;
                    $translation->save();
                    Session::flash('success','Subcategory created successfully.');
                }

            }//foreach

        } //if
        else
        {
            Session::flash('error','Problem occured, while creating subcategory.');
            
        }
        return redirect()->back();
    }

    public function edit($enc_id)
    {
        $id = base64_decode($enc_id);

        $arr_lang = $this->LanguageService->get_all_language();      

        $obj_subcategory = $this->SubCategoriesModel->where('id', $id)
                                ->with(['translations'])
                                ->first();
        $arr_subcategory = [];
        if($obj_subcategory!=FALSE)
        {
           $arr_subcategory = $obj_subcategory->toArray(); 
           $arr_subcategory['translations'] = $this->arrange_locale_wise($arr_subcategory['translations']);
        }

        //get all categories
        $obj_categories = $this->CategoriesModel->get();
        if ($obj_categories!=FALSE) 
        {
            $arr_categories = $obj_categories->toArray();
        }

        $this->arr_view_data['enc_id'] = $enc_id;
        $this->arr_view_data['arr_lang'] = $this->LanguageService->get_all_language();          
        $this->arr_view_data['arr_subcategory'] = $arr_subcategory;  
        $this->arr_view_data['arr_categories'] = $arr_categories;

        $this->arr_view_data['page_title'] = "Edit Subcategories";
        $this->arr_view_data['module_title'] = "Subcategories";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.subcategories.edit',$this->arr_view_data);  

    }

    public function update(Request $request, $enc_id)
    {
        $subcategory_id = base64_decode($enc_id);
        $arr_rules = array();
        $status = FALSE;

        $arr_rules['subcategory_title_en']     = "required";        
        $arr_rules['parent_category']     = "required";        
        
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = array();
        $form_data = $request->all(); 

        /* Get All Active Languages */
        $arr_lang = $this->LanguageService->get_all_language();

        $subcategory = $this->SubCategoriesModel->where('id',$subcategory_id)->first();
         
        /* Insert Multi Lang Fields */
        if(sizeof($arr_lang) > 0 && $subcategory)
        { 

            $arr_data['subcategory_slug'] = str_slug($form_data['subcategory_title_en']);
            $arr_data['category_id'] = $form_data['parent_category'];
            
            $subcategory->update($arr_data);

            foreach($arr_lang as $i => $lang)
            {
                $translate_data_ary = array();
                $subcategory_title   = 'subcategory_title_'.$lang['locale'];

                if(isset($form_data[$subcategory_title]) && $form_data[$subcategory_title]!="")
                {
                    /* Get Existing Language Entry and update it */
                    $translation = $subcategory->getTranslation($lang['locale']);    
                    if($translation)
                    {
                        $translation->subcategory_title =  ucfirst($form_data['subcategory_title_'.$lang['locale']]);
                        $status = $translation->save();                       
                    }  
                    else
                    {
                        /* Create New Language Entry  */
                        $translation     = $subcategory->getNewTranslation($lang['locale']);
                        $translation->subcategories_id  =  $subcategory_id;
                        $translation->subcategory_title =  ucfirst($form_data['subcategory_title_'.$lang['locale']]);
                        $status = $translation->save();
                    } 
                }   
            }
            
        }

        if ($status) 
        {
            Session::flash('success','Subcategory updated successfully.');    
        }
        else
        {
            Session::flash('error','Error while updating subcategory.');       
        }
        
        return redirect()->back();
    }

    public function activate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while subcategory activation.');
            return redirect()->back();
        }

        if($this->perform_activate(base64_decode($enc_id)))
        {
            Session::flash('success','Subcategory activated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while subcategory activation.');
        }

        return redirect()->back();
    }

    public function deactivate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while subcategory deactivation.');
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            Session::flash('success','Subcategory deactivated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while subcategory deactivation.');
        }

        return redirect()->back();
    }

    public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while subcategory deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Subcategory deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while subcategory deletion.');
        }

        return redirect()->back();
    }


    public function perform_activate($id)
    {
        if ($id) 
        {
            $obj_sub_cat = $this->SubCategoriesModel->where('id',$id)->first();
            if($obj_sub_cat)
            {
                return $obj_sub_cat->update(['is_active'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_deactivate($id)
    {
        if ($id) 
        {
            $obj_sub_cat = $this->SubCategoriesModel->where('id',$id)->first();
            if($obj_sub_cat)
            {
                return $obj_sub_cat->update(['is_active'=>0]);
            }
        }

        return FALSE;
    }

    public function perform_delete($id)
    {
        if ($id) 
        {
            $obj_sub_cat= $this->SubCategoriesModel->where('id',$id)->first();
            if($obj_sub_cat)
            {
                return $obj_sub_cat->delete();
            }
        }
        return FALSE;
    }
   
    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Session::flash('success','Subcategory(s) deleted successfully.');
            } 
            elseif($multi_action=="activate")
            {
               $this->perform_activate(base64_decode($record_id)); 
               Session::flash('success','Subcategory(s) activated successfully');               
            }
            elseif($multi_action=="deactivate")
            {
               $this->perform_deactivate(base64_decode($record_id));    
               Session::flash('success','Subcategory(s) blocked successfully.');
            }
        }

        return redirect()->back();
    }

    public function arrange_locale_wise(array $arr_data)
    {
        if(sizeof($arr_data)>0)
        {
            foreach ($arr_data as $key => $data)
            {
                $arr_tmp = $data;
                unset($arr_data[$key]);

                $arr_data[$data['locale']] = $data;                    
            }

            return $arr_data;
        }
        else
        {
            return [];
        }
    }
}
