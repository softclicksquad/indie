<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models\NewslettersModel;
use App\Models\NewslettersSubscriberModel;
use Session;
use Validator;
use Sentinel;

class NewsletterSubscriberController extends Controller
{
    /*
        Auther : Sagar Sainkar
        Comments: controller for manage NewslettersSubscriber
    */

    public function __construct(NewslettersModel $newsletters,NewslettersSubscriberModel $newsletters_subscriber)
    {      
       $this->NewslettersModel = $newsletters;
       $this->NewslettersSubscriberModel = $newsletters_subscriber;
       $this->module_url_path = url(config('app.project.admin_panel_slug')."/newsletter_subscriber");
    }

    /*
        Auther : Sagar Sainkar
        Comments: display all newsletter_subscriber
    */
	public function index()
    {

        $obj_subscriber = $this->NewslettersSubscriberModel->get();

        if($obj_subscriber != FALSE)
        {
            $arr_subscribers = $obj_subscriber->toArray();
        }

        $this->arr_view_data['arr_subscribers'] = $arr_subscribers;

        $this->arr_view_data['page_title'] = "Manage Newsletters Subscriber";
        $this->arr_view_data['module_title'] = "Newsletters Subscriber";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
         
        return view('admin.newsletter_subscriber.index',$this->arr_view_data);
    }


    /*
    | Following Fuctions for active ,deactive and delete
    | auther :Sagar Sainkar    
    | 
    */ 

    public function activate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while newsletter subscriber activation.');
            return redirect()->back();
        }

        if($this->perform_activate(base64_decode($enc_id)))
        {
            Session::flash('success','Newsletter subscriber activated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while newsletter subscriber activation.');
        }

        return redirect()->back();
    }

    public function deactivate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
        	Session::flash('error','Problem occured while newsletter subscriber deactivation.');
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            Session::flash('success','Newsletter subscriber deactivated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while newsletter subscriber deactivation.');
        }

        return redirect()->back();
    }

    public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
        	Session::flash('error','Problem occured while newsletter subscriber deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Newsletter subscriber deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while newsletter subscriber deletion.');
        }

        return redirect()->back();
    }


    public function perform_activate($id)
    {
        if ($id) 
        {
            $newsletter_subscriber = $this->NewslettersSubscriberModel->where('id',$id)->first();
            if($newsletter_subscriber)
            {
                return $newsletter_subscriber->update(['is_active'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_deactivate($id)
    {
        if ($id) 
        {
            $newsletter_subscriber = $this->NewslettersSubscriberModel->where('id',$id)->first();
            if($newsletter_subscriber)
            {
                return $newsletter_subscriber->update(['is_active'=>0]);
            }
        }

        return FALSE;
    }

    public function perform_delete($id)
    {
        if ($id) 
        {
            $newsletter_subscriber= $this->NewslettersSubscriberModel->where('id',$id)->first();
            if($newsletter_subscriber)
            {
                return $newsletter_subscriber->delete();
            }
        }
        return FALSE;
    }
   

     /*
    | multi_action: Following Fuctions for active ,deactive and delete for multiple records
    | auther :Sagar Sainkar    
    | 
    */ 
    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Session::flash('success','Newsletter subscriber(s) deleted successfully.');
            } 
            elseif($multi_action=="activate")
            {
               $this->perform_activate(base64_decode($record_id)); 
               Session::flash('success','Newsletter subscriber(s) activated successfully');               
            }
            elseif($multi_action=="deactivate")
            {
               $this->perform_deactivate(base64_decode($record_id));    
               Session::flash('success','Newsletter subscriber(s) blocked successfully.');
            }
        }

        return redirect()->back();
    }
}