<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\CurrencyConversionModel;

class CurrencyRateController extends Controller
{
    public function __construct(CurrencyConversionModel $currency_conversion)
    {      
       $this->CurrencyConversionModel = $currency_conversion;
       $this->module_url_path         = url(config('app.project.admin_panel_slug')."/currency_rate");
    }


    /*
        Auther : Sagar Sainkar
        Comments: display catergories
    */
    public function index()
    {
        $last_updated_at = '';
        $arr_currency_conversion = [];
        $obj_currency_conversion = $this->CurrencyConversionModel
                                                    ->with('from_currency_detail','to_currency_detail')
                                                    ->get();

        if($obj_currency_conversion != FALSE)
        {
            $arr_currency_conversion = $obj_currency_conversion->toArray();
        }
        
        if(isset($arr_currency_conversion) && count($arr_currency_conversion)>0){
            foreach ($arr_currency_conversion as $key => $value) 
            {
                if($key == 0) {
                    if(isset($value['updated_at'])){
                        $last_updated_at = date('d-M-Y H:i A',strtotime($value['updated_at']));
                    }
                }
                $arr_currency_conversion[$key]['from_currency_code'] = isset($value['from_currency_detail']['currency_code']) ? $value['from_currency_detail']['currency_code'] : '';
                $arr_currency_conversion[$key]['to_currency_code']   = isset($value['to_currency_detail']['currency_code']) ? $value['to_currency_detail']['currency_code'] : '';
                unset($arr_currency_conversion[$key]['from_currency_detail']);
                unset($arr_currency_conversion[$key]['to_currency_detail']);
            }
        }
        $arr_currency_conversion = $this->rekey_array('from_currency_code',$arr_currency_conversion,true);

        $this->arr_view_data['arr_currency_conversion'] = $arr_currency_conversion;
        $this->arr_view_data['last_updated_at']         = $last_updated_at;
        $this->arr_view_data['page_title']              = "Manage Currency Rates";
        $this->arr_view_data['module_title']            = "Currency Rates";
        $this->arr_view_data['module_url_path']         = $this->module_url_path;

        return view('admin.currency_rate.index',$this->arr_view_data);
    }

    private function rekey_array($str_key,$arr_data,$is_multiple_array_with_same_key = false) {
        $str_key = trim($str_key);
        if(!isset($arr_data) || !is_array($arr_data) || count($arr_data)<=0){
            return $arr_data;
        }
        $arr_rekey_data = [];
        foreach ($arr_data as $key => $value) {
            $str_key_value = isset($value[$str_key]) ? trim($value[$str_key]) : '';
            if($str_key_value!='') {
                if($is_multiple_array_with_same_key){
                    $arr_rekey_data[$str_key_value][] = $value;     
                } else {
                    $arr_rekey_data[$str_key_value] = $value;
                }
            }
        }
        return $arr_rekey_data;
    }

}