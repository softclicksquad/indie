<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\SealedEntryRatesModel;

use Validator;
use Session;
Use Sentinel;

class SealedEntryRatesController extends Controller
{
    public function __construct(SealedEntryRatesModel $sealed_entry_rates)
    {
        $this->SealedEntryRatesModel = $sealed_entry_rates;
        $this->module_url_path = url(config('app.project.admin_panel_slug')."/sealed_entry_rates");
    }

    public function index()
    {
        $arr_data = [];

        $obj_data = $this->SealedEntryRatesModel->get();

         if($obj_data!=FALSE)
         {
            $arr_data =  $obj_data->toArray();
         }  

        $this->arr_view_data['arr_data']        = $arr_data;
        $this->arr_view_data['page_title']      = "Manage Sealed Entry Rates";
        $this->arr_view_data['module_title']    = "Sealed Entry Rates";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        
        return view('admin.sealed_entry_rates.index',$this->arr_view_data);
    }

    public function create()
    {
    	$arr_quantity = [];

    	for ($i=1; $i <=100 ; $i++) { 
    		$arr_quantity[] = $i;	
    	}

        $this->arr_view_data['page_title']      = "Create Sealed Entry Rates";
        $this->arr_view_data['module_title']    = "Sealed Entry Rates";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        $this->arr_view_data['arr_quantity']    = $arr_quantity;

        return view('admin.sealed_entry_rates.create',$this->arr_view_data);
    }

    public function store(Request $request)
    {
        $form_data = array();

        $form_data = $request->all();

        $arr_rules['quantity'] = "required";
        $arr_rules['price']    = "required";
        
        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
             return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        /* Check if subSealed Entry Rates already exists with given translation */
        $quantity = $form_data['quantity'];
        $does_exists = $this->SealedEntryRatesModel
                                            ->where('quantity',$quantity)
                                            ->count();
        
        if($does_exists)
        {
            Session::flash('error','Sealed Entry Rates already exists for selected quantity.');      
            return redirect()->back();
        }

        $arr_data              = array();
        $arr_data['quantity']  = $form_data['quantity'];
        $arr_data['price']     = $form_data['price'];
        $arr_data['is_active'] = 1;
        
        $obj_data = $this->SealedEntryRatesModel->create($arr_data);
		
		if($obj_data)
        {
        	Session::flash('success','Sealed Entry Rates created successfully.');
        	return redirect()->back();
        }
        Session::flash('error','Problem occured, while creating sealed entry rates.');
 		return redirect()->back();
    }

    public function edit($enc_id)
    {
        $id = base64_decode($enc_id);

        $arr_data = [];
        $obj_data = $this->SealedEntryRatesModel->where('id', $id)
                                ->first();
        if($obj_data!=FALSE)
        {
           $arr_data = $obj_data->toArray(); 
        }

        $arr_quantity = [];
    	for ($i=1; $i <=100 ; $i++) { 
    		$arr_quantity[] = $i;	
    	}

        $this->arr_view_data['enc_id']          = $enc_id;
        $this->arr_view_data['arr_data']        = $arr_data;
        $this->arr_view_data['arr_quantity']    = $arr_quantity;
        $this->arr_view_data['page_title']      = "Edit Sealed Entry Rates";
        $this->arr_view_data['module_title']    = "Sealed Entry Rates";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.sealed_entry_rates.edit',$this->arr_view_data);  

    }

    public function update(Request $request, $enc_id)
    {
        $enc_id = base64_decode($enc_id);
     
        $arr_rules = array();
     
        $arr_rules['quantity'] = "required";
        $arr_rules['price']    = "required";
        
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
             return redirect()->back()->withErrors($validator)->withInput($request->all());
        }
        
        $form_data = $request->all(); 

        /* Check if subSealed Entry Rates already exists with given translation */
        $quantity = $form_data['quantity'];
        $does_exists = $this->SealedEntryRatesModel
                                            ->where('quantity',$quantity)
                                            ->where('id','!=',$enc_id)
                                            ->count();
        
        if($does_exists)
        {
            Session::flash('error','Sealed Entry Rates already exists for selected quantity.');      
            return redirect()->back();
        }

        $arr_data              = array();
        $arr_data['quantity']  = $form_data['quantity'];
        $arr_data['price']     = $form_data['price'];
        
        $obj_data = $this->SealedEntryRatesModel->where('id',$enc_id)->update($arr_data);
		
		if($obj_data)
        {
        	Session::flash('success','Sealed Entry Rates updated successfully.');
        	return redirect()->back();
        }
        Session::flash('error','Problem occured, while updating sealed entry rates.');
 		return redirect()->back();
    }

    public function activate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while Sealed Entry Rates activation.');
            return redirect()->back();
        }

        if($this->perform_activate(base64_decode($enc_id)))
        {
            Session::flash('success','Sealed Entry Rates activated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while Sealed Entry Rates activation.');
        }

        return redirect()->back();
    }

    public function deactivate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while Sealed Entry Rates deactivation.');
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            Session::flash('success','Sealed Entry Rates deactivated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while Sealed Entry Rates deactivation.');
        }

        return redirect()->back();
    }

    public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while Sealed Entry Rates deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Sealed Entry Rates deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while Sealed Entry Rates deletion.');
        }

        return redirect()->back();
    }
    
    public function perform_activate($id)
    {
        if ($id) 
        {
            $obj_sub_cat = $this->SealedEntryRatesModel->where('id',$id)->first();
            if($obj_sub_cat)
            {
                return $obj_sub_cat->update(['is_active'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_deactivate($id)
    {
        if ($id) 
        {
            $obj_sub_cat = $this->SealedEntryRatesModel->where('id',$id)->first();
            if($obj_sub_cat)
            {
                return $obj_sub_cat->update(['is_active'=>0]);
            }
        }

        return FALSE;
    }

    public function perform_delete($id)
    {
        if ($id) 
        {
            $obj_sub_cat= $this->SealedEntryRatesModel->where('id',$id)->first();
            if($obj_sub_cat)
            {
                return $obj_sub_cat->delete();
            }
        }
        return FALSE;
    }
   
    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Session::flash('success','Sealed Entry Rates(s) deleted successfully.');
            } 
            elseif($multi_action=="activate")
            {
               $this->perform_activate(base64_decode($record_id)); 
               Session::flash('success','Sealed Entry Rates(s) activated successfully');               
            }
            elseif($multi_action=="deactivate")
            {
               $this->perform_deactivate(base64_decode($record_id));    
               Session::flash('success','Sealed Entry Rates(s) blocked successfully.');
            }
        }

        return redirect()->back();
    }

    public function arrange_locale_wise(array $arr_data)
    {
        if(sizeof($arr_data)>0)
        {
            foreach ($arr_data as $key => $data)
            {
                $arr_tmp = $data;
                unset($arr_data[$key]);

                $arr_data[$data['locale']] = $data;                    
            }

            return $arr_data;
        }
        else
        {
            return [];
        }
    }
}
