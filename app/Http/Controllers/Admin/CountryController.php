<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\CountryModel; 

use Validator;
use Session;
use Input;
use DB;
 
class CountryController extends Controller
{

     /*
    | Constructor : creates instances of model class 
    |               & handles the admin authantication
    | auther : Sagar Sainkar
    | Date : 05/05/2016
    | @return \Illuminate\Http\Response
    */

    /*NOTE: all deletion methods and code is commented in controller and view because we doesnot have to give delete option to admin(if required please only make on delete fuctions in view and controller) */
 
    public function __construct(CountryModel $countries)
    {
        $this->CountryModel = $countries;
        
        $this->arr_view_data = [];
        $this->module_url_path = url(config('app.project.admin_panel_slug')."/countries");
    }   


     /*
    | Index : Display listing of countries
    | auther :Sagar Sainkar
    | Date : 05/05/2016
    | @return \Illuminate\Http\Response
    */ 
 
    public function index()
    {
        $arr_countries = array();

        $obj_countries = $this->CountryModel->get();

        if($obj_countries != FALSE)
        {
            $arr_countries = $obj_countries->toArray();
        }

        $this->arr_view_data['arr_countries'] = $arr_countries;

        $this->arr_view_data['page_title'] = "Manage Countries";
        $this->arr_view_data['module_title'] = "Countries";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.country.index',$this->arr_view_data);
    }



    public function create()
    {

        $this->arr_view_data['page_title'] = "Create Country";
        $this->arr_view_data['module_title'] = "Country";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.country.create',$this->arr_view_data);
    }



    /*
    | store() : store country details
    | auther : Sagar Sainkar
    | Date : 05/05/2016
    | @param  \Illuminate\Http\Request  $request
    | 
    */

    public function store(Request $request)
    {
        $form_data = array();

        
        $arr_rules['country_name'] = "required";
        $arr_rules['country_code'] = "required";
        
        $form_data = $request->all();

        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }   

        /* Check if country already exists with given translation */
        $does_exists = $this->CountryModel->where('country_name',$form_data['country_name'])->count();
                      
        if($does_exists)
        {
            Session::flash('error','Country already exists.');            
            return redirect()->back();
        }

        /* Insert into Country Table */
        $arr_data = array();
        $arr_data['country_name'] =  $form_data['country_name'];
        $arr_data['country_code'] = strtoupper($form_data['country_code']);
        $arr_data['is_active'] =  '1';

        $country = $this->CountryModel->create($arr_data);


        if($country)      
        {
            Session::flash('success','Country created successfully.');
        }
        else
        {
            Session::flash('error','Problem occured, while creating country.');
        }

        return redirect()->back();
    }


    /*
    | edit() : edit country details
    | auther : Sagar Sainkar
    | Date : 05/05/2016    
    | 
    */
    public function edit($enc_id)
    {

        $Country_id = base64_decode($enc_id); 

        $obj_country = $this->CountryModel->where('id', $Country_id)->first();

        $arr_country = [];
        if($obj_country)
        {
           $arr_country = $obj_country->toArray();
        }


        $this->arr_view_data['enc_id'] = $enc_id;
        $this->arr_view_data['arr_country'] = $arr_country;  
        $this->arr_view_data['page_title'] = "Edit Country";
        $this->arr_view_data['module_title'] = "Country";
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.country.edit',$this->arr_view_data);    
    }


    /*
    | update() : update country details
    | auther : Sagar Sainkar
    | Date : 05/05/2016
    | @param  \Illuminate\Http\Request  $request
    | 
    */

    public function update(Request $request, $enc_id)
    {

        $country_id = base64_decode($enc_id);
        $arr_rules = array();
        
        $arr_rules['country_name'] = "required";
        $arr_rules['country_code'] = "required";
      
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return back()->withErrors($validator)->withInput();
        }

        $form_data = array();
        $form_data = $request->all();  

        /* Retrieve Existing country */
        $country = $this->CountryModel->where('id',$country_id)->first();

        if(!$country)
        {
            Session::flash('error','Problem occured while updating country.');
            return redirect()->back();   
        }

        /*Check if country already exists */
        $does_exists = $this->CountryModel->where('country_name',$form_data['country_name'])->count();
                      
        if($does_exists)
        {
            Session::flash('error','Country already exists.');            
            return redirect()->back();
        }
        

        $arr_data['country_name'] =  $form_data['country_name'];
        $arr_data['country_code'] = strtoupper($form_data['country_code']);

        $country_instance = clone $country ;        
        $status_update = $country_instance->update($arr_data);

        if ($status_update) 
        {
            Session::flash('success','Country updated successfully.');    
        }

        return redirect()->back();
    }

    /*
    | Following Fuctions for active ,deactive and delete
    | auther :Sagar Sainkar
    | Date : 06/05/2016
    | 
    */ 



    public function activate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while country activation.');
            return redirect()->back();
        }

        if($this->perform_activate(base64_decode($enc_id)))
        {
            Session::flash('success','Country activated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while country activation.');
        }

        return redirect()->back();
    }

    public function deactivate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while country deactivation.');
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            Session::flash('success','Country deactivated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while country deactivation.');
        }

        return redirect()->back();
    }

  /*  public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while country deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Country deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while country deletion.');
        }

        return redirect()->back();
    }*/


    public function perform_activate($id)
    {
        if ($id) 
        {
            $obj_country = $this->CountryModel->where('id',$id)->first();
            if($obj_country)
            {
                return $obj_country->update(['is_active'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_deactivate($id)
    {
        if ($id) 
        {
            $obj_country = $this->CountryModel->where('id',$id)->first();
            if($obj_country)
            {
                return $obj_country->update(['is_active'=>0]);
            }
        }

        return FALSE;
    }


    public function block($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while country Blocked.');
            return redirect()->back();
        }

        if($this->perform_block(base64_decode($enc_id)))
        {
            Session::flash('success','Country Blocked successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while country Blocked.');
        }

        return redirect()->back();
    }

    public function un_block($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while country Un Blocked.');
            return redirect()->back();
        }

        if($this->perform_un_block(base64_decode($enc_id)))
        {
            Session::flash('success','Country Un Blocked successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while country Un Blocked.');
        }

        return redirect()->back();
    }
    public function perform_block($id)
    {
        if ($id) 
        {
            $obj_country = $this->CountryModel->where('id',$id)->first();
            if($obj_country)
            {
                return $obj_country->update(['is_mangopay_blocked'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_un_block($id)
    {
        if ($id) 
        {
            $obj_country = $this->CountryModel->where('id',$id)->first();
            if($obj_country)
            {
                return $obj_country->update(['is_mangopay_blocked'=>0]);
            }
        }

        return FALSE;
    }

   /* public function perform_delete($id)
    {
        if ($id) 
        {
            $obj_country= $this->CountryModel->where('id',$id)->first();
            if($obj_country)
            {
                return $obj_country->delete();
            }
        }
        return FALSE;
    }*/
   

    /*
    | multi_action: Following Fuctions for active ,deactive and delete for multiple records
    | auther :Sagar Sainkar
    | Date : 06/05/2016
    | 
    */ 
    
    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            Session::flash('error','please select at least one record.');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            
            if($multi_action=="activate")
            {
               $this->perform_activate(base64_decode($record_id)); 
               Session::flash('success','Country(s) activated successfully');               
            }
            elseif($multi_action=="deactivate")
            {
               $this->perform_deactivate(base64_decode($record_id));    
               Session::flash('success','Country(s) blocked successfully.');
            }
            elseif($multi_action=="mangopay_activate")
            {
                $this->perform_un_block(base64_decode($record_id)); 
                Session::flash('success','Country(s) activated successfully'); 
            }
            elseif($multi_action=="mangopay_deactivate")
            {
                $this->perform_block(base64_decode($record_id));    
                Session::flash('success','Country(s) blocked successfully.');
            }

            

            /*elseif($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Session::flash('success','Country(s) deleted successfully.');
            } */
        }

        return redirect()->back();
    }
    
}