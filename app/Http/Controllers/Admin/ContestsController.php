<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\NotificationsModel;
use App\Models\ContestPricingModel;
use App\Models\ContestModel;
use App\Models\ContestEntryModel;
use App\Common\Services\MailService;
use App\Models\ContestEntryImagesModel;

use Session;
use Validator;
use Sentinel;
use Mail;
use Lang;

class ContestsController extends Controller
{
    /*
        Auther : Shankar Jamdhade
        Comments: controller for manage all projects
    */

        public function __construct(
          MailService $mail_service,
          NotificationsModel $notifications,
          ContestPricingModel $ContestPricingModel,
          ContestModel $ContestModel,
          ContestEntryModel $ContestEntryModel,
          ContestEntryImagesModel $ContestEntryImagesModel
        )
        {      
         $this->NotificationsModel      = $notifications;
         $this->ContestPricingModel     = $ContestPricingModel;
         $this->MailService             = $mail_service;
         $this->BaseModel               = $ContestModel;
         $this->ContestEntryModel       = $ContestEntryModel;
         $this->ContestEntryImagesModel = $ContestEntryImagesModel;
         $user                          = Sentinel::check();
         if(!$user){
          return redirect()->back();
        }
        $this->user_id             = $user->id;
        $this->module_url_path     = url(config('app.project.admin_panel_slug')."/contests");
      }
      public function pricing_listing() {
        $pricing             = [];
        $this->arr_view_data = [];
        $get_pricing = $this->ContestPricingModel
        ->where('status','!=','2')
        ->get();
        if(sizeof($get_pricing)>0){
          $pricing = $get_pricing->toArray();
        }           

        $this->arr_view_data['pricing']         = $pricing;
        $this->arr_view_data['page_title']      = "Manage";
        $this->arr_view_data['module_title']    = "Contest Pricing";
        $this->arr_view_data['module_url_path'] = $this->module_url_path.'/pricing';
        return view('admin.contests.pricing.index',$this->arr_view_data);
      }
      public function edit($enc_id)
      {
        $id          = base64_decode($enc_id);
        $obj_pricing = $this->ContestPricingModel->where('id', $id)->first();
        $arr_pricing = [];
        if($obj_pricing) {
         $arr_pricing  = $obj_pricing->toArray(); 
       }
       $pricing_for = "";
       if(isset($arr_pricing['pricing_for']) && $arr_pricing['pricing_for'] !=""){ $pricing_for = $arr_pricing['pricing_for']; }
       $this->arr_view_data['enc_id']          = $enc_id;
       $this->arr_view_data['pricing']         = $arr_pricing;
       $this->arr_view_data['page_title']      = $pricing_for.": Edit";
       $this->arr_view_data['module_title']    = "Contests Pricing";
       $this->arr_view_data['module_url_path'] = $this->module_url_path.'/pricing';
       return view('admin.contests.pricing.edit',$this->arr_view_data);  
     }
     public function update(Request $request){
      $form_data = $request->all();
      $arr_rules['price_type'] = "required";
      $arr_rules['price']      = "required";
      $arr_rules['pricing_id'] = "required";
      $validator = Validator::make($request->all(),$arr_rules);
      if($validator->fails()) {
       return redirect()->back()->withErrors($validator)->withInput($request->all());
     }
     $update_data                     = [];
     $update_data['price_type']       = $request->input('price_type');
     $update_data['price']            = $request->input('price'); 

     $store = $this->ContestPricingModel
     ->where('id',$request->input('pricing_id'))
     ->update($update_data); 
     if($store){
      Session::flash('success','Pricing updated successfully.');
    } else {
      Session::flash('error','Problem occured while pricing updated.'); 
    }
    return redirect($this->module_url_path.'/pricing');
  }
  public function manage(Request $request)
  {
    $contests            = [];
    $this->arr_view_data = [];
    $get_contests        = $this->BaseModel
    ->with("user_details")
    ->where('is_active','!=','2')
    ->get();
    if(sizeof($get_contests)>0){
      $contests = $get_contests->toArray();
    }           
    $this->arr_view_data['contests']        = $contests;
    $this->arr_view_data['page_title']      = "Manage";
    $this->arr_view_data['module_title']    = "All Contest";
    $this->arr_view_data['module_url_path'] = $this->module_url_path.'/manage';
    return view('admin.contests.all.index',$this->arr_view_data);
  }
  public function show(Request $request,$enc_id='')
  {
    $arr_info    = [];
    if($enc_id == '')
    {
      return redirect()->back();
    }
    $obj_contest = $this->BaseModel
    ->with("category_details")
    ->with("user_details")
    ->with("client_user_details")
    ->where('id',base64_decode($enc_id))
    ->first();
    if($obj_contest)
    {
      $arr_info = $obj_contest->toArray();
    }
    $this->arr_view_data['arr_info']              = $arr_info;
    $this->arr_view_data['page_title']            = "Contest Details";
    $this->arr_view_data['module_url_path']       = $this->module_url_path;
    return view('admin.contests.all.show',$this->arr_view_data);
  }
  public function load_entries(Request $request,$enc_id='')
  {
    $arr_info    = [];
    if($enc_id == '')
    {
      return redirect()->back();
    }
    $contest_name        = $this->BaseModel
    ->where('id',base64_decode($enc_id))->first();
    
    $obj_contest_entries = $this->ContestEntryModel
    ->with("expert_details")
    ->with("contest_details")
    ->where('contest_id',base64_decode($enc_id))
    ->get();
    if($obj_contest_entries)
    {
      $arr_info = $obj_contest_entries->toArray();
    }
    $this->arr_view_data['contest_title']   = isset($contest_name['contest_title'])?$contest_name['contest_title']:'N/A';
    $this->arr_view_data['arr_info']        = $arr_info;
    $this->arr_view_data['page_title']      = "Contest Entries List";
    $this->arr_view_data['module_title']    = "All Contest Entries";
    $this->arr_view_data['module_url_path'] = $this->module_url_path.'/manage';
    return view('admin.contests.all.contest_entry_index',$this->arr_view_data);

  }
  public function show_entry_details(Request $request,$enc_id='')
  {
    if($enc_id=='')
    {
      Session::flash('error',"Please try again.");
      return redirect()->back();
    }
    $obj_contest_entry_details = $this->ContestEntryModel
    ->with(['contest_details','contest_entry_files','contest_entry_files_rating','client_user_details','expert_details'])
    ->where('id',base64_decode($enc_id))->first();
    
    if($obj_contest_entry_details)
    {
      $arr_info = $obj_contest_entry_details->toArray();
    }
    $this->arr_view_data['arr_info']              = isset($arr_info)?$arr_info:[];
    $this->arr_view_data['page_title']            = "Contest Entry Details";
    $this->arr_view_data['module_url_path']       = $this->module_url_path;
    // dd($arr_info);
    return view('admin.contests.all.Show_contest_entry',$this->arr_view_data);
  }
  public function remove_file(Request $request)
  {
    $outputdata = 'false';
    $id        = $request->input('file_id',null);
    $file_name = $request->input('file_name',null);
    if($id == '')
    {
      Session::flash('error','Something goes wrong');
      return redirect()->back();  
    }
    $is_file_exist   = file_exists('public/uploads/front/postcontest/send_entry/'.$file_name); 
    if($is_file_exist)
    {
      $delete_file = $this->ContestEntryImagesModel->where('id',$id)->update(['is_admin_deleted'=>'1']);
      if($delete_file)
      {
        // unlink('uploads/front/postcontest/send_entry/'.$file_name);
        Session::flash('success','File removed successfully');
        $outputdata = 'true';  
      }
    }
    else
    {
      Session::flash('error','Sorry File Could not be removed');
      $outputdata = 'false';  
    }
    echo $outputdata;
  }
} 