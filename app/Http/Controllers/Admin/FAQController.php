<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FAQModel;
use App\Common\Services\LanguageService;  

use Validator;
use Session;

class FAQController extends Controller
{
	function __construct(FAQModel $faq_model,LanguageService $langauge)
	{
		$this->arr_data                 = [];
		$this->admin_panel_slug         = config('app.project.admin_panel_slug');
		$this->admin_url_path           = url(config('app.project.admin_panel_slug'));
		$this->module_url_path          = $this->admin_url_path."/faq";
		$this->module_title             = "FAQ";
		$this->module_view_folder       = "admin.faq";
		$this->module_icon              = "fa fa-question-circle";
		$this->FAQModel           		= $faq_model;	
		$this->BaseModel                = $faq_model;
		$this->ip_address               = isset($_SERVER['REMOTE_ADDR'])?$_SERVER['REMOTE_ADDR']:false;
		$this->LanguageService = $langauge;
	} 
	/*
    | Index : Display listing of pages
    | auther :Sagar Sainkar
    | Date : 04/05/2016
    | 
    */
    public function index()
    {
        $arr_lang   =  $this->LanguageService->get_all_language();  
        $obj_question = $this->FAQModel->get();
        if($obj_question != FALSE)
        {
            $arr_question = $obj_question->toArray();
        }
        $this->arr_view_data['arr_question'] = $arr_question;
        $this->arr_view_data['page_title']   = "Manage ".$this->module_title;
        $this->arr_view_data['module_title'] = $this->module_title;
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.faq.index',$this->arr_view_data);
    }
    /*
    | Create : create new page
    | auther :Sagar Sainkar
    | Date : 04/05/2016
    | 
    */ 
    public function create()
    {

        $this->arr_view_data['arr_lang'] = $this->LanguageService->get_all_language();
        $this->arr_view_data['page_title']   = "Create ".$this->module_title;
        $this->arr_view_data['module_title'] = $this->module_title;
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('admin.faq.create',$this->arr_view_data);
    }
    /*
    | store() : store page details
    | auther : Sagar Sainkar
    | Date : 04/05/2016
    | @param  \Illuminate\Http\Request  $request
    | 
    */
    public function store(Request $request)
    {
        $form_data = array();

        $form_data = $request->all();

        $arr_rules['question_name_en']    = "required";  
        $arr_rules['question_ans_en']     = "required";  
        $arr_rules['faq_type_en']         = "required";  

        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
             return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = $request->all();

        $arr_data = array();
        $arr_data['question_slug'] = str_slug($form_data['question_name_en']);
        $arr_data['faq_type']      = $form_data['faq_type_en'];
        $arr_data['is_active'] = 1;            

        $duplication=$this->FAQModel->where('question_slug',$arr_data['question_slug'])->count();

        if ($duplication) 
        {
          Session::flash('error', trans('This page already exist.') );
          return redirect()->back()->withInput($request->all());
        }
        
        $question    = $this->FAQModel->create($arr_data);

        $question_id = $question->id;

        /* Fetch All Languages*/
        $arr_lang =  $this->LanguageService->get_all_language();

        if(sizeof($arr_lang) > 0 )
        {
            foreach ($arr_lang as $lang) 
            {            
                $arr_data       = array();
                $question_name  = 'question_name_'.$lang['locale'];
                $question_ans   = 'question_ans_'.$lang['locale'];

                if( isset($form_data[$question_name]) && $form_data[$question_name] != '')
                { 
                    $translation = $question->translateOrNew($lang['locale']);

                    $translation->question_name   = ucfirst($form_data[$question_name]);
                    $translation->question_ans    = $form_data[$question_ans];
                    $translation->question_id     = $question_id;

                    $translation->save();

                    Session::flash('success','Question Created Successfully.');
                }
            }//foreach
        } //if
        else
        {
            Session::flash('error','Problem Occured, While Creating question.');
            
        }
        return redirect()->back();
    }
    /*
    | edit() : edit page details
    | auther : Sagar Sainkar
    | Date : 04/05/2016    
    | 
    */
    public function edit($enc_id)
    {
        $id = base64_decode($enc_id);
        $arr_lang = $this->LanguageService->get_all_language();      

        $obj_question = $this->FAQModel->where('id', $id)->with(['translations'])->first();

        $arr_question = [];

        if($obj_question)
        {
           $arr_question = $obj_question->toArray(); 
           /* Arrange Locale Wise */
           $arr_question['translations'] = $this->arrange_locale_wise($arr_question['translations']);
        }

        /*echo '<pre>';
        print_r($arr_question);die();*/

        $this->arr_view_data['enc_id'] = $enc_id;
        $this->arr_view_data['arr_lang'] = $this->LanguageService->get_all_language();          
        $this->arr_view_data['arr_question'] = $arr_question;  
        $this->arr_view_data['page_title']   = "Edit ".$this->module_title;
        $this->arr_view_data['module_title'] = $this->module_title;
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        return view('admin.faq.edit',$this->arr_view_data);  
    }

    /*
    | update() : update page details
    | auther : Sagar Sainkar
    | Date : 04/05/2016
    | @param  \Illuminate\Http\Request  $request
    | 
    */
    public function update(Request $request, $enc_id)
    {
        $page_id = base64_decode($enc_id);
        $arr_rules = array();
        $status = FALSE;

        $arr_rules['question_name_en'] = "required";
        $arr_rules['question_ans_en']  = "required";

        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = array();
        $form_data = $request->all(); 
      
         /* Get All Active Languages */ 

        $duplication=$this->FAQModel->where('question_slug',str_slug($form_data['question_name_en']))->whereNotIn('id',[$page_id])->count();

        if ($duplication) 
        {
          Session::flash('error', trans('This page already exist.') );
          return redirect()->back()->withInput($request->all());
        }

        $arr_lang = $this->LanguageService->get_all_language();
        $pages = $this->FAQModel->where('id',$page_id)->first();

        if ($pages) 
        {
            $arr_data = array();
            $arr_data['question_slug'] = str_slug($form_data['question_name_en']);
            $arr_data['faq_type']      = isset($form_data['faq_type_en'])?$form_data['faq_type_en']:"";

            $question    = $pages->update($arr_data);
        }
        else
        {
            Session::flash('error','Error while updating page.');       
            return redirect()->back();
        }
        

         /* Insert Multi Lang Fields */

        if(sizeof($arr_lang) > 0)
        { 
            foreach($arr_lang as $i => $lang)
            {
                $translate_data_ary = array();
                $title = 'question_name_'.$lang['locale'];

                if(isset($form_data[$title]) && $form_data[$title]!="")
                {
                    /* Get Existing Language Entry */
                    $translation = $pages->getTranslation($lang['locale']);    
                    if($translation)
                    {
                        $translation->question_name       =  ucfirst($form_data['question_name_'.$lang['locale']]);
                        $translation->question_ans        =  $form_data['question_ans_'.$lang['locale']];
                        $status = $translation->save();                       
                    }  
                    else
                    {
                        /* Create New Language Entry  */
                        $translation     = $pages->getNewTranslation($lang['locale']);
                        $translation->question_id  =  $page_id;
                        $translation->question_name       =  ucfirst($form_data['question_name_'.$lang['locale']]);
                        $translation->question_ans        =  $form_data['question_ans_'.$lang['locale']];
                        $status = $translation->save();
                    } 
                }   
            }
            
        }
        if ($status) 
        {
            Session::flash('success','Question updated successfully.');    
        }
        else
        {
            Session::flash('error','Error while updating question.');       
        }
        return redirect()->back();
    }


    /*
    | Following Fuctions for active ,deactive and delete
    | auther :Sagar Sainkar
    | Date : 04/05/2016
    | 
    */ 
    public function activate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while question activation.');
            return redirect()->back();
        }

        if($this->perform_activate(base64_decode($enc_id)))
        {
            Session::flash('success','Question activated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while question activation.');
        }

        return redirect()->back();
    }

    public function deactivate($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while question deactivation.');
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            Session::flash('success','Question deactivated successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while question deactivation.');
        }

        return redirect()->back();
    }

    public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            Session::flash('error','Problem occured while question deletion.');
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success','Question deleted successfully.');
        }
        else
        {
            Session::flash('error','Problem occured while question deletion.');
        }

        return redirect()->back();
    }


    public function perform_activate($id)
    {
        if ($id) 
        {
            $question = $this->FAQModel->where('id',$id)->first();
            if($question)
            {
                return $question->update(['is_active'=>1]);
            }
        }
        return FALSE;

    }

    public function perform_deactivate($id)
    {
        if ($id) 
        {
            $question = $this->FAQModel->where('id',$id)->first();
            if($question)
            {
                return $question->update(['is_active'=>0]);
            }
        }

        return FALSE;
    }

    public function perform_delete($id)
    {
        if ($id) 
        {
            $question= $this->FAQModel->where('id',$id)->first();
            if($question)
            {
                return $question->delete();
            }
        }
        return FALSE;
    }
   

   /*
    | multi_action: Following Fuctions for active ,deactive and delete for multiple records
    | auther :Sagar Sainkar
    | Date : 04/05/2016
    | 
    */ 

    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem occured, while doing multi action.');
            return redirect()->back();
        }

        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Session::flash('success','Question(s) deleted successfully.');
            } 
            elseif($multi_action=="activate")
            {
               $this->perform_activate(base64_decode($record_id)); 
               Session::flash('success','Question(s) activated successfully');               
            }
            elseif($multi_action=="deactivate")
            {
               $this->perform_deactivate(base64_decode($record_id));    
               Session::flash('success','Question(s) blocked successfully.');
            }
        }

        return redirect()->back();
    }

    public function arrange_locale_wise(array $arr_data)
    {
        if(sizeof($arr_data)>0)
        {
            foreach ($arr_data as $key => $data) 
            {
                $arr_tmp = $data;
                unset($arr_data[$key]);

                $arr_data[$data['locale']] = $data;                    
            }

            return $arr_data;
        }
        else
        {
            return [];
        }
    }
}
