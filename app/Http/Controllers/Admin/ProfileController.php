<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models\AdminProfileModel;
use App\Models\UserModel;

use Session;
use Validator;
use Sentinel;



class ProfileController extends Controller
{

    public function __construct(
                                    AdminProfileModel $admin_profile,
                                    UserModel $user_model
                                )
    {
        $this->AdminProfileModel = $admin_profile;
        $this->UserModel         = $user_model;
        $this->arr_view_data     = [];
        $this->admin_panel_slug  = config('app.project.admin_panel_slug');
        $this->module_url_path   = url(config('app.project.admin_panel_slug')."/profile");
    }

    /*
    | Index : Display profile details
    | auther :Sagar Sainkar
    | Date : 
    */ 
    
    public function index()
    {
    	$page_title = "Update Profile";
    	$admin_img_path = url('/').'/uploads/admin/profile/';

        $user = Sentinel::check();
        if($user)
        {
            if($user->inRole('admin') || $user->inRole('subadmin'))
            {
                $arr_profile = $this->AdminProfileModel->where('user_id',$user->id)->first();

                if($arr_profile)
                {
                    $arr_profile = $arr_profile->toArray(); 
                }
                $this->arr_view_data['arr_profile'] = $arr_profile;
                $this->arr_view_data['admin_img_path'] = $admin_img_path;
                $this->arr_view_data['page_title'] = $page_title;
                $this->arr_view_data['module_url_path'] = $this->module_url_path;

                return view('admin.profile.index',$this->arr_view_data);
            }
            else
            {
                Session::flash('error','You don\'t have sufficient privileges.');
            }
        }
        else
        {
            Session::flash('error','Please login to your account.');
        }
    	
        return redirect($this->admin_panel_slug.'/login');
    	
    }
     

    /*
    | update : update profile details
    | auther :Sagar Sainkar
    | Date : 
    | 
    */ 

    public function update(Request $request)
    {
        $arr_rules=array();
        $arr_rules['name'] = "required";
        $arr_rules['email'] = "required|email";
        $arr_rules['contact_number'] = "required";
        $arr_rules['address'] = "required";
        
        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
             return back()->withErrors($validator)->withInput();
        }

         $form_data = array();
         $arr_data  = array();
         $form_data = $request->all();

         $arr_data['name'] = $form_data['name'];
         /*$arr_data['contact_email'] = $form_data['contact_email'];*/
         $arr_data['contact_number'] = $form_data['contact_number'];
         $arr_data['fax'] = $form_data['fax'];
         $arr_data['address'] = $form_data['address'];
         $arr_data['image'] = $form_data['old_profile_image'];

         /*file uploade code for laravel auther : sagar sainkar*/
         if ($request->hasFile('image')) 
         {  
            $img_valiator = Validator::make(array('image'=>$request->file('image')),array(
                                                'image' => 'mimes:png,jpeg,gif')); 

            if ($request->file('image')->isValid() && $img_valiator->passes())
            {

                list($width, $height) = getimagesize($form_data['image']);
                if ($width<=300 && $height<=300) 
                {
                
                    $company_logo_name = $form_data['image'];
                    $imageExtension = $request->file('image')->getClientOriginalExtension();
                    $imageName = sha1(uniqid().$company_logo_name.uniqid()).'.'.$imageExtension;
             
                    $request->file('image')->move(
                        base_path() . '/public/uploads/admin/profile/', $imageName
                    );

                    $arr_data['image'] = $imageName;
                }
                else
                {
                     Session::flash("error","Please upload image with size less than 300*300.");
                     return back()->withErrors($validator)->withInput($request->all());
                }
            }
            else
            {
                 Session::flash("error","Please upload valid image.");
                 return back()->withErrors($validator)->withInput($request->all());
            }
        }
        /*file uploade code end here*/ 

        $user = Sentinel::check();
        
        if($user)
        {
            $status = $this->AdminProfileModel->where('user_id',$user->id)->update($arr_data);

            $user = Sentinel::check();
       
            if($user)
            {
                $update_arr        = array('email' => $form_data['email']);
                $check_email_exist = $this->UserModel->where('email',$form_data['email'])
                                                     ->where('id','!=',$user->id)
                                                     ->count();
               
                if($check_email_exist)
                {
                     Session::flash("error","This Email Id Already Exist."); 
                     return redirect()->back(); 
                }
                else
                {
                    $update_status = $this->UserModel->where('id',$user->id)
                                                     ->update($update_arr);
                   
                }
            }
        
        }

        if($status && $update_status) 
        {
            Session::flash("success","Profile Updated Successfullly.");
        }
        else
        {
            Session::flash("error","Error while updating Profile.");  
        }

        return redirect()->back();
    }
}
