<?php

namespace App\Http\Controllers\Front;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App;

use App\Models\ExpertsModel;
use App\Models\ProjectpostModel;

use App\Models\CountryModel;
use App\Models\SkillsModel;
use App\Models\ReviewsModel;

use App\Common\Services\ExpertSearchService;

use App\Common\Services\LanguageService;  
use App\Models\CategoriesModel;  
use App\Models\ProfessionModel; 
use App\Models\InviteProjectModel;
use App\Models\ExpertsSpokenLanguagesModel;
use App\Models\ProjectsBidsModel;
use App\Models\SubCategoriesModel;


use Validator;
use Session;
use Mail;
use sentinel;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class ExpertListingController extends Controller
{
  public function __construct(ExpertsModel $experts,
                              ProjectpostModel $projects,
                              CountryModel $country, 
                              ExpertSearchService $expert_search,
                              SkillsModel $skill,
                              ReviewsModel $review,
                              CategoriesModel $categories,
                              ProfessionModel $Profession,
                              LanguageService $langauge,
                              InviteProjectModel $InviteProjectModel,
                              ExpertsSpokenLanguagesModel $ExpertsSpokenLanguagesModel,
                              ProjectsBidsModel $ProjectsBidsModel,
                              SubCategoriesModel $SubCategoriesModel
                              )
	{
        $user                              = Sentinel::check();
        $this->user                        = $user;
        $this->arr_view_data               = [];
        $cover_size_path                   = '';
        $this->ExpertsModel                = $experts;
        $this->CountryModel                = $country;
        $this->ProjectpostModel            = $projects;
        $this->ReviewsModel                = $review;
        $this->SkillsModel                 = $skill;
        $this->ExpertSearchService         = $expert_search;
        $this->CategoriesModel             = $categories;
        $this->LanguageService             = $langauge;
        $this->ProfessionModel             = $Profession;
        $this->InviteProjectModel          = $InviteProjectModel;
        $this->ExpertsSpokenLanguagesModel = $ExpertsSpokenLanguagesModel;
        $this->ProjectsBidsModel           = $ProjectsBidsModel;
        $this->SubCategoriesModel          = $SubCategoriesModel;

        $this->module_url_path           = url("experts");
        $this->cover_resize_path         = 'public/uploads/front/cover_image/';
        $this->profile_img_public_path   = url('/public').config('app.project.img_path.profile_image');
        $this->cover_img_public_path     = url('/public').config('app.project.img_path.cover_image');
        $this->portfolio_resize_path     = 'uploads/front/portfolio/';
        $this->portfolio_img_public_path = url('/public').config('app.project.img_path.portfolio_image');

	}
	  /*
    | Comment : Expert listing page
    | auther  : Ashwini K.
    */
    public function index($obj_search_data=FALSE,$obj_skill_search_data=FALSE)
    {   
        //dd($obj_skill_search_data, $obj_search_data);
        $arr_experts   = array();
        $arr_pagination= array();
        $arr_country   = array();

        $obj_experts = $this->ExpertsModel->with(['user_details','country_details','city_details','expert_skills','profession_details','expert_categories'])->whereHas('user_details',function ($query)
        {
          $query->where('is_active',1);
          $query->where('is_available',1);
        })->get();

      //dd($obj_search_data!=FALSE);
        /* chanegs in this section */
        if($obj_search_data != FALSE)
        {
          $arr_experts        = $obj_search_data->toArray();
        }
        else if($obj_skill_search_data != FALSE)
        {
          $arr_experts        = $obj_skill_search_data->toArray();
        }
        else if($obj_experts != FALSE)
        {
          $arr_experts        = $obj_experts->toArray();
        }  


        
        if( isset($arr_experts) && count($arr_experts) > 0 )
        { 
          
          /* default critearea */
              foreach ($arr_experts as $key => $expert_data){  
                /* saving ratings in an array */
                $avg_rating = 0;
                $cnt_completed_project = 0;
                $tbl_last_login = '';
                if($expert_data['user_id']){
                  $arr_profile_data = sidebar_information($expert_data['user_id']);
                  // dd($arr_profile_data);
                  if(isset($arr_profile_data['average_rating'])){ 
                    $avg_rating = $arr_profile_data['average_rating'];
                  } 
                  $tbl_last_login = isset($arr_profile_data['tbl_last_login']) ? $arr_profile_data['tbl_last_login'] : '';
                  /* saving number of completed projects in an array */
                  /*$cnt_completed_project = $this->ProjectpostModel->where('expert_user_id',$expert_data['user_id'])
                                                                  ->where('project_status','3')
                                                                  ->count();*/
                }
                $arr_experts[$key]['rating']                    = $avg_rating;
                $arr_experts[$key]['tbl_last_login']                    = $tbl_last_login;
                /*$arr_experts[$key]['completed_project_count']   = $cnt_completed_project;
                $arr_experts[$key]['rating_and_completed_cnt']  = $avg_rating * $cnt_completed_project;*/
              }
              /* sort array by by rating and number of completed projects DESC order */
              $_sortBy = [];
              foreach ($arr_experts as $key => $row)
              {
                $_sortBy['rating'][$key]                    = $row['rating'];
                $_sortBy['tbl_last_login'][$key]                    = $row['tbl_last_login'];
                /*$_sortBy['completed_project_count'][$key]   = intval($row['completed_project_count']);
                $_sortBy['rating_and_completed_cnt'][$key]  = intval($row['rating_and_completed_cnt']);*/
              }
              if(isset($_sortBy) && count($_sortBy) > 0){
                array_multisort($_sortBy['tbl_last_login'], SORT_DESC, $arr_experts);
                //array_multisort($_sortBy['rating'], SORT_DESC, $arr_experts);
                /*array_multisort($_sortBy['rating'], SORT_DESC, $_sortBy['completed_project_count'], SORT_DESC, $arr_experts);*/
                /*array_multisort($_sortBy['rating_and_completed_cnt'], SORT_DESC, $arr_experts);*/
                //array_multisort($_sortBy['rating_and_completed_cnt'], SORT_DESC, $_sortBy['completed_project_count'], SORT_DESC, $arr_experts);
              }
              //dd($arr_experts);
          /* default critearea */    

          if(Session::has('show_expert_listing_cnt')) { $pagi_cnt = Session::get('show_expert_listing_cnt'); }else { $pagi_cnt = config('app.project.pagi_cnt'); }
          $obj_tmp_experts = $this->make_pagination_links($arr_experts,$pagi_cnt)->appends(request()->query());
          $arr_experts     = $obj_tmp_experts->toArray();
          $arr_pagination  = clone $obj_tmp_experts;
        }
        
         $obj_country  = $this->CountryModel
                             ->whereHas('country_details', function($q){
                                      $q->join('users','users.id','=','experts.user_id');
                                                        $q->where('users.is_active','1');
                                                        $q->where('users.is_available','1');
                                     })
                             ->with(['country_details' => function($q){
                                  $q->join('users','users.id','=','experts.user_id');
                                  $q->where('users.is_active','1');
                                  $q->where('users.is_available','1');
                               }])
                             ->where('is_active',1)
                             ->orderBy('country_name','asc')
                             ->get();
        if($obj_country!=FALSE)
        {
          $arr_country  = $obj_country->toArray();
        }
        
        if(app('request')->input('country')!=FALSE)
        {
          $country_id=app('request')->input('country');
          if($country_id!=FALSE)
          {
           $this->arr_view_data['country_id']         = $country_id;
          }
        }
        /* get categories */
        $arr_categories=[];
        $arr_lang     = $this->LanguageService->get_all_language();
        $obj_category = $this->CategoriesModel->where('categories.is_active','1')
                                              ->with(['category_traslation','expert_categories'])
                                              ->whereHas('expert_categories', function($q){})
                                              ->groupBy('categories.id')
                                              ->orderBy('category_slug','asc')
                                              ->get();
        if($obj_category != FALSE)        {
            $arr_categories = $obj_category->toArray();
        }

        $this->arr_view_data['arr_categories'] = $arr_categories; 

        $arr_skills = [];
        $obj_skills = $this->SkillsModel->where('is_active',1)->with(['translations'])->get();
        if($obj_skills != FALSE)
        {
            $arr_skills = $obj_skills->toArray();
        }

        // $arr_categories = [];
        // $obj_categories = $this->CategoriesModel->where('is_active','=','1')->with(['translations'])->get();
        // if($obj_categories != FALSE)
        // {
        //   $arr_categories = $obj_categories->toArray();
        // }
        //$this->arr_view_data['arr_categories']       = $arr_categories;
        $this->arr_view_data['arr_skills']              = $arr_skills;
        $this->arr_view_data['page_title']              = trans('controller_translations.page_title_experts');
        $this->arr_view_data['arr_experts']             = $arr_experts;
        $this->arr_view_data['arr_country']             = $arr_country;
        $this->arr_view_data['arr_pagination']          = $arr_pagination;
        $this->arr_view_data['profile_img_public_path'] = $this->profile_img_public_path;
        $this->arr_view_data['cover_img_public_path']   = $this->cover_img_public_path;
        $this->arr_view_data['module_url_path']         = $this->module_url_path;
        
        $this->arr_view_data['min_hourly_rate']         = 0; 
        $get_max_hourly_rate = $this->ExpertsModel->max('hourly_rate');
        if(isset($get_max_hourly_rate) && $get_max_hourly_rate > 0){
        $this->arr_view_data['max_hourly_rate']         = intval($get_max_hourly_rate) + intval(1);
        }else{
        $this->arr_view_data['max_hourly_rate']         = 100;  
        }
        if(isset($_REQUEST['h_rate_min_val']) && $_REQUEST['h_rate_min_val'] !=""){
          $this->arr_view_data['min_hourly_rate'] = $_REQUEST['h_rate_min_val'];
        }
        
        $arr_lang            = $this->LanguageService->get_all_language();
        $arr_spoken_lang = [];
        $obj_lang = $this->ExpertsSpokenLanguagesModel
                                              ->with(['expert_spoken_language'])
                                              ->whereHas('expert_spoken_language', function($q){
                                                  $q->groupBy('expert_user_id');
                                              })
                                              ->orderBy('language','asc')
                                              ->get();
        if($obj_lang != FALSE){
            $arr_spoken_lang = $obj_lang->toArray();
        }
        $modified_spoken_lang = [];
        $duplicate_arr        = [];
        foreach ($arr_spoken_lang as $key => $value) {
            if(in_array($value['language'], $duplicate_arr)){
               foreach ($duplicate_arr as $dumpkey => $dumpvalue) {
               if($value['language'] == $dumpvalue)
                    if(isset($value['expert_spoken_language'][0])){
                       array_push($modified_spoken_lang[$dumpkey]['expert_spoken_language'],$value['expert_spoken_language'][0]);
                    }
               }
            } else {
              $modified_spoken_lang[$key]['id']                       = $value['id'];; 
              $modified_spoken_lang[$key]['expert_user_id']           = $value['expert_user_id'];
              $modified_spoken_lang[$key]['language']                 = $value['language']; 
              $modified_spoken_lang[$key]['expert_spoken_language']   = $value['expert_spoken_language']; 
              $duplicate_arr[$key]   = $value['language'];
            }
        }
        $this->arr_view_data['arr_lang']                = $modified_spoken_lang;
        $this->arr_view_data['cover_resize_path']       = $this->cover_resize_path;
        $arr_professions     = [];
        $obj_profession      = $this->ProfessionModel->where('profession.is_active','1')
                                                     ->with(['translations','expert_profession'])
                                                     ->whereHas('expert_profession', function($q){})
                                                     ->orderBy('profession_slug','asc')->get();
        if($obj_profession   != FALSE){
            $arr_professions = $obj_profession->toArray();
        }
        $this->arr_view_data['arr_professions'] = $arr_professions; 
        return view('front.expert_listing.listing',$this->arr_view_data);
    }

    public function get_subcategory($enc_id, Request $request)
    {
      $arr_subcategories = [];

      $category_id = base64_decode($enc_id);
      $subcategory = $request->has('subcategory') ? $request->input('subcategory') : '';

      $obj_subcategories = $this->SubCategoriesModel->where(['is_active'=>'1','category_id'=>$category_id])
                                            ->with(['translations'])->get();
      if($obj_subcategories != FALSE)
      {
          $arr_subcategories = $obj_subcategories->toArray();
      }

      if(isset($arr_subcategories) && is_array($arr_subcategories) && count($arr_subcategories)>0)
      {
          $html = '';
          foreach($arr_subcategories as $category){
              $selected = ($subcategory == $category['id']) ? 'selected' : '' ;
              $html .= '<option value="'.$category['id'].'" '.$selected.' > '.$category['subcategory_slug'].' </option>';
          }
          $resp = array('status' => 'success','html'=> $html,'customMsg'=> 'Records found.');
          return response()->json($resp);
      }
      else
      {
          $resp = array('status' => 'fail','customMsg'=> 'Records not found.');
          return response()->json($resp);
      }

    }

    public function show_cnt(Request $Request) 
    {
        \Session::put('show_expert_listing_cnt' , $Request->input('show_cnt'));
        $preurl = url()->previous();
        $explode_url = explode('?page=',$preurl);
        if(empty($explode_url[1])){
            return redirect()->back();
        }
        else{
            $redirect =  $explode_url[0].'?page='.'1';
            return redirect($redirect);
        }
    } 
    /*
    | Comment :Search expert page
    | auther  : Ashwini K.
    */
    public function search_expert(Request $request)
    {
      $form_data        = [];
      $form_data        = $request->all();
      $search_result    = $this->ExpertSearchService->make_filer_search($form_data)->get();
      return $this->index($obj_search_data = $search_result);
    }
    /*
    | Comment : View expert portfolio page
    | auther  : Ashwini K.
    */
    public function view($exp_id=""){
      $arr_portfolio = array();
      $arr_expert    = array();
      $arr_review    = array();
      $arr_pagination= array();
      $arr_portfolio_links = array();
      $review_cnt =$cnt_projects= 0;
       if($exp_id!=FALSE && $exp_id!="")
       {
           $exp_id        = base64_decode($exp_id);
           $obj_expert    = $this->ExpertsModel->where('user_id',$exp_id)
                                               ->with(['expert_skills.skills','expert_portfolio','project_details','user_details','country_details','city_details','expert_spoken_languages','expert_work_experience','expert_certification_details','education_details'])
                                               ->first();
           $review_cnt    = $this->ReviewsModel->where('to_user_id',$exp_id)->with('client_details')->count();
           $cnt_projects  = $this->ProjectpostModel->where('expert_user_id',$exp_id)
                                                   ->count();
           if($obj_expert!=FALSE)
           {
              $arr_expert      = $obj_expert->toArray();
              if(isset($arr_expert['expert_portfolio'])  && count($arr_expert['expert_portfolio']) > 0 ) 
              {
                $arr_portfolio_links = $this->make_pagination_links($arr_expert['expert_portfolio'],6);
                $arr_portfolio       = $arr_portfolio_links->toArray();  
              }
           }
        }
        
        $this->arr_view_data['page_title']               =  trans('controller_translations.page_title_expert_portfolio');
        $this->arr_view_data['arr_expert']               =  $arr_expert;
        $this->arr_view_data['arr_portfolio']            =  $arr_portfolio;
        $this->arr_view_data['arr_portfolio_links']      =  $arr_portfolio_links;
        $this->arr_view_data['review_cnt']               =  $review_cnt;
        $this->arr_view_data['cnt_projects']             =  $cnt_projects;
        $this->arr_view_data['profile_img_public_path']  =  $this->profile_img_public_path;
        $this->arr_view_data['portfolio_img_public_path']=  $this->portfolio_img_public_path;
        $this->arr_view_data['portfolio_resize_path']    =  $this->portfolio_resize_path;
        return view('front.expert_listing.view_portfolio',$this->arr_view_data);
    }
    public function viewreview($exp_id="")
    {
       $arr_expert       = array();
       $review_cnt       = $cnt_projects= 0;
       $arr_review_links = array();
       $arr_review       = array();
       if($exp_id!=FALSE && $exp_id!=""){

           $exp_id        = base64_decode($exp_id);
           $obj_expert    = $this->ExpertsModel->where('user_id',$exp_id)
                                               ->with(['expert_skills.skills','expert_portfolio','project_details','country_details','city_details','expert_spoken_languages','user_details'])
                                               ->first();
                    
           $obj_reviews   = $this->ReviewsModel->where('to_user_id',$exp_id)
                                               ->with('client_details')
                                               ->with(['project_details' => function ($query) {
                                                    $query->select('id','expert_user_id','project_name','project_type');
                                                 }])
                                               ->orderBy('created_at', 'DESC')
                                               ->paginate(6);

           $review_cnt    = $this->ReviewsModel->where('to_user_id',$exp_id)
                                               ->with('client_details')
                                               ->count();  

           
           $cnt_projects  = $this->ProjectpostModel->where('expert_user_id',$exp_id)
                                                   ->count();
           
           if($obj_expert!=FALSE)
           {
              $arr_expert      = $obj_expert->toArray();
           }


           if($obj_reviews!=FALSE)
           {
             $arr_review_links= clone $obj_reviews;
             $arr_review = $obj_reviews->toArray();
           }
        }

        $this->arr_view_data['page_title']               = trans('controller_translations.page_title_expert_portfolio');
        $this->arr_view_data['arr_expert']               =  $arr_expert;
        
        $this->arr_view_data['arr_review']               =  $arr_review;
        $this->arr_view_data['review_cnt']               =  $review_cnt;
        $this->arr_view_data['cnt_projects']             =  $cnt_projects;


        $this->arr_view_data['arr_review_links']         =  $arr_review_links;

        $this->arr_view_data['profile_img_public_path']  =  $this->profile_img_public_path;
        $this->arr_view_data['portfolio_img_public_path']=  $this->portfolio_img_public_path;
        $this->arr_view_data['portfolio_resize_path']    =  $this->portfolio_resize_path;

        return view('front.expert_listing.view_portfolio_review',$this->arr_view_data);
    }
    /*
      Comments : Skill wise searching by clicking skills on expert portfolio page.
      Auther   : Nayan S.
    */  

    public function search_expert_by_skill($enc_id)
    {
        $skill_id = base64_decode($enc_id);

        $obj_skill_search_data = $this->ExpertsModel->whereHas('expert_skills',function ($query) use ($skill_id)
                                           {
                                             $query->where('skill_id',$skill_id);
                                           })
                                          ->whereHas('user_details',function ($query)
                                           {
                                             $query->where('is_active',1);
                                             $query->where('is_available',1);
                                           })
                                          ->with(['user_details','country_details','city_details','expert_skills','profession_details'])
                                          ->get();

      return $this->index($obj_search_data=FALSE,$obj_skill_search_data);
    } 

    /*
      Comments : Skill wise searching by clicking skills on expert portfolio page.
      Auther   : Nayan S.
    */  

    public function search_expert_by_country($enc_id)
    {
        $country_id = base64_decode($enc_id);

        $obj_search_data = $this->ExpertsModel->where('country',$country_id)
                                              ->whereHas('user_details',function ($query)
                                               {
                                                 $query->where('is_active',1);
                                                 $query->where('is_available',1);
                                               })
                                              ->with(['user_details','country_details','city_details','expert_skills','profession_details'])
                                              ->get();

      return $this->index($obj_search_data,$obj_skill_search_data=FALSE);
    } 
    /* get autocompleted data */
    public function get_autocomplete_data(Request $request){
      $arr_property = $experts = $arr_experts = [];
      $term         = $request->input('term');
      /*$obj_result   = $this->SkillsModel->whereHas('skill_traslation',function ($query) use($term) {
                                              $query->where('skill_name','like',"%".$term."%");
                                            })
                                            ->with(['skill_traslation'=> function ($query) use($term) {
                                               $query->where('skill_name','like',"%".$term."%");
                                            }])->get();
      if($obj_result){
        foreach ($obj_result->toArray() as $key => $value) {
          $locale =  \App::getLocale();
          if($locale == 'en') {
            if(isset($value['skill_traslation'][0]['skill_name'])){ 
              $arr_skill[$key]['label'] = $value['skill_traslation'][0]['skill_name'];
              $arr_skill[$key]['id']    = $value['id'];
            } 
          } 
          else if( $locale == 'de' ) {
            if(isset($value['skill_traslation'][1]['skill_name'])){ 
              $arr_skill[$key]['label'] = $value['skill_traslation'][0]['skill_name'];
              $arr_skill[$key]['id']    = $value['id'];
            }
          }  
        }
      }*/
      $obj_experts = $this->ExpertsModel
                     ->join('users','users.id' , '=' , 'experts.user_id')
                     ->where('users.is_active',1)
                     ->where('users.is_available',1)
                     ->with(['user_details'])->whereHas('user_details',function ($query){
                        $query->where('is_active',1);
                        $query->where('is_available',1);
                      })
                     ->groupBy('experts.user_id')
                     ->get();

      if($obj_experts != FALSE){
        $arr_experts        = $obj_experts->toArray();
        if(isset($arr_experts) && (sizeof($arr_experts)>0)){ 
          foreach($arr_experts as $key => $expert){
            $id         = isset($expert['user_details']['role_info']['id'])?$expert['user_details']['role_info']['id']:'0';
            $first_name = isset($expert['user_details']['role_info']['first_name'])?$expert['user_details']['role_info']['first_name']:'Unknown user';
            $last_name  = isset($expert['user_details']['role_info']['last_name'])?substr($expert['user_details']['role_info']['last_name'],0,1).'.':'';
            $experts[$key]['label'] = $first_name.' '.$last_name;
            $experts[$key]['id']    = $id;
          }
        } 
      }
      echo json_encode( $experts );
    }
    /* THis function makes pagination array */
    public function make_pagination_links($items,$perPage)
    {
        $pageStart = \Request::get('page', 1);
        // Start displaying items from this number;
        $offSet = ($pageStart * $perPage) - $perPage; 

        // Get only the items you need using array_slice
        $itemsForCurrentPage = array_slice($items, $offSet, $perPage, true);

        return new LengthAwarePaginator($itemsForCurrentPage, count($items), $perPage,Paginator::resolveCurrentPage(), array('path' => Paginator::resolveCurrentPath()));
    }  

    public function get_projects(Request $request){
      $html            = '';
      $already_invited = $already_invited_arr = $already_bided = $already_bided_arr = [];
      $client_id = $request->input('client_id');
      if(!empty($request->input('expert_id')) && $request->input('expert_id') !=""){
          $expert_id = base64_decode($request->input('expert_id'));
          $get_already_invited = $this->InviteProjectModel->select('project_id')->where('expert_user_id',$expert_id)->get();
          if($get_already_invited){
             $already_invited = $get_already_invited->toArray(); 
             foreach ($already_invited as $key => $value) {
               $already_invited_arr[] = $value['project_id'];
             }
          }

          $get_already_bided = $this->ProjectsBidsModel->select('project_id')->where('expert_user_id',$expert_id)->get();
          if($get_already_bided){
             $already_bided = $get_already_bided->toArray(); 
             foreach ($already_bided as $key => $value) {
               $already_bided_arr[] = $value['project_id'];
             }
          }

          $arr_projects = []; 
          $get_experts  = get_expert($expert_id);
          $arr_projects = ProjectpostModel::where('client_user_id',$client_id)
                                            ->where(function ($q){
                                                $q->where('project_status','=','1');
                                                $q->where('projects.is_hire_process','NO');
                                                $q->where('projects.bid_closing_date','>',date('Y-m-d'));
                                                $q->orWhere('project_status','=','2');
                                            }) 
                                            ->with('skill_details','project_skills.skill_data');

          if(isset($get_experts['expert_skills']) && $get_experts['expert_skills'] !=""){
            $skills = $get_experts['expert_skills'];
                  $arr_projects =$arr_projects->where('projects.project_status','2')->where('projects.is_hire_process','NO')->where('projects.bid_closing_date','>',date('Y-m-d'))
                  ->where(function ($q) use($skills) 
            {
              $q->whereHas('project_skills.skill_data.skill_search', function ($query) use($skills) 
              {
                if (isset($skills) && sizeof($skills)){
                  foreach ($skills as $key => $skill){
                    if($key == 0){
                      $query->where('skill_id',$skill['skill_id']); 
                    } else {              
                      $query->orWhere('skill_id',$skill['skill_id']); 
                    }
                  }
                }
              });
            }); 
         }
         if(isset($get_experts['expert_categories']) && $get_experts['expert_categories'] !=""){
            $categories = $get_experts['expert_categories'];
                  $arr_projects =$arr_projects->where('projects.project_status','2')->where('projects.is_hire_process','NO')->where('projects.bid_closing_date','>',date('Y-m-d'))
                  ->where(function ($q) use($categories) 
            {
                if (isset($categories) && sizeof($categories)){
                  foreach ($categories as $key => $category){
                    if($key == 0){
                      $q->where('category_id',$category['category_id']); 
                    } else {              
                      $q->orWhere('category_id',$category['category_id']); 
                    }
                  }
                }
            }); 
         }
         $arr_projects =  $arr_projects->get();

         if(count($arr_projects) == 0){
          echo 'empty';
          exit;
         }
         $html .='<option value="">-'.trans('client/projects/invite_experts.text_select').'--</option>'; 
          if($arr_projects != FALSE){
            $arr_project        = $arr_projects->toArray();
            foreach($arr_project as $projects){
              $disabled = $style = $title = '';
              if(isset($already_invited_arr)){
                  if(in_array($projects['id'], $already_invited_arr)){
                    $disabled = 'disabled';
                    $style    = 'style="cursor:no-drop;"';
                    $title    = '- <small>(Already invited)</small>';
                  }
              }
              if(isset($already_bided_arr)){
                  if(in_array($projects['id'], $already_bided_arr)){
                    $disabled = 'disabled';
                    $style    = 'style="cursor:no-drop;"';
                    $title    = '- <small> ('.trans('client/projects/invite_experts.text_bid_already_placed').' )</small>';
                  }
              }  
              if(isset($projects['project_handle_by']) && $projects['project_handle_by'] == 2 && $this->user->inRole('client')){
                     $disabled = 'disabled';
                     $style    = 'style="cursor:no-drop;"';
                     $title    = '- <small>(Managed by manager)</small>';
              } 
              $html .='<option '.$disabled.' '.$style.' value="'.base64_encode($projects['id']).'">'.$projects['project_name'].$title.'</option>';
            }
          }  
          echo $html;
      } else {
          echo false; 
          exit;
      }
    }
} //  end Class
