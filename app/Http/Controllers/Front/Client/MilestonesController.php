<?php
namespace App\Http\Controllers\Front\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\ClientsModel;
use App\Models\ProjectpostModel;
use App\Models\MilestonesModel;
use App\Models\MilestoneReleaseModel;
use App\Models\SubscriptionUsersModel;
use App\Models\SiteSettingModel;
use App\Models\ProjectWalletsModel;
use App\Models\NotificationsModel;
use App\Common\Services\WalletService;
use App\Models\TransactionsModel;
use App\Models\UserModel;
use App\Models\UserWalletModel;
use App\Models\CurrencyModel;
use App\Common\Services\MailService;
use App\Models\MilestoneRefundRequests;
use Sentinel;
use Validator;
use Session;
use URL;
use Mail;
use Lang;
use Redirect;
class MilestonesController extends Controller
{
    public $arr_view_data;
    public function __construct(  ProjectpostModel $projectpost,
                                  ClientsModel $clients,
                                  MilestonesModel $milestones,
                                  MilestoneReleaseModel $milestone_release,
                                  SubscriptionUsersModel $subscription_users,
                                  SiteSettingModel $site_settings,
                                  NotificationsModel $notifications,
                                  WalletService $WalletService,
                                  ProjectWalletsModel $ProjectWalletsModel,
                                  TransactionsModel $TransactionsModel,
                                  UserModel $UserModel,
                                  MilestoneRefundRequests $MilestoneRefundRequests,
                                  MailService $MailService
                                  
                                )
    {
      $this->arr_view_data = [];
      if(! $user = Sentinel::check()) {
        return redirect('/login');
      }
      $this->user_id                 = $user->id;
      $this->user                    = $user;
      $this->ClientsModel            = $clients;
      $this->ProjectpostModel        = $projectpost;
      $this->MilestonesModel         = $milestones;
      $this->CurrencyModel           = new CurrencyModel;
      $this->MilestoneReleaseModel   = $milestone_release;
      $this->SubscriptionUsersModel  = $subscription_users;
      $this->SiteSettingModel        = $site_settings;
      $this->NotificationsModel      = $notifications;
      $this->ProjectWalletsModel     = $ProjectWalletsModel;
      $this->WalletService           = $WalletService;
      $this->TransactionsModel       = $TransactionsModel;
      $this->UserModel               = $UserModel;
      $this->UserWalletModel         = new UserWalletModel;
      $this->MilestoneRefundRequests = $MilestoneRefundRequests;
      $this->MailService             = $MailService;

      $this->view_folder_path        = 'milestones';
      $this->module_url_path         = url("/client/projects");
    }
    /*
      Comment : Stores milestones information of project.
      Auther  : Nayan S.  
    */
    public function store_milestone(Request $request)
    { 
        $arr_rules                = [];
        $arr_rules['title']       = "required";
        $arr_rules['description'] = "required";
        $arr_rules['amount']      = "required|numeric|min:1";
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }
        $form_data       = [];
        $form_data       = $request->all();
        $arr_milestone   = [];
        $user            = Sentinel::check(); 
        $wallet_details  = 'fail'; 
        //dd($form_data);
        // wallet 
        $logged_user                           = $this->user->toArray();
        if(isset($logged_user['mp_wallet_created']) && $logged_user['mp_wallet_created'] == 'Yes')
        {
           $arr_data = get_user_wallet_details($this->user_id,$form_data['project_currency']);
           
           $wallet_data['mp_user_id']          = $arr_data['mp_user_id'];
           $wallet_data['mp_wallet_id']        = $arr_data['mp_wallet_id'];
           $wallet_data['mp_wallet_created']   = $logged_user['mp_wallet_created'];
           //dd($wallet_data);

           $wallet_user_detail = $this->WalletService->get_user_details($wallet_data['mp_user_id']);
           $get_wallet_details = $this->WalletService->get_wallet_details($wallet_data['mp_wallet_id']);    

            if($wallet_user_detail)
            {
              if($get_wallet_details)
              {
                $debited_wallet_amout = $get_wallet_details->Balance->Amount/100;
                $milestone_amout      = (float) $form_data['amount'];

                if($debited_wallet_amout < $milestone_amout)
                {
                    $amount = (float) $milestone_amout - (float)$debited_wallet_amout;

                    $arr_new_service_charge =  get_service_charge($amount,$form_data['project_currency']);

                    $service_charge_payin = isset($arr_new_service_charge['service_charge']) ? $arr_new_service_charge['service_charge'] : 0;

                    $project_id = base64_decode($form_data['project_id']);
                    if(isset($arr_data) && count($arr_data)>0)
                    {
                      $transaction_inp['mp_user_id']      = isset($arr_data['mp_user_id'])?$arr_data['mp_user_id']:'';
                      $transaction_inp['mp_wallet_id']    = isset($arr_data['mp_wallet_id'])?$arr_data['mp_wallet_id']:'';
                      $transaction_inp['ReturnURL']       = url('/client/projects/store_milestone_return?project_id='.$project_id.'&expert_user_id='.$form_data['expert_user_id'].'&project_currency='.$form_data['project_currency'].'&amount='.$form_data['amount'].'&client_user_id='.$form_data['client_user_id'].'&title='.$form_data['title'].'&description='.$form_data['description'].'&due_date='.$form_data['due_date'].'&service_charge_payin='.$service_charge_payin);
                      $transaction_inp['currency_code']   = $form_data['project_currency'];
                      
                    }
                    
                    $total_amount = (float)$amount + (float)$service_charge_payin;

                    $transaction_inp['amount']          = $total_amount;
                    $transaction_inp['fee']             = 0;

                    $mp_add_money_in_wallet             = $this->WalletService->payInWallet($transaction_inp);
                    //dd($mp_add_money_in_wallet);
                    if(isset($mp_add_money_in_wallet->Status) && $mp_add_money_in_wallet->Status == 'CREATED')
                    {   
                        return Redirect::to($mp_add_money_in_wallet->ExecutionDetails->RedirectURL)->send();
                    } 
                    else 
                    {
                      
                      if(isset($mp_add_money_in_wallet) && $mp_add_money_in_wallet == false){
                        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
                        return redirect()->back();
                      }else{
                        Session::flash('error',$mp_add_money_in_wallet);
                        return redirect()->back();
                      }
                    }
                  // Session::flash('error',trans('common/wallet/text.text_you_dont_have_sufficinet_balance_in_your_wallet_to_create_a_Milestone'));
                  // return redirect()->back();
                }

                $project_id = base64_decode($form_data['project_id']);
                $get_project_wallet_details = $this->ProjectWalletsModel->where('project_id',$project_id)->first(); 
                if($get_project_wallet_details == null)
                {
                  Session::flash('error',trans('common/wallet/text.text_project_wallet_not_created_yet'));
                  return redirect()->back(); 
                }

                $obj_credit_data = $this->UserWalletModel->where('user_id',$form_data['expert_user_id'])
                                             ->where('currency_code',$form_data['project_currency'])
                                             ->first();
                if($obj_credit_data)
                {
                  $arr_credit_data = $obj_credit_data->toArray();
                }

                $project_wallet_details = $get_project_wallet_details->toArray(); 

                $project_details            = $this->ProjectpostModel->where('id',base64_decode($form_data['project_id']))->first();
                
                //dd($wallet_data['mp_wallet_id'],$project_details->mp_job_wallet_id);

                //$get_wallet_details                = $this->WalletService->get_wallet_details($wallet_data['mp_wallet_id']); 

                $transaction_inp                             = [];
                $transaction_inp['tag']                      = 'Milestone Create';
                $transaction_inp['debited_UserId']           = $wallet_data['mp_user_id'];
                $transaction_inp['debited_walletId']         = $wallet_data['mp_wallet_id'];
                $transaction_inp['total_pay']                = $form_data['amount'];
                $transaction_inp['currency_code']            = $form_data['project_currency'];
                $transaction_inp['cost_website_commission']  = 0;
                
                $transaction_inp['credited_UserId']          = $wallet_data['mp_user_id'];
                $transaction_inp['credited_walletId']        = (string)$project_details->mp_job_wallet_id;
                $wallet_details                              = 'true';
                // $pay_fees        = $this->WalletService->walletTransfer($transaction_inp);
              }
              else
              {
                Session::flash('error',trans('common/wallet/text.text_project_wallet_not_created_yet'));
                return redirect()->back();
              }             
            } 
            else
            {
              Session::flash('error',trans('common/wallet/text.text_create_your_wallet_first'));
              return redirect()->back();
            } 
        } 
        // End wallet 
        /* checking if milestone created by whom ?*/
        if($user->inRole('client')){
            $arr_milestone['created_by']  = 1;  /* client */
        }else if ($user->inRole('project_manager')){
            $arr_milestone['created_by']  = 2;  /* project_manager */
        }
        $project_id     = base64_decode($form_data['project_id']);
        $client_user_id = base64_decode($form_data['client_user_id']);
        if($request->has('title') && $request->has('description') && $request->has('amount') && $request->has('due_date') && $project_id != "" && $client_user_id!= "")
        {
            $arr_milestone['title']               = $form_data['title'];
            $arr_milestone['cost']                = $form_data['amount'];
            $arr_milestone['description']         = $form_data['description'];
            $time = date('H:i:s');
            $current_date = date('Y-m-d');
            if($current_date == $form_data['due_date']){
              $time = '23:59:59';
            }
            $arr_milestone['milestone_due_date']  = $form_data['due_date'].' '.$time;

            $arr_milestone['project_id']          = $project_id;
            $arr_milestone['client_user_id']      = $client_user_id;
            $arr_milestone['expert_user_id']      = $this->get_expert_from_project($project_id);

            /*this is commission by expert depend on expert subscription current pack*/
            $website_commission                               = $this->get_expert_subscription_commission($project_id);
            /*this is commisson by client if he choose porject handel by project manager*/
            $project_manager_commission                       = $this->get_project_manager_commission($project_id);
            $arr_milestone['project_manager_commission']      = $project_manager_commission;
            $arr_milestone['website_commission']              = $website_commission;
            /*project manager commisson actual cost*/
            $arr_milestone['cost_project_manager_commission'] = ($form_data['amount']*$project_manager_commission)/100;
            /*total cost which client will pay*/
            $arr_milestone['cost_from_client']                = ($form_data['amount']+$arr_milestone['cost_project_manager_commission']);
            /*website commisson actual cost*/
            $arr_milestone['cost_website_commission']         = ($form_data['amount']*$website_commission)/100;            
            /*total cost which expert will get after release milestone*/
            $arr_milestone['cost_to_expert']                  = ($form_data['amount']-$arr_milestone['cost_website_commission']);
            $last_milestone_count                             = 0;
            $get_last_milestone_count                         = $this->MilestonesModel->select('milestone_no')->where('project_id',$project_id)->where('client_user_id' , $client_user_id)->where('expert_user_id' , $arr_milestone['expert_user_id'])->orderBy('id','desc')->limit(1)->first();

            if($get_last_milestone_count == null){
              $last_milestone_count        = 0;
            } else {
              $last_milestone_count        = $get_last_milestone_count->milestone_no; 
            }
            $next_milestone_count          = ($last_milestone_count + 1);
            $arr_milestone['milestone_no'] = $next_milestone_count;
            
            $res_milestone     = 'fail';
            if($wallet_details == 'true')
            {
              $res_milestone                 = $this->MilestonesModel->create($arr_milestone)->id;  
            }
            //dd($res_milestone);
            if($res_milestone != "fail") 
            {
                $invoice_id                          = $this->_generate_invoice_id(); 
                /*First make transaction  */ 
                $arr_transaction['user_id']          = $this->user_id;
                $arr_transaction['invoice_id']       = $invoice_id;
                /*transaction_type is type for transactions 1-Subscription 2-Milestone 3-Release Milestones */
                $arr_transaction['transaction_type'] = 2;
                $arr_transaction['paymen_amount']    = $form_data['amount'];
                $arr_transaction['payment_method']   = 3; // wallet
                $arr_transaction['payment_date']     = date('c');
                $arr_transaction['payment_status']   = 1;

                /* get currency for transaction */
                $arr_currency                        = setCurrencyForTransaction($project_id);
                $arr_transaction['currency']         = isset($arr_currency['currency']) ? $arr_currency['currency'] : '$';
                $arr_transaction['currency_code']    = isset($arr_currency['currency_code']) ? $arr_currency['currency_code'] : 'USD';



                $transaction                         = $this->TransactionsModel->insertGetId($arr_transaction);

                if($transaction)
                {
                    // update invoice id
                    $walletTowallet_transfer         = $this->WalletService->walletTransfer($transaction_inp);
                    $wallet_transaction_details                            = [];
                    $wallet_transaction_details['AuthorId']                = $walletTowallet_transfer->AuthorId;
                    $wallet_transaction_details['TransactionId']           = $walletTowallet_transfer->Id;
                    $wallet_transaction_details['mp_transaction_status']   = $walletTowallet_transfer->Status;
                    $wallet_transaction_details['mp_transaction_type']     = $walletTowallet_transfer->Type;
                    if(isset($walletTowallet_transfer->Status) && $walletTowallet_transfer->Status == 'FAILED'){
                      $this->MilestonesModel->where('id',$res_milestone)->update($wallet_transaction_details);  
                      Session::flash('error',$walletTowallet_transfer->ResultMessage);
                      return redirect()->back();
                    } else if(isset($walletTowallet_transfer->Status) && $walletTowallet_transfer->Status == 'SUCCEEDED'){

                      /* get project name */                       
                      $milestone_project_name     = "";
                      $project_details            = $this->ProjectpostModel->where('id',$project_id)->first(['id','project_name','project_currency']);
                      if ($project_details){
                          $milestone_project_name = isset($project_details->project_name)?$project_details->project_name:'';
                      }

                      /* Create Notification for expert*/
                       $arr_data               =  [];
                       $arr_data['user_id']    =  $arr_milestone['expert_user_id'];
                       $arr_data['user_type']  = '3'; 
                       $arr_data['url']                  = "expert/projects/milestones/".base64_encode($project_id);
                       $arr_data['notification_text_en'] = $milestone_project_name . ' - ' . Lang::get('controller_translations.new_project_milestone_created_by_client',[],'en','en');
                       $arr_data['notification_text_de'] = $milestone_project_name . ' - ' . Lang::get('controller_translations.new_project_milestone_created_by_client',[],'de','en');
                       $arr_data['project_id'] = $project_id;
                       $this->NotificationsModel->create($arr_data); 
                      /* end create Notification for expert*/


                      /* get Expert name */
                      $obj_user = $this->UserModel->where('id',$arr_milestone['expert_user_id'])->first(['id','first_name','last_name','email']);   
                      if ($obj_user){
                        $arr_user_details  = $obj_user->toArray();
                        $expert_last_name  = isset($arr_user_details['role_info']['last_name'])?$arr_user_details['role_info']['last_name']:'';
                        $expert_first_name = isset($arr_user_details['role_info']['first_name'])?$arr_user_details['role_info']['first_name']:'';
                        $expert_email      = isset($arr_user_details['email'])?$arr_user_details['email']:'';
                        $expert_name       = $expert_first_name;
                      }
                      /* mail notification for expert*/
                      //$mail_form               = get_site_email_address();
                      $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';

                      $data                    = [];
                      $data['name']            = isset($expert_name)?$expert_name:'';
                      $data['project_title']   = isset($milestone_project_name)?$milestone_project_name:'';
                      $data['milestone_title'] = isset($arr_milestone['title'])?$arr_milestone['title']:'';
                      $data['currency']        = isset($arr_transaction['currency'])?$arr_transaction['currency']:'$';

                      $payment_status = isset($arr_transaction['payment_status'])?$arr_transaction['payment_status']:'0';
                      

                      if(isset($payment_status) && $payment_status==0){
                       $payment_status ='Pending';
                      }
                      elseif(isset($payment_status) && $payment_status==1){
                       $payment_status ='Paid';
                      }
                      elseif(isset($payment_status) && $payment_status==2){
                       $payment_status ='Paid';
                      }
                      elseif(isset($payment_status) && $payment_status==3){
                       $payment_status ='Lost';
                      }

                      $data['payment_status']  = $payment_status;
                      $data['deposited_cost']  = isset($arr_transaction['paymen_amount'])?$arr_transaction['paymen_amount']:'0';
                      $data['login_url']       = url('/expert/projects/milestones/'.base64_encode($project_id));
                      $data['email_id']        = isset($mail_form)?$mail_form:'';
                      $mail_form               = isset($mail_form)?$mail_form:'';
                      $email_to                = isset($expert_email)?$expert_email:'';
                      $project_name            = isset($milestone_project_name)?$milestone_project_name:'';
                      try{
                            //$mail_status = $this->MailService->send_milestone_deposit_to_expert_email($data);
                          Mail::send('front.email.milestone_deposit_to_expert', $data, function ($message) use ($email_to,$mail_form,$project_name) 
                          {
                                $message->from($mail_form, $project_name);
                                $message->subject($project_name.' : Milestone Deposit');
                                $message->to($email_to); 
                          });
                      }
                      catch(\Exception $e){
                      //Session::Flash('error'   , trans('controller_translations.text_mail_not_sent'));
                      }
                      /* end mail notification for expert*/
                      $wallet_transaction_details['invoice_id']     = $invoice_id;
                      $wallet_transaction_details['status']         = 1;
                      $this->MilestonesModel->where('id',$res_milestone)->update($wallet_transaction_details);  
                      $walletTowallet_transferid = 0;
                      if(isset($wallet_transaction_details['TransactionId']) && $wallet_transaction_details['TransactionId']!=""){
                        $walletTowallet_transferid = $wallet_transaction_details['TransactionId'];
                      }
                      $this->TransactionsModel->where('id',$transaction)->update(['response_data'=>json_encode($walletTowallet_transfer),'WalletTransactionId'=>$walletTowallet_transferid]);  
                    } else {
                      Session::flash('error',trans('controller_translations.error_while_addiing_milestone_information'));
                      return redirect()->back();
                    }
                } 
                //return redirect($this->module_url_path.'/milestones/payment/'.base64_encode($res_milestone));
                Session::flash('success','Milestone created successfully');
                $back_url = $this->module_url_path."/milestones/".base64_encode($project_id);  
                return redirect($back_url);
            } else {
                Session::flash('error',trans('controller_translations.error_while_addiing_milestone_information'));
            }
        } else {   
            Session::flash('error',trans('controller_translations.error_while_addiing_milestone_information'));
        }
        return redirect()->back();
    }

    public function store_milestone_return(Request $request)
    {
          $form_data = $request->all();
          
          $transactionId = isset($form_data['transactionId']) ? $form_data['transactionId']:NULL;

          $project_id = isset($form_data['project_id']) ? base64_encode($form_data['project_id']) : '';
          $redirect_back_url = url('/').'/client/dashboard';
          if($project_id!=''){
              $redirect_back_url = url('/').'/client/projects/milestones/'.$project_id;
          }

          $arr_result = $this->WalletService->viewPayIn($transactionId);
          
          if(isset($arr_result['status']) && $arr_result['status'] == 'error') {
            $msg = isset($arr_result['msg']) ? $arr_result['msg'] : 'Something went, wrong, Unable to process payment, Please try again.';
            Session::flash('error',$msg);  
            return redirect($redirect_back_url);
          }

          $service_charge_payin = isset($form_data['service_charge_payin']) ? (float) $form_data['service_charge_payin']:0;
          
          $user      = Sentinel::check(); 
          $obj_data  = $this->UserWalletModel->where('user_id',$this->user_id)
                                             ->where('currency_code',$form_data['project_currency'])
                                             ->first();
           if($obj_data)
           {
              $arr_data = $obj_data->toArray();
           }
           $logged_user                        = $this->user->toArray(); 
           $wallet_data['mp_user_id']          = $arr_data['mp_user_id'];
           $wallet_data['mp_wallet_id']        = $arr_data['mp_wallet_id'];
           $wallet_data['mp_wallet_created']   = $logged_user['mp_wallet_created'];

           //$get_wallet_details      = $this->WalletService->get_wallet_details($wallet_data['mp_wallet_id']);
           //dd($get_wallet_details);
          //$project_id = base64_decode($form_data['project_id']);
          $get_project_wallet_details = $this->ProjectWalletsModel->where('project_id',$form_data['project_id'])->first(); 
          //dd($get_project_wallet_details);
          if($get_project_wallet_details == null)
          {
            Session::flash('error',trans('common/wallet/text.text_project_wallet_not_created_yet'));
            return redirect($redirect_back_url); 
          }

          $obj_credit_data = $this->UserWalletModel->where('user_id',$form_data['expert_user_id'])
                                       ->where('currency_code',$form_data['project_currency'])
                                       ->first();
          if($obj_credit_data)
          {
            $arr_credit_data = $obj_credit_data->toArray();
          }
          $project_details            = $this->ProjectpostModel->where('id',$form_data['project_id'])->first();
          //dd($project_details);
          $project_wallet_details = $get_project_wallet_details->toArray(); 
          $transaction_inp                             = [];
          $transaction_inp['tag']                      = 'Milestone Create';
          $transaction_inp['debited_UserId']           = $wallet_data['mp_user_id'];
          $transaction_inp['debited_walletId']         = $wallet_data['mp_wallet_id'];
          $transaction_inp['total_pay']                = $form_data['amount'];
          $transaction_inp['currency_code']            = $form_data['project_currency'];
          $transaction_inp['cost_website_commission']  = 0;
          
          $transaction_inp['credited_UserId']          = $arr_credit_data['mp_user_id'];
          $transaction_inp['credited_walletId']        = (string)$project_details->mp_job_wallet_id;
          
          $get_wallet_details = $this->WalletService->get_wallet_details($arr_data['mp_wallet_id']);
     
          $wallet_details                              = 'true';

          //dd($transaction_inp);
          /* checking if milestone created by whom ?*/
        if($user->inRole('client')){
            $arr_milestone['created_by']  = 1;  /* client */
        }else if ($user->inRole('project_manager')){
            $arr_milestone['created_by']  = 2;  /* project_manager */
        }
        $project_id     = $form_data['project_id'];
        $client_user_id = base64_decode($form_data['client_user_id']);
        //dd($project_id,$client_user_id);

        if($request->has('title') && $request->has('description') && $request->has('amount') && $request->has('due_date') && $project_id != "" && $client_user_id!= "")
        {
            // admin service charge payin section
            $arr_admin_details = get_user_wallet_details('1',$form_data['project_currency']);  
            if($arr_admin_details)
            {
              $transaction_admin_inp['tag']                      = $transactionId.'-Milestone Create Service Fee';
              $transaction_admin_inp['debited_UserId']           = $wallet_data['mp_user_id'];           
              $transaction_admin_inp['credited_UserId']          = $arr_admin_details['mp_user_id'];
              $transaction_admin_inp['total_pay']                = $service_charge_payin;              
              $transaction_admin_inp['debited_walletId']         = (string)$wallet_data['mp_wallet_id']; 
              $transaction_admin_inp['credited_walletId']        = (string)$arr_admin_details['mp_wallet_id']; 
              $transaction_admin_inp['cost_website_commission']  = '0';
              $transaction_admin_inp['currency_code']            = $form_data['project_currency'];

              $pay_admin_fees        = $this->WalletService->walletTransfer($transaction_admin_inp);
            }
            

            $arr_milestone['title']               = $form_data['title'];
            $arr_milestone['cost']                = $form_data['amount'];
            $arr_milestone['description']         = $form_data['description'];
            $time = date('H:i:s');
            $current_date = date('Y-m-d');
            if($current_date == $form_data['due_date']){
              $time = '23:59:59';
            }
            $arr_milestone['milestone_due_date']  = $form_data['due_date'].' '.$time;

            $arr_milestone['project_id']          = $project_id;
            $arr_milestone['client_user_id']      = $client_user_id;
            $arr_milestone['expert_user_id']      = $this->get_expert_from_project($project_id);

            /*this is commission by expert depend on expert subscription current pack*/
            $website_commission                               = $this->get_expert_subscription_commission($project_id);
            /*this is commisson by client if he choose porject handel by project manager*/
            $project_manager_commission                       = $this->get_project_manager_commission($project_id);
            $arr_milestone['project_manager_commission']      = $project_manager_commission;
            $arr_milestone['website_commission']              = $website_commission;
            /*project manager commisson actual cost*/
            $arr_milestone['cost_project_manager_commission'] = ($form_data['amount']*$project_manager_commission)/100;
            /*total cost which client will pay*/
            $arr_milestone['cost_from_client']                = ($form_data['amount']+$arr_milestone['cost_project_manager_commission']);
            /*website commisson actual cost*/
            $arr_milestone['cost_website_commission']         = ($form_data['amount']*$website_commission)/100;            
            /*total cost which expert will get after release milestone*/
            $arr_milestone['cost_to_expert']                  = ($form_data['amount']-$arr_milestone['cost_website_commission']);
            $last_milestone_count                             = 0;
            //dd($arr_milestone);
            $get_last_milestone_count                         = $this->MilestonesModel->select('milestone_no')->where('project_id',$project_id)->where('client_user_id' , $client_user_id)->where('expert_user_id' , $arr_milestone['expert_user_id'])->orderBy('id','desc')->limit(1)->first();

            if($get_last_milestone_count == null){
              $last_milestone_count        = 0;
            } else {
              $last_milestone_count        = $get_last_milestone_count->milestone_no; 
            }
            $next_milestone_count          = ($last_milestone_count + 1);
            $arr_milestone['milestone_no'] = $next_milestone_count;
            
            $res_milestone     = 'fail';
            if($wallet_details == 'true')
            {
              $res_milestone                 = $this->MilestonesModel->create($arr_milestone)->id;  
            }
            if($res_milestone != "fail") {
                $invoice_id                          = $this->_generate_invoice_id(); 
                /*First make transaction  */ 
                $arr_transaction['user_id']          = $this->user_id;
                $arr_transaction['invoice_id']       = $invoice_id;
                /*transaction_type is type for transactions 1-Subscription 2-Milestone 3-Release Milestones */
                $arr_transaction['transaction_type'] = 2;
                $arr_transaction['paymen_amount']    = $form_data['amount'];
                $arr_transaction['payment_method']   = 3; // wallet
                $arr_transaction['payment_date']     = date('c');
                $arr_transaction['payment_status']   = 1;

                /* get currency for transaction */
                $arr_currency                        = setCurrencyForTransaction($project_id);
                $arr_transaction['currency']         = isset($arr_currency['currency']) ? $arr_currency['currency'] : '$';
                $arr_transaction['currency_code']    = isset($arr_currency['currency_code']) ? $arr_currency['currency_code'] : 'USD';

                //dd($transaction_inp);

                $transaction                         = $this->TransactionsModel->insertGetId($arr_transaction);

                if($transaction)
                {
                    // update invoice id
                    $walletTowallet_transfer         = $this->WalletService->walletTransfer($transaction_inp);
                    $wallet_transaction_details                            = [];
                    $wallet_transaction_details['AuthorId']                = $walletTowallet_transfer->AuthorId;
                    $wallet_transaction_details['TransactionId']           = $walletTowallet_transfer->Id;
                    $wallet_transaction_details['mp_transaction_status']   = $walletTowallet_transfer->Status;
                    $wallet_transaction_details['mp_transaction_type']     = $walletTowallet_transfer->Type;

                    if(isset($walletTowallet_transfer->Status) && $walletTowallet_transfer->Status == 'FAILED'){
                      $this->MilestonesModel->where('id',$res_milestone)->update($wallet_transaction_details);  
                      Session::flash('error',$walletTowallet_transfer->ResultMessage);
                      return redirect($redirect_back_url);
                    } else if(isset($walletTowallet_transfer->Status) && $walletTowallet_transfer->Status == 'SUCCEEDED'){
                      
                      /* get project name */                       
                      $milestone_project_name     = "";
                      $project_details            = $this->ProjectpostModel->where('id',$project_id)->first(['id','project_name','project_currency']);
                      if ($project_details){
                          $milestone_project_name = isset($project_details->project_name)?$project_details->project_name:'';
                      }

                      /* Create Notification for expert*/
                       $arr_data               =  [];
                       $arr_data['user_id']    =  $arr_milestone['expert_user_id'];
                       $arr_data['user_type']  = '3'; 
                       $arr_data['url']                  = "expert/projects/milestones/".base64_encode($project_id);
                       $arr_data['notification_text_en'] = $milestone_project_name . ' - ' . Lang::get('controller_translations.new_project_milestone_created_by_client',[],'en','en');
                       $arr_data['notification_text_de'] = $milestone_project_name . ' - ' . Lang::get('controller_translations.new_project_milestone_created_by_client',[],'de','en');
                       $arr_data['project_id'] = $project_id;
                       $this->NotificationsModel->create($arr_data); 
                      /* end create Notification for expert*/

                      /* get Expert name */
                      $obj_user = $this->UserModel->where('id',$arr_milestone['expert_user_id'])->first(['id','first_name','last_name','email']);   
                      if ($obj_user){
                        $arr_user_details  = $obj_user->toArray();
                        $expert_last_name  = isset($arr_user_details['role_info']['last_name'])?$arr_user_details['role_info']['last_name']:'';
                        $expert_first_name = isset($arr_user_details['role_info']['first_name'])?$arr_user_details['role_info']['first_name']:'';
                        $expert_email      = isset($arr_user_details['email'])?$arr_user_details['email']:'';
                        $expert_name       = $expert_first_name;
                      }
                      /* mail notification for expert*/
                      //$mail_form               = get_site_email_address();
                      $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';

                      $data                    = [];
                      $data['name']            = isset($expert_name)?$expert_name:'';
                      $data['project_title']   = isset($milestone_project_name)?$milestone_project_name:'';
                      $data['milestone_title'] = isset($arr_milestone['title'])?$arr_milestone['title']:'';
                      $data['currency']        = isset($arr_transaction['currency'])?$arr_transaction['currency']:'$';

                      $payment_status = isset($arr_transaction['payment_status'])?$arr_transaction['payment_status']:'0';
                      

                      if(isset($payment_status) && $payment_status==0){
                       $payment_status ='Pending';
                      }
                      elseif(isset($payment_status) && $payment_status==1){
                       $payment_status ='Paid';
                      }
                      elseif(isset($payment_status) && $payment_status==2){
                       $payment_status ='Paid';
                      }
                      elseif(isset($payment_status) && $payment_status==3){
                       $payment_status ='Lost';
                      }

                      $data['payment_status']  = $payment_status;
                      $data['deposited_cost']  = isset($arr_transaction['paymen_amount'])?$arr_transaction['paymen_amount']:'0';
                      $data['login_url']       = url('/expert/projects/milestones/'.base64_encode($project_id));
                      $data['email_id']        = isset($mail_form)?$mail_form:'';
                      $mail_form               = isset($mail_form)?$mail_form:'';
                      $email_to                = isset($expert_email)?$expert_email:'';
                      $project_name            = isset($milestone_project_name)?$milestone_project_name:'';
                      try{
                            //$mail_status = $this->MailService->send_milestone_deposit_to_expert_email($data);
                          Mail::send('front.email.milestone_deposit_to_expert', $data, function ($message) use ($email_to,$mail_form,$project_name) 
                          {
                                $message->from($mail_form, $project_name);
                                $message->subject($project_name.' : Milestone Deposit');
                                $message->to($email_to); 
                          });
                      }
                      catch(\Exception $e){
                      //Session::Flash('error'   , trans('controller_translations.text_mail_not_sent'));
                      }
                      /* end mail notification for expert*/
                      $wallet_transaction_details['invoice_id']     = $invoice_id;
                      $wallet_transaction_details['status']         = 1;
                      $this->MilestonesModel->where('id',$res_milestone)->update($wallet_transaction_details);  
                      $walletTowallet_transferid = 0;
                      if(isset($wallet_transaction_details['TransactionId']) && $wallet_transaction_details['TransactionId']!=""){
                        $walletTowallet_transferid = $wallet_transaction_details['TransactionId'];
                      }
                      $this->TransactionsModel->where('id',$transaction)->update(['response_data'=>json_encode($walletTowallet_transfer),'WalletTransactionId'=>$walletTowallet_transferid]);  
                    } else {
                      Session::flash('error',trans('controller_translations.error_while_addiing_milestone_information'));
                      return redirect($redirect_back_url);
                    }
                } 
                //return redirect($this->module_url_path.'/milestones/payment/'.base64_encode($res_milestone));
                Session::flash('success','Milestone created successfully');
                $back_url = $this->module_url_path."/milestones/".base64_encode($project_id);  
                return redirect($back_url);
            } else {
                Session::flash('error',trans('controller_translations.error_while_addiing_milestone_information'));
            }
        } else {   
            Session::flash('error',trans('controller_translations.error_while_addiing_milestone_information'));
        }
        return redirect($redirect_back_url);


    }


    private function _generate_invoice_id(){
          $secure      = TRUE;    
          $bytes       = openssl_random_pseudo_bytes(3, $secure);
          $order_token = 'INV'.date('Ymd').strtoupper(bin2hex($bytes));
          return $order_token;
    }
    /*
      Comment : Shows all Milestone for a project.
      Auther  : Nayan S.  
    */
    public function show_project_milestones($enc_id)
    { 
      $project_id = base64_decode($enc_id);
      $this->arr_view_data['mp_job_wallet_id'] = '';
      $project_currency_code = '';

      $project_details  = $this->ProjectpostModel->where('id',$project_id)->first(['id','project_name','project_currency','project_currency_code','mp_job_wallet_id']);

      if ($project_details){
          $project_currency_code = isset($project_details->project_currency_code)?$project_details->project_currency_code:'';
          $arr_currency_data = [];
          $obj_currency_data = $this->CurrencyModel->with('deposite')
                                                    ->where('currency_code',$project_currency_code)
                                                    ->first();
          if($obj_currency_data)
          {
              $arr_currency_data = $obj_currency_data->toArray();
              $this->arr_view_data['arr_currency_data'] = $arr_currency_data;
          }

          $this->arr_view_data['milestone_project_name']          = isset($project_details->project_name)?$project_details->project_name:'';
          $this->arr_view_data['milestone_project_currency']      = isset($project_details->project_currency)?$project_details->project_currency:'';
          $this->arr_view_data['milestone_project_currency_code'] = isset($project_details->project_currency_code)?$project_details->project_currency_code:'';
          $this->arr_view_data['mp_job_wallet_id']                = isset($project_details->mp_job_wallet_id)?$project_details->mp_job_wallet_id:'';
      }

      $bid_deatils = $this->ProjectpostModel->select('id','expert_user_id','project_currency','project_currency_code')
                                            ->with(['expert_info','expert_details','project_bid_info'=>function($qry){
                                                $qry->where('bid_status','=','1');
                                            }])
                                            ->where('id',$project_id)->first();
      if($bid_deatils)
      {
        $arr_bid_details = $bid_deatils->toArray();
      }

      //dd($arr_bid_details);

      $obj_milestone = $this->MilestonesModel->where('project_id',$project_id)
                                             ->with(['transaction_details','milestone_refund_details','project_details'=>function ($query) {
                                                  //$query->select('id','project_name','project_status'); 
                                                }])
                                             ->with(['milestone_release_details'=> function ($query_nxt) {
                                                  $query_nxt->select('id','milestone_id','status','project_id');    
                                                }])
                                             ->orderBy('id','ASC')
                                             ->paginate(config('app.project.pagi_cnt'));
      $arr_milestone         = [];
      $arr_pagination        = [];
      if($obj_milestone){
          $arr_pagination    = clone $obj_milestone; 
          $arr_milestone     = $obj_milestone->toArray();
      }

      // get wallet details
        $wallet_details =  $this->WalletService->get_wallet_details($this->arr_view_data['mp_job_wallet_id']);
        if(isset($wallet_details)){
          $wallet_details         = $wallet_details;
        }
      // end get wallet details

      $user_wallet_balance_amount = 0;
      $arr_data = get_user_wallet_details($this->user_id,$project_currency_code);
      if(isset($arr_data['mp_wallet_id']) && $arr_data['mp_wallet_id']!='') {
        $get_wallet_details = $this->WalletService->get_wallet_details($arr_data['mp_wallet_id']);
        $user_wallet_balance_amount = isset($get_wallet_details->Balance->Amount) ? $get_wallet_details->Balance->Amount/100:0;
      }
      
      $this->arr_view_data['enc_project_id']            = $enc_id;
      $this->arr_view_data['arr_bid_details']           = $arr_bid_details;
      $this->arr_view_data['page_title']                = trans('controller_translations.page_title_milestones');
      $this->arr_view_data['arr_milestone']             = $arr_milestone;
      $this->arr_view_data['arr_pagination']            = $arr_pagination;
      $this->arr_view_data['mangopay_wallet_details']   = $wallet_details;
      $this->arr_view_data['user_wallet_balance_amount']= $user_wallet_balance_amount;
      $this->arr_view_data['module_url_path']           = $this->module_url_path;
      return view($this->view_folder_path.'.project_milestones',$this->arr_view_data);
    }
    /*
      Comment : Show Update Milestone Page.
      Auther  : Nayan S.  
    */
    public function show_update_milestone($enc_id){ 
      $milestone_id = base64_decode($enc_id);
      if($milestone_id == ""){
        return redirect()->back();
      }  
      $obj_milestone = $this->MilestonesModel->where('id',$milestone_id)
                                             ->with(['milestone_refund_details','project_details'=>function ($query) {
                                                  $query->select('id','project_name'); 
                                                }])
                                             ->first();
      $arr_milestone = [];
      if($obj_milestone){
          $arr_milestone    = $obj_milestone->toArray();
      }
      $this->arr_view_data['page_title']      = trans('controller_translations.page_title_update_milestone');
      $this->arr_view_data['arr_milestone']   = $arr_milestone;
      $this->arr_view_data['module_url_path'] = $this->module_url_path;
      return view($this->view_folder_path.'.update_project_milestone',$this->arr_view_data);
    }
    /*
      Comment : Function updates milestone.
      Auther  : Nayan S.  
    */
    public function update_milestone(Request $request){ 
        $arr_rules                = [];
        $arr_rules['title']       = "required";
        $arr_rules['description'] = "required";
        $arr_rules['milestone_id']= "required";
        $arr_rules['amount']      = "required|numeric|min:1";                 
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }
        $form_data = [];
        $form_data = $request->all();
        $milestone_id   = base64_decode($form_data['milestone_id']);
        if($milestone_id == ""){
          return redirect()->back();
        }
        $arr_milestone = [];
        $arr_milestone['title']          = $form_data['title'];
        $arr_milestone['cost']           = $form_data['amount'];
        $arr_milestone['description']    = $form_data['description'];
        $res_milestone = $this->MilestonesModel->where('id',$milestone_id)->update($arr_milestone);  
        if($res_milestone){
            Session::flash('success',trans('controller_translations.success_milestone_information_updated_successfully'));
        } else {
            Session::flash('error',trans('controller_translations.error_while_updating_milestone_information'));
        }
        return redirect()->back();
    }
    /*
      Authe: Sagar Sainkar  
      comment: delete milestone only in the case milsetone payment get fails on not get paid
    */
    public function delete_milestone($enc_id){
      if ($enc_id) {
          $milestone_id = base64_decode($enc_id);
          /* checking the condition that user can only delete milestone if its not paid */
          $obj_milestone = $this->MilestonesModel->where('id',$milestone_id)->where('client_user_id','=',$this->user_id)->with('transaction_details')->first();
          if($obj_milestone){
            $payment_status = '0';
            if( isset($obj_milestone->transaction_details->payment_status) && isset($obj_milestone->transaction_details->payment_status) ) {
              $payment_status =  $obj_milestone->transaction_details->payment_status ;
            }
            if( isset($payment_status) && $payment_status != '1' && $payment_status != '2'){
              $delete_status = $obj_milestone->delete();
              if($delete_status) {
                  Session::flash('success',trans('controller_translations.success_milestone_deleted_successfully'));
              } else {
                  Session::flash('error',trans('controller_translations.error_while_deleting_milestone'));
              }
            } else{
                Session::flash('error',trans('controller_translations.error_while_deleting_milestone'));
            }
          } else{
            Session::flash('error',trans('controller_translations.error_while_deleting_milestone'));
          }
      }
      return back(); 
    }
    /*
      Authe: Sagar Sainkar  
      comment: show payment methods for milestones
    */
    public function payment_methods($enc_id){
      if ($enc_id) 
      {
          $milestone_id = base64_decode($enc_id);
          $obj_milestone = $this->MilestonesModel->where('id',$milestone_id)->with(['project_details'])->first();  
          if($obj_milestone != FALSE){
              $arr_milestone = $obj_milestone->toArray();
          }
          $this->arr_view_data['arr_milestone'] = $arr_milestone;
          $this->arr_view_data['page_title']    = trans('controller_translations.page_title_milestone_payment');
          $this->arr_view_data['module_url_path'] = $this->module_url_path;
          return view($this->view_folder_path.'.payment_methods',$this->arr_view_data);
      }
      return back();
    }
    /*
      Comment : Client will approve the request recieved by client.
      Auther  : Nayan S.  
    */
    // public function approve_milestone_request($enc_id,$currency)
    // {  
    //   //dd('Hereee');
    //   $id                 = base64_decode($enc_id);
    //   $obj_milestone      = $this->MilestoneReleaseModel->where('id',$id)->with('milestone_details')->first();
    //   //dd($obj_milestone->toArray());
    //   if($obj_milestone)
    //   { 
    //     // transafer milestone from project wallet to expert wallet
    //     $project_id       = 0;
    //     $milestone_data   = [];
    //     $wallet_details   = 'false';
    //     if(isset($obj_milestone))
    //     {
    //       $milestone_data = $obj_milestone->toArray();
    //       $project_id     = $milestone_data['project_id']; 
    //       $milestone_id   = $milestone_data['milestone_details']['id'];
    //     } 
    //     $logged_user                           = $this->user->toArray();

    //     if(isset($logged_user['mp_wallet_created']) && $logged_user['mp_wallet_created'] == 'Yes'){
           
    //        $obj_client_data = $this->UserWalletModel->where('user_id',$this->user_id)
    //                                                 ->where('currency_code',$currency)
    //                                                 ->first();
    //        if($obj_client_data)
    //        {
    //          $arr_client_data = $obj_client_data->toArray();
    //        }
    //        $wallet_data['mp_user_id']          = $arr_client_data['mp_user_id'];
    //        $wallet_data['mp_wallet_id']        = $arr_client_data['mp_wallet_id'];
    //        $wallet_data['mp_wallet_created']   = $logged_user['mp_wallet_created'];
    //        $wallet_user_detail                 = $this->WalletService->get_user_details($wallet_data['mp_user_id']);
    //         if($wallet_user_detail){
    //             $project_id                 = $project_id;
    //             $get_project_wallet_details = $this->ProjectWalletsModel->where('project_id',$project_id)->first(); 
    //             if($get_project_wallet_details == null){
    //               Session::flash('error',trans('common/wallet/text.text_project_wallet_not_created_yet'));
    //               return redirect()->back(); 
    //             }
    //             $project_wallet_details     = $get_project_wallet_details->toArray(); 
    //             if(1)//isset($project_wallet_details['project_mp_wallet_id']) && $project_wallet_details['project_mp_wallet_id'] !="")
    //             {
    //               $project_mp_wallet_id     = $wallet_data['mp_wallet_id'];
    //               $get_wallet_details       = $this->WalletService->get_wallet_details($project_mp_wallet_id);
    //               $debited_wallet_amout     = $get_wallet_details->Balance->Amount/100;
    //               $cost_from_client         = $milestone_data['milestone_details']['cost_from_client'];
    //               $cost_website_commission  = $milestone_data['milestone_details']['cost_website_commission'];
    //               $cost_to_expert           = $milestone_data['milestone_details']['cost_to_expert'];
    //               $milestone_amout          = $cost_from_client;
    //               //dd($debited_wallet_amout,$milestone_amout);
    //               if($debited_wallet_amout < $milestone_amout){
    //                 Session::flash('error',trans('common/wallet/text.text_you_dont_have_sufficinet_balance_in_your_wallet_to_create_a_Milestone'));
    //                 return redirect()->back();
    //               }
    //               // get credit user details 
    //                   $credit_user_detail                 = $this->UserWalletModel->where('user_id',$milestone_data['expert_user_id'])->where('currency_code',$currency)->first();

    //                   if(isset($credit_user_detail) && $credit_user_detail != null){
    //                       $credit_user_arr = $credit_user_detail->toArray();
    //                       // end get credit user details
    //                       $transaction_inp                             = [];
    //                       $transaction_inp['tag']                      = 'Milestone Release';
    //                       $transaction_inp['debited_UserId']           = $wallet_data['mp_user_id'];
    //                       $transaction_inp['credited_UserId']          = $credit_user_arr['mp_user_id'];
    //                       $transaction_inp['total_pay']                = $cost_from_client;             // 
    //                       $transaction_inp['debited_walletId']         = (string)$wallet_data['mp_wallet_id'];
    //                       $transaction_inp['credited_walletId']        = (string)$credit_user_arr['mp_wallet_id'];
    //                       $transaction_inp['cost_website_commission']  = $cost_website_commission;
    //                       $transaction_inp['currency_code']            = $currency;
    //                       $wallet_details                              = 'true';
    //                   } else{
    //                     Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
    //                     return redirect()->back();
    //                   }
    //             } else{
    //               Session::flash('error',trans('common/wallet/text.text_create_your_wallet_first'));
    //               return redirect()->back();
    //             } 
    //         } else{
    //           Session::flash('error',trans('common/wallet/text.text_create_your_wallet_first'));
    //           return redirect()->back();
    //         } 
    //     } 
    //     // End wallet 
    //     $result = 'false';
    //     if($wallet_details == 'true')
    //     {
    //       $result = $this->MilestoneReleaseModel->where('id',$id)->update(['status'=>'2']);  
    //     }
    //     if($result != 'false'){
    //        // update invoice id
    //         $walletTowallet_transfer         = $this->WalletService->walletTransfer($transaction_inp);
    //         if(isset($walletTowallet_transfer->Status) && $walletTowallet_transfer->Status == 'FAILED'){
    //           Session::flash('error',$walletTowallet_transfer->ResultMessage);
    //           $this->MilestoneReleaseModel->where('id',$id)->update(['status'=>'0']);  
    //           return redirect()->back();
    //         } 
    //         else if(isset($walletTowallet_transfer->Status) && $walletTowallet_transfer->Status == 'SUCCEEDED')
    //         {
    //           $this->MilestonesModel->where('id',$milestone_id)->update(['is_milestone'=>'3']);

    //           Session::flash('success',trans('controller_translations.success_milestone_approval_sent_successfully'));
    //           /* Send email to client about milestone release request. */
    //           $project_name     = config('app.project.name');
    //           //$mail_form        = get_site_email_address();   /* getting email address of admin from helper functions */
    //           $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
    //           $obj_project_info = $this->ProjectpostModel->where('id',$obj_milestone->project_id)
    //                                     ->with(['client_info'=> function ($query) {
    //                                           $query->select('user_id','first_name','last_name');
    //                                         },
    //                                         'client_details'=>function ($query_nxt) {
    //                                           $query_nxt->select('id','email');
    //                                         }])
    //                                     ->with(['expert_info'=> function ($query_nxt_1) {
    //                                         $query_nxt_1->select('user_id','first_name','last_name');
    //                                     }])
    //                                    ->first(['id','project_name','client_user_id','expert_user_id']);
    //           $data                          = [];
    //           if($obj_project_info){
    //             $arr_project_info            = $obj_project_info->toArray();
    //             /* get client name */
    //             $client_first_name           = isset($arr_project_info['client_info']['first_name'])?$arr_project_info['client_info']['first_name']:'';
    //             $client_last_name            = isset($arr_project_info['client_info']['last_name'])?$arr_project_info['client_info']['last_name']:'';
    //             /*$client_name               = $client_first_name.' '.$client_last_name;*/
    //             $client_name                 = $client_first_name;
    //             $expert_first_name           = isset($arr_project_info['expert_info']['first_name'])?$arr_project_info['expert_info']['first_name']:'';
    //             $expert_last_name            = isset($arr_project_info['expert_info']['last_name'])?$arr_project_info['expert_info']['last_name']:'';
    //             $expert_name                 = $expert_first_name.' '.$expert_last_name;
    //             /* Getting admins information from helper function */
    //             $admin_data                  = get_admin_email_address();
    //             $data['login_url']           = url('/redirection?redirect=MILESTONE_APPROVED&id='.base64_encode($obj_milestone->project_id)) ;
    //             $data['project_name']        = isset($arr_project_info['project_name'])?$arr_project_info['project_name']:'';
    //             $data['milestone_title']     = isset($obj_milestone->milestone_details['title'])?$obj_milestone->milestone_details['title']:'';
    //             $data['milestone_amount']    = isset($obj_milestone->milestone_details['cost'])?$obj_milestone->milestone_details['cost']:'';
    //             $data['client_name']         = $client_name;
    //             $data['expert_name']         = $expert_name;
    //             $data['admin_name']          = isset($admin_data['user_name'])?$admin_data['user_name']:'';
    //           }

    //           /*$email_to = isset($admin_data['email'])?$admin_data['email']:'';
    //           if($email_to!= ""){
    //             try{
    //                 Mail::send('front.email.milestone_approval_by_client_to_admin', $data, function ($message) use ($email_to,$mail_form,$project_name) {
    //                     $message->from($mail_form, $project_name);
    //                     $message->subject($project_name.': Milestone Released.');
    //                     $message->to($email_to);
    //                 });
    //             } catch(\Exception $e){
    //             Session::Flash('error'   , trans('controller_translations.text_mail_not_sent'));
    //             }
    //           }*/

    //           /* Create Notification for EXPERT */      
    //           $arr_data                         = [];
    //           $arr_data['user_id']              = $obj_milestone->expert_user_id;
    //           $arr_data['user_type']            = '3';
    //           $arr_data['url']                  = 'expert/projects/milestones/'.base64_encode($obj_milestone->project_id);
    //           $arr_data['project_id']           = $obj_milestone->project_id;
    //           /*$arr_data['notification_text']  = trans('controller_translations.milestone_request_approved');*/
    //           $arr_data['notification_text_en'] = Lang::get('controller_translations.milestone_request_approved',[],'en','en');
    //           $arr_data['notification_text_de'] = Lang::get('controller_translations.milestone_request_approved',[],'de','en');
    //           $this->NotificationsModel->create($arr_data);

    //           /* Notification to admin */
    //           $arr_admin_data                         =  [];
    //           $arr_admin_data['user_id']              =  1;
    //           $arr_admin_data['user_type']            = '1';
    //           $arr_admin_data['url']                  = 'admin/project_milestones/all/'.base64_encode($obj_milestone->project_id);
    //           $arr_admin_data['project_id']           = $obj_milestone->project_id;
    //           /*$arr_admin_data['notification_text']  = trans('controller_translations.milestone_request_approved');*/
    //           $arr_admin_data['notification_text_en'] = Lang::get('controller_translations.milestone_request_approved',[],'en','en');
    //           $arr_admin_data['notification_text_de'] = Lang::get('controller_translations.milestone_request_approved',[],'de','en');
    //           $this->NotificationsModel->create($arr_admin_data); 
    //           /* Notificatin ends */  
    //         } else {
    //           Session::flash('error',trans('controller_translations.error_while_sending_milestone_approval'));
    //           $this->MilestoneReleaseModel->where('id',$id)->update(['status'=>'0']);  
    //           return redirect()->back();
    //         }
    //     } else {
    //       Session::flash('error',trans('controller_translations.error_while_sending_milestone_approval'));
    //     }
    //   } else {
    //     Session::flash('error',trans('controller_translations.error_while_sending_milestone_approval'));
    //   }
    //   return redirect()->back();
    // } 

    public function approve_milestone_request($enc_id,$currency)
    {  // after wallet functionality creates this function work for milestone release process 
      $id                 = base64_decode($enc_id);
      $obj_milestone      = $this->MilestoneReleaseModel->where('id',$id)->with('milestone_details')->first();

      if($obj_milestone)
      { 
        // transafer milestone from project wallet to expert wallet
        $project_id       = 0;
        $milestone_data   = [];
        $wallet_details   = 'false';
        if(isset($obj_milestone))
        {
          $milestone_data = $obj_milestone->toArray();
          $project_id     = $milestone_data['project_id']; 
          $milestone_id   = $milestone_data['milestone_details']['id'];
        } 
        $logged_user                           = $this->user->toArray(); 
        if(isset($logged_user['mp_wallet_created']) && $logged_user['mp_wallet_created'] == 'Yes'){
           $wallet_owner = get_user_wallet_details($this->user_id,$currency); 
           $wallet_data['mp_user_id']          = $wallet_owner['mp_user_id'];
           $wallet_data['mp_wallet_id']        = $logged_user['mp_wallet_id'];
           $wallet_data['mp_wallet_created']   = $logged_user['mp_wallet_created'];
           $wallet_user_detail                 = $this->WalletService->get_user_details($wallet_data['mp_user_id']);
            if($wallet_user_detail){
                $project_id                 = $project_id;
                $get_project_wallet_details = $this->ProjectWalletsModel->where('project_id',$project_id)->first(); 
                if($get_project_wallet_details == null){
                  Session::flash('error',trans('common/wallet/text.text_project_wallet_not_created_yet'));
                  return redirect()->back(); 
                }
                $project_wallet_details     = $get_project_wallet_details->toArray(); 
                if(isset($project_wallet_details['project_mp_wallet_id']) && $project_wallet_details['project_mp_wallet_id'] !=""){
                  $project_mp_wallet_id     = $project_wallet_details['project_mp_wallet_id'];
                  $get_wallet_details       = $this->WalletService->get_wallet_details($project_mp_wallet_id);
                  $debited_wallet_amout     = $get_wallet_details->Balance->Amount/100;
                  $cost_from_client         = $milestone_data['milestone_details']['cost_from_client'];
                  $cost_website_commission  = $milestone_data['milestone_details']['cost_website_commission'];
                  $cost_to_expert           = $milestone_data['milestone_details']['cost_to_expert'];
                  $milestone_amout          = $cost_from_client;
                  //dd($milestone_amout,$debited_wallet_amout);
                  if($debited_wallet_amout < $milestone_amout){
                    Session::flash('error',trans('common/wallet/text.text_you_dont_have_sufficinet_balance_in_your_wallet_to_create_a_Milestone'));
                    return redirect()->back();
                  }
                  // get credit user details 
                      $credit_user_arr = get_user_wallet_details($milestone_data['expert_user_id'],$currency); 
                      //$this->UserModel->where('id',$milestone_data['expert_user_id'])->first(['mp_user_id','mp_wallet_id']);
                      if(isset($credit_user_arr) && $credit_user_arr != null){
                          //$credit_user_arr = $credit_user_detail->toArray();
                          // end get credit user details
                          $transaction_inp                             = [];
                          $transaction_inp['tag']                      = 'Milestone Release';
                          $transaction_inp['debited_UserId']           = $wallet_data['mp_user_id'];
                          $transaction_inp['credited_UserId']          = $credit_user_arr['mp_user_id'];
                          $transaction_inp['total_pay']                = $cost_from_client;             // 
                          $transaction_inp['debited_walletId']         = (string)$project_wallet_details['project_mp_wallet_id'];
                          $transaction_inp['credited_walletId']        = (string)$credit_user_arr['mp_wallet_id'];
                          $transaction_inp['cost_website_commission']  = $cost_website_commission;
                          $transaction_inp['currency_code']            = $currency;
                          $wallet_details                              = 'true';
                      } else{
                        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
                        return redirect()->back();
                      }
                } else{
                  Session::flash('error',trans('common/wallet/text.text_create_your_wallet_first'));
                  return redirect()->back();
                } 
            } else{
              Session::flash('error',trans('common/wallet/text.text_create_your_wallet_first'));
              return redirect()->back();
            } 
        } 
        // End wallet 
        $result = 'false';
        if($wallet_details == 'true')
        {
          $result = $this->MilestoneReleaseModel->where('id',$id)->update(['status'=>'2']);  
        }
        if($result != 'false'){
           // update invoice id
            $walletTowallet_transfer         = $this->WalletService->walletTransfer($transaction_inp);
            if(isset($walletTowallet_transfer->Status) && $walletTowallet_transfer->Status == 'FAILED'){
              Session::flash('error',$walletTowallet_transfer->ResultMessage);
              $this->MilestoneReleaseModel->where('id',$id)->update(['status'=>'0']);  
              return redirect()->back();
            } 
            else if(isset($walletTowallet_transfer->Status) && $walletTowallet_transfer->Status == 'SUCCEEDED')
            {
              $this->MilestonesModel->where('id',$milestone_id)->update(['is_milestone'=>'3']);

              Session::flash('success',trans('controller_translations.success_milestone_approval_sent_successfully'));
              /* Send email to client about milestone release request. */
              $project_name     = config('app.project.name');
              //$mail_form        = get_site_email_address();   /* getting email address of admin from helper functions */
              $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
              $obj_project_info = $this->ProjectpostModel->where('id',$obj_milestone->project_id)
                                        ->with(['client_info'=> function ($query) {
                                              $query->select('user_id','first_name','last_name');
                                            },
                                            'client_details'=>function ($query_nxt) {
                                              $query_nxt->select('id','email');
                                            }])
                                        ->with(['expert_info'=> function ($query_nxt_1) {
                                            $query_nxt_1->select('user_id','first_name','last_name');
                                        }])
                                       ->first(['id','project_name','client_user_id','expert_user_id']);
              $data                          = [];
              if($obj_project_info){
                $arr_project_info            = $obj_project_info->toArray();
                /* get client name */
                $client_first_name           = isset($arr_project_info['client_info']['first_name'])?$arr_project_info['client_info']['first_name']:'';
                $client_last_name            = isset($arr_project_info['client_info']['last_name'])?$arr_project_info['client_info']['last_name']:'';
                /*$client_name               = $client_first_name.' '.$client_last_name;*/
                $client_name                 = $client_first_name;
                $expert_first_name           = isset($arr_project_info['expert_info']['first_name'])?$arr_project_info['expert_info']['first_name']:'';
                $expert_last_name            = isset($arr_project_info['expert_info']['last_name'])?$arr_project_info['expert_info']['last_name']:'';
                $expert_name                 = $expert_first_name.' '.$expert_last_name;
                /* Getting admins information from helper function */
                $admin_data                  = get_admin_email_address();
                $data['login_url']           = url('/redirection?redirect=MILESTONE_APPROVED&id='.base64_encode($obj_milestone->project_id)) ;
                $data['project_name']        = isset($arr_project_info['project_name'])?$arr_project_info['project_name']:'';
                $data['milestone_title']     = isset($obj_milestone->milestone_details['title'])?$obj_milestone->milestone_details['title']:'';
                $data['milestone_amount']    = isset($obj_milestone->milestone_details['cost'])?$obj_milestone->milestone_details['cost']:'';
                $data['client_name']         = $client_name;
                $data['expert_name']         = $expert_name;
                $data['admin_name']          = isset($admin_data['user_name'])?$admin_data['user_name']:'';
              }

              /*$email_to = isset($admin_data['email'])?$admin_data['email']:'';
              if($email_to!= ""){
                try{
                    Mail::send('front.email.milestone_approval_by_client_to_admin', $data, function ($message) use ($email_to,$mail_form,$project_name) {
                        $message->from($mail_form, $project_name);
                        $message->subject($project_name.': Milestone Released.');
                        $message->to($email_to);
                    });
                } catch(\Exception $e){
                Session::Flash('error'   , trans('controller_translations.text_mail_not_sent'));
                }
              }*/

              $posted_project_name = isset($arr_project_info['project_name'])?$arr_project_info['project_name']:'';

              /* Create Notification for EXPERT */      
              $arr_data                         = [];
              $arr_data['user_id']              = $obj_milestone->expert_user_id;
              $arr_data['user_type']            = '3';
              $arr_data['url']                  = 'expert/projects/milestones/'.base64_encode($obj_milestone->project_id);
              $arr_data['project_id']           = $obj_milestone->project_id;
              /*$arr_data['notification_text']  = trans('controller_translations.milestone_request_approved');*/
              $arr_data['notification_text_en'] = $posted_project_name . ' - ' . Lang::get('controller_translations.milestone_request_approved',[],'en','en');
              $arr_data['notification_text_de'] = $posted_project_name . ' - ' . Lang::get('controller_translations.milestone_request_approved',[],'de','en');
              $this->NotificationsModel->create($arr_data);

              /* Notification to admin */
              $arr_admin_data                         =  [];
              $arr_admin_data['user_id']              =  1;
              $arr_admin_data['user_type']            = '1';
              $arr_admin_data['url']                  = 'admin/project_milestones/all/'.base64_encode($obj_milestone->project_id);
              $arr_admin_data['project_id']           = $obj_milestone->project_id;
              /*$arr_admin_data['notification_text']  = trans('controller_translations.milestone_request_approved');*/
              $arr_admin_data['notification_text_en'] = Lang::get('controller_translations.milestone_request_approved',[],'en','en');
              $arr_admin_data['notification_text_de'] = Lang::get('controller_translations.milestone_request_approved',[],'de','en');
              $this->NotificationsModel->create($arr_admin_data); 
              /* Notificatin ends */  
            } else {
              Session::flash('error',trans('controller_translations.error_while_sending_milestone_approval'));
              $this->MilestoneReleaseModel->where('id',$id)->update(['status'=>'0']);  
              return redirect()->back();
            }
        } else {
          Session::flash('error',trans('controller_translations.error_while_sending_milestone_approval'));
        }
      } else {
        Session::flash('error',trans('controller_translations.error_while_sending_milestone_approval'));
      }
      return redirect()->back();
    } 

    public function not_approve_milestone_request($enc_id)
    {
      
      if($enc_id!='')
      {
        $id                 = base64_decode($enc_id);
        $obj_milestone      = $this->MilestoneReleaseModel->with('milestone_details')->where('id',$id)->first();
        

        $obj_project_info = $this->ProjectpostModel->where('id',$obj_milestone->project_id)
                                                    ->with(['client_info'=> function ($query) {
                                                            $query->select('user_id','first_name','last_name');
                                                          },
                                                          'client_details'=>function ($query_nxt) {
                                                            $query_nxt->select('id','email','user_name');
                                                          }])
                                                    ->with(['expert_info'=> function ($query_nxt_1) {
                                                          $query_nxt_1->select('user_id','first_name','last_name');
                                                          },
                                                          'expert_details'=>function ($query_nxt_2) {
                                                            $query_nxt_2->select('id','email','user_name');
                                                          }
                                                          ])
                                                    ->first(['id','project_name','client_user_id','expert_user_id','project_currency']);

        if($obj_project_info)
        {
            $arr_project_info  = $obj_project_info->toArray();
        }

        $client_first_name = isset($arr_project_info['client_info']['first_name'])?$arr_project_info['client_info']['first_name']:'';
        $client_last_name  = isset($arr_project_info['client_info']['last_name'])?$arr_project_info['client_info']['last_name']:'';
        $client_name = $client_first_name.' '.$client_last_name;
        $client_username = isset($arr_project_info['client_details']['user_name'])?$arr_project_info['client_details']['user_name']:'';

        $expert_first_name = isset($arr_project_info['expert_info']['first_name'])?$arr_project_info['expert_info']['first_name']:'';
        $expert_last_name  = isset($arr_project_info['expert_info']['last_name'])?$arr_project_info['expert_info']['last_name']:'';
        $expert_name       = $expert_first_name;


        $data = [];
        $data['project_name'] = $project_name = isset($arr_project_info['project_name'])?$arr_project_info['project_name']:'';

        $data['milestone_title']     = isset($obj_milestone->milestone_details->title)?$obj_milestone->milestone_details->title:'';
        $data['milestone_amount']    = isset($obj_milestone->milestone_details->cost)?$obj_milestone->milestone_details->cost:'';
        $data['client_name']         = $client_name;
        $data['expert_name']         = $expert_name;
        $data['client_username']     = $client_username;
        $data['project_currency']    = isset($arr_project_info['project_currency'])?$arr_project_info['project_currency']:'$';
        $data['email_id']            = isset($arr_project_info['expert_details']['email'])?
                                             $arr_project_info['expert_details']['email']:'';
        //dd($data);
        $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
        $email_to = isset($arr_project_info['expert_details']['email'])? $arr_project_info['expert_details']['email']:'';

        if($obj_milestone)
        {
          $expert_user_id = isset($obj_milestone->expert_user_id)?$obj_milestone->expert_user_id:'';
          $project_id = isset($obj_milestone->project_id)?$obj_milestone->project_id:'';

          $status = $obj_milestone->delete();
          if($status)
          {
            $posted_project_name =  isset($arr_project_info['project_name'])?$arr_project_info['project_name']:'';

            /* Create Notification for expert*/
            $arr_data               =  [];
            $arr_data['user_id']    =  $expert_user_id;
            $arr_data['user_type']  = '3'; 
            $arr_data['url']                  = "expert/projects/milestones/".base64_encode($project_id);
            $arr_data['notification_text_en'] = $posted_project_name . ' - ' . Lang::get('controller_translations.milestone_rejected_by_client',[],'en','en');
            $arr_data['notification_text_de'] = $posted_project_name . ' - ' . Lang::get('controller_translations.milestone_rejected_by_client',[],'de','en');
            $arr_data['project_id'] = $project_id;
            $this->NotificationsModel->create($arr_data); 
            /* end create Notification for expert*/

            if($email_to!= "")
            {
              try{
                    //$mail_status = $this->MailService->send_milestone_release_requested_rejected_by_client_email($data);
                  Mail::send('front.email.milestone_release_requested_rejected_by_client', $data, function ($message) use ($email_to,$mail_form,$project_name) {
                      $message->from($mail_form, $project_name);
                      $message->subject($project_name.': Milestone Release Request Rejected by client.');
                      $message->to($email_to);
                  });
              }
              catch(\Exception $e)
              {
                Session::Flash('error',trans('controller_translations.text_mail_not_sent'));
              }
            }


            Session::flash('success',trans('controller_translations.milestone_rejected_success'));
            return redirect()->back();
          }
          else
          {
            Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
            return redirect()->back();
          }
        }
        else
        {
          Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
          return redirect()->back();
        }
      }
      else
      {
        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
        return redirect()->back();
      }
      
    }



    public function get_expert_subscription_commission($project_id)
    {
      if ($project_id) 
      {
        $project_details = $this->ProjectpostModel->where('id',$project_id)->first(['expert_user_id']);
        if ($project_details!=FALSE) 
        {
          if (isset($project_details->expert_user_id) && $project_details->expert_user_id!=NULL) 
          {
            $obj_subscription = $this->SubscriptionUsersModel->where('is_active','1')
                               ->where('user_id',$project_details->expert_user_id) ->orderBy('id','DESC')->first(['website_commision']);
            if ($obj_subscription!=FALSE && isset($obj_subscription->website_commision)) 
            {
                return $obj_subscription->website_commision;
            }
          }
        }
      }
      return 0;
    }
    public function get_project_manager_commission($project_id){
      if ($project_id){
        $project_details = $this->ProjectpostModel->where('id',$project_id)->first(['project_handle_by']);
        if ($project_details!=FALSE){
            if (isset($project_details->project_handle_by) && $project_details->project_handle_by==2) 
            {
                $obj_settings = $this->SiteSettingModel->where('site_settting_id',1)->first(['wesite_project_manager_commission']);

                if ($obj_settings!=FALSE) 
                {
                  if (isset($obj_settings->wesite_project_manager_commission)) 
                  {
                     return $obj_settings->wesite_project_manager_commission;
                  }
                }
            }
        }
      }
      return 0;
    }
    public function get_expert_from_project($project_id){
      if ($project_id) {
        $project_details = $this->ProjectpostModel->where('id',$project_id)->first(['expert_user_id']);
        if ($project_details!=FALSE) {
            if (isset($project_details->expert_user_id) && $project_details->expert_user_id!=NULL) { 
               return $project_details->expert_user_id;
            }
        }
      }
      return 0;
    }
    public function send_milestone_refund_request_to_admin(Request $request)
    {
       $arr_rules                           = [];
       $arr_rules['ref_milestone_id']       = "required";
       $arr_rules['ref_project_id']         = "required";
       $arr_rules['refunc_request_reasn']   = "required";
       $validator = Validator::make($request->all(),$arr_rules);
       if($validator->fails()){
           return redirect()->back()->withErrors($validator)->withInput($request->all());
       }
       // existed
       $get_existed = $this->MilestoneRefundRequests
            ->where('project_id',$request->input('ref_project_id'))
            ->where('milestone_id',$request->input('ref_milestone_id'))
            ->first();
       // end existed
       if($get_existed != null)
       {
          $existed = $get_existed->toArray();
          if($existed['is_refunded'] == 'no')
          {
            $this->MilestoneRefundRequests
                 ->where('project_id',$request->input('ref_project_id'))
                 ->where('milestone_id',$request->input('ref_milestone_id'))
                 ->where('is_refunded','no')
                 ->delete();
          } 
          else if($existed['is_refunded'] == 'yes')
          {
            Session::flash('error',trans('common/wallet/text.text_sorry_this_milestone_had_refunded'));
            return redirect()->back();
          }
       }
       $refund_data                  = [];
       $refund_data['project_id']    = $request->input('ref_project_id');
       $refund_data['milestone_id']  = $request->input('ref_milestone_id');
       $refund_data['Refund_reason'] = $request->input('refunc_request_reasn');
       $send_request                 = $this->MilestoneRefundRequests->create($refund_data);

       $this->MilestonesModel->where('id',$request->input('ref_milestone_id'))->update(['is_milestone'=>'2']); 
       
       if($send_request)
       {
          
          $obj_project_info = $this->ProjectpostModel->where('id',$request->input('ref_project_id'))
                                        ->with(['client_info'=> function ($query) {
                                              $query->select('user_id','first_name','last_name');
                                            },
                                            'client_details'=>function ($query_nxt) {
                                              $query_nxt->select('id','email');
                                            }])
                                        ->with(['expert_info'=> function ($query_nxt_1) {
                                            $query_nxt_1->select('user_id','first_name','last_name');
                                        }])
                                       ->first(['id','project_name','client_user_id','expert_user_id']);

          $posted_project_name = isset($obj_project_info->project_name) ? $obj_project_info->project_name : '';

          /* Create Notification for EXPERT */      
          $arr_data                         = [];
          $arr_data['user_id']              = $request->input('ref_expert_id');
          $arr_data['user_type']            = '3';
          $arr_data['url']                  = 'expert/projects/milestones/'.base64_encode($request->input('ref_project_id'));
          $arr_data['project_id']           = $request->input('ref_project_id');
          $arr_data['notification_text_en'] = $posted_project_name . ' - ' . Lang::get('common/wallet/text.text_notifiy_milestone_refund_request',[],'en','en');
          $arr_data['notification_text_de'] = $posted_project_name . ' - ' . Lang::get('common/wallet/text.text_notifiy_milestone_refund_request',[],'de','en');
          $this->NotificationsModel->create($arr_data);

          /* Notification to admin */
          $arr_admin_data                         =  [];
          $arr_admin_data['user_id']              =  1;
          $arr_admin_data['user_type']            = '1';
          $arr_admin_data['url']                  = 'admin/milestones/refund/requests?mid='.base64_encode($request->input('ref_milestone_id'));
          $arr_admin_data['project_id']           = $request->input('ref_project_id');
          $arr_admin_data['notification_text_en'] = Lang::get('common/wallet/text.text_milestone_refund_request',[],'en','en');
          $arr_admin_data['notification_text_de'] = Lang::get('common/wallet/text.text_milestone_refund_request',[],'de','en');
          $this->NotificationsModel->create($arr_admin_data); 
            // send email to admin
              $project_name     = config('app.project.name');
              $obj_milestone    = [];
              $obj_milestone    = $this->MilestonesModel->where('id',$request->input('ref_milestone_id'))->first();
              if($obj_milestone){
                 $obj_milestone = $obj_milestone->toArray(); 
              }
              //$mail_form        = get_site_email_address();   /* getting email address of admin from helper functions */
              $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
              
              $data                          = [];
              if($obj_project_info){
                $arr_project_info            = $obj_project_info->toArray();
                /* get client name */
                $client_first_name           = isset($arr_project_info['client_info']['first_name'])?$arr_project_info['client_info']['first_name']:'';
                $client_last_name            = isset($arr_project_info['client_info']['last_name'])?$arr_project_info['client_info']['last_name']:'';
                /*$client_name               = $client_first_name.' '.$client_last_name;*/
                $client_name                 = $client_first_name;
                $expert_first_name           = isset($arr_project_info['expert_info']['first_name'])?$arr_project_info['expert_info']['first_name']:'';
                $expert_last_name            = isset($arr_project_info['expert_info']['last_name'])?$arr_project_info['expert_info']['last_name']:'';
                $expert_name                 = $expert_first_name.' '.$expert_last_name;
                /* Getting admins information from helper function */
                $admin_data                  = get_admin_email_address();
                $data['login_url']           = url('/redirection?redirect=MILESTONE_APPROVED&id='.base64_encode(isset($obj_milestone['project_id'])?$obj_milestone['project_id']:0));
                $data['project_name']        = isset($arr_project_info['project_name'])?$arr_project_info['project_name']:'';
                $data['milestone_title']     = isset($obj_milestone['title'])?$obj_milestone['title']:'';
                $data['milestone_amount']    = isset($obj_milestone['cost'])?$obj_milestone['cost'].'USD':'';
                $data['client_name']         = $client_name;
                $data['expert_name']         = $expert_name;
                $data['admin_name']          = isset($admin_data['user_name'])?$admin_data['user_name']:'';
                $data['email_id']            = isset($admin_data['email'])?$admin_data['email']:'';
              }
              $email_to = isset($admin_data['email'])?$admin_data['email']:'';
              if($email_to!= ""){
                try{

                    $mail_status = $this->MailService->send_milestone_refund_by_client_to_admin_email($data);

                    // $sendmail = Mail::send('front.email.milestone_refund_by_client_to_admin', $data, function ($message) use ($email_to,$mail_form,$project_name) {
                    //     $message->from($mail_form, $project_name);
                    //     $message->subject($project_name.': Milestone Refund Request.');
                    //     $message->to($email_to);
                    // });
                } catch(\Exception $e){
                Session::Flash('error'   , trans('controller_translations.text_mail_not_sent'));
                }
              }
          /* Notificatin ends */
          Session::flash('success',trans('common/wallet/text.text_thank_you_for_your_request_We_will_take_care_of_it_and_well_come_back_to_you_soon'));
          return redirect()->back();
       } else {
          Session::flash('error',trans('common/wallet/text.text_your_refund_request_is_not_sent_successfully_to_archexpert_admin_please_try_again_later'));
          return redirect()->back();
       }
    } 
} // end class