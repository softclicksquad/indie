<?php
namespace App\Http\Controllers\Front\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\UserModel;
use App\Models\ClientsModel;
use App\Models\CountryModel; 
use App\Models\ReviewsModel; 
use App\Models\ProjectpostModel;

use App\Common\Services\LanguageService;  
use App\Common\Services\WalletService;  
use App\Models\ProfessionModel;  

/*Used helper method to fire event*/
use App\Events\sendEmailOnPostProject;
use App\Common\Services\MailService;

use Validator;
use Session;
use Sentinel;
use Mail;
use Activation;
use Reminder;
use URL;
use DB;

class ProfileController extends Controller
{
    public $arr_view_data;
    public function __construct(
                                  UserModel $user_model,
                                  ClientsModel $clients,
                                  CountryModel $countries,
                                  ReviewsModel $reviews,
                                  LanguageService $langauge,
                                  WalletService $wallet_service,
                                  ProfessionModel $Profession,
                                  ProjectpostModel $project_post,
                                  MailService $MailService
                                )
    { 

      if(! $user = Sentinel::check()) 
      {
        return redirect('/login')->send();
      }

      $this->user_id                        = $user->id;
      $this->UserModel                      = $user_model;
      $this->ClientsModel                   = $clients;
      $this->CountryModel                   = $countries;
      $this->ReviewsModel                   = $reviews;
      $this->ProjectpostModel               = $project_post;
      $this->ProfessionModel                = $Profession;
      $this->LanguageService                = $langauge;
      $this->WalletService                  = $wallet_service;
      $this->MailService                    = $MailService;
      $this->arr_view_data                  = [];
      $this->module_url_path                = url("/client/profile");
      $this->profile_img_base_path          = base_path() . '/public'.config('app.project.img_path.profile_image');
      $this->profile_img_public_path        = url('/').config('app.project.img_path.profile_image');
      $this->cover_resize_path              = 'uploads/front/cover_image/';
      $this->cover_img_public_path          = url('/').config('app.project.img_path.cover_image');
      $this->project_attachment_base_path   = base_path() . '/public'.config('app.project.img_path.project_attachment');
      $this->project_attachment_public_path = url('/').config('app.project.img_path.project_attachment');
    }
    /*Profile: load page of client Profile
    Author: Bharat khairnar*/
    public function index(Request $request)
    { 
      //get all countries here
      $arr_countries = array();
      $obj_countries = $this->CountryModel->orderBy('country_name', 'asc')->where('is_active',1)->get();
      if($obj_countries != FALSE){
          $arr_countries = $obj_countries->toArray();
      }

      $arr_mangopay_blocked_countries = get_mangopay_blocked_countries_id();
      $obj_client = $this->ClientsModel->where('user_id',$this->user_id)->with(['user_details','country_details.states','state_details.cities','city_details'])->first();
      $arr_client_details = array();
      if($obj_client != FALSE){
        $arr_client_details = $obj_client->toArray();
      }
      //dd($arr_client_details);
      if($arr_client_details['user_details']['user_name']=='')
      {
          $country_string   = isset($arr_client_details['country_details']['country_code'])?
                                    $arr_client_details['country_details']['country_code']:'';
          $user_type_string = isset($arr_client_details['user_type'])?$arr_client_details['user_type']:'';
          $first_string     = isset($arr_client_details['first_name'])?$arr_client_details['first_name']:'';
          $user_name_string = $this->GetRandomString($country_string,$user_type_string,$first_string);
      }
      else
      {
        $user_name_string  = $arr_client_details['user_details']['user_name'];
      }
      
      if(isset($arr_client_details['phone_code']) && $arr_client_details['phone_code'] == '') {
        // $ip = '52.25.109.230';
        $ip = $request->ip();
        $obj_location = @json_decode(file_get_contents( "http://www.geoplugin.net/json.gp?ip=" . $ip));
        if(isset($obj_location->geoplugin_countryCode) && $obj_location->geoplugin_countryCode!=''){
          $arr_client_details['phone_code'] = get_phone_code_by_country_code($obj_location->geoplugin_countryCode);
        }
      }

      $this->arr_view_data['user_name_string']               = $user_name_string;
      $this->arr_view_data['arr_client_details']             = $arr_client_details;
      $this->arr_view_data['arr_countries']                  = $arr_countries;
      $this->arr_view_data['arr_mangopay_blocked_countries'] = $arr_mangopay_blocked_countries;
      $this->arr_view_data['page_title']                     = trans('controller_translations.page_title_client_profile');
      $this->arr_view_data['module_url_path']                = $this->module_url_path;

      $arr_lang            = $this->LanguageService->get_all_language();
      $arr_professions     = [];
      $obj_profession      = $this->ProfessionModel->where('profession.is_active','1')->with(['translations'])->get();
      if($obj_profession   != FALSE)        {
          $arr_professions = $obj_profession->toArray();
      }
      $this->arr_view_data['arr_professions'] = $arr_professions; 
      return view('client.profile.index',$this->arr_view_data);
    }
    
    public function update(Request $request)
    {
        $arr_rules = array();
        $status = FALSE;

        $arr_rules['first_name']    = "required|max:255";
        $arr_rules['last_name']     = "required|max:255";
        //$arr_rules['user_name']     = "required|max:32|min:3";
        $arr_rules['phone_number']  = "required|max:12|min:10";
        $arr_rules['country']       = "required";
        $arr_rules['phone_code']    = "required|min:1";
        $arr_rules['zip']           = "required|min:4";
        $arr_rules['timezone']      = "required";
        $arr_rules['email']         = "required|email";
        //$arr_rules['currency_code'] = "required";

        if($request->input('user_type') == 'business')
        {
          $arr_rules['vat_number']   = "required";
          $arr_rules['company_name'] = "required";
        }

        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = array();
        $form_data = $request->all(); 

        $obj_client = $this->ClientsModel->with(['user_details'])->where('user_id',$this->user_id)->first();

        if($obj_client && sizeof($obj_client) > 0)
        {
           $arr_client = array();
           $arr_client['first_name']    = ucfirst($form_data['first_name']);
           $arr_client['last_name']     = ucfirst($form_data['last_name']);
           $arr_client['phone_code']    = $form_data['phone_code'];
           $arr_client['phone_number']  = $form_data['phone_number'];

           if(isset($request->input['user_name'])){
            $arr_client['user_name']     = $form_data['user_name'];
           }
           
           //$arr_client['user_name']     = $form_data['user_name'];

           $arr_client['country']       = $form_data['country'];
           $arr_client['state']         = $form_data['state'];
           $arr_client['city']          = $form_data['city'];
           $arr_client['zip']           = $form_data['zip'];
           $arr_client['address']       = '';//$form_data['address'];
           if($request->input('user_type') == 'business')
           {
              $arr_client['vat_number']    = $form_data['vat_number'];                    
              $arr_client['company_name']  = $form_data['company_name'];                    
           }

           $user_data = [];

            $user_data['mp_user_id'] = isset($obj_client->user_details->mp_user_id)?$obj_client->user_details->mp_user_id:'';

           /*-------------------------------------------*/
            if(isset($form_data['user_name']))
            {
              $this->UserModel->where('id',$this->user_id)->update(['user_name'=>$form_data['user_name']]);
            }
           /*--------------------------------------------*/

            // $currency_code     = $form_data['currency_code'];
            // $old_currency_code = $form_data['old_currency_code'];

            // if($old_currency_code!='' && $old_currency_code != $currency_code)
            // {
            //   $AppWallet = $this->WalletService->create_wallet_currency_wise($user_data['mp_user_id'],'User Wallet',$currency_code);
            //   if($AppWallet)
            //   {
            //     $mp_wallet_id = isset($AppWallet->Id)?$AppWallet->Id:"";
            //     $this->UserModel->where('id',$this->user_id)->update(['mp_wallet_id'=>$mp_wallet_id,'currency_code'=>$currency_code]);
            //   }
            // }

            // if(isset($form_data['user_name']))
            // {
            //   $this->UserModel->where('id',$this->user_id)->update(['user_name'=>$form_data['user_name'],'currency_code'=>$currency_code,'currency_symbol'=>$form_data['currency_symbol']]);
            // }

            if (isset($form_data['spoken_languages']) && sizeof($form_data['spoken_languages']) > 0) 
            {
              $arr_client['spoken_languages']  = implode(",",$form_data['spoken_languages']); 
            }
            else
            {
              Session::flash('error',trans('controller_translations.error_please_select_at_least_one_language'));
              return back()->withInput($request->all());
            }

            if($request->input('user_type') == 'business')
            {
              $arr_client['company_name']  = $form_data['company_name'];
            }
            
            $arr_client['timezone']      = $form_data['timezone'];
            $arr_client['owner_name']    = isset($form_data['owner_name'])?$form_data['owner_name']:'';
            $arr_client['occupation']    = '';//$form_data['occupation'];
            $arr_client['company_size']  = isset($form_data['company_size'])?$form_data['company_size']:'';
            $arr_client['founding_year'] = isset($form_data['founding_year'])?$form_data['founding_year']:'';
            $arr_client['website']       = isset($form_data['website'])?$form_data['website']:'';
            $arr_client['profile_summary']    = $form_data['profile_summary'];

            $status = $obj_client->update($arr_client);

            $first_name = ucfirst($form_data['first_name']);
            $last_name = ucfirst($form_data['last_name']);

            $this->UserModel->where('id',$this->user_id)->update(['first_name'=>$first_name,'last_name'=>$last_name]);
            
            if(isset($form_data['email']) && isset($form_data['old_email']) && $form_data['email'] != $form_data['old_email'])
            {
              $new_email = $form_data['email'];
              $this->UserModel->where('id',$this->user_id)->update(['email'=>$new_email,'is_email_changed'=>'1','is_email_verified'=>'0']);

              $user                      = Sentinel::findById($this->user_id);
              $activation                = Activation::create($user); /* Create avtivation */
              $activation_code           = $activation->code; // get activation code
              $email_id                  = $new_email;
              $data['user_id']           = $this->user_id;
              $data['activation_code']   = base64_encode($activation_code);
              $data['name']              = ucfirst($obj_client->first_name);//ucfirst($form_data['first_name']);
              $data['email_id']          = $new_email;
              $data['confirmation_link'] = url('/').'/confirm_email/'.base64_encode($this->user_id).'/'.base64_encode($activation_code);
              $project_name = config('app.project.name');
              //$mail_form = isset($website_contact_email)?$website_contact_email:'support@archexperts.com';
              $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
              try{
                    $mail_status = $this->MailService->send_change_email_confirmation_email($data);
                  // Mail::send('front.email.change_email_confirmation', $data, function ($message) use ($email_id,$mail_form,$project_name) {
                  //           $message->from($mail_form, $project_name);
                  //           $message->subject($project_name.':Email Confirmation.');
                  //           $message->to($email_id);
                  // });
              } 
              catch(\Exception $e)
              {
                Session::Flash('error',trans('controller_translations.text_mail_not_sent'));
              }
              
              $user_data['email'] = $new_email;

              Session::flash('success', trans('controller_translations.success_email_changed_successfully_please_check_your_email_account_for_confirmation_link'));
              Sentinel::logout();
              return redirect(url('/login'));
            }
            
        }

        if ($status) 
        {
            $user_data['first_name'] = ucfirst($form_data['first_name']);
            $user_data['last_name'] = ucfirst($form_data['last_name']);
            $this->WalletService->update_user($user_data);

            Session::flash('success',trans('controller_translations.success_profile_details_updated_successfully'));
        }
        else
        {
            Session::flash('error',trans('controller_translations.error_while_updating_profile_details'));
        }
        
        return redirect()->back();
    }



     /*
    | Change pass : load view for change password
    | auther :Sagar Sainkar
    */
    public function change_password()
    {
      $this->arr_view_data['page_title'] = trans('controller_translations.page_title_change_password');
      return view('client.profile.change_password',$this->arr_view_data);
    }

    /*
    | description : validate and update client password
    | auther :Sagar Sainkar
    */ 

    public function update_password(Request $request)
    {
        $arr_rules = array();
        $arr_rules['current_password'] = "required";
        $arr_rules['password']         = 'required|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/';
        $arr_rules['confirm_password'] = 'required|same:password|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/';

        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        
        $user = Sentinel::check();
        
        $credentials = array();
        $password = trim($request->input('current_password'));
        $credentials['email'] = $user->email;        
        $credentials['password'] = $password;

        if (Sentinel::validateCredentials($user,$credentials)) 
        { 
          $new_credentials = [];
          $new_credentials['password'] = $request->input('password');

          if(Sentinel::update($user,$new_credentials))
          {
            Session::flash('success', trans('controller_translations.success_password_change_successfully'));
          }
          else
          {
            Session::flash('error', trans('controller_translations.error_problem_occured_while_changing_password'));
          }          
        } 
        else
        {
          Session::flash('error', trans('controller_translations.error_your_current_password_is_invalid'));          
        }       
        
        return redirect()->back(); 
    }

    /*
    | Comment : Loading availability page
    | auther  : Ashwini K.
    */

     public function availability()
    {
      $arr_client_details = array();

      $obj_client = $this->UserModel->where('id',$this->user_id)->first(['is_available']);

      if ($obj_client!=FALSE) 
      {
          $arr_client_details = $obj_client->toArray();
      }
      $this->arr_view_data['page_title'] = trans('controller_translations.page_title_client_availability');
      $this->arr_view_data['arr_client_details'] = $arr_client_details;
      $this->arr_view_data['module_url_path'] = $this->module_url_path;

      return view('client.availability.index',$this->arr_view_data);
    }

     /*
    | Comment : Update availability 
    | auther  : Ashwini K.
    */

     public function update_availability(Request $request)
    {
        $arr_rules = array();
        $arr_rules['user_availability']= "required";

        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = array();
        $form_data = $request->all();
        if(isset($form_data['user_availability']))
        {    
         $obj_client = $this->UserModel->where('id',$this->user_id)->first();
         
          if($obj_client!=FALSE)
          {
            if ($form_data['user_availability']==1) 
            {
              $status = $obj_client->update(['is_available'=>1]);
            }
            elseif ($form_data['user_availability']==0) 
            {
              $status = $obj_client->update(['is_available'=>0]); 
            }
            
          }
        
          if($status!=false) 
          {
            Session::flash('success',trans('controller_translations.success_availability_updated_successfully'));
          }
          else
          {
            Session::flash('error',trans('controller_translations.error_while_updating_availability'));
          } 
        }

        return redirect()->back(); 

     }   


     /* 
      Comments : Delete images from database and from folder.
      Auther   : Nayan S.
     */

     public function delete_image(Request $request)
     {
        $arr_client = $json = [];
        $obj_client = $this->ClientsModel->where('user_id',$this->user_id)->first(['id','user_id','profile_image']);
        if($obj_client)
        {
          $arr_client = $obj_client->toArray();  
          $profile_image = isset($arr_client['profile_image'])?trim($arr_client['profile_image']):'';
          if($profile_image != "")
          {
            $result = $this->ClientsModel->where('user_id',$this->user_id)->update(['profile_image'=>'']);
            if($result)
            {
              $json['status'] = 'SUCCESS_PROFILE';
              $json['msg']  = trans('controller_translations.msg_profile_image_deleted_successfully');
              @unlink($this->profile_img_base_path.$profile_image);
            }
          }
        }
        return response()->json($json);
     }

     /* 
      Comments : Delete images from database and from folder.
      Auther   : Nayan S.
     */

     public function delete_project_attachment(Request $request)
     {
        $arr_project = $json = [];
        $project_id  = base64_decode($request->input('project_id'));

        $obj_project = $this->ProjectpostModel->where('id',$project_id)
                                              ->first(['id','client_user_id','project_attachment']);
        if($obj_project)
        {
          $arr_project = $obj_project->toArray();  
          
          $project_attachment = isset($arr_project['project_attachment'])?trim($arr_project['project_attachment']):'';

          if($project_attachment != "")
          {
            $result = $this->ProjectpostModel->where('id',$project_id)->update(['project_attachment'=>'']);
            
            if($result)
            {
              $json['status'] = 'SUCCESS_PROJECT';
              $json['msg']  = trans('controller_translations.msg_profile_attachment_deleted_successfully');
              @unlink($this->project_attachment_base_path.$project_attachment);
            }

          }

        }
        return response()->json($json);
     }


    /*
      Comments : Show Client portfolo. 
      Auther   : Nayan S.
    */ 

    public function show_portfolio($enc_id)
    {
        $arr_client       = array();
        $review_cnt       = $cnt_projects= 0;
        $arr_review_links = array();
        $arr_review       = array();

       if($enc_id!=FALSE && $enc_id!="")
       {

           $client_id     = base64_decode($enc_id);
           $obj_client    = $this->ClientsModel->where('user_id',$client_id)
                                               ->with(['user_details','country_details','city_details'])
                                               ->first();
                    
           $obj_reviews   = $this->ReviewsModel->where('to_user_id',$client_id)
                                               ->with('expert_details')
                                               ->with(['project_details' => function ($query) {
                                                    //$query->select('id','client_user_id','project_name');
                                                 }])
                                               ->orderBy('created_at', 'DESC')
                                               ->paginate(6);
                                               

           $review_cnt    = $this->ReviewsModel->where('to_user_id',$client_id)
                                               ->with('expert_details')
                                               ->count();  

           
           $cnt_projects  = $this->ProjectpostModel->where('client_user_id',$client_id)
                                                   ->count();
           
           if($obj_client!=FALSE)
           {
              $arr_client      = $obj_client->toArray();
           }

           if($obj_reviews!=FALSE)
           {
             $arr_review_links= clone $obj_reviews;
             $arr_review = $obj_reviews->toArray();
           }
       }

        $this->arr_view_data['page_title']               =  trans('controller_translations.client_portfolio');
        //trans('controller_translations.page_title_expert_portfolio');
        $this->arr_view_data['arr_client']               =  $arr_client;
        
        $this->arr_view_data['arr_review']               =  $arr_review;
        $this->arr_view_data['review_cnt']               =  $review_cnt;
        $this->arr_view_data['cnt_projects']             =  $cnt_projects;

        $this->arr_view_data['arr_review_links']         =  $arr_review_links;
        $this->arr_view_data['profile_img_public_path']  =  $this->profile_img_public_path;

        return view('client.profile.view_portfolio',$this->arr_view_data);
    }
    
    public function store_profile_image(Request $request)
    {
        $image_upload  = FALSE;
        $arr_response  = [];
        $user_id       = Sentinel::getUser()->id;
        $obj_user      = $this->ClientsModel->where('user_id','=',$user_id)->first(['id','user_id','profile_image']);
        if($obj_user)
        {
          $profile_image_name =  $obj_user->profile_image;
        }

        if($request->hasFile('file')) 
        {
            $image_validation = Validator::make(array('file'=>$request->file('file')),
                                                array('file'=>'mimes:jpg,jpeg,png'));

            if($request->file('file')->isValid() && $image_validation->passes())
            {
                $image_path         =   $request->file('file')->getClientOriginalName();
                $image_extention    =   $request->file('file')->getClientOriginalExtension();
                $image_name         =   sha1(uniqid().$image_path.uniqid()).'.'.$image_extention;
                $final_image = $request->file('file')->move($this->profile_img_base_path , $image_name);

                if(isset($profile_image_name) && $profile_image_name != "")
                { 
                  @unlink($this->profile_img_base_path.'/'.$profile_image_name);
                }

                $image_upload = TRUE; 
            }
            else
            {
                $arr_response['status'] =   "ERROR";
                $arr_response['msg']    =   trans('controller_translations.file_is_not_an_image_file');
                return response()->json($arr_response);
            }
        }
        if( $image_upload  == TRUE )
        {
            $obj_update  = $this->ClientsModel->where('user_id','=',$user_id)->update(['profile_image'=>$image_name]);
            
            if($obj_update)
            {
                $arr_response['status']     =   "SUCCESS";
                $arr_response['image_name'] =   $this->profile_img_public_path.$image_name;
                $arr_response['msg']        =   trans('controller_translations.profile_image_saved_successfully');
            }
            else
            {   
                $arr_response['status'] =   "ERROR";
                $arr_response['msg']    =   trans('controller_translations.error_saving_file');
            }
            return response()->json($arr_response);
        }
    }
    public function collaboration_listing()
    {
      $this->arr_view_data['page_title'] = 'Collaboration listing';
      $this->arr_view_data['module_url_path'] = $this->module_url_path;
      return view('client.collaboration.index',$this->arr_view_data);
    }

    function GetRandomString($country_string,$user_type_string,$first_string) 
    { 
      $user = Sentinel::check();
      $user_type_string = isset($user_type_string)?substr($user_type_string,0,3):'';
      $first_string     = isset($first_string)?substr($first_string, 0, 3):'';


      $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
      $randomString = ''; 
    
      for ($i = 0; $i < 4; $i++) { 
          $index = rand(0, strlen($characters) - 1); 
          $randomString .= $characters[$index]; 
      } 
      
      $final_string = $country_string.''.$user_type_string.''.$first_string.''.$randomString;
      //dd($final_string);
      if(isset($user->id) && $user->id != ""){
        $user_count = $this->UserModel->where('user_name',$final_string)->where('id','!=',$user->id)->count();
      }
      else{
        $user_count = $this->UserModel->where('user_name',$final_string)->count();  
      }

      if ($user_count > 0) 
      {
          $this->GetRandomString($country_string,$user_type_string,$first_string);
      }
      else
      {
          return $final_string;
      }

    } 

} // end class
