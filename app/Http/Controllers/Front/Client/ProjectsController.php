<?php
  namespace App\Http\Controllers\Front\Client;
  use Illuminate\Http\Request;
  use App\Http\Controllers\Controller;
  use App\Http\Requests;
  use App\Models\UserModel;
  use App\Models\ClientsModel;
  use App\Models\CategoriesModel;
  use App\Models\ProjectpostModel;
  use App\Models\SubCategoriesModel;
  use App\Models\SkillsModel;
  use App\Models\ProjectsBidsModel;
  use App\Models\ReviewsModel;
  use App\Models\ProjectRequestsModel;
  use App\Common\Services\LanguageService; 
  use App\Models\ProjectSkillsModel;
  use App\Models\StaticPageModel;
  use App\Models\NotificationsModel;
  use App\Models\SiteSettingModel;
  use App\Models\ProjectAttchmentModel;
  use App\Common\Services\MailService;
  use App\Common\Services\WalletService;
  use App\Models\ExpertsModel;
  use App\Models\InviteProjectModel;
  use App\Models\ProjectPriorityPackages;
  use App\Models\MilestonesModel;
  use App\Models\CurrencyModel;
  use App\Models\UserWalletModel;
  use App\Models\TransactionsModel;

  use App\Models\ProjectPostDocumentsModel;
  /*Used helper method to fire event*/
  use App\Events\sendEmailOnPostProject;
  use Sentinel;
  use Validator;
  use Session;
  use Mail;
  use App;
  use Lang;
  use DB;
  use Image;
  use Zipper;
  use Redirect;
  use Illuminate\Pagination\Paginator;
  use Illuminate\Pagination\LengthAwarePaginator;

  class ProjectsController extends Controller
  {
      public $arr_view_data;
      public function __construct(ProjectpostModel $projectpost,
                                  SkillsModel $skill,
                                  SubCategoriesModel $sub_category,
                                  UserModel $user_model,
                                  ClientsModel $clients,
                                  CategoriesModel $category,
                                  LanguageService $langauge,
                                  ProjectsBidsModel $project_bids,
                                  ReviewsModel $review,
                                  ProjectRequestsModel $project_requests,
                                  ProjectSkillsModel  $project_skills,
                                  StaticPageModel $static_pages,
                                  NotificationsModel $notifications,
                                  SiteSettingModel $site_settings,
                                  ProjectAttchmentModel $project_attachment,
                                  ExpertsModel $experts,
                                  InviteProjectModel $Invite_project,
                                  MailService $mail_service,
                                  ProjectPriorityPackages $ProjectPriorityPackages,
                                  WalletService $WalletService,
                                  MilestonesModel $milestones_model,
                                  ProjectPostDocumentsModel $project_post_documents_model
                                  )
      {
        if(! $user = Sentinel::check()) {
          return redirect('/login');
        }
        $this->user_id                               = $user->id;
        $this->email                                 = $user->email;
        $this->user_name                             = $user->user_name;
        $this->UserModel                             = $user_model;
        $this->ClientsModel                          = $clients;
        $this->ProjectpostModel                      = $projectpost;
        $this->CategoriesModel                       = $category;
        $this->LanguageService                       = $langauge;
        $this->SkillsModel                           = $skill;
        $this->SubCategoriesModel                    = $sub_category;
        $this->ProjectsBidsModel                     = $project_bids;
        $this->ReviewsModel                          = $review;
        $this->NotificationsModel                    = $notifications;
        $this->ProjectRequestsModel                  = $project_requests;
        $this->ProjectSkillsModel                    = $project_skills;
        $this->StaticPageModel                       = $static_pages;
        $this->SiteSettingModel                      = $site_settings;
        $this->ProjectAttchmentModel                 = $project_attachment;
        $this->ProjectPostDocumentsModel             = $project_post_documents_model;
        $this->MailService                           = $mail_service;
        $this->ExpertsModel                          = $experts;
        $this->InviteProjectModel                    = $Invite_project;
        $this->ProjectPriorityPackages               = $ProjectPriorityPackages;
        $this->WalletService                         = $WalletService;
        $this->MilestonesModel                       = $milestones_model;
        $this->CurrencyModel                         = new CurrencyModel;
        $this->UserWalletModel                       = new UserWalletModel;
        $this->TransactionsModel                     = new TransactionsModel;
        $this->arr_view_data                         = [];
        $this->module_url_path                       = url("/client/projects");
        $this->login_url                             = url("/login");
        $this->profile_img_base_path                 = base_path() . '/public'.config('app.project.img_path.profile_image');
        $this->profile_img_public_path               = url('/public').config('app.project.img_path.profile_image');
        $this->bid_attachment_public_path            = url('/public').config('app.project.img_path.bid_attachment');
        $this->project_attachment_public_path        = url('/public').config('app.project.img_path.project_attachment');
        $this->project_attachment_base_path          = public_path().config('app.project.img_path.project_attachment');
        $this->expert_project_attachment_public_path = url('/public').config('app.project.img_path.expert_project_attachment');

        if(isset($user)){
          $logged_user                 = $user->toArray();  
          if($logged_user['mp_wallet_created'] == 'Yes'){
            $this->mp_user_id          = $logged_user['mp_user_id'];
            $this->mp_wallet_id        = $logged_user['mp_wallet_id'];
            $this->mp_wallet_created   = $logged_user['mp_wallet_created'];
          } else {
            $this->mp_user_id          = '';
            $this->mp_wallet_id        = '';
            $this->mp_wallet_created   = 'No';
          }
        } else {
          $this->mp_user_id          = '';
          $this->mp_wallet_id        = '';
          $this->mp_wallet_created   = 'No';
        }
      }
      /*Post: load page of post project
      Author: Tushar Ahire*/
      public function post($project_id = FALSE)
      {
        $arr_job_document_formats = $arr_currency = array();
        $arr_job_document_formats = config('app.project_post_document_formats');

        $arr_skills           = array();
        $arr_client_details   = array();
        $arr_category_details = array();
        $arr_categories       = array();
        $aar_fixed_rates      = array();
        $arr_project_currency = array();
        $aar_hourly_rates     = array();
        $obj_client = $this->ClientsModel->where('user_id',$this->user_id)
                                         ->with(['user_details','country_details.states','state_details.cities','city_details'])
                                         ->first();
        if($obj_client != FALSE) {
          $arr_client_details = $obj_client->toArray();
        }
        

        // if($arr_client_details['first_name']             == "" &&
        // $arr_client_details['last_name']                 == "" &&
        // $arr_client_details['user_details']['user_name'] == ""){
        //   Session::flash("error",trans('controller_translations.post_project_check_profile_completion'));
        //   return redirect('/client/profile');
        // }
        // if($arr_client_details['first_name']             == ""){
        //   Session::flash("error",trans('controller_translations.post_project_check_first_name_exist'));
        //   return redirect('/client/profile');
        // }
        // if($arr_client_details['last_name']              == ""){
        //   Session::flash("error",trans('controller_translations.post_project_check_last_name_exist'));
        //   return redirect('/client/profile');
        // }     
        // if($arr_client_details['user_type'] == 'business' && $arr_client_details['vat_number'] == ""){
        //   Session::flash("error",trans('controller_translations.post_project_check_vat_number_exist'));
        //   return redirect('/client/profile');
        // }
        // if($arr_client_details['user_details']['user_name']  == ""){
        //   Session::flash("error",trans('controller_translations.post_project_check_username_exist'));
        //   return redirect('/client/profile');
        // }
        /* getting category array */
        $obj_category     = $this->CategoriesModel->where('is_active','1')->get();
        if($obj_category != FALSE)
        {
          $arr_category_details = $obj_category->toArray();
        }
        $arr_lang     = $this->LanguageService->get_all_language();
        $obj_category = $this->CategoriesModel->get();
        if($obj_category != FALSE)
        {
            $arr_categories = $obj_category->toArray();
        }
        $obj_skills = $this->SkillsModel->where('is_active',1)->get();
        if($obj_skills != FALSE)
        {
            $arr_skills = $obj_skills->toArray();
        }
        
        $obj_static_privacy =  $this->StaticPageModel->where('page_slug','privacy-policy')->where('is_active',"1")->first();
        $arr_static_privacy = array();
        if($obj_static_privacy)
        {
          $arr_static_privacy = $obj_static_privacy->toArray();
        }
        $obj_static_terms =  $this->StaticPageModel->where('page_slug','terms-of-service')->where('is_active',"1")->first();
        $arr_static_terms = array();
        if($obj_static_terms)
        {
          $arr_static_terms = $obj_static_terms->toArray();
        }

        if (App::isLocale('de')) 
        {
          $aar_fixed_rates  = config('app.project_fixed_rates.de');
          $aar_hourly_rates = config('app.project_hourly_rates.de');
        }
        else
        {
          $aar_fixed_rates  = config('app.project_fixed_rates.en');
          $aar_hourly_rates = config('app.project_hourly_rates.en');
        }
        $arr_project_currency = config('app.project_currency');
        /* for repost */
        if(isset($project_id) && $project_id !=""){
          $project_id          = base64_decode($project_id);
          $get_project_details = $this->ProjectpostModel->with(['skill_details','project_skills.skill_data'])->where('id',$project_id)->first();
          if($get_project_details != null){
            $project_details   = $get_project_details->toArray();
            $this->arr_view_data['project_details']      = $project_details;  
          }
        }


        /* end for repost */
        // get highlighted job packages from admin
        $packages     = [];
        $get_packages = $this->ProjectPriorityPackages
                             ->where('status','0')
                             ->get();
        if(sizeof($get_packages)>0)
        {
          $packages   = $get_packages->toArray();
        }  

        $arr_project_type_cost = [];
        $obj_project_type_cost = $this->SiteSettingModel->select('website_pm_initial_cost','wesite_project_manager_commission','website_pm_initial_cost','nda_price','urgent_price','private_price','recruiter_price')->first();
        if($obj_project_type_cost)
        {
          $arr_project_type_cost = $obj_project_type_cost->toArray();
        }

        $obj_currency = $this->CurrencyModel->select('id','currency_code','currency','description')
                                            ->where('is_active','1')
                                            ->get();
        if($obj_currency)
        {
          $arr_currency  = $obj_currency->toArray();
        }

        $this->arr_view_data['arr_project_duration'] = config('app.project_duration');
        $this->arr_view_data['arr_hours_per_week'] = config('app.hours_per_week');
        
        $this->arr_view_data['highlighted_packages'] = $packages;
        $this->arr_view_data['arr_project_type_cost'] = $arr_project_type_cost;
        // end get highlighted job packages from admin
        $this->arr_view_data['arr_client_details']       = $arr_client_details;
        $this->arr_view_data['arr_categories']           = $arr_categories;
        $this->arr_view_data['arr_skills']               = $arr_skills;
        $this->arr_view_data['arr_currency']             = $arr_currency;
        $this->arr_view_data['aar_fixed_rates']          = $aar_fixed_rates;
        $this->arr_view_data['aar_hourly_rates']         = $aar_hourly_rates;
        $this->arr_view_data['arr_project_currency']     = $arr_project_currency;
        $this->arr_view_data['arr_static_privacy']       = $arr_static_privacy;
        $this->arr_view_data['arr_static_terms']         = $arr_static_terms;
        $this->arr_view_data['arr_job_document_formats'] = $arr_job_document_formats;
        $this->arr_view_data['page_title']               = trans('controller_translations.page_title_post_a_job');
        $this->arr_view_data['module_url_path']          = $this->module_url_path;

        return view('client.projects.post',$this->arr_view_data);
      }
      /*Post: load page of hire expert
      Author: Tushar Ahire*/
      public function hire($expert_user_id = FALSE)
      {
        $arr_skills           = array();
        $arr_client_details   = array();
        $arr_category_details = array();
        $arr_categories       = array();
        $aar_fixed_rates      = array();
        $arr_project_currency = array();
        $aar_hourly_rates     = array();
        $obj_client = $this->ClientsModel->where('user_id',$this->user_id)
                                         ->with(['user_details','country_details.states','state_details.cities','city_details'])
                                         ->first();

        if($obj_client != FALSE) {
          $arr_client_details = $obj_client->toArray();
        }
        if($arr_client_details['first_name']             == "" &&
        $arr_client_details['last_name']                 == "" &&
        $arr_client_details['company_name']              == "" &&
        $arr_client_details['vat_number']                == "" &&
        $arr_client_details['user_details']['user_name'] == ""){
          Session::flash("error",trans('controller_translations.post_project_check_profile_completion'));
          return redirect('/client/profile');
        }
        if($arr_client_details['first_name']             == ""){
          Session::flash("error",trans('controller_translations.post_project_check_first_name_exist'));
          return redirect('/client/profile');
        }
        if($arr_client_details['last_name']              == ""){
          Session::flash("error",trans('controller_translations.post_project_check_last_name_exist'));
          return redirect('/client/profile');
        }
        if($arr_client_details['company_name']           == ""){
          Session::flash("error",trans('controller_translations.post_project_check_company_name_exist'));
          return redirect('/client/profile');
        }
        if($arr_client_details['vat_number']             == ""){
          Session::flash("error",trans('controller_translations.post_project_check_vat_number_exist'));
          return redirect('/client/profile');
        }
        if($arr_client_details['user_details']['user_name']  == ""){
          Session::flash("error",trans('controller_translations.post_project_check_username_exist'));
          return redirect('/client/profile');
        }
        /* getting category array */
        $obj_category     = $this->CategoriesModel->where('is_active','1')->get();
        if($obj_category != FALSE)
        {
          $arr_category_details = $obj_category->toArray();
        }
        $arr_lang     = $this->LanguageService->get_all_language();
        $obj_category = $this->CategoriesModel->get();
        if($obj_category != FALSE)
        {
            $arr_categories = $obj_category->toArray();
        }
        $obj_skills = $this->SkillsModel->where('is_active',1)->get();
        if($obj_skills != FALSE)
        {
            $arr_skills = $obj_skills->toArray();
        }
        $obj_static_privacy =  $this->StaticPageModel->where('page_slug','privacy-policy')->where('is_active',"1")->first();
        $arr_static_privacy = array();
        if($obj_static_privacy)
        {
          $arr_static_privacy = $obj_static_privacy->toArray();
        }
        $obj_static_terms =  $this->StaticPageModel->where('page_slug','terms-of-service')->where('is_active',"1")->first();
        $arr_static_terms = array();
        if($obj_static_terms)
        {
          $arr_static_terms = $obj_static_terms->toArray();
        }
        if (App::isLocale('de')) 
        {
          $aar_fixed_rates  = config('app.project_fixed_rates.de');
          $aar_hourly_rates = config('app.project_hourly_rates.de');
        }
        else
        {
          $aar_fixed_rates  = config('app.project_fixed_rates.en');
          $aar_hourly_rates = config('app.project_hourly_rates.en');
        }
        
        $obj_currency = $this->CurrencyModel->select('id','currency_code','currency','description')
                                            ->where('is_active','1')
                                            ->get();
        if($obj_currency)
        {
          $arr_currency  = $obj_currency->toArray();
        }


        /* for hire */
        if(isset($expert_user_id) && $expert_user_id !=""){
            $this->arr_view_data['expert_user_id']      = $expert_user_id;  
        }
        /* end for repost */

        $arr_job_document_formats = array();
        $arr_job_document_formats = config('app.project_post_document_formats');

        $this->arr_view_data['arr_project_duration'] = config('app.project_duration');
        $this->arr_view_data['arr_hours_per_week'] = config('app.hours_per_week');
        
        $this->arr_view_data['arr_job_document_formats']  = $arr_job_document_formats;
        $this->arr_view_data['arr_client_details']  = $arr_client_details;
        $this->arr_view_data['arr_categories']      = $arr_categories;
        $this->arr_view_data['arr_skills']          = $arr_skills;
        $this->arr_view_data['aar_fixed_rates']     = $aar_fixed_rates;
        $this->arr_view_data['aar_hourly_rates']    = $aar_hourly_rates;
        $this->arr_view_data['arr_currency']        = $arr_currency;
        $this->arr_view_data['arr_static_privacy']  = $arr_static_privacy;
        $this->arr_view_data['arr_static_terms']    = $arr_static_terms;
        $this->arr_view_data['page_title']          = trans('controller_translations.page_title_post_a_job');
        $this->arr_view_data['module_url_path']     = $this->module_url_path;
        $this->profile_img_public_path   = url('/').config('app.project.img_path.profile_image');
        return view('client.projects.hire',$this->arr_view_data);
      }

      public function create(Request $request)
      {
          $arr_rules                        = array();
          $status                           = FALSE;
          $arr_rules['category']            = "required";
          $arr_rules['project_name']        = "required";
          //$arr_rules['project_skills']      = "required";
          $arr_rules['project_description'] = "required";
          $arr_rules['sub_category']        = "required";
          $arr_rules['project_duration']    = "required";
          $arr_rules['project_currency']    = "required";
          if (isset($request->price_method) && $request->price_method=='1') {
            $arr_rules['fixed_rate']    = "required";  
          }
          else if (isset($request->price_method) && $request->price_method=='2') {
            $arr_rules['hourly_rate']   = "required";
          }
          $validator = Validator::make($request->all(),$arr_rules);
          if($validator->fails()){
              Session::flash("error",$validator->errors()->first());
              return redirect()->back()->withErrors($validator)->withInput($request->all());
          }

          $hire_expert_user_id   = '0';
          $is_hire_process       = 'NO';
          $arr_data              = array();
          $category              = $request->category;
          $project_name          = $request->project_name;
          $project_skills        = $request->project_skills;
          $sub_category          = $request->sub_category;
          $project_description   = $request->project_description;
          $start_date            = '0000-00-00 00:00:00';//$request->start_date;
          $end_date              = '0000-00-00 00:00:00';//$request->end_date;
          
          $bid_closing_date      = '0000-00-00 00:00:00';
          if(!empty($request->bid_closing_date) && $request->bid_closing_date != ''){
              
              // $tmp_date = $request->bid_closing_date.' '.date('H:i:s');
              // $bid_closing_date      = date('Y-m-d H:i:s',strtotime($tmp_date));
              $today_date = date('Y-m-d H:i:s');
              $today_date = strtotime($today_date);
              $today_date = strtotime("+".$request->bid_closing_date." day", $today_date);
              $final_date =  date('Y-m-d H:i:s', $today_date);
              $bid_closing_date      = $final_date;
          }
          $project_duration      = $request->project_duration;
          $fixed_cost            = $request->fixed_rate;
          $price_method          = $request->price_method;
          $hourly_cost           = $request->hourly_rate;
          $project_currency      = $request->project_currency;
          $handle_by             = '1';

          $project_type_private           = '0';
          $project_type_nda               = '0';
          $project_type_urgent            = '0';
          $project_highlight              = '0';
          $project_handle_by_recruiter    = '0';
          $project_highlight_start_date   = '';
          $project_highlight_end_date     = '';
          $urjent_heighlight_end_date     = '';
          $is_project_highlighted         = '0';
          
          if(!empty($request->input('project_type_private')) && $request->input('project_type_private') == 'on')
          {
            $project_type_private = '1'; 
          }
          if(!empty($request->input('project_type_nda')) && $request->input('project_type_nda') == 'on')
          {
            $project_type_nda     = '1';  
          }
          if(!empty($request->input('project_type_urgent')) && $request->input('project_type_urgent') == 'on')
          {
            $project_type_urgent     = '1';  
            $urjent_heighlight_end_date    = date('Y-m-d H:i:s', strtotime("+".'1'." days"));
          }
          //dd($request->input('project_highlight'));
          if(!empty($request->input('project_highlight')) && $request->input('project_highlight') !="")
          {
            $expld = explode('-', $request->input('project_highlight'));
            if(isset($expld[0])){ 
              $project_highlight             = $expld[0]; 
              $project_highlight_start_date  = date('Y-m-d H:i:s');
              $project_highlight_end_date    = date('Y-m-d H:i:s', strtotime("+".$expld[0]." days"));
              $is_project_highlighted        = '1';
            }
          }
          //dd($project_highlight_start_date, $project_highlight_end_date);

          if(!empty($request->input('project_handle_by_recruiter')) && $request->input('project_handle_by_recruiter') == 'on')
          { 
            $handle_by                       = '2';   // Recruiter
          }

          $project_currency_code = $request->input('project_currency_code');

          /*------------Client Profile Update-----------------------*/
            if($request->has('first_name')!='' && $request->has('last_name')!='')
            { 
              $arr_user_data['first_name'] = $request->input('first_name');
              $arr_user_data['last_name'] = $request->input('last_name');
              $this->UserModel->where('id',$this->user_id)->update($arr_user_data);

              if($request->has('vat_number'))
              {
                $arr_client_details_update['vat_number'] = $request->input('vat_number'); 
              }

              $arr_client_details_update['first_name'] = $request->input('first_name');
              $arr_client_details_update['last_name'] = $request->input('last_name');
              $this->ClientsModel->where('user_id',$this->user_id)->update($arr_client_details_update);

            }
            $obj_user = $this->UserModel->where('id',$this->user_id)->first();
            if($obj_user->mp_wallet_created!='Yes')
            {
              $arr_client = $obj_user->toArray();
              $arr_client['selected_currency_code'] = $project_currency_code;
              $this->create_user_wallet($arr_client);
            }

          /*------------End of Client Profile Update-----------------------*/

          // check wallets existance
          if (isset($handle_by) && $handle_by == '2'                       ||
              isset($project_type_private) && $project_type_private == '1' ||
              isset($project_type_nda) && $project_type_nda == '1'         ||
              isset($project_highlight) && $project_highlight > '0' )
          {
   
              $admin_data                  = get_admin_email_address();
              if(isset($admin_data) && isset($admin_data['mp_wallet_created']) && $admin_data['mp_wallet_created'] =='No')
              {
                // send notification to admin
                $arr_admin_data                         =  [];
                $arr_admin_data['user_id']              =  $admin_data['id'];
                $arr_admin_data['user_type']            = '1';
                $arr_admin_data['url']                  = 'admin/wallet/archexpert';
                $arr_admin_data['project_id']           = '';
                $arr_admin_data['notification_text_en'] = Lang::get('controller_translations.create_wallet',[],'en','en');
                $arr_admin_data['notification_text_de'] = Lang::get('controller_translations.create_wallet',[],'de','en');
                $this->NotificationsModel->create($arr_admin_data);      
                // end send notification to admin   
                Session::flash("error",trans('controller_translations.you_dont_have_permission_to_get_project_features_because_admin_dont_have_created_his_wallet_for_transactions'));
                return redirect()->back()->withErrors($validator)->withInput($request->all());
              }


              $obj_client = $this->ClientsModel->where('user_id',$this->user_id)
                                         ->with(['user_details','country_details.states','state_details.cities','city_details'])
                                         ->first();
              if($obj_client != FALSE) {
                $arr_client_details = $obj_client->toArray();
              }

             /* if($arr_client_details['first_name']             == "" &&
              $arr_client_details['last_name']                 == "" &&
              $arr_client_details['user_details']['user_name'] == "")
              {
                Session::flash("error",trans('controller_translations.post_project_check_profile_completion'));
                return redirect('/client/profile');
              }
              if($arr_client_details['first_name']             == ""){
                Session::flash("error",trans('controller_translations.post_project_check_first_name_exist'));
                return redirect('/client/profile');
              }
              if($arr_client_details['last_name']              == ""){
                Session::flash("error",trans('controller_translations.post_project_check_last_name_exist'));
                return redirect('/client/profile');
              }     
              if($arr_client_details['user_type'] == 'business' && $arr_client_details['vat_number'] == ""){
                Session::flash("error",trans('controller_translations.post_project_check_vat_number_exist'));
                return redirect('/client/profile');
              }
              if($arr_client_details['user_details']['user_name']  == ""){
                Session::flash("error",trans('controller_translations.post_project_check_username_exist'));
                return redirect('/client/profile');
              }
              if($arr_client_details['country']== "" || $arr_client_details['country']== NULL){
                Session::flash("error",'Please select your country');
                return redirect('/client/profile');
              }*/
              
              /*$wallet_url = url('/').'/client/wallet/dashboard';
              $obj_data = $this->UserModel->where('id',$this->user_id)->first();
              $is_wallet_created = isset($obj_data->mp_wallet_created)?$obj_data->mp_wallet_created:'';
              if($is_wallet_created!='Yes')
              {
                Session::flash("error_sticky",trans('controller_translations.text_please_create_an').' <a href="'.$wallet_url.'">'.trans('controller_translations.text_e_wallet').'</a> '.trans('controller_translations.text_first_and_add_your_payment_method_to_be_able_to_buy_website_features_and_make_transactions'));
                return redirect(url('/client/projects/post'));
              }*/
              
          }
          // end check wallet existance

          //Store project cost in min and max
          $min_fixed_cost = 0;
          $max_fixed_cost = 0;
          if(isset($fixed_cost) && $fixed_cost!=null)
          {
            $explode_fixed_cost   = explode('-',$fixed_cost);  
            $min_fixed_cost       = isset($explode_fixed_cost[0])?$explode_fixed_cost[0]:'';
            $max_fixed_cost       = isset($explode_fixed_cost[1])?$explode_fixed_cost[1]:'';
            if($min_fixed_cost!="" && $max_fixed_cost=="")
            {
              $max_fixed_cost  = $min_fixed_cost;
            }
          }
          elseif(isset($hourly_cost) && $hourly_cost!=null)
          {
            $explode_hourly_cost  = explode('-',$hourly_cost);
            $min_fixed_cost       = isset($explode_hourly_cost[0])?$explode_hourly_cost[0]:'';
            $max_fixed_cost       = isset($explode_hourly_cost[1])?$explode_hourly_cost[1]:'';
            if($min_fixed_cost!="" && $max_fixed_cost=="")
            {
              $max_fixed_cost = $min_fixed_cost;
            }
          }

          if($price_method == "1")
          {
              $project_cost = $fixed_cost;   
          }
          if($price_method == "2")
          {
              $project_cost = $hourly_cost;   
          }

          $is_custom_hour_per_week = "0";
          $hour_per_week = $request->hour_per_week;
          if(isset($request->hour_custom_rate) && $request->hour_custom_rate!="")
          {
            $is_custom_hour_per_week = "1";
            $hour_per_week = $request->hour_custom_rate;
          }

          $is_custom_price = "0";  
          if(isset($request->custom_rate) && $request->custom_rate!="")
          {
              $project_cost    = $request->custom_rate;
              $is_custom_price = "1";  
              $max_fixed_cost  = $project_cost;
          }
          $award_request_date_time = '';
          if(isset($request->hire_expert_user_id) && $request->hire_expert_user_id !=""){
               $project_status = '2';     
               $hire_expert_user_id = $request->hire_expert_user_id;     
               $is_hire_process     = 'YES';

               $award_request_date_time = date('Y-m-d H:i:s');

          }
          else
          {
              if (isset($handle_by) && $handle_by == '2'                       ||
                  isset($project_type_private) && $project_type_private == '1' ||
                  isset($project_type_nda) && $project_type_nda == '1'         ||
                  isset($project_type_urgent) && $project_type_urgent == '1'         ||
                  isset($project_highlight) && $project_highlight > '0' ){
                  $project_status = '0';   
              }
              else
              {
                  $project_status = '2';
              }
          }
          //Currenct code set 

         /* if(isset($project_currency) && $project_currency == "$")
          {
              $project_currency_code = trans('controller_translations.currency_usd');
          }
          else
          {
            $project_currency_code = trans('controller_translations.currency_eur');
          }*/
          
          /*file uploade code for laravel auther : Shankar*/
           $form_data = array();
           $form_data = $request->all();

           
           
          $usd_min_fixed_cost = $usd_max_fixed_cost = 0;
          if($project_currency_code != 'USD')
          {
            $to_currency = 'USD';
            $usd_min_fixed_cost  = currencyConverterAPI($project_currency_code,$to_currency,$min_fixed_cost);
            $usd_min_fixed_cost  = floor($usd_min_fixed_cost);

            $usd_max_fixed_cost  = currencyConverterAPI($project_currency_code,$to_currency,$max_fixed_cost);
            $usd_max_fixed_cost  = floor($usd_max_fixed_cost);
          }
          else
          {
            $usd_min_fixed_cost = $min_fixed_cost;
            $usd_max_fixed_cost = $max_fixed_cost;
          }
          
          /* Duplication Check */
          $project = $this->ProjectpostModel->create(['category_id'=>$category,
                                              'sub_category_id'=>$sub_category,
                                              'client_user_id'=>$this->user_id,
                                              'expert_user_id'=>$hire_expert_user_id,
                                              'project_name'=>ucfirst($project_name),
                                              'project_description'=>$project_description,
                                              'project_start_date'=>$start_date,
                                              'project_end_date'=>$end_date,
                                              'bid_closing_date'=>$bid_closing_date,
                                              'project_type'=>$project_type_private,
                                              'project_expected_duration'=>$project_duration,
                                              'project_cost'=>$project_cost,
                                              'min_project_cost'=>$min_fixed_cost,
                                              'max_project_cost'=>$max_fixed_cost,
                                              'project_currency'=>$project_currency,
                                              'project_currency_code'=>$project_currency_code,
                                              'project_pricing_method'=>$price_method,
                                              'project_handle_by'=>$handle_by,
                                              'project_status'=>$project_status,
                                              'is_hire_process'=>$is_hire_process,
                                              'is_nda'=>$project_type_nda,
                                              'is_urgent'=>$project_type_urgent,
                                              'urjent_heighlight_end_date'=>$urjent_heighlight_end_date,
                                              'highlight_days'=>$project_highlight,
                                              'highlight_start_date'=>$project_highlight_start_date,
                                              'highlight_end_date'=>$project_highlight_end_date,
                                              'award_request_date_time'=>$award_request_date_time,
                                              'is_custom_price'=>$is_custom_price,
                                              'is_highlight_edited'=>$is_project_highlighted,
                                              'usd_min_project_cost'=>$usd_min_fixed_cost,
                                              'usd_max_project_cost'=>$usd_max_fixed_cost,
                                              'is_custom_hour_per_week'=>$is_custom_hour_per_week,
                                              'hour_per_week'=>$hour_per_week
                                              ]);
          $insertedId = isset($project->id) ? $project->id : '';

          if(isset($insertedId) && $insertedId!='')
          {
            if($request->hasFile('contest_attachments'))
            {                 
              $documents  = $request->file('contest_attachments');

              foreach($documents as $key =>$doc)
              {
                $count = $this->ProjectPostDocumentsModel->where('project_id',$insertedId)->count();
        
                if($count < 10)
                { 
                  if(isset($doc))
                  {            
                    $filename         = $doc->getClientOriginalName();
                    $file_extension   = strtolower($doc->getClientOriginalExtension());
                    $file_size        = $doc->getClientSize();

                    if(in_array($file_extension,config('app.project_post_document_formats')) && $file_size <= 5000000)
                    {
                        $file_name       = sha1(uniqid().$doc.uniqid()).'.'.$file_extension;
                        $doc->move(base_path().'/public/uploads/front/postprojects/',$file_name);
                        $obj = $this->ProjectPostDocumentsModel->create(['project_id'=>$insertedId,
                                                                    'image_name'=>$file_name,
                                                                    'image_original_name'=>$filename]); 
                    }
                  }
                }
              }
            }

            /* Inserting project skills to the table */
            if(isset($project_skills) && count($project_skills)>0){
              $this->add_new_skills($project_skills,$insertedId);
            }

            $arr_job_document_formats = array();
            $arr_job_document_formats = config('app.project_post_document_formats');



            if(isset($request->hire_expert_user_id) && $request->hire_expert_user_id !=""){
                   $result = $this->ProjectRequestsModel->create([
                                                        'expert_user_id'=>base64_decode($request->hire_expert_user_id),
                                                        'project_id'=>$insertedId,
                                                        'client_user_id'=>$this->user_id
                                                      ]); 
                   $arr_bid_data                           = [];
                   $arr_bid_data['project_id']             = $insertedId;
                   $arr_bid_data['expert_user_id']         = base64_decode($request->hire_expert_user_id);
                   $arr_bid_data['bid_cost']               = $project_cost;
                   $arr_bid_data['bid_estimated_duration'] = $project_duration;
                   $arr_bid_data['bid_description']        = 'Hired by client';
                   $arr_bid_data['bid_status']             = '4';
                   $storebid                               = $this->ProjectsBidsModel->create($arr_bid_data);
                   
                  $posted_project_name = isset($project_name) ? ucfirst($project_name) : '';

                   /* Create Notification for client*/
                   $arr_data               =  [];
                   $arr_data['user_id']    =  $this->user_id;
                   $arr_data['user_type']  = '2'; 
                   $arr_data['url']                  = "client/projects/details/".base64_encode($insertedId);
                   $arr_data['notification_text_en'] = $posted_project_name . ' - ' . Lang::get('controller_translations.new_project_added_and_hired_successfull',[],'en','en');
                   $arr_data['notification_text_de'] = $posted_project_name . ' - ' . Lang::get('controller_translations.new_project_added_and_hired_successfull',[],'de','en');
                   $arr_data['project_id'] = $insertedId;
                   $this->NotificationsModel->create($arr_data); 
                   /* end create Notification for client*/


                   /* Create Notification for expert*/
                   $arr_data               =  [];
                   $arr_data['user_id']    =  $request->hire_expert_user_id;
                   $arr_data['user_type']  = '3'; 
                   $arr_data['url']                  = "expert/projects/details/".base64_encode($insertedId);
                   $arr_data['notification_text_en'] = $posted_project_name . ' - ' . Lang::get('controller_translations.new_project_awarded_by_client',[],'en','en');
                   $arr_data['notification_text_de'] = $posted_project_name . ' - ' . Lang::get('controller_translations.new_project_awarded_by_client',[],'de','en');
                   $arr_data['project_id'] = $insertedId;
                   $this->NotificationsModel->create($arr_data); 
                   /* end create Notification for expert*/

                   Session::flash("success",trans('controller_translations.new_project_added_and_hired_successfull'));
                   return redirect('client/projects/awarded');

            } 
            else 
            {
                 
                    /* Create Notification for admin */      
                    $arr_data =  [];
                    $arr_data['user_id']    =  1;
                    $arr_data['user_type']  = '1';
                    if($handle_by == '1')
                    {
                      $arr_data['url']                  = "admin/projects/open";
                      $arr_data['notification_text_en'] = Lang::get('controller_translations.new_project_added',[],'en','en');
                      $arr_data['notification_text_de'] = Lang::get('controller_translations.new_project_added',[],'de','en');
                    } 
                    else if($handle_by == '2')
                    {
                      $arr_data['url']                  = "admin/projects/posted_recruiter";
                      $arr_data['notification_text_en'] = Lang::get('controller_translations.assign_recruiter',[],'en','en');
                      $arr_data['notification_text_de'] = Lang::get('controller_translations.assign_recruiter',[],'de','en');
                    }  
                    $arr_data['project_id'] = $insertedId;
                    $this->NotificationsModel->create($arr_data);  
                    /* Notificatin ends */

                    /*if project is handel by recruiter / highlight project / private project / NDA project then client have to pay fixed cost : updated on 08-08-2016*/
                    if (isset($handle_by) && $handle_by == '2'                       ||
                        isset($project_type_private) && $project_type_private == '1' ||
                        isset($project_type_nda) && $project_type_nda == '1'         ||
                        isset($project_type_urgent) && $project_type_urgent == '1'   ||
                        isset($project_highlight) && $project_highlight > '0' )
                    {
                        return redirect($this->module_url_path.'/payment/'.base64_encode($insertedId));  
                    }
                    /*Used helper method to fire event*/
                    event(new sendEmailOnPostProject($insertedId));
                    Session::flash("success",trans('controller_translations.success_project_posted_successfully'));
                    return redirect('client/projects/posted');
            }        
          } else {
            Session::flash("error",trans('controller_translations.error_while_posting_project_please_try_again')); 
          }
          return redirect()->back();
      }
      
      public function subcatdata(Request $request)
      {  
          $id                = $request->id;
          $arr_subcategories = array();
          $arr_lang          = $this->LanguageService->get_all_language();
          $obj_subcategories = $this->SubCategoriesModel->with('category_details.translations')->where('category_id',$id)->get();

          if($obj_subcategories!=FALSE)
          {
            $arr_subcategories =  $obj_subcategories->toArray();
          }  

          echo '<option value="">'.trans('client/projects/post.text_select_sub_category').'</option>';
          foreach($arr_subcategories as $arr_option){
            $id = $arr_option['id'];
            $subcat_name = $arr_option['subcategory_title'];
            echo '<option value="'.$id.'">'.$subcat_name.'</option>';
          }     
          exit ();
      }

      /*Post: load page of posted project
        Author: Shankar Jamdhade*/
      public function posted(Request $request) 
      {
        $sort_by = '';
        if($request->has('sort_by') && $request->input('sort_by')!=''){
          $sort_by = $request->input('sort_by');
        }

        $this->arr_view_data['page_title'] = trans('controller_translations.page_title_manage_posted_projects');

        $obj_tmp_post_project = $this->ProjectpostModel->where('client_user_id',$this->user_id)
                                                    ->where('is_hire_process','<>','YES')
                                                   ->where(function ($q) 
                                                     {
                                                        $q->where('project_status','=','1');
                                                        $q->orWhere('project_status','=','2');
                                                     }) 
                                                   ->with('skill_details','project_skills.skill_data',
                                                    'project_bid_info','category_details','sub_category_details');
        if($sort_by == "is_urgent")
        {
          $obj_tmp_post_project = $obj_tmp_post_project->where('is_urgent','1')->orderBy('created_at','desc');
        }
        else if($sort_by == "public_type" || $sort_by == "private_type")
        {
          $project_type = '0';
          if($sort_by == "public_type")
          {
            $project_type = '0';
          }
          elseif($sort_by == "private_type")
          {
            $project_type = '1';
          }
          $obj_tmp_post_project = $obj_tmp_post_project->where('project_type',$project_type)->orderBy('created_at','desc');
        }
        else if($sort_by == "is_nda")
        {
          $obj_tmp_post_project = $obj_tmp_post_project->where('is_nda','1')->orderBy('created_at','desc');
        }

        $obj_tmp_post_project      = $obj_tmp_post_project
                                          ->get();

        $obj_tmp_post_project = collect($obj_tmp_post_project);

        if( $sort_by == "asc")
        { 
          $obj_tmp_post_project = $obj_tmp_post_project
                                  ->sortBy(['created_at']);
        }
        
        if($sort_by == "desc")
        { 
          $obj_tmp_post_project = $obj_tmp_post_project
                                  ->sortByDesc(['created_at']);
        }

        if( $sort_by == "budget-asc")
        { 
          $obj_tmp_post_project = $obj_tmp_post_project
                                  ->sortBy(['usd_max_project_cost']);
        }
        
        if($sort_by == "budget-desc")
        { 
          $obj_tmp_post_project = $obj_tmp_post_project
                                  ->sortByDesc(['usd_max_project_cost']);
        }

        if( $sort_by == "bids-asc")
        { 
          $obj_tmp_post_project = $obj_tmp_post_project
                                  ->sortBy(['bid_count']);
        }
        
        if($sort_by == "bids-desc")
        { 
          $obj_tmp_post_project = $obj_tmp_post_project
                                  ->sortByDesc(['bid_count']);
        }

        if($sort_by == "")
        {
          $obj_tmp_post_project = $obj_tmp_post_project
                                  ->sortByDesc(['created_at']);
        }

        $arr_tmp_posted_projects = [];
        if($obj_tmp_post_project){
          $arr_tmp_posted_projects = $obj_tmp_post_project->toArray();
          $arr_tmp_posted_projects = array_values($arr_tmp_posted_projects);
        }

        $arr_posted_projects = $arr_pagination = [];
        $obj_post_project =  $this->make_pagination_links($arr_tmp_posted_projects,config('app.project.pagi_cnt'));
        if($obj_post_project)
        {
            $arr_posted_projects = $obj_post_project->toArray();
            $arr_pagination      = clone $obj_post_project;
        }

        /*-------------------End of added by-------------------*/
        $this->arr_view_data['posted_projects'] = $arr_posted_projects;
        $this->arr_view_data['arr_pagination']  = $arr_pagination;
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        $this->arr_view_data['sort_by']         = $sort_by;
        
        $this->arr_view_data['profile_img_public_path'] = $this->profile_img_public_path;
        return view('client.projects.manage-posted-projects',$this->arr_view_data);
      }

      public function add_additional_info(Request $request)
      {
        $html            = '';
        $new_information = '';
        $project_id      = base64_decode($request->input('project_id',null));
        $project_info    = $request->input('project_add_info',null);
        
        if($project_id == null){
          Session::flash("error",trans('controller_translations.text_sorry_somthing_goes_wrong_please_try_again'));
        }
        $get_info = $this->ProjectpostModel->where('id',$project_id)
                                        ->where('client_user_id',$this->user_id)
                                        ->select('project_additional_info')
                                        ->first();
    
        if(isset($get_info['project_additional_info']) && $get_info['project_additional_info'] ==null)
        {
          if($project_info==null || $project_info=='')
          {
            $html="false";
          }

          $save_info = $this->ProjectpostModel->where('id',$project_id)->where('client_user_id',$this->user_id)->update(['project_additional_info'=>$project_info]);
        }
        else
        {
          $old_information = isset($get_info['project_additional_info']) ? $get_info['project_additional_info'] : '';

          $new_information = $old_information.' '.$project_info;
          $update_info = $this->ProjectpostModel->where('id',$project_id)->where('client_user_id',$this->user_id)->update(['project_additional_info'=>$new_information]);
        }

        if($html != "false"){
          if(isset($save_info) && $save_info) {
            Session::flash("success",trans('client/contest/common.text_additional_information_added_succesfully'));
            $html = "true";
          } elseif(isset($update_info) && $update_info) {
            Session::flash("success",trans('client/contest/common.text_additional_information_updated_succesfully'));
            $html = "true";
          }else{
            Session::flash('error',trans('client/contest/common.text_Sorry_Failed_to_add_additional_information_Please_try_again'));
            $html="false";
          }
        } else{
            Session::flash('error',trans('client/contest/common.text_sorry_please_add_the_details'));
            $html="false";
          }
        echo $html;
      }

      public function update_description(Request $request)
      {
        $status = 'error';
        $project_id = base64_decode($request->input('project_id',null));
        $project_description = trim($request->input('project_add_info'));

        if($project_id == null){
          $status = 'error';
          Session::flash("error",trans('controller_translations.text_sorry_somthing_goes_wrong_please_try_again'));
        }

        $update = $this->ProjectpostModel->where('id',$project_id)->update(['project_description'=>$project_description]);

        if($update)
        {
          $status = 'success';
          Session::flash("success",trans('client/contest/common.text_description_updated_succesfully'));
        }
        else
        {
          $status = 'error';
          Session::flash('error',trans('client/contest/common.text_Sorry_Failed_to_update_description_Please_try_again'));
        }
        echo $status;
      }

      public function adding_shortlist_desc(Request $request)
      {
        $bid_id      = base64_decode($request->input('bid_id',null));
        $shortlist_desc    = $request->input('shortlist_desc',null);
        
        if($bid_id == null){
          Session::flash("error",trans('controller_translations.text_sorry_somthing_goes_wrong_please_try_again'));
        }
        
        $update_info = $this->ProjectsBidsModel->where('id',$bid_id)->update(['shortlist_desc'=>$shortlist_desc]);

        if($html != "false"){
          if(isset($update_info) && $update_info) {
            Session::flash("success",'Note added successfully.');
            $html = "true";
          }else{
            Session::flash('error',"Problem occured while adding note.");
            $html="false";
          }
        } else{
            Session::flash('error',trans('client/contest/common.text_sorry_please_add_the_details'));
            $html="false";
          }
        echo $html;
      }

      public function remove_shortlist_desc($enc_id)
      {
        $bid_id      = base64_decode($enc_id);
        if($bid_id == null){
          Session::flash("error",trans('controller_translations.text_sorry_somthing_goes_wrong_please_try_again'));
        }
        
        $update_info = $this->ProjectsBidsModel->where('id',$bid_id)->update(['shortlist_desc'=>'']);
        if($html != "false"){
          if(isset($update_info) && $update_info) {
            Session::flash("success",'Note added successfully.');
            $html = "true";
          }else{
            Session::flash('error',"Problem occured while adding note.");
            $html="false";
          }
        } else{
            Session::flash('error',trans('client/contest/common.text_sorry_please_add_the_details'));
            $html="false";
          }
        echo $html;
      }

      private function make_pagination_links($items,$perPage)
      {
          $pageStart = \Request::get('page', 1);
          // Start displaying items from this number;
          $offSet = ($pageStart * $perPage) - $perPage; 

          // Get only the items you need using array_slice
          $itemsForCurrentPage = array_slice($items, $offSet, $perPage, true);

          return new LengthAwarePaginator($itemsForCurrentPage, count($items), $perPage,Paginator::resolveCurrentPage(), array('path' => Paginator::resolveCurrentPath()));
      } 

      /*Post: Update posted project
      Author: Shankar Jamdhade*/
      public function editproject($post_id)
      { 
        $arr_info             = array();
        $arr_client_details   = array();
        $arr_categories       = array();
        $arr_subcategories    = array();
        $aar_fixed_rates      = array();
        $arr_project_currency = config('app.project_currency');
        $aar_hourly_rates     = array();
        $arr_skills           = array();

        $arr_job_document_formats = array();
        $arr_job_document_formats = config('app.project_post_document_formats');

        $obj_client = $this->ClientsModel->where('user_id',$this->user_id)->with(['user_details','country_details.states','state_details.cities','city_details'])->first();
        if($obj_client != FALSE)
        {
          $arr_client_details = $obj_client->toArray();
        }
        $obj_category = $this->CategoriesModel->where('is_active','1')->get();
        if($obj_category != FALSE)
        {
          $arr_category_details = $obj_category->toArray();
        }
        $arr_lang   =  $this->LanguageService->get_all_language();

        $obj_category = $this->CategoriesModel->get();
        if($obj_category != FALSE)
        {
            $arr_categories = $obj_category->toArray();
        }

        $obj_skills = $this->SkillsModel->get();
        if($obj_skills != FALSE)
        {
            $arr_skills = $obj_skills->toArray();
        }
        $id=base64_decode($post_id);
        if($id=="")
        {
          return redirect()->back();
        }
        $obj_post_pro = $this->ProjectpostModel->where('id',$id)->with('project_skills','category_details','sub_category_details','project_post_documents')->first();
        if($obj_post_pro!=FALSE)
        {
          $arr_info = $obj_post_pro->toArray();
        }


         //dd($arr_info);
        // $html_built = $this->built_edit_job_highlighted_html($arr_info['project_currency_code']);
         //dd($html_built);
        /*dd($arr_info);*/
        $arr_subcategories = array();
        $arr_lang          = $this->LanguageService->get_all_language();
        $obj_subcategories = $this->SubCategoriesModel->get();
        if($obj_subcategories!=FALSE) {
          $arr_subcategories =  $obj_subcategories->toArray();
        }  
        $id                 = base64_encode($id);
        $obj_static_privacy =  $this->StaticPageModel->where('page_slug','privacy-policy')->where('is_active',"1")->first();
        $arr_static_privacy = array();
        if($obj_static_privacy) {
          $arr_static_privacy = $obj_static_privacy->toArray();
        }
        $obj_static_terms =  $this->StaticPageModel->where('page_slug','terms-of-service')->where('is_active',"1")->first();
        $arr_static_terms = array();
        if($obj_static_terms) {
          $arr_static_terms = $obj_static_terms->toArray();
        }
        /* fixed rates */
        /*$aar_fixed_rates  = config('app.project_fixed_rates');
        $aar_hourly_rates = config('app.project_hourly_rates');*/
        $arr_project_currency = config('app.project_currency');
        if (App::isLocale('de')){
          $aar_fixed_rates  = config('app.project_fixed_rates.de');
          $aar_hourly_rates = config('app.project_hourly_rates.de');
        } else {
          $aar_fixed_rates  = config('app.project_fixed_rates.en');
          $aar_hourly_rates = config('app.project_hourly_rates.en');
        }

        $packages     = [];
        $get_packages = $this->ProjectPriorityPackages
                             ->where('status','0')
                             ->get();
        if(sizeof($get_packages)>0)
        {
          $packages   = $get_packages->toArray();
        }  

        $arr_project_type_cost = [];
        $obj_project_type_cost = $this->SiteSettingModel->select('website_pm_initial_cost','wesite_project_manager_commission','website_pm_initial_cost','nda_price','urgent_price','private_price','recruiter_price')->first();
        if($obj_project_type_cost)
        {
          $arr_project_type_cost = $obj_project_type_cost->toArray();
        }

        $obj_currency = $this->CurrencyModel->select('id','currency_code','currency','description')
                                            ->where('is_active','1')
                                            ->get();
        if($obj_currency)
        {
          $arr_currency  = $obj_currency->toArray();
        }

        $project_currency      = isset($arr_info['project_currency']) ? $arr_info['project_currency'] : '$';
        $project_currency_code = isset($arr_info['project_currency_code']) ? $arr_info['project_currency_code'] : '$';

        $arr_project_type_cost['currency_code']   = $project_currency_code;
        $arr_project_type_cost['currency_sym']    = $project_currency;

        if(isset($arr_project_type_cost) && count($arr_project_type_cost)>0 && $project_currency_code!='USD') {
            $base_currency_code = 'USD';
            
            $nda_price       = isset($arr_project_type_cost['nda_price']) ? $arr_project_type_cost['nda_price'] : 0;
            $urgent_price    = isset($arr_project_type_cost['urgent_price']) ? $arr_project_type_cost['urgent_price'] : 0;
            $private_price   = isset($arr_project_type_cost['private_price']) ? $arr_project_type_cost['private_price'] : 0;
            $recruiter_price = isset($arr_project_type_cost['recruiter_price']) ? $arr_project_type_cost['recruiter_price'] : 0;
            
            $arr_project_type_cost['nda_price']       = round(currencyConverterAPI($base_currency_code, $project_currency_code, $nda_price)); 
            $arr_project_type_cost['urgent_price']    = round(currencyConverterAPI($base_currency_code, $project_currency_code, $urgent_price));
            $arr_project_type_cost['private_price']   = round(currencyConverterAPI($base_currency_code, $project_currency_code, $private_price));
            $arr_project_type_cost['recruiter_price'] = round(currencyConverterAPI($base_currency_code, $project_currency_code, $recruiter_price));
        }

        $this->arr_view_data['arr_project_duration'] = config('app.project_duration');
        $this->arr_view_data['arr_hours_per_week'] = config('app.hours_per_week');
        //$this->arr_view_data['html_built']               = $html_built;
        $this->arr_view_data['arr_currency']             = $arr_currency;
        $this->arr_view_data['arr_project_type_cost']    = $arr_project_type_cost;
        $this->arr_view_data['id']                       = $id;
        $this->arr_view_data['arr_info']                 = $arr_info;
        $this->arr_view_data['arr_job_document_formats'] = $arr_job_document_formats;
        $this->arr_view_data['arr_client_details']       = $arr_client_details;
        $this->arr_view_data['arr_categories']           = $arr_categories;
        $this->arr_view_data['arr_subcategories']        = $arr_subcategories;
        $this->arr_view_data['aar_fixed_rates']          = $aar_fixed_rates;
        $this->arr_view_data['aar_hourly_rates']         = $aar_hourly_rates;
        $this->arr_view_data['arr_project_currency']     = $arr_project_currency;
        $this->arr_view_data['arr_skills']               = $arr_skills;
        $this->arr_view_data['arr_static_privacy']       = $arr_static_privacy;
        $this->arr_view_data['arr_static_terms']         = $arr_static_terms;
        $this->arr_view_data['page_title']               = "Update a Job";
        $this->arr_view_data['module_url_path']          = $this->module_url_path;
        $this->arr_view_data['highlighted_packages']     = $packages;

        $this->arr_view_data['project_attachment_base_path']          = $this->project_attachment_base_path;
        $this->arr_view_data['project_attachment_public_path']        = $this->project_attachment_public_path;

        return view('client.projects.edit-post-projects',$this->arr_view_data);
      }

      public function updateproject($post_id,Request $request)
      {
        $id=base64_decode($post_id);
        if($id==""){
          return redirect()->back();
        }
        $arr_rules = array();
        $status    = FALSE;
        
        $arr_rules['project_name']        = "required";
        //$arr_rules['project_skills']      = "required";
        //$arr_rules['start_date']        = "required";
        //$arr_rules['end_date']          = "required";

        $arr_rules['project_duration']    = "required";
        $arr_rules['project_currency']    = "required";
        $arr_rules['project_description'] = "required";

        
        if(isset($request->bid_closing_date)){
          $arr_rules['bid_closing_date']    = "required";
        } else if(isset($request->hidden_bid_closing_date)){
          $arr_rules['hidden_bid_closing_date']    = "required";
        }

        if(isset($request->custom_rate) && $request->custom_rate != "" )
        {
          /* no action should be performed so kept empty if here */
        } 
        elseif (isset($request->price_method) && $request->price_method==1) 
        {
          $arr_rules['fixed_rate']    = "required";  
        }
        else  if (isset($request->price_method) && $request->price_method==2) 
        {
          $arr_rules['hourly_rate']    = "required";
        }

        $validator = Validator::make($request->all(),$arr_rules);
       
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $obj_project_post_details = $this->ProjectpostModel->where('id',$id)->first();
        if($obj_project_post_details == false) {
          return redirect()->back();
        }

        $arr_data = array();

        $category              = $request->category;
        $project_name          = $request->project_name;
        $project_skills[]      = $request->project_skills;
        $project_description   = $request->project_description;
        $start_date            = '0000-00-00 00:00:00';//$request->start_date;
        $end_date              = '0000-00-00 00:00:00';//$request->end_date;
        // $bid_closing_date      = $request->bid_closing_date;
        $project_duration      = $request->project_duration;
        $fixed_cost            = $request->fixed_rate;
        $price_method          = $request->price_method;
        $hourly_cost           = $request->hourly_rate;
        $project_currency      = $request->project_currency;
        $project_currency_code = $request->project_currency_code;
        $is_highlight_edited   = 0;
        
        $tmp_bid_closing_date = '';
        if(isset($request->bid_closing_date)){
          $tmp_bid_closing_date = $request->bid_closing_date;
        } else if(isset($request->hidden_bid_closing_date)){
          $tmp_bid_closing_date = $request->hidden_bid_closing_date;
        }

        $bid_closing_date      = '0000-00-00 00:00:00';
        if(!empty($obj_project_post_details->created_at) && $obj_project_post_details->created_at != '')
        {
            //$tmp_date       = $request->bid_closing_date.' '.date('H:i:s');
            $today_date       = $obj_project_post_details->created_at;
            $today_date       = strtotime($today_date);
            $today_date       = strtotime("+".$tmp_bid_closing_date." day", $today_date);
            $final_date       = date('Y-m-d H:i:s', $today_date);
            $bid_closing_date = $final_date;
        }

        $project_highlight                = $request->input('project_highlight_days','0');
        $handle_by                        = $request->input('is_recruiter_selected')!== null && $request->input('is_recruiter_selected')=='on' ? '2' : '1';
        $project_type_private             = $request->input('is_private_selected')!== null && $request->input('is_private_selected')=='on' ? '1' : '0';
        $project_type_nda                 = $request->input('is_nda_selected')!== null && $request->input('is_nda_selected')=='on' ? '1' : '0';
        $project_type_urgent              = $request->input('is_urgent_selected')!==null && $request->input('is_urgent_selected')=='on'?'1':'0';
        $old_project_type_private         = $request->input('is_private_selected')!== null && $request->input('is_private_selected')=='on' ? '1' : '0';
        $old_project_type_nda             = $request->input('is_nda_selected')!== null && $request->input('is_nda_selected')=='on' ? '1' : '0';
        $old_project_type_urgent          = $request->input('is_urgent_selected')!==null && $request->input('is_urgent_selected')=='on'?'1':'0';
        $old_project_highlighted_selected = $request->input('is_highlight_selected')!==null && $request->input('is_highlight_selected')=='on'?'1':'0';
        $old_handle_by                    = $request->input('is_recruiter_selected')!== null && $request->input('is_recruiter_selected')=='on' ? '2' : '1';

        $project_highlight_start_date   = $request->input('highlight_start_date','0000-00-00 00:00:00');
        $project_highlight_end_date     = $request->input('highlight_end_date','0000-00-00 00:00:00');
        $urjent_heighlight_end_date     = '';

        $is_project_highlight_selected  = 'no';

        $arr_project_old_details = [];

        if(!empty($request->input('project_type_private')) && !empty($request->input('is_private_selected')) && $request->input('project_type_private') == 'on' && $request->input('project_type_private')!=$request->input('is_private_selected'))
        {
          $project_type_private = '1'; 
        }

        if(!empty($request->input('project_type_nda')) && !empty($request->input('is_nda_selected')) && $request->input('project_type_nda') == 'on' && $request->input('project_type_nda')!=$request->input('is_nda_selected'))
        {
          $project_type_nda     = '1';  
        }

        $urjent_heighlight_end_date    = isset($obj_project_post_details->urjent_heighlight_end_date) ? $obj_project_post_details->urjent_heighlight_end_date : '';
        if(!empty($request->input('project_type_urgent')) && !empty($request->input('is_urgent_selected')) && $request->input('project_type_urgent') == 'on' && $request->input('project_type_urgent')!=$request->input('is_urgent_selected'))
        {
          $project_type_urgent     = '1';  
          $urjent_heighlight_end_date    = date('Y-m-d H:i:s', strtotime("+".'1'." days"));
        }

        if(!empty($request->input('project_highlight')) && $request->input('project_highlight') !="" )
        {
          $expld = explode('-', $request->input('project_highlight'));
          if(isset($expld[0]))
          { 
            $project_highlight             = $expld[0]; 
            $project_highlight_start_date  = date('Y-m-d H:i:s');
            $project_highlight_end_date    = date('Y-m-d H:i:s', strtotime("+".$expld[0]." days"));
            $is_highlight_edited           = 1;
            $is_project_highlight_selected = 'yes';
          }
        }

        if(!empty($request->input('project_handle_by_recruiter')) && !empty($request->input('is_recruiter_selected')) && $request->input('project_handle_by_recruiter') == 'on' && $request->input('project_handle_by_recruiter')!=$request->input('is_recruiter_selected'))
        { 
          $handle_by                       = '2';   // Recruiter
        }

        $obj_user = $this->UserModel->where('id',$this->user_id)->first();
        if($obj_user->mp_wallet_created!='Yes')
        {
          $arr_client = $obj_user->toArray();
          $arr_client['selected_currency_code'] = $project_currency_code;
          $this->create_user_wallet($arr_client);
        }

        // check wallets existance
        if (isset($handle_by) && $handle_by == '2' && $old_handle_by!='2' ||
            isset($project_type_private) && $project_type_private == '1' && $old_project_type_private!='1' ||
            isset($project_type_nda) && $project_type_nda == '1' && $old_project_type_nda!='1' ||
            isset($project_highlight) && $project_highlight > '0' && $is_highlight_edited == '1')
        {
            $admin_data                  = get_admin_email_address();
            if(isset($admin_data) && isset($admin_data['mp_wallet_created']) && $admin_data['mp_wallet_created'] =='No')
            {
              // send notification to admin
              $arr_admin_data                         =  [];
              $arr_admin_data['user_id']              =  $admin_data['id'];
              $arr_admin_data['user_type']            = '1';
              $arr_admin_data['url']                  = 'admin/wallet/archexpert';
              $arr_admin_data['project_id']           = '';
              $arr_admin_data['notification_text_en'] = Lang::get('controller_translations.create_wallet',[],'en','en');
              $arr_admin_data['notification_text_de'] = Lang::get('controller_translations.create_wallet',[],'de','en');
              $this->NotificationsModel->create($arr_admin_data);      
              // end send notification to admin   
              Session::flash("error",trans('controller_translations.you_dont_have_permission_to_get_project_features_because_admin_dont_have_created_his_wallet_for_transactions'));
              return redirect()->back()->withErrors($validator)->withInput($request->all());
            }

            /*$wallet_url = url('/').'/client/wallet/dashboard';

            if(isset($this->mp_wallet_created) && $this->mp_wallet_created == "No")
            {
              Session::flash("error",trans('controller_translations.text_please_create_an').' <a href="'.$wallet_url.'">'.trans('controller_translations.text_e_wallet').'</a> '.trans('controller_translations.text_first_and_add_your_payment_method_to_be_able_to_buy_website_features_and_make_transactions'));
              return redirect()->back()->withErrors($validator)->withInput($request->all());
            }*/
        }
        // end check wallet existance

        //dd($request->all());
        //Store project cost in min and max  
        $min_fixed_cost          = 0;
        $max_fixed_cost          = 0;
        $is_custom_price         = 0;
        $project_cost            = 0;
        $hour_per_week           = '';
        $is_custom_hour_per_week = 0;

        if($price_method == '1')
        {
          if(isset($fixed_cost) && $fixed_cost!=null)
          {
            if($fixed_cost == 'CUSTOM_RATE')
            {
              $project_cost = $request->custom_rate;
              $is_custom_price = "1";  
              $max_fixed_cost       = $project_cost;  
            }
            else
            {
              $project_cost         = $fixed_cost;
              $explode_fixed_cost   = explode('-',$fixed_cost);  
              $min_fixed_cost       = isset($explode_fixed_cost[0])?$explode_fixed_cost[0]:'';
              $max_fixed_cost       = isset($explode_fixed_cost[1])?$explode_fixed_cost[1]:'';
              if($min_fixed_cost!="" && $max_fixed_cost=="")
              {
                $max_fixed_cost     = $min_fixed_cost;
              }
            }
          }
        } 
        else if($price_method == '2')
        {
          if(isset($hourly_cost) && $hourly_cost!=null)
          {
            if($hourly_cost == 'CUSTOM_RATE')
            {
              $project_cost    = $request->custom_rate;
              $is_custom_price = "1";
              $max_fixed_cost  = $project_cost;
            }
            else
            {
              $project_cost         = $hourly_cost;
              $explode_hourly_cost  = explode('-',$hourly_cost);
              $min_fixed_cost       = isset($explode_hourly_cost[0])?$explode_hourly_cost[0]:'';
              $max_fixed_cost       = isset($explode_hourly_cost[1])?$explode_hourly_cost[1]:'';

              if($min_fixed_cost!="" && $max_fixed_cost=="")
              {
                $max_fixed_cost = $min_fixed_cost;
              }
            }
          }
          $is_custom_hour_per_week = "0";
          $hour_per_week = $request->hour_per_week;
          if($hour_per_week == 'HOUR_CUSTOM_RATE')
          {
            $is_custom_hour_per_week = "1";
          }
        }

        // if(isset($fixed_cost) && $fixed_cost!=null)
        // {
        //   if($fixed_cost == 'CUSTOM_RATE')
        //   {
        //     if(isset($request->custom_rate) && $request->custom_rate != "")
        //     {
        //       $fixed_cost = $request->custom_rate;
        //       //$project_cost = $request->custom_rate;
        //       $is_custom_price = "1";  
        //     }
        //   }
        //   else
        //   {
        //     $explode_fixed_cost   = explode('-',$fixed_cost);  
        //     $min_fixed_cost       = isset($explode_fixed_cost[0])?$explode_fixed_cost[0]:'';
        //     $max_fixed_cost       = isset($explode_fixed_cost[1])?$explode_fixed_cost[1]:'';
        //     if($min_fixed_cost!="" && $max_fixed_cost=="")
        //     {
        //       $max_fixed_cost     = $min_fixed_cost;
        //     }
        //   }
        // }
        // elseif(isset($hourly_cost) && $hourly_cost!=null)
        // {
        //   $explode_hourly_cost  = explode('-',$hourly_cost);
        //   $min_fixed_cost       = isset($explode_hourly_cost[0])?$explode_hourly_cost[0]:'';
        //   $max_fixed_cost       = isset($explode_hourly_cost[1])?$explode_hourly_cost[1]:'';

        //   if($min_fixed_cost!="" && $max_fixed_cost=="")
        //   {
        //     $max_fixed_cost = $min_fixed_cost;
        //   }
        // }

        if($handle_by == '1')
        {
          $project_status  = '2';  /* Making project status Open bcz its handled by client himself */
        }
        else
        {
          $project_status = '1';
        }

        // if($price_method == "1")
        // {
        //     $project_cost = $fixed_cost; 
        // }  

        // if($price_method == "2")
        // {
        //     $project_cost = $fixed_cost;
        // }

        // $is_custom_price = "0";     
        // if(isset($request->custom_rate) && $request->custom_rate != "")
        // {
        //   $project_cost = $request->custom_rate;
        //   $is_custom_price = "1";  
        // }

        if(isset($request->hire_expert_user_id) && $request->hire_expert_user_id !="")
        {
             $project_status = '2';     
             $hire_expert_user_id = $request->hire_expert_user_id;     
             $is_hire_process     = 'YES';

             $award_request_date_time = date('Y-m-d H:i:s');

        }
        else
        {
            if(isset($handle_by) && $handle_by == '2' && $old_handle_by!='2' ||
            isset($project_type_private) && $project_type_private == '1' && $old_project_type_private!='1' ||
            isset($project_type_nda) && $project_type_nda == '1' && $old_project_type_nda!='1' ||
            isset($project_type_urgent) && $project_type_urgent == '1' && $old_project_type_urgent!='1'        ||
            isset($project_highlight) && $project_highlight > '0'&& $is_highlight_edited == '1')
            {
                $project_status = '0';   
            }
            else
            {
                $project_status = '2';
            }
        }

        //Currenct code set 
        // if(isset($project_currency) && $project_currency == "$") {
        //     $project_currency_code = trans('controller_translations.currency_usd');
        // } else {
        //   $project_currency_code = trans('controller_translations.currency_eur');
        // }


        /*file uploade code for laravel auther : Shankar*/
        $form_data    = array();
        $form_data    = $request->all();
        $poject_image = "";

        if($request->hasFile('project_attachment')) 
        {   
          $fileExtension  = strtolower($request->file('project_attachment')->getClientOriginalExtension()); 

          $arr_file_types = ['png','docx','xls','xlsx','gif','png','jpeg','jpg','cad','pdf','odt','doc','txt','zip'];

          if(in_array($fileExtension, $arr_file_types))
          {
            $project_attachment = $form_data['project_attachment'];
            $imageExtension = $request->file('project_attachment')->getClientOriginalExtension();
            $imageName = sha1(uniqid().$project_attachment.uniqid()).'.'.$imageExtension;
            $request->file('project_attachment')->move(
                base_path() . '/public/uploads/front/postprojects/', $imageName
            );
            
            $poject_image = $imageName; 
          }
          else
          {
              Session::flash('error',trans('controller_translations.error_please_upload_valid_attachment') );
              return redirect('/client/projects/edit/'.$post_id)->withErrors($validator)->withInput($form_data);
          }
        }
        /*file uploade code end here*/ 

        $usd_min_fixed_cost = $usd_max_fixed_cost = 0;
        if($project_currency_code != 'USD')
        {
          $to_currency = 'USD';
          $usd_min_fixed_cost  = currencyConverterAPI($project_currency_code,$to_currency,$min_fixed_cost);
          $usd_min_fixed_cost  = floor($usd_min_fixed_cost);

          $usd_max_fixed_cost  = currencyConverterAPI($project_currency_code,$to_currency,$max_fixed_cost);
          $usd_max_fixed_cost  = floor($usd_max_fixed_cost);
        }
        else
        {
          $usd_min_fixed_cost = $min_fixed_cost;
          $usd_max_fixed_cost = $max_fixed_cost;
        }


        $arr_data['project_attachment'] = $poject_image;  
        /*Duplication Check*/
        $obj_existing_pro = $this->ProjectpostModel->where('id',$id)->first();

        $project = $this->ProjectpostModel->where('id',$id)
                                          ->update(['project_name'=>$project_name,
                                                    'project_attachment'=>$arr_data['project_attachment'],
                                                    'project_description'=>$project_description,
                                                    'project_start_date'=>$start_date,
                                                    'project_end_date'=>$end_date,
                                                    'bid_closing_date'=>$bid_closing_date,
                                                    'project_expected_duration'=>$project_duration,
                                                    'project_cost'=>$project_cost,
                                                    'min_project_cost'=>$min_fixed_cost,
                                                    'max_project_cost'=>$max_fixed_cost,
                                                    'project_pricing_method'=>$price_method,
                                                    'is_custom_price'=>$is_custom_price,
                                                    'project_type'=>$project_type_private,
                                                    'is_nda'=>$project_type_nda,
                                                    'is_urgent'=>$project_type_urgent,
                                                    'urjent_heighlight_end_date'=>$urjent_heighlight_end_date,
                                                    'project_handle_by'=>$handle_by,
                                                    'highlight_days'=>$project_highlight,
                                                    'highlight_start_date'=>$project_highlight_start_date,
                                                    'highlight_end_date'=>$project_highlight_end_date,
                                                    'is_highlight_edited'=>$is_highlight_edited,
                                                    'usd_min_project_cost'=>$usd_min_fixed_cost,
                                                    'usd_max_project_cost'=>$usd_max_fixed_cost,
                                                    'edited_at'=>date('Y-m-d H:i:s'),
                                                    'updated_at'=> isset($obj_existing_pro->updated_at) ? $obj_existing_pro->updated_at : NULL,
                                                    'hour_per_week'=> $hour_per_week,
                                                    'is_custom_hour_per_week'=> $is_custom_hour_per_week
                                                    /*'project_hourly_rate'=>$hourly_rate,*/
                                                    /*'project_status'=>$project_status,*/
                                                    /*'project_skills'=>$project_skills*/
                                                  ]);
        
        if($project)
        {
          if($request->hasFile('contest_attachments'))
          {                 
            $documents  = $request->file('contest_attachments');

            foreach($documents as $key =>$doc)
            {
              $count = $this->ProjectPostDocumentsModel->where('project_id',$id)->count();
      
              if($count < 10)
              { 
                if(isset($doc))
                {            
                  $filename         = $doc->getClientOriginalName();
                  $file_extension   = strtolower($doc->getClientOriginalExtension());
                  $file_size        = $doc->getClientSize();

                  if(in_array($file_extension,config('app.project_post_document_formats')) && $file_size <= 5000000)
                  {
                      $file_name       = sha1(uniqid().$doc.uniqid()).'.'.$file_extension;
                      $doc->move(base_path().'/public/uploads/front/postprojects/',$file_name);
                      $obj = $this->ProjectPostDocumentsModel->create(['project_id'=>$id,
                                                                  'image_name'=>$file_name,
                                                                  'image_original_name'=>$filename]); 
                  }
                }
              }
            }
          }
          
          if(isset($project_skills) && count($project_skills) > 0)
          {
            
           /*$arr_tmp_skills = $arr_skills = [];
            
            $obj_skills = $this->ProjectSkillsModel->where('project_id',$id)->get();
            
            if($obj_skills) 
            {
              $arr_tmp_skills = $obj_skills->toArray();
            }

            if($arr_tmp_skills > 0 )
            {
              foreach ($arr_tmp_skills as $key => $value) 
              {
                array_push($arr_skills,$value['skill_id']);
              }
            }*/

            /* making array flat for easier comparing */
            $project_skills = array_flatten($project_skills);

            /* Checking if existing skills are updated  or not , if updated then create new skillset */

            /*if(count(array_intersect($project_skills,$arr_skills)) != 0) 
            {
              $this->add_new_skills($project_skills,$id);
            }*/
            // if(isset($project_skills[0]) && $project_skills[0]!=null)
            // {
              $this->add_new_skills($project_skills,$id);  
            //}

          }

          /*if project is handle by recruiter / highlight project / private project / NDA project then client have to pay fixed cost : updated on 08-08-2016*/
          if(isset($handle_by) && $handle_by == '2' && $old_handle_by!='2' ||
          isset($project_type_private) && $project_type_private == '1' && $old_project_type_private!='1' ||
          isset($project_type_nda) && $project_type_nda == '1' && $old_project_type_nda!='1' ||
          isset($project_type_urgent) && $project_type_urgent == '1' && $old_project_type_urgent!='1'        ||
          isset($project_highlight) && $project_highlight > '0' && $is_highlight_edited == '1')
          {
              return redirect($this->module_url_path.'/payment/'.base64_encode($id));  
          }

          Session::flash("success",trans('controller_translations.success_project_updated_successfully'));
        }
        else
        {
            Session::flash("error",trans('controller_translations.error_while_updating_project')); 
        }
        return redirect()->back();
      }

      public function delete_document(Request $request)
      {
        $id = $request->input('image_primary_id');

          if(isset($id) && $id!=null) 
          {
            $obj_img = $this->ProjectPostDocumentsModel->where('id',$id)->first();
            $documents = isset($obj_img->image_name)?$obj_img->image_name:'';
            $status  = $this->ProjectPostDocumentsModel->where('id',$id)->delete();
            //$base_path = base_path().'/uploads/front/postcontest';
            if ($status) 
            {
              if (file_exists($this->project_attachment_base_path.$documents))
              {
                @unlink($this->project_attachment_base_path.$documents);
              }
              return 'success';
            }
          }
          return 'error'; 
      }

      /*
        Auther  : Nayan S.
        Content : To Insert New Skills in the project skill table.
      */
      public function add_new_skills($project_skills,$project_id)
      { 
        $obj_skills = $this->ProjectSkillsModel->where('project_id',$project_id)->get();
              
        if($obj_skills) 
        {
          $delete_existing_skills = $this->ProjectSkillsModel->where('project_id',$project_id)->delete();
        }
          
        foreach ($project_skills as $key => $skill) 
        {   
          if($skill != '' || $skill != null)
          {
            $this->ProjectSkillsModel->create(['project_id'=>$project_id,'skill_id'=>$skill]);
          }
        }       
        return TRUE;
      }

      public function details($id)
      {
        $id=base64_decode($id);
        $arr_project_info = array();

        if($id=="")
        {
          return redirect()->back();
        }

        $this->arr_view_data['page_title'] = trans('controller_translations.page_title_projects');

        $obj_project_info = $this->ProjectpostModel->with('skill_details','client_info','project_skills.skill_data','client_details','category_details','sub_category_details')->where('project_status','1')->where('id',$id)->first();
        
        if($obj_project_info != FALSE)
        {
          $arr_project_info = $obj_project_info->toArray();
        }
        
        $this->arr_view_data['projectInfo']=$arr_project_info;
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('client.projects.show',$this->arr_view_data);

      }
      /*
        Comment: Load page of ongoing projects. 
        Author : Nayan S.
      */
      public function show_ongoing_projects(Request $request)
      {
        $sort_by = '';
        if($request->has('sort_by') && $request->input('sort_by')!='')
        {
          $sort_by = $request->input('sort_by');
        }

        $this->arr_view_data['page_title'] = trans('controller_translations.page_title_manage_ongoing_projects');

       /* $obj_ongoing_projects = $this->ProjectpostModel->where('client_user_id',$this->user_id)
                                                       ->where('project_status','=','4')
                                                       ->with(['skill_details','project_skills.skill_data','expert_details'])
                                                       ->with(['project_milestones' => function ($q) 
                                                          { 
                                                            $q->where('status','!=',0);
                                                            $q->select('id','project_id','milestone_due_date');
                                                            $q->orderBy('milestone_due_date','DESC');
                                                          }])
                                                        ->with(['milestone_release_details'=> function ($query_nxt) 
                                                          {   
                                                              $query_nxt->where('status',0);
                                                              $query_nxt->select('id','milestone_id','status','project_id');
                                                          }])
                                                       ->orderBy('created_at','DESC')
                                                       ->paginate(config('app.project.pagi_cnt')); 
                                          
        $arr_ongoing_projects = array();
        $arr_pagination       = array();

        if($obj_ongoing_projects)
        {
          $arr_pagination       = clone $obj_ongoing_projects;
          $arr_ongoing_projects = $obj_ongoing_projects->toArray();
        }*/

        $obj_ongoing_projects = $this->ProjectpostModel->where('client_user_id',$this->user_id)
                                                       ->where('project_status','=','4')
                                                       ->with(['project_bid_info','skill_details','project_skills.skill_data','expert_details','category_details','sub_category_details'])
                                                       ->with(['project_milestones' => function ($q) 
                                                          { 
                                                            $q->where('status','!=',0);
                                                            $q->select('id','project_id','milestone_due_date');
                                                            $q->orderBy('milestone_due_date','DESC');
                                                          }])
                                                        ->with(['milestone_release_details'=> function ($query_nxt) 
                                                          {   
                                                              $query_nxt->where('status',0);
                                                              $query_nxt->select('id','milestone_id','status','project_id');
                                                          }]);

          if($sort_by == "is_urgent")
          {
            $obj_ongoing_projects = $obj_ongoing_projects->where('is_urgent','1')->orderBy('created_at','desc');
          }
          else if($sort_by == "public_type" || $sort_by == "private_type")
          {
            $project_type = '0';
            if($sort_by == "public_type")
            {
              $project_type = '0';
            }
            elseif($sort_by == "private_type")
            {
              $project_type = '1';
            }
            $obj_ongoing_projects = $obj_ongoing_projects->where('project_type',$project_type)->orderBy('created_at','desc');
          }
          else if($sort_by == "is_nda")
          {
            $obj_ongoing_projects = $obj_ongoing_projects->where('is_nda','1')->orderBy('created_at','desc');
          }

          $obj_ongoing_projects      = $obj_ongoing_projects
                                            ->get();

          $obj_ongoing_projects = collect($obj_ongoing_projects);

          if( $sort_by == "asc")
          { 
            $obj_ongoing_projects = $obj_ongoing_projects
                                    ->sortBy(['created_at']);
          }
          else if($sort_by == "desc")
          { 
            $obj_ongoing_projects = $obj_ongoing_projects
                                    ->sortByDesc(['created_at']);
          }

          if($sort_by == "")
          {
            $obj_ongoing_projects = $obj_ongoing_projects
                                    ->sortByDesc(['created_at']);
          }

          $arr_tmp_ongoing_projects = [];
          if($obj_ongoing_projects){
            $arr_tmp_ongoing_projects = $obj_ongoing_projects->toArray();
            $arr_tmp_ongoing_projects = array_values($arr_tmp_ongoing_projects);
          }

          $arr_ongoing_projects = $arr_pagination = [];
          $obj_ongoing_project =  $this->make_pagination_links($arr_tmp_ongoing_projects,config('app.project.pagi_cnt'));
          if($obj_ongoing_project)
          {
              $arr_ongoing_projects = $obj_ongoing_project->toArray();
              $arr_pagination      = clone $obj_ongoing_project;
          }

        
       
        $this->arr_view_data['arr_ongoing_projects']  = $arr_ongoing_projects;
        $this->arr_view_data['arr_pagination']        = $arr_pagination;
        $this->arr_view_data['module_url_path']       = $this->module_url_path;
        $this->arr_view_data['sort_by']         = $sort_by;
        return view('client.projects.ongoing_projects',$this->arr_view_data);
      } 
      /*
        Comment: Load page of Awarded projects. 
        Author : Ankit Aher.
      */
      public function show_awarded_projects(Request $request)
      { 
          $sort_by = '';
          if($request->has('sort_by') && $request->input('sort_by')!='')
          {
            $sort_by = $request->input('sort_by');
          }

          $this->arr_view_data['page_title'] = trans('controller_translations.page_title_manage_awarded_projects');

            /*$obj_awarded_projects = $this->ProjectpostModel->where('client_user_id',$this->user_id)->Where('project_status','=','2')
                                                         ->whereHas('project_bid_info',function ($query)
                                                         {
                                                           $query->where('bid_status',4);
                                                         })
                                                         ->with(['skill_details','project_skills.skill_data'])
                                                         ->orderBy('created_at','DESC')
                                                         ->paginate(config('app.project.pagi_cnt'));                           
            $arr_awarded_projects = array();
            $arr_pagination = array();

            if($obj_awarded_projects)
            {
              $arr_pagination       = clone $obj_awarded_projects;
              $arr_awarded_projects = $obj_awarded_projects->toArray();
            }*/

            $obj_awarded_projects = $this->ProjectpostModel->where('client_user_id',$this->user_id)->Where('project_status','=','2')
                                                         ->whereHas('project_bid_info',function ($query)
                                                         {
                                                           $query->where('bid_status',4);
                                                         })
                                                         ->with(['skill_details','project_skills.skill_data','category_details','sub_category_details']);
                                                        


            if($sort_by == "is_urgent")
            {
              $obj_awarded_projects = $obj_awarded_projects->where('is_urgent','1')->orderBy('created_at','desc');
            }
            else if($sort_by == "public_type" || $sort_by == "private_type")
            {
              $project_type = '0';
              if($sort_by == "public_type")
              {
                $project_type = '0';
              }
              elseif($sort_by == "private_type")
              {
                $project_type = '1';
              }
              $obj_awarded_projects = $obj_awarded_projects->where('project_type',$project_type)->orderBy('created_at','desc');
            }
            else if($sort_by == "is_nda")
            {
              $obj_awarded_projects = $obj_awarded_projects->where('is_nda','1')->orderBy('created_at','desc');
            }

            $obj_awarded_projects      = $obj_awarded_projects
                                              ->get();

            $obj_awarded_projects = collect($obj_awarded_projects);

            if( $sort_by == "asc")
            { 
              $obj_awarded_projects = $obj_awarded_projects
                                      ->sortBy(['created_at']);
            }
            else if($sort_by == "desc")
            { 
              $obj_awarded_projects = $obj_awarded_projects
                                      ->sortByDesc(['created_at']);
            }

            if($sort_by == "")
            {
              $obj_awarded_projects = $obj_awarded_projects
                                      ->sortByDesc(['created_at']);
            }

            $arr_tmp_award_projects = [];
            if($obj_awarded_projects){
              $arr_tmp_award_projects = $obj_awarded_projects->toArray();
              $arr_tmp_award_projects = array_values($arr_tmp_award_projects);
            }

            $arr_award_projects = $arr_pagination = [];
            $obj_post_project =  $this->make_pagination_links($arr_tmp_award_projects,config('app.project.pagi_cnt'));
            if($obj_post_project)
            {
                $arr_award_projects = $obj_post_project->toArray();
                $arr_pagination      = clone $obj_post_project;
            }

           
            $this->arr_view_data['arr_awarded_projects']  = $arr_award_projects;
            //dd($this->arr_view_data['arr_awarded_projects']);
            $this->arr_view_data['arr_pagination']        = $arr_pagination;
            $this->arr_view_data['module_url_path']       = $this->module_url_path;
            $this->arr_view_data['sort_by']         = $sort_by;
            return view('client.projects.awarded_projects',$this->arr_view_data);
      } 
      /*
        Comment: Load page of Open projects. 
        Author : Nayan S.
      */
      public function show_open_projects()
      {
        $this->arr_view_data['page_title'] = trans('controller_translations.page_title_manage_open_projects');

        $obj_open_projects = $this->ProjectpostModel->where('client_user_id',$this->user_id)
                                                     ->where(function ($q) 
                                                       {
                                                          //$q->where('project_status','=','1');
                                                          $q->orWhere('project_status','=','2');
                                                       }) 
                                                     ->with(['skill_details','project_skills.skill_data'])
                                                     ->orderBy('created_at','DESC')
                                                     ->paginate(config('app.project.pagi_cnt')); 

        $arr_open_projects = array();
        $arr_pagination    = array();
        if($obj_open_projects){
          $arr_pagination         = clone $obj_open_projects;
          $arr_open_projects      = $obj_open_projects->toArray();
        }
        $this->arr_view_data['arr_open_projects']     = $arr_open_projects;
        $this->arr_view_data['arr_pagination']        = $arr_pagination;
        $this->arr_view_data['module_url_path']       = $this->module_url_path;
        return view('client.projects.open_projects',$this->arr_view_data);
      }
      /*
        Comment: Load page of Canceled projects. 
        Author : Ashwini K.
      */
      public function show_canceled_projects()
      {
        $arr_canceled_projects = array();
        $arr_pagination        = array();

        $this->arr_view_data['page_title'] = trans('controller_translations.page_title_manage_canceled_projects');

        $obj_canceled_projects = $this->ProjectpostModel->where('client_user_id',$this->user_id)
                                                        ->where('project_status','=','5')
                                                        ->with(['skill_details','project_skills.skill_data','category_details','sub_category_details'])
                                                        ->orderBy('created_at','DESC')
                                                        ->paginate(config('app.project.pagi_cnt')); 
                                          
        if($obj_canceled_projects)
        {
          $arr_pagination             = clone $obj_canceled_projects;
          $arr_canceled_projects      = $obj_canceled_projects->toArray();
        }

        $this->arr_view_data['arr_canceled_projects'] = $arr_canceled_projects;
        $this->arr_view_data['arr_pagination']        = $arr_pagination;
        $this->arr_view_data['module_url_path']       = $this->module_url_path;
        return view('client.projects.canceled_projects',$this->arr_view_data);
      }

      /*
        Comment: Load page of project details. 
        Author : Nayan S.
      */

      public function show_project_details_page(Request $request ,$enc_id )
      {
        $result = [];
        $obj_bids_data = [];
        $arr_bids_data = [];
        $request_time  = '';
        /*unset PROJECT_ID FROm Session */
        if(Session::has('PROJECT_ID'))
        {   
          $request->session()->forget('PROJECT_ID');
        }

        $sort_by = $request->input('sort_by',null);

        $arr_bids_data = $arr_review_details = $arr_pagination = $arr_project_attachment = [];
        $is_review_exists=0;

        $this->arr_view_data['page_title'] = trans('controller_translations.page_title_project_details');

        $project_id = base64_decode($enc_id);

        if($project_id=="")
        { 
          return redirect()->back();
        }

        $obj_handle_by = $this->ProjectpostModel->where('id',$project_id)->first(['id','project_handle_by']);


        if( isset($obj_handle_by->project_handle_by) && $obj_handle_by->project_handle_by == '1')
        {
          $result = $this->ProjectsBidsModel->where('project_id',$project_id)
                                            ->with(['expert_details.expert_categories.categories','expert_details.expert_skills.skills','review_details']);
        } 
        else if(isset($obj_handle_by->project_handle_by) && $obj_handle_by->project_handle_by == '2')
        {
          $result = $this->ProjectsBidsModel->where('project_id',$project_id)
                                            ->where('is_suggested','1')
                                            ->with('expert_details.expert_categories.categories','expert_details.expert_skills.skills');
        }
       /* else if(isset($obj_handle_by->project_handle_by) && $obj_handle_by->project_handle_by == '3')
        {
          $result = $this->ProjectsBidsModel->where('project_id',$project_id)
                                            ->where('is_suggested','1')
                                            ->with('expert_details.expert_categories.categories');
        }*/

        if(isset($sort_by) && $sort_by!='')
        {
          if($sort_by=='price_ascending')
          {
            $result = $result->orderBy('bid_cost','asc');
          }
          elseif($sort_by=='price_descending')
          {
            $result = $result->orderBy('bid_cost','desc');            
          }
          elseif($sort_by=='date_ascending')
          {
            $result = $result->orderBy('created_at','asc');            
          }
          elseif($sort_by=='date_descending')
          {
            $result = $result->orderBy('created_at','desc');            
          }
          elseif($sort_by=='shortlist_ascending')
          {
            $result = $result->orderBy('is_shortlist','asc');            
          }
          elseif($sort_by=='shortlist_descending')
          {
            $result = $result->orderBy('is_shortlist','desc');            
          }
        }

        if($result)
        {
          $obj_bids_data = $result->paginate(config('app.project.pagi_cnt'));
          $obj_bids_data->setPath('?sort_by='.$sort_by.'');
        } 

        $arr_new_bids_data1 = $average_rating = $arr_new_bids_data = [];
        $award_request = 'not expired';

        if($obj_bids_data)
        {
          $arr_pagination =  clone $obj_bids_data;
          $arr_bids_data  =  $obj_bids_data->toArray(); 

          $time = $this->ProjectRequestsModel->where('project_id',$project_id)
                                             ->where('is_accepted','0')
                                             ->select('created_at')
                                             ->first();
          if($time)
          {
            $time = $time->toArray();
            $request_time = $time['created_at'];

            $expired_time = strtotime(date('Y-m-d H:i:s',strtotime('+2day', strtotime($request_time))));
            $current_time = strtotime(date('Y-m-d H:i:s'));
            $diffInSeconds = $expired_time - $current_time;

            if($diffInSeconds<=0)
            {
              $award_request = 'expired';
            }
            else
            {
              $days    = ($diffInSeconds / 86400) % 365;
              $hours   = ($diffInSeconds / 3600 ) % 24;
              $minutes = ($diffInSeconds / 60 ) % 60;
              $seconds = $diffInSeconds % 60;             

              if($days>2 && ($hours>1 || $minutes>1 || $seconds>1))
              {
                $award_request = 'expired';
              }
              else
              {
                $award_request = 'not expired';
              }
            }
          }

          if(isset($sort_by) && $sort_by=='rating')
          {
            if(isset($arr_bids_data['data']) && count($arr_bids_data['data']))
            {
              $i=0;
              foreach($arr_bids_data['data'] as $bid_data)
              {
                if(isset($bid_data['review_details']) && count($bid_data['review_details']))
                {
                  $sum = 0;
                  $count = 0;
                  foreach($bid_data['review_details'] as $review_data)
                  {
                    $sum = $sum + $review_data['rating'];                  
                  }
                  $average_rating[$i]['average'] = $sum/count($bid_data['review_details']);      
                  $average_rating[$i]['expert_id'] = $review_data['to_user_id'];
                  $i++;
                }
                else
                {
                  $average_rating[$i]['average'] = 0;      
                  $average_rating[$i]['expert_id'] = $bid_data['expert_user_id'];
                  $i++;
                }
              }
            }

            if(isset($average_rating) && count($average_rating)>0)
            {
              rsort($average_rating);
              $j=0;

              foreach($average_rating as $key1=>$value1)
              {
                foreach($arr_bids_data['data'] as $key=>$value)
                {
                  if($value1['expert_id']==$value['expert_user_id'])
                  {
                    $arr_new_bids_data[$j] = $value;
                    $j++;
                  }
                }        
              }          
              $arr_bids_data['data'] = $arr_new_bids_data;    
            } 
          }

          if(isset($sort_by) && $sort_by=='reputation')
          {
            if(isset($arr_bids_data['data']) && count($arr_bids_data['data'])>0)
            {
              $i=0;
              foreach($arr_bids_data['data'] as $key=>$value)
              {
                $user_id = $value['expert_user_id'];
                $project_count            = $this->ProjectpostModel->where('expert_user_id',$user_id)
                                                                   ->where('project_status','!=','0')
                                                                   ->count();

                $completed_project_count  = $this->ProjectpostModel->where('expert_user_id',$user_id)
                                                                   ->where('project_status','=','3')
                                                                   ->count();

                if($completed_project_count>0 && $project_count>0)
                {
                  $completion_rate[$i]['rate'] = ($completed_project_count/$project_count)*100;
                  $completion_rate[$i]['expert_id'] = $user_id;
                  $i++;
                }
                else
                {
                  $completion_rate[$i]['rate'] = 0;
                  $completion_rate[$i]['expert_id'] = $user_id;
                  $i++;
                }
              }

              if(isset($completion_rate) && count($completion_rate)>0)
              {
                rsort($completion_rate);
                $j=0;

                foreach($completion_rate as $key1=>$value1)
                {
                  foreach($arr_bids_data['data'] as $key=>$value)
                  {
                    if($value1['expert_id']==$value['expert_user_id'])
                    {
                      $arr_new_bids_data1[$j] = $value;
                      $j++;
                    }
                  }        
                }  
              }

              $arr_bids_data['data'] = $arr_new_bids_data1;
            }
          }
        }

        $obj_project_attachment = $this->ProjectAttchmentModel->where('project_id',$project_id)->get();
        if($obj_project_attachment)
        {
          $arr_project_attachment = $obj_project_attachment->toArray();         
        }

        $obj_review_details  = $this->ReviewsModel->where('project_id',$project_id)->orderBy('created_at', 'DESC')->get();
        if($obj_review_details != FALSE)
        {
          $arr_review_details = $obj_review_details->toArray(); 
        }
        
        $is_review_exists                             =  $this->ReviewsModel->where('project_id',$project_id)->where('from_user_id',$this->user_id)->count();
        $this->arr_view_data['projectInfo']                           = $this->get_project_details($project_id);
        $this->arr_view_data['project_attachment_base_path']          = $this->project_attachment_base_path;
        $this->arr_view_data['project_attachment_public_path']        = $this->project_attachment_public_path;
        $this->arr_view_data['arr_bids_data']                         = $arr_bids_data;
        $this->arr_view_data['arr_review_details']                    = $arr_review_details;
        $this->arr_view_data['is_review_exists']                      = $is_review_exists;
        $this->arr_view_data['arr_pagination']                        = $arr_pagination;
        $this->arr_view_data['arr_project_attachment']                = $arr_project_attachment;
        $this->arr_view_data['module_url_path']                       = $this->module_url_path;
        $this->arr_view_data['expert_project_attachment_public_path'] = $this->expert_project_attachment_public_path;
        $this->arr_view_data['sort_by']                               = $sort_by;
        $this->arr_view_data['request_time']                          = $request_time;
        $this->arr_view_data['award_request']                         = $award_request;

        // dd($arr_bids_data['data']); 


          /*----------Collaboration Section----------------------*/
          $project_main_attachment        = [];
          $project_attachment             = [];
          $arr_collaboration_pagination                 = [];
          $obj_project_main_attachment    = $this->ProjectpostModel
                                                 ->where('id',$project_id)
                                                 //->where('project_status','4') // ongoing
                                                 //->orWhere('project_status','3') // completed
                                                 ->first(['id','project_name','project_status','project_attachment']);
          if(isset($obj_project_main_attachment) && $obj_project_main_attachment != null){
            $project_main_attachment = $obj_project_main_attachment->toArray(); 
          }
          // project attachments 
          $obj_collaboration_project_attachment    = $this->ProjectAttchmentModel
                                           ->where('project_id',$project_id)
                                           ->orderBy('created_at','DESC') 
                                           ->paginate(12);  
          if($obj_collaboration_project_attachment) {
            $arr_collaboration_pagination          = clone $obj_collaboration_project_attachment;
            $project_attachment      = $obj_collaboration_project_attachment->toArray();
          }

          // end project attachments 
          $this->arr_view_data['project_main_attachment'] = $project_main_attachment;
          $this->arr_view_data['project_id']              = $project_id;
          $this->arr_view_data['project_attachment']      = $project_attachment;
          $this->arr_view_data['arr_collaboration_pagination']          = $arr_collaboration_pagination;

          /*----------End Of Collaboration Section----------------------*/


          /*----------Milestone section---------------------------------*/
            $this->arr_view_data['mp_job_wallet_id'] = '';
            $project_details  = $this->ProjectpostModel->where('id',$project_id)->first(['id','project_name','project_currency','mp_job_wallet_id']);
            if ($project_details)
            {
                $this->arr_view_data['milestone_project_name']       = isset($project_details->project_name)?$project_details->project_name:'';
                $this->arr_view_data['milestone_project_currency']   = isset($project_details->project_currency)?$project_details->project_currency:'';
                $this->arr_view_data['mp_job_wallet_id']             = isset($project_details->mp_job_wallet_id)?$project_details->mp_job_wallet_id:'';
            }
            $obj_milestone = $this->MilestonesModel->where('project_id',$project_id)
                                                   ->with(['transaction_details','milestone_refund_details','project_details'=>function ($query) {
                                                        //$query->select('id','project_name','project_status'); 
                                                      }])
                                                   ->with(['milestone_release_details'=> function ($query_nxt) {
                                                        $query_nxt->select('id','milestone_id','status','project_id');    
                                                      }])
                                                   ->orderBy('milestone_due_date','ASC')
                                                   ->paginate(config('app.project.pagi_cnt'));
            $arr_milestone         = [];
            $arr_milestone_pagination        = [];
            if($obj_milestone)
            {
                $arr_milestone_pagination    = clone $obj_milestone; 
                $arr_milestone     = $obj_milestone->toArray();
            }
            // get wallet details
            $wallet_details =  $this->WalletService->get_wallet_details($this->arr_view_data['mp_job_wallet_id']);
            if(isset($wallet_details))
            {
              $wallet_details         = $wallet_details;
            }
            // end get wallet details
            $this->arr_view_data['enc_project_id']            = $enc_id;
            $this->arr_view_data['arr_milestone']             = $arr_milestone;
            $this->arr_view_data['arr_milestone_pagination']  = $arr_milestone_pagination;
            $this->arr_view_data['mangopay_wallet_details']   = $wallet_details;
          /*-------------------------------------------------------------*/
            $this->arr_view_data['count_milestone'] = 0;
            //dd($this->arr_view_data);
        return view('client.projects.project_details',$this->arr_view_data);
      }

      public function cancel_project_request($project_id=false,$expert_id=false)
      {
        if($project_id!=false && $expert_id!=false)
        {
          $project_id = base64_decode($project_id);
          $expert_id = base64_decode($expert_id);
          $update = $this->ProjectsBidsModel->where('project_id',$project_id)->update(['bid_status'=>'0']);
          if($update)
          {
            $delete = $this->ProjectRequestsModel->where('project_id',$project_id)->where('expert_user_id',$expert_id)->delete();
            Session::flash('success',trans('controller_translations.project_request_cancel'));
            return redirect()->back();
          }
          else
          {
            Session::flash('error', trans('controller_translations.something_went_wrong_please_try_again_later'));
            return redirect()->back();
          }
        }
        else
        {
          Session::flash('error', trans('controller_translations.something_went_wrong_please_try_again_later')); 
          return redirect()->back();
        }
      }

      public function make_zip($project_id=FALSE)
      {
        if($project_id == FALSE){
          Session::flash('error', trans('controller_translations.something_went_wrong_please_try_again_later')); 
          return redirect()->back();
        }

        $project_main_attachment        = [];
        $project_attachment             = [];
        $arr_project                    = [];
        $project_id                     = base64_decode($project_id); 

        $obj_project_main_attachment    = $this->ProjectPostDocumentsModel
                                                                      ->where('project_id',$project_id)
                                                                      ->get();

        if($obj_project_main_attachment)
        {
          $project_main_attachment = $obj_project_main_attachment->toArray(); 
        }
        
        $obj_project = $this->ProjectpostModel
                                            ->select('project_name')
                                            ->where('id',$project_id)
                                            ->first();
        if($obj_project)
        {          
          $arr_project = $obj_project->toArray();
        }

        $project_name = isset($arr_project['project_name']) ? $arr_project['project_name'] : '';
        
        $files = []; 
        if($project_main_attachment)
        {
            if($project_main_attachment)
            {
              foreach($project_main_attachment as $key => $attch)
              {
                if(file_exists(base_path().'/public/uploads/front/postprojects/'.$attch['image_name']))
                {
                  $files[]     = glob(public_path('uploads/front/postprojects/'.$attch['image_name']));
                }
              }
            }
    
            if(isset($files) && sizeof($files) > 0)
            {
              $zipfilename   = $project_name.date('YmdHis').".zip";
              
              Zipper::make('uploads/front/postprojects/zipfiles/'.$zipfilename)->add($files);
              return redirect($this->module_url_path.'/details/download-zip/'.$zipfilename);
            } 
            else
            {
              Session::flash('error', 'Sorry, no files found for create a zip file'); 
              return redirect()->back();
            }        
        }
        else 
        {
          Session::flash('error', 'Sorry, no files found for create a zip file'); 
          return redirect()->back();
        }
    }

    public function download_zip($zipname=FALSE)
    {
        if(file_exists('public/uploads/front/postprojects/zipfiles/'.$zipname))
        {
          return response()->download(public_path('/uploads/front/postprojects/zipfiles/'.$zipname));
        }
        else
        {
          return false;
        }
    }
      /*
      | Comments    : To get all project details.
      | auther      : Nayan S.
      */
      public function get_project_details($id)
      { 
        $obj_project_info = $this->ProjectpostModel
                                 ->with('skill_details','client_info','client_details','project_skills.skill_data','category_details','project_bids_infos','project_bid_info','sub_category_details','project_post_documents')
                                 ->with(['project_requests'=>function ($query) {
                                        $query->where('is_accepted','1');
                                    }])
                                 ->where('id',$id)
                                 ->first();

        $arr_project_info = array();

        if($obj_project_info != FALSE)
        {
          $arr_project_info = $obj_project_info->toArray();
        }
        
        return $arr_project_info;
      }

      /*
      | Comments    : This function make request to expert about project being awarded to him/her.
      | auther      : Nayan S.
      */
      public function make_project_request($enc_expert_id,$enc_project_id)
      {
        //dd(123);
        $expert_id  = base64_decode($enc_expert_id);
        $project_id = base64_decode($enc_project_id);
        $client_id  = $this->user_id;
        if($expert_id == "" || $project_id == "" || $client_id == "")
        {
          return redirect()->back();
        }

        $obj_is_project_cancled = $this->ProjectpostModel->where('project_status','5')
                                                      ->where('id',$project_id)
                                                      ->first();
        $is_project_cancled     = isset($obj_is_project_cancled)? $obj_is_project_cancled :'';
        if($is_project_cancled && $is_project_cancled!='')
        {
          Session::flash('error', trans('controller_translations.text_sorry_This_project_is_canceled'));  
          return redirect()->back();
        }
        $obj_bid_closing_date = $this->ProjectpostModel->select('project_name','bid_closing_date')
                                                           ->where('id',$project_id)
                                                           ->first();
        $bid_closing_date = isset($obj_bid_closing_date['bid_closing_date'])?strtotime($obj_bid_closing_date['bid_closing_date']):0;
        $currentDateTime  = strtotime(date('Y-m-d H:i:s')); //in seconds
        $date_differnce   = $bid_closing_date - $currentDateTime;

        if($date_differnce<=0){
          $date_differnce = 0;
        }

       /* if($date_differnce==0){
          Session::flash("error",trans('controller_translations.text_sorry_this_project_is_expired'));
          return redirect()->back();
        }*/

        $result = $this->ProjectRequestsModel->create([
                                                        'expert_user_id'=>$expert_id,
                                                        'project_id'=>$project_id,
                                                        'client_user_id'=>$client_id
                                                      ]);

        if($result)
        {
          $award_request_date_time = date('Y-m-d H:i:s');
          $this->ProjectpostModel->where('id',$project_id)->update(['award_request_date_time'=>$award_request_date_time]);
          /* updating bid status to waiting for acceptance of expert */
          $this->ProjectsBidsModel->where('project_id',$project_id)
                                  ->where('expert_user_id',$expert_id)
                                  ->update(['bid_status'=>'4']);
          
          $posted_project_name = isset($obj_bid_closing_date->project_name) ? $obj_bid_closing_date->project_name : '';
          /* Create Notification for expert */      
          $arr_data               =  [];
          $arr_data['user_id']    = $expert_id;
          $arr_data['user_type']  = '3';
          $arr_data['url']        = 'expert/projects/awarded';
          $arr_data['project_id'] = $project_id;
          /*$arr_data['notification_text'] =  trans('controller_translations.new_project_awarded');*/
          $arr_data['notification_text_en'] = $posted_project_name . ' - ' . Lang::get('controller_translations.new_project_awarded',[],'en','en');
          $arr_data['notification_text_de'] = $posted_project_name . ' - ' . Lang::get('controller_translations.new_project_awarded',[],'de','en');
          $this->NotificationsModel->create($arr_data);  
          /* Notificatin ends */
          $project_name = config('app.project.name');
          //$mail_form    = get_site_email_address();   /* getting email address of admin from helper functions */
          $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';

          $obj_project_info = $this->ProjectpostModel->where('id',$project_id)
                                                      ->with(['client_info'=> function ($query) {
                                                                        $query->select('user_id','first_name','last_name','company_name','address');
                                                                      },
                                                                      'client_details'=>function ($query_nxt) {
                                                                        $query_nxt->select('id','email','user_name');
                                                                      }])
                                                                ->with(['expert_info'=> function ($query_nxt_1) {
                                                                      $query_nxt_1->select('user_id','first_name','last_name');
                                                                      },
                                                                        'expert_details'=>function ($query_nxt) {
                                                                        $query_nxt->select('id','email','user_name');
                                                                      }])
                                                      ->first(['id','project_name','project_description','client_user_id','expert_user_id']);

          $data = [];

          if($obj_project_info)
          {
            $arr_project_info = $obj_project_info->toArray();

            $client_name = "";
            $expert_name = "";
            /* get client name */
            $client_first_name = isset($arr_project_info['client_info']['first_name'])?$arr_project_info['client_info']['first_name']:'';
            $client_last_name  = isset($arr_project_info['client_info']['last_name'])?$arr_project_info['client_info']['last_name']:'';

            $client_name = $client_first_name.' '.$client_last_name;

            /* get Expert name */
            $obj_user = $this->UserModel->where('id',$expert_id)->first(['id','first_name','last_name','email']);   
            if ($obj_user){
              $arr_user_details = $obj_user->toArray();
              $expert_first_name = isset($arr_user_details['role_info']['first_name'])?$arr_user_details['role_info']['first_name']:'';
              $expert_last_name  = isset($arr_user_details['role_info']['last_name'])?$arr_user_details['role_info']['last_name']:'';
              /*$expert_name = $expert_first_name.' '.$expert_last_name;*/
              $expert_name = $expert_first_name;
            }
            
            $client_username = isset($arr_project_info['client_details']['user_name'])?$arr_project_info['client_details']['user_name']:'';
            $expert_username = isset($arr_project_info['expert_details']['email'])?$arr_project_info['expert_details']['email']:'';
            $data['client_username']     = $client_username;
            $data['expert_username']     = $expert_username;
            $data['project_name']        = isset($arr_project_info['project_name'])?$arr_project_info['project_name']:'';
            $data['project_description'] = isset($arr_project_info['project_description'])?$arr_project_info['project_description']:'';
            $data['client_name']         = $client_name;
            $data['expert_name']         = $expert_name;
            $data['company_name']        = isset($arr_project_info['client_info']['company_name'])?$arr_project_info['client_info']['company_name']:'';
            $data['login_url']           = url('/redirection?redirect=PROJECT&id='.base64_encode($project_id));
            $data['email_id']            = isset($obj_user->email)? $obj_user->email:'';
          }
          $email_to = isset($obj_user->email)? $obj_user->email:'';
          if($email_to!= "")
          {
            try{
                  $mail_status = $this->MailService->send_new_project_awarded_to_expert_email($data);
                // Mail::send('front.email.new_project_awarded_to_expert', $data, function ($message) use ($email_to,$mail_form,$project_name) {
                //     $message->from($mail_form, $project_name);
                //     $message->subject($project_name.': New Project Awarded');
                //     $message->to($email_to);
                // });
            }
            catch(\Exception $e){
            Session::Flash('error'   , trans('controller_translations.text_mail_not_sent'));
            }
          }
          //Session::flash("success",trans('controller_translations.success_project_request_to_expert_sent_successfully'));
        }
        else
        {
          Session::flash("error",trans('controller_translations.error_while_sending_project_request_to_expert'));
        }

        return redirect()->back();
      }
      /*
        Comment: show all completed projects
        Author : sagar sainkar
      */
      public function completed_projects(Request $request)
      {
        $sort_by = '';
        if($request->has('sort_by') && $request->input('sort_by')!='')
        {
          $sort_by = $request->input('sort_by');
        }


        $arr_completed_projects = [];
        $arr_pagination         = [];

        $this->arr_view_data['page_title'] = trans('controller_translations.page_title_manage_completed_projects');

        /*$obj_open_projects                 = $this->ProjectpostModel->where('client_user_id',$this->user_id)
                                                    ->where('project_status','=','3')
                                                    ->with(['skill_details','project_skills.skill_data','project_reviews'])
                                                    ->orderBy('created_at','DESC')
                                                    ->paginate(config('app.project.pagi_cnt')); 
        if($obj_open_projects){
          $arr_pagination              = clone $obj_open_projects;
          $arr_completed_projects      = $obj_open_projects->toArray();
        }*/

        /*$is_review_exists   =  $this->ReviewsModel->where('project_id',$project_id)->where('from_user_id',$this->user_id)->count();*/

        $obj_open_projects                 = $this->ProjectpostModel->where('client_user_id',$this->user_id)
                                                    ->where('project_status','=','3')
                                                    ->with(['project_bid_info','skill_details','project_skills.skill_data','project_reviews','category_details','sub_category_details']);

        if($sort_by == "is_urgent")
        {
          $obj_open_projects = $obj_open_projects->where('is_urgent','1')->orderBy('created_at','desc');
        }
        else if($sort_by == "public_type" || $sort_by == "private_type")
        {
          $project_type = '0';
          if($sort_by == "public_type")
          {
            $project_type = '0';
          }
          elseif($sort_by == "private_type")
          {
            $project_type = '1';
          }
          $obj_open_projects = $obj_open_projects->where('project_type',$project_type)->orderBy('created_at','desc');
        }
        else if($sort_by == "is_nda")
        {
          $obj_open_projects = $obj_open_projects->where('is_nda','1')->orderBy('created_at','desc');
        }

        $obj_open_projects      = $obj_open_projects
                                          ->get();

        $obj_open_projects = collect($obj_open_projects);

        if( $sort_by == "asc")
        { 
          $obj_open_projects = $obj_open_projects
                                  ->sortBy(['created_at']);
        }
        else if($sort_by == "desc")
        { 
          $obj_open_projects = $obj_open_projects
                                  ->sortByDesc(['created_at']);
        }

        if($sort_by == "")
        {
          $obj_open_projects = $obj_open_projects
                                  ->sortByDesc(['created_at']);
        }

        $arr_tmp_open_projects = [];
        if($obj_open_projects){
          $arr_tmp_open_projects = $obj_open_projects->toArray();
          $arr_tmp_open_projects = array_values($arr_tmp_open_projects);
        }

        $arr_completed_projects = $arr_pagination = [];
        $obj_opens_project =  $this->make_pagination_links($arr_tmp_open_projects,config('app.project.pagi_cnt'));
        if($obj_opens_project)
        {
            $arr_completed_projects = $obj_opens_project->toArray();
            $arr_pagination      = clone $obj_opens_project;
        }
                                       
        $this->arr_view_data['arr_completed_projects']= $arr_completed_projects;
        $this->arr_view_data['arr_pagination']        = $arr_pagination;
        $this->arr_view_data['sort_by'] = $sort_by;
        $this->arr_view_data['module_url_path']       = $this->module_url_path;
        return view('client.projects.completed_projects',$this->arr_view_data);
      }
      /*
      This Function Gives all the details of expert.
      */  
      public function expert_details($enc_id, $user_id)
      {
        $arr_ongoing_projects = [];
        //$arr_pagination = [];
        $arr_project_bid_info =[];

        $user_id = base64_decode($user_id);
        $id      = base64_decode($enc_id);
        $check_details = \DB::table('users')->where('id',$user_id)->first();
        if($check_details){
          if(isset($check_details->email) && $check_details->email != ""){}else {
            Session::flash("error",trans('controller_translations.user_is_no_more'));  
            return redirect()->back();
          }
        }
        $this->arr_view_data['page_title'] = trans('controller_translations.page_title_expert_details');

        $obj_project_bid_info = $this->ProjectsBidsModel->where('project_id',$id)->where('expert_user_id',$user_id)->with(['project_details','user_details','expert_details.expert_work_experience','expert_details.expert_certification_details','expert_details.education_details'])->first();

        if($obj_project_bid_info != FALSE)
        {
          $arr_project_bid_info = $obj_project_bid_info->toArray();
        }

        $this->arr_view_data['arr_project_bid_info']        = $arr_project_bid_info;
        $this->arr_view_data['module_url_path']             = $this->module_url_path;
        $this->arr_view_data['bid_attachment_public_path']  = $this->bid_attachment_public_path;
        return view('client.projects.expert_details',$this->arr_view_data);
      }   
      /*
        @Comments : Change Project Status to Cancel
        @Auther   : Nayan S.
      */
      public function cancel_project($enc_id)
      {
        $project_id = base64_decode($enc_id); 
        /* Checking that we if we are Cancelling valid users Project or not */
        $obj_valid_project = $this->ProjectpostModel->where('client_user_id','=',$this->user_id)
                                                    ->where('id','=',$project_id)
                                                    ->where(function ($query) {
                                                        $query->where('project_status','=','1');
                                                        $query->orWhere('project_status','=','2');
                                                      })
                                                    ->with("project_manager_details",'category_details','sub_category_details')
                                                    ->first();  

        if($obj_valid_project)
        { 
            $obj_cancel = $this->ProjectpostModel->where('id','=',$project_id)->update(['project_status'=>'5']);
            if($obj_cancel)
            {
              
              /*----Awarded request send by client but no response from expert 48 hours then remove from award request list*/

              $this->ProjectRequestsModel->where('project_id',$project_id)->delete();

              /* If Project Has Project Manager then Notification to Project manager */
              /* Create Notification for expert */  
              if($obj_valid_project->project_handle_by == '2')
              {
                if($obj_valid_project->project_manager_user_id != '')
                { 
                  $posted_project_name = isset($obj_valid_project->project_name) ? $obj_valid_project->project_name : '';
                  $arr_data =  [];
                  $arr_data['user_id']    = $obj_valid_project->project_manager_user_id;
                  $arr_data['user_type']  = '4';
                  $arr_data['url']        = 'project_manager/projects/canceled';
                  $arr_data['project_id'] = $project_id;
                  
                  /*$arr_data['notification_text'] = trans('controller_translations.project_canceled_by_client');*/
                  $arr_data['notification_text_en'] = $posted_project_name . ' - ' . Lang::get('controller_translations.project_canceled_by_client',[],'en','en');
                  $arr_data['notification_text_de'] = $posted_project_name . ' - ' . Lang::get('controller_translations.project_canceled_by_client',[],'de','en');
                  $this->NotificationsModel->create($arr_data);  
                }
              }
              /* Notificatin ends */
              $client_name = '';
              $obj_client_details = $this->UserModel->with(['client_details'])
                                                    ->where('id',$this->user_id)
                                                    ->first();
              
              $client_first_name = isset($obj_client_details->client_details->first_name)?$obj_client_details->client_details->first_name:'';
              $client_last_name = isset($obj_client_details->client_details->last_name)?substr($obj_client_details->client_details->last_name,0,1):'';
              $client_name = $client_first_name.' '.$client_last_name;

              /* Notification to Admin */
              $arr_admin_data =  [];
              $arr_admin_data['user_id']    = '1';
              $arr_admin_data['user_type']  = '1';
              $arr_admin_data['url']        = 'admin/projects/canceled';
              $arr_admin_data['project_id'] = $project_id;
              /*$arr_admin_data['notification_text'] =  trans('controller_translations.project_canceled_by_client');*/
              $arr_admin_data['notification_text_en'] =  Lang::get('controller_translations.project_canceled_by_client',[],'en','en');
              $arr_admin_data['notification_text_de'] =  Lang::get('controller_translations.project_canceled_by_client',[],'de','en');
              
             $is_noti_send = $this->NotificationsModel->create($arr_admin_data); 
                //send mail notification to admin for project cancellation
                if(isset($is_noti_send) && $is_noti_send)
                {
                $project_name                     = isset($obj_valid_project->project_name)?$obj_valid_project->project_name:"NA";
                $data                             = [];
                $data['project_name']             = isset($project_name)?$project_name:"";
                $data['cancelled_by']             = $client_name;
                $data['client_username']          = isset($this->user_name)?$this->user_name:"NA";
                $data['user_first_name']          = isset($this->user_name)?$this->user_name:"NA";
                $data['project_description']      = isset($obj_valid_project->project_description)?$obj_valid_project->project_description:"NA";
                $data['project_handle_by']        = isset($obj_valid_project->project_handle_by)?$obj_valid_project->project_handle_by:"";
                $data['project_manager_username'] = isset($obj_valid_project->project_manager_details->user_name)?$obj_valid_project->project_manager_details->user_name:"NA";
                
                $admin_dtails = get_admin_email_address();
                $email_to     = isset($admin_dtails['email'])?$admin_dtails['email']:"";
                //$mail_form    = $this->email;
                $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
                  
                  try{
                        Mail::send('front.email.project_cancellation', $data, function ($message) use ($email_to,$mail_form,$project_name) {
                            $message->from($mail_form, $project_name);
                            $message->subject($project_name.':Project Cancellation.');
                            $message->to($email_to);
                        });
                    }
                    catch(\Exception $e){
                    Session::Flash('error'   , trans('controller_translations.text_mail_not_sent'));
                    }
                // end mail notification to admin for project cancellation

                // send mail notification to project manager for project cancellation
                if(isset($obj_valid_project->project_handle_by) && $obj_valid_project->project_handle_by == '2')
                {
                $data['user_first_name'] = isset($data['project_manager_username'])?$data['project_manager_username']:"";
                $data['cancelled_by']    = $client_name;
                $email_to                = isset($obj_valid_project->project_manager_details->email)?$obj_valid_project->project_manager_details->email:"";
                  
                        try{
                            Mail::send('front.email.project_cancellation', $data, function ($message) use ($email_to,$mail_form,$project_name) {
                                $message->from($mail_form, $project_name);
                                $message->subject($project_name.':Project Cancellation.');
                                $message->to($email_to);
                            });
                        }
                        catch(\Exception $e){
                        Session::Flash('error'   , trans('controller_translations.text_mail_not_sent'));
                        }
                  }
                // send mail notification to project manager for project cancellation
                }
              /* Notificatin ends */
              Session::flash("success",trans('controller_translations.success_project_canceled_successfully'));
            } 
            else
            {
               Session::flash("error",trans('controller_translations.error_unauthorized_action'));     
            }
        }
        else
        {
            Session::flash("error",trans('controller_translations.error_unauthorized_action'));
        }

        return redirect()->back();
      }
       /*
        Auther : Bharat K.
      */
      public function notifications_seen($id)
      {
        if($id && $id!="")
        {
          $notification_id =  base64_decode($id);
          $status          =  $this->NotificationsModel->where('id',$notification_id)->update(['is_seen'=>'1']);
          if($status) {
            $data['status'] = 'success';
            echo json_encode($data);
          } else  {
            $data['status'] = 'error';
            echo json_encode($data);
          }
        }  
      } 
      /* Authe: Sagar Sainkar  
         comment: show payment methods for milestones
      */
      public function payment_methods($enc_id)
      {
        if ($enc_id) 
        {
            $arr_services      = array();
            $arr_settings      = array();
            $arr_project       = array();
            $arr_paid_service  = array();
            $obj_site_settings = FALSE;
            $obj_project       = FALSE;
            $project_id        = base64_decode($enc_id);
            $services_string   = '';
            $obj_project       = $this->ProjectpostModel->where('id',$project_id)->first();

            if ($obj_project!=FALSE) 
            {
              $arr_project = $obj_project->toArray();
              $paid_services = isset($arr_project['paid_services'])?$arr_project['paid_services']:'';
              if(isset($paid_services) && $paid_services!='')
              {
                $arr_paid_services = explode(',',$paid_services);
              }
            }
            $currency_code = isset($arr_project['project_currency_code'])?$arr_project['project_currency_code']:'USD';
            $obj_site_settings = $this->SiteSettingModel->where('site_settting_id',1)->first(['wesite_project_manager_commission','website_pm_initial_cost','nda_price','urgent_price','private_price','recruiter_price']);

            if($obj_site_settings != FALSE)
            {
                $arr_settings = $obj_site_settings->toArray();
            }

            if(isset($arr_paid_services) && count($arr_paid_services)>0)
            {
              if(!in_array('private',$arr_paid_services) && isset($arr_project['project_type']) && $arr_project['project_type'] == '1')
              {
                $prize = currencyConverterAPI('USD',$currency_code,$arr_settings['private_price']);
                $this->arr_view_data['private_project_cost']  = round($prize);
                array_push($arr_services,'private');
              }
            }
            else
            {
              if(isset($arr_project['project_type']) && $arr_project['project_type'] == '1')
              {
                $prize = currencyConverterAPI('USD',$currency_code,$arr_settings['private_price']);
                $this->arr_view_data['private_project_cost']  = round($prize);
                array_push($arr_services,'private');
              }
            }

            if(isset($arr_paid_services) && count($arr_paid_services)>0)
            {
              if(!in_array('nda',$arr_paid_services) && isset($arr_project['is_nda']) && $arr_project['is_nda'] == '1')
              {
                $prize = currencyConverterAPI('USD',$currency_code,$arr_settings['nda_price']);

                $this->arr_view_data['nda_project_cost']      = round($prize);
                array_push($arr_services,'nda');
              }
            }
            else
            {
              if(isset($arr_project['is_nda']) && $arr_project['is_nda'] == '1')
              {
                $prize = currencyConverterAPI('USD',$currency_code,$arr_settings['nda_price']);
                $this->arr_view_data['nda_project_cost']      = round($prize);
                array_push($arr_services,'nda');
              }
            }

            if(isset($arr_paid_services) && count($arr_paid_services)>0)
            {
              if(!in_array('urgent',$arr_paid_services) && isset($arr_project['is_urgent']) && $arr_project['is_urgent'] == '1')
              {
                $prize = currencyConverterAPI('USD',$currency_code,$arr_settings['urgent_price']);
                $this->arr_view_data['project_type_urgent']      = round($prize);
                array_push($arr_services,'urgent');
              }
            }
            else
            {
              if(isset($arr_project['is_urgent']) && $arr_project['is_urgent'] == '1')
              {
                $prize = currencyConverterAPI('USD',$currency_code,$arr_settings['urgent_price']);
                $this->arr_view_data['project_type_urgent']      = round($prize);
                array_push($arr_services,'urgent');
              }
            }

          
            if(isset($arr_project['highlight_days']) && $arr_project['highlight_days'] > '0' && $arr_project['is_highlight_edited'] == '1')
            {
              //dd($arr_project['highlight_days']);
              $packages     = [];
              $get_packages = $this->ProjectPriorityPackages
                                   ->where('status','0')
                                   ->where('highlited_days',$arr_project['highlight_days'])
                                   ->first();
              if(sizeof($get_packages)>0){
                $packages   = $get_packages->toArray();
              }  
              //dd($packages);
              if(isset($packages[''.$currency_code.'_prize']) && $packages[''.$currency_code.'_prize']!=""){
              //dd($arr_project['highlight_days'],$packages[''.$currency_code.'_prize']);
              $this->arr_view_data['highlight_days_cost']   = $packages[''.$currency_code.'_prize'];
              array_push($arr_services,'highlight');
              }
            }

            if(isset($arr_paid_services) && count($arr_paid_services)>0)
            {
              if(!in_array('recruiter',$arr_paid_services) && isset($arr_project['project_handle_by']) && $arr_project['project_handle_by'] == '2')
              {
                

                $this->arr_view_data['commission_cost']   = 0;
                $prize = currencyConverterAPI('USD',$currency_code,$arr_settings['recruiter_price']);
                if (isset($arr_settings['recruiter_price']))
                {
                  $this->arr_view_data['commission_cost'] = round($prize);
                  array_push($arr_services,'recruiter');
                }
              }
            }
            else
            {
              if(isset($arr_project['project_handle_by']) && $arr_project['project_handle_by'] == '2')
              {
                $this->arr_view_data['commission_cost']   = 0;
                $prize = currencyConverterAPI('USD',$currency_code,$arr_settings['recruiter_price']);
                if (isset($arr_settings['recruiter_price']))
                {
                  $this->arr_view_data['commission_cost'] = round($prize);
                  array_push($arr_services,'recruiter');
                }
              }
            }
            //dd($this->arr_view_data);
              //dd($arr_project);
              $project_currency_code = isset($arr_project['project_currency_code'])&& $arr_project['project_currency_code']!=''?$arr_project['project_currency_code']:'USD';
              //dd($project_currency_code);
                    // get wallet balance //
              $mangopay_wallet         = [];
              $mangopay_wallet_details = [];
              $arr_data                = [];
              $get_wallet_details      = [];

              $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)
                          ->where('currency_code',$project_currency_code)
                          ->first();
              if($obj_data)
              {
                $arr_data = $obj_data->toArray();
              }
               //dd($arr_data);
              //foreach ($arr_data as $key => $value) {
                //$mp_wallet_id = $arr_data['mp_wallet_id'];
              if(isset($arr_data) && count($arr_data)>0)
              {
                $get_wallet_details[] = $this->WalletService->get_wallet_details($arr_data['mp_wallet_id']);
              }
              //}
              //dd($get_wallet_details);
              if(isset($get_wallet_details) && $get_wallet_details!='false' || $get_wallet_details != false){
                $mangopay_wallet         = $get_wallet_details;
                $mangopay_wallet_details = $mangopay_wallet;
              }
            // end get wallet balance //

            if(isset($arr_services) && count($arr_services)>0)
            {
              $services_string = implode(',',$arr_services);
            }

            $arr_service_charge = [];
            $service_charge = $sub_total_payment_amount = $total_payment_amount = $total_pay_amount = $wallet_amount = 0;

            if(isset($mangopay_wallet_details[0]->Balance->Amount) && $mangopay_wallet_details[0]->Balance->Amount>0) {
              $wallet_amount = $mangopay_wallet_details[0]->Balance->Amount/100;
            }

            if(isset($this->arr_view_data['commission_cost'])) {
               $total_payment_amount += $this->arr_view_data['commission_cost'];
            }

            if(isset($this->arr_view_data['private_project_cost'])) {
               $total_payment_amount += $this->arr_view_data['private_project_cost'];
            }

            if(isset($this->arr_view_data['nda_project_cost'])) {
               $total_payment_amount += $this->arr_view_data['nda_project_cost'];
            }

            if(isset($this->arr_view_data['highlight_days_cost'])) {
               $total_payment_amount += $this->arr_view_data['highlight_days_cost'];
            }

            if(isset($this->arr_view_data['project_type_urgent'])) {
               $total_payment_amount += $this->arr_view_data['project_type_urgent'];
            }

            if($total_payment_amount>$wallet_amount)
            {
                $sub_total_payment_amount = (float)$total_payment_amount - (float)$wallet_amount;
                $arr_service_charge       = get_service_charge($sub_total_payment_amount,$currency_code);
                $service_charge           = isset($arr_service_charge['service_charge']) ? $arr_service_charge['service_charge'] : 0;
                $total_pay_amount         = $sub_total_payment_amount + $service_charge;
                $total_pay_amount         = round($total_pay_amount,2);
            } else {
                // $arr_service_charge = get_service_charge($total_payment_amount,$currency_code);
                // $service_charge     = isset($arr_service_charge['service_charge']) ? $arr_service_charge['service_charge'] : 0;
                $total_pay_amount   = $total_payment_amount;
                $total_pay_amount   = round($total_pay_amount,2);
            }
            
            // $mangopay_cards_details = $this->WalletService->get_cards_list($arr_data['mp_user_id']);

            $this->arr_view_data['service_charge']           = $service_charge;
            $this->arr_view_data['sub_total_payment_amount'] = $sub_total_payment_amount;
            $this->arr_view_data['total_payment_amount']     = $total_payment_amount;
            $this->arr_view_data['total_pay_amount']         = $total_pay_amount;
            $this->arr_view_data['wallet_amount']            = $wallet_amount;
            $this->arr_view_data['arr_service_charge']       = $arr_service_charge;
            // $this->arr_view_data['mangopay_cards_details']   = $mangopay_cards_details;

            $this->arr_view_data['page_title']                = trans('controller_translations.project_payment');
            $this->arr_view_data['enc_project_id']            = $enc_id;
            $this->arr_view_data['arr_project']               = $arr_project;
            $this->arr_view_data['mangopay_wallet']           = $mangopay_wallet;
            $this->arr_view_data['mangopay_wallet_details']   = $mangopay_wallet_details;
            $this->arr_view_data['module_url_path']           = $this->module_url_path;
            $this->arr_view_data['services_string']           = $services_string;
            //dd($this->arr_view_data);
            return view('client.projects.payment_methods',$this->arr_view_data);
        }
        return redirect()->back();
      }


      public function payment_methods_return(Request $request)
      {
          $form_data = $request->all();

          $transactionId = isset($form_data['transactionId']) ? $form_data['transactionId']:NULL;
          $invoice_id    = isset($form_data['invoice_id']) ? $form_data['invoice_id']:NULL;
          $service_cost  = isset($form_data['service_cost']) ? $form_data['service_cost']:0;

          $get_project = $this->ProjectpostModel
                                          ->where('invoice_id',$invoice_id)
                                          ->first(['id','project_handle_by','paid_services','updated_at']);

          $project_id = isset($get_project->id) ? base64_encode($get_project->id) : '';

          $redirect_back_url = url('/').'/client/dashboard';
          if($project_id!=''){
            $redirect_back_url = url('/').'/client/projects/payment/'.$project_id;
          }

          $arr_result = $this->WalletService->viewPayIn($transactionId);

          if(isset($arr_result['status']) && $arr_result['status'] == 'error') {
            $msg = isset($arr_result['msg']) ? $arr_result['msg'] : 'Something went, wrong, Unable to process payment, Please try again.';
            Session::flash('error',$msg);  
            return redirect($redirect_back_url);

          }

          $admin_mp_details = get_user_wallet_details('1',$form_data['currency_code']);
          $mp_details       = get_user_wallet_details($this->user_id,$form_data['currency_code']);
          $paid_services    = isset($form_data['paid_services'])?$form_data['paid_services']:'';

          $transaction_inp['tag']                      = $form_data['invoice_id'].'- Job Service fee';
          $transaction_inp['debited_UserId']           = $mp_details['mp_user_id']; // client user id
          $transaction_inp['debited_walletId']         = (string)$mp_details['mp_wallet_id'];

          $transaction_inp['credited_UserId']          = $admin_mp_details['mp_user_id'];
          $transaction_inp['credited_walletId']        = (string)$admin_mp_details['mp_wallet_id'];
          $transaction_inp['total_pay']                = $form_data['amount_int'];
          $transaction_inp['currency_code']            = $form_data['currency_code'];

          $transaction_inp['cost_website_commission']  = '0';

          $pay_fees        = $this->WalletService->walletTransfer($transaction_inp);

          $project         = [];

          if(isset($pay_fees->Status) && $pay_fees->Status == 'SUCCEEDED')
          {
              // update project details
                
                $updated_at = isset($get_project->updated_at) ? $get_project->updated_at : NULL; 
                if(isset($get_project))
                {
                  $project = $get_project->toArray();

                  if(isset($paid_services) && $paid_services!='')
                  {
                    $new_string = $project['paid_services'].','.$paid_services;
                    $this->ProjectpostModel->where('id',$project['id'])->update(['paid_services'=>$new_string,'edited_at'=>date('Y-m-d H:i:s'),'updated_at'=>$updated_at]);
                  }
                }

                if(isset($project['project_handle_by']) && $project['project_handle_by']=="2")
                {
                $update_project = $this->ProjectpostModel->where('invoice_id',$form_data['invoice_id'])->update(['project_status'=>'1','edited_at'=>date('Y-m-d H:i:s'),'updated_at'=>$updated_at]);
                } 
                else if(isset($project['project_handle_by']) && $project['project_handle_by']=="1")
                {
                $update_project = $this->ProjectpostModel->where('invoice_id',$form_data['invoice_id'])->update(['project_status'=>'2','edited_at'=>date('Y-m-d H:i:s'),'updated_at'=>$updated_at]);
                }
                // end update project details
               
                // update transaction details
                $update_transaction   = [];
                $update_transaction['WalletTransactionId']    = isset($pay_fees->Id) ? $pay_fees->Id : '0';
                $update_transaction['payment_status']         = 2; // paid
                $update_transaction['response_data']          = json_encode($pay_fees); // paid
                $updatetransaction = $this->TransactionsModel->where('invoice_id',$form_data['invoice_id'])->update($update_transaction); 
                // end update transaction details    

                // send service fee to admin walllet 
                if($admin_mp_details)
                {
                  $transaction_admin_inp['tag']                      = $form_data['invoice_id'].'- Job Service fee';
                  $transaction_admin_inp['debited_UserId']           = $mp_details['mp_user_id'];           
                  $transaction_admin_inp['credited_UserId']          = $admin_mp_details['mp_user_id'];
                  $transaction_admin_inp['total_pay']                = $service_cost;              
                  $transaction_admin_inp['debited_walletId']         = (string)$mp_details['mp_wallet_id']; 
                  $transaction_admin_inp['credited_walletId']        = (string)$admin_mp_details['mp_wallet_id']; 
                  $transaction_admin_inp['cost_website_commission']  = '0';
                  $transaction_admin_inp['currency_code']            = $form_data['currency_code'];
                  $pay_admin_fees        = $this->WalletService->walletTransfer($transaction_admin_inp);
                }

                $posted_project_name = isset($get_project->project_name) ? $get_project->project_name : '';

                // send notification to admin
                $arr_admin_data                         =  [];
                $arr_admin_data['user_id']              =  $form_data['user_id'];
                $arr_admin_data['user_type']            = '1';
                $arr_admin_data['url']                  = 'admin/wallet/archexpert';
                $arr_admin_data['project_id']           = '';
                $arr_admin_data['notification_text_en'] = $posted_project_name . ' - ' . $form_data['invoice_id'].':'.Lang::get('controller_translations.project_service_fee_paid',[],'en','en').' '.$pay_fees->Id;
                $arr_admin_data['notification_text_de'] = $posted_project_name . ' - ' . $form_data['invoice_id'].':'.Lang::get('controller_translations.project_service_fee_paid',[],'de','en').' '.$pay_fees->Id;
                $this->NotificationsModel->create($arr_admin_data); 
                      // end send notification to admin    

                // Session::flash('payment_success_msg',trans('controller_translations.msg_payment_transaction_for_post_project_is_successfully'));
                Session::Flash('success',trans('controller_translations.msg_payment_transaction_for_post_project_is_successfully'));
                //Session::flash('payment_return_url','/client/projects/posted');
                $this->MailService->ProjectPaymentMail($form_data['invoice_id']);
                Redirect::to(url('/client/projects/posted'))->send();
                //Redirect::to($this->payment_success_url)->send();
          } 
          else
          {
             if(isset($pay_fees->ResultMessage)){
                Session::flash('error',$pay_fees->ResultMessage);  
             } else {
                             if(gettype($pay_fees) == 'string'){
                              Session::flash('error',$pay_fees);
                             }
             }
            return redirect($redirect_back_url);
          }
      }

      public function project_manager_payment_return(Request $request){
        
        $form_data = $request->all();

        $transactionId = isset($form_data['transactionId']) ? $form_data['transactionId']:NULL;
        $invoice_id    = isset($form_data['invoice_id']) ? $form_data['invoice_id']:NULL;
        $service_cost  = isset($form_data['service_cost']) ? $form_data['service_cost']:0;

        $get_project = $this->ProjectpostModel
                                        ->where('invoice_id',$invoice_id)
                                        ->first();

        $project_id = isset($get_project->id) ? base64_encode($get_project->id) : '';

        $redirect_back_url = url('/client/projects/ongoing');
        if($project_id!=''){
          $redirect_back_url = url('/').'/client/projects/pm_payment/'.$project_id;
        }

        $arr_result = $this->WalletService->viewPayIn($transactionId);
        if(isset($arr_result['status']) && $arr_result['status'] == 'error') {
          $msg = isset($arr_result['msg']) ? $arr_result['msg'] : 'Something went, wrong, Unable to process payment, Please try again.';
          Session::flash('error',$msg);  
          return redirect($redirect_back_url);
        }

        $amount_int   = floatval($form_data['amount_int']);
        $service_cost = floatval($form_data['service_cost']);
        $total_pay    = $amount_int - $service_cost;

        $admin_mp_details = get_user_wallet_details('1',$form_data['currency_code']);
        $mp_details       = get_user_wallet_details($this->user_id,$form_data['currency_code']);

        $transaction_inp['tag']                      = $invoice_id.'- Job project manager fee';
        $transaction_inp['debited_UserId']           = $mp_details['mp_user_id'];           // client user id
        $transaction_inp['credited_UserId']          = $admin_mp_details['mp_user_id'];   // archexpert admin user id
        $transaction_inp['total_pay']                = $total_pay;                 // services amount 
        $transaction_inp['debited_walletId']         = (string)$mp_details['mp_wallet_id']; // client wallet id
        $transaction_inp['credited_walletId']        = (string)$admin_mp_details['mp_wallet_id']; // archexpert admin wallet id
        $transaction_inp['cost_website_commission']  = '0';
        $transaction_inp['currency_code']            = $form_data['currency_code'];
        $pay_fees = $this->WalletService->walletTransfer($transaction_inp);

        if(isset($pay_fees->Status) && $pay_fees->Status == 'SUCCEEDED')
        {
            // update project details
            $this->ProjectpostModel->where('invoice_id',$invoice_id)->update(['project_handle_by_pm'=>'1']);
            // end update project details
             
            // update transaction details
            $update_transaction   = [];
            $update_transaction['WalletTransactionId']    = isset($pay_fees->Id) ? $pay_fees->Id : '0';
            $update_transaction['payment_status']         = 2; // paid
            $update_transaction['response_data']          = json_encode($pay_fees); // paid
            $updatetransaction = $this->TransactionsModel->where('invoice_id',$invoice_id)->update($update_transaction); 
            // end update transaction details    

            // send service fee to admin walllet 
            if($admin_mp_details)
            {
              $transaction_admin_inp['tag']                      = $form_data['invoice_id'].'- Job Service fee';
              $transaction_admin_inp['debited_UserId']           = $mp_details['mp_user_id'];           
              $transaction_admin_inp['credited_UserId']          = $admin_mp_details['mp_user_id'];
              $transaction_admin_inp['total_pay']                = $service_cost;              
              $transaction_admin_inp['debited_walletId']         = (string)$mp_details['mp_wallet_id']; 
              $transaction_admin_inp['credited_walletId']        = (string)$admin_mp_details['mp_wallet_id']; 
              $transaction_admin_inp['cost_website_commission']  = '0';
              $transaction_admin_inp['currency_code']            = $form_data['currency_code'];
              $pay_admin_fees        = $this->WalletService->walletTransfer($transaction_admin_inp);
            }
            
            $posted_project_name = isset($get_project->project_name) ? $get_project->project_name : '';

            // send notification to admin
            $arr_admin_data                         =  [];
            $arr_admin_data['user_id']              =  $admin_mp_details['id'];
            $arr_admin_data['user_type']            = '1';
            $arr_admin_data['url']                  = 'admin/wallet/archexpert';
            $arr_admin_data['project_id']           = '';
            $arr_admin_data['notification_text_en'] = $posted_project_name . ' - ' . $invoice_id.':'.Lang::get('controller_translations.project_manager_fee_paid',[],'en','en').' '.$pay_fees->Id;
            $arr_admin_data['notification_text_de'] = $posted_project_name . ' - ' . $invoice_id.':'.Lang::get('controller_translations.project_manager_fee_paid',[],'de','en').' '.$pay_fees->Id;
            $this->NotificationsModel->create($arr_admin_data); 
            // end send notification to admin    

            //Admin notification for assign project manager
            $arr_data['user_id']              =  $admin_mp_details['id'];
            $arr_data['url']                  = "admin/projects/posted";
            $arr_data['notification_text_en'] = $posted_project_name . ' - ' . Lang::get('controller_translations.assign_project_manager',[],'en','en');
            $arr_data['notification_text_de'] = $posted_project_name . ' - ' . Lang::get('controller_translations.assign_project_manager',[],'de','en');
            $arr_data['project_id'] = $project_id;
            $this->NotificationsModel->create($arr_data);
            //End of Admin notification for assign project manager

            Session::flash('success',trans('controller_translations.msg_payment_transaction_for_assign_project_manager'));

            Session::flash('payment_return_url','/client/projects/ongoing');

            $this->MailService->ProjectPaymentMail($invoice_id);
            
            return redirect(url('/client/projects/ongoing'));

            // return Redirect::to($this->payment_success_url)->send();
        } 
        else 
        {
          if(isset($pay_fees->ResultMessage)){
              Session::flash('error',$pay_fees->ResultMessage);  
          } 
          else 
          {
              if(gettype($pay_fees) == 'string'){
                Session::flash('error',$pay_fees);
              }
          }
          
          return redirect($redirect_back_url);
        }

    }

      public function pm_payment($enc_id)
      {
        if ($enc_id) 
        {
            $arr_settings      = array();
            $arr_project       = array();
            $obj_site_settings = FALSE;
            $obj_project       = FALSE;
            $project_id        = base64_decode($enc_id);
            $obj_project       = $this->ProjectpostModel->with(['project_bids_infos'=>function($query) use($project_id){
                              $query->where('project_id','=',$project_id);
                              $query->where('bid_status','=','1');

                          }])
                          ->where('id',$project_id)
                          ->first();
            if ($obj_project!=FALSE) 
            {
              $arr_project = $obj_project->toArray();
            }
            
            $project_currency_symbol = isset($arr_project['project_currency'])?$arr_project['project_currency']:'$';
            $project_currency_code = isset($arr_project['project_currency_code'])?$arr_project['project_currency_code']:'USD';

            $final_project_cost = 0;
            $final_project_cost = isset($arr_project['project_bids_infos'][0]['bid_cost'])?$arr_project['project_bids_infos'][0]['bid_cost']:'';

            $obj_site_settings = $this->SiteSettingModel->where('site_settting_id',1)->first(['wesite_project_manager_commission','website_pm_initial_cost','nda_price','urgent_price','private_price','recruiter_price','project_cost']);

            if($obj_site_settings != FALSE)
            {
                $arr_settings = $obj_site_settings->toArray();
            }

            $pm_cost = 0;

            if(isset($arr_settings['project_cost']) && $arr_settings['project_cost']!='' && $arr_settings['project_cost'] > $final_project_cost)
            {
              $pm_cost = isset($arr_settings['website_pm_initial_cost'])?$arr_settings['website_pm_initial_cost']:'';
            }
            else
            {
              $admin_pm_cost = isset($arr_settings['wesite_project_manager_commission'])?$arr_settings['wesite_project_manager_commission']:0;
              $pm_cost = $final_project_cost * $admin_pm_cost / 100;
            }
            
            // convert price into current currency code
            $base_currency_code = 'USD';
            if($base_currency_code != $project_currency_code) {
              $pm_cost = round(currencyConverterAPI($base_currency_code, $project_currency_code, $pm_cost));
            }

            $this->arr_view_data['project_currency_symbol'] = $project_currency_symbol;
            $this->arr_view_data['project_currency_code']   = $project_currency_code;
            $this->arr_view_data['commission_cost']         = $pm_cost;
            

            $mangopay_wallet           = [];
            $mangopay_wallet_details   = [];

            $service_charge = $sub_total_payment_amount = $total_payment_amount = $total_pay_amount = $wallet_amount = 0;

            $arr_data = get_user_wallet_details($this->user_id,$project_currency_code);
            
            if(isset($arr_data['mp_wallet_id']) && $arr_data['mp_wallet_id']!='') {
              $get_wallet_details = $this->WalletService->get_wallet_details($arr_data['mp_wallet_id']);
              if(isset($get_wallet_details) && $get_wallet_details!='false' || $get_wallet_details != false){
                $mangopay_wallet         = $get_wallet_details;
                $mangopay_wallet_details = $mangopay_wallet;
              }
              $wallet_amount = isset($get_wallet_details->Balance->Amount) ? $get_wallet_details->Balance->Amount/100:0;
            }
       
            $total_payment_amount = $pm_cost;

            if($total_payment_amount>$wallet_amount)
            {
                $sub_total_payment_amount = (float)$total_payment_amount - (float)$wallet_amount;
                $arr_service_charge       = get_service_charge($sub_total_payment_amount,$project_currency_code);
                $service_charge           = isset($arr_service_charge['service_charge']) ? $arr_service_charge['service_charge'] : 0;
                $total_pay_amount         = $sub_total_payment_amount + $service_charge;
                $total_pay_amount         = round($total_pay_amount,2);
            } else {
                $total_pay_amount   = $total_payment_amount;
                $total_pay_amount   = round($total_pay_amount,2);
            }

            $this->arr_view_data['service_charge']           = $service_charge;
            $this->arr_view_data['sub_total_payment_amount'] = $sub_total_payment_amount;
            $this->arr_view_data['total_payment_amount']     = $total_payment_amount;
            $this->arr_view_data['total_pay_amount']         = $total_pay_amount;
            $this->arr_view_data['wallet_amount']            = $wallet_amount;

            $this->arr_view_data['page_title']                = "Project Manager payment";
            $this->arr_view_data['enc_project_id']            = $enc_id;
            $this->arr_view_data['arr_project']               = $arr_project;
            $this->arr_view_data['mangopay_wallet']           = $mangopay_wallet;
            $this->arr_view_data['mangopay_wallet_details']   = $mangopay_wallet_details;
            $this->arr_view_data['module_url_path']           = $this->module_url_path;
            
            return view('client.projects.pm_payment',$this->arr_view_data);
        }
        return redirect()->back();
      }
      /*
        Comments : Accept project completion request recived from client
        Auther   : Nayan S.
      */
      public function accept_completion_request($enc_id)
      { 
        $project_id = base64_decode($enc_id);
        if($project_id == "")
        {
          Session::flash('error', trans('controller_translations.invalid_project_details_available'));
          return redirect()->back();
        }
        $obj_project_info = $this->ProjectpostModel->where('id','=',$project_id)->first();

        if($obj_project_info && isset($obj_project_info->client_user_id) && ( $this->user_id == $obj_project_info->client_user_id ) )
        {
          $res_project = $obj_project_info->update(['project_status' => 3 ,
                                                    'has_completion_request' => 2]);
          if($res_project)
          {
            Session::flash('success',trans('controller_translations.project_completion'));  
            /* Create Notification for client */

            $posted_project_name = isset($obj_project_info->project_name) ? $obj_project_info->project_name : '';      
            $arr_data =  [];
            /*$arr_data['notification_text'] =  trans('controller_translations.project_completed_by_client');  */
            $arr_data['notification_text_en'] = $posted_project_name . ' - ' . Lang::get('controller_translations.project_completed_by_client',[],'en','en');
            $arr_data['notification_text_de'] = $posted_project_name . ' - ' . Lang::get('controller_translations.project_completed_by_client',[],'de','en');
            $arr_data['project_id']           = $project_id;
            
           /* if(isset($obj_project_info->client_user_id) && $obj_project_info->client_user_id != "")
            {
              $arr_data['user_id']    = $obj_project_info->client_user_id;
              $arr_data['user_type']  = '2';
              $arr_data['url']        = 'client/projects/details/'.base64_encode($project_id);
              $this->completed_project_notification($arr_data);
            }*/

            if(isset($obj_project_info->expert_user_id) && $obj_project_info->expert_user_id != "")
            {
              $arr_data['user_id']    = $obj_project_info->expert_user_id;
              $arr_data['user_type']  = '3';
              $arr_data['url']        = 'expert/projects/details/'.base64_encode($project_id);
              $this->completed_project_notification($arr_data);
            }

            if(isset($obj_project_info->project_manager_user_id) && $obj_project_info->project_manager_user_id != "")
            {
              $arr_data['user_id']    = $obj_project_info->project_manager_user_id;
              $arr_data['user_type']  = '4';
              $arr_data['url']        = 'project_manager/projects/details/'.base64_encode($project_id);
              $this->completed_project_notification($arr_data);
            }
            /* to admin */
            $arr_data['user_id']    = 1;
            $arr_data['user_type']  = '1';
            $arr_data['url']        = 'admin/projects/completed';
            $this->completed_project_notification($arr_data);
            /* Notificatin ends */
            /* Send mail to client */
            $this->MailService->project_completed($project_id);
          }
          else
          {
            Session::flash('error', trans('controller_translations.error_while_updating_project_completion_request'));  
          }
        }
        else
        {
          Session::flash('error', trans('controller_translations.invalid_action_for_project_completion'));  
        }
        return redirect()->back();
      }
      public function completed_project_notification($arr_data)
      {
        $this->NotificationsModel->create($arr_data);  
        return TRUE;
      }

      public function old_get_experts(Request $request)
      {
        $html            = '';
        $already_invited = $already_bidded_arr = $already_invited_arr = [];
        if(!empty($request->input('project_id')) && $request->input('project_id') !="")
        {
            $project_id = base64_decode($request->input('project_id'));

            $get_already_invited = $this->InviteProjectModel->select('expert_user_id')->where('project_id',$project_id)->groupBy('expert_user_id')->get();
            
            if($get_already_invited){
               $already_invited = $get_already_invited->toArray(); 
               foreach ($already_invited as $key => $value) {
                 $already_invited_arr[] = $value['expert_user_id'];
               }
            }

            $get_already_bidded = $this->ProjectsBidsModel->select('expert_user_id')->where('project_id',$project_id)->groupBy('expert_user_id')->get();
            
            if($get_already_bidded){
               $already_bidded = $get_already_bidded->toArray(); 
               foreach ($already_bidded as $key => $value) {
                 $already_bidded_arr[] = $value['expert_user_id'];
               }
            }
            $arr_experts = []; 
            $obj_experts = $this->ExpertsModel
                           ->join('users','users.id' , '=' , 'experts.user_id')
                           ->join('experts_skills','experts_skills.expert_user_id' , '=' , 'experts.user_id')
                           ->join('project_skills','project_skills.skill_id' , '=' , 'experts_skills.skill_id')
                           ->join('experts_categories','experts_categories.expert_user_id' , '=' , 'experts.user_id')
                           ->join('projects','projects.category_id' , '=' , 'experts_categories.category_id')
                           ->where('projects.id',$project_id)
                           ->where('users.is_active',1)
                           ->where('users.is_available',1)
                           ->with(['user_details','country_details'])->whereHas('user_details',function ($query){
                              $query->where('is_active',1);
                              $query->where('is_available',1);
                            })
                           ->groupBy('experts.user_id')
                           ->get();

            if($obj_experts != FALSE){
              $arr_experts        = $obj_experts->toArray();

                $html = '<option value="">--'.trans('client/projects/invite_experts.text_select').'--</option>';
                if(isset($arr_experts) && (sizeof($arr_experts)>0))
                { 
                  foreach($arr_experts as $expert)
                  {
                    $disabled = $style = $title = '';
                    if(isset($this->profile_img_public_path) && isset($expert['profile_image']) && $expert['profile_image']!=NULL && file_exists('public/uploads/front/profile/'.$expert['profile_image'])){
                    $profile_image = $this->profile_img_public_path.$expert['profile_image'];
                    } else {
                    $profile_image = $this->profile_img_public_path.'default_profile_image.png';
                    }
                    $first_name = isset($expert['user_details']['role_info']['first_name'])?$expert['user_details']['role_info']['first_name']:'Unknown user';
                    $last_name  = isset($expert['user_details']['role_info']['last_name'])?substr($expert['user_details']['role_info']['last_name'],0,1).'.':'';

                    $average_rating = ReviewsModel::where('to_user_id',$expert['user_id'])->avg('rating');
                    $average_rating = number_format($average_rating,1);

                    $country = isset($expert['country_details']['country_name'])?$expert['country_details']['country_name']:'';
                    
                    if(isset($already_invited_arr)){
                        if(in_array($expert['user_id'], $already_invited_arr)){
                           $disabled = 'disabled';
                           $style    = 'style="cursor:no-drop;"';
                           $title    = '- <small>(Already invited)</small>';
                        }
                    }
                    if(isset($already_bidded_arr)){
                        if(in_array($expert['user_id'], $already_bidded_arr)){
                           $disabled = 'disabled';
                           $style    = 'style="cursor:no-drop;"';
                           $title    = '- <small>(Bid already arrived)</small>';
                        }
                    }                 
                    $html .= '<option '.$disabled.' '.$style.'  value="'.base64_encode($expert['user_id']).'" email="'.$expert['user_details']['email'].'" src="'.$profile_image.'" name="'.$first_name.'"> '.$first_name.' '.$last_name.' '.$title.' '.$average_rating.' '.$country.'</option>';
                  }
                } 
                else
                {
                    $html.= "<option disabled>We have not found any expert \r\n who fit the job requirements.</option>";
                }
            }  
            echo $html;
        } else {
            echo false; 
            exit;
        }
      }

      public function get_experts(Request $request)
      {
        $html            = '';
        $already_invited = $already_bidded_arr = $already_invited_arr = [];
        if(!empty($request->input('project_id')) && $request->input('project_id') !="")
        {
            $project_id = base64_decode($request->input('project_id'));

            $get_already_invited = $this->InviteProjectModel->select('expert_user_id')->where('project_id',$project_id)->groupBy('expert_user_id')->get();
            
            if($get_already_invited){
               $already_invited = $get_already_invited->toArray(); 
               foreach ($already_invited as $key => $value) {
                 $already_invited_arr[] = $value['expert_user_id'];
               }
            }

            $get_already_bidded = $this->ProjectsBidsModel->select('expert_user_id')->where('project_id',$project_id)->groupBy('expert_user_id')->get();
            
            if($get_already_bidded){
               $already_bidded = $get_already_bidded->toArray(); 
               foreach ($already_bidded as $key => $value) {
                 $already_bidded_arr[] = $value['expert_user_id'];
               }
            }
            $arr_experts = []; 
            $obj_experts = $this->ExpertsModel
                           ->join('users','users.id' , '=' , 'experts.user_id')
                           ->join('experts_skills','experts_skills.expert_user_id' , '=' , 'experts.user_id')
                           ->join('project_skills','project_skills.skill_id' , '=' , 'experts_skills.skill_id')
                           ->join('experts_categories','experts_categories.expert_user_id' , '=' , 'experts.user_id')
                           ->join('experts_categories_subcategories','experts_categories_subcategories.expert_user_id' , '=' , 'experts.user_id')
                           ->join('projects', function($join)
                           {
                             $join->on('projects.category_id', '=', 'experts_categories.category_id');
                             $join->on('projects.sub_category_id', '=', 'experts_categories_subcategories.subcategory_id');

                           })
                           ->where('projects.id',$project_id)
                           ->where('users.is_active',1)
                           ->where('users.is_available',1)
                           ->with(['user_details','country_details'=>function($qry){
                                $qry->orderBy('country_name','desc');
                           }])->whereHas('user_details',function ($query){
                              $query->where('is_active',1);
                              $query->where('is_available',1);
                            })
                           ->groupBy('experts.user_id')
                           ->get();

            if($obj_experts != FALSE)
            {
              $arr_experts        = $obj_experts->toArray();
               
                if(isset($arr_experts) && (sizeof($arr_experts)>0))
                { 
                  foreach($arr_experts as $key => $expert)
                  {
                    $disabled = $style = $title = '';

                    if(isset($this->profile_img_public_path) && isset($expert['profile_image']) && $expert['profile_image']!=NULL && file_exists('public/uploads/front/profile/'.$expert['profile_image'])){
                    $profile_image = $this->profile_img_public_path.$expert['profile_image'];
                    } else {
                    $profile_image = $this->profile_img_public_path.'default_profile_image.png';
                    }

                    $arr_experts[$key]['first_name'] = isset($expert['user_details']['role_info']['first_name'])?$expert['user_details']['role_info']['first_name']:'Unknown user';
                    $arr_experts[$key]['last_name']  = isset($expert['user_details']['role_info']['last_name'])?substr($expert['user_details']['role_info']['last_name'],0,1).'.':'';

                    $average_rating = ReviewsModel::where('to_user_id',$expert['user_id'])->avg('rating');
                    $arr_experts[$key]['average_rating'] = number_format($average_rating,1);

                    $arr_experts[$key]['country'] = isset($expert['country_details']['country_name'])?$expert['country_details']['country_name']:'';
                    $arr_experts[$key]['profile_image'] = $profile_image;
                    
                    if(isset($already_invited_arr))
                    {
                        if(in_array($expert['user_id'], $already_invited_arr)){
                           $disabled = 'disabled';
                           $style    = 'style="cursor:no-drop;"';
                           $title    = '- <small>(Already invited)</small>';
                        }
                    }
                    if(isset($already_bidded_arr))
                    {
                        if(in_array($expert['user_id'], $already_bidded_arr)){
                           $disabled = 'disabled';
                           $style    = 'style="cursor:no-drop;"';
                           $title    = '- <small>(Bid already arrived)</small>';
                        }
                    }      

                    $arr_experts[$key]['disabled'] = $disabled;
                    $arr_experts[$key]['style'] = $style;
                    $arr_experts[$key]['title'] = $title;
                    $arr_experts[$key]['user_id'] = base64_encode($expert['user_id']);
                    $arr_experts[$key]['email'] = $expert['user_details']['email'];
                  }
                } 
                
            }  

            $html = '<option value="">--'.trans('client/projects/invite_experts.text_select').'--</option>';
            if(isset($arr_experts) && sizeof($arr_experts)>0)
            {
              $_sortBy = [];
              foreach ($arr_experts as $key => $row)
              {
                $_sortBy['average_rating'][$key]                    = $row['average_rating'];
              }

              if(isset($_sortBy) && count($_sortBy) > 0)
              {
                array_multisort($_sortBy['average_rating'], SORT_DESC, $arr_experts);
              }

              foreach ($arr_experts as $exp_key => $exp_value) 
              {
                $html .= '<option '.$exp_value['disabled'].' '.$exp_value['style'].'  value="'.$exp_value['user_id'].'" email="'.$exp_value['email'].'" name="'.$exp_value['first_name'].'" src="'.$exp_value['profile_image'].'"> '.$exp_value['first_name'].' '.$exp_value['last_name'].' '.$exp_value['title'].' <span class="rating-box">'.$exp_value['average_rating'].' </span> '.$exp_value['country'].'</option>';

                // $html .= '<option '.$exp_value['disabled'].' '.$exp_value['style'].'  value="'.$exp_value['user_id'].'" email="'.$exp_value['email'].'" name="'.$exp_value['first_name'].'" src="'.$exp_value['profile_image'].'" data-thumbnail="images/icon-chrome.png" data-subtext="<span class=rating-box>'.$exp_value['average_rating'].'</span>" data-icon="glyphicon glyphicon-star"  > '.$exp_value['first_name'].' '.$exp_value['last_name'].' '.$exp_value['title'].' '.$exp_value['country'].'</option>';
              }
            }
            else
            {
              $html.= "<option disabled>We have not found any expert \r\n who fit the job requirements.</option>";
            }

            echo $html;
        } 
        else 
        {
            echo false; 
            exit;
        }

      }
      
      public function invite_experts(Request $request)
      {
          if(!empty($request->input('project-id'))          && 
                    $request->input('project-id') != ""     &&
             !empty($request->input('experts'))             && 
                    $request->input('experts') != ""        &&
             !empty($request->input('expert-email'))        && 
                    $request->input('expert-email') != ""   &&
             !empty($request->input('invite-message'))      && 
                    $request->input('invite-message') != ""
          )
          {

            $get_project_invitations_count  = \DB::table('invite_to_project')->where('project_id', base64_decode($request->input('project-id')))->count();
            if($get_project_invitations_count >= 5){
              Session::flash('error', trans('controller_translations.sorry_you_have_exited_invitation_sent_request_limit')); 
              return redirect()->back();
            }
            $expire_on        = date('Y-m-d H:i:s', strtotime("+2 days"));
            $project_id       = $request->input('project-id',null);
            $experts_user_id  = $request->input('experts',null);
            $expert_email     = $request->input('expert-email',null);
            $invite_message   = $request->input('invite-message',null);
            $arr_profile_data = [];

            $project_info_name = $this->ProjectpostModel->select('id','project_name')->where('id',base64_decode($project_id))->first();

            $job_name = isset($project_info_name->project_name)?$project_info_name->project_name:'';

            $is_project_awarded = $this->ProjectsBidsModel->where("bid_status","4")
                                                          ->where("project_id",base64_decode($project_id))
                                                          ->first();
            if(isset($is_project_awarded) && $is_project_awarded != null){
              Session::flash("error",trans('controller_translations.text_this_project_is_already_awarded_so_you_can_not_invite_any_other_expert'));
              return redirect()->back();
            }
            $is_expert_bided    = $this->ProjectsBidsModel
                                  ->where('project_id',base64_decode($project_id))
                                  ->where('expert_user_id',base64_decode($experts_user_id))
                                  ->count();
            if(isset($is_expert_bided) && $is_expert_bided >'0')
            {
              Session::flash("error","This expert have already bidded on this project,so you can not invite him again.");
              return redirect()->back();
            }
            $is_invitation_sent = $this->InviteProjectModel
                                  ->where('project_id',base64_decode($project_id))
                                  ->where('expert_user_id',base64_decode($experts_user_id))
                                  ->count();

            if(isset($is_invitation_sent) && $is_invitation_sent >'0')
            {
              Session::flash("error",trans('invitations/invitation.text_You_have_already_sent_invitation_to_this_expert'));
              return redirect()->back();
            } 
            if(isset($experts_user_id) && $experts_user_id != "")
            {
              $arr_profile_data = sidebar_information(base64_decode($experts_user_id));
            }
            $expert_first_name = $expert_last_name = "";
            $expert_first_name = isset($arr_profile_data['user_details']['first_name'])?$arr_profile_data['user_details']['first_name']:'Unknown user';
            $expert_last_name  = isset($arr_profile_data['user_details']['last_name'])?substr($arr_profile_data['user_details']['last_name'],0,1):''; 

            // create unique indentifier
              $rs  = \DB::table('invite_to_project')->select('unique_id')->where('unique_id' ,'!=' , null)->where('unique_id' ,'!=' , '')->orderBy('id' , 'DESC')->LIMIT('1')->get();
              $res = $rs;
              if(count($res) > 0)
              {                 
                  $max_inv_nbr = $res[0]->unique_id;
                  // remove STU from that no.                
                  $max_inv_nbr = substr($max_inv_nbr, 3);
                  // get count of that no.
                  $cnt = strlen($max_inv_nbr);
                  // increment that no. by 1
                  $max_inv_nbr = $max_inv_nbr + 1;                        
                  // add no. of zero's to the no.                
                  $max_inv_nbr = str_pad($max_inv_nbr, $cnt, '0', STR_PAD_LEFT);
                  $max_inv_nbr = 'INV'.$max_inv_nbr;  
              }
              else
              {
                  $max_inv_nbr = 'INV000000001';  
              }
            // create unique indentifier
            
            
            $email_to                   = isset($expert_email)? $expert_email:''; //'tushara@webwingtechnologies.com';//
            /* store invite project data*/
               $store_arr = [];
               $store_arr['project_id']      = base64_decode($project_id);
               $store_arr['client_user_id']  = $this->user_id;
               $store_arr['expert_user_id']  = base64_decode($experts_user_id);
               $store_arr['expert_email_id'] = $email_to;
               $store_arr['message']         = $invite_message;
               $store_arr['notification_id'] = '0';//$send_notification;
               $store_arr['expire_on']       = $expire_on;
               $store_arr['unique_id']       = $max_inv_nbr;
               $store_notification = $this->InviteProjectModel->insertGetId($store_arr);
            /* end store invite project data*/
           
            if($store_notification){
              $arr_data['invite_message'] = $invite_message;
              $arr_data['expert_name']    = $expert_first_name/*.' '.$expert_last_name*/;
              $arr_data['unique_id']      = $max_inv_nbr;
              $arr_data['email_id']       = isset($expert_email)? $expert_email:'';
              $email_to                   = isset($expert_email)? $expert_email:''; //'tushara@webwingtechnologies.com';//
             /* $mail_form                  = get_site_email_address();
              $mail_form                  = isset($mail_form)? $mail_form:'archexperts@gmail.com';*/

              $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';

                if($email_to!= "")
                {

                  $posted_project_name = isset($project_info_name->project_name) ? $project_info_name->project_name : '';
                  /* Send notification */
                  $arr_data                         = [];
                  $arr_data['user_id']              = base64_decode($experts_user_id);
                  $arr_data['user_type']            = '3';
                  $arr_data['url']                  = "expert/projects/invitations/show/".base64_encode($store_notification);

                  $arr_data['notification_text_en'] = $posted_project_name .' - ' . $max_inv_nbr.':'.Lang::get('controller_translations.you_have_new_invitation_for_project_bid',[],'en','en');
                  $arr_data['notification_text_de'] = $posted_project_name .' - ' . $max_inv_nbr.':'.Lang::get('controller_translations.you_have_new_invitation_for_project_bid',[],'de','en');

                  $arr_data['project_id']           = base64_decode($project_id);

                  $send_notification = $this->NotificationsModel->insertGetId($arr_data); 
                  /* End Send notification */

                  if($send_notification){
                    $project_name = config('app.project.name');
                    try{
                         $mail_status = $this->MailService->send_invite_expert_email($arr_data);
                        // Mail::send('front.email.invite_expert', $arr_data, function ($invite_message) use ($email_to,$mail_form,$project_name) {
                        //     $invite_message->from($mail_form, $project_name);
                        //     $invite_message->subject($project_name.': Invitation for project bid');
                        //     $invite_message->to($email_to);
                        // });
                    }
                    catch(\Exception $e){
                      Session::flash('error',trans('controller_translations.text_mail_not_sent')); 
                    }


                    $update_arr = [];
                    $update_arr['notification_id'] = $send_notification;
                    $this->InviteProjectModel->where('id',$store_notification)->update($update_arr);
                  } 
                  
                  Session::flash('success', trans('controller_translations.you_have_successfully_sent_invitation_to_client')); 
                  return redirect()->back();
                }
            } 
            else 
            {
              Session::flash('error', trans('controller_translations.something_went_wrong_please_try_again_later')); 
              return redirect()->back();
            }
          }
          else
          {
            Session::flash('error', trans('controller_translations.something_went_wrong_please_try_again_later')); 
            return redirect()->back();
          }
      }  

      public function delete_project($id)
      {
        $id=base64_decode($id);
        if($id=="")
        {
          return redirect()->back();
        }

        $status = $this->ProjectpostModel->where('id',$id)->delete();
        if($status)
        {
          Session::flash('success','Project deleted successfully,');
          return redirect()->back();
        }
        else
        {
          Session::flash('error',trans('controller_translations.something_went_wrong_please_try_again_later'));
          return redirect()->back();
        }
      }

    public function get_currency_budget_html(Request $request)
    {
        $arr_project = [];
        $project_id = $request->input('project_id');
        $obj_project = $this->ProjectpostModel
                                      ->where('id',$project_id)
                                      ->first();
        if($obj_project){
          $arr_project = $obj_project->toArray();
        }

        $arr_resp = [];
        $arr_resp['status'] = 'error';

        $type = '';

        if($request->has('budget_type') && $request->input('budget_type') == 'hourly'){
            $type = 'hourly';
        }else{
            $type = 'fixed';
        }

        if($request->has('cuurency_id') && $request->input('cuurency_id') != '')
        {
            $obj_currency = $this->CurrencyModel->where('id', $request->input('cuurency_id'))
                                                    ->whereHas('job_post_budget', function(){})
                                                    ->with('job_post_budget')
                                                    ->first();

            if($obj_currency)
            {
                $arr_currency = $obj_currency->toArray();

                if(isset($arr_currency['job_post_budget']) && !empty($arr_currency['job_post_budget']))
                {
                    $symbol = $arr_currency['currency'];

                    

                    if($type == 'hourly')
                    {
                        $html = '<option value="">Select hourly rate</option>';

                        $hourly_price = $arr_currency['job_post_budget']['hourly_price'];
                        $arr_hourly_price = unserialize($hourly_price);

                        if(!empty($arr_hourly_price))
                        {
                            $project_cost = isset($arr_project['project_cost'])?$arr_project['project_cost']:'';
                            foreach($arr_hourly_price as $from => $to)
                            {

                                if($to <= 0){

                                    $selectedbug = '';
                                    if($project_cost == $from)
                                    {
                                      $selectedbug = 'selected="selected"';
                                    }

                                    $text = 'Over '.$symbol.$from.' /hour';
                                    $html .= '<option value="'.$from.'" '.$selectedbug.' >'.$text.'</option>';
                                }else{

                                    $from_to = $from.'-'.$to;
                                    $selectedbug = '';
                                    if($project_cost == $from_to)
                                    {
                                      $selectedbug = 'selected="selected"';
                                    }

                                    $text = $symbol.$from.'-'.$symbol.$to.'/hour';
                                    $html .= '<option value="'.$from.'-'.$to.'" '.$selectedbug.' >'.$text.'</option>';
                                }
                            }

                            //dd($html);

                            $selected = '';
                            if(isset($arr_project['is_custom_price']) && $arr_project['is_custom_price'] == '1'){
                              $selected = 'selected="selected"';
                            }

                            $html .= '<option '. $selected .' value="CUSTOM_RATE">Customize Budget</option>';

                            $arr_resp['status'] = 'success';
                            $arr_resp['html'] = $html;
                        }
                    }
                    else
                    {
                        $project_cost = isset($arr_project['project_cost'])?$arr_project['project_cost']:'';
                        $html = '<option value="">Select job budget</option>';
                        $jumbo_from = isset($arr_currency['job_post_budget']['jumbo_from'])?$arr_currency['job_post_budget']['jumbo_from']:'';
                        $jumbo = 'Over '.$symbol.$jumbo_from.' (Jumbo)';

                        $selectedbug = '';
                        if($project_cost == $jumbo_from)
                        {
                          $selectedbug = 'selected="selected"';
                        }
                        $html .= '<option value="'.$jumbo_from.'" '.$selectedbug.' >'.$jumbo.'</option>';

                        $big_from = isset($arr_currency['job_post_budget']['big_from'])?$arr_currency['job_post_budget']['big_from']:'';
                        $big_to = isset($arr_currency['job_post_budget']['big_to'])?$arr_currency['job_post_budget']['big_to']:'';
                        $big = $symbol.$big_from.'-'.$symbol.$big_to.' (Big)';

                        $selectedbug = '';
                        if($project_cost == $big_from.'-'.$big_to)
                        {
                          $selectedbug = 'selected="selected"';
                        }
                        $html .= '<option value="'.$big_from.'-'.$big_to.'" '.$selectedbug.' >'.$big.'</option>';

                        $large_from=isset($arr_currency['job_post_budget']['large_from'])?$arr_currency['job_post_budget']['large_from']:'';
                        $large_to = isset($arr_currency['job_post_budget']['large_to'])?$arr_currency['job_post_budget']['large_to']:'';
                        $large = $symbol.$large_from.'-'.$symbol.$large_to.' (Large)';

                        $selectedbug = '';
                        if($project_cost == $large_from.'-'.$large_to)
                        {
                          $selectedbug = 'selected="selected"';
                        }
                        $html .= '<option value="'.$large_from.'-'.$large_to.'" '.$selectedbug.' >'.$large.'</option>';

                        $medium_from = isset($arr_currency['job_post_budget']['medium_from'])?$arr_currency['job_post_budget']['medium_from']:'';
                        $medium_to = isset($arr_currency['job_post_budget']['medium_to'])?$arr_currency['job_post_budget']['medium_to']:'';
                        $medium = $symbol.$medium_from.'-'.$symbol.$medium_to.' (Medium)';

                        $selectedbug = '';
                        if($project_cost == $medium_from.'-'.$medium_to)
                        {
                          $selectedbug = 'selected="selected"';
                        }
                        $html .= '<option value="'.$medium_from.'-'.$medium_to.'" '.$selectedbug.' >'.$medium.'</option>';

                        $small_from=isset($arr_currency['job_post_budget']['small_from'])?$arr_currency['job_post_budget']['small_from']:'';
                        $small_to = isset($arr_currency['job_post_budget']['small_to'])?$arr_currency['job_post_budget']['small_to']:'';
                        $small = $symbol.$small_from.'-'.$symbol.$small_to.' (Small)';

                        $selectedbug = '';
                        if($project_cost == $small_from.'-'.$small_to)
                        {
                          $selectedbug = 'selected="selected"';
                        }
                        $html .= '<option value="'.$small_from.'-'.$small_to.'" '.$selectedbug.' >'.$small.'</option>';

                        $tiny_from = isset($arr_currency['job_post_budget']['tiny_from'])?$arr_currency['job_post_budget']['tiny_from']:'';
                        $tiny_to = isset($arr_currency['job_post_budget']['tiny_to'])?$arr_currency['job_post_budget']['tiny_to']:'';
                        $tiny = $symbol.$tiny_from.'-'.$symbol.$tiny_to.' (Tiny)';

                        $selectedbug = '';
                        if($project_cost == $tiny_from.'-'.$tiny_to)
                        {
                          $selectedbug = 'selected="selected"';
                        }
                        $html .= '<option value="'.$tiny_from.'-'.$tiny_to.'" '.$selectedbug.' >'.$tiny.'</option>';



                        $selected = '';
                        if(isset($arr_project['is_custom_price']) && $arr_project['is_custom_price'] == '1'){
                          $selected = 'selected="selected"';
                        }

                        $html .= '<option ' . $selected . ' value="CUSTOM_RATE">Customize Budget</option>';
                        
                        $arr_resp['status'] = 'success';
                        $arr_resp['html'] = $html;
                    }
                }
            }
        }

        return json_encode($arr_resp);

    }

    public function get_highlighted_html(Request $request)
    {
        $arr_resp = [];
        $arr_resp['status'] = 'error';
        $arr_resp['arr_project_type_cost'] = [];

        $type = '';
        
        if($request->has('cuurency_id') && $request->input('cuurency_id') != '')
        {
            $obj_currency = $this->CurrencyModel->where('id', $request->input('cuurency_id'))
                                                    ->first();

            if($obj_currency)
            {
                $currency_code = isset($obj_currency->currency_code)?$obj_currency->currency_code:'USD';
                $currency_sym  = isset($obj_currency->currency)?$obj_currency->currency:'$';
                $obj_data = $this->ProjectPriorityPackages->where('status','0')->get();
                if($obj_data)
                {
                  $arr_data = $obj_data->toArray();
                }

                if(isset($arr_data) && !empty($arr_data))
                {
                    $html = '';

                    if(!empty($arr_data))
                    {
                        foreach($arr_data as $key => $val)
                        {
                            if($currency_code=='USD'){
                              $value = $val['highlited_days'].'-'.isset($val['USD_prize'])?$val['USD_prize']:'0';
                              $high_price = isset($val['USD_prize'])?$val['USD_prize']:'0';

                              $html .= '<div class="radio_area regi"><input type="radio" class="css-checkbox" id="'.$key.'-radio_fixed" value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'" name="project_highlight"><label class="css-label radGroup1" for="'.$key.'-radio_fixed"> '.$currency_sym.' '.$high_price.' '.$currency_code.' for '.$val['highlited_days'].' Days </label></div>';
                            }
                            
                            if($currency_code=='EUR'){
                              $value = $val['highlited_days'].'-'.isset($val['EUR_prize'])?$val['EUR_prize']:'0';
                              $high_price = isset($val['EUR_prize'])?$val['EUR_prize']:'0';
                              $html .= '<div class="radio_area regi"><input type="radio" class="css-checkbox" id="'.$key.'-radio_fixed" value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'" name="project_highlight"><label class="css-label radGroup1" for="'.$key.'-radio_fixed"> '.$currency_sym.' '.$high_price.' '.$currency_code.' for '.$val['highlited_days'].' Days </label></div>';
                            }
                            if($currency_code=='GBP'){
                              
                              $value = $val['highlited_days'].'-'.isset($val['GBP_prize'])?$val['GBP_prize']:'0';
                              $high_price = isset($val['GBP_prize'])?$val['GBP_prize']:'0';
                              $html .= '<div class="radio_area regi"><input type="radio" class="css-checkbox" id="'.$key.'-radio_fixed" value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'" name="project_highlight"><label class="css-label radGroup1" for="'.$key.'-radio_fixed"> '.$currency_sym.' '.$high_price.' '.$currency_code.' for '.$val['highlited_days'].' Days </label></div>';
                            }
                            if($currency_code=='PLN'){
                              $value = $val['highlited_days'].'-'.isset($val['PLN_prize'])?$val['PLN_prize']:'0';
                              $high_price = isset($val['PLN_prize'])?$val['PLN_prize']:'0';

                              $html .= '<div class="radio_area regi"><input type="radio" class="css-checkbox" id="'.$key.'-radio_fixed" value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'" name="project_highlight"><label class="css-label radGroup1" for="'.$key.'-radio_fixed"> '.$currency_sym.' '.$high_price.' '.$currency_code.' for '.$val['highlited_days'].' Days </label></div>';
                            }
                            if($currency_code=='CHF'){

                              $value = $val['highlited_days'].'-'.isset($val['CHF_prize'])?$val['CHF_prize']:'0';
                              $high_price = isset($val['CHF_prize'])?$val['CHF_prize']:'0';

                              $html .= '<div class="radio_area regi"><input type="radio" class="css-checkbox" id="'.$key.'-radio_fixed" value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'" name="project_highlight"><label class="css-label radGroup1" for="'.$key.'-radio_fixed"> '.$currency_sym.' '.$high_price.' '.$currency_code.' for '.$val['highlited_days'].' Days </label></div>';
                            }
                            if($currency_code=='NOK'){
                              $value = $val['highlited_days'].'-'.isset($val['NOK_prize'])?$val['NOK_prize']:'0';
                              $high_price = isset($val['NOK_prize'])?$val['NOK_prize']:'0';

                              $html .= '<div class="radio_area regi"><input type="radio" class="css-checkbox" id="'.$key.'-radio_fixed" value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'" name="project_highlight"><label class="css-label radGroup1" for="'.$key.'-radio_fixed"> '.$currency_sym.' '.$high_price.' '.$currency_code.' for '.$val['highlited_days'].' Days </label></div>';
                            
                            }
                            if($currency_code=='SEK'){
                              $value = $val['highlited_days'].'-'.isset($val['SEK_prize'])?$val['SEK_prize']:'0';
                              $high_price = isset($val['SEK_prize'])?$val['SEK_prize']:'0';

                              $html .= '<div class="radio_area regi"><input type="radio" class="css-checkbox" id="'.$key.'-radio_fixed" value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'" name="project_highlight"><label class="css-label radGroup1" for="'.$key.'-radio_fixed"> '.$currency_sym.' '.$high_price.' '.$currency_code.' for '.$val['highlited_days'].' Days </label></div>';
                           
                            }
                            if($currency_code=='DKK'){
                              $value = $val['highlited_days'].'-'.isset($val['DKK_prize'])?$val['DKK_prize']:'0';
                              $high_price = isset($val['DKK_prize'])?$val['DKK_prize']:'0';

                              $html .= '<div class="radio_area regi"><input type="radio" class="css-checkbox" id="'.$key.'-radio_fixed" value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'" name="project_highlight"><label class="css-label radGroup1" for="'.$key.'-radio_fixed"> '.$currency_sym.' '.$high_price.' '.$currency_code.' for '.$val['highlited_days'].' Days </label></div>';
                            }
                            if($currency_code=='CAD'){
                              $value = $val['highlited_days'].'-'.isset($val['CAD_prize'])?$val['CAD_prize']:'0';
                              $high_price = isset($val['CAD_prize'])?$val['CAD_prize']:'0';

                              $html .= '<div class="radio_area regi"><input type="radio" class="css-checkbox" id="'.$key.'-radio_fixed" value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'" name="project_highlight"><label class="css-label radGroup1" for="'.$key.'-radio_fixed"> '.$currency_sym.' '.$high_price.' '.$currency_code.' for '.$val['highlited_days'].' Days </label></div>';
                            }
                            if($currency_code=='ZAR'){
                              $value = $val['highlited_days'].'-'.isset($val['ZAR_prize'])?$val['ZAR_prize']:'0';
                              $high_price = isset($val['ZAR_prize'])?$val['ZAR_prize']:'0';

                              $html .= '<div class="radio_area regi"><input type="radio" class="css-checkbox" id="'.$key.'-radio_fixed" value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'" name="project_highlight"><label class="css-label radGroup1" for="'.$key.'-radio_fixed"> '.$currency_sym.' '.$high_price.' '.$currency_code.' for '.$val['highlited_days'].' Days </label></div>';
                            }
                            if($currency_code=='AUD'){
                              $value = $val['highlited_days'].'-'.isset($val['AUD_prize'])?$val['AUD_prize']:'0';
                              $high_price = isset($val['AUD_prize'])?$val['AUD_prize']:'0';

                              $html .= '<div class="radio_area regi"><input type="radio" class="css-checkbox" id="'.$key.'-radio_fixed" value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'" name="project_highlight"><label class="css-label radGroup1" for="'.$key.'-radio_fixed"> '.$currency_sym.' '.$high_price.' '.$currency_code.' for '.$val['highlited_days'].' Days </label></div>';
                            }
                            if($currency_code=='HKD'){

                              $value = $val['highlited_days'].'-'.isset($val['HKD_prize'])?$val['HKD_prize']:'0';
                              $high_price = isset($val['HKD_prize'])?$val['HKD_prize']:'0';

                              $html .= '<div class="radio_area regi"><input type="radio" class="css-checkbox" id="'.$key.'-radio_fixed" value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'" name="project_highlight"><label class="css-label radGroup1" for="'.$key.'-radio_fixed"> '.$currency_sym.' '.$high_price.' '.$currency_code.' for '.$val['highlited_days'].' Days </label></div>';
                            }
                            if($currency_code=='CZK'){
                              $value = $val['highlited_days'].'-'.isset($val['CZK_prize'])?$val['CZK_prize']:'0';
                              $high_price = isset($val['CZK_prize'])?$val['CZK_prize']:'0';

                              $html .= '<div class="radio_area regi"><input type="radio" class="css-checkbox" id="'.$key.'-radio_fixed" value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'" name="project_highlight"><label class="css-label radGroup1" for="'.$key.'-radio_fixed"> '.$currency_sym.' '.$high_price.' '.$currency_code.' for '.$val['highlited_days'].' Days </label></div>';
                            }
                            if($currency_code=='JPY'){

                              $value = $val['highlited_days'].'-'.isset($val['JPY_prize'])?$val['JPY_prize']:'0';
                              $high_price = isset($val['JPY_prize'])?$val['JPY_prize']:'0';

                              $html .= '<div class="radio_area regi"><input type="radio" class="css-checkbox" id="'.$key.'-radio_fixed" value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'" name="project_highlight"><label class="css-label radGroup1" for="'.$key.'-radio_fixed"> '.$currency_sym.' '.$high_price.' '.$currency_code.' for '.$val['highlited_days'].' Days </label></div>';
                            }

                            /*if($currency_code=='USD'){
                              $value = $val['highlited_days'].'-'.isset($val['USD_prize'])?$val['USD_prize']:'0';
                              $high_price = isset($val['USD_prize'])?$val['USD_prize']:'0';

                              $html .= '<option value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$val['highlited_days'].' Days </option>';
                            }
                            
                            if($currency_code=='EUR'){
                              $value = $val['highlited_days'].'-'.isset($val['EUR_prize'])?$val['EUR_prize']:'0';
                              $high_price = isset($val['EUR_prize'])?$val['EUR_prize']:'0';

                              $html .= '<option value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$val['highlited_days'].' Days </option>';
                            }
                            if($currency_code=='GBP'){
                            
                            $value = $val['highlited_days'].'-'.isset($val['GBP_prize'])?$val['GBP_prize']:'0';
                              $high_price = isset($val['GBP_prize'])?$val['GBP_prize']:'0';

                              $html .= '<option value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$val['highlited_days'].' Days </option>';
                            }
                            if($currency_code=='PLN'){
                              $value = $val['highlited_days'].'-'.isset($val['PLN_prize'])?$val['PLN_prize']:'0';
                              $high_price = isset($val['PLN_prize'])?$val['PLN_prize']:'0';

                              $html .= '<option value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$val['highlited_days'].' Days </option>';
                            }
                            if($currency_code=='CHF'){

                              $value = $val['highlited_days'].'-'.isset($val['CHF_prize'])?$val['CHF_prize']:'0';
                              $high_price = isset($val['CHF_prize'])?$val['CHF_prize']:'0';

                              $html .= '<option value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$val['highlited_days'].' Days </option>';
                            }
                            if($currency_code=='NOK'){
                              $value = $val['highlited_days'].'-'.isset($val['NOK_prize'])?$val['NOK_prize']:'0';
                              $high_price = isset($val['NOK_prize'])?$val['NOK_prize']:'0';

                              $html .= '<option value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$val['highlited_days'].' Days </option>';
                            
                            }
                            if($currency_code=='SEK'){
                              $value = $val['highlited_days'].'-'.isset($val['SEK_prize'])?$val['SEK_prize']:'0';
                              $high_price = isset($val['SEK_prize'])?$val['SEK_prize']:'0';

                              $html .= '<option value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$val['highlited_days'].' Days </option>';
                           
                            }
                            if($currency_code=='DKK'){
                              $value = $val['highlited_days'].'-'.isset($val['DKK_prize'])?$val['DKK_prize']:'0';
                              $high_price = isset($val['DKK_prize'])?$val['DKK_prize']:'0';

                              $html .= '<option value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$val['highlited_days'].' Days </option>';
                            }
                            if($currency_code=='CAD'){
                              $value = $val['highlited_days'].'-'.isset($val['CAD_prize'])?$val['CAD_prize']:'0';
                              $high_price = isset($val['CAD_prize'])?$val['CAD_prize']:'0';

                              $html .= '<option value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$val['highlited_days'].' Days </option>';
                            }
                            if($currency_code=='ZAR'){
                              $value = $val['highlited_days'].'-'.isset($val['ZAR_prize'])?$val['ZAR_prize']:'0';
                              $high_price = isset($val['ZAR_prize'])?$val['ZAR_prize']:'0';

                              $html .= '<option value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$val['highlited_days'].' Days </option>';
                            }
                            if($currency_code=='AUD'){
                              $value = $val['highlited_days'].'-'.isset($val['AUD_prize'])?$val['AUD_prize']:'0';
                              $high_price = isset($val['AUD_prize'])?$val['AUD_prize']:'0';

                              $html .= '<option value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$val['highlited_days'].' Days </option>';
                            }
                            if($currency_code=='HKD'){

                              $value = $val['highlited_days'].'-'.isset($val['HKD_prize'])?$val['HKD_prize']:'0';
                              $high_price = isset($val['HKD_prize'])?$val['HKD_prize']:'0';

                              $html .= '<option value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$val['highlited_days'].' Days </option>';
                            }
                            if($currency_code=='CZK'){
                              $value = $val['highlited_days'].'-'.isset($val['CZK_prize'])?$val['CZK_prize']:'0';
                              $high_price = isset($val['CZK_prize'])?$val['CZK_prize']:'0';

                              $html .= '<option value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$val['highlited_days'].' Days </option>';
                            }
                            if($currency_code=='JPY'){

                              $value = $val['highlited_days'].'-'.isset($val['JPY_prize'])?$val['JPY_prize']:'0';
                              $high_price = isset($val['JPY_prize'])?$val['JPY_prize']:'0';

                              $html .= '<option value="'.$val['highlited_days'].'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$val['highlited_days'].' Days </option>';
                            }*/
                        }

                        $arr_resp['status'] = 'success';
                        $arr_resp['html'] = $html;
                    }
                    
                }
            }

            $currency_code = isset($obj_currency->currency_code)?$obj_currency->currency_code:'USD';
            $currency_sym  = isset($obj_currency->currency)?$obj_currency->currency:'$';

            $arr_project_type_cost = [];
            $obj_project_type_cost = $this->SiteSettingModel->select('nda_price','urgent_price','private_price','recruiter_price')->first();
            if($obj_project_type_cost)
            {
              $arr_project_type_cost = $obj_project_type_cost->toArray();
            }
            
            $arr_project_type_cost['currency_code']   = $currency_code;
            $arr_project_type_cost['currency_sym']    = $currency_sym;

            if(isset($arr_project_type_cost) && count($arr_project_type_cost)>0 && $currency_code!='USD') {
                $base_currency_code = 'USD';
                
                $nda_price       = isset($arr_project_type_cost['nda_price']) ? $arr_project_type_cost['nda_price'] : 0;
                $urgent_price    = isset($arr_project_type_cost['urgent_price']) ? $arr_project_type_cost['urgent_price'] : 0;
                $private_price   = isset($arr_project_type_cost['private_price']) ? $arr_project_type_cost['private_price'] : 0;
                $recruiter_price = isset($arr_project_type_cost['recruiter_price']) ? $arr_project_type_cost['recruiter_price'] : 0;
                
                $arr_project_type_cost['nda_price']       = round(currencyConverterAPI($base_currency_code, $currency_code, $nda_price)); 
                $arr_project_type_cost['urgent_price']    = round(currencyConverterAPI($base_currency_code, $currency_code, $urgent_price));
                $arr_project_type_cost['private_price']   = round(currencyConverterAPI($base_currency_code, $currency_code, $private_price));
                $arr_project_type_cost['recruiter_price'] = round(currencyConverterAPI($base_currency_code, $currency_code, $recruiter_price));
            }
            $arr_resp['arr_project_type_cost'] = $arr_project_type_cost;
        }

        return json_encode($arr_resp);

    }

    public function built_edit_job_highlighted_html($currency)
    {
        $arr_resp = [];
        $arr_resp['status'] = 'error';

        $type = '';

        if($currency && $currency != '')
        {
            $obj_currency = $this->CurrencyModel->where('currency_code', $currency)
                                                    ->first();

            if($obj_currency)
            {
                $currency_code = isset($obj_currency->currency_code)?$obj_currency->currency_code:'USD';
                $currency_sym  = isset($obj_currency->currency)?$obj_currency->currency:'$';
                $obj_data = $this->ProjectPriorityPackages->where('status','0')->get();
                if($obj_data)
                {
                  $arr_data = $obj_data->toArray();
                }

                if(isset($arr_data) && !empty($arr_data))
                {
                    $html = '<option>-- Select --</option>';
                    if(!empty($arr_data))
                    {
                        foreach($arr_data as $key => $val)
                        {
                            if($currency_code=='USD'){
                            
                            $value = $val['highlited_days'].'-'.isset($val['USD_prize'])?$val['USD_prize']:'0';
                              $high_price = isset($val['USD_prize'])?$val['USD_prize']:'0';

                              $html .= '<option value="'.$value.'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$value.' Days </option>';
                            }
                            
                            if($currency_code=='EUR'){
                              $value = $val['highlited_days'].'-'.isset($val['EUR_prize'])?$val['EUR_prize']:'0';
                              $high_price = isset($val['EUR_prize'])?$val['EUR_prize']:'0';

                              $html .= '<option value="'.$value.'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$value.' Days </option>';
                            }
                            if($currency_code=='GBP'){
                            
                            $value = $val['highlited_days'].'-'.isset($val['GBP_prize'])?$val['GBP_prize']:'0';
                              $high_price = isset($val['GBP_prize'])?$val['GBP_prize']:'0';

                              $html .= '<option value="'.$value.'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$value.' Days </option>';
                            }
                            if($currency_code=='PLN'){
                              $value = $val['highlited_days'].'-'.isset($val['PLN_prize'])?$val['PLN_prize']:'0';
                              $high_price = isset($val['PLN_prize'])?$val['PLN_prize']:'0';

                              $html .= '<option value="'.$value.'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$value.' Days </option>';
                            }
                            if($currency_code=='CHF'){

                              $value = $val['highlited_days'].'-'.isset($val['CHF_prize'])?$val['CHF_prize']:'0';
                              $high_price = isset($val['CHF_prize'])?$val['CHF_prize']:'0';

                              $html .= '<option value="'.$value.'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$value.' Days </option>';
                            }
                            if($currency_code=='NOK'){
                              $value = $val['highlited_days'].'-'.isset($val['NOK_prize'])?$val['NOK_prize']:'0';
                              $high_price = isset($val['NOK_prize'])?$val['NOK_prize']:'0';

                              $html .= '<option value="'.$value.'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$value.' Days </option>';
                            
                            }
                            if($currency_code=='SEK'){
                              $value = $val['highlited_days'].'-'.isset($val['SEK_prize'])?$val['SEK_prize']:'0';
                              $high_price = isset($val['SEK_prize'])?$val['SEK_prize']:'0';

                              $html .= '<option value="'.$value.'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$value.' Days </option>';
                           
                            }
                            if($currency_code=='DKK'){
                              $value = $val['highlited_days'].'-'.isset($val['DKK_prize'])?$val['DKK_prize']:'0';
                              $high_price = isset($val['DKK_prize'])?$val['DKK_prize']:'0';

                              $html .= '<option value="'.$value.'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$value.' Days </option>';
                            }
                            if($currency_code=='CAD'){
                              $value = $val['highlited_days'].'-'.isset($val['CAD_prize'])?$val['CAD_prize']:'0';
                              $high_price = isset($val['CAD_prize'])?$val['CAD_prize']:'0';

                              $html .= '<option value="'.$value.'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$value.' Days </option>';
                            }
                            if($currency_code=='ZAR'){
                              $value = $val['highlited_days'].'-'.isset($val['ZAR_prize'])?$val['ZAR_prize']:'0';
                              $high_price = isset($val['ZAR_prize'])?$val['ZAR_prize']:'0';

                              $html .= '<option value="'.$value.'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$value.' Days </option>';
                            }
                            if($currency_code=='AUD'){
                              $value = $val['highlited_days'].'-'.isset($val['AUD_prize'])?$val['AUD_prize']:'0';
                              $high_price = isset($val['AUD_prize'])?$val['AUD_prize']:'0';

                              $html .= '<option value="'.$value.'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$value.' Days </option>';
                            }
                            if($currency_code=='HKD'){

                              $value = $val['highlited_days'].'-'.isset($val['HKD_prize'])?$val['HKD_prize']:'0';
                              $high_price = isset($val['HKD_prize'])?$val['HKD_prize']:'0';

                              $html .= '<option value="'.$value.'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$value.' Days </option>';
                            }
                            if($currency_code=='CZK'){
                              $value = $val['highlited_days'].'-'.isset($val['CZK_prize'])?$val['CZK_prize']:'0';
                              $high_price = isset($val['CZK_prize'])?$val['CZK_prize']:'0';

                              $html .= '<option value="'.$value.'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$value.' Days </option>';
                            }
                            if($currency_code=='JPY'){

                              $value = $val['highlited_days'].'-'.isset($val['JPY_prize'])?$val['JPY_prize']:'0';
                              $high_price = isset($val['JPY_prize'])?$val['JPY_prize']:'0';

                              $html .= '<option value="'.$value.'" data-highlight-price="'.$high_price.'" data-days="'.$val['highlited_days'].'"> '.$currency_sym.' '.$high_price.' for '.$value.' Days </option>';
                            }
                        }
                        //$arr_resp['status'] = 'success';
                        return  $html;
                    }
                    
                }
            }
        }

        //return json_encode($arr_resp);

    }

    private function _generate_invoice_id()
    {
      $secure      = TRUE;    
        $bytes       = openssl_random_pseudo_bytes(3, $secure);
        $order_token = 'INV'.date('Ymd').strtoupper(bin2hex($bytes));
        return $order_token;
    }

    public function create_user_wallet($arr_client)
    {
      $arr_currency = [];
      $obj_currency = $this->CurrencyModel->select('id','currency_code')->get();
      if($obj_currency)
      {
        $arr_currency = $obj_currency->toArray();
      }

      if(isset($arr_client) && count($arr_client)>0)
      {
        $first_name   = "-";
        $last_name    = "-";
        $email        = "-";

        $first_name   = isset($arr_client['first_name'])?$arr_client['first_name']:'';
        $last_name    = isset($arr_client['last_name'])?$arr_client['last_name']:'';
        $email        = isset($arr_client['email'])?$arr_client['email']:'';

        try{
          $timezoneinfo = file_get_contents('http://ip-api.com/json/' . $_SERVER['REMOTE_ADDR']);
        }catch(\Exception $e)
        {
          $timezoneinfo = file_get_contents('http://ip-api.com/json');  
        } 
        $mp_user_data = [];

        $mp_user_data['FirstName']          = $first_name;
        $mp_user_data['LastName']           = $last_name;
        $mp_user_data['Email']              = $email;
        $mp_user_data['CountryOfResidence'] = isset($timezoneinfo->countryCode)?$timezoneinfo->countryCode:"IN";
        $mp_user_data['Nationality']        = isset($timezoneinfo->countryCode)?$timezoneinfo->countryCode:"IN";
        
        $currency_code =  isset($logged_user['currency_code'])?$logged_user['currency_code']:'USD';
        if(isset($arr_client['selected_currency_code']) && $arr_client['selected_currency_code']!='')
        {
          $currency_code = isset($arr_client['selected_currency_code'])?$arr_client['selected_currency_code']:'USD';
        }

        $mp_user_data['currency_code']      = $currency_code;

        $Mangopay_create_user               = $this->WalletService->create_user_with_all_currency_wallet($mp_user_data,$arr_currency,$arr_client['id']);
        if($Mangopay_create_user)
        {
          $this->UserModel->where('id',$this->user_id)->update(['currency_code'=>$currency_code]);
          return true;
        }
        else
        {
          return false;
        }
      }
      else
      {
        return false;
      }   
    }  

    public function add_shortlist($project_bid_id)
    {
      if($project_bid_id!=false)
      {
        $project_bid_id = base64_decode($project_bid_id);
        $status = $this->ProjectsBidsModel->where('id',$project_bid_id)
                                          ->update(['is_shortlist'=>'1']);
        if($status)
        {
          Session::flash('success','Expert added in shortlisted successfully.'); 
          return redirect()->back();
        }
        else
        {
          Session::flash('error', trans('controller_translations.something_went_wrong_please_try_again_later')); 
          return redirect()->back();
        } 
      }
      else
      {
        Session::flash('error', trans('controller_translations.something_went_wrong_please_try_again_later')); 
        return redirect()->back();
      }
    }
    
    public function remove_shortlist($project_bid_id)
    {
      if($project_bid_id!=false)
      {
        $project_bid_id = base64_decode($project_bid_id);
        $status = $this->ProjectsBidsModel->where('id',$project_bid_id)
                                          ->update(['is_shortlist'=>'0']);
        if($status)
        {
          Session::flash('success','Expert removed from shortlisted successfully.'); 
          return redirect()->back();
        }
        else
        {
          Session::flash('error', trans('controller_translations.something_went_wrong_please_try_again_later')); 
          return redirect()->back();
        } 
      }
      else
      {
        Session::flash('error', trans('controller_translations.something_went_wrong_please_try_again_later'));  
        return redirect()->back();
      }
    }
    


} // end class