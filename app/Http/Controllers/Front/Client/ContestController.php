<?php
namespace App\Http\Controllers\Front\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\UserModel;
use App\Models\ClientsModel;
use App\Models\CategoriesModel;
use App\Models\SubCategoriesModel;
use App\Models\SkillsModel;
use App\Common\Services\LanguageService; 
use App\Common\Services\WalletService; 
use App\Models\ContestSkillsModel;
use App\Models\StaticPageModel;
use App\Models\NotificationsModel;
use App\Models\SiteSettingModel;
use App\Models\ContestEntryModel;  
use App\Common\Services\MailService;
use App\Models\ContestModel;
use App\Models\UserWalletModel;
use App\Models\ContestEntryImagesCommentsModel;
use App\Models\ContestEntryImagesRatingModel;
use App\Models\ContestEntryOriginalFilesModel;
use App\Models\ContestPricingModel;
use App\Models\ContestEntryImagesModel;
use App\Models\ContestPostDocumentsModel;
use App\Models\CurrencyModel;
/*Used helper method to fire event*/
use App\Events\sendEmailOnPostProject;
use Sentinel;
use Validator;
use Session;
use Mail;
use App;
use Lang;
use Redirect;
class ContestController extends Controller{
  public $arr_view_data;
  public function __construct(
    SkillsModel $skill,
    UserModel $user_model,
    ClientsModel $clients,
    CategoriesModel $category,
    SubCategoriesModel $sub_categories_model,
    LanguageService $langauge,
    ContestSkillsModel  $contest_skills,
    StaticPageModel $static_pages,
    NotificationsModel $notifications,
    SiteSettingModel $site_settings,
    ContestModel $ContestModel,
    MailService $mail_service,
    ContestEntryModel  $contest_entry,
    ContestEntryImagesCommentsModel $ContestEntryImagesCommentsModel,
    ContestEntryImagesRatingModel $ContestEntryImagesRatingModel,
    ContestEntryOriginalFilesModel $ContestEntryOriginalFilesModel,
    ContestPricingModel $ContestPricingModel,
    ContestEntryImagesModel $ContestEntryImagesModel,
    WalletService $WalletService,
    ContestPostDocumentsModel $ContestPostDocumentsModel
  )

  {
    if(! $user = Sentinel::check()){
      $this->user                               = [];
      return redirect('/login');
    }else{
      $this->user                               = $user;
    }

    $this->user_id                               = $user->id;
    $this->UserModel                             = $user_model;
    $this->ClientsModel                          = $clients;
    $this->CategoriesModel                       = $category;
    $this->SubCategoriesModel                    = $sub_categories_model;
    $this->LanguageService                       = $langauge;
    $this->SkillsModel                           = $skill;
    $this->NotificationsModel                    = $notifications;
    $this->ContestSkillsModel                    = $contest_skills;
    $this->StaticPageModel                       = $static_pages;
    $this->SiteSettingModel                      = $site_settings;
    $this->MailService                           = $mail_service;
    $this->ContestModel                          = $ContestModel;
    $this->ContestEntryModel                     = $contest_entry;  
    $this->ContestEntryImagesCommentsModel       = $ContestEntryImagesCommentsModel;
    $this->ContestEntryImagesRatingModel         = $ContestEntryImagesRatingModel;
    $this->ContestEntryOriginalFilesModel        = $ContestEntryOriginalFilesModel;
    $this->ContestPricingModel                   = $ContestPricingModel;
    $this->ContestEntryImagesModel               = $ContestEntryImagesModel;
    $this->WalletService                         = $WalletService;
    $this->UserWalletModel                       = new UserWalletModel;
    $this->CurrencyModel                         = new CurrencyModel();
    
    $this->ContestPostDocumentsModel             = $ContestPostDocumentsModel;
    $this->user_id                               = $user->id;
    $this->module_url_path                       = url("/client/contest");
    $this->login_url                             = url("/login");
    $this->arr_view_data                         = [];
    $this->contest_send_entry_public_file_path   = url('/').config('app.project.img_path.contest_send_entry_files');
    $this->contest_send_entry_base_file_path     = base_path().'/public'.config('app.project.img_path.contest_send_entry_files');

    if(isset($user))
    {
      $logged_user                 = $user->toArray();  
      if($logged_user['mp_wallet_created'] == 'Yes')
      {
        $this->mp_user_id          = $logged_user['mp_user_id'];
        $this->mp_wallet_id        = $logged_user['mp_wallet_id'];
        $this->mp_wallet_created   = $logged_user['mp_wallet_created'];
      } 
      else
      {
        $this->mp_user_id          = '';
        $this->mp_wallet_id        = '';
        $this->mp_wallet_created   = 'No';
      }
    } 
    else 
    {
      $this->mp_user_id          = '';
      $this->mp_wallet_id        = '';
      $this->mp_wallet_created   = 'No';
    }
  }
    /*
    Post  : contest project
    Author: Tushar Ahire*/
    public function post(Request $request){

      $contest_id = $request->input('id');
      
      //dd($this->user_id);
      //dd('Working on this functionality.......');
      $this->arr_view_data['page_title']          = trans('controller_translations.page_title_post_a_contest');
      $this->arr_view_data['module_url_path']     = $this->module_url_path;
      $arr_categories   = [];
      $arr_skills       = [];
      $arr_lang         = $this->LanguageService->get_all_language();
      $obj_category     = $this->CategoriesModel->where('is_active',1)->get();
      if($obj_category != FALSE)
      {
        $arr_categories = $obj_category->toArray();
      }
      $obj_skills     = $this->SkillsModel->where('is_active',1)->get();
      if($obj_skills != FALSE){
        $arr_skills = $obj_skills->toArray();
      }
      //$arr_contest_currency = config('app.project_currency');
      $arr_contest_currency = [];
      $obj_currency = $this->CurrencyModel->select('id','currency_code','currency','description')
                                            ->where('is_active','1')
                                            ->get();
      if($obj_currency)
      {
        $arr_contest_currency  = $obj_currency->toArray();
      }

      $obj_static_privacy   = $this->StaticPageModel->where('page_slug','privacy-policy')->where('is_active',"1")->first();
      $arr_static_privacy   = array();
      if($obj_static_privacy){
        $arr_static_privacy   = $obj_static_privacy->toArray();
      }
      $obj_static_terms     = $this->StaticPageModel->where('page_slug','terms-of-service')->where('is_active',"1")->first();
      $arr_static_terms     = array();
      if($obj_static_terms){
        $arr_static_terms     = $obj_static_terms->toArray();
      }

      $obj_client = $this->ClientsModel->where('user_id',$this->user_id)
                                       ->with(['user_details','country_details.states','state_details.cities','city_details'])
                                       ->first();
      if($obj_client != FALSE) {
        $arr_client_details = $obj_client->toArray();
      }

      $arr_job_document_formats = array();
      $arr_job_document_formats = config('app.project_post_document_formats');

      $arr_repost_contest = [];
      if($contest_id!=false){
        $obj_repost_contest = $this->ContestModel->with('contest_skills')->where('id',base64_decode($contest_id))->first();
        if($obj_repost_contest){
            $arr_repost_contest = $obj_repost_contest->toArray();
        }
      }
      // dump($arr_repost_contest);
      $this->arr_view_data['arr_categories']           = $arr_categories;
      $this->arr_view_data['arr_skills']               = $arr_skills;
      $this->arr_view_data['arr_contest_currency']     = $arr_contest_currency;
      $this->arr_view_data['arr_static_privacy']       = $arr_static_privacy;
      $this->arr_view_data['arr_static_terms']         = $arr_static_terms;
      $this->arr_view_data['arr_job_document_formats'] = $arr_job_document_formats;
      $this->arr_view_data['user_details']             = $this->user;
      $this->arr_view_data['arr_client_details']       = $arr_client_details;
      $this->arr_view_data['arr_repost_contest']       = $arr_repost_contest;

      return view('client.contests.post',$this->arr_view_data);
    }
    
    public function create(Request $request)
    {
      $arr_rules                        = array();
      $status                           = FALSE;
      $count                            = '0';

      $arr_rules['category']            = "required";
      $arr_rules['sub_category']        = "required";
      $arr_rules['contest_name']        = "required";
      //$arr_rules['contest_skills']      = "required";
      $arr_rules['cont_description']    = "required";
      $arr_rules['contest_currency']    = "required";
      $arr_rules['contest_price']       = "required";
      $arr_rules['contest_end_date']    = "required";
        //$arr_rules['contest_attachment']  = "required";
        //$arr_rules['check_terms']         = "required";
        // $arr_rules['cont_additional_info']= "required";
      $validator = Validator::make($request->all(),$arr_rules);
      if($validator->fails()){
        return redirect()->back()->withErrors($validator)->withInput($request->all());
      }
      $arr_data = array();
      $category                = $request->category;
      $sub_category            = $request->sub_category;
      $contest_name            = $request->contest_name;
      $contest_skills[]        = $request->contest_skills;
      $contest_description     = $request->cont_description;
      $contest_end_date        = $request->contest_end_date;
      $contest_price           = $request->contest_price;
      $contest_currency        = $request->contest_currency;
      $contest_additional_info = $request->cont_additional_info;
      $contest_attachment      = "";      

      $usd_contest_price = 0;
      if($contest_currency != 'USD')
      {
        $to_currency = 'USD';
        $usd_contest_price  = currencyConverterAPI($contest_currency,$to_currency,$contest_price);
        $usd_contest_price  = floor($usd_contest_price);
      }
      else
      {
        $usd_contest_price = $contest_price;
      }

      
      /*Duplication Check*/
      $arr_data['category_id']             = $category;
      $arr_data['sub_category_id']         = $sub_category;
      $arr_data['client_user_id']          = $this->user_id;
      $arr_data['contest_title']           = $contest_name;
      $arr_data['contest_description']     = $contest_description;
      $arr_data['contest_additional_info'] = $contest_additional_info;
     
      $arr_data['contest_currency']        = $contest_currency;
      $arr_data['contest_price']           = $contest_price;
      $arr_data['usd_contest_price']       = $usd_contest_price;
      $arr_data['payment_status']          = '0';
      $contest_end_date                    = '0000-00-00 00:00:00';

      if(!empty($request->contest_end_date) && $request->contest_end_date != '')
      {
          //$tmp_date = $request->contest_end_date.' '.date('H:i:s');
          $today_date = date('Y-m-d H:i:s');
          $today_date = strtotime($today_date);
          $today_date = strtotime("+".$request->contest_end_date." day", $today_date);
          $final_date =  date('Y-m-d H:i:s', $today_date);
          $contest_end_date      = $final_date;
      }

      $arr_data['contest_end_date']        = $contest_end_date;

      /*------------Client Profile Update-----------------------*/
        if($request->has('first_name')!='' && $request->has('last_name')!='')
        { 
          $arr_user_data['first_name'] = $request->input('first_name');
          $arr_user_data['last_name'] = $request->input('last_name');
          $this->UserModel->where('id',$this->user_id)->update($arr_user_data);

          if($request->has('vat_number'))
          {
            $arr_client_details_update['vat_number'] = $request->input('vat_number'); 
          }

          $arr_client_details_update['first_name'] = $request->input('first_name');
          $arr_client_details_update['last_name'] = $request->input('last_name');
          $this->ClientsModel->where('user_id',$this->user_id)->update($arr_client_details_update);

        }
        $obj_user = $this->UserModel->where('id',$this->user_id)->first();
        if($obj_user->mp_wallet_created!='Yes')
        {
          $arr_client = $obj_user->toArray();
          $arr_client['contest_currency'] = $contest_currency;
          $this->create_user_wallet($arr_client);
        }

      /*------------End of Client Profile Update-----------------------*/


      if (isset($arr_data['contest_price']) && $arr_data['contest_price'] > '0')
      {
          $admin_data = get_admin_email_address();
          if(isset($admin_data) && isset($admin_data['mp_wallet_created']) && $admin_data['mp_wallet_created'] =='No')
          {
            // send notification to admin
            $arr_admin_data                         =  [];
            $arr_admin_data['user_id']              =  $admin_data['id'];
            $arr_admin_data['user_type']            = '1';
            $arr_admin_data['url']                  = 'admin/wallet/archexpert';
            $arr_admin_data['project_id']           = '';
            $arr_admin_data['notification_text_en'] = Lang::get('controller_translations.create_wallet',[],'en','en');
            $arr_admin_data['notification_text_de'] = Lang::get('controller_translations.create_wallet',[],'de','en');
            $this->NotificationsModel->create($arr_admin_data);      
            // end send notification to admin   
            Session::flash("error",trans('controller_translations.you_dont_have_permission_to_get_project_features_because_admin_dont_have_created_his_wallet_for_transactions'));
            return redirect(url('/client/contest/post'));
          }

              /*$obj_client = $this->ClientsModel->where('user_id',$this->user_id)
                                         ->with(['user_details','country_details.states','state_details.cities','city_details'])
                                         ->first();
              if($obj_client != FALSE) {
                $arr_client_details = $obj_client->toArray();
              }

              if($arr_client_details['first_name']             == "" &&
              $arr_client_details['last_name']                 == "" &&
              $arr_client_details['user_details']['user_name'] == "")
              {
                Session::flash("error",trans('controller_translations.post_project_check_profile_completion'));
                return redirect('/client/profile');
              }
              if($arr_client_details['first_name']             == ""){
                Session::flash("error",trans('controller_translations.post_project_check_first_name_exist'));
                return redirect('/client/profile');
              }
              if($arr_client_details['last_name']              == ""){
                Session::flash("error",trans('controller_translations.post_project_check_last_name_exist'));
                return redirect('/client/profile');
              }     
              if($arr_client_details['user_type'] == 'business' && $arr_client_details['vat_number'] == ""){
                Session::flash("error",trans('controller_translations.post_project_check_vat_number_exist'));
                return redirect('/client/profile');
              }
              if($arr_client_details['user_details']['user_name']  == ""){
                Session::flash("error",trans('controller_translations.post_project_check_username_exist'));
                return redirect('/client/profile');
              }
              if($arr_client_details['country']== "" || $arr_client_details['country']== NULL){
                Session::flash("error",'Please select your country');
                return redirect('/client/profile');
              }

              $wallet_url = url('/').'/client/wallet/dashboard';
              $obj_data = $this->UserModel->where('id',$this->user_id)->first();
              $is_wallet_created = isset($obj_data->mp_wallet_created)?$obj_data->mp_wallet_created:'';
              if($is_wallet_created!='Yes')
              {
                
                Session::flash("error_sticky",trans('controller_translations.text_please_create_an').' <a href="'.$wallet_url.'">'.trans('controller_translations.text_e_wallet').'</a> '.trans('controller_translations.text_first_and_add_your_payment_method_to_be_able_to_buy_website_features_and_make_transactions'));
                return redirect(url('/client/contest/post'));
              }*/
      }

      $post_contest = $this->ContestModel->create($arr_data);

      if($post_contest)
      {
        $contest_id = isset($post_contest->id) ? $post_contest->id : 0;

        if($request->hasFile('contest_attachments'))
        {                 
          $documents  = $request->file('contest_attachments');

          foreach($documents as $key =>$doc)
          {
            $count = $this->ContestPostDocumentsModel->where('contest_id',$contest_id)->count();
    
            if($count < 10)
            { 
              if(isset($doc))
              {            
                $filename         = $doc->getClientOriginalName();
                $file_extension   = strtolower($doc->getClientOriginalExtension());
                $file_size        = $doc->getClientSize();

                if(in_array($file_extension,config('app.project_post_document_formats')) && $file_size <= 5000000)
                {
                    $file_name       = sha1(uniqid().$doc.uniqid()).'.'.$file_extension;
                    $doc->move(base_path().'/public/uploads/front/postcontest/',$file_name);
                    $obj = $this->ContestPostDocumentsModel->create(['contest_id'=>$contest_id,
                                                                'image_name'=>$file_name,
                                                                'image_original_name'=>$filename]); 
                }
              }
            }
          }
        }

        /* Inserting project skills to the table */
        if(isset($contest_skills) && count($contest_skills)>0)
        {
          $this->add_new_skills($contest_skills,$contest_id);
        }

        if(isset($post_contest->contest_price) && $post_contest->contest_price>0)
        {
          return redirect($this->module_url_path.'/contest_payment/'.base64_encode($contest_id));
        }
        Session::flash("success",trans('controller_translations.contest_posted_successfully'));
      }
      return redirect($this->module_url_path.'/posted');
    }

    public function contest_payment($contest_id=false)
    {
      $contest_id = base64_decode($contest_id);

      $contest_details = [];

      $obj_contest = $this->ContestModel->where('id',$contest_id)->first();

      if($obj_contest)
      {
        $contest_details = $obj_contest->toArray();
      }

      $currency = $contest_details['contest_currency'];
      $amount   = $contest_details['contest_price'];

      if($currency == 'JPY'){
        $amount = round($amount);
      }

      // $arr_charge = get_service_charge($amount,$currency);
      $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)->where('currency_code',$contest_details['contest_currency'])->first();
      
      if($obj_data)
      {
        $arr_data = $obj_data->toArray();
      }
      // get wallet balance //
        $mangopay_wallet           = [];
        $mangopay_wallet_details   = [];
        // foreach ($arr_data as $key => $value) 
        // {
          //$mango_data['mp_wallet_id']  = $arr_data['mp_wallet_id'];
          $get_wallet_details[] = $this->WalletService->get_wallet_details($arr_data['mp_wallet_id']);
        //}

        //$get_wallet_details        = $this->WalletService->get_wallet_details($this->mp_wallet_id);

        if(isset($get_wallet_details) && count($get_wallet_details)>0 || $get_wallet_details != false){
          $mangopay_wallet         = $get_wallet_details;
          $mangopay_wallet_details = $mangopay_wallet;
        }
      // end get wallet balance //

      $service_fee      = 0;
      $service_fee_type = 'prcentage';
      $percentage_cost  = 0; 

      $get_contest_service_fee   = $this->ContestPricingModel
                                                          ->where('status','!=','2')
                                                          ->where('slug','=','service-fee')
                                                          ->first();
      if(sizeof($get_contest_service_fee)>0)
      {
        $contest_service_fee_arr = $get_contest_service_fee->toArray();
        if(isset($contest_service_fee_arr['price']) && $contest_service_fee_arr['price'] !="" && $contest_service_fee_arr['price'] !="0")
        {
          if($contest_service_fee_arr['price_type'] == 'prcentage')
          {
            $this->arr_view_data['service_fee'] = (floatval($contest_service_fee_arr['price']) / 100) * floatval($contest_details['contest_price']);
          }
          else
          {
            $this->arr_view_data['service_fee']   = $contest_service_fee_arr['price'];
          }
        }
        if(isset($contest_service_fee_arr['price_type']) && $contest_service_fee_arr['price'] !=""){
          $this->arr_view_data['service_fee_type']   = $contest_service_fee_arr['price_type']; 
          $this->arr_view_data['percentage_cost']    = $contest_service_fee_arr['price']; 
        }
      }

      $balance       = isset($mangopay_wallet_details[0]->Balance->Amount)?$mangopay_wallet_details[0]->Balance->Amount/100:0;
      $contest_price = $contest_details['contest_price'];

      if($currency == 'JPY'){
        $balance = round($balance);
        $contest_price = round($contest_price);
        $contest_details['contest_price'] = round($contest_details['contest_price']); 
      }

      if($contest_price>$balance)
      {
         $contest_price_minus_balance = (float)$contest_price - (float)$balance;
         $new_service_charge =  get_service_charge($contest_price_minus_balance,$currency);
         if($currency == 'JPY'){
            $contest_price_minus_balance = round($contest_price_minus_balance);
            $new_service_charge = round($new_service_charge);
         }
      }
        
      // $mangopay_cards_details = $this->WalletService->get_cards_list($arr_data['mp_user_id']);

      $this->arr_view_data['page_title']                = trans('controller_translations.contest_payment');
      $this->arr_view_data['enc_contest_id']            = base64_encode($contest_id);
      $this->arr_view_data['arr_contest']               = $contest_details;
      // $this->arr_view_data['mangopay_cards_details']    = $mangopay_cards_details;
      $this->arr_view_data['contest_price_minus_balance']= isset($contest_price_minus_balance)?$contest_price_minus_balance:0;
      $this->arr_view_data['new_service_charge']         = isset($new_service_charge['service_charge'])?
                                                                 $new_service_charge['service_charge']:0;
      // $this->arr_view_data['contest_price_minus_balance']= isset($contest_price_minus_balance)?$contest_price_minus_balance:0;
      
      // $this->arr_view_data['service_charge']            = isset($arr_charge['service_charge'])?$arr_charge['service_charge']:0;
      // $this->arr_view_data['total_amount']              = isset($arr_charge['total_amount'])?$arr_charge['total_amount']:0;
      $this->arr_view_data['mangopay_wallet_details']   = $mangopay_wallet_details;
      $this->arr_view_data['module_url_path']           = $this->module_url_path;
      $this->arr_view_data['contest_currency']          = isset($contest_details['contest_currency'])?
                                                                $contest_details['contest_currency']:'';
      //dd($this->arr_view_data['service_charge'],$this->arr_view_data['new_service_charge']);

      return view('client.contests.contest_payment_methods',$this->arr_view_data);
    }


    public function update_contest_payment($contest_id=false,$new_amount)
    {
      $contest_id = base64_decode($contest_id);
      //dd($contest_id,$new_amount);
      $contest_details = [];

      $obj_contest = $this->ContestModel->where('id',$contest_id)->first();

      if($obj_contest)
      {
        $contest_details = $obj_contest->toArray();
      }
      //dd($contest_details);
      $currency = $contest_details['contest_currency'];
      $amount   = $new_amount;
      // $arr_charge = get_service_charge($amount,$currency);

      $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)->where('currency_code',$contest_details['contest_currency'])->first();
      
      if($obj_data)
      {
        $arr_data = $obj_data->toArray();
      }
      // get wallet balance //
        $mangopay_wallet           = [];
        $mangopay_wallet_details   = [];
        // foreach ($arr_data as $key => $value) 
        // {
          //$mango_data['mp_wallet_id']  = $arr_data['mp_wallet_id'];
          $get_wallet_details[] = $this->WalletService->get_wallet_details($arr_data['mp_wallet_id']);
        //}

        //$get_wallet_details        = $this->WalletService->get_wallet_details($this->mp_wallet_id);

        if(isset($get_wallet_details) && count($get_wallet_details)>0 || $get_wallet_details != false){
          $mangopay_wallet         = $get_wallet_details;
          $mangopay_wallet_details = $mangopay_wallet;
        }
      // end get wallet balance //

      $service_fee      = 0;
      $service_fee_type = 'prcentage';
      $percentage_cost  = 0; 

      $get_contest_service_fee   = $this->ContestPricingModel
                                                          ->where('status','!=','2')
                                                          ->where('slug','=','service-fee')
                                                          ->first();
      if(sizeof($get_contest_service_fee)>0)
      {
        $contest_service_fee_arr = $get_contest_service_fee->toArray();
        //dd($contest_service_fee_arr);
        if(isset($contest_service_fee_arr['price']) && $contest_service_fee_arr['price'] !="" && $contest_service_fee_arr['price'] !="0")
        {
          if($contest_service_fee_arr['price_type'] == 'prcentage')
          {
            $this->arr_view_data['service_fee'] = (floatval($contest_service_fee_arr['price']) / 100) * floatval($new_amount);
          }
          else
          {
            $this->arr_view_data['service_fee']   = $contest_service_fee_arr['price'];
          }
        }
        if(isset($contest_service_fee_arr['price_type']) && $contest_service_fee_arr['price'] !=""){
          $this->arr_view_data['service_fee_type']   = $contest_service_fee_arr['price_type']; 
          $this->arr_view_data['percentage_cost']    = $contest_service_fee_arr['price']; 
        }
      }
      // dd($mangopay_wallet_details);
      $balance       = isset($mangopay_wallet_details[0]->Balance->Amount)?$mangopay_wallet_details[0]->Balance->Amount/100:0;
      $contest_price = $new_amount;
      if($contest_price>$balance)
      {
         $contest_price_minus_balance = (float)$contest_price - (float)$balance;
         $new_service_charge =  get_service_charge($contest_price_minus_balance,$currency);
      }

      // $mangopay_cards_details = $this->WalletService->get_cards_list($arr_data['mp_user_id']);

      $this->arr_view_data['page_title']                = trans('controller_translations.contest_payment');
      $this->arr_view_data['enc_contest_id']            = base64_encode($contest_id);
      $this->arr_view_data['arr_contest']               = $contest_details;
      // $this->arr_view_data['mangopay_cards_details']    = $mangopay_cards_details;

      $this->arr_view_data['contest_price_minus_balance']= isset($contest_price_minus_balance)?$contest_price_minus_balance:0;
      $this->arr_view_data['new_service_charge']         = isset($new_service_charge['service_charge'])?
                                                                 $new_service_charge['service_charge']:0;
      // $this->arr_view_data['contest_price_minus_balance']= isset($contest_price_minus_balance)?$contest_price_minus_balance:0;
      
      $this->arr_view_data['service_charge']            = isset($arr_charge['service_charge'])?$arr_charge['service_charge']:0;
      $this->arr_view_data['new_amount']                = isset($new_amount)?$new_amount:0;

      $this->arr_view_data['total_amount']              = isset($new_amount)?$new_amount:0;
      $this->arr_view_data['mangopay_wallet_details']   = $mangopay_wallet_details;
      $this->arr_view_data['module_url_path']           = $this->module_url_path;
      $this->arr_view_data['contest_currency']          = isset($contest_details['contest_currency'])?
                                                                  $contest_details['contest_currency']:'';
    
      //dd($this->arr_view_data['service_charge'],$this->arr_view_data['new_service_charge']);
      //dd($this->arr_view_data);
      return view('client.contests.update_contest_payment_methods',$this->arr_view_data);
    }


    public function add_new_skills($contest_skills,$contest_id)
    { 
      $obj_skills = $this->ContestSkillsModel->where('contest_id',$contest_id)->get();
      if($obj_skills){
        $delete_existing_skills = $this->ContestSkillsModel->where('contest_id',$contest_id)->delete();
      }
      if(isset($contest_skills[0])){
        foreach ($contest_skills[0] as $key => $skill){   
          $this->ContestSkillsModel->create(['contest_id'=>$contest_id,'skill_id'=>$skill]);
        }       
      }
      return TRUE;
    }

    public function posted()
    { 
        $this->arr_view_data['page_title'] = 'Posted Contests';

        $obj_open_contests = $this->ContestModel->where('client_user_id',$this->user_id)
                                                ->where('is_active','0')
                                                ->where('payment_status','1')
                                                // ->whereDoesntHave('contest_entry',function($q){})
                                                ->with(['skill_details','contest_skills.skill_data','contest_entry','category_details','sub_category_details'])
                                                ->orderBy('created_at','desc')
                                                ->paginate(config('app.project.pagi_cnt'));

        $arr_open_contests = $arr_pagination = array();

        if($obj_open_contests)
        {
            $arr_pagination         = clone $obj_open_contests;
            $arr_open_contests      = $obj_open_contests->toArray();
        }

        $this->arr_view_data['arr_open_contests']     = $arr_open_contests;
        $this->arr_view_data['arr_pagination']        = $arr_pagination;
        $this->arr_view_data['module_url_path']       = $this->module_url_path;

        return view('client.contests.posted',$this->arr_view_data);
    }
    
    public function ongoing()
    {
      $this->arr_view_data['page_title'] = 'Ongoing Contests';
      $obj_ongoing_contests = $this->ContestModel->where('is_active','0')->where('contest_end_date','>=',date('Y-m-d'))
      ->with(['skill_details','contest_skills.skill_data','contest_entry'])
      ->whereHas('contest_entry',function($q){
        $q->where('is_winner','NO');
      })
      ->orderBy('created_at','DESC')
      ->where('client_user_id',$this->user_id)
      ->paginate(config('app.project.pagi_cnt')); 

      $arr_ongoing_contests = array();
      $arr_pagination    = array();
      if($obj_ongoing_contests)
      {
        $arr_pagination         = clone $obj_ongoing_contests;
        $arr_ongoing_contests      = $obj_ongoing_contests->toArray();
      }
      $this->arr_view_data['arr_ongoing_contests']     = $arr_ongoing_contests;
      $this->arr_view_data['arr_pagination']        = $arr_pagination;
      $this->arr_view_data['module_url_path']       = $this->module_url_path;
      return view('client.contests.ongoing',$this->arr_view_data);
    }
    
    public function completed()
    {
        $this->arr_view_data['page_title'] = 'Completed Contests';
        $obj_completed_contests = $this->ContestModel->where('client_user_id',$this->user_id)
        ->where('contest_status','2') //ended
        ->where(function ($q){
        $q->where('is_active','0');
        }) 
        ->with(['skill_details','contest_skills.skill_data','contest_entry'])
        ->orderBy('created_at','DESC')
        ->paginate(config('app.project.pagi_cnt')); 
        $arr_completed_contests = array();
        $arr_pagination    = array();
        if($obj_completed_contests)
        {
        $arr_pagination         = clone $obj_completed_contests;
        $arr_completed_contests      = $obj_completed_contests->toArray();
        }
        $this->arr_view_data['arr_completed_contests']     = $arr_completed_contests;
        $this->arr_view_data['arr_pagination']        = $arr_pagination;
        $this->arr_view_data['module_url_path']       = $this->module_url_path;
        return view('client.contests.completed',$this->arr_view_data);
    }

    public function expired(){

      $this->arr_view_data['page_title'] = 'Expired Contests';

      $obj_expired_contests = $this->ContestModel->where('contest_status','=','0')
                                            ->where('client_user_id',$this->user_id)
                                            ->where('is_active','2')
                                            // ->where('contest_end_date','<',date('Y-m-d'))
                                            ->with(['skill_details','contest_skills.skill_data','contest_entry'])
                                            /*->whereHas('contest_entry',function($q){
                                              $q->where('is_winner','NO');
                                            })*/
                                            ->orderBy('created_at','DESC')
                                            ->paginate(config('app.project.pagi_cnt'));
      
      $arr_expired_contests = array();
      $arr_pagination       = array();
      if($obj_expired_contests)
      {
        $arr_pagination         = clone $obj_expired_contests;
        $arr_expired_contests   = $obj_expired_contests->toArray();
      }
      $this->arr_view_data['arr_expired_contests']  = $arr_expired_contests;
      $this->arr_view_data['arr_pagination']        = $arr_pagination;
      $this->arr_view_data['module_url_path']       = $this->module_url_path;
      return view('client.contests.expired',$this->arr_view_data);
    }

    public function delete_contest($enc_id)
    {
        $contest_id        = base64_decode($enc_id);
        $obj_valid_project = $this->ContestModel->with(['contest_entry'])
                                                ->where('client_user_id','=',$this->user_id)
                                                ->where('id','=',$contest_id)
                                                ->first();
        if($obj_valid_project)
        {
            if(isset($obj_valid_project->is_active) && $obj_valid_project->is_active == '2' ) {
              $obj_valid_project->is_active = '3';
              $is_update = $obj_valid_project->save();
              if($is_update){
                  Session::flash("success",'Contest deleted successfully');
                  return redirect()->back();
              }
              Session::flash("error",trans('controller_translations.text_sorry_somthing_goes_wrong_please_try_again'));     
              return redirect()->back();
            }

            $obj_arr = $obj_valid_project->toArray();

            /*if(isset($obj_arr['contest_entry']) && count($obj_arr['contest_entry']) > 0 ){
                Session::flash("error",trans('controller_translations.text_contest_entry_no_available')); 
                return redirect()->back();
            }*/
            //dd($obj_arr);
            /*if(isset($obj_arr['contest_end_date']) && $obj_arr['contest_end_date'] < date('Y-m-d')){
                  Session::flash("error",trans('controller_translations.text_cotest_is_expired')); 
                  return redirect()->back();
            }*/

            $currency_code  = isset($obj_arr['contest_currency'])?$obj_arr['contest_currency']:'';
            $client_wallet  = get_user_wallet_details($this->user_id,$currency_code);

            $contest_price  = isset($obj_arr['contest_price'])?$obj_arr['contest_price']:'';
            $mp_contest_wallet_id = isset($obj_arr['mp_contest_wallet_id'])?$obj_arr['mp_contest_wallet_id']:'';

            $get_wallet_details      = $this->WalletService->get_wallet_details($mp_contest_wallet_id);
            
            $amount = $get_wallet_details->Balance->Amount/100;
            
            $transaction_admin_inp['tag']                      = ' Contest price return to client';
            $transaction_admin_inp['debited_UserId']           = $client_wallet['mp_user_id'];
            $transaction_admin_inp['credited_UserId']          = $client_wallet['mp_user_id'];
            $transaction_admin_inp['total_pay']                = $amount;              
            $transaction_admin_inp['debited_walletId']         = (string)$mp_contest_wallet_id; 
            $transaction_admin_inp['credited_walletId']        = (string)$client_wallet['mp_wallet_id']; 
            $transaction_admin_inp['currency_code']            = $currency_code; 
            $transaction_admin_inp['cost_website_commission']  = 0; 
            
            $pay_winner      = $this->WalletService->walletTransfer($transaction_admin_inp);
            if(isset($pay_winner->Status) && $pay_winner->Status == 'SUCCEEDED') {
                $obj_delete = $this->ContestModel
                                        ->where('id','=',$contest_id)
                                        ->update([
                                                    'is_active'=>'2',
                                                    'is_refunded'=>'1',
                                                    'refund_payment_response'=> isset($pay_winner) ? json_encode($pay_winner) : NULL,
                                                  ]);    
              if($obj_delete){
                  /* Notificatin ends */
                  Session::flash("success",'Contest cancelled successfully');
              } else {
                  Session::flash("error",trans('controller_translations.text_sorry_somthing_goes_wrong_please_try_again'));     
              }
            } else {
              Session::flash("error",'Something went wrong, Unable to refund amount. Please try again');     
            }
            
        } else {
            Session::flash("error",trans('controller_translations.text_sorry_contest_is_not_available'));
        }
        return redirect()->back();
    }

    public function editcontest($contest_id)
    {
      $arr_info             = array();
      $arr_client_details   = array();
      $arr_categories       = array();
      $arr_subcategories    = array();
      $aar_fixed_rates      = array();
      $arr_project_currency = config('app.project_currency');
      $aar_hourly_rates     = array();
      $arr_skills           = array();
      $obj_client = $this->ClientsModel->where('user_id',$this->user_id)->with(['user_details','country_details.states','state_details.cities','city_details'])->first();
      if($obj_client != FALSE){
        $arr_client_details = $obj_client->toArray();
      }
      $obj_category = $this->CategoriesModel->where('is_active','1')->get();
      if($obj_category != FALSE){
        $arr_category_details = $obj_category->toArray();
      }
      $arr_lang   =  $this->LanguageService->get_all_language();
      $obj_category = $this->CategoriesModel->get();
      if($obj_category != FALSE){
        $arr_categories = $obj_category->toArray();
      }
      $obj_skills = $this->SkillsModel->get();
      if($obj_skills != FALSE){
        $arr_skills = $obj_skills->toArray();
      }
      $id=base64_decode($contest_id);
      if($id==""){
        return redirect()->back();
      }
      $obj_post_pro = $this->ContestModel->where('id',$id)->with('contest_skills','contest_entry','contest_post_documents')->first();

      if($obj_post_pro!=FALSE){
        $arr_info = $obj_post_pro->toArray();
        if(isset($arr_info['contest_entry']) && count($arr_info['contest_entry']) > 0 ){
          Session::flash("error",trans('controller_translations.text_sorry_you_dont_have_permission_to_edit_contest')); 
          return redirect()->back();
        }
        if(isset($arr_info['contest_end_date']) && $arr_info['contest_end_date'] < date('Y-m-d')){
          Session::flash("error",trans('controller_translations.text_cotest_is_expired')); 
          return redirect()->back();
        }

        if(isset($arr_info['is_active']) && $arr_info['is_active'] == 2){
          Session::flash("error",'Deleted contest cannot be edited.'); 
          return redirect()->back();
        }

      }

      $id=base64_encode($id);
      $obj_static_privacy =  $this->StaticPageModel->where('page_slug','privacy-policy')->where('is_active',"1")->first();
      $arr_static_privacy = array();
      if($obj_static_privacy){
        $arr_static_privacy = $obj_static_privacy->toArray();
      }
      $obj_static_terms =  $this->StaticPageModel->where('page_slug','terms-of-service')->where('is_active',"1")->first();
      $arr_static_terms = array();
      if($obj_static_terms){
        $arr_static_terms = $obj_static_terms->toArray();
      }
      //$arr_contest_currency = config('app.project_currency');
      $arr_contest_currency = [];
      $obj_currency = $this->CurrencyModel->select('id','currency_code','currency','description')
                                            ->where('is_active','1')
                                            ->get();
      if($obj_currency)
      {
        $arr_contest_currency  = $obj_currency->toArray();
      }
      
      if (App::isLocale('de')){
        $aar_fixed_rates  = config('app.project_fixed_rates.de');
        $aar_hourly_rates = config('app.project_hourly_rates.de');
      } else {
        $aar_fixed_rates  = config('app.project_fixed_rates.en');
        $aar_hourly_rates = config('app.project_hourly_rates.en');
      }

      $arr_job_document_formats = array();
      $arr_job_document_formats = config('app.project_post_document_formats');

      $this->arr_view_data['id']                       = $id;
      $this->arr_view_data['arr_info']                 = $arr_info;
      $this->arr_view_data['arr_client_details']       = $arr_client_details;
      $this->arr_view_data['arr_categories']           = $arr_categories;
      $this->arr_view_data['aar_fixed_rates']          = $aar_fixed_rates;
      $this->arr_view_data['aar_hourly_rates']         = $aar_hourly_rates;
      $this->arr_view_data['arr_contest_currency']     = $arr_contest_currency;
      $this->arr_view_data['arr_skills']               = $arr_skills;
      $this->arr_view_data['arr_static_privacy']       = $arr_static_privacy;
      $this->arr_view_data['arr_static_terms']         = $arr_static_terms;
      $this->arr_view_data['page_title']               = "Update a Contest";
      $this->arr_view_data['module_url_path']          = $this->module_url_path;
      $this->arr_view_data['arr_job_document_formats'] = $arr_job_document_formats;
      return view('client.contests.edit-posted-contest',$this->arr_view_data);
    }

    public function delete_document(Request $request)
    {
      $id = $request->input('image_primary_id');

        if(isset($id) && $id!=null) 
        {
            $obj_img = $this->ContestPostDocumentsModel->where('id',$id)->first();
            $documents = isset($obj_img->image_name)?$obj_img->image_name:'';
            $status  = $this->ContestPostDocumentsModel->where('id',$id)->delete();
            $base_path = base_path().'/uploads/front/postcontest';
            if ($status) 
            {
                if (file_exists($base_path.$documents))
                {
                    @unlink($base_path.$documents);
                }
                return 'success';
            }
        }
        return 'error'; 
    }

    public function update(Request $request)
    {
      $arr_rules                            = array();
      $status                               = FALSE;
      $count                                = '0' ;
      $prev_amount = $amount_diff           = 0;
      $arr_rules['category']                = "required";
      $arr_rules['sub_category']            = "required";
      $arr_rules['contest_name']            = "required";
      //$arr_rules['contest_skills']          = "required";
      $arr_rules['cont_description']        = "required";
      // $arr_rules['contest_additional_info'] = "required";
      $arr_rules['contest_currency']        = "required";
      $arr_rules['contest_price']           = "required";
      //$arr_rules['contest_attachment']    = "required";
      $arr_rules['contest_currency']        = "required";
      
      if(isset($request->contest_end_date)){
        $arr_rules['contest_end_date']    = "required";
      } else if(isset($request->hidden_contest_end_date)){
        $arr_rules['hidden_contest_end_date']    = "required";
      }

      $validator   = Validator::make($request->all(),$arr_rules);
      if($validator->fails()){
        return redirect()->back()->withErrors($validator)->withInput($request->all());
      }

      $contest_id  = base64_decode($request->input('contest_id'));
      $arr_data    = array();
      
      $category                = $request->category;
      $sub_category            = $request->sub_category;
      $contest_name            = $request->contest_name;
      $contest_skills[]        = $request->contest_skills;
      $contest_description     = $request->cont_description;
      $contest_additional_info = $request->contest_additional_info;
      $contest_end_date        = $request->contest_end_date;
      $contest_price           = $request->contest_price;
      $contest_currency        = $request->contest_currency;

      if($request->hasFile('contest_attachments'))
      {                 
        $documents  = $request->file('contest_attachments');

        foreach($documents as $key =>$doc)
        {
          $count = $this->ContestPostDocumentsModel->where('contest_id',$contest_id)->count();
  
          if($count < 10)
          { 
            if(isset($doc))
            {            
              $filename         = $doc->getClientOriginalName();
              $file_extension   = strtolower($doc->getClientOriginalExtension());
              $file_size        = $doc->getClientSize();
            
              if(in_array($file_extension,config('app.project_post_document_formats')) && $file_size <= 5000000)
              {
                  $file_name       = sha1(uniqid().$doc.uniqid()).'.'.$file_extension;
                  $doc->move(base_path().'/public/uploads/front/postcontest/',$file_name);
                  $obj = $this->ContestPostDocumentsModel->create(['contest_id'=>$contest_id,
                                                              'image_name'=>$file_name,
                                                              'image_original_name'=>$filename]); 
              }
            }
          }
        }
      }

      /*Duplication Check*/
      $arr_data['category_id']             = $category;
      $arr_data['sub_category_id']         = $sub_category;
      $arr_data['contest_title']           = $contest_name;
      $arr_data['contest_description']     = $contest_description;
      $arr_data['contest_additional_info'] = $contest_additional_info;
      //$arr_data['contest_end_date']        = $contest_end_date;
      // $arr_data['contest_price']           = $contest_price;
      $arr_data['contest_currency']        = $contest_currency;
      $obj_prev_amount = $this->ContestModel->where('id',$contest_id)->first();
      if($obj_prev_amount)
      {
        $prev_amount = isset($obj_prev_amount->contest_price)?$obj_prev_amount->contest_price:0; 
      }
      $amount_diff = $contest_price - $prev_amount;
      if($amount_diff<0){
        $amount_diff=0;
      }
      if(isset($amount_diff) && $amount_diff>0)
      {
        //$arr_data['payment_status'] = 0;
      }

      $tmp_contest_end_date = '';
      if(isset($request->contest_end_date)){
        $tmp_contest_end_date = $request->contest_end_date;
      } else if(isset($request->hidden_contest_end_date)){
        $tmp_contest_end_date = $request->hidden_contest_end_date;
      }

      //dd($amount_diff);
      $contest_end_date      = '0000-00-00 00:00:00';

      if(!empty($obj_prev_amount->created_at) && $obj_prev_amount->created_at != '')
      {
          //$tmp_date       = $request->contest_end_date.' '.date('H:i:s');
          $today_date       = $obj_prev_amount->created_at;
          $today_date       = strtotime($today_date);
          $today_date       = strtotime("+".$tmp_contest_end_date." day", $today_date);
          $final_date       = date('Y-m-d H:i:s', $today_date);
          $contest_end_date = $final_date;
      }

      $arr_data['contest_end_date'] = $contest_end_date;
      $arr_data['edited_at'] = date('Y-m-d H:i:s');
      $arr_data['updated_at'] = isset($obj_prev_amount->updated_at) ? $obj_prev_amount->updated_at : NULL;
      
      $post_contest = $this->ContestModel->where('id',$contest_id)->update($arr_data);
      if($post_contest){
        /* Inserting project skills to the table */
        if(isset($contest_skills) && count($contest_skills)>0){
          $this->add_new_skills($contest_skills,$post_contest);
        }
        //dd($post_contest->contest_price);
        if(isset($amount_diff) && $amount_diff>0)
        {
          return redirect($this->module_url_path.'/update_contest_payment/'.base64_encode($contest_id).'/'.$amount_diff);
        }
        Session::flash("success",trans('controller_translations.contest_updated_successfully'));
      }
      return redirect()->back();
    }
    
    public function details($enc_id)
    {
      $arr_contest_entry_data = [];
      $arr_pagination         = [];
      $arr_contest_details    = [];
      $mango_wallet_details   = [];
      $contest_id = base64_decode($enc_id);
      if($contest_id)
      {
        $obj_contest_details = $this->ContestModel->where('id',$contest_id)->with(["contest_entry","user_details","category_details","contest_skills.skill_data",'sub_category_details','contest_post_documents'])->first();
        if($obj_contest_details)
        {
          $arr_contest_details = $obj_contest_details->toArray();

          $wallet_details =  $this->WalletService->get_wallet_details($arr_contest_details['mp_contest_wallet_id']);
          if(isset($wallet_details))
          {
            $mango_wallet_details         = $wallet_details;
          }
        }

        $obj_contest_entry_data = $this->ContestEntryModel->where('contest_id',$contest_id)
        ->with('contest_details','contest_entry_files','contest_entry_files_highest_rating')      
        ->orderBy('created_at','DESC')  
        ->paginate(config('app.project.pagi_cnt'));
        if($obj_contest_entry_data)
        {
          $arr_pagination         = clone $obj_contest_entry_data;
          $arr_contest_entry_data = $obj_contest_entry_data->toArray();
        } 
        if(empty($arr_contest_details))
        {
          Session::flash("error",trans('controller_translations.text_sorry_contest_is_not_available'));
          return redirect($this->module_url_path);
        }

        $this->arr_view_data['arr_contest_details']      = $arr_contest_details;
        $this->arr_view_data['arr_pagination']           = $arr_pagination;
        $this->arr_view_data['arr_contest_entry_data']   = $arr_contest_entry_data;
        $this->arr_view_data['page_title']               = "Contest Entries";
        $this->arr_view_data['module_url_path']          = $this->module_url_path;
        $this->arr_view_data['mangopay_wallet_details']     = $mango_wallet_details;
        return view('client.contests.posted_details',$this->arr_view_data);
      }
      return redirect()->back();
    }

    public function details_api(Request $request)
    {
        Session::forget('error');
        $get_data = $request->all();
        
        $is_contest_update = isset($get_data['is_contest_update']) ? $get_data['is_contest_update']:NULL;
        $transactionId     = isset($get_data['transactionId']) ? $get_data['transactionId']:NULL;
        $admin_charge      = isset($get_data['admin_charge']) ? round($get_data['admin_charge'],2):0;
        $service_cost      = isset($get_data['service_cost']) ? $get_data['service_cost']:'0';

        $payment_obj_id = isset($get_data['payment_obj_id']) ? base64_encode(trim($get_data['payment_obj_id'])) : '';
        $redirect_back_url = url('/').'/client/dashboard';
        if($payment_obj_id!=''){
            $redirect_back_url = url('/').'/client/contest/contest_payment/'.$payment_obj_id;
            if($is_contest_update == '1'){
              $redirect_back_url = url('/').'/client/contest/update_contest_payment/'.$payment_obj_id.'/'.$service_cost;
            }
        }

        $obj_contest = $this->ContestModel->where('id',$get_data['payment_obj_id'])->first();
        if($obj_contest == false) {
          Session::flash('error','Unable to fetch contest details,Please try again.');  
          return redirect($redirect_back_url);
        }

        $arr_result = $this->WalletService->viewPayIn($transactionId);

        if(isset($arr_result['status']) && $arr_result['status'] == 'error') {
          $msg = isset($arr_result['msg']) ? $arr_result['msg'] : 'Something went, wrong, Unable to process payment, Please try again.';
          Session::flash('error',$msg);  
          return redirect($redirect_back_url);
        }

        $get_wallet_details      = $this->WalletService->get_wallet_details($get_data['mp_wallet_id']);
      
        $wallet_balance = $get_wallet_details->Balance->Amount/100;

        $total_pay = $wallet_balance - $admin_charge;

        $contest_currency = isset($get_data['currency_code'])?$get_data['currency_code']:'USD'; 
        $AppWallet = NULL;

        if(isset($obj_contest->mp_contest_wallet_id) && $obj_contest->mp_contest_wallet_id!=''){
          $AppWallet     = $this->WalletService->get_wallet_details($obj_contest->mp_contest_wallet_id);
        } else {
          $AppWallet     = $this->WalletService->create_wallet_currency_wise($get_data['mp_user_id'],$get_data['project_name'].' contest wallet',$contest_currency);
        }
        
        if($AppWallet)
        {
            $admin_invoice_id = $this->_generate_invoice_id();
            $invoice_id       = $this->_generate_invoice_id();
            //$admin_details    = get_user_wallet_details('1',$contest_currency);

            $arr_project_data                           = [];
            $arr_project_data['mp_contest_wallet_id']   = isset($AppWallet->Id)?$AppWallet->Id:""; 

            $store_mp_wallet_id = $this->ContestModel->where('id',$get_data['payment_obj_id'])->update($arr_project_data); 

            $transaction_inp['tag']                      = $invoice_id.'-Contest Service Fee';
            $transaction_inp['debited_UserId']           = $get_data['mp_user_id'];

            $transaction_inp['credited_UserId']          = $get_data['mp_user_id']; 

            $transaction_inp['total_pay']                = round($total_pay,2);              
            $transaction_inp['debited_walletId']         = (string)$get_data['mp_wallet_id']; 
            $transaction_inp['credited_walletId']        = (string)$arr_project_data['mp_contest_wallet_id']; 
            $transaction_inp['currency_code']            = $contest_currency; 
            $transaction_inp['cost_website_commission']  = '0';
            $pay_fees            = $this->WalletService->walletTransfer($transaction_inp);
            
            if(isset($pay_fees->Status) && $pay_fees->Status == 'SUCCEEDED')
            {
                $contest_price = isset($obj_contest->contest_price) ? $obj_contest->contest_price : 0;
                $contest_price = $contest_price + round($total_pay,2);

                $usd_contest_price = 0;
                if($contest_currency != 'USD')
                {
                  $to_currency = 'USD';
                  $usd_contest_price  = currencyConverterAPI($contest_currency,$to_currency,$contest_price);
                  $usd_contest_price  = floor($usd_contest_price);
                }
                else
                {
                  $usd_contest_price = $contest_price;
                }

                $updated_at = isset($obj_contest_data->updated_at) ? $obj_contest_data->updated_at : NULL;

                if($is_contest_update == '1') {
                  $store_mp_wallet_id = $this->ContestModel->where('id',$get_data['payment_obj_id'])->update(['invoice_id'=>$invoice_id,'payment_status'=>'1','contest_price'=>$contest_price,'usd_contest_price'=>$usd_contest_price,'edited_at'=>date('Y-m-d H:i:s'),'updated_at' => $updated_at]);
                } else {
                  $store_mp_wallet_id = $this->ContestModel->where('id',$get_data['payment_obj_id'])->update(['invoice_id'=>$invoice_id,'payment_status'=>'1','edited_at'=>date('Y-m-d H:i:s'),'updated_at' => $updated_at]);
                }

                $arr_admin_details = get_user_wallet_details('1',$contest_currency);  
                if($arr_admin_details)
                {
                  $transaction_admin_inp['tag']                      = $admin_invoice_id.'-Contest Service Fee';
                  $transaction_admin_inp['debited_UserId']           = $get_data['mp_user_id'];           
                  $transaction_admin_inp['credited_UserId']          = $arr_admin_details['mp_user_id'];
                  $transaction_admin_inp['total_pay']                = $admin_charge;              
                  $transaction_admin_inp['debited_walletId']         = (string)$get_data['mp_wallet_id']; 
                  $transaction_admin_inp['credited_walletId']        = (string)$arr_admin_details['mp_wallet_id']; 
                  $transaction_admin_inp['cost_website_commission']  = '0';
                  $transaction_admin_inp['currency_code']            = $contest_currency;
                  $pay_admin_fees        = $this->WalletService->walletTransfer($transaction_admin_inp);
                }

                Session::flash("success",trans('controller_translations.contest_posted_successfully'));
                  return redirect(url('/').'/client/contest/posted');
                  //Redirect::to($this->payment_success_url)->send();
            }
            else
            {
              Session::flash('error','Error while creating contest.');
              return redirect($redirect_back_url);  
            }
        }
        else
        {
          Session::flash('error','Error while creating contest.');
          return redirect($redirect_back_url);
        }
    }

    public function show_contest_entry_details($enc_id)
    {
      $arr_contest_entry_data = [];
      $contest_entry_id = base64_decode($enc_id);    
      if($contest_entry_id=="")
      {
        return redirect()->back();
      }
      $obj_contest_entry_data = $this->ContestEntryModel->where('id',$contest_entry_id)
                                                        ->with('contest_details','contest_entry_files.file_rating','contest_entry_files_rating','contest_entry_original_files','expert_details','expert_details.user_details')      
                                                        ->first();
      if($obj_contest_entry_data)
      {
        $arr_contest_entry_data = $obj_contest_entry_data->toArray();
      }
      //dd($arr_contest_entry_data['expert_details']['user_details']['kyc_verified']);

      if(empty($arr_contest_entry_data))
      {
        Session::flash("error",trans('controller_translations.text_contest_entry_no_available'));
        return redirect($this->module_url_path);
      }
      $this->arr_view_data['contest_send_entry_public_file_path'] = $this->contest_send_entry_public_file_path;
      $this->arr_view_data['contest_send_entry_base_file_path']   = $this->contest_send_entry_base_file_path;
      $this->arr_view_data['arr_contest_entry_data']     = $arr_contest_entry_data;
      $this->arr_view_data['page_title']                 = trans('controller_translations.page_title_project_details');
      $this->arr_view_data['module_url_path']            = $this->module_url_path;
      return view('client.contests.contest_entry_details',$this->arr_view_data);    
    }
    
    public function store_contest_entry_file_comment(Request $request){
      $arr_rules = [];
      $arr_rules['contest_id']       = "required";
      $arr_rules['contest_entry_id'] = "required";
      $arr_rules['file_no']          = "required";
      $arr_rules['file_cmnt']        = "required";
      $validator = Validator::make($request->all(),$arr_rules);
      if($validator->fails()){
        if(!empty($validator->errors()->first()) && $validator->errors()->first() !=""){
          Session::flash("error",$validator->errors()->first());
        }
        else {
          Session::flash("error",trans('controller_translations.text_please_fill_all_mandatory_fields'));
        }
        return redirect()->back()->withErrors($validator)->withInput($request->all());
      }
      $chk_admin_deleted = $this->ContestEntryImagesModel
      ->where('contest_entry_id',$request->input('contest_entry_id'))
      ->where('file_no',$request->input('file_no'))
      ->where('is_admin_deleted','1')
      ->count();  
      if($chk_admin_deleted > 0){
        Session::flash("error",trans('controller_translations.contest_file_deleted_by_admin'));
        return redirect()->back()->with('active_file', $request->input('file_no')); 
      }
      $store_cmt_arr   = [];
      $store_cmt_arr['contest_id']       = $request->input('contest_id');
      $store_cmt_arr['contest_entry_id'] = $request->input('contest_entry_id');
      $store_cmt_arr['comment_user_id']  = $this->user_id;
      $store_cmt_arr['file_no']          = $request->input('file_no');
      $store_cmt_arr['comment']          = $request->input('file_cmnt');
      $store = $this->ContestEntryImagesCommentsModel->create($store_cmt_arr);
      if($store){
        $obj_get_client_id = $this->ContestEntryModel->where('id',$store_cmt_arr['contest_entry_id'])->select('expert_id')->first();
        if(isset($obj_get_client_id) && $obj_get_client_id != null )
        {
          $arr_client_id = $obj_get_client_id->toArray(); 
        }
        /* send notification to expert */
        $arr_noti_data                         =  [];
        $arr_noti_data['user_id']              =  isset($arr_client_id['expert_id'])?$arr_client_id['expert_id']:'';
        $arr_noti_data['user_type']            = '3';
        $arr_noti_data['url']                  = 'expert/contest/show_contest_entry_details/'.base64_encode($request->input('contest_entry_id',null));
        $arr_noti_data['project_id']           = '';
        $arr_noti_data['notification_text_en'] = Lang::get('controller_translations.text_new_comment_has_been_received_from_client',[],'en','en');
        $arr_noti_data['notification_text_de'] = Lang::get('controller_translations.text_new_comment_has_been_received_from_client',[],'de','en');
        $this->NotificationsModel->create($arr_noti_data);
        /* send notification to expert */

        Session::flash("success",trans('client/contest/common.text_your_comment_of_file').' #'.$store_cmt_arr['file_no'].' '.trans('client/contest/common.text_has_been_successfully_stored'));
        return redirect()->back()->with('active_file', $request->input('file_no')); 
      } else {
        Session::flash("error",trans('controller_translations.failed_to_save_file'));
        return redirect()->back()->with('active_file', $request->input('file_no'));  
      }
    }
    
    public function contest_entry_file_get_comments(Request $request){
      $outputdata = "";
      $comment_user_first_name = 'Unknown';
      $comment_user_last_name = '';

      $arr_rules  = [];
      $arr_rules['contest_id']       = "required";
      $arr_rules['contest_entry_id'] = "required";
      $arr_rules['file_no']          = "required";
      $validator = Validator::make($request->all(),$arr_rules);
      if($validator->fails()){
        echo 'error';
        exit;
      }
      $contest_id       = $request->input('contest_id');
      $contest_entry_id = $request->input('contest_entry_id');
      $comment_user_id  = $this->user_id;
      $file_no          = $request->input('file_no');

      $get_coments      = $this->ContestEntryImagesCommentsModel
      ->with('comment_user')      
      ->where('contest_id',$contest_id)
      ->where('contest_entry_id',$contest_entry_id)
      ->where('file_no',$file_no)
      ->orderBy('id','DESC')
      ->get();

      if(isset($get_coments) && count($get_coments)>0){
        $coments     = $get_coments->toArray();
        $outputdata .= '<h4>'.trans('client/contest/common.text_comments').' <span class="file_index">'.trans('client/contest/common.text_file').' #'.$file_no.'</span> (<span id="cmt_cnt">'.count($get_coments).'</span>)</h4>';
        $cnt     = 1;
        $showing = 5;
        foreach ($coments as $key => $cmt) {
          if(isset($cmt['comment_user']['0']['role_info']['last_name'])){$comment_user_first_name = $cmt['comment_user']['0']['role_info']['first_name']; }
          if(isset($cmt['comment_user']['0']['role_info']['last_name'])){$comment_user_last_name = str_limit($cmt['comment_user']['0']['role_info']['last_name'],1);}

          if($cnt > 5){ $display = 'style="display:none;"'; }else{ $display = 'style="display:block"'; }
          if($cnt > $showing){ $showing = $showing*2;}
          $outputdata .='<div class="comment-block show'.$showing.'" '.$display.'>
          <h5>'.$comment_user_first_name.' '.$comment_user_last_name.' <small><i>'.time_ago($cmt['created_at']).'</i></small></h5>
          <p>'.$cmt['comment'].'</p>
          </div>';
          $cnt++;
        }
        if(count($get_coments) > 5){
          $outputdata .='<div><a style="float:none;" class="black-btn shwmore" shw="10">'.trans('client/contest/common.text_view_more').'</span></a></div>';
        }
      } else {
        $outputdata .= '<h4>'.trans('client/contest/common.text_comments').' <span class="file_index">'.trans('client/contest/common.text_file').' #'.$file_no.'</span> (0)</h4>';
        $outputdata .= '<div class="comment-block">'.trans('client/contest/common.text_no_comment_on').' <span class="file_index">'.trans('client/contest/common.text_file').' #'.$file_no.'</span></div>';
      }                        
      echo $outputdata;
    }
    
    public function store_contest_entry_file_rating(Request $request)
    {
      $arr_rules = [];
      $arr_rules['rt_contest_id']       = "required";
      $arr_rules['rt_contest_entry_id'] = "required";
      $arr_rules['rt_file_no']          = "required";
      $arr_rules['file_rating']         = "required";
      $validator = Validator::make($request->all(),$arr_rules);
      if($validator->fails()){
        if(!empty($validator->errors()->first()) && $validator->errors()->first() != ""){
          Session::flash("error",$validator->errors()->first());
        } else {
          Session::flash("error",trans('controller_translations.text_please_fill_all_mandatory_fields'));
        }
        return redirect()->back()->withErrors($validator)->withInput($request->all());
      }

      $chk_admin_deleted = $this->ContestEntryImagesModel
      ->where('contest_entry_id',$request->input('rt_contest_entry_id'))
      ->where('file_no',$request->input('rt_file_no'))
      ->where('is_admin_deleted','1')
      ->count();  
      if($chk_admin_deleted > 0)
      {
        Session::flash("error",trans('controller_translations.contest_file_deleted_by_admin'));
        return redirect()->back()->with('active_file', $request->input('rt_file_no'));  
      }
      $chk_exist = $this->ContestEntryImagesRatingModel
      ->where('contest_id',$request->input('rt_contest_id'))
      ->where('contest_entry_id',$request->input('rt_contest_entry_id'))
      ->where('comment_user_id',$this->user_id)
      ->where('file_no',$request->input('rt_file_no'))
      ->first(); 

      if($chk_exist == null)
      {
        $store_rtn_arr   = [];
        $store_rtn_arr['contest_id']       = $request->input('rt_contest_id');
        $store_rtn_arr['contest_entry_id'] = $request->input('rt_contest_entry_id');
        $store_rtn_arr['comment_user_id']  = $this->user_id;
        $store_rtn_arr['file_no']          = $request->input('rt_file_no');
        $store_rtn_arr['rating']           = $request->input('file_rating');
        $store = $this->ContestEntryImagesRatingModel->create($store_rtn_arr);
        if($store)
        {
          Session::flash("success",trans('client/contest/common.text_your_rating_of').' '.trans('client/contest/common.text_file').' #'.$store_rtn_arr['file_no'].' '.trans('client/contest/common.text_has_been_successfully_stored'));
          return redirect()->back()->with('active_file', $request->input('rt_file_no'));  
        } else 
        {
          Session::flash("error",trans('controller_translations.error_unauthorized_action'));
          return redirect()->back()->with('active_file', $request->input('rt_file_no'));  
        }
      }
      else
      {
        $pre_rating = $chk_exist->toArray();
        $update_rtn_arr   = [];
        $update_rtn_arr['rating']           = $request->input('file_rating');
        $update = $this->ContestEntryImagesRatingModel->where('id',$pre_rating['id'])->update($update_rtn_arr);
        if($update){
          Session::flash("success",trans('client/contest/common.text_your_rating_of').' '.trans('client/contest/common.text_file').' #'.$request->input('rt_file_no').' '.trans('client/contest/common.text_has_been_successfully_updated'));
          return redirect()->back()->with('active_file', $request->input('rt_file_no'));  
        } else {
          Session::flash("error",trans('controller_translations.error_unauthorized_action'));
          return redirect()->back()->with('active_file', $request->input('rt_file_no')); ; 
        }
      }
    }

    public function store_rating(Request $request)
    { 
      $resp = [];
      $store_rtn_arr['contest_id']       = $request->input('contest_id');
      $store_rtn_arr['contest_entry_id'] = $request->input('contest_entry_id');
      $store_rtn_arr['comment_user_id'] = $user_id  = $this->user_id;
      $store_rtn_arr['file_no'] = $file_no = $request->input('file_no');
      $store_rtn_arr['rating']           = $request->input('rating');
      $store_rtn_arr['image_id']           = $request->input('image_id');

      $chk_admin_deleted = $this->ContestEntryImagesModel
                                ->where('contest_entry_id',$request->input('contest_entry_id'))
                                ->where('file_no',$request->input('file_no'))
                                ->where('is_admin_deleted','1')
                                ->count();  
      if($chk_admin_deleted > 0)
      {
        /*Session::flash("error",trans('controller_translations.contest_file_deleted_by_admin'));
        return redirect()->back()->with('active_file', $request->input('rt_file_no')); */ 
        $resp['status']  = 'error';
        $resp['message'] = trans('controller_translations.contest_file_deleted_by_admin');
        Session::flash('error', 'Category saved successfully.');
      }

      $result = $this->ContestEntryImagesRatingModel->updateOrCreate(['file_no'=>$file_no,'comment_user_id'=>$user_id],$store_rtn_arr);
      if($result)
      {
        $resp['status']  = 'success';
        $resp['message'] = trans('client/contest/common.text_your_rating_of').' '.trans('client/contest/common.text_file').' #'.$request->input('file_no').' '.trans('client/contest/common.text_has_been_successfully_updated');
       /* Session::flash('success', trans('client/contest/common.text_your_rating_of').' '.trans('client/contest/common.text_file').' #'.$request->input('file_no').' '.trans('client/contest/common.text_has_been_successfully_updated'));*/
      }
      else
      {
        $resp['status']  = 'error';
        $resp['message'] = trans('controller_translations.error_unauthorized_action');
        Session::flash('error',trans('controller_translations.error_unauthorized_action'));
      }

      return $resp;
    }
    
    public function choose_contest_winner($contest_id = FALSE,$contest_entry_id = FALSE)
    {
        if(empty($contest_id) || empty($contest_entry_id)){
            Session::flash("error",trans('controller_translations.error_unauthorized_action'));
            return redirect()->back();
        }

        $contest_id       = base64_decode($contest_id);
        $contest_entry_id = base64_decode($contest_entry_id);
        //dd($contest_id,$contest_entry_id);
        $chk_contest      = $this->ContestModel->where('id',$contest_id)->first();

        if($chk_contest)
        {
            $conest = $chk_contest->toArray();

            if($conest['contest_end_date'] < date('Y-m-d')) {
                Session::flash("error",trans('controller_translations.error_unauthorized_action'));
                return redirect()->back();
            }

            if($conest['winner_choose'] == 'YES') {
                Session::flash("error",trans('client/contest/common.text_this_entry_already_have_winner'));
                return redirect()->back();
            }

            return redirect(url("/client/contest").'/payment/'.base64_encode($contest_id).'/'.base64_encode($contest_entry_id));

            
        }
        else
        {
            Session::flash("error",trans('controller_translations.error_unauthorized_action'));
            return redirect()->back();
        }
    }
    
    public function payment_methods($contest_id=FALSE,$contest_entry_id = FALSE)
    {
        if ($contest_id) 
        {
            $arr_settings      = array();
            $arr_contest       = array();
            $obj_site_settings = FALSE;
            $obj_project       = FALSE;
            $contest_id        = base64_decode($contest_id);
            $contest_entry_id  = base64_decode($contest_entry_id);
            $obj_project       = $this->ContestModel->where('id',$contest_id)->first();

            if ($obj_project!=FALSE) {
                $arr_contest = $obj_project->toArray();
            }

            $obj_contest_entry = $this->ContestEntryModel->where('id',$contest_entry_id)->first();

            $expert_id  = isset($obj_contest_entry->expert_id)?$obj_contest_entry->expert_id:'0';
            $currency   = isset($arr_contest['contest_currency'])?$arr_contest['contest_currency']:'';
            
            $winner_wallet = get_user_wallet_details($expert_id,$currency);
            $debit_wallet  = get_user_wallet_details($this->user_id,$currency);
            //dd($winner_wallet);

            $contest_price = $service_fee = $percentage_cost = 0;

            if(isset($arr_contest['contest_price']) && $arr_contest['contest_price'] != ''){
                $contest_price = $arr_contest['contest_price']; 
            }


           
            // dd($arr_contest);
            $contest_currency        = isset($arr_contest['contest_currency'])?$arr_contest['contest_currency']:'';
            $contest_price           = isset($arr_contest['contest_price'])? (int) $arr_contest['contest_price']:'';
            $mp_contest_wallet_id    = isset($arr_contest['mp_contest_wallet_id'])?$arr_contest['mp_contest_wallet_id']:'';

            // dd($mp_contest_wallet_id);
            
            // get wallet balance //
            $mangopay_wallet           = [];
            $mangopay_wallet_details   = [];
           
            $get_wallet_details  = $this->WalletService->get_wallet_details($mp_contest_wallet_id);

            //dd($contest_price,$get_wallet_details->Balance->Amount/100);
            if($contest_price>$get_wallet_details->Balance->Amount/100)
            {
                Session::flash("error",'Contest wallet do not have sufficient balance');
                return redirect()->back();
            }

            $transaction_admin_inp['tag']                      = ' Contest Winner transfer';
            $transaction_admin_inp['debited_UserId']           = $debit_wallet['mp_user_id'];           
            $transaction_admin_inp['credited_UserId']          = $winner_wallet['mp_user_id'];
            $transaction_admin_inp['total_pay']                = $contest_price;              
            $transaction_admin_inp['debited_walletId']         = (string)$mp_contest_wallet_id; 
            $transaction_admin_inp['credited_walletId']        = (string)$winner_wallet['mp_wallet_id']; 
            $transaction_admin_inp['currency_code']            = $currency; 
            $transaction_admin_inp['cost_website_commission']  = 0; 

            $pay_winner      = $this->WalletService->walletTransfer($transaction_admin_inp);
            if(isset($pay_winner->Status) && $pay_winner->Status == 'SUCCEEDED')
            {
                $update = $this->ContestEntryModel->where('id',$contest_entry_id)->update(['is_winner'=>'YES']);
                if($update){
                $this->ContestModel->where('id',$contest_id)->update(['winner_choose'=>'YES','contest_status'=>'2']);
                Session::flash("success",trans('client/contest/common.text_congratualtion_you_have_choose_winner_for_this_contest'));
                return redirect()->back();
                } else {
                Session::flash("error",trans('controller_translations.error_unauthorized_action'));
                return redirect()->back();
                }
            }

        }
        return back();
    }

    public function adding_info(Request $request)
    {
      $html            = '';
      $new_information = '';
      $contest_id      = $request->input('contest_id',null);
      $contest_info    = $request->input('contest_add_info',null);
      
      if($contest_id == null){
        Session::flash("error",trans('controller_translations.text_sorry_somthing_goes_wrong_please_try_again'));
      }
      $get_info = $this->ContestModel->where('id',$contest_id)
                                      ->where('client_user_id',$this->user_id)
                                      ->select('contest_additional_info','updated_at')
                                      ->first();

      $updated_at = isset($get_info->updated_at) ? $get_info->updated_at : NULL;

      if(isset($get_info['contest_additional_info']) && $get_info['contest_additional_info'] ==null)
      {
        if($contest_info == null)
        {
          $html="false";
        }

        $save_info = $this->ContestModel->where('id',$contest_id)->where('client_user_id',$this->user_id)->update(['contest_additional_info'=>$contest_info,'edited_at'=>date('Y-m-d H:i:s'),'updated_at' => $updated_at]);
      }
      else
      {
        $old_information = isset($get_info['contest_additional_info']) ? $get_info['contest_additional_info'] : '';

        $new_information = $old_information.' '.$contest_info;
        $update_info = $this->ContestModel->where('id',$contest_id)->where('client_user_id',$this->user_id)->update(['contest_additional_info'=>$new_information,'edited_at'=>date('Y-m-d H:i:s'),'updated_at' => $updated_at]);
      }

      if($html != "false"){
        if(isset($save_info) && $save_info) {
          Session::flash("success",trans('client/contest/common.text_additional_information_added_succesfully'));
          $html = "true";
        } elseif(isset($update_info) && $update_info) {
          Session::flash("success",trans('client/contest/common.text_additional_information_updated_succesfully'));
          $html = "true";
        }else{
          Session::flash('error',trans('client/contest/common.text_Sorry_Failed_to_add_additional_information_Please_try_again'));
          $html="false";
        }
      } else{
          Session::flash('error',trans('client/contest/common.text_sorry_please_add_the_details'));
          $html="false";
        }
      echo $html;
    }

    public function subcatdata(Request $request)
    {  
        $id                = $request->id;
        $sub_category_id   = $request->sub_category_id;

        $arr_subcategories = array();
        $arr_lang          = $this->LanguageService->get_all_language();
        $obj_subcategories = $this->SubCategoriesModel->with('category_details.translations')->where('category_id',$id)->get();

        if($obj_subcategories!=FALSE)
        {
          $arr_subcategories =  $obj_subcategories->toArray();
        }  

        echo '<option value="">'.trans('client/projects/post.text_select_sub_category').'</option>';
        foreach($arr_subcategories as $arr_option){
          $id = $arr_option['id'];
          $subcat_name = $arr_option['subcategory_title'];
          $selected = '';
          if($id == $sub_category_id ){
            $selected = 'selected';
          }
          

          echo '<option '. $selected .' value="'.$id.'">'.$subcat_name.'</option>';
        }     
        exit ();
    }

    public function subcatdata_selected(Request $request)
    {  
        $id                = $request->id;
        $contest_id        = $request->contest_id;

        $arr_subcategories = array();
        $arr_lang          = $this->LanguageService->get_all_language();
        $obj_subcategories = $this->SubCategoriesModel->with('category_details.translations')->where('category_id',$id)->get();

        if($obj_subcategories!=FALSE)
        {
          $arr_subcategories =  $obj_subcategories->toArray();
        }  

        $contest_subcategory = $this->ContestModel->select('sub_category_id')->where('id',$contest_id)->first();

        $slected_contest_subcategory_id = isset($contest_subcategory->sub_category_id)?$contest_subcategory->sub_category_id:'';

        echo '<option value="">'.trans('client/projects/post.text_select_sub_category').'</option>';
        foreach($arr_subcategories as $arr_option)
        {
          $selected = '';
          $id = $arr_option['id'];
          $subcat_name = $arr_option['subcategory_title'];

          if($id == $slected_contest_subcategory_id)
          {
            $selected = 'selected';
          }
          else
          {
            $selected = '';
          }

          echo '<option value="'.$id.'" '.$selected.'>'.$subcat_name.'</option>';
        }     
        exit ();
    }

    private function _generate_invoice_id()
    {
      $secure      = TRUE;    
        $bytes       = openssl_random_pseudo_bytes(3, $secure);
        $order_token = 'INV'.date('Ymd').strtoupper(bin2hex($bytes));
        return $order_token;
    }

    public function create_user_wallet($arr_client)
    {
      $arr_currency = [];
      $obj_currency = $this->CurrencyModel->select('id','currency_code')->get();
      if($obj_currency)
      {
        $arr_currency = $obj_currency->toArray();
      }

      if(isset($arr_client) && count($arr_client)>0)
      {
        $first_name   = "-";
        $last_name    = "-";
        $email        = "-";

        $first_name   = isset($arr_client['first_name'])?$arr_client['first_name']:'';
        $last_name    = isset($arr_client['last_name'])?$arr_client['last_name']:'';
        $email        = isset($arr_client['email'])?$arr_client['email']:'';

        try{
          $timezoneinfo = file_get_contents('http://ip-api.com/json/' . $_SERVER['REMOTE_ADDR']);
        }catch(\Exception $e)
        {
          $timezoneinfo = file_get_contents('http://ip-api.com/json');  
        } 
        $mp_user_data = [];

        $mp_user_data['FirstName']          = $first_name;
        $mp_user_data['LastName']           = $last_name;
        $mp_user_data['Email']              = $email;
        $mp_user_data['CountryOfResidence'] = isset($timezoneinfo->countryCode)?$timezoneinfo->countryCode:"IN";
        $mp_user_data['Nationality']        = isset($timezoneinfo->countryCode)?$timezoneinfo->countryCode:"IN";
        
        $currency_code = isset($logged_user['currency_code'])?$logged_user['currency_code']:'USD';
        if(isset($arr_client['contest_currency']) && $arr_client['contest_currency']!='')
        {
          $currency_code = isset($arr_client['contest_currency'])?$arr_client['contest_currency']:'USD';
        }

        $mp_user_data['currency_code']      = $currency_code;

        $Mangopay_create_user               = $this->WalletService->create_user_with_all_currency_wallet($mp_user_data,$arr_currency,$arr_client['id']);
        if($Mangopay_create_user)
        {
          $this->UserModel->where('id',$this->user_id)->update(['currency_code'=>$currency_code]);
          return true;
        }
        else
        {
          return false;
        }
      }
      else
      {
        return false;
      }   
    }  

} // end class
