<?php

namespace App\Http\Controllers\Front\Client;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;
use App\Models\ProjectpostModel;
use App\Models\MilestoneReleaseModel;
use App\Common\Services\WalletService;
use App\Models\UserModel;
use App\Models\CurrencyModel;
use App\Models\UserWalletModel;


use Sentinel;
use Validator;
use Session;

class WalletDashboardController extends Controller
{
    public $arr_view_data;
    public function __construct(  
                                  ProjectpostModel $projectpost,
                                  MilestoneReleaseModel  $milestone,
                                  WalletService $WalletService, 
                                  UserModel $UserModel
                                )
    {
      $this->arr_view_data = [];
      if(! $user = Sentinel::check()) {
        return redirect('/login');
      }
      if(!$user->inRole('client')) {
        return redirect('/login'); 
      }
      $this->user                    = $user;
      $this->user_id                 = $user->id;
      $this->ProjectpostModel        = $projectpost;
      $this->MilestoneReleaseModel   = $milestone;
      $this->WalletService           = $WalletService;
      $this->UserModel               = $UserModel;
      $this->CurrencyModel           = new CurrencyModel;
      $this->UserWalletModel         = new UserWalletModel;
      $this->view_folder_path        = 'client/wallet';
      $this->module_url_path         = url("/client");
      if(isset($this->user))
      {
        $logged_user                 = $this->user->toArray();  
        $this->currency_code         = isset($logged_user['currency_code'])?$logged_user['currency_code']:'USD';
        $this->currency_symbol       = isset($logged_user['currency_symbol'])?$logged_user['currency_symbol']:'$';

        if($logged_user['mp_wallet_created'] == 'Yes')
        {
          $this->mp_user_id          = $logged_user['mp_user_id'];
          $this->mp_wallet_id        = $logged_user['mp_wallet_id'];
          $this->mp_wallet_created   = $logged_user['mp_wallet_created'];
        } 
        else 
        {
          $this->mp_user_id          = '';
          $this->mp_wallet_id        = '';
          $this->mp_wallet_created   = 'No';
        }

      } 
      else 
      {
        $this->mp_user_id          = '';
        $this->mp_wallet_id        = '';
        $this->mp_wallet_created   = 'No';
      }
    }
    public function show_dashboard(Request $request)
    { 
      $mango_user_detail                = [];
      $mangopay_wallet_details          = [];
      $mangopay_kyc_details             = [];
      $mangopay_bank_details            = [];
      $mangopay_transaction_details     = [];
      $mangopay_cards_details           = [];
      $is_blocked_country               = '0';
      $get_transaction_pagi_links_count = 0;
      
      $service_charge                   = $request->input('service_charge',null);
      $currency_code                    = $request->input('currency',null);
      $transactionId                    = $request->input('transactionId',null);
      
      //dd($service_charge,$currency_code);
      if($service_charge!=null && $currency_code!=null && $transactionId!= null)
      {
        $arr_result = $this->WalletService->viewPayIn($transactionId);
        if(isset($arr_result['status']) && $arr_result['status'] == 'error') {
          $msg = isset($arr_result['msg']) ? $arr_result['msg'] : 'Something went, wrong, Unable to process payment, Please try again.';
          Session::flash('error',$msg);  
          return redirect(url('/client/wallet/dashboard'));

        }
        $arr_data   = get_user_wallet_details($this->user_id,$currency_code);
        $admin_data = get_user_wallet_details('1',$currency_code);
        $transaction_admin['tag']                     = ' Wallet Payin ';
        $transaction_admin['debited_UserId']          = isset($arr_data['mp_user_id']) ? $arr_data['mp_user_id']:'';
        $transaction_admin['credited_UserId']         = isset($admin_data['mp_user_id']) ? $admin_data['mp_user_id']:'';  
        $transaction_admin['currency_code']           = $currency_code;
        $transaction_admin['total_pay']               = $service_charge;
        $transaction_admin['cost_website_commission'] = 0;
        $transaction_admin['debited_walletId']        = isset($arr_data['mp_wallet_id']) ? $arr_data['mp_wallet_id']:'';;
        $transaction_admin['credited_walletId']       = isset($admin_data['mp_wallet_id']) ? $admin_data['mp_wallet_id']:'';
        $admin_transfer = $this->WalletService->walletTransfer($transaction_admin);
        return redirect(url('/client/wallet/dashboard'));
      }

      if(isset($this->user))
      {
        $logged_user = $this->user->toArray(); 
        //dd($logged_user);
        $user_type = isset($logged_user['role_info']['user_type'])?$logged_user['role_info']['user_type']:'';
        if($logged_user['mp_wallet_created'] == 'Yes')
        {
          $obj_data = $this->UserWalletModel->where('user_id',$logged_user['id'])->get();
      
          if($obj_data)
          {
            $arr_data = $obj_data->toArray();
          }
          
          $mango_data['mp_user_id']          = isset($arr_data[0]['mp_user_id'])?$arr_data[0]['mp_user_id']:'';
          
          $mango_data['mp_wallet_created']   = $logged_user['mp_wallet_created'];
          $mango_user_detail                 = $this->WalletService->get_user_details($mango_data['mp_user_id']);  
          
          if(isset($mango_user_detail))
          {
              // get mangopay wallet balance 
            $get_mangopay_wallet_details = Session::get('get_mangopay_wallet_details_client');
            //dd($get_mangopay_wallet_details);
            if($get_mangopay_wallet_details==null)
            {
              foreach ($arr_data as $key => $value) 
              {
                $mango_data['mp_wallet_id']  = $value['mp_wallet_id'];
                $get_mangopay_wallet_details[] = $this->WalletService->get_wallet_details($mango_data['mp_wallet_id']);
                Session::forget('get_mangopay_wallet_details_client');
                Session::put('get_mangopay_wallet_details_client',$get_mangopay_wallet_details);
              }
            }
            
            if(isset($get_mangopay_wallet_details))
            {
              $mangopay_wallet         = $get_mangopay_wallet_details;
              $mangopay_wallet_details = $mangopay_wallet;
            }
            // end get mangopay wallet balance 
            
            // get mangopay kyc details
            $mangopay_kyc_details   = $this->WalletService->get_kyc_details($mango_data['mp_user_id']);
            $mangopay_cards_details = $this->WalletService->get_cards_list($mango_data['mp_user_id']);
            //dd($mangopay_cards_details);
            

            // end get mangopay kyc details

            $obj_user = $this->UserModel->with(['client_details'=>function($qry){
                                            $qry->select('id','user_id','country','user_type');
                                        }])->where('id',$this->user->id)
                                        ->first();

            $country_id = isset($obj_user->client_details->country)?$obj_user->client_details->country:'0';
            $user_type = isset($obj_user->client_details->user_type)?$obj_user->client_details->user_type:'0';


            $blocked_country = ['3','33','18','36','120','71','85','97','111','129','234','181','193','133','215','225','245','248','229','110','42'];

            $is_blocked_country = '0';
            if(in_array($country_id,$blocked_country))
            {
              $is_blocked_country = '1';
            }


            $is_kyc_verified = isset($obj_user->kyc_verified)?$obj_user->kyc_verified:'0';
            
            if($is_kyc_verified == 0)
            {
              $mangopay_kyc_details = (array)$mangopay_kyc_details;
              if(isset($mangopay_kyc_details))
              {
                foreach ($mangopay_kyc_details as $key => $value) 
                {
                    $arr_mangopay_kyc_details[$key] = (array) $value ;
                }
              }

              $kyc_key = 'Status';
              $kyc_val = 'VALIDATED';
              $verified = 0;

              if(isset($arr_mangopay_kyc_details) && is_array($arr_mangopay_kyc_details)){
                if(find_key_value_in_array($arr_mangopay_kyc_details, $kyc_key, $kyc_val)){
                  $verified = 1;
                }
              }

              $this->UserModel->where('id',$this->user->id)->update(['kyc_verified'=>$verified]);  
            }
            
            // get mangopay bank details;
            $mangopay_bank_details = $this->WalletService->get_banks_details($mango_data['mp_user_id']);
            // end get mangopay bank details;

            // get mangopay transaction details;

            // $mangopay_transaction_details = Session::get('mangopay_transaction_details_client');
            // if($mangopay_transaction_details==null){
            //   $get_transaction_pagi_links_count = 1;//$this->WalletService->get_wallet_transaction_pagi_links_count($mango_data['mp_wallet_id']);
            //   if(isset($_REQUEST['page_cnt']) && $_REQUEST['page_cnt'] !=""){
                
            //     foreach ($arr_data as $key => $value) {
            //     $mp_wallet_id  = $value['mp_wallet_id'];
            //     $mangopay_transaction_details[] = $this->WalletService->get_wallet_transaction_details($mp_wallet_id,$_REQUEST['page_cnt']);
            //     }

              
            //   } else {
            //   foreach ($arr_data as $key => $value) {
            //       $mp_wallet_id  = $value['mp_wallet_id'];
            //       $mangopay_transaction_details[]= $this->WalletService->get_wallet_transaction_details($mp_wallet_id,$get_transaction_pagi_links_count);
            //     }
            //   }
            // }
            // if(isset($mangopay_transaction_details) && count($mangopay_transaction_details)>0)
            // {
            //   Session::forget('mangopay_transaction_details_client');
            //   Session::put('mangopay_transaction_details_client',$mangopay_transaction_details);
            // }

            // end get mangopay transaction details;            
            $this->arr_view_data['mp_wallet_created']        = 'Yes';  
          }    
        } 
        else 
        {
          $this->arr_view_data['mp_wallet_created']          = 'No'; 
        }
      }
      //dd($mangopay_cards_details);
       $arr_country = ['AD','AE','AF','AG','AI','AL','AM','AN','AO','AQ','AR','AS','AT','AU','AW','AX','AZ','BA','BB','BD','BE','BF','BG','BH','BI','BJ','BL','BM','BN','BO','BQ','BR','BS','BT','BV','BW','BY','BZ','CA','CC','CD','CF','CG','CH','CI','CK','CL','CM','CN','CO','CR','CU','CV','CW','CX','CY','CZ','DE','DJ','DK','DM','DO','DZ','EC','EE','EG','EH','ER','ES','ET','FI','FJ','FK','FM','FO','FR','GA','GB','GD','GE','GF','GG','GH','GI','GL','GM','GN','GP','GQ','GR','GS','GT','GU','GW','GY','HK','HM','HN','HR','HT','HU','ID','IE','IL','IM','IN','IO','IQ','IR','IS','IT','JE','JM','JO','JP','KE','KG','KH','KI','KM','KN','KP','KR','KW','KY','KZ','LA','LB','LC','LI','LK','LR','LS','LT','LU','LV','LY','MA','MC','MD','ME','MF','MG','MH','MK','ML','MM','MN','MO','MP','MQ','MR','MS','MT','MU','MV','MW','MX','MY','MZ','NA','NC','NE','NF','NG','NI','NL','NO','NP','NR','NU','NZ','OM','PA','PE','PF','PG','PH','PK','PL','PM','PN','PR','PS','PT','PW','PY','QA','RE','RO','RS','RU','RW','SA','SB','SC','SD','SE','SG','SH','SI','SJ','SK','SL','SM','SN','SO','SR','SS','ST','SV','SX','SY','SZ','TC','TD','TF','TG','TH','TJ','TK','TL','TM','TN','TO','TR','TT','TV','TW','TZ','UA','UG','UM','US','UY','UZ','VA','VC','VE','VG','VI','VN','VU','WF','WS','YE','YT','ZA','ZM','ZW'];
      $this->arr_view_data['arr_country'] = $arr_country;
      
      $this->arr_view_data['mp_user_id']                              = isset($mango_data['mp_user_id'])?
                                                                              $mango_data['mp_user_id']:'';
      $this->arr_view_data['page_title']                              = 'Mangopay';
      $this->arr_view_data['currency_code']                           = $this->currency_code;
      $this->arr_view_data['user_type']                               = $user_type;
      $this->arr_view_data['is_blocked_country']                      = $is_blocked_country;
      $this->arr_view_data['mango_user_detail']                       = $mango_user_detail;
      $this->arr_view_data['mangopay_wallet_details']                 = $mangopay_wallet_details;
      $this->arr_view_data['mangopay_kyc_details']                    = $mangopay_kyc_details;
      $this->arr_view_data['mangopay_bank_details']                   = $mangopay_bank_details;
      $this->arr_view_data['mangopay_cards_details']                  = $mangopay_cards_details;
      $this->arr_view_data['mangopay_transaction_pagi_links_count']   = $get_transaction_pagi_links_count;
      $this->arr_view_data['module_url_path'] = $this->module_url_path;
      return view($this->view_folder_path.'.dashboard',$this->arr_view_data);
    }


    public function transactions()
    {
       $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)->get();
       if($obj_data)
       {
          $arr_data = $obj_data->toArray();
       }
       $get_transaction_pagi_links_count = isset($_GET['page_cnt'])?$_GET['page_cnt']:1;
       $mangopay_transaction_details = Session::get('mangopay_transaction_details_client');
        if($mangopay_transaction_details==null){
          
          foreach ($arr_data as $key => $value) {
              $mp_wallet_id  = $value['mp_wallet_id'];
              $mangopay_transaction_details[]= $this->WalletService->get_wallet_transaction_details($mp_wallet_id,$get_transaction_pagi_links_count);
            }
          }
        
        if(isset($mangopay_transaction_details) && count($mangopay_transaction_details)>0)
        {
          Session::forget('mangopay_transaction_details_client');
          Session::put('mangopay_transaction_details_client',$mangopay_transaction_details);
        }

        $get_mangopay_wallet_details = Session::get('get_mangopay_wallet_details_expert');
        if($get_mangopay_wallet_details==null)
        {
          foreach ($arr_data as $key => $value) 
          {
            $mango_data['mp_wallet_id']  = $value['mp_wallet_id'];
            $get_mangopay_wallet_details[] = $this->WalletService->get_wallet_details($mango_data['mp_wallet_id']);
          }
        }

        if(isset($get_mangopay_wallet_details)){
          $mangopay_wallet = $get_mangopay_wallet_details;
          $mangopay_wallet_details = $mangopay_wallet;
          Session::forget('get_mangopay_wallet_details_expert');
          Session::put('get_mangopay_wallet_details_expert',$get_mangopay_wallet_details);
        }
                
        //dd($mangopay_transaction_details);


        $this->arr_view_data['mangopay_wallet_details']                 = $get_mangopay_wallet_details;
        $this->arr_view_data['mangopay_transaction_pagi_links_count']   = $get_transaction_pagi_links_count;
        $this->arr_view_data['mangopay_transaction_details']            = $mangopay_transaction_details;
        //dd($this->arr_view_data);
        return view($this->view_folder_path.'.transactions',$this->arr_view_data);
       
    }


    public function withdraw()
    {

      $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)->get();
      
      if($obj_data)
      {
        $arr_data = $obj_data->toArray();
      }
      
      $mp_user_id          = isset($arr_data[0]['mp_user_id'])?$arr_data[0]['mp_user_id']:'';

      $mangopay_bank_details = $this->WalletService->get_banks_details($mp_user_id);
      
      $arr_country = ['AD','AE','AF','AG','AI','AL','AM','AN','AO','AQ','AR','AS','AT','AU','AW','AX','AZ','BA','BB','BD','BE','BF','BG','BH','BI','BJ','BL','BM','BN','BO','BQ','BR','BS','BT','BV','BW','BY','BZ','CA','CC','CD','CF','CG','CH','CI','CK','CL','CM','CN','CO','CR','CU','CV','CW','CX','CY','CZ','DE','DJ','DK','DM','DO','DZ','EC','EE','EG','EH','ER','ES','ET','FI','FJ','FK','FM','FO','FR','GA','GB','GD','GE','GF','GG','GH','GI','GL','GM','GN','GP','GQ','GR','GS','GT','GU','GW','GY','HK','HM','HN','HR','HT','HU','ID','IE','IL','IM','IN','IO','IQ','IR','IS','IT','JE','JM','JO','JP','KE','KG','KH','KI','KM','KN','KP','KR','KW','KY','KZ','LA','LB','LC','LI','LK','LR','LS','LT','LU','LV','LY','MA','MC','MD','ME','MF','MG','MH','MK','ML','MM','MN','MO','MP','MQ','MR','MS','MT','MU','MV','MW','MX','MY','MZ','NA','NC','NE','NF','NG','NI','NL','NO','NP','NR','NU','NZ','OM','PA','PE','PF','PG','PH','PK','PL','PM','PN','PR','PS','PT','PW','PY','QA','RE','RO','RS','RU','RW','SA','SB','SC','SD','SE','SG','SH','SI','SJ','SK','SL','SM','SN','SO','SR','SS','ST','SV','SX','SY','SZ','TC','TD','TF','TG','TH','TJ','TK','TL','TM','TN','TO','TR','TT','TV','TW','TZ','UA','UG','UM','US','UY','UZ','VA','VC','VE','VG','VI','VN','VU','WF','WS','YE','YT','ZA','ZM','ZW'];
      $mangopay_wallet_details = get_user_all_wallet_details();
      if (isset($mangopay_wallet_details) && count($mangopay_wallet_details)>0)
      {
        foreach ($mangopay_wallet_details as $key => $value) 
        {     
           if(isset($value->Balance->Amount) && $value->Balance->Amount > 0)
           {
             $arr_data['balance'][]  = isset($value->Balance->Amount)?$value->Balance->Amount/100:'0';
             $arr_data['currency'][] = isset($value->Balance->Currency)?$value->Balance->Currency:'';
           }
        }
      }

      $this->arr_view_data['arr_country'] = $arr_country;
      $this->arr_view_data['arr_data']    = $arr_data;
      //dd($mangopay_bank_details);
      $this->arr_view_data['mangopay_bank_details'] = $mangopay_bank_details;

      return view($this->view_folder_path.'.withdraw',$this->arr_view_data);
    }


    public function add_money_wallet(Request $request)
    {
      $transaction_inp = [];
      $user_id         = $this->user_id;
      $currency_code   = $request->input('project_currency_code');
      $amount          = $request->input('amount');
      $currency_symbol = $request->input('currency_symbol');

      $admin_data = get_user_wallet_details('1',$currency_code);

      $arr_service_charge = get_service_charge($amount,$currency_code);
      $service_charge     = isset($arr_service_charge['service_charge']) ? $arr_service_charge['service_charge'] : 0;
      $total_amount       = isset($arr_service_charge['total_amount']) ? $arr_service_charge['total_amount'] : 0;

      /*get mpuser id and wallet_id from table*/
      $obj_data = $this->UserWalletModel->where('user_id',$user_id)->where('currency_code',$currency_code)->first();
      
      if($obj_data)
      {
        $arr_data = $obj_data->toArray();
      }
      else
      {
        Session::flash('error','Please create wallet first');
        return redirect()->back();
      }
      
      if(isset($arr_data) && count($arr_data)>0)
      {
          $transaction_inp['mp_user_id']      = isset($arr_data['mp_user_id'])?$arr_data['mp_user_id']:'';
          $transaction_inp['mp_wallet_id']    = isset($arr_data['mp_wallet_id'])?$arr_data['mp_wallet_id']:'';
          $transaction_inp['ReturnURL']       = url('/client/wallet/dashboard?service_charge='.$service_charge.'&currency='.$currency_code);
          $transaction_inp['currency_code']   = $request->input('project_currency_code');//$this->currency_code;
          $transaction_inp['currency_symbol'] = $request->input('currency_symbol');//$this->currency_symbol;
      }

      $transaction_inp['amount']          = (float) $total_amount;

      $mp_add_money_in_wallet             = $this->WalletService->payInWallet($transaction_inp);

      // $transaction_admin['tag']                     = ' Wallet Payin ';
      // $transaction_admin['debited_UserId']          = isset($arr_data['mp_user_id'])?
      //                                                       $arr_data['mp_user_id']:'';
      // $transaction_admin['credited_UserId']         = isset($admin_data['mp_user_id'])?
      //                                                       $admin_data['mp_user_id']:'';  
      // $transaction_admin['currency_code']           = $currency_code;
      // $transaction_admin['total_pay']               = $service_charge;
      // $transaction_admin['cost_website_commission'] = 0;
      // $transaction_admin['debited_walletId']        = isset($arr_data['mp_wallet_id'])?
      //                                                       $arr_data['mp_wallet_id']:'';;
      // $transaction_admin['credited_walletId']       = isset($admin_data['mp_wallet_id'])?
      //                                                       $admin_data['mp_wallet_id']:'';
      // $admin_transfer = $this->WalletService->walletTransfer($transaction_admin);

      if(isset($mp_add_money_in_wallet->Status) && $mp_add_money_in_wallet->Status == 'CREATED')
      {   
          return redirect($mp_add_money_in_wallet->ExecutionDetails->RedirectURL); 
      } 
      else 
      {
        //dd($mp_add_money_in_wallet->ResultMessage);
        if(isset($mp_add_money_in_wallet) && $mp_add_money_in_wallet == false){
          Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
          return redirect()->back();
        }else{
          Session::flash('error',$mp_add_money_in_wallet->ResultMessage);
          return redirect()->back();
        }
      }
    }
    
    // public function card_registration(Request $request)
    // {
    //   $transaction_inp  = [];
    //   $user_id          = $this->user_id;
    //   $card_type        = $request->input('card_type');
    //   $currency_code    = $request->input('currency_code');
      
    // }


    public function create_user()
    {
      //Session::forget('arr_data_bal');
      $arr_currency = [];

      $obj_currency = $this->CurrencyModel->select('id','currency_code')->get();
      if($obj_currency)
      {
        $arr_currency = $obj_currency->toArray();
      }
      // dd($arr_currency);
      if(isset($this->user))
      {
        $logged_user  = $this->user->toArray();  
        //dd($logged_user);
        if(1)//(!empty($logged_user['currency_code']))
        {
          $first_name   = "-";
          $last_name    = "-";
          $email        = "-";
          if(isset($logged_user['role_info']['first_name']) && $logged_user['role_info']['first_name'] !=""){
            $first_name = $logged_user['role_info']['first_name'];
          }
          if(isset($logged_user['role_info']['last_name']) && $logged_user['role_info']['last_name'] !=""){
            $last_name = $logged_user['role_info']['last_name'];
          }
          if(isset($logged_user['email']) && $logged_user['email'] !="")
          {
            $email = $logged_user['email'];
          }
          try{
          $timezoneinfo = file_get_contents('http://ip-api.com/json/' . $_SERVER['REMOTE_ADDR']);
          }catch(\Exception $e){
          $timezoneinfo = file_get_contents('http://ip-api.com/json');  
          } 
          $mp_user_data = [];
          //Create account in mangopay and create wallet Start.......
            $mp_user_data['FirstName']          = $first_name;
            $mp_user_data['LastName']           = $last_name;
            $mp_user_data['Email']              = $email;
            $mp_user_data['CountryOfResidence'] = isset($timezoneinfo->countryCode)?$timezoneinfo->countryCode:"IN";
            $mp_user_data['Nationality']        = isset($timezoneinfo->countryCode)?$timezoneinfo->countryCode:"IN";
            // Register user on MangoPay as well
            $mp_user_data['currency_code']      = isset($logged_user['currency_code'])?$logged_user['currency_code']:'USD';
            $Mangopay_create_user               =  $this->WalletService->create_user_with_all_currency_wallet($mp_user_data,$arr_currency,$this->user->id);
            //dd($Mangopay_create_user);

            // if($Mangopay_create_user)
            // {
                Session::flash('success',trans('common/wallet/text.text_archexpert_wallet_account_has_been_created_successfully'));
                return redirect()->back();
            //}
              
              
            } 
            else
            {
              Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
              return redirect()->back();
            } 
        //Mangopay Settings End.......
        }
        else
        {
          Session::flash('error',trans('common/wallet/text.text_please_set_default_currency'));
          return redirect()->back();
        }          
      
    }   

    public function create_card(Request $request)
    {
         $preregData = $request->all();
         $this->WalletService = new WalletService;
         if(isset($preregData) && !empty($preregData))
         {
            $response_prereg = $this->WalletService->card_preregistration($preregData);
            if(isset($response_prereg['status']) && $response_prereg['status']=='CREATED')
            {
                $arr_data['AccessKeyRef']       = $response_prereg['accessKey'];
                $arr_data['data']               = $response_prereg['preregistrationData']; 
                $arr_data['AccessKey']          = $response_prereg['accessKey']; // get return acces ley
                $arr_data['cardNumber']         = $preregData['card_number']; // card number
                $arr_data['cardExpirationDate'] = $preregData['expire_month'].$preregData['expire_year']; //card expiry date
                $arr_data['cardCvx']            = $preregData['cvv_no']; //card cvv no
                $arr_data['cardRegisterId']     = $response_prereg['cardPreregistrationId']; //card cvv no
                $arr_data['returnURL']          = '';//url('/client/wallet/create_card_return'); // return url of card registration
                $arr_data['cardRegistrationURL']= $response_prereg['cardRegistrationURL'];
                //dd($arr_data);
                $curl = curl_init();

                curl_setopt_array($curl, array(
                CURLOPT_URL => "https://homologation-webpayment.payline.com/webpayment/getToken",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 300,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "accessKeyRef=".$response_prereg['accessKey']."&data=".$response_prereg['preregistrationData']."&cardNumber=".$preregData['card_number']."&cardExpirationDate=".$arr_data['cardExpirationDate']."&cardCvx=".$preregData['cvv_no']."&returnURL=",
                CURLOPT_HTTPHEADER => array(
                  "Authorization: Basic{{HEADER}}",
                  "Content-Type: application/x-www-form-urlencoded",
                  "Postman-Token: fbf9a7c2-be81-43b1-a000-9a4cc564a5c8",
                  "cache-control: no-cache"
                ),
              ));
              $response = curl_exec($curl);
              $err      = curl_error($curl);
              curl_close($curl);
              if ($err) 
              {
                echo "cURL Error #:" . $err;
              }

              $arr_final_data['data']           = $response;
              $arr_final_data['cardRegisterId'] = $response_prereg['cardPreregistrationId'];

              $pre_reg_data = $this->WalletService->card_preregistration_data($arr_final_data);
              
              if(isset($pre_reg_data) && $pre_reg_data['Status'] == 'VALIDATED')
              {
                  Session::flash('success','Card registered successfully.');
                  return redirect(url('/client/wallet/dashboard'));
              }
              else
              {
                  Session::flash('error','Something went wrong.');
                  return redirect(url('/client/wallet/dashboard'));
              }

            }
            else
            {
              Session::flash('error','Something went wrong,please try again');
              return redirect(url('/client/wallet/dashboard'));
            }
         }
    }


    public function add_bank(Request $request){
      $transaction_inp               = [];
      $bank_type    = $request->input('bank_type');

      if($bank_type == 'IBAN'){
        $validator  = Validator::make($request->all(),[
            'FirstName'             => 'required',
            'LastName'              => 'required',
            'Country'               => 'required',
            'City'                  => 'required',
            'Region'                => 'required',
            'Address'               => 'required',
            'PostalCode'            => 'required',
            'IBAN'                  => 'required',
            'BIC'                   => 'required'
        ]);
      } else if($bank_type == 'GB'){
        $validator = Validator::make($request->all(),[
            'FirstName'             => 'required',
            'LastName'              => 'required',
            'City'                  => 'required',
            'PostalCode'            => 'required',
            'Region'                => 'required',
            'Country'               => 'required',
            'gbSortCode'            => 'required',
            'Address'               => 'required',
            'gbAccountNumber'       => 'required'
        ]);
      } else if($bank_type == 'US'){
        $validator = Validator::make($request->all(),[
            'FirstName'             => 'required',
            'LastName'              => 'required',
            'City'                  => 'required',
            'PostalCode'            => 'required',
            'Region'                => 'required',
            'Country'               => 'required',
            'Address'               => 'required',
            'usAccountNumber'       => 'required',
            'usDepositAccountType'  => 'required',
            'usABA'                 => 'required'
        ]);
      } else if($bank_type == 'CA'){
        $validator = Validator::make($request->all(),[
            'FirstName'            => 'required',
            'LastName'             => 'required',
            'City'                 => 'required',
            'PostalCode'           => 'required',
            'Region'               => 'required',
            'Country'              => 'required',
            'Address'              => 'required',
            'caAccountNumber'      => 'required',
            'caBranchCode'         => 'required',
            'caBankName'           => 'required',
            'caInstitutionNumber'  => 'required'
        ]);
      } else if($bank_type == 'OTHER'){
        $validator = Validator::make($request->all(),[
            'FirstName'            => 'required',
            'LastName'             => 'required',
            'PostalCode'           => 'required',
            'Address'              => 'required',
            'Region'               => 'required',
            'City'                 => 'required',
            'Country'              => 'required',
            'otherAccountNumber'   => 'required',
            'otherBIC'             => 'required'
        ]);
      } 
      if($validator->fails()){
        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
        return redirect('/client/wallet/dashboard');
      }

      $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)->get();
      
      if($obj_data)
      {
        $arr_data = $obj_data->toArray();
      }


      $transaction_inp['UserId']     = isset($arr_data[0]['mp_user_id'])?$arr_data[0]['mp_user_id']:'';
      $transaction_inp['FirstName']  = $request->input('FirstName');
      $transaction_inp['LastName']   = $request->input('LastName');
      $transaction_inp['Country']    = $request->input('Country');
      $transaction_inp['PostalCode'] = $request->input('PostalCode');
      $transaction_inp['City']       = $request->input('City');
      $transaction_inp['bank_type']  = $request->input('bank_type');
      $transaction_inp['Address']    = $request->input('Address');
      $transaction_inp['Region']     = $request->input('Region');

      if($bank_type == 'IBAN'){
        $transaction_inp['IBAN']                 = $request->input('IBAN');
        $transaction_inp['BIC']                  = $request->input('BIC');
      } else if($bank_type == 'GB'){
        $transaction_inp['gbSortCode']           = $request->input('gbSortCode');
        $transaction_inp['gbAccountNumber']      = $request->input('gbAccountNumber');       
      } else if($bank_type == 'US'){
        $transaction_inp['usAccountNumber']      = $request->input('usAccountNumber');
        $transaction_inp['usDepositAccountType'] = $request->input('usDepositAccountType');
        $transaction_inp['usABA']                = $request->input('usABA');       
      } else if($bank_type == 'CA'){
        $transaction_inp['caAccountNumber']      = $request->input('caAccountNumber');
        $transaction_inp['caBranchCode']         = $request->input('caBranchCode');
        $transaction_inp['caBankName']           = $request->input('caBankName');
        $transaction_inp['caInstitutionNumber']  = $request->input('caInstitutionNumber');       
      } else if($bank_type == 'OTHER'){
        $transaction_inp['otherAccountNumber']   = $request->input('otherAccountNumber');
        $transaction_inp['otherBIC']             = $request->input('otherBIC');     
      }
      $mp_add_bank = $this->WalletService->createBankAccount($transaction_inp);
      if(isset($mp_add_bank) && $mp_add_bank == 'true'){
        Session::flash('success',trans('common/wallet/text.text_bank_has_been_successfully_registered'));
        return redirect('/client/wallet/dashboard');
      } else{
         if(isset($mp_add_bank) && $mp_add_bank != ""){
          Session::flash('error',$mp_add_bank);
         } else {
         Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
         }
         return redirect('/client/wallet/dashboard');
      }
    }
    public function create_wallet_using_mp_owner_id($mp_owners_id = false){
      if($mp_owners_id == false){
        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
        return redirect()->back();
      }
      $desciption = config('app.project.name')." App wallet";
      $AppWallet = $this->WalletService->create_wallet(base64_decode($mp_owners_id),$desciption);
      $arr_data['mp_wallet_id']      = isset($AppWallet->Id)?$AppWallet->Id:"";
      $arr_data['mp_wallet_created'] = "Yes";
      if($AppWallet){
        $update_mangopya_details = $this->UserModel->where('id',$this->user->id)->update($arr_data); 
        Session::flash('success',trans('common/wallet/text.text_archexpert_wallet_account_has_been_created_successfully'));
      } else {
        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
      }
      return redirect()->back();
    } 

    public function cashout_request(Request $request,$bank_id)
    { 
      
      $mangopay_wallet_details = get_user_all_wallet_details();
      if (isset($mangopay_wallet_details) && count($mangopay_wallet_details)>0)
      {
        foreach ($mangopay_wallet_details as $key => $value) 
        {     
           if(isset($value->Balance->Amount) && $value->Balance->Amount > 0)
           {
             $arr_data['balance'][]  = isset($value->Balance->Amount)?$value->Balance->Amount/100:'0';
             $arr_data['currency'][] = isset($value->Balance->Currency)?$value->Balance->Currency:'';
           }
        }
      }
      //$this->arr_view_data['arr_country'] = $arr_country;
      $this->arr_view_data['arr_data']    = $arr_data;
      
      $this->arr_view_data['bank_id'] = $bank_id;

      return view($this->view_folder_path.'.payout',$this->arr_view_data);

        
    }


    public function make_cashout_in_bank(Request $request)
    {
        $validator = Validator::make($request->all(),[
          'act_cashout_amount' => 'required',
          'walletId'           => 'required',
          'bankId'             => 'required'
          
        ]);
        if($validator->fails()){
          Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
          return redirect()->back();
        }
        $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)->get();
      
        if($obj_data)
        {
          $arr_data = $obj_data->toArray();
        }
        $act_cashout_amount = $request->input('act_cashout_amount');
        $walletId           = $request->input('walletId');
        $bankId             = $request->input('bankId');
        $tag                = $request->input('tag',null);
        //  check wallet balance 
        $get_mangopay_wallet_details = $this->WalletService->get_wallet_details($walletId);
        if(isset($get_mangopay_wallet_details->Balance->Amount) && $get_mangopay_wallet_details->Balance->Amount !=""){
            $wallet_balance = $get_mangopay_wallet_details->Balance->Amount/100;
            if($act_cashout_amount > $wallet_balance) {
              Session::flash('error',trans('common/wallet/text.text_wallet_balance_is_not_sufficient'));
              return redirect('/client/wallet/dashboard');
            } else {
              $transaction_inp                      = [];
              $transaction_inp['UserId']            = isset($arr_data[0]['mp_user_id'])?$arr_data[0]['mp_user_id']:'';
              $transaction_inp['bankId']            = $bankId;
              $transaction_inp['tag']               = $tag;
              $transaction_inp['commission_amount'] = '0';
              $transaction_inp['walletId']          = $walletId;
              $transaction_inp['total_pay']         = $act_cashout_amount;
              $payOutBankWire  = $this->WalletService->payOutBankWire($transaction_inp);
              if(isset($payOutBankWire->Id) && $payOutBankWire->Id != ""){
                Session::flash('success', trans('common/wallet/text.text_your_cashout_has_been_successfully_done_You_can_check_your_transaction_status_using')." ( ".$payOutBankWire->Id." ) ".trans('common/wallet/text.text_transaction_id'));
                return redirect('/client/wallet/dashboard');
              } else {
                Session::flash('error',$payOutBankWire);
                return redirect()->back();
              }
            }
        } else {
          Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
          return redirect('/client/wallet/dashboard');
        }
    } 

    public function upload_kyc_docs(Request $request)
    {
      $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)->get();
      if($obj_data)
      {
        $arr_data = $obj_data->toArray();
      }
      
      $this->mp_user_id = isset($arr_data[0]['mp_user_id'])?$arr_data[0]['mp_user_id']:'';
      $transaction_inp          = [];

      $validator = Validator::make($request->all(),[
          'user_type'  => 'required',
          'id_proof'   => 'required',
      ]);
      if($validator->fails())
      {
        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
        return redirect('/client/wallet/dashboard');
      }

      $user_type = $request->input('user_type');
      if(isset($user_type) && $user_type!='' && $user_type == 'business')
      {
        $user_type = '1'; //business
      }
      else
      {
        $user_type = '2'; //private
      }

      if(isset($user_type) && $user_type!='' && $user_type == '1')
      {
        $transaction['mp_user_id']       = $this->mp_user_id;
        $transaction['KycDocumentType']  = 'ADDRESS_PROOF';
        $transaction['KycDocument']      = base64_encode(file_get_contents($request->file('reg_proof')));
        $transaction['KycDocumentPath']  = $request->file('reg_proof')->getrealPath();
        $transaction['KycDocumentName']  = $request->file('reg_proof')->getClientOriginalName();

        $mp_upload_kyc_doc1 = $this->WalletService->submit_kyc_docs($transaction);
      }

      $transaction_inp['mp_user_id']       = $this->mp_user_id;
      $transaction_inp['KycDocumentType']  = 'IDENTITY_PROOF';
      $transaction_inp['KycDocument']      = base64_encode(file_get_contents($request->file('id_proof')));
      $transaction_inp['KycDocumentPath']  = $request->file('id_proof')->getrealPath();
      $transaction_inp['KycDocumentName']  = $request->file('id_proof')->getClientOriginalName();
      $mp_upload_kyc_doc = $this->WalletService->submit_kyc_docs($transaction_inp);


      if(isset($mp_upload_kyc_doc) && $mp_upload_kyc_doc == true){
        Session::flash('success',trans('common/wallet/text.text_your_kyc_details_submited_successfully'));
        return redirect('/client/wallet/dashboard');
      } else {
        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
        return redirect('/client/wallet/dashboard');
      }
    }

    public function old_upload_kyc_docs(Request $request)
    {
      $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)->get();
      if($obj_data)
      {
        $arr_data = $obj_data->toArray();
      }
      
      $this->mp_user_id = isset($arr_data[0]['mp_user_id'])?$arr_data[0]['mp_user_id']:'';

      $transaction_inp                          = [];
      $validator = Validator::make($request->all(),[
          'kyc_upload_documents_natural_types'  => 'required',
          'Kyc_doc'                             => 'required'
      ]);
      if($validator->fails()){
        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
        return redirect('/client/wallet/dashboard');
      }
      $transaction_inp['mp_user_id']       = $this->mp_user_id;
      $transaction_inp['KycDocumentType']  = $request->input('kyc_upload_documents_natural_types');
      $transaction_inp['KycDocument']      = base64_encode(file_get_contents($request->file('Kyc_doc')));
      $transaction_inp['KycDocumentPath']  = $request->file('Kyc_doc')->getrealPath();
      $transaction_inp['KycDocumentName']  = $request->file('Kyc_doc')->getClientOriginalName();
      $mp_upload_kyc_doc = $this->WalletService->submit_kyc_docs($transaction_inp);
      if(isset($mp_upload_kyc_doc) && $mp_upload_kyc_doc == true){
        Session::flash('success',trans('common/wallet/text.text_your_kyc_details_submited_successfully'));
        return redirect('/client/wallet/dashboard');
      } else {
        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
        return redirect('/client/wallet/dashboard');
      }
    }
    public function payin_refund(Request $request){
      $validator = Validator::make($request->all(),[
        'act_refund_amount' => 'required',
        'walletId'          => 'required',
        'authorId'          => 'required',
        'currency'          => 'required',
        'payinId'           => 'required'
      ]);
      if($validator->fails()){
        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
        return redirect('/client/wallet/dashboard');
      }

      $act_refund_amount  = $request->input('act_refund_amount');
      $walletId           = $request->input('walletId');
      $authorId           = $request->input('authorId');
      $payinId            = $request->input('payinId');
      $tag                = $request->input('tag',null);
      $currency           = $request->input('currency',null);

      // check wallet balance 
        $get_mangopay_wallet_details = $this->WalletService->get_wallet_details($walletId);
        if(isset($get_mangopay_wallet_details->Balance->Amount) && $get_mangopay_wallet_details->Balance->Amount !=""){
            $wallet_balance = $get_mangopay_wallet_details->Balance->Amount/100;
            /*if($act_refund_amount > $wallet_balance) {
              Session::flash('error',trans('common/wallet/text.text_wallet_balance_is_not_sufficient'));
              return redirect('/client/wallet/dashboard');
            } else {*/
              $transaction_inp                       = [];
              $transaction_inp['act_refund_amount']  = $act_refund_amount;
              $transaction_inp['walletId']           = $walletId;
              $transaction_inp['tag']                = $tag;
              $transaction_inp['authorId']           = $authorId;
              $transaction_inp['currency']           = $currency;
              $transaction_inp['payinId']            = $payinId;
              $payinRefund  = $this->WalletService->payinRefund($transaction_inp);
              if(isset($payinRefund->Id) && $payinRefund->Id != ""){
                Session::flash('success', trans('common/wallet/text.text_your_refund_request_has_been_successfully_sent_Please_check_your_trasaction_status_using')." ( ".$payinRefund->Id." ) ".trans('common/wallet/text.text_transaction_id'));
                return redirect('/client/wallet/dashboard');
              } else {
                Session::flash('error',$payOutBankWire);
                return redirect('/client/wallet/dashboard');
              }
            /*}*/
        } else {
          Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
          return redirect('/client/wallet/dashboard');
        }
    } 

    public function update_user_details(Request $request)
    {
      $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)->get();
      if($obj_data)
      {
        $arr_data = $obj_data->toArray();
      }
      
      $this->mp_user_id = isset($arr_data[0]['mp_user_id'])?$arr_data[0]['mp_user_id']:'';

       $validator = Validator::make($request->all(),[
          'nationality' => 'required',
          'residence'   => 'required'
        ]);
        if($validator->fails()){
          Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
          return redirect('/client/wallet/dashboard');
        }
        $nationality = $request->input('nationality');
        $residence = $request->input('residence');

        $user_details['mp_user_id']  = $this->mp_user_id;
        $user_details['nationality'] = $nationality;
        $user_details['residence']   = $residence;

        $upadte_user_details         = $this->WalletService->update_user($user_details);

        if(isset($upadte_user_details) && $upadte_user_details == true)
        {
          Session::flash('success','User details updated successfully.');
          return redirect('/client/wallet/dashboard');
        } 
        else 
        {
          Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
          return redirect('/client/wallet/dashboard');
        }
    }
} // end class