<?php

namespace App\Http\Controllers\Front\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\ProjectpostModel;
use App\Models\MilestoneReleaseModel;
use App\Models\ContestModel;
use App\Common\Services\WalletService;

use Sentinel;
use Validator;
use Session;

class DashboardController extends Controller
{
    public $arr_view_data;
    public function __construct(  
                                  ProjectpostModel $projectpost,MilestoneReleaseModel  $milestone,WalletService $WalletService,ContestModel $ContestModel
                                )
    {
      $this->arr_view_data = [];

      if(! $user = Sentinel::check()) 
      {
        return redirect('/login');
      }

      if(!$user->inRole('client')) 
      {
        return redirect('/login'); 
      }

      $this->user                  = Sentinel::check();
      $this->user_id = $user->id;

      $this->ContestModel          = $ContestModel;
      $this->ProjectpostModel      = $projectpost;
      $this->MilestoneReleaseModel = $milestone;
      $this->WalletService         = $WalletService;
      $this->view_folder_path      = 'client/dashboard';
      $this->module_url_path       = url("/client");
    }

    /* 
      Auther : Nayan S.
    */

    public function show_dashboard()
    {   
        /*Finding Ongoing projects count*/
        $logged_user                = isset($this->user)?$this->user->toArray():[];
        $mangopay_wallet_details    = [];
        $arr_data                   = [];
        if($logged_user['mp_wallet_created'] == 'Yes')
        {
          $mango_data['mp_user_id']          = $logged_user['mp_user_id'];
          $mango_data['mp_wallet_id']        = $logged_user['mp_wallet_id'];
          $mango_data['mp_wallet_created']   = $logged_user['mp_wallet_created'];
          $mango_user_detail                 = $this->WalletService->get_user_details($mango_data['mp_user_id']);
          if(isset($mango_user_detail)){
            // get mangopay wallet balance 
              $get_mangopay_wallet_details = $this->WalletService->get_wallet_details($mango_data['mp_wallet_id']);
              if(isset($get_mangopay_wallet_details)){
                $mangopay_wallet         = $get_mangopay_wallet_details;
                $mangopay_wallet_details = $mangopay_wallet;
              }
          }
        }
        $count_ongoing_projects = $this->ProjectpostModel->where('client_user_id',$this->user_id)
                                                       ->where('project_status','=','4')
                                                       ->count(); 

        /*Finding Completed projects count*/
        $count_completed_projects = $this->ProjectpostModel->where('client_user_id',$this->user_id)
                                                        ->where('project_status','=','3')
                                                        ->count();    
        /*Finding Posted projects count*/
        $count_posted_projects   = $this->ProjectpostModel->where('client_user_id',$this->user_id)
                                                       ->where( function ($q) 
                                                        {
                                                          $q->where('project_status','1');
                                                          $q->orWhere('project_status','2');
                                                        })
                                                       ->count();
        /*Finding posted contest count */ 
        $count_posted_contests = $this->ContestModel->where('contest_status','=','0')
                                                    ->where('client_user_id',$this->user_id)
                                                    ->where('is_active','0')
                                                    ->where('contest_end_date','>=',date('Y-m-d'))
                                                    ->whereDoesntHave('contest_entry',function($q){})
                                                    ->count();
        /*Finding ongoing contest count */
        $count_ongoing_contests = $this->ContestModel->where('contest_status','=','0')
                                                      ->where('client_user_id',$this->user_id)
                                                      ->where('is_active','0')
                                                      ->where('contest_end_date','>=',date('Y-m-d'))
                                                      ->whereHas('contest_entry',function($q){})
                                                      ->count();
                                                
        $count_milestone_release   = $this->MilestoneReleaseModel->where('client_user_id',$this->user_id)
                                                       ->where( function ($q) 
                                                         {
                                                           $q->Where('status','0');
                                                         })
                                                       ->count();      

      $posted_contests_cnt  = isset($count_posted_contests)?$count_posted_contests:0;
      $ongoing_contests_cnt = isset($count_ongoing_contests)?$count_ongoing_contests:0;
      $total_contest_cnt    = $posted_contests_cnt + $ongoing_contests_cnt;

      $mangopay_wallet_details = get_user_all_wallet_details();
      if (isset($mangopay_wallet_details) && count($mangopay_wallet_details)>0)
      {
        foreach ($mangopay_wallet_details as $key => $value) 
        {     
           if(isset($value->Balance->Amount) && $value->Balance->Amount > 0)
           {
             $arr_data['balance'][]  = isset($value->Balance->Amount)?$value->Balance->Amount/100:'0';
             $arr_data['currency'][] = isset($value->Balance->Currency)?$value->Balance->Currency:'';
           }
        }
      }

      $this->arr_view_data['arr_data'] = $arr_data;

      $this->arr_view_data['wallet_amount']            = isset($mangopay_wallet_details->Balance->Amount)?$mangopay_wallet_details->Balance->Amount:0;
      $this->arr_view_data['amount_currency']          = isset($mangopay_wallet_details->Balance->Currency)?$mangopay_wallet_details->Balance->Currency:'N/A';
      $this->arr_view_data['page_title']               = trans('controller_translations.page_title_dashboard');
      //projects/jobs count starts
      $this->arr_view_data['count_ongoing_projects']   = isset($count_ongoing_projects)?$count_ongoing_projects:0;
      $this->arr_view_data['count_completed_projects'] = isset($count_completed_projects)?$count_completed_projects:0;
      $this->arr_view_data['count_posted_projects']    = isset($count_posted_projects)?$count_posted_projects:0;
      $this->arr_view_data['count_milestone_release']  = isset($count_milestone_release)?$count_milestone_release:0;
      //projects/jobs count ends
      //Contest count starts
      $this->arr_view_data['count_total_contests']     = isset($total_contest_cnt)?$total_contest_cnt:0;
      //Contest count ends
      /*$this->arr_view_data['count_open_projects']    = $count_open_projects;*/
      $this->arr_view_data['module_url_path']          = $this->module_url_path;
      return view($this->view_folder_path.'.dashboard',$this->arr_view_data);
    }

    public function notifications(Request $request)
    {
        //$this->user

        $arr_notifications = [];

        dd($user->toArray());

        $this->arr_view_data['arr_notifications'] = $arr_notifications;
        
        return view('front.home.subscription_plans',$this->arr_view_data);

    }
}