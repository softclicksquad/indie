<?php
namespace App\Http\Controllers\Front\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\UserModel;
use App\Models\ClientsModel;
use App\Models\ProjectpostModel;
use App\Models\NotificationsModel;
use App\Models\SiteSettingModel;
use App\Models\ProjectAttchmentModel;
use App\Common\Services\MailService;
use App\Models\ExpertsModel;
/*Used helper method to fire event*/
use Sentinel;
use Validator;
use Session;
use Mail;
use App;
use Lang;
use Zipper;
class ProjectCollaborationController extends Controller
{
    public $arr_view_data;
    public function __construct(ProjectpostModel $projectpost,
                                UserModel $user_model,
                                ClientsModel $clients,
                                NotificationsModel $notifications,
                                SiteSettingModel $site_settings,
                                ProjectAttchmentModel $project_attachment,
                                ExpertsModel $experts,
                                MailService $mail_service
                                )
    { 
      if(! $user = Sentinel::check()) {
        return redirect('/login');
      }
      $this->user_id                               = $user->id;
      $this->UserModel                             = $user_model;
      $this->ClientsModel                          = $clients;
      $this->ProjectpostModel                      = $projectpost;
      $this->NotificationsModel                    = $notifications;
      $this->SiteSettingModel                      = $site_settings;
      $this->ProjectAttchmentModel                 = $project_attachment;
      $this->MailService                           = $mail_service;
      $this->ExpertsModel                          = $experts;


      $this->arr_view_data                         = [];
      $this->module_url_path                       = url("/client/projects");
      $this->login_url                             = url("/login");
      $this->profile_img_base_path                 = base_path() . '/public'.config('app.project.img_path.profile_image');
      $this->project_attachment_public_path        = url('/').config('app.project.img_path.project_attachment');
      $this->expert_project_attachment_public_path = url('/').config('app.project.img_path.expert_project_attachment');
    }

    public function index($project_id=FALSE) {
      $this->arr_view_data['page_title']  = trans('controller_translations.page_title_project_collaboration');
      if($project_id == "" || $project_id == FALSE){
        Session::flash('error', trans('controller_translations.something_went_wrong_please_try_again_later')); 
        return redirect()->back();
      }
      $project_main_attachment        = [];
      $project_attachment             = [];
      $arr_pagination                 = [];
      $project_id                     = base64_decode($project_id); 
      $obj_project_main_attachment    = $this->ProjectpostModel
                                             ->where('id',$project_id)
                                             //->where('project_status','4') // ongoing
                                             //->orWhere('project_status','3') // completed
                                             ->first(['id','project_name','project_status','project_attachment']);
      if(isset($obj_project_main_attachment) && $obj_project_main_attachment != null){
        $project_main_attachment = $obj_project_main_attachment->toArray(); 
      }
      // project attachments 
      $obj_project_attachment    = $this->ProjectAttchmentModel
                                       ->where('project_id',$project_id)
                                       ->orderBy('created_at','DESC') 
                                       ->paginate(12);  
      if($obj_project_attachment) {
        $arr_pagination          = clone $obj_project_attachment;
        $project_attachment      = $obj_project_attachment->toArray();
      }
      // end project attachments 
      $this->arr_view_data['project_main_attachment'] = $project_main_attachment;
      $this->arr_view_data['project_id']              = $project_id;
      $this->arr_view_data['project_attachment']      = $project_attachment;
      $this->arr_view_data['arr_pagination']          = $arr_pagination;
      $this->arr_view_data['module_url_path']         = $this->module_url_path;
      return view('client.projects.collaboration.index',$this->arr_view_data);
    }

    public function details($collaboration_id=FALSE,$project_id=FALSE) {
      $this->arr_view_data['page_title']  = trans('controller_translations.page_title_project_collaboration_details');
      if($collaboration_id == "" || $project_id == FALSE){
        Session::flash('error', trans('controller_translations.something_went_wrong_please_try_again_later')); 
        return redirect()->back();
      }
      $project_attachment         = [];
      $previous                   = 0;
      $next                       = 0;
      $collaboration_id           = base64_decode($collaboration_id); 
      $project_id                 = base64_decode($project_id); 

      // project attachments 
        $obj_project_attachment  = $this->ProjectAttchmentModel->with(['project_details'=> function($q){
                                           //$q->where('project_status','4'); // ongoing
                                           //$q->orWhere('project_status','3'); // completed
                                        }]) 
                                        ->where('project_id',$project_id)
                                        ->where('id',$collaboration_id)
                                        ->orderBy('created_at','DESC') 
                                        ->first();  
        if($obj_project_attachment)  {
          $project_attachment   = $obj_project_attachment->toArray();
        }
      // end project attachments 

      // get previous 
        $obj_get_previous   = $obj_project_attachment->where('id', '<', $collaboration_id)->where('project_id', $project_id)->max('id');
        if($obj_get_previous)  {
          $previous         = $obj_get_previous;
        }
      // end get previous 
      // get next 
        $obj_get_next   = $obj_project_attachment->where('id', '>', $collaboration_id)->where('project_id', $project_id)->min('id');
        if($obj_get_next)  {
          $next         = $obj_get_next;
        }
      // end get next   

      $this->arr_view_data['collaboration_id']        = $collaboration_id;
      $this->arr_view_data['project_attachment']      = $project_attachment;
      $this->arr_view_data['previous']                = $previous;
      $this->arr_view_data['next']                    = $next;
      $this->arr_view_data['module_url_path']         = $this->module_url_path;
      $this->arr_view_data['project_id']              = isset($project_attachment['project_id'])?$project_attachment['project_id']:'0';
      $this->arr_view_data['profile_img_base_path']   = $this->profile_img_base_path;
      return view('client.projects.collaboration.detail',$this->arr_view_data);
    }

    public function make_zip($project_id=FALSE)
    {
        if($project_id == FALSE){
          Session::flash('error', trans('controller_translations.something_went_wrong_please_try_again_later')); 
          return redirect()->back();
        }

        $project_main_attachment        = [];
        $project_attachment             = [];
        $arr_pagination                 = [];
        $project_id                     = base64_decode($project_id); 
        $obj_project_main_attachment    = $this->ProjectpostModel
                                               ->where('id',$project_id)
                                               ->first(['id','project_name','project_status','project_attachment']);
        if(isset($obj_project_main_attachment) && $obj_project_main_attachment != null){
          $project_main_attachment = $obj_project_main_attachment->toArray(); 
        }
        // project attachments 
        $obj_project_attachment    = $this->ProjectAttchmentModel
                                         ->where('project_id',$project_id)
                                         ->orderBy('created_at','DESC') 
                                         ->paginate(12);  
        if($obj_project_attachment) {
          $arr_pagination          = clone $obj_project_attachment;
          $project_attachment      = $obj_project_attachment->toArray();
        }
        $files = []; 

        if($project_attachment['data'] || $project_main_attachment){
            if($project_attachment['data']){
              foreach($project_attachment['data'] as $key => $attch){
                if(file_exists('public/uploads/front/project_attachments/'.$attch['attachment'])){
                  $files[]     = glob(public_path('uploads/front/project_attachments/'.$attch['attachment']));
                }
              }
            }
            if(isset($project_main_attachment['project_attachment']) && $project_main_attachment['project_attachment'] != ""){
              if(file_exists('public/uploads/front/postprojects/'.$project_main_attachment['project_attachment'])){
                  $attachment  = isset($project_main_attachment['project_attachment'])?$project_main_attachment['project_attachment']:"";
                  $files[]     = glob(public_path('uploads/front/postprojects/'.$attachment));
              }
            }
          
            if(isset($files) && sizeof($files) > 0){
            $project_title = str_slug($project_main_attachment['project_name'], '-');
            $zipfilename   = $project_title.date('YmdHis').".zip";
            //$files[]       = glob(public_path('front/js/accordian.js'));
            //$files[]       = glob(public_path('front/js/backtotop.js'));
            Zipper::make('uploads/front/project_attachments/zipfiles/'.$zipfilename)->add($files);
            return redirect($this->module_url_path.'/collaboration/download-zip/'.$zipfilename);
            } else {
              Session::flash('error', 'No files found for creation of a ZIP file.'); 
              return redirect()->back();
            }
        } else {
          Session::flash('error', 'No files found for creation of a ZIP file.'); 
          return redirect()->back();
        }
    }
    
    public function download_zip($zipname=FALSE){
        if(file_exists('public/uploads/front/project_attachments/zipfiles/'.$zipname)){
          return response()->download(public_path('/uploads/front/project_attachments/zipfiles/'.$zipname));
        } else{
          return false;
        }
    }
} // end class
