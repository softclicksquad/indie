<?php

namespace App\Http\Controllers\Front\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\ClientsModel;
use App\Models\ProjectpostModel;
use App\Models\ReviewsModel;

use App\Common\Services\MailService;

use Sentinel;
use Validator;
use Session;
use URL;

class ReviewController extends Controller
{
    public $arr_view_data;
    public function __construct(  ProjectpostModel $projectpost,
                                  ClientsModel $clients,
                                  ReviewsModel $reviews,
                                  MailService $mail_service
                                )
    {
      $this->arr_view_data = [];

      if(! $user = Sentinel::check()) 
      {
        return redirect('/login');
      }

      $this->user_id = $user->id;

      $this->ClientsModel       = $clients;
      $this->ProjectpostModel   = $projectpost;
      $this->ReviewsModel       = $reviews;
      $this->MailService        = $mail_service;

      $this->arr_view_data = [];
      $this->view_folder_path   = 'reviews';
      $this->module_url_path    = url("/client/review");
    }

    /*
    | Comment : Add review page for completed projects client
    | auther  : Ashwini K.
    */

    public function add(Request $request)
    { 
        $arr_rules = [];
        $arr_rules['review']               = "required";
        $arr_rules['rating_type_one']      = "required"; 
        $arr_rules['rating_type_two']      = "required";
        $arr_rules['rating_type_three']    = "required";
        $arr_rules['rating_type_four']     = "required";
        $arr_rules['rating_type_five']     = "required";      	            

        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = [];
        $form_data = $request->all();

        $arr_review = [];
      
        $project_id     = base64_decode($form_data['project_id']);
        $expert_user_id = base64_decode($form_data['expert_user_id']);

        if($request->has('review') && $request->has('rating_type_one') && $request->has('rating_type_two') && $request->has('rating_type_three') && $request->has('rating_type_four') && $request->has('rating_type_five')  && $project_id != "" && $expert_user_id!= "")
        {
          $all_rating =  $form_data['rating_type_one'] +  $form_data['rating_type_two'] +  $form_data['rating_type_three'] +  $form_data['rating_type_four'] + $form_data['rating_type_five'];

            $avg_rating = $all_rating/5;

            $arr_review['to_user_id']        = $expert_user_id;
            $arr_review['review']            = $form_data['review'];
            $arr_review['rating']            = number_format($avg_rating,1);
            $arr_review['project_id']        = $project_id;
            $arr_review['from_user_id']      = $this->user_id;
            $is_review_exists  =  $this->ReviewsModel->where('project_id',$project_id)->where('from_user_id',$this->user_id)->count();
            
            if(isset($is_review_exists) && $is_review_exists==0 )
            {
                $res_review = $this->ReviewsModel->create($arr_review); 
            }
            if($res_review)
            { 
              Session::flash('success',trans('controller_translations.success_review_submitted_successfully'));
              /* Send mail to expert informing about review */
              $this->MailService->send_review_mail_to_expert($form_data);
              return redirect('/client/projects/details/'.$form_data['project_id']);

            }
            else
            {
                Session::flash('error',trans('controller_translations.error_while_submitting_review'));
            }
        }
        else
        {   
            Session::flash('error',trans('controller_translations.error_while_submitting_review'));
        }

        return redirect()->back();

    }

    /*
        Comments : Show Reviews page.
        Auther   : Nayan S.
    */

    public function show_review_page($enc_id)
    {
      
      $project_id = base64_decode($enc_id);      

      if($project_id == "")
      {
        return redirect()->back();
      }

      $arr_review_details= [];
        
      $obj_review_details  = $this->ReviewsModel->where('project_id',$project_id)->orderBy('created_at', 'DESC')->get();
      
      if($obj_review_details!=FALSE)
      {
        $arr_review_details = $obj_review_details->toArray(); 
      }
     
      $is_review_exists   =  $this->ReviewsModel->where('project_id',$project_id)->where('from_user_id',$this->user_id)->count();

      $this->arr_view_data['page_title']           = trans('controller_translations.page_title_reviews');
      $this->arr_view_data['projectInfo']           = $this->get_project_details($project_id);
      $this->arr_view_data['arr_review_details']    = $arr_review_details;
      $this->arr_view_data['is_review_exists']      = $is_review_exists;
      
      return view('client.projects.project_reviews',$this->arr_view_data);
    }

    /*
    | Comments    : To get all project details.
    | auther      : Nayan S.
    */

    public function get_project_details($id)
    { 
      $obj_project_info = $this->ProjectpostModel
                               ->with('skill_details','client_info','client_details','project_skills.skill_data','category_details','project_bid_info','project_bids_infos')
                               ->where('id',$id)
                               ->first();

      $arr_project_info = array();

      if($obj_project_info != FALSE)
      {
        $arr_project_info = $obj_project_info->toArray();
      }
      return $arr_project_info;
    }

}
