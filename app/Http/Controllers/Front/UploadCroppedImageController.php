<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ProjectManagerModel; 
use App\Models\ClientsModel; 
use App\Models\ExpertsModel; 
use App\Models\RecruiterModel; 
use App\Models\ExpertPortfolioModel;

use Sentinel;

class UploadCroppedImageController extends Controller
{
    /*
    | Comment: This is the inbuilt library function which performs cropping of an image.
    | Auther : Nayan S.
    */

    public $src;
    public $data;
    public $dst;
    public $type;
    public $extension;
    public $msg;

    

    public function __construct(
                                  ProjectManagerModel $project_manager,
                                  ClientsModel $client,
                                  ExpertsModel $expert,
                                  RecruiterModel $recruitermodel,
                                  ExpertPortfolioModel $expert_portfolio
                              )
    {
      $this->ProjectManagerModel  = $project_manager;
      $this->ClientsModel         = $client;
      $this->ExpertsModel         = $expert;
      $this->ExpertPortfolioModel = $expert_portfolio;
      $this->RecruiterModel       = $recruitermodel;
    
    }

    function init_crop($src, $data, $file) {
     
      $this -> setSrc($src);
      $this -> setData($data);
      $this -> setFile($file);
      $this -> crop($this -> src, $this -> dst, $this -> data);

    }

    public function setSrc($src) {

    if (!empty($src)) {
      $type = exif_imagetype($src);

      if ($type) {
        $this -> src = $src;
        $this -> type = $type;
        $this -> extension = image_type_to_extension($type);
        $this -> setDst();
      }
    }
  }

  public function setData($data) {
    if (!empty($data)) {
      $this -> data = json_decode(stripslashes($data));
    }
  }

  public function setFile($file) {

    $errorCode = $file['error'];

    if ($errorCode === UPLOAD_ERR_OK) {
      $type = exif_imagetype($file['tmp_name']);

      if ($type) {

        $extension = image_type_to_extension($type);

        $this->tmp_name = sha1(uniqid().date('YmdHis').uniqid());

        if($GLOBALS['upload_portfolio'] == "YES")
        {
          $src = base_path().'/public'.config('app.project.img_path.portfolio_image'). sha1(uniqid().date('YmdHis').uniqid()).'.png';
        }
        else
        {
          $src = base_path().'/public'.config('app.project.img_path.profile_image'). sha1(uniqid().date('YmdHis').uniqid()).'.png';
        }

        if ( $type == IMAGETYPE_GIF || $type == IMAGETYPE_JPEG || $type == IMAGETYPE_PNG ) {

          if (file_exists($src)) {
            unlink($src);
          }

          $result = move_uploaded_file($file['tmp_name'], $src);

          if ($result) 
          {
            
            $this -> src = $src;
            $this -> type = $type;
            $this -> extension = $extension;
            $this -> setDst();

           
          } else {
             $this -> msg = trans('controller_translations.failed_to_save_file');
          }
        } else {
          $this -> msg = trans('controller_translations.please_upload_image');
        }
      } else {
        $this -> msg = trans('controller_translations.please_upload_image_file');
      }
    } else {

      $this -> msg = $this -> codeToMessage($errorCode);
    }
  }

  public function setDst() {

    if($GLOBALS['upload_portfolio'] == "YES")
    {
      $this -> dst = base_path().'/public/'.config('app.project.img_path.portfolio_image') . $this->tmp_name .'.png';
    }
    else
    {
      $this -> dst = base_path().'/public/'.config('app.project.img_path.profile_image') . $this->tmp_name .'.png';
    }
 }

  public function crop($src, $dst, $data) {
    if (!empty($src) && !empty($dst) && !empty($data)) {
      switch ($this -> type) {
        case IMAGETYPE_GIF:
          $src_img = imagecreatefromgif($src);
          break;

        case IMAGETYPE_JPEG:
          $src_img = imagecreatefromjpeg($src);
          break;

        case IMAGETYPE_PNG:
          $src_img = imagecreatefrompng($src);
          break;
      }

      if (!$src_img) {
        $this -> msg = trans('controller_translations.failed_to_read_the_image_file');
        return;
      }

      $size = getimagesize($src);
      $size_w = $size[0]; // natural width
      $size_h = $size[1]; // natural height

      $src_img_w = $size_w;
      $src_img_h = $size_h;

      $degrees = $data -> rotate;

      // Rotate the source image
      if (is_numeric($degrees) && $degrees != 0) {
        // PHP's degrees is opposite to CSS's degrees
        $new_img = imagerotate( $src_img, -$degrees, imagecolorallocatealpha($src_img, 0, 0, 0, 127) );

        imagedestroy($src_img);
        $src_img = $new_img;

        $deg = abs($degrees) % 180;
        $arc = ($deg > 90 ? (180 - $deg) : $deg) * M_PI / 180;

        $src_img_w = $size_w * cos($arc) + $size_h * sin($arc);
        $src_img_h = $size_w * sin($arc) + $size_h * cos($arc);

        // Fix rotated image miss 1px issue when degrees < 0
        $src_img_w -= 1;
        $src_img_h -= 1;
      }

      $tmp_img_w = $data -> width;
      $tmp_img_h = $data -> height;
      $dst_img_w = 271;
      $dst_img_h = 249;

      $src_x = $data -> x;
      $src_y = $data -> y;

      if ($src_x <= -$tmp_img_w || $src_x > $src_img_w) {
        $src_x = $src_w = $dst_x = $dst_w = 0;
      } else if ($src_x <= 0) {
        $dst_x = -$src_x;
        $src_x = 0;
        $src_w = $dst_w = min($src_img_w, $tmp_img_w + $src_x);
      } else if ($src_x <= $src_img_w) {
        $dst_x = 0;
        $src_w = $dst_w = min($tmp_img_w, $src_img_w - $src_x);
      }

      if ($src_w <= 0 || $src_y <= -$tmp_img_h || $src_y > $src_img_h) {
        $src_y = $src_h = $dst_y = $dst_h = 0;
      } else if ($src_y <= 0) {
        $dst_y = -$src_y;
        $src_y = 0;
        $src_h = $dst_h = min($src_img_h, $tmp_img_h + $src_y);
      } else if ($src_y <= $src_img_h) {
        $dst_y = 0;
        $src_h = $dst_h = min($tmp_img_h, $src_img_h - $src_y);
      }

      // Scale to destination position and size
      $ratio = $tmp_img_w / $dst_img_w;
      $dst_x /= $ratio;
      $dst_y /= $ratio;
      $dst_w /= $ratio;
      $dst_h /= $ratio;

      $dst_img = imagecreatetruecolor($dst_img_w, $dst_img_h);

      // Add transparent background to destination image
      imagefill($dst_img, 0, 0, imagecolorallocatealpha($dst_img, 0, 0, 0, 127));
      imagesavealpha($dst_img, true);

      $result = imagecopyresampled($dst_img, $src_img, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);

      if ($result) {
        if (!imagepng($dst_img, $dst)) {
          $this -> msg = trans('controller_translations.failed_to_save_the_cropped_image_file');
        }
      } else {
        $this -> msg = trans('controller_translations.failed_to_crop_the_image_file');
      }

      imagedestroy($src_img);
      imagedestroy($dst_img);
    }
  }

  public function codeToMessage($code) {
    
    $errors = array(
      UPLOAD_ERR_INI_SIZE =>'The uploaded file exceeds the upload_max_filesize directive in php.ini',
      UPLOAD_ERR_FORM_SIZE =>'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
      UPLOAD_ERR_PARTIAL =>'The uploaded file was only partially uploaded',
      UPLOAD_ERR_NO_FILE =>'No file was uploaded',
      UPLOAD_ERR_NO_TMP_DIR =>'Missing a temporary folder',
      UPLOAD_ERR_CANT_WRITE =>'Failed to write file to disk',
      UPLOAD_ERR_EXTENSION =>'File upload stopped by extension',
    );

    if (array_key_exists($code, $errors)) {
      return $errors[$code];
    }

    return 'Unknown upload error';
  }

  public function getResult() {
    if (file_exists($this -> src)) {
            unlink($this -> src);
          }
    return !empty($this -> data) ? $this -> dst : $this -> src;
  }

  public function getMsg() {
    return $this -> msg;
  }

  public function upload_cropped_image(Request $request)
  {
      $upload_portfolio = $request->input('upload_portfolio');
      
      /* Setting up global variable which is accessible in any function */
      $GLOBALS['upload_portfolio'] =  $upload_portfolio;


      $crop  = $this->init_crop(   
                    isset($_POST['avatar_src']) ? $_POST['avatar_src'] : null,
                    isset($_POST['avatar_data']) ? $_POST['avatar_data'] : null,
                    isset($_FILES['avatar_file']) ? $_FILES['avatar_file'] : null 
                  );
       
      $response = array (
        'state'  => 200,
        'message' => $this->getMsg(),
        'result' => $this->getResult()
      );
      
      if(isset($response['result'])  && $response['result'] != "")
      {
        $arr_tmp = explode('/', $response['result'] );

        $logged_user = Sentinel::check();

        if(isset($logged_user) && $logged_user != FALSE)
        {
          if($GLOBALS['upload_portfolio'] == "YES")
          {
            $portfolio_model = $this->ExpertPortfolioModel;
          } 
          else
          {
            if($logged_user->inRole('project_manager'))
            {
              $model = $this->ProjectManagerModel;
            } 
            else if($logged_user->inRole('client'))
            {
              $model = $this->ClientsModel;
            }
            else if($logged_user->inRole('expert'))
            {
              $model = $this->ExpertsModel;
            } 
            else if($logged_user->inRole('recruiter'))
            {
              $model = $this->RecruiterModel;
            } 
          } 
        }

        /* Saving profile image into the database */
        if(isset($model) )
        { 
          $obj_user = $model->where('user_id','=',$logged_user->id)->first(['id','user_id','profile_image']);
          if($obj_user)
          {
            if(isset($obj_user->profile_image) && $obj_user->profile_image != "" )
            {
              @unlink(base_path().'/public/'.config('app.project.img_path.profile_image') .'/'.$obj_user->profile_image);
            }
          }

          $image_name  = array_last($arr_tmp);

          $upload_img = []; 
          $upload_img['profile_image']           = $image_name;


          $result_user = $model->where('user_id','=',$logged_user->id)->update(['profile_image'=>$image_name]);
        }

        /* Saving portfolio image into the database */
        if(isset($portfolio_model))
        {
          $image_name  = array_last($arr_tmp);
          $result_user = $portfolio_model->create([
                                                   'expert_user_id'=>$logged_user->id , 
                                                   'file_name'=>$image_name
                                                  ]);
        }


        if(isset($result_user) &&  $result_user == false )
        {
          $this -> msg = trans('controller_translations.error_while_updating_profile_image'); 
        }

        echo json_encode($response);
    }
  }
}