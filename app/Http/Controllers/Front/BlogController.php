<?php

namespace App\Http\Controllers\Front;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App;
use Validator;
use Session;

use App\Models\BlogsModel;
use App\Models\BlogCommentsModel;
use App\Models\BlogViewModel;
use DB;

use Sentinel;
use ZipArchive;

class BlogController extends Controller
{
  public function __construct()
  {
        
        $this->arr_view_data             = [];
        $this->BlogViewModel                   = new BlogViewModel();
        $this->BlogsModel                      = new BlogsModel();
        $this->BlogCommentsModel               = new BlogCommentsModel();
        $this->module_url_path                 = url('/blog');
        $this->module_view_folder              = "front.blog";
        $this->blog_image_base_path            = base_path().'/public'.config('app.project.img_path.blog_image');
        $this->blog_image_public_path          = url('/public').config('app.project.img_path.blog_image');
        $this->user_image_public_path          = url('/public').config('app.project.image_path.user_profile_images');
        //dd($this->blog_image_base_path);
  }
  /*
  | Comment : Project listing page
  | auther  : Nayan S.
  */
    public function index( $tags = NULL)
    {
        $arr_blogs      = [];
        $obj_blogs      = $this->BlogsModel;
        
        if($tags!=null)
        {
          $obj_blogs =$obj_blogs->where('tags','like', '%'.$tags.'%');

        }

        $obj_blogs      =$obj_blogs->with(['category'=>function($q){
          $q->select('id','name');
        }])->where('is_active', '1')->orderBy('created_at','DESC')->paginate(10);
        //$obj_pagination = $obj_blogs->links();
        
        if($obj_blogs)
        {
          $arr_blogs = $obj_blogs->toArray();
        }
        //dd($arr_blogs);
        $this->arr_view_data['arr_blogs']                       = isset($arr_blogs['data'])? $arr_blogs['data']:[];
        $this->arr_view_data['obj_blogs']                       = $obj_blogs;
        $this->arr_view_data['blog_image_public_path']          = $this->blog_image_public_path;
        $this->arr_view_data['blog_image_base_path']            = $this->blog_image_base_path;
        
        return view($this->module_view_folder.'.index',$this->arr_view_data);
        // return view($this->module_view_folder.'.index',$this->arr_view_data);
    }

  public function details(Request $request, $slug='')
  {
    if($slug=='')
    {
      return redirect()->back();
    }
    $arr_blog     = [];
    $arr_comments = [];
    $obj_blog     = $this->BlogsModel->with(['category','comments'=>function($q)
    {
      $q->orderBy('id','desc');
      $q->select(['id', 'blog_id', 'created_at', 'user_name', 'comment_by', 'comment']);
    }])->where('slug', $slug)->first();

    if($obj_blog)
    {
      $arr_blog = $obj_blog->toArray();
    }
    else
    {
      return redirect()->back();
    }

    $page = $request->input('page',1);

    $obj_comments  = $this->BlogCommentsModel->where('blog_id',$arr_blog['id'])->select(['id', 'blog_id', 'created_at', 'user_name', 'comment_by', 'comment'])->with(['user'=>function($q){
      //$q->select(['id','profile_image']);
    }])->orderBy('id', 'desc')->paginate(3);
    $comment_count = $this->BlogCommentsModel->where('blog_id',$arr_blog['id'])->count();

    if($obj_comments)
    {
      $arr_comments = $obj_comments->toArray();
    }
   
    $this->upgrade_view($arr_blog['id']);
    
    $this->arr_view_data['arr_blog']                     = $arr_blog;
    $this->arr_view_data['blog_image']                   = isset($arr_blog['image'])? $arr_blog['image']:null;
    $this->arr_view_data['arr_comments']                 = isset($arr_comments['data'])? $arr_comments['data']:[];
    $this->arr_view_data['comment_count']                = $comment_count;
    $this->arr_view_data['blog_image_public_path']       = $this->blog_image_public_path;
    $this->arr_view_data['module_url_path']              = $this->module_url_path;
    $this->arr_view_data['user_image_public_path']       = $this->user_image_public_path;
    $this->arr_view_data['home_page_title']              = isset($arr_blog['name'])? $arr_blog['name']:'';
    $this->arr_view_data['search_title']                 = isset($arr_blog['name'])? $arr_blog['name']:'';
    $this->arr_view_data['page_description']             = isset($arr_blog['description'])? strip_tags($arr_blog['description']):'';
    $this->arr_view_data['blog_image_base_path']         = base_path().config('app.project.image_path.blog_image');
    
    if($request->ajax()) 
    {
      if(isset($page) && $page!=1)
      {
        if(isset($arr_comments['data'])&& is_array($arr_comments['data']) && sizeof($arr_comments['data'])>0)
        {
          return view($this->module_view_folder.'.load_comment',$this->arr_view_data)->render();
        }
        else
        {
          $arr_json['status'] = 'end';
          return response()->json($arr_json);
        }
      }
      return view($this->module_view_folder.'.ajax_comment',$this->arr_view_data)->render();
    }

    return view($this->module_view_folder.'.detail',$this->arr_view_data);
  }

  public function submit_comment(Request $request)
  {

    $arr_json               = [];
    $arr_data               = [];
    $arr_rules['message']   = "required";
    $arr_rules['user_name'] = "required";
    $arr_rules['email']     = "required";
    $arr_rules['enc_id']    = "required";

    $validator = Validator::make($request->all(),$arr_rules);

    if($validator->fails()) 
    {   
      $arr_json['status']  = 'error';
      $arr_json['message'] = 'Please fill up the all mandatory fields.';

      return response()->json($arr_json);
    }

    $message   = $request->input('message', null);
    $user_name = $request->input('user_name', null);
    $email     = $request->input('email', null);
    $blog_id   = $request->input('enc_id', null);

    $password  = $request->input('password', null);   
    $status    = $request->input('status',null);
    
    $user_id  = isset(Sentinel::check()->id)? Sentinel::check()->id:0;
    if($user_id==0)
    {
      $arr_json['message'] = 'Please Login First.';
      $arr_json['status']  = 'ERROR';
      
      return response()->json($arr_json);
    }
    else 
    {
      $user_id   = isset(Sentinel::check()->id)? Sentinel::check()->id:1;
    }

    
    $arr_data['blog_id']    = decrypt_data($blog_id);
    $arr_data['comment_by'] = $user_id;
    $arr_data['user_name']  = $user_name;
    $arr_data['user_email'] = $email;
    $arr_data['comment']    = $message;

    $create = $this->BlogCommentsModel->create($arr_data);

    if($create)
    {
      if($status=="before_login")
      {
        Session::flash('success','Your comment posted successfully.');
      }
      $arr_json['login_status'] = $status;
      $arr_json['status']       = 'success';
      $arr_json['message']      = 'Your comment posted successfully.';

      return response()->json($arr_json); 
    }
    else
    {
      $arr_json['status']  = 'error';
      $arr_json['message'] = 'Error while posting your comment.';
      return response()->json($arr_json);
    }

  }

  public function upgrade_view($blog_id = null)
  {
    $ip_address = isset($_SERVER['REMOTE_ADDR'])?$_SERVER['REMOTE_ADDR']:false;

    if($blog_id!=null)
    {
      $this->BlogViewModel->updateOrCreate(['blog_id'=>$blog_id, 'unique_id'=>$ip_address,
        'created_at' => $this->BlogViewModel->where('created_at', '>', DB::raw('CURDATE()'))->first()->created_at ?? null
      ]);
    }

    return true;
  }

} // end class