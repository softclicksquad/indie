<?php
namespace App\Http\Controllers\Front;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Common\Services\MailService;
use App\Common\Services\WalletService;
use App;
use Validator;
use Session;
use Sentinel;
use App\Models\StaticPageModel;
use App\Models\AdminProfileModel;
use App\Models\ContactEnquiryModel;
use App\Models\UserModel;
use App\Models\DepositeCurrencyModel;
use App\Models\CurrencyModel;
use App\Models\UserWalletModel;
use App\Models\ClientsModel;

use Mail;
use App\Models\ProjectpostModel;
use App\Common\Services\LanguageService;  
use App\Common\Services\CommonDataService;  

use App\Models\CategoriesModel;  
use App\Models\NotificationsModel;  
use App\Models\SubscriptionPacksModel;  
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use redirect;
class HomeController extends Controller
{

	/*
    | Comment : Home controller for dishplay website fornt section
    | auther : Sagar Sainkar
    | 
    */ 

    public function __construct(StaticPageModel $static_page,
                                AdminProfileModel $admin_profile,
                                ContactEnquiryModel $contact,
                                UserModel $users_model,
                                CategoriesModel $categories,
                                LanguageService $langauge,
                                ProjectpostModel $projectpost,
                                NotificationsModel $NotificationsModel,
                                SubscriptionPacksModel $SubscriptionPacksModel,
                                CommonDataService $common_data_service,
                                MailService $MailService,
                                WalletService $WalletService
                                )
	{
        $this->arr_view_data           = [];
        $this->StaticPageModel         = $static_page;
        $this->AdminProfileModel       = $admin_profile;
        $this->ContactEnquiryModel     = $contact;
        $this->UserModel               = $users_model;
        $this->CategoriesModel         = $categories;
        $this->LanguageService         = $langauge;
        $this->ProjectpostModel        = $projectpost;
        $this->MailService             = $MailService;
        $this->WalletService           = $WalletService;
        $this->UserWalletModel         = new UserWalletModel;
        $this->NotificationsModel      = $NotificationsModel;
        $this->SubscriptionPacksModel  = $SubscriptionPacksModel;
        $this->CommonDataService       = $common_data_service;
        $this->DepositeCurrencyModel   = new DepositeCurrencyModel;
        $this->CurrencyModel           = new CurrencyModel;
        $this->ClientsModel            = new ClientsModel;
	}
	/*
    | comment : Display website home page
    | auther  : Sagar Sainkar
    */
    public function index()
    {
     	$this->arr_view_data['page_title'] = trans('controller_translations.page_title_home');
        /* get categories */
        $arr_lang           = $this->LanguageService->get_all_language();
        $arr_categories     = [];
        $obj_category       = $this->CategoriesModel->where('categories.is_active','1')->with(['translations'])->limit(10)->get();
        if($obj_category != FALSE)        {
          $arr_categories   = $obj_category->toArray();
        }
        $this->arr_view_data['arr_categories'] = $arr_categories; 
        /* get categories */
        
    	return view('front.home.index',$this->arr_view_data);
    }
    /*
    | comment :more categories page
    | auther  :tushar ahire
    */
    public function more_categories()
    {
        $this->arr_view_data['page_title'] = trans('common/home.text_categories');
        /* get categories */
        $arr_categories = [];
        $arr_lang     = $this->LanguageService->get_all_language();
        /*$obj_category = $this->CategoriesModel->where('categories.is_active','1')->with(['translations'])->paginate(16)*/;
        $obj_category = $this->CategoriesModel->where('categories.is_active','1')->with(['translations'])->get();
        if($obj_category != FALSE)        {

            //$arr_pagination   = clone $obj_category; 
            $arr_categories = $obj_category->toArray();
        }

        //$this->arr_view_data['arr_pagination']  = $arr_pagination;
        $this->arr_view_data['arr_categories'] = $arr_categories; 
        /* get categories */  

        return view('front.home.more_categories',$this->arr_view_data);
    }

    /*
    | comment :Set language
    | auther  :Sagar Sainkar
    | 
    */
    public function set_lang($locale)
    {
        Session::set('locale',$locale);
        app()->setLocale($locale);
        return redirect()->back();
    }

    /*
    | comment : show static pages
    | auther :Sagar Sainkar
    | 
    */

    public function show_static_page($page_slug = FALSE)
    {
        $arr_page = array();
        if($page_slug==FALSE)
        {
            abort(404);
        }

        $obj_page =  $this->StaticPageModel->where('page_slug',$page_slug)->with(['translations'])->first();

        if(!$obj_page)
        {
            abort(404);
        }
        else
        {
            $arr_page = $obj_page->toArray();     
        }
        if($page_slug=='about-us')
        {
           $this->arr_view_data['page_title'] = isset($arr_page['page_name'])?$arr_page['page_name']:trans('controller_translations.page_title_about_us');
           $this->arr_view_data['arr_page']   = $arr_page;
           return view('front.home.about_us',$this->arr_view_data);
        }
        elseif($page_slug=='how-it-works')
        {
           $this->arr_view_data['page_title'] = isset($arr_page['page_name'])?$arr_page['page_name']:trans('controller_translations.page_title_how_it_works');
           $this->arr_view_data['arr_page'] = $arr_page;
           return view('front.home.how_it_works',$this->arr_view_data);
        }

        //dd($arr_page);
        $this->arr_view_data['page_title'] = isset($arr_page['page_name'])?$arr_page['page_name']:trans('controller_translations.page_title_home');
        $this->arr_view_data['arr_page'] = $arr_page;
        
        return view('front.home.static_page',$this->arr_view_data);
    }

    public function contact_us()
    { 
        $arr_page = array();
        $obj_page =  $this->AdminProfileModel->where('user_id','1')->first();
        if(!$obj_page){
            abort(404);
        } else {
            $arr_page = $obj_page->toArray();     
        }
        $this->arr_view_data['page_title'] = trans('controller_translations.page_title_contact_us');
        $this->arr_view_data['arr_page'] = $arr_page;
        return view('front.home.contact_us',$this->arr_view_data);
    }

    public function store_contact_enquiry(Request $request)
    { 
        $form_data = array();
        $form_data = $request->all();
        
        $arr_rules['name']            = "required";  
        $arr_rules['email']           = "required|email";  
        $arr_rules['contact_number']  = "required"; 
        $arr_rules['enquiry_message'] = "required"; 
        $arr_rules['enquiry_subject'] = "required"; 

        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        if(isset($form_data['recaptcha_response']) == false || $form_data['recaptcha_response'] == '') {
            Session::flash('error',trans('Invalid google recaptcha, Unable to process please try again.'));   
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $recaptcha_url      = 'https://www.google.com/recaptcha/api/siteverify';
        $recaptcha_secret   = config('app.project.RECAPTCHA_SECRET_KEY');
        $recaptcha_response = $form_data['recaptcha_response'];

        // Make and decode POST request:
        $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
        $arr_response = json_decode($recaptcha,true);

        if(isset($arr_response['success']) && $arr_response['success'] == false){
            Session::flash('error',trans('Invalid google recaptcha, Unable to process please try again.'));   
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        if(isset($arr_response['score']) && $arr_response['score']<=0.5){
            Session::flash('error',trans('Invalid google recaptcha, Unable to process please try again.'));   
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // create unique indentifier
        $rs  = \DB::table('contact_enquiry')->select('enquiry_indentifier')->where('enquiry_indentifier' ,'!=' , null)->where('enquiry_indentifier' ,'!=' , '')->orderBy('id' , 'DESC')->LIMIT('1')->get();
        $res = $rs;
        if(count($res) > 0)
        {                 
            $max_inq_nbr = $res[0]->enquiry_indentifier;
            // remove STU from that no.                
            $max_inq_nbr = substr($max_inq_nbr, 3);
            // get count of that no.
            $cnt = strlen($max_inq_nbr);
            // increment that no. by 1
            $max_inq_nbr = $max_inq_nbr + 1;                        
            // add no. of zero's to the no.                
            $max_inq_nbr = str_pad($max_inq_nbr, $cnt, '0', STR_PAD_LEFT);
            $max_inq_nbr = 'INQ'.$max_inq_nbr;  
        }
        else
        {
            $max_inq_nbr = 'INQ000000001';  
        }
        // create unique indentifier
    
        $form_data = $request->all();
        $arr_data  = array();
        $arr_data['name']            = $form_data['name'];
        $arr_data['email']           = $form_data['email']; 
        $arr_data['contact_number']  = $form_data['contact_number'];  
        $arr_data['enquiry_message'] = $form_data['enquiry_message'];  
        $arr_data['enquiry_subject'] = $max_inq_nbr.'-'.$form_data['enquiry_subject'];
        $arr_data['inquiry_number']  = $max_inq_nbr;

        if(isset($form_data['report_a_problem']) && $form_data['report_a_problem'] !=""){
            $arr_data['report_a_problem']    = $form_data['report_a_problem'];
            $arr_data['user_id']             = isset($form_data['report_user_id'])?$form_data['report_user_id']:'0';
            $arr_data['is_report_a_problem'] = 'YES';
        }  

        $arr_data['enquiry_indentifier'] = $max_inq_nbr;
        $enquiry    = $this->ContactEnquiryModel->create($arr_data);
        if($enquiry)
        {
            $arr_admin = array();
            $obj_admin =  $this->UserModel->where('id','1')->first();
            if(!$obj_admin){
                //abort(404);
            } else {
                $arr_admin = $obj_admin->toArray();     
            }
            $data          = array();
            $to_email_id   = isset($arr_admin['email'])?$arr_admin['email']:'support@archexperts.com';
            $data['email_id'] = $to_email_id;
            
            $project_name  = config('app.project.name');
            $mail_subject  = 'New Contact Inquiry';
            //$mail_form     = isset($website_contact_email)?$website_contact_email:'support@archexperts.com';
            $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
            
            $data          = $arr_data;
            $data['email_id'] = $to_email_id;
            try{
                    $mail_status = $this->MailService->send_new_contact_enquiry_email($data);
                  // Mail::send('front.email.new_contact_enquiry', $data, function ($message) use ($to_email_id,$mail_form,$project_name,$mail_subject) {
                  //     $message->from($mail_form, $project_name);
                  //     $message->subject($project_name.' : '.$mail_subject);
                  //     $message->to($to_email_id);
                  // });
              /* Send notification */
              $arr_data                         = [];
              $arr_data['user_id']              = '1';
              $arr_data['user_type']            = '1';
              $arr_data['url']                  = "admin/contact_inquiries";
              $arr_data['notification_text_en'] = \Lang::get('controller_translations.you_have_recieved_new_contact_inqury',[],'en','en');
              $arr_data['notification_text_de'] = \Lang::get('controller_translations.you_have_recieved_new_contact_inqury',[],'de','en');
              $send_notification = $this->NotificationsModel->insertGetId($arr_data); 
              /* End Send notification */ 
              Session::flash('success',trans('controller_translations.success_enquiry_send_successfully'));
            }
            catch(\Exception $e){
             Session::Flash('error',trans('controller_translations.text_mail_not_sent'));
            }
        }
        else
        {
          Session::flash('error',trans('controller_translations.error_problem_occured_while_sending_enquiry'));
        }
        return redirect()->back();
    }
    public function thank_you(){
        $this->arr_view_data['page_title'] = trans('controller_translations.thank_you');
        return view('front.common.thank-you',$this->arr_view_data);
    }
    public function subscription_plans()
    {
        $getIP = $this->CommonDataService->ip_info($_SERVER['REMOTE_ADDR'], "Location");
        if(isset($getIP['currency_code']) && $getIP['currency_code'] != "")
        {
           $this->arr_view_data['currency_code'] = $getIP['currency_code'];
        } 
        else 
        {
           $this->arr_view_data['currency_code'] = "USD"; 
        }

        $this->arr_view_data['page_title'] = 'Subscription Plans';

        /*$arr_packs = [];
        $obj_packs = $this->SubscriptionPacksModel->where('is_active',1)->get();
        if($obj_packs != FALSE)
        {
            $arr_packs = $obj_packs->toArray();
        }
        $this->arr_view_data['arr_packs']         = $arr_packs;*/
        $arr_currency = [];
        $obj_currency = $this->CurrencyModel->select('currency','currency_code')->where('is_active','=','1')->get();
        if($obj_currency)
        {
            $arr_currency = $obj_currency->toArray();
        }


        $arr_monthly_packs = [];
        $obj_monthly_packs = $this->SubscriptionPacksModel
                          ->where('is_active',1)
                          ->where('type','=','MONTHLY')
                          ->get();
        if($obj_monthly_packs != FALSE)
        {
            $arr_monthly_packs = $obj_monthly_packs->toArray();
        }
        $this->arr_view_data['arr_monthly_packs']         = $arr_monthly_packs;

        $arr_quarterly_packs = [];
        $obj_quarterly_packs = $this->SubscriptionPacksModel
                          ->where('is_active',1)
                          ->where('type','=','QUARTERLY')
                          ->get();
        if($obj_quarterly_packs != FALSE)
        {
            $arr_quarterly_packs = $obj_quarterly_packs->toArray();
        }
        $this->arr_view_data['arr_quarterly_packs']         = $arr_quarterly_packs;

        $arr_yearly_packs = [];
        $obj_yearly_packs = $this->SubscriptionPacksModel
                          ->where('is_active',1)
                          ->where('type','=','YEARLY')
                          ->get();
        if($obj_yearly_packs != FALSE)
        {
            $arr_yearly_packs = $obj_yearly_packs->toArray();
        }
        $this->arr_view_data['arr_yearly_packs']         = $arr_yearly_packs;
        $this->arr_view_data['arr_currency']             = $arr_currency;
        
        return view('front.home.subscription_plans',$this->arr_view_data);
    }

    public function get_service_charge(Request $request)
    {
        //dd($request->all());
        $form_data=$arr_data=[];
        $form_data                          = $request->all();
        $arr_rules['project_currency_code'] = "required";
        $arr_rules['amount']                = "required";
        //$arr_rules['currency_symbol']       = "required";
        
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails()){
            //dd($validator->messages());
            $data['status'] = 'error';
            $data['message']= 'Faild';
            return $data;
        }

        $project_currency_code  = $request->input('project_currency_code');
        $amount                 = $request->input('amount');
        $currency_symbol        = $request->input('currency_symbol');

        $arr_service_charge = get_service_charge($amount,$project_currency_code);
        
        if($project_currency_code == 'JPY') {
            $data['service_charge']     = isset($arr_service_charge['service_charge']) ? $arr_service_charge['service_charge'] : 0;
            $data['total_amount']       = isset($arr_service_charge['total_amount']) ? $arr_service_charge['total_amount'] : 0;
            $data['total_funds_amount'] = $amount;
            return $data;
        }
        $data['service_charge']     = isset($arr_service_charge['service_charge']) ? number_format($arr_service_charge['service_charge'],2) : 0;
        $data['total_amount']       = isset($arr_service_charge['total_amount']) ? number_format($arr_service_charge['total_amount'],2) : 0;
        $data['total_funds_amount'] = number_format($amount,2);
        //dd($data);
        return $data;

    }

    public function get_wallet_details(Request $request)
    {
        //$WalletService = new WalletService;
        $user = Sentinel::check();
        $wallet_details = [];
        if(isset($user) && $user != null){
            $logged_user = $user->toArray();
            $obj_data = $this->UserWalletModel->where('user_id',$logged_user['id'])->get();
      
            if($obj_data)
            {
               $arr_data = $obj_data->toArray();
            } 
            if($logged_user['mp_wallet_created'] == 'Yes')
            {
              foreach ($arr_data as $key => $value) {
                  $get_mangopay_wallet_details[] = $this->WalletService->get_wallet_details($value['mp_wallet_id']);
              }
              $this->arr_view_data['mangopay_wallet_details']= $get_mangopay_wallet_details;
              return view('front.home.header_content',$this->arr_view_data);
            }
        }
    }
    public function get_wallet_balance(Request $request)
    {
        //$WalletService = new WalletService;
        $arr_data = [];
        $currency_code = $request->input('currency_code');
        //dd($currency_code);
        $user = Sentinel::check();
        $wallet_details = [];
        if(isset($user) && $user != null){
            $logged_user = $user->toArray();
            $obj_data = $this->UserWalletModel->where('user_id',$logged_user['id'])
                                              ->where('currency_code',$currency_code)
                                              ->first();
      
            if($obj_data)
            {
               $arr_data = $obj_data->toArray();
            } 
            //dd($arr_data);
            $get_mangopay_wallet_details = $this->WalletService->get_wallet_details($arr_data['mp_wallet_id']);

            $return_arr['balance']  = $get_mangopay_wallet_details->Balance->Amount/100;
            $return_arr['walletId'] = $arr_data['mp_wallet_id'];
            $return_arr['bankId']   = $get_mangopay_wallet_details->Id;
            return $return_arr;
        }
    }

    public function get_minimum_charge(Request $request)
    {
        $form_data=$arr_data=[];
        $form_data                  = $request->all();
        $arr_rules['contest_currency'] = "required";
        
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails()){
            //dd($validator->messages());
            $data['status'] = 'error';
            $data['message']= 'Faild';
            return $data;
        }

        $project_currency_code  = $request->input('contest_currency');

        $obj_data = $this->CurrencyModel->with('deposite')
                                        ->where('currency_code',$project_currency_code)
                                        ->first();
        if($obj_data)
        {
            $arr_data = $obj_data->toArray();

            $min_amount = isset($arr_data['deposite']['min_amount'])?$arr_data['deposite']['min_amount']:'';
            
            $data['msg']         = 'success';
            $data['min_amount']  = $min_amount;
            return $data;
        }  
        else
        {
            $data['msg']         = 'error';
            return $data;
        }
    }

    public function check_profile_filled(Request $request)
    {
        $type = $request->input('type',null);
        $user = Sentinel::check();

        $obj_client = $this->ClientsModel->where('user_id',$user->id)
                                         ->with(['user_details','country_details.states','state_details.cities','city_details'])
                                         ->first();

              if($obj_client != FALSE) {
                $arr_client_details = $obj_client->toArray();
              }

              if($arr_client_details['first_name']             == "" &&
              $arr_client_details['last_name']                 == "" &&
              $arr_client_details['user_details']['user_name'] == "")
              {
                Session::flash("error",'Please fill your profile for posting '.$type);
                return 'error';
              }
              if($arr_client_details['first_name']             == ""){
                Session::flash("error",'Please fill your profile for posting '.$type);
                return 'error';
              }
              if($arr_client_details['last_name']              == ""){
                Session::flash("error",'Please fill your profile for posting '.$type);
                return 'error';
              }     
              if($arr_client_details['user_type'] == 'business' && $arr_client_details['vat_number'] == ""){
                Session::flash("error",'Please fill your profile for posting '.$type);
                return 'error';
              }
              if($arr_client_details['user_details']['user_name']  == ""){
                Session::flash("error",'Please fill your profile for posting '.$type);
                return 'error';
              }
              if($arr_client_details['country']== "" || $arr_client_details['country']== NULL){
                Session::flash("error",'Please fill your profile for posting '.$type);
                return 'error';
              }

              $obj_data = $this->UserModel->where('id',$user->id)->first();
              $is_wallet_created = isset($obj_data->mp_wallet_created)?$obj_data->mp_wallet_created:'';
              if($is_wallet_created!='Yes')
              {

                //Session::flash("error",'Please fill your profile for posting contest');
                return redirect(url('/client/wallet/create_user'))->send();

              }
              return 'success';
    }

    public function check_profile_filled_client(Request $request)
    {
        $type = $request->input('type',null);
        $user = Sentinel::check();


        $obj_data = $this->UserModel->where('id',$user->id)->first();
        $is_wallet_created = isset($obj_data->mp_wallet_created)?$obj_data->mp_wallet_created:'';
        if($is_wallet_created!='Yes')
        {
            return redirect(url('/client/wallet/create_user'))->send();
        }
        
        return 'success';
    }
} // end class
