<?php

namespace App\Http\Controllers\Front;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App;
use Validator;
use Session;
use App\Models\ProjectpostModel;
use App\Models\FavouriteProjectsModel;
use App\Common\Services\ProjectSearchService;
use App\Common\Services\SubscriptionService;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\SkillsModel;
use App\Models\SubCategoriesModel;
use App\Models\ProjectPostDocumentsModel;
use App\Models\ProfessionModel;
use App\Models\SiteSettingModel;
use Mail;

use Sentinel;
use ZipArchive;

class ProjectListingController extends Controller
{
  public function __construct(
                                ProjectpostModel $project_post,
                                ProjectSearchService $project_search,
                                FavouriteProjectsModel $favourites_projects,
                                SubscriptionService $subscription_service,
                                SkillsModel $skills,
                                SubCategoriesModel $sub_categories_model,
                                ProjectPostDocumentsModel $project_post_documents_model,
                                ProfessionModel $profession_model

                             )
  {
        
        $this->arr_view_data             = [];
        $this->ProjectpostModel          = $project_post;
        $this->ProjectSearchService      = $project_search;
        $this->FavouriteProjectsModel    = $favourites_projects;
        $this->SubscriptionService       = $subscription_service;
        $this->SkillsModel               = $skills;
        $this->SubCategoriesModel        = $sub_categories_model;
        $this->ProjectPostDocumentsModel = $project_post_documents_model;
        $this->ProfessionModel           = $profession_model;
        $this->SiteSettingModel          = new SiteSettingModel;
        $this->project_attachment_public_path        = url('/').config('app.project.img_path.project_attachment');
        $this->project_attachment_base_path          = public_path().config('app.project.img_path.project_attachment');


        /* Common data required for both views */
        $this->all_projects_count = $this->ProjectpostModel->where('projects.project_status','=','2')
                                                            ->where('projects.bid_closing_date','>',date('Y-m-d'));
                                                            /*if(Sentinel::check() == false){
                                                            $this->all_projects_count->where('projects.project_type','0');
                                                            }*/
                                                            $this->all_projects_count = $this->all_projects_count->where('projects.is_hire_process','NO')
                                                            ->join('clients', function($join)
                                                            {
                                                                $join->on('clients.user_id', '=', 'projects.client_user_id');
                                                            })
                                                            ->select('projects.*','clients.user_id')
                                                            ->with(['category_details'])
                                                            ->count();

        $this->obj_all_projects   = $this->ProjectpostModel->where('projects.project_status','2')
                                                            ->where('projects.bid_closing_date','>',date('Y-m-d'));
                                                            /*if(Sentinel::check() == false){
                                                            $this->obj_all_projects->where('projects.project_type','0');
                                                            }*/
                                                            $this->obj_all_projects = $this->obj_all_projects->where('projects.is_hire_process','NO')
                                                            ->join('clients', function($join)
                                                            {
                                                                $join->on('clients.user_id', '=', 'projects.client_user_id');
                                                            })
                                                            ->select('projects.*','clients.user_id')
                                                            ->with(['category_details']);
        /* common data ends */
        $this->module_url_path = url("/projects");
  }
  /*
  | Comment : Project listing page
  | auther  : Nayan S.
  */
    public function index( $enc_id = FALSE , $obj_search_data = FALSE )
    {
        if($enc_id != FALSE && $enc_id == ""){
            return redirect()->back(); 
        } 
        $this->arr_view_data['page_title'] = trans('controller_translations.page_title_projects');
        $project_count = $this->ProjectpostModel->where('projects.project_status','=','2')
                                                ->whereDate('projects.bid_closing_date','>',date('Y-m-d h:i:s'))
                                                ->where('projects.is_hire_process','NO')
                                                ->count();
        //dd($project_count);                                        
        $obj_projects = $this->ProjectpostModel->where('projects.project_status','=','2')
                                               ->whereDate('projects.bid_closing_date','>',date('Y-m-d h:i:s'));
                                                /*if(Sentinel::check() == false){
                                                $obj_projects->where('projects.project_type','0');
                                                }*/
        $obj_projects = $obj_projects->where('projects.is_hire_process','NO')
                                      ->join('clients', function($join)
                                      {
                                          $join->on('clients.user_id', '=', 'projects.client_user_id');
                                      })
                                      ->select('projects.*','clients.user_id','projects.urjent_heighlight_end_date AS urjent_heighlight_end_date')
                                     // ->select('projects.*','clients.user_id','projects.urjent_heighlight_end_date AS highlight_end_date')
                                      ->with(['skill_details','client_details','category_details','sub_category_details','project_skills.skill_data']);

        $obj_projects = $obj_projects->orderBy('projects.urjent_heighlight_end_date','DESC');
        $obj_projects = $obj_projects->orderBy('projects.created_at','DESC');

                                      

        $user = Sentinel::check();                                     
        if( isset($user) && $user == TRUE  &&  $user->inRole('expert') )
        {
          $user_id = $user->id;
          /*$obj_projects = $obj_projects->with(['favourite_project'=>function ($qry) use($user_id) 
                                     {
                                        $qry->where('expert_user_id','=',$user_id);
                                        $qry->orderBy('id','DESC');
                                     }]);*/
        }
        if($enc_id != FALSE)
        {
          $id = base64_decode($enc_id);
          $obj_projects = $obj_projects->where('category_id',$id);
        }
        if(Session::has('show_job_listing_cnt')) { $pagi_cnt = Session::get('show_job_listing_cnt'); }else { $pagi_cnt = config('app.project.pagi_cnt'); }
        // dd($obj_projects->get()->toArray());
        // $obj_projects = $obj_projects->paginate($pagi_cnt)->appends(request()->query());
        $obj_projects = $obj_projects->get();
        
        $arr_projects = [];

        if($obj_search_data != FALSE)
        {
            $arr_pagination     = clone $obj_search_data;
            $arr_projects       = $obj_search_data->toArray();
            $arr_sidebar_data   = $this->get_sidebar_array();
        }
        else if($obj_projects)
        {
            //$arr_pagination     = clone $obj_projects;
            $arr_projects       = $obj_projects->toArray();
            $obj_all_project    =  $this->make_pagination_links($arr_projects,$pagi_cnt);
            $arr_projects       = $obj_all_project->toArray();
            $arr_sidebar_data   = $this->get_sidebar_array();
        }
        //dd($arr_projects);
        if(isset($arr_projects) && !empty($arr_projects))
        {
            $max_fixed_amt = $this->ProjectpostModel->where('project_pricing_method','1')
                                                    ->where('project_status','2')
                                                    ->whereDate('projects.bid_closing_date','>',date('Y-m-d h:i:s'))
                                                    ->where('projects.is_hire_process','NO')
                                                    ->max('usd_max_project_cost');

            $min_fixed_amt = 0; //$this->ProjectpostModel->where('project_pricing_method','1')
                               //                     ->where('project_status','2')
                                 //                   ->whereDate('projects.bid_closing_date','>',date('Y-m-d h:i:s'))
                                  //                  ->where('projects.is_hire_process','NO')
                                    //                ->min('min_project_cost');

            $max_project_amt   = $this->ProjectpostModel->where('project_pricing_method','2')
                                                        ->where('project_status','2')
                                                        ->whereDate('projects.bid_closing_date','>',date('Y-m-d h:i:s'))
                                                        ->where('projects.is_hire_process','NO')
                                                        ->max('usd_max_project_cost');

            $min_project_amt   = 0; //$this->ProjectpostModel->where('project_pricing_method','2')
                                      //                  ->where('project_status','2')
                                        //                ->whereDate('projects.bid_closing_date','>',date('Y-m-d h:i:s'))
                                          //              ->where('projects.is_hire_process','NO')
                                            //            ->min('min_project_cost');
        }
        //dd($max_project_amt,$min_project_amt,$max_fixed_amt,$min_fixed_amt);
        $obj_skills = $this->SkillsModel->where('is_active',1)->with(['translations'])->get();
        if($obj_skills != FALSE)
        {
            $arr_skills = $obj_skills->toArray();
        }
        // dd($arr_sidebar_data);
        $this->arr_view_data['all_project_count']     = $project_count;
        $this->arr_view_data['max_project_amt']       = $max_project_amt;
        $this->arr_view_data['min_project_amt']       = $min_project_amt;
        $this->arr_view_data['max_fixed_amt']         = $max_fixed_amt;
        $this->arr_view_data['min_fixed_amt']         = $min_fixed_amt;

        $this->arr_view_data['arr_skills']            = $arr_skills;
        $this->arr_view_data['arr_projects']          = $arr_projects;
        $this->arr_view_data['obj_project']           = $obj_all_project;

        $this->arr_view_data['arr_sidebar_data']      = $arr_sidebar_data;
        $this->arr_view_data['all_projects_count']    = $this->all_projects_count;
        //$this->arr_view_data['arr_pagination']        = $arr_pagination;
        $this->arr_view_data['module_url_path']       = $this->module_url_path;
        return view('front.project_listing.listing',$this->arr_view_data);
    }

    public function show_cnt(Request $Request) 
    {
        \Session::put('show_job_listing_cnt' , $Request->input('show_cnt'));
        $preurl = url()->previous();
        $explode_url = explode('?page=',$preurl);
        if(empty($explode_url[1])){
            return redirect()->back();
        }
        else{
            $redirect =  $explode_url[0].'?page='.'1';
            return redirect($redirect);
        }
    } 

    /*
    | Comment : Get side bar array to show on the pages.
    | auther  : Nayan S.
    */
    public function get_sidebar_array()
    {
        $tmp_arr = [];
        $obj_all_projects = $this->obj_all_projects->get(['projects.id','projects.category_id']);
        
        if($obj_all_projects)
        {
          $all_projects = $obj_all_projects->toArray();
          
          foreach ($all_projects as $key => $value) 
          {
            $tmp_arr_project_data = [];

            $tmp_arr_projects = $this->obj_all_projects->get()->toArray();
            $tmp_count = 0;
            foreach ($tmp_arr_projects as  $tmp_key => $tmp_value) 
            { 
              if($value['category_id'] == $tmp_value['category_id'])
              { 
                $tmp_count+=1;
              }
            } 
            $tmp_arr_project_data['project_count']  = $tmp_count;
            $tmp_arr_project_data['category_title'] = isset($value['category_details']['category_title'])?$value['category_details']['category_title']:'';
            $tmp_arr_project_data['category_slug']  = isset($value['category_details']['category_slug'])?$value['category_details']['category_slug']:'';
            $tmp_arr_project_data['category_id']    = $value['category_id'];
            array_push($tmp_arr,$tmp_arr_project_data);
          }
        }
        $arr_sidebar_data = [];
        if(count($tmp_arr)>0)
        {
          foreach ($tmp_arr as $key => $value) 
          {
            if(count($arr_sidebar_data) > 0)
            {
              if(!in_array($value,$arr_sidebar_data))
              {
                array_push($arr_sidebar_data,$value);  
              }
            }
            else 
            {
              array_push($arr_sidebar_data,$value);
            }
          } 
        }
        /* To sort array in alphabetically */
        $_sortBy = [];
        foreach ($arr_sidebar_data as $key => $row){
            $_sortBy[$key] = $row['category_title'];
        }
        array_multisort($_sortBy,SORT_ASC,$arr_sidebar_data);
        return $arr_sidebar_data;
    }
    /*
      Comment:  Show project Details. 
      Author :  Nayan Sonawane
    */  
    public function show_project_details($id){
      $id   = base64_decode($id);
      if($id==""){
        return redirect()->back();
      }
      $user = Sentinel::check();     
      $this->arr_view_data['page_title'] = trans('controller_translations.page_title_project_details');
      $obj_project_info = $this->ProjectpostModel
                               ->join('clients', function($join){
                                    $join->on('clients.user_id', '=', 'projects.client_user_id');
                                })
                               ->select('projects.*','clients.user_id')
                               ->with('skill_details','client_info','project_skills.skill_data','client_details','category_details','project_bids_infos','project_bid_info','sub_category_details','project_post_documents')
                               ->where('projects.id',$id)
                               ->first();

      $arr_project_info = array();
      if($obj_project_info != FALSE) {
        $arr_project_info   = $obj_project_info->toArray();
      } else {
        Session::flash('error',trans('controller_translations.project_details_not_available'));
        return redirect()->back();
      }
      if($arr_project_info['project_type'] == 1 && Sentinel::check() == false || $arr_project_info['project_type'] == 1 && Sentinel::check() != false && $user->inRole('client') && $arr_project_info['client_user_id'] !=$user->id){
        Session::flash('error',trans('controller_translations.private_project_note'));
        return redirect()->back();
      }
      $arr_sidebar_data   = $this->get_sidebar_array();
      $obj_skills = $this->SkillsModel->where('is_active',1)->with(['translations'])->get();
      if($obj_skills != FALSE) {
          $arr_skills = $obj_skills->toArray();
      }
      
      if(isset($arr_project_info) && !empty($arr_project_info))
      {
          $max_fixed_amt = $this->ProjectpostModel->where('project_pricing_method','1')
                                                    ->where('project_status','2')
                                                    ->whereDate('projects.bid_closing_date','>',date('Y-m-d h:i:s'))
                                                    ->where('projects.is_hire_process','NO')
                                                    ->max('usd_max_project_cost');

            $min_fixed_amt    = 0; //$this->ProjectpostModel->where('project_pricing_method','1')
                                    //                ->where('project_status','2')
                                     //               ->whereDate('projects.bid_closing_date','>',date('Y-m-d h:i:s'))
                                      //              ->where('projects.is_hire_process','NO')
                                       //             ->min('min_project_cost');

            $max_project_amt   = $this->ProjectpostModel->where('project_pricing_method','2')
                                                        ->where('project_status','2')
                                                        ->whereDate('projects.bid_closing_date','>',date('Y-m-d h:i:s'))
                                                        ->where('projects.is_hire_process','NO')
                                                        ->max('usd_max_project_cost');

            $min_project_amt   = 0; //$this->ProjectpostModel->where('project_pricing_method','2')
                                    //                    ->where('project_status','2')
                                     //                   ->whereDate('projects.bid_closing_date','>',date('Y-m-d h:i:s'))
                                      //                  ->where('projects.is_hire_process','NO')
                                       //                 ->min('min_project_cost');
      }

      $project_count = $this->ProjectpostModel->where('projects.project_status','=','2')
                                                ->whereDate('projects.bid_closing_date','>',date('Y-m-d h:i:s'))
                                                ->where('projects.is_hire_process','NO')
                                                ->count();


      $this->arr_view_data['all_project_count']              = $project_count; 
      $this->arr_view_data['max_fixed_amt']                  = $max_fixed_amt;
      $this->arr_view_data['min_fixed_amt']                  = $min_fixed_amt;
      $this->arr_view_data['max_project_amt']                = $max_project_amt;
      $this->arr_view_data['min_project_amt']                = $min_project_amt;
      $this->arr_view_data['arr_skills']                     = $arr_skills;
      $this->arr_view_data['projectInfo']                    = $arr_project_info;
      $this->arr_view_data['arr_sidebar_data']               = $arr_sidebar_data;
      $this->arr_view_data['all_projects_count']             = $this->all_projects_count;

      $this->arr_view_data['module_url_path']                = $this->module_url_path;
      $this->arr_view_data['project_attachment_base_path']   = $this->project_attachment_base_path;
      $this->arr_view_data['project_attachment_public_path'] = $this->project_attachment_public_path;
      

      return view('front.project_listing.project_details',$this->arr_view_data);
    }
    /*
      Comment:  Show Searched result. 
      Author :  Nayan Sonawane
    */  

    public function make_zip($project_id=FALSE)
    {
        if($project_id == FALSE){
          Session::flash('error', trans('controller_translations.something_went_wrong_please_try_again_later')); 
          return redirect()->back();
        }

        $project_main_attachment        = [];
        $project_attachment             = [];
        $arr_project                    = [];
        $project_id                     = base64_decode($project_id); 

        $obj_project_main_attachment    = $this->ProjectPostDocumentsModel
                                                                      ->where('project_id',$project_id)
                                                                      ->get();

        if($obj_project_main_attachment)
        {
          $project_main_attachment = $obj_project_main_attachment->toArray(); 
        }
        
        $obj_project = $this->ProjectpostModel
                                            ->select('project_name')
                                            ->where('id',$project_id)
                                            ->first();
        if($obj_project)
        {          
          $arr_project = $obj_project->toArray();
        }

        $project_name = isset($arr_project['project_name']) ? $arr_project['project_name'] : '';

        $files = []; 
        if($project_main_attachment)
        {
            if($project_main_attachment)
            {
              foreach($project_main_attachment as $key => $attch)
              {
                if(file_exists(base_path().'/public/uploads/front/postprojects/'.$attch['image_name']))
                {
                  $files[] = public_path('uploads/front/postprojects/'.$attch['image_name']);
                }
              }
            }

            if(isset($files) && sizeof($files) > 0)
            {
              $project_name  = str_slug($project_name);

              # create new zip opbject
              $zip = new ZipArchive();

              # create a temp file & open it

              // $tmp_file = tempnam('.','');
              
              $tmp_file = tempnam("/tmp", "");

              $zip->open($tmp_file, ZipArchive::CREATE);
              foreach($files as $file)
              {
                $download_file = file_get_contents($file);
                $zip->addFromString(basename($file),$download_file);
              }

              $zip->close();
              
              header('Content-disposition: attachment; filename='.$project_name.'-'.date('Y-m-d h-i-s').'.zip');
              header('Content-type: application/zip');
              readfile($tmp_file);
              return redirect()->back();
            } 
            else
            {
              Session::flash('error', 'No files found for creation of a ZIP file.'); 
              return redirect()->back();
            }        
        }
        else 
        {
          Session::flash('error', 'No files found for creation of a ZIP file.'); 
          return redirect()->back();
        }
    }

    public function download_zip($zipname=FALSE)
    {
        if(file_exists('public/uploads/front/postprojects/zipfiles/'.$zipname))
        {
          return response()->download(public_path('/uploads/front/postprojects/zipfiles/'.$zipname));
          unlink(public_path('/uploads/front/postprojects/zipfiles/'.$zipname));
        }
        else
        {
          return false;
        }
    }


    public function search_project(Request $request)
    { 
      $this->arr_view_data['page_title'] = trans('controller_translations.page_title_projects');
      $form_data    = [];
      $obj_all_project = '';
      $arr_projects = $arr_pagination = $arr_sidebar_data = $search_keys = $arr_subcategory = $arr_skills =  [];
      $project_count = $min_fixed_amt = $max_fixed_amt = $min_project_amt = $max_project_amt = 0;
      $form_data    = $request->all();

      $arr_settings = $this->SiteSettingModel->where('site_settting_id',1)->first();

      if($arr_settings)
      {
          $arr_settings = $arr_settings->toArray(); 
      }
      $form_data['default_currency']      = isset($arr_settings['default_currency'])?$arr_settings['default_currency']:'';
      $form_data['default_currency_code'] = isset($arr_settings['default_currency_code'])?$arr_settings['default_currency_code']:'';
      
      
      $search_result        = $this->ProjectSearchService->make_filer_search($form_data);

     /* if($search_result == false)
      {
        return redirect('projects');
      }*/

      if($search_result!='')
      {
          $category_id = '';
          if(isset($form_data['category']) && is_array($form_data['category']))
          {
            $category_id = '';
          }
          elseif(isset($form_data['category']))
          {
            $category_id = base64_decode($form_data['category']);
          }

          $ref_search_result    = $search_result->with(['skill_details','client_info','client_details','sub_category_details','category_details','project_skills.skill_data','project_bid_count','favourite_project']);

          $user = Sentinel::check();  

          if(isset($user) && $user == TRUE &&  $user->inRole('expert') )
          {
            $user_id = $user->id;
            $ref_search_result = $ref_search_result->with(['favourite_project'=>function ($qry) use($user_id) 
                                       {
                                          $qry->where('expert_user_id','=',$user_id);
                                          $qry->orderBy('id','DESC');
                                       }]);
          }
          if(Session::has('show_job_listing_cnt')) 
          { 
            $pagi_cnt = Session::get('show_job_listing_cnt'); 
          } 
          else 
          {
            $pagi_cnt = config('app.project.pagi_cnt'); 
          }
          //$obj_projects = $ref_search_result->paginate($pagi_cnt);

          //$arr_pagination     = clone $obj_projects;
          $search_result      = $ref_search_result->get();
          $arr_projects       = $search_result->toArray();
          //dump($arr_projects);
          if(isset($form_data['min_fixed_amt']) && $form_data['min_fixed_amt']>=0 && isset($form_data['max_fixed_amt']) && $form_data['max_fixed_amt']>=0 && isset($form_data['min_project_amt']) && $form_data['min_project_amt']>=0 && isset($form_data['max_project_amt']) && $form_data['max_project_amt']>=0)
          {
            $arr_projects = $this->filter_currency_wise($arr_projects,$form_data);  
          }
          // if ((isset($form_data['min_fixed_amt']) && isset($form_data['max_fixed_amt'])) && sizeof($form_data['fixed_rate'])>0) ||  (isset($form_data['hourly_rate']) && sizeof($form_data['hourly_rate'])>0)) 
          // {
          //     $arr_projects = $this->filter_currency_wise($arr_projects,$form_data);   
          // }

          $obj_all_project  =  $this->make_pagination_links($arr_projects,$pagi_cnt);
          $arr_projects     = $obj_all_project->toArray();
          if(isset($arr_projects) && !empty($arr_projects))
          {
                $max_fixed_amt = $this->ProjectpostModel->where('project_pricing_method','1')
                                                        ->where('project_status','2')
                                                        ->whereDate('projects.bid_closing_date','>',date('Y-m-d h:i:s'))
                                                        ->where('projects.is_hire_process','NO')
                                                        ->max('usd_max_project_cost');

                $min_fixed_amt = 0; //$this->ProjectpostModel->where('project_pricing_method','1')
                                    //                    ->where('project_status','2')
                                    //                    ->whereDate('projects.bid_closing_date','>',date('Y-m-d h:i:s'))
                                    //                    ->where('projects.is_hire_process','NO')
                                    //                    ->min('min_project_cost');

                $max_project_amt   = $this->ProjectpostModel->where('project_pricing_method','2')
                                                            ->where('project_status','2')
                                                            ->whereDate('projects.bid_closing_date','>',date('Y-m-d h:i:s'))
                                                            ->where('projects.is_hire_process','NO')
                                                            ->max('usd_max_project_cost');

                $min_project_amt   = 0; //$this->ProjectpostModel->where('project_pricing_method','2')
                                        //                    ->where('project_status','2')
                                        //                    ->whereDate('projects.bid_closing_date','>',date('Y-m-d h:i:s'))
                                        //                    ->where('projects.is_hire_process','NO')
                                        //                    ->min('min_project_cost');
          }
          //dd($arr_projects);
          //dd($max_fixed_amt);
          $arr_sidebar_data   = $this->get_sidebar_array();


          $obj_skills = $this->SkillsModel->where('is_active',1)->with(['translations'])->get();
          if($obj_skills != FALSE){
              $arr_skills = $obj_skills->toArray();
          }

          $arr_subcategory = [];
          $obj_subcategory = $this->SubCategoriesModel->where('category_id',$category_id)->where('is_active','=','1')->get();
          if($obj_subcategory)
          {
            $arr_subcategory = $obj_subcategory->toArray();
          }
          $project_count = $this->ProjectpostModel->where('projects.project_status','=','2')
                                                    ->whereDate('projects.bid_closing_date','>',date('Y-m-d h:i:s'))
                                                    ->where('projects.is_hire_process','NO')
                                                    ->count();
      }

      //dd($max_fixed_amt);
      $this->arr_view_data['all_project_count']     = $project_count; 
      $this->arr_view_data['min_fixed_amt']         = $min_fixed_amt; 
      $this->arr_view_data['max_fixed_amt']         = $max_fixed_amt; 
      $this->arr_view_data['min_project_amt']       = $min_project_amt; 
      $this->arr_view_data['max_project_amt']       = $max_project_amt; 
      $this->arr_view_data['arr_subcategory']       = $arr_subcategory; 
      $this->arr_view_data['arr_skills']            = $arr_skills; 
      $this->arr_view_data['arr_projects']          = $arr_projects;
      $this->arr_view_data['obj_project']           = $obj_all_project;
      $this->arr_view_data['arr_sidebar_data']      = $arr_sidebar_data;
      $this->arr_view_data['arr_pagination']        = $arr_pagination;
      $this->arr_view_data['all_projects_count']    = $this->all_projects_count;
      $this->arr_view_data['module_url_path']       = $this->module_url_path;
      //dd($this->arr_view_data);
      
      return view('front.project_listing.listing',$this->arr_view_data);
    }

    public function filter_currency_wise($arr_data,$form_data)
    {
        $arr_data_new = $arr_rates = [];
        
        if(isset($form_data) && !empty($form_data))
        {
            $min_fixed_amt = isset($form_data['min_fixed_amt']) ? $form_data['min_fixed_amt'] : 0;
            $max_fixed_amt = isset($form_data['max_fixed_amt']) ? $form_data['max_fixed_amt'] : 0;
            $min_project_amt = isset($form_data['min_project_amt']) ? $form_data['min_project_amt'] : 0;
            $max_project_amt = isset($form_data['max_project_amt']) ? $form_data['max_project_amt'] : 0;

            if(isset($arr_data) && sizeof($arr_data)>0)
            {
                foreach ($arr_data as $key => $value) 
                {

                    if( ($value['project_pricing_method'] == '1') && ($value['usd_max_project_cost']>=$form_data['min_fixed_amt'] && $value['usd_max_project_cost']<=$form_data['max_fixed_amt']))
                    {
                        $arr_data_new[]= $value;
                    }

                    if(($value['project_pricing_method'] == '2') && ($value['usd_max_project_cost']>=$form_data['min_project_amt'] && $value['usd_max_project_cost']<=$form_data['max_project_amt']))
                    {
                        $arr_data_new[]= $value;
                    }
                }
            }
        }
        return $arr_data_new;

    }

    /* function to give closest search result first */
    public function show_closest_search_result($cmp_term = "", $arr_projects = [] , $arr_key_to_match )
    { 
      $arr_tmp  = [];

      if($cmp_term != "")
      {
        /* remove space from a string */

        $arr_cmp_terms = [];
        $arr_cmp_terms = explode(' ', $cmp_term );
        $cmp_term = str_replace(' ', '', $cmp_term);

        foreach ( $arr_projects as $_key => $project ) 
        {

          $project_name = $project['project_name'];

          foreach ($arr_cmp_terms as $term_key => $term){
          }
          /* compare the string to get the unmached character count */
          $pos = strpos($str_one, $str_two);
          if ($pos === false) 
          {
            $project['str_position'] = abs($pos);      
          } 
          else 
          {
            $project['str_position'] = abs($pos);      
          }
          $unmatched_chars = strcasecmp( $str_one, $str_two );
          $project['unmatched_chars'] = abs($unmatched_chars);  
          array_push( $arr_tmp, $project );
        }
      }
      /* sorting an array by its key */
      $_sortBy = [];
      foreach ($arr_tmp as $key => $row){
          //$_sortBy[$key] = $row['unmatched_chars'];
          $_sortBy[$key] = $row['str_position'];
      }
      array_multisort($_sortBy,SORT_DESC,$arr_tmp);
    }

    /*function for sort projects array with index value */
    public function _sortBy()
    {
        $args = func_get_args();
        $data = array_shift($args);
        foreach ($args as $n => $field) 
        {
            if (is_string($field))
            {
                $tmp = array();
                foreach ($data as $key => $row)
                    $tmp[$key] = $row[$field];
                $args[$n] = $tmp;
            }
        }
        $args[] = &$data;
        call_user_func_array('array_multisort', $args);
        return array_pop($args);
    }

    /*function for sort projects array with budget value*/

    public function _orderByBudget()
    {
        $args = func_get_args();
        $data = array_shift($args);
        foreach ($args as $n => $field) 
        {
            if (is_string($field))
            {
                $tmp = array();
                foreach ($data as $key => $row)
                {
                  $tmp[$key] = (int)$row[$field];
                }

                $args[$n] = $tmp;
            }
        }
        $args[] = &$data;
        call_user_func_array('array_multisort', $args);
        return array_pop($args);
    }
    /*
      Comments : To Add or Remove project from favourites.
      Auther : Nayan S.
    */
    public function add_or_remove_favourite(Request $request)
    {
        $user = Sentinel::check();

        if(!$user)
        {
          return redirect('/login');
        }
        
        $json = $result_favourite = [];
        $action = "";

        $expert_user_id  = $user->id;

        $project_id  = base64_decode($request->input('project_id'));
        $action  = base64_decode($request->input('action'));
    
        if($action == 'add')
        {
          $result_favourite = $this->SubscriptionService->validate_favourite_projects($project_id);

          if(count($result_favourite) > 0)
          { 
              $message = isset($result_favourite['msg'])?$result_favourite['msg']: trans('controller_translations.error_while_favoriting_project');
              Session::flash('error',$message);
              $json['status']   = "ERROR"; 
              return response()->json($json);
          }
        }

        if($project_id != "")
        {
            $obj_fav = $this->FavouriteProjectsModel->where(array('expert_user_id'=>$expert_user_id,'project_id'=>$project_id))->first();
            
            if($obj_fav)
            { 
                $un_favorite = $this->FavouriteProjectsModel->where(array('expert_user_id'=>$expert_user_id,'project_id'=>$project_id))->delete();
                
                if( $un_favorite == TRUE && $action="remove" )
                {
                  Session::flash('success',trans('controller_translations.success_project_successfully_removed_from_favourites'));
                  $json['status']   = "UNFAVOURITE"; 
                }
                else
                {
                  Session::flash('error',trans('controller_translations.error_problem_occured_while_removing_project_from_favourites'));
                }
            }
            else
            {
                $favorite = $this->FavouriteProjectsModel->create(array('expert_user_id'=>$expert_user_id,'project_id'=>$project_id));
                
                if($favorite && $action="add" )
                {
                  Session::flash('success',trans('controller_translations.success_project_successfully_added_to_favourites'));
                  $json['status']   = "FAVOURITE"; 
                }
                else
                {
                  Session::flash('error',trans('controller_translations.error_problem_occured_while_adding_project_to_favourites'));
                }
            } 
        }

        return response()->json($json);
    }


    /* used in auth controller for checking login */

    public function browse_skills($enc_id)
    {
      $skill_id = base64_decode($enc_id);
      if($skill_id == "")
      {
        return redirect()->back();
      }
      /* Used function from service */

      $obj_search_data = $this->ProjectSearchService->search_with_skill_id($skill_id);

      return $this->index( $enc_id = FALSE ,$obj_search_data);
    }

    /* common function to show function projects of perticular user for sutocomplete */

    public function project_search_for_autocomplete(Request $request)
    { 
      $user = Sentinel::check();
      if(isset($user) && $user != FALSE)
      {
        $arr_projects = [];
        $term         = $request->input('term');
        $obj_result   = $this->ProjectpostModel->where('project_name','like',"%".$term."%");
        /* taking user role into consideration forshowing projecs */
        if($user->inRole('project_manager'))
        {
          $obj_result = $obj_result->where('project_manager_user_id','=',$user->id); 
        }  
        else if ($user->inRole('client'))
        {
          $obj_result = $obj_result->where('client_user_id','=',$user->id); 
        }
        else if ($user->inRole('expert'))
        { 
          $obj_result = $obj_result->where('expert_user_id','=',$user->id); 
        } 

        $segment_two    = \Request::segment(2);
        $segment_three  = \Request::segment(3);
        $obj_result = $obj_result->get();
        if($obj_result)
        {
          foreach ($obj_result->toArray() as $key => $value) 
          {
            if(isset($value['project_name']) &&  $value['project_name'] != "")
            { 
              $arr_projects[$key]['label'] = $value['project_name'];
              $arr_projects[$key]['id']    = $value['id'];
            } 
          }
        }
      }
    }

    public function get_autocomplete_data(Request $request)
    {
      $arr_projects = $arr_skill = [];
      $term         = $request->input('term');
      $locale =  \App::getLocale();

     $obj_result   = $this->ProfessionModel->whereHas('profession_traslation',function ($query) use($term,$locale) {
                                              $query->where('profession_title','like',"%".$term."%");
                                              $query->where('locale',$locale);
                                            })
                                            ->with(['profession_traslation'=> function ($query) use($term,$locale) {
                                               $query->where('profession_title','like',"%".$term."%");
                                               $query->where('locale',$locale);
                                            }])->get();
                                            

      /*$obj_result   = $this->ProfessionModel->whereHas('profession_traslation',function ($query) use($term,$locale) {
                                              $query->where('profession_title','like',"%".$term."%");
                                            })
                                            ->with(['profession_traslation'=> function ($query) use($term,$locale) {
                                               $query->where('profession_title','like',"%".$term."%");
                                            }])->get();*/


      if($obj_result)
      {
        foreach ($obj_result->toArray() as $key => $value) 
        {
         
          if($locale == 'en') 
          {
            if(isset($value['profession_traslation'][0]['profession_title'])){ 
              $arr_skill[$key]['label'] = $value['profession_traslation'][0]['profession_title'];
              $arr_skill[$key]['id']    = $value['id'];
            } 
          } 
          else if($locale == 'de') 
          {
            if(isset($value['profession_traslation'][1]['profession_title'])){ 
              $arr_skill[$key]['label'] = $value['profession_traslation'][1]['profession_title'];
              $arr_skill[$key]['id']    = $value['id'];
            }
          }  
        }
      }

      echo json_encode( $arr_skill );
    }

    public function subcatdata(Request $request)
    {  
        $id                = $request->id;
        $arr_subcategories = array();
        //$arr_lang          = $this->LanguageService->get_all_language();
        $obj_subcategories = $this->SubCategoriesModel->with('category_details.translations')->where('category_id',$id)->get();

        if($obj_subcategories!=FALSE)
        {
          $arr_subcategories =  $obj_subcategories->toArray();
        }  

        echo '<option value="">'.trans('client/projects/post.text_select_sub_category').'</option>';
        foreach($arr_subcategories as $arr_option){
          $id = $arr_option['id'];
          $subcat_name = $arr_option['subcategory_title'];
          echo '<option value="'.$id.'">'.$subcat_name.'</option>';
        }     
        exit ();
    }

    public function make_pagination_links($items,$perPage)
    {
        $pageStart = \Request::get('page', 1);
        $offSet = ($pageStart * $perPage) - $perPage; 
        $itemsForCurrentPage = array_slice($items, $offSet, $perPage, true);

        return new LengthAwarePaginator($itemsForCurrentPage, count($items), $perPage,Paginator::resolveCurrentPage(), array('path' => Paginator::resolveCurrentPath()));
    }

} // end class