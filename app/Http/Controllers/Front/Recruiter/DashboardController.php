<?php

namespace App\Http\Controllers\Front\Recruiter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\ProjectpostModel;

use Sentinel;
use Validator;
use Session;

class DashboardController extends Controller
{
    public $arr_view_data;
    public function __construct(  ProjectpostModel $projectpost
                                )
    {
      $this->arr_view_data = [];

      if(! $user = Sentinel::check()) 
      {
        return redirect('/login');
      }

      if(!$user->inRole('recruiter')) 
      {
        return redirect('/login'); 
      }

      $this->user_id = $user->id;

      $this->ProjectpostModel   = $projectpost;

      $this->view_folder_path   = 'recruiter/dashboard';
      $this->module_url_path    = url("/recruiter");
    }

    /* 
      Auther : Nayan S.
    */

    public function show_dashboard()
    {   
      
      /*Finding Completed projects count*/
      $count_completed_projects = $this->ProjectpostModel->where('project_recruiter_user_id',$this->user_id)
                                                       ->where('project_status','4')
                                                       ->count();    

      /*Finding Open projects count*/
      $count_open_projects = $this->ProjectpostModel->where('project_recruiter_user_id',$this->user_id)
                                                     ->where('project_status','=','2')
                                                     ->count();

     
      $this->arr_view_data['page_title']      = trans('controller_translations.page_title_dashboard');
      $this->arr_view_data['count_completed_projects']  = $count_completed_projects;
      $this->arr_view_data['count_open_projects']       = $count_open_projects;
      $this->arr_view_data['module_url_path'] = $this->module_url_path;
      return view($this->view_folder_path.'.dashboard',$this->arr_view_data);
   }


}
