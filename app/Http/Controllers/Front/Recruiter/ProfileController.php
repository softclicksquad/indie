<?php

namespace App\Http\Controllers\Front\Recruiter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\UserModel;
use App\Models\CountryModel; 
use App\Models\RecruiterModel;
use App\Models\ClientsModel;
use App\Models\ProjectpostModel;

use App\Common\Services\LanguageService; 
use Validator;
use Session;
use Sentinel;

class ProfileController extends Controller
{
    public $arr_view_data;
    public function __construct(UserModel $user_model,
                                RecruiterModel $recruiter,
                                CountryModel $countries,
                                ClientsModel $clients,
                                ProjectpostModel $projectpost,
                                LanguageService $langauge )
    { 

      if(!$user = Sentinel::check()) 
      {
        return redirect('/login')->send();
      }

      $this->user_id = $user->id;

      $this->UserModel            = $user_model;
      $this->CountryModel         = $countries;
      $this->LanguageService      = $langauge;
      $this->ProjectpostModel     = $projectpost;
      $this->ClientsModel         = $clients;
      $this->RecruiterModel  = $recruiter;

      /* Base Model is the Project Manager model */
      $this->BaseModel = $this->RecruiterModel;            

      $this->arr_view_data    = [];
      $this->module_url_path  = url("/recruiter/profile");
      $this->profile_img_base_path = base_path() . '/public'.config('app.project.img_path.profile_image');
      $this->profile_img_public_path = url('/').config('app.project.img_path.profile_image');
    }

    /*
    Profile: load page of ecpert profile
    Author: Sagar Sainkar
    */

    public function index()
    {
      //get all countries here
      $arr_countries = array();
      $arr_skills = array();
      $obj_countries = $this->CountryModel->orderBy('country_name', 'asc')->get();

      if($obj_countries != FALSE)
      {
          $arr_countries = $obj_countries->toArray();
      }

      $obj_project_manager = $this->BaseModel->where('user_id',$this->user_id)
                                                     ->with(['user_details','country_details.states','state_details.cities','city_details'])->first();

      $arr_project_manager = [];
      if($obj_project_manager != FALSE)
      {
        $arr_project_manager = $obj_project_manager->toArray();
      }
      //dd($arr_project_manager);

      $this->arr_view_data['arr_project_manager'] = $arr_project_manager;
      $this->arr_view_data['arr_countries']       = $arr_countries;
      $this->arr_view_data['arr_lang']            = $this->LanguageService->get_all_language();
      $this->arr_view_data['page_title']          = trans('controller_translations.page_title_project_manager_profile');
      $this->arr_view_data['module_url_path']     = $this->module_url_path;

      return view('recruiter.profile.index',$this->arr_view_data);
    }

    
    public function update(Request $request)
    {
        $arr_rules = array();
        $status = FALSE;
 
        /*$arr_rules['first_name']    = "required|alpha|max:255";
        $arr_rules['last_name']     = "required|alpha|max:255";*/
        $arr_rules['first_name']    = "required|max:255";
        $arr_rules['last_name']     = "required|max:255";
        $arr_rules['phone_number']  = "required|max:17|min:10";
        $arr_rules['country']       = "required";
        $arr_rules['state']         = "required";
        $arr_rules['phone_code']    = "required|min:2";
        $arr_rules['city']          = "required";
        $arr_rules['zip']           = "required|max:10";
        $arr_rules['address']       = "required|max:450";
        
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = [];
        $form_data = $request->all(); 

        //dd($form_data);
        
        $obj_project_manager = $this->BaseModel->where('user_id',$this->user_id)->first();

        if($obj_project_manager && sizeof($obj_project_manager) > 0)
        {
          $arr_project_manager = [];
               
          if(isset($form_data['profile_image']) && $form_data['profile_image']!=FALSE)
          {    
              if ($request->hasFile('profile_image')) 
              {
                $img_valiator = Validator::make(array('image'=>$request->file('profile_image')),array(
                                                'image' => 'mimes:png,jpeg,jpg')); 

                  if ($request->file('profile_image')->isValid() && $img_valiator->passes())
                  {
                      $profile_image_name = $form_data['profile_image'];
                      $fileExtension = strtolower($request->file('profile_image')->getClientOriginalExtension()); 

                      if($fileExtension == 'png' || $fileExtension == 'jpg' || $fileExtension == 'jpeg')
                      {
                          $profile_image_name = sha1(uniqid().$profile_image_name.uniqid()).'.'.$fileExtension;
                          $request->file('profile_image')->move($this->profile_img_base_path, $profile_image_name);
                          $arr_project_manager['profile_image'] = $profile_image_name;

                          //unlink exiting image
                          if ($profile_image_name && isset($obj_expert->profile_image) && $obj_expert->profile_image!="") 
                          {
                              if ($obj_expert->profile_image!='defaul_profile_image.png') 
                              {
                                  $file_exits = file_exists($this->profile_img_base_path.$obj_expert->profile_image);

                                  if ($file_exits) 
                                  {
                                      unlink($this->profile_img_base_path.$obj_expert->profile_image);
                                  }
                              }
                          }
                      }
                      else
                      {
                           Session::flash('error',trans('controller_translations.error_invalid_file_extension_for_profile_image'));
                           return back()->withInput();
                      }
                  }
                  else
                  {
                       Session::flash("error",trans('controller_translations.error_please_upload_valid_image'));
                       return back()->withErrors($validator)->withInput($request->all());
                  }
                      
              }
          }

           $arr_project_manager['first_name']    = ucfirst($form_data['first_name']);
           $arr_project_manager['last_name']     = ucfirst($form_data['last_name']);
           $arr_project_manager['phone_code']    = $form_data['phone_code'];
           $arr_project_manager['phone']         = $form_data['phone_number'];
           $arr_project_manager['country']       = $form_data['country'];
           $arr_project_manager['state']         = $form_data['state'];
           $arr_project_manager['city']          = $form_data['city'];
           $arr_project_manager['zip']           = $form_data['zip'];
           $arr_project_manager['address']       = $form_data['address'];
        
           $status = $this->BaseModel->where('user_id',$this->user_id)->update($arr_project_manager);

        }

        if ($status) 
        {
            /*if(isset($form_data['user_name']))
            {
               $this->UserModel->where('id',$this->user_id)->update(['user_name'=>$form_data['user_name']]);
            }*/
            Session::flash('success',trans('controller_translations.success_profile_details_updated_successfully'));
        }
        else
        {
            Session::flash('error',trans('controller_translations.error_while_updating_profile_details'));
        }
        
        return redirect()->back();
    }

   /*
    | Change pass : load view for change password
    | auther :Sagar Sainkar
    */
    public function change_password()
    {
      $this->arr_view_data['page_title'] = trans('controller_translations.page_title_expert_change_password');
      return view('recruiter.profile.change_password',$this->arr_view_data);
    }

    /*
    | description : validate and update expert password
    | auther :Sagar Sainkar
    */ 

    public function update_password(Request $request)
    {
        $arr_rules = array();
        $arr_rules['current_password'] = "required";
        $arr_rules['password']         = 'required|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/';
        $arr_rules['confirm_password'] = 'required|same:password|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/';

        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        
        $user = Sentinel::check();
        
        $credentials = array();
        $password = trim($request->input('current_password'));
        $credentials['email'] = $user->email;        
        $credentials['password'] = $password;

        if (Sentinel::validateCredentials($user,$credentials)) 
        { 
          $new_credentials = [];
          $new_credentials['password'] = $request->input('password');

          if(Sentinel::update($user,$new_credentials))
          {
            Session::flash('success', trans('controller_translations.success_password_change_successfully'));
          }
          else
          {
            Session::flash('error', trans('controller_translations.error_problem_occured_while_changing_password'));
          }          
        } 
        else
        {
          Session::flash('error', trans('controller_translations.error_your_current_password_is_invalid'));          
        }       
        
        return redirect()->back(); 
    }

     /*
     Profile: load page of availability
     Author: Ashwini K
    */

     public function availability()
    {
      $arr_proj_manager_details = array();

      $obj_proj_manager = $this->UserModel->where('id',$this->user_id)->first(['is_available']);

      if ($obj_proj_manager!=FALSE) 
      {
          $arr_proj_manager_details = $obj_proj_manager->toArray();
      }

      $this->arr_view_data['page_title'] = trans('controller_translations.page_title_project_manager_availability');
      $this->arr_view_data['arr_proj_manager_details'] = $arr_proj_manager_details;
      $this->arr_view_data['module_url_path'] = $this->module_url_path;

      return view('recruiter.availability.index',$this->arr_view_data);
    }

    /*
     Profile: Update availability
     Author: Ashwini K
    */

     public function update_availability(Request $request)
    {
        $arr_rules = array();
        $arr_rules['user_availability']= "required";

        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = array();
        $form_data = $request->all();

        if(isset($form_data['user_availability']))
        {    
          $obj_project_manager = $this->UserModel->where('id',$this->user_id)->first();
          if($obj_project_manager!=FALSE)
          {
             if ($form_data['user_availability']==1) 
            {
              $status = $obj_project_manager->update(['is_available'=>1]);
            }
            elseif ($form_data['user_availability']==0) 
            {
              $status = $obj_project_manager->update(['is_available'=>0]); 
            }
          }
          if($status!=false) 
          {
            Session::flash('success',trans('controller_translations.success_availability_updated_successfully'));
          }
          else
          {
            Session::flash('error',trans('controller_translations.error_while_updating_availability'));
          } 
        }

        return redirect()->back(); 

     }   


     /* 
      Comments : Delete images from database and from folder.
      Auther   : Nayan S.
     */

     public function delete_image(Request $request)
     {
        $arr_project_manager = $json = [];
        $obj_project_manager = $this->RecruiterModel->where('user_id',$this->user_id)->first(['id','user_id','profile_image']);
        if($obj_project_manager)
        {
          $arr_project_manager = $obj_project_manager->toArray();  
          $profile_image = isset($arr_project_manager['profile_image'])?trim($arr_project_manager['profile_image']):'';
          if($profile_image != "")
          {
            $result = $this->RecruiterModel->where('user_id',$this->user_id)->update(['profile_image'=>'']);
            if($result)
            {
              $json['status'] = 'SUCCESS_PROFILE';
              $json['msg']  = trans('controller_translations.msg_profile_image_deleted_successfully');
              @unlink($this->profile_img_base_path.$profile_image);
            }
          }
        }
        return response()->json($json);
     }

    /*
      Comments : Show Project Manager portfolo. 
      Auther   : Nayan S.
    */ 

    public function show_portfolio($enc_id)
    {
       $arr_project_manager  = array();
        
       if($enc_id!=FALSE && $enc_id!="")
       {
         $project_manager_id = base64_decode($enc_id);

         $obj_project_manager    = $this->BaseModel->where('user_id',$project_manager_id)
                                             ->with(['user_details','country_details','city_details'])
                                             ->first();
         
         $cnt_projects  = $this->ProjectpostModel->where('client_user_id',$project_manager_id)
                                                 ->count();
         
         if($obj_project_manager!=FALSE)
         {
            $arr_project_manager      = $obj_project_manager->toArray();
         }
       }



        $this->arr_view_data['page_title']               =  trans('controller_translations.project_manager_portfolio');// trans('controller_translations.page_title_expert_portfolio');
        $this->arr_view_data['arr_project_manager']      =  $arr_project_manager;
        $this->arr_view_data['cnt_projects']             =  $cnt_projects;
        $this->arr_view_data['profile_img_public_path']  =  $this->profile_img_public_path;

        return view('recruiter.profile.view_portfolio',$this->arr_view_data);
    }


    public function store_profile_image(Request $request)
    {
        $image_upload  = FALSE;
        $arr_response  = [];
        $user_id       = Sentinel::getUser()->id;
        $obj_user      = $this->BaseModel->where('user_id','=',$user_id)->first(['id','user_id','profile_image']);
        if($obj_user)
        {
          $profile_image_name =  $obj_user->profile_image;
        }

        if($request->hasFile('file')) 
        {
            $image_validation = Validator::make(array('file'=>$request->file('file')),
                                                array('file'=>'mimes:jpg,jpeg,png'));

            if($request->file('file')->isValid() && $image_validation->passes())
            {

                $image_path         =   $request->file('file')->getClientOriginalName();
                $image_extention    =   $request->file('file')->getClientOriginalExtension();
                $image_name         =   sha1(uniqid().$image_path.uniqid()).'.'.$image_extention;
                $final_image = $request->file('file')->move($this->profile_img_base_path , $image_name);

                if(isset($profile_image_name) && $profile_image_name != "")
                {   
                  if($image_path != "default_profile_image.png")
                  {
                    @unlink($this->profile_img_base_path.'/'.$profile_image_name);
                  }
                }

                $image_upload = TRUE; 
            }
            else
            {
                $arr_response['status'] =   "ERROR";
                $arr_response['msg']    =   trans('controller_translations.file_is_not_an_image_file');
                return response()->json($arr_response);
            }
        }

        if( $image_upload  == TRUE )
        {
            $obj_update  = $this->BaseModel->where('user_id','=',$user_id)->update(['profile_image'=>$image_name]);
            
            if($obj_update)
            {
                $arr_response['status']     =   "SUCCESS";
                $arr_response['image_name'] =   $this->profile_img_public_path.$image_name;
                $arr_response['msg']        =   trans('controller_translations.profile_image_saved_successfully');
            }
            else
            {   
                $arr_response['status'] =   "ERROR";
                $arr_response['msg']    =   trans('controller_translations.error_saving_file');
            }
         
            return response()->json($arr_response);
        }

    }


}
