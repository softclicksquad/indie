<?php

namespace App\Http\Controllers\Front;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\StaticPageModel;
use App\Models\SubscriptionPacksModel;
use App\Models\SubscriptionUsersModel;
use App\Models\TransactionsModel;
use App\Models\ExpertsCategoriesModel;
use App\Models\ExpertsSkillModel;
use App\Models\FavouriteProjectsModel;
use App\Models\UserModel;
use App\Models\CronLogsModel;
use App\Common\Services\MailService;


//use App;
//use Validator;
//use Session;
use Mail;
//use URL;
//use DB;
use Log;


class CronController extends Controller
{
    public function __construct(SubscriptionPacksModel $subscriptionpacksmodel,
                                SubscriptionUsersModel $subscriptionusermodel,
                                TransactionsModel      $transactionsmodel,
                                ExpertsCategoriesModel $expertscategorymodel,
                                ExpertsSkillModel      $expertsskillmodel,
                                FavouriteProjectsModel $favouriteprojectsmodel,
                                CronLogsModel          $CronLogsModel,
                                MailService            $MailService
                                )

	{
		    $this->arr_view_data = [];
        $this->SubscriptionPacksModel   = $subscriptionpacksmodel;
        $this->SubscriptionUsersModel   = $subscriptionusermodel;
        $this->TransactionsModel        = $transactionsmodel;
        $this->ExpertsCategoriesModel   = $expertscategorymodel;
        $this->ExpertsSkillModel        = $expertsskillmodel;
        $this->FavouriteProjectsModel   = $favouriteprojectsmodel;
        $this->CronLogsModel            = $CronLogsModel;
        $this->MailService              = $MailService;

	}
	
    public function expiry_subscription_notification()
    {
        $before_one_day         = date('Y-m-d',strtotime("+ 1 day"));
        $before_three_day       = date('Y-m-d',strtotime("+ 3 day"));
        $before_one_weeks       = date('Y-m-d',strtotime("+ 1 weeks"));
        $before_two_weeks       = date('Y-m-d',strtotime("+ 2 weeks"));
        $arr_subscription       = $data = array();
        $arr_exp_dates          = array();
        $arr_exp_dates[0]       = $before_one_day;
        $arr_exp_dates[1]       = $before_three_day;
        $arr_exp_dates[2]       = $before_one_weeks;
        $arr_exp_dates[3]       = $before_two_weeks;
        $current_date           = date('Y-m-d');

        //dd($before_one_day, $before_three_day, $current_date, $arr_exp_dates);
        
        //$obj_subscription = $this->SubscriptionUsersModel->whereDate('expiry_at','>=',$current_date)->with(['user_details'])->get(['id','user_id','expiry_at','pack_name']);

        $obj_subscription = $this->SubscriptionUsersModel->where('is_active',1)->whereIn('expiry_at',$arr_exp_dates)->with(['user_details'])->get(['id','user_id','expiry_at','pack_name']);
        if($obj_subscription && $obj_subscription!=FALSE){
            $arr_subscription = $obj_subscription->toArray();
        }
        
        $project_name = config('app.project.name');
        //mail_form    = get_site_email_address(); 
          
        $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';

        if(isset($arr_subscription) && sizeof($arr_subscription)>0)
        {
            foreach ($arr_subscription as $key => $subscription)
            {
                $expire_date = date('d M Y', strtotime($subscription['expiry_at']));
                $expert_first_name = isset($subscription['user_details']['role_info']['first_name'])?$subscription['user_details']['role_info']['first_name']:'';

                $expert_name = $expert_first_name;

                $data['expert_name'] = $expert_name;
                $data['pack_name'] = $subscription['pack_name'];
                $data['expire_date'] = $expire_date;
                $data['email_id']    = isset($subscription['user_details']['email'])? $subscription['user_details']['email']:'';

                $email_to = isset($subscription['user_details']['email'])? $subscription['user_details']['email']:'';

                if($email_to!= "")
                {
                    try{

                         $mail_status = $this->MailService->send_expiry_subscription_notification_email($data);

                        // $main_status = Mail::send('front.email.expiry_subscription_notification', $data, function ($message) use ($email_to,$mail_form,$project_name) {
                        //     $message->from($mail_form, $project_name);
                        //     $message->subject($project_name.':Subscription Expiry.');
                        //     $message->to($email_to);
                        // });
                    }
                    catch(\Exception $e){
                    //Session::Flash('error',trans('controller_translations.text_mail_not_sent'));
                    }
                }

                $log_info = array();
                $log_info['corn_type'] = 1;
                $log_info['subscription_users_id'] = isset($subscription['id'])?$subscription['id']:0;
                $log_info['user_id']      = isset($subscription['user_id'])?$subscription['user_id']:0;
                $log_info['title']        = 'Subscription Expiry Notification';

                if (isset($subscription['expiry_at'])) 
                {
                    $log_info['description']   = 'Your Subscription will expire on '.$subscription['expiry_at'];
                }
                else
                {
                    $log_info['description']   = 'Renew your subscription as soon as possible';
                }

                $log_status = $this->CronLogsModel->create($log_info);
            }
        }
    }

    public function subscription_expire(){
        $current_date = date('Y-m-d');
        $arr_subscription = array();
        $obj_subscription = $this->SubscriptionUsersModel->where('is_active','1')->whereDate('expiry_at','=',$current_date)->get(['id','user_id','expiry_at']);
        if($obj_subscription && $obj_subscription!=FALSE){
            $arr_subscription = $obj_subscription->toArray();
        }
        //dd($arr_subscription);
        if(isset($arr_subscription) && sizeof($arr_subscription)>0){
            foreach ($arr_subscription as $key => $subscription) {
                $this->free_subscription($subscription['user_id'],'1');
                /*delete category and skills of expert*/
                $this->remove_subscription_dependency($subscription['user_id']);
                $log_info['corn_type'] = 2;
                $log_info['subscription_users_id'] = isset($subscription['id'])?$subscription['id']:0;
                $log_info['user_id']      = isset($subscription['user_id'])?$subscription['user_id']:0;
                $log_info['title']        = 'Subscription Expiry';
                $log_info['description']  = 'Subscription Expiried Successfully';
                $log_status = $this->CronLogsModel->create($log_info);
            }
        }
    }
    public function free_subscription($user_id,$pack_id){
      $arr_pack_details = array();
      $arr_user_subcription = array();
      if (isset($user_id) && $user_id!=null) {
        $obj_pack_details =  $this->SubscriptionPacksModel->where('id',$pack_id)->first();
        if ($obj_pack_details!=FALSE) {
            $arr_pack_details = $obj_pack_details->toArray();
            
            if (sizeof($arr_pack_details)>0) {
              if (isset($arr_pack_details['pack_price']) && $arr_pack_details['pack_price']==0) {
                  $invoice_id = $this->_generate_invoice_id();
                  /*First make transaction  */ 
                  $arr_transaction['user_id'] = $user_id;
                  $arr_transaction['invoice_id'] = $invoice_id;
                  /*transaction_type is type for transactions 1-Subscription*/
                  $arr_transaction['transaction_type'] = 1;
                  $arr_transaction['payment_method'] = 0;
                  $arr_transaction['payment_status'] = 1;
                  $arr_transaction['payment_date'] = date('c');
                  $arr_transaction['paymen_amount'] = $arr_pack_details['pack_price'];
                  $arr_transaction['payment_date'] = date('c');
                  /* get currency for transaction */
                  /*$arr_currency = setCurrencyForTransaction();
                  $arr_transaction['currency']      = isset($arr_currency['currency']) ? $arr_currency['currency'] : '$';
                  $arr_transaction['currency_code'] = isset($arr_currency['currency_code']) ? $arr_currency['currency_code'] : 'USD';*/
                  $transaction = $this->TransactionsModel->create($arr_transaction);
                  if ($transaction) {
                    /*insert record in user_subcription for selected pack and invoice details*/
                    $arr_user_subcription['user_id'] = $user_id;
                    $arr_user_subcription['subscription_pack_id'] = isset($arr_pack_details['id'])?$arr_pack_details['id']:'';
                    $arr_user_subcription['invoice_id'] = $invoice_id;
                    $arr_user_subcription['expiry_at'] = null;
                    $arr_user_subcription['pack_name'] = isset($arr_pack_details['pack_name'])?$arr_pack_details['pack_name']:'';
                    $arr_user_subcription['pack_price'] = isset($arr_pack_details['pack_price'])?$arr_pack_details['pack_price']:'';
                    $arr_user_subcription['validity'] = isset($arr_pack_details['validity'])?$arr_pack_details['validity']:'';
                    $arr_user_subcription['number_of_bids'] = isset($arr_pack_details['number_of_bids'])?$arr_pack_details['number_of_bids']:'0';
                    $arr_user_subcription['topup_bid_price'] = isset($arr_pack_details['topup_bid_price'])?$arr_pack_details['topup_bid_price']:'0';
                    $arr_user_subcription['number_of_categories'] = isset($arr_pack_details['number_of_categories'])?$arr_pack_details['number_of_categories']:'0';
                    $arr_user_subcription['number_of_subcategories'] = isset($arr_pack_details['number_of_subcategories'])?$arr_pack_details['number_of_subcategories']:'0';
                    $arr_user_subcription['number_of_skills'] = isset($arr_pack_details['number_of_skills'])?$arr_pack_details['number_of_skills']:'0';
                    $arr_user_subcription['number_of_favorites_projects'] = isset($arr_pack_details['number_of_favorites_projects'])?$arr_pack_details['number_of_favorites_projects']:'0';
                    $arr_user_subcription['website_commision'] = isset($arr_pack_details['website_commision'])?$arr_pack_details['website_commision']:'0';
                    $arr_user_subcription['allow_email'] = isset($arr_pack_details['allow_email'])?$arr_pack_details['allow_email']:'0';
                    $arr_user_subcription['allow_chat'] = isset($arr_pack_details['allow_chat'])?$arr_pack_details['allow_chat']:'0';
                    $arr_user_subcription['is_active'] = 1;
                    //update all previous subscription status to 0 and then create new subscription
                    $subscription_update = $this->SubscriptionUsersModel->where('user_id',$user_id)->where('is_active',1)->update(['is_active'=>0]);
                    $subscription = $this->SubscriptionUsersModel->create($arr_user_subcription);
                    return true;
              }
            }
          }
        }
      }
      return false;
    }
    private function _generate_invoice_id(){
            $secure      = TRUE;    
            $bytes       = openssl_random_pseudo_bytes(3, $secure);
            $order_token = 'INV'.date('Ymd').strtoupper(bin2hex($bytes));
            return $order_token;
    }
    public function remove_subscription_dependency($expert_user_id=null){
         if (isset($expert_user_id) && $expert_user_id!=null) {
           $delete_category  = $this->ExpertsCategoriesModel->where('expert_user_id',$expert_user_id)->delete();
           $delete_skills    = $this->ExpertsSkillModel->where('expert_user_id',$expert_user_id)->delete();
           $delete_favourite = $this->FavouriteProjectsModel->where('expert_user_id',$expert_user_id)->delete();
         }
    }
    public function applozic_fallback_emails(Request $request){
          //Log::info($request->all());
          /*Log::info('in call back');*/
          /*'key' => '5-ab58572c-6813-4084-9e5b-95da3508d47f-1555492552707',
          'from' => 'archEx2',
          'to' => 'archEx6',
          'message' => 'ata tri ye ye re ye',
          'timeStamp' => 1555492552711,
          'receiverLastSeenAtTime' => 1554902326230,
          'receiverConnected' => false,
          'metadata' => 
          array (
          ),*/
          if(!empty($request->input('key'))){$key     = $request->input('key');}else{     $key  ="0"; }
          if(!empty($request->input('key'))){$from    = $request->input('from');}else{    $from =""; }
          if(!empty($request->input('key'))){$to      = $request->input('to');}else{      $to   =""; }
          if(!empty($request->input('key'))){$msg     = $request->input('message');}else{ $msg  =""; }
          
          if(isset($from) && $from != "" && isset($to) && $to != ""){
              /* get from name and email */
              $expload_from = explode('archEx',$from);
              $expload_to   = explode('archEx',$to);
              if(isset($expload_from[1]) && $expload_from[1] !="" && $expload_from[1] !='0' && isset($expload_to[1]) && $expload_to[1] !="" && $expload_to[1] !='0'){
                $from_user_details = sidebar_information($expload_from[1]);
                $from_first_name   = isset($from_user_details['user_details']['first_name'])?$from_user_details['user_details']['first_name']:'';
                $from_last_name    = isset($from_user_details['user_details']['last_name'])?str_limit($from_user_details['user_details']['last_name'],1,'.'):'';
                $from_email        = get_user_email($expload_from[1]);
                $to_user_details   = sidebar_information($expload_to[1]);
                $to_first_name     = isset($to_user_details['user_details']['first_name'])?$to_user_details['user_details']['first_name']:'';
                $to_last_name      = isset($to_user_details['user_details']['last_name'])?str_limit($to_user_details['user_details']['last_name'],1,'.'):'';
                $to_email          = get_user_email($expload_to[1]);
                $project_name      = config('app.project.name');
                $email_to          = $to_email;
                $mail_form         = 'no-reply@archexperts.com';//$from_email;
                $data = array(
                    'key'          => $key,
                    'from'         => $from_first_name.' '.$from_last_name,
                    'to'           => $to_first_name.' '.$to_last_name,
                    'message'      => $msg,
                    'url'          => url('/'),
                    'to_email'     => $to_email

                );
                try{

                    $mail_status = $this->MailService->send_applozic_fallback_email($data);

                    // Mail::send('front.email.applozic_fallback', $data, function ($message) use ($email_to,$mail_form,$project_name,$from_first_name,$from_last_name) {
                    //     $message->from($mail_form, $project_name);
                    //     $message->subject($from_first_name.' '.$from_last_name.' Via '.$project_name);
                    //     $message->to($email_to);
                    // });
                  Log::info($key.' : Applozic Fallback Mail Sent Successfully');
                }catch(\Exception $e){
                  Log::info($key.' : Applozic Fallback Mail Not Sent Successfully');
                }
              }
          }
    }
} // end class
