<?php
namespace App\Http\Controllers\Front;
 
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\UserModel;
use App\Models\ClientsModel;
use App\Models\ExpertsModel;
use App\Models\CountryModel;
use App\Models\ProjectManagerModel;
use App\Models\SubscriptionPacksModel;
use App\Models\TransactionsModel;
use App\Models\SubscriptionUsersModel;
use App\Models\ProjectpostModel;
use App\Models\MilestonesModel;
use App\Common\Services\CommonDataService;
use App\Common\Services\WalletService;
use App\Common\Services\MailService;
use Validator;
use Sentinel;
use Session;
use Mail;
use Activation;
use Reminder;
use URL;
use DB;

class AuthController extends Controller
{
    public $arr_view_data;
    public $admin_panel_slug;

    /*
    | Constructor : 
    | auther : Sagar Sainkar
    */

    public function __construct(MailService $MailService,UserModel $user_model,ClientsModel $clients,ExpertsModel $experts,CountryModel $countries,ProjectManagerModel $project_manager,SubscriptionPacksModel $subc_packs,TransactionsModel $transaction,SubscriptionUsersModel $user_subcription,
        ProjectpostModel $project_post,CommonDataService $CommonDataService,WalletService $wallet_service,MilestonesModel $milestones_model)
    {

      $this->UserModel               = $user_model;
      $this->ClientsModel            = $clients;
      $this->ExpertsModel            = $experts;
      $this->CountryModel            = $countries;
      $this->ProjectManagerModel     = $project_manager;
      $this->ProjectpostModel        = $project_post;
      $this->WalletService           = $wallet_service;
      $this->MailService             = $MailService;

      $this->SubscriptionPacksModel  = $subc_packs;
      $this->TransactionsModel       = $transaction;
      $this->SubscriptionUsersModel  = $user_subcription;
      $this->CommonDataService       = $CommonDataService;
      $this->MilestonesModel         = $milestones_model;
      $this->arr_view_data           = [];
      $this->admin_panel_slug        = config('app.project.admin_panel_slug');
      $this->module_url_path         = url("/signup");

      $this->profile_img_base_path   = base_path() . '/public'.config('app.project.img_path.profile_image');
      $this->profile_img_public_path = url('/').config('app.project.img_path.profile_image');
    }
    public function signup()
    {
      $this->arr_view_data['page_title'] = "Create an account";
      $this->arr_view_data['module_url_path'] = $this->module_url_path;
      return view('front.auth.create_account',$this->arr_view_data);
    }
     /*
    | Login : load login page for admin
    | auther : Sagar Sainkar
    */
    public function registeration_client()
    {
      $arr_countries = $arr_mangopay_blocked_countries = [];
      $getIP = $this->CommonDataService->ip_info($_SERVER['REMOTE_ADDR'], "Location");
      //get all countries here
      $arr_countries = array();
      $obj_countries = $this->CountryModel->orderBy('country_name', 'asc')->where('is_active',1)->get();
      if($obj_countries != FALSE){
          $arr_countries = $obj_countries->toArray();
      }
      if(isset($getIP['country']) && $getIP['country'] != ""){
         $this->arr_view_data['client_country'] = $getIP['country'];
      } else {
         $this->arr_view_data['client_country'] = ""; 
      }

      $arr_mangopay_blocked_countries = get_mangopay_blocked_countries_id();

      $this->arr_view_data['arr_countries'] = $arr_countries;
      $this->arr_view_data['arr_mangopay_blocked_countries'] = $arr_mangopay_blocked_countries;
      $this->arr_view_data['page_title'] = trans('controller_translations.page_title_sign_up');
      $this->arr_view_data['module_url_path'] = $this->module_url_path;
      return view('front.auth.registeration_client',$this->arr_view_data);
    }
    /*
    | Login : load login page for admin
    | auther : Sagar Sainkar
    */
    public function registeration_expert()
    {
      $arr_countries = $arr_mangopay_blocked_countries = [];
      $getIP = $this->CommonDataService->ip_info($_SERVER['REMOTE_ADDR'], "Location");
      //get all countries here
      $arr_countries = array();
      $obj_countries = $this->CountryModel->orderBy('country_name', 'asc')->where('is_active',1)->get();
      if($obj_countries != FALSE){
          $arr_countries = $obj_countries->toArray();
      }
      if(isset($getIP['country']) && $getIP['country'] != ""){
         $this->arr_view_data['client_country'] = $getIP['country'];
      } else {
         $this->arr_view_data['client_country'] = ""; 
      } 
      $arr_mangopay_blocked_countries = get_mangopay_blocked_countries_id();

      $this->arr_view_data['arr_countries']   = $arr_countries;
      $this->arr_view_data['arr_mangopay_blocked_countries'] = $arr_mangopay_blocked_countries;
      $this->arr_view_data['page_title']      = trans('controller_translations.page_title_sign_up');
      $this->arr_view_data['module_url_path'] = $this->module_url_path;
      return view('front.auth.registeration_expert',$this->arr_view_data);
    }
    /*
    | User Name Availabilty 
    | auther : Tushar Ahire
    */
    public function chk_username_availabilty(Request $request){
      $user = Sentinel::check(); 
      $username = $request->input('username');
      if(isset($user->id) && $user->id != ""){
        $user = $this->UserModel->where('user_name',$username)->where('id','!=',$user->id)->count();
      }
      else{
        $user = $this->UserModel->where('user_name',$username)->count();  
      }
      if ($user > 0) {
          return trans('controller_translations.error_this_user_name_is_already_present');
      }
      else
      {
          return 'Available';
      }
    }
    /*
    | Signup for client
    | auther : Sagar Sainkar
    */
    public function store_client(Request $request){
      
        $form_data                     = array();
        $arr_user_details              = array();
        $arr_data                      = array();
        $status                        = FALSE;
        $ip_address                    = $_SERVER['REMOTE_ADDR'];
        /*$arr_rules['first_name']     = "required|max:250";
        $arr_rules['last_name']        = "required|max:250";
        $arr_rules['user_name']        = "required|unique:users,user_name";
        $arr_rules['company_name']     = "max:250";*/
        $arr_rules['user_type']        = "required";
        $arr_rules['country']          = "required";
        $arr_rules['email']            = "required|email|max:250|unique:users,email";
        $arr_rules['password']         = 'required|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/';
        $arr_rules['confirm_password'] = 'required|same:password|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/';
        /*$arr_rules['profile_image']  = "mimes:jpeg,jpg,png";*/

        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails()){
          return redirect()->back()->withErrors($validator)->withInput($request->all());
        }
        $form_data       = $request->all();
        $first_name      = '';   
        $expload_email   = explode('@', $request->input('email'));
        if(isset($expload_email[0]) && $expload_email[0] != ""){
          $first_name    = $expload_email[0];
        } 
        //check for duplicate user_name for user
        /*$user = $this->UserModel->where('user_name',$request->input('user_name'))->first();
        if ($user){
          Session::flash('error',trans('controller_translations.error_this_user_name_is_already_present'));
          return redirect()->back()->withInput($request->all());
        }*/
        //check for duplicate email for user
        $credentials = ['email' => $request->input('email')];
        $user = Sentinel::findByCredentials($credentials);
        if ($user) {
          Session::flash('error',trans('controller_translations.error_this_email_is_already_present'));
          return redirect()->back()->withInput($request->all());
        }

        $blocked_country = ['3','33','18','36','120','71','85','97','111','129','234','181','193','133','215','225','245','248','229','110','42'];

        if(in_array($form_data['country'],$blocked_country))
        {
          Session::flash('country_blocked','We currently are not able to process any payments for persons living in the selected country. We are very sorry! Please check our website from time to time. As changes in regulations happen we would be very happy to to welcome you to our website soon again.');
          return redirect()->back()->withInput($request->all())->with('CountryError','Yes');
        }

        $arr_data['email']      = $form_data['email'];
        $arr_data['password']   = $form_data['password'];
        $arr_data['user_name']  = '';//$form_data['user_name'];
        $arr_data['ip_address'] = $ip_address;
        $arr_data['is_active']  = 0;
        $obj_user               = Sentinel::register($arr_data);
        // send email with activation link see routes only use Sentinel
        if ($obj_user)
        {
          $arr_user_details['user_id']       = $obj_user->id;
          $arr_user_details['first_name']    = ucfirst($first_name);//ucfirst($form_data['first_name']);
          $arr_user_details['last_name']     = '';//ucfirst($form_data['last_name']);
          $arr_user_details['country']       = $form_data['country'];
          $arr_user_details['company_name']  = '';//$form_data['company_name'];
          $arr_user_details['user_type']     = $form_data['user_type'];
          
          try{
          $timezoneinfo = file_get_contents('http://ip-api.com/json/' . $_SERVER['REMOTE_ADDR']);
          }catch(\Exception $e){
          $timezoneinfo = file_get_contents('http://ip-api.com/json');  
          }
          if(isset($timezoneinfo)){
            $timezoneinfo = json_decode($timezoneinfo);
            if(isset($timezoneinfo->timezone) && $timezoneinfo->timezone !=""){
              $arr_user_details['timezone']      = $timezoneinfo->timezone;
            } else { 
              $arr_user_details['timezone']      = date_default_timezone_get();
            }
          } else { 
            $arr_user_details['timezone']      = date_default_timezone_get();
          }
          //assign role to user client
          $role   = Sentinel::findRoleBySlug('client');
          $obj_user->roles()->attach($role);
          $status = $this->ClientsModel->create($arr_user_details);
          if($status){ 
            $user                      = Sentinel::findById($obj_user->id);
            $activation                = Activation::create($user); /* Create avtivation */
            $activation_code           = $activation->code; // get activation code
            $email_id                  = $form_data['email'];
            $data['user_id']           = $obj_user->id;
            $data['activation_code']   = base64_encode($activation_code);
            $data['name']              = ucfirst($first_name);//ucfirst($form_data['first_name']);
            $data['email_id']          = $form_data['email'];
            $data['confirmation_link'] = url('/').'/confirm_email/'.base64_encode($obj_user->id).'/'.base64_encode($activation_code);
            $project_name = config('app.project.name');
            //$mail_form = isset($website_contact_email)?$website_contact_email:'support@archexperts.com';
            $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
            try{
                 $mail_status = $this->MailService->send_user_registration_verification_email($data);
                // Mail::send('front.email.signup_confirmation', $data, function ($message) use ($email_id,$mail_form,$project_name) {
                //           $message->from($mail_form, $project_name);
                //           $message->subject($project_name.':Email Confirmation.');
                //           $message->to($email_id);
                // });
            }
            catch(\Exception $e)
            {
              Session::Flash('error',trans('controller_translations.text_mail_not_sent'));
            }
            Session::flash('success', trans('controller_translations.success_thank_you_registration_successfully_please_check_your_email_account_for_confirmation_link'));
            return redirect()->back();
          } else {
              Session::flash('error',trans('controller_translations.error_problem_occured_while_client_registration'));
          }
        }
        return redirect()->back()->withInput($request->all());
    }
    /*
    | Signup for client
    | auther : Sagar Sainkar
    */
    public function store_expert(Request $request){
        $form_data        = array();
        $arr_user_details = array();
        $arr_data         = array();
        $status           = FALSE;
        $ip_address       = $_SERVER['REMOTE_ADDR'];
        $arr_rules['country']          = "required";
        $arr_rules['email']            = "required|email|max:250|unique:users,email";
        $arr_rules['password']         = 'required|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/';
        $arr_rules['user_type']        = "required";
        $arr_rules['confirm_password'] = 'required|same:password|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/';
        //$arr_rules['user_type'] = "required";

        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
             return redirect()->back()->withErrors($validator)->withInput($request->all());
        }
        $form_data = $request->all();
        $first_name      = '';   
        $expload_email   = explode('@', $request->input('email'));
        if(isset($expload_email[0]) && $expload_email[0] != ""){
          $first_name    = $expload_email[0];
        } 
        //check for duplicate email for user
        $credentials = ['email' => $request->input('email')];
        $user = Sentinel::findByCredentials($credentials);
        if ($user) 
        {
          Session::flash('error',trans('controller_translations.error_this_email_is_already_present'));
          return redirect()->back()->withInput($request->all());
        }

        $blocked_country = ['3','33','18','36','120','71','85','97','111','129','234','181','193','133','215','225','245','248','229','110','42'];

        if(in_array($form_data['country'],$blocked_country))
        {
          Session::flash('country_blocked','We currently are not able to process any payments for persons living in the selected country. We are very sorry! Please check our website from time to time. As changes in regulations happen we would be very happy to to welcome you to our website soon again.');
          return redirect()->back()->withInput($request->all())->with('CountryError','Yes');
        }
        
        $arr_data['email']      = $form_data['email'];
        $arr_data['password']   = $form_data['password'];
        $arr_data['user_name']  = '';//$form_data['user_name'];
        $arr_data['ip_address'] = $ip_address;
        $arr_data['is_active']  = 0;
        //$arr_data['user_type']  = $form_data['user_type'];

        $obj_user = Sentinel::register($arr_data);
        // send email with activation link see routes only use Sentinel
        if($obj_user) 
        {
          $arr_user_details['user_id']       = $obj_user->id;
          $arr_user_details['first_name']    = ucfirst($first_name);//ucfirst($form_data['first_name']);
          $arr_user_details['last_name']     = '';
          $arr_user_details['country']       = $form_data['country'];
          $arr_user_details['company_name']  = '';
          $arr_user_details['user_type']     = $form_data['user_type'];
          try{
          $timezoneinfo = file_get_contents('http://ip-api.com/json/' . $_SERVER['REMOTE_ADDR']);
          }catch(\Exception $e){
          $timezoneinfo = file_get_contents('http://ip-api.com/json');  
          }
          if(isset($timezoneinfo)){
            $timezoneinfo = json_decode($timezoneinfo);
            if(isset($timezoneinfo->timezone) && $timezoneinfo->timezone !=""){
              $arr_user_details['timezone']      = $timezoneinfo->timezone;
            } else { 
              $arr_user_details['timezone']      = date_default_timezone_get();
            }
          } else { 
            $arr_user_details['timezone']      = date_default_timezone_get();
          }
          if(isset($timezone) && $timezone !=""){
          $arr_user_details['timezone']      = $timezone;
          } else { 
          $arr_user_details['timezone']      = date_default_timezone_get();
          }
          $role   = Sentinel::findRoleBySlug('expert');
          $obj_user->roles()->attach($role);
          $status = $this->ExpertsModel->create($arr_user_details);
          // add expert subscription pack that is default pack of expert with zero price
          $assign_subscription = $this->initialize_expet($obj_user->id);

          if($status)
          { 
            $user = Sentinel::findById($obj_user->id);
            $activation = Activation::create($user); /* Create avtivation */
            $activation_code = $activation->code; // get activation code

            $email_id                  = $form_data['email'];
            $data['user_id']           = $obj_user->id;
            $data['activation_code']   = base64_encode($activation_code);
            $data['name']              = ucfirst($first_name);//ucfirst($form_data['first_name']);
            $data['email_id']          = $form_data['email'];
            $data['confirmation_link'] = url('/').'/confirm_email/'.base64_encode($obj_user->id).'/'.base64_encode($activation_code);

            $project_name = config('app.project.name');
            //$mail_form = isset($website_contact_email)?$website_contact_email:'support@archexperts.com';
            $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
            try{
                $mail_status = $this->MailService->send_user_registration_verification_email($data);
                // Mail::send('front.email.signup_confirmation', $data, function ($message) use ($email_id,$mail_form,$project_name) {
                //     $message->from($mail_form, $project_name);
                //     $message->subject($project_name.':Email Confirmation.');
                //     $message->to($email_id);
                // });
            }
            catch(\Exception $e){
            Session::Flash('error',trans('controller_translations.text_mail_not_sent'));
            }
            Session::flash('success', trans('controller_translations.success_thank_you_registration_successfully_please_check_your_email_account_for_confirmation_link'));
            return redirect()->back();
          } 
          else
          {
              Session::flash('error', trans('controller_translations.error_problem_occured_while_client_registration'));
          }
        }
        return redirect()->back()->withInput($request->all());
    }
    /*
    | Email confirmation process.
    | auther : Sagar Sainkar
    */
    public function confirm_email($enc_user_id,$activation_code)
    {
        $id = base64_decode($enc_user_id);
        $activation_code = base64_decode($activation_code);

        $user = Sentinel::findById($id);
        $activation = Activation::exists($user); // check if activation aleady done ...

        if($activation) // if activation is done
        {
            if (Activation::complete($user, $activation_code)) // complete an activation process
            {
                $tmp_user = $this->UserModel->where('id',$id)->first();
                if($tmp_user)
                {
                    $tmp_user->is_active = 1;
                    if($tmp_user->is_email_changed == '1')
                    {
                      $tmp_user->is_email_changed = 0;
                      $tmp_user->is_email_verified = 1;
                    }

                    $tmp_user->save(); 
                }
                $check_authentication = Sentinel::login($user);
                if($check_authentication)
                {
                  $role_client = Sentinel::findRoleBySlug('client'); // check if the user has the specified role which user

                  $role_expert = Sentinel::findRoleBySlug('expert');

                  $role_project_manager = Sentinel::findRoleBySlug('project_manager');

                  if(Sentinel::inRole($role_client))
                  {
                       Session::flash('success', trans('controller_translations.success_thank_you_email_confirmation_successfully') );
                       return redirect('/client/profile');
                  } 
                  elseif (Sentinel::inRole($role_expert)) 
                  {
                        Session::flash('success', trans('controller_translations.success_thank_you_email_confirmation_successfully') );
                        return redirect('/expert/profile');
                  } 
                  elseif (Sentinel::inRole($role_project_manager)) 
                  {
                        Session::flash('success', trans('controller_translations.success_thank_you_email_confirmation_successfully') );
                        return redirect('/projectmanager/profile');
                  } 
              }
               /* Session::flash('success','Thank you! Email confirmation successfully.');
                return redirect('/login');*/
            }
            else
            {
                Session::flash('error',trans('controller_translations.error_error_their_is_some_problem_while_email_confirmation'));
                return redirect('/login');
            }
        }
        else
        {
            Session::flash('error', trans('controller_translations.error_invalid_request_for_email_confirmation') );
            return redirect('/login');
        }
    }  
    /*
    | Login : load login page for admin
    | auther : Sagar Sainkar
    | @return \Illuminate\Http\Response
    */
    public function login()
    {
      $this->arr_view_data['page_title']      = trans('controller_translations.page_title_login');
      $this->arr_view_data['module_url_path'] = $this->module_url_path;
    	return view('front.auth.login',$this->arr_view_data);
    }
     /*
    | process_login : validate client credentials and login it into system with client
    | auther : Sagar Sainkar
    | @return \Illuminate\Http\Response
    */
    public function validate_login(Request $request)
    { 
       $form_data    = [];
       $form_data    = $request->all();
    	 $validator    = Validator::make($form_data, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if ($validator->fails()) 
        {
            return redirect('/login')
                        ->withErrors($validator)
                        ->withInput($request->all());
        }

    		$credentials = [
    		    'email'    => $request->input('email'),
    		    'password' => $request->input('password'),
    		];
      $user = Sentinel::findByCredentials($credentials); // check if user exists
      $last_login = '0000-00-00 00:00:00';
      if($user)
      {
        $last_login = $user->last_login;
        $activation = Activation::completed($user);
        if($activation) // check if activation is completed
        {
          $remember_me  =   $request->input('remember');
          if($remember_me =='on')
          {
              setcookie('remember_me_email', $request->input('email'), time()+60*60*24*100);
              setcookie('remember_me_password', $request->input('password'), time()+60*60*24*100);
              $check_authentication = Sentinel::authenticateAndRemember($credentials);  //authenticate a user
              
          }
          else if($remember_me =='')
          { 
              setcookie("remember_me_email","");
              setcookie("remember_me_password","");
              $check_authentication = Sentinel::authenticate($credentials);   //authenticate a user
          }

          if(isset($user->is_email_changed) && isset($user->is_email_verified) && ($user->is_email_changed!=0 && $user->is_email_verified!=1)) 
          {
             Session::flash('error',trans('controller_translations.error_your_account_is_temporary_unverified'));
             Sentinel::logout();
             return redirect()->back();
          }

          if (isset($user->is_active) && $user->is_active!=1) 
          {
             Session::flash('error',trans('controller_translations.error_your_account_is_temporary_blocked_please_login_after_some_time'));
             Sentinel::logout();
             return redirect()->back();
          }

          if($check_authentication)
          {
              $role_client = Sentinel::findRoleBySlug('client'); // check if the user has the specified role which user
              $role_expert = Sentinel::findRoleBySlug('expert');
              $role_project_manager = Sentinel::findRoleBySlug('project_manager');
              $role_recruiter = Sentinel::findRoleBySlug('recruiter');

              $arr_mangopay_kyc_details = $mangopay_kyc_details = [];

              /*-----------if KYC is done then update--------------*/
              $mp_user_id = isset($user->mp_user_id)?$user->mp_user_id:'';
              if(isset($mp_user_id) && $mp_user_id!='')
              {
                $mangopay_kyc_details   = $this->WalletService->get_kyc_details($mp_user_id);  

                $mangopay_kyc_details = (array)$mangopay_kyc_details;
                if(isset($mangopay_kyc_details))
                {
                  foreach ($mangopay_kyc_details as $key => $value) 
                  {
                      $arr_mangopay_kyc_details[$key] = (array) $value ;
                  }
                }

                $kyc_key = 'Status';
                $kyc_val = 'VALIDATED';
                $verified = 0;

                if(isset($arr_mangopay_kyc_details) && is_array($arr_mangopay_kyc_details)){
                  if(find_key_value_in_array($arr_mangopay_kyc_details, $kyc_key, $kyc_val)){
                    $verified = 1;
                  }
                }

                $this->UserModel->where('id',$user->id)->update(['kyc_verified'=>$verified]);  
              }

               /*-----------end of KYC update--------------*/
            
              if(Sentinel::inRole($role_client))
              { 
                  $login_status = Sentinel::login($user);

                  if($login_status)
                  {  

                    if(Session::has('PROJECT_ID'))
                    {
                      $project_id = Session::get('PROJECT_ID');
                      
                      if(Session::has('MILESTONE'))
                      {
                        return redirect('/client/projects/milestones/'.$project_id);
                      }
                      else
                      {
                        $cnt_project = $this->ProjectpostModel->where('client_user_id','=',$user->id)
                                                              ->where('id','=',base64_decode($project_id))
                                                              ->count();
                                                              
                        if($project_id != "" && $cnt_project > 0 )
                        {
                          return redirect('/client/projects/details/'.$project_id);
                        }
                      }
                    }
                    Session::put('last_login',$last_login);      

                    $this->UserModel->where('id',$user->id)->update(['last_logout'=>'0000-00-00 00:00:00']); 

                    Session::flash('success', trans('controller_translations.success_you_are_successfully_login'));
                    return redirect('/client/dashboard');
                  }
                  else
                  {
                      Session::flash('error', trans('controller_translations.error_invalid_credentials_please_try_again') );
                  }
              }
              elseif (Sentinel::inRole($role_expert)) 
              {

                  $login_status = Sentinel::login($user);

                  if($login_status)
                  {  
                    if(Session::has('PROJECT_ID'))
                    {

                      $project_id = Session::get('PROJECT_ID');
                      
                      if(Session::has('MILESTONE'))
                      {
                        return redirect('/expert/projects/milestones/'.$project_id);
                      }
                      else
                      {
                        $cnt_project = $this->ProjectpostModel->where('expert_user_id','=',$user->id)
                                                              ->where('id','=',base64_decode($project_id))
                                                              ->count();

                        if($project_id != "" && $cnt_project > 0 )
                        {
                          return redirect('/expert/projects/details/'.$project_id);
                        }
                      } 
                    }
                    Session::put('last_login',$last_login);   
                    $this->UserModel->where('id',$user->id)->update(['last_logout'=>'0000-00-00 00:00:00']); 
                    Session::flash('success', trans('controller_translations.success_you_are_successfully_login') );
                    return redirect('/expert/dashboard');
                  }
                  else
                  {
                      Session::flash('error', trans('controller_translations.error_invalid_credentials_please_try_again'));
                  }
              }
              elseif (Sentinel::inRole($role_project_manager)) 
              {
                  $login_status = Sentinel::login($user);
                  if($login_status)
                  {  
                    if(Session::has('PROJECT_ID'))
                    {
                      $project_id = Session::get('PROJECT_ID');

                      if(Session::has('MILESTONE'))
                      {
                        return redirect('/project_manager/projects/milestones/'.$project_id);
                      }
                      else
                      {
                        $cnt_project = $this->ProjectpostModel->where('project_manager_user_id','=',$user->id)
                                                              ->where('id','=',base64_decode($project_id))
                                                              ->count();
                                                              
                        if($project_id != "" && $cnt_project > 0 )
                        {
                          return redirect('/project_manager/projects/details/'.$project_id);
                        }
                      }  
                    }
                    Session::put('last_login',$last_login);   
                    $this->UserModel->where('id',$user->id)->update(['last_logout'=>'0000-00-00 00:00:00']); 
                    Session::flash('success',trans('controller_translations.success_you_are_successfully_login'));
                    return redirect('/project_manager/dashboard');
                  }
                  else
                  {
                      Session::flash('error', trans('controller_translations.error_invalid_credentials_please_try_again') );
                  }
              }
              elseif (Sentinel::inRole($role_recruiter)) 
              {
                  $login_status = Sentinel::login($user);
                  if($login_status)
                  {  
                    if(Session::has('PROJECT_ID'))
                    {
                      $project_id = Session::get('PROJECT_ID');

                      if(Session::has('MILESTONE'))
                      {
                        return redirect('/recruiter/projects/milestones/'.$project_id);
                      }
                      else
                      {
                        $cnt_project = $this->ProjectpostModel->where('project_recruiter_user_id','=',$user->id)
                                                              ->where('id','=',base64_decode($project_id))
                                                              ->count();
                                                              
                        if($project_id != "" && $cnt_project > 0 )
                        {
                          return redirect('/recruiter/projects/details/'.$project_id);
                        }
                      }  
                    }
                    Session::put('last_login',$last_login);   
                    $this->UserModel->where('id',$user->id)->update(['last_logout'=>'0000-00-00 00:00:00']); 
                    Session::flash('success',trans('controller_translations.success_you_are_successfully_login'));
                    return redirect('/recruiter/dashboard');
                  }
                  else
                  {
                      Session::flash('error', trans('controller_translations.error_invalid_credentials_please_try_again') );
                  }
              }
              else
              {
                  Session::flash('error',trans('controller_translations.error_not_sufficient_privileges') );
              }
          }
          else
          {
            Session::flash('error',trans('controller_translations.error_invalid_credentials_please_try_again'));
          }
        }
        else
        {
            Session::flash('error',trans('controller_translations.error_your_account_has_not_been_activated_yet') );
        }
      }
      else
      {
        Session::flash('error',trans('controller_translations.error_invalid_credentials_please_try_again'));
      }
      return redirect()->back();
    }
    /*
    | description : Logout user from system
    | auther :Sagar Sainkar
    */ 
    public function logout()
    { 
      Session::forget('arr_data_bal');
      Session::forget('get_mangopay_wallet_details_expert');
      Session::forget('mangopay_transaction_details_expert');
      Session::forget('get_mangopay_wallet_details_client');
      Session::forget('mangopay_transaction_details_client');
      $user = Sentinel::check();
      if($user){
      $this->UserModel->where('id',$user->id)->update(['last_logout'=>date('Y-m-d H:i:s')]);
      }
      Sentinel::logout();
      return redirect(url('/login'));
    }

    /*
    | description : validate email and send forgot password link 
    | auther :Sagar Sainkar
    */ 

    public function show_forgot_password()
    {
      $this->arr_view_data['page_title'] = trans('controller_translations.page_title_forgot_password');
      $this->arr_view_data['module_url_path'] = $this->module_url_path;
      return view('front.auth.forgot_password',$this->arr_view_data);
    }

    public function process_forgot_password(Request $request)
    {
        $arr_rules['email']      = "required|email";
        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            Session::flash('error',trans('controller_translations.error_invalid_email_please_check_your_email_id'));
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $email = $request->input('email');

        $user  = Sentinel::findByCredentials(['email' => $email]);

        if ($user) 
        {

            $users_details=FALSE;
            $reminder = Reminder::create($user);
            if ($reminder) 
            {
              
              
            $reminder_url = URL::to('validate_reset_password/'.base64_encode($user->id).'/'.base64_encode($reminder->code) );

            if ($user->inRole('client')) 
            {
              $users_details = $this->ClientsModel->where('user_id',$user->id)->first(['first_name','last_name']);  
            }
            elseif($user->inRole('expert')) 
            {
              $users_details = $this->ExpertsModel->where('user_id',$user->id)->first(['first_name','last_name']);   
            }
            elseif($user->inRole('project_manager')) 
            {
              $users_details = $this->ProjectManagerModel->where('user_id',$user->id)->first(['first_name','last_name']);   
            }
            if ($users_details) 
            {
                $user_auth_details = array();
                $users_details = $users_details->toArray();
                $data['name'] = isset($users_details['first_name'])?$users_details['first_name']:'';
            }
            $data['reminder_url'] = $reminder_url;
            $data['email_id']     = $email;
            
              try{
                  $mail_status = $this->MailService->send_user_forget_password_email($data);

              }
              catch(\Exception $e)
              {
                Session::Flash('error',trans('controller_translations.text_mail_not_sent'));
              }
            
              Session::flash('success',trans('controller_translations.success_password_reset_link_is_send_to_your_email_address') );
              return redirect()->back();  
            }
            else 
            {
                Session::flash('error',trans('controller_translations.error_error_while_processing_forget_password_request'));
                return redirect()->back();
            }
        }
        else 
        {
            Session::flash('error',trans('controller_translations.error_invalid_email_please_check_your_email_id'));
            return redirect()->back();
        }

        
    }



    public function validate_reset_password_link($enc_id, $enc_reminder_code)
    {
        $user_id    = base64_decode($enc_id);
        $reminder_code  = base64_decode($enc_reminder_code);

        $user = Sentinel::findById($user_id);
        
        if(!$user)        
        {
          Session::flash('error',trans('controller_translations.error_invalid_user_request'));
          return redirect('/login');
        }

        if(Reminder::exists($user))
        {
            $this->arr_view_data['page_title'] = "Reset Password";
            $this->arr_view_data['enc_id'] = $enc_id;
            $this->arr_view_data['enc_reminder_code'] = $enc_reminder_code;
            $this->arr_view_data['module_url_path'] = $this->module_url_path;
            
            return view('front.auth.reset_password',$this->arr_view_data);
        }
        else
        { 

          Session::flash('error',trans('controller_translations.error_password_reset_link_was_expired'));
          return redirect('/login');
        }

        return redirect('/login');
    }


    public function reset_password(Request $request)
    {
        $arr_rules                      = array();
        $arr_rules['password']          = 'required|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/';
        $arr_rules['confirm_password']  = 'required|same:password|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/';
        $arr_rules['enc_id']            = "required";
        $arr_rules['enc_reminder_code'] = "required";

        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back();
        }

        $enc_id            = $request->input('enc_id');
        $enc_reminder_code = $request->input('enc_reminder_code');
        $password          = $request->input('password');
        $confirm_password  = $request->input('confirm_password');

        if($password  !=  $confirm_password )
        {
          Session::flash('error',trans('controller_translations.error_passwords_and_confirm_password_does_not_match'));
          return redirect()->back();
        }

        $user_id    = base64_decode($enc_id);
        $reminder_code  = base64_decode($enc_reminder_code);

        $user = Sentinel::findById($user_id);
        
        if(!$user)        
        {
          Session::flash('error',trans('controller_translations.error_you_request_to_reset_password_is_invalid'));
          return redirect()->back();
        }

        if ($reminder = Reminder::complete($user, $reminder_code, $password))
        {
          Session::flash('success',trans('controller_translations.success_your_password_has_been_successfully_reset') );
        }
        else
        {
          Session::flash('error',trans('controller_translations.error_password_reset_link_was_expired'));
        }     
        return redirect('/login');   
    } 



    public function initialize_expet($user_id)
    {
      $arr_pack_details = array();
      $arr_user_subcription = array();

      if (isset($user_id) && $user_id!=null) 
      {
        $obj_pack_details =  $this->SubscriptionPacksModel->where('id',1)->first();

        if ($obj_pack_details!=FALSE) 
        {
            $arr_pack_details = $obj_pack_details->toArray();

            if (sizeof($arr_pack_details)>0) 
            {
              if (isset($arr_pack_details['pack_price'])) 
              {
                  $invoice_id = $this->_generate_invoice_id();

                  /*First make transaction  */ 
                  $arr_transaction['user_id']          = $user_id;
                  $arr_transaction['invoice_id']       = $invoice_id;
                  /*transaction_type is type for transactions 1-Subscription 2-Milestone 3-Release Milestones */
                  $arr_transaction['transaction_type'] = 1;
                  $arr_transaction['payment_method']   = 0;
                  $arr_transaction['payment_status']   = 1;
                  $arr_transaction['payment_date']     = date('c');
                  $arr_transaction['paymen_amount']    = $arr_pack_details['pack_price'];
                  $arr_transaction['payment_date']     = date('c');


                  /* get currency for transaction */
                  /*$arr_currency = setCurrencyForTransaction();

                  $arr_transaction['currency']      = isset($arr_currency['currency']) ? $arr_currency['currency'] : '$';
                  $arr_transaction['currency_code'] = isset($arr_currency['currency_code']) ? $arr_currency['currency_code'] : 'USD';*/


                  $transaction = $this->TransactionsModel->create($arr_transaction);

                  if ($transaction) 
                  {
                  
                    /*insert record in user_subcription for selected pack and invoice details*/
                  $arr_user_subcription['user_id']                      = $user_id;
                  $arr_user_subcription['subscription_pack_id']         = isset($arr_pack_details['id'])?$arr_pack_details['id']:'';
                  $arr_user_subcription['invoice_id']                   = $invoice_id;
                  $arr_user_subcription['expiry_at']                    = null;
                  $arr_user_subcription['pack_name']                    = isset($arr_pack_details['pack_name'])?$arr_pack_details['pack_name']:'';
                  $arr_user_subcription['pack_price']                   = isset($arr_pack_details['pack_price'])?$arr_pack_details['pack_price']:'';
                  $arr_user_subcription['validity']                     = isset($arr_pack_details['validity'])?$arr_pack_details['validity']:'';
                  $arr_user_subcription['number_of_bids']               = isset($arr_pack_details['number_of_bids'])?$arr_pack_details['number_of_bids']:'0';
                  $arr_user_subcription['topup_bid_price']              = isset($arr_pack_details['topup_bid_price'])?$arr_pack_details['topup_bid_price']:'0';
                  $arr_user_subcription['number_of_categories']         = isset($arr_pack_details['number_of_categories'])?$arr_pack_details['number_of_categories']:'0';
                  $arr_user_subcription['number_of_subcategories']      = isset($arr_pack_details['number_of_subcategories'])?$arr_pack_details['number_of_subcategories']:'0';
                  $arr_user_subcription['number_of_skills']             = isset($arr_pack_details['number_of_skills'])?$arr_pack_details['number_of_skills']:'0';
                  $arr_user_subcription['number_of_favorites_projects'] = isset($arr_pack_details['number_of_favorites_projects'])?$arr_pack_details['number_of_favorites_projects']:'0';
                  $arr_user_subcription['website_commision']            = isset($arr_pack_details['website_commision'])?$arr_pack_details['website_commision']:'0';
                  $arr_user_subcription['allow_email']                  = isset($arr_pack_details['allow_email'])?$arr_pack_details['allow_email']:'0';
                  $arr_user_subcription['allow_chat']                   = isset($arr_pack_details['allow_chat'])?$arr_pack_details['allow_chat']:'0';
                  $arr_user_subcription['is_active']                    = 1;

                  $subscription = $this->SubscriptionUsersModel->create($arr_user_subcription);

                  return true;
              }
            }
          }
        }
      }
    }


   private function _generate_invoice_id()
   {
      $secure = TRUE;    
        $bytes = openssl_random_pseudo_bytes(3, $secure);
        $order_token = 'INV'.date('Ymd').strtoupper(bin2hex($bytes));
        return $order_token;
   }

   /* 
    Comments : common function to handle redirection.
    Auther   : Nayan S.
   */

    public function handle_email_redirection(Request $request)
    {
      $redirect = \Request::get('redirect');  
      $id       = \Request::get('id');
      
      if($redirect == 'COMPLETED')
      {
        $user = Sentinel::check();
        if($user != false )
        { 
          if($user->inRole('admin'))
          {
            return redirect('admin/projects/completed'); 
          }
        } 
        else
        {
          Session::put('COMPLETED','COMPLETED');
          return redirect('admin/login');  
        }
      }
      else if($redirect == 'DISPUTE')
      {

        $user = Sentinel::check();
        if($user != false)
        {
          if($user->inRole('admin') != false)
          {
              return redirect('admin/dispute'); 
          }
        } 
        else
        {
          Session::put('DISPUTE','DISPUTE');
          return redirect('admin/login');
        }
        
      }
      else if($redirect == 'MILESTONE_APPROVED' && $id != "" )
      {
          $user = Sentinel::check();
          
          Session::put('PROJECT_ID',$id);
          
          if($user != false)
          { 
            if($user->inRole('admin') != false )
            {
              return redirect('admin/project_milestones/all/'.$id); 
            }
          } 
          else
          {
            Session::put('MILESTONE_APPROVED','MILESTONE_APPROVED');
            return redirect('admin/login');
          }
      }
      else if($redirect == 'PROJECT')
      {
          $user = Sentinel::check();
          
          Session::put('PROJECT_ID',$id);
          
          if($user)
          { 
            $is_open_project = $this->checkProjectStatus($id);
            
            if($is_open_project == TRUE)
            {
              return redirect('/projects/details/'.$id);
            }
            else if($is_open_project == FALSE)
            {
              $is_valid = $this->validProjectForRespectiveUser($id);           
              
              if($is_valid != FALSE)
              {
                return redirect('/'.$is_valid.'/projects/details/'.$id);
              }
            } 
            return redirect('login');
          }
          else
          {
            return redirect('login');
          }
      }
      else if($redirect == 'MILESTONE')
      { 
          $user = Sentinel::check();
          
          Session::put('MILESTONE','MILESTONE');
          Session::put('PROJECT_ID',$id);
          
          if($user)
          { 
            $is_valid = $this->validProjectForRespectiveUser($id);           
            
            if($is_valid != FALSE)
            {
              return redirect('/'.$is_valid.'/projects/milestones/'.$id);
            }
            return redirect('login');
          }
          else
          {
            return redirect('login');
          }
      }
      else
      {
        return redirect('login');
      }
   } 
    public function validProjectForRespectiveUser($enc_id)
    {
      $id = base64_decode($enc_id);

      $user = Sentinel::check();

      if($user->inRole('client'))
      {
        $cnt_project = $this->ProjectpostModel->where('id','=',$id)->where('client_user_id','=',$user->id)->count();

        if($cnt_project == 1)
        {
          return 'client';  
        }
      }
      else if('expert')
      {
        $cnt_project = $this->ProjectpostModel->where('id','=',$id)->where('expert_user_id','=',$user->id)->count();
        if($cnt_project == 1)
        {
          return 'expert';  
        }
      }
      else if('project_manager')
      {
        $cnt_project = $this->ProjectpostModel->where('id','=',$id)->where('project_manager_user_id','=',$user->id)->count();
        if($cnt_project == 1)
        {
          return 'project_manager';  
        }
      }
      return FALSE;  
    } 
    public function checkProjectStatus($enc_id)
    {
      $id = base64_decode($enc_id);

      $obj_project = $this->ProjectpostModel->where('id','=',$id)->first();

      if($obj_project)
      {
        if(isset($obj_project->project_status) && $obj_project->project_status == '2')
        {
          return TRUE;
        } 
      }
      return FALSE;
    }

    function delete_account()
    {
      $user = Sentinel::check();
      if($user)
      {
          /*$get_project_status = $this->ProjectpostModel
                                     ->with(['project_milestones'])
                                     ->where('client_user_id',$user->id)
                                     ->where('project_status','!=','3')
                                     ->where('project_status','!=','5')
                                     ->orWhere('expert_user_id',$user->id)
                                     ->orWhere('project_manager_user_id',$user->id)  
                                     ->get();*/
          if($user->inRole('client') || $user->inRole('expert'))
          {
              $wallet_amount = 0;
              if(isset($user->mp_wallet_id) && $user->mp_wallet_id!='')
              {
                $get_mangopay_wallet_details = $this->WalletService->get_wallet_details($user->mp_wallet_id);
                if(isset($get_mangopay_wallet_details))
                {
                  $mangopay_wallet         = $get_mangopay_wallet_details;
                  $mangopay_wallet_details = $mangopay_wallet;
                }

                $wallet_amount = isset($mangopay_wallet_details->Balance->Amount)? $mangopay_wallet_details->Balance->Amount/100:'0';
              } 

              $enc_user_id = isset($user->id) ? $user->id : 0;
              $is_exist_milestone = $this->MilestonesModel
                                                    ->where('is_milestone','!=','3')
                                                    ->where(function($query) use($enc_user_id){
                                                        $query->where('client_user_id',$enc_user_id);
                                                        $query->orWhere('expert_user_id',$enc_user_id);
                                                    })
                                                    ->count();

              if($is_exist_milestone > 0)
              {
                  Session::flash('error',trans('controller_translations.please_complete_your_milestone_for_delete_your_account'));

                  if($user->inRole('client'))
                  {
                    return redirect(url('client/dashboard'));
                  }
                  else if($user->inRole('expert'))
                  {
                    return redirect(url('expert/dashboard'));
                  }
                  else if($user->inRole('project_manager'))
                  {
                    return redirect(url('/login'));
                  }
              }

              if($wallet_amount > 0)
              {
                 Session::flash('error',trans('controller_translations.wallet_for_delete_your_account'));

                  if($user->inRole('client'))
                  {
                    return redirect(url('client/dashboard'));
                  }
                  else if($user->inRole('expert'))
                  {
                    return redirect(url('expert/dashboard'));
                  }
                  else if($user->inRole('project_manager'))
                  {
                    return redirect(url('/login'));
                  }
              }
          }

          $email = isset($user->email)?$user->email:'';
          $delete_mail = "ae_deleted_".$email;

          $user_data['mp_user_id'] = isset($user->mp_user_id)?$user->mp_user_id:'';
          $user_data['email'] = $delete_mail;

          if($user->inRole('client'))
          {
            //DB::table('clients')->where('user_id',$user->id)->delete();
            $update_arr = array('email'=>$delete_mail,'password'=>"-",'is_active'=>'0','deleted_at'=>date('Y-m-d H:i:s'));
            DB::table('users')->where('id',$user->id)->update($update_arr);

            if(isset($user->mp_wallet_created) && $user->mp_wallet_created == 'Yes')
            {
              $this->WalletService->update_user($user_data); 
            }

            Session::flash('success',trans('controller_translations.your_account_has_been_deleted_successfully'));
            Sentinel::logout();
            return redirect(url('/login'));
          }
          else if($user->inRole('expert'))
          { 
            //DB::table('experts')->where('user_id',$user->id)->delete();
            $update_arr = array('email'=>$delete_mail,'password'=>"-",'is_active'=>'0','deleted_at'=>date('Y-m-d H:i:s'));
            DB::table('users')->where('id',$user->id)->update($update_arr);

            if(isset($user->mp_wallet_created) && $user->mp_wallet_created == 'Yes')
            {
              $this->WalletService->update_user($user_data); 
            }

            Session::flash('success',trans('controller_translations.your_account_has_been_deleted_successfully'));
            Sentinel::logout();
            return redirect(url('/login'));
          }
          else if($user->inRole('project_manager'))
          {
            //DB::table('project_manager')->where('user_id',$user->id)->delete();
            $update_arr = array('email'=>$delete_mail,'password'=>"-",'is_active'=>'0','deleted_at'=>date('Y-m-d H:i:s'));
            DB::table('users')->where('id',$user->id)->update($update_arr);

            if(isset($user->mp_wallet_created) && $user->mp_wallet_created == 'Yes')
            {
              $this->WalletService->update_user($user_data); 
            }

            Session::flash('success',trans('controller_translations.your_account_has_been_deleted_successfully'));
            Sentinel::logout();
            return redirect(url('/login'));
          }
          else {
              Session::flash('error',trans('controller_translations.something_went_wrong_please_try_again_later'));
              Sentinel::logout();
              return redirect(url('/login'));
          }
      }
      else {
          Session::flash('error',trans('controller_translations.something_went_wrong_please_try_again_later'));
          Sentinel::logout();
          return redirect(url('/login'));
      }
    }
} // end class
