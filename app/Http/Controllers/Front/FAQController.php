<?php
namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\FAQModel;
use App\Models\FAQTranslationModel;
use App\Common\Services\CommonDataService;
use App\Common\Services\LanguageService;  

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;


use Validator;
use Sentinel;
use Session;
use Mail;
use Activation;
use Reminder;
use URL;

class FAQController extends Controller
{
    public $arr_view_data;
    public $admin_panel_slug;
    /*
    | Constructor : 
    | auther : Sagar Sainkar
    */
    public function __construct(FAQModel $FAQModel,FAQTranslationModel $FAQTranslationModel,LanguageService $langauge)
    {
      $this->FAQModel                = $FAQModel;
      $this->FAQTranslationModel     = $FAQTranslationModel;
      $this->LanguageService         = $langauge;
      $this->arr_view_data           = [];
      $this->admin_panel_slug        = config('app.project.admin_panel_slug');
      $this->module_url_path         = url("/faq");
    }
    public function index($type=null)
    {
      $type_status = "";
      $view_status = "";
      if($type=='client')
      {
        $type_status = '1';
        $view_status = trans('faq.text_faq_for_client');
      }
      elseif($type=='expert')
      {
        $type_status = '2'; 
        $view_status = trans('faq.text_faq_for_expert');
      }

      $arr_question = [];
      $this->arr_view_data['page_title']      = "FAQ";
      $this->arr_view_data['module_url_path'] = $this->module_url_path;
      $arr_lang     =  $this->LanguageService->get_all_language();  
      $obj_question = $this->FAQModel->where('is_active','1')->where('faq_type',$type_status)->paginate(15);
      if($obj_question != FALSE)
      {
          $arr_pagination   = clone $obj_question; 
          $arr_question    = $obj_question->toArray();
      }

      $this->arr_view_data['faq_type']        = $view_status;
      $this->arr_view_data['arr_question']    = $arr_question;
      $this->arr_view_data['arr_pagination']  = $arr_pagination;
      return view('front.home.faq',$this->arr_view_data);
    }
    public function make_pagination_links($items,$perPage)
    {
        $pageStart = \Request::get('page', 1);
        // Start displaying items from this number;
        $offSet = ($pageStart * $perPage) - $perPage; 
        // Get only the items you need using array_slice
        $itemsForCurrentPage = array_slice($items, $offSet, $perPage, true);
        return new LengthAwarePaginator($itemsForCurrentPage, count($items), $perPage,Paginator::resolveCurrentPage(), array('path' => Paginator::resolveCurrentPath()));
    }  
} // end class
