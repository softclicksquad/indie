<?php
namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\UserModel;
use App\Models\ProjectMessagingThreadsModel;
use App\Models\ProjectpostModel;

/*    START - Laravel messanger */

use Cmgmyr\Messenger\Models\Thread as MessangerThreadModel;
use Cmgmyr\Messenger\Models\Message as MessangerMessageModel;
use Cmgmyr\Messenger\Models\Participant as MessangerParticipantModel;

use Carbon\Carbon;
use Messagable;
    
/*    END - Laravel messanger */

use Sentinel;
use Validator;
use Session;
use Mail;

use App\Common\Services\NotificationService;

/* for firing events */
/*use Event;
use App\Events\GetMessageOnChatting;*/
/* for firing events ends */

class MessagingController extends Controller
{
    public $arr_view_data;

    public function __construct(
                                UserModel $user,
                  							MessangerMessageModel $messanger_message,
                  							MessangerParticipantModel $messanger_participant,
                  							MessangerThreadModel $messanger_thread,
                                ProjectMessagingThreadsModel $project_messaging_threads,
                                ProjectpostModel $projects
                               )

    {
      if(! $user = Sentinel::check()) 
      {
        return redirect('/login');
      }

      $this->user_id              = $user->id;
      $this->UserModel            = $user;

      $this->MessangerMessageModel        = $messanger_message;
      $this->MessangerParticipantModel    = $messanger_participant;
      $this->MessangerThreadModel         = $messanger_thread;
      $this->ProjectMessagingThreadsModel = $project_messaging_threads;
      $this->ProjectpostModel             = $projects;

      $this->arr_view_data        = [];
      $this->module_url_path      = url("/conversation");
    }

    /* Show Messaging View */

    public function show_conversations($enc_project_id,$enc_recipient_id)
    { 
      if(! $user = Sentinel::check()) 
      {
        return redirect('/login');
      }

      $thread_id = FALSE;

      /* Here We have to create a thread */

      $project_id   = base64_decode($enc_project_id);
      $recipient_id = (int) base64_decode($enc_recipient_id);
      
      $project_handle_by  = '1';
      $project_name       = "";
      $last_msg_created_at = "";
      $arr_sidebar_data   = [];
      $arr_conversation   = [];

      if( $project_id == "" || $recipient_id == "" )
      {
        return redirect()->back();
      }  

      /* find experts user id from user table */

      $logged_user_role = $this->get_user_role($this->user_id);
 
      $recipient_role   = $this->get_user_role($recipient_id);

     /* Check user available or not for client ,expert and project manager*/

      $arr_user_available =  array();
      $obj_user = $this->UserModel->where('id',$recipient_id)->first(['is_available']);
      if($obj_user!=FALSE)
      {
        $arr_user_available = $obj_user->toArray();
      }

      /* Checking if project handeled by project manager or not ?*/
      $obj_projects = $this->ProjectpostModel->where('id','=',$project_id)->first();

      $arr_projects = [];

      if($obj_projects)
      {
        $arr_projects = $obj_projects->toArray();

        if( count($arr_projects) > 0 )
        {
          $project_handle_by = $arr_projects['project_handle_by'];
          $project_name      = $arr_projects['project_name'];
        }

      }
      else
      {
        Session::flash("error",trans('controller_translations.error_unauthorized_action')); /* project not found */
        return redirect()->back();    
      }
      /* ends */

      /*---------------------------------------------------------------------------------------------------------
        First section : If Logged In user role is client then initialize the chatting and create threads
       ---------------------------------------------------------------------------------------------------------*/
      
      if($logged_user_role == 'client' )
      {
          $obj_thread_exist = $this->ProjectMessagingThreadsModel->where('project_id','=',$project_id)
                                                                 ->where('client_user_id','=',$this->user_id);

          if($recipient_role == "expert")
          {
            $obj_thread_exist = $obj_thread_exist->where('expert_user_id','=',$recipient_id)
                                                 ->first(); 
          }                                                       

          if($recipient_role == "project_manager" && $project_handle_by == "2")
          {
            $obj_thread_exist = $obj_thread_exist->where('project_manager_user_id','=',$recipient_id)
                                                 ->first(); 
          }

          if($obj_thread_exist == FALSE)
          { 
              /* Create New Thread because only client can create thread */

              $result_thread_id = $this->create_messanger_thread($this->user_id,$recipient_id);

            
              if($result_thread_id != "")
              {
                $arr_thread_data  = [];
                
                $arr_thread_data['client_user_id'] = $this->user_id;
                $arr_thread_data['project_id']     = $project_id;

                if($recipient_role == "expert")
                {
                  $arr_thread_data['message_thread_id']      = $result_thread_id;  
                  $arr_thread_data['expert_user_id'] = $recipient_id; 
                }                                                       

                if($recipient_role == "project_manager" && $project_handle_by == "2")
                {
                  $arr_thread_data['message_thread_id']  = $result_thread_id;  
                  $arr_thread_data['project_manager_user_id'] = $recipient_id;
                }    

                $obj_result = $this->ProjectMessagingThreadsModel->create($arr_thread_data);        
                if($obj_result)
                {
                  $thread_id = $result_thread_id;
                }
                else
                { 
                  Session::flash("error",trans('controller_translations.error_error_while_starting_a_conversation')); 
                  return redirect()->back();
                }
                
              }
              else
              {
                Session::flash("error",trans('controller_translations.error_error_while_starting_a_conversation')); 
                return redirect()->back();
              }

          }       
          else
          {
            $arr_thread = $obj_thread_exist->toArray();
            if($recipient_role == "expert")
            {
              $thread_id  = $arr_thread['message_thread_id'];  
            }                                                       

            if($recipient_role == "project_manager" && $project_handle_by == "2")
            {
              $thread_id  = $arr_thread['message_thread_id'];
            }
         
          }     

      }
      else if($logged_user_role == 'expert' && $recipient_role == "client" )
      {
          $obj_thread_exist = $this->ProjectMessagingThreadsModel->where('project_id','=',$project_id)
                                                                 ->where('client_user_id','=',$recipient_id)
                                                                 ->where('expert_user_id','=',$this->user_id)
                                                                 ->first();  
          // dd($obj_thread_exist);                                                      
          if($obj_thread_exist == FALSE)
          { 
              Session::flash("error",trans('controller_translations.error_client_has_not_yet_started_conversation_with_you')); 
              return redirect()->back();
          }       
          else
          {
            $arr_thread = $obj_thread_exist->toArray();
            $thread_id  = $arr_thread['message_thread_id'];
          } 
      }


      /*-----------------------------------------
        First section ends
       ----------------------------------------*/

      /*-------------------------------------------------------------------------------------------------------------
       Second Section : If Logged In user role is project manager then initialize the chatting and create threads
      ---------------------------------------------------------------------------------------------------------------*/
     
        if($logged_user_role == 'project_manager' && $project_handle_by == "2" )
        {
            $obj_thread_exist = $this->ProjectMessagingThreadsModel->where('project_id','=',$project_id)
                                                                   ->where('project_manager_user_id','=',$this->user_id);

            if($recipient_role == "expert")
            {
              $obj_thread_exist = $obj_thread_exist->where('expert_user_id','=',$recipient_id)
                                                   ->first(); 
            }                                                       

            if($recipient_role == "client")
            {
              $obj_thread_exist = $obj_thread_exist->where('client_user_id','=',$recipient_id)
                                                   ->first(); 
            }

            if($obj_thread_exist == FALSE)
            { 
                /* Create New Thread because only client can create thread */

                $result_thread_id = $this->create_messanger_thread($this->user_id,$recipient_id);

                if($result_thread_id != "")
                {
                  $arr_thread_data  = [];
                  $arr_thread_data['project_id']     = $project_id;
                  $arr_thread_data['project_manager_user_id'] = $this->user_id;

                  if($recipient_role == "expert")
                  {
                    $arr_thread_data['message_thread_id']  = $result_thread_id;  
                    $arr_thread_data['expert_user_id']     = $recipient_id; 
                  }                                                       

                  if($recipient_role == "client" )
                  {
                    $arr_thread_data['message_thread_id']  = $result_thread_id;  
                    $arr_thread_data['client_user_id']     = $recipient_id;
                  }    

                  $obj_result = $this->ProjectMessagingThreadsModel->create($arr_thread_data);        
                  
                  if($obj_result)
                  {
                    $thread_id = $result_thread_id;
                  }
                  else
                  { 
                    Session::flash("error", trans('controller_translations.error_error_while_starting_a_conversation')); 
                    return redirect()->back();
                  }
                  
                }
                else
                {
                  Session::flash("error", trans('controller_translations.error_error_while_starting_a_conversation')); 
                  return redirect()->back();
                }

            }       
            else
            {
              $arr_thread = $obj_thread_exist->toArray();

              if($recipient_role == "expert")
              {
                $thread_id  = $arr_thread['message_thread_id'];
              }                                                       

              if($recipient_role == "client")
              {
                $thread_id  = $arr_thread['message_thread_id'];
              }
           
            }     

        }
        else if($logged_user_role == 'expert' && $recipient_role == "project_manager" )
        {
            $obj_thread_exist = $this->ProjectMessagingThreadsModel->where('project_id','=',$project_id)
                                                                   ->where('project_manager_user_id','=',$recipient_id)
                                                                   ->where('expert_user_id','=',$this->user_id)
                                                                   ->first();  

            if($obj_thread_exist == FALSE)
            { 
                Session::flash("error",trans('controller_translations.error_project_manager_has_not_yet_started_conversation_with_you')); 
                return redirect()->back();
            }       
            else
            {
              $arr_thread = $obj_thread_exist->toArray();
              $thread_id  = $arr_thread['message_thread_id']; 
            } 
        }
        
      /*-----------------------------------------
        Second section ends
       ----------------------------------------*/

       if ($thread_id==FALSE) 
       {
          return back()->with('error','Client has not yet started conversation with you.');
       }

      /*----------------------------------------------
        Get conversation with current logged in user
      ------------------------------------------------*/

      $threads = $this->MessangerMessageModel
                          ->with( 'user' )      
                          ->where(['thread_id' => $thread_id])
                          ->orderBy('id', 'ASC')
                          ->get();
      
      if($threads)
      {
          $arr_conversation = $threads->toArray();
      }

      /*-------Ends --------*/

      /*----------------------------------------------------------------
        Get Sidebar data of conversation page from show inbox function
      ------------------------------------------------------------------*/

      $arr_sidebar_data = $this->show_inbox($is_conversation_page = TRUE);

        
      
      /*-------Ends --------*/


      $arr_threads = [];

      $obj_notification   = app(NotificationService::class);
      $arr_notification   = $obj_notification->show_notifications();
      
      if(isset($arr_notification['thread_info_of_msgs']) && count($arr_notification['thread_info_of_msgs']) > 0)
      {
        foreach ($arr_notification['thread_info_of_msgs'] as $key => $msg) 
        {
          if($msg['thread_id'] != "" && ( (int) $msg['unread_messages'] > 0 ) ) 
          {
            array_push($arr_threads,$msg['thread_id']);
          }
        }
      }


      foreach($arr_sidebar_data as $key => $chat_data )
      {
        $last_msg_created_at = 0;

        if(isset($chat_data['message_thread_id']) && $chat_data['message_thread_id'] != "")
        {
          if(in_array($chat_data['message_thread_id'],$arr_threads))
          {
            $arr_sidebar_data[$key]['unread'] = '1';
          }
          else
          {
            $arr_sidebar_data[$key]['unread'] = '0';
          } 
        }
        else
        {
          $arr_sidebar_data[$key]['unread'] = '0';
        }

        if(isset($chat_data['messages'][0]['created_at']))
        {
          $last_msg_created_at = strtotime($chat_data['messages'][0]['created_at']);
        }

        $arr_sidebar_data[$key]['last_msg_created_at'] = $last_msg_created_at;    

      }
    //dd($arr_sidebar_data);
      /* sort array by date DESC order */
      $_sortBy = [];

      foreach ($arr_sidebar_data as $key => $row)
      {
        $_sortBy['last_msg_created_at'][$key] = $row['last_msg_created_at'];
        $_sortBy['unread'][$key] = $row['unread'];
      }
      
      array_multisort($_sortBy['unread'], SORT_DESC, $_sortBy['last_msg_created_at'], SORT_DESC, $arr_sidebar_data);

      //$arr_sidebar_data = $this->_sortBy($arr_sidebar_data, 'unread', SORT_DESC);

      $this->arr_view_data['arr_conversation']    = $arr_conversation;
      $this->arr_view_data['enc_project_id']   = $enc_project_id;
      $this->arr_view_data['arr_sidebar_data']    = $arr_sidebar_data;     
      $this->arr_view_data['page_title']          = trans('controller_translations.page_title_conversations');
      $this->arr_view_data['thread_id']           = $thread_id;
      $this->arr_view_data['project_name']        = $project_name;
      $this->arr_view_data['arr_user_available']  = $arr_user_available;
      $this->arr_view_data['module_url_path']     = $this->module_url_path;

      return view('conversation.conversation',$this->arr_view_data);
    }

    /* find roles of particular user By user_id */
    
    public function get_user_role($user_id) 
    {
      $obj_role  = Sentinel::findById($user_id)->roles()->first();
      
      $role_slug = "";

      if($obj_role)
      {
        $role_slug = $obj_role->slug; 
      }

      return $role_slug;
    }

    /* send message */

    public function send_message(Request $request)
    {   
        $arr_rules['thread_id']   = "required";
        $arr_rules['message']     = "required";

        $thread_id  = base64_decode($request->input('thread_id'));
        if($thread_id == "")
        {
          Session::flash("error",trans('controller_translations.error_error_while_sending_message') ); 
          return redirect()->back();
        }

        $body       = $request->input('message');

        $json = [];

        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        } 

        $message = $this->MessangerMessageModel->create(
            [
                'thread_id' => $thread_id,
                'user_id'   => Sentinel::getUser()->id,
                'body'      => $body, 
            ]
        );

        /* Add replier as a participant*/
        $participant = $this->MessangerParticipantModel->firstOrCreate(
            [
                'thread_id' => $thread_id,
                'user_id'   => Sentinel::getUser()->id,
            ]
        );
        $participant->last_read = new Carbon;
        $participant->save();

        if($message)
        {
            //Session::flash("success","Message Sent Successfully."); 
         
            $arr_user = $this->user_info($this->user_id);
            $first_name =  $last_name = $profile_image = "";
            if(count($arr_user)> 0)
            {
              $first_name     = isset($arr_user['first_name'])?$arr_user['first_name']:'';
              $last_name      = isset($arr_user['last_name'])?$arr_user['last_name']:'';  
              $profile_image  = isset($arr_user['profile_image'])?$arr_user['profile_image']:'';  
            }


            $user_name  = Sentinel::getUser()->user_name;

            $time = strtotime(date('Y-m-d H:i:s'));
            $json['status']         = "SUCCESS";
            $json['msg']            = trans('controller_translations.msg_message_sent_successfully') ;
            $json['time_ago']       = $this->humanTiming($time);
            //$json['full_name']      = $first_name.' '.$last_name;
            $json['user_name']      = $user_name;
            $json['profile_image']  = $profile_image;
            
        }
        else
        {
            //Session::flash("error","Error While Sending Message."); 
            $json['status'] = "ERROR";
            $json['msg']    = trans('controller_translations.msg_error_while_sending_message');
        }

        return response()->json($json);
        //return redirect()->back();
    }


    /* create threads */

    public function create_messanger_thread($sender, $recipient)
    {
        $thread = MessangerThreadModel::create(
            [
                'subject' => 'default'
            ]
        );

        $thread_id = $thread->id;  
       
        /* Mmaking Participants */             

        $this->make_participants($thread_id,$sender);

        $this->make_participants($thread_id,$recipient);

        return $thread_id;
    }

    /* Make Participants */

    public function make_participants($thread_id , $participant)
    {
        MessangerParticipantModel::create(
            [
                'thread_id' => $thread_id,
                'user_id'   => $participant,
                'last_read' => new Carbon,
            ]
        );

        return true;
    }

    /*
      Comments : Show inbox of user ( expert, client & project manager ). 
      Auther   : Nayan S.
    */

    public function show_inbox($is_conversation_page = FALSE)
    {
      $module_url_path  = ""; /* setting up defalult value */
      $arr_conversation = [];
      $user_id          = $this->user_id;


      $user_role = $this->get_user_role($user_id);


      if($user_role == "expert")
      {
        $module_url_path  = url('/').'expert/inbox';

        $obj_threads = $this->ProjectMessagingThreadsModel->where('expert_user_id','=',$user_id)
                                                          ->with(['messages'=> function ($query) use ($user_id) 
                                                            {
                                                                $query->orderBy('id','DESC');
                                                            }])
                                                          ->with(['project_data'=>function ($query_one) 
                                                            {
                                                              $query_one->select('id','project_name');
                                                            }])
                                                          ->with(['chat_participants' => function ($query_nxt) use ($user_id) 
                                                            {
                                                                $query_nxt->where('user_id','<>',$user_id);
                                                            }])
                                                          /*->with(['client_availability'])*/
                                                          ->get();


        if($obj_threads)
        {
          $arr_conversation = $obj_threads->toArray();
        }                                                            

      } 
      else if($user_role == "client")
      {
        $module_url_path  = url('/').'client/inbox';

        $obj_threads = $this->ProjectMessagingThreadsModel->where('client_user_id','=',$user_id)
                                                          ->with(['messages'=> function ($query) use ($user_id) 
                                                            {
                                                               $query->orderBy('id','DESC');
                                                            }])
                                                          ->with(['project_data'=>function ($query_one) 
                                                            {
                                                              $query_one->select('id','project_name');
                                                            }])
                                                          ->with(['chat_participants' => function ($query_nxt) use ($user_id) 
                                                            {
                                                                $query_nxt->where('user_id','<>',$user_id);
                                                            }])
                                                          ->get();
        
        if($obj_threads)
        {
          $arr_conversation = $obj_threads->toArray();
        }  

      }
      else if($user_role == "project_manager")
      {
        $module_url_path      = url('/').'project_manager/inbox';

        $obj_threads = $this->ProjectMessagingThreadsModel->where('project_manager_user_id','=',$user_id)
                                                          ->with(['messages'=> function ($query) use ($user_id) 
                                                            {
                                                                $query->orderBy('id','DESC');
                                                            }])
                                                          ->with(['project_data'=>function ($query_one) 
                                                            {
                                                             $query_one->select('id','project_name');
                                                            }])
                                                          ->with(['chat_participants' => function ($query_nxt) use ($user_id) 
                                                            {
                                                                $query_nxt->where('user_id','<>',$user_id);
                                                            }]) 
                                                       ->get();

        
        if($obj_threads)
        {
          $arr_conversation = $obj_threads->toArray();
          
        }  
      }

      if($is_conversation_page == TRUE) 
      {
        return $arr_conversation; 
      } 

      $arr_threads = [];

      $obj_notification   = app(NotificationService::class);
      $arr_notification   = $obj_notification->show_notifications();
        
      if(isset($arr_notification['thread_info_of_msgs']) && count($arr_notification['thread_info_of_msgs']) > 0)
      {
        foreach ($arr_notification['thread_info_of_msgs'] as $key => $msg) 
        {
          if($msg['thread_id'] != "" && ( (int) $msg['unread_messages'] > 0 ) ) 
          {
            array_push($arr_threads,$msg['thread_id']);
          }
        }
      }


    if(isset($arr_conversation ) && count($arr_conversation) > 0 )
    { 

          foreach($arr_conversation as $key => $chat_data )
          {

            if(isset($chat_data['message_thread_id']) && $chat_data['message_thread_id'] != "")
            {
              if(in_array($chat_data['message_thread_id'],$arr_threads))
              {
                $arr_conversation[$key]['unread'] = '1';
              }
              else
              {
                $arr_conversation[$key]['unread'] = '0';
              } 
            }
            else
            {
              $arr_conversation[$key]['unread'] = '0';
            }

            /*code added for showing latet chat */ 

            $last_msg_created_at = 0;

            if(isset($chat_data['messages'][0]['created_at']))
            {
              $last_msg_created_at = strtotime($chat_data['messages'][0]['created_at']);
            }

            $arr_conversation[$key]['last_msg_created_at'] = $last_msg_created_at;   

          }

          /* sort array by date DESC order */
          $_sortBy = [];

          

          foreach ($arr_conversation as $key => $row)
          {
            $_sortBy['last_msg_created_at'][$key] = $row['last_msg_created_at'];
            $_sortBy['unread'][$key] = $row['unread'];
          }
          
          /* Please check this */
          
          if(isset($_sortBy) && count($_sortBy) > 0)
          {
            array_multisort($_sortBy['unread'], SORT_DESC, $_sortBy['last_msg_created_at'], SORT_DESC, $arr_conversation);
          }
         
         //$arr_conversation = $this->_sortBy($arr_conversation, 'unread', SORT_DESC);
      }

      $this->arr_view_data['page_title']            = trans('controller_translations.page_title_messages') ;  
      $this->arr_view_data['arr_conversation']      = $arr_conversation;
      $this->arr_view_data['module_url_path']       = $module_url_path;
      return view('conversation.inbox',$this->arr_view_data);
    } 

    /*
      Comments : Function to get User Information.
      Auther   : Nayan S. 
    */
    public function user_info($user_id)
    {
        $user_role = $this->get_user_role($user_id);

        if( $user_role == "client" )
        {
            $model = app(\App\Models\ClientsModel::class);
        } 
        else if( $user_role == "expert" )
        {
            $model = app(\App\Models\ExpertsModel::class);
        } 
        else if( $user_role == "project_manager" )
        {
            $model = app(\App\Models\ProjectManagerModel::class);
        }

        $obj_user = $model->where('user_id','=',$user_id)->first();
       
        $arr_user = [];

        if($obj_user)
        {
            $arr_user = $obj_user->toArray();
        }
        return $arr_user;
    }


    /*
      function to get time ago, 
    */

    public function humanTiming($time)
    {
      $time = time() - $time; // to get the time since that moment
      $time = ($time<1) ? 1 : $time;
      $tokens = array (
          31536000 => 'year',
          2592000 => 'month',
          604800 => 'week',
          86400 => 'day',
          3600 => 'hour',
          60 => 'minute',
          1 => 'second'
      );
      
        foreach ($tokens as $unit => $text) {
          if ($time < $unit) continue;
          $numberOfUnits = floor($time / $unit);
          return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
      }
    }
    
    /* 
    Comments : updates last read.
    */
    public function update_last_read(Request $request)
    {
      $thread_id = base64_decode($request->input('thread_id'));
      $user_id   = base64_decode($request->input('user_id'));

      $json = [];

      if($thread_id != "" && $user_id != "")
      {
          $obj_result = $this->MessangerParticipantModel->where('thread_id','=',$thread_id)
                                                        ->where('user_id','=',$user_id)
                                                        ->update(['last_read' => new Carbon]);
          if($obj_result)
          {
            $json['status'] = "SUCCESS";
          }                             
          else
          {
            $json['status'] = "ERROR";
          }   
      } 
      return response()->json($json);
    } 

    /* 
    Autehr   : Nayan S.
    Comments : Show user details.
    */

    public function user_details($enc_id)
    { 
      $user_id = base64_decode($enc_id);

      $role = "";

      if($user_id != "" && $enc_id != "")
      {
        $role = $this->get_user_role($user_id);

        if($role == "expert")
        {
          return redirect('experts/portfolio/'.$enc_id);
        }
        else if($role == "client")
        { 
          return redirect('clients/portfolio/'.$enc_id);
        }
        else if($role == "project_manager")
        {
          return redirect()->back()->with('err_project_manager', [trans('controller_translations.you_can_not_view_project_manager_profile')]);
          //return redirect('project_managers/portfolio/'.$enc_id);
        }
      }
      else
      {
        return redirect()->back();
      }
    }



    /*function for sort projects array with index value */

    public function _sortBy()
    {
        $args = func_get_args();
        $data = array_shift($args);
        foreach ($args as $n => $field) 
        {
            if (is_string($field))
            {
                $tmp = array();
                foreach ($data as $key => $row)
                    $tmp[$key] = $row[$field];
                $args[$n] = $tmp;
            }
        }
        $args[] = &$data;
        call_user_func_array('array_multisort', $args);
        return array_pop($args);
    }

}
