<?php
namespace App\Http\Controllers\Front;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App;
use Validator;
use Session;
use App\Models\ContestModel;
use App\Common\Services\ContestSearchService;
use App\Common\Services\SubscriptionService;
use Illuminate\Pagination\Paginator;
use App\Models\SkillsModel;
use App\Common\Services\LanguageService;  
use App\Models\CategoriesModel;  
use App\Models\ContestEntryModel;
use App\Models\ContestSkillsModel;
use App\Models\ExpertsSkillModel;
use App\Models\ExpertsCategoriesModel;
use App\Models\ContestPricingModel;
use App\Models\ContestPostDocumentsModel;
use App\Models\SubCategoriesModel;
use App\Models\SealedEntryRatesModel;
use App\Models\SealedEntriesTransactionsModel;

use Mail;
use Sentinel;
use ZipArchive;

class ContestListingController extends Controller
{
  public function __construct(
                                ContestModel $contest_post,
                                ContestSearchService $contest_search,
                                SubscriptionService $subscription_service,
                                SkillsModel $skills,
                                CategoriesModel $categories,
                                LanguageService $langauge,
                                ContestEntryModel $contest_entry,
                                ContestSkillsModel $ContestSkillsModel,
                                ExpertsSkillModel $ExpertsSkillModel,
                                ExpertsCategoriesModel $ExpertsCategoriesModel,
                                ContestPricingModel $ContestPricingModel,
                                ContestPostDocumentsModel $contest_post_documents_model,
                                SealedEntryRatesModel $sealed_entry_rates,
                                SealedEntriesTransactionsModel $sealed_entries_transactions
                             )
	{
        
        $this->arr_view_data                         = [];
        $this->ContestModel                          = $contest_post;
        $this->ContestPostDocumentsModel             = $contest_post_documents_model;
        $this->ContestSearchService                  = $contest_search;
        $this->SubscriptionService                   = $subscription_service;
        $this->SkillsModel                           = $skills;
        $this->CategoriesModel                       = $categories;
        $this->LanguageService                       = $langauge;
        $this->ContestEntryModel                     = $contest_entry;
        $this->ContestSkillsModel                    = $ContestSkillsModel;
        $this->ExpertsSkillModel                     = $ExpertsSkillModel;
        $this->ExpertsCategoriesModel                = $ExpertsCategoriesModel;
        $this->ContestPricingModel                   = $ContestPricingModel;
        $this->SubCategoriesModel                    = new SubCategoriesModel;
        $this->SealedEntryRatesModel                 = $sealed_entry_rates;
        $this->SealedEntriesTransactionsModel        = $sealed_entries_transactions;
        $this->contest_public_file_path              = url('/').config('app.project.img_path.contest_files');
        $this->contest_base_file_path                = base_path().'/public'.config('app.project.img_path.contest_files');
        $this->contest_send_entry_public_file_path   = url('/').config('app.project.img_path.contest_send_entry_files');
        $this->contest_send_entry_base_file_path     = base_path().'/public'.config('app.project.img_path.contest_send_entry_files');

        $this->obj_all_contest   = $this->ContestModel->where('is_active','!=','2')
                                              ->whereDate('contest_end_date','>',date('Y-m-d H:i:s'))
                                              ->where('payment_status','1')
                                              ->with(['skill_details','contest_skills.skill_data','contest_entry','category_details']);

        


        /* common data ends */
        $this->module_url_path = url("/contests");
	}
	/*
  | Comment : Project listing page
  | auther  : Nayan S.
  */
    public function index($enc_id = FALSE)
    {
        $this->arr_view_data['page_title'] = trans('controller_translations.page_title_contests');
        $arr_contests_data = [];
        $arr_pagination    = [];
        $max_contest_amt = 0;
        $min_contest_amt = 0;
        if($enc_id != FALSE && $enc_id == "")
        {
            return redirect()->back(); 
        }

        if(Session::has('show_contest_listing_cnt')) { $pagi_cnt = Session::get('show_contest_listing_cnt'); }else { $pagi_cnt = config('app.project.pagi_cnt'); }
        $obj_open_contests = $this->ContestModel
                                              ->where('is_active','!=','2')
                                              ->whereDate('contest_end_date','>',date('Y-m-d H:i:s'))
                                              ->where('payment_status','1')
                                              ->with(['skill_details','contest_skills.skill_data','contest_entry','category_details','sub_category_details'])
                                              ->orderBy('created_at','DESC');
        if(isset($show_cnt) && $show_cnt!=null)
        {
            $obj_open_contests = $obj_open_contests->paginate($pagi_cnt);
        }
        else
        {
            $obj_open_contests = $obj_open_contests->paginate($pagi_cnt);   
        }                                      
                                              
        if($obj_open_contests)
        {
            $arr_pagination    = clone $obj_open_contests;
            $arr_contests_data = $obj_open_contests->toArray();

            if(isset($arr_contests_data) && sizeof($arr_contests_data)>0)
            {
                $max_contest_amt = $obj_open_contests = $this->ContestModel
                                              ->where('is_active','!=','2')
                                              ->whereDate('contest_end_date','>',date('Y-m-d H:i:s'))
                                              ->where('payment_status','1')
                                              ->max('usd_contest_price');

                /*$min_contest_amt = $obj_open_contests = $this->ContestModel
                                              ->where('is_active','!=','2')
                                              ->whereDate('contest_end_date','>',date('Y-m-d H:i:s'))
                                              ->where('payment_status','1')
                                              ->min('usd_contest_price');*/
            }    
        }   
        
        /* get categories */
        $arr_lang       = $this->LanguageService->get_all_language();
        $arr_categories = [];
        $obj_category   = $this->CategoriesModel->where('categories.is_active','1')->with(['translations'])->get();
        if($obj_category != FALSE)        
        {
            $arr_categories = $obj_category->toArray();
        }
        /* get categories end */
        
        $arr_sidebar_data = $this->get_sidebar_array();
        //dd($arr_sidebar_data);
        $all_contest_count = $this->ContestModel->where('is_active','!=','2')
                                                ->whereDate('contest_end_date','>',date('Y-m-d H:i:s'))
                                                ->where('payment_status','1')
                                                ->count();


        $this->arr_view_data['arr_sidebar_data']         = $arr_sidebar_data;
        $this->arr_view_data['all_contest_count']        = $all_contest_count;
        $this->arr_view_data['max_contest_amt']          = (int) $max_contest_amt;
        $this->arr_view_data['min_contest_amt']          = (int) $min_contest_amt;
        $this->arr_view_data['arr_categories']           = $arr_categories; 
        $this->arr_view_data['arr_categories']           = $arr_categories; 
        $this->arr_view_data['arr_pagination']           = $arr_pagination;
        $this->arr_view_data['arr_contests']             = $arr_contests_data;
        $this->arr_view_data['module_url_path']          = $this->module_url_path;
        return view('front.contest_listing.listing',$this->arr_view_data);
    }

    public function get_sidebar_array()
    {
        $tmp_arr = [];
        $obj_all_contest = $this->obj_all_contest->get();
        
        if($obj_all_contest)
        {
          $all_contests = $obj_all_contest->toArray();
          
          foreach ($all_contests as $key => $value) 
          {
            $tmp_arr_contest_data = [];

            $tmp_arr_contests = $this->obj_all_contest->get()->toArray();
            $tmp_count = 0;
            foreach ($tmp_arr_contests as  $tmp_key => $tmp_value) 
            { 
              if($value['category_id'] == $tmp_value['category_id'])
              { 
                $tmp_count+=1;
              }
            } 
            $tmp_arr_contest_data['contest_count']  = $tmp_count;
            $tmp_arr_contest_data['category_title'] = isset($value['category_details']['category_title'])?$value['category_details']['category_title']:'';
            $tmp_arr_contest_data['category_slug']  = isset($value['category_details']['category_slug'])?$value['category_details']['category_slug']:'';
            $tmp_arr_contest_data['category_id']    = $value['category_id'];
            array_push($tmp_arr,$tmp_arr_contest_data);
          }
        }
        $arr_sidebar_data = [];
        if(count($tmp_arr)>0)
        {
          foreach ($tmp_arr as $key => $value) 
          {
            if(count($arr_sidebar_data) > 0)
            {
              if(!in_array($value,$arr_sidebar_data))
              {
                array_push($arr_sidebar_data,$value);  
              }
            }
            else 
            {
              array_push($arr_sidebar_data,$value);
            }
          } 
        }
        /* To sort array in alphabetically */
        $_sortBy = [];
        foreach ($arr_sidebar_data as $key => $row){
            $_sortBy[$key] = $row['category_title'];
        }
        array_multisort($_sortBy,SORT_ASC,$arr_sidebar_data);
        return $arr_sidebar_data;
    }

    public function search(Request $request)
    {
        $this->arr_view_data['page_title'] = trans('controller_translations.page_title_contests');

        $form_data = $request_data = $arr_projects = $arr_pagination = $arr_sidebar_data = $search_keys = $arr_subcategory = $arr_categories = $arr_contests_data = [];
        $max_contest_amt = $min_contest_amt = $all_contest_count = 0;

        $form_data = $request->all();

        $search_result = $this->ContestSearchService->make_filer_search($form_data);

        /*if($search_result == false)
        {
            return redirect($this->module_url_path);
        }*/
        if($search_result != false)
        {
            $search_result = $search_result->appends($form_data);

            if($search_result)
            {
                $arr_pagination    = clone $search_result;
                $arr_contests_data = $search_result->toArray();
                $request_data      = $form_data;
            }

            /* get categories */

            $category_id = '';
            if(isset($form_data['cat']) && is_array($form_data['cat']))
            {
                $category_id = '';
            }
            elseif(isset($form_data['cat']))
            {
                $category_id = base64_decode($form_data['cat']);
            }


            $arr_lang       = $this->LanguageService->get_all_language();
            $arr_categories = [];
            $obj_category   = $this->CategoriesModel->where('categories.is_active','1')
                                                    ->with(['translations'])
                                                    ->orderby('id','DESC')
                                                    ->get();
            if($obj_category != FALSE)        
            {
                $arr_categories = $obj_category->toArray();

                if(isset($arr_contests_data) && sizeof($arr_contests_data)>0)
                {
                    /*$max_contest_amt = $this->ContestModel->max('usd_contest_price');
                    $min_contest_amt = $this->ContestModel->min('usd_contest_price');*/

                    $max_contest_amt = $this->ContestModel
                                              ->where('is_active','!=','2')
                                              ->whereDate('contest_end_date','>',date('Y-m-d H:i:s'))
                                              ->where('payment_status','1')
                                              ->max('usd_contest_price');
                    $min_contest_amt = 0;
                }    
            }

            /* get categories end */

            $arr_sidebar_data = $this->get_sidebar_array();
            //dd($arr_sidebar_data);
            $all_contest_count = $this->ContestModel->where('is_active','!=','2')
                                                    ->whereDate('contest_end_date','>',date('Y-m-d H:i:s'))
                                                    ->where('payment_status','1')
                                                    ->count();

            $arr_subcategory = [];
            $obj_subcategory = $this->SubCategoriesModel->where('category_id',$category_id)->where('is_active','=','1')->get();
            if($obj_subcategory)
            {
                $arr_subcategory = $obj_subcategory->toArray();
            }
        }
        
        $this->arr_view_data['arr_subcategory']          = $arr_subcategory;
        $this->arr_view_data['arr_sidebar_data']         = $arr_sidebar_data;
        $this->arr_view_data['all_contest_count']        = $all_contest_count;
        $this->arr_view_data['max_contest_amt']          = (int) $max_contest_amt;
        $this->arr_view_data['min_contest_amt']          = (int) $min_contest_amt;
        $this->arr_view_data['request_data']             = $request_data; 
        $this->arr_view_data['arr_categories']           = $arr_categories; 
        $this->arr_view_data['arr_pagination']           = $arr_pagination;
        $this->arr_view_data['arr_contests']             = $arr_contests_data;
        $this->arr_view_data['module_url_path']          = $this->module_url_path;

        return view('front.contest_listing.listing',$this->arr_view_data);
    }

    public function show_cnt(Request $Request) 
    {
        \Session::put('show_contest_listing_cnt' , $Request->input('show_cnt'));
        $preurl = url()->previous();
        $explode_url = explode('?page=',$preurl);
        if(empty($explode_url[1])){
            return redirect()->back();
        }
        else{
            $redirect =  $explode_url[0].'?page='.'1';
            return redirect($redirect);
        }
    }

    public function show_contest_details($enc_id) 
    {   
        $arr_contest            = [];
        $arr_contest_entry_data = [];
        $arr_pagination         = [];
        $id = base64_decode($enc_id);
        if($id==""){
            return redirect()->back();
        }

        $arr_sealed_entry_rates = $arr_expert_sealed_entry_details = [];

        $is_entry_exists = "";
        $user = \Sentinel::check();
        if(isset($user) && $user!=false){
            if($user->inRole('expert')){
                $is_entry_exists = $this->ContestEntryModel
                                    ->where('contest_id',$id)
                                    ->where('expert_id',$user->id)
                                    ->count();
                
                $arr_expert_sealed_entry_details = $this->expert_sealed_entry_details($user->id);  
                // dd($arr_expert_sealed_entry_details);
                if(isset($arr_expert_sealed_entry_details['remaning_sealed_entries']) && $arr_expert_sealed_entry_details['remaning_sealed_entries'] <= 0 ) {

                    $expert_default_currency_code = isset($user->currency_code) ? $user->currency_code : 'USD';
                    $default_currency_code = 'USD';

                    $arr_sealed_entry_rates = [];
                    $obj_sealed_entry_rates = $this->SealedEntryRatesModel
                                                            ->where('is_active','1')
                                                            ->get();
                    if($obj_sealed_entry_rates){
                        $arr_sealed_entry_rates = $obj_sealed_entry_rates->toArray();
                    }

                    if(isset($arr_sealed_entry_rates) && count($arr_sealed_entry_rates)>0) {
                        foreach ($arr_sealed_entry_rates as $key => $value) 
                        {
                            $converted_price = 0;
                            $price = isset($value['price']) ? floatval($value['price']) : 0;
                            if( $expert_default_currency_code != $default_currency_code ) {
                                $converted_price = round(currencyConverterAPI($default_currency_code, $expert_default_currency_code, $price));
                            } else{
                                $converted_price = $price;
                            }
                            $arr_sealed_entry_rates[$key]['converted_price'] = $converted_price; 
                            $arr_sealed_entry_rates[$key]['currency_code']   = $expert_default_currency_code; 
                        }
                    }
                }
            } 
        }

        $obj_contest_data = $this->ContestModel->where('id',$id)
                                               ->where('contest_end_date','>',date('Y-m-d')) 
                                               ->where('is_active','!=','2')  
                                               ->with(['category_details'=>function($query)
                                                   {
                                                        $query->select('id');
                                                   }
                                                ,'skill_details','contest_skills.skill_data','contest_entry'=>function($query)
                                                {
                                                    $query->orderBy('id','ASC');    
                                                },'sub_category_details','contest_post_documents'])
                                               ->first();     

        if($obj_contest_data){
            $arr_contest = $obj_contest_data->toArray();
        }

        /*unauthorized_action*/
        if(empty($arr_contest)){
            Session::flash("error",trans('controller_translations.error_unauthorized_action'));
            return redirect($this->module_url_path);
        }
        //Get Contest Expert entry record
        $arr_contest_expert = [];
        if(isset($user) && $user!=false)
        {
            if($user->inRole('expert'))   
            {
                $login_expert_id = $user->id;
                $obj_contest_expert = $this->ContestEntryModel->where('contest_id',$id)    
                                                      ->with('contest_details','contest_entry_files','contest_entry_files_highest_rating')       
                                                      ->where('expert_id',$login_expert_id)              
                                                      ->first();
                if($obj_contest_expert)
                {
                    $arr_contest_expert = $obj_contest_expert->toArray();
                }  
            } 
        }

        //Get Contest Expert entry record
        $arr_contest_winner = [];

        if(isset($user) && $user!=false)
        {
            $obj_contest_winner = $this->ContestEntryModel->where('contest_id',$id)    
                                                  ->with('contest_details','contest_entry_files','contest_entry_files_highest_rating')       
                                                  ->where('is_winner','YES')            
                                                  ->first();
            if($obj_contest_winner)
            {
                $arr_contest_winner = $obj_contest_winner->toArray();
            }  
        }

        //Get Contest entry data
        $obj_contest_entry_data = $this->ContestEntryModel->with('contest_details','contest_entry_files','contest_entry_files_highest_rating')->where('contest_id',$id);
        if(isset($user) && $user!=false){
            if($user->inRole('expert')) {
                $obj_contest_entry_data = $obj_contest_entry_data->where('expert_id','<>',$login_expert_id);
            }
        }        
        $obj_contest_entry_data = $obj_contest_entry_data->orderBy('created_at','DESC')  
                                                         ->paginate(config('app.project.pagi_cnt'));
        if($obj_contest_entry_data){
            $arr_pagination         = clone $obj_contest_entry_data;
            $arr_contest_entry_data = $obj_contest_entry_data->toArray();
        }

        $user = \Sentinel::check();
        $chk_valid_category_and_skills = false;
        if(isset($user) && $user != null){
            // check category and skill is match or not
            $chk_valid_category_and_skills = $this->SubscriptionService->validate_contest_critearea($id);
            // endcheck category and skill is match or not
        }

        $this->arr_view_data['is_skill_category_matched'] = $chk_valid_category_and_skills;
        $this->arr_view_data['arr_contest_winner']        = $arr_contest_winner;
        $this->arr_view_data['arr_contest_expert']        = $arr_contest_expert;
        $this->arr_view_data['arr_pagination']            = $arr_pagination;
        $this->arr_view_data['arr_contest_entry_data']    = $arr_contest_entry_data;
        $this->arr_view_data['is_entry_exists']           = $is_entry_exists;
        $this->arr_view_data['contest_public_file_path']  = $this->contest_public_file_path;
        $this->arr_view_data['contest_base_file_path']    = $this->contest_base_file_path;
        $this->arr_view_data['arr_contest']               = $arr_contest; 
        $this->arr_view_data['page_title']                = trans('controller_translations.page_title_contest_details');
        $this->arr_view_data['module_url_path']           = $this->module_url_path;
        $this->arr_view_data['subscription_url']           = url('/').'/expert/subscription';
        
        $this->arr_view_data['arr_sealed_entry_rates']          = $arr_sealed_entry_rates;
        $this->arr_view_data['arr_expert_sealed_entry_details'] = $arr_expert_sealed_entry_details;

        return view('front.contest_listing.contest_details',$this->arr_view_data);
    }

    public function make_zip($project_id=FALSE)
    {
        if($project_id == FALSE){
          Session::flash('error', trans('controller_translations.something_went_wrong_please_try_again_later')); 
          return redirect()->back();
        }

        $project_main_attachment        = [];
        $project_attachment             = [];
        $arr_project                    = [];
        $project_id                     = base64_decode($project_id); 

        $obj_project_main_attachment    = $this->ContestPostDocumentsModel
                                                                      ->where('contest_id',$project_id)
                                                                      ->get();

        if($obj_project_main_attachment)
        {
          $project_main_attachment = $obj_project_main_attachment->toArray(); 
        }
        
        $obj_project = $this->ContestModel
                                            ->select('contest_title')
                                            ->where('id',$project_id)
                                            ->first();
        if($obj_project)
        {          
          $arr_project = $obj_project->toArray();
        }

        $project_name = isset($arr_project['contest_title']) ? $arr_project['contest_title'] : '';

        $files = [];

        if($project_main_attachment)
        {
            if($project_main_attachment)
            {
              foreach($project_main_attachment as $key => $attch)
              {
                if(file_exists(base_path().'/public/uploads/front/postcontest/'.$attch['image_name']))
                {
                  $files[] = public_path('uploads/front/postcontest/'.$attch['image_name']);
                }
              }
            }

            if(isset($files) && sizeof($files) > 0)
            {
              $project_name  = str_slug($project_name);  

              # create new zip opbject
              $zip = new ZipArchive();

              # create a temp file & open it

              // $tmp_file = tempnam('.','');
              
              $tmp_file = tempnam("/tmp", "");

              $zip->open($tmp_file, ZipArchive::CREATE);
              foreach($files as $file)
              {
                $download_file = file_get_contents($file);
                $zip->addFromString(basename($file),$download_file);
              }

              $zip->close();
              
              header('Content-disposition: attachment; filename='.$project_name.'-'.date('Y-m-d h-i-s').'.zip');
              header('Content-type: application/zip');
              readfile($tmp_file);
              return redirect()->back();
            } 
            else
            {
              Session::flash('error', 'No files found for creation of a ZIP file.'); 
              return redirect()->back();
            }        
        }
        else 
        {
          Session::flash('error', 'No files found for creation of a ZIP file.'); 
          return redirect()->back();
        }
    }

    public function show_contest_entry_details($enc_id)
    {
        ///  functionality blocked   //
            /*Session::flash("error",trans('controller_translations.error_unauthorized_action'));
            return redirect($this->module_url_path);*/
        ///  functionality blocked   //

        $arr_contest_entry_data = [];
        $contest_winner_id = 0;

        $contest_entry_id = base64_decode($enc_id);  
        
        $user = \Sentinel::check();
        if(!isset($user) && $user==false || $user==false)
        {
          Session::flash("error",trans('controller_translations.error_unauthorized_action'));
          return redirect($this->module_url_path);
        }

        if($contest_entry_id=="")
        {
            return redirect()->back();
        } 
        $obj_contest_entry_data = $this->ContestEntryModel->where('id',$contest_entry_id)
                                                          ->with('contest_details','contest_entry_files','contest_entry_files_rating','contest_entry_original_files')      
                                                          ->first();
        if($obj_contest_entry_data)
        {
            $arr_contest_entry_data = $obj_contest_entry_data->toArray();

            $contest_id = $arr_contest_entry_data['contest_id'];

            $winner_id = $this->ContestEntryModel->where('contest_id',$contest_id)
                                                 ->where('is_winner','YES')
                                                 ->select('expert_id')
                                                 ->first();
            if($winner_id)
            {
              $contest_winner_id = $winner_id['expert_id'];
            }
        }

        if(empty($arr_contest_entry_data))
        {
            Session::flash("error",trans('controller_translations.error_unauthorized_action'));
            return redirect()->back();
        }
        else
        {
            $user = \Sentinel::check();
            if(isset($user) && $user!=false){
                if($user->inRole('expert'))   
                {
                    $contest_entry_expert_id = isset($arr_contest_entry_data['expert_id'])?$arr_contest_entry_data['expert_id']:"";
                    /*if($user->id!=$contest_entry_expert_id)
                    {
                        Session::flash("error",trans('controller_translations.error_unauthorized_action'));
                        return redirect()->back();       
                    }*/
                }
                else if($user->inRole('client'))   
                { 
                    $contest_entry_client_id = isset($arr_contest_entry_data['client_user_id'])?$arr_contest_entry_data['client_user_id']:"";
                    if($user->id!=$contest_entry_client_id)
                    {
                        Session::flash("error",trans('controller_translations.error_unauthorized_action'));
                        return redirect()->back();       
                    }
                }
                else if($user->inRole('project_manager'))   
                {
                    Session::flash("error",trans('controller_translations.error_unauthorized_action'));
                    return redirect()->back(); 
                }

            }
        }


        $this->arr_view_data['contest_send_entry_public_file_path'] = $this->contest_send_entry_public_file_path;
        $this->arr_view_data['contest_send_entry_base_file_path']   = $this->contest_send_entry_base_file_path;
        $this->arr_view_data['arr_contest_entry_data']     = $arr_contest_entry_data;
        $this->arr_view_data['page_title']                 = trans('controller_translations.page_title_project_details');
        $this->arr_view_data['module_url_path']            = $this->module_url_path;
        $this->arr_view_data['contest_winner_id']          = $contest_winner_id;

        return view('front.contest_listing.contest_entry_details',$this->arr_view_data);    
    }
    
    private function expert_sealed_entry_details($user_id){
        
        $total_sealed_entries = $used_sealed_entries = $remaning_sealed_entries = 0;

        $obj_transactions = $this->SealedEntriesTransactionsModel
                                                    ->where('user_id',$user_id)
                                                    ->where('payment_status','1')
                                                    ->where('transaction_type','1')
                                                    ->get();
        if($obj_transactions!=FALSE)
        {
            foreach ($obj_transactions as $key => $value ) {
                if(isset($value->payment_status) && $value->payment_status == '1') {
                    // transaction_type 1 - credit 2 - debit
                    if(isset($value->transaction_type) && $value->transaction_type == '1') {
                        if(isset($value->sealed_entries_quantity)){
                            $total_sealed_entries = $total_sealed_entries + $value->sealed_entries_quantity;
                        }
                    }

                    if(isset($value->transaction_type) && $value->transaction_type == '2') {
                        if(isset($value->sealed_entries_quantity)){
                            $used_sealed_entries = $used_sealed_entries + $value->sealed_entries_quantity;
                        }
                    }
                }
            }            
        }

        if($total_sealed_entries > $used_sealed_entries) {
            $remaning_sealed_entries = $total_sealed_entries - $used_sealed_entries;
        }

        $arr_expert_sealed_entry_details['total_sealed_entries']    = $total_sealed_entries;
        $arr_expert_sealed_entry_details['used_sealed_entries']     = $used_sealed_entries;
        $arr_expert_sealed_entry_details['remaning_sealed_entries'] = $remaning_sealed_entries;
        return $arr_expert_sealed_entry_details;
    }
    
    public function get_seal_entry_payment_details(Request $request){
        
        $user_id = 0;
        $user = Sentinel::check();
        if( $user ) 
        {
          $user_id = $user->id;
        }

        $total_sealed_entries = $used_sealed_entries = $remaning_sealed_entries = 0;

        $obj_transactions = $this->SealedEntriesTransactionsModel
                                                    ->where('user_id',$user_id)
                                                    ->where('payment_status','1')
                                                    ->where('transaction_type','1')
                                                    ->get();
        if($obj_transactions!=FALSE)
        {
            foreach ($obj_transactions as $key => $value ) {
                if(isset($value->payment_status) && $value->payment_status == '1') {
                    // transaction_type 1 - credit 2 - debit
                    if(isset($value->transaction_type) && $value->transaction_type == '1') {
                        if(isset($value->sealed_entries_quantity)){
                            $total_sealed_entries = $total_sealed_entries + $value->sealed_entries_quantity;
                        }
                    }

                    if(isset($value->transaction_type) && $value->transaction_type == '2') {
                        if(isset($value->sealed_entries_quantity)){
                            $used_sealed_entries = $used_sealed_entries + $value->sealed_entries_quantity;
                        }
                    }
                }
            }            
        }

        if($total_sealed_entries > $used_sealed_entries) {
            $remaning_sealed_entries = $total_sealed_entries - $used_sealed_entries;
        }

        $seal_entry_html = '';
        $seal_entry_html .= '<div class="note-block">';
        $seal_entry_html .=    '<b> Credits available: '.$remaning_sealed_entries.'</b>';
        $seal_entry_html .= '</div>';
        $seal_entry_html .= '<div class="clearfix"></div>';
        $seal_entry_html .= '<a class="black-border-btn btn-right-mrg" href="#sealed_entry_topup" class="mp-add-btn-text add-money-but" data-keyboard="false" data-backdrop="static" data-toggle="modal">Top Up Credits</a>';

        if($remaning_sealed_entries>0){

            $seal_entry_html = '';
            $seal_entry_html .= '<div class="note-block">';
            $seal_entry_html .=    '<b> Credits available: '.$remaning_sealed_entries.'</b>';
            $seal_entry_html .= '</div>';
            $seal_entry_html .= '<div class="clearfix"></div>';
            $seal_entry_html .= '<i>( <span class="label label-important" style="background-color:red; margin-top:-15px;">Note!</span>Your <b class="seal_entry_price">1 Sealed entry</b> quantity will be used for sealing your listing ).</i>';
        }


        $responce_arr['status']                  = 'success';
        $responce_arr['remaning_sealed_entries'] = $remaning_sealed_entries;
        $responce_arr['seal_entry_html']         = $seal_entry_html;
        return json_encode($responce_arr);

        /*$responce_arr = [];
        $seal_entry_arr       = [];
        $seal_entry_price     = 0;
        $wallet_amount        = 0;
        $wallet_amount_with_currency ='USD 0';
        $get_seal_entry_price = $this->ContestPricingModel
                             ->where('status','!=','2')
                             ->where('slug','=','seal-entry')
                             ->first();

        if(sizeof($get_seal_entry_price)>0){
          $seal_entry_arr = $get_seal_entry_price->toArray();
        }     

        if(isset($seal_entry_arr['price']) && $seal_entry_arr['price'] !=""){
           $seal_entry_price=$seal_entry_arr['price'];
        }

        $wallet_details = get_logged_user_wallet_details();
        if($wallet_details == false){
           $wallet_amount  = 0;
        } else{
          $wallet_amount_with_currency = $wallet_details->Balance->Currency.' '.($wallet_details->Balance->Amount/100);
          $wallet_amount               = ($wallet_details->Balance->Amount/100);
        }

        $responce_arr['status']                         = 'success';
        $responce_arr['seal_entry_price']               = $seal_entry_price;
        $responce_arr['seal_entry_price_with_currency'] = config('app.project_currency.$').' '.$seal_entry_price;
        $responce_arr['wallet_amount']                  = $wallet_amount;
        $responce_arr['wallet_amount_with_currency']    = $wallet_amount_with_currency;
        return json_encode($responce_arr);*/
    }
} // end class