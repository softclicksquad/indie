<?php
namespace App\Http\Controllers\Front\Expert;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App;

use App\Models\ExpertsModel;
use App\Models\ExpertPortfolioModel;

use Validator;
use Session;
use Mail;
use sentinel;
use Image;

class PortfolioController extends Controller
{
  public function __construct(ExpertsModel $experts,
                              ExpertPortfolioModel $portfolio)
	{

        $this->arr_view_data             = [];
        $this->ExpertsModel              = $experts;
        $this->ExpertPortfolioModel      = $portfolio;
        $this->module_url_path           = url("portfolio");
        $this->portfolio_img_base_path   = base_path() . '/public'.config('app.project.img_path.portfolio_image');
        $this->portfolio_img_public_path = url('/').config('app.project.img_path.portfolio_image');
        $this->portfolio_resize_path     = 'uploads/front/portfolio/';

        if(! $user = Sentinel::check()) 
        {
          return redirect('/login')->send();
        }

        $this->user_id = $user->id;
	}

	/*
    | Comment : Expert Portfolio page
    | auther  : Ashwini K.
    */

    public function index()
    {  
        $arr_pagination = array();
        $arr_portfolio  = array();

        $obj_portfolio = $this->ExpertPortfolioModel->where('expert_user_id',$this->user_id)->paginate(11);
        if($obj_portfolio!=FALSE)
        {
           $arr_pagination = clone $obj_portfolio;
           $arr_portfolio  = $obj_portfolio->toArray();
        }

        $this->arr_view_data['page_title']                = trans('controller_translations.page_title_portfolio');
        $this->arr_view_data['arr_portfolio']             = $arr_portfolio;
        $this->arr_view_data['arr_pagination']            = $arr_pagination;
        $this->arr_view_data['module_url_path']           = $this->module_url_path;
        $this->arr_view_data['portfolio_img_public_path'] = $this->portfolio_img_public_path;
        $this->arr_view_data['portfolio_resize_path']     = $this->portfolio_resize_path;

        return view('expert.portfolio.index',$this->arr_view_data);
    }

    /*
    | Comment : Update Expert Portfolio 
    | auther  : Ashwini K.
    */

    public function update_portfolio(Request $request)
    {
          $arr_expert = array();
          $form_data  = array();
          $form_data  = $request->all();
          $validator = Validator::make($request->all(),$arr_expert);
          if($validator->fails())
         {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
         }
         //dd($form_data['portfolio_image']);
        

           if(isset($form_data['portfolio_image']) && $form_data['portfolio_image']!=FALSE)
          {    
              if ($request->hasFile('portfolio_image')) 
              {
                list($width, $height) = getimagesize($form_data['portfolio_image']);

                $img_valiator = Validator::make(array('image'=>$request->file('portfolio_image')),array(
                                                'image' => 'mimes:png,jpeg,jpg')); 

                  if ($request->file('portfolio_image')->isValid() && $img_valiator->passes())
                  {
                    if($height>=480 && $width>=720)
                    {

                      $portfolio_image_name = $form_data['portfolio_image'];

                      $fileExtension = strtolower($request->file('portfolio_image')->getClientOriginalExtension()); 

                      if($fileExtension == 'png' || $fileExtension == 'jpg' || $fileExtension == 'jpeg')
                      {
                          $portfolio_image_name = sha1(uniqid().$portfolio_image_name.uniqid()).'.'.$fileExtension;

                          if( $height>=720 && $width>=1080 )
                          { 
                            $path = $this->portfolio_img_base_path.$portfolio_image_name;
                            $test = Image::make($request->file('portfolio_image')->getRealPath())->resize(1080, 720)->save($path);
                          }
                          else
                          {
                            $request->file('portfolio_image')->move($this->portfolio_img_base_path, $portfolio_image_name);
                          }

                          $arr_expert['file_name'] = $portfolio_image_name;
                      }
                      else
                      {
                           Session::flash('error',trans('controller_translations.error_invalid_file_extension_for_portfolio_image'));
                           return back()->withInput();
                      }

                    }
                    else
                    {
                       Session::flash('error',trans('controller_translations.error_please_upload_image_of_size_atleast_720_width_and_480_height'));
                       return back()->withInput();
                    }
                  }
                  else
                  {
                       Session::flash("error",trans('controller_translations.error_please_upload_valid_image'));
                       return back()->withErrors($validator)->withInput($request->all());
                  }
                      
              }
          }

           $arr_expert['expert_user_id']  = $this->user_id;
           $status = $this->ExpertPortfolioModel->create($arr_expert);
            if ($status) 
            {
                Session::flash('success',trans('controller_translations.success_image_uploaded_successfully'));
            }
            else
            {
                Session::flash('error',trans('controller_translations.error_error_while_uploading_image'));
            }
            
        return redirect()->back();
    }  

    /*
    | Comment : Delete Expert Portfolio Images 
    | auther  : Ashwini K.
    */

     public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
          Session::flash('error',trans('controller_translations.error_problem_occured_while_image_deletion') );
          return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Session::flash('success',trans('controller_translations.success_image_deleted_successfully'));
        }
        else
        {
            Session::flash('error',trans('controller_translations.error_problem_occured_while_image_deletion'));
        }

        return redirect()->back();
    }

    public function perform_delete($id)
    {
        if ($id) 
        {
            $portfolio = $this->ExpertPortfolioModel->where('id',$id)->first();
            if($portfolio)
            { 
              $file_exits = file_exists($this->portfolio_img_base_path.$portfolio->file_name);

              if ($file_exits) 
              {
                 unlink($this->portfolio_img_base_path.$portfolio->file_name);
              }
                return $portfolio->delete();
            }
        }
        return FALSE;
    }
}
