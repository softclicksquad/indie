<?php
namespace App\Http\Controllers\Front\Expert;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\UserModel;
use App\Models\NotificationsModel;
use Sentinel;
use Session;
use App;
use Lang;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class NotificationController extends Controller
{
    public $arr_view_data;

    public function __construct()
    {
        if(!$user = Sentinel::check()){
            $this->user                               = [];
            return redirect('/login');
        }else{
            $this->user                               = $user;
        }

        $this->user_id                              = $user->id;
        $this->NotificationsModel                   = new NotificationsModel;
        $this->module_url_path                      = url("/expert/notifications");
        $this->login_url                            = url("/login");
        $this->arr_view_data                        = [];
        $this->view_folder_path                     = 'expert/notifications';
    }

    public function index(Request $request)
    {
        $arr_data       = [];
        $obj_data       = $this->NotificationsModel->where(['user_id'=>$this->user_id,'user_type'=>'3'])
                                                   ->orderBy('id','DESC')
                                                   ->paginate(10);

        if($obj_data)
        {
            $arr_data = $obj_data->toArray();   
        }
        $this->arr_view_data['arr_data']       = isset($arr_data)? $arr_data:[];
        $this->arr_view_data['obj_data']       = $obj_data;
        $this->arr_view_data['module_url_path']   = $this->module_url_path;
        $this->arr_view_data['login_url']     = $this->login_url;
        return view($this->view_folder_path.'.index',$this->arr_view_data);

    }



} // end class