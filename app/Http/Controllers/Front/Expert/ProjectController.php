<?php
namespace App\Http\Controllers\Front\Expert;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ProjectpostModel;
use App\Models\ProjectsBidsModel;
use App\Models\ProjectRequestsModel;
use App\Models\UserModel;
use App\Models\ReviewsModel;
use App\Models\NotificationsModel;
use App\Models\FavouriteProjectsModel;
use App\Models\ProjectAttchmentModel;
use App\Models\ProjectWalletsModel;
use App\Models\MilestonesModel;
use App\Models\MilestoneReleaseModel;
use App\Models\MilestoneRefundRequests;
use App\Models\UserWalletModel;
use App\Common\Services\MailService;
use App\Common\Services\WalletService;
use Validator;
use Session;
use Sentinel;
use Mail;
use Lang;
use Carbon\Carbon;
use App\Common\Services\PaymentService;
class ProjectController extends Controller
{
    public $arr_view_data;

    public function __construct(
                                ProjectpostModel $project_post,
                                ProjectsBidsModel $project_bids,
                                ProjectRequestsModel $project_requests,
                                UserModel $user,
                                ReviewsModel $review,
                                NotificationsModel $notifications,
                                FavouriteProjectsModel $favourite_projects,
                                ProjectAttchmentModel $project_attachment,
                                WalletService $WalletService,
                                ProjectWalletsModel $ProjectWalletsModel,
                                MailService $mail_service,
                                MilestonesModel $MilestonesModel,
                                MilestoneReleaseModel $MilestoneReleaseModel,
                                MilestoneRefundRequests $MilestoneRefundRequests
                                )
     { 
      if(! $user = Sentinel::check()){
        return redirect('/login');
      }
      if(!$user->inRole('expert')){
        return redirect()->back();
      }
      $this->user_id                                = $user->id;
      $this->ProjectpostModel                       = $project_post;
      $this->ProjectsBidsModel                      = $project_bids;
      $this->ReviewsModel                           = $review;
      $this->UserModel                              = $user;
      $this->NotificationsModel                     = $notifications;
      $this->FavouriteProjectsModel                 = $favourite_projects;
      $this->WalletService                          = $WalletService;
      $this->ProjectAttchmentModel                  = $project_attachment;
      $this->ProjectWalletsModel                    = $ProjectWalletsModel;
      $this->ProjectRequestsModel                   = $project_requests;
      $this->MilestonesModel                        = $MilestonesModel;
      $this->MilestoneReleaseModel                  = $MilestoneReleaseModel;
      $this->MilestoneRefundRequests                = $MilestoneRefundRequests;
      $this->MailService                            = $mail_service;
      $this->UserWalletModel                        = new UserWalletModel;
      $this->arr_view_data                          = [];
      $this->module_url_path                        = url("/expert/projects");
      $this->bid_attachment_base_path               = base_path() . '/public'.config('app.project.img_path.bid_attachment');
      $this->bid_attachment_public_path             = url('/public').config('app.project.img_path.bid_attachment');
      $this->project_attachment_base_path           = base_path() . '/public'.config('app.project.img_path.project_attachment');
      $this->project_attachment_public_path         = url('/').config('app.project.img_path.project_attachment');
      $this->expert_project_attachment_base_path    = base_path() . '/public'.config('app.project.img_path.expert_project_attachment');
      $this->expert_project_attachment_public_path  = url('/').config('app.project.img_path.expert_project_attachment');
    }
    /*
      Comment: Show List of appiled Projects. 
      Author : Nayan S.
    */

    public function show_applied_projects()
    {
      $arr_applied_projects = array();
      $arr_pagination       = array();

      $obj_applied_projects = $this->ProjectsBidsModel->with(['project_details.skill_details','project_details.category_details','project_details.project_skills.skill_data','project_details.sub_category_details'])
                                                      ->where('expert_user_id',$this->user_id)
                                                      ->whereHas('project_details',function ($query) 
                                                         { 
                                                           $query->where('project_status','=',2);
                                                           $query->where('is_hire_process','<>','YES');
                                                         })
                                                      ->orderBy('created_at','DESC')
                                                      ->paginate(config('app.project.pagi_cnt'));

      $this->arr_view_data['page_title'] = trans('controller_translations.page_title_manage_applied_projects');

      if($obj_applied_projects)
      {
        $arr_pagination       = clone $obj_applied_projects;
        $arr_applied_projects = $obj_applied_projects->toArray();
      }


      $this->arr_view_data['arr_applied_projects']  = $arr_applied_projects;
      $this->arr_view_data['arr_pagination']        = $arr_pagination;
      $this->arr_view_data['module_url_path']       = $this->module_url_path;
      return view('expert.projects.applied_projects',$this->arr_view_data);
    }  
    /*
     	Comment: load page of ongoing project 
    	Author :  Nayan Sonawane
    */
    public function show_ongoing_projects()
    {
      $arr_pagination       = array();
      $arr_ongoing_projects = array();
      $this->arr_view_data['page_title'] = trans('controller_translations.page_title_manage_ongoing_projects'); 
      $obj_ongoing_projects = $this->ProjectpostModel->where('expert_user_id',$this->user_id)
                                                     ->where('project_status','4')
                                                     ->with(['project_bid_info','skill_details','client_details','project_skills.skill_data','category_details','sub_category_details'])
                                                     ->with(['project_milestones' => function ($q) 
                                                        {
                                                          $q->where('status','!=',0);
                                                          $q->select('id','project_id','milestone_due_date');
                                                          $q->orderBy('milestone_due_date','DESC');
                                                        }])
                                                     ->orderBy('project_accepted_time','DESC')
                                                     ->paginate(config('app.project.pagi_cnt'));
      if($obj_ongoing_projects) {
        $arr_pagination       = clone $obj_ongoing_projects;
        $arr_ongoing_projects = $obj_ongoing_projects->toArray();
      }
      //dd($arr_ongoing_projects);
      $this->arr_view_data['arr_ongoing_projects']  = $arr_ongoing_projects;
      $this->arr_view_data['arr_pagination']        = $arr_pagination;
      $this->arr_view_data['module_url_path']       = $this->module_url_path;
      return view('expert.projects.ongoing_projects',$this->arr_view_data);
    }  

    /*
      Comment: load page of Completed projects
      Author :  Ashwini k
    */

    public function show_completed_projects()
    {
      $arr_completed_projects = [];
      $arr_pagination         = array();
      $this->arr_view_data['page_title'] = trans('controller_translations.page_title_manage_completed_projects'); 
      $obj_completed_projects            = $this->ProjectpostModel->where('expert_user_id',$this->user_id)
                                                       ->where('project_status','3')
                                                       ->with(['project_bid_info','skill_details','client_details','project_skills.skill_data','category_details','project_reviews','sub_category_details'])
                                                       ->orderBy('created_at','DESC')
                                                       ->paginate(config('app.project.pagi_cnt'));
      if($obj_completed_projects){
        $arr_pagination         =  clone $obj_completed_projects;
        $arr_completed_projects = $obj_completed_projects->toArray();
      }
      $this->arr_view_data['arr_completed_projects']  = $arr_completed_projects;
      $this->arr_view_data['arr_pagination']          = $arr_pagination;
      $this->arr_view_data['module_url_path']         = $this->module_url_path;
      return view('expert.projects.completed_projects',$this->arr_view_data);
    }
    /*
      Comment: load page of Canceled projects
      Author :  Ashwini k
    */
    public function show_canceled_projects()
    {
      $arr_canceled_projects = array();
      $arr_pagination        = array();
      $this->arr_view_data['page_title'] = trans('controller_translations.page_title_manage_canceled_projects');
      $obj_canceled_projects = $this->ProjectpostModel->where('expert_user_id',$this->user_id)
                                                      ->where('project_status','5')
                                                      ->with(['skill_details','client_details','project_skills.skill_data','category_details','sub_category_details'])
                                                      ->orderBy('created_at','DESC')
                                                      ->paginate(config('app.project.pagi_cnt'));
      if($obj_canceled_projects!=FALSE){
        $arr_pagination         = clone $obj_canceled_projects;
        $arr_canceled_projects  = $obj_canceled_projects->toArray();
      }
      $this->arr_view_data['arr_canceled_projects']   = $arr_canceled_projects;
      $this->arr_view_data['arr_pagination']          = $arr_pagination;
      $this->arr_view_data['module_url_path']         = $this->module_url_path;
      return view('expert.projects.canceled_projects',$this->arr_view_data);
    }
    /*
      Comment:  Show update project Details. 
      Author :  Nayan Sonawane
    */  
    public function show_update_project_bid($enc_id,$is_details_page = FALSE)
    { 
      $arr_bid_data = [];
      $this->arr_view_data['page_title']   = trans('controller_translations.page_title_update_project_bid'); 
      if($is_details_page == TRUE){
        $this->arr_view_data['page_title'] = trans('controller_translations.page_title_project_details'); 
      }
      $id = base64_decode($enc_id);
      if($id==""){
        return redirect()->back();
      }
      $obj_bid_data = $this->ProjectsBidsModel->where('expert_user_id',$this->user_id)
                                              ->where('project_id',$id)
                                              ->with('project_details.project_attachment','expert_details')
                                              ->first();
      if($obj_bid_data){
        $arr_bid_data = $obj_bid_data->toArray();

        $time = $this->ProjectRequestsModel->where('project_id',$arr_bid_data['project_id'])
                                           ->where('expert_user_id',$arr_bid_data['expert_user_id'])
                                           ->where('is_accepted','0')
                                           ->select('created_at')
                                           ->first();
        if($time)
        {
          $arr_time = $time->toArray();
          $this->arr_view_data['request_time'] = $arr_time['created_at'];
        }
      }              
      $this->arr_view_data['arr_project_info']                       = $this->get_project_details($id);   /* getting details of the project*/
      $this->arr_view_data['arr_bid_data']                           = $arr_bid_data;  
      $this->arr_view_data['module_url_path']                        = $this->module_url_path;
      $this->arr_view_data['bid_attachment_public_path']             = $this->bid_attachment_public_path; 
      $this->arr_view_data['project_attachment_public_path']         = $this->project_attachment_public_path;
      $this->arr_view_data['project_attachment_base_path']           = $this->project_attachment_base_path; 
      $this->arr_view_data['expert_project_attachment_public_path']  = $this->expert_project_attachment_public_path; 
      if($is_details_page == TRUE){
        return $this->arr_view_data;
      }
      
      return view('expert.projects.update_project_bid',$this->arr_view_data);
    }
    /*
      Comment:  Show project Details. 
      Author :  Nayan Sonawane
    */  
    public function show_project_details(Request $request,$enc_id)
    {
      /*unset PROJECT_ID FROm Session */
      if(Session::has('PROJECT_ID')){   
        $request->session()->forget('PROJECT_ID');
      }
      $proj_id             = base64_decode($enc_id);
      $arr_review_details  = [];
      $is_review_exists    = 0;
      $this->arr_view_data = $this->show_update_project_bid($enc_id , $is_details_page = TRUE);
      $obj_review_details  = $this->ReviewsModel->where('project_id',$proj_id)->orderBy('created_at', 'DESC')->get();
      if($obj_review_details){
        $arr_review_details = $obj_review_details->toArray();
      }
      $is_review_exists    =  $this->ReviewsModel->where('project_id',$proj_id)
                                                 ->where('from_user_id',$this->user_id)
                                                 ->count();
      $this->arr_view_data['arr_review_details'] = $arr_review_details;
      $this->arr_view_data['is_review_exists']   = $is_review_exists;


        /*-------------Milestone Section------------------------*/
          /*$expert_user_id = $this->user_id;
          $obj_milestone = $this->MilestoneReleaseModel->where('project_id',$proj_id)->get();
          $arr_milestone      = [];
          if($obj_milestone) 
          {
              $arr_milestone  = $obj_milestone->toArray();
          }
          
          $kyc_key = 'status';
          $kyc_val = '2';

          $arr_milestone = find_key_value_in_all_array($arr_milestone, $kyc_key, $kyc_val);*/
          $count_milestone = 0;
          $expert_user_id = $this->user_id;
          $count_milestone = $this->MilestonesModel->where('project_id',$proj_id)
                                                 ->where('is_milestone','<>','3')
                                                 ->count();
         
          $this->arr_view_data['count_milestone']            = $count_milestone;
          
        /*-------------End of Milesyone section*/


       /*----------Collaboration Section----------------------*/
          $project_id = $proj_id;
          $project_main_attachment        = [];
          $project_attachment             = [];
          $mp_wallet_id                   = '';
          $arr_collaboration_pagination                 = [];
          $obj_project_main_attachment    = $this->ProjectpostModel
                                                 ->where('id',$project_id)
                                                 //->where('project_status','4') // ongoing
                                                 //->orWhere('project_status','3') // completed
                                                 ->first(['id','project_name','project_status','project_attachment','project_currency_code']);

          if(isset($obj_project_main_attachment) && $obj_project_main_attachment != null){
            $project_main_attachment = $obj_project_main_attachment->toArray(); 
          }
          //dd($project_main_attachment);
          // project attachments 
          $obj_collaboration_project_attachment    = $this->ProjectAttchmentModel
                                           ->where('project_id',$project_id)
                                           ->orderBy('created_at','DESC') 
                                           ->paginate(12);  
          if($obj_collaboration_project_attachment) {
            $arr_collaboration_pagination          = clone $obj_collaboration_project_attachment;
            $project_attachment      = $obj_collaboration_project_attachment->toArray();
          }

          // end project attachments 
          $this->arr_view_data['project_main_attachment'] = $project_main_attachment;
          $this->arr_view_data['project_id']              = $project_id;
          $this->arr_view_data['project_attachment']      = $project_attachment;
          $this->arr_view_data['arr_collaboration_pagination']          = $arr_collaboration_pagination;

          /*----------End Of Collaboration Section----------------------*/

        /*----------Milestone section---------------------------------*/
          $this->arr_view_data['mp_job_wallet_id'] = '';
          $project_details  = $this->ProjectpostModel->where('id',$project_id)->first(['id','project_name','project_currency','project_currency_code','mp_job_wallet_id']);
          if ($project_details)
          {
              $this->arr_view_data['milestone_project_name']       = isset($project_details->project_name)?$project_details->project_name:'';
              $this->arr_view_data['milestone_project_currency']   = isset($project_details->project_currency)?$project_details->project_currency:'';
              $this->arr_view_data['mp_job_wallet_id']             = isset($project_details->mp_job_wallet_id)?$project_details->mp_job_wallet_id:'';

          }
          $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)
                                ->where('currency_code',$project_details->project_currency_code)
                                ->first();
          if($obj_data)
          {
              $mp_wallet_id = isset($obj_data->mp_wallet_id)?$obj_data->mp_wallet_id:'';
          }

          $obj_milestone = $this->MilestonesModel->where('project_id',$project_id)
                                                 ->with(['transaction_details','milestone_refund_details','project_details'=>function ($query) {
                                                      //$query->select('id','project_name','project_status'); 
                                                    }])
                                                 ->with(['milestone_release_details'=> function ($query_nxt) {
                                                      $query_nxt->select('id','milestone_id','status','project_id');    
                                                    }])
                                                 ->orderBy('milestone_due_date','ASC')
                                                 ->paginate(config('app.project.pagi_cnt'));
          $arr_milestone_data         = [];
          $arr_milestone_pagination        = [];
          if($obj_milestone)
          {
              $arr_milestone_pagination    = clone $obj_milestone; 
              $arr_milestone_data     = $obj_milestone->toArray();
          }
          // get wallet details
          $wallet_details =  $this->WalletService->get_wallet_details($this->arr_view_data['mp_job_wallet_id']);
          if(isset($wallet_details))
          {
            $wallet_details         = $wallet_details;
          }
          // end get wallet details
          $this->arr_view_data['enc_project_id']            = $enc_id;
          $this->arr_view_data['arr_milestone_data']        = $arr_milestone_data;
          $this->arr_view_data['arr_milestone_pagination']  = $arr_milestone_pagination;
          $this->arr_view_data['mangopay_wallet_details']   = $wallet_details;
        /*-------------------------------------------------------------*/
      return view('expert.projects.project_details',$this->arr_view_data);
    } 
    /*
    | Comments    : To get all project details.
    | auther      : Nayan S.
    */
    public function get_project_details($id){ 
      $obj_project_info = $this->ProjectpostModel
                               ->with('skill_details','category_details','project_skills.skill_data','client_details','client_info','project_bids_infos','project_bids_infos','sub_category_details','project_post_documents')
                               ->where('id',$id)
                               ->first();
      $arr_project_info = array();
      if($obj_project_info != FALSE){
        $arr_project_info = $obj_project_info->toArray();
      }
      return $arr_project_info;
    }
    /*
    | Comments    : Store project bid details.
    | auther      : Nayan S.
    */
    public function update_bid_details(Request $request)
    {
      $arr_rules                      = array();
      $arr_rules['bid_id']            = "required";
      $arr_rules['bid_cost']          = "required|numeric|min:1";
      $arr_rules['expected_duration'] = "required";
      $arr_rules['proposal_details']  = "required";
      
      $validator = Validator::make($request->all(),$arr_rules);
      if($validator->fails()){
          return redirect()->back()->withErrors($validator)->withInput();
      }
      $user = Sentinel::check();
      if(!$user->inRole('expert')){
         return redirect()->back()->withInput();
      }
      $form_data = [];
      $form_data = $request->all();
      if($form_data['bid_id'] == "" || (base64_decode($form_data['bid_id']) == "" ) )
      {
        return redirect()->back()->withInput($form_data);
      }

      $bid_id   =  base64_decode($form_data['bid_id']);

      $arr_bid_data = [];
      $arr_bid_data['bid_cost']               = $form_data['bid_cost'];
      $arr_bid_data['bid_estimated_duration'] = $form_data['expected_duration'];
      $arr_bid_data['bid_description']        = $form_data['proposal_details'];

      if($request->has('hour_per_week') && $request->has('hour_per_week')!='')
      {
        if($form_data['hour_per_week'] == 'HOUR_CUSTOM_RATE')
        {
          $arr_bid_data['hour_per_week'] = $form_data['hour_custom_rate'];
          $arr_bid_data['is_hour_custom_rate'] = '1';
        }
        else
        {
          $arr_bid_data['hour_per_week'] = $form_data['hour_per_week'];
          $arr_bid_data['is_hour_custom_rate'] = '0';
        }
      }

      // if(isset($form_data['bid_attachment']) && $form_data['bid_attachment']!=FALSE)
      // {    
      //   if ($request->hasFile('bid_attachment')) 
      //   {
      //     $bid_attachment_name = $form_data['bid_attachment'];
      //     $fileExtension       = strtolower($request->file('bid_attachment')->getClientOriginalExtension()); 
      //     $arr_file_types      = ['png','docx','xls','xlsx','gif','png','jpeg','jpg','cad','pdf','odt','doc','txt'];
      //     if(in_array($fileExtension, $arr_file_types))
      //     {
      //         $bid_attachment_name = sha1(uniqid().$bid_attachment_name.uniqid()).'.'.$fileExtension;
      //         $request->file('bid_attachment')->move($this->bid_attachment_base_path, $bid_attachment_name);
      //         $arr_bid_data['bid_attachment']  = $bid_attachment_name;
      //         $obj_bid_attachment = $this->ProjectsBidsModel->where('id',$bid_id)->first(['id','expert_user_id','project_id','bid_attachment']);

      //         if($obj_bid_attachment)
      //         {
      //           if(isset($obj_bid_attachment->bid_attachment) && $obj_bid_attachment->bid_attachment != "")
      //           {
      //             $unlink_path = $this->bid_attachment_base_path.'/'.$obj_bid_attachment->bid_attachment;
      //             @unlink($unlink_path);  
      //           }
      //         }
      //     }
      //     else
      //     {
      //          Session::flash('error',trans('controller_translations.error_invalid_file_extension_for_attachment'));
      //          return redirect()->back()->withInput(); 
      //     }
      //   }
      // }

      if($request->hasFile('contest_attachments'))
      {                 
        $documents  = $request->file('contest_attachments');

        foreach($documents as $key =>$doc)
        {           
          if(isset($doc))
          {            
            $filename         = $doc->getClientOriginalName();
            $file_extension   = strtolower($doc->getClientOriginalExtension());
            $file_size        = $doc->getClientSize();

            $arr_file_types = ['png','docx','xls','xlsx','gif','png','jpeg','jpg','cad','pdf','odt','doc','txt'];

            if(in_array($file_extension,$arr_file_types) && $file_size <= 5000000)
            {
                $file_name       = sha1(uniqid().$doc.uniqid()).'.'.$file_extension;
                $doc->move($this->bid_attachment_base_path,$file_name);
                $arr_bid_data['bid_attachment'] = $file_name;

                $obj_bid_attachment = $this->ProjectsBidsModel->where('id',$bid_id)->first(['id','expert_user_id','project_id','bid_attachment']);

                if($obj_bid_attachment)
                {
                  if(isset($obj_bid_attachment->bid_attachment) && $obj_bid_attachment->bid_attachment != "")
                  {
                    $unlink_path = $this->bid_attachment_base_path.'/'.$obj_bid_attachment->bid_attachment;
                    @unlink($unlink_path);  
                  }
                }
            }
            else
            {
                Session::flash('error',trans('controller_translations.error_invalid_file_extension_for_attachment') );
                return back()->withInput($form_data);
            }
          }          
        }
      }


      $bid_result = $this->ProjectsBidsModel->where('id',$bid_id)->update($arr_bid_data);
      if($bid_result){
          Session::flash('success',trans('controller_translations.success_project_bid_information_updated_successfully') );
          return redirect()->back()->withInput();
      } else {
          Session::flash('error',trans('controller_translations.error_error_while_updating_project_bid_information'));
          return redirect()->back()->withInput(); 
      }
    }
    /*
      Comment: Load page of Awarded projects 
      Author : Nayan Sonawane
    */
    public function show_awarded_projects()
    {
      $this->arr_view_data['page_title'] =  trans('controller_translations.page_title_manage_awarded_projects'); 
      $expert_user_id = $this->user_id;
      $obj_awarded = $this->ProjectRequestsModel->where('expert_user_id',$expert_user_id)
                                                ->where('is_accepted','0')
                                                ->with('project_details','project_details.skill_details','project_details.project_skills.skill_data','project_details.client_details','project_details.category_details','project_details.sub_category_details')
                                                ->with(['project_details.project_bid_info' => function ($q) use($expert_user_id) {
                                                    $q->where('expert_user_id','=' ,$expert_user_id);
                                                }])
                                                ->orderBy('created_at','DESC')
                                                ->paginate(config('app.project.pagi_cnt'));
      $arr_awarded_projects     = [];
      $arr_pagination           = array();
      if($obj_awarded){ 
        $arr_pagination         = clone $obj_awarded;
        $arr_awarded_projects   = $obj_awarded->toArray();
      }                       
      /*dd($arr_awarded_projects);*/
      $this->arr_view_data['arr_awarded_projects']  = $arr_awarded_projects;
      $this->arr_view_data['arr_pagination']        = $arr_pagination;
      $this->arr_view_data['module_url_path']       = $this->module_url_path;
      return view('expert.projects.awarded_projects',$this->arr_view_data);
    }
    /*
      Comment: View single Awarded project information
      Author : Nayan Sonawane
    */
    public function accept_awarded_project($enc_id)
    {
      $project_id  = base64_decode($enc_id);
      //dd($project_id);
      if($project_id=="")
      {
        return redirect()->back();
      } 
      $is_accepted         = 1;
      $expert_id           = $this->user_id;
      $is_project_accepted = $this->change_awarded_project_status($expert_id , $project_id ,$is_accepted );

      if($is_project_accepted == TRUE )
      {
          /*  Inserting experts user id in project table and updating the status to ongoing */
          $update_project_status = $this->ProjectpostModel->where('id',$project_id)->where('project_status','2')->update(['project_status'=>'4',
                                                                                             'expert_user_id'=>$expert_id,
                                                                                             'is_awarded'=>'1',
                                                                                             'project_accepted_time'=>new Carbon ]); 
          if($update_project_status)
          {
              $project_name     = config('app.project.name');
              //$mail_form        = get_site_email_address();   /* getting email address of admin from helper functions */
              $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';

              $obj_project_info = $this->ProjectpostModel->where('id',$project_id)
                                                          ->with(['client_info'=> function ($query) {
                                                                $query->select('user_id','first_name','last_name','company_name','address');
                                                            }])
                                                          ->with(['client_details'=> function ($query) {
                                                                $query->select('email','id');
                                                            }])
                                                          ->first(['id','project_name','project_description','client_user_id','project_currency_code']);
              $data = [];
              if($obj_project_info)
              {
                $arr_project_info  = $obj_project_info->toArray();
                // get client wallet details 
                $project_currency_code = isset($arr_project_info['project_currency_code'])?$arr_project_info['project_currency_code']:'USD'; 

                // $client_mp_details = $this->UserModel->where('id',$arr_project_info['client_user_id'])->first(['id','mp_user_id']);
                $mp_details = get_user_wallet_details($arr_project_info['client_user_id'],$project_currency_code);

                // $this->UserWalletModel->where('user_id',$arr_project_info['client_user_id'])
                //                                            ->where('currency_code',$project_currency_code)
                //                                            ->first();

                if($mp_details)
                {
                  //$mp_details      = $client_mp_details->toArray();

                  if(isset($mp_details['mp_user_id']) && $mp_details['mp_user_id'] !="")
                  {
                    $project_name  = $arr_project_info['id'].'-'.$arr_project_info['project_name']; 
                    // create a project wallet against client mp account
                    $project_currency_code = isset($arr_project_info['project_currency_code'])?$arr_project_info['project_currency_code']:'USD'; 
                    $AppWallet     = $this->WalletService->create_wallet_currency_wise($mp_details['mp_user_id'],$project_name.' job wallet',$project_currency_code);
                    if($AppWallet)
                    {
                      $arr_project_data                           = [];
                      $arr_project_data['mp_job_wallet_id']       = isset($AppWallet->Id)?$AppWallet->Id:"";
                      $arr_project_wallet                         = [];
                      $arr_project_wallet['project_id']           = $project_id;
                      $arr_project_wallet['client_user_id']       = $arr_project_info['client_user_id'];
                      $arr_project_wallet['expert_user_id']       = $expert_id;
                      $arr_project_wallet['project_mp_wallet_id'] = $arr_project_data['mp_job_wallet_id']; 
                      try{
                            $store_mp_wallet_id = $this->ProjectpostModel->where('id',$project_id)->update($arr_project_data); 

                            $this->ProjectWalletsModel->insertGetId($arr_project_wallet);

                          }catch(\Exception $e){
                            $update_project_status = $this->ProjectpostModel->where('id',$project_id)->where('project_status','4')->update(['project_status'=>'2',
                                                                                                 'expert_user_id'=>$expert_id,
                                                                                                 'is_awarded'=>'0',
                                                                                                 'project_accepted_time'=>'0000-00-00 00:00:00' ]);  


                            /* In case of failed to update project table then updating request table. by doing this expert will be able to again see the project request */ 
                            $this->ProjectRequestsModel->where('expert_user_id',$expert_id)
                                           ->where('project_id',$project_id)
                                           ->update(['is_accepted'=>'0']);
                            Session::flash('error',trans('controller_translations.error_error_while_accepting_project'));
                            return redirect()->back();
                          }
                    }

                  } 
                  else
                  {
                    $update_project_status = $this->ProjectpostModel->where('id',$project_id)->where('project_status','4')->update(['project_status'=>'2',
                                                                                             'expert_user_id'=>$expert_id,
                                                                                             'is_awarded'=>'0',
                                                                                             'project_accepted_time'=>'0000-00-00 00:00:00' ]);  

                    /* In case of failed to update project table then updating request table. by doing this expert will be able to again see the project request */
                    $this->ProjectRequestsModel->where('expert_user_id',$expert_id)
                                               ->where('project_id',$project_id)
                                               ->update(['is_accepted'=>'0']);
                    Session::flash('error',trans('common/wallet/text.text_client_not_created_wallet_yet'));
                    return redirect()->back();
                  }
                }
                else
                {
                 
                  $update_project_status = $this->ProjectpostModel->where('id',$project_id)->where('project_status','4')->update(['project_status'=>'2',
                                                                                             'expert_user_id'=>$expert_id,
                                                                                             'is_awarded'=>'0',
                                                                                             'project_accepted_time'=>'0000-00-00 00:00:00' ]);  
                  /* In case of failed to update project table then updating request table. by doing this expert will be able to again see the project request */
                  $this->ProjectRequestsModel->where('expert_user_id',$expert_id)
                                             ->where('project_id',$project_id)
                                             ->update(['is_accepted'=>'0']);
                  Session::flash('error',trans('controller_translations.error_error_while_accepting_project') );
                  return redirect()->back();
                }
                // end get client wallet details 

                /* get client name */
                $client_first_name = isset($arr_project_info['client_info']['first_name'])?$arr_project_info['client_info']['first_name']:'';
                $client_last_name  = isset($arr_project_info['client_info']['last_name'])?$arr_project_info['client_info']['last_name']:'';

                /*$client_name = $client_first_name.' '.$client_last_name;*/
                $client_name       = $client_first_name;
                /* get Expert name */
                $obj_user          =  $this->UserModel->where('id',$expert_id)
                                           ->with(['expert_details'=>function ($query) {
                                                  $query->select('id','user_id','first_name','last_name');
                                            }])
                                           ->first(['id','email','user_name']);   
                $arr_user   = [];                                            
                if($obj_user){
                  $arr_user = $obj_user->toArray();
                }                                            
                $expert_first_name           = isset($arr_user['expert_details']['first_name'])?$arr_user['expert_details']['first_name']:'';
                $expert_last_name            = isset($arr_user['expert_details']['last_name'])?$arr_user['expert_details']['last_name']:'';
                $expert_name                 = $expert_first_name.' '.$expert_last_name;
                $expert_username             = isset($arr_user['user_name'])?$arr_user['user_name']:'';
                $data['expert_username']     = $expert_username;
                $data['project_name']        = isset($arr_project_info['project_name'])?$arr_project_info['project_name']:'';
                $data['project_description'] = isset($arr_project_info['project_description'])?$arr_project_info['project_description']:'';
                $data['client_name']         = $client_name;
                $data['expert_name']         = $expert_name;
                $data['login_url']           = url('/redirection?redirect=PROJECT&id='.base64_encode($project_id));
                $data['email_id']            = isset($arr_project_info['client_details']['email'])? 
                                                     $arr_project_info['client_details']['email']:'';
              }
              $email_to = isset($arr_project_info['client_details']['email'])? $arr_project_info['client_details']['email']:'';
              if($email_to!= ""){
                try{
                      $mail_status = $this->MailService->send_project_accepted_by_expert_mail_to_client_email($data);
                  // Mail::send('front.email.project_accepted_by_expert_mail_to_client', $data, function ($message) use ($email_to,$mail_form,$project_name) {
                  //     $message->from($mail_form, $project_name);
                  //     $message->subject($project_name.': Project Award Accepted');
                  //     $message->to($email_to);
                  // });
                }
                catch(\Exception $e){
                Session::Flash('error'   , trans('controller_translations.text_mail_not_sent'));
                }
              } 
              
              $posted_project_name = isset($arr_project_info['project_name'])?$arr_project_info['project_name']:'';

              /* Create Notification for client */      
              $arr_data                         = [];
              $arr_data['user_id']              = $arr_project_info['client_user_id'];
              $arr_data['user_type']            = '2';
              $arr_data['url']                  = 'client/projects/details/'.base64_encode($project_id);
              $arr_data['project_id']           = $project_id;
              /*$arr_data['notification_text']  = 'Project Accepted By Expert';*/
              $arr_data['notification_text_en'] = $posted_project_name . ' - ' . Lang::get('controller_translations.notification_project_accepted_by_expert',[],'en','en');
              $arr_data['notification_text_de'] = $posted_project_name . ' - ' . Lang::get('controller_translations.notification_project_accepted_by_expert',[],'de','en');
              $this->NotificationsModel->create($arr_data);  
              /* Notificatin ends */
              Session::flash('success',trans('controller_translations.success_project_accepted_successfully') );
              return redirect()->back();
          }
          else
          {
            /* In case of failed to update project table then updating request table. by doing this expert will be able to again see the project request */
            $this->ProjectRequestsModel->where('expert_user_id',$expert_id)
                                       ->where('project_id',$project_id)
                                       ->update(['is_accepted'=>'0']);
            Session::flash('error',trans('controller_translations.error_error_while_accepting_project') );
            return redirect()->back();
          }
      } else {
        Session::flash('error',trans('controller_translations.error_error_while_accepting_project'));
        return redirect()->back();
      }
    }
    /*
      Comment: Reject Awarded project information
      Author : Nayan Sonawane
    */
    public function reject_awarded_project(Request $request)
    {      
      $enc_id = $request->input('project_id');
      $rejection_reason = $request->input('rejection_reason');
      $project_id = base64_decode($enc_id);

      if($project_id=="" && $enc_id == ""){
        return redirect()->back();
      }
      $expert_id = $this->user_id;

      /* save the details for project rejection */
      $store_reason = $this->ProjectsBidsModel->where('project_id',$project_id)
                                              ->where('expert_user_id',$expert_id)
                                              ->update(['rejection_reason'=>$rejection_reason]);

      if( $store_reason == FALSE ){
        Session::flash('error', trans('controller_translations.error_while_submitting_rejection_reason'));
        return redirect()->back();
      }
      $is_accepted         = 2;
      $expert_id           = $this->user_id;
      $is_project_accepted = $this->change_awarded_project_status($expert_id , $project_id ,$is_accepted );

      if($is_project_accepted == TRUE ) 
      {
          $project_name     = config('app.project.name');
          //$mail_form        = get_site_email_address();   /* getting email address of admin from helper functions */
          $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
          
          $obj_project_info = $this->ProjectpostModel->where('id',$project_id)
                                                     ->with(['client_info'=> function ($query) {
                                                            $query->select('user_id','first_name','last_name','company_name','address');
                                                        }])
                                                     ->with(['client_details'=> function ($query) {
                                                            $query->select('email','id');
                                                        }])
                                                     ->first(['id','project_name','project_description','client_user_id']);

          $data   = [];
          if($obj_project_info){
            $arr_project_info  = $obj_project_info->toArray();
            /* get client name */
            $client_first_name = isset($arr_project_info['client_info']['first_name'])?$arr_project_info['client_info']['first_name']:'';
            $client_last_name  = isset($arr_project_info['client_info']['last_name'])?$arr_project_info['client_info']['last_name']:'';
            /*$client_name = $client_first_name.' '.$client_last_name;*/
            $client_name = $client_first_name;
            /* get Expert name */
            $obj_user    = $this->UserModel->where('id',$expert_id)
                                           ->with(['expert_details'=>function ($query) {
                                              $query->select('id','user_id','first_name','last_name');
                                             }])
                                           ->first(['id','email','user_name']);   
            $arr_user   = [];                                            
            if($obj_user){
              $arr_user = $obj_user->toArray();
            }                                            
            $expert_first_name           = isset($arr_user['expert_details']['first_name'])?$arr_user['expert_details']['first_name']:'';
            $expert_last_name            = isset($arr_user['expert_details']['last_name'])?$arr_user['expert_details']['last_name']:'';
            $expert_name                 = $expert_first_name.' '.$expert_last_name;
            $expert_username             = isset($arr_user['user_name'])?$arr_user['user_name']:'';
            $data['expert_username']     = $expert_username;
            $data['project_name']        = isset($arr_project_info['project_name'])?$arr_project_info['project_name']:'';
            $data['project_description'] = isset($arr_project_info['project_description'])?$arr_project_info['project_description']:'';
            $data['client_name']         = $client_name;
            $data['expert_name']         = $expert_name;
            $data['login_url']           = url('/redirection?redirect=PROJECT&id='.base64_encode($project_id));
          }
          $email_to = isset($arr_project_info['client_details']['email'])? $arr_project_info['client_details']['email']:'';
          if($email_to!= ""){
            try{
              Mail::send('front.email.project_rejected_by_expert_mail_to_client', $data, function ($message) use ($email_to,$mail_form,$project_name) {
                  $message->from($mail_form, $project_name);
                  $message->subject($project_name.': Project Award Rejected');
                  $message->to($email_to);
              });
            } catch(\Exception $e){
            Session::Flash('error'   , trans('controller_translations.text_mail_not_sent'));
            }
          } 

          $posted_project_name = isset($arr_project_info['project_name'])?$arr_project_info['project_name']:'';
          /* Create Notification for client */      
            $arr_data =  [];
            $arr_data['user_id']    = $arr_project_info['client_user_id'];
            $arr_data['user_type']  = '2';
            $arr_data['url']        = 'client/projects/details/'.$enc_id;
            $arr_data['project_id'] = $project_id;
            /*$arr_data['notification_text'] = trans('controller_translations.msg_project_rejected_by_expert');*/
            $arr_data['notification_text_en'] =  $posted_project_name . ' - ' . Lang::get('controller_translations.msg_project_rejected_by_expert',[],'en','en');
            $arr_data['notification_text_de'] =  $posted_project_name . ' - ' . Lang::get('controller_translations.msg_project_rejected_by_expert',[],'de','en');
            $this->NotificationsModel->create($arr_data);  
          /* Notificatin ends */
          Session::flash('success',trans('controller_translations.success_project_rejected_successfully'));
          return redirect()->back();
      } else {
          Session::flash('error',trans('controller_translations.error_error_while_rejecting_project') );
          return redirect()->back();
      }
    }    
    /*
      Auther : Nayan S.
    */
    public function change_awarded_project_status($expert_id , $project_id ,$is_accepted )
    {      
      $result = $this->ProjectRequestsModel->where('expert_user_id',$expert_id)
                                           ->where('project_id',$project_id)
                                           ->update(['is_accepted'=>$is_accepted]);

      /* Changing bid status if its accepted or rejected in the bids table */
      if($is_accepted == '1')
      {                                           
        $this->ProjectsBidsModel->where('project_id',$project_id)
                                ->where('expert_user_id',$expert_id)
                                ->update(['bid_status'=>'1']);

        $this->ProjectsBidsModel->where('project_id',$project_id)
                                ->where('expert_user_id','<>',$expert_id)
                                ->update(['bid_status'=>'2']);                              
      } 
      /* Confirming that bid is rejected by expert */
      if($is_accepted == '2')
      {
          $this->ProjectsBidsModel->where('project_id',$project_id)
                                  ->where('expert_user_id','=',$expert_id)
                                  ->update(['bid_status'=>'3']); 
      }

      if($result){
          return TRUE;
      }else{
          return FALSE;
      }
    }
    /* 
      Comments : Delete attachment from database and from folder.
      Auther   : Bharat K.
    */
    public function delete_attachment(Request $request){
        $arr_expert = $json = [];
        $bid_id     = $request->input('bid_id');
        $obj_expert = $this->ProjectsBidsModel->where('id',base64_decode($bid_id))->where('expert_user_id',$this->user_id)->first(['id','expert_user_id','bid_attachment']);
        if($obj_expert){
          $arr_expert = $obj_expert->toArray();  
          $bid_attachment = isset($arr_expert['bid_attachment'])?trim($arr_expert['bid_attachment']):'';
          if($bid_attachment != ""){
            $result = $this->ProjectsBidsModel->where('id',base64_decode($bid_id))->where('expert_user_id',$this->user_id)->update(['bid_attachment'=>'']);
            if($result){
              $json['status'] = 'SUCCESS_BID_ATTACHMENT';
              $json['msg']  = trans('controller_translations.msg_bid_attachment_deleted_successfully');
              @unlink($this->bid_attachment_base_path.$bid_attachment);
            }
          }
        }
      return response()->json($json);
     }
    /*
      Auther : Bharat K.
    */
    public function notifications_seen($id){
      if($id && $id!=""){
        $notification_id =  base64_decode($id);
        $status = $this->NotificationsModel->where('id',$notification_id)->update(['is_seen'=>'1']);
        if($status){
          $data['status'] = "success";
          echo json_encode($data);
        } else {
          $data['status'] = "error";
          echo json_encode($data);
        }
      }  
    } 
    /*
      Comment: Load page of Awarded projects 
      Author : Nayan Sonawane
    */
    public function show_favourite_projects() {
      $this->arr_view_data['page_title'] =  trans('controller_translations.page_title_favourite_projects'); 
      $obj_favourite_projects            =  $this->FavouriteProjectsModel->where('expert_user_id',$this->user_id)
                                                 ->with('project_details','project_details.skill_details','project_details.project_skills.skill_data','project_details.client_details')
                                                 ->orderBy('created_at','DESC')
                                                 ->paginate(config('app.project.pagi_cnt'));
      $arr_favourite_projects = [];
      $arr_pagination         = array();
      if($obj_favourite_projects) { 
        $arr_pagination           = clone $obj_favourite_projects;
        $arr_favourite_projects   = $obj_favourite_projects->toArray();
      }                                                 
      $this->arr_view_data['arr_favourite_projects']  = $arr_favourite_projects;
      $this->arr_view_data['arr_pagination']          = $arr_pagination;
      $this->arr_view_data['module_url_path']         = $this->module_url_path;
      return view('expert.projects.favourite_projects',$this->arr_view_data);
    }
    /*
    Commenst : function to add project attachments 
    Auther   : Nayan S.
    */
    public function add_project_attachment(Request $request){
      $form_data  = [];
      $form_data  = $request->all();
      $project_id = "";
      if(isset($form_data['project_id_attachment']) && $form_data['project_id_attachment'] != ""){
        $project_id = base64_decode($form_data['project_id_attachment']);       
      }
      else
      {
        Session::flash('error',trans('controller_translations.invalid_details_for_adding_project_attchment'));  
        return redirect()->back();
      }
      if(isset($form_data['project_attachment']) && $form_data['project_attachment']!=FALSE)
      {    
        if ($request->hasFile('project_attachment')) 
        {
            $count_project_attachments = 0;
            $count_project_attachments = $this->ProjectAttchmentModel->where('project_id',$project_id)->count();
            if($count_project_attachments >= 25)
            {
                Session::flash('error', trans('controller_translations.you_can_only_add'));
                return redirect()->back()->withInput(); 
            }
            $project_attachment_name = $form_data['project_attachment'];
            $file_original_name = $request->file('project_attachment')->getClientOriginalName(); 
            $arr_original_name = explode('.',$file_original_name);
            // array after removing last elemet of an array.
            $last_key = count($arr_original_name) - 1;
            unset($arr_original_name[$last_key]);
            $file_name_without_ext = implode('_',$arr_original_name);
            $fileExtension      = strtolower($request->file('project_attachment')->getClientOriginalExtension()); 
            $arr_file_types = ['png','docx','xls','xlsx','gif','png','jpeg','jpg','cad','pdf','odt','doc','txt'];
            if(in_array($fileExtension, $arr_file_types))
            { 
              $project_attachment_name = $file_name_without_ext.'_'.str_random(6).'.'.$fileExtension;
              $upload_success =  $request->file('project_attachment')->move($this->expert_project_attachment_base_path, $project_attachment_name);
                  if($upload_success)
                  {
                      // create unique indentifier
                      $res  = \DB::table('project_attachment')->where('project_id',$project_id)->select('attchment_no')->where('attchment_no' ,'!=' , 0)->where('attchment_no' ,'!=' , '')->orderBy('id' , 'DESC')->LIMIT('1')->get();
                      $res = $res;
                      if(count($res) > 0){                 
                          $max_entry_id = $res[0]->attchment_no;
                          $max_entry_id = $max_entry_id + 1;                        
                      } else {
                          $max_entry_id = '1';  
                      }
                      // create unique indentifier
                      $result = $this->ProjectAttchmentModel->create([
                                                        'attchment_no'    => $max_entry_id,
                                                        'expert_user_id'  => $this->user_id,
                                                        'project_id'      => $project_id,
                                                        'attachment'      => $project_attachment_name
                                                      ]);
                      if($result) {
                        Session::flash('success', trans('controller_translations.project_attchment_added_successfully'));
                      } else {
                        Session::flash('error', trans('controller_translations.error_while_adding_project_attchment_12'));      
                      }
                  } else {
                    Session::flash('error', trans('controller_translations.error_while_adding_project_attchment')); 
                  }
              }  else {
                Session::flash('error',trans('controller_translations.error_invalid_file_extension_for_attachment'));
              }
        }
      }
      else
      {
        Session::flash('error', trans('controller_translations.please_select_a_file_to_upload'));
      }
      return redirect()->back()->withInput(); 
    }
    public function delete_project_attachment(Request $request)
    {
      $arr_expert = $json = [];
      $id         = base64_decode($request->input('id'));

      $obj_expert = $this->ProjectAttchmentModel->where('id',$id)->where('expert_user_id',$this->user_id)->first(['id','expert_user_id','attachment']);
      if($obj_expert)
      {
        $arr_expert         = $obj_expert->toArray();  
        $project_attachment = isset($arr_expert['attachment'])?trim($arr_expert['attachment']):'';
    
        if($project_attachment != "")
        {
          $result = $this->ProjectAttchmentModel->where('id',$id)->delete();
          
          if($result)
          {
            $json['status']     = 'SUCCESS_PROJECT_ATTACHMENT';
            Session::flash('success', trans('controller_translations.project_attachment_deleted_successfully'));
            @unlink($this->expert_project_attachment_base_path.'/'.$project_attachment);
          }
          else
          {
            Session::flash('error', trans('controller_translations.error_while_deleting_project_attachment'));
          }
        }
      }
        return response()->json($json);
    }
    /*
      Comments : Send project completion request to client.
      Auther   : Nayan S.
    */
    public function request_to_complete($enc_id)
    {
      $project_id = base64_decode($enc_id);
      if($project_id == "")
      {
        Session::flash('error', trans('controller_translations.invalid_project_details_available'));  
        return redirect()->back();
      }
      $auth_expert_id = Sentinel::getUser()->id;
      if(!isset($auth_expert_id) && $auth_expert_id == '')
      {
        Session::flash('error', trans('controller_translations.text_sorry_somthing_goes_wrong_please_try_again'));  
        return redirect()->back(); 
      }

      $obj_is_project_cancled            = $this->ProjectpostModel
                                              ->where('id',$project_id)        
                                              ->where('project_status','5')
                                              ->where('expert_user_id',$this->user_id)
                                              ->first();
      $is_project_cancled                = isset($obj_is_project_cancled)? $obj_is_project_cancled :'';
      if($is_project_cancled && $is_project_cancled!='')
      {
        Session::flash('error', trans('controller_translations.text_sorry_This_project_is_canceled'));  
        return redirect()->back();
      }

      $is_all_milestone_released_cnt = $this->MilestonesModel
                                              ->where('expert_user_id',$auth_expert_id)
                                              ->where('project_id',$project_id)
                                              ->count();

      $milestone_release_cnt         = $this->MilestoneReleaseModel
                                              ->where('expert_user_id',$auth_expert_id)
                                              ->where('project_id',$project_id)
                                              ->where('status','2')
                                              ->count();

      $milestone_request_cnt         = $this->MilestoneRefundRequests
                                            ->where('project_id',$project_id)
                                            ->count();

      $total_release_and_request_cnt = $milestone_release_cnt + $milestone_request_cnt;                                                                        

      $total_milestone_count = isset($is_all_milestone_released_cnt)?$is_all_milestone_released_cnt:'';
      $milestone_release_cnt = isset($milestone_release_cnt)?$milestone_release_cnt:'';
      
      if($total_release_and_request_cnt !='' && $total_milestone_count !='' && $total_release_and_request_cnt != $total_milestone_count)
      {
        Session::flash('error',trans('controller_translations.text_your_milestones_not_released_please_send_request_to_your_client_for_releasing_milestones'));  
        return redirect()->back();
      }

      $obj_project_info = $this->ProjectpostModel->where('id','=',$project_id)->first();
      if($obj_project_info && isset($obj_project_info->expert_user_id) && ( $this->user_id == $obj_project_info->expert_user_id ) )
      {
        $res_project = $obj_project_info->update(['has_completion_request' => 1]);
        if($res_project)
        {
          Session::flash('success', trans('controller_translations.project_completion_request_sent_successfully_to_client'));  
          /* Create Notification for client */      
          $arr_data =  [];
          if(isset($obj_project_info->client_user_id) && $obj_project_info->client_user_id != "")
          {
            $posted_project_name = isset($obj_project_info->project_name)?$obj_project_info->project_name:'';

            $arr_data['user_id']    = $obj_project_info->client_user_id;
            $arr_data['user_type']  = '2';
            $arr_data['url']        = 'client/projects/details/'.base64_encode($project_id);
            $arr_data['project_id'] = $project_id;
            /*$arr_data['notification_text'] = trans('controller_translations.new_project_completion_request');*/
            $arr_data['notification_text_en'] =  $posted_project_name . ' - ' . Lang::get('controller_translations.new_project_completion_request',[],'en','en');
            $arr_data['notification_text_de'] =  $posted_project_name . ' - ' . Lang::get('controller_translations.new_project_completion_request',[],'de','en');
            $this->NotificationsModel->create($arr_data);  
          }
          /* Notificatin ends */
          /* Send mail to client */
          $this->MailService->project_completion_request($project_id);
        }
        else
        {
          Session::flash('error', trans('controller_translations.error_while_updating_project_completion_request'));  
        }
      }
      else
      {
        Session::flash('error', trans('controller_translations.invalid_action_for_project_completion'));  
      }
      return redirect()->back();
    }
} // end class
