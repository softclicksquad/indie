<?php

namespace App\Http\Controllers\Front\Expert;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App;

use App\Models\TransactionsModel;
use App\Common\Services\ProjectSearchService;

use Validator;
use Session;
use Mail;
use sentinel;

class TransactionsController extends Controller
{
  public function __construct(TransactionsModel $transaction)
	{
		    $this->arr_view_data      = [];
        $this->TransactionsModel  = $transaction;
        $this->module_url_path = url("transactions");

        if(! $user = Sentinel::check()) 
        {
          return redirect('/login');
        }

        $this->user_id = $user->id;
	}

	/*
    | Comment : Transaction listing of expert 
    | auther  : Ashwini K.
    */

    public function index()
    {   
        $arr_transactions = array();
        $arr_pagination   = array();

        if($this->user_id) 
        { 
           if(Session::has('show_case_images_cnt')) { $pagi_cnt = Session::get('show_case_images_cnt'); }else { $pagi_cnt = config('app.project.pagi_cnt'); }
           $obj_transactions = $this->TransactionsModel->where('user_id',$this->user_id)->with(['user_details'])->paginate($pagi_cnt);
          
           if($obj_transactions!=FALSE)
           {
                $arr_pagination   =clone $obj_transactions;
                $arr_transactions =$obj_transactions->toArray();
           }
        }

        $this->arr_view_data['page_title']            = trans('controller_translations.page_title_transactions');
        $this->arr_view_data['arr_transactions']      = $arr_transactions;
        $this->arr_view_data['arr_pagination']        = $arr_pagination;
        $this->arr_view_data['module_url_path']       = $this->module_url_path;


        return view('expert.transactions.index',$this->arr_view_data);
    }

    /*
    | Comment : Transaction details of expert 
    | auther  : Ashwini K.
    */
    public function show($id="")
    {
      $transaction = array();
      
       if($id!=FALSE && $id!="")
       { 
          $id = base64_decode($id);
          $obj_transaction_info = $this->TransactionsModel->with('user_details')->where('id',$id)->first();
          if($obj_transaction_info)
          {
             $transaction = $obj_transaction_info->toArray();
          }
       } 

      $this->arr_view_data['page_title'] = trans('controller_translations.page_title_transaction_details');
      $this->arr_view_data['transaction'] = $transaction;
      $this->arr_view_data['module_url_path'] = $this->module_url_path;
      return view('expert.transactions.show',$this->arr_view_data);
    }

    public function show_cnt(Request $Request) 
    {
        \Session::put('show_transaction_cnt' , $Request->input('show_cnt'));
        $preurl = url()->previous();
        $explode_url = explode('?page=',$preurl);
        if(empty($explode_url[1])){
            return redirect()->back();
        }
        else{
            $redirect =  $explode_url[0].'?page='.'1';
            return redirect($redirect);
        }
    }

}
