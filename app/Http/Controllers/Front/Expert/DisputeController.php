<?php

namespace App\Http\Controllers\Front\Expert;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\DisputeModel;
use App\Models\ProjectpostModel;
use App\Models\NotificationsModel;
use App\Models\StaticPageModel;
use App\Common\Services\MailService;
use Sentinel;
use Validator;
use Session;
use Mail;
use Lang;

class DisputeController extends Controller
{
    public function __construct(  
                  								 DisputeModel $dispute,
                  								 ProjectpostModel $projectpost,
                                   NotificationsModel $notifications,
                                   StaticPageModel $static_pagemodel,
                                   MailService $MailService
                               )
    {
      $this->arr_view_data = [];

      if(! $user = Sentinel::check()) 
      {
        return redirect('/login');
      }

      $this->user_id = $user->id;

      $this->DisputeModel       = $dispute;
      $this->ProjectpostModel   = $projectpost;
      $this->NotificationsModel = $notifications;
      $this->StaticPageModel    = $static_pagemodel;
      $this->MailService        = $MailService;
      
      $this->arr_view_data 		= [];
      $this->view_folder_path   = '/expert/projects';
      $this->module_url_path    = url("/expert/dispute");
    }

     /*
    Comments : Show Projects page.
    Auther   : Nayan S.     
    */    

    public function show_projects()
    {
      $this->arr_view_data['page_title'] = trans('controller_translations.page_title_manage_dispute');

      $obj_dispute_projects = $this->ProjectpostModel->where('expert_user_id',$this->user_id)
                                                     ->where('project_status','=','4')
                                                     ->with(['skill_details','project_skills.skill_data','dispute'])
                                                     ->orderBy('created_at','DESC')
                                                     ->paginate(config('app.project.pagi_cnt')); 
                                        
      $arr_dispute_projects = array();
      $arr_pagination = array();

      if($obj_dispute_projects)
      {
        $arr_pagination       = clone $obj_dispute_projects;
        $arr_dispute_projects = $obj_dispute_projects->toArray();
      }
        
      
      $this->arr_view_data['arr_dispute_projects']  = $arr_dispute_projects;
      $this->arr_view_data['arr_pagination']        = $arr_pagination;
      $this->arr_view_data['module_url_path']       = $this->module_url_path;

      return view($this->view_folder_path.'.dispute_projects',$this->arr_view_data);

    }

   	/*
		Comments : Show dipsuetes page.
		Auther   : Nayan S. 		
   	*/ 		

	public function show_dispute($enc_id)
	{
		$project_id  = base64_decode($enc_id);      

		/*--------------------------------------------------------
		 Checking that in the url Project id is correctly passed 
		 ---------------------------------------------------------*/
		$is_valid_id = $this->ProjectpostModel->where('id','=',$project_id)
											  ->where('expert_user_id','=',$this->user_id)
											  ->count();

		if($is_valid_id == 0)
		{
			Session::flash('error',trans('controller_translations.error_unauthorized_action'));
			return redirect()->back();
		}	
		/*------- ends ------*/										  	

	    $arr_dispute_details  = [];
	        
	    $obj_dispute_details  = $this->DisputeModel->where('project_id',$project_id)->first();
	      
	    if($obj_dispute_details != FALSE)
	    {
	        $arr_dispute_details = $obj_dispute_details->toArray(); 
	    }
	     
      
	    $is_dispute_exists   =  $this->DisputeModel->where('project_id',$project_id)->count();


      $arr_static_privacy = $arr_static_terms = [];
      $obj_static_privacy =  $this->StaticPageModel->where('page_slug','privacy-policy')->where('is_active',"1")->first();
      $arr_static_privacy = array();
      if($obj_static_privacy)
      {
        $arr_static_privacy = $obj_static_privacy->toArray();
      }
      
      $obj_static_terms =  $this->StaticPageModel->where('page_slug','terms-of-service')->where('is_active',"1")->first();
      $arr_static_terms = array();
      if($obj_static_terms)
      {
        $arr_static_terms = $obj_static_terms->toArray();
      }

        
      $this->arr_view_data['arr_static_privacy']   = $arr_static_privacy;
      $this->arr_view_data['arr_static_terms']     = $arr_static_terms;
        

	    $this->arr_view_data['page_title']           = trans('controller_translations.page_title_dispute');
	    $this->arr_view_data['projectInfo']          = $this->get_project_details($project_id);
	    $this->arr_view_data['arr_dispute_details']  = $arr_dispute_details;
	    $this->arr_view_data['is_dispute_exists']    = $is_dispute_exists;
	      
	    return view($this->view_folder_path.'.dispute',$this->arr_view_data);
	}

	/*
     	Comments    : To get projects all details.
     	Auther      : Nayan S.
    */

    public function get_project_details($id)
    { 
      $obj_project_info = $this->ProjectpostModel
                               ->with('skill_details','client_info','client_details','project_skills.skill_data','category_details','sub_category_details','project_bid_info','project_bids_infos')
                               ->where('id',$id)
                               ->first();

      $arr_project_info = array();

      if($obj_project_info != FALSE)
      {
        $arr_project_info = $obj_project_info->toArray();
      }
      return $arr_project_info;
    }	

    /*
     	Comment : Add Disputes Against Client.
     	Auther  : Nayan S.
    */

    public function add(Request $request)
    { 
        $arr_rules = [];
        $arr_rules['title']       = "required";
        $arr_rules['description'] = "required";
        
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = [];
        $form_data = $request->all();
         
        $arr_dispute = [];
      
        $project_id     = base64_decode($form_data['project_id']);
        $client_user_id = base64_decode($form_data['client_user_id']);

        if($request->has('title') && $request->has('description')  && $project_id != "" && $client_user_id!= "")
        {
            $arr_dispute['title']        	= $form_data['title'];
            $arr_dispute['description']  	= $form_data['description'];
            $arr_dispute['project_id']   	= $project_id;
            $arr_dispute['client_user_id'] 	= $client_user_id;
            $arr_dispute['expert_user_id'] 	= $this->user_id;
            $arr_dispute['added_by'] 		= '2';
            $arr_dispute['status'] 			= '0'; /* status open */

            $res_dispute = $this->DisputeModel->create($arr_dispute); 

            if($res_dispute)
            { 
                  Session::flash('success', trans('controller_translations.success_dispute_submitted_successfully'));

                  /* Send email to Client about dispute added against him. */
                  
                  $project_name = config('app.project.name');
                  //$mail_form    = get_site_email_address();   /* getting email address of admin from helper functions */
                  $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
                  
                  $obj_project_info = $this->ProjectpostModel->where('id',$project_id)
                                                              ->with(['client_info'=> function ($query) {
                                                                      $query->select('user_id','first_name','last_name');
                                                                    },
                                                                    'client_details'=>function ($query_nxt) {
                                                                      $query_nxt->select('id','email');
                                                                    }])
                                                              ->with(['expert_info'=> function ($query_nxt_1) {
                                                                    $query_nxt_1->select('user_id','first_name','last_name');
                                                                    },
                                                                    'expert_details'=>function ($query_nxt) {
                                                                      $query_nxt->select('id','email','user_name');
                                                                    }])
                                                              ->first(['id','project_name','client_user_id','expert_user_id']);


                  $data = [];

                  if($obj_project_info)
                  {
                    $arr_project_info      = $obj_project_info->toArray();
                    /* get client name */
                    $client_first_name     = isset($arr_project_info['client_info']['first_name'])?$arr_project_info['client_info']['first_name']:'';
                    $client_last_name      = isset($arr_project_info['client_info']['last_name'])?$arr_project_info['client_info']['last_name']:'';
                    $client_username       = isset($arr_project_info['client_details']['user_name'])?$arr_project_info['client_details']['user_name']:'';
                    $client_name           = $client_first_name/*.' '.$client_last_name*/;
                    $expert_first_name     = isset($arr_project_info['expert_info']['first_name'])?$arr_project_info['expert_info']['first_name']:'';
                    $expert_last_name      = isset($arr_project_info['expert_info']['first_name'])?$arr_project_info['expert_info']['first_name']:'';
                    $expert_name           = $expert_first_name.' '.$expert_last_name;
                    $expert_username       = isset($arr_project_info['expert_details']['user_name'])?$arr_project_info['expert_details']['user_name']:'';
                    /* Getting admins information from helper function */
                    $admin_data = get_admin_email_address();
                    $data['project_name']        = isset($arr_project_info['project_name'])?$arr_project_info['project_name']:'';
                    $data['client_name']         = $client_name;
                    $data['expert_name']         = $expert_name;
                    $data['client_username']     = $client_username;
                    $data['expert_username']     = $expert_username;
                    $data['admin_name']          = isset($admin_data['user_name'])?$admin_data['user_name']:'';
                    $data['login_url']           = url('/redirection?redirect=PROJECT&id='.base64_encode($project_id)) ;
                    $data['admin_login_url']     = url('/redirection?redirect=DISPUTE');
                  }

                  /* mail to admin */
                  $email_to_admin = isset($admin_data['email'])?$admin_data['email']:'';

                  if($email_to_admin!= "")
                  {
                    try{
                        $data['email_id']     = $email_to_admin;
                        $mail_status = $this->MailService->send_dispute_added_by_expert_to_admin_email($data);
                        // Mail::send('front.email.dispute_added_by_expert_to_admin', $data, function ($message) use ($email_to_admin,$mail_form,$project_name) {
                        //     $message->from($mail_form, $project_name);
                        //     $message->subject($project_name.': Dispute Added By Expert.');
                        //     $message->to($email_to_admin);
                        // });
                    }
                    catch(\Exception $e){
                    Session::Flash('error',trans('controller_translations.text_mail_not_sent'));
                    }
                  }
                  /* mail to admin ends */

                  /* mail to expert */
                  $email_to_expert = isset($arr_project_info['client_details']['email'])?$arr_project_info['client_details']['email']:'';

                  if($email_to_expert!= "")
                  {
                    try{
                          $data['email_id']  = $email_to_expert;
                          $mail_status       = $this->MailService->send_dispute_added_by_expert_to_client_email($data);
                          
                        // Mail::send('front.email.dispute_added_by_expert_to_client', $data, function ($message) use ($email_to_expert,$mail_form,$project_name) {
                        //     $message->from($mail_form, $project_name);
                        //     $message->subject($project_name.': Dispute Added By Expert.');
                        //     $message->to($email_to_expert);
                        // });
                    }
                    catch(\Exception $e){
                    Session::Flash('error',trans('controller_translations.text_mail_not_sent'));
                    }
                  }
                  /* mail to expert ends */                  

                  $posted_project_name = isset($obj_project_info->project_name) ? $obj_project_info->project_name : '';
                  /* Create Notification for Client */      
                  $arr_data =  [];

                  $arr_data['user_id']    = $client_user_id;
                  $arr_data['user_type']  = '2';
                  $arr_data['url']        = 'client/projects/details/'.base64_encode($project_id);
                  $arr_data['project_id'] = $project_id;
                  
                  /*$arr_data['notification_text'] =  trans('controller_translations.msg_dispute_added_by_expert') ;*/

                  $arr_data['notification_text_en'] =  $posted_project_name . ' - ' . Lang::get('controller_translations.msg_dispute_added_by_expert',[],'en','en');
                  $arr_data['notification_text_de'] =  $posted_project_name . ' - ' . Lang::get('controller_translations.msg_dispute_added_by_expert',[],'de','en');

                  $this->NotificationsModel->create($arr_data);  

                   /* Notification to admin */
                  $arr_admin_data =  [];

                  $arr_admin_data['user_id']    = 1;
                  $arr_admin_data['user_type']  = '1';
                  $arr_admin_data['url']        = 'admin/dispute/details/'.base64_encode($project_id);
                  $arr_admin_data['project_id'] = $project_id;

                  /*$arr_admin_data['notification_text'] = trans('controller_translations.msg_dispute_added_by_expert') ;*/

                  $arr_admin_data['notification_text_en'] =  Lang::get('controller_translations.msg_dispute_added_by_expert',[],'en','en');
                  $arr_admin_data['notification_text_de'] =  Lang::get('controller_translations.msg_dispute_added_by_expert',[],'de','en');

                  $this->NotificationsModel->create($arr_admin_data); 
               
                  /* Notification ends */
			      }
            else
            {
                Session::flash('error',trans('controller_translations.error_error_while_submitting_dispute'));
            }
        }
        else
        {   
            Session::flash('error',trans('controller_translations.error_error_while_submitting_dispute'));
        }

        return redirect()->back();

    }

}
