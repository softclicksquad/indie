<?php

namespace App\Http\Controllers\Front\Expert;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\ProjectpostModel;

use Sentinel;
use Validator;
use Session;

class InvoiceController extends Controller
{
    public $arr_view_data;
    public function __construct(  
                                  ProjectpostModel $projectpost
                                )
    {
      $this->arr_view_data = [];

      if(! $user = Sentinel::check()) 
      {
        return redirect('/login');
      }

      if(!$user->inRole('expert')) 
      {
        return redirect('/login'); 
      }

      $this->user_id = $user->id;

      $this->ProjectpostModel   = $projectpost;
      
      $this->view_folder_path   = 'expert/invoice';
      $this->module_url_path    = url("/expert");
    }
    public function index()
    {
    	//dd("asd");
        $this->arr_view_data['page_title']        = /*trans('controller_translations.page_title_dashboard')*/ 'Invoice';
        $this->arr_view_data['module_url_path'] 	= $this->module_url_path;

    	return view($this->view_folder_path.'.index',$this->arr_view_data);
    }
}
