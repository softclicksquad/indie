<?php
namespace App\Http\Controllers\Front\Expert;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Models\ProjectpostModel;
use App\Models\MilestoneReleaseModel;
use App\Common\Services\WalletService;
use App\Models\UserModel;
use App\Models\DocumentsMasterModel;
use App\Models\ExpertsModel;
use App\Models\CurrencyModel;
use App\Models\UserWalletModel;



use Sentinel;
use Validator;
use Session;

class WalletDashboardController extends Controller
{
    public $arr_view_data;
    public function __construct(  
                                  ProjectpostModel $projectpost,MilestoneReleaseModel  $milestone,WalletService $WalletService, UserModel $UserModel,
                                    DocumentsMasterModel $DocumentsMasterModel,
                                    ExpertsModel $ExpertsModel
                                )
    {
      $this->arr_view_data = [];
      if(! $user = Sentinel::check()) 
      {
        return redirect('/login');
      }
      if(!$user->inRole('expert')) 
      {
        return redirect('/login'); 
      }

      $this->user    = $user;
      $this->user_id = $user->id;
      $this->ProjectpostModel        = $projectpost;
      $this->MilestoneReleaseModel   = $milestone;
      $this->WalletService           = $WalletService;
      $this->DocumentsMasterModel    = $DocumentsMasterModel;
      $this->UserModel               = $UserModel;
      $this->ExpertsModel            = $ExpertsModel;
      $this->CurrencyModel           = new CurrencyModel;
      $this->UserWalletModel         = new UserWalletModel;

      $this->view_folder_path        = 'expert/wallet';
      $this->module_url_path         = url("/expert");
      if(isset($this->user))
      {
        $logged_user                         = $this->user->toArray();  
        $this->currency_code         = isset($logged_user['currency_code'])?$logged_user['currency_code']:'USD';
        $this->currency_symbol       = isset($logged_user['currency_symbol'])?$logged_user['currency_symbol']:'$';

        if($logged_user['mp_wallet_created'] == 'Yes')
        {
          $this->mp_user_id          = $logged_user['mp_user_id'];
          $this->mp_wallet_id        = $logged_user['mp_wallet_id'];
          $this->mp_wallet_created   = $logged_user['mp_wallet_created'];
        } 
        else 
        {
            $this->mp_user_id          = '';
            $this->mp_wallet_id        = '';
            $this->mp_wallet_created   = 'No';
        }
      } 
      else 
      {
        $this->mp_user_id          = '';
        $this->mp_wallet_id        = '';
        $this->mp_wallet_created   = 'No';
      }
    }
    public function show_dashboard(Request $request)
    {   
      $mango_user_detail                = [];
      $mangopay_wallet_details          = []; 
      $mangopay_kyc_details             = []; 
      $mangopay_bank_details            = []; 
      $mangopay_transaction_details     = [];
      $mangopay_cards_details           = [];
      $mangopay_ubo_declaration = [];
      $is_blocked_country               = '0';
      $get_transaction_pagi_links_count = 0;
      $service_charge                   = $request->input('service_charge',null);
      $currency_code                    = $request->input('currency',null);
      $currency_symbol                  = $request->input('currency_symbol',null);
      //dd($service_charge,$currency_code);
      if($service_charge!=null && $currency_code!=null)
      {
        $arr_data   = get_user_wallet_details($this->user_id,$currency_code);
        $admin_data = get_user_wallet_details('1',$currency_code);

        $transaction_admin['tag']                     = ' Wallet Payin ';
        $transaction_admin['debited_UserId']          = isset($arr_data['mp_user_id']) ? $arr_data['mp_user_id']:'';
        $transaction_admin['credited_UserId']         = isset($admin_data['mp_user_id']) ? $admin_data['mp_user_id']:'';  
        $transaction_admin['currency_code']           = $currency_code;
        $transaction_admin['total_pay']               = $service_charge;
        $transaction_admin['cost_website_commission'] = 0;
        $transaction_admin['debited_walletId']        = isset($arr_data['mp_wallet_id']) ? $arr_data['mp_wallet_id']:'';
        $transaction_admin['credited_walletId']       = isset($admin_data['mp_wallet_id']) ? $admin_data['mp_wallet_id']:'';
        $admin_transfer = $this->WalletService->walletTransfer($transaction_admin);
        return redirect(url('/expert/wallet/dashboard'));
      }

      if(isset($this->user))
      {
        $logged_user                       = $this->user->toArray();  
        $user_type = isset($logged_user['role_info']['user_type'])?$logged_user['role_info']['user_type']:'';

        if($logged_user['mp_wallet_created'] == 'Yes'){
          
          $obj_data = $this->UserWalletModel->where('user_id',$logged_user['id'])->get();
      
          if($obj_data)
          {
            $arr_data = $obj_data->toArray();
          }

          // $mango_data['mp_user_id']          = $logged_user['mp_user_id'];
          // $mango_data['mp_wallet_id']        = $logged_user['mp_wallet_id'];
          // $mango_data['mp_wallet_created']   = $logged_user['mp_wallet_created'];
          $mango_data['mp_user_id']          = isset($arr_data[0]['mp_user_id'])?$arr_data[0]['mp_user_id']:'';
          
          $mango_data['mp_wallet_created']   = $logged_user['mp_wallet_created'];
          $mango_user_detail                 = $this->WalletService->get_user_details($mango_data['mp_user_id']);

          //$mango_user_detail                 = $this->WalletService->get_user_details($mango_data['mp_user_id']);

          if(isset($mango_user_detail)){
            // get mangopay wallet balance 
                $get_mangopay_wallet_details = Session::get('get_mangopay_wallet_details_expert');
                if($get_mangopay_wallet_details==null)
                {
                  foreach ($arr_data as $key => $value) 
                  {
                    $mango_data['mp_wallet_id']  = $value['mp_wallet_id'];
                    $get_mangopay_wallet_details[] = $this->WalletService->get_wallet_details($mango_data['mp_wallet_id']);
                  }
                }
                  //$get_mangopay_wallet_details = $this->WalletService->get_wallet_details($mango_data['mp_wallet_id']);
                  if(isset($get_mangopay_wallet_details)){
                    $mangopay_wallet = $get_mangopay_wallet_details;
                    $mangopay_wallet_details = $mangopay_wallet;
                    Session::forget('get_mangopay_wallet_details_expert');
                    Session::put('get_mangopay_wallet_details_expert',$get_mangopay_wallet_details);
                  }
                
            // end get mangopay wallet balance 
            
            // get mangopay kyc details
            $mangopay_kyc_details = $this->WalletService->get_kyc_details($mango_data['mp_user_id']);
            // end get mangopay kyc details

            // GET CARD LIST
            $mangopay_cards_details = $this->WalletService->get_cards_list($mango_data['mp_user_id']);
            // END GET CARDS LIST 

            // GET UBO LIST
            $mangopay_ubo_declaration = $this->WalletService->get_ubo($mango_data['mp_user_id']);
            // END UBO LIST

            if(isset($mangopay_ubo_declaration) && sizeof($mangopay_ubo_declaration)>0 && $mangopay_ubo_declaration != '0')
            {
              $mangopay_ubo_declaration = array_reverse($mangopay_ubo_declaration);
            }

            $obj_user = $this->UserModel->where('id',$this->user->id)
                                        ->with(['expert_details'=>function($qry){
                                            $qry->select('id','user_id','country');
                                        }])->first();

            $country_id = isset($obj_user->expert_details->country)?$obj_user->expert_details->country:'0';

            $blocked_country = ['3','33','18','36','120','71','85','97','111','129','234','181','193','133','215','225','245','248','229','110','42'];

            $is_blocked_country = '0';
            if(in_array($country_id,$blocked_country))
            {
              $is_blocked_country = '1';
            }


            $is_kyc_verified = isset($obj_user->kyc_verified)?$obj_user->kyc_verified:'0';
            
            if($is_kyc_verified == 0)
            {
              $mangopay_kyc_details = (array)$mangopay_kyc_details;
              if(isset($mangopay_kyc_details))
              {
                foreach ($mangopay_kyc_details as $key => $value) 
                {
                    $arr_mangopay_kyc_details[$key] = (array) $value ;
                }
              }

              $kyc_key = 'Status';
              $kyc_val = 'VALIDATED';
              $verified = 0;

              if(isset($arr_mangopay_kyc_details) && is_array($arr_mangopay_kyc_details)){
                if(find_key_value_in_array($arr_mangopay_kyc_details, $kyc_key, $kyc_val)){
                  $verified = 1;
                }
              }

              $this->UserModel->where('id',$this->user->id)->update(['kyc_verified'=>$verified]);  
            }
            

            // get mangopay bank details;
            $mangopay_bank_details = $this->WalletService->get_banks_details($mango_data['mp_user_id']);
            // end get mangopay bank details;

            // get mangopay transaction details;
            

            // $mangopay_transaction_details = Session::get('mangopay_transaction_details_expert');
            // if($mangopay_transaction_details==null){
            //   $get_transaction_pagi_links_count = 1;//$this->WalletService->get_wallet_transaction_pagi_links_count($mango_data['mp_wallet_id']);
            //   if(isset($_REQUEST['page_cnt']) && $_REQUEST['page_cnt'] !=""){
                
            //     foreach ($arr_data as $key => $value) {
            //     $mp_wallet_id  = $value['mp_wallet_id'];
            //     $mangopay_transaction_details[] = $this->WalletService->get_wallet_transaction_details($mp_wallet_id,$_REQUEST['page_cnt']);
            //     }

              
            //   } else {
            //   foreach ($arr_data as $key => $value) {
            //       $mp_wallet_id  = $value['mp_wallet_id'];
            //       $mangopay_transaction_details[]= $this->WalletService->get_wallet_transaction_details($mp_wallet_id,$get_transaction_pagi_links_count);
            //     }
            //   }
            // }

            // if(isset($mangopay_transaction_details) && count($mangopay_transaction_details)>0)
            // {
            //   Session::forget('mangopay_transaction_details_expert');
            //   Session::put('mangopay_transaction_details_expert',$mangopay_transaction_details);
            // }


            // end get mangopay transaction details; 
            $this->arr_view_data['mp_wallet_created']        = 'Yes';  
          }    
        } else {
          $this->arr_view_data['mp_wallet_created']          = 'No'; 
        }
      }
      $country_id = isset($logged_user['role_info']['country_details']['id'])?
                          $logged_user['role_info']['country_details']['id']:'0';
      $this->arr_view_data['mp_user_id']                     = isset($mango_data['mp_user_id'])?
                                                                     $mango_data['mp_user_id']:'';

      $this->arr_view_data['currency_code']                  = $this->currency_code;
      $this->arr_view_data['country_id']                     = $country_id;
      $this->arr_view_data['page_title']                     = 'Mangopay';
      $this->arr_view_data['is_blocked_country']             = $is_blocked_country;
      $this->arr_view_data['mango_user_detail']              = $mango_user_detail;
      $this->arr_view_data['mangopay_wallet_details']        = $mangopay_wallet_details;
      $this->arr_view_data['mangopay_kyc_details']           = $mangopay_kyc_details;
      $this->arr_view_data['mangopay_bank_details']          = $mangopay_bank_details;
      $this->arr_view_data['mangopay_ubo_declaration']       = $mangopay_ubo_declaration;
      $this->arr_view_data['mangopay_cards_details']         = $mangopay_cards_details;
      
      $this->arr_view_data['mangopay_transaction_pagi_links_count']   = $get_transaction_pagi_links_count;
      $this->arr_view_data['module_url_path'] = $this->module_url_path;

      $obj_expert_deatils = $this->ExpertsModel->where('user_id',$this->user_id)->first();
      $user_type = isset($obj_expert_deatils->user_type)?$obj_expert_deatils->user_type:'';
      $this->arr_view_data['user_type'] = $user_type;


      $arr_country = ['AD','AE','AF','AG','AI','AL','AM','AN','AO','AQ','AR','AS','AT','AU','AW','AX','AZ','BA','BB','BD','BE','BF','BG','BH','BI','BJ','BL','BM','BN','BO','BQ','BR','BS','BT','BV','BW','BY','BZ','CA','CC','CD','CF','CG','CH','CI','CK','CL','CM','CN','CO','CR','CU','CV','CW','CX','CY','CZ','DE','DJ','DK','DM','DO','DZ','EC','EE','EG','EH','ER','ES','ET','FI','FJ','FK','FM','FO','FR','GA','GB','GD','GE','GF','GG','GH','GI','GL','GM','GN','GP','GQ','GR','GS','GT','GU','GW','GY','HK','HM','HN','HR','HT','HU','ID','IE','IL','IM','IN','IO','IQ','IR','IS','IT','JE','JM','JO','JP','KE','KG','KH','KI','KM','KN','KP','KR','KW','KY','KZ','LA','LB','LC','LI','LK','LR','LS','LT','LU','LV','LY','MA','MC','MD','ME','MF','MG','MH','MK','ML','MM','MN','MO','MP','MQ','MR','MS','MT','MU','MV','MW','MX','MY','MZ','NA','NC','NE','NF','NG','NI','NL','NO','NP','NR','NU','NZ','OM','PA','PE','PF','PG','PH','PK','PL','PM','PN','PR','PS','PT','PW','PY','QA','RE','RO','RS','RU','RW','SA','SB','SC','SD','SE','SG','SH','SI','SJ','SK','SL','SM','SN','SO','SR','SS','ST','SV','SX','SY','SZ','TC','TD','TF','TG','TH','TJ','TK','TL','TM','TN','TO','TR','TT','TV','TW','TZ','UA','UG','UM','US','UY','UZ','VA','VC','VE','VG','VI','VN','VU','WF','WS','YE','YT','ZA','ZM','ZW'];
      $this->arr_view_data['arr_country'] = $arr_country;
      //dd($this->arr_view_data);
      return view($this->view_folder_path.'.dashboard',$this->arr_view_data);
    }

   public function transactions()
    {
       $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)->get();
       if($obj_data)
       {
          $arr_data = $obj_data->toArray();
       }
       $get_transaction_pagi_links_count = isset($_GET['page_cnt'])?$_GET['page_cnt']:1;
       $mangopay_transaction_details = Session::get('mangopay_transaction_details_client');
        if($mangopay_transaction_details==null){
          
          foreach ($arr_data as $key => $value) {
              $mp_wallet_id  = $value['mp_wallet_id'];
              $mangopay_transaction_details[]= $this->WalletService->get_wallet_transaction_details($mp_wallet_id,$get_transaction_pagi_links_count);
            }
          }
        
        if(isset($mangopay_transaction_details) && count($mangopay_transaction_details)>0)
        {
          Session::forget('mangopay_transaction_details_client');
          Session::put('mangopay_transaction_details_client',$mangopay_transaction_details);
        }

        $get_mangopay_wallet_details = Session::get('get_mangopay_wallet_details_expert');
        if($get_mangopay_wallet_details==null)
        {
          foreach ($arr_data as $key => $value) 
          {
            $mango_data['mp_wallet_id']  = $value['mp_wallet_id'];
            $get_mangopay_wallet_details[] = $this->WalletService->get_wallet_details($mango_data['mp_wallet_id']);
          }
        }

        if(isset($get_mangopay_wallet_details)){
          $mangopay_wallet = $get_mangopay_wallet_details;
          $mangopay_wallet_details = $mangopay_wallet;
          Session::forget('get_mangopay_wallet_details_expert');
          Session::put('get_mangopay_wallet_details_expert',$get_mangopay_wallet_details);
        }
                


        $this->arr_view_data['mangopay_wallet_details']                 = $get_mangopay_wallet_details;
        $this->arr_view_data['mangopay_transaction_pagi_links_count']   = $get_transaction_pagi_links_count;
        $this->arr_view_data['mangopay_transaction_details']            = $mangopay_transaction_details;
        return view($this->view_folder_path.'.transactions',$this->arr_view_data);
       
    }

    public function withdraw()
    {
        $arr_data    = [];
        $logged_user = $this->user->toArray();  
        $user_type   = isset($logged_user['role_info']['user_type'])?$logged_user['role_info']['user_type']:'';
        $country_id  = isset($logged_user['role_info']['country_details']['id'])?
                             $logged_user['role_info']['country_details']['id']:'0';

        $obj_data    = $this->UserWalletModel->where('user_id',$logged_user['id'])->get();
        if($obj_data)
        {
          $arr_data = $obj_data->toArray();
        }
        $mp_user_id            = isset($arr_data[0]['mp_user_id'])?$arr_data[0]['mp_user_id']:'';
        $mangopay_bank_details = $this->WalletService->get_banks_details($mp_user_id);

        $arr_country = ['AD','AE','AF','AG','AI','AL','AM','AN','AO','AQ','AR','AS','AT','AU','AW','AX','AZ','BA','BB','BD','BE','BF','BG','BH','BI','BJ','BL','BM','BN','BO','BQ','BR','BS','BT','BV','BW','BY','BZ','CA','CC','CD','CF','CG','CH','CI','CK','CL','CM','CN','CO','CR','CU','CV','CW','CX','CY','CZ','DE','DJ','DK','DM','DO','DZ','EC','EE','EG','EH','ER','ES','ET','FI','FJ','FK','FM','FO','FR','GA','GB','GD','GE','GF','GG','GH','GI','GL','GM','GN','GP','GQ','GR','GS','GT','GU','GW','GY','HK','HM','HN','HR','HT','HU','ID','IE','IL','IM','IN','IO','IQ','IR','IS','IT','JE','JM','JO','JP','KE','KG','KH','KI','KM','KN','KP','KR','KW','KY','KZ','LA','LB','LC','LI','LK','LR','LS','LT','LU','LV','LY','MA','MC','MD','ME','MF','MG','MH','MK','ML','MM','MN','MO','MP','MQ','MR','MS','MT','MU','MV','MW','MX','MY','MZ','NA','NC','NE','NF','NG','NI','NL','NO','NP','NR','NU','NZ','OM','PA','PE','PF','PG','PH','PK','PL','PM','PN','PR','PS','PT','PW','PY','QA','RE','RO','RS','RU','RW','SA','SB','SC','SD','SE','SG','SH','SI','SJ','SK','SL','SM','SN','SO','SR','SS','ST','SV','SX','SY','SZ','TC','TD','TF','TG','TH','TJ','TK','TL','TM','TN','TO','TR','TT','TV','TW','TZ','UA','UG','UM','US','UY','UZ','VA','VC','VE','VG','VI','VN','VU','WF','WS','YE','YT','ZA','ZM','ZW'];
        
        $mangopay_wallet_details = get_user_all_wallet_details();
        if (isset($mangopay_wallet_details) && count($mangopay_wallet_details)>0)
        {
          foreach ($mangopay_wallet_details as $key => $value) 
          {     
             if(isset($value->Balance->Amount) && $value->Balance->Amount > 0)
             {
               $arr_data['balance'][]  = isset($value->Balance->Amount)?$value->Balance->Amount/100:'0';
               $arr_data['currency'][] = isset($value->Balance->Currency)?$value->Balance->Currency:'';
             }
          }
        }

        $this->arr_view_data['arr_data']    = $arr_data;
        $this->arr_view_data['arr_country']            = $arr_country;
        $this->arr_view_data['country_id']             = $country_id;
        $this->arr_view_data['user_type']              = $user_type;
        $this->arr_view_data['mangopay_bank_details']  = $mangopay_bank_details;
        //dd($this->arr_view_data);
        return view($this->view_folder_path.'.withdraw',$this->arr_view_data);
    }


    public function add_money_wallet(Request $request)
    {
      $transaction_inp  = [];
      $default_currency = '';
      $user_id          = $this->user_id;
      $currency_code    = $request->input('project_currency_code');
      $amount           = $request->input('amount');
      $currency_symbol  = $request->input('currency_symbol');
      $admin_data       = get_user_wallet_details('1',$currency_code);
      
      $obj_user      = UserModel::where('id',$user_id)
                              ->first();
      //dd($obj_user);
      if($obj_user)
      {
          $default_currency = isset($obj_user->currency_code)?$obj_user->currency_code:'';
      }
      // dd($default_currency);
      if($default_currency== '')
      {
          $data['currency_code'] = $request->input('project_currency_code'); 
          UserModel::where('id',$user_id)->update($data);
      }

      $amount = round($amount);
      
      $arr_service_charge = get_service_charge($amount,$currency_code);
      $service_charge     = isset($arr_service_charge['service_charge']) ? $arr_service_charge['service_charge'] : 0;
      $total_amount       = isset($arr_service_charge['total_amount']) ? $arr_service_charge['total_amount'] : 0;
      
      /*get mpuser id and wallet_id from table*/
      $obj_data = $this->UserWalletModel->where('user_id',$user_id)->where('currency_code',$currency_code)->first();
      
      if($obj_data)
      {
        $arr_data = $obj_data->toArray();
      }
      else
      {
        Session::flash('error','Please create wallet first');
        return redirect()->back();
      }
      //dd($arr_data);
      
      if(isset($arr_data) && count($arr_data)>0)
      {
        $transaction_inp['mp_user_id']      = isset($arr_data['mp_user_id'])?$arr_data['mp_user_id']:'';
        $transaction_inp['mp_wallet_id']    = isset($arr_data['mp_wallet_id'])?$arr_data['mp_wallet_id']:'';
        $transaction_inp['ReturnURL']       = url('/expert/wallet/dashboard?service_charge='.$service_charge.'&currency='.$currency_code.'&currency_symbol='.$currency_symbol);
        $transaction_inp['currency_code']   = $request->input('project_currency_code');//$this->currency_code;
        $transaction_inp['currency_symbol'] = $request->input('currency_symbol');//$this->currency_symbol;
      }

      $transaction_inp['amount']          = (float) $total_amount;

      $mp_add_money_in_wallet             = $this->WalletService->payInWallet($transaction_inp);


      if(isset($mp_add_money_in_wallet->Status) && $mp_add_money_in_wallet->Status == 'CREATED'){
          return redirect($mp_add_money_in_wallet->ExecutionDetails->RedirectURL); 
      } else {
        if(isset($mp_add_money_in_wallet) && $mp_add_money_in_wallet == false){
          Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
          return redirect()->back();
        }else{
          Session::flash('error',$mp_add_money_in_wallet->ResultMessage);
          return redirect()->back();
        }
      }
    }
    // public function add_money_wallet(Request $request)
    // {
    //   $transaction_inp                    = [];
    //   $user_id       = $this->user_id;
    //   $currency_code = $request->input('project_currency_code');
      
    //   $obj_data = $this->UserWalletModel->where('user_id',$user_id)->where('currency_code',$currency_code)->first();
      
    //   if($obj_data)
    //   {
    //     $arr_data = $obj_data->toArray();
    //   }
    //   //dd($arr_data);
      
    //   if(isset($arr_data) && count($arr_data)>0)
    //   {
    //     $transaction_inp['mp_user_id']      = isset($arr_data['mp_user_id'])?$arr_data['mp_user_id']:'';
    //     $transaction_inp['mp_wallet_id']    = isset($arr_data['mp_wallet_id'])?$arr_data['mp_wallet_id']:'';
    //     $transaction_inp['ReturnURL']       = url('/client/wallet/dashboard');
    //     $transaction_inp['currency_code']   = $request->input('project_currency_code');//$this->currency_code;
    //     $transaction_inp['currency_symbol'] = $request->input('currency_symbol');//$this->currency_symbol;
    //   }

    //   $amount                 = $request->input('amount');
    //   $currency_symbol        = $request->input('currency_symbol');

    //   $obj_data = $this->CurrencyModel->with('deposite')
    //                                   ->where('currency_code',$currency_code)
    //                                   ->first();
    //   if($obj_data)
    //   {
    //       $arr_data = $obj_data->toArray();
    //       $min_amount = isset($arr_data['deposite']['min_amount'])?$arr_data['deposite']['min_amount']:'';
    //       $max_amount = isset($arr_data['deposite']['max_amount'])?$arr_data['deposite']['max_amount']:'';
    //       $min_charge = isset($arr_data['deposite']['min_amount_charge'])?$arr_data['deposite']['min_amount_charge']:'';
    //       $max_charge = isset($arr_data['deposite']['max_amount_charge'])?$arr_data['deposite']['max_amount_charge']:'';
          
    //       if($amount>$min_amount)
    //       {
    //           $service_charge = (float)$amount * (float)$max_charge/100;
    //       }
    //       else
    //       {
    //           $service_charge =  (float) $min_charge;
    //       }
    //       $total_amount = (float)$amount + (float)$service_charge;
    //   }

    //   $transaction_inp['amount']          = (float) $total_amount;

    //   $mp_add_money_in_wallet             = $this->WalletService->payInWallet($transaction_inp);
    //   //dd($mp_add_money_in_wallet);
    //   if(isset($mp_add_money_in_wallet->Status) && $mp_add_money_in_wallet->Status == 'CREATED'){
    //       return redirect($mp_add_money_in_wallet->ExecutionDetails->RedirectURL); 
    //   } else {
    //     if(isset($mp_add_money_in_wallet) && $mp_add_money_in_wallet == false){
    //       Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
    //       return redirect()->back();
    //     }else{
    //       Session::flash('error',$mp_add_money_in_wallet);
    //       return redirect()->back();
    //     }
    //   }
    // }
    public function create_user()
    {
      $arr_currency = [];

      $obj_currency = $this->CurrencyModel->select('id','currency_code')->get();
      if($obj_currency)
      {
        $arr_currency = $obj_currency->toArray();
      }

      if(isset($this->user))
      {
        $logged_user  = $this->user->toArray();  
        if(1)//(!empty($logged_user['currency_code']))
        {

          $obj_expert_deatils = $this->ExpertsModel->where('user_id',$this->user_id)->first();
          
          $user_type = isset($obj_expert_deatils->user_type)?$obj_expert_deatils->user_type:'';
          $company_name = isset($obj_expert_deatils->company_name)?$obj_expert_deatils->company_name:'';

          $first_name   = "-";
          $last_name    = "-";
          $email        = "-";
          if(isset($logged_user['role_info']['first_name']) && $logged_user['role_info']['first_name'] !="")
          {
            $first_name = $logged_user['role_info']['first_name'];
          }
          if(isset($logged_user['role_info']['last_name']) && $logged_user['role_info']['last_name'] !="")
          {
            $last_name = $logged_user['role_info']['last_name'];
          }
          if(isset($logged_user['email']) && $logged_user['email'] !=""){
            $email = $logged_user['email'];
          }
          try
          {
            $timezoneinfo = file_get_contents('http://ip-api.com/json/' . $_SERVER['REMOTE_ADDR']);
          }catch(\Exception $e)
          {
            $timezoneinfo = file_get_contents('http://ip-api.com/json');  
          } 
          $mp_user_data = [];
          //Create account in mangopay and create wallet Start.......
            $mp_user_data['FirstName']          = $first_name;
            $mp_user_data['LastName']           = $last_name;
            $mp_user_data['Email']              = $email;
            $mp_user_data['CountryOfResidence'] = isset($timezoneinfo->countryCode)?$timezoneinfo->countryCode:"IN";
            $mp_user_data['Nationality']        = isset($timezoneinfo->countryCode)?$timezoneinfo->countryCode:"IN";
            
            //$mp_user_data['currency_code']      = isset($logged_user['currency_code'])?$logged_user['currency_code']:'USD';

            // Register user on MangoPay as well
            if(isset($user_type) && $user_type!='' && $user_type == 'private')
            {
              $Mangopay_create_user =  $this->WalletService->create_user_with_all_currency_wallet($mp_user_data,$arr_currency,$this->user->id);
            }

            if(isset($user_type) && $user_type!='' && $user_type == 'business')
            {
              $mp_user_data['company_name'] = $company_name;
              $Mangopay_create_user =  $this->WalletService->create_legal_user($mp_user_data,$arr_currency,$this->user->id);
            }

            //$Mangopay_create_user =$this->WalletService->create_user_with_all_currency_wallet($mp_user_data,$arr_currency,$this->user->id);
            if($Mangopay_create_user)
            {
              // $arr_data['mp_user_id']        = isset($Mangopay_create_user['createdUserId'])?$Mangopay_create_user['createdUserId']:"";
              // $arr_data['mp_wallet_id']      = isset($Mangopay_create_user['AppWalletId'])?$Mangopay_create_user['AppWalletId']:"";
              // $arr_data['mp_wallet_created'] = "Yes";
              // $update_mangopya_details = $this->UserModel->where('id',$this->user->id)->update($arr_data); 
              Session::flash('success',trans('common/wallet/text.text_archexpert_wallet_account_has_been_created_successfully'));
              return redirect()->back();
            } 
            else{
              Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
              return redirect()->back();
            } 
        }
        else
        {
          Session::flash('error',trans('common/wallet/text.text_please_set_default_currency'));
          return redirect()->back();
        }
      } 
      else 
      {
        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
        return redirect()->back();
      }
    }   

    
    public function create_card(Request $request)
    {
         $preregData = $request->all();
         $this->WalletService = new WalletService;
         if(isset($preregData) && !empty($preregData))
         {
            $response_prereg = $this->WalletService->card_preregistration($preregData);
            if(isset($response_prereg['status']) && $response_prereg['status']=='CREATED')
            {
                $arr_data['AccessKeyRef']       = $response_prereg['accessKey'];
                $arr_data['data']               = $response_prereg['preregistrationData']; 
                $arr_data['AccessKey']          = $response_prereg['accessKey']; // get return acces ley
                $arr_data['cardNumber']         = $preregData['card_number']; // card number
                $arr_data['cardExpirationDate'] = $preregData['expire_month'].$preregData['expire_year']; //card expiry date
                $arr_data['cardCvx']            = $preregData['cvv_no']; //card cvv no
                $arr_data['cardRegisterId']     = $response_prereg['cardPreregistrationId']; //card cvv no
                $arr_data['returnURL']          = '';//url('/client/wallet/create_card_return'); // return url of card registration
                $arr_data['cardRegistrationURL']= $response_prereg['cardRegistrationURL'];
                //dd($arr_data);
                $curl = curl_init();

                curl_setopt_array($curl, array(
                CURLOPT_URL => "https://homologation-webpayment.payline.com/webpayment/getToken",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 300,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "accessKeyRef=".$response_prereg['accessKey']."&data=".$response_prereg['preregistrationData']."&cardNumber=".$preregData['card_number']."&cardExpirationDate=".$arr_data['cardExpirationDate']."&cardCvx=".$preregData['cvv_no']."&returnURL=",
                CURLOPT_HTTPHEADER => array(
                  "Authorization: Basic{{HEADER}}",
                  "Content-Type: application/x-www-form-urlencoded",
                  "Postman-Token: fbf9a7c2-be81-43b1-a000-9a4cc564a5c8",
                  "cache-control: no-cache"
                ),
              ));
              $response = curl_exec($curl);
              $err      = curl_error($curl);
              curl_close($curl);
              if ($err) 
              {
                echo "cURL Error #:" . $err;
              }

              $arr_final_data['data']           = $response;
              $arr_final_data['cardRegisterId'] = $response_prereg['cardPreregistrationId'];

              $pre_reg_data = $this->WalletService->card_preregistration_data($arr_final_data);
              
              if(isset($pre_reg_data) && $pre_reg_data['Status'] == 'VALIDATED')
              {
                  Session::flash('success','Card registered successfully.');
                  return redirect(url('/expert/wallet/dashboard'));
              }
              else
              {
                  Session::flash('error','Something went wrong.');
                  return redirect(url('/expert/wallet/dashboard'));
              }

            }
            else
            {
              Session::flash('error','Something went wrong,please try again');
              return redirect(url('/expert/wallet/dashboard'));
            }
         }
    }


    public function add_bank(Request $request)
    {
      $transaction_inp               = [];
      $bank_type    = $request->input('bank_type');

      if($bank_type == 'IBAN'){
        $validator  = Validator::make($request->all(),[
            'FirstName'             => 'required',
            'LastName'              => 'required',
            'Country'               => 'required',
            'City'                  => 'required',
            'Region'                => 'required',
            'Address'               => 'required',
            'PostalCode'            => 'required',
            'IBAN'                  => 'required',
            'BIC'                   => 'required'
        ]);
      } else if($bank_type == 'GB'){
        $validator = Validator::make($request->all(),[
            'FirstName'             => 'required',
            'LastName'              => 'required',
            'City'                  => 'required',
            'PostalCode'            => 'required',
            'Region'                => 'required',
            'Country'               => 'required',
            'gbSortCode'            => 'required',
            'Address'               => 'required',
            'gbAccountNumber'       => 'required'
        ]);
      } else if($bank_type == 'US'){
        $validator = Validator::make($request->all(),[
            'FirstName'             => 'required',
            'LastName'              => 'required',
            'City'                  => 'required',
            'PostalCode'            => 'required',
            'Region'                => 'required',
            'Country'               => 'required',
            'Address'               => 'required',
            'usAccountNumber'       => 'required',
            'usDepositAccountType'  => 'required',
            'usABA'                 => 'required'
        ]);
      } else if($bank_type == 'CA'){
        $validator = Validator::make($request->all(),[
            'FirstName'            => 'required',
            'LastName'             => 'required',
            'City'                 => 'required',
            'PostalCode'           => 'required',
            'Region'               => 'required',
            'Country'              => 'required',
            'Address'              => 'required',
            'caAccountNumber'      => 'required',
            'caBranchCode'         => 'required',
            'caBankName'           => 'required',
            'caInstitutionNumber'  => 'required'
        ]);
      } else if($bank_type == 'OTHER'){
        $validator = Validator::make($request->all(),[
            'FirstName'            => 'required',
            'LastName'             => 'required',
            'PostalCode'           => 'required',
            'Address'              => 'required',
            'Region'               => 'required',
            'City'                 => 'required',
            'Country'              => 'required',
            'otherAccountNumber'   => 'required',
            'otherBIC'             => 'required'
        ]);
      } 

      if($validator->fails()){
        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
        return redirect('/expert/wallet/dashboard');
      }
      $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)->get();
      
      if($obj_data)
      {
        $arr_data = $obj_data->toArray();
      }

      $transaction_inp['UserId']     = isset($arr_data[0]['mp_user_id'])?$arr_data[0]['mp_user_id']:'';
      $transaction_inp['FirstName']  = $request->input('FirstName');
      $transaction_inp['LastName']   = $request->input('LastName');
      $transaction_inp['Country']    = $request->input('Country');
      $transaction_inp['PostalCode'] = $request->input('PostalCode');
      $transaction_inp['City']       = $request->input('City');
      $transaction_inp['bank_type']  = $request->input('bank_type');
      $transaction_inp['Address']    = $request->input('Address');
      $transaction_inp['Region']     = $request->input('Region');


      if($bank_type == 'IBAN'){
        $transaction_inp['IBAN']                 = $request->input('IBAN');
        $transaction_inp['BIC']                  = $request->input('BIC');
      } else if($bank_type == 'GB'){
        $transaction_inp['gbSortCode']           = $request->input('gbSortCode');
        $transaction_inp['gbAccountNumber']      = $request->input('gbAccountNumber');       
      } else if($bank_type == 'US'){
        $transaction_inp['usAccountNumber']      = $request->input('usAccountNumber');
        $transaction_inp['usDepositAccountType'] = $request->input('usDepositAccountType');
        $transaction_inp['usABA']                = $request->input('usABA');       
      } else if($bank_type == 'CA'){
        $transaction_inp['caAccountNumber']      = $request->input('caAccountNumber');
        $transaction_inp['caBranchCode']         = $request->input('caBranchCode');
        $transaction_inp['caBankName']           = $request->input('caBankName');
        $transaction_inp['caInstitutionNumber']  = $request->input('caInstitutionNumber');       
      } else if($bank_type == 'OTHER'){
        $transaction_inp['otherAccountNumber']   = $request->input('otherAccountNumber');
        $transaction_inp['otherBIC']             = $request->input('otherBIC');     
      }
      $mp_add_bank = $this->WalletService->createBankAccount($transaction_inp);
      if(isset($mp_add_bank) && $mp_add_bank == 'true'){
        Session::flash('success',trans('common/wallet/text.text_bank_has_been_successfully_registered'));
        return redirect('/expert/wallet/dashboard');
      } else{
         if(isset($mp_add_bank) && $mp_add_bank != ""){
          Session::flash('error',$mp_add_bank);
         } else {
         Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
         }
         return redirect('/expert/wallet/dashboard');
      }
    }

    public function create_ubo()
    {
      //dd('create',$this->user_id);
      if(isset($this->user))
      {
        $obj_data     = $this->UserWalletModel->where('user_id',$this->user_id)->get();
        if($obj_data)
        {
          $arr_data = $obj_data->toArray();
        } 
        
        $userId = isset($arr_data[0]['mp_user_id'])?$arr_data[0]['mp_user_id']:'';
        //dd($userId);
        $ubo_declaration = $this->WalletService->create_ubo($userId);
        //dd($ubo_declaration);
        if($ubo_declaration)
        {
          Session::flash('success','UBO Declaration created successfully.');
          return redirect()->back();
        } 
        else if($ubo_declaration == 0)
        {
          Session::flash('error','You can not create a declaration because you already have a declaration in progress'); 
          return redirect()->back();
        } 
        else
        {
          Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
          return redirect()->back();
        } 
      } 
      else
      {
        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
        return redirect()->back();
      }
    }

    public function add_UBO(Request $request)
    {
      dd('Add',$this->user_id);
      $validator = Validator::make($request->all(),[
          'FirstName'         => 'required',
          'LastName'          => 'required',
          'Address'           => 'required',
          'City'              => 'required',
          'Region'            => 'required',
          'PostalCode'        => 'required',
          'Country'           => 'required',
          'Birthday'          => 'required',
          'Nationality'       => 'required',
          'Birthplace'        => 'required',
          'Birthplace_Country'=> 'required',
          'ubo_declaration_id'=> 'required'
        ]);
      if($validator->fails())
      {
        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
        return redirect('/expert/wallet/dashboard');
      }
      $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)->get();
      
      if($obj_data)
      {
        $arr_data = $obj_data->toArray();
      }
      $arr_ubo = [];
      $arr_ubo['UserId']             = isset($arr_data[0]['mp_user_id'])?$arr_data[0]['mp_user_id']:'';
      $arr_ubo['ubo_declaration_id'] = $request->input('ubo_declaration_id');
      $arr_ubo['FirstName']          = $request->input('FirstName');
      $arr_ubo['LastName']           = $request->input('LastName');
      $arr_ubo['Address']            = $request->input('Address');
      $arr_ubo['Address1']           = $request->input('Address1');
      $arr_ubo['City']               = $request->input('City');
      $arr_ubo['Region']             = $request->input('Region');
      $arr_ubo['PostalCode']         = $request->input('PostalCode');
      $arr_ubo['Country']            = $request->input('Country');
      $arr_ubo['Nationality']        = $request->input('Nationality');
      $arr_ubo['Birthday']           = $request->input('Birthday');
      $arr_ubo['Birthplace']         = $request->input('Birthplace');
      $arr_ubo['Birthplace_Country'] = $request->input('Birthplace_Country');

      $AddUBO = $this->WalletService->add_UBO($arr_ubo);
      if(isset($AddUBO) && $AddUBO == 'true')
      {
        Session::flash('success','UBO created successfully.');
        return redirect('/expert/wallet/dashboard');
      }
      else
      {
        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
        return redirect('/expert/wallet/dashboard');
      }
    }

    public function submit_declaration(Request $request)
    {
      $ubo_declaration_id = '';
      $ubo_declaration_id = $request->input('ubo_declaration_id');
      if(isset($ubo_declaration_id) && $ubo_declaration_id!='')
      {
         $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)->get();
      
         if($obj_data)
         {
           $arr_data = $obj_data->toArray();
         }
         //$transaction_inp['UserId']     = isset($arr_data[0]['mp_user_id'])?$arr_data[0]['mp_user_id']:'';

        $userId = isset($arr_data[0]['mp_user_id'])?$arr_data[0]['mp_user_id']:'';
        $uboDeclarationId = $ubo_declaration_id;
        $result = $this->WalletService->submit_ubo_declaration($userId,$uboDeclarationId);

        if(isset($result) && $result == 'true')
        {
          Session::flash('success','Declaration submitted successfully.');
          return redirect('/expert/wallet/dashboard');
        }
        else
        {
          Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
          return redirect('/expert/wallet/dashboard');
        }

      }
      else
      {
        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
        return redirect('/expert/wallet/dashboard');
      }
    }
    
    public function create_wallet_using_mp_owner_id($mp_owners_id = false){
      if($mp_owners_id == false){
        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
        return redirect()->back();
      }
      $desciption = config('app.project.name')." App wallet";
      $AppWallet = $this->WalletService->create_wallet(base64_decode($mp_owners_id),$desciption);
      $arr_data['mp_wallet_id']      = isset($AppWallet->Id)?$AppWallet->Id:"";
      $arr_data['mp_wallet_created'] = "Yes";
      if($AppWallet){
        $update_mangopya_details = $this->UserModel->where('id',$this->user->id)->update($arr_data); 
        Session::flash('success',trans('common/wallet/text.text_archexpert_wallet_account_has_been_created_successfully'));
      } else {
        Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
      }
      return redirect()->back();
    }  


    public function cashout_request(Request $request,$bank_id)
    { 
      
      $mangopay_wallet_details = get_user_all_wallet_details();
      if (isset($mangopay_wallet_details) && count($mangopay_wallet_details)>0)
      {
        foreach ($mangopay_wallet_details as $key => $value) 
        {     
           if(isset($value->Balance->Amount) && $value->Balance->Amount > 0)
           {
             $arr_data['balance'][]  = isset($value->Balance->Amount)?$value->Balance->Amount/100:'0';
             $arr_data['currency'][] = isset($value->Balance->Currency)?$value->Balance->Currency:'';
           }
        }
      }
      //$this->arr_view_data['arr_country'] = $arr_country;
      $this->arr_view_data['arr_data'] = $arr_data;
      $this->arr_view_data['bank_id']  = $bank_id;

      return view($this->view_folder_path.'.payout',$this->arr_view_data);

        
    }


    public function make_cashout_in_bank(Request $request)
    {
      //dd($request->all());
        $validator = Validator::make($request->all(),[
          'act_cashout_amount' => 'required',
          'bankId'             => 'required',
          'currency_code'      => 'required',
          'cashout_amount'     => 'required'
        ]);
        $currency_code = $request->input('currency_code');
        if($validator->fails()){
          Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
          return redirect('/expert/wallet/dashboard');
        }
        $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)
                                          ->where('currency_code',$currency_code)
                                          ->first();
      
        if($obj_data)
        {
           $arr_data = $obj_data->toArray();
        }
        $mp_user_id     = isset($arr_data['mp_user_id'])?$arr_data['mp_user_id']:'';

        $act_cashout_amount = $request->input('cashout_amount');
        $walletId           = isset($arr_data['mp_wallet_id'])?$arr_data['mp_wallet_id']:'';
        $bankId             = $request->input('bankId');
        $tag                = $request->input('tag',null);
        //  check wallet balance 
        $get_mangopay_wallet_details = $this->WalletService->get_wallet_details($walletId);
        if(isset($get_mangopay_wallet_details->Balance->Amount) && $get_mangopay_wallet_details->Balance->Amount !=""){
            $wallet_balance = $get_mangopay_wallet_details->Balance->Amount/100;
            if($act_cashout_amount > $wallet_balance) {
              Session::flash('error',trans('controller_translations.your_wallet_amount_is_not_suffeciant_to_make_transaction'));
              return redirect('/expert/wallet/dashboard');
            } else {
              $transaction_inp                      = [];
              $transaction_inp['UserId']            = $mp_user_id;
              $transaction_inp['bankId']            = $bankId;
              $transaction_inp['tag']               = $tag;
              $transaction_inp['commission_amount'] = '0';
              $transaction_inp['walletId']          = $walletId;
              $transaction_inp['total_pay']         = $act_cashout_amount;
              //$transaction_inp['total_pay']         = $act_cashout_amount;
              $payOutBankWire  = $this->WalletService->payOutBankWire($transaction_inp);
              if(isset($payOutBankWire->Id) && $payOutBankWire->Id != ""){
                Session::flash('success', trans('common/wallet/text.text_your_cashout_has_been_successfully_done_You_can_check_your_transaction_status_using')." ( ".$payOutBankWire->Id." ) ".trans('common/wallet/text.text_transaction_id'));
                return redirect('/expert/wallet/dashboard');
              } else {
                Session::flash('error',$payOutBankWire);
                return redirect('/expert/wallet/dashboard');
              }
            }
        } else {
          Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
          return redirect('/expert/wallet/dashboard');
        }
    }

    public function upload_kyc_docs(Request $request)
    {
        //dd('hereeee');
        $arr_data = [];
        $transaction_inp = [];
        $validator = Validator::make($request->all(),[
            'user_type' => 'required',
            'id_proof'   => 'required',
            'reg_proof' => 'required'
        ]);
        if($validator->fails()){
            Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
            return redirect('/expert/wallet/dashboard');
        }

        
        $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)->get();
      
        if($obj_data)
        {
           $arr_data = $obj_data->toArray();
        }
        // dd($arr_data);
        $mp_user_id     = isset($arr_data[0]['mp_user_id'])?$arr_data[0]['mp_user_id']:'';
        if(trim($mp_user_id) == ''){
            Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
            return redirect('/expert/wallet/dashboard');
        }

        $transaction_inp['mp_user_id']       = $mp_user_id;
        $transaction_inp['KycDocumentType']  = $request->input('user_type');
        $transaction_inp['KycDocument']      = base64_encode(file_get_contents($request->file('id_proof')));
        $transaction_inp['KycDocumentPath']  = $request->file('id_proof')->getrealPath();
        $transaction_inp['KycDocumentName']  = $request->file('id_proof')->getClientOriginalName();

        $mp_upload_kyc_doc = $this->WalletService->submit_kyc_docs($transaction_inp);

        //dd($mp_upload_kyc_doc);
        $transaction['mp_user_id']       = $mp_user_id;
        $transaction['KycDocumentType']  = $request->input('user_type');
        $transaction['KycDocument']      = base64_encode(file_get_contents($request->file('reg_proof')));
        $transaction['KycDocumentPath']  = $request->file('reg_proof')->getrealPath();
        $transaction['KycDocumentName']  = $request->file('reg_proof')->getClientOriginalName();

        $mp_upload_kyc_doc1 = $this->WalletService->submit_kyc_docs($transaction);

        if(isset($mp_upload_kyc_doc) && $mp_upload_kyc_doc == true){
            Session::flash('success',trans('common/wallet/text.text_your_kyc_details_submited_successfully'));
            return redirect('/expert/wallet/dashboard');
        } else {
            Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
            return redirect('/expert/wallet/dashboard');
        }
    }

    public function get_note(Request $request)
    {
      $arr_data  = [];
      $obj_data  = $this->DocumentsMasterModel->where('country',$request->input('country'))
                                 ->where('user_type',$request->input('user_type'))
                                 ->first();
      if($obj_data)
      {
        $arr_data = $obj_data->toArray();
      }
      else
      {
        $obj_data_new  = $this->DocumentsMasterModel->where('country',0)
                                 ->where('user_type',$request->input('user_type'))
                                 ->first();
        if($obj_data_new)
        {
          $arr_data = $obj_data_new->toArray();
        }                
      }
       return $arr_data;                           
    }

    public function update_user_details(Request $request)
    {
       $validator = Validator::make($request->all(),[
          'nationality' => 'required',
          'residence'   => 'required',
          'user_type'   => 'required'
        ]);
        if($validator->fails()){
          Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
          return redirect('/expert/wallet/dashboard');
        }
        $nationality = $request->input('nationality');
        $residence = $request->input('residence');
        $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)->get();
      
        if($obj_data)
        {
           $arr_data = $obj_data->toArray();
        }
        $mp_user_id     = isset($arr_data[0]['mp_user_id'])?$arr_data[0]['mp_user_id']:'';
        $user_details['mp_user_id']  = $mp_user_id;
        $user_details['nationality'] = $nationality;
        $user_details['residence']   = $residence;
        $user_type =  $request->input('user_type');
        if(isset($user_type) && $user_type!='' && $user_type == 'BUSINESS')
        {
          $upadte_user_details         = $this->WalletService->update_legal_user($user_details);
        }
        else
        {
          $upadte_user_details         = $this->WalletService->update_user($user_details);
        }

        if(isset($upadte_user_details) && $upadte_user_details == true)
        {
          Session::flash('success','User details updated successfully.');
          return redirect('/expert/wallet/dashboard');
        } 
        else 
        {
          Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
          return redirect('/expert/wallet/dashboard');
        }
    }
} // end class