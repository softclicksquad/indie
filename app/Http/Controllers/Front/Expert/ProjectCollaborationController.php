<?php
namespace App\Http\Controllers\Front\Expert;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\UserModel;
use App\Models\ClientsModel;
use App\Models\ProjectpostModel;
use App\Models\NotificationsModel;
use App\Models\SiteSettingModel;
use App\Models\ProjectAttchmentModel;
use App\Common\Services\MailService;
use App\Models\ExpertsModel;
/*Used helper method to fire event*/
use Sentinel;
use Validator;
use Session;
use Mail;
use App;
use Lang;

class ProjectCollaborationController extends Controller
{
    public $arr_view_data;
    public function __construct(ProjectpostModel $projectpost,
                                UserModel $user_model,
                                ClientsModel $clients,
                                NotificationsModel $notifications,
                                SiteSettingModel $site_settings,
                                ProjectAttchmentModel $project_attachment,
                                ExpertsModel $experts,
                                MailService $mail_service
                                )
    {
      if(! $user = Sentinel::check()) {
        return redirect('/login');
      }
      $this->user_id                               = $user->id;
      $this->UserModel                             = $user_model;
      $this->ClientsModel                          = $clients;
      $this->ProjectpostModel                      = $projectpost;
      $this->NotificationsModel                    = $notifications;
      $this->SiteSettingModel                      = $site_settings;
      $this->ProjectAttchmentModel                 = $project_attachment;
      $this->MailService                           = $mail_service;
      $this->ExpertsModel                          = $experts;


      $this->arr_view_data                         = [];
      $this->module_url_path                       = url("/expert/projects");
      $this->login_url                             = url("/login");
      $this->profile_img_base_path                 = base_path() . '/public'.config('app.project.img_path.profile_image');
      $this->project_attachment_public_path        = url('/').config('app.project.img_path.project_attachment');
      $this->expert_project_attachment_base_path    = base_path() . '/public'.config('app.project.img_path.expert_project_attachment');
      $this->expert_project_attachment_public_path  = url('/').config('app.project.img_path.expert_project_attachment');
    }

    public function index($project_id=FALSE) {
      $this->arr_view_data['page_title']  = trans('controller_translations.page_title_project_collaboration');
      
      if($project_id == "" || $project_id == FALSE){
        Session::flash('error', trans('controller_translations.something_went_wrong_please_try_again_later')); 
        return redirect()->back();
      }

      $project_main_attachment        = [];
      $project_attachment             = [];
      $arr_pagination                 = [];
      $project_id                     = base64_decode($project_id); 
      $obj_project_main_attachment    = $this->ProjectpostModel
                                             ->where('id',$project_id)
                                             ->first(['id','project_name','project_status','project_attachment']);
      if(isset($obj_project_main_attachment) && $obj_project_main_attachment != null){
        $project_main_attachment = $obj_project_main_attachment->toArray(); 
      }


      // project attachments 
      $obj_project_attachment   = $this->ProjectAttchmentModel
                                       ->where('project_id',$project_id)
                                       ->orderBy('created_at','DESC') 
                                       ->paginate(12);  
      if($obj_project_attachment) {
        $arr_pagination         = clone $obj_project_attachment;
        $project_attachment     = $obj_project_attachment->toArray();
      }
      // end project attachments 
      $this->arr_view_data['project_main_attachment'] = $project_main_attachment;
      $this->arr_view_data['project_id']              = $project_id;
      $this->arr_view_data['project_attachment']      = $project_attachment;
      $this->arr_view_data['arr_pagination']          = $arr_pagination;
      $this->arr_view_data['module_url_path']         = $this->module_url_path;
      return view('expert.projects.collaboration.index',$this->arr_view_data);
    }
    public function details($collaboration_id=FALSE,$project_id=FALSE) {
      $this->arr_view_data['page_title']  = trans('controller_translations.page_title_project_collaboration_details');
      if($collaboration_id == "" || $collaboration_id == FALSE){
        Session::flash('error', trans('controller_translations.something_went_wrong_please_try_again_later')); 
        return redirect()->back();
      }
      $project_attachment         = [];
      $previous                   = 0;
      $next                       = 0;
      $collaboration_id           = base64_decode($collaboration_id); 
      $project_id                 = base64_decode($project_id); 

      // project attachments 
        $obj_project_attachment  = $this->ProjectAttchmentModel->with(['project_details']) 
                                        ->where('id',$collaboration_id)
                                        ->where('project_id',$project_id)
                                        ->orderBy('created_at','DESC') 
                                        ->first();  
        if($obj_project_attachment)  {
          $project_attachment   = $obj_project_attachment->toArray();
        }
      // end project attachments 

      // get previous 
        $obj_get_previous   = $obj_project_attachment->where('id', '<', $collaboration_id)->where('project_id', $project_id)->max('id');
        if($obj_get_previous)  {
          $previous         = $obj_get_previous;
        }
      // end get previous 
      // get next 
        $obj_get_next   = $obj_project_attachment->where('id', '>', $collaboration_id)->where('project_id', $project_id)->min('id');
        if($obj_get_next)  {
          $next         = $obj_get_next;
        }
      // end get next   

      $this->arr_view_data['collaboration_id']        = $collaboration_id;
      $this->arr_view_data['project_attachment']      = $project_attachment;
      $this->arr_view_data['previous']                = $previous;
      $this->arr_view_data['next']                    = $next;
      $this->arr_view_data['module_url_path']         = $this->module_url_path;
      $this->arr_view_data['project_id']              = isset($project_attachment['project_id'])?$project_attachment['project_id']:'0';
      $this->arr_view_data['profile_img_base_path']   = $this->profile_img_base_path;
      return view('expert.projects.collaboration.detail',$this->arr_view_data);
    }
    /*Send entry  : contest 
    Author: Tushar Ahire*/
    public function upload_collaboration_files(Request $request)
    {
      $arr_json                    = [];
      $arr_data                    = [];
      $arr_data['colla_file_desc'] = 'required';
      $arr_data['wrk-done-by-me']  = 'required';
      
      $validator = Validator::make($request->all(),$arr_data);
      if($validator->fails()){
        if(!empty($validator->errors()->first()) && $validator->errors()->first() !=""){
          $arr_json['status']       = 'error';
          $arr_json['message']      = $validator->errors()->first(); 
        } else {
          $arr_json['status']       = 'error';
          $arr_json['message']      = trans('contets_listing/listing.text_error_occure_while_send_entry');
        }  
        return response()->json($arr_json);     
      } 

      $user_id                     = $this->user_id;
      $project_id                  = base64_decode($request->input('project_id'));
      if($request->input('colla_file_desc') != null) { $description = $request->input('colla_file_desc'); } else { $description = ''; }
      // create unique indentifier
          $res  = \DB::table('project_attachment')->select('attchment_no')->where('attchment_no' ,'!=' , 0)->where('attchment_no' ,'!=' , '')->orderBy('id' , 'DESC')->LIMIT('1')->get();
          $res = $res;
          if(count($res) > 0){                 
              $max_entry_id = $res[0]->attchment_no;
              $max_entry_id = $max_entry_id + 1;                        
          } else {
              $max_entry_id = '1';  
          }
      // create unique indentifier  
     
          $arr_images           = $request->file('file',"");
          if(isset($arr_images) && sizeof($arr_images)>0)
          {
            $file_no = $max_entry_id;
            foreach($arr_images as $key=>$file) 
            {
                // Image compress //
                $fileExtension   = strtolower($file->getClientOriginalExtension()); 
                $imageName       = $file->getClientOriginalName();//sha1(uniqid().$file.uniqid()).'.'.$fileExtension;
                if($fileExtension == 'doc'  ||
                   $fileExtension == 'docx' ||
                   $fileExtension == 'odt'  ||
                   $fileExtension == 'pdf'  ||
                   $fileExtension == 'txt'  ||
                   $fileExtension == 'xlsx'  
                )
                {
                  $file->move(base_path() . '/public/uploads/front/project_attachments/', $imageName);
                } else{
                   $destinationPath = '/public/uploads/front/project_attachments/';
                        $img = \Image::make($file->getRealPath());
                        $img->resize(651, 366, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save(base_path() . '/public/uploads/front/project_attachments/'.$imageName);
                    // end Image compress //    
                    if(isset($file)){
                      // add watermark //  
                        try{
                          $log_user_details = get_user_details();
                          $first_name       = '';
                          $last_name        = '';
                          if(isset($log_user_details['first_name'])){ $first_name = $log_user_details['first_name'];}
                          if(isset($log_user_details['last_name'])){ $last_name = str_limit($log_user_details['last_name'],1);}
                          add_watermark('uploads/front/project_attachments/',$imageName,$first_name.' '.$last_name);
                        }
                        catch(\Exeption $e){
                        }
                      //end add watermark //   
                    }
                }  
                $colla_entry_image                = $imageName; 
                $arr_images                       = [];
                $arr_images['expert_user_id']     = $user_id;   
                $arr_images['attachment']         = $colla_entry_image; 
                $arr_images['attchment_no']       = $file_no; 
                $arr_images['project_id']         = $project_id; 
                $arr_images['description']        = $description; 
                $arr_images['description']        = $description; 
                $arr_images['is_own_work']        = $request->input('wrk-done-by-me',null); 
                $this->ProjectAttchmentModel->create($arr_images);
                $file_no++;
            }  

            $client_deatils = $this->ProjectpostModel->with(['client_details','client_info'])->where('id',$project_id)->first();
            if($client_deatils)
            {
              $arr_client_details = $client_deatils->toArray();
            }

            $posted_project_name = isset($client_deatils->project_name)?$client_deatils->project_name:'';

            /* send notification to client */
            $arr_noti_data                         =  [];
            $arr_noti_data['user_id']              =  isset($arr_client_details['client_user_id'])?$arr_client_details['client_user_id']:'';
            $arr_noti_data['user_type']            = '2';
            $arr_noti_data['url']                  = 'client/projects/details/'.base64_encode($project_id);
            $arr_noti_data['project_id']           = '';
            $arr_noti_data['notification_text_en'] = $posted_project_name . ' - ' . Lang::get('controller_translations.text_contest_entry_upload',[],'en','en');
            $arr_noti_data['notification_text_de'] = $posted_project_name . ' - ' . Lang::get('controller_translations.text_contest_entry_upload',[],'de','en');
            $this->NotificationsModel->create($arr_noti_data);
            /* send notification to client */


            $data['user_id']           =  isset($arr_client_details['client_user_id'])?$arr_client_details['client_user_id']:'';
            $data['name']              =  isset($arr_client_details['client_info']['client_user_details']['first_name'])?$arr_client_details['client_info']['client_user_details']['first_name']:'';
            $data['email_id'] = $email_id = isset($arr_client_details['client_details']['email'])?$arr_client_details['client_details']['email']:'';
            $data['project_name'] = isset($arr_client_details['project_name'])?$arr_client_details['project_name']:'';

            $project_name = config('app.project.name');

            $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
            try
            {
                $mail_status = $this->MailService->send_file_upload_project_email($data);
                // Mail::send('front.email.file_upload_project', $data, function ($message) use ($email_id,$mail_form,$project_name) {
                //           $message->from($mail_form, $project_name);
                //           $message->subject($project_name.':File Upload');
                //           $message->to($email_id);
                // });
            }
            catch(\Exception $e)
            {
              Session::Flash('error',trans('controller_translations.text_mail_not_sent'));
            }



            $arr_json['status']       = 'success';
            $arr_json['message']      = trans('contets_listing/listing.text_your_entry_has_been_send_successfully'); 
          }
          else
          {
            $arr_json['status']  = 'error';
            $arr_json['message'] = trans('contets_listing/listing.text_error_occure_while_send_entry');      
          }  
      return response()->json($arr_json);
    }
} // end class
