<?php

namespace App\Http\Controllers\Front\Expert;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\UserModel;
use App\Models\ExpertsModel;

use App\Models\SkillsModel;
use App\Models\CategoriesModel;
use App\Models\ExpertsSkillModel;
use App\Models\ExpertsCategoriesModel;
use App\Models\SubscriptionUsersModel;
use App\Models\ProjectsBidsModel;
use App\Models\ProjectpostModel;
use App\Models\ProjectSkillsModel;
use App\Models\ProjectsBidsTopupModel;
use App\Models\NotificationsModel;
use App\Models\ProjectAttchmentModel;
use App\Models\UserWalletModel;
use App\Models\TransactionsModel;
use App\Models\CurrencyModel;
use App\Common\Services\WalletService;
use App\Common\Services\MailService;

use App\Common\Services\LanguageService;
use App\Common\Services\SubscriptionService;

use Validator;
use Session;
use Sentinel;
use Mail;
use Lang;
use PDF;
class ProjectBidsController extends Controller
{
    public $arr_view_data;

    public function __construct(
                                  UserModel $user_model,
                                  ExpertsModel $expert,
                                  SkillsModel $skills,
                                  LanguageService $langauge,
                                  ExpertsSkillModel $expert_skills,
                                  ExpertsCategoriesModel $expert_categories,
                                  CategoriesModel $category,
                                  SubscriptionUsersModel $user_subscription,
                                  ProjectsBidsModel $project_bids,
                                  ProjectpostModel $project_post,
                                  ProjectSkillsModel $project_skills,
                                  SubscriptionService $subscription,
                                  ProjectsBidsTopupModel $bid_topup,
                                  NotificationsModel $notifications,
                                  ProjectAttchmentModel $bid_attachment_model,
                                  WalletService $WalletService,
                                  MailService $MailService
                                )
    { 

      if(! $user = Sentinel::check()) 
      {
        return redirect('/login');
      }

      $this->user                   = $user;
      $this->user_id                = $user->id;
      $this->UserModel              = $user_model;
      $this->ExpertsModel           = $expert;
      $this->SkillsModel            = $skills;
      $this->CategoriesModel        = $category;
      $this->LanguageService        = $langauge;
      $this->ExpertsSkillModel      = $expert_skills;
      $this->ExpertsCategoriesModel = $expert_categories;
      $this->SubscriptionUsersModel = $user_subscription;
      $this->ProjectsBidsModel      = $project_bids;
      $this->ProjectpostModel       = $project_post;
      $this->ProjectSkillsModel     = $project_skills;
      $this->SubscriptionService    = $subscription;
      $this->ProjectsBidsTopupModel = $bid_topup;
      $this->NotificationsModel     = $notifications;
      $this->ProjectAttchmentModel  = $bid_attachment_model;
      $this->WalletService          = $WalletService;
      $this->MailService            = $MailService;
      $this->UserWalletModel        = new UserWalletModel;
      $this->TransactionsModel      = new TransactionsModel;
      $this->CurrencyModel          = new CurrencyModel;
      $this->arr_view_data   = [];
      $this->module_url_path = url("/expert/bids");

      $this->bid_attachment_base_path    = base_path() . '/public'.config('app.project.img_path.bid_attachment');
      $this->bid_attachment_public_path  = url('/public').config('app.project.img_path.bid_attachment');
      if(isset($user)){
        $logged_user                 = $user->toArray();  
        if($logged_user['mp_wallet_created'] == 'Yes'){
          $this->mp_user_id          = $logged_user['mp_user_id'];
          $this->mp_wallet_id        = $logged_user['mp_wallet_id'];
          $this->mp_wallet_created   = $logged_user['mp_wallet_created'];
        } else {
          $this->mp_user_id          = '';
          $this->mp_wallet_id        = '';
          $this->mp_wallet_created   = 'No';
        }
      } else {
        $this->mp_user_id          = '';
        $this->mp_wallet_id        = '';
        $this->mp_wallet_created   = 'No';
      }
    }

    /*
    | description : show project deatils and bid form to make bid on project
    | auther :Sagar Sainkar
    */
    public function make_bid($project_id)
    {
      $obj_expert = $this->ExpertsModel->where('user_id',$this->user_id)->with(['user_details','country_details.states','state_details.cities','city_details','expert_skills','expert_categories'])->first();
      $arr_expert_details = array();
      if($obj_expert != FALSE){
        $arr_expert_details = $obj_expert->toArray();
      }
      return view('expert.profile.index',$this->arr_view_data);     
    }
    /*
    | Comments    : Show project bid add page.
    | auther      : Nayan S.
    */

    public function show_add_project_bid($enc_id)
    { 
      $check = false;
      $user_id = $this->user_id;
      if($user_id>0)
      {
        $check = check_default_currency_set($user_id);
      }
      if($check==true)
      {
        
        $obj_data = $this->UserModel->where('id',$user_id)->first();
        $is_wallet_created = isset($obj_data->mp_wallet_created)?$obj_data->mp_wallet_created:'';
        if($is_wallet_created!='Yes')
        {
                $obj_currency = $this->CurrencyModel->select('id','currency_code')->get();
                if($obj_currency)
                {
                  $arr_currency = $obj_currency->toArray();
                }

                $obj_expert_deatils = $this->ExpertsModel->where('user_id',$this->user_id)->first();
          
                $user_type = isset($obj_expert_deatils->user_type)?$obj_expert_deatils->user_type:'';
                $company_name = isset($obj_expert_deatils->company_name)?$obj_expert_deatils->company_name:'';

                $first_name   = "-";
                $last_name    = "-";
                $email        = "-";
                $logged_user  = $this->user->toArray();
                if(isset($logged_user['role_info']['first_name']) && $logged_user['role_info']['first_name'] !="")
                {
                  $first_name = $logged_user['role_info']['first_name'];
                }
                if(isset($logged_user['role_info']['last_name']) && $logged_user['role_info']['last_name'] !="")
                {
                  $last_name = $logged_user['role_info']['last_name'];
                }
                if(isset($logged_user['email']) && $logged_user['email'] !=""){
                  $email = $logged_user['email'];
                }
                try
                {
                  $timezoneinfo = file_get_contents('http://ip-api.com/json/' . $_SERVER['REMOTE_ADDR']);
                }catch(\Exception $e)
                {
                  $timezoneinfo = file_get_contents('http://ip-api.com/json');  
                } 
                $mp_user_data = [];
                //Create account in mangopay and create wallet Start.......
                  $mp_user_data['FirstName']          = $first_name;
                  $mp_user_data['LastName']           = $last_name;
                  $mp_user_data['Email']              = $email;
                  $mp_user_data['CountryOfResidence'] = isset($timezoneinfo->countryCode)?$timezoneinfo->countryCode:"IN";
                  $mp_user_data['Nationality']        = isset($timezoneinfo->countryCode)?$timezoneinfo->countryCode:"IN";
                  $Mangopay_create_user =$this->WalletService->create_user_with_all_currency_wallet($mp_user_data,$arr_currency,$this->user->id);
          // Session::flash('error', 'Please create your wallet first');  
          // return redirect(url('expert/wallet/dashboard'));
        }
        $this->arr_view_data['page_title'] = trans('controller_translations.page_title_add_project_bid');
        $project_id = base64_decode($enc_id);
        /* CHecking that the subscription for the bid and skills */
        $subscription_result = $this->SubscriptionService->validate_subscription($project_id);
        if(count($subscription_result) > 0)
        { 
          Session::flash('error',$subscription_result['msg']);
          return back();
        }
        if($project_id=="")
        {
          return redirect()->back();
        }
        $this->arr_view_data['arr_project_info']    = $this->get_project_details($project_id);   /* getting details of the project*/
        $this->arr_view_data['module_url_path']     = $this->module_url_path;
        return view('expert.projects.add_project_bid',$this->arr_view_data);
      }
      Session::flash('error','Set default currency first');
      return redirect('/expert/profile')->send();
    }

    /*
    | Comments    : To get all project details.
    | auther      : Nayan S.
    */
    public function get_project_details($id)
    { 
      $obj_project_info = $this->ProjectpostModel
                               ->with('project_post_documents','skill_details','category_details','sub_category_details','client_details','project_skills.skill_data','project_bid_info','project_bids_infos')
                               ->where('id',$id)
                               ->first();

      $arr_project_info = array();
      if($obj_project_info != FALSE){
        $arr_project_info = $obj_project_info->toArray();
      }
      return $arr_project_info;
    }

    /*
    | Comments    : Store project bid details.
    | auther      : Nayan S.
    */
    public function store_bid_details(Request $request)
    {
      $arr_rules                      = array();
      $arr_rules['project_id']        = "required";
      $arr_rules['bid_cost']          = "required";
      $arr_rules['expected_duration'] = "required";
      $arr_rules['proposal_details']  = "required";
      
      $validator = Validator::make($request->all(),$arr_rules);
      if($validator->fails()){
          return redirect()->back()->withErrors($validator)->withInput();
      }
      
      $form_data = [];
      $form_data = $request->all(); 
      $user      = Sentinel::check();

      if(!$user->inRole('expert')){
         return redirect()->back()->withInput($form_data);
      }
      if($form_data['project_id'] == "" || (base64_decode($form_data['project_id']) == "" ) ){
        return redirect()->back()->withInput();
      }
      $arr_bid_data = [];
      $arr_bid_data['project_id']             = base64_decode($form_data['project_id']);
      $arr_bid_data['expert_user_id']         = $this->user_id;
      $arr_bid_data['bid_cost']               = $form_data['bid_cost'];
      $arr_bid_data['bid_estimated_duration'] = $form_data['expected_duration'];
      $arr_bid_data['bid_description']        = $form_data['proposal_details'];

      if($request->has('hour_per_week') && $request->has('hour_per_week')!='')
      {
        $arr_bid_data['hour_per_week'] = $form_data['hour_per_week'];
        if($form_data['hour_per_week'] == 'HOUR_CUSTOM_RATE')
        {
          $arr_bid_data['hour_per_week'] = $form_data['hour_custom_rate'];
          $arr_bid_data['is_hour_custom_rate'] = '1';
        }
      }

      $obj_is_project_cancled = $this->ProjectpostModel->where('project_status','5')
                                                    ->where('id',$arr_bid_data['project_id'])
                                                    ->first();
      $is_project_cancled     = isset($obj_is_project_cancled)? $obj_is_project_cancled :'';
      if($is_project_cancled && $is_project_cancled!='')
      {
        Session::flash('error', trans('controller_translations.text_sorry_This_project_is_canceled'));  
        return redirect()->back();
      }
      $obj_bid_closing_date = $this->ProjectpostModel->select('bid_closing_date')
                                                         ->where('id',$arr_bid_data['project_id'])
                                                         ->first();
      $bid_closing_date = isset($obj_bid_closing_date['bid_closing_date'])?strtotime($obj_bid_closing_date['bid_closing_date']):0;
      $currentDateTime  = strtotime(date('Y-m-d H:i:s')); //in seconds
      $date_differnce   = $bid_closing_date - $currentDateTime;

      if($date_differnce<=0){
        $date_differnce = 0;
      }

      if($date_differnce==0){
        Session::flash("error",trans('controller_translations.text_sorry_this_project_is_expired'));
        return redirect()->back();
      }

      // if(isset($form_data['nda-agrrement-proposal']) && $form_data['nda-agrrement-proposal']=='on')
      // {
      //   $nda_aggrement_id = $this->_generate_nda_aggrement_id();
      //   // generate aggrement PDF
      //   $page_title = 'NDA Aggrement';
      //   PDF::SetTitle('NDA Aggrement');
      //   PDF::AddPage();
      //   PDF::writeHTML(view('expert/projects/nda_agrrements/agreement-pdf'));
      //   //ob_end_clean();
      //   $filename = $nda_aggrement_id.'.pdf';
      //   //PDF::Output($filename.'.pdf','I'); // open
      //   PDF::Output(public_path().'/uploads/front/nda-agreement/'.$filename, 'F'); // save
      //   // end generate aggrement PDF
      //   $arr_bid_data['nda_aggreement_accept']  = 'YES';
      //   $arr_bid_data['nda_aggreement_pdf']     = $filename;
      // }
      //dd('hereeee');
      $is_bid_exists = $this->ProjectsBidsModel->where(['expert_user_id'=>$this->user_id , 'project_id'=> $arr_bid_data['project_id'] ])->count();
      if($is_bid_exists > 0){
          Session::flash('error',trans('controller_translations.error_you_have_already_added_a_bid_for_this_project') );
          return back()->withInput();
      }

      // if(isset($form_data['bid_attachment']) && $form_data['bid_attachment']!=FALSE)
      // {    
      //   if($request->hasFile('bid_attachment')) 
      //   {
      //     $bid_attachment_name = $form_data['bid_attachment'];
      //     $fileExtension = strtolower($request->file('bid_attachment')->getClientOriginalExtension()); 

      //     $arr_file_types = ['png','docx','xls','xlsx','gif','png','jpeg','jpg','cad','pdf','odt','doc','txt'];

      //     if(in_array($fileExtension, $arr_file_types))
      //     {
      //         $bid_attachment_name            = sha1(uniqid().$bid_attachment_name.uniqid()).'.'.$fileExtension;
      //         $request->file('bid_attachment')->move($this->bid_attachment_base_path, $bid_attachment_name);
      //         $arr_bid_data['bid_attachment'] = $bid_attachment_name;
      //     }
      //     else
      //     {
      //         Session::flash('error',trans('controller_translations.error_invalid_file_extension_for_attachment') );
      //         return back()->withInput($form_data);
      //     }
      //   }
      // }


      if($request->hasFile('contest_attachments'))
      {                 
        $documents  = $request->file('contest_attachments');

        foreach($documents as $key =>$doc)
        {           
          if(isset($doc))
          {            
            $filename         = $doc->getClientOriginalName();
            $file_extension   = strtolower($doc->getClientOriginalExtension());
            $file_size        = $doc->getClientSize();

            $arr_file_types = ['png','docx','xls','xlsx','gif','png','jpeg','jpg','cad','pdf','odt','doc','txt'];

            if(in_array($file_extension,$arr_file_types) && $file_size <= 5000000)
            {
                $file_name       = sha1(uniqid().$doc.uniqid()).'.'.$file_extension;
                $doc->move($this->bid_attachment_base_path,$file_name);

                $arr_bid_data['bid_attachment'] = $file_name;

                // $obj = $this->ProjectPostDocumentsModel->create(['project_id'=>$insertedId,
                //                                             'image_name'=>$file_name,
                //                                             'image_original_name'=>$filename]); 
            }
            else
            {
                Session::flash('error',trans('controller_translations.error_invalid_file_extension_for_attachment') );
                return back()->withInput($form_data);
            }
          }          
        }
      }


      /* getting Project info for updating notifications table */
      $obj_project = $this->ProjectpostModel->where('id','=',base64_decode($form_data['project_id']))
                                            ->first(['id','client_user_id','project_handle_by','project_recruiter_user_id']);            
      if($obj_project)
      {
        $is_recruiter      = '0'; 
        $client_user_id          =  $obj_project->client_user_id;
        $project_id              =  $obj_project->id;
        $project_recruiter_user_id = $obj_project->project_recruiter_user_id;
        if( $obj_project->project_handle_by == "2" ) /* Project manager Handles project */
        {
          $is_recruiter = '1';
        }
      }
      else
      {
          Session::flash('error',trans('controller_translations.error_unauthorized_action') );
          return back()->withInput($form_data);
      }     

      /* creating project bid */
      $bid_result = $this->ProjectsBidsModel->create($arr_bid_data);

      if($bid_result)
      {
          $posted_project_name = isset($obj_project->project_name)?$obj_project->project_name:'';
          /* create notification */
          $arr_data =  []; 
          
          if($is_recruiter == '0')
          { 
            if(isset($client_user_id) && $client_user_id != "")
            {
              $arr_data['user_id']    =  $client_user_id;
              $arr_data['user_type']  =  '2';
              $arr_data['url']        =  'client/projects/details/'.base64_encode($project_id);
              $arr_data['project_id'] =  $project_id;
              
              /*$arr_data['notification_text'] =  trans('controller_translations.msg_new_bids_available');*/

              $arr_data['notification_text_en'] =  $posted_project_name . ' - ' . Lang::get('controller_translations.msg_new_bids_available',[],'en','en');
              $arr_data['notification_text_de'] =  $posted_project_name . ' - ' . Lang::get('controller_translations.msg_new_bids_available',[],'de','en');

              $this->send_project_bid_email($project_id, $this->user_id , $bid_result->id);
              $this->NotificationsModel->create($arr_data);
            }
          } 
          else if ($is_recruiter == '1')
          {
            if(isset($project_recruiter_user_id) && $project_recruiter_user_id != "")
            {
              $arr_data['user_id'] = $project_recruiter_user_id;
              $arr_data['user_type']  =  '4';
              $arr_data['url']        =  'recruiter/projects/open';
              $arr_data['project_id'] =  $project_id;

              $arr_data['notification_text_en'] =  $posted_project_name . ' - ' . Lang::get('controller_translations.msg_new_bids_available',[],'en','en');
              $arr_data['notification_text_de'] =  $posted_project_name . ' - ' . Lang::get('controller_translations.msg_new_bids_available',[],'de','en');
              $this->send_project_bid_email_recruiter($project_id, $this->user_id , $bid_result->id);
              $this->NotificationsModel->create($arr_data);
              
            }
          }
          /* Notification ends*/

          $obj_topup_bid = $this->ProjectsBidsTopupModel->where('is_used','0')
                                                           ->where('payment_status','1')
                                                           ->where('expert_user_id',$this->user_id)
                                                           ->first();
          if($obj_topup_bid)
          {
                                                           //->update(['is_used'=>'1']);

              $update_topup_bid = $this->ProjectsBidsTopupModel->where('is_used','0')
                                                           ->where('payment_status','1')
                                                           ->where('expert_user_id',$this->user_id)
                                                           ->where('id',$obj_topup_bid->id)
                                                           ->update(['is_used'=>'1']);
          }
          //return back()->withInput();
          Session::flash('success',trans('controller_translations.success_bid_for_that_job_was_submitted_successfuly'));
          /* To send mail function to client */
          //$this->send_project_bid_email($project_id, $this->user_id , $bid_result->id);
          return redirect('/expert/projects/applied')->send();
      }
      else
      {
          Session::flash('error',trans('controller_translations.error_error_while_appling_bid_to_project'));
          return back()->withInput(); 
      }
    }
    /*
      Auther: Sagar Sainkar
      Comment: (topup bid) this function for purchase one extar bid to make a bid on project
              show payment methods for topup bid payment

            Added all extra conditions for logical issue if user copy and past link if he have valid bid in subscription on have valid topup bid so he can not purches bid 

            user can onlu purchase bid if bid limit get complated and he don't have any unused topup bid
    */
    public function purchase_bid()
    {
      $arr_subscription = $result = [];
      $obj_subscription = NULL;
      $is_starter_plan  = 0;
      $topup_bid        = $this->ProjectsBidsTopupModel->where('is_used','0')->where('payment_status','1')
                               ->where('expert_user_id',$this->user_id)
                               ->whereHas('transaction_details',function ($query)
                                 {
                                   $query->whereIN('payment_status',['1','2']);
                                 })
                               ->count();

        if ($topup_bid==0) 
        {
            $obj_subscription_initial = $this->SubscriptionUsersModel->where('user_id',$this->user_id)->where('is_active','1')->orderBy('id','DESC')->first();
            if($obj_subscription_initial){
              $arr_tmp_subscription = $obj_subscription_initial->toArray();
              if(isset($arr_tmp_subscription['subscription_pack_id'])  && $arr_tmp_subscription['subscription_pack_id'] == '1') {
                $is_starter_plan = 1;
              }
            }
            if($is_starter_plan == 0){ 
              $obj_subscription = $this->SubscriptionUsersModel->where('is_active','1')
                                       ->where('user_id',$this->user_id)
                                       ->whereRaw('DATE_FORMAT(expiry_at,"%Y-%m-%d") >= DATE_FORMAT(NOW(),"%Y-%m-%d") AND DATE_FORMAT(created_at,"%Y-%m-%d") <= DATE_FORMAT(NOW(),"%Y-%m-%d")')
                                       ->orderBy('id','DESC')
                                       ->with(['transaction_details'])
                                       ->whereHas('transaction_details',function ($query)
                                        {
                                            $query->whereIN('payment_status',array('1','2'));
                                        })->first();
            } else {
              $obj_subscription = $this->SubscriptionUsersModel->where('is_active','1')
                                       ->where('user_id',$this->user_id)
                                       ->orderBy('id','DESC')
                                       ->with(['transaction_details'])
                                       ->whereHas('transaction_details',function ($query)
                                        {
                                            $query->whereIN('payment_status',array('1','2'));
                                        })->first();
            }
            if ($obj_subscription) 
            {
              $arr_subscription = $obj_subscription->toArray();
              //dd($arr_subscription['subscription_pack_currency']);
              if($is_starter_plan == 0)
              {
                $bids_count = $this->ProjectsBidsModel->where('expert_user_id',$this->user_id)
                                                      ->whereRaw('DATE_FORMAT(created_at,"%Y-%m-%d") BETWEEN DATE_FORMAT("'.$arr_subscription['created_at'].'","%Y-%m-%d") AND DATE_FORMAT("'.$arr_subscription['expiry_at'].'","%Y-%m-%d")')
                                                      ->count();
              }
              else 
              {
                $bids_count = $this->ProjectsBidsModel->where('expert_user_id',$this->user_id)
                                                      ->orderBy('id','DESC')
                                                      ->where('created_at','>=',$arr_subscription['created_at'])
                                                      ->count();
              }
              /* checking bid limit */
              if( $bids_count >= $arr_subscription['number_of_bids']){
                  
                  $service_charge_payin = 0;
                  $arr_bid_transaction  = [];
                  // get wallet balance //
                  $currency_code = isset($arr_subscription['subscription_pack_currency'])?
                                         $arr_subscription['subscription_pack_currency']:'USD';
                  $mangopay_wallet           = [];
                  $mangopay_wallet_details   = [];
                  $obj_user_wallet = $this->UserWalletModel->where('user_id',$this->user_id)
                                                           //->where('currency_code',$currency_code)
                                                           ->get();
                  //dd($obj_user_wallet);
                  if($obj_user_wallet)
                  {
                    $arr_data = $obj_user_wallet->toArray();
                    foreach ($arr_data as $key => $value) 
                    {
                        $get_wallet_details[] = $this->WalletService->get_wallet_details($value['mp_wallet_id']);
                    }
                    //dd($get_wallet_details);
                    if(isset($get_wallet_details) && $get_wallet_details!='false' || $get_wallet_details != false){
                      $mangopay_wallet         = $get_wallet_details;
                      $mangopay_wallet_details = $mangopay_wallet;
                    }

                    /* get currency for transaction */
                    $arr_currency = setCurrencyForTransaction();
                    $arr_bid_transaction['currency']      = isset($arr_currency['currency']) ? $arr_currency['currency'] : '$';
                    $arr_bid_transaction['currency_code'] = isset($arr_currency['currency_code']) ? $arr_currency['currency_code'] : 'USD';
                    $arr_bid_transaction['service_charge_payin'] = 0;

                    // calcualte service fee
                    $obj_currency_data = $this->CurrencyModel->with('deposite')
                                      ->where('currency_code',$arr_bid_transaction['currency_code'])
                                      ->first();
                    
                    if($obj_currency_data)
                    {
                        $min_amount = isset($obj_currency_data->deposite->min_amount)? floatval($obj_currency_data->deposite->min_amount) :0;
                        $max_amount = isset($obj_currency_data->deposite->max_amount)? floatval($obj_currency_data->deposite->max_amount) :0;
                        
                        $min_charge = isset($obj_currency_data->deposite->min_amount_charge)? floatval($obj_currency_data->deposite->min_amount_charge) :0;
                        $max_charge = isset($obj_currency_data->deposite->max_amount_charge)? floatval($obj_currency_data->deposite->max_amount_charge) :0;
                      
                        $service_charge_payin = 0;
                        if(isset($arr_subscription_details['topup_bid_price']) && floatval($arr_subscription_details['topup_bid_price'])>$min_amount) {
                          $service_charge_payin = (float)$arr_subscription_details['topup_bid_price'] * (float)$max_charge/100;
                        } else {
                          $service_charge_payin =  (float) $min_charge;
                        }
                        $arr_bid_transaction['service_charge_payin'] = $service_charge_payin;
                    }

                  }
                  else
                  {
                    Session::flash('error','Please create wallet first');
                    return redirect(url('/expert/wallet/dashboard'))->send();
                  }
                  // end get wallet balance //
                  // dd($service_charge_payin);
                  $this->arr_view_data['currency_code']            = $currency_code;
                  $this->arr_view_data['service_charge_payin']     = $service_charge_payin;
                  $this->arr_view_data['mangopay_wallet']          = $mangopay_wallet;
                  $this->arr_view_data['mangopay_wallet_details']  = $mangopay_wallet_details;
                  $this->arr_view_data['arr_subscription_details'] = $arr_subscription;
                  $this->arr_view_data['page_title']               = trans('controller_translations.page_title_topup_bid');
                  $this->arr_view_data['module_url_path']          = $this->module_url_path;
                  return view('expert.subscription.topup_bid_payment_methods',$this->arr_view_data);
              }
            }
            else
            {
              Session::flash('error',trans('controller_translations.error_you_do_not_have_a_valid_subscription_plan_please_upgrade_subscription'));    
            }
        }
        return redirect('/projects')->send();
    }

    public function purchase_bid_return(Request $request)
    {
        $form_data = $request->all();

        $transaction_inp['tag']                      = $form_data['invoice_id'].'-Top up bid fee';
        $transaction_inp['debited_UserId']           = $form_data['debited_UserId'];
        $transaction_inp['credited_UserId']          = $form_data['credited_UserId'];
        $transaction_inp['total_pay']                = $form_data['amount_int'];
        $transaction_inp['debited_walletId']         = $form_data['debited_walletId'];
        $transaction_inp['credited_walletId']        = $form_data['credited_walletId'];
        $transaction_inp['cost_website_commission']  = '0';
        $transaction_inp['currency_code']            = $form_data['currency_code']; // archexpert admin wallet id
        $pay_fees        = $this->WalletService->walletTransfer($transaction_inp);
        $project         = [];
        if(isset($pay_fees->Status) && $pay_fees->Status == 'SUCCEEDED')
        {
           // update topup details
             $update_topup = $this->ProjectsBidsTopupModel->where('invoice_id',$form_data['invoice_id'])->update(['payment_status'=>'1']);
                       // end update topup details
                       
                       // update transaction details
                         $update_transaction   = [];
                         $update_transaction['WalletTransactionId']    = isset($pay_fees->Id) ? $pay_fees->Id : '0';
                         $update_transaction['payment_status']         = 2; // paid
                         $update_transaction['response_data']          = json_encode($pay_fees); // paid
                       $updatetransaction = $this->TransactionsModel->where('invoice_id',$form_data['invoice_id'])->update($update_transaction); 
                   // end update transaction details    

                   // send notification to admin
                $arr_admin_data                         =  [];
                $arr_admin_data['user_id']              = '1';
                $arr_admin_data['user_type']            = '1';
                $arr_admin_data['url']                  = 'admin/wallet/archexpert';
                $arr_admin_data['project_id']           = '';
                $arr_admin_data['notification_text_en'] = $form_data['invoice_id'].':'.Lang::get('controller_translations.project_service_fee_paid',[],'en','en').' '.$pay_fees->Id;
                $arr_admin_data['notification_text_de'] = $form_data['invoice_id'].':'.Lang::get('controller_translations.project_service_fee_paid',[],'de','en').' '.$pay_fees->Id;
                $this->NotificationsModel->create($arr_admin_data); 
                   // end send notification to admin    

         Session::flash('payment_success_msg',trans('controller_translations.msg_payment_transaction_for_extra_bid_successfully'));
         Session::flash('payment_return_url','/projects');
           $this->MailService->TopupBidMail($form_data['invoice_id']);
           return Redirect(url('/expert/projects/applied'));
        } else {
           if(isset($pay_fees->ResultMessage)){
           Session::flash('payment_error_msg',$pay_fees->ResultMessage);  
           } else {
                     if(gettype($pay_fees) == 'string'){
                      Session::flash('payment_error_msg',$pay_fees);
                     }
           }
                     return redirect()->back();
        }
                          


    }
    /* 
    Comments : This function sends email to client when project bid is added by expert.
    Auther : Nayan S.
    */
    public function send_project_bid_email($project_id, $expert_id ,$bid_id )
    {
        $project_name = config('app.project.name');
        //$mail_form    = get_site_email_address();   /* getting email address of admin from helper functions */
        $bid_result   = $this->ProjectsBidsModel->where('id',$bid_id)->first();
        $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';

        if($bid_result) {
          $obj_project_info = $this->ProjectpostModel->where('id',$project_id)
                                                      ->with(['client_info'=> function ($query) {
                                                            $query->select('user_id','first_name','last_name','company_name','address');
                                                        }])
                                                      ->with(['client_details'=> function ($query) {
                                                            $query->select('email','id','user_name');
                                                        }])
                                                      ->first(['id','project_name','project_description','client_user_id','project_currency']);

          $data = [];

          if($obj_project_info)
          {
            $arr_project_info = $obj_project_info->toArray();
            /* get client name */
            $client_first_name = isset($arr_project_info['client_info']['first_name'])?$arr_project_info['client_info']['first_name']:'';
            $client_last_name  = isset($arr_project_info['client_info']['last_name'])?$arr_project_info['client_info']['last_name']:'';
            /*$client_name = $client_first_name.' '.$client_last_name;*/
            $client_name = $client_first_name;
            /* get Expert name */
            $obj_user    = $this->UserModel->where('id',$expert_id)
                                        ->with(['expert_details'=>function ($query) {
                                              $query->select('id','user_id','first_name','last_name');
                                          }])
                                        ->first(['id','email','user_name']);   
            $arr_user = [];                                            
            if($obj_user)
            {
              $arr_user = $obj_user->toArray();
            }                                            
            $expert_first_name           = isset($arr_user['expert_details']['first_name'])?$arr_user['expert_details']['first_name']:'';
            $expert_last_name            = isset($arr_user['expert_details']['last_name'])?substr($arr_user['expert_details']['last_name'],0,1):'';
            $expert_name                 = ucfirst($expert_first_name).' '.ucfirst($expert_last_name);
            
            $expert_username             = $expert_name;
            //$expert_username             = isset($arr_user['user_name'])?$arr_user['user_name']:'';
            $data['project_name']        = isset($arr_project_info['project_name'])?$arr_project_info['project_name']:'';
            $data['project_description'] = isset($arr_project_info['project_description'])?$arr_project_info['project_description']:'';
            $data['project_currency']    = isset($arr_project_info['project_currency'])?$arr_project_info['project_currency']:'$';


            $data['client_name']             = $client_name;
            $data['expert_name']             = $expert_name;
            $data['expert_username']         = $expert_username;
            $data['bid_cost']                = isset($bid_result->bid_cost) ? $bid_result->bid_cost:'';
            $data['bid_estimated_duration']  = isset($bid_result->bid_estimated_duration) ? $bid_result->bid_estimated_duration:'';
            $data['bid_description']         = isset($bid_result->bid_description) ? $bid_result->bid_description:'';
            $data['login_url']               = url('/redirection?redirect=PROJECT&id='.base64_encode($project_id));
            $data['email_id']               = isset($arr_project_info['client_details']['email'])? 
                                                    $arr_project_info['client_details']['email']:'';

          $email_to = isset($arr_project_info['client_details']['email'])? $arr_project_info['client_details']['email']:'';
          if($email_to!= "")
          {
            try{
                 $mail_status = $this->MailService->send_new_project_bid_information_mail_to_client_email($data);
              // Mail::send('front.email.new_project_bid_information_mail_to_client', $data , function ($message) use ($email_to,$mail_form,$project_name) {
              //     $message->from($mail_form, $project_name);
              //     $message->subject($project_name.': New Bid On Project.');
              //     $message->to($email_to);
              // });
            }
            catch(\Exception $e){
            //Session::Flash('error'   , trans('controller_translations.text_mail_not_sent'));
            }
          } 
        }
      }
    }
    /* 
    Comments : This function sends email to manger when project bid is added by expert.
    Auther : Ankit Aher
    */
    public function send_project_bid_email_recruiter($project_id, $expert_id ,$bid_id)
    { 
        $project_name = config('app.project.name');
        //$mail_form    = get_site_email_address();   /* getting email address of admin from helper functions */
        $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
        $bid_result   = $this->ProjectsBidsModel->where('id',$bid_id)->first();
        if($bid_result)
        {
          $obj_project_info = $this->ProjectpostModel->where('id',$project_id)
                                                      ->with('project_recruiter_details')
                                                       ->with(['project_recruiter_info'=> function ($query) {
                                                                  $query->select('user_id','first_name','last_name');
                                                                }])
                                                      ->first(['id','project_name','project_description','client_user_id','project_currency','project_manager_user_id']);
          $data = [];
          if($obj_project_info) 
          {
            $arr_project_info = $obj_project_info->toArray();

            /* get client name */ 
            $manager_first_name = isset($arr_project_info['project_recruiter_info']['first_name'])?$arr_project_info['project_recruiter_info']['first_name']:'';
            $manager_last_name  = isset($arr_project_info['project_recruiter_info']['last_name'])?$arr_project_info['project_recruiter_info']['last_name']:'';

            /*$client_name = $client_first_name.' '.$client_last_name;*/
            $pm_name = $manager_first_name;
            
            /* get Expert name */
            $obj_user = $this->UserModel->where('id',$expert_id)
                                        ->with(['expert_details'=>function ($query) {
                                              $query->select('id','user_id','first_name','last_name');
                                          }])
                                        ->first(['id','email','user_name']);   
            $arr_user = [];                                            
            if($obj_user)
            {
              $arr_user = $obj_user->toArray();
            }      

            $pm_user_name  = isset($arr_project_info['project_recruiter_details']['user_name'])?$arr_project_info['project_recruiter_details']['user_name']:'';

            $expert_first_name = isset($arr_user['expert_details']['first_name'])?$arr_user['expert_details']['first_name']:'';
            $expert_last_name  = isset($arr_user['expert_details']['last_name'])?substr($arr_user['expert_details']['last_name'],0,1) :'';
            $expert_name = ucfirst($expert_first_name).' '.ucfirst($expert_last_name);

            $expert_username  = $expert_name;
            //$expert_username  = isset($arr_user['user_name'])?$arr_user['user_name']:'';
            

            $data['project_name']        = isset($arr_project_info['project_name'])?$arr_project_info['project_name']:'';
            $data['project_description'] = isset($arr_project_info['project_description'])?$arr_project_info['project_description']:'';

            $data['project_currency'] = isset($arr_project_info['project_currency'])?$arr_project_info['project_currency']:'$';


            //$data['client_name']         = $client_name;
            $data['project_manager_name']= $pm_name;
            $data['expert_name']         = $expert_name;
            $data['expert_username']     = $expert_username;

            $data['bid_cost']                = isset($bid_result->bid_cost) ? $bid_result->bid_cost:'';
            $data['bid_estimated_duration']  = isset($bid_result->bid_estimated_duration) ? $bid_result->bid_estimated_duration:'';
            $data['bid_description']         = isset($bid_result->bid_description) ? $bid_result->bid_description:'';
            $data['login_url']               = url('/redirection?redirect=PROJECT&id='.base64_encode($project_id));
            $data['email_id']                = isset($arr_project_info['project_recruiter_details']['email'])?
                                                     $arr_project_info['project_recruiter_details']['email']:'';
                                                     

            $email_to = isset($arr_project_info['project_recruiter_details']['email'])?$arr_project_info['project_recruiter_details']['email']:'';
            
            if($email_to!= "")
            {
              try{
                  $mail_status = $this->MailService->send_new_project_bid_information_mail_to_project_manager_email($data);
                // Mail::send('front.email.new_project_bid_information_mail_to_project_manager', $data , function ($message) use ($email_to,$mail_form,$project_name) {
                //     $message->from($mail_form, $project_name);
                //     $message->subject($project_name.': New Bid On Project.');
                //     $message->to($email_to);
                // });
              }
              catch(\Exception $e){
                //Session::Flash('error'   ,trans('controller_translations.text_mail_not_sent'));
              }
            } 
          }
        }
    }




    private function _generate_nda_aggrement_id()
    {
      $secure      = TRUE;    
        $bytes       = openssl_random_pseudo_bytes(3, $secure);
        $order_token = 'NDA'.date('Ymd').strtoupper(bin2hex($bytes));
        return $order_token;
    }
} // end class
