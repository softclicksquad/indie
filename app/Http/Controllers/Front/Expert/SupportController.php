<?php
namespace App\Http\Controllers\Front\Expert;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\UserModel;
use App\Models\ClientsModel;
use App\Models\SupportTicketModel;
use App\Models\NotificationsModel;
use App\Models\SupportCategoryModel;
use App\Models\SupportTicketFilesModel;

/*Used helper method to fire event*/
use App\Events\sendEmailOnPostProject;
use Sentinel;
use Validator;
use Session;
use Mail;
use App;
use Lang;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class SupportController extends Controller
{
    public $arr_view_data;

    public function __construct()
    {
        if(!$user = Sentinel::check()){
            $this->user                               = [];
            return redirect('/login');
        }else{
            $this->user                               = $user;
        }

        $this->user_id                              = $user->id;
        $this->UserModel                            = new UserModel;
        $this->ClientsModel                         = new ClientsModel;
        $this->SupportTicketModel                   = new SupportTicketModel;
        $this->NotificationsModel                   = new NotificationsModel;
        $this->SupportCategoryModel                 = new SupportCategoryModel;
        $this->SupportTicketFilesModel              = new SupportTicketFilesModel;
        $this->user_id                              = $user->id;
        $this->module_url_path                      = url("/expert/tickets");
        $this->login_url                            = url("/login");
        $this->arr_view_data                        = [];

        $this->view_folder_path                     = 'expert/support';

        $this->support_ticket_base_img_path         = base_path().config('app.project.img_path.support_ticket_files');
    }

    public function index(Request $request)
    {
        $arr_tickets = $arr_pagination = [];

        $obj_ticket = $this->SupportTicketModel->whereHas('get_user_details', function(){})
                                        ->whereHas('get_category', function(){})
                                        ->with(['get_user_details','get_category'])
                                        ->where('user_id', $this->user_id)
                                        ->where('thread_id',null)
                                        ->orderBy('id', 'DESC');

        if($request->has('type') && $request->input('type') == 'closed' ){
            $obj_ticket = $obj_ticket->where('status', 'closed');
        }
        elseif($request->has('type') && $request->input('type') == 'resolved' ){
            $obj_ticket = $obj_ticket->where('status', 'resolved');
        }
        else{
            $obj_ticket->whereIn('status', ['in_progress', 're_opened']);
        }

        $obj_ticket = $obj_ticket->paginate(5);

        /*if($obj_ticket)
        {
            $arr_tickets = $obj_ticket->toArray();
        }*/

        //$this->arr_view_data['arr_tickets']         = $arr_tickets;
        $this->arr_view_data['user_id']             = $this->user_id;
        $this->arr_view_data['page_title']          = trans('common/common.support_ticket');
        $this->arr_view_data['obj_ticket']          = $obj_ticket;
        $this->arr_view_data['module_url_path']     = $this->module_url_path;
        $this->arr_view_data['arr_transactions']    = [];

        return view($this->view_folder_path.'.index',$this->arr_view_data);
    }

    public function details($ticket_number, Request $request)
    {
        $obj_threads = null;

        $obj_ticket = $this->SupportTicketModel->whereHas('get_user_details', function(){})
                                        //->whereHas('get_category', function(){})
                                        ->with(['get_user_details','get_category','get_files'])
                                        ->where('ticket_number', $ticket_number)
                                        ->where('thread_id',null)
                                        ->first();

        if($obj_ticket)
        {
            $obj_threads = $this->SupportTicketModel
                                       ->with(['get_user_details','get_files','get_category'])
                                       ->with(['get_admin_details', 'get_admin_profile_details'])
                                       ->where('thread_id', $obj_ticket->id)
                                       ->orderBy('created_at','desc')
                                       ->paginate(5);
        }
        else
        {
            Session::flash('error','Invalid request.');
            return redirect()->back();
        }

        $admin_img_path = url('/').'/uploads/admin/profile/';
        $user_img_path = url('/').config('app.project.img_path.profile_image');

        $this->arr_view_data['admin_img_path']      = $admin_img_path;
        $this->arr_view_data['user_img_path']       = $user_img_path;
        $this->arr_view_data['support_ticket_path'] = url('/').str_replace("public/","",config('app.project.img_path.support_ticket_files'));
        $this->arr_view_data['page_title']          = trans('common/common.support_ticket');

        $this->arr_view_data['user_id']             = $this->user_id;
        $this->arr_view_data['obj_ticket']          = $obj_ticket;
        $this->arr_view_data['obj_threads']         = $obj_threads;
        $this->arr_view_data['module_url_path']     = $this->module_url_path;

        return view($this->view_folder_path.'.details',$this->arr_view_data);
    }

    public function create(Request $request)
    {
        $arr_categories = [];

        $obj_cats = $this->SupportCategoryModel->get();

        if($obj_cats){
            $arr_categories = $obj_cats->toArray();
        }

        $this->arr_view_data['page_title']          = trans('common/common.support_ticket');
        $this->arr_view_data['arr_categories']      = $arr_categories;
        $this->arr_view_data['module_url_path']     = $this->module_url_path;
        $this->arr_view_data['arr_transactions']    = [];

        return view($this->view_folder_path.'.create',$this->arr_view_data);
    }

    public function store(Request $request)
    {
        $arr_rules['category'] = "required";
        $arr_rules['priority'] = "required";
        $arr_rules['subject'] = "required";
        $arr_rules['message'] = "required";

        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $arr_data_file['ticket_number']     = "E".mt_rand(0,99999);
        $arr_data_file['user_id']           = $this->user_id;
        $arr_data_file['title']             = $request->input('subject', null);
        $arr_data_file['priority']          = $request->input('priority', null);
        $arr_data_file['cat_id']            = $request->input('category', null);
        $arr_data_file['description']       = $request->input('message', null);

        $obj_ticket = $this->SupportTicketModel->create($arr_data_file);

        if($obj_ticket)
        {
            if($request->hasFile('attachment'))
            {
                if(count($request->file('attachment')) > 0)
                {
                    foreach($request->file('attachment') as $file)
                    {
                        $file_extension = strtolower($file->getClientOriginalExtension());

                        $filename = sha1(uniqid().uniqid()) . '.' . $file->getClientOriginalExtension();
                        $path     = $this->support_ticket_base_img_path . $filename;

                        $isUpload = $file->move($this->support_ticket_base_img_path , $filename);

                        if($isUpload)
                        {
                            $arr_data_file['file']              = $filename;
                            $arr_data_file['type']              = $file_extension;
                            $arr_data_file['thread_id']         = isset($obj_ticket->id) ? $obj_ticket->id : '';    
                            $arr_data_file['support_ticket_id'] = $obj_ticket->id;    
                        }

                        $status = $this->SupportTicketFilesModel->create($arr_data_file);
                    }
                }
            }

            $username = '';

            if(isset($this->user->role_info) && !empty($this->user->role_info)){
                if(isset($this->user->role_info['first_name']) && $this->user->role_info['first_name'] != ''){
                    $username .= $this->user->role_info['first_name'].' ';
                }
                if(isset($this->user->role_info['last_name']) && $this->user->role_info['last_name'] != ''){
                    $username .= $this->user->role_info['last_name'];
                }
            }

            $admin_data = get_admin_email_address();
            // send notification to admin
            $arr_admin_data                         =  [];
            $arr_admin_data['user_id']              =  isset($admin_data['id']) ? $admin_data['id'] : 1 ;
            $arr_admin_data['user_type']            = '1';
            $arr_admin_data['url']                  = 'admin/support_ticket';
            $arr_admin_data['project_id']           = '';
            $arr_admin_data['notification_text_en'] = $username.Lang::get('common/support.has_created_support_ticket',[],'en','en').$obj_ticket->ticket_number;
            $arr_admin_data['notification_text_de'] = $username.Lang::get('common/support.has_created_support_ticket',[],'de','en').$obj_ticket->ticket_number;

            $this->NotificationsModel->create($arr_admin_data);

            Session::flash('success','Ticket created successfully.');
            return redirect($this->module_url_path);

        }else{
            Session::flash('error','Error while creating ticket.');
            return redirect()->back();
        }

    }

    public function submit_reply(Request $request, $enc_id)
    {
        $id = base64_decode($enc_id);

        if(!is_numeric($id)){
            Session::flash('error','Invalid request');
            return redirect()->back();
        }

        $obj_ticket = $this->SupportTicketModel->with(['get_files'])
                               ->with(['get_user_details'])
                               ->where('id','=',$id)
                               ->first();

        if(!$obj_ticket){
            Session::flash('error','Invalid request');
            return redirect()->back();
        }

        $arr_rules['reply_msg'] = "required";

        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            Session::flash('error','Please enter reply message');
            return redirect()->back();
            //return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $admin = Sentinel::check();

        $support = new SupportTicketModel;

        $support->user_id           = $this->user_id;
        $support->ticket_number     = $obj_ticket->ticket_number;   
        //$support->title               = $request->input('title', null);   
        $support->description       = $request->input('reply_msg', null);   
        $support->thread_id         = isset($obj_ticket->id) ? $obj_ticket->id : '';    

        $support_obj                = $support->save();
        $last_inserted_id           = $support->id;
        $support->uploaded_file     = $request->input('_token', null);

        if($request->hasFile('upload_file'))
        {
            if(count($request->file('upload_file')) > 0)
            {
                foreach($request->file('upload_file') as $file)
                {
                    $file_extension = strtolower($file->getClientOriginalExtension());

                    $filename = sha1(uniqid().uniqid()) . '.' . $file->getClientOriginalExtension();
                    $path     = $this->support_ticket_base_img_path . $filename;

                    $isUpload = $file->move($this->support_ticket_base_img_path , $filename);

                    if($isUpload)
                    {
                        $arr_data_file['file']              = $filename;
                        $arr_data_file['type']              = $file_extension;
                        $arr_data_file['thread_id']         = isset($obj_ticket->id) ? $obj_ticket->id : '';    
                        $arr_data_file['support_ticket_id'] = $last_inserted_id;    
                    }

                    $status = $this->SupportTicketFilesModel->create($arr_data_file);
                }
            }
        }

        if($support_obj)
        {
            $this->SupportTicketModel->where('id', $obj_ticket->id)->update(['last_reply_by' => 'user']);

            $username = '';

            if(isset($this->user->role_info) && !empty($this->user->role_info)){
                if(isset($this->user->role_info['first_name']) && $this->user->role_info['first_name'] != ''){
                    $username .= $this->user->role_info['first_name'].' ';
                }
                if(isset($this->user->role_info['last_name']) && $this->user->role_info['last_name'] != ''){
                    $username .= $this->user->role_info['last_name'];
                }
            }

            $admin_data = get_admin_email_address();
            // send notification to admin
            $arr_admin_data                         =  [];
            $arr_admin_data['user_id']              =  isset($admin_data['id']) ? $admin_data['id'] : 1 ;
            $arr_admin_data['user_type']            = '1';
            $arr_admin_data['url']                  = 'admin/support_ticket';
            $arr_admin_data['project_id']           = '';
            $arr_admin_data['notification_text_en'] = $username.Lang::get('common/support.has_replied_to_support_ticket',[],'en','en').$obj_ticket->ticket_number;
            $arr_admin_data['notification_text_de'] = $username.Lang::get('common/support.has_replied_to_support_ticket',[],'de','en').$obj_ticket->ticket_number;

            $this->NotificationsModel->create($arr_admin_data);

            Session::flash('success', 'Reply given successfully.');
            return redirect()->back();
        }

        Session::flash('error', 'Error while creating '.str_singular($this->module_title).'.');
        return redirect()->back();
    }

    public function reopen_ticket($enc_id)
    {
        $id = base64_decode($enc_id);

        if(!is_numeric($id)){
            Session::flash('error','Invalid request');
            return redirect()->back();
        }

        $obj_ticket = $this->SupportTicketModel->with(['get_files'])
                               ->with(['get_user_details'])
                               ->where('id','=',$id)
                               ->first();

        if(!$obj_ticket){
            Session::flash('error','Invalid request');
            return redirect()->back();
        }

        if($this->SupportTicketModel->where('id',$id)->update(['status'=>'re_opened']))
        {
            $username = $role = '';

            $username = '';

            if(isset($this->user->role_info) && !empty($this->user->role_info)){
                if(isset($this->user->role_info['first_name']) && $this->user->role_info['first_name'] != ''){
                    $username .= $this->user->role_info['first_name'].' ';
                }
                if(isset($this->user->role_info['last_name']) && $this->user->role_info['last_name'] != ''){
                    $username .= $this->user->role_info['last_name'];
                }
            }

            $admin_data = get_admin_email_address();
            // send notification to admin
            $arr_admin_data                         =  [];
            $arr_admin_data['user_id']              =  isset($admin_data['id']) ? $admin_data['id'] : 1 ;
            $arr_admin_data['user_type']            = '1';
            $arr_admin_data['url']                  = 'admin/support_ticket';
            $arr_admin_data['notification_text_en'] = $username.Lang::get('common/support.has_reopened_support_ticket',[],'en','en').$obj_ticket->ticket_number;
            $arr_admin_data['notification_text_de'] = $username.Lang::get('common/support.has_reopened_support_ticket',[],'de','en').$obj_ticket->ticket_number;

            $this->NotificationsModel->create($arr_admin_data);
            /* Notification to User */

            Session::flash('success', 'Ticket re-opened successfully.');
            return redirect($this->module_url_path);
        }else{
            Session::flash('error', 'Error re-openinng '.str_singular($this->module_title).'.');
            return redirect()->back();
        }
    }

    public function resolve_ticket(Request $request,$enc_id)
    {
        $arr_rules      = $arr_data = array();
        $id             = base64_decode($enc_id);

        $obj_ticket     = $this->SupportTicketModel->where('id',$id)->first();

        if(!$obj_ticket)
        {
            Session::flash('error', 'Error while resolving Ticket.');
            return redirect()->back();
        }

        if($this->SupportTicketModel->where('id',$id)->update(['status'=>'resolved']))
        {
            $username = $role = '';

            if(isset($obj_ticket->get_user_details->role_info) && !empty($obj_ticket->get_user_details->role_info)){
                if(isset($obj_ticket->get_user_details->role_info['first_name']) && $obj_ticket->get_user_details->role_info['first_name'] != ''){
                    $username .= $obj_ticket->get_user_details->role_info['first_name'].' ';
                }
                if(isset($obj_ticket->get_user_details->role_info['last_name']) && $obj_ticket->get_user_details->role_info['last_name'] != ''){
                    $username .= $obj_ticket->get_user_details->role_info['last_name'];
                }

                $role = get_user_role($obj_ticket->get_user_details->role_info['user_id']);
            }


            /* Notification to User */
            $admin_data = get_admin_email_address();

            $arr_admin_data                         =  [];
            $arr_admin_data['user_id']              =  isset($admin_data['id']) ? $admin_data['id'] : 1 ;
            $arr_admin_data['user_type']            = '1';
            $arr_admin_data['url']                  = 'admin/support_ticket';
            $arr_admin_data['notification_text_en'] = $username.' has resolved ticket : #'.$obj_ticket->ticket_number;
            $arr_admin_data['notification_text_de'] = $username.' hat Ticket aufgelöst : #'.$obj_ticket->ticket_number;
            
            $this->NotificationsModel->create($arr_admin_data);
            /* Notification to User */

            Session::flash('success', 'Your ticket has been resolved.');
            return redirect($this->module_url_path);
        }else{
            Session::flash('error', 'Error while resolving Ticket.');
            return redirect()->back();
        }

    }

} // end class