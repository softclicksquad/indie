<?php
namespace App\Http\Controllers\Front\Expert;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App;
use App\Models\InviteProjectModel;
use Validator;
use Session;
use Mail;
use sentinel;
 
class InvitationsController extends Controller
{
  public function __construct(InviteProjectModel $invitation)
	{
        $this->arr_view_data      = [];
        $this->InviteProjectModel = $invitation;
        $this->module_url_path    = url("invitations");
        if(! $user = Sentinel::check()) {
          return redirect('/login');
        }
        $this->user_id = $user->id;
	}

	/*
    | Comment : Invitations listing of expert 
    | auther  : Tushar A.
  */
  public function index()
  {   
      $arr_invitations = array();
      $arr_pagination   = array();

      if($this->user_id) 
      { 
         if(Session::has('show_invitations_cnt')) { $pagi_cnt = Session::get('show_invitations_cnt'); }else { $pagi_cnt = config('app.project.pagi_cnt'); }
         $obj_invitations = $this->InviteProjectModel->where('expert_user_id',$this->user_id)
                                                     ->with(['notification_details','project_details','client_details'])
                                                     ->orderBy('id','DESC')
                                                     ->paginate($pagi_cnt);
         if($obj_invitations!=FALSE)
         {
              $arr_pagination   =clone $obj_invitations;
              $arr_invitations =$obj_invitations->toArray();
         }
      }
      $this->arr_view_data['page_title']            = trans('controller_translations.page_title_invitations');
      $this->arr_view_data['arr_invitations']       = $arr_invitations;
      $this->arr_view_data['arr_pagination']        = $arr_pagination;
      $this->arr_view_data['module_url_path']       = $this->module_url_path;
      return view('expert.invitations.index',$this->arr_view_data);
  }
  /*
  | Comment : Invitations details of expert 
  | auther  : Tushar A.
  */
  public function show($id="")
  {
     $invitation = array();
     if($id!=FALSE && $id!="")
     { 
        $id = base64_decode($id);
        $obj_invitation_info = $this->InviteProjectModel->with(['notification_details','project_details','client_details'])->where('id',$id)->first();
        if($obj_invitation_info)
        {
           $invitation = $obj_invitation_info->toArray();
           $this->InviteProjectModel->where('id',$id)->update(['is_read' => 'YES']);
        }
     } 
    $this->arr_view_data['page_title']      = trans('controller_translations.page_title_invitation_details');
    $this->arr_view_data['invitation']      = $invitation;
    $this->arr_view_data['module_url_path'] = $this->module_url_path;
    return view('expert.invitations.show',$this->arr_view_data);
  }
  public function show_cnt(Request $Request) 
  {
      \Session::put('show_invitations_cnt' , $Request->input('show_cnt'));
      $preurl = url()->previous();
      $explode_url = explode('?page=',$preurl);
      if(empty($explode_url[1])){
          return redirect()->back();
      }
      else{
          $redirect =  $explode_url[0].'?page='.'1';
          return redirect($redirect);
      }
  }
  public function delete($enc_id='')
  {
    if($enc_id=='')
    {
      Session::flash("error",trans('controller_translations.text_sorry_somthing_goes_wrong_please_try_again'));
      return redirect('expert/projects/invitations');
    }

    $is_delete = $this->InviteProjectModel->where('id',base64_decode($enc_id))->delete();
    if($is_delete)
    {
      Session::flash("success",trans('controller_translations.text_invitation_deleted_successfully'));
      return redirect('expert/projects/invitations');
    }
    else
    {
      Session::flash("error",trans('controller_translations.text_sorry_somthing_goes_wrong_please_try_again'));
      return redirect('expert/projects/invitations');
    }
  }
} // end class
