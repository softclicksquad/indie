<?php

namespace App\Http\Controllers\Front\Expert;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App;

use App\Models\SubscriptionUsersModel;

use Validator;
use Session;
use Mail;
use sentinel;

class SubscriptionHistoryController extends Controller
{
  public function __construct(SubscriptionUsersModel $subscription)
	{
		    $this->arr_view_data      = [];
        $this->SubscriptionUsersModel  = $subscription;
        $this->module_url_path = url("subscriptionhistory");

        if(! $user = Sentinel::check()) 
        {
          return redirect('/login');
        }

        $this->user_id = $user->id;
	}

	/*
    | Comment : Subscription history listing of expert
    | auther  : Ashwini K.
    */

    public function index()
    {   


        $arr_subscription = array();
        $arr_pagination   = array();

        if($this->user_id) 
        {
           $obj_subscription = $this->SubscriptionUsersModel->where('user_id',$this->user_id)->with(['transaction_details'])->paginate(config('app.project.pagi_cnt'));
          
           if($obj_subscription!=FALSE)
           {
                $arr_pagination   = clone $obj_subscription;
                $arr_subscription = $obj_subscription->toArray();
           }
        }

        $this->arr_view_data['page_title']            = trans('controller_translations.page_title_subscription_history');
        $this->arr_view_data['arr_subscription']      = $arr_subscription;
        $this->arr_view_data['arr_pagination']        = $arr_pagination;
        $this->arr_view_data['module_url_path']       = $this->module_url_path;

        return view('expert.subscription_history.index',$this->arr_view_data);
    }

    /*
    | Comment : Subscription history details of expert
    | auther  : Ashwini K.
    */

     public function show($id="")
    {

      $subscription = array();

       if($id!=FALSE && $id!="")
       { 
          $id = base64_decode($id);
          $obj_subscription = $this->SubscriptionUsersModel->where('id',$id)->with(['transaction_details'])->first();

          if($obj_subscription)
          {
            $subscription = $obj_subscription->toArray();
          }
       } 
        $this->arr_view_data['page_title'] = trans('controller_translations.page_title_subscription_history_details'); 
        $this->arr_view_data['subscription'] = $subscription;
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view('expert.subscription_history.show',$this->arr_view_data);

    }

}
