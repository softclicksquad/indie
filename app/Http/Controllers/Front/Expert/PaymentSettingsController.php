<?php

namespace App\Http\Controllers\Front\Expert;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models\PaymentSettingsModule;

use App\Models\ExpertsModel;

use Session;
use Validator;
use Sentinel;
use App\Common\Services\PaymentService as PaymentService;

class PaymentSettingsController extends Controller
{
    public function __construct(PaymentSettingsModule $admin_payment,
                                ExpertsModel $expert_module
                                )
    {
        $this->PaymentSettingsModule = $admin_payment;
        
        $this->ExpertsModel = $expert_module;

        $this->arr_view_data = [];
        $this->module_url_path = url("/expert/settings/payment");
        $this->PaymentService = new PaymentService();

        if(!$user = Sentinel::check()) 
        {
         return redirect('/login');
        }

        $this->user_id = $user->id;
    }


    public function index()
    {
    	
    	$page_title = trans('controller_translations.page_title_payment_settings');
    	$payment_details = array();
    	
    	if ($this->user_id) 
    	{
            $payment_details = $this->ExpertsModel->where('user_id',$this->user_id)->first();
            
    	   //$payment_details = $this->PaymentService->get_user_payment_settings($this->user_id);
    	}

        $this->arr_view_data['payment_details'] = $payment_details;
        $this->arr_view_data['page_title'] = $page_title;
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        
    	return view('expert.payment_settings.index',$this->arr_view_data);
    }


     /*
    | update : update payment settings details
    | auther :Sagar Sainkar
    */ 

    public function update_old(Request $request)
    {
        $arr_rules=array();
        $arr_rules['paypal_client_id'] = "required";
        $arr_rules['paypal_secret_key'] = "required";
        /*$arr_rules['stripe_secret_key'] = "required";
        $arr_rules['stripe_publishable_key'] = "required";*/
        
        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
             return back()->withErrors($validator)->withInput();
        }

         $form_data = array();
         $arr_data = array();
         $form_data = $request->all();
         $ecn_arr_data = FALSE;
         $status = FALSE;

         $arr_data['paypal_client_id'] = $form_data['paypal_client_id'];
         $arr_data['paypal_secret_key'] = $form_data['paypal_secret_key'];
         $arr_data['paypal_payment_mode'] = '1';
         /*$arr_data['stripe_secret_key'] = $form_data['stripe_secret_key'];
         $arr_data['stripe_publishable_key'] = $form_data['stripe_publishable_key'];*/

        
        $payment_settings = $this->PaymentSettingsModule->where('user_id',$this->user_id)->first();
        $ecn_arr_data = $payment_details = $this->PaymentService->_encrypt($arr_data);

        if ($payment_settings!=FALSE && $ecn_arr_data!=FALSE)
        {
            $status = $payment_settings->update(['payment_details'=>$ecn_arr_data]);
        }
        else if($ecn_arr_data!=FALSE)
        {
            $payment_settings = array();
            $payment_settings['user_id'] = $this->user_id;
            $payment_settings['payment_details'] = $ecn_arr_data;

            $status = $this->PaymentSettingsModule->create($payment_settings);
        }


        if($status)
        {
            Session::flash("success",trans('controller_translations.success_payment_settings_updated_successfullly'));
        }
        else
        {
            Session::flash("error",trans('controller_translations.success_payment_settings_updated_successfullly'));  
        }

        return redirect()->back();
    }


    public function update(Request $request)
    {
        $arr_rules=array();
        $arr_rules['paypal_email'] = "required|email";
        
        
        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
             return back()->withErrors($validator)->withInput();
        }

         $form_data = array();
         $arr_data = array();
         $form_data = $request->all();
         $ecn_arr_data = FALSE;
         $status = FALSE;

         $arr_data['paypal_email'] = $form_data['paypal_email'];
        
        $payment_details = $this->ExpertsModel->where('user_id',$this->user_id)->first();

        if ($payment_details)
        {
            $status = $payment_details->update($arr_data);
        }
        
        if($status)
        {
            Session::flash("success",trans('controller_translations.success_payment_settings_updated_successfullly'));
        }
        else
        {
            Session::flash("error",trans('controller_translations.success_payment_settings_updated_successfullly'));  
        }

        return redirect()->back();
    }

}
