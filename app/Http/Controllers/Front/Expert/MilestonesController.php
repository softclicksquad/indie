<?php

namespace App\Http\Controllers\Front\Expert;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\ProjectpostModel;
use App\Models\MilestonesModel;
use App\Models\MilestoneReleaseModel;
use App\Models\NotificationsModel;
use App\Models\ExpertsModel;
use App\Models\UserModel;
use App\Models\UserWalletModel;
use App\Common\Services\WalletService;
use App\Common\Services\MailService;
use Sentinel;
use Validator;
use Session;
use Mail;
use PDF;
use Lang;

class MilestonesController extends Controller
{
    public $arr_view_data;
    public function __construct(  
                                  ProjectpostModel $projectpost,
                                  MilestonesModel $milestones,
                                  MilestoneReleaseModel $milestone_release,
                                  NotificationsModel $notifications,
                                  ExpertsModel $expert,
                                  WalletService $WalletService,
                                  UserModel $UserModel,
                                  MailService $MailService
                               )
    {
      $this->arr_view_data = [];
      if(! $user = Sentinel::check()){
        return redirect('/login');
      }
      $this->user_id = $user->id;
      $this->ProjectpostModel       = $projectpost;
      $this->MilestonesModel        = $milestones;
      $this->MilestoneReleaseModel  = $milestone_release;
      $this->NotificationsModel     = $notifications;
      $this->ExpertsModel           = $expert;
      $this->WalletService          = $WalletService;
      $this->UserModel              = $UserModel;
      $this->MailService            = $MailService;
      $this->UserWalletModel        = new UserWalletModel;

      $this->view_folder_path       = 'milestones';
      $this->module_url_path        = url("/expert/projects");
      $this->logo_image_base_path   = base_path() . '/public'.config('app.project.img_path.invoices_image');
      $this->logo_img_public_path   = url('/').config('app.project.img_path.invoices_image');
    }

    /* 
      Comments  : Show Project milestones.
      Auther    : Nayan S.
    */
    public function show_project_milestones($enc_id)
    { 
      $project_id                              = base64_decode($enc_id);
      $expert_user_id                          = $this->user_id;
      $this->arr_view_data['mp_job_wallet_id'] = '';


      $project_details = $this->ProjectpostModel->where('id',$project_id)->first(['id','project_name','project_currency','mp_job_wallet_id']);
      if ($project_details){
          $this->arr_view_data['milestone_project_name']       = isset($project_details->project_name)?$project_details->project_name:'';
          $this->arr_view_data['milestone_project_currency']   = isset($project_details->project_currency)?$project_details->project_currency:'';
          $this->arr_view_data['mp_job_wallet_id']             = isset($project_details->mp_job_wallet_id)?$project_details->mp_job_wallet_id:'';
      }
      $obj_milestone = $this->MilestonesModel->where('project_id',$project_id)
                                             ->where('status','1')
                                             ->with(['milestone_refund_details','transaction_details','project_details'=>function ($query)  use ($expert_user_id) {
                                                  //$query->select('id','project_name','expert_user_id','project_status');
                                                  $query->where('expert_user_id',$expert_user_id);
                                                }])
                                             ->with(['milestone_release_details'=> function ($query_nxt) {
                                                  $query_nxt->select('id','milestone_id','status','project_id');    
                                                }])->orderBy('milestone_due_date','ASC')
                                             ->paginate(config('app.project.pagi_cnt'));

      $arr_milestone      = [];
      if($obj_milestone) {
          $arr_pagination = clone $obj_milestone; 
          $arr_milestone  = $obj_milestone->toArray();
      }
      // get wallet details
      $wallet_details     = $this->WalletService->get_wallet_details($this->arr_view_data['mp_job_wallet_id']);
      if(isset($wallet_details)){
        $wallet_details   = $wallet_details;
      }
      // end get wallet details
      $this->arr_view_data['enc_project_id']            = $enc_id;
      $this->arr_view_data['page_title']                = trans('controller_translations.page_title_milestones');
      $this->arr_view_data['arr_milestone']             = $arr_milestone;
      $this->arr_view_data['arr_pagination']            = $arr_pagination;
      $this->arr_view_data['mangopay_wallet_details']   = $wallet_details;
      $this->arr_view_data['module_url_path']           = $this->module_url_path;
      //dd($this->arr_view_data);
      return view($this->view_folder_path.'.project_milestones',$this->arr_view_data);
    }
    /* 
      Comments  : Expert requests for milestones release to client.
      Auther    : Nayan S.
    */
    public function request_to_release_milestones($enc_id,$currency)
    {
      $milestone_id  = base64_decode($enc_id);
      $obj_milestone = $this->MilestonesModel->where('id',$milestone_id)->first();
      if($obj_milestone)
      { 
        $arr_request = [];
        $arr_request['milestone_id']   = $obj_milestone->id;
        $arr_request['project_id']     = $obj_milestone->project_id;
        $arr_request['expert_user_id'] = $this->user_id;
        $arr_request['client_user_id'] = $obj_milestone->client_user_id;
        $arr_request['status']         = '0';
        /* Finding that expert has filled his paypal account email address in Payment Settings*/

        $obj_payment_details           = $this->UserWalletModel->where('user_id',$this->user_id)
                                                               ->where('currency_code',$currency)
                                                               ->first();
        if($obj_payment_details){
          $arr_payment = $obj_payment_details->toArray();
          if($arr_payment['mp_wallet_id'] == NULL || $arr_payment['mp_wallet_id'] == '' ){
            Session::flash('error',trans('controller_translations.please_fill_your_paypal'));
            return redirect()->back();
          }
        } else {
          Session::flash('error',trans('controller_translations.error_while_sending_milestone_release_request'));
          return redirect()->back();
        }

        $result = $this->MilestoneReleaseModel->create($arr_request);  
        if($result)
        {
          $this->MilestonesModel->where('id',$milestone_id)->update(['is_milestone'=>'1']);

          Session::flash('success',trans('controller_translations.success_milestone_request_sent_successfully'));
          /* Send email to client about milestone release request. */
          $project_name     = config('app.project.name');
          //$mail_form        = get_site_email_address();   /* getting email address of admin from helper functions */
          $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';

          
          $obj_project_info = $this->ProjectpostModel->where('id',$obj_milestone->project_id)
                                                      ->with(['client_info'=> function ($query) {
                                                              $query->select('user_id','first_name','last_name');
                                                            },
                                                            'client_details'=>function ($query_nxt) {
                                                              $query_nxt->select('id','email','user_name');
                                                            }])
                                                      ->with(['expert_info'=> function ($query_nxt_1) {
                                                            $query_nxt_1->select('user_id','first_name','last_name');
                                                            },
                                                            'expert_details'=>function ($query_nxt_2) {
                                                              $query_nxt_2->select('id','email','user_name');
                                                            }
                                                            ])
                                                      ->first(['id','project_name','client_user_id','expert_user_id','project_currency']);
          $data = [];
          if($obj_project_info)
          {
            $arr_project_info  = $obj_project_info->toArray();
            /* get client name */
            $client_first_name = isset($arr_project_info['client_info']['first_name'])?$arr_project_info['client_info']['first_name']:'';
            $client_last_name  = isset($arr_project_info['client_info']['last_name'])?$arr_project_info['client_info']['last_name']:'';

            /*$client_name = $client_first_name.' '.$client_last_name;*/
            $client_name       = $client_first_name;
            $expert_first_name = isset($arr_project_info['expert_info']['first_name'])?$arr_project_info['expert_info']['first_name']:'';
            $expert_last_name  = isset($arr_project_info['expert_info']['last_name'])?$arr_project_info['expert_info']['last_name']:'';
            $expert_name       = $expert_first_name.' '.$expert_last_name;
            $expert_username   = isset($arr_project_info['expert_details']['user_name'])?$arr_project_info['expert_details']['user_name']:'';

            $data['project_name']        = isset($arr_project_info['project_name'])?$arr_project_info['project_name']:'';
            $data['milestone_title']     = isset($obj_milestone->title)?$obj_milestone->title:'';
            $data['milestone_amount']    = isset($obj_milestone->cost)?$obj_milestone->cost:'';
            $data['client_name']         = $client_name;
            $data['expert_name']         = $expert_name;
            $data['expert_username']     = $expert_username;
            $data['project_currency']    = isset($arr_project_info['project_currency'])?$arr_project_info['project_currency']:'$';
            $data['login_url']           = url('/redirection?redirect=MILESTONE&id='.base64_encode($obj_milestone->project_id));
            $data['email_id']            = isset($arr_project_info['client_details']['email'])?
                                                 $arr_project_info['client_details']['email']:'';
                                                 
          }

          $email_to = isset($arr_project_info['client_details']['email'])? $arr_project_info['client_details']['email']:'';
          if($email_to!= "")
          {
            try{
                  $mail_status = $this->MailService->send_milestone_release_request_by_expert_to_client_email($data);
                  // Mail::send('front.email.milestone_release_request_by_expert_to_client', $data, function ($message) use ($email_to,$mail_form,$project_name) {
                  //     $message->from($mail_form, $project_name);
                  //     $message->subject($project_name.': Milestone Release Request.');
                  //     $message->to($email_to);
                  // });
            }
            catch(\Exception $e){
            Session::Flash('error',trans('controller_translations.text_mail_not_sent'));
            }
          }

          $posted_project_name = isset($arr_project_info['project_name'])?$arr_project_info['project_name']:'';

          /* Create Notification for client */      
          $arr_data =  [];
          $arr_data['user_id']    = $obj_milestone->client_user_id;
          $arr_data['user_type']  = '2';
          $arr_data['url']        = 'client/projects/milestones/'.base64_encode($obj_milestone->project_id);
          $arr_data['project_id'] = $obj_milestone->project_id;

          /*$arr_data['notification_text'] =  trans('controller_translations.msg_milestone_release_request') ;*/
          $arr_data['notification_text_en'] =  $posted_project_name . ' - ' . Lang::get('controller_translations.msg_milestone_release_request',[],'en','en');
          $arr_data['notification_text_de'] =  $posted_project_name . ' - ' . Lang::get('controller_translations.msg_milestone_release_request',[],'de','en');

          $this->NotificationsModel->create($arr_data);  
          /* Notification to admin */
          $arr_admin_data =  [];
          $arr_admin_data['user_id']    = 1;
          $arr_admin_data['user_type']  = '1';
          $arr_admin_data['url']        = 'admin/project_milestones/all/'.base64_encode($obj_milestone->project_id);
          $arr_admin_data['project_id'] = $obj_milestone->project_id;
          
          /*$arr_admin_data['notification_text']  = trans('controller_translations.msg_milestone_release_request') ;*/
          $arr_admin_data['notification_text_en'] =  Lang::get('controller_translations.msg_milestone_release_request',[],'en','en');
          $arr_admin_data['notification_text_de'] =  Lang::get('controller_translations.msg_milestone_release_request',[],'de','en');

          $this->NotificationsModel->create($arr_admin_data);  
          /* Notificatin ends */
        }
        else
        {
          Session::flash('error',trans('controller_translations.error_error_while_sending_milestone_request'));
        }
      }
      else
      {
        Session::flash('error',trans('controller_translations.error_error_while_sending_milestone_request'));
      }
      return redirect()->back();
    }  
    /* 
      Comments  : Generate PDF File of Invoice at Expert .
      Auther    : Bharat Khairnar.
    */
    public function generate_invoice($enc_id)
    {
      $milestone_id = base64_decode($enc_id);
  
      $expert_user_id = $this->user_id;
    
      $obj_milestone = $this->MilestoneReleaseModel->where('milestone_id',$milestone_id)
                                             ->where('status','2')
                                             ->with(['expert_details','client_details','transaction_details','project_details','milestone_details'])
                                             ->first();

      $arr_milestone = [];
      if($obj_milestone)
      {
          $arr_milestone    = $obj_milestone->toArray();
      }

      $page_title = 'Generate Invoice';
      PDF::SetTitle('Invoice');
      PDF::AddPage();
      PDF::writeHTML(view('expert/invoice/invoice_pdf',compact('page_title','arr_milestone')), 'Invoice');
       //ob_end_clean();
      $filename = 'Invoice-'.$obj_milestone->invoice_id;
      PDF::Output($filename.'.pdf','D');
    }
    public function show_invoice($enc_id)
    {
      $milestone_id   = base64_decode($enc_id);
      $expert_user_id = $this->user_id;
      $obj_milestone  = $this->MilestoneReleaseModel->where('milestone_id',$milestone_id)
                                             ->where('status','2')
                                             ->with(['expert_details','client_details','transaction_details','project_details','milestone_details'])
                                             ->first();
      $arr_milestone = [];
      if($obj_milestone)
      {
          $arr_milestone    = $obj_milestone->toArray();
      }

      $page_title = 'Generate Invoice';


      $this->arr_view_data['page_title']      = 'Generate Invoice';
      $this->arr_view_data['arr_milestone']   = $arr_milestone;
      $this->arr_view_data['module_url_path'] = $this->module_url_path;

      //dd($arr_milestone);
      
      return view('expert/invoice/invoice_form',$this->arr_view_data);
    }


    public function create_invoice(Request $request,$enc_id)
    {

      $arr_rules['vat_percentage'] = "required|numeric|min:1|max:100";
      $arr_rules['logo_image'] = "mimes:png,jpeg,jpg,gif";      
      $validator = Validator::make($request->all(),$arr_rules);

      if($validator->fails())
      {
          return redirect()->back()->withErrors($validator)->withInput($request->all());
      }
      
      $form_data = [];
      $form_data = $request->all(); 
      $temp_logo = FALSE;
      $logo_image_name = FALSE;


      if(isset($form_data['logo_image']) && $form_data['logo_image']!=FALSE)
        {    
            if ($request->hasFile('logo_image')) 
            {
              $img_valiator = Validator::make(array('image'=>$request->file('logo_image')),array(
                                              'image' => 'mimes:png,jpeg,jpg,gif')); 

                if ($request->file('logo_image')->isValid() && $img_valiator->passes())
                {

                    list($width, $height) = getimagesize($form_data['logo_image']);
                    if ($width<=200 && $height<=90) 
                    {

                         $logo_image_name = $form_data['logo_image'];
                        $fileExtension = strtolower($request->file('logo_image')->getClientOriginalExtension()); 

                        if($fileExtension == 'png' || $fileExtension == 'jpg' || $fileExtension == 'jpeg' || $fileExtension == 'gif')
                        {
                            $logo_image_name = sha1(uniqid().$logo_image_name.uniqid()).'.'.$fileExtension;
                            $request->file('logo_image')->move($this->logo_image_base_path, $logo_image_name);
                            //$arr_expert['logo_image'] = $logo_image_name;
                            $temp_logo = $this->logo_img_public_path.'/'.$logo_image_name;
                        }
                        else
                        {
                             Session::flash('error',trans('controller_translations.error_invalid_file_extension_for_logo_image') );
                             return back()->withInput();
                        }

                    }
                    else
                    {
                       Session::flash("error",trans('controller_translations.text_logo_image_size_must_be_queal_to_or_less_than')."200*90");
                       return back()->withErrors($validator)->withInput($request->all());
                    } 
                }
                else
                {
                     Session::flash("error", trans('controller_translations.error_please_upload_valid_image'));
                     return back()->withErrors($validator)->withInput($request->all());
                }
                    
            }
        }



      $milestone_id = base64_decode($enc_id);
      $expert_user_id = $this->user_id;

      $obj_milestone = $this->MilestoneReleaseModel->where('milestone_id',$milestone_id)
                                             ->where('status','2')
                                             ->with(['expert_details','client_details','transaction_details','project_details','milestone_details'])
                                             ->first();

      $arr_milestone = [];
      $arr_invoice_costs = [];

      if($obj_milestone)
      {
          $arr_milestone    = $obj_milestone->toArray();
      }

      if (isset($arr_milestone['milestone_details']['cost'])) 
      {
        if (isset($form_data['vat_percentage']) && $form_data['vat_percentage']>0) 
        {
          /*$vat_cost = ($arr_milestone['milestone_details']['cost']*$form_data['vat_percentage'])/100;
            $milestone_cost = $arr_milestone['milestone_details']['cost'] - $vat_cost;*/

          /*Calculate VTA with VAT Formula*/

          $vat_percentage = ($form_data['vat_percentage']/100);
          $divisor = 1+$vat_percentage;
          
          $milestone_cost = ($arr_milestone['milestone_details']['cost']/$divisor);
          $vat_cost = ($arr_milestone['milestone_details']['cost'] - $milestone_cost);

          $arr_invoice_costs['total_cost'] = $arr_milestone['milestone_details']['cost'];
          $arr_invoice_costs['milestone_cost'] = number_format($milestone_cost,2);
          $arr_invoice_costs['vat_cost'] = number_format($vat_cost,2);
          $arr_invoice_costs['vat_percentage'] = $form_data['vat_percentage'];
        }
      }

     //$logo_image_path = config('app.project.img_path.invoices_image');
     //$final_logo_image = get_resized_image_path($logo_image_name,$logo_image_path.'/',40,150);

      $page_title = 'Generate Invoice';
      PDF::SetTitle('Invoice');
      PDF::AddPage();
      PDF::writeHTML(view('expert/invoice/invoice_pdf',compact('page_title','arr_milestone','arr_invoice_costs','temp_logo')), 'Invoice');
       //ob_end_clean();
      $filename = 'Invoice-'.$obj_milestone->invoice_id;
      PDF::Output($filename.'.pdf','D');
      if (isset($logo_image_name) && $logo_image_name!=FALSE) 
      {
          $file_exits = file_exists($this->logo_image_base_path.$logo_image_name);
          if ($file_exits) 
          {
              unlink($this->logo_image_base_path.$logo_image_name);
          }
      }
    }
}