<?php

namespace App\Http\Controllers\Front\Expert;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\UserModel;
use App\Models\ExpertsModel;
use App\Models\CountryModel; 
use App\Models\SkillsModel;
use App\Models\CategoriesModel;
use App\Models\ExpertsSkillModel;
use App\Models\SubCategoriesModel;
use App\Models\ExpertsCategoriesModel;
use App\Models\SubscriptionUsersModel;
use App\Models\ExpertsSpokenLanguagesModel;
use App\Models\ExpertCategorySubCategoryModel;
use App\Models\EducationModel;
use App\Models\SubscriptionPacksModel;
use App\Models\CurrencyModel;

use App\Models\ExpertWorkExperienceModel;
use App\Models\ExpertsCertificationDetailsModel;

use App\Common\Services\LanguageService;
use App\Models\ProfessionModel; 
use App\Common\Services\WalletService;
use App\Common\Services\MailService;



use Validator;
use Session;
use Sentinel;
use Mail;
use Activation;
use Reminder;
use URL;
use DB;

class ProfileController extends Controller
{
	  public $arr_view_data;
    public function __construct(
                                  UserModel $user_model,
                                  ExpertsModel $expert,
                                  CountryModel $countries,
                                  SkillsModel $skills,
                                  LanguageService $langauge,
                                  ExpertsSkillModel $expert_skills,
                                  ExpertsCategoriesModel $expert_categories,
                                  CategoriesModel $category,
                                  SubCategoriesModel $subcategory,
                                  SubscriptionUsersModel $user_subscription,
                                  ProfessionModel $Profession,
                                  ExpertCategorySubCategoryModel $expert_category_subcategory_model,
                                  ExpertsSpokenLanguagesModel $expertsspokenlanguage,
                                  WalletService $wallet_service,
                                  EducationModel $education_model,
                                  ExpertWorkExperienceModel $expert_work_experience_model,
                                  ExpertsCertificationDetailsModel $experts_cetification_details_model,
                                  MailService $MailService
                                )
    { 
 
      if(! $user = Sentinel::check()) 
      {
        return redirect('/login');
      }

      $this->user_id                        = $user->id;
      $this->user                           = $user;
      $this->UserModel                      = $user_model;
      $this->ExpertsModel                   = $expert;
      $this->CountryModel                   = $countries;
      $this->SkillsModel                    = $skills;
      $this->CategoriesModel                = $category;
      $this->SubCategoriesModel             = $subcategory;
      $this->LanguageService                = $langauge;
      $this->ExpertsSkillModel              = $expert_skills;
      $this->ExpertsCategoriesModel         = $expert_categories;
      $this->SubscriptionUsersModel         = $user_subscription;
      $this->EducationModel                 = $education_model;
      $this->ExpertWorkExperienceModel      = $expert_work_experience_model;
      $this->ExpertCertificationDetailsModel = $experts_cetification_details_model;
      $this->ExpertsSpokenLanguagesModel    = $expertsspokenlanguage;
      $this->ExpertCategorySubCategoryModel = $expert_category_subcategory_model;
      $this->ProfessionModel                = $Profession;
      $this->WalletService                  = $wallet_service;
      $this->MailService                    = $MailService;
      $this->CurrencyModel                  = new CurrencyModel();
      $this->SubscriptionPacksModel         = new SubscriptionPacksModel;
      $this->arr_view_data                  = [];
      $this->module_url_path                = url("/expert/profile");
      $this->profile_img_base_path          = base_path() . '/public'.config('app.project.img_path.profile_image');
      $this->profile_img_public_path        = url('/public').config('app.project.img_path.profile_image');
      $this->cover_img_base_path            = base_path() . '/public'.config('app.project.img_path.cover_image');
      $this->cover_img_public_path          = url('/public').config('app.project.img_path.cover_image');
      $logged_user                         = $this->user->toArray(); 
    }

    /*Profile: load page of expert profile
    Author: Sagar Sainkar*/

    public function index(Request $request,$enc_id = null,$cat_id = null)
    {
      //get all countries here
      $obj_category_id = base64_decode($cat_id);
      $expert_id       = base64_decode($enc_id);
      $user_id         = $this->user_id;
      //dd($expert_id,$user_id);
      $arr_countries   = array();
      $arr_skills      = array();
      $arr_categories  = $arr_all_categories = array();
      $user_categories = [];
      $arr_education   = [];
      $arr_mangopay_blocked_countries = [];

      $obj_countries = $this->CountryModel->orderBy('country_name', 'asc')->get();

      if($obj_countries != FALSE)
      {
          $arr_countries = $obj_countries->toArray();
      }

      $arr_mangopay_blocked_countries = get_mangopay_blocked_countries_id();

      $obj_skills = $this->SkillsModel->where('is_active',1)->with(['translations'])->get();
      if($obj_skills != FALSE)
      {
          $arr_skills = $obj_skills->toArray();
      }

      $arr_cat_id = $arr_sub_cat_id = [];
      $arr_experts_categories = [];
      $count_of_categories    = '0';
      $count_of_subcategories = '0';
      $total_subcategories    = '0';
      $obj_experts_categories = $this->ExpertsCategoriesModel
                                                  ->with(['categories','experts_categories_subcategories_details'=>function($query){
                                                    $query->with(['subcategory_traslation']);
                                                  }])
                                                  ->with('is_sub_category_exist')
                                                  ->where('expert_user_id',$user_id)
                                                  ->orderBy('id','DESC')
                                                  ->get();

      if($obj_experts_categories)
      {
          $arr_experts_categories = $obj_experts_categories->toArray();
           
          $count_of_categories = count($arr_experts_categories);
          //dd($count_of_categories);
          if($count_of_categories>0)
          {
            foreach($arr_experts_categories as $key=>$data)
            {
              $count_of_subcategories = $count_of_subcategories + isset($data['experts_categories_subcategories_details']) && count($data['experts_categories_subcategories_details'])>0 ? count($data['experts_categories_subcategories_details']) :'0';

              $total_subcategories = $total_subcategories + $count_of_subcategories;
            }              
          }
      }
      //dd($total_subcategories);
      if(isset($arr_experts_categories) && count($arr_experts_categories)>0){
        foreach ($arr_experts_categories as $key => $value) 
        {
            if(isset($value['category_id'])){
              $arr_cat_id[] = $value['category_id'];
            }
        }
      }
      //dd($arr_experts_categories);
      /*Start of edit category*/
      if(isset($expert_id) && $expert_id !="")
      {
          $edit_arr_experts_categories = [];
          $edit_obj_experts_categories = $this->ExpertsCategoriesModel
                                                      ->with(['categories','experts_categories_subcategories_details'=>function($query){
                                                        $query->with(['subcategory_traslation']);
                                                      }])
                                                      ->with('is_sub_category_exist')
                                                      ->where('expert_user_id',$user_id)
                                                      ->where('id',$expert_id)
                                                      ->first();
          if($edit_obj_experts_categories)
          {
              $edit_arr_experts_categories = $edit_obj_experts_categories->toArray();
              $check_flag = 1;
          }
          $arr_cat_id = \array_diff($arr_cat_id, [$obj_category_id]); 
      }
      /*End of edit category*/
      
      $obj_categories = $this->CategoriesModel
                                      ->where('is_active',1)
                                      ->with(['translations'])
                                      ->with('is_sub_category_exist')
                                      ->whereNotIn('id', $arr_cat_id)
                                      ->get();
      
      if($obj_categories != FALSE)
      {
          $arr_categories = $obj_categories->toArray();
      }
      //dd($arr_categories);
      $obj_all_categories = $this->CategoriesModel
                                      ->where('is_active','1')
                                      ->with(['translations'])

                                      ->get();
      
      if($obj_all_categories != FALSE)
      {
          $arr_all_categories = $obj_all_categories->toArray();
      }

      
      $obj_expert = $this->ExpertsModel->where('user_id',$this->user_id)->with(['user_details','country_details.states','state_details.cities','city_details','expert_skills','expert_categories','expert_spoken_languages','expert_work_experience','expert_certification_details'])->first();
      $arr_expert_details = array();
      if($obj_expert != FALSE)
      {
      	$arr_expert_details = $obj_expert->toArray();
      }

      if($arr_expert_details['user_details']['user_name']=='')
      {
          $country_string   = isset($arr_expert_details['country_details']['country_code'])?
                                    $arr_expert_details['country_details']['country_code']:'';
          $user_type_string = isset($arr_expert_details['user_type'])?$arr_expert_details['user_type']:'';
          $first_string     = isset($arr_expert_details['first_name'])?$arr_expert_details['first_name']:'';
          $user_name_string = $this->GetRandomString($country_string,$user_type_string,$first_string);
      }
      else
      {
        $user_name_string  = $arr_expert_details['user_details']['user_name'];
      }
      
      if(isset($arr_expert_details['phone_code']) && $arr_expert_details['phone_code'] == '') {

        // $ip = '52.25.109.230';
        $ip = $request->ip();
        $obj_location = @json_decode(file_get_contents( "http://www.geoplugin.net/json.gp?ip=" . $ip));
        if(isset($obj_location->geoplugin_countryCode) && $obj_location->geoplugin_countryCode!=''){
          $arr_expert_details['phone_code'] = get_phone_code_by_country_code($obj_location->geoplugin_countryCode);
        }
      }


      $this->arr_view_data['user_name_string']   = $user_name_string;


      $obj_subscription = $this->SubscriptionUsersModel
                              ->with('subscription_pack_details')
                              ->where('user_id',$this->user_id)
                              ->orderBy('id','DESC')
                              ->limit(1)
                              ->first();

      if ($obj_subscription!=FALSE) 
      {
        $arr_subscription = $obj_subscription->toArray();
      }
      
      $arr_subcategories = [];

      if(isset($obj_category_id) && $obj_category_id!=null){

        $obj_subcategories = $this->SubCategoriesModel->where(['is_active'=>'1','category_id'=>$obj_category_id])
                                              ->with(['translations'])->get();
        if($obj_subcategories != FALSE)
        {
            $arr_subcategories = $obj_subcategories->toArray();
        }
      }
      $arr_all_subcategories = [];

      $obj_all_subcategories = $this->SubCategoriesModel->where('is_active','1')
                                                        ->with('subcategory_traslation')
                                                        ->get();
      if($obj_all_subcategories != FALSE)
      {
          $arr_all_subcategories = $obj_all_subcategories->toArray();
      }
      
      //dd($arr_all_subcategories);
      $this->arr_view_data['arr_all_subcategories']  = $arr_all_subcategories;
      $this->arr_view_data['arr_subcategories']      = $arr_subcategories;
      $this->arr_view_data['arr_expert_details']     = $arr_expert_details;

      if(isset($expert_id) && $expert_id !='')
      {
         $this->arr_view_data['check_flag']                    = 1;
         $this->arr_view_data['edit_arr_experts_categories']   = $edit_arr_experts_categories;
         
      }else{
        $this->arr_view_data['check_flag']                     = 0;
      }

      $obj_education = $this->EducationModel->get();

      if($obj_education)
      {
        $arr_education = $obj_education->toArray();
      }

      $obj_currency = $this->CurrencyModel->select('id','currency_code','currency','description')
                                            ->where('is_active','1')
                                            ->get();
      if($obj_currency)
      {
        $arr_currency  = $obj_currency->toArray();
      }
        

      $this->arr_view_data['arr_currency']           = $arr_currency;
      $this->arr_view_data['arr_countries']           = $arr_countries;
      $this->arr_view_data['arr_experts_categories']  = $arr_experts_categories;
      $this->arr_view_data['count_of_categories']     = $count_of_categories;
      $this->arr_view_data['count_of_subcategories']  = $total_subcategories;
      $this->arr_view_data['arr_skills']              = $arr_skills;
      $this->arr_view_data['arr_categories']          = $arr_categories;
      $this->arr_view_data['arr_all_categories']      = $arr_all_categories;
      $this->arr_view_data['arr_education']           = $arr_education;
      $this->arr_view_data['number_of_skills']        = $arr_subscription['number_of_skills'];
      $this->arr_view_data['number_of_categories']    = isset($arr_subscription['subscription_pack_details']['number_of_categories'])?
                                                              $arr_subscription['subscription_pack_details']['number_of_categories']:'';
      $this->arr_view_data['number_of_subcategories'] = isset($arr_subscription['subscription_pack_details']['number_of_subcategories'])?
                                                              $arr_subscription['subscription_pack_details']['number_of_subcategories']:'';

      $this->arr_view_data['arr_lang']                = $this->LanguageService->get_all_language();
      $this->arr_view_data['page_title']              = trans('controller_translations.page_title_expert_profile');
      $this->arr_view_data['module_url_path']         = $this->module_url_path;
      //dd($arr_experts_categories);
      $arr_lang            = $this->LanguageService->get_all_language();
      $arr_professions     = [];
      $obj_profession      = $this->ProfessionModel->where('profession.is_active','1')->with(['translations'])->get();
      if($obj_profession   != FALSE)        {
          $arr_professions = $obj_profession->toArray();
      }
      
      $this->arr_view_data['arr_professions']                = $arr_professions;
      $this->arr_view_data['arr_mangopay_blocked_countries'] = $arr_mangopay_blocked_countries;
      
      return view('expert.profile.index',$this->arr_view_data);
    }

    public function get_subcategory($enc_id)
    {
      $arr_subcategories = [];

      $category_id = $enc_id;

      $obj_subcategories = $this->SubCategoriesModel->where(['is_active'=>'1','category_id'=>$category_id])
                                            ->with(['translations'])->get();
      if($obj_subcategories != FALSE)
      {
          $arr_subcategories = $obj_subcategories->toArray();
      }

      if(isset($arr_subcategories) && is_array($arr_subcategories) && count($arr_subcategories)>0)
      {   
          $html = '';
          foreach($arr_subcategories as $category){
              $html .= '<option value="'.$category['id'].'"> '.$category['subcategory_slug'].' </option>';
          }
          $resp = array('status' => 'success','html'=> $html,'customMsg'=> 'Records found.');
          return response()->json($resp);
      }
      else
      {
          $resp = array('status' => 'fail','customMsg'=> 'Records not found.');
          return response()->json($resp);
      }

    }

    public function edit_subcategory($enc_id,$id)
    {
      $arr_cat_id = $arr_sub_cat_id = [];
      $arr_experts_categories = [];
      $count_of_categories    = '0';
      $count_of_subcategories = '0';
      $total_subcategories    = '0';
      $obj_category_id = $enc_id;
      $user_id         = $this->user_id;

      $obj_experts_categories = $this->ExpertsCategoriesModel
                                                  ->with(['categories','experts_categories_subcategories_details'=>function($query){
                                                    $query->with(['subcategory_traslation']);
                                                  }])
                                                  ->with('is_sub_category_exist')
                                                  ->where('expert_user_id',$user_id)
                                                  ->get();

      if($obj_experts_categories)
      {
          $arr_experts_categories = $obj_experts_categories->toArray();

          $count_of_categories = count($arr_experts_categories);

          if($count_of_categories>0)
          {
            foreach($arr_experts_categories as $key=>$data)
            {
              $count_of_subcategories = $count_of_subcategories + isset($data['experts_categories_subcategories_details']) && count($data['experts_categories_subcategories_details'])>0 ? count($data['experts_categories_subcategories_details']) :'0';

              $total_subcategories = $total_subcategories + $count_of_subcategories;
            }              
          }
      }

      if(isset($user_id) && $user_id !="")
      {
          $edit_arr_experts_categories = [];
          $edit_obj_experts_categories = $this->ExpertsCategoriesModel
                                                      ->with(['categories','experts_categories_subcategories_details'=>function($query){
                                                        $query->with(['subcategory_traslation']);
                                                      }])
                                                      ->with('is_sub_category_exist')
                                                      ->where('expert_user_id',$user_id)
                                                      ->where('id',$id)
                                                      ->first();
          if($edit_obj_experts_categories)
          {
              $edit_arr_experts_categories = $edit_obj_experts_categories->toArray();
              $check_flag = 1;
          }
          $arr_cat_id = \array_diff($arr_cat_id, [$obj_category_id]); 
      }

      if(isset($arr_experts_categories) && count($arr_experts_categories)>0){
        foreach ($arr_experts_categories as $key => $value) 
        {
            if(isset($value['category_id'])){
              $arr_cat_id[] = $value['category_id'];
            }
        }
      }

      $obj_categories = $this->CategoriesModel
                                      ->where('is_active','1')
                                      ->with(['translations'])
                                      ->whereNotIn('id', $arr_cat_id)
                                      ->get();
      
      if($obj_categories != FALSE)
      {
          $arr_categories = $obj_categories->toArray();
      }

      $arr_subcategories = [];

      if(isset($obj_category_id) && $obj_category_id!=null){

        $obj_subcategories = $this->SubCategoriesModel->where(['is_active'=>'1','category_id'=>$obj_category_id])
                                              ->with(['translations'])->get();
        if($obj_subcategories != FALSE)
        {
            $arr_subcategories = $obj_subcategories->toArray();
        }
      }

      $this->arr_view_data['arr_categories']              = $arr_categories;
      $this->arr_view_data['arr_experts_categories']      = $arr_experts_categories;
      $this->arr_view_data['edit_arr_experts_categories'] = $edit_arr_experts_categories;
      $this->arr_view_data['arr_subcategories']           = $arr_subcategories;
      $this->arr_view_data['check_flag']                  = $check_flag;

      return view('expert.profile.ajax_edit_category',$this->arr_view_data);
      //dd($arr_categories);

    }

    public function save_category(Request $request)
    {
      $arr_subcategory = [];
      $catgory_limit   = 0;
      $user_id               = $this->user_id;
      $category_id           = $request->category_id;
      $arr_subcategory       = $request->subcategory;
      $arr_subcategory_count = count($arr_subcategory);
      $stored_subcategory_cnt = 0;

      $arr_experts_categories = [];
      $obj_subscription_count = $this->SubscriptionUsersModel
                                     ->with('subscription_pack_details')
                                     ->where('user_id',$this->user_id)
                                     ->orderBy('id','DESC')
                                     ->limit(1)
                                     ->first();
      if($obj_subscription_count!=FALSE) 
      {
        $arr_subscription = $obj_subscription_count->toArray();
        //dd($arr_subscription);
        $total_subcategories = $arr_subscription['subscription_pack_details']['number_of_subcategories'];
        $catgory_limit       = $arr_subscription['subscription_pack_details']['number_of_categories'];
      }


     /*START of Code to get stored subcategory count*/
      $obj_experts_categories = $this->ExpertsCategoriesModel
                                      ->with(['categories','experts_categories_subcategories_details'=>function($query){
                                        // $query->count('subcategory_id');
                                      }])
                                      ->with('is_sub_category_exist')
                                      ->where('expert_user_id',$user_id)
                                      ->orderBy('id','DESC')
                                      ->get();

      if($obj_experts_categories)
      {
        $arr_experts_categories = $obj_experts_categories->toArray();
      }
      $remainong_cat_count = $catgory_limit - count($arr_experts_categories);

      /*END of Code to get stored subcategory count*/
     

      $total_subcategory_cnt = ($stored_subcategory_cnt + $arr_subcategory_count);
      if($remainong_cat_count >=1)
      {
          $count_success_status = 0;
          if(isset($category_id))
          {
            $obj_status = $this->ExpertsCategoriesModel->create(['expert_user_id'=>$user_id,'category_id'=>$category_id]);
            if($obj_status){
              $count_success_status++;
              $experts_categories_id = isset($obj_status->id) ? $obj_status->id : 0;
              if(isset($arr_subcategory) && sizeof($arr_subcategory)>0)
              {
                  foreach ($arr_subcategory as $key => $value) 
                  {
                      $arr_categories['expert_user_id']        = $user_id;
                      $arr_categories['experts_categories_id'] = $experts_categories_id;
                      $arr_categories['subcategory_id']        = $value;
                    
                      $this->ExpertCategorySubCategoryModel->create($arr_categories);
                      $count_success_status++;
                  }
              }
            }
          }
          if($count_success_status > 0)
          {

                  $obj_experts_categories = $this->ExpertsCategoriesModel
                                                ->with(['categories','experts_categories_subcategories_details'=>function($query){
                                                  $query->with(['subcategory_traslation']);
                                                }])
                                                ->with('is_sub_category_exist')
                                                ->where('expert_user_id',$user_id)
                                                ->orderBy('id','DESC')
                                                ->get();

                  if($obj_experts_categories)
                  {
                      $arr_experts_categories = $obj_experts_categories->toArray();
                  }
                  if(isset($arr_experts_categories) && count($arr_experts_categories)>0){
                    foreach ($arr_experts_categories as $key => $value) 
                    {
                        if(isset($value['category_id'])){
                          $arr_cat_id[] = $value['category_id'];
                        }
                    }
                  }

                  $obj_categories = $this->CategoriesModel
                                      ->where('is_active',1)
                                      ->with(['translations'])
                                      ->whereNotIn('id', $arr_cat_id)
                                      ->get();
      
                  if($obj_categories != FALSE)
                  {
                      $arr_categories = $obj_categories->toArray();
                  }

                  $obj_all_categories = $this->CategoriesModel
                                                  ->where('is_active','1')
                                                  ->with(['translations'])
                                                  ->get();
                  
                  if($obj_all_categories != FALSE)
                  {
                      $arr_all_categories = $obj_all_categories->toArray();
                  }

                  $arr_all_subcategories = [];

                  $obj_all_subcategories = $this->SubCategoriesModel->where('is_active','1')
                                                                    ->with('subcategory_traslation')
                                                                    ->get();
                  if($obj_all_subcategories != FALSE)
                  {
                      $arr_all_subcategories = $obj_all_subcategories->toArray();
                  }

                  $this->arr_view_data['count_of_subcategories'] = $total_subcategories;
                  $this->arr_view_data['arr_all_subcategories']  = $arr_all_subcategories;
                  $this->arr_view_data['arr_all_categories']     = $arr_all_categories;
                  $this->arr_view_data['arr_categories']         = $arr_categories;
                  $this->arr_view_data['arr_experts_categories'] = $arr_experts_categories;
                  $this->arr_view_data['check_flag']             = 0;
                  $this->arr_view_data['count_of_categories']    = count($arr_experts_categories);
                  $this->arr_view_data['number_of_categories']   = isset($catgory_limit)?$catgory_limit:0;
                  $this->arr_view_data['number_of_subcategories']= isset($total_subcategories)?$total_subcategories:0;
                  //dd($this->arr_view_data);
                  return view('expert.profile.ajax_edit_category',$this->arr_view_data);

          }
          else
          {
              $resp['status']  = 'error';
              $resp['message'] = 'Something went wrong please try again';
          }
        
        

      }
      else
      {
               $resp['status']  = 'error';
               $resp['message'] = 'Something went wrong please try again';
               //Session::flash('error', 'You can add only '.$subcatgory_limit.' subcategories'); 
      }
      return $resp;
    }

    public function update_category(Request $request,$enc_id)
    {
      $arr_subcategory = [];
      $catgory_limit   = 0;
      $expert_user_id        = base64_decode($enc_id);

      $user_id               = $this->user_id;
      $category_id           = $request->category_id;
      $arr_subcategory       = $request->subcategory;
      //dd($arr_subcategory);
      $arr_subcategory_count = count($arr_subcategory);
      $stored_subcategory_cnt = 0;


      $arr_experts_categories = [];
      $obj_subscription_count = $this->SubscriptionUsersModel
                                     ->with('subscription_pack_details')
                                     ->where('user_id',$this->user_id)
                                     ->orderBy('id','DESC')
                                     ->limit(1)
                                     ->first();

      if($obj_subscription_count!=FALSE) 
      {
        $arr_subscription = $obj_subscription_count->toArray();
        $total_subcategories = $arr_subscription['subscription_pack_details']['number_of_subcategories'];
        $catgory_limit       = $arr_subscription['subscription_pack_details']['number_of_categories'];
      }
      
       /*START of Code to get stored subcategory count*/
      $obj_experts_categories = $this->ExpertsCategoriesModel
                                      ->with('is_sub_category_exist')
                                      ->with(['categories','experts_categories_subcategories_details'=>function($query){
                                        // $query->count('subcategory_id');
                                      }])
                                      ->where('expert_user_id',$user_id)
                                      ->get();

      if($obj_experts_categories)
      {
        $arr_experts_categories = $obj_experts_categories->toArray();
      }
      //dd($arr_experts_categories);
      //$remainong_cat_count = $catgory_limit - count($arr_experts_categories);
      // if(isset($arr_experts_categories) && sizeof($arr_experts_categories)>0)
      // {
      //    foreach ($arr_experts_categories as $key => $value) 
      //    {
            
      //       if(isset($value['experts_categories_subcategories_details']) && sizeof($value['experts_categories_subcategories_details'])>0)
      //       {

      //         foreach ($value['experts_categories_subcategories_details'] as $key => $in_value)
      //         {
      //           $stored_subcategory_cnt = $stored_subcategory_cnt+1;
      //         }
      //       }
      //    }
      // }

      //$obj_sub_category_count = $this->ExpertCategorySubCategoryModel->where('experts_categories_id',$expert_user_id)
                                                                             //->count();
      /*END of Code to get stored subcategory count*/

      $arr_experts_categories = [];
      //$total_subcategory_cnt = ($stored_subcategory_cnt + $arr_subcategory_count)-$obj_sub_category_count;
      if(1)
      {
    
        $count_success_status = 0;
        if(isset($category_id))
            {
              $obj_category = $this->ExpertsCategoriesModel->where('id',$expert_user_id)
                                                           ->update(['category_id'=>$category_id]);


               if($obj_category){
                $obj_sub_category = $this->ExpertCategorySubCategoryModel->where('experts_categories_id',$expert_user_id)
                                                                         ->delete();
                $count_success_status++;
                $experts_categories_id = isset($expert_user_id) ? $expert_user_id : 0;
                //dd(sizeof($arr_subcategory),'sudarshan');
                if(isset($arr_subcategory) && sizeof($arr_subcategory)>0)
                {
                    foreach ($arr_subcategory as $key => $value) 
                    {
                        $arr_categories['expert_user_id']        = $user_id;
                        $arr_categories['experts_categories_id'] = $experts_categories_id;
                        $arr_categories['subcategory_id']        = $value;
                      
                        $this->ExpertCategorySubCategoryModel->create($arr_categories);
                        $count_success_status++;
                    }
                }
              }
            }
            //dd($count_success_status);
            if($count_success_status > 0)
            {
                $obj_experts_categories = $this->ExpertsCategoriesModel
                                              ->with(['categories','experts_categories_subcategories_details'=>function($query){
                                                $query->with(['subcategory_traslation']);
                                              }])
                                              ->with('is_sub_category_exist')
                                              ->where('expert_user_id',$user_id)
                                              ->orderBy('id','DESC')
                                              ->get();

                if($obj_experts_categories)
                {
                    $arr_experts_categories = $obj_experts_categories->toArray();
                }
                //dd($arr_experts_categories);
                if(isset($arr_experts_categories) && count($arr_experts_categories)>0){
                  foreach ($arr_experts_categories as $key => $value) 
                  {
                      if(isset($value['category_id'])){
                        $arr_cat_id[] = $value['category_id'];
                      }
                  }
                }

                //dd($arr_experts_categories);
                $obj_categories = $this->CategoriesModel
                                      ->where('is_active',1)
                                      ->with(['translations'])
                                      ->whereNotIn('id', $arr_cat_id)
                                      ->get();
      
                  if($obj_categories != FALSE)
                  {
                      $arr_categories = $obj_categories->toArray();
                  }

                  $obj_all_categories = $this->CategoriesModel
                                                  ->where('is_active','1')
                                                  ->with(['translations'])
                                                  ->get();
                  
                  if($obj_all_categories != FALSE)
                  {
                      $arr_all_categories = $obj_all_categories->toArray();
                  }

                // $arr_all_subcategories = [];

                // $obj_all_subcategories = $this->SubCategoriesModel->where('is_active','1')
                //                                                   ->with('subcategory_traslation')
                //                                                   ->get();
                // if($obj_all_subcategories != FALSE)
                // {
                //     $arr_all_subcategories = $obj_all_subcategories->toArray();
                // }
                
                // //dd($arr_all_subcategories);
                // $this->arr_view_data['arr_all_subcategories']  = $arr_all_subcategories;
                $this->arr_view_data['arr_categories']         = $arr_categories;
                $this->arr_view_data['arr_all_categories']     = $arr_all_categories;
                $this->arr_view_data['arr_experts_categories'] = $arr_experts_categories;
                $this->arr_view_data['count_of_categories']    = count($arr_experts_categories);
                $this->arr_view_data['check_flag']             = 0;
                
                $this->arr_view_data['count_of_subcategories'] = $total_subcategories;
                $this->arr_view_data['number_of_categories']   = isset($catgory_limit)?$catgory_limit:0;
                $this->arr_view_data['number_of_subcategories']= isset($total_subcategories)?
                                                                       $total_subcategories:0;

                return view('expert.profile.ajax_edit_category',$this->arr_view_data);                    
            }
            else
            {
                $resp['status']  = 'error';
                $resp['message'] = 'Something went wrong please try again';
            }
          
          
      }
      else
      {
            $resp['status']  = 'error';
            $resp['message'] = 'Something went wrong please try again';
            //Session::flash('error', 'You can add only '.$subcatgory_limit.' subcategories');
      }
    return $resp;

    }

    public function update(Request $request)
    {   
        $arr_rules = array();
        $status = FALSE;
 
        // $arr_rules['first_name']    = "required|alpha|max:255";
        // $arr_rules['last_name']     = "required|alpha|max:255";
        $arr_rules['first_name']    = "required|max:255";
        $arr_rules['last_name']     = "required|max:255";

        $arr_rules['phone_number']  = "required|max:17|min:7";
        if(isset($request->user_name)){
           // $arr_rules['user_name']     = "required|alpha|max:32|min:3";
            $arr_rules['user_name']     = "required|max:32|min:3";
        }
        $arr_rules['country']       = "required";
        $arr_rules['phone_code']    = "required|min:1|max:6";
        $arr_rules['zip']           = "required|min:4";
        //$arr_rules['vat_number']    = "required|max:20";
        $arr_rules['address']       = "required|max:450";
        $arr_rules['company_name']  = "required";
        $arr_rules['timezone']      = "required";
        $arr_rules['profession']    = "required";
        $arr_rules['hourly_rate']   = "required";
        $arr_rules['email']         = "required|email";
        
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = array();
        $form_data = $request->all();

        $arr_designation       = [];
        $arr_work_company_name = [];
        $arr_from_year         = [];
        $arr_to_year           = [];

        $arr_certification_name = [];
        $arr_completion_year    = [];

        $obj_subscription = $this->SubscriptionUsersModel
                              ->where('user_id',$this->user_id)
                              ->orderBy('id','DESC')
                              ->limit(1)
                              ->first();

        if ($obj_subscription!=FALSE) {
          $arr_subscription = $obj_subscription->toArray();
        }
        $obj_expert = $this->ExpertsModel->with(['user_details'])->where('user_id',$this->user_id)->first();
        if($obj_expert && sizeof($obj_expert) > 0 && isset($arr_subscription) && sizeof($arr_subscription)>0)
        {
          $arr_expert = array();

          if (isset($form_data['skills']) && sizeof($form_data['skills'])>0 && isset($arr_subscription['number_of_skills']) && $arr_subscription['number_of_skills']<sizeof($form_data['skills']))  
          {
            $error_msg = trans('controller_translations.erro_expert_profile_skill',array('number_skill'=>$arr_subscription['number_of_skills'],'subscription_link'=>url('/expert/subscription')));
            Session::flash('error',$error_msg);
            return redirect()->back();
          }

          if (isset($form_data['categories']) && sizeof($form_data['categories'])>0 && isset($arr_subscription['number_of_categories']) && $arr_subscription['number_of_categories']<sizeof($form_data['categories']))  
          {
            $error_msg = trans('controller_translations.erro_expert_profile_category',array('number_cate'=>$arr_subscription['number_of_categories'],'subscription_link'=>url('/expert/subscription')));
            Session::flash('error',$error_msg);
            return redirect()->back();
          }

          $profile_show_image_name = "";     
          if(isset($form_data['profile_image']) && $form_data['profile_image']!=FALSE)
          {    
              if ($request->hasFile('profile_image')) 
              {
                $img_valiator = Validator::make(array('image'=>$request->file('profile_image')),array(
                                                'image' => 'mimes:png,jpeg,jpg')); 

                  if ($request->file('profile_image')->isValid() && $img_valiator->passes())
                  {
                      $profile_image_name = $form_data['profile_image'];
                      $fileExtension = strtolower($request->file('profile_image')->getClientOriginalExtension()); 

                      if($fileExtension == 'png' || $fileExtension == 'jpg' || $fileExtension == 'jpeg')
                      {
                          $profile_image_name = sha1(uniqid().$profile_image_name.uniqid()).'.'.$fileExtension;

                          $request->file('profile_image')->move($this->profile_img_base_path, $profile_image_name);
                          $arr_expert['profile_image']           = $profile_image_name;
                          $arr_expert['show_profile_image_name'] = $request->file('profile_image')->getClientOriginalName();

                          //unlink exiting image
                          if ($profile_image_name && isset($obj_expert->profile_image) && $obj_expert->profile_image!="") 
                          {
                              if ($obj_expert->profile_image!='defaul_profile_image.png') 
                              {
                                  $file_exits = file_exists($this->profile_img_base_path.$obj_expert->profile_image);

                                  if ($file_exits) 
                                  {
                                      unlink($this->profile_img_base_path.$obj_expert->profile_image);
                                  }
                              }
                          }
                      }
                      else
                      {
                           Session::flash('error',trans('controller_translations.error_invalid_file_extension_for_profile_image') );
                           return back()->withInput();
                      }
                  }
                  else
                  {
                       Session::flash("error", trans('controller_translations.error_please_upload_valid_image'));
                       return back()->withErrors($validator)->withInput($request->all());
                  }
                      
              }
          }


          $arr_expert['first_name']    = ucfirst($form_data['first_name']);
          $arr_expert['last_name']     = ucfirst($form_data['last_name']);
          $arr_expert['phone_code']    = $form_data['phone_code'];
          $arr_expert['phone_number']  = $form_data['phone_number'];
          $arr_expert['country']       = $form_data['country'];
          if(isset($request->user_name)){
          $arr_expert['user_name']     = $form_data['user_name'];
          }
          $arr_expert['state']         = $form_data['state'];
          $arr_expert['city']          = $form_data['city'];
          $arr_expert['zip']           = $form_data['zip'];
          $arr_expert['vat_number']    = $form_data['vat_number'];
          $arr_expert['address']       = $form_data['address'];
          $arr_expert['profession']    = $form_data['profession'];
          $arr_expert['hourly_rate']   = $form_data['hourly_rate'];
          $arr_expert['education_id']  = $form_data['education'];
          
          if(empty($form_data['lat']) && $form_data['lat'] != "" && empty($form_data['long']) && $form_data['long'] != ""){
              // get lat long using php if jquery lat long fail
              $address = $form_data['address'];
              // Get JSON results from this request
              $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?key='.env('GOOGLEAPIKEY').'&address='.urlencode($address).'&sensor=false');
              // Convert the JSON to an array
              $geo = json_decode($geo, true);
              if ($geo['status'] == 'OK') {
               // Get Lat & Long
               $latitude    = $geo['results'][0]['geometry']['location']['lat'];
               $longitude   = $geo['results'][0]['geometry']['location']['lng'];
               //$city        = $geo['results']['0']['address_components']['2']['long_name'];
              }
          } else {
            $latitude    = $form_data['lat'];
            $longitude   = $form_data['long'];
          }

          $arr_expert['latitude']      = $latitude;
          $arr_expert['longitude']     = $longitude;
          if (isset($form_data['spoken_languages']) && sizeof($form_data['spoken_languages'])) 
          {  
              $this->ExpertsSpokenLanguagesModel->where('expert_user_id',$this->user_id)->delete();

              foreach ($form_data['spoken_languages'] as $_key => $language)
              { 
                /* storing language details in seperate table */
                $this->ExpertsSpokenLanguagesModel->create([
                                                            'expert_user_id' => $this->user_id,
                                                            'language' => $language
                                                           ]);
              }
          }

          $arr_expert['timezone']      = $form_data['timezone'];
          $arr_expert['about_me']      = $form_data['about_me'];
          $arr_expert['company_name']  = $form_data['company_name'];
          
          $status = $obj_expert->update($arr_expert);

          $user_data = [];
          $user_data['mp_user_id'] = isset($obj_expert->user_details->mp_user_id)?$obj_expert->user_details->mp_user_id:'';


          if(isset($request->user_name)){
          $this->UserModel->where('id',$this->user_id)->update(['user_name'=>$form_data['user_name']]);
          }
          $currency_code     = $form_data['currency_code'];
          $old_currency_code = $form_data['old_currency_code'];
          $this->UserModel->where('id',$this->user_id)->update(['currency_code'=>$currency_code]);

          if(isset($obj_expert->user_details->mp_wallet_created) && $obj_expert->user_details->mp_wallet_created!='' && $obj_expert->user_details->mp_wallet_created == 'No')
          {
            $this->create_expert_wallet();
          }

           if (isset($form_data['skills']) && sizeof($form_data['skills'])>0) 
           {
              $this->ExpertsSkillModel->where('expert_user_id',$this->user_id)->delete();

              foreach ($form_data['skills'] as $key => $skill) 
              {
                if (isset($skill) && $skill!="") 
                {
                  $this->ExpertsSkillModel->create(['expert_user_id'=>$this->user_id,'skill_id'=>$skill]);
                }
                  
              }
           }

           if (isset($form_data['categories']) && sizeof($form_data['categories'])>0) 
           {
              $this->ExpertsCategoriesModel->where('expert_user_id',$this->user_id)->delete();

              foreach ($form_data['categories'] as $key => $category) 
              {
                if (isset($category) && $category!="") 
                {
                  $this->ExpertsCategoriesModel->create(['expert_user_id'=>$this->user_id,'category_id'=>$category]);
                }
                  
              }
           }

            $arr_designation       = isset($form_data['designation']) ? $form_data['designation'] : null;
            $arr_work_company_name = isset($form_data['work_company_name']) ? $form_data['work_company_name'] : null;
            $arr_from_year         = isset($form_data['from_year']) ? $form_data['from_year'] : null;
            $arr_to_year           = isset($form_data['to_year']) ? $form_data['to_year'] : null;

            if(isset($arr_designation) && count($arr_designation)>0 && isset($arr_work_company_name) && count($arr_work_company_name)>0 && isset($arr_from_year) && count($arr_from_year)>0 && isset($arr_to_year) && count($arr_to_year)>0)
            {
              $this->ExpertWorkExperienceModel->where('expert_user_id',$this->user_id)->delete();
              $count = count($arr_designation);

              for($i=0;$i<$count;$i++)
              {
                if($arr_designation[$i]!='' && $arr_work_company_name[$i]!='' && $arr_from_year[$i]!='' && $arr_to_year[$i]!='')
                {
                  $arr_work_details['expert_user_id'] = $this->user_id;
                  $arr_work_details['designation']    = isset($arr_designation[$i]) ? $arr_designation[$i] : '';
                  $arr_work_details['company_name']   = isset($arr_work_company_name[$i]) ? $arr_work_company_name[$i] : '';
                  $arr_work_details['from_year']      = isset($arr_from_year[$i]) ? $arr_from_year[$i] : '';
                  $arr_work_details['to_year']        = isset($arr_to_year[$i]) ? $arr_to_year[$i] : '';

                  $success = $this->ExpertWorkExperienceModel->create($arr_work_details); 
                }
              }
            }


            $arr_certification_name   = isset($form_data['certification_name']) ? $form_data['certification_name'] : null;
            $arr_completion_year      = isset($form_data['completion_year']) ? $form_data['completion_year'] : null;
            $arr_expire_year          = isset($form_data['expire_year']) ? $form_data['expire_year'] : null;

            //dd($arr_certification_name);

            if(isset($arr_certification_name) && count($arr_certification_name)>0 && isset($arr_completion_year) && count($arr_completion_year)>0)
            {
              $this->ExpertCertificationDetailsModel->where('expert_user_id',$this->user_id)->delete();
              $count = count($arr_certification_name);

              for($i=0;$i<$count;$i++)
              {
                if($arr_certification_name[$i]!='' && $arr_completion_year[$i]!='')
                {
                  $arr_certification_details['expert_user_id'] = $this->user_id;
                  $arr_certification_details['certification_name']    = isset($arr_certification_name[$i]) ? $arr_certification_name[$i] : '';
                  $arr_certification_details['completion_year']   = isset($arr_completion_year[$i]) ? date('Y-m-d',strtotime($arr_completion_year[$i])) : '';
                  $arr_certification_details['expire_year']   = isset($arr_expire_year[$i]) ? date('Y-m-d',strtotime($arr_expire_year[$i])) : '';
                  //dd($arr_certification_details);
                  $success = $this->ExpertCertificationDetailsModel->create($arr_certification_details); 
                }
              }
            }

            if(isset($form_data['email']) && isset($form_data['old_email']) && $form_data['email'] != $form_data['old_email'])
            {
              $new_email = $form_data['email'];
              $this->UserModel->where('id',$this->user_id)->update(['email'=>$new_email,'is_email_changed'=>'1','is_email_verified'=>'0']);

              $user                      = Sentinel::findById($this->user_id);
              $activation                = Activation::create($user); /* Create avtivation */
              $activation_code           = $activation->code; // get activation code
              $email_id                  = $new_email;
              $data['user_id']           = $this->user_id;
              $data['activation_code']   = base64_encode($activation_code);
              $data['name']              = ucfirst($obj_expert->first_name);//ucfirst($form_data['first_name']);
              $data['email_id']          = $new_email;
              $data['confirmation_link'] = url('/').'/confirm_email/'.base64_encode($this->user_id).'/'.base64_encode($activation_code);
              $project_name = config('app.project.name');
              //$mail_form = isset($website_contact_email)?$website_contact_email:'support@archexperts.com';
              $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
              try{

                  $mail_status = $this->MailService->send_change_email_confirmation_email($data);
                  // Mail::send('front.email.change_email_confirmation', $data, function ($message) use ($email_id,$mail_form,$project_name) {
                  //           $message->from($mail_form, $project_name);
                  //           $message->subject($project_name.':Email Confirmation.');
                  //           $message->to($email_id);
                  // });
              }
              catch(\Exception $e)
              {
                Session::Flash('error',trans('controller_translations.text_mail_not_sent'));
              }

             
              $user_data['email'] = $new_email;

              

              Session::flash('success', trans('controller_translations.success_email_changed_successfully_please_check_your_email_account_for_confirmation_link'));
              Sentinel::logout();
              return redirect(url('/login'));
            }
        }
        else
        {
          Session::flash('error',trans('controller_translations.error_please_upgrade_your_subscription_plan') );
          return redirect()->back();
        }

        if ($status) 
        {
            $user_data['first_name'] = ucfirst($form_data['first_name']);
            $user_data['last_name']  = ucfirst($form_data['last_name']);

            $user_type = isset($obj_expert->user_type)?$obj_expert->user_type:'';

            if(isset($user_type) && $user_type!='' && $user_type == 'private')
            {
              $this->WalletService->update_user($user_data); 
            }
            else
            {
              $this->WalletService->update_legal_user($user_data); 
            }


            Session::flash('success',trans('controller_translations.success_profile_details_updated_successfully') );
        }
        else
        {
            Session::flash('error',trans('controller_translations.error_error_while_updating_profile_details') );
        }
        
        return redirect()->back();
    }

    public function delete_category($category_id)
    {
      $arr_cat_id = [];
      if($category_id!='')
      {
        $status = $this->ExpertsCategoriesModel->where('category_id',$category_id)
                                               ->where('expert_user_id',$this->user_id)
                                               ->delete();
        $status1 = $this->ExpertCategorySubCategoryModel->where('experts_categories_id',$category_id)                                              ->where('expert_user_id',$this->user_id)
                                                        ->delete();
        //if($status && $status1)
        if(1)
        {
          
          $obj_experts_categories = $this->ExpertsCategoriesModel
                                                  ->with(['categories','experts_categories_subcategories_details'=>function($query){
                                                    $query->with(['subcategory_traslation']);
                                                  }])
                                                  ->with('is_sub_category_exist')
                                                  ->where('expert_user_id',$this->user_id)
                                                  ->orderBy('id','DESC')
                                                  ->get();

          if($obj_experts_categories)
          {
              $arr_experts_categories = $obj_experts_categories->toArray();
          }

          if(isset($arr_experts_categories) && count($arr_experts_categories)>0){
            foreach ($arr_experts_categories as $key => $value) 
            {
                if(isset($value['category_id'])){
                  $arr_cat_id[] = $value['category_id'];
                }
            }
          }
          $obj_subscription_count = $this->SubscriptionUsersModel
                                     ->with('subscription_pack_details')
                                     ->where('user_id',$this->user_id)
                                     ->orderBy('id','DESC')
                                     ->limit(1)
                                     ->first();

          if($obj_subscription_count!=FALSE) 
          {
            $arr_subscription    = $obj_subscription_count->toArray();
            $total_subcategories = $arr_subscription['subscription_pack_details']['number_of_subcategories'];
            $catgory_limit       = $arr_subscription['subscription_pack_details']['number_of_categories'];
          }


          $obj_categories = $this->CategoriesModel
                                      ->where('is_active',1)
                                      ->with(['translations'])
                                      ->whereNotIn('id', $arr_cat_id)
                                      ->get();
      
          if($obj_categories != FALSE)
          {
              $arr_categories = $obj_categories->toArray();
          }

          $obj_all_categories = $this->CategoriesModel
                                          ->where('is_active','1')
                                          ->with(['translations'])
                                          ->get();
          
          if($obj_all_categories != FALSE)
          {
              $arr_all_categories = $obj_all_categories->toArray();
          }

          $arr_all_subcategories = [];

          $obj_all_subcategories = $this->SubCategoriesModel->where('is_active','1')
                                                            ->with('subcategory_traslation')
                                                            ->get();
          if($obj_all_subcategories != FALSE)
          {
              $arr_all_subcategories = $obj_all_subcategories->toArray();
          }

          $this->arr_view_data['arr_all_subcategories']  = $arr_all_subcategories;
          $this->arr_view_data['arr_categories']         = $arr_categories;
          $this->arr_view_data['arr_all_categories']     = $arr_all_categories;
          $this->arr_view_data['arr_experts_categories'] = $arr_experts_categories;
          $this->arr_view_data['count_of_categories']    = count($arr_experts_categories);
          $this->arr_view_data['check_flag']             = 0;

          $this->arr_view_data['count_of_subcategories'] = $total_subcategories;
          $this->arr_view_data['number_of_categories']   = isset($catgory_limit)?$catgory_limit:0;
          $this->arr_view_data['number_of_subcategories']= isset($total_subcategories)?
                                                                 $total_subcategories:0;


          return view('expert.profile.ajax_edit_category',$this->arr_view_data);


          //Session::flash("success",'Category & Sub categories deleted successfully');
        }
      }
      else
      {
        Session::flash("error",trans('controller_translations.error_unauthorized_action'));
      }
      //return redirect()->back();
    }

   /*
    | Change pass : load view for change password
    | auther :Sagar Sainkar
    */
    public function change_password()
    {
      $this->arr_view_data['page_title'] = trans('controller_translations.page_title_expert_change_password');
      return view('expert.profile.change_password',$this->arr_view_data);
    }

    /*
    | description : validate and update expert password
    | auther :Sagar Sainkar
    */ 

    public function update_password(Request $request)
    {
        $arr_rules = array();
        $arr_rules['current_password'] = "required";
        $arr_rules['password']         = 'required|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/';
        $arr_rules['confirm_password'] = 'required|same:password|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/';

        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        
        $user = Sentinel::check();
        
        $credentials = array();
        $password = trim($request->input('current_password'));
        $credentials['email'] = $user->email;        
        $credentials['password'] = $password;

        if (Sentinel::validateCredentials($user,$credentials)) 
        { 
          $new_credentials = [];
          $new_credentials['password'] = $request->input('password');

          if(Sentinel::update($user,$new_credentials))
          {
            Session::flash('success', trans('controller_translations.success_password_changed_successfully'));
          }
          else
          {
            Session::flash('error',trans('controller_translations.error_problem_occured_while_changing_password') );
          }          
        } 
        else
        {
          Session::flash('error', trans('controller_translations.error_your_current_password_is_invalid'));          
        }       
        
        return redirect()->back(); 
    }

    /*comment: load page of availability
    Author: Ashwini K*/


    public function availability()
    {
      $arr_expert_details = array();

      $obj_expert = $this->UserModel->where('id',$this->user_id)->first(['is_available']);

      if ($obj_expert!=FALSE) 
      {
          $arr_expert_details = $obj_expert->toArray();
      }


      $this->arr_view_data['arr_lang'] = $this->LanguageService->get_all_language();
      $this->arr_view_data['page_title'] = trans('controller_translations.page_title_expert_availability');
      $this->arr_view_data['arr_expert_details'] = $arr_expert_details;
      $this->arr_view_data['module_url_path'] = $this->module_url_path;

      return view('expert.availability.index',$this->arr_view_data);
    }


    /*comment: update availability
    Author: Ashwini K*/
    

    public function update_availability(Request $request)
    {

        $arr_rules = array();
        $status = false;
        $arr_rules['user_availability']= "required";

        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = array();
        $form_data = $request->all();
        //dd($form_data);
        if(isset($form_data['user_availability']))
        {    
              
          $obj_expert = $this->UserModel->where('id',$this->user_id)->first();
          if($obj_expert!=FALSE)
          {
            if ($form_data['user_availability']==1) 
            {
              $status = $obj_expert->update(['is_available'=>1]);
            }
            elseif ($form_data['user_availability']==0) 
            {
              $status = $obj_expert->update(['is_available'=>0]); 
            }
             
          }
          if($status!=false) 
          {
            Session::flash('success',trans('controller_translations.success_availability_updated_successfully'));
          }
          else
          {
            Session::flash('error',trans('controller_translations.error_error_while_updating_availability'));
          }
        }

        return redirect()->back(); 
    }   
    /* 
      Comments : Delete images from database and from folder.
      Auther   : Nayan S.
    */
    public function delete_image(Request $request)
    {
        $arr_expert = $json = [];
        $image_type = $request->input('image_type');

        if($image_type != "")
        { 
          $obj_expert = ExpertsModel::where('user_id',$this->user_id)->first(['id','user_id','profile_image','cover_image']);
          if($obj_expert)
          {
            $arr_expert = $obj_expert->toArray();  
            if($image_type == "profile")
            {
              $profile_image = isset($arr_expert['profile_image'])?trim($arr_expert['profile_image']):'';
              if($profile_image != "")
              {
                $result = ExpertsModel::where('user_id',$this->user_id)->update(['profile_image'=>'','show_cover_image_name'=>'']);
                if($result)
                {
                  $json['status'] = 'SUCCESS_PROFILE';
                  $json['msg']  = trans('controller_translations.msg_profile_image_deleted_successfully');
                  @unlink($this->profile_img_base_path.$profile_image);
                }
              }
            }
            else if($image_type == "cover")
            {
              $cover_image = isset($arr_expert['cover_image'])?trim($arr_expert['cover_image']):'';
              if($cover_image != "")
              {
                $result = ExpertsModel::where('user_id',$this->user_id)->update(['cover_image'=>'','show_cover_image_name'=>'']);
                if($result)
                {
                  $json['status'] = 'SUCCESS_COVER';
                  $json['msg']  = trans('controller_translations.msg_cover_image_deleted_successfully');
                  @unlink($this->cover_img_base_path.$cover_image); 
                }
              }
            }
          }
        }
        return response()->json($json);
    }
    public function store_profile_image(Request $request)
    {
        $image_upload  = FALSE;
        $arr_response  = [];
        $user_id       = Sentinel::getUser()->id;
        $obj_user      = $this->ExpertsModel->where('user_id','=',$user_id)->first(['id','user_id','profile_image']);
        
        if($obj_user){
          $profile_image_name =  $obj_user->profile_image;
        }
        if($request->hasFile('file')) 
        {
            $image_validation = Validator::make(array('file'=>$request->file('file')),
                                                array('file'=>'mimes:jpg,jpeg,png'));

            if($request->file('file')->isValid() && $image_validation->passes())
            {

                $image_path              = $request->file('file')->getClientOriginalName();
                $image_extention         = $request->file('file')->getClientOriginalExtension();
                $image_name              = sha1(uniqid().$image_path.uniqid()).'.'.$image_extention;
                $final_image = $request->file('file')->move($this->profile_img_base_path , $image_name);

                if(isset($profile_image_name) && $profile_image_name != "")
                {   
                  if($image_path != "default_profile_image.png")
                  {
                    @unlink($this->profile_img_base_path.'/'.$profile_image_name);
                  }
                }

                $image_upload = TRUE; 
            }
            else
            {
                $arr_response['status'] =   "ERROR";
                $arr_response['msg']    =   trans('controller_translations.file_is_not_an_image_file');
                return response()->json($arr_response);
            }
        }
        if( $image_upload  == TRUE )
        {
            $upload_img = []; 
            $upload_img['profile_image']           = $image_name;
            $obj_update  = $this->ExpertsModel->where('user_id','=',$user_id)->update($upload_img);
            if($obj_update)
            {
                $arr_response['status']     =   "SUCCESS";
                $arr_response['image_name'] =   $this->profile_img_public_path.$image_name;
                $arr_response['msg']        =   trans('controller_translations.profile_image_saved_successfully');
            }
            else
            {   
                $arr_response['status'] =   "ERROR";
                $arr_response['msg']    =   trans('controller_translations.error_saving_file');
            }
            return response()->json($arr_response);
        }
    }

    function GetRandomString($country_string,$user_type_string,$first_string) 
    { 
      $user = Sentinel::check();
      $user_type_string = isset($user_type_string)?substr($user_type_string,0,3):'';
      $first_string     = isset($first_string)?substr($first_string, 0, 3):'';


      $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
      $randomString = ''; 
    
      for ($i = 0; $i < 4; $i++) { 
          $index = rand(0, strlen($characters) - 1); 
          $randomString .= $characters[$index]; 
      } 
      
      $final_string = $country_string.''.$user_type_string.''.$first_string.''.$randomString;
      //dd($final_string);
      if(isset($user->id) && $user->id != ""){
        $user_count = $this->UserModel->where('user_name',$final_string)->where('id','!=',$user->id)->count();
      }
      else{
        $user_count = $this->UserModel->where('user_name',$final_string)->count();  
      }

      if ($user_count > 0) 
      {
          $this->GetRandomString($country_string,$user_type_string,$first_string);
      }
      else
      {
          return $final_string;
      }

    }
    
    public function create_expert_wallet()
    {
      $arr_currency = [];

      $obj_currency = $this->CurrencyModel->select('id','currency_code')->get();
      if($obj_currency)
      {
        $arr_currency = $obj_currency->toArray();
      }

      if(isset($this->user))
      {
        $logged_user  = $this->user->toArray();  
        if(1)//(!empty($logged_user['currency_code']))
        {
          $obj_expert_deatils = $this->ExpertsModel->where('user_id',$this->user_id)->first();
          
          $user_type = isset($obj_expert_deatils->user_type)?$obj_expert_deatils->user_type:'';
          $company_name = isset($obj_expert_deatils->company_name)?$obj_expert_deatils->company_name:'';

          $first_name   = "-";
          $last_name    = "-";
          $email        = "-";
          if(isset($logged_user['role_info']['first_name']) && $logged_user['role_info']['first_name'] !="")
          {
            $first_name = $logged_user['role_info']['first_name'];
          }
          if(isset($logged_user['role_info']['last_name']) && $logged_user['role_info']['last_name'] !="")
          {
            $last_name = $logged_user['role_info']['last_name'];
          }
          if(isset($logged_user['email']) && $logged_user['email'] !=""){
            $email = $logged_user['email'];
          }
          try
          {
            $timezoneinfo = file_get_contents('http://ip-api.com/json/' . $_SERVER['REMOTE_ADDR']);
          }catch(\Exception $e)
          {
            $timezoneinfo = file_get_contents('http://ip-api.com/json');  
          } 
          $mp_user_data = [];
          //Create account in mangopay and create wallet Start.......
            $mp_user_data['FirstName']          = $first_name;
            $mp_user_data['LastName']           = $last_name;
            $mp_user_data['Email']              = $email;
            $mp_user_data['CountryOfResidence'] = isset($timezoneinfo->countryCode)?$timezoneinfo->countryCode:"IN";
            $mp_user_data['Nationality']        = isset($timezoneinfo->countryCode)?$timezoneinfo->countryCode:"IN";

            // Register user on MangoPay as well
            if(isset($user_type) && $user_type!='' && $user_type == 'private')
            {
              $Mangopay_create_user =  $this->WalletService->create_user_with_all_currency_wallet($mp_user_data,$arr_currency,$this->user->id);
            }

            if(isset($user_type) && $user_type!='' && $user_type == 'business')
            {
              $mp_user_data['company_name'] = $company_name;
              $Mangopay_create_user =  $this->WalletService->create_legal_user($mp_user_data,$arr_currency,$this->user->id);
            }

            if($Mangopay_create_user)
            {
                return true;
            } 
            else{
              return false;
            } 
        }
        else
        {
          return false;
        }
      } 
      else 
      {
        return false;
      }
    } 

}
