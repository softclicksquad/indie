<?php

namespace App\Http\Controllers\Front\Expert;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\SubscriptionPacksModel;
use App\Models\SubscriptionUsersModel; 
use App\Models\TransactionsModel;
use App\Models\ProjectsBidsModel;
use App\Models\ProjectsBidsTopupModel;
use App\Models\ExpertsCategoriesModel;
use App\Models\ExpertsSkillModel;
use App\Models\UserWalletModel;
use App\Models\UserModel;
use App\Models\NotificationsModel;
use App\Models\CurrencyModel;
use App\Models\ExpertsModel;

use App\Models\FavouriteProjectsModel;
use App\Common\Services\WalletService;
use App\Common\Services\MailService;

use Validator;
use Session;
use Sentinel;
use Lang;
use Redirect;
use App\Common\Services\PaymentService;

class SubscriptionExpertController extends Controller
{
    public $arr_view_data;
    public function __construct(
                                SubscriptionPacksModel $subscription_packs, 
                                SubscriptionUsersModel $user_subscription,
                                TransactionsModel $transaction,
                                ProjectsBidsModel $project_bids,
                                ProjectsBidsTopupModel $topup_bids,
                                ExpertsCategoriesModel $expert_category,
                                ExpertsSkillModel $expert_skills,
                                FavouriteProjectsModel $expert_favourite,
                                WalletService $WalletService,
                                MailService $MailService
                                )
    { 
      if(! $user = Sentinel::check()) 
      {
        return redirect('/login');
      }
      $this->user                   = $user;
      $this->user_id                = $user->id;
      $this->SubscriptionPacksModel = $subscription_packs;
      $this->SubscriptionUsersModel = $user_subscription;
      $this->TransactionsModel      = $transaction;
      $this->ProjectsBidsModel      = $project_bids;
      $this->ProjectsBidsTopupModel = $topup_bids;
      $this->ExpertsCategoriesModel = $expert_category;
      $this->ExpertsSkillModel      = $expert_skills;
      $this->FavouriteProjectsModel = $expert_favourite;
      $this->WalletService          = $WalletService;
      $this->MailService            = $MailService;
      $this->UserWalletModel        = new UserWalletModel;
      $this->UserModel              = new UserModel;
      $this->NotificationsModel     = new NotificationsModel;
      $this->CurrencyModel          = new CurrencyModel;
      $this->ExpertsModel           = new ExpertsModel;
      $this->arr_view_data          = [];
      $this->module_url_path        = url("/expert/subscription");

      if(isset($user)){
        $logged_user                 = $user->toArray();  
        if($logged_user['mp_wallet_created'] == 'Yes'){
          $this->mp_user_id          = $logged_user['mp_user_id'];
          $this->mp_wallet_id        = $logged_user['mp_wallet_id'];
          $this->mp_wallet_created   = $logged_user['mp_wallet_created'];
        } else {
          $this->mp_user_id          = '';
          $this->mp_wallet_id        = '';
          $this->mp_wallet_created   = 'No';
        }
      } else {
        $this->mp_user_id          = '';
        $this->mp_wallet_id        = '';
        $this->mp_wallet_created   = 'No';
      }
    }
    /*
    	Comment: load page of subsciption packs
    	Author: Sagar Sainkar
    */
    public function index()
    {
      $arr_subscription = array();
      $arr_packs = array();
      $arr_monthly_packs = array();
      $arr_quarterly_packs = array();
      $arr_yearly_packs = array();
      $is_starter_plan = 0;
      $bids_count  = $topup_bids = 0;

      $obj_monthly_packs = $this->SubscriptionPacksModel->where('is_active',1)->where('type','=','MONTHLY')->get();
      if($obj_monthly_packs != FALSE)
      {
          $arr_monthly_packs = $obj_monthly_packs->toArray();
      }

      $obj_quarterly_packs = $this->SubscriptionPacksModel->where('is_active',1)->where('type','=','QUARTERLY')->get();
      if($obj_quarterly_packs != FALSE)
      {
          $arr_quarterly_packs = $obj_quarterly_packs->toArray();
      }

      $obj_yearly_packs = $this->SubscriptionPacksModel->where('is_active',1)->where('type','=','YEARLY')->get();
      if($obj_yearly_packs != FALSE)
      {
          $arr_yearly_packs = $obj_yearly_packs->toArray();
      }
      
      $obj_subscription = $this->SubscriptionUsersModel
                               ->where('user_id',$this->user_id)
                               ->where('is_active','1')
                               ->orderBy('id','DESC')
                               ->limit(1)
                               ->with(['transaction_details','subscription_pack_details'])
                               ->whereHas('transaction_details',function ($query){
                                $query->whereIN('payment_status',array('1','2'));
                               })->first();

      if($obj_subscription != FALSE)
      {
          $arr_subscription = $obj_subscription->toArray();
      }
      
      if(isset($arr_subscription['subscription_pack_id']) && $arr_subscription['subscription_pack_id'] == '1')
      { 
        $is_starter_plan = 1;
      }  
      //dd($arr_subscription);
      /*
      if(isset($is_starter_plan) && isset($arr_subscription) && sizeof($arr_subscription) > 0)
      {
        $bids_count = $this->ProjectsBidsModel->where('expert_user_id',$this->user_id)
                                              ->whereRaw('DATE_FORMAT(created_at,"%Y-%m-%d %h:%i") BETWEEN DATE_FORMAT("'.$arr_subscription['created_at'].'","%Y-%m-%d %h:%i") AND DATE_FORMAT("'.$arr_subscription['expiry_at'].'","%Y-%m-%d %h:%i")')
                                              ->count();
      } 
      elseif (isset($arr_subscription) && sizeof($arr_subscription) > 0) 
      {*/
        // $bids_count = $this->ProjectsBidsModel->where('expert_user_id',$this->user_id)
        //                                       ->where('created_at','>=',$arr_subscription['created_at'])
        //                                       ->orderBy('id','DESC')
        //                                       ->count();

        if(isset($arr_subscription) && count($arr_subscription)>0)
        {
          $bids_count = $this->ProjectsBidsModel->where('expert_user_id',$this->user_id)
                                                ->where('created_at','>=',$arr_subscription['created_at'])
                                                ->orderBy('id','DESC')
                                                ->count();
        }
        else
        {
          $bids_count = $this->ProjectsBidsModel->where('expert_user_id',$this->user_id)
                                                ->orderBy('id','DESC')
                                                ->count();
        }

        /* }*/
      
      if(isset($arr_subscription['id']) && $arr_subscription['id'] != "")
      {
        $topup_bids = $this->ProjectsBidsTopupModel->where('expert_user_id','=',$this->user_id)
                                                   ->where('user_subscription_id','=',$arr_subscription['id'])
                                                   ->whereHas('transaction_details',function ($query)
                                                   {
                                                     $query->whereIN('payment_status',['1','2']);
                                                   })->count();
      }

      $obj_currency = $this->CurrencyModel->select('id','currency_code','currency','description')
                                            ->where('is_active','1')
                                            ->get();
      if($obj_currency)
      {
        $arr_currency  = $obj_currency->toArray();
      }



      $this->arr_view_data['arr_packs']           = $arr_packs;
      $this->arr_view_data['arr_monthly_packs']   = $arr_monthly_packs;
      $this->arr_view_data['arr_quarterly_packs'] = $arr_quarterly_packs;
      $this->arr_view_data['arr_yearly_packs']    = $arr_yearly_packs;
      $this->arr_view_data['arr_subscription']    = $arr_subscription;
      $this->arr_view_data['bids_applied']        = $bids_count;
      $this->arr_view_data['topup_bids']          = $topup_bids;
      $this->arr_view_data['arr_currency']        = $arr_currency;
      $this->arr_view_data['page_title']          = trans('controller_translations.page_title_subscription') ;
      $this->arr_view_data['module_url_path']     = $this->module_url_path;
      
      return view('expert.subscription.index',$this->arr_view_data);
    }
    public function payment_methods($enc_pack_id,$enc_pre_pack_id,$enc_currency)
    {

      $user_id  = $this->user_id;
      $obj_data = $this->UserModel->where('id',$user_id)->first();
      //dd($obj_data->currency_code);
      $currency = isset($obj_data->currency_code)?$obj_data->currency_code:'';
      if($currency==null)
      {
        $data['currency_code'] = $enc_currency;
        $obj_update = $this->UserModel->where('id',$user_id)->update($data);  
      }
      if ($enc_pack_id)
      {
          $arr_pack_details     = array();
          $arr_pre_pack_details = array();
          $pack_id              = base64_decode($enc_pack_id);
          $enc_pre_pack_id      = base64_decode($enc_pre_pack_id);
          // dd($enc_currency);
          $obj_pack = $this->SubscriptionPacksModel->where('is_active','1')->where('id',$pack_id)->first();

          if($obj_pack != FALSE){
              $arr_pack_details = $obj_pack->toArray();
          }

          if(isset($arr_pack_details['pack_price']) && $arr_pack_details['pack_price']==0)
          {
            $subscribe = $this->free_subscription($this->user_id,$arr_pack_details['id']);
          
            if($subscribe){
              Session::flash('success',trans('controller_translations.success_thank_you_your_subsciption_is_successfull'));
              return back();
            }
              Session::flash('error',trans('controller_translations.error_error_while_proccesing_to_your_subscription_please_try_again'));
              return back();
          }
          /* get privious pack details */
          $obj_pre_pack     = $this->SubscriptionPacksModel
                                   ->with('subscription_user_details')
                                   ->where('is_active',1)
                                   ->where('id',$enc_pre_pack_id)
                                   ->first();

          if($obj_pre_pack != FALSE){
              $arr_pre_pack_details = $obj_pre_pack->toArray();
          }

          if(isset($arr_pre_pack_details['pack_name']) && $arr_pre_pack_details['pack_name'] == "Basic" && isset($arr_pack_details['pack_name']) && $arr_pack_details['pack_name'] == 'Starter' ){
             Session::flash('error',trans('controller_translations.something_went_wrong_please_try_again_later'));
             return redirect('expert/subscription');
          } else if(isset($arr_pre_pack_details['pack_name']) && $arr_pre_pack_details['pack_name'] == "Plus" && isset($arr_pack_details['pack_name']) && $arr_pack_details['pack_name'] == 'Basic' ||
                    isset($arr_pre_pack_details['pack_name']) && $arr_pre_pack_details['pack_name'] == "Plus" && isset($arr_pack_details['pack_name']) && $arr_pack_details['pack_name'] == 'Starter' ){
             Session::flash('error',trans('controller_translations.something_went_wrong_please_try_again_later'));
             return redirect('expert/subscription');
          } else if(isset($arr_pre_pack_details['pack_name']) && $arr_pre_pack_details['pack_name'] == "Premium" && isset($arr_pack_details['pack_name']) && $arr_pack_details['pack_name'] == 'Starter' ||
                    isset($arr_pre_pack_details['pack_name']) && $arr_pre_pack_details['pack_name'] == "Premium" && isset($arr_pack_details['pack_name']) && $arr_pack_details['pack_name'] == 'Basic'   ||
                    isset($arr_pre_pack_details['pack_name']) && $arr_pre_pack_details['pack_name'] == "Premium" && isset($arr_pack_details['pack_name']) && $arr_pack_details['pack_name'] == 'Plus'  ){
             Session::flash('error',trans('controller_translations.something_went_wrong_please_try_again_later'));
             return redirect('expert/subscription');
          } else if(isset($arr_pre_pack_details['pack_name']) && $arr_pre_pack_details['pack_name'] == "Professional" && isset($arr_pack_details['pack_name']) && $arr_pack_details['pack_name'] == 'Starter' ||
                    isset($arr_pre_pack_details['pack_name']) && $arr_pre_pack_details['pack_name'] == "Professional" && isset($arr_pack_details['pack_name']) && $arr_pack_details['pack_name'] == 'Basic'   ||
                    isset($arr_pre_pack_details['pack_name']) && $arr_pre_pack_details['pack_name'] == "Professional" && isset($arr_pack_details['pack_name']) && $arr_pack_details['pack_name'] == 'Plus'    ||
                    isset($arr_pre_pack_details['pack_name']) && $arr_pre_pack_details['pack_name'] == "Professional" && isset($arr_pack_details['pack_name']) && $arr_pack_details['pack_name'] == 'Premium' ){
             Session::flash('error',trans('controller_translations.something_went_wrong_please_try_again_later'));
             return redirect('expert/subscription');
          }
          if(isset($arr_pre_pack_details['pack_name']) && $arr_pre_pack_details['pack_name'] != "Starter" &&
             isset($arr_pre_pack_details['pack_price']) && $arr_pre_pack_details['pack_price'] != 0){
             $current_data = date('Y-m-d'); 
             $purchased_at = isset($arr_pre_pack_details['subscription_user_details']['created_at'])?$arr_pre_pack_details['subscription_user_details']['created_at']:'null';
             $expiry_at    = isset($arr_pre_pack_details['subscription_user_details']['expiry_at'])?$arr_pre_pack_details['subscription_user_details']['expiry_at']:'null';
             $paid_price   = isset($arr_pre_pack_details['subscription_user_details']['pack_price'])?$arr_pre_pack_details['subscription_user_details']['pack_price']:'null';

             if($expiry_at > date('Y-m-d').' 00:00:00'){
               $date1               = strtotime(date('Y-m-d').' 00:00:00');
               $date2               = strtotime($expiry_at);
               $datediff            = $date1 - $date2;
               $remaining_days      = abs(($datediff / (60 * 60 * 24)));
               $cal_daywise_price   = ($paid_price/30);
               $remaining_price     = ($cal_daywise_price*$remaining_days);
             }
          }
          /* end get privious pack details */
          $pre_pack_remaining_days  = isset($remaining_days)?$remaining_days:'0';
          $pre_pack_remaining_price = isset($remaining_price)?$remaining_price:'0';
          //dd($pre_pack_remaining_days,$pre_pack_remaining_price);

          // get wallet balance //
            $mangopay_wallet           = [];
            $mangopay_wallet_details   = [];

            
            $arr_data = get_user_wallet_details($this->user_id,$enc_currency);
            //dd($arr_data);
            // $this->UserWalletModel->where('user_id',$this->user_id)
            //                                   ->where('currency_code',$enc_currency)
            //                                   ->first();
            if(empty($arr_data))
            {
                $obj_currency = $this->CurrencyModel->select('id','currency_code')->get();
                if($obj_currency)
                {
                  $arr_currency = $obj_currency->toArray();
                }

                $obj_expert_deatils = $this->ExpertsModel->where('user_id',$this->user_id)->first();
          
                $user_type = isset($obj_expert_deatils->user_type)?$obj_expert_deatils->user_type:'';
                $company_name = isset($obj_expert_deatils->company_name)?$obj_expert_deatils->company_name:'';

                $first_name   = "-";
                $last_name    = "-";
                $email        = "-";
                $logged_user  = $this->user->toArray();
                if(isset($logged_user['role_info']['first_name']) && $logged_user['role_info']['first_name'] !="")
                {
                  $first_name = $logged_user['role_info']['first_name'];
                }
                if(isset($logged_user['role_info']['last_name']) && $logged_user['role_info']['last_name'] !="")
                {
                  $last_name = $logged_user['role_info']['last_name'];
                }
                if(isset($logged_user['email']) && $logged_user['email'] !=""){
                  $email = $logged_user['email'];
                }
                try
                {
                  $timezoneinfo = file_get_contents('http://ip-api.com/json/' . $_SERVER['REMOTE_ADDR']);
                }catch(\Exception $e)
                {
                  $timezoneinfo = file_get_contents('http://ip-api.com/json');  
                } 
                $mp_user_data = [];
                //Create account in mangopay and create wallet Start.......
                  $mp_user_data['FirstName']          = $first_name;
                  $mp_user_data['LastName']           = $last_name;
                  $mp_user_data['Email']              = $email;
                  $mp_user_data['CountryOfResidence'] = isset($timezoneinfo->countryCode)?$timezoneinfo->countryCode:"IN";
                  $mp_user_data['Nationality']        = isset($timezoneinfo->countryCode)?$timezoneinfo->countryCode:"IN";
                  $Mangopay_create_user =$this->WalletService->create_user_with_all_currency_wallet($mp_user_data,$arr_currency,$this->user->id);
            }
            //foreach ($arr_data as $key => $value) {
              //$mp_wallet_id = $value['mp_wallet_id'];
              $arr_data = get_user_wallet_details($this->user_id,$enc_currency);
              $get_wallet_details[] = $this->WalletService->get_wallet_details($arr_data['mp_wallet_id']);
            //}

            // GET CARD LIST
            // $mangopay_cards_details = $this->WalletService->get_cards_list($arr_data['mp_user_id']);
            // END GET CARDS LIST 
            // dd($mangopay_cards_details);
            $account_balance         = number_format($get_wallet_details[0]->Balance->Amount/100,2);
            $subscription_pack_price = number_format($arr_pre_pack_details['pack_price'],2);
            //dd($account_balance,$arr_pre_pack_details['pack_price']);
            // if($account_balance < $arr_pre_pack_details['pack_price'])
            // {
            //     Session::flash('error',$enc_currency.' Wallet not have Sufficient balance');
            //     return redirect('expert/subscription');
            // }
            if(isset($get_wallet_details) && $get_wallet_details!='false' || $get_wallet_details != false){
              $mangopay_wallet         = $get_wallet_details;
              $mangopay_wallet_details = $mangopay_wallet;
            }
          // end get wallet balance //

          // dd($arr_pack_details,$arr_pre_pack_details);

          $this->arr_view_data['pre_pack_remaining_days']  = $pre_pack_remaining_days;
          $this->arr_view_data['pre_pack_remaining_price'] = $pre_pack_remaining_price;
          $this->arr_view_data['mangopay_wallet']          = $mangopay_wallet;
          $this->arr_view_data['mangopay_wallet_details']  = $mangopay_wallet_details;
          // $this->arr_view_data['mangopay_cards_details']   = $mangopay_cards_details;
          $this->arr_view_data['arr_pack_details']         = $arr_pack_details;
          $this->arr_view_data['arr_pre_pack_details']     = $arr_pre_pack_details;
          $this->arr_view_data['currency']                 = $enc_currency;
          $this->arr_view_data['page_title']               = trans('controller_translations.page_title_subscription') ;
          $this->arr_view_data['module_url_path']          = $this->module_url_path;
          return view('expert.subscription.payment_methods',$this->arr_view_data);
      }
      return redirect()->back();
    }

    public function payment_methods_return(Request $request)
    {
        $form_data = $request->all();

        $transactionId = isset($form_data['transactionId']) ? $form_data['transactionId']:NULL;

        $redirect_back_url = url('/').'/expert/dashboard';
          
        $arr_result = $this->WalletService->viewPayIn($transactionId);
        if(isset($arr_result['status']) && $arr_result['status'] == 'error') {
          $msg = isset($arr_result['msg']) ? $arr_result['msg'] : 'Something went, wrong, Unable to process payment, Please try again.';
          Session::flash('error',$msg);  
          return redirect($redirect_back_url);

        }

        $admin_data   = get_user_wallet_details('1',$form_data['currency_code']);
        $user_data    = get_user_wallet_details($this->user_id,$form_data['currency_code']);

        $transaction_inp['tag']                      = $form_data['invoice_id'].'- Subscription fee';
        $transaction_inp['credited_UserId']          = $admin_data['mp_user_id'];
        $transaction_inp['credited_walletId']        = (string)$admin_data['mp_wallet_id']; 
        $transaction_inp['total_pay']                = $form_data['amount_int'];
        $transaction_inp['debited_UserId']           = $user_data['mp_user_id'];
        $transaction_inp['debited_walletId']         = (string)$user_data['mp_wallet_id'];
        $transaction_inp['currency_code']            = $form_data['currency_code'];
        $transaction_inp['cost_website_commission']  = '0';
        //dd($transaction_inp);
        $pay_fees        = $this->WalletService->walletTransfer($transaction_inp);
        $project         = [];
        if(isset($pay_fees->Status) && $pay_fees->Status == 'SUCCEEDED')
        {

            // update transaction details
            $update_transaction   = [];
            $update_transaction['WalletTransactionId']    = isset($pay_fees->Id) ? $pay_fees->Id : '0';
            $update_transaction['payment_status']         = 2; // paid
            $update_transaction['response_data']          = json_encode($pay_fees); // paid
            $updatetransaction = $this->TransactionsModel->where('invoice_id',$form_data['invoice_id'])->update($update_transaction); 
            // end update transaction details    

            // send notification to admin
            $arr_admin_data                         =  [];
            $arr_admin_data['user_id']              =  $admin_data['id'];
            $arr_admin_data['user_type']            = '1';
            $arr_admin_data['url']                  = 'admin/wallet/archexpert';
            $arr_admin_data['project_id']           = '';
            $arr_admin_data['notification_text_en'] = $form_data['invoice_id'].'-'.Lang::get('controller_translations.subscription_fee_paid',[],'en','en').' '.$pay_fees->Id;
            $arr_admin_data['notification_text_de'] = $form_data['invoice_id'].'-'.Lang::get('controller_translations.subscription_fee_paid',[],'de','en').' '.$pay_fees->Id;
            $this->NotificationsModel->create($arr_admin_data); 
            // end send notification to admin    

            $subscription_update = $this->SubscriptionUsersModel->where('user_id',$this->user_id)->where('is_active',1)->update(['is_active'=>0]);  
            $subscription_update_paid = $this->SubscriptionUsersModel->where('user_id',$this->user_id)->where('invoice_id',$form_data['invoice_id'])->update(['is_active'=>'1']); 
            if($form_data['payment_obj_id']=='1'){
            $this->remove_subscription_dependency($this->user_id);
            }

            Session::Flash('success_sticky',trans('controller_translations.msg_payment_transaction_for_subscription_is_successfully'));
            // Session::flash('payment_return_url','/expert/profile');
            $this->MailService->subscriptionMail($form_data['invoice_id']);
            Redirect::to(url('/expert/profile'))->send();
        } 
        else 
        {
          if(isset($pay_fees->ResultMessage))
          {
              Session::flash('error',$pay_fees->ResultMessage);  
          }
          else 
          {
            if(gettype($pay_fees) == 'string')
            {
                Session::flash('error',$pay_fees);
            }
          }
          return redirect($redirect_back_url);
        }
    }

    public function downgrade_plan($enc_pack_id,$enc_pre_pack_id,$enc_currency)
    {
      if ($enc_pack_id)
      {
          $arr_pack_details     = array();
          $arr_pre_pack_details = array();
          $pack_id              = base64_decode($enc_pack_id);
          $enc_pre_pack_id      = base64_decode($enc_pre_pack_id);
          
          $obj_pack = $this->SubscriptionPacksModel->where('is_active',1)->where('id',$pack_id)->first();

          if($obj_pack != FALSE){
              $arr_pack_details = $obj_pack->toArray();
          }
          //dd($arr_pack_details);
          if(isset($arr_pack_details['pack_price']) && $arr_pack_details['pack_price']==0)
          {
            $subscribe = $this->free_subscription($this->user_id,$arr_pack_details['id']);
          
            if($subscribe){
              Session::flash('success',trans('controller_translations.success_thank_you_your_subsciption_is_successfull'));
              return back();
            }
              Session::flash('error',trans('controller_translations.error_error_while_proccesing_to_your_subscription_please_try_again'));
              return back();
          }
          /* get privious pack details */
          $obj_pre_pack     = $this->SubscriptionPacksModel
                                   ->with('subscription_user_details')
                                   ->where('is_active',1)
                                   ->where('id',$enc_pre_pack_id)
                                   ->first();

          if($obj_pre_pack != FALSE){
              $arr_pre_pack_details = $obj_pre_pack->toArray();
          }
          $c = strtolower($enc_currency);
          
          if(isset($arr_pre_pack_details['pack_name']) && $arr_pre_pack_details['pack_name'] != "Starter" &&
             isset($arr_pre_pack_details['pack_price']) && $arr_pre_pack_details['pack_price'] != 0){
             $current_data = date('Y-m-d'); 
             $purchased_at = isset($arr_pre_pack_details['subscription_user_details']['created_at'])?$arr_pre_pack_details['subscription_user_details']['created_at']:'null';
             $expiry_at    = isset($arr_pre_pack_details['subscription_user_details']['expiry_at'])?$arr_pre_pack_details['subscription_user_details']['expiry_at']:'null';
             $paid_price   = isset($arr_pre_pack_details['subscription_user_details']['pack_price'])?$arr_pre_pack_details['subscription_user_details']['pack_price']:'null';

             if($expiry_at > date('Y-m-d').' 00:00:00'){
               $date1               = strtotime(date('Y-m-d').' 00:00:00');
               $date2               = strtotime($expiry_at);
               $datediff            = $date1 - $date2;
               $remaining_days      = abs(($datediff / (60 * 60 * 24)));
               $cal_daywise_price   = ($paid_price/30);
               $remaining_price     = ($cal_daywise_price*$remaining_days);
             }
          }
          /* end get privious pack details */

          $today_date         = date('Y-m-d H:i:s'); 
          $plan_activate_date = isset($arr_pre_pack_details['subscription_user_details']['created_at'])?
                                      $arr_pre_pack_details['subscription_user_details']['created_at']:'';
          //dd($today_date,$plan_activate_date);
          $diff   = abs(strtotime($today_date) - strtotime($plan_activate_date));
          $days   = floor(($diff/(60*60*24)));

          if((int)$days>=30)
          {
            Session::flash('error','You can not able to downgrade plan now');
            return redirect('expert/subscription');
          }

          $pre_pack_date  = isset($arr_pre_pack_details['subscription_user_details']['created_at'])?
                                  $arr_pre_pack_details['subscription_user_details']['created_at']:'0';
          //dd($pre_pack_date);
          $pre_pack_remaining_price = isset($remaining_price)?$remaining_price:'0';

          $current_pack_price = isset($arr_pack_details['pack_price'])?(float)$arr_pack_details['pack_price']:0;

          // dd($current_pack_price,$pre_pack_remaining_price);
          $refund_amount = $pre_pack_remaining_price-$current_pack_price;
          //dd($refund_amount);
          //get wallet balance //
          $mangopay_wallet           = [];
          $mangopay_wallet_details   = [];

          $arr_admin_wallet = get_user_wallet_details('1',$enc_currency);
          $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)
                                            ->where('currency_code',$enc_currency)
                                            ->first();
          if($obj_data)
          {
            $arr_data=$obj_data->toArray();
          }
          else
          {
               Session::flash('error','Please create your wallet first.');
               return redirect('expert/subscription');
          }

          $get_wallet_details = $this->WalletService->get_wallet_details($arr_admin_wallet['mp_wallet_id']);
          $account_balance    = number_format($get_wallet_details->Balance->Amount/100,2);

          if((float)$account_balance < $refund_amount)
          {
              Session::flash('error',' Something went wrong, please contact to admin.');
              return redirect('expert/subscription');
          }
          else
          {
              $transaction_inp['tag']                      = 'Subscription pack downgrade remaining payment refund';
              $transaction_inp['debited_UserId']           = $arr_admin_wallet['mp_user_id'];
              $transaction_inp['credited_UserId']          = $arr_data['mp_user_id'];
              $transaction_inp['total_pay']                = isset($refund_amount)?$refund_amount:0;
              $transaction_inp['debited_walletId']         = (string)$arr_admin_wallet['mp_wallet_id'];
              $transaction_inp['credited_walletId']        = (string)$arr_data['mp_wallet_id'];
              $transaction_inp['cost_website_commission']  = 0;
              $transaction_inp['currency_code']            = isset($enc_currency)?$enc_currency:'USD';
              $pay_fees        = $this->WalletService->walletTransfer($transaction_inp);
              // dd($pay_fees);
              //$project         = [];
              if(isset($pay_fees->Status) && $pay_fees->Status == 'SUCCEEDED')
              {  
                   $update_plan = $this->paid_subscription($this->user_id,$arr_pack_details['id']);
                   
                   if($update_plan){

                   Session::flash('success',' You have downgrade the subscription plan successfully, your remaining amount is refunded, please check your wallet');
                   return redirect()->back();
                  }
                          
              }
              else
              {
                        Session::flash('error',' Something went wrong.');
                        return redirect()->back();
              }
          }
          Session::flash('error',' Something went wrong.');
          return redirect()->back();
      }
      Session::flash('error',' Something went wrong.');
      return redirect()->back();
    }


    public function downgrademonthly_plan($enc_pack_id,$enc_pre_pack_id,$enc_currency)
    {
      // dd($enc_pack_id,$enc_pre_pack_id);
      $prev_pack = base64_decode($enc_pre_pack_id);
      $curr_pack = base64_decode($enc_pack_id);

      //dd($curr_pack,$prev_pack,$this->user_id);
      
      $arr_update['is_downgrade_request'] = '1'; 
      $arr_update['downgrade_pack_id']    = $curr_pack;

      $obj_data  = $this->SubscriptionUsersModel->where('user_id',$this->user_id)->where('subscription_pack_id',$prev_pack)->update($arr_update);
      if($obj_data)
      {
        Session::Flash('success','Your downgrade subscription request send successfully');
        return redirect()->back();
      }
      else
      {

        Session::flash('error',' Something went wrong');
        return redirect()->back();
      }

    }


    public function free_subscription($user_id,$pack_id){
      //dd($user_id,$pack_id);
      $arr_pack_details = array();
      $arr_user_subcription = array();
      if (isset($user_id) && $user_id!=null) {
        $obj_pack_details =  $this->SubscriptionPacksModel->where('id',$pack_id)->first();
        if ($obj_pack_details!=FALSE) {
            $arr_pack_details = $obj_pack_details->toArray();
            if (sizeof($arr_pack_details)>0) {
              if (isset($arr_pack_details['pack_price']) && $arr_pack_details['pack_price']==0) {
                  $invoice_id = $this->_generate_invoice_id();
                  /*First make transaction  */ 
                  $arr_transaction['user_id'] = $user_id;
                  $arr_transaction['invoice_id'] = $invoice_id;
                  /*transaction_type is type for transactions 1-Subscription*/
                  $arr_transaction['transaction_type'] = 1;
                  $arr_transaction['payment_method'] = 0;
                  $arr_transaction['payment_status'] = 1;
                  $arr_transaction['payment_date'] = date('c');
                  $arr_transaction['paymen_amount'] = $arr_pack_details['pack_price'];
                  $arr_transaction['payment_date'] = date('c');
                  /* get currency for transaction */
                  /*$arr_currency = setCurrencyForTransaction();
                  $arr_transaction['currency']      = isset($arr_currency['currency']) ? $arr_currency['currency'] : '$';
                  $arr_transaction['currency_code'] = isset($arr_currency['currency_code']) ? $arr_currency['currency_code'] : 'USD';*/
                  $transaction = $this->TransactionsModel->create($arr_transaction);
                  if ($transaction) {
                  /*insert record in user_subcription for selected pack and invoice details*/
                  $arr_user_subcription['user_id']                      = $user_id;
                  $arr_user_subcription['subscription_pack_id']         = isset($arr_pack_details['id'])?$arr_pack_details['id']:'';
                  $arr_user_subcription['invoice_id']                   = $invoice_id;
                  $arr_user_subcription['expiry_at']                    = null;
                  $arr_user_subcription['pack_name']                    = isset($arr_pack_details['pack_name'])?$arr_pack_details['pack_name']:'';
                  $arr_user_subcription['pack_price']                   = isset($arr_pack_details['pack_price'])?$arr_pack_details['pack_price']:'';
                  $arr_user_subcription['validity']                     = isset($arr_pack_details['validity'])?$arr_pack_details['validity']:'';
                  $arr_user_subcription['number_of_bids']               = isset($arr_pack_details['number_of_bids'])?$arr_pack_details['number_of_bids']:'0';
                  $arr_user_subcription['topup_bid_price']              = isset($arr_pack_details['topup_bid_price'])?$arr_pack_details['topup_bid_price']:'0';
                  $arr_user_subcription['number_of_categories']         = isset($arr_pack_details['number_of_categories'])?$arr_pack_details['number_of_categories']:'0';
                  $arr_user_subcription['number_of_subcategories']      = isset($arr_pack_details['number_of_subcategories'])?$arr_pack_details['number_of_subcategories']:'0';
                  $arr_user_subcription['number_of_skills']             = isset($arr_pack_details['number_of_skills'])?$arr_pack_details['number_of_skills']:'0';
                  $arr_user_subcription['number_of_favorites_projects'] = isset($arr_pack_details['number_of_favorites_projects'])?$arr_pack_details['number_of_favorites_projects']:'0';
                  $arr_user_subcription['website_commision']            = isset($arr_pack_details['website_commision'])?$arr_pack_details['website_commision']:'0';
                  $arr_user_subcription['allow_email']                  = isset($arr_pack_details['allow_email'])?$arr_pack_details['allow_email']:'0';
                  $arr_user_subcription['allow_chat']                   = isset($arr_pack_details['allow_chat'])?$arr_pack_details['allow_chat']:'0';
                  $arr_user_subcription['is_active']                    = '1';
                  //update all previous subscription status to 0 and then create new subscription
                  $subscription_update = $this->SubscriptionUsersModel->where('user_id',$this->user_id)->where('is_active',1)->update(['is_active'=>0]);
                  $subscription = $this->SubscriptionUsersModel->create($arr_user_subcription);
                                    
                  if($pack_id=='1'){
                    /*delete category and skills of expert*/
                    $this->remove_subscription_dependency($this->user_id);
                  }
                  return true;
              }
            }
          }
        }
      }
    return false;
   }

    public function paid_subscription($user_id,$pack_id)
    {
      //dd('Paiddd',$user_id,$pack_id);
      $arr_pack_details     = array();
      $arr_user_subcription = array();
      //dd($user_id);
      if(isset($user_id) && $user_id!=null) 
      {
        //dd($user_id);
        $obj_pack_details =  $this->SubscriptionPacksModel->where('id',$pack_id)->first();
        if($obj_pack_details!=FALSE) 
        {
            $arr_pack_details = $obj_pack_details->toArray();
            //dd($arr_pack_details);
            if(count($arr_pack_details)>0) 
            {
                  $invoice_id = $this->_generate_invoice_id();
                  /*First make transaction  */ 
                  $arr_transaction['user_id'] = $user_id;
                  $arr_transaction['invoice_id'] = $invoice_id;
                  /*transaction_type is type for transactions 1-Subscription*/
                  $arr_transaction['transaction_type'] = 1;
                  $arr_transaction['payment_method'] = 0;
                  $arr_transaction['payment_status'] = 1;
                  $arr_transaction['payment_date'] = date('c');
                  $arr_transaction['paymen_amount'] = $arr_pack_details['pack_price'];
                  $arr_transaction['payment_date'] = date('c');

                  $transaction = $this->TransactionsModel->create($arr_transaction);
                  //dd($transaction);
                  if ($transaction) 
                  {
                      /*insert record in user_subcription for selected pack and invoice details*/
                      $arr_user_subcription['user_id']                      = $user_id;
                      $arr_user_subcription['subscription_pack_id']         = isset($arr_pack_details['id'])?$arr_pack_details['id']:'';
                      $arr_user_subcription['invoice_id']                   = $invoice_id;
                      $arr_user_subcription['expiry_at']                    = date('Y-m-d',strtotime("+365 days"));
                      $arr_user_subcription['pack_name']                    = isset($arr_pack_details['pack_name'])?$arr_pack_details['pack_name']:'';
                      $arr_user_subcription['pack_price']                   = isset($arr_pack_details['pack_price'])?$arr_pack_details['pack_price']:'';
                      $arr_user_subcription['validity']                     = isset($arr_pack_details['validity'])?$arr_pack_details['validity']:'';
                      $arr_user_subcription['number_of_bids']               = isset($arr_pack_details['number_of_bids'])?$arr_pack_details['number_of_bids']:'0';
                      $arr_user_subcription['topup_bid_price']              = isset($arr_pack_details['topup_bid_price'])?$arr_pack_details['topup_bid_price']:'0';
                      $arr_user_subcription['number_of_categories']         = isset($arr_pack_details['number_of_categories'])?$arr_pack_details['number_of_categories']:'0';
                      $arr_user_subcription['number_of_subcategories']      = isset($arr_pack_details['number_of_subcategories'])?$arr_pack_details['number_of_subcategories']:'0';
                      $arr_user_subcription['number_of_skills']             = isset($arr_pack_details['number_of_skills'])?$arr_pack_details['number_of_skills']:'0';
                      $arr_user_subcription['number_of_favorites_projects'] = isset($arr_pack_details['number_of_favorites_projects'])?$arr_pack_details['number_of_favorites_projects']:'0';
                      $arr_user_subcription['website_commision']            = isset($arr_pack_details['website_commision'])?$arr_pack_details['website_commision']:'0';
                      $arr_user_subcription['allow_email']                  = isset($arr_pack_details['allow_email'])?$arr_pack_details['allow_email']:'0';
                      $arr_user_subcription['allow_chat']                   = isset($arr_pack_details['allow_chat'])?$arr_pack_details['allow_chat']:'0';
                      $arr_user_subcription['is_active']                    = '1';
                      //dd($arr_user_subcription);
                      //update all previous subscription status to 0 and then create new subscription
                      $subscription_update = $this->SubscriptionUsersModel->where('user_id',$this->user_id)->where('is_active',1)->update(['is_active'=>0]);
                      $subscription = $this->SubscriptionUsersModel->create($arr_user_subcription);
                                        
                      if($pack_id=='1')
                      {
                        /*delete category and skills of expert*/
                        $this->remove_subscription_dependency($this->user_id);
                      }
                      return true;
                  }
              
            }
        }
      }
    return false;
   }
   private function _generate_invoice_id(){
        $secure = TRUE;    
        $bytes  = openssl_random_pseudo_bytes(3, $secure);
        $order_token = 'INV'.date('Ymd').strtoupper(bin2hex($bytes));
        return $order_token;
   }
   /*
      Auther: sagar sainkar
      Comment : this is function for remove category , skills , and favourite project for subscription
   */
   public function remove_subscription_dependency($expert_user_id=null){
     if (isset($expert_user_id) && $expert_user_id!=null){
       $delete_category  = $this->ExpertsCategoriesModel->where('expert_user_id',$expert_user_id)->delete();
       $delete_skills    = $this->ExpertsSkillModel->where('expert_user_id',$expert_user_id)->delete();
       $delete_favourite = $this->FavouriteProjectsModel->where('expert_user_id',$expert_user_id)->delete();
     }
   }
} // 