<?php

namespace App\Http\Controllers\Front\Expert;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\ProjectpostModel;
use App\Models\ProjectsBidsModel;
use App\Models\ProjectRequestsModel;
use App\Models\UserWalletModel;
use App\Common\Services\WalletService;

use Sentinel;
use Validator;
use Session;

class DashboardController extends Controller
{
    public $arr_view_data;
    public function __construct(  
                                  ProjectpostModel $projectpost,
                                  ProjectsBidsModel $project_bids,
                                  ProjectRequestsModel $project_requests,
                                  WalletService $WalletService,
                                  UserWalletModel $user_wallet
                                )
    {
      $this->arr_view_data = [];

      if(! $user = Sentinel::check()) 
      {
        return redirect('/login');
      }

      if(!$user->inRole('expert')) 
      {
        return redirect('/login'); 
      }

      $this->user                 = Sentinel::check();
      $this->user_id              = $user->id;
      $this->ProjectpostModel     = $projectpost;
      $this->ProjectsBidsModel    = $project_bids;
      $this->ProjectRequestsModel = $project_requests;
      $this->UserWalletModel      = $user_wallet;
      
      $this->WalletService        = $WalletService;
      $this->view_folder_path     = 'expert/dashboard';
      $this->module_url_path      = url("/expert");
    }
 
    /* 
      Auther : Nayan S.
    */

    public function show_dashboard()
    {
        // $mp_user_data['user_id']   = '72729652';
        // $mp_user_data['currency']  = 'USD';
        // $mp_user_data['card_type'] = 'CB_VISA_MASTERCARD';

        // $data  = $this->WalletService->create_card_registration($mp_user_data);
        // dd($data);
        
        $arr_data                   = [];
        $logged_user                = isset($this->user)?$this->user->toArray():[];

        /*Finding Ongoing projects count*/
         $count_ongoing_projects = $this->ProjectpostModel->where('expert_user_id',$this->user_id)
                                                       ->where('project_status','4')
                                                       ->count();

        /*Finding Completed projects count*/
        $count_completed_projects = $this->ProjectpostModel->where('expert_user_id',$this->user_id)
                                                         ->where('project_status','3')
                                                         ->count();    

        /*Finding Awarded projects count*/
         $count_awarded_projects = $this->ProjectRequestsModel->where('expert_user_id',$this->user_id)
                                                  ->where('is_accepted','0')
                                                  ->count();

        /* Finding applied projects count */
        $count_applied_projects = $this->ProjectsBidsModel->with(['project_details'])
                                ->where('expert_user_id',$this->user_id)
                                ->whereHas('project_details',function($query){

                                  $query->where('project_status','=',2);

                                  })->count();
        
        $mangopay_wallet_details = get_user_all_wallet_details();
        if (isset($mangopay_wallet_details) && count($mangopay_wallet_details)>0)
        {
           foreach ($mangopay_wallet_details as $key => $value) 
           {     
               if(isset($value->Balance->Amount) && $value->Balance->Amount > 0)
               {
                 $arr_data['balance'][]  = isset($value->Balance->Amount)?$value->Balance->Amount/100:'0';
                 $arr_data['currency'][] = isset($value->Balance->Currency)?$value->Balance->Currency:'';
               }
           }
        }

          $mangopay_kyc_details = [];

          $obj_data = $this->UserWalletModel->where('user_id',$this->user_id)->get();
      
          if($obj_data)
          {
            $arr_kyc_details = $obj_data->toArray();
          }

          if($arr_kyc_details)
          {
            $mango_data['mp_user_id']          = isset($arr_kyc_details[0]['mp_user_id'])?$arr_kyc_details[0]['mp_user_id']:'';

            $mangopay_kyc_details = $this->WalletService->get_kyc_details($mango_data['mp_user_id']);
          }

        $this->arr_view_data['arr_data'] = $arr_data;
        $this->arr_view_data['page_title']               = trans('controller_translations.page_title_dashboard');
        $this->arr_view_data['count_ongoing_projects']   = $count_ongoing_projects;
        $this->arr_view_data['count_completed_projects'] = $count_completed_projects;
        $this->arr_view_data['count_awarded_projects']   = $count_awarded_projects;
        $this->arr_view_data['count_applied_projects']   = $count_applied_projects;
        $this->arr_view_data['mangopay_kyc_details']     = $mangopay_kyc_details;
        $this->arr_view_data['module_url_path']          = $this->module_url_path;
        return view($this->view_folder_path.'.dashboard',$this->arr_view_data);
   }


}
