<?php
namespace App\Http\Controllers\Front\Expert;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\UserModel;
use App\Models\CategoriesModel;
use App\Models\SkillsModel;
use App\Common\Services\LanguageService; 
use App\Models\ContestSkillsModel;
use App\Models\StaticPageModel;
use App\Models\NotificationsModel;
use App\Models\ContestPricingModel;
use App\Models\SiteSettingModel;
use App\Common\Services\MailService;
use App\Common\Services\SubscriptionService;
use App\Common\Services\WalletService;
use App\Models\ContestModel;
use App\Models\ContestEntryModel;
use App\Models\ContestEntryImagesModel;
use App\Models\ContestEntryImagesCommentsModel;
use App\Models\ContestEntryImagesRatingModel;
use App\Models\ContestEntryOriginalFilesModel;
use App\Models\TransactionsModel;
use App\Models\SealedEntriesTransactionsModel;
/*Used helper method to fire event*/
use App\Events\sendEmailOnPostProject;
use Sentinel;
use Validator;
use Session;
use Mail;
use App;
use Lang;

class ContestController extends Controller
{
    public $arr_view_data;
    public function __construct(
        SkillsModel $skill,
        UserModel $user_model,
        CategoriesModel $category,
        LanguageService $langauge,
        ContestSkillsModel  $contest_skills,
        StaticPageModel $static_pages,
        NotificationsModel $notifications,
        SiteSettingModel $site_settings,
        ContestModel $ContestModel,
        MailService $mail_service,
        ContestEntryModel $contest,
        ContestEntryImagesModel $contest_images,
        ContestEntryImagesCommentsModel $ContestEntryImagesCommentsModel,
        ContestEntryImagesRatingModel $ContestEntryImagesRatingModel,
        SubscriptionService $subscription_service,
        ContestEntryOriginalFilesModel $ContestEntryOriginalFilesModel,
        ContestPricingModel $ContestPricingModel,
        TransactionsModel $TransactionsModel,
        WalletService $WalletService,
        SealedEntriesTransactionsModel $sealed_entries_transactions
      )

    {
        //dd('Working on this functionality.......');
        if(! $user = Sentinel::check()){return redirect('/login');}

        $this->UserModel                             = $user_model;
        $this->CategoriesModel                       = $category;
        $this->LanguageService                       = $langauge;
        $this->SkillsModel                           = $skill;
        $this->NotificationsModel                    = $notifications;
        $this->ContestSkillsModel                    = $contest_skills;
        $this->StaticPageModel                       = $static_pages;
        $this->SiteSettingModel                      = $site_settings;
        $this->MailService                           = $mail_service;
        $this->ContestModel                          = $ContestModel;
        $this->ContestEntryModel                     = $contest;
        $this->ContestEntryImagesModel               = $contest_images;
        $this->ContestEntryOriginalFilesModel        = $ContestEntryOriginalFilesModel;
        $this->user_id                               = $user->id;
        $this->ContestEntryImagesCommentsModel       = $ContestEntryImagesCommentsModel;
        $this->ContestEntryImagesRatingModel         = $ContestEntryImagesRatingModel;
        $this->SubscriptionService                   = $subscription_service;
        $this->ContestPricingModel                   = $ContestPricingModel;
        $this->TransactionsModel                     = $TransactionsModel;
        $this->WalletService                         = $WalletService;
        $this->SealedEntriesTransactionsModel        = $sealed_entries_transactions;
        $this->module_url_path                       = url("/expert/contest");
        $this->login_url                             = url("/login");
        $this->arr_view_data                         = [];
        $this->contest_send_entry_public_file_path   = url('/').config('app.project.img_path.contest_send_entry_files');
        $this->contest_send_entry_base_file_path     = base_path().'/public'.config('app.project.img_path.contest_send_entry_files');
    }
    /*Send entry  : contest 
    Author: Tushar Ahire*/
    public function send_entry(Request $request)
    {
      // dd($request->all());
      $arr_rule                    = $arr_json = [];
      $arr_rule['work_done_by_me'] = 'required';
      $validator                   = Validator::make($request->all(),$arr_rule);
      if($validator->fails())
      {
        if(!empty($validator->errors()->first()) && $validator->errors()->first() !=""){
          $arr_json['status']     = 'error';
          $arr_json['message']    = $validator->errors()->first(); 
        } else {
          $arr_json['status']     = 'error';
          $arr_json['message']    = trans('contets_listing/listing.text_error_occure_while_send_entry_files'); 
        }
        return response()->json($arr_json);
      }  

      $arr_json                    = [];
      $arr_data                    = [];
      $user_id                     = $this->user_id;
      // check category and skill is match or not
      $chk_valid_category_and_skills = $this->SubscriptionService->validate_contest_critearea($request->input('contest_id',0));
      if($chk_valid_category_and_skills == false){
        $arr_json['status']       = 'error';
        $arr_json['view_type']    = $request->input('view_type',null);
        $arr_json['message']      = trans('controller_translations.msg_skill_category_did_not_match_contest');    
        return response()->json($arr_json);
      }
      // endcheck category and skill is match or not
      $check_exist = $this->ContestEntryModel->where('expert_id',$user_id)->where('contest_id',$request->input('contest_id',0))->count(); 
      if($check_exist > 0){
        $arr_json['status']       = 'error';
        $arr_json['view_type']    = $request->input('view_type',null);
        $arr_json['message']      = trans('contets_listing/listing.text_error_occure_while_send_entry');    
        return response()->json($arr_json);
      }
      $check_contest_expiry = $this->ContestModel
      ->select('contest_end_date')
      ->where('winner_choose','NO')
      ->where('id',$request->input('contest_id',0))
      ->where('is_active','!=','2')
      ->first(); 
      if($check_contest_expiry == null){
        $arr_json['status']       = 'error';
        $arr_json['view_type']    = $request->input('view_type',null);
        $arr_json['message']      = trans('contets_listing/listing.text_error_occure_while_send_entry');    
        return response()->json($arr_json);
      }
      if(isset($check_contest_expiry) && $check_contest_expiry != null){
       $contest_expiry = $check_contest_expiry->toArray();
       if(isset($contest_expiry['contest_end_date'])){
         if($contest_expiry['contest_end_date'] < date('Y-m-d')){
          $arr_json['status']       = 'error';
          $arr_json['view_type']    = $request->input('view_type',null);
          $arr_json['message']      = trans('contets_listing/listing.text_error_occure_while_send_entry');    
          return response()->json($arr_json);
        }
      }
    }

      // create unique indentifier
    $rs  = \DB::table('contests_entry')->select('entry_number')->where('entry_number' ,'!=' , null)->where('entry_number' ,'!=' , '')->orderBy('id' , 'DESC')->LIMIT('1')->get();
    $res = $rs;
    if(count($res) > 0)
    {                 
      $max_entry_nbr = $res[0]->entry_number;
              // remove STU from that no.                
      $max_entry_nbr = substr($max_entry_nbr, 3);
              // get count of that no.
      $cnt = strlen($max_entry_nbr);
              // increment that no. by 1
      $max_entry_nbr = $max_entry_nbr + 1;                        
              // add no. of zero's to the no.                
      $max_entry_nbr = str_pad($max_entry_nbr, $cnt, '0', STR_PAD_LEFT);
      $max_entry_nbr = 'ENT'.$max_entry_nbr;  
    } else {
      $max_entry_nbr = 'ENT000000001';  
    }
      // create unique indentifier
      // create unique indentifier
    $rs  = \DB::table('contests_entry')->select('entry_id')->where('entry_id' ,'!=' , null)->where('entry_id' ,'!=' , '')->where('contest_id',$request->input('contest_id'))->orderBy('id' , 'DESC')->LIMIT('1')->get();
    $res = $rs;
    if(count($res) > 0)
    {                 
      $max_entry_id = $res[0]->entry_id;
              // increment that no. by 1
      $max_entry_id = $max_entry_id + 1;                        
    }
    else
    {
      $max_entry_id = '1';  
    }
      // create unique indentifier  
    $view_type                   = $request->input('view_type',null);
    $arr_data['contest_id']      = $request->input('contest_id',0);  
    $arr_data['client_user_id']  = $request->input('client_user_id',0);  
    $arr_data['expert_id']       = $user_id;
    $arr_data['title']           = $request->input('cnst_title',null);  
    $arr_data['description']     = $request->input('cnt_enrtry_desc',null);    
    $arr_data['entry_number']    = $max_entry_nbr;
    $arr_data['entry_id']        = $max_entry_id;
    $arr_data['is_own_work']     = $request->input('work_done_by_me',null);

    if($request->input('sealed_entry') == 'on'){

      $seal_entry_price = 0;
      $wallet_amount    = 0;
      $get_seal_entry_price = $this->ContestPricingModel
                                      ->where('status','!=','2')
                                      ->where('slug','=','seal-entry')
                                      ->first();
      if(sizeof($get_seal_entry_price)>0){
        $seal_entry_arr = $get_seal_entry_price->toArray();
      }     
      if(isset($seal_entry_arr['price']) && $seal_entry_arr['price'] !=""){
        $seal_entry_price=$seal_entry_arr['price'];
      }

      $total_sealed_entries = $used_sealed_entries = $remaning_sealed_entries = 0;

      $obj_transactions = $this->SealedEntriesTransactionsModel
                                                  ->where('user_id',$user_id)
                                                  ->where('payment_status','1')
                                                  ->get();
      if($obj_transactions!=FALSE)
      {
          foreach ($obj_transactions as $key => $value ) {
              if(isset($value->payment_status) && $value->payment_status == '1') {
                  // transaction_type 1 - credit 2 - debit
                  if(isset($value->transaction_type) && $value->transaction_type == '1') {
                      if(isset($value->sealed_entries_quantity)){
                          $total_sealed_entries = $total_sealed_entries + $value->sealed_entries_quantity;
                      }
                  }

                  if(isset($value->transaction_type) && $value->transaction_type == '2') {
                      if(isset($value->sealed_entries_quantity)){
                          $used_sealed_entries = $used_sealed_entries + $value->sealed_entries_quantity;
                      }
                  }
              }
          }            
      }

      if($total_sealed_entries > $used_sealed_entries) {
          $remaning_sealed_entries = $total_sealed_entries - $used_sealed_entries;
      }
      if($remaning_sealed_entries<=0){
        $arr_json['status']     = 'error';
        $arr_json['view_type']  = $view_type;
        $arr_json['message']    = 'Sorry, Your sealed entry quantity is not suffeciant to make transaction for seal entry.'; 
        return response()->json($arr_json);
      } 
      else 
      {
        
        $invoice_id = $this->_generate_invoice_id();
        /*First make transaction  */ 
        $arr_transaction['user_id']          = $this->user_id;
        $arr_transaction['invoice_id']       = $invoice_id;
        /*transaction_type is type for transactions 1-Subscription 2-Milestone 3-Release Milestones 5- Project Payment 6- Contest sealed entry commistion */
        $arr_transaction['transaction_type']    = 6;
        $arr_transaction['paymen_amount']       = $seal_entry_price;
        $arr_transaction['payment_method']      = 3;
        $arr_transaction['payment_date']        = date('c');
        $arr_transaction['currency']            = '$';
        $arr_transaction['currency_code']       = 'USD';
        $arr_transaction['WalletTransactionId'] = '0';
        $arr_transaction['payment_status']      = 2; // paid
        $arr_transaction['response_data']       = ''; // paid

        $transaction = $this->TransactionsModel->create($arr_transaction);
        
        $admin_data = get_admin_email_address();
        
        if(isset($transaction) && isset($admin_data)){
            
              // create sealed_entries debit transaction 
              $arr_sealed_entries_transaction['user_id']                 = $this->user_id;
              $arr_sealed_entries_transaction['invoice_id']              = $invoice_id;
              $arr_sealed_entries_transaction['sealed_entry_rates_id']   = 0;
              $arr_sealed_entries_transaction['currency_code']           = isset($currency_code) ? $currency_code : 'USD';
              $arr_sealed_entries_transaction['currency']                = isset($currency) ? $currency : '$';
              $arr_sealed_entries_transaction['transaction_type']        = 2;
              $arr_sealed_entries_transaction['payment_amount']          = $seal_entry_price;
              $arr_sealed_entries_transaction['actual_amount']           = 0;
              $arr_sealed_entries_transaction['sealed_entries_quantity'] = 1;
              $arr_sealed_entries_transaction['payment_method']          = 1;
              $arr_sealed_entries_transaction['payment_status']          = 1;
              $arr_sealed_entries_transaction['wallet_transaction_id']   = NULL;
              $arr_sealed_entries_transaction['payment_date']            = date('c');

              $this->SealedEntriesTransactionsModel->create($arr_sealed_entries_transaction);

              // send notification to admin
              $arr_admin_data                         =  [];
              $arr_admin_data['user_id']              =  $admin_data['id'];
              $arr_admin_data['user_type']            = '1';
              $arr_admin_data['url']                  = 'admin/wallet/archexpert';
              $arr_admin_data['project_id']           = '';
              $arr_admin_data['notification_text_en'] = $arr_transaction['invoice_id'].'-'.Lang::get('controller_translations.project_service_fee_paid',[],'en','en');
              $arr_admin_data['notification_text_de'] = $arr_transaction['invoice_id'].'-'.Lang::get('controller_translations.project_service_fee_paid',[],'de','en');
              $this->NotificationsModel->create($arr_admin_data); 
               // end send notification to admin    
              try{
               $this->MailService->ProjectPaymentMail($arr_transaction['invoice_id']);
             } catch(\Exeption $e){}
        } 
        else 
        {
          $arr_json['status']     = 'error';
          $arr_json['view_type']  = $view_type;
          $arr_json['message']    = trans('controller_translations.admin_dont_have_wallet_created_yet'); 
          return response()->json($arr_json);
        }
      }
      $arr_data['sealed_entry']             = '1';
      if(isset($arr_transaction['invoice_id']) && $arr_transaction['invoice_id'] !=""){
        $arr_data['sealed_entry_invoice_id']  = $arr_transaction['invoice_id'];
      }
    }

                      $last_insert_id       = $this->ContestEntryModel->insertGetId($arr_data);
                      if($last_insert_id)
                      {
                        $arr_images           = $request->file('file',"");
                        if(isset($arr_images) && sizeof($arr_images)>0)
                        {
                          $file_no = 1;
                          foreach($arr_images as $key=>$file) 
                          {
                              // Image compress //
                            $fileExtension   = strtolower($file->getClientOriginalExtension()); 
                            $imageName       = sha1(uniqid().$file.uniqid()).'.'.$fileExtension;
                            $destinationPath = 'public/uploads/front/postcontest/send_entry/';
                            $img = \Image::make($file->getRealPath());
                            /*$img->resize(651, 366, function ($constraint) 
                            {
                              $constraint->aspectRatio();
                            })->save(base_path() . '/public/uploads/front/postcontest/send_entry/'.$imageName);*/
                             $img->save(base_path() . '/public/uploads/front/postcontest/send_entry/'.$imageName);

                              // end Image compress //    

                            if($file){
                              // add watermark //  
                              try{
                                $log_user_details = get_user_details();
                                $first_name = '';
                                $last_name  = '';
                                if(isset($log_user_details['first_name'])){ $first_name = $log_user_details['first_name'];}
                                if(isset($log_user_details['last_name'])){ $last_name = str_limit($log_user_details['last_name'],1);}
                                add_watermark('/uploads/front/postcontest/send_entry/',$imageName,$first_name.' '.$last_name);
                              }
                              catch(\Exeption $e){
                              }
                               // end add watermark //   
                            }
                            $contest_entry_image              = $imageName; 
                            $arr_images                       = [];
                            $arr_images['contest_entry_id']   = isset($last_insert_id)?$last_insert_id:"";   
                            $arr_images['image']              = $contest_entry_image; 
                            $arr_images['file_no']            = $file_no; 
                            $arr_images['is_own_work']        = $request->input('work_done_by_me',null);

                            $this->ContestEntryImagesModel->create($arr_images);
                            $file_no++;
                          }  

                          $obj_contest_data = $this->ContestModel
                                                          ->where('id',$request->input('contest_id',0))
                                                          ->first();

                          $contest_title = isset($obj_contest_data->contest_title) ? $obj_contest_data->contest_title : '';

                          /* send notification to client */
                          $arr_noti_data                         =  [];
                          $arr_noti_data['user_id']              =  isset($arr_data['client_user_id'])?$arr_data['client_user_id']:'';
                          $arr_noti_data['user_type']            = '2';
                          $arr_noti_data['url']                  = 'client/contest/show_contest_entry_details/'.base64_encode($last_insert_id);
                          $arr_noti_data['project_id']           = '';
                          $arr_noti_data['notification_text_en'] = $contest_title . ' - ' . Lang::get('controller_translations.text_contest_entry_has_been_received',[],'en','en');
                          $arr_noti_data['notification_text_de'] = $contest_title . ' - ' . Lang::get('controller_translations.text_contest_entry_has_been_received',[],'de','en');
                          $this->NotificationsModel->create($arr_noti_data);
                          /* send notification to client */

                          if(isset($view_type) && $view_type=="details")
                          {
                            Session::flash("success",trans('contets_listing/listing.text_your_entry_has_been_send_successfully'));
                          }

                          $arr_json['status']       = 'success';
                          $arr_json['view_type']    = $view_type;
                          $arr_json['message']      = trans('contets_listing/listing.text_your_entry_has_been_send_successfully'); 
                        }
                        else
                        {
                          $arr_json['status']  = 'error';
                          $arr_json['view_type']    = $view_type;
                          $arr_json['message'] = trans('contets_listing/listing.text_error_occure_while_send_entry');      
                        }  
                      }
                      else
                      {
                        $arr_json['status']     = 'error';
                        $arr_json['view_type']  = $view_type;
                        $arr_json['message']    = trans('contets_listing/listing.text_error_occure_while_send_entry');
                      }    
                      return response()->json($arr_json);
                    }

        public function update_entry(Request $request)
        {
            $arr_rule = $arr_json = [];
            $arr_rule['work_done_by_me']  = 'required';
            $validator = Validator::make($request->all(),$arr_rule);

            if($validator->fails())
            {
                if(!empty($validator->errors()->first()) && $validator->errors()->first() !=""){
                    $arr_json['status']     = 'error';
                    $arr_json['message']    = $validator->errors()->first(); 
                } else {
                    $arr_json['status']     = 'error';
                    $arr_json['message']    = trans('contets_listing/listing.text_error_occure_while_send_entry_files'); 
                }
                return response()->json($arr_json);
            }    

            $arr_json = $arr_data = $get_last_fileno = [];

            if(!empty($request->input('contest_entry_id')))
            {
                $entry_id = $request->input('contest_entry_id');
                $get_last_fileno = $this->ContestEntryImagesModel->select('file_no')->where('contest_entry_id',$entry_id)->orderBy('file_no','DESC')->limit(1)->first(); 
            }else{
                $entry_id = 0;
            }

            $file_no = 1;

            if(isset($get_last_fileno->file_no) && $get_last_fileno->file_no > 0){
                $file_no = ($get_last_fileno->file_no + 1);
                if($file_no > 25){
                    $arr_json['status']  = 'error';
                    $arr_json['message'] = trans('contets_listing/listing.text_error_occure_while_send_entry_files');      
                } 
            }

            $arr_contest_details = [];
            $obj_contest_details = $this->ContestEntryImagesModel
                                      ->with(['contest_details'=>function($qry){
                                          $qry->with(['user_details','client_user_details']);
                                      }])->where('contest_entry_id',$entry_id)->first();

            if($obj_contest_details){
                $arr_contest_details = $obj_contest_details->toArray();
            }

            $file_no_success = 0;

            if($entry_id > 0)
            {
                $arr_images = $request->file('file',"");

                if(isset($arr_images) && sizeof($arr_images)>0)
                {
                    foreach($arr_images as $key=>$file) 
                    {
                        /*$imageExtension = $file->getClientOriginalExtension();
                        $imageName        = sha1(uniqid().$file.uniqid()).'.'.$imageExtension;
                        $file->move(base_path() . '/public/uploads/front/postcontest/send_entry/', $imageName);*/

                        // Image compress //
                        $fileExtension   = strtolower($file->getClientOriginalExtension()); 
                        $imageName       = sha1(uniqid().$file.uniqid()).'.'.$fileExtension;
                        $destinationPath = 'public/uploads/front/postcontest/send_entry/';
                        $img = \Image::make($file->getRealPath());
                        /* $img->resize(651, 366, function ($constraint) {
                          $constraint->aspectRatio();
                        })->save(base_path() . '/public/uploads/front/postcontest/send_entry/'.$imageName);*/
                        $img->save(base_path() . '/public/uploads/front/postcontest/send_entry/'.$imageName);

                        // end Image compress //

                        if($file){
                            // add watermark //  
                            try{
                                $log_user_details = get_user_details();
                                $first_name = '';
                                $last_name  = '';
                                if(isset($log_user_details['first_name'])){ $first_name = $log_user_details['first_name'];}
                                if(isset($log_user_details['last_name'])){ $last_name = str_limit($log_user_details['last_name'],1);}
                                add_watermark('public/uploads/front/postcontest/send_entry/',$imageName,$first_name.' '.$last_name);
                            }
                            catch(\Exeption $e){
                              //
                            }
                            //end add watermark //   
                        }

                        $contest_entry_image              = $imageName;
                        $arr_images                       = [];
                        $arr_images['contest_entry_id']   = isset($entry_id)?$entry_id:"0";   
                        $arr_images['image']              = $contest_entry_image; 
                        $arr_images['file_no']            = $file_no; 
                        $arr_images['is_own_work']        = $request->input('work_done_by_me',null); 

                        $this->ContestEntryImagesModel->create($arr_images);

                        $file_no++;
                        $file_no_success ++;
                    }

                    $contest_title = isset($arr_contest_details['contest_details']['contest_title'])?$arr_contest_details['contest_details']['contest_title']:'';

                    /* send notification to client */
                    $arr_noti_data                         =  [];
                    $arr_noti_data['user_id']              =  isset($arr_contest_details['contest_details']['client_user_id'])?$arr_contest_details['contest_details']['client_user_id']:'';
                    $arr_noti_data['user_type']            = '2';
                    $contest_id = isset($arr_contest_details['contest_details']['id'])?$arr_contest_details['contest_details']['id']:'';
                    $arr_noti_data['url']                  = 'client/contest/show_contest_entry_details/'.base64_encode($contest_id);
                    $arr_noti_data['project_id']           = '';
                    $arr_noti_data['notification_text_en'] = $contest_title . ' - ' . Lang::get('controller_translations.text_contest_entry_upload',[],'en','en');
                    $arr_noti_data['notification_text_de'] = $contest_title . ' - ' . Lang::get('controller_translations.text_contest_entry_upload',[],'de','en');
                    $this->NotificationsModel->create($arr_noti_data);
                    /* send notification to client */

                    $data['user_id']           =  isset($arr_contest_details['contest_details']['client_user_id'])?$arr_contest_details['contest_details']['client_user_id']:'';
                    $data['name']              = isset($arr_contest_details['contest_details']['client_user_details']['first_name'])?$arr_contest_details['contest_details']['client_user_details']['first_name']:'';
                    $data['email_id'] = $email_id = isset($arr_contest_details['contest_details']['user_details']['email'])?$arr_contest_details['contest_details']['user_details']['email']:'';
                    $data['contest_title'] = isset($arr_contest_details['contest_details']['contest_title'])?$arr_contest_details['contest_details']['contest_title']:'';

                    $project_name = config('app.project.name');

                    $mail_status = 0;

                    $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
                    try
                    {
                        $mail_status = $this->MailService->send_file_upload_contest_email($data);
                        // Mail::send('front.email.file_upload_contest', $data, function ($message) use ($email_id,$mail_form,$project_name) {
                        //           $message->from($mail_form, $project_name);
                        //           $message->subject($project_name.':File Upload');
                        //           $message->to($email_id);
                        // });
                    }
                    catch(\Exception $e)
                    {
                        Session::Flash('error',trans('controller_translations.text_mail_not_sent'));
                    }
            
                    $arr_json['status']          = 'success';
                    $arr_json['file_no_success'] = $file_no_success;
                    $arr_json['message']         = trans('contets_listing/listing.text_your_entry_files_has_been_send_successfully');
                }
                else
                {
                    $arr_json['status']  = 'error';
                    $arr_json['message'] = trans('contets_listing/listing.text_error_occure_while_send_entry_files');      
                }  
            }
            else
            {
                $arr_json['status']     = 'error';
                $arr_json['message']    = trans('contets_listing/listing.text_error_occure_while_send_entry_files'); 
            }    
            return response()->json($arr_json);
        }

        public function applied_contest()
        {
            $this->arr_view_data['page_title'] = trans('controller_translations.page_title_manage_open_projects');

            $obj_open_contests = $this->ContestEntryModel->where('expert_id',$this->user_id)
                                                                ->whereHas('contest_details',function($q){
                                                                    /*$q->where('is_active','=','0');
                                                                    $q->orWhere('is_active','=','1');*/
                                                                    //$q->where('is_winner','NO');
                                                                })
                                                                ->with(['contest_details'])
                                                                ->orderBy('created_at','DESC')
                                                                ->paginate(config('app.project.pagi_cnt'));

            $arr_open_contests = $arr_pagination = array();

            if($obj_open_contests)
            {
                $arr_pagination         = clone $obj_open_contests;
                $arr_open_contests      = $obj_open_contests->toArray();
            }

            $this->arr_view_data['arr_open_contests']     = $arr_open_contests;
            $this->arr_view_data['arr_pagination']        = $arr_pagination;
            $this->arr_view_data['module_url_path']       = $this->module_url_path;

            return view('expert.contests.applied_contest',$this->arr_view_data);
        }

        public function details($enc_id){
          $arr_contest_entry_data = [];
          $arr_pagination         = [];
          $arr_contest_details    = [];
          $contest_id = base64_decode($enc_id);
          if($contest_id)
          {

            $obj_contest_details = $this->ContestModel->where('id',$contest_id)->first(['id','contest_title','category_details','contest_skills.skill_data']);
            if($obj_contest_details)
            {
              $arr_contest_details = $obj_contest_details->toArray();
            }

            $obj_contest_entry_data = $this->ContestEntryModel->where('contest_id',$contest_id)
            ->orderBy('created_at','DESC')  
            ->paginate(config('app.project.pagi_cnt'));
            if($obj_contest_entry_data)
            {
              $arr_pagination         = clone $obj_contest_entry_data;
              $arr_contest_entry_data = $obj_contest_entry_data->toArray();
            } 

            if(empty($arr_contest_details)){
              Session::flash("error",trans('controller_translations.text_sorry_contest_is_not_available'));
              return redirect($this->module_url_path);
            }

            $this->arr_view_data['arr_contest_details']      = $arr_contest_details;
            $this->arr_view_data['arr_pagination']           = $arr_pagination;
            $this->arr_view_data['arr_contest_entry_data']   = $arr_contest_entry_data;
            $this->arr_view_data['page_title']               = "Contest Entries";
            $this->arr_view_data['module_url_path']          = $this->module_url_path;  
            return view('expert.contests.posted_details',$this->arr_view_data);
          }
          return redirect()->back();     
        }

        public function show_contest_entry_details($enc_id)
        {  
          $arr_contest_entry_data = [];
          $contest_winner_id = 0;
          $contest_entry_id = base64_decode($enc_id);   
          if($contest_entry_id=="")
          {
            return redirect()->back();
          }
          $obj_contest_entry_data = $this->ContestEntryModel->where('id',$contest_entry_id)
                                                            ->with('contest_details','contest_entry_files','contest_entry_files_rating','contest_entry_original_files','client_user_details')
                                                            ->first();
          if($obj_contest_entry_data)
          {
            $arr_contest_entry_data = $obj_contest_entry_data->toArray();

            $contest_id = $arr_contest_entry_data['contest_id'];

            $winner_id = $this->ContestEntryModel->where('contest_id',$contest_id)
                                                 ->where('is_winner','YES')
                                                 ->select('expert_id')
                                                 ->first();
            if($winner_id)
            {
              $contest_winner_id = $winner_id['expert_id'];
            }
          } 

          if(empty($arr_contest_entry_data))
          {
            Session::flash("error",trans('controller_translations.text_sorry_contest_is_not_available'));
            return redirect($this->module_url_path);
          }
          
          $this->arr_view_data['contest_send_entry_public_file_path'] = $this->contest_send_entry_public_file_path;
          $this->arr_view_data['contest_send_entry_base_file_path']   = $this->contest_send_entry_base_file_path;
          $this->arr_view_data['arr_contest_entry_data']     = $arr_contest_entry_data;
          $this->arr_view_data['page_title']                 = trans('controller_translations.page_title_project_details');
          $this->arr_view_data['module_url_path']            = $this->module_url_path;
          $this->arr_view_data['contest_winner_id']          = $contest_winner_id;
          
          return view('expert.contests.contest_entry_details',$this->arr_view_data);    
        }
        public function store_contest_entry_file_comment(Request $request){
          $arr_rules = [];
          $arr_rules['contest_id']       = "required";
          $arr_rules['contest_entry_id'] = "required";
          $arr_rules['file_no']          = "required";
          $arr_rules['file_cmnt']        = "required";
          $validator = Validator::make($request->all(),$arr_rules);
          if($validator->fails()){
            if(!empty($validator->errors()->first()) && $validator->errors()->first() != ""){
             Session::flash("error",$validator->errors()->first());
           } else {
             Session::flash("error",trans('controller_translations.text_please_fill_all_mandatory_fields'));
           }
           return redirect()->back()->withErrors($validator)->withInput($request->all());
         }
         $chk_admin_deleted = $this->ContestEntryImagesModel
         ->where('contest_entry_id',$request->input('contest_entry_id'))
         ->where('file_no',$request->input('file_no'))
         ->where('is_admin_deleted','1')
         ->count();  
         if($chk_admin_deleted > 0){
          Session::flash("error",trans('controller_translations.contest_file_deleted_by_admin'));
          return redirect()->back()->with('active_file', $request->input('file_no'));  
        }

        
        $store_cmt_arr   = [];
        $store_cmt_arr['contest_id']       = $request->input('contest_id');
        $store_cmt_arr['contest_entry_id'] = $request->input('contest_entry_id');
        $store_cmt_arr['comment_user_id']  = $this->user_id;
        $store_cmt_arr['file_no']          = $request->input('file_no');
        $store_cmt_arr['comment']          = $request->input('file_cmnt');
        $store = $this->ContestEntryImagesCommentsModel->create($store_cmt_arr);
        if($store){
          $obj_get_client_id = $this->ContestModel->where('id',$store_cmt_arr['contest_id'])->select('client_user_id')->first();
          if(isset($obj_get_client_id) && $obj_get_client_id != null )
          {
            $arr_client_id = $obj_get_client_id->toArray(); 
          }
          $contest_title = isset($obj_get_client_id->contest_title) ? $obj_get_client_id->contest_title : '';
          /* send notification to client */
          $arr_noti_data                         =  [];
          $arr_noti_data['user_id']              =  isset($arr_client_id['client_user_id'])?$arr_client_id['client_user_id']:'';
          $arr_noti_data['user_type']            = '2';
          $arr_noti_data['url']                  = 'client/contest/show_contest_entry_details/'.base64_encode($request->input('contest_entry_id',null));
          $arr_noti_data['project_id']           = '';
          $arr_noti_data['notification_text_en'] = $contest_title . ' - ' . Lang::get('controller_translations.text_new_comment_has_been_received_from_expert',[],'en','en');
          $arr_noti_data['notification_text_de'] = $contest_title . ' - ' . Lang::get('controller_translations.text_new_comment_has_been_received_from_expert',[],'de','en');
          $this->NotificationsModel->create($arr_noti_data);
          /* send notification to client */

          Session::flash("success",trans('client/contest/common.text_your_comment_of_file').' '.trans('client/contest/common.text_file').' #'.$store_cmt_arr['file_no'].' '.trans('client/contest/common.text_has_been_successfully_stored'));
          return redirect()->back()->with('active_file', $request->input('file_no')); 
        } else {
          Session::flash("error",trans('controller_translations.text_sorry_entry_files_not_upload_successfully'));
          return redirect()->back()->with('active_file', $request->input('file_no'));  
        }
      }
      public function contest_entry_file_get_comments(Request $request){
        $outputdata = "";
        $comment_user_first_name = 'Unknown';
        $comment_user_last_name = '';

        $arr_rules  = [];
        $arr_rules['contest_id']       = "required";
        $arr_rules['contest_entry_id'] = "required";
        $arr_rules['file_no']          = "required";
        $validator = Validator::make($request->all(),$arr_rules);
        if($validator->fails()){
          echo 'error';
          exit;
        }
        $contest_id       = $request->input('contest_id');
        $contest_entry_id = $request->input('contest_entry_id');
        $comment_user_id  = $this->user_id;
        $file_no          = $request->input('file_no');
        
        $get_coments      = $this->ContestEntryImagesCommentsModel
        ->with('comment_user')      
        ->where('contest_id',$contest_id)
        ->where('contest_entry_id',$contest_entry_id)
        ->where('file_no',$file_no)
        ->orderBy('id','DESC')
        ->get();

        if(isset($get_coments) && count($get_coments)>0){
         $coments     = $get_coments->toArray();
         $outputdata .= '<h4>'.trans('client/contest/common.text_comments').' <span class="file_index">'.trans('client/contest/common.text_file').' #'.$file_no.'</span> (<span id="cmt_cnt">'.count($get_coments).'</span>)</h4>';
         $cnt     = 1;
         $showing = 5;
         foreach ($coments as $key => $cmt) {
          if(isset($cmt['comment_user']['0']['role_info']['last_name'])){$comment_user_first_name = $cmt['comment_user']['0']['role_info']['first_name']; }
          if(isset($cmt['comment_user']['0']['role_info']['last_name'])){$comment_user_last_name = str_limit($cmt['comment_user']['0']['role_info']['last_name'],1);}
          
          if($cnt > 5){ $display = 'style="display:none;"'; }else{ $display = 'style="display:block"'; }
          if($cnt > $showing){ $showing = $showing*2;}
          $outputdata .='<div class="comment-block show'.$showing.'" '.$display.'>
          <h5>'.$comment_user_first_name.' '.$comment_user_last_name.' <small><i>'.time_ago($cmt['created_at']).'</i></small></h5>
          <p>'.$cmt['comment'].'</p>
          </div>';
          $cnt++;
        }
        if(count($get_coments) > 5){
         $outputdata .='<div><a style="float:none;" class="black-btn shwmore" shw="10">'.trans('client/contest/common.text_view_more').'</span></a></div>';
       }
     } else {
       $outputdata .= '<h4>'.trans('client/contest/common.text_comments').' <span class="file_index">'.trans('client/contest/common.text_file').' #'.$file_no.'</span> (0)</h4>';
       $outputdata .= '<div class="comment-block">'.trans('client/contest/common.text_no_comment_on').' <span class="file_index">'.trans('client/contest/common.text_file').' #'.$file_no.'</span></div>';
     }                        
     echo $outputdata;
   }
   public function contest_entry_original_files_upload(Request $request)
   {
    /*file uploade code for laravel auther : Shankar*/
    $form_data = array();
    $form_data = $request->all();
    $chk_already_uploaded_count  = $this->ContestEntryOriginalFilesModel
    ->where('contest_id',$form_data['original_files_contest_id'])
    ->where('contest_entry_id',$form_data['original_files_contest_entry_id'])
    ->count();
    if($chk_already_uploaded_count >= 3){
      Session::flash("error",trans('client/contest/common.text_sorry_you_can_not_upload_more_files_because_you_have_exceeded_your_upload_file_limit'));
      return redirect()->back();
    }
    $contest_entry_original_file = ""; 
    if($request->hasFile('contest_entry_original_files')) 
    {
      $cv_valiator = Validator::make(array('contest_entry_original_files'=>$request->file('contest_entry_original_files')),array(
       'contest_entry_original_files' => 'mimes:zip')); 
      if ($request->file('contest_entry_original_files')->isValid() && $cv_valiator->passes()){
        $contest_entry_original_files = $form_data['contest_entry_original_files'];
        $original_file_name = $contest_entry_original_files->getClientOriginalName();
        $imageExtension = $request->file('contest_entry_original_files')->getClientOriginalExtension();
        $imageName = $form_data['original_files_contest_id'].'-'.$form_data['original_files_contest_entry_id'].'-'.sha1(uniqid().$contest_entry_original_files.uniqid()).'.'.$imageExtension;
        $request->file('contest_entry_original_files')->move(
          base_path() . '/public/uploads/front/postcontest/send_entry/original_files/', $imageName
        );
        $contest_entry_original_file = $imageName; 
        if($contest_entry_original_file){
          $upload_original_file = [];
          $upload_original_file['contest_id']       = $form_data['original_files_contest_id'];
          $upload_original_file['contest_entry_id'] = $form_data['original_files_contest_entry_id'];
          $upload_original_file['file_name']        = $imageName;
          $upload_original_file['original_file_name'] = $original_file_name;
          $upload_original_file['created_at']         = date('Y-m-d h:i:s');
          $upload_files = $this->ContestEntryOriginalFilesModel->insertGetId($upload_original_file);
          if($upload_files){
           Session::flash("success",trans('client/contest/common.text_you_have_successfully_uploaded_your_files'));
           return redirect()->back();
         } else {
          Session::flash("error",trans('controller_translations.failed_to_save_file'));
          return redirect()->back(); 
        }
      } else {
        Session::flash("error",trans('controller_translations.failed_to_save_file'));
        return redirect()->back(); 
      }
    } else {
     Session::flash("error",trans('controller_translations.error_please_upload_valid_attachment'));
     return redirect()->back()->withErrors($validator)->withInput($form_data);
   }
 } else {
  Session::flash("error",trans('controller_translations.failed_to_save_file'));
  return redirect()->back(); 
}
/*file uploade code end here*/ 
}
private function _generate_invoice_id()
{
  $secure      = TRUE;    
  $bytes       = openssl_random_pseudo_bytes(3, $secure);
  $order_token = 'INV'.date('Ymd').strtoupper(bin2hex($bytes));
  return $order_token;
}
} // end class
