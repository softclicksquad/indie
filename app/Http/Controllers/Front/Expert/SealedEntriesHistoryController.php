<?php

namespace App\Http\Controllers\Front\Expert;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Models\UserModel;
use App\Models\UserWalletModel;
use App\Models\SealedEntryRatesModel;
use App\Models\SealedEntriesTransactionsModel;

use App\Common\Services\WalletService;

use Validator;
use Session;
use sentinel;
use App;
use Redirect;

class SealedEntriesHistoryController extends Controller
{
    public function __construct(UserModel $user,UserWalletModel $user_wallet,SealedEntryRatesModel $sealed_entry_rates,SealedEntriesTransactionsModel $sealed_entries_transactions)
	{
		$this->arr_view_data                  = [];
		$this->module_url_path                = url('/expert/sealedentrieshistory');
		$this->UserModel 				      = $user;
		$this->UserWalletModel 	      		  = $user_wallet;
		$this->SealedEntryRatesModel 	      = $sealed_entry_rates;
		$this->SealedEntriesTransactionsModel = $sealed_entries_transactions;
		$this->WalletService                  = new WalletService();
        
        $this->user_id = NULL;
        $user = Sentinel::check();
        if( $user ) 
        {
          $this->user_id = $user->id;
        }

	}

	public function index()
    {   
    	$arr_transactions = array();
        $arr_pagination   = array();
        
        $total_sealed_entries = $used_sealed_entries = $remaning_sealed_entries = 0;
        
        if($this->user_id) 
        { 
           	$obj_transactions = $this->SealedEntriesTransactionsModel
           											->where('user_id',$this->user_id)
           											->orderBy('id','DESC')
           											->get();
           	if($obj_transactions!=FALSE)
           	{
           		foreach ($obj_transactions as $key => $value ) {
           			if(isset($value->payment_status) && $value->payment_status == '1') {
	           			// transaction_type 1 - credit 2 - debit
	           			if(isset($value->transaction_type) && $value->transaction_type == '1') {
	           				if(isset($value->sealed_entries_quantity)){
	           					$total_sealed_entries = $total_sealed_entries + $value->sealed_entries_quantity;
	           				}
	           			}

	           			if(isset($value->transaction_type) && $value->transaction_type == '2') {
	           				if(isset($value->sealed_entries_quantity)){
	           					$used_sealed_entries = $used_sealed_entries + $value->sealed_entries_quantity;
	           				}
	           			}
           			}
           		}

           		$pagi_cnt = config('app.project.pagi_cnt');
           		
           		$arr_tmp_data = $obj_transactions->toArray();
           		$obj_transactions =  $this->make_pagination_links($arr_tmp_data,$pagi_cnt);
	            if($obj_transactions)
	            {
	                $arr_pagination   = clone $obj_transactions;
	                $arr_transactions = $obj_transactions->toArray();
	            }            
           	}
        }


        if($total_sealed_entries > $used_sealed_entries) {
			$remaning_sealed_entries = $total_sealed_entries - $used_sealed_entries;
        }

        $arr_sealed_entries_counts                            = [];
        $arr_sealed_entries_counts['total_sealed_entries']    = $total_sealed_entries;
        $arr_sealed_entries_counts['used_sealed_entries']     = $used_sealed_entries;
        $arr_sealed_entries_counts['remaning_sealed_entries'] = $remaning_sealed_entries;

        $obj_user = $this->UserModel
			        			->where('id',$this->user_id)
			        			->first();

        $expert_default_currency_code = isset($obj_user->currency_code) ? $obj_user->currency_code : 'USD';
        $default_currency_code = 'USD';

        $arr_sealed_entry_rates = [];
        $obj_sealed_entry_rates = $this->SealedEntryRatesModel
        										->where('is_active','1')
        										->get();
        if($obj_sealed_entry_rates){
        	$arr_sealed_entry_rates = $obj_sealed_entry_rates->toArray();
        }

        if(isset($arr_sealed_entry_rates) && count($arr_sealed_entry_rates)>0) {
        	foreach ($arr_sealed_entry_rates as $key => $value) 
        	{
        		$converted_price = 0;
        		$price = isset($value['price']) ? floatval($value['price']) : 0;
        		if( $expert_default_currency_code != $default_currency_code ) {
			        $converted_price = round(currencyConverterAPI($default_currency_code, $expert_default_currency_code, $price));
        		} else{
        			$converted_price = $price;
        		}
        		$arr_sealed_entry_rates[$key]['converted_price'] = $converted_price; 
        		$arr_sealed_entry_rates[$key]['currency_code']   = $expert_default_currency_code; 
        	}
        }

        $this->arr_view_data['page_title']                = 'Sealed Entries Transactions';
        $this->arr_view_data['arr_transactions']          = $arr_transactions;
        $this->arr_view_data['arr_pagination']            = $arr_pagination;
        $this->arr_view_data['arr_sealed_entries_counts'] = $arr_sealed_entries_counts;
        $this->arr_view_data['arr_sealed_entry_rates']    = $arr_sealed_entry_rates;
        $this->arr_view_data['module_url_path']           = $this->module_url_path;

        return view('expert.sealed_entries_transactions.index',$this->arr_view_data);
    }

    public function show($id="")
    {
      	$transaction = array();
       	if($id!=FALSE && $id!="")
       	{ 
          	$id = base64_decode($id);
          	$obj_transaction_info = $this->SealedEntriesTransactionsModel
          												->where('id',$id)
          												->first();
          	if($obj_transaction_info)
          	{
             	$transaction = $obj_transaction_info->toArray();
          	}
       	} 

      	$this->arr_view_data['page_title']      = 'View Sealed Entries Transactions';
      	$this->arr_view_data['transaction']     = $transaction;
      	$this->arr_view_data['module_url_path'] = $this->module_url_path;

      	return view('expert.sealed_entries_transactions.show',$this->arr_view_data);
    }
    public function payment(Request $request){

    	$currency_code 		   = $request->input('currency_code');
    	$currency_symbol 	   = $request->input('currency_symbol');
    	$sealed_entry_rates_id = $request->input('sealed_entry_rates_id');
    	$contest_id            = $request->input('contest_id');
    	$is_json_data          = $request->input('is_json_data');

    	if($currency_code == ''){
    		if($is_json_data == '1') {
    			$data['status'] = 'error';
	            $data['message']= 'Invalid currency code identifier.';
	            return response()->json($data);
    		}
		    Session::flash('error','Invalid currency code identifier.');
		    return redirect()->back();
    	}

    	if($sealed_entry_rates_id == ''){
		    if($is_json_data == '1') {
    			$data['status'] = 'error';
	            $data['message']= 'Invalid sealed entry rates identifier.';
	            return response()->json($data);
    		}
		    Session::flash('error','Invalid sealed entry rates identifier.');
		    return redirect()->back();
    	}

        $obj_sealed_entry_rates = $this->SealedEntryRatesModel
        										->where('id',$sealed_entry_rates_id)
        										->first();
        if($obj_sealed_entry_rates == FALSE){
		    if($is_json_data == '1') {
    			$data['status'] = 'error';
	            $data['message']= 'Invalid sealed entry rates details.';
	            return response()->json($data);
    		}
		    Session::flash('error','Invalid sealed entry rates details.');
		    return redirect()->back();
        }

        $arr_mp_details  = get_user_wallet_details($this->user_id,$currency_code);
        if(isset($arr_mp_details) && count($arr_mp_details)>0){
			
			$get_wallet_details = $this->WalletService->get_wallet_details($arr_mp_details['mp_wallet_id']);
        	if($get_wallet_details == false){
        		if($is_json_data == '1') {
	    			$data['status'] = 'error';
		            $data['message']= 'Invalid default wallet details, unable to process request.';
		            return response()->json($data);
	    		}
        		Session::flash('error','Invalid default wallet details, unable to process request.');
				return redirect()->back();
        	}

        	$wallet_amount= 0;
            if(isset($get_wallet_details->Balance->Amount) && $get_wallet_details->Balance->Amount >0 ){
               $wallet_amount = floatval($get_wallet_details->Balance->Amount/100);
            }
            
            $sealed_entry_price = isset($obj_sealed_entry_rates->price) ? $obj_sealed_entry_rates->price : 0;
			$sealed_entry_price = round(currencyConverterAPI('USD', $currency_code, $sealed_entry_price));
            
		  	$invoice_id	                                = $this->_generate_invoice_id();
		  	$arr_transaction['user_id']                 = $this->user_id;
		  	$arr_transaction['invoice_id']              = $invoice_id;
		  	$arr_transaction['sealed_entry_rates_id']   = isset($obj_sealed_entry_rates->id) ? $obj_sealed_entry_rates->id : 0;
		  	$arr_transaction['currency_code']           = isset($currency_code) ? $currency_code : 'USD';
		  	$arr_transaction['currency']                = isset($currency) ? $currency : '$';
		  	$arr_transaction['transaction_type']        = 1;
		  	$arr_transaction['payment_amount']          = $sealed_entry_price;
		  	$arr_transaction['actual_amount']           = isset($obj_sealed_entry_rates->price) ? $obj_sealed_entry_rates->price : 0;
		  	$arr_transaction['sealed_entries_quantity'] = isset($obj_sealed_entry_rates->quantity) ? $obj_sealed_entry_rates->quantity : 0;
		  	$arr_transaction['payment_method']          = 1;
		  	$arr_transaction['payment_status']          = 0;
		  	$arr_transaction['wallet_transaction_id']   = isset($arr_mp_details['mp_wallet_id']) ? $arr_mp_details['mp_wallet_id'] : NULL;
		  	$arr_transaction['payment_date']            = date('c');

			if($sealed_entry_price > $wallet_amount){
				$arr_transaction['payment_method'] = 2;
			}

			$obj_sealed_entries_transactions = $this->SealedEntriesTransactionsModel->create($arr_transaction);
            if($obj_sealed_entries_transactions){

            	$sealed_entries_transactions_id = isset($obj_sealed_entries_transactions->id) ? $obj_sealed_entries_transactions->id : 0;
            	if($sealed_entry_price > $wallet_amount){

            		$payment_amount = $sealed_entry_price - $wallet_amount;

	            	$transaction_inp['mp_user_id']      = isset($arr_mp_details['mp_user_id'])?$arr_mp_details['mp_user_id']:'';
	              	$transaction_inp['mp_wallet_id']    = isset($arr_mp_details['mp_wallet_id'])?$arr_mp_details['mp_wallet_id']:'';
	              	$transaction_inp['ReturnURL']       = $this->module_url_path.'/payment_response?invoice_id='.$invoice_id.'&sealed_entries_transactions_id='.$sealed_entries_transactions_id.'&contest_id='.$contest_id;
	              	$transaction_inp['currency_code']   = $currency_code;
	              	$transaction_inp['amount']          = $payment_amount;
	                $transaction_inp['fee']             = 0;

	                $mp_add_money_in_wallet = $this->WalletService->payInWallet($transaction_inp);
	                if(isset($mp_add_money_in_wallet->Status) && $mp_add_money_in_wallet->Status == 'CREATED')
	                {   
	                    return Redirect::to($mp_add_money_in_wallet->ExecutionDetails->RedirectURL)->send();
	                } 
	                else 
	                {
	                  	if(isset($mp_add_money_in_wallet) && $mp_add_money_in_wallet == false){
	                    	if($is_json_data == '1') {
								$data['status'] = 'error';
					            $data['message']= trans('common/wallet/text.text_something_went_wrong_please_try_again_later');
					            return response()->json($data);
							}
	                    	Session::flash('error',trans('common/wallet/text.text_something_went_wrong_please_try_again_later'));
	                    	return redirect()->back();
	                  	}
	                  	else
	                  	{
	                  		if($is_json_data == '1') {
								$data['status'] = 'error';
					            $data['message']= $mp_add_money_in_wallet;
					            return response()->json($data);
							}
	                    	Session::flash('error',$mp_add_money_in_wallet);
	                    	return redirect()->back();
	                  	}
	                }
	            }
	            else {

	            	$admin_data  = get_user_wallet_details('1',$currency_code);

	            	$transaction_inp['tag']                     = $invoice_id.'-Top up Sealed entry fee';
	            	$transaction_inp['debited_UserId']          = isset($arr_mp_details['mp_user_id'])?$arr_mp_details['mp_user_id']:'';
	            	$transaction_inp['credited_UserId']         = isset($admin_data['mp_user_id']) ? $admin_data['mp_user_id'] : '';
	            	$transaction_inp['total_pay']               = $sealed_entry_price;
	            	$transaction_inp['debited_walletId']        = isset($arr_mp_details['mp_wallet_id'])?$arr_mp_details['mp_wallet_id']:'';
	            	$transaction_inp['credited_walletId']       = isset($admin_data['mp_wallet_id']) ? $admin_data['mp_wallet_id'] : '';
	            	$transaction_inp['currency_code']           = $currency_code;
	            	$transaction_inp['cost_website_commission'] = '0';
	                
	                $pay_fees = $this->WalletService->walletTransfer($transaction_inp);

	                if(isset($pay_fees->Status) && $pay_fees->Status == 'SUCCEEDED')
	                {
			  		   	$this->SealedEntriesTransactionsModel
	  		   								->where('id',$sealed_entries_transactions_id)
	  		   								->update([
	  		   											'payment_txn_id' => isset($pay_fees->Id) ? $pay_fees->Id : '',
	  		   											'payment_status' =>'1',
	  		   											'response_data'  => json_encode($pay_fees)
	  		   										]);
	                   
		                if($is_json_data == '1') {
			    			$data['status'] = 'success';
				            $data['message']= 'Sealed entry topup payment succssfully completed.';
				            return response()->json($data);
			    		}
	                   	Session::flash('success','Sealed entry topup payment succssfully completed.');			       
	                   	return redirect()->back();
			  		} 
			  		else 
			  		{
			  			$this->SealedEntriesTransactionsModel
	  		   								->where('id',$sealed_entries_transactions_id)
	  		   								->update([
	  		   											'payment_txn_id' => isset($pay_fees->Id) ? $pay_fees->Id : '',
	  		   											'payment_status' => '2',
	  		   											'response_data'  => json_encode($pay_fees)
	  		   										]);

			  		   	if(isset($pay_fees->ResultMessage)){
			  		   		if($is_json_data == '1') {
				    			$data['status'] = 'error';
					            $data['message']= $pay_fees->ResultMessage;
					            return response()->json($data);
				    		}
			  		   		Session::flash('error',$pay_fees->ResultMessage);
			  		   		return redirect()->back();
			  		   	} 
			  		   	else 
			  		   	{
	                     	if(gettype($pay_fees) == 'string')
	                     	{
	                     		if($is_json_data == '1') {
					    			$data['status'] = 'error';
						            $data['message']= $pay_fees;
						            return response()->json($data);
					    		}
	                     		Session::flash('error',$pay_fees);
	                   			return redirect()->back();
	                     	}
			  		   	}
			  		   	if($is_json_data == '1') {
			    			$data['status'] = 'error';
				            $data['message']= 'Something went wrong, unable to process request.';
				            return response()->json($data);
			    		}
			  		   	Session::flash('error','Something went wrong, unable to process request.');
						return redirect()->back();
			  		}
	            }

            } else {
            	if($is_json_data == '1') {
	    			$data['status'] = 'error';
		            $data['message']= 'Something went wrong, unable to process request.';
		            return response()->json($data);
	    		}
            	Session::flash('error','Something went wrong, unable to process request.');
				return redirect()->back();
            }
            
        }
        if($is_json_data == '1') {
			$data['status'] = 'error';
            $data['message']= 'Invalid default wallet details, unable to process request.';
            return response()->json($data);
		}
        Session::flash('error','Invalid default wallet details, unable to process request.');
		return redirect()->back();
    }

    public function payment_response(Request $request){

    	$get_data = $request->all();
    	$transactionId                  = isset($get_data['transactionId']) ? $get_data['transactionId']:NULL;
    	$sealed_entries_transactions_id = isset($get_data['sealed_entries_transactions_id']) ? $get_data['sealed_entries_transactions_id']:'0';
    	$invoice_id                     = isset($get_data['invoice_id']) ? $get_data['invoice_id']:'0';
    	$contest_id                     = isset($get_data['contest_id']) ? $get_data['contest_id']:NULL;

    	$return_redirect_url = $this->module_url_path;
    	if(isset($contest_id) && $contest_id!=''){
    		$return_redirect_url = url('/contests/details/'.$contest_id);
    	}
    	$arr_result = $this->WalletService->viewPayIn($transactionId);
        if(isset($arr_result['status']) && $arr_result['status'] == 'error') {
         	$msg = isset($arr_result['msg']) ? $arr_result['msg'] : 'Something went, wrong, Unable to process payment, Please try again.';
          	Session::flash('error',$msg);  
          	return redirect($return_redirect_url);
        }

        $obj_sealed_entries_transactions = $this->SealedEntriesTransactionsModel
        													->where('id',$sealed_entries_transactions_id)
        													->where('invoice_id',$invoice_id)
        													->first();
        if($obj_sealed_entries_transactions == false) {
          	Session::flash('error','Unable to fetch sealed entry transactions details,Please try again.');  
          	return redirect($return_redirect_url);
        }

        $mp_details  = get_user_wallet_details($obj_sealed_entries_transactions->user_id,$obj_sealed_entries_transactions->currency_code);
        $admin_data  = get_user_wallet_details('1',$obj_sealed_entries_transactions->currency_code);

        if((isset($mp_details) && count($mp_details)>0) && (isset($admin_data) && count($admin_data)>0)){

        	$transaction_inp['tag']                     = $invoice_id.'-Top up Sealed entry fee';
        	$transaction_inp['debited_UserId']          = isset($mp_details['mp_user_id'])?$mp_details['mp_user_id']:'';
        	$transaction_inp['credited_UserId']         = isset($admin_data['mp_user_id']) ? $admin_data['mp_user_id'] : '';
        	$transaction_inp['total_pay']               = $obj_sealed_entries_transactions->payment_amount;
        	$transaction_inp['debited_walletId']        = isset($mp_details['mp_wallet_id'])?$mp_details['mp_wallet_id']:'';
        	$transaction_inp['credited_walletId']       = isset($admin_data['mp_wallet_id']) ? $admin_data['mp_wallet_id'] : '';
        	$transaction_inp['currency_code']           = $obj_sealed_entries_transactions->currency_code;
        	$transaction_inp['cost_website_commission'] = '0';

            $pay_fees = $this->WalletService->walletTransfer($transaction_inp);

            if(isset($pay_fees->Status) && $pay_fees->Status == 'SUCCEEDED')
            {
	  		   	$this->SealedEntriesTransactionsModel
		   								->where('id',$sealed_entries_transactions_id)
		   								->update([
		   											'payment_txn_id' => isset($pay_fees->Id) ? $pay_fees->Id : '',
		   											'payment_status' =>'1',
		   											'response_data'  => json_encode($pay_fees)
		   										]);
               
               	Session::flash('success','Sealed entry topup payment succssfully completed.');			       
               	return redirect($return_redirect_url);
	  		} 
	  		else 
	  		{
	  			$this->SealedEntriesTransactionsModel
		   								->where('id',$sealed_entries_transactions_id)
		   								->update([
		   											'payment_txn_id' => isset($pay_fees->Id) ? $pay_fees->Id : '',
		   											'payment_status' => '2',
		   											'response_data'  => json_encode($pay_fees)
		   										]);

	  		   	if(isset($pay_fees->ResultMessage)){
	  		   		Session::flash('error',$pay_fees->ResultMessage);
	  		   		return redirect($return_redirect_url);
	  		   	} 
	  		   	else 
	  		   	{
                 	if(gettype($pay_fees) == 'string')
                 	{
                 		Session::flash('error',$pay_fees);
               			return redirect($return_redirect_url);
                 	}
	  		   	}
	  		   	Session::flash('error','Something went wrong, unable to process request.');
				return redirect($return_redirect_url);
	  		}
        }

        Session::flash('error','Something went wrong unable to process your request,Please try again.');  
 		return redirect($return_redirect_url);
    }

    public function convert_rates(Request $request){

    	$currency_code 		   = $request->input('currency_code','USD');
    	$sealed_entry_rates_id = $request->input('sealed_entry_rates_id');

        $obj_sealed_entry_rates = $this->SealedEntryRatesModel
        										->where('id',$sealed_entry_rates_id)
        										->first();
        if($obj_sealed_entry_rates == FALSE){
        	$data['status'] = 'error';
            $data['message']= 'Faild';
            return response()->json($data);
        }

        $price = isset($obj_sealed_entry_rates->price) ? $obj_sealed_entry_rates->price : 0;

        $price = round(currencyConverterAPI('USD', $currency_code, $price));

        $arr_mp_details  = get_user_wallet_details($this->user_id,$currency_code);
        
        $wallet_amount = 0;
        if(isset($arr_mp_details) && count($arr_mp_details)>0){
			$get_wallet_details = $this->WalletService->get_wallet_details($arr_mp_details['mp_wallet_id']);
        	if(isset($get_wallet_details->Balance->Amount) && $get_wallet_details->Balance->Amount >0 ){
               $wallet_amount = floatval($get_wallet_details->Balance->Amount/100);
            }
        }

        $paid_amount = 0;
        $is_higt_wallet_amount = '0';
        if($price>$wallet_amount) {
        	$paid_amount = $price - $wallet_amount;
        	$is_higt_wallet_amount = '0';
        } else {
        	$paid_amount = $price;
        	$is_higt_wallet_amount = '1';
        }

        $data['status']                = 'success';
        $data['message']               = '';
        $data['price']                 = number_format($price,2);
        $data['wallet_amount']         = number_format($wallet_amount,2);
        $data['paid_amount']           = number_format($paid_amount,2);
        $data['is_higt_wallet_amount'] = $is_higt_wallet_amount;

        return response()->json($data);	
    }

    private function make_pagination_links($items,$perPage)
    {
        $pageStart = \Request::get('page', 1);
        // Start displaying items from this number;
        $offSet = ($pageStart * $perPage) - $perPage; 

        // Get only the items you need using array_slice
        $itemsForCurrentPage = array_slice($items, $offSet, $perPage, true);

        return new LengthAwarePaginator($itemsForCurrentPage, count($items), $perPage,Paginator::resolveCurrentPage(), array('path' => Paginator::resolveCurrentPath()));
    }
    private function _generate_invoice_id()
   	{
   		$secure = TRUE;    
        $bytes = openssl_random_pseudo_bytes(3, $secure);
        $order_token = 'INV'.date('Ymd').strtoupper(bin2hex($bytes));
        return $order_token;
   	}
}
