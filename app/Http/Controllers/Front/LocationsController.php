<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Models\CountryModel; 
use App\Models\StateModel; 
use App\Models\CityModel; 

class LocationsController extends Controller
{
     /*
    | Comment: locations for all countries ,states and cities in front sections
    | auther : Sagar Sainkar
    */

    public function __construct(CountryModel $countries, StateModel $state, CityModel $city)
    {
      $this->CountryModel     = $countries;
      $this->StateModel       = $state;
      $this->CityModel       = $city;

      $this->arr_view_data = [];
      $this->module_url_path = url("/locations");
      
    }

    /*
    | Comment: This function will return array of countries
    | auther : Sagar Sainkar
    */
    
	public function get_countries()
	{
		$arr_countries = array();		
		$obj_countries = CountryModel::where('is_active',1)->get();

		if (isset($obj_countries) && $obj_countries!=FALSE) 
		{
			$arr_countries = $obj_countries->toArray();
		}

		return $arr_countries;
	}


 	/*
    | get_states : Ajax fuction to get all states for specific country
    | auther :Sagar Sainkar
    | 
    */ 

    public function get_states($country_id=null)
    {
    	$response_data = array();
    	if ($country_id) 
    	{
    		$country_id = base64_decode($country_id);

    		$obj_states = StateModel::where('country_id',$country_id)->where('is_active',1)->orderBy('state_name', 'asc')->get();

	        if($obj_states != FALSE)
	        {
	            $response_data['states'] = $obj_states->toArray();
	            $response_data['status'] = 'success';
	            $response_data['msg'] = trans('controller_translations.msg_states_found');
	        }
	        else
	        {
	        	$response_data['states'] = null;
	            $response_data['status'] = 'error';
	            $response_data['msg'] = trans('controller_translations.msg_no_state_found_for_this_country'); 
	        }
    	}
    	else
    	{
    		$response_data['states'] = null;
	        $response_data['status'] = 'error';
	        $response_data['msg'] =   trans('controller_translations.msg_no_state_found_for_this_country');
    	}

    	echo json_encode($response_data);
	    exit();
    }

    /*
    | get_states : Ajax fuction to get all cities for specific states
    | auther :Sagar Sainkar
    | 
    */ 

    public function get_cities($state_id=null)
    {
    	$response_data = array();
    	if ($state_id) 
    	{
    		$state_id = base64_decode($state_id);

    		$obj_states = CityModel::where('state_id',$state_id)->where('is_active',1)->orderBy('city_name', 'asc')->get();

	        if($obj_states != FALSE)
	        {
	            $response_data['cities'] = $obj_states->toArray();
	            $response_data['status'] = 'success';
	            $response_data['msg'] =  trans('controller_translations.msg_states_found');
	        }
	        else
	        {
	        	$response_data['cities'] = null;
	            $response_data['status'] = 'error';
	            $response_data['msg'] = trans('controller_translations.msg_no_city_found_for_this_state');
	        }
    	}
    	else
    	{
    		$response_data['cities'] = null;
	        $response_data['status'] = 'error';
	        $response_data['msg'] = trans('controller_translations.msg_no_city_found_for_this_state'); 
    	}

    	echo json_encode($response_data);
	    exit();
    }
}
