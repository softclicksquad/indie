<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\NewslettersSubscriberModel;
use App\Common\Services\MailService;
use Validator;
use Session;
use Mail;

use App\Common\Services\MailChimpService;

class NewsletterController extends Controller
{
    public function __construct(NewslettersSubscriberModel $newsletters_subscriber,
                                MailService $MailService)
    {
      $this->NewslettersSubscriberModel = $newsletters_subscriber;
      $this->MailService                = $MailService;
      $this->arr_view_data = [];
      $this->module_url_path = url("/newsletter");
      
    }
    public function send(Request $request)
    {

    	$form_data = $arr_data = array();
    	$arr_rules = array();

        $arr_rules['email']    = "required|email|unique:newsletters_subscriber,subscriber_email";
       
        
        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
             return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $form_data = $request->all();
        $email_id = $form_data['email'];
       
        $obj_subscriber = $this->NewslettersSubscriberModel->where('subscriber_email',$email_id)->first();
        $arr_subscribers = array();
        if($obj_subscriber != FALSE)
        {
            $arr_subscribers = $obj_subscriber->toArray();
        }
       
       if(isset($arr_subscribers) && sizeof($arr_subscribers)>0)
       {
       		Session::flash('error', trans('controller_translations.email_id_already_exist'));
       }
       else
       {
       		$arr_data['subscriber_email'] = $email_id;
			$status = $this->NewslettersSubscriberModel->create($arr_data);

        	if($status)
        	{
				    $data =array();
            $data['email_id'] = $email_id;
            $data['username'] = 'Dear';
	        	$project_name = config('app.project.name');
	        	//$mail_form = isset($website_contact_email)?$website_contact_email:'support@archexperts.com';
            $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
            try{
                  $mail_status = $this->MailService->send_newsletter_subscribe_email($data);
                  // Mail::send('front.email.newsletter_subscribe', $data, function ($message) use ($email_id,$mail_form,$project_name) {
                  //     $message->from($mail_form, $project_name);
                  //     $message->subject($project_name.': Newsletter');
                  //     $message->to($email_id);
                  //  });
	      	      Session::flash('success', trans('controller_translations.success_newsletter_subscribed_successfully'));
                }
                catch(\Exception $e){
                 Session::Flash('error',trans('controller_translations.text_mail_not_sent'));
                } 
			}
        	else
        	{
				Session::flash('error', trans('controller_translations.error_problem_occured_while_newsletter_subscriber'));
			}
	    }
        return redirect()->back();
    }
    public function signup_newsletter(Request $request)
    {
        
        $arr_rules = [];
        $arr_rules['email'] ='required|email';

        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return response()->json(['status'=>'ERROR','msg'=>$validator->messages()->first()]);
        }

        $mailchimp = new MailChimpService();

        $mailchimp_responce = $mailchimp->subscribe($request->input('email'));

        if(isset($mailchimp_responce) && $mailchimp_responce['status']==TRUE)
        {
            return response()->json(['status'=>'SUCCESS','msg'=>trans('common/footer.thank_you_for_signup_newsletter')]);
        }   
        else
        {
            if (isset($mailchimp_responce['error_code']) && $mailchimp_responce['error_code']==214) 
            {
                return response()->json(['status'=>'ERROR','msg'=>trans('common/footer.text_already_subscribed')]);    
            }

            return response()->json(['status'=>'ERROR','msg'=>trans('common/footer.problem_occured_in_signup_newsletter')]);
        }
    }



}
