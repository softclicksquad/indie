<?php

namespace App\Http\Controllers\Front\Expert;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\SubscriptionPacksModel; 

use Validator;
use Session;
use Sentinel;

use App\Common\Services\PaymentService;

class SubscriptionController extends Controller
{
    public $arr_view_data;
    public function __construct(SubscriptionPacksModel $subscription_packs)
    { 

      if(! $user = Sentinel::check()) 
      {
        return redirect('/login');
      }

      $this->user_id = $user->id;
      $this->SubscriptionPacksModel = $subscription_packs;

      $this->arr_view_data = [];
      $this->module_url_path = url("/expert/subscription");
    }


    /*
    	Comment: load page of subsciption packs
    	Author: Sagar Sainkar
    */

    public function index()
    {

      $obj_packs = $this->SubscriptionPacksModel->where('is_active',1)->get();
      if($obj_packs != FALSE)
      {
          $arr_packs = $obj_packs->toArray();
      }

      //echo date('Y-m-d',strtotime("+30 days"));dd('ll');
     
      //dd($arr_packs);
      $this->arr_view_data['arr_packs'] = $arr_packs;
      $this->arr_view_data['page_title'] = trans('controller_translations.page_title_subscription');
      $this->arr_view_data['module_url_path'] = $this->module_url_path;

      return view('expert.subscription.index',$this->arr_view_data);
    }

    public function payment_methodssss($enc_pack_id)
    {
      if ($enc_pack_id) 
      {
          $arr_pack_details = array();
          $pack_id = base64_decode($enc_pack_id);

          $obj_pack = $this->SubscriptionPacksModel->where('is_active',1)->where('id',$pack_id)->first();

          if($obj_pack != FALSE)
          {
              $arr_pack_details = $obj_pack->toArray();
          }
          
          //dd($arr_pack_details);
          $this->arr_view_data['arr_pack_details'] = $arr_pack_details;
          $this->arr_view_data['page_title'] = trans('controller_translations.page_title_subscription');
          $this->arr_view_data['module_url_path'] = $this->module_url_path;

          return view('expert.subscription.payment_methods',$this->arr_view_data);
      }
     
      return back();

    }
}
