<?php

namespace App\Http\Controllers\Front\Project_manager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\ProjectpostModel;
use App\Models\MilestonesModel;
use App\Common\Services\WalletService;


use Sentinel;
use Validator;
use Session;


class MilestonesController extends Controller
{
    public $arr_view_data;
    public function __construct(  ProjectpostModel $projectpost,
                                  MilestonesModel $milestones,
                                  WalletService $WalletService
                                )
    {
      $this->arr_view_data = [];

      if(! $user = Sentinel::check())  {
        return redirect('/login');
      }
      $this->user_id = $user->id;
      $this->ProjectpostModel   = $projectpost;
      $this->MilestonesModel    = $milestones;
      $this->WalletService      = $WalletService;
      $this->view_folder_path   = 'milestones';
      $this->module_url_path    = url("/project_manager/projects");
    }

    /* 
      Auther : Nayan S.
    */

    public function show_project_milestones($enc_id)
    { 
      $project_id = base64_decode($enc_id);

      $project_manager_user_id = $this->user_id;

      $this->arr_view_data['mp_job_wallet_id'] = '';
      $project_details  = $this->ProjectpostModel->where('id',$project_id)->first(['id','project_name','project_currency','mp_job_wallet_id']);
      if ($project_details){
          $this->arr_view_data['milestone_project_name']       = isset($project_details->project_name)?$project_details->project_name:'';
          $this->arr_view_data['milestone_project_currency']   = isset($project_details->project_currency)?$project_details->project_currency:'';
          $this->arr_view_data['mp_job_wallet_id']             = isset($project_details->mp_job_wallet_id)?$project_details->mp_job_wallet_id:'';
      }
      $obj_milestone = $this->MilestonesModel->where('project_id',$project_id)
                                             ->where('status','1')  
                                             ->with(['transaction_details','project_details'=>function ($query) use ($project_manager_user_id) {
                                                  //$query->select('id','project_name','project_manager_user_id'); 
                                                  $query->where('project_manager_user_id',$project_manager_user_id);
                                                }])
                                             ->paginate(config('app.project.pagi_cnt'));

      $arr_milestone = [];
      if($obj_milestone)
      {
          $arr_pagination   = clone $obj_milestone; 
          $arr_milestone    = $obj_milestone->toArray();
      }
      // get wallet details
        $wallet_details =  $this->WalletService->get_wallet_details($this->arr_view_data['mp_job_wallet_id']);
        if(isset($wallet_details)){
          $wallet_details         = $wallet_details;
        }
      // end get wallet details

      $this->arr_view_data['enc_project_id']   = $enc_id;
      $this->arr_view_data['page_title']      = trans('controller_translations.page_title_milestones');
      $this->arr_view_data['arr_milestone']   = $arr_milestone;
      $this->arr_view_data['arr_pagination']  = $arr_pagination;
      $this->arr_view_data['mangopay_wallet_details']   = $wallet_details;
      $this->arr_view_data['module_url_path'] = $this->module_url_path;
      return view($this->view_folder_path.'.project_milestones',$this->arr_view_data);
   }
} // end class
