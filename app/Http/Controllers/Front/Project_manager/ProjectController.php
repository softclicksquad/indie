<?php

namespace App\Http\Controllers\Front\Project_manager;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ProjectpostModel;
use App\Models\ProjectsBidsModel;
use App\Models\ExpertsModel;
use App\Models\NotificationsModel;
use App\Models\ProjectAttchmentModel;

use App\Models\InviteProjectModel;

use App\Common\Services\MailService;

use Validator;
use Session;
use Sentinel;
use Flash;
use Lang;
use Mail;
use App\Common\Services\PaymentService;

use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class ProjectController extends Controller
{
    public $arr_view_data;

    public function __construct(  ProjectpostModel $project_post ,
                                  ProjectsBidsModel $project_bid, 
                                  ExpertsModel $experts ,
                                  NotificationsModel $notifications ,
                                  ProjectAttchmentModel $attchment_model,
                                  MailService $mail_service,
                                  InviteProjectModel $InviteProjectModel
                                  )
    { 

      if(! $user = Sentinel::check()) 
      {
        return redirect('/login');
      }
      
      $this->user_id = $user->id;
      
      $this->InviteProjectModel = $InviteProjectModel;
      $this->profile_img_public_path = url('/').config('app.project.img_path.profile_image');

      $this->ProjectpostModel   = $project_post;
      $this->ProjectsBidsModel  = $project_bid;
      $this->NotificationsModel = $notifications; 
      $this->MailService        = $mail_service; 
      $this->ProjectAttchmentModel = $attchment_model;

      $this->ExpertsModel = $experts;

      $this->arr_view_data   = [];
      $this->module_url_path = url("/project_manager/projects");

      $this->bid_attachment_base_path    = base_path().'/public'.config('app.project.img_path.bid_attachment');
      $this->bid_attachment_public_path  = url('/public').config('app.project.img_path.bid_attachment');

      $this->project_attachment_public_path         = url('/').config('app.project.img_path.project_attachment');
      $this->expert_project_attachment_public_path  = url('/').config('app.project.img_path.expert_project_attachment');  

    }

    /*
     	Comment: load page of ongoing project 
    	Author : Nayan Sonawane
    */

    public function show_ongoing_projects(Request $request)
    {
      $sort_by = '';
      if($request->has('sort_by') && $request->input('sort_by')!='')
      {
        $sort_by = $request->input('sort_by');
      }


      $this->arr_view_data['page_title'] =trans('controller_translations.page_title_manage_ongoing_projects');

       $obj_ongoing_projects = $this->ProjectpostModel->where('project_manager_user_id',$this->user_id)
                                                     ->where('project_status','=','4')
                                                     ->with(['skill_details','client_details','project_skills.skill_data','category_details','sub_category_details']);

          if($sort_by == "is_urgent")
          {
            $obj_ongoing_projects = $obj_ongoing_projects->where('is_urgent','1')->orderBy('created_at','desc');
          }
          else if($sort_by == "public_type" || $sort_by == "private_type")
          {
            $project_type = '0';
            if($sort_by == "public_type")
            {
              $project_type = '0';
            }
            elseif($sort_by == "private_type")
            {
              $project_type = '1';
            }
            $obj_ongoing_projects = $obj_ongoing_projects->where('project_type',$project_type)->orderBy('created_at','desc');
          }
          else if($sort_by == "is_nda")
          {
            $obj_ongoing_projects = $obj_ongoing_projects->where('is_nda','1')->orderBy('created_at','desc');
          }

          $obj_ongoing_projects      = $obj_ongoing_projects
                                            ->get();

          $obj_ongoing_projects = collect($obj_ongoing_projects);

          if( $sort_by == "asc")
          { 
            $obj_ongoing_projects = $obj_ongoing_projects
                                    ->sortBy(['created_at']);
          }
          else if($sort_by == "desc")
          { 
            $obj_ongoing_projects = $obj_ongoing_projects
                                    ->sortByDesc(['created_at']);
          }

          if($sort_by == "")
          {
            $obj_ongoing_projects = $obj_ongoing_projects
                                    ->sortByDesc(['created_at']);
          }

          $arr_tmp_ongoing_projects = [];
          if($obj_ongoing_projects){
            $arr_tmp_ongoing_projects = $obj_ongoing_projects->toArray();
            $arr_tmp_ongoing_projects = array_values($arr_tmp_ongoing_projects);
          }

          $arr_ongoing_projects = $arr_pagination = [];
          $obj_ongoing_project =  $this->make_pagination_links($arr_tmp_ongoing_projects,config('app.project.pagi_cnt'));
          if($obj_ongoing_project)
          {
              $arr_ongoing_projects = $obj_ongoing_project->toArray();
              $arr_pagination      = clone $obj_ongoing_project;
          }



      $this->arr_view_data['arr_ongoing_projects']  = $arr_ongoing_projects;
      $this->arr_view_data['arr_pagination']        = $arr_pagination;
      $this->arr_view_data['module_url_path']       = $this->module_url_path;
      return view('project_manager.projects.ongoing_projects',$this->arr_view_data);

    }  

    /*
      Comment: load page of open project 
      Author : Nayan Sonawane
    */
      
    public function show_open_projects(Request $request)
    {
      $sort_by = '';
      if($request->has('sort_by') && $request->input('sort_by')!='')
      {
        $sort_by = $request->input('sort_by');
      }

      $this->arr_view_data['page_title'] = trans('controller_translations.page_title_manage_open_projects');
      
     /* $obj_open_projects = $this->ProjectpostModel->where('project_manager_user_id',$this->user_id)
                                                     ->where('project_status','=','2')
                                                     ->with(['skill_details','client_details','project_skills.skill_data','category_details','project_bid_count'])
                                                     ->orderBy('created_at','DESC')
                                                     ->paginate(config('app.project.pagi_cnt'));

      $arr_open_projects = [];

      if($obj_open_projects)
      {
        $arr_pagination       = clone $obj_open_projects;
        $arr_open_projects    = $obj_open_projects->toArray();
      }*/

      $obj_open_projects = $this->ProjectpostModel->where('project_manager_user_id',$this->user_id)
                                                     ->where('project_status','=','2')
                                                     ->with(['skill_details','client_details','project_skills.skill_data','category_details','project_bid_count']);

        if($sort_by == "is_urgent")
        {
          $obj_open_projects = $obj_open_projects->where('is_urgent','1')->orderBy('created_at','desc');
        }
        else if($sort_by == "public_type" || $sort_by == "private_type")
        {
          $project_type = '0';
          if($sort_by == "public_type")
          {
            $project_type = '0';
          }
          elseif($sort_by == "private_type")
          {
            $project_type = '1';
          }
          $obj_open_projects = $obj_open_projects->where('project_type',$project_type)->orderBy('created_at','desc');
        }
        else if($sort_by == "is_nda")
        {
          $obj_open_projects = $obj_open_projects->where('is_nda','1')->orderBy('created_at','desc');
        }

        $obj_open_projects      = $obj_open_projects
                                          ->get();

        $obj_open_projects = collect($obj_open_projects);

        if( $sort_by == "asc")
        { 
          $obj_open_projects = $obj_open_projects
                                  ->sortBy(['created_at']);
        }
        else if($sort_by == "desc")
        { 
          $obj_open_projects = $obj_open_projects
                                  ->sortByDesc(['created_at']);
        }

        if($sort_by == "")
        {
          $obj_open_projects = $obj_open_projects
                                  ->sortByDesc(['created_at']);
        }

        $arr_tmp_open_projects = [];
        if($obj_open_projects){
          $arr_tmp_open_projects = $obj_open_projects->toArray();
          $arr_tmp_open_projects = array_values($arr_tmp_open_projects);
        }

        $arr_open_projects = $arr_pagination = [];
        $obj_open_project =  $this->make_pagination_links($arr_tmp_open_projects,config('app.project.pagi_cnt'));
        if($obj_open_project)
        {
            $arr_open_projects = $obj_open_project->toArray();
            $arr_pagination      = clone $obj_open_project;
        }


      $this->arr_view_data['arr_open_projects']     = $arr_open_projects;
      $this->arr_view_data['arr_pagination']        = $arr_pagination;
      $this->arr_view_data['sort_by']               = $sort_by;
      $this->arr_view_data['module_url_path']       = $this->module_url_path;
      return view('project_manager.projects.open_projects',$this->arr_view_data);
    }

    /*
      Comment: load page of awarded project 
      Author :  Akash Sonawane
    */
   public function show_awarded_projects(Request $request)
   {
    $sort_by = '';
    if($request->has('sort_by') && $request->input('sort_by')!='')
    {
      $sort_by = $request->input('sort_by');
    }

    $this->arr_view_data['page_title'] = trans('controller_translations.page_title_manage_awarded_projects');
 
    /*$obj_awarded_projects = $this->ProjectpostModel->where('project_manager_user_id',$this->user_id)
                                                      ->Where('project_status','=','2')
                                                      ->whereHas('project_bid_info',function ($query)
                                                       {
                                                         $query->where('bid_status',4);
                                                       })
                                                      ->with(['skill_details','project_skills.skill_data'])
                                                      ->orderBy('created_at','DESC')
                                                      ->paginate(config('app.project.pagi_cnt'));
          $arr_awarded_projects = array();
          $arr_pagination = array();

          if($obj_awarded_projects)
          {
            $arr_pagination       = clone $obj_awarded_projects;
            $arr_awarded_projects = $obj_awarded_projects->toArray();
          }*/
          $obj_awarded_projects = $this->ProjectpostModel->where('project_manager_user_id',$this->user_id)
                                                      ->Where('project_status','=','2')
                                                      ->whereHas('project_bid_info',function ($query)
                                                       {
                                                         $query->where('bid_status',4);
                                                       })
                                                      ->with(['skill_details','project_skills.skill_data']);

          if($sort_by == "is_urgent")
          {
            $obj_awarded_projects = $obj_awarded_projects->where('is_urgent','1')->orderBy('created_at','desc');
          }
          else if($sort_by == "public_type" || $sort_by == "private_type")
          {
            $project_type = '0';
            if($sort_by == "public_type")
            {
              $project_type = '0';
            }
            elseif($sort_by == "private_type")
            {
              $project_type = '1';
            }
            $obj_awarded_projects = $obj_awarded_projects->where('project_type',$project_type)->orderBy('created_at','desc');
          }
          else if($sort_by == "is_nda")
          {
            $obj_awarded_projects = $obj_awarded_projects->where('is_nda','1')->orderBy('created_at','desc');
          }

          $obj_awarded_projects      = $obj_awarded_projects
                                            ->get();

          $obj_awarded_projects = collect($obj_awarded_projects);

          if( $sort_by == "asc")
          { 
            $obj_awarded_projects = $obj_awarded_projects
                                    ->sortBy(['created_at']);
          }
          else if($sort_by == "desc")
          { 
            $obj_awarded_projects = $obj_awarded_projects
                                    ->sortByDesc(['created_at']);
          }

          if($sort_by == "")
          {
            $obj_awarded_projects = $obj_awarded_projects
                                    ->sortByDesc(['created_at']);
          }

          $arr_tmp_award_projects = [];
          if($obj_awarded_projects){
            $arr_tmp_award_projects = $obj_awarded_projects->toArray();
            $arr_tmp_award_projects = array_values($arr_tmp_award_projects);
          }

          $arr_awarded_projects = $arr_pagination = [];
          $obj_award_project =  $this->make_pagination_links($arr_tmp_award_projects,config('app.project.pagi_cnt'));
          if($obj_award_project)
          {
              $arr_awarded_projects = $obj_award_project->toArray();
              $arr_pagination      = clone $obj_award_project;
          }

         
          $this->arr_view_data['arr_awarded_projects']  = $arr_awarded_projects;
          $this->arr_view_data['arr_pagination']        = $arr_pagination;
          $this->arr_view_data['sort_by']        = $sort_by;
          $this->arr_view_data['module_url_path']       = $this->module_url_path;
          return view('project_manager.projects.awarded_projects',$this->arr_view_data);
   }

    /*
      Comment: load page of Completed project 
      Author :  Nayan Sonawane
    */

    public function show_completed_projects(Request $request)
    {
       $sort_by = '';
      if($request->has('sort_by') && $request->input('sort_by')!='')
      {
        $sort_by = $request->input('sort_by');
      }

      $this->arr_view_data['page_title'] = trans('controller_translations.page_title_manage_completed_projects');
      
      /*$obj_completed_projects = $this->ProjectpostModel->where('project_manager_user_id',$this->user_id)
                                                       ->where('project_status','3')
                                                       ->with(['skill_details','client_details','project_skills.skill_data','category_details'])
                                                       ->orderBy('created_at','DESC')
                                                       ->paginate(config('app.project.pagi_cnt'));
                                                       
      $arr_completed_projects = [];

      if($obj_completed_projects)
      {
        $arr_pagination         =  clone $obj_completed_projects;
        $arr_completed_projects = $obj_completed_projects->toArray();
      }*/

      $obj_completed_projects = $this->ProjectpostModel->where('project_manager_user_id',$this->user_id)
                                                       ->where('project_status','3')
                                                       ->with(['skill_details','client_details','project_skills.skill_data','category_details']);
        if($sort_by == "is_urgent")
        {
          $obj_completed_projects = $obj_completed_projects->where('is_urgent','1')->orderBy('created_at','desc');
        }
        else if($sort_by == "public_type" || $sort_by == "private_type")
        {
          $project_type = '0';
          if($sort_by == "public_type")
          {
            $project_type = '0';
          }
          elseif($sort_by == "private_type")
          {
            $project_type = '1';
          }
          $obj_completed_projects = $obj_completed_projects->where('project_type',$project_type)->orderBy('created_at','desc');
        }
        else if($sort_by == "is_nda")
        {
          $obj_completed_projects = $obj_completed_projects->where('is_nda','1')->orderBy('created_at','desc');
        }

        $obj_completed_projects      = $obj_completed_projects
                                          ->get();

        $obj_completed_projects = collect($obj_completed_projects);

        if( $sort_by == "asc")
        { 
          $obj_completed_projects = $obj_completed_projects
                                  ->sortBy(['created_at']);
        }
        else if($sort_by == "desc")
        { 
          $obj_completed_projects = $obj_completed_projects
                                  ->sortByDesc(['created_at']);
        }

        if($sort_by == "")
        {
          $obj_completed_projects = $obj_completed_projects
                                  ->sortByDesc(['created_at']);
        }

        $arr_tmp_completed_projects = [];
        if($obj_completed_projects){
          $arr_tmp_completed_projects = $obj_completed_projects->toArray();
          $arr_tmp_completed_projects = array_values($arr_tmp_completed_projects);
        }

        $arr_completed_projects = $arr_pagination = [];
        $obj_complete_project =  $this->make_pagination_links($arr_tmp_completed_projects,config('app.project.pagi_cnt'));
        if($obj_complete_project)
        {
            $arr_completed_projects = $obj_complete_project->toArray();
            $arr_pagination      = clone $obj_complete_project;
        }

      $this->arr_view_data['arr_completed_projects']  = $arr_completed_projects;
      $this->arr_view_data['arr_pagination']          = $arr_pagination;
      $this->arr_view_data['sort_by']        = $sort_by;
      $this->arr_view_data['module_url_path']         = $this->module_url_path;
      return view('project_manager.projects.completed_projects',$this->arr_view_data);
    }



    /*
      Comment: load page of Canceled project 
      Author :  Ashwini k
    */

    public function show_canceled_projects()
    {
      $this->arr_view_data['page_title'] = trans('controller_translations.page_title_manage_canceled_projects');
      
      $obj_canceled_projects = $this->ProjectpostModel->where('project_manager_user_id',$this->user_id)
                                                       ->where('project_status','5')
                                                       ->with(['skill_details','client_details','project_skills.skill_data','category_details'])
                                                       ->orderBy('created_at','DESC')
                                                       ->paginate(config('app.project.pagi_cnt'));
                                                       
      $arr_canceled_projects = [];

      if($obj_canceled_projects)
      {
        $arr_pagination         =  clone $obj_canceled_projects;
        $arr_canceled_projects  =  $obj_canceled_projects->toArray();
      }

      $this->arr_view_data['arr_canceled_projects']   = $arr_canceled_projects;
      $this->arr_view_data['arr_pagination']          = $arr_pagination;
      $this->arr_view_data['module_url_path']         = $this->module_url_path;
      return view('project_manager.projects.canceled_projects',$this->arr_view_data);
    }


    /*
      Comment:  Show project Details. 
      Author :  Nayan Sonawane
    */  
    public function show_project_details(Request $request , $id)
    {
      /*unset PROJECT_ID FROm Session */
      if(Session::has('PROJECT_ID')){   
        $request->session()->forget('PROJECT_ID');
      }
      $id=base64_decode($id);
      if($id==""){
        return redirect()->back();
      }
      $this->arr_view_data['page_title'] = trans('controller_translations.page_title_project_details');
      $obj_project_info = $this->ProjectpostModel
                               ->with('skill_details','client_info','project_skills.skill_data','client_details','category_details','project_bid_info','project_bids_infos','project_post_documents','sub_category_details')
                               ->where('id',$id)
                               ->first();
      $arr_project_info       = array();
      $arr_project_bid_info   = array();
      $arr_project_attachment = array();
      if($obj_project_info != FALSE){
        $arr_project_info = $obj_project_info->toArray();
      }
      $project_id = $arr_project_info['id'];
      $obj_project_bid_info = $this->ProjectsBidsModel->where('project_id',$project_id)
                                                      ->with('expert_details.expert_categories.categories')
                                                      ->with(['user_details','expert_details'])
                                                      ->orderBy('created_at','DESC')
                                                      ->paginate(2);
      if($obj_project_bid_info != FALSE){
        $arr_pagination       = clone $obj_project_bid_info;
        $arr_project_bid_info = $obj_project_bid_info->toArray();
      }
      $obj_project_attachment = $this->ProjectAttchmentModel->where('project_id',$project_id)->get();
      if($obj_project_attachment){
        $arr_project_attachment = $obj_project_attachment->toArray();         
      }
      $is_bid_awarded = $this->ProjectsBidsModel->where('project_id',$project_id)
                                                ->where('bid_status','4')
                                                ->count();
      $this->arr_view_data['is_bid_awarded']        = isset($is_bid_awarded)?$is_bid_awarded:0;
      $this->arr_view_data['projectInfo']           = $arr_project_info;
      $this->arr_view_data['arr_project_bid_info']  = $arr_project_bid_info;
      $this->arr_view_data['arr_pagination']        = $arr_pagination;
      $this->arr_view_data['module_url_path']       = $this->module_url_path;
      $this->arr_view_data['arr_project_attachment']= $arr_project_attachment;
      $this->arr_view_data['expert_project_attachment_public_path']  = $this->expert_project_attachment_public_path; 
      return view('project_manager.projects.project_details',$this->arr_view_data);
    }

    
    public function suggest($enc_id)
    {
      $id =  base64_decode($enc_id);


      $arr_data['is_suggested'] = 1;

      /* create notification */
      $obj_project = $this->ProjectsBidsModel->where('id','=',$id)
                                             ->whereHas('project_details', function () {})
                                             ->with(['project_details' => function ($query) {
                                                  //$query->select('id','client_user_id');
                                             }])->first();  

      $client_user_id = '';
      $arr_project  = [];

      if($obj_project)
      {
        $arr_project    = $obj_project->toArray();
        $client_user_id = isset($arr_project['project_details']['client_user_id'])?$arr_project['project_details']['client_user_id']:'';
        $project_id     = isset($arr_project['project_details']['id'])?$arr_project['project_details']['id']:'';
        $project_status = isset($arr_project['project_details']['project_status'])?$arr_project['project_details']['project_status']:'';

        if($project_id == ""){
          session::flash('error',trans('controller_translations.error_unauthorized_action'));
          return redirect()->back();
        }
        //chech project is already awarded or not
         $is_project_awarded = $this->ProjectsBidsModel->where('project_id',$project_id)
                                                      ->where('bid_status','4')
                                                      ->first();
          if(isset($is_project_awarded) && $project_status != null){
           session::flash('error',trans('controller_translations.text_this_project_is_already_awarded'));
            return redirect()->back();
          }
      }else{ 
        session::flash('error',trans('controller_translations.error_unauthorized_action'));
        return redirect()->back();
      }

      $arr_notification_data =  [];

      if(isset($client_user_id) && $client_user_id != "")
      {
        $arr_notification_data['user_id']    = $client_user_id;
        $arr_notification_data['user_type']  = '2';
        $arr_notification_data['url']        = 'client/projects/open';
        $arr_notification_data['project_id'] = $project_id;
        
        $posted_project_name = isset($obj_project->project_name)?$obj_project->project_name:'';

        /*$arr_notification_data['notification_text'] =  trans('controller_translations.new_bids_suggested_by_project_manager');*/

        $arr_notification_data['notification_text_en'] =  $posted_project_name . ' - ' . Lang::get('controller_translations.new_bids_suggested_by_project_manager',[],'en','en');
        $arr_notification_data['notification_text_de'] =  $posted_project_name . ' - ' . Lang::get('controller_translations.new_bids_suggested_by_project_manager',[],'de','en');


        $this->NotificationsModel->create($arr_notification_data);
      }
      /* Notification ends*/

      /* Send mail to client that project is suggested by project manager */
      $this->MailService->email_to_client_on_bid_suggestion_by_pm($project_id);
      /*mail sent ends*/

      $is_update = $this->ProjectsBidsModel->where('id',$id)->update($arr_data);

      if($is_update)
      {
        session::flash('success',trans('controller_translations.success_expert_successfully_suggested'));
      }
      else
      {
        session::flash('error',trans('controller_translations.error_problem_while_suggest_expert'));
      }

        return redirect()->back();
    }

    public function unsuggest($enc_id)
    {
      $id =  base64_decode($enc_id);

      $arr_data['is_suggested'] = 0;

      $is_update = $this->ProjectsBidsModel->where('id',$id)->update($arr_data);

      if($is_update)
      {
        session::flash('success',trans('controller_translations.success_expert_successfully_unsuggested'));
      }
      else
      {
        session::flash('error',trans('controller_translations.error_problem_while_suggest_expert'));
      }

        return redirect()->back();
    }

    public function more_details($enc_id, $user_id)
    { 
      $user_id = base64_decode($user_id);

      $id      = base64_decode($enc_id);

      $this->arr_view_data['page_title'] = trans('controller_translations.page_title_more_details_of_projects');

      $obj_ongoing_projects = $this->ProjectpostModel->where('id',$id)
                                                     ->where('project_status','4')
                                                     ->with(['skill_details','client_details','category_details'])
                                                     ->orderBy('created_at','DESC')
                                                     ->paginate(1);
      $arr_ongoing_projects = [];

      if($obj_ongoing_projects)
      {
        $arr_pagination         =  clone $obj_ongoing_projects;
        $arr_ongoing_projects = $obj_ongoing_projects->toArray();
      }

      $obj_project_bid_info = $this->ProjectsBidsModel->where('project_id',$id)
                                                      ->where('expert_user_id',$user_id)
                                                      ->with(['project_details','expert_details.education_details','expert_details.expert_work_experience','expert_details.expert_certification_details'])
                                                      ->first();

      if($obj_project_bid_info != FALSE)
      {
        $arr_project_bid_info = $obj_project_bid_info->toArray();
      }

      $this->arr_view_data['arr_ongoing_projects']  = $arr_ongoing_projects;
      $this->arr_view_data['arr_project_bid_info']  = $arr_project_bid_info;
      $this->arr_view_data['arr_pagination']        = $arr_pagination;
      $this->arr_view_data['module_url_path']       = $this->module_url_path;
      $this->arr_view_data['bid_attachment_public_path']  = $this->bid_attachment_public_path;

      return view('project_manager.projects.ongoing_projects_details',$this->arr_view_data);
    } 
     /*
      Auther : Bharat K.
    */
    public function notifications_seen($id)
    {
      if($id && $id!="")
      {
        $notification_id =  base64_decode($id);
        $status = $this->NotificationsModel->where('id',$notification_id)->update(['is_seen'=>'1']);

        if($status)
        {
          $data['status'] = 'success';
          echo json_encode($data);
        } 
        else
        {
          $data['status'] = 'error';
          echo json_encode($data);
        }
      }  
    } 

    public function get_experts(Request $request){
      $html            = '';
      $already_invited = $already_bidded_arr = $already_invited_arr = [];
      if(!empty($request->input('project_id')) && $request->input('project_id') !=""){
          $project_id = base64_decode($request->input('project_id'));

          $get_already_invited = $this->InviteProjectModel->select('expert_user_id')->where('project_id',$project_id)->groupBy('expert_user_id')->get();
          
          if($get_already_invited){
             $already_invited = $get_already_invited->toArray(); 
             foreach ($already_invited as $key => $value) {
               $already_invited_arr[] = $value['expert_user_id'];
             }
          }

          $get_already_bidded = $this->ProjectsBidsModel->select('expert_user_id')->where('project_id',$project_id)->groupBy('expert_user_id')->get();
          
          if($get_already_bidded){
             $already_bidded = $get_already_bidded->toArray(); 
             foreach ($already_bidded as $key => $value) {
               $already_bidded_arr[] = $value['expert_user_id'];
             }
          }
          $arr_experts = []; 
          $obj_experts = $this->ExpertsModel
                         ->join('users','users.id' , '=' , 'experts.user_id')
                         ->join('experts_skills','experts_skills.expert_user_id' , '=' , 'experts.user_id')
                         ->join('project_skills','project_skills.skill_id' , '=' , 'experts_skills.skill_id')
                         ->join('experts_categories','experts_categories.expert_user_id' , '=' , 'experts.user_id')
                         ->join('projects','projects.category_id' , '=' , 'experts_categories.category_id')
                         ->where('projects.id',$project_id)
                         ->where('users.is_active',1)
                         ->where('users.is_available',1)
                         ->with(['user_details'])->whereHas('user_details',function ($query){
                            $query->where('is_active',1);
                            $query->where('is_available',1);
                          })
                         ->groupBy('experts.user_id')
                         ->get();

          if($obj_experts != FALSE){
            $arr_experts        = $obj_experts->toArray();
            $html = '<option value="">--'.trans('client/projects/invite_experts.text_select').'--</option>';
              if(isset($arr_experts) && (sizeof($arr_experts)>0)){ 
                foreach($arr_experts as $expert){
                  $disabled = $style = $title = '';
                  if(isset($this->profile_img_public_path) && isset($expert['profile_image']) && $expert['profile_image']!=NULL && file_exists('public/uploads/front/profile/'.$expert['profile_image'])){
                  $profile_image = $this->profile_img_public_path.$expert['profile_image'];
                  } else {
                  $profile_image = $this->profile_img_public_path.'default_profile_image.png';
                  }
                  $first_name = isset($expert['user_details']['role_info']['first_name'])?$expert['user_details']['role_info']['first_name']:'Unknown user';
                  $last_name  = isset($expert['user_details']['role_info']['last_name'])?substr($expert['user_details']['role_info']['last_name'],0,1).'.':'';
                  
                  if(isset($already_invited_arr)){
                      if(in_array($expert['user_id'], $already_invited_arr)){
                         $disabled = 'disabled';
                         $style    = 'style="cursor:no-drop;"';
                         $title    = '- <small>(Already invited)</small>';
                      }
                  }
                  if(isset($already_bidded_arr)){
                      if(in_array($expert['user_id'], $already_bidded_arr)){
                         $disabled = 'disabled';
                         $style    = 'style="cursor:no-drop;"';
                         $title    = '- <small>(Bid already arrived)</small>';
                      }
                  }                 
                  $html .= '<option '.$disabled.' '.$style.'  value="'.base64_encode($expert['user_id']).'" email="'.$expert['user_details']['email'].'" src="'.$profile_image.'" name="'.$first_name.'"> '.$first_name.' '.$last_name.' '.$title.'</option>';
                }
              } 
          }  
          echo $html;
      } else {
          echo false; 
          exit;
      }
    }
    public function invite_experts(Request $request){
        if(!empty($request->input('project-id'))          && 
                  $request->input('project-id') != ""     &&
           !empty($request->input('experts'))             && 
                  $request->input('experts') != ""        &&
           !empty($request->input('expert-email'))        && 
                  $request->input('expert-email') != ""   &&
           !empty($request->input('invite-message'))      && 
                  $request->input('invite-message') != ""
        ){

          $get_project_invitations_count  = \DB::table('invite_to_project')->where('project_id', base64_decode($request->input('project-id')))->count();
          if($get_project_invitations_count >= 5){
            Session::flash('error', trans('controller_translations.sorry_you_have_exited_invitation_sent_request_limit')); 
            return redirect()->back();
          }
          $expire_on        = date('Y-m-d H:i:s', strtotime("+2 days"));
          $project_id       = $request->input('project-id',null);
          $experts_user_id  = $request->input('experts',null);
          $expert_email     = $request->input('expert-email',null);
          $invite_message   = $request->input('invite-message',null);
          $arr_profile_data = [];

          $is_expert_bided    = $this->ProjectsBidsModel
                                ->where('project_id',base64_decode($project_id))
                                ->where('expert_user_id',base64_decode($experts_user_id))
                                ->count();
          if(isset($is_expert_bided) && $is_expert_bided >'0')
          {
            Session::flash("error",trans('controller_translations.text_this_expert_have_already_bidded_on_this_project_so_you_can_not_invite_him_again'));
            return redirect()->back();
          }

          $is_invitation_sent = $this->InviteProjectModel
                                ->where('project_id',base64_decode($project_id))
                                ->where('expert_user_id',base64_decode($experts_user_id))
                                ->count();

          if(isset($is_invitation_sent) && $is_invitation_sent >'0')
          {
            Session::flash("error",trans('invitations/invitation.text_You_have_already_sent_invitation_to_this_expert'));
            return redirect()->back();
          } 
          if(isset($experts_user_id) && $experts_user_id != ""){
            $arr_profile_data = sidebar_information(base64_decode($experts_user_id));
          }
          $expert_first_name = $expert_last_name = "";
          $expert_first_name = isset($arr_profile_data['user_details']['first_name'])?$arr_profile_data['user_details']['first_name']:'Unknown user';
          $expert_last_name  = isset($arr_profile_data['user_details']['last_name'])?substr($arr_profile_data['user_details']['last_name'],0,1):''; 

          // create unique indentifier
            $rs  = \DB::table('invite_to_project')->select('unique_id')->where('unique_id' ,'!=' , null)->where('unique_id' ,'!=' , '')->orderBy('id' , 'DESC')->LIMIT('1')->get();
            $res = $rs;
            if(count($res) > 0)
            {                 
                $max_inv_nbr = $res[0]->unique_id;
                // remove STU from that no.                
                $max_inv_nbr = substr($max_inv_nbr, 3);
                // get count of that no.
                $cnt = strlen($max_inv_nbr);
                // increment that no. by 1
                $max_inv_nbr = $max_inv_nbr + 1;                        
                // add no. of zero's to the no.                
                $max_inv_nbr = str_pad($max_inv_nbr, $cnt, '0', STR_PAD_LEFT);
                $max_inv_nbr = 'INV'.$max_inv_nbr;  
            }
            else
            {
                $max_inv_nbr = 'INV000000001';  
            }
          // create unique indentifier
          
          /* Send notification */
          $arr_data                         = [];
          $arr_data['user_id']              = base64_decode($experts_user_id);
          $arr_data['user_type']            = '3';
          $arr_data['url']                  = "expert/projects/details/".$project_id;
          $arr_data['notification_text_en'] = $max_inv_nbr.'-'.Lang::get('controller_translations.you_have_new_invitation_for_project_bid',[],'en','en');
          $arr_data['notification_text_de'] = $max_inv_nbr.'-'.Lang::get('controller_translations.you_have_new_invitation_for_project_bid',[],'de','en');
          $arr_data['project_id']           = base64_decode($project_id);
          $send_notification = $this->NotificationsModel->insertGetId($arr_data); 
          /* End Send notification */ 
         
          if($send_notification){
            $arr_data['invite_message'] = $invite_message;
            $arr_data['expert_name']    = $expert_first_name/*.' '.$expert_last_name*/;
            $arr_data['unique_id']      = $max_inv_nbr;
            $arr_data['email_id']       = isset($expert_email)? $expert_email:'';
            $email_to                   = isset($expert_email)? $expert_email:''; //'tushara@webwingtechnologies.com';//
            $mail_form                  = get_site_email_address();
            $mail_form                  = isset($mail_form)? $mail_form:'archexperts@gmail.com';

                if($email_to!= "")
                {
                  $project_name = config('app.project.name');
                  try{
                        $mail_status = $this->MailService->send_invite_expert_email($arr_data);
                      // Mail::send('front.email.invite_expert', $arr_data, function ($invite_message) use ($email_to,$mail_form,$project_name) {
                      //     $invite_message->from($mail_form, $project_name);
                      //     $invite_message->subject($project_name.trans('controller_translations.text_invitation_for_project_bid'));
                      //     $invite_message->to($email_to);
                      // });
                  }
                  catch(\Exception $e){
                    Session::flash('error',trans('controller_translations.text_mail_not_sent')); 
                  }
                  /* store invite project data*/
                     $store_arr = [];
                     $store_arr['project_id']      = base64_decode($project_id);
                     $store_arr['client_user_id']  = $this->user_id;
                     $store_arr['expert_user_id']  = base64_decode($experts_user_id);
                     $store_arr['expert_email_id'] = $email_to;
                     $store_arr['message']         = $invite_message;
                     $store_arr['notification_id'] = $send_notification;
                     $store_arr['expire_on']       = $expire_on;
                     $store_arr['unique_id']       = $max_inv_nbr;
                     $this->InviteProjectModel->insertGetId($store_arr);
                  /* end store invite project data*/
                  Session::flash('success', trans('controller_translations.you_have_successfully_sent_invitation_to_client')); 
                  return redirect()->back();
              }
          } else {
            Session::flash('error', trans('controller_translations.something_went_wrong_please_try_again_later')); 
            return redirect()->back();
          }
      }
      else{
        Session::flash('error', trans('controller_translations.something_went_wrong_please_try_again_later')); 
        return redirect()->back();
      }
    }

    private function make_pagination_links($items,$perPage)
    {
        $pageStart = \Request::get('page', 1);
        // Start displaying items from this number;
        $offSet = ($pageStart * $perPage) - $perPage; 

        // Get only the items you need using array_slice
        $itemsForCurrentPage = array_slice($items, $offSet, $perPage, true);

        return new LengthAwarePaginator($itemsForCurrentPage, count($items), $perPage,Paginator::resolveCurrentPage(), array('path' => Paginator::resolveCurrentPath()));
    } 
}