<?php

namespace App\Http\Controllers\Front\Project_manager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\ProjectpostModel;
use App\Models\MilestonesModel;

use Sentinel;
use Validator;
use Session;

class DashboardController extends Controller
{
    public $arr_view_data;
    public function __construct(  ProjectpostModel $projectpost,
                                  MilestonesModel $milestones
                                )
    {
      $this->arr_view_data = [];

      if(! $user = Sentinel::check()) 
      {
        return redirect('/login');
      }

      if(!$user->inRole('project_manager')) 
      {
        return redirect('/login'); 
      }

      $this->user_id = $user->id;

      $this->ProjectpostModel   = $projectpost;
      $this->MilestonesModel    = $milestones;

      $this->view_folder_path   = 'project_manager/dashboard';
      $this->module_url_path    = url("/project_manager");
    }

    /* 
      Auther : Nayan S.
    */

    public function show_dashboard()
    {   
       /*Finding Ongoing projects count*/
       $count_ongoing_projects = $this->ProjectpostModel->where('project_manager_user_id',$this->user_id)
                                                     ->where('project_status','=','4')
                                                     ->count();

      /*Finding Completed projects count*/
      $count_completed_projects = $this->ProjectpostModel->where('project_manager_user_id',$this->user_id)
                                                       ->where('project_status','3')
                                                       ->count();    

      /*Finding Open projects count*/
       $count_open_projects = $this->ProjectpostModel->where('project_manager_user_id',$this->user_id)
                                                     ->where('project_status','=','2')
                                                     ->count();

      /* Finding Canceled projects count */
      $count_canceled_projects = $this->ProjectpostModel->where('project_manager_user_id',$this->user_id)
                                                       ->where('project_status','5')
                                                       ->count();

     
      $this->arr_view_data['page_title']      = trans('controller_translations.page_title_dashboard');
      $this->arr_view_data['count_ongoing_projects']    = $count_ongoing_projects;
      $this->arr_view_data['count_completed_projects']  = $count_completed_projects;
      $this->arr_view_data['count_open_projects']       = $count_open_projects;
      $this->arr_view_data['count_canceled_projects']   = $count_canceled_projects;
      $this->arr_view_data['module_url_path'] = $this->module_url_path;
      return view($this->view_folder_path.'.dashboard',$this->arr_view_data);
   }


}
