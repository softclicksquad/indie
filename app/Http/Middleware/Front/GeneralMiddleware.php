<?php

namespace App\Http\Middleware\Front;

use Closure;
use Sentinel;
use Session;


use App\Models\SiteSettingModel;
use App\Models\CategoriesModel;
use App\Models\ClientsModel;
use App\Models\ExpertsModel;
use App\Models\ProjectManagerModel;
use App\Models\RecruiterModel;
use App\Models\StaticPageModel;
use App\Models\SkillsModel;
use App\Models\CurrencyModel;
use App\Common\Services\TwilioChatService; 
use App;


class GeneralMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next )
    {
        $arr_currency = [];
        $obj_currency = CurrencyModel::select('id','currency_code','currency','description')
                                            ->where('is_active','1')
                                            ->get();
        if($obj_currency)
        {
          $arr_currency  = $obj_currency->toArray();
          view()->share('arr_currency_backend',$arr_currency);
        }
       //dd($arr_currency);
        //share selected languge to view and also set in session
        if(!Session::has('locale'))
        {
           Session::put('locale', \Config::get('app.locale'));
        }
        app()->setLocale(Session::get('locale'));
        view()->share('selected_lang',\Session::get('locale'));
        
        view()->share('admin_panel_slug',config('app.project.admin_panel_slug'));

        $arr_settings = SiteSettingModel::where('site_settting_id',1)->first();

        if($arr_settings)
        {
            $arr_settings = $arr_settings->toArray(); 
           
           if (isset($arr_settings['site_status']) && $arr_settings['site_status']==0) 
           {
                return response()->view('front.home.maintenance_mode');
                //return redirect('/maintenance_mode')->send();
           }
           view()->share('arr_settings',$arr_settings);
        }

       

        $user = Sentinel::check();
        $current_url_route = app()->router->getCurrentRoute()->uri();
        //dd($user->id);
        if($user)
        {
            $is_valid = true;
            if($request->ajax()){
                $is_valid = false;
            } else if($request->isMethod('post')){
                $is_valid = false;
            }

            // create access token for twilio chat
            if(\Request::segment(2) != 'twilio-chat' && $is_valid == true) {
                $objTwilioChatService = new TwilioChatService();
                $access_token = $objTwilioChatService->genrate_access_token( $user->id );
                view()->share('twilio_access_token',$access_token);
                $twilio_user_type = '';
                if($user->inRole('client')){
                    $twilio_user_type = 'client';
                } else if($user->inRole('expert')){
                    $twilio_user_type = 'expert';
                }
                view()->share('twilio_user_type',$twilio_user_type);
            }

            if($user->inRole('client'))
            {
                $client_details = ClientsModel::with(['user_details'])->where('user_id',$user->id)->first();
                if ($client_details) 
                {
                    $user_auth_details = array();
                    $client_details = $client_details->toArray();
                    
                    $profile_image           = url('/public/uploads/front/profile/default_profile_image.png');
                    $profile_img_base_path   = base_path().'/public'.config('app.project.img_path.profile_image');
                    $profile_img_public_path = url('/public').config('app.project.img_path.profile_image');
                    
                    if( isset($client_details['profile_image']) && $client_details['profile_image']!='' && file_exists($profile_img_base_path.$client_details['profile_image'])){
                        $profile_image = $profile_img_public_path.$client_details['profile_image'];
                    }
                    
                    $user_auth_details['is_login'] = TRUE;
                    $user_auth_details['first_name'] = isset($client_details['first_name'])?$client_details['first_name']:'';
                    $user_auth_details['last_name'] = isset($client_details['last_name'])?$client_details['last_name']:'';
                    $user_auth_details['profile_image'] = $profile_image;
                    $user_auth_details['is_online']  = isset($client_details['user_details']['is_online'])?$client_details['user_details']['is_online']:'';
                    $user_auth_details['currency_code']  = isset($client_details['user_details']['currency_code'])?$client_details['user_details']['currency_code']:'';
                    $user_auth_details['currency_symbol']  = isset($client_details['user_details']['currency_symbol'])?$client_details['user_details']['currency_symbol']:'';
                    $user_auth_details['kyc_verified']  = isset($client_details['user_details']['kyc_verified'])?$client_details['user_details']['kyc_verified']:'';
                    view()->share('user_auth_details',$user_auth_details);
                }
            }
            elseif($user->inRole('expert'))
            {
                
                $expert_details = ExpertsModel::with(['user_details'])->where('user_id',$user->id)->first();
                if ($expert_details) 
                {
                    $user_auth_details = array();
                    $expert_details = $expert_details->toArray();

                    $profile_image           = url('/public/uploads/front/profile/default_profile_image.png');
                    $profile_img_base_path   = base_path().'/public'.config('app.project.img_path.profile_image');
                    $profile_img_public_path = url('/public').config('app.project.img_path.profile_image');
                    
                    if( isset($expert_details['profile_image']) && $expert_details['profile_image']!='' && file_exists($profile_img_base_path.$expert_details['profile_image'])){
                        $profile_image = $profile_img_public_path.$expert_details['profile_image'];
                    }

                    $user_auth_details['is_login'] = TRUE;
                    $user_auth_details['first_name'] = isset($expert_details['first_name'])?$expert_details['first_name']:'';
                    $user_auth_details['last_name'] = isset($expert_details['last_name'])?$expert_details['last_name']:'';
                    $user_auth_details['profile_image'] = $profile_image;
                    $user_auth_details['is_online']  = isset($expert_details['user_details']['is_online'])?$expert_details['user_details']['is_online']:'';
                    $user_auth_details['kyc_verified']  = isset($expert_details['user_details']['kyc_verified'])?$expert_details['user_details']['kyc_verified']:'';
                    $user_auth_details['currency_code']  = isset($expert_details['user_details']['currency_code'])?$expert_details['user_details']['currency_code']:'';
                    $user_auth_details['currency_symbol']  = isset($expert_details['user_details']['currency_symbol'])?$expert_details['user_details']['currency_symbol']:'';
                    view()->share('user_auth_details',$user_auth_details);
                }
            }
            elseif($user->inRole('project_manager'))
            {
                
                $expert_details = ProjectManagerModel::where('user_id',$user->id)->first(['id','first_name','last_name','profile_image']);
                if ($expert_details) 
                {
                    $user_auth_details = array();
                    $expert_details = $expert_details->toArray();

                    $profile_image           = url('/public/uploads/front/profile/default_profile_image.png');
                    $profile_img_base_path   = base_path().'/public'.config('app.project.img_path.profile_image');
                    $profile_img_public_path = url('/public').config('app.project.img_path.profile_image');
                    
                    if( isset($expert_details['profile_image']) && $expert_details['profile_image']!='' && file_exists($profile_img_base_path.$expert_details['profile_image'])){
                        $profile_image = $profile_img_public_path.$expert_details['profile_image'];
                    }

                    $user_auth_details['is_login'] = TRUE;
                    $user_auth_details['first_name'] = isset($expert_details['first_name'])?$expert_details['first_name']:'';
                    $user_auth_details['last_name'] = isset($expert_details['last_name'])?$expert_details['last_name']:'';
                    $user_auth_details['profile_image'] = $profile_image;
                    $user_auth_details['is_online']  = isset($expert_details['user_details']['is_online'])?$expert_details['user_details']['is_online']:'';
                    $user_auth_details['kyc_verified']  = isset($expert_details['user_details']['kyc_verified'])?$expert_details['user_details']['kyc_verified']:'';

                    view()->share('user_auth_details',$user_auth_details);
                }
            }
            elseif($user->inRole('recruiter'))
            {
                
                $expert_details = RecruiterModel::where('user_id',$user->id)->first(['id','first_name','last_name','profile_image']);
                if ($expert_details) 
                {
                    $user_auth_details = array();
                    $expert_details = $expert_details->toArray();

                    $profile_image           = url('/public/uploads/front/profile/default_profile_image.png');
                    $profile_img_base_path   = base_path().'/public'.config('app.project.img_path.profile_image');
                    $profile_img_public_path = url('/public').config('app.project.img_path.profile_image');
                    
                    if( isset($expert_details['profile_image']) && $expert_details['profile_image']!='' && file_exists($profile_img_base_path.$expert_details['profile_image'])){
                        $profile_image = $profile_img_public_path.$expert_details['profile_image'];
                    }

                    $user_auth_details['is_login'] = TRUE;
                    $user_auth_details['first_name'] = isset($expert_details['first_name'])?$expert_details['first_name']:'';
                    $user_auth_details['last_name'] = isset($expert_details['last_name'])?$expert_details['last_name']:'';
                    $user_auth_details['profile_image'] = $profile_image;
                    $user_auth_details['is_online']  = isset($expert_details['user_details']['is_online'])?$expert_details['user_details']['is_online']:'';
                    $user_auth_details['kyc_verified']  = isset($expert_details['user_details']['kyc_verified'])?$expert_details['user_details']['kyc_verified']:'';

                    view()->share('user_auth_details',$user_auth_details);
                }
            }


            //$session_value = Session::get('arr_data_bal');//to get the session value
            //dd($session_value);
            $is_valid = true;
            if($request->ajax()){
                $is_valid = false;
            } else if($request->isMethod('post')){
                $is_valid = false;
            }
            // $is_valid = false;
            if($is_valid) 
            {
                $curr = get_default_currency($user->id);
            
                $user = Sentinel::check();  
                $arr_data_bal = [];
                $mangopay_wallet_details = get_logged_user_wallet_details($curr);
                
                if (isset($mangopay_wallet_details) && count($mangopay_wallet_details)>0)
                {
                  foreach ($mangopay_wallet_details as $key => $value) 
                  {
                       $arr_data_bal['balance'][]  = isset($value->Balance->Amount)?$value->Balance->Amount/100:'0';
                       $arr_data_bal['currency'][] = isset($value->Balance->Currency)?$value->Balance->Currency:'';
                  }
                }
                //Session::put('arr_data_bal',$arr_data_bal); //to put the session value
                view()->share('arr_data_bal',$arr_data_bal);

                $count_mangopay_wallet = 0;
                $count_mangopay_wallet_details = get_user_all_wallet_details();
                if(!empty($count_mangopay_wallet_details))
                {
                    $count_mangopay_wallet = count($count_mangopay_wallet_details);
                }
                view()->share('count_mangopay_wallet',$count_mangopay_wallet);
            }


            /* Notifications array */
            $arr_notification   = [];
            $obj_notification   = app(App\Common\Services\NotificationService::class);
            $arr_notification   = $obj_notification->show_notifications();
            view()->share('arr_notification',$arr_notification); 
            
        }
        
        $site_setting = SiteSettingModel::where('site_settting_id',1)->first();
        if ($site_setting) 
        {
            $site_setting = $site_setting->toArray();
            if (isset($site_setting['site_email_address']) && $site_setting['site_email_address']!="") 
            {
                view()->share('website_contact_email',$site_setting['site_email_address']);    
                view()->share('mail_form','no-reply@archexperts.com');    
            }
            
        }

        //static pages links share in footer
        $static_pages = StaticPageModel::where('is_active',1)->get();
        if ($static_pages)
        {
            $static_pages = $static_pages->toArray();
            if(isset($static_pages) && sizeof($static_pages))
            {
                view()->share('static_pages',$static_pages); 
            }
        }

        $obj_static_privacy =  StaticPageModel::where('page_slug','privacy-policy')->where('is_active',"1")->first();
        $arr_static_privacy = array();
        if($obj_static_privacy)
        {
          $arr_static_privacy = $obj_static_privacy->toArray();
          view()->share('arr_static_privacy',$arr_static_privacy); 
        }
        
        $obj_static_terms =  StaticPageModel::where('page_slug','terms-of-service')->where('is_active',"1")->first();
        $arr_static_terms = array();
        if($obj_static_terms)
        {
          $arr_static_terms = $obj_static_terms->toArray();
          view()->share('arr_static_terms',$arr_static_terms); 
        }


        $obj_skills = SkillsModel::where('is_active','1')->limit(5)->get();

        if ($obj_skills!=FALSE) 
        {
            $footer_skills = [];
            $footer_skills = $obj_skills->toArray();
            if (isset($footer_skills) && sizeof($footer_skills)>0) 
            {
                view()->share('footer_skills',$footer_skills);
            }
        }


        $obj_category = CategoriesModel::where('is_active','1')->limit(5)->get();

        if ($obj_category!=FALSE) 
        {
            $footer_categories = [];
            $footer_categories = $obj_category->toArray();
            //dd($footer_categories);
            if (isset($footer_categories) && sizeof($footer_categories)>0) 
            {
                view()->share('footer_categories',$footer_categories);
            }
        }

        
      
        if ($current_url_route=='login' || $current_url_route=="signup/expert" || $current_url_route=="signup/client") 
        {
            if(Session::has('PROJECT_ID') && Session::has('PROJECT_ID') != "")
            {
                // do not perform any action.
            }
            else if ($user) 
            {
                if($user->inRole('client'))
                {
                    return redirect('/client/profile');
                }
                elseif($user->inRole('expert'))
                {
                    return redirect('/expert/profile');
                }   
                elseif($user->inRole('project_manager'))
                {   
                    return redirect('/project_manager/profile');
                }   
            }
        }

        /* data for header browse projects section */
        
        /* Sharing skills */
        $_arr_skills    = [];
        $tmp_obj_skills = SkillsModel::where('is_active',1)->get();

        if ($tmp_obj_skills!=FALSE ) 
        {
            $_arr_skills = $tmp_obj_skills->toArray();

            if (isset($_arr_skills) && sizeof($_arr_skills)>0 ) 
            {
                view()->share('_arr_skills',$_arr_skills);
            }
        }

        /* Sharing skills */
        $_arr_categories    = [];

        $tmp_obj_categories = CategoriesModel::where('is_active',1)->get();

        if ($tmp_obj_categories!=FALSE ) 
        {
            $_arr_categories = $tmp_obj_categories->toArray();

            if (isset($_arr_categories) && sizeof($_arr_categories)>0 ) 
            {
                view()->share('_arr_categories',$_arr_categories);
            }
        }

        return $next($request);
    }
}
