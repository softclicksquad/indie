<?php

namespace App\Http\Middleware\Front;

use Closure;
use Sentinel;

use App\Models\UserModel;
use App\Models\ClientsModel;
use App\Models\ExpertsModel;
use App\Models\ProjectManagerModel;
use App\Models\SubscriptionUsersModel;

use App\Common\Services\NotificationService;

class AuthenticateClientMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $arr_except = array();
        $current_url_route = app()->router->getCurrentRoute()->uri();


        if(!in_array($current_url_route, $arr_except))
        {
            
            $user = Sentinel::check();
            if($user)
            {
                if($user->inRole('client') && $user->is_active=="1")
                {
                    $client_details = ClientsModel::where('user_id',$user->id)->with('user_details')->with('review_details')->first(['id','user_id','first_name','last_name','profile_image']);

                    if ($client_details) 
                    {

                        $user_auth_details = array();
                        $client_details = $client_details->toArray();

                        $profile_image           = url('/public/uploads/front/profile/default_profile_image.png');
                        $profile_img_base_path   = base_path().'/public'.config('app.project.img_path.profile_image');
                        $profile_img_public_path = url('/public').config('app.project.img_path.profile_image');
                        
                        if( isset($client_details['profile_image']) && $client_details['profile_image']!='' && file_exists($profile_img_base_path.$client_details['profile_image'])){
                            $profile_image = $profile_img_public_path.$client_details['profile_image'];
                        }

                        $user_auth_details['user_id'] = $user->id;
                        $user_auth_details['is_login'] = TRUE;
                        $user_auth_details['first_name'] = isset($client_details['first_name'])?$client_details['first_name']:'';
                        $user_auth_details['last_name'] = isset($client_details['last_name'])?$client_details['last_name']:'';
                        $user_auth_details['profile_image'] = $profile_image;
                        $user_auth_details['is_available'] = isset($client_details['user_details']['is_available'])?$client_details['user_details']['is_available']:'';
                        $user_auth_details['review'] = isset($client_details['review_details'])?count($client_details['review_details']):'0';
                        $user_auth_details['kyc_verified']  = isset($client_details['user_details']['kyc_verified'])?$client_details['user_details']['kyc_verified']:'';
                        $user_auth_details['is_online']  = isset($client_details['user_details']['is_online'])?$client_details['user_details']['is_online']:'';

                        view()->share('profile_img_public_path',$profile_img_public_path);
                        view()->share('user_auth_details',$user_auth_details);

                        /* Notifications array */
                        $arr_notification   = [];
                        $obj_notification   = app(NotificationService::class);
                        $arr_notification   = $obj_notification->show_notifications();
                        view()->share('arr_notification',$arr_notification);  
                    }
                    return $next($request);    
                }

                else
                {
                    Sentinel::logout();
                    return redirect('/login');
                }    
            }
            else
            {
                return redirect('/login');
            }
            
        }
        else
        {
            return $next($request); 
        }
    }

}
