<?php

namespace App\Http\Middleware\Front;

use Closure;
use Sentinel;

use App\Models\RecruiterModel;

use App\Common\Services\NotificationService;

class AuthenticateRecruiterMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $arr_except = array();
        $current_url_route = app()->router->getCurrentRoute()->uri();
        if(!in_array($current_url_route, $arr_except))
        {
            $user = Sentinel::check();
            if($user)
            {
                if($user->inRole('recruiter'))
                {
                    
                    $project_manager_details = RecruiterModel::where('user_id',$user->id)->with('user_details')->first(['id','user_id','first_name','last_name','profile_image']);

                    if ($project_manager_details) 
                    {
                        $user_auth_details = array();
                        $project_manager_details = $project_manager_details->toArray();


                        $profile_image           = url('/public/uploads/front/profile/default_profile_image.png');
                        $profile_img_base_path   = base_path().'/public'.config('app.project.img_path.profile_image');
                        $profile_img_public_path = url('/public').config('app.project.img_path.profile_image');
                        
                        if( isset($project_manager_details['profile_image']) && $project_manager_details['profile_image']!='' && file_exists($profile_img_base_path.$project_manager_details['profile_image'])){
                            $profile_image = $profile_img_public_path.$project_manager_details['profile_image'];
                        }

                        $user_auth_details['user_id'] = $user->id;
                        $user_auth_details['is_login'] = TRUE;
                        $user_auth_details['first_name'] = isset($project_manager_details['first_name'])?$project_manager_details['first_name']:'';
                        $user_auth_details['last_name'] = isset($project_manager_details['last_name'])?$project_manager_details['last_name']:'';
                        $user_auth_details['profile_image'] = $profile_image;
                        $user_auth_details['is_available'] = isset($project_manager_details['user_details']['is_available'])?$project_manager_details['user_details']['is_available']:'';
                        $user_auth_details['is_online']  = isset($project_manager_details['user_details']['is_online'])?$project_manager_details['user_details']['is_online']:'';

                        view()->share('profile_img_public_path',$profile_img_public_path);
                        view()->share('user_auth_details',$user_auth_details);

                        /* Notifications array */
                        $arr_notification   = [];
                        $obj_notification   = app(NotificationService::class);
                        $arr_notification   = $obj_notification->show_notifications();
                        view()->share('arr_notification',$arr_notification);  
                    }
                    return $next($request);    
                }

                else
                {
                    Sentinel::logout();
                    return redirect('/login');
                }    
            }
            else
            {
                return redirect('/login');
            }
            
        }
        else
        {
            return $next($request); 
        }
    }

}
