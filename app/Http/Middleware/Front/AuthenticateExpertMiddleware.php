<?php

namespace App\Http\Middleware\Front;

use Closure;
use Sentinel;

use App\Models\UserModel;
use App\Models\ClientsModel;
use App\Models\ExpertsModel;
use App\Models\ProjectManagerModel;
use App\Models\SubscriptionUsersModel;
//use App\Common\Services\WalletService;


use App\Common\Services\NotificationService;


class AuthenticateExpertMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $arr_except = array();
        $current_url_route = app()->router->getCurrentRoute()->uri();

        if(!in_array($current_url_route, $arr_except))
        {
            $user = Sentinel::check();
            if($user)
            {
                if($user->inRole('expert') && $user->is_active=="1")
                {
                    $obj_subscription = FALSE;
                    $expert_details = ExpertsModel::where('user_id',$user->id)->with('user_details','expert_skills','expert_categories')->with('review_details')->first(['id','user_id','first_name','last_name','profile_image']);
                    $ex_skills = '';
                    $ex_category_arr = '';
                    if ($expert_details) 
                    {
                        $user_auth_details = array();
                        $expert_details = $expert_details->toArray();
                        foreach ($expert_details['expert_skills'] as $key => $skill) {
                          $ex_skills .= '&skills='.base64_encode($skill['skill_id']);
                        }
                        foreach ($expert_details['expert_categories'] as $key => $category) {
                          if($key == 0){
                          $ex_category_arr .= '&category='.base64_encode($category['category_id']);
                          }
                        }
                        
                        $profile_image           = url('/public/uploads/front/profile/default_profile_image.png');
                        $profile_img_base_path   = base_path().'/public'.config('app.project.img_path.profile_image');
                        $profile_img_public_path = url('/public').config('app.project.img_path.profile_image');
                        
                        if( isset($expert_details['profile_image']) && $expert_details['profile_image']!='' && file_exists($profile_img_base_path.$expert_details['profile_image'])){
                            $profile_image = $profile_img_public_path.$expert_details['profile_image'];
                        }
                        
                        $user_auth_details['user_id']      = $user->id;
                        $user_auth_details['is_login']      = TRUE;
                        $user_auth_details['first_name']    = isset($expert_details['first_name'])?$expert_details['first_name']:'';
                        $user_auth_details['last_name']     = isset($expert_details['last_name'])?$expert_details['last_name']:'';
                        $user_auth_details['profile_image'] = $profile_image;
                        $user_auth_details['is_available']  = isset($expert_details['user_details']['is_available'])?$expert_details['user_details']['is_available']:'';
                        $user_auth_details['review']        = isset($expert_details['review_details'])?count($expert_details['review_details']):'0';
                        $user_auth_details['kyc_verified']  = isset($expert_details['user_details']['kyc_verified'])?$expert_details['user_details']['kyc_verified']:'';
                        $user_auth_details['is_online']  = isset($expert_details['user_details']['is_online'])?$expert_details['user_details']['is_online']:'';
                        $user_auth_details['currency_code']  = isset($expert_details['user_details']['currency_code'])?$expert_details['user_details']['currency_code']:'';
                        $user_auth_details['currency_symbol']  = isset($expert_details['user_details']['currency_symbol'])?$expert_details['user_details']['currency_symbol']:'';

                        

                        view()->share('profile_img_public_path',$profile_img_public_path);
                        view()->share('profile_img_base_path',$profile_img_base_path);
                        view()->share('user_auth_details',$user_auth_details);
                        view()->share('ex_skills',$ex_skills);
                        view()->share('ex_category_arr',$ex_category_arr);

                    }

                    $subscription_except = array();
                    $subscription_except[] =  'expert/subscription';
                    $subscription_except[] =  'expert/subscription/payment/{enc_pack_id}';
                    if(!in_array($current_url_route, $subscription_except))
                    {
                        $obj_subscription = FALSE;
                       
                        $obj_subscription = SubscriptionUsersModel::where('user_id',$user->id)
                                                                    ->where('is_active','1')
                                                                    ->orderBy('id','DESC')
                                                                    ->limit(1)
                                                                    ->with(['transaction_details'])
                                                                    ->whereHas('transaction_details',function ($query)
                                                                    {
                                                                        $query->whereIN('payment_status',array('1','2'));
                                                                    })->first();

                          if($obj_subscription != FALSE)
                          {
                              $arr_subscription = $obj_subscription->toArray();
                              return $next($request);      
                          }
                          else
                          {
                            return redirect('/expert/subscription');
                          }
                    }
                     /* Notifications array */
                     $arr_notification   = [];
                     $obj_notification   = app(NotificationService::class);
                     $arr_notification   = $obj_notification->show_notifications();
                     view()->share('arr_notification',$arr_notification);  
                     return $next($request);    
                }
                else
                {
                    Sentinel::logout();
                    return redirect('/login');
                }    
            }
            else
            {
                return redirect('/login');
            }
            
        }
        else
        {
            return $next($request); 
        }
    }

}
