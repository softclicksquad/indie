<?php

namespace App\Http\Middleware\Front;

use Closure;
use Sentinel;
use Cache;
use Carbon\Carbon;
use App\Models\CurrencyModel;
class LastUserActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $obj_currency = CurrencyModel::select('id','currency_code','currency')
                                            ->where('is_active','1')
                                            ->get();
        if($obj_currency)
        {
          $arr_currency  = $obj_currency->toArray();
          view()->share('arr_currency',$arr_currency);
        }
        $obj_user = Sentinel::check();
        if($obj_user)
        {
            $enc_id   = isset($obj_user->id) ? $obj_user->id : 0;
            $expiredAt = Carbon::now()->addMinutes(1);
            Cache::put('user-is-online-'.$enc_id,true,$expiredAt);
        }
        return $next($request);
    }
}
