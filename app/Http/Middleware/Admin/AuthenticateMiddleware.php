<?php

namespace App\Http\Middleware\Admin;

use Closure;
use Sentinel;
use Session;

use App\Models\AdminProfileModel;
use App\Common\Services\NotificationService;

class AuthenticateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    { 
        $arr_except = array();
        $arr_protected_modules = array();

        $admin_path = config('app.project.admin_panel_slug');

        $arr_except[] =  $admin_path;
        $arr_except[] =  $admin_path.'/login';
        $arr_except[] =  $admin_path.'/process_login';
        $arr_except[] =  $admin_path.'/forgot_password';        
        $arr_except[] =  $admin_path.'/reset_password/{token}';
        $arr_except[] =  $admin_path.'/reset_password/{token}/{enc_id}';
        
        $current_url_route = app()->router->getCurrentRoute()->uri();


        //this modules will not shown to subadmins
        $arr_protected_modules[] = $admin_path.'/site_settings';
        $arr_protected_modules[] = $admin_path.'/subadmins';
        $arr_protected_modules[] = $admin_path.'/payment_settings';
        $arr_protected_modules[] = $admin_path.'/subscription_packs';
        $arr_protected_modules[] = $admin_path.'/project_milestones';
        $arr_protected_modules[] = $admin_path.'/project_milestones/all/{enc_id}';
        $arr_protected_modules[] = $admin_path.'/project_milestones/release/{enc_id}';
        
        if(!in_array($current_url_route, $arr_except))
        {
            $user = Sentinel::check();
            if($user)
            {
                //added by sagar sainkar for share admin profile image and name to header
                $admin_img_path = url('/').'/uploads/admin/profile/';
                $arr_profile = AdminProfileModel::where('user_id',$user->id)->first();
                if($arr_profile)
                {
                    $arr_profile = $arr_profile->toArray(); 
                    if (isset($arr_profile['image'])) 
                    {
                        view()->share('admin_profile_img',$admin_img_path.$arr_profile['image']);
                    }
                    if (isset($arr_profile['name']) && $arr_profile['name']!="") 
                    {
                        view()->share('admin_name',$arr_profile['name']);    
                    }
                }

                if($user->inRole('admin'))
                {
                    /* Notifications array */
                    $arr_notification   = [];
                    $obj_notification   = app(NotificationService::class);
                    $arr_notification   = $obj_notification->show_notifications();
                    view()->share('arr_notification',$arr_notification);     
                    return $next($request);    
                }
                elseif($user->inRole('subadmin'))
                {
                    if (!in_array($current_url_route, $arr_protected_modules)) 
                    {
                        /* Notifications array */
                        $arr_notification   = [];
                        $obj_notification   = app(NotificationService::class);
                        $arr_notification   = $obj_notification->show_notifications();
                        view()->share('arr_notification',$arr_notification);     

                        return $next($request);    
                    }
                    else
                    {
                        Sentinel::logout();
                        Session::flash('error','You don\'t have sufficient privileges.');
                        return redirect($admin_path.'/login');
                    }

                }
                else
                {
                    Sentinel::logout();
                    Session::flash('error','You don\'t have sufficient privileges.');
                    return redirect($admin_path.'/login');
                }    
            }
            else
            {
                Sentinel::logout();
                return redirect($admin_path.'/login');
            }
            
        }
        else
        {
            return $next($request); 
        }
    }


    public function detectLocale(\Illuminate\Routing\Route $route)
    {
        // gets the locale from parameter
        $locale = $route->getParameter('token');
        // set the current request app locale
        app()->setLocale($locale);
        // remove the locale parameter from the current route
        $route->forgetParameter('token');
    }
}
