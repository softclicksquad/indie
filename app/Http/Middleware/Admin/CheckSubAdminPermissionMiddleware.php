<?php

namespace App\Http\Middleware\Admin;

use Closure;
use Sentinel;
use Session;

class CheckSubAdminPermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Sentinel::check();
        $user_role = get_user_role($user->id);
        $admin_path = config('app.project.admin_panel_slug');

        if($user_role == 'subadmin')
        {
            $url = \Request::segment(2);
            $url1 = \Request::segment(3);
            $user_permissions = isset($user['permissions']) ? $user['permissions'] : '';

            // This block is only for read notication
            if($url == 'projects' && $url1 == 'notification'){
                return $next($request);
            }

            if($url == 'countries' || $url == 'states' || $url == 'cities')
            {
                if(isset($user_permissions) && count($user_permissions)>0 && array_key_exists('locations',$user_permissions))
                {
                    return $next($request);
                }
                else
                {
                    Sentinel::logout();
                    Session::flash('error', trans('controller_translations.text_you_do_not_have_sufficient_privileges'));
                    return redirect($admin_path.'/login');
                }
            }
            else if(isset($url) && isset($url1) && $url == 'projects' && ($url1 == 'posted' || $url1 == 'posted_recruiter'))
            {
                if(isset($user_permissions) && count($user_permissions)>0 && (array_key_exists('project_manager',$user_permissions) || (array_key_exists('recruiter',$user_permissions))))
                {
                    return $next($request);
                }
                else
                {
                    Sentinel::logout();
                    Session::flash('error', trans('controller_translations.text_you_do_not_have_sufficient_privileges'));
                    return redirect($admin_path.'/login');
                }
            }
            else if(isset($user_permissions) && count($user_permissions)>0 && array_key_exists($url,$user_permissions))
            {
                return $next($request);
            }
            else
            {
                Sentinel::logout();
                Session::flash('error', trans('controller_translations.text_you_do_not_have_sufficient_privileges'));
                return redirect($admin_path.'/login');
            }
        }
        else if($user_role == 'admin')
        {
            return $next($request);
        }
        else
        {
            Sentinel::logout();
            Session::flash('error', trans('controller_translations.text_you_do_not_have_sufficient_privileges'));
            return redirect($admin_path.'/login');
        }
    }
}
