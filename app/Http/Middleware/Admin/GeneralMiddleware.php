<?php

namespace App\Http\Middleware\Admin;

use Closure;
use App\Models\AdminProfileModel;
use App\Models\SiteSettingModel;
use Sentinel;

class GeneralMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        view()->share('admin_panel_slug',config('app.project.admin_panel_slug'));

        $admin_img_path = url('/').'/uploads/admin/profile/';

        $site_setting = SiteSettingModel::where('site_settting_id',1)->first();
        if ($site_setting) 
        {
            $site_setting = $site_setting->toArray();
            if (isset($site_setting['site_email_address']) && $site_setting['site_email_address']!="") 
            {
                view()->share('website_contact_email',$site_setting['site_email_address']);    
                view()->share('mail_form','no-reply@archexperts.com');
            }
        }

        $user = Sentinel::check();
        $current_url_route = app()->router->getCurrentRoute()->uri();

        //dd($current_url_route);
        $login_url = config('app.project.admin_panel_slug').'/login';
        if ($current_url_route==$login_url) 
        {
            if ($user) 
            {
                if($user->inRole('admin') || $user->inRole('subadmin'))
                {
                    return redirect(config('app.project.admin_panel_slug').'/dashboard');
                }
            }
        }
        return $next($request);
    }
}
