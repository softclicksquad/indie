<?php
	Route::any('expire_contest', function () {
		//dd('dev');
		\Artisan::call('expire:contest');
		dd("Cron Successfully Run");
		//return redirect()->back();
	});

	Route::any('choose_winner', function () {
		//dd('dev');
		\Artisan::call('winner:choose');
		dd("Cron Successfully Run");
		//return redirect()->back();
	});
    
    Route::any('/add_new_card',['as'=>'front_client_wallet_create_card','uses'=>'Front\Client\WalletDashboardController@create_card']);
    Route::any('/client/wallet/create_card_return',['as'=>'front_client_wallet_create_card','uses'=>'Front\Client\WalletDashboardController@create_card_return']);

    /* fronts expert routes group starts */
	Route::group(array('prefix' => '/expert','middleware' =>'front_expert'), function()
	{

		Route::group(array('prefix' => '/twilio-chat','middleware' =>'front_expert'), function() {
			Route::get('/chat_list' ,['as'=>'front_projectbid_twilio-chat' ,'uses'=>'Common\TwilioChatController@show_chat_list']);
			Route::get('/contest/{enc_id}' ,['as'=>'front_contest_twilio-chat' ,'uses'=>'Common\TwilioChatController@contest_chat']);
			Route::get('/projectbid/{enc_id}' ,['as'=>'front_projectbid_twilio-chat' ,'uses'=>'Common\TwilioChatController@project_bid_chat']);
			Route::get('/refresh_token' ,['as'=>'front_refresh_token_twilio-chat' ,'uses'=>'Common\TwilioChatController@refresh_token']);
			Route::get('/update_channel' ,['as'=>'front_update_channel_twilio-chat' ,'uses'=>'Common\TwilioChatController@update_channel']);
			Route::get('/video' ,['as'=>'front_video_twilio-chat' ,'uses'=>'Common\TwilioChatController@video_call']);
		});
		
		Route::get('profile/{enc_id?}/{cat_id?}',['as'=>'front_expert_profile','uses'=>'Front\Expert\ProfileController@index']);
		Route::post('profile/update',['as'=>'front_expert_profile','uses'=>'Front\Expert\ProfileController@update']);
		Route::get('dashboard',['as'=>'front_expert_dashboard','uses'=>'Front\Expert\DashboardController@index']);


		Route::get('get_subcategory/{enc_id?}',['as'=>'front_expert_subcategory','uses'=>'Front\Expert\ProfileController@get_subcategory']);

		Route::any('edit_subcategory/{enc_id?}/{id?}',['as'=>'front_expert_subcategory','uses'=>'Front\Expert\ProfileController@edit_subcategory']);
		

		Route::post('save_category',['as'=>'front_save_category','uses'=>'Front\Expert\ProfileController@save_category']);

		Route::any('update_category/{id}',['as'=>'front_update_category','uses'=>'Front\Expert\ProfileController@update_category']);

		Route::any('delete_category/{id}',['as'=>'front_delete_category','uses'=>'Front\Expert\ProfileController@delete_category']);

		/*Route::any('edit_category/{enc_id}',['as'=>'front_edit_category','uses'=>'Front\Expert\ProfileController@edit_category']);*/

		/*Delete Image From database and folder */
		Route::get('delete_image',['as'=>'front_expert_profile_delete_image','uses'=>'Front\Expert\ProfileController@delete_image']);
		Route::get('delete_attachment',['as'=>'front_expert_delete_attachment','uses'=>'Front\Expert\ProjectController@delete_attachment']);

		//change password
		Route::get('change_password',['as'=>'front_expert_change_password','uses'=>'Front\Expert\ProfileController@change_password']);	
		Route::post('update_password',['as'=>'front_expert_update_password','uses'=>'Front\Expert\ProfileController@update_password']);

		/* Dashboard */		
		Route::get('dashboard',			['as'=>'front_project_manager_dashboard','uses'=>'Front\Expert\DashboardController@show_dashboard']);

		//Portfolio
		Route::get('portfolio',['as'=>'front_expert_portfolio','uses'=>'Front\Expert\PortfolioController@index']);	
		Route::post('update_portfolio',['as'=>'front_expert_update_portfolio','uses'=>'Front\Expert\PortfolioController@update_portfolio']);
        Route::get('delete/{enc_id}',['as'=>'front_expert_delete_portfolio','uses'=>'Front\Expert\PortfolioController@delete']);	

       //Availability
		Route::get('availability',['as'=>'front_expert_availability','uses'=>'Front\Expert\ProfileController@availability']);


		Route::post('update_availability',['as'=>'front_expert_update_availability','uses'=>'Front\Expert\ProfileController@update_availability']);

		/* Change profile picture */
 		Route::any('store_profile_image',['as'=>'store_profile_image','uses'=>'Front\Expert\ProfileController@store_profile_image']);

 		/* Add project attchments */
 		Route::post('add_project_attachment', ['as'=>'_add_project_attachment','uses'=>'Front\Expert\ProjectController@add_project_attachment']);

 		/* DElete project attchments */
 		Route::get('delete_project_attachment', ['as'=>'_delete_project_attachment','uses'=>'Front\Expert\ProjectController@delete_project_attachment']);

		Route::group(array('prefix' => '/subscription','middleware' =>'front_expert'), function()
		{
			Route::get('/',['as'=>'front_expert_subscription','uses'=>'Front\Expert\SubscriptionExpertController@index']);
			Route::get('payment/{enc_pack_id}/{enc_pre_pack_id}/{enc_currency}',['as'=>'front_expert_subscription_payment','uses'=>'Front\Expert\SubscriptionExpertController@payment_methods']);

			Route::any('payment/return/return',['as'=>'front_expert_subscription_payment','uses'=>'Front\Expert\SubscriptionExpertController@payment_methods_return']);



			Route::get('payment/downgrade/{enc_pack_id}/{enc_pre_pack_id}/{enc_currency}',['as'=>'downgrade_plan','uses'=>'Front\Expert\SubscriptionExpertController@downgrade_plan']);

			Route::get('payment/downgrademonthly/{enc_pack_id}/{enc_pre_pack_id}/{enc_currency}',['as'=>'downgrade_plan','uses'=>'Front\Expert\SubscriptionExpertController@downgrademonthly_plan']);


		});

		Route::group(array('prefix' => '/transactions','middleware' =>'front_expert'), function()
		{
			Route::get('/',['as'=>'front_expert_transactions','uses'=>'Front\Expert\TransactionsController@index']);
			Route::get('show/{enc_id}',['as'=>'front_expert_transactions','uses'=>'Front\Expert\TransactionsController@show']);
			Route::any('show_cnt',['as'=>'show_cnt','uses'=>'Front\Expert\TransactionsController@show_cnt']);

		});

		Route::group(array('prefix' => '/sealedentrieshistory','middleware' =>'front_expert'), function()
		{
			Route::get('/',['as'=>'front_expert_sealedentrieshistory','uses'=>'Front\Expert\SealedEntriesHistoryController@index']);
			Route::post('/payment',['as'=>'front_expert_sealedentrieshistory','uses'=>'Front\Expert\SealedEntriesHistoryController@payment']);
			Route::get('/payment_response',['as'=>'front_expert_sealedentrieshistory','uses'=>'Front\Expert\SealedEntriesHistoryController@payment_response']);
			Route::get('/convert_rates',['as'=>'front_expert_sealedentrieshistory','uses'=>'Front\Expert\SealedEntriesHistoryController@convert_rates']);
			Route::get('show/{enc_id}',['as'=>'front_expert_sealedentrieshistory','uses'=>'Front\Expert\SealedEntriesHistoryController@show']);
		});
		
	    Route::group(array('prefix' => '/subscriptionhistory','middleware' =>'front_expert'), function()
		{
			Route::get('/',['as'=>'front_expert_subscription_history','uses'=>'Front\Expert\SubscriptionHistoryController@index']);
			Route::get('show/{enc_id}',['as'=>'front_expert_subscription_history','uses'=>'Front\Expert\SubscriptionHistoryController@show']);

		});

		/* dispute ends */
		Route::group(array('prefix' => '/review','middleware' =>'front_expert'), function()
		{
			Route::post('/add',['as'=>'front_expert_review','uses'=>'Front\Expert\ReviewController@add']);
		 });

		Route::get('reviews/{enc_id}',['as'=>'show_review_page','uses'=>'Front\Expert\ReviewController@show_review_page']);
		/* ends */

		/* Dispute Start */
		Route::get('projects/dispute',	['as'=>'show_projects',	'uses'=>'Front\Expert\DisputeController@show_projects']);
		Route::get('dispute/{enc_id}',	['as'=>'show_dispute',	'uses'=>'Front\Expert\DisputeController@show_dispute']);
		Route::post('add_dispute',    	['as'=>'add_dispute',	'uses'=>'Front\Expert\DisputeController@add']);
		/* Dispute Ends */	

		
		/* Show expert inbox */
		Route::get('inbox',				['as'=>'_show_inbox',	'uses'=>'Front\MessagingController@show_inbox']);

		/* project completion request */
		Route::get('request_to_complete/{enc_id}',				['as'=>'send_project_completion_request_to_client',	'uses'=>'Front\Expert\ProjectController@request_to_complete']);		




		/* Project details of Expert */
		Route::group(array('prefix' => '/projects','middleware' =>'front_expert'), function()
		{
			Route::get('ongoing',				['as'=>'ongoing_projects',		'uses'=>'Front\Expert\ProjectController@show_ongoing_projects']);
			Route::get('applied',				['as'=>'applied_projects',		'uses'=>'Front\Expert\ProjectController@show_applied_projects']);
			Route::get('awarded',				['as'=>'awarded_projects',		'uses'=>'Front\Expert\ProjectController@show_awarded_projects']);
			Route::get('completed',				['as'=>'completed_projects',	'uses'=>'Front\Expert\ProjectController@show_completed_projects']);
            Route::get('canceled',				['as'=>'canceled_projects',	    'uses'=>'Front\Expert\ProjectController@show_canceled_projects']);
            Route::get('favourites',			['as'=>'show_favourite_projects',	  'uses'=>'Front\Expert\ProjectController@show_favourite_projects']);

            /* for invitations */
		    Route::any('invitations',	['as'=>'invitations',	 'uses'=>'Front\Expert\InvitationsController@index']);
		    Route::any('invitations/delete/{enc_id}',	['as'=>'invitations',	 'uses'=>'Front\Expert\InvitationsController@delete']);
		    Route::get('invitations/show/{enc_id}',['as'=>'front_expert_invitations','uses'=>'Front\Expert\InvitationsController@show']);

			Route::get('awarded/accept/{enc_id}', ['as'=>'accept_awarded_project',	'uses'=>'Front\Expert\ProjectController@accept_awarded_project']);
				
			/* reject project*/
			/*Route::post('awarded/reject/{enc_id}', ['as'=>'reject_awarded_project',	'uses'=>'Front\Expert\ProjectController@reject_awarded_project']);*/
			Route::post('awarded/reject', ['as'=>'reject_awarded_project',	'uses'=>'Front\Expert\ProjectController@reject_awarded_project']);

			Route::get('details/{enc_id}',      ['as'=>'show_project_details',	'uses'=>'Front\Expert\ProjectController@show_project_details']);	
			Route::get('bid/update/{enc_id}',   ['as'=>'update_project_bid',	'uses'=>'Front\Expert\ProjectController@show_update_project_bid']);
			Route::post('update_bid_details',	['as'=>'update_bid_details',	'uses'=>'Front\Expert\ProjectController@update_bid_details']);

			/*Notification */
			Route::get('notification/{enc_id}',	['as'=>'notification_seen',	'uses'=>'Front\Expert\ProjectController@notifications_seen']);
			
			/* Milestones */
			Route::get('milestones/{enc_id}',	['as'=>'show_project_milestones',	'uses'=>'Front\Expert\MilestonesController@show_project_milestones']);
			Route::get('milestones/release/{enc_id}/{currency_code}',	['as'=>'request_to_release_milestones',	'uses'=>'Front\Expert\MilestonesController@request_to_release_milestones']);
			/*Generate Invoice*/
			Route::get('invoice/{enc_id}',	['as'=>'show_invoice',	'uses'=>'Front\Expert\MilestonesController@show_invoice']);
			Route::post('create_invoice/{enc_id}',	['as'=>'create_invoice',	'uses'=>'Front\Expert\MilestonesController@create_invoice']);
			/*Route::get('generate_invoice/{enc_id}',	['as'=>'generate_invoice',	'uses'=>'Front\Expert\MilestonesController@generate_invoice']);*/

		    Route::group(array('prefix' => '/collaboration','middleware' =>'front_expert'), function(){
	     		Route::any('/{enc_id}'         ,['as'=>'front_expert_collaboration'              ,'uses'=>'Front\Expert\ProjectCollaborationController@index']);
	     		Route::any('/details/{enc_id}/{enc_id1}'  ,['as'=>'front_expert_collaboration_details'      ,'uses'=>'Front\Expert\ProjectCollaborationController@details']);
	     		Route::any('/upload/files'      ,['as'=>'front_expert_collaboration_upload_files' ,'uses'=>'Front\Expert\ProjectCollaborationController@upload_collaboration_files']);
	     	});
		});
		/* Bid add Section of Expert */
		Route::group(array('prefix' => '/bids','middleware' =>'front_expert'), function(){
			Route::get('add/{enc_id}',			['as'=>'add_project_bid','uses'=>'Front\Expert\ProjectBidsController@show_add_project_bid']);
			Route::post('store_bid_details',	['as'=>'store_bid_details','uses'=>'Front\Expert\ProjectBidsController@store_bid_details']);
			// for purchase extra bid if user get over limit of bid of his current subscription
			Route::get('purchase',['as'=>'purchase_project_extra_bid','uses'=>'Front\Expert\ProjectBidsController@purchase_bid']);

			Route::any('purchase/return',['as'=>'purchase_project_extra_bid','uses'=>'Front\Expert\ProjectBidsController@purchase_bid_return']);

		});
		Route::group(array('prefix' => '/settings','middleware' =>'front_expert'), function(){
			Route::get('payment',['as'=>'front_expert_show_payment_Settings','uses'=>'Front\Expert\PaymentSettingsController@index']);	
			Route::post('payment/update',['as'=>'front_expert_update_payment_settings','uses'=>'Front\Expert\PaymentSettingsController@update']);
		});
		
		/*Generate Invoice Section of Expert*/
		/*Route::group(array('prefix' => '/invoice','middleware' =>'front'), function(){
			Route::get('/',['as'=>'','uses'=>'Front\Expert\InvoiceController@index']);
		});*/
		Route::group(array('prefix' => '/contest','middleware' =>'front_expert'), function(){
			Route::any('send_entry' , ['as'=>'front_expert_send_enrty',  'uses'=>'Front\Expert\ContestController@send_entry']);			
			Route::any('update_entry',['as'=>'front_expert_update_entry','uses'=>'Front\Expert\ContestController@update_entry']);
			Route::any('applied-contest'  ,['as'                     => 'front_expert_send_enrty'   ,'uses'     => 'Front\Expert\ContestController@applied_contest']);
			Route::get('details/{enc_id}',	['as'                     => 'front_client_contest_edit',	'uses'     => 'Front\Expert\ContestController@details']);
			Route::get('/show_contest_entry_details/{enc_id}',	['as' => 'show_contest_details','uses'           => 'Front\Expert\ContestController@show_contest_entry_details']);
			Route::post('store-comment',	['as'                       => 'store-comment',	'uses'                 => 'Front\Expert\ContestController@store_contest_entry_file_comment']);
			Route::any('get-comments',	['as'                         => 'get-comments',	'uses'                  => 'Front\Expert\ContestController@contest_entry_file_get_comments']);
			Route::any('upload-contest-original-files',	['as'        => 'upload-contest-original-files',	'uses' => 'Front\Expert\ContestController@contest_entry_original_files_upload']);
		}); 
        // Mangopay
        Route::group(array('prefix' => '/wallet','middleware' =>'front_expert'), function(){
            Route::get('/dashboard',['as'=>'front_expert_wallet_dashboard','uses'=>'Front\Expert\WalletDashboardController@show_dashboard']);
            Route::get('/withdraw',['as'=>'withdraw','uses'=>'Front\Expert\WalletDashboardController@withdraw']);
            Route::get('/transactions',['as'=>'withdraw','uses'=>'Front\Expert\WalletDashboardController@transactions']);
            Route::any('/add_money',['as'=>'front_expert_wallet_add_money','uses'=>'Front\Expert\WalletDashboardController@add_money_wallet']);
            Route::any('/create_user',['as'=>'front_expert_wallet_create_user','uses'=>'Front\Expert\WalletDashboardController@create_user']);
            
            Route::any('/create_card',['as'=>'front_client_wallet_create_card','uses'=>'Front\Expert\WalletDashboardController@create_card']);
            Route::any('/add_new_card',['as'=>'front_client_wallet_create_card','uses'=>'Front\Expert\WalletDashboardController@create_card']);

            
            Route::any('/finish_card_registration',['as'=>'front_expert_wallet_finish_card_registration','uses'=>'Front\Expert\WalletDashboardController@finish_card_registration']);
            Route::any('/card_pre_authorization',['as'=>'front_expert_wallet_card_pre_authorization','uses'=>'Front\Expert\WalletDashboardController@card_pre_authorization']);
            Route::any('/pre_authorization_payin',['as'=>'front_expert_wallet_pre_authorization_pay_in','uses'=>'Front\Expert\WalletDashboardController@pre_authorization_payin']);
            Route::any('/get_preauthorization_object',['as'=>'front_expert_wallet_get_preauthorization_object','uses'=>'Front\Expert\WalletDashboardController@get_preauthorization_object']);
            Route::any('/create_wallet_using_mp_owner_id/{mp_owner_id}',['as'=>'front_expert_wallet_get_preauthorization_object','uses'=>'Front\Expert\WalletDashboardController@create_wallet_using_mp_owner_id']);
            Route::any('/add_bank',['as'=>'front_expert_wallet_add_bank','uses'=>'Front\Expert\WalletDashboardController@add_bank']);

            Route::any('/cashout_request/{bank_id}',['as'=>'front_client_wallet_cashout_in_bank','uses'=>'Front\Expert\WalletDashboardController@cashout_request']);
            
            Route::any('/cashout_in_bank',['as'=>'front_expert_wallet_cashout_in_bank','uses'=>'Front\Expert\WalletDashboardController@make_cashout_in_bank']);
            Route::any('/upload_kyc_docs',['as'=>'front_expert_wallet_upload_kyc_docs','uses'=>'Front\Expert\WalletDashboardController@upload_kyc_docs']);
            Route::any('/get_note',['as'=>'get_note','uses'=>'Front\Expert\WalletDashboardController@get_note']);
            Route::any('/create_ubo',['as'=>'create_ubo','uses'=>'Front\Expert\WalletDashboardController@create_ubo']);
            Route::any('/add_UBO',['as'=>'add_UBO','uses'=>'Front\Expert\WalletDashboardController@add_UBO']);
            Route::any('/submit_declaration',['as'=>'submit_declaration','uses'=>'Front\Expert\WalletDashboardController@submit_declaration']);
            Route::any('/update_user_details',['as'=>'front_client_update_user_details','uses'=>'Front\Expert\WalletDashboardController@update_user_details']);
        });

        Route::group(array('prefix' => '/tickets','middleware' =>'front_expert'), function()
        {
			Route::get('/',						['uses'=>'Front\Expert\SupportController@index']);
			Route::get('/details/{id}',			['uses'=>'Front\Expert\SupportController@details']);
			Route::get('/create',				['uses'=>'Front\Expert\SupportController@create']);
			Route::post('/store',				['uses'=>'Front\Expert\SupportController@store']);
			Route::post('/submit_reply/{id}',	['uses'=>'Front\Expert\SupportController@submit_reply']);
			Route::get('/reopen_ticket/{id}',	['uses'=>'Front\Expert\SupportController@reopen_ticket']);
			Route::get('/resolve_ticket/{id}',	['uses'=>'Front\Expert\SupportController@resolve_ticket']);
	 	});

	 	Route::group(array('prefix' => '/notifications','middleware' =>'front_expert'), function()
        {
			Route::get('/',['uses'=>'Front\Expert\NotificationController@index']);
	 	});

	});

    /* fronts expert routes group starts */
	Route::group(array('prefix' => '/experts','middleware' =>'front'), function(){
		Route::get('/',['as'=>'front_expert_listing','uses'=>'Front\ExpertListingController@index']);
		Route::get('/portfolio/{enc_id}',['as'=>'front_expert_portfolio','uses'=>'Front\ExpertListingController@view']);
	    Route::get('/review/{enc_id}',['as'=>'front_expert_portfolio','uses'=>'Front\ExpertListingController@viewreview']);
	    Route::get('get_subcategory/{enc_id?}',['as'=>'front_expert_subcategory','uses'=>'Front\ExpertListingController@get_subcategory']);

	    Route::any('show_cnt',['as'=>'show_cnt','uses'=>'Front\ExpertListingController@show_cnt']);
		/* Searching*/
		Route::any('/search/all/result',	['as'=>'search_expert_details','uses'=>'Front\ExpertListingController@search_expert']);
		/* Skill searching on expert side by clicking skill on expert portfolio page .*/
		Route::any('/search_skill/{enc_id}',	['as'=>'search_expert_details_by_skill','uses'=>'Front\ExpertListingController@search_expert_by_skill']);			
		/* expert searching on expert side by clicking country on expert portfolio page .*/
		Route::any('/search_country/{enc_id}',	['as'=>'search_expert_by_country','uses'=>'Front\ExpertListingController@search_expert_by_country']);
		/* for automplete */
		Route::any('/autocomplete',	['as'=>'get_autocomplete_data','uses'=>'Front\ExpertListingController@get_autocomplete_data']);		
        /* get_projects */
		Route::any('get_projects',['as'=>'front_expert_get_projects','uses'=>'Front\ExpertListingController@get_projects']);
	 	Route::post('subcatdata',['as'=>'front_client_project_subcatdata','uses'=>'Front\ExpertListingController@subcatdata']);
	});

	
?>