<?php
$route_slug = 'admin';
/* Site Setting Route Start */
Route::group(array('middleware'=>'check_permission'), function () use($route_slug)
{
	Route::group(array('prefix' => '/twilio-chat'), function() {
			Route::get('/chat_list' ,['as'=>'projectbid_twilio-chat' ,'uses'=>'Admin\TwilioChatController@show_chat_list']);
			Route::get('/projectbid/{enc_id}' ,['as'=>'projectbid_twilio-chat' ,'uses'=>'Admin\TwilioChatController@project_bid_chat']);
			Route::get('/contest/{enc_id}' ,['as'=>'contest_twilio-chat' ,'uses'=>'Admin\TwilioChatController@contest_chat']);
			Route::get('/refresh_token' ,['as'=>'refresh_token_twilio-chat' ,'uses'=>'Admin\TwilioChatController@refresh_token']);
			Route::get('/update_channel' ,['as'=>'update_channel_twilio-chat' ,'uses'=>'Admin\TwilioChatController@update_channel']);
			Route::get('/request_admin' ,['as'=>'request_admin_twilio-chat' ,'uses'=>'Admin\TwilioChatController@request_admin']);
		});

	Route::group(array('prefix' => '/profile'), function()
	{
		Route::get('/', ['as' => 'site_settings', 'uses' => 'Admin\ProfileController@index']);
		Route::post('/update', ['as' => 'site_settings', 'uses' => 'Admin\ProfileController@update']);
	});

	/* Site Setting Route Start */
	Route::group(array('prefix' => '/site_settings'), function()
	{
		Route::get('/', ['as' => 'site_settings', 'uses' => 'Admin\SiteSettingController@index']);
		Route::post('/update/{enc_id}', ['as' => 'site_settings', 'uses' => 'Admin\SiteSettingController@update']);
	});
 
	/* Admin CMS Routes Starts */
   	Route::group(array('prefix' => '/static_pages'), function()
	{
		Route::get('/',['as' => 'static_pages_manage' ,'uses' => 'Admin\StaticPageController@index']);
		Route::get('create',['as' => 'static_pages_create' ,'uses' => 'Admin\StaticPageController@create']);
		Route::get('edit/{enc_id}',['as' => 'static_pages_edit' ,'uses' => 'Admin\StaticPageController@edit']);
		Route::any('store',['as' => 'static_pages_store' ,'uses' => 'Admin\StaticPageController@store']);
		Route::post('update/{enc_id}',['as' => 'static_pages_update' ,'uses' => 'Admin\StaticPageController@update']);
		Route::get('delete/{enc_id}',['as'=>'static_pages_delete','uses'=>'Admin\StaticPageController@delete']);	
		Route::get('activate/{enc_id}',['as'=>'static_pages_activate','uses'=>'Admin\StaticPageController@activate']);	
		Route::get('deactivate/{enc_id}',['as'=>'static_pages_deactivate','uses'=>'Admin\StaticPageController@deactivate']);	
		Route::post('multi_action',['as'=>'static_pages_multi_action','uses'=>'Admin\StaticPageController@multi_action']);	

	});

   	/* Email template route */
	Route::group(array('prefix' => '/email_template'), function()
	{
		Route::get('/', ['as' => 'email', 'uses' => 'Admin\EmailTemplateController@index']);
		Route::get('/load', ['as' => 'search_user', 'uses' => 'Admin\EmailTemplateController@load_data']);
		Route::post('/store', ['as' => 'send_email', 'uses' => 'Admin\EmailTemplateController@store']);
		Route::get('/edit/{id}', ['as' => 'send_email_listing', 'uses' => 'Admin\EmailTemplateController@edit']);
		Route::any('/update/{id}', ['as' => 'send_details', 'uses' => 'Admin\EmailTemplateController@update']);
		Route::any('/preview', ['as' => 'send_details', 'uses' => 'Admin\EmailTemplateController@preview']);
	});

	/* Admin Service Categories Routes Starts */
    /* Route::group(array('prefix' => '/service_categories'), function()
	{
		Route::get('/',['as' => 'service_categories_manage' ,'uses' => 'Admin\ServiceCategoriesController@index']);
		Route::get('create',['as' => 'service_categories_create' ,'uses' => 'Admin\ServiceCategoriesController@create']);
		Route::get('edit/{enc_id}',['as' => 'service_categories_edit' ,'uses' => 'Admin\ServiceCategoriesController@edit']);
		Route::any('store',['as' => 'service_categories_store' ,'uses' => 'Admin\ServiceCategoriesController@store']);
		Route::post('update/{enc_id}',['as' => 'service_categories_update' ,'uses' => 'Admin\ServiceCategoriesController@update']);
		Route::get('delete/{enc_id}',['as'=>'service_categories_delete','uses'=>'Admin\ServiceCategoriesController@delete']);	
		Route::get('activate/{enc_id}',['as'=>'service_categories_activate','uses'=>'Admin\ServiceCategoriesController@activate']);	
		Route::get('deactivate/{enc_id}',['as'=>'service_categories_deactivate','uses'=>'Admin\ServiceCategoriesController@deactivate']);	
		Route::post('multi_action',['as'=>'service_categories_multi_action','uses'=>'Admin\ServiceCategoriesController@multi_action']);	

	});*/
 
    Route::group(array('prefix' => 'faq'), function () use($route_slug)
	{
		$module_controller = "Admin\FAQController@";
		Route::get('/',				['as' =>$route_slug.'faq', 'uses' => $module_controller.'index']);
		Route::get('/create',		['as' =>$route_slug.'faq', 'uses' => $module_controller.'create']);
		Route::post('/store',		['as' =>$route_slug.'faq', 'uses' => $module_controller.'store']);
		Route::get('/edit/{id?}',	['as' =>$route_slug.'faq', 'uses' => $module_controller.'edit']);
		Route::post('/update/{id?}',['as' =>$route_slug.'faq', 'uses' => $module_controller.'update']);
		Route::get('/delete/{id?}',	['as' =>$route_slug.'faq', 'uses' => $module_controller.'delete']);
		Route::get('/deactivate/{id?}',	['as' =>$route_slug.'faq', 'uses' => $module_controller.'deactivate']);
		Route::get('/activate/{id?}',['as' =>$route_slug.'faq', 'uses' => $module_controller.'activate']);
		Route::post('/multi_action',['as' =>$route_slug.'faq', 'uses' => $module_controller.'multi_action']);
	});


	/* Admin Country section of location module*/
   	Route::group(array('prefix' => '/countries'), function()
	{
		Route::get('/',['as' => 'countries_manage' ,'uses' => 'Admin\CountryController@index']);
		Route::get('create',['as' => 'countries_create' ,'uses' => 'Admin\CountryController@create']);
		Route::get('edit/{enc_id}',['as' => 'countries_edit' ,'uses' => 'Admin\CountryController@edit']);
		Route::any('store',['as' => 'countries_store' ,'uses' => 'Admin\CountryController@store']);
		Route::post('update/{enc_id}',['as' => 'countries_update' ,'uses' => 'Admin\CountryController@update']);
		/*Route::get('delete/{enc_id}',['as'=>'countries_delete','uses'=>'Admin\CountryController@delete']);	*/
		Route::get('activate/{enc_id}',['as'=>'countries_activate','uses'=>'Admin\CountryController@activate']);	
		Route::get('deactivate/{enc_id}',['as'=>'countries_deactivate','uses'=>'Admin\CountryController@deactivate']);	
		Route::post('multi_action',['as'=>'countries_multi_action','uses'=>'Admin\CountryController@multi_action']);
		
		Route::get('block/{enc_id}',['as'=>'countries_block','uses'=>'Admin\CountryController@block']);	
		Route::get('un_block/{enc_id}',['as'=>'countries_un_block','uses'=>'Admin\CountryController@un_block']);		

	});	

	/* Admin states section of location module*/
   	Route::group(array('prefix' => '/states'), function()
	{
		Route::get('/',['as' => 'states_manage' ,'uses' => 'Admin\StateController@index']);
		Route::get('create',['as' => 'states_create' ,'uses' => 'Admin\StateController@create']);
		Route::get('edit/{enc_id}',['as' => 'states_edit' ,'uses' => 'Admin\StateController@edit']);
		Route::any('store',['as' => 'states_store' ,'uses' => 'Admin\StateController@store']);
		Route::post('update/{enc_id}',['as' => 'states_update' ,'uses' => 'Admin\StateController@update']);
		/*Route::get('delete/{enc_id}',['as'=>'states_delete','uses'=>'Admin\StateController@delete']);	*/
		Route::get('activate/{enc_id}',['as'=>'states_activate','uses'=>'Admin\StateController@activate']);
		Route::get('deactivate/{enc_id}',['as'=>'states_deactivate','uses'=>'Admin\StateController@deactivate']);
		Route::post('multi_action',['as'=>'states_multi_action','uses'=>'Admin\StateController@multi_action']);
		Route::get('search',['as'=>'states_search','uses'=>'Admin\StateController@search']);
	});

	/* Admin cities section of location module*/
   	Route::group(array('prefix' => '/cities'), function()
	{
		Route::get('/',['as' => 'cities_manage' ,'uses' => 'Admin\CityController@index']);
		Route::get('create',['as' => 'cities_create' ,'uses' => 'Admin\CityController@create']);
		Route::get('edit/{enc_id}',['as' => 'cities_edit' ,'uses' => 'Admin\CityController@edit']);
		Route::any('store',['as' => 'cities_store' ,'uses' => 'Admin\CityController@store']);
		Route::post('update/{enc_id}',['as' => 'cities_update' ,'uses' => 'Admin\CityController@update']);
		/*Route::get('delete/{enc_id}',['as'=>'cities_delete','uses'=>'Admin\CityController@delete']);	*/
		Route::get('activate/{enc_id}',['as'=>'cities_activate','uses'=>'Admin\CityController@activate']);
		Route::get('deactivate/{enc_id}',['as'=>'cities_deactivate','uses'=>'Admin\CityController@deactivate']);
		Route::post('multi_action',['as'=>'cities_multi_action','uses'=>'Admin\CityController@multi_action']);
		Route::get('get_states/{enc_country_id}',['as'=>'cities_get_states_by_country','uses'=>'Admin\CityController@get_states']);
		Route::get('search',['as'=>'cities_search','uses'=>'Admin\CityController@search']);
	});	

	/* Admin skills Routes Starts */
   	Route::group(array('prefix' => '/skills'), function()
	{
		Route::get('/',['as' => 'skill_manage' ,'uses' => 'Admin\SkillsController@index']);
		Route::get('create',['as' => 'skill_create' ,'uses' => 'Admin\SkillsController@create']);
		Route::get('edit/{enc_id}',['as' => 'skill_edit' ,'uses' => 'Admin\SkillsController@edit']);
		Route::any('store',['as' => 'skill_store' ,'uses' => 'Admin\SkillsController@store']);
		Route::post('update/{enc_id}',['as' => 'skill_update' ,'uses' => 'Admin\SkillsController@update']);
		Route::get('delete/{enc_id}',['as'=>'skill_delete','uses'=>'Admin\SkillsController@delete']);	
		Route::get('activate/{enc_id}',['as'=>'skill_activate','uses'=>'Admin\SkillsController@activate']);	
		Route::get('deactivate/{enc_id}',['as'=>'skill_deactivate','uses'=>'Admin\SkillsController@deactivate']);	
		Route::post('multi_action',['as'=>'skill_multi_action','uses'=>'Admin\SkillsController@multi_action']);
		Route::get('export',['as'=>'skills_export','uses'=>'Admin\SkillsController@export_skills']);
	});

   	/* Admin Categories Routes Starts */
   	Route::group(array('prefix' => '/categories'), function()
	{	
		Route::get('/',['as' => 'categories_manage' ,'uses' => 'Admin\CategoriesController@index']);
		Route::get('create',['as' => 'categories_create' ,'uses' => 'Admin\CategoriesController@create']);
		Route::get('edit/{enc_id}',['as' => 'categories_edit' ,'uses' => 'Admin\CategoriesController@edit']);
		Route::any('store',['as' => 'categories_store' ,'uses' => 'Admin\CategoriesController@store']);
		Route::post('update/{enc_id}',['as' => 'categories_update' ,'uses' => 'Admin\CategoriesController@update']);
		Route::get('delete/{enc_id}',['as'=>'categories_delete','uses'=>'Admin\CategoriesController@delete']);	
		Route::get('activate/{enc_id}',['as'=>'categories_activate','uses'=>'Admin\CategoriesController@activate']);	
		Route::get('deactivate/{enc_id}',['as'=>'categories_deactivate','uses'=>'Admin\CategoriesController@deactivate']);	
		Route::post('multi_action',['as'=>'categories_multi_action','uses'=>'Admin\CategoriesController@multi_action']);	
		Route::get('export',['as'=>'categories_export','uses'=>'Admin\CategoriesController@export_categories']);	
	});

	/* Admin subategories Routes Starts */
   	Route::group(array('prefix' => '/subcategories'), function()
	{
		Route::get('/',['as' => 'subcategories_manage' ,'uses' => 'Admin\SubCategoriesController@index']);
		Route::get('create',['as' => 'subcategories_create' ,'uses' => 'Admin\SubCategoriesController@create']);
		Route::get('edit/{enc_id}',['as' => 'subcategories_edit' ,'uses' => 'Admin\SubCategoriesController@edit']);
		Route::any('store',['as' => 'subcategories_store' ,'uses' => 'Admin\SubCategoriesController@store']);
		Route::post('update/{enc_id}',['as' => 'subcategories_update' ,'uses' => 'Admin\SubCategoriesController@update']);
		Route::get('delete/{enc_id}',['as'=>'subcategories_delete','uses'=>'Admin\SubCategoriesController@delete']);	
		Route::get('activate/{enc_id}',['as'=>'subcategories_activate','uses'=>'Admin\SubCategoriesController@activate']);	
		Route::get('deactivate/{enc_id}',['as'=>'subcategories_deactivate','uses'=>'Admin\SubCategoriesController@deactivate']);	
		Route::post('multi_action',['as'=>'subcategories_multi_action','uses'=>'Admin\SubCategoriesController@multi_action']);	

	});	

   	Route::group(array('prefix' => '/sealed_entry_rates'), function()
	{	
		Route::get('/',['as' => 'sealed_entry_rates_manage' ,'uses' => 'Admin\SealedEntryRatesController@index']);
		Route::get('create',['as' => 'sealed_entry_rates_create' ,'uses' => 'Admin\SealedEntryRatesController@create']);
		Route::get('edit/{enc_id}',['as' => 'sealed_entry_rates_edit' ,'uses' => 'Admin\SealedEntryRatesController@edit']);
		Route::any('store',['as' => 'sealed_entry_rates_store' ,'uses' => 'Admin\SealedEntryRatesController@store']);
		Route::post('update/{enc_id}',['as' => 'sealed_entry_rates_update' ,'uses' => 'Admin\SealedEntryRatesController@update']);
		Route::get('delete/{enc_id}',['as'=>'sealed_entry_rates_delete','uses'=>'Admin\SealedEntryRatesController@delete']);	
		Route::get('activate/{enc_id}',['as'=>'sealed_entry_rates_activate','uses'=>'Admin\SealedEntryRatesController@activate']);	
		Route::get('deactivate/{enc_id}',['as'=>'sealed_entry_rates_deactivate','uses'=>'Admin\SealedEntryRatesController@deactivate']);	
		Route::post('multi_action',['as'=>'sealed_entry_rates_multi_action','uses'=>'Admin\SealedEntryRatesController@multi_action']);	
		Route::get('export',['as'=>'sealed_entry_rates_export','uses'=>'Admin\SealedEntryRatesController@export_categories']);	
	});

	/* Admin currency Routes Starts */

   	Route::group(array('prefix' => '/currency'), function()
	{
		Route::get('/',['as' => 'currency_manage' ,'uses' => 'Admin\CurrencyController@index']);
		Route::get('create',['as' => 'currency_create' ,'uses' => 'Admin\CurrencyController@create']);
		Route::get('edit/{enc_id}',['as' => 'currency_edit' ,'uses' => 'Admin\CurrencyController@edit']);
		Route::any('store',['as' => 'currency_store' ,'uses' => 'Admin\CurrencyController@store']);
		Route::post('update/{enc_id}',['as' => 'currency_update' ,'uses' => 'Admin\CurrencyController@update']);
		Route::get('delete/{enc_id}',['as'=>'currency_delete','uses'=>'Admin\CurrencyController@delete']);	
		Route::get('activate/{enc_id}',['as'=>'currency_activate','uses'=>'Admin\CurrencyController@activate']);	
		Route::get('deactivate/{enc_id}',['as'=>'currency_deactivate','uses'=>'Admin\CurrencyController@deactivate']);	
		Route::post('multi_action',['as'=>'currency_multi_action','uses'=>'Admin\CurrencyController@multi_action']);
		Route::get('job_post_budget/{enc_id}',['as'=>'job_post_budget','uses'=>'Admin\CurrencyController@job_post_budget']);	
		Route::post('store_job_post_budget/{enc_id}',['as'=>'store_job_post_budget','uses'=>'Admin\CurrencyController@store_job_post_budget']);
		Route::post('store_job_post_hourly_budget/{enc_id}',['as'=>'store_job_post_hourly_budget','uses'=>'Admin\CurrencyController@store_job_post_hourly_budget']);

	});


   	/* Admin currency Routes Starts */

   	Route::group(array('prefix' => '/currency_rate'), function()
	{
		Route::get('/',['as' => 'currency_rate_manage' ,'uses' => 'Admin\CurrencyRateController@index']);
	});


	/* Admin Deposite currency Routes Starts */

   	Route::group(array('prefix' => '/deposite_currency'), function()
	{
		Route::get('/',['as' => 'deposite_currency_manage' ,'uses' => 'Admin\DepositeCurrencyController@index']);
		Route::get('create',['as' => 'deposite_currency_create' ,'uses' => 'Admin\DepositeCurrencyController@create']);
		Route::get('edit/{enc_id}',['as' => 'deposite_currency_edit' ,'uses' => 'Admin\DepositeCurrencyController@edit']);
		Route::any('store',['as' => 'deposite_currency_store' ,'uses' => 'Admin\DepositeCurrencyController@store']);
		Route::post('update/{enc_id}',['as' => 'deposite_currency_update' ,'uses' => 'Admin\DepositeCurrencyController@update']);
		Route::get('delete/{enc_id}',['as'=>'deposite_currency_delete','uses'=>'Admin\DepositeCurrencyController@delete']);	
		Route::get('activate/{enc_id}',['as'=>'deposite_currency_activate','uses'=>'Admin\DepositeCurrencyController@activate']);	
		Route::get('deactivate/{enc_id}',['as'=>'deposite_currency_deactivate','uses'=>'Admin\DepositeCurrencyController@deactivate']);	
		Route::post('multi_action',['as'=>'deposite_currency_multi_action','uses'=>'Admin\DepositeCurrencyController@multi_action']);	
	});	


	/* Admin Professions Routes Starts */
   	Route::group(array('prefix' => '/professions'), function()
	{
		Route::get('/',['as' => 'professions_manage' ,'uses' => 'Admin\ProfessionController@index']);
		Route::get('create',['as' => 'professions_create' ,'uses' => 'Admin\ProfessionController@create']);
		Route::get('edit/{enc_id}',['as' => 'professions_edit' ,'uses' => 'Admin\ProfessionController@edit']);
		Route::any('store',['as' => 'professions_store' ,'uses' => 'Admin\ProfessionController@store']);
		Route::post('update/{enc_id}',['as' => 'professions_update' ,'uses' => 'Admin\ProfessionController@update']);
		Route::get('delete/{enc_id}',['as'=>'professions_delete','uses'=>'Admin\ProfessionController@delete']);	
		Route::get('activate/{enc_id}',['as'=>'professions_activate','uses'=>'Admin\ProfessionController@activate']);	
		Route::get('deactivate/{enc_id}',['as'=>'professions_deactivate','uses'=>'Admin\ProfessionController@deactivate']);	
		Route::post('multi_action',['as'=>'professions_multi_action','uses'=>'Admin\ProfessionController@multi_action']);	
		Route::get('export',['as'=>'professions_export','uses'=>'Admin\ProfessionController@export_professions']);	
	});	

	/* Admin newsletters Routes Starts */
   	Route::group(array('prefix' => '/newsletters'), function()
	{
		Route::get('/',['as' => 'newsletters_manage' ,'uses' => 'Admin\NewslettersController@index']);
		Route::get('create',['as' => 'newsletters_create' ,'uses' => 'Admin\NewslettersController@create']);
		Route::get('edit/{enc_id}',['as' => 'newsletters_edit' ,'uses' => 'Admin\NewslettersController@edit']);
		Route::any('store',['as' => 'newsletters_store' ,'uses' => 'Admin\NewslettersController@store']);
		Route::post('update/{enc_id}',['as' => 'newsletters_update' ,'uses' => 'Admin\NewslettersController@update']);
		Route::get('show/{enc_id}',['as' => 'newsletters_show' ,'uses' => 'Admin\NewslettersController@show']);
		Route::get('delete/{enc_id}',['as'=>'newsletters_delete','uses'=>'Admin\NewslettersController@delete']);	
		Route::get('activate/{enc_id}',['as'=>'newsletters_activate','uses'=>'Admin\NewslettersController@activate']);	
		Route::get('deactivate/{enc_id}',['as'=>'newsletters_deactivate','uses'=>'Admin\NewslettersController@deactivate']);	
		Route::post('multi_action',['as'=>'newsletters_multi_action','uses'=>'Admin\NewslettersController@multi_action']);	
		Route::get('send',['as' => 'newsletters_send_view' ,'uses' => 'Admin\NewslettersController@send']);
		Route::post('send',['as' => 'newsletters_send_email' ,'uses' => 'Admin\NewslettersController@send_email']);

	});	

	/* Admin newsletter_subscriber Routes Starts */
   	Route::group(array('prefix' => '/newsletter_subscriber'), function()
	{
		Route::get('/',['as' => 'newsletter_subscriber_manage' ,'uses' => 'Admin\NewsletterSubscriberController@index']);
		Route::get('delete/{enc_id}',['as'=>'newsletter_subscriber_delete','uses'=>'Admin\NewsletterSubscriberController@delete']);	
		Route::get('activate/{enc_id}',['as'=>'newsletter_subscriber_activate','uses'=>'Admin\NewsletterSubscriberController@activate']);	
		Route::get('deactivate/{enc_id}',['as'=>'newsletter_subscriber_deactivate','uses'=>'Admin\NewsletterSubscriberController@deactivate']);	
		Route::post('multi_action',['as'=>'newsletter_subscriber_multi_action','uses'=>'Admin\NewsletterSubscriberController@multi_action']);	
	});	

   	/* Admin contact_enquiry Routes Starts */
   	Route::group(array('prefix' => '/contact_inquiries'), function()
	{
		Route::get('/',['as' => 'contact_inquiries_manage' ,'uses' => 'Admin\ContactEnquiryController@index']);
		Route::get('show/{enc_id}',['as' => 'contact_inquiries_show' ,'uses' => 'Admin\ContactEnquiryController@show']);
		Route::post('reply/{enc_id}',['as' => 'contact_inquiries_update' ,'uses' => 'Admin\ContactEnquiryController@reply']);
		Route::get('delete/{enc_id}',['as'=>'contact_inquiries_delete','uses'=>'Admin\ContactEnquiryController@delete']);
		Route::post('multi_action',['as'=>'contact_inquiries_multi_action','uses'=>'Admin\ContactEnquiryController@multi_action']);
	});	

	/* Admin project_manager Routes Starts */
   	Route::group(array('prefix' => '/project_manager'), function()
	{
		Route::get('/',['as' => 'project_manager_manage' ,'uses' => 'Admin\ProjectManagerController@index']);
		Route::get('create',['as' => 'project_manager_create' ,'uses' => 'Admin\ProjectManagerController@create']);
		Route::get('edit/{enc_id}',['as' => 'project_manager_edit' ,'uses' => 'Admin\ProjectManagerController@edit']);
		Route::any('store',['as' => 'project_manager_store' ,'uses' => 'Admin\ProjectManagerController@store']);
		Route::post('update/{enc_id}',['as' => 'project_manager_update' ,'uses' => 'Admin\ProjectManagerController@update']);
		Route::get('show/{enc_id}',['as' => 'project_manager_show' ,'uses' => 'Admin\ProjectManagerController@show']);
		Route::get('projects/{enc_id}',['as' => 'projects_show' ,'uses' => 'Admin\ProjectManagerController@projects']);
		Route::get('delete/{enc_id}',['as'=>'project_manager_delete','uses'=>'Admin\ProjectManagerController@delete']);	
		Route::get('activate/{enc_id}',['as'=>'project_manager_activate','uses'=>'Admin\ProjectManagerController@activate']);	
		Route::get('deactivate/{enc_id}',['as'=>'project_manager_deactivate','uses'=>'Admin\ProjectManagerController@deactivate']);	
		Route::post('multi_action',['as'=>'project_manager_multi_action','uses'=>'Admin\ProjectManagerController@multi_action']);
	});

	Route::group(array('prefix' => '/recruiter'), function()
	{
		Route::get('/',['as' => 'recruiter_manage' ,'uses' => 'Admin\RecruiterController@index']);
		Route::get('create',['as' => 'recruiter_create' ,'uses' => 'Admin\RecruiterController@create']);
		Route::get('edit/{enc_id}',['as' => 'recruiter_edit' ,'uses' => 'Admin\RecruiterController@edit']);
		Route::any('store',['as' => 'recruiter_store' ,'uses' => 'Admin\RecruiterController@store']);
		Route::post('update/{enc_id}',['as' => 'recruiter_update' ,'uses' => 'Admin\RecruiterController@update']);
		Route::get('show/{enc_id}',['as' => 'recruiter_show' ,'uses' => 'Admin\RecruiterController@show']);
		Route::get('projects/{enc_id}',['as' => 'projects_show' ,'uses' => 'Admin\RecruiterController@projects']);
		Route::get('delete/{enc_id}',['as'=>'recruiter_delete','uses'=>'Admin\RecruiterController@delete']);	
		Route::get('activate/{enc_id}',['as'=>'recruiter_activate','uses'=>'Admin\RecruiterController@activate']);	
		Route::get('deactivate/{enc_id}',['as'=>'recruiter_deactivate','uses'=>'Admin\RecruiterController@deactivate']);	
		Route::post('multi_action',['as'=>'recruiter_multi_action','uses'=>'Admin\RecruiterController@multi_action']);
	});

	/* Admin clients_manage Routes Starts */
   	Route::group(array('prefix' => '/clients'), function()
	{
		Route::get('/',['as' => 'clients_manage' ,'uses' => 'Admin\ClientsController@index']);
		Route::get('/deleted',['as' => 'deleted_clients_manage' ,'uses' => 'Admin\ClientsController@deleted_client']);
		Route::get('view/{enc_id}',['as' => 'view_deleted_client' ,'uses' => 'Admin\ClientsController@view_deleted_clients']);

		Route::get('edit/{enc_id}',['as' => 'clients_manage_edit' ,'uses' => 'Admin\ClientsController@edit']);
		Route::post('update/{enc_id}',['as' => 'client_manage_update' ,'uses' => 'Admin\ClientsController@update']);
		Route::get('delete/{enc_id}',['as'=>'client_manage_delete','uses'=>'Admin\ClientsController@delete']);	
		Route::get('activate/{enc_id}',['as'=>'client_manage_activate','uses'=>'Admin\ClientsController@activate']);	
		Route::get('deactivate/{enc_id}',['as'=>'client_manage_deactivate','uses'=>'Admin\ClientsController@deactivate']);	
		Route::post('multi_action',['as'=>'client_manage_multi_action','uses'=>'Admin\ClientsController@multi_action']);
		Route::get('projects/{enc_id}',['as'=>'client_manage_projects','uses'=>'Admin\ClientsController@projects']);
		Route::get('projects/show/{enc_id}',['as'=>'client_projects_show','uses'=>'Admin\ClientsController@show']);		
		Route::get('projects/delete/{enc_id}',['as'=>'client_projects_delete','uses'=>'Admin\ClientsController@delete_project']);	
		Route::get('private/{enc_id}',['as'=>'client_manage_private','uses'=>'Admin\ClientsController@make_private']);	
		Route::get('business/{enc_id}',['as'=>'client_manage_public','uses'=>'Admin\ClientsController@make_business']);	
	});

    /* Admin clients_manage Routes Starts */
   	Route::group(array('prefix' => '/experts'), function()
	{
		Route::get('/',['as' => 'experts_manage' ,'uses' => 'Admin\ExpertsController@index']);
		Route::get('/deleted',['as' => 'deleted_expert_manage' ,'uses' => 'Admin\ExpertsController@deleted_expert']);
		Route::get('view/{enc_id}',['as' => 'experts_manage_edit' ,'uses' => 'Admin\ExpertsController@view_deleted_experts']);
		Route::get('edit/{enc_id}',['as' => 'experts_manage_edit' ,'uses' => 'Admin\ExpertsController@edit']);
		Route::post('update/{enc_id}',['as' => 'experts_manage_update' ,'uses' => 'Admin\ExpertsController@update']);
		Route::get('delete/{enc_id}',['as'=>'expert_manage_delete','uses'=>'Admin\ExpertsController@delete']);	
		Route::get('activate/{enc_id}',['as'=>'expert_manage_activate','uses'=>'Admin\ExpertsController@activate']);	
		Route::get('deactivate/{enc_id}',['as'=>'expert_manage_deactivate','uses'=>'Admin\ExpertsController@deactivate']);	
		Route::post('multi_action',['as'=>'expert_manage_multi_action','uses'=>'Admin\ExpertsController@multi_action']);
		Route::get('projects/{enc_id}',['as'=>'expert_manage_projects','uses'=>'Admin\ExpertsController@projects']);
		Route::get('projects/show/{enc_id}',['as'=>'expert_projects_show','uses'=>'Admin\ExpertsController@show']);		
		Route::get('projects/delete/{enc_id}',['as'=>'expert_projects_delete','uses'=>'Admin\ClientsController@delete_project']);
		Route::get('subscription/{enc_id}',['as'=>'expert_manage_subscription','uses'=>'Admin\ExpertsController@subscription']);	
	});

	/* Admin projects_manage Routes Starts */
   	Route::group(array('prefix' => '/projects'), function()
	{
		Route::get('/',['as' => 'projects_all' ,'uses' => 'Admin\ProjectsController@all']);
        Route::get('all/',['as' => 'projects_all' ,'uses' => 'Admin\ProjectsController@all']);
        Route::get('posted/',['as' => 'projects_new' ,'uses' => 'Admin\ProjectsController@posted_projects']);
        Route::get('posted_recruiter/',['as' => 'projects_new_recruiter' ,'uses' => 'Admin\ProjectsController@assign_recruiter']);
        Route::get('open/',['as' => 'projects_new' ,'uses' => 'Admin\ProjectsController@open_projects']);
        Route::get('ongoing/',['as' => 'projects_new' ,'uses' => 'Admin\ProjectsController@ongoing_projects']);
        Route::get('completed/',['as' => 'projects_closed' ,'uses' => 'Admin\ProjectsController@completed_project']);
        Route::get('canceled/',['as' => 'projects_new' ,'uses' => 'Admin\ProjectsController@canceled_project']);
        Route::get('show/{enc_id}',['as'=>'projects_show','uses'=>'Admin\ProjectsController@show']);
        Route::post('update_project_status',['as' => 'update_project_status' ,'uses' => 'Admin\ProjectsController@update_project_status']);	
        Route::post('update_recruiter_project_status',['as' => 'update_recruiter_project_status' ,'uses' => 'Admin\ProjectsController@update_recruiter_project_status']);		
		Route::post('update_project_manager',['as' => 'update_project_manager' ,'uses' => 'Admin\ProjectsController@update_project_manager']);		
		Route::get('delete/{enc_id}',['as'=>'projects_delete','uses'=>'Admin\ProjectsController@delete']);	
		Route::post('multi_action',['as'=>'project_manage_multi_action','uses'=>'Admin\ProjectsController@multi_action']);
		/*Notification */
		Route::get('notification/{enc_id}',	['as'=>'notification_seen',	'uses'=>'Admin\ProjectsController@notifications_seen']);
		/* Reviews */
		Route::get('reviews/{enc_id}',	['as'=>'project_reviews','uses'=>'Admin\ProjectsController@project_reviews']);
		Route::get('reviews/details/{enc_id}',	['as'=>'project_reviews','uses'=>'Admin\ProjectsController@project_review_details']);
		Route::post('reviews_update',	['as'=>'project_reviews','uses'=>'Admin\ProjectsController@project_review_update']);

	});

    /* Contests routes */
	Route::group(array('prefix' => '/contests'), function()
	{
		Route::group(array('prefix' => '/pricing'), function()
    	{
		   Route::get('/', 					['as' => 'pricing_listing'     ,'uses' => 'Admin\ContestsController@pricing_listing']);
		   Route::get('edit/{enc_id}',      ['as' => 'pricing_edit'        ,'uses' => 'Admin\ContestsController@edit']);
		   Route::any('update',             ['as' => 'pricing_edit_update' ,'uses' => 'Admin\ContestsController@update']);
		});
		
		Route::group(array('prefix'=>'/manage'),function()
		{
		$controller = 'Admin\ContestsController'; 
			Route::any('/',		            	['as' => 'contests_manage' ,'uses' => $controller.'@manage']);
			Route::any('show/{enc_id}',			['as' => 'contests_show' ,'uses' => $controller.'@show']);
			Route::any('load-entries/{enc_id}',	['as' => 'contests_load-entries' ,'uses' => $controller.'@load_entries']);
			Route::any('show-entry-details/{enc_id}',	['as' => 'show-entry-details' ,'uses' => $controller.'@show_entry_details']);
			Route::post('remove-file',          ['as'=>'remove-file','uses'=>$controller.'@remove_file']);
		});
	});

    /* Admin projects_manage Routes Starts */
   	Route::group(array('prefix' => '/project-priority-pacakges'), function()
   	{
		Route::get('manage/',['as'             => 'project-priority-pacakges'      ,'uses' => 'Admin\ProjectPriorityPacakge@index']);
		Route::get('activate/{enc_id}',['as'   =>'project-priority-activate'       ,'uses' => 'Admin\ProjectPriorityPacakge@activate']);	
		Route::get('deactivate/{enc_id}',['as' =>'project-priority-deactivate'     ,'uses' => 'Admin\ProjectPriorityPacakge@deactivate']);	
		Route::post('multi_action',['as'       =>'project-priority-multi-action'   ,'uses' => 'Admin\ProjectPriorityPacakge@multi_action']);
		Route::any('create',['as'              =>'project-priority-multi-create'   ,'uses' => 'Admin\ProjectPriorityPacakge@create']);
		Route::any('store',['as'               =>'project-priority-multi-store'    ,'uses' => 'Admin\ProjectPriorityPacakge@store']);
		Route::get('edit/{enc_id}',['as'       =>'project-priority-multi-edit'     ,'uses' => 'Admin\ProjectPriorityPacakge@edit']);
		Route::get('delete/{enc_id}',['as'     =>'project-priority-multi-delete'   ,'uses' => 'Admin\ProjectPriorityPacakge@delete']);
		Route::any('update',['as'              =>'project-priority-multi-update'   ,'uses' => 'Admin\ProjectPriorityPacakge@update']);
	});

	/* Admin subadmins Routes Starts */
   	Route::group(array('prefix' => '/subadmins'), function()
	{
		Route::get('/',['as' => 'subadmins_manage' ,'uses' => 'Admin\SubAdminController@index']);
		Route::get('create',['as' => 'subadmins_create' ,'uses' => 'Admin\SubAdminController@create']);
		Route::get('edit/{enc_id}',['as' => 'subadmins_edit' ,'uses' => 'Admin\SubAdminController@edit']);
		Route::post('store',['as' => 'subadmins_store' ,'uses' => 'Admin\SubAdminController@store']);
		Route::post('update/{enc_id}',['as' => 'subadmins_update' ,'uses' => 'Admin\SubAdminController@update']);
		Route::get('show/{enc_id}',['as' => 'subadmins_show' ,'uses' => 'Admin\SubAdminController@show']);
		/*Route::get('delete/{enc_id}',['as'=>'subadmins_delete','uses'=>'Admin\SubAdminController@delete']);	*/
		Route::get('activate/{enc_id}',['as'=>'subadmins_activate','uses'=>'Admin\SubAdminController@activate']);	
		Route::get('deactivate/{enc_id}',['as'=>'subadmins_deactivate','uses'=>'Admin\SubAdminController@deactivate']);	
		Route::post('multi_action',['as'=>'subadmins_multi_action','uses'=>'Admin\SubAdminController@multi_action']);
		Route::get('change_password/{enc_id}',['as' => 'subadmins_change_password' ,'uses' => 'Admin\SubAdminController@change_password']);
		Route::post('update_password/{enc_id}',['as' => 'subadmins_update_password' ,'uses' => 'Admin\SubAdminController@update_password']);
		Route::get('download/{enc_id}',['as' => 'subadmins_attachment_download' ,'uses' => 'Admin\SubAdminController@attachment_download']);
		Route::get('set_permissions/{enc_id}',['as' => 'subadmins_set_permissions' ,'uses' => 'Admin\SubAdminController@set_permissions']);
		Route::post('store_permissions',['as' => 'subadmins_store_permissions' ,'uses' => 'Admin\SubAdminController@store_permissions']);	
	});

	/* Admin payment_settings Routes Starts */

   	Route::group(array('prefix' => '/payment_settings'), function()
	{
		Route::get('/',['as' => 'payment_settings_edit' ,'uses' => 'Admin\PaymentSettingsController@index']);
		Route::post('update',['as' => 'payment_settings_update' ,'uses' => 'Admin\PaymentSettingsController@update']);
	});	

	/* Admin subscription_packs module*/
   	Route::group(array('prefix' => '/subscription_packs'), function()
	{
		Route::get('/',['as' => 'subscription_packs_manage' ,'uses' => 'Admin\SubscriptionPacksController@index']);
		Route::get('edit/{enc_id}',['as' => 'subscription_packs_edit' ,'uses' => 'Admin\SubscriptionPacksController@edit']);
		Route::post('update/{enc_id}',['as' => 'subscription_packs_update' ,'uses' => 'Admin\SubscriptionPacksController@update']);
		/*Route::get('activate/{enc_id}',['as'=>'subscription_packs_activate','uses'=>'Admin\SubscriptionPacksController@activate']);	
		Route::get('deactivate/{enc_id}',['as'=>'subscription_packs_deactivate','uses'=>'Admin\SubscriptionPacksController@deactivate']);	*/
		Route::get('set_default/{enc_id}',['as'=>'subscription_packs_activate','uses'=>'Admin\SubscriptionPacksController@set_default']);	
		/*Route::post('multi_action',['as'=>'subscription_packs_multi_action','uses'=>'Admin\SubscriptionPacksController@multi_action']);	*/
	});	

	Route::group(array('prefix' => '/transactions'), function()
	{
		Route::get('/', ['as' => 'admin_transaction', 'uses' => 'Admin\TransactionsController@index']);
		Route::get('/show/{enc_id}', ['as' => 'admin_transaction_show', 'uses' => 'Admin\TransactionsController@show']);
		Route::any('/filter',['as' => 'admin_transaction_filtering' ,'uses' => 'Admin\TransactionsController@filter']);
		Route::post('/export', ['as' => 'admin_transaction_export', 'uses' => 'Admin\TransactionsController@export']);
	});

	Route::group(array('prefix' => '/email'), function()
	{
		Route::get('/', ['as' => 'email', 'uses' => 'Admin\EmailController@index']);
		Route::get('/search_users/{search}', ['as' => 'search_user', 'uses' => 'Admin\EmailController@search_users']);
		Route::post('/send', ['as' => 'send_email', 'uses' => 'Admin\EmailController@send']);
		Route::get('/listing', ['as' => 'send_email_listing', 'uses' => 'Admin\EmailController@listing']);
		Route::get('/details/{enc_id}', ['as' => 'send_details', 'uses' => 'Admin\EmailController@details']);
	});

	Route::group(array('prefix' => '/bids'), function()
	{
		Route::get('/{enc_id}', ['as' => 'bids', 'uses' => 'Admin\BidsController@all']);
		Route::get('show/{enc_id}', ['as' => 'bid_details', 'uses' => 'Admin\BidsController@show']);
		Route::get('delete/{enc_id}',['as'=>'bid_delete','uses'=>'Admin\BidsController@delete']);	
		Route::post('multi_action',['as'=>'bid_manage_multi_action','uses'=>'Admin\BidsController@multi_action']);
	
	});

	Route::group(array('prefix' => '/milestones'), function()
	{
		Route::get('/{enc_id}', ['as' => 'bids', 'uses' => 'Admin\MilestonesController@all']);
		Route::get('show/{enc_id}', ['as' => 'bid_details', 'uses' => 'Admin\MilestonesController@show']);
		Route::get('delete/{enc_id}',['as'=>'bid_delete','uses'=>'Admin\BidsController@delete']);	
		Route::post('multi_action',['as'=>'bid_manage_multi_action','uses'=>'Admin\BidsController@multi_action']);
        Route::group(array('prefix' => '/refund'), function()
        {
  		  Route::any('requests',['as'=>'refunds-requests','uses'=>'Admin\MilestoneRefundController@index']);
  		  Route::any('process',['as'=>'process','uses'=>'Admin\WalletController@transfer_refund']);
	    });
	});

	/* following group is created later because of seperately showing milestone requests to the admin side*/
	Route::group(array('prefix' => '/project_milestones'), function()
	{
		Route::get('/', 				['as' => 'show_all_projects_with_milestones', 'uses' => 'Admin\MilestoneRequestsController@show_all_projects_with_milestones']);
		Route::get('all/{enc_id}', 		['as' => 'show_all_milestones', 'uses' => 'Admin\MilestoneRequestsController@show_all_milestones']);
		Route::get('show/{enc_id}', 	['as' => 'show_milestone', 'uses' => 'Admin\MilestoneRequestsController@show_milestone']);
		Route::get('release/{enc_id}',['as'=>'bid_manage_multi_action','uses'=>'Admin\MilestoneRequestsController@payment_methods']);
		Route::get('delete/{enc_id}',	['as'=>'bid_delete','uses'=>'Admin\BidsController@delete']);	
		Route::post('multi_action',		['as'=>'bid_manage_multi_action','uses'=>'Admin\BidsController@multi_action']);
	});

	/* Admin User Reports Routes Starts */
	Route::group(array('prefix' => '/reports'), function()
	{
		Route::get('/', ['as' => 'user_report', 'uses' => 'Admin\UserReportController@index']);
		Route::any('/filter',['as' => 'user_report_filtering' ,'uses' => 'Admin\UserReportController@filter']);
		Route::get('/show/{enc_id}', ['as' => 'user_report', 'uses' => 'Admin\UserReportController@show']);
		Route::post('/export', ['as' => 'user_export_report', 'uses' => 'Admin\UserReportController@export']);
	});

	/* Dispute Section Starts */
	Route::group(array('prefix' => '/dispute'), function()
	{
		Route::get('/', 					['as' => 'show_disputes', 		'uses' => 'Admin\DisputeController@index']);
		Route::get('details/{enc_id}', 		['as' => 'show_dispute', 		'uses' => 'Admin\DisputeController@view']);
		Route::get('messages/{enc_id}', 	['as' => 'show_messages', 		'uses' => 'Admin\DisputeController@show_messages']);
		Route::get('manager_messages/{enc_id}', 	['as' => 'show_messages_for_project_manager', 		'uses' => 'Admin\DisputeController@show_messages_for_project_manager']);
		Route::post('/comments', 			['as' => 'add_admin_comments', 	'uses' => 'Admin\DisputeController@add_admin_comments']);
	});

	/* Wallet */
	Route::group(array('prefix' => '/wallet'), function()
	{  
		Route::get('/', 					['as' => 'show_wallet', 		'uses' => 'Admin\WalletController@wallet']);
		Route::get('/fee', 					['as' => 'show_fee_wallet', 	'uses' => 'Admin\WalletController@fee_wallet']);
		Route::get('/archexpert', 			['as' => 'show_ewallet', 		'uses' => 'Admin\WalletController@archexpert_wallet']);
		Route::any('/create_user',          ['as' => 'wallet_create_user',  'uses' => 'Admin\WalletController@create_user']);
		Route::any('/create_wallet_using_mp_owner_id/{mp_owner_id}',['as'=>'create_wallet_using_mp_owner','uses'=>'Admin\WalletController@create_wallet_using_mp_owner_id']);
		Route::any('/add_bank',['as'=>'wallet_add_bank','uses'=>'Admin\WalletController@add_bank']);
		Route::any('/cashout_in_bank',['as'=>'wallet_cashout_in_bank','uses'=>'Admin\WalletController@make_cashout_in_bank']);
		Route::any('/upload_kyc_docs',['as'=>'wallet_upload_kyc_docs','uses'=>'Admin\WalletController@upload_kyc_docs']);
	});

	/* Support tickets Route Start */		
	Route::group(array('prefix' => 'support_ticket'), function () 
	{
		$route_slug = 'admin_';
		$module_controller = "Admin\SupportTicketController@";

		Route::get('/',				['as' =>$route_slug.'create', 'uses' => $module_controller.'index']);

		Route::get('/create',		['as' =>$route_slug.'create', 'uses' => $module_controller.'create']);

		Route::get('/reply/{id}',	['as' =>$route_slug.'reply', 'uses' => $module_controller.'reply']);
		
		Route::post('/submit_reply/{id}',	['as' =>$route_slug.'submit_reply', 'uses' => $module_controller.'submit_reply']);

		Route::get('/resolve_ticket/{id}',	['as' =>$route_slug.'resolve', 'uses' => $module_controller.'resolve_ticket']);

		Route::group(array('prefix' => 'support_categories'), function () use($route_slug)
		{
			$module_controller = "Admin\SupportCategoryController@";

			Route::get('/',						['as' =>$route_slug.'list', 'uses' => $module_controller.'index']);

			Route::get('/create',				['as' =>$route_slug.'create', 'uses' => $module_controller.'create']);

			Route::post('/store',				['as' =>$route_slug.'store', 'uses' => $module_controller.'store']);
			
			Route::get('/edit/{id}',			['as' =>$route_slug.'edit', 'uses' => $module_controller.'edit']);

			Route::post('/update/{id}',			['as' =>$route_slug.'update', 'uses' => $module_controller.'update']);

			Route::get('/delete/{id}',			['as' =>$route_slug.'delete', 'uses' => $module_controller.'delete']);

		});


	});

	Route::group(array('prefix' => 'blogs'), function () use($route_slug)
	{
		$module_controller = "Admin\BlogsController@";

		Route::get('/',['as' =>$route_slug.'index', 'uses' => $module_controller.'index']);
		Route::get('/load_data',['as' =>$route_slug.'index', 'uses' => $module_controller.'load_data']);
		Route::get('/create',['as' =>$route_slug.'index', 'uses' => $module_controller.'create']);
		Route::post('/store',['as' =>$route_slug.'index', 'uses' => $module_controller.'store']);
		Route::get('/edit/{enc_id}',['as' =>$route_slug.'index', 'uses' => $module_controller.'edit']);
		Route::get('/view/{enc_id}',['as' =>$route_slug.'index', 'uses' => $module_controller.'view']);
		Route::get('/delete/{enc_id}',['as' =>$route_slug.'index', 'uses' => $module_controller.'delete']);
		Route::post('/update/{enc_id}',['as' =>$route_slug.'index', 'uses' => $module_controller.'update']);
		Route::get('/deactivate/{enc_id}',['as' =>$route_slug.'index', 'uses' => $module_controller.'deactivate']);
		Route::get('/activate/{enc_id}',['as' =>$route_slug.'index', 'uses' => $module_controller.'activate']);
		Route::post('/update',['as' =>$route_slug.'index', 'uses' => $module_controller.'update']);
		Route::post('/multi_action',['as' =>$route_slug.'index', 'uses' => $module_controller.'multi_action']);
		
		Route::group(array('prefix' => 'comments'), function () use($route_slug)
		{
			$module_controller = "Admin\BlogCommentsController@";

			Route::get('/load_data/{id?}',['as' =>$route_slug.'index', 'uses' => $module_controller.'load_data']);
			Route::get('/{id?}',['as' =>$route_slug.'index', 'uses' => $module_controller.'index']);
			Route::get('/delete/{enc_id}',['as' =>$route_slug.'index', 'uses' => $module_controller.'delete']);
			Route::post('/multi-action',['as' =>$route_slug.'index', 'uses' => $module_controller.'multi_action']);
		});

		Route::group(array('prefix' => 'category'), function () use($route_slug)
		{
			$module_controller = "Admin\BlogCategoriesController@";

			Route::get('/',['as' =>$route_slug.'index', 'uses' => $module_controller.'index']);
			Route::get('/load_data',['as' =>$route_slug.'index', 'uses' => $module_controller.'load_data']);
			Route::get('/create',['as' =>$route_slug.'index', 'uses' => $module_controller.'create']);
			Route::post('/store',['as' =>$route_slug.'index', 'uses' => $module_controller.'store']);
			Route::get('/edit/{enc_id}',['as' =>$route_slug.'index', 'uses' => $module_controller.'edit']);
			Route::post('/update/{enc_id}',['as' =>$route_slug.'index', 'uses' => $module_controller.'update']);
			Route::get('/deactivate/{enc_id}',['as' =>$route_slug.'index', 'uses' => $module_controller.'deactivate']);
			Route::get('/activate/{enc_id}',['as' =>$route_slug.'index', 'uses' => $module_controller.'activate']);
			Route::post('/update',['as' =>$route_slug.'index', 'uses' => $module_controller.'update']);
			Route::post('/multi-action',['as' =>$route_slug.'index', 'uses' => $module_controller.'multi_action']);
			Route::post('/check-category',['as' =>$route_slug.'index', 'uses' => $module_controller.'check_category']);
		});

	});

});

?>