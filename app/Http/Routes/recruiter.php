<?php
    /* fronts expert routes group starts */
	Route::group(array('prefix' => '/recruiter','middleware' =>'front_recruiter'), function()
	{
		Route::get('profile',			['as'=>'front_project_manager_profile','uses'=>'Front\Recruiter\ProfileController@index']);
		Route::post('profile/update',	['as'=>'front_project_manager_profile','uses'=>'Front\Recruiter\ProfileController@update']);
		
		/*Delete Image From database and folder */
		Route::get('delete_image',['as'=>'front_expert_profile_delete_image','uses'=>'Front\Recruiter\ProfileController@delete_image']);

		/* Dashboard */		
		Route::get('dashboard',			['as'=>'front_project_manager_dashboard','uses'=>'Front\Recruiter\DashboardController@show_dashboard']);

		/* Change Password */
		Route::get('change_password',	['as'=>'front_project_manager_change_password','uses'=>'Front\Recruiter\ProfileController@change_password']);
		Route::post('update_password',	['as'=>'front_project_manager_update_password','uses'=>'Front\Recruiter\ProfileController@update_password']);

        
        /*Availability*/
		Route::get('availability',['as'=>'front_project_manager_availability','uses'=>'Front\Recruiter\ProfileController@availability']);	
		Route::post('update_availability',['as'=>'front_project_manager_availability','uses'=>'Front\Recruiter\ProfileController@update_availability']);
 
 		/* Change profile picture */
 		Route::any('store_profile_image',['as'=>'store_profile_image','uses'=>'Front\Recruiter\ProfileController@store_profile_image']);

		/* Show project inbox */
		Route::get('inbox',				['as'=>'_show_inbox',	'uses'=>'Front\MessagingController@show_inbox']);

		/* Project details of project manager */
		Route::group(array('prefix' => '/projects','middleware' =>'front_recruiter'), function()
		{
			Route::get('open',			    ['as'=>'ongoing_projects','uses'=>'Front\Recruiter\ProjectController@show_open_projects']);
			Route::get('completed',			['as'=>'completed_projects','uses'=>'Front\Recruiter\ProjectController@show_completed_projects']);
			Route::get('details/{enc_id}',	['as'=>'client_editproject','uses'=>'Front\Recruiter\ProjectController@show_project_details']);
			Route::any('suggest/{enc_id}',	['as'=>'client_suggest','uses'=>'Front\Recruiter\ProjectController@suggest']);
			Route::any('unsuggest/{enc_id}',['as'=>'client_suggest','uses'=>'Front\Recruiter\ProjectController@unsuggest']);
			Route::any('more_details/{enc_id}/{user_id}',	['as'=>'client_suggest',	'uses'=>'Front\Recruiter\ProjectController@more_details']);

			/*Notification */
			Route::get('notification/{enc_id}',	['as'=>'notification_seen',	'uses'=>'Front\Recruiter\ProjectController@notifications_seen']);

			  //invite experts
			// Route::any('invite_experts',		        ['as'=>'invite_experts',			'uses'=>'Front\Recruiter\ProjectController@invite_experts']);
			// //get experts
			// Route::any('get_experts',    		        ['as'=>'get_experts',	    		'uses'=>'Front\Recruiter\ProjectController@get_experts']);

			Route::any('/get_experts',['as'=>'get_experts','uses'=>'Front\Recruiter\ProjectController@get_experts']);
			Route::any('/invite_experts',['as'=>'invite_experts','uses'=>'Front\Recruiter\ProjectController@invite_experts']);
			
		});

		/* project bidding group */
		Route::group(array('prefix' => '/project_bid','middleware' =>'front_recruiter'), function()
		{
			Route::get('add/{enc_id}',			['as'=>'add_project_bid','uses'=>'Front\Recruiter\ProjectController@show_add_project_bid']);
			Route::post('store_bid_details',	['as'=>'store_bid_details','uses'=>'Front\Recruiter\ProjectController@store_bid_details']);
		});

		Route::group(array('prefix' => '/notifications','middleware' =>'front_recruiter'), function()
        {
			Route::get('/',						['uses'=>'Front\Recruiter\NotificationController@index']);
	 	});

	});

	// Route::group(array('prefix' => '/project_managers','middleware' =>'front'), function()
	// {
	// 	Route::get('/portfolio/{enc_id}',['as'=>'show_portfolio','uses'=>'Front\Recruiter\ProfileController@show_portfolio']);
	// });

?>