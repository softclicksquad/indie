<?php
    /* fronts expert routes group starts */
	Route::group(array('prefix' => '/project_manager','middleware' =>'front_project_manager'), function()
	{
		Route::get('profile',			['as'=>'front_project_manager_profile','uses'=>'Front\Project_manager\ProfileController@index']);
		Route::post('profile/update',	['as'=>'front_project_manager_profile','uses'=>'Front\Project_manager\ProfileController@update']);
		
		/*Delete Image From database and folder */
		Route::get('delete_image',['as'=>'front_expert_profile_delete_image','uses'=>'Front\Project_manager\ProfileController@delete_image']);

		/* Dashboard */		
		Route::get('dashboard',			['as'=>'front_project_manager_dashboard','uses'=>'Front\Project_manager\DashboardController@show_dashboard']);

		/* Change Password */
		Route::get('change_password',	['as'=>'front_project_manager_change_password','uses'=>'Front\Project_manager\ProfileController@change_password']);
		Route::post('update_password',	['as'=>'front_project_manager_update_password','uses'=>'Front\Project_manager\ProfileController@update_password']);

        
        /*Availability*/
		Route::get('availability',['as'=>'front_project_manager_availability','uses'=>'Front\Project_manager\ProfileController@availability']);	
		Route::post('update_availability',['as'=>'front_project_manager_availability','uses'=>'Front\Project_manager\ProfileController@update_availability']);
 
 		/* Change profile picture */
 		Route::any('store_profile_image',['as'=>'store_profile_image','uses'=>'Front\Project_manager\ProfileController@store_profile_image']);

		/* Show project inbox */
		Route::get('inbox',				['as'=>'_show_inbox',	'uses'=>'Front\MessagingController@show_inbox']);

		/* Project details of project manager */
		Route::group(array('prefix' => '/projects','middleware' =>'front_project_manager'), function()
		{

			Route::get('ongoing',			['as'=>'ongoing_projects','uses'=>'Front\Project_manager\ProjectController@show_ongoing_projects']);
			Route::get('open',			    ['as'=>'ongoing_projects','uses'=>'Front\Project_manager\ProjectController@show_open_projects']);
			Route::get('awarded',			    ['as'=>'awarded_projects','uses'=>'Front\Project_manager\ProjectController@show_awarded_projects']);
			
			Route::get('completed',			['as'=>'completed_projects','uses'=>'Front\Project_manager\ProjectController@show_completed_projects']);
			Route::get('canceled',			['as'=>'canceled_projects','uses'=>'Front\Project_manager\ProjectController@show_canceled_projects']);
			Route::get('details/{enc_id}',	['as'=>'client_editproject','uses'=>'Front\Project_manager\ProjectController@show_project_details']);
			Route::any('suggest/{enc_id}',	['as'=>'client_suggest','uses'=>'Front\Project_manager\ProjectController@suggest']);
			Route::any('unsuggest/{enc_id}',['as'=>'client_suggest','uses'=>'Front\Project_manager\ProjectController@unsuggest']);

			Route::any('more_details/{enc_id}/{user_id}',	['as'=>'client_suggest',	'uses'=>'Front\Project_manager\ProjectController@more_details']);

			/*Notification */
			Route::get('notification/{enc_id}',	['as'=>'notification_seen',	'uses'=>'Front\Project_manager\ProjectController@notifications_seen']);

			/* Milestones */
			Route::get('milestones/{enc_id}',	['as'=>'show_project_milestones',	'uses'=>'Front\Project_manager\MilestonesController@show_project_milestones']);
			Route::any('/get_experts',['as'=>'get_experts','uses'=>'Front\Project_manager\ProjectController@get_experts']);
			Route::any('/invite_experts',['as'=>'invite_experts','uses'=>'Front\Project_manager\ProjectController@invite_experts']);
		});

		/* project bidding group */
		Route::group(array('prefix' => '/project_bid','middleware' =>'front_project_manager'), function()
		{
			Route::get('add/{enc_id}',			['as'=>'add_project_bid','uses'=>'Front\Project_manager\ProjectController@show_add_project_bid']);
			Route::post('store_bid_details',	['as'=>'store_bid_details','uses'=>'Front\Project_manager\ProjectController@store_bid_details']);
			/*Route::get('completed',	['as'=>'completed_projects','uses'=>'Front\Project_manager\ProjectController@show_completed_projects']);
			Route::get('details/{enc_id}',	['as'=>'client_editproject','uses'=>'Front\Project_manager\ProjectController@show_project_details']);*/
		});

		Route::group(array('prefix' => '/notifications','middleware' =>'front_project_manager'), function()
        {
			Route::get('/',						['uses'=>'Front\Project_manager\NotificationController@index']);
	 	});

	});

	Route::group(array('prefix' => '/project_managers','middleware' =>'front'), function()
	{
		Route::get('/portfolio/{enc_id}',['as'=>'show_portfolio','uses'=>'Front\Project_manager\ProfileController@show_portfolio']);
	});

?>