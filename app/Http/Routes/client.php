<?php
/*fronts Client profile starts */
    Route::any('/add_new_card',['as'=>'front_client_wallet_create_card','uses'=>'Front\Client\WalletDashboardController@create_card']);


	  Route::any('/create_card_return',['as'=>'front_client_wallet_create_card','uses'=>'Front\Client\WalletDashboardController@create_card_return']);



   	Route::group(array('prefix' => '/client','middleware' =>'front_client'), function()
	{
		Route::group(array('prefix' => '/twilio-chat','middleware' =>'front_client'), function() {
			Route::get('/chat_list' ,['as'=>'front_projectbid_twilio-chat' ,'uses'=>'Common\TwilioChatController@show_chat_list']);
			Route::get('/projectbid/{enc_id}' ,['as'=>'front_projectbid_twilio-chat' ,'uses'=>'Common\TwilioChatController@project_bid_chat']);
			Route::get('/contest/{enc_id}' ,['as'=>'front_contest_twilio-chat' ,'uses'=>'Common\TwilioChatController@contest_chat']);
			Route::get('/refresh_token' ,['as'=>'front_refresh_token_twilio-chat' ,'uses'=>'Common\TwilioChatController@refresh_token']);
			Route::get('/update_channel' ,['as'=>'front_update_channel_twilio-chat' ,'uses'=>'Common\TwilioChatController@update_channel']);
			Route::get('/request_admin' ,['as'=>'front_request_admin_twilio-chat' ,'uses'=>'Common\TwilioChatController@request_admin']);
			Route::get('/video' ,['as'=>'front_video_twilio-chat' ,'uses'=>'Common\TwilioChatController@video_call']);
		});

        /* collaboration page */
        Route::get('collaboration_listing'  ,['as'    =>'front_client_profile','uses'=>'Front\Client\ProfileController@collaboration_listing']);   
        /* end collaboration page */
		Route::get('profile'                   ,['as'    =>'front_client_profile'                 ,'uses'=>'Front\Client\ProfileController@index']);
		Route::post('profile/update'           ,['as'    =>'front_client_profile'                 ,'uses'=>'Front\Client\ProfileController@update']);
		Route::get('dashboard'                 ,['as'    =>'front_client_dashboard'               ,'uses'=>'Front\Client\DashboardController@index']);
		Route::get('delete_image'              ,['as'    =>'front_expert_profile_delete_image'    ,'uses'=>'Front\Client\ProfileController@delete_image']);
		Route::get('delete_project_attachment' ,['as'    =>'delete_project_attachment'            ,'uses'=>'Front\Client\ProfileController@delete_project_attachment']);
		Route::get('change_password'           ,['as'    =>'front_client_change_password'         ,'uses'=>'Front\Client\ProfileController@change_password']);	
		Route::post('update_password'          ,['as'    =>'front_client_update_password'         ,'uses'=>'Front\Client\ProfileController@update_password']);
		Route::get('dashboard'                 ,['as'    =>'front_client_dashboard'               ,'uses'=>'Front\Client\DashboardController@show_dashboard']);
		Route::get('availability'              ,['as'    =>'front_client_availability'            ,'uses'=>'Front\Client\ProfileController@availability']);	
		Route::post('update_availability'      ,['as'    =>'front_client_availability'            ,'uses'=>'Front\Client\ProfileController@update_availability']);
		Route::any('store_profile_image'       ,['as'    =>'store_profile_image'                  ,'uses'=>'Front\Client\ProfileController@store_profile_image']);
        //Transactions
        Route::group(array('prefix' => '/transactions','middleware' =>'front_client'), function() {
			Route::get('/'              ,['as'=>'front_client_transactions'        ,'uses'=>'Front\Client\TransactionsController@index']);
			Route::get('show/{enc_id}'  ,['as'=>'front_client_transactions'        ,'uses'=>'Front\Client\TransactionsController@show']);
			Route::any('show_cnt'       ,['as'=>'show_cnt'                         ,'uses'=>'Front\Expert\TransactionsController@show_cnt']);
		});

		Route::group(array('prefix' => '/review','middleware' =>'front_client'), function(){
			Route::post('/add'   ,['as'=>'front_client_review'   ,'uses'=>'Front\Client\ReviewController@add']);
		});

		Route::get('reviews/{enc_id}'  ,['as'=>'show_review_page'  ,'uses'=>'Front\Client\ReviewController@show_review_page']);
		Route::get('projects/dispute'  ,['as'=>'show_projects'     ,'uses'=>'Front\Client\DisputeController@show_projects']);
		Route::get('dispute/{enc_id}'  ,['as'=>'show_dispute'      ,'uses'=>'Front\Client\DisputeController@show_dispute']);
		Route::post('add_dispute'      ,['as'=>'add_dispute'       ,'uses'=>'Front\Client\DisputeController@add']);

		Route::get('inbox'             ,['as'=>'_show_inbox'       ,'uses'=>'Front\MessagingController@show_inbox']);
		Route::get('accept_completion_request/{enc_id}'    ,['as'=>'accepet_project_completion_request_from_expert',	'uses'=>'Front\Client\ProjectsController@accept_completion_request']);

		Route::get('/notifications',					['as'=>'list', 'uses'=>'Front\HomeController@notifications']);
		
		
		Route::group(array('prefix' => '/projects','middleware' =>'front_client'), function()
		{
			Route::get('payment/{enc_id}',['as'=>'front_client_projects_post_payment','uses'=>'Front\Client\ProjectsController@payment_methods']);
			Route::any('payment/return/return',['as'=>'payment_return','uses'=>'Front\Client\ProjectsController@payment_methods_return']);
			Route::any('payment/return/project_manager_payment_return',['as'=>'payment_return','uses'=>'Front\Client\ProjectsController@project_manager_payment_return']);
	
			Route::get('pm_payment/{enc_id}',['as'=>'front_client_projects_post_payment','uses'=>'Front\Client\ProjectsController@pm_payment']);
			/* Project Post routes */
			Route::get('post',['as'=>'front_client_projects_post','uses'=>'Front\Client\ProjectsController@post']);
			Route::get('post/{enc_id}',['as'=>'front_client_projects_repost','uses'=>'Front\Client\ProjectsController@post']);
			Route::get('hire/{enc_id}',['as'=>'front_client_projects_repost','uses'=>'Front\Client\ProjectsController@hire']);
			Route::post('create',['as'=>'front_client_project_create','uses'=>'Front\Client\ProjectsController@create']);
			Route::post('subcatdata',['as'=>'front_client_project_subcatdata','uses'=>'Front\Client\ProjectsController@subcatdata']);
			/* Project type */
			Route::get('posted',				['as'=>'front_client_project_posted',	'uses'=>'Front\Client\ProjectsController@posted']);
			Route::get('awarded',				['as'=>'front_client_project_awarded',	'uses'=>'Front\Client\ProjectsController@show_awarded_projects']);
			Route::get('ongoing',				['as'=>'front_client_project_ongoing',	'uses'=>'Front\Client\ProjectsController@show_ongoing_projects']);

			Route::get('open',					['as'=>'ongoing_projects',		'uses'=>'Front\Client\ProjectsController@show_open_projects']);
			Route::get('completed',				['as'=>'front_client_project_completed',	'uses'=>'Front\Client\ProjectsController@completed_projects']);
		    Route::get('canceled',				['as'=>'front_client_project_canceled',	'uses'=>'Front\Client\ProjectsController@show_canceled_projects']);

			Route::get('details/{enc_id}',		['as'=>'show_project_details_page',		'uses'=>'Front\Client\ProjectsController@show_project_details_page']);
			Route::get('request/{enc_expert_id}/{enc_project_id}',		
												['as'=>'make_project_request',			'uses'=>'Front\Client\ProjectsController@make_project_request']);
			Route::get('cancel_award/{enc_project_id}/{enc_expert_id}',		
												['as'=>'cancel_project_request',			'uses'=>'Front\Client\ProjectsController@cancel_project_request']);

			Route::get('add_shortlist/{enc_project_id}',		
												['as'=>'add_shortlist',			'uses'=>'Front\Client\ProjectsController@add_shortlist']);
			Route::get('remove_shortlist/{enc_project_id}',		
												['as'=>'remove_shortlist',			'uses'=>'Front\Client\ProjectsController@remove_shortlist']);

			Route::get('edit/{enc_id}',			['as'=>'client_editproject',			'uses'=>'Front\Client\ProjectsController@editproject']);
	        Route::post('update/{enc_id}',		['as'=>'client_updateproject',			'uses'=>'Front\Client\ProjectsController@updateproject']);
	        Route::post('delete-document',	['as'=>'front_client_project_delete_document',	'uses'=>'Front\Client\ProjectsController@delete_document']);
	        Route::post('adding_info',		['as'=>'adding_info',			'uses'=>'Front\Client\ProjectsController@add_additional_info']);
	        Route::post('adding_shortlist_desc',		['as'=>'adding_shortlist_desc',			'uses'=>'Front\Client\ProjectsController@adding_shortlist_desc']);
	        Route::get('remove_shortlist_desc/{enc_id}',		['as'=>'remove_shortlist_desc',			'uses'=>'Front\Client\ProjectsController@remove_shortlist_desc']);
	        Route::post('update_description',		['as'=>'update_description',			'uses'=>'Front\Client\ProjectsController@update_description']);
			Route::post('manage',				['as'=>'front_client_project_manage',	'uses'=>'Front\Client\ProjectsController@manage']);
			/* Expert Details */
			Route::any('expert_details/{enc_id}/{user_id}',	['as'=>'expert_detail',	'uses'=>'Front\Client\ProjectsController@expert_details']);

			/*Notification */
			Route::get('notification/{enc_id}',	['as'=>'notification_seen',	'uses'=>'Front\Client\ProjectsController@notifications_seen']);

			/* Milestones */
			Route::post('store_milestone',				['as'=>'store_milestone',			'uses'=>'Front\Client\MilestonesController@store_milestone']);

			Route::any('store_milestone_return/',	    ['as'=>'store_milestone_return',			'uses'=>'Front\Client\MilestonesController@store_milestone_return']);

			Route::any('milestones/{enc_id}',			['as'=>'show_project_milestones',	'uses'=>'Front\Client\MilestonesController@show_project_milestones']);
			Route::get('milestones/update/{enc_id}',	['as'=>'show_update_milestone',	'uses'=>'Front\Client\MilestonesController@show_update_milestone']);
			Route::post('update_milestone',				['as'=>'update_milestone',	'uses'=>'Front\Client\MilestonesController@update_milestone']);
			Route::get('milestones/payment/{enc_id}',	['as'=>'show_payment_milestone',	'uses'=>'Front\Client\MilestonesController@payment_methods']);
			Route::get('milestones/delete/{enc_id}',	['as'=>'delete_milestone',	'uses'=>'Front\Client\MilestonesController@delete_milestone']);
			Route::any('milestones/approve/{enc_id}/{currency}',	['as'=>'approve_milestone_request',	'uses'=>'Front\Client\MilestonesController@approve_milestone_request']);
			Route::get('milestones/not_approve/{enc_id}',	['as'=>'approve_milestone_request',	'uses'=>'Front\Client\MilestonesController@not_approve_milestone_request']);
			Route::any('refund_requests',            	['as'=>'refund_requests',	'uses'=>'Front\Client\MilestonesController@send_milestone_refund_request_to_admin']);


			/* Cancel Project */
			Route::get('cancel_project/{enc_id}',		['as'=>'cancel_project',			'uses'=>'Front\Client\ProjectsController@cancel_project']);
            //invite experts
			Route::any('invite_experts',		        ['as'=>'invite_experts',			'uses'=>'Front\Client\ProjectsController@invite_experts']);
			//get experts
			Route::any('get_experts',    		        ['as'=>'get_experts',	    		'uses'=>'Front\Client\ProjectsController@get_experts']);

			Route::get('delete_project/{enc_id}',		['as'=>'delete_project',			'uses'=>'Front\Client\ProjectsController@delete_project']);
			
			Route::post('get_currency_budget_html',		['as'=>'get_currency_budget_html',	'uses'=>'Front\Client\ProjectsController@get_currency_budget_html']);

			Route::any('get_highlighted_html',		['as'=>'get_highlighted_html',	'uses'=>'Front\Client\ProjectsController@get_highlighted_html']);
			

			Route::group(array('prefix' => '/collaboration','middleware' =>'front_client'), function(){
	     		Route::any('/{enc_id}'  ,['as'=>'front_client_collaboration'   ,'uses'=>'Front\Client\ProjectCollaborationController@index']);
	     		Route::any('details/{enc_id}/{enc_id1}'  ,['as'=>'front_client_collaboration_details'   ,'uses'=>'Front\Client\ProjectCollaborationController@details']);
	     		Route::any('make-zip/{enc_id}'  ,['as'=>'front_client_collaboration_downloadzip'   ,'uses'=>'Front\Client\ProjectCollaborationController@make_zip']);
	     		Route::any('download-zip/{enc_id}'  ,['as'=>'front_client_collaboration_downloadzip'   ,'uses'=>'Front\Client\ProjectCollaborationController@download_zip']);
	     	}); 
		});
        
        Route::group(array('prefix' => '/contest','middleware' =>'front_client'), function()
        {
			Route::get('post'  ,['as'=>'front_client_contest_post'   ,'uses'=>'Front\Client\ContestController@post']);

			Route::any('create',['as'=>'front_client_contest_create' ,'uses'=>'Front\Client\ContestController@create']);

			Route::any('contest_payment/{enc_id}',['as'=>'front_client_contest_create_payment' ,'uses'=>'Front\Client\ContestController@contest_payment']);

			Route::any('update_contest_payment/{enc_id}/{amount}',['as'=>'front_client_contest_create_payment' ,'uses'=>'Front\Client\ContestController@update_contest_payment']);

			Route::any('posted',['as'=>'front_client_contest_posted' ,'uses'=>'Front\Client\ContestController@posted']);

			Route::any('ongoing',['as'=>'front_client_contest_ongoing' ,'uses'=>'Front\Client\ContestController@ongoing']);

			Route::any('completed',['as'=>'front_client_contest_completed' ,'uses'=>'Front\Client\ContestController@completed']);

			Route::any('expired',['as'=>'front_client_contest_expired' ,'uses'=>'Front\Client\ContestController@expired']);

			Route::any('canceled',['as'=>'front_client_contest_canceled' ,'uses'=>'Front\Client\ContestController@canceled']);
			

			Route::any('delete_contest/{enc_id}',['as'=>'front_client_delete_contest' ,'uses'=>'Front\Client\ContestController@delete_contest']);

			Route::get('edit/{enc_id}',	['as'=>'front_client_contest_edit',	'uses'=>'Front\Client\ContestController@editcontest']);

			Route::post('delete-document',	['as'=>'front_client_contest_delete_document',	'uses'=>'Front\Client\ContestController@delete_document']);

			Route::post('update',	['as'=>'front_client_contest_update',	'uses'=>'Front\Client\ContestController@update']);

			Route::get('details/{enc_id}',	['as'=>'front_client_contest_edit',	'uses'=>'Front\Client\ContestController@details']);

			Route::get('details/{enc_id}/API',	['as'=>'front_client_contest_edit',	'uses'=>'Front\Client\ContestController@details_api']);


			Route::get('/show_contest_entry_details/{enc_id}',	['as'=>'show_contest_details','uses'=>'Front\Client\ContestController@show_contest_entry_details']); 

			Route::post('store-comment',	['as'=>'store-comment',	'uses'=>'Front\Client\ContestController@store_contest_entry_file_comment']);

			Route::any('get-comments',	['as'=>'get-comments',	'uses'=>'Front\Client\ContestController@contest_entry_file_get_comments']);

			Route::post('store-rating',	['as'=>'store-rating',	'uses'=>'Front\Client\ContestController@store_contest_entry_file_rating']);

			Route::any('choose-winner/{contest_id}/{cnt_enty_id}',	['as'=>'choose-winner',	'uses'=>'Front\Client\ContestController@choose_contest_winner']);

			Route::get('payment/{enc_id}/{enc_id1}',['as'=>'front_client_contest_payment','uses'=>'Front\Client\ContestController@payment_methods']);

			Route::post('adding_info',['as'=>'front_client_contest_adding_info','uses'=>'Front\Client\ContestController@adding_info']);

			Route::post('subcatdata',['as'=>'front_client_contest_subcatdata','uses'=>'Front\Client\ContestController@subcatdata']);

			Route::POST('subcatdata_selected',['as'=>'front_client_contest_subcatdata','uses'=>'Front\Client\ContestController@subcatdata_selected']);

			Route::post('store_rating',	['as'=>'store-rating',	'uses'=>'Front\Client\ContestController@store_rating']);
     	});

     	// Mangopay
        Route::group(array('prefix' => '/wallet','middleware' =>'front_client'), function(){
            Route::get('/dashboard',['as'=>'front_client_wallet_dashboard','uses'=>'Front\Client\WalletDashboardController@show_dashboard']);
            Route::get('/withdraw',['as'=>'withdraw','uses'=>'Front\Client\WalletDashboardController@withdraw']);

            Route::get('/transactions',['as'=>'transactions','uses'=>'Front\Client\WalletDashboardController@transactions']);
            Route::any('/add_money',['as'=>'front_client_wallet_add_money','uses'=>'Front\Client\WalletDashboardController@add_money_wallet']);
            Route::any('/create_user',['as'=>'front_client_wallet_create_user','uses'=>'Front\Client\WalletDashboardController@create_user']);
            Route::any('/create_card',['as'=>'front_client_wallet_create_card','uses'=>'Front\Client\WalletDashboardController@create_card']);
            Route::any('/add_new_card',['as'=>'front_client_wallet_create_card','uses'=>'Front\Client\WalletDashboardController@create_card']);

            // Route::any('/create_card_return',['as'=>'front_client_wallet_create_card','uses'=>'Front\Client\WalletDashboardController@create_card_return']);
            
            
            Route::any('/finish_card_registration',['as'=>'front_client_wallet_finish_card_registration','uses'=>'Front\Client\WalletDashboardController@finish_card_registration']);
            Route::any('/card_pre_authorization',['as'=>'front_client_wallet_card_pre_authorization','uses'=>'Front\Client\WalletDashboardController@card_pre_authorization']);
            Route::any('/pre_authorization_payin',['as'=>'front_client_wallet_pre_authorization_pay_in','uses'=>'Front\Client\WalletDashboardController@pre_authorization_payin']);
            Route::any('/get_preauthorization_object',['as'=>'front_client_wallet_get_preauthorization_object','uses'=>'Front\Client\WalletDashboardController@get_preauthorization_object']);
            Route::any('/create_wallet_using_mp_owner_id/{mp_owner_id}',['as'=>'front_client_wallet_get_preauthorization_object','uses'=>'Front\Client\WalletDashboardController@create_wallet_using_mp_owner_id']);
            Route::any('/add_bank',['as'=>'front_client_wallet_add_bank','uses'=>'Front\Client\WalletDashboardController@add_bank']);
            
            Route::any('/cashout_request/{bank_id}',['as'=>'front_client_wallet_cashout_in_bank','uses'=>'Front\Client\WalletDashboardController@cashout_request']);

            Route::any('/cashout_in_bank',['as'=>'front_client_wallet_cashout_in_bank','uses'=>'Front\Client\WalletDashboardController@make_cashout_in_bank']);


            Route::any('/upload_kyc_docs',['as'=>'front_client_wallet_upload_kyc_docs','uses'=>'Front\Client\WalletDashboardController@upload_kyc_docs']);
            Route::any('/payin_refund',['as'=>'front_client_wallet_payin_refund','uses'=>'Front\Client\WalletDashboardController@payin_refund']);
            Route::any('/update_user_details',['as'=>'front_client_update_user_details','uses'=>'Front\Client\WalletDashboardController@update_user_details']);
        });

        Route::group(array('prefix' => '/tickets','middleware' =>'front_client'), function()
        {
			Route::get('/',						['uses'=>'Front\Client\SupportController@index']);
			Route::get('/details/{id}',			['uses'=>'Front\Client\SupportController@details']);
			Route::get('/create',				['uses'=>'Front\Client\SupportController@create']);
			Route::post('/store',				['uses'=>'Front\Client\SupportController@store']);
			Route::post('/submit_reply/{id}',	['uses'=>'Front\Client\SupportController@submit_reply']);
			Route::get('/reopen_ticket/{id}',	['uses'=>'Front\Client\SupportController@reopen_ticket']);
			Route::get('/resolve_ticket/{id}',	['uses'=>'Front\Client\SupportController@resolve_ticket']);
	 	});

	 	Route::group(array('prefix' => '/notifications','middleware' =>'front_client'), function()
        {
			Route::get('/',						['uses'=>'Front\Client\NotificationController@index']);
	 	});

    });

	Route::group(array('prefix' => '/clients','middleware' =>'front'), function(){
		Route::get('/portfolio/{enc_id}',['as'=>'show_portfolio','uses'=>'Front\Client\ProfileController@show_portfolio']);
	});

?>