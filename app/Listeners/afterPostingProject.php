<?php

namespace App\Listeners;

/* have to usee event name here */
use App\Events\sendEmailOnPostProject;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\ProjectpostModel;
use Sentinel;
use Mail;

class afterPostingProject
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(ProjectpostModel $project_post)
    {
        $this->ProjectpostModel = $project_post;
    }
    /**
     * Handle the event.
     *
     * @param  sendEmailOnPostProject  $event
     * @return void
    */
    public function handle(sendEmailOnPostProject $event) {   
        /* checking if user exists */
        $user = Sentinel::check();
        if(isset($user) && $user!= FALSE && $user->inRole('client'))
        {         
            $project_id       = $event->project_id;   
            $project_name     = config('app.project.name');
            //$mail_form        = get_site_email_address();   /* getting email address of admin from helper functions */
            $mail_form = isset($mail_form)?$mail_form:'no-reply@archexperts.com';
            
            $obj_project_info = $this->ProjectpostModel->where('id',$project_id)
                                                       ->with(['client_info'=> function ($query) {
                                                              $query->select('user_id','first_name','last_name','company_name','address');
                                                          }])
                                                       ->with(['project_skills','category_details'])
                                                       ->first(['id','project_name','project_description','client_user_id','category_id','sub_category_id']);

            $data = [];
            if($obj_project_info){
                $arr_project_info            = $obj_project_info->toArray();
                $client_name                 = "";
                /* get client name */
                $client_name                 = isset($arr_project_info['client_info']['first_name'])?$arr_project_info['client_info']['first_name']:'';
                $data['project_name']        = isset($arr_project_info['project_name'])?$arr_project_info['project_name']:'';
                $data['project_description'] = isset($arr_project_info['project_description'])?$arr_project_info['project_description']:'';
                $data['client_name']         = $client_name;
                $data['login_url']           = url('/redirection?redirect=PROJECT&id='.base64_encode($project_id));
                $email_to                    = isset($user->email)? $user->email:'';
                if($email_to!= "") {
                  try{
                      Mail::send('front.email.project_posted_success_email_to_client', $data, function ($message) use ($email_to,$mail_form,$project_name) {
                          $message->from($mail_form, $project_name);
                          $message->subject($project_name.': Project Posted Successfully.');
                          $message->to($email_to);
                      });
                  }
                  catch(\Exception $e){
                   \Session::Flash('error'   , 'E-Mail not sent');
                  }
                }
                /* send mail to experts */

            }
            return true;
        }
    }
}