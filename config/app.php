<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://localhost'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    //'timezone' => 'UTC',
    'timezone' => 'Europe/Berlin',
    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    //'key' => env('APP_KEY'),

     'key' => env('APP_KEY','base64:zAlmVdgLy6wIwBwdL5Dgk8LWCu8BLVlMND4CzAJzuq4='),


    'cipher' => 'AES-256-CBC',
    // 'cipher' => '',

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
    */

    'log' => env('APP_LOG', 'single'),

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,
        Maatwebsite\Excel\ExcelServiceProvider::class,


        /*
         * Application Service Providers...
         */

        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,
        Laracasts\Flash\FlashServiceProvider::class,
        App\Providers\HelperServiceProvider::class,
        Dimsav\Translatable\TranslatableServiceProvider::class,
        Cartalyst\Sentinel\Laravel\SentinelServiceProvider::class,
        App\Providers\HelperServiceProvider::class,
        Intervention\Image\ImageServiceProvider::class, // image resizing
        Cmgmyr\Messenger\MessengerServiceProvider::class, // messenger
        Elibyy\TCPDF\ServiceProvider::class, // PDF
        Chumper\Zipper\ZipperServiceProvider::class, // Zipper

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [
        'App'        => Illuminate\Support\Facades\App::class,
        'Artisan'    => Illuminate\Support\Facades\Artisan::class,
        'Auth'       => Illuminate\Support\Facades\Auth::class,
        'Blade'      => Illuminate\Support\Facades\Blade::class,
        'Cache'      => Illuminate\Support\Facades\Cache::class,
        'Config'     => Illuminate\Support\Facades\Config::class,
        'Cookie'     => Illuminate\Support\Facades\Cookie::class,
        'Crypt'      => Illuminate\Support\Facades\Crypt::class,
        'DB'         => Illuminate\Support\Facades\DB::class,
        'Eloquent'   => Illuminate\Database\Eloquent\Model::class,
        'Event'      => Illuminate\Support\Facades\Event::class,
        'File'       => Illuminate\Support\Facades\File::class,
        'Gate'       => Illuminate\Support\Facades\Gate::class,
        'Hash'       => Illuminate\Support\Facades\Hash::class,
        'Lang'       => Illuminate\Support\Facades\Lang::class,
        'Log'        => Illuminate\Support\Facades\Log::class,
        'Mail'       => Illuminate\Support\Facades\Mail::class,
        'Password'   => Illuminate\Support\Facades\Password::class,
        'Queue'      => Illuminate\Support\Facades\Queue::class,
        'Redirect'   => Illuminate\Support\Facades\Redirect::class,
        'Redis'      => Illuminate\Support\Facades\Redis::class,
        'Request'    => Illuminate\Support\Facades\Request::class,
        'Response'   => Illuminate\Support\Facades\Response::class,
        'Route'      => Illuminate\Support\Facades\Route::class,
        'Schema'     => Illuminate\Support\Facades\Schema::class,
        'Session'    => Illuminate\Support\Facades\Session::class,
        'Storage'    => Illuminate\Support\Facades\Storage::class,
        'URL'        => Illuminate\Support\Facades\URL::class,
        'Validator'  => Illuminate\Support\Facades\Validator::class,
        'View'       => Illuminate\Support\Facades\View::class,
        'Excel'      => Maatwebsite\Excel\Facades\Excel::class,
        'Flash'      => Laracasts\Flash\Flash::class,
        'Activation' => Cartalyst\Sentinel\Laravel\Facades\Activation::class,
        'Reminder'   => Cartalyst\Sentinel\Laravel\Facades\Reminder::class,
        'Sentinel'   => Cartalyst\Sentinel\Laravel\Facades\Sentinel::class,
        'CreditCard' => Inacho\CreditCard::class,
        'Image'      => Intervention\Image\Facades\Image::class,
        'PDF'        => Elibyy\TCPDF\Facades\TCPDF::class,
        'Zipper'     => Chumper\Zipper\Zipper::class,
    ],

    'project' => [
        'name'      =>'ArchExperts',
        'img_path'  =>[
                        'category'                  => '/uploads/categories/',
                        'profile_image'             => '/uploads/front/profile/',
                        'cover_image'               => '/uploads/front/cover_image/',
                        'bid_attachment'            => '/uploads/front/bids/',
                        'project_attachment'        => '/uploads/front/postprojects/',
                        'portfolio_image'           => '/uploads/front/portfolio/',
                        'blog_image'                => '/uploads/blog_image/',
                        'invoices_image'            => '/uploads/front/invoices/',
                        'expert_project_attachment' => '/uploads/front/project_attachments/',
                        'category_image'            => '/uploads/admin/categories_images/',
                        'profession_image'          => '/uploads/admin/professions_images/',
                        'contest_files'             => '/uploads/front/postcontest/',
                        'support_ticket_files'      => '/public/uploads/support_ticket/',
                        'contest_send_entry_files'  => '/uploads/front/postcontest/send_entry/',
                      ],
        'admin_panel_slug' =>'admin',
        'pagi_cnt'         => 10,
        //'project_url'      => url('/'),
        'project_url'      => 'https://www.archexperts.com/demo/public/',
        'RECAPTCHA_SITE_KEY'=> env('RECAPTCHA_SITE_KEY','6LeuzrMZAAAAAOwwwMQop4cgEQXrPhw4niSLh3dw'),
        'RECAPTCHA_SECRET_KEY'=> env('RECAPTCHA_SECRET_KEY','6LeuzrMZAAAAAE7uDpshLkNU76ZCspoygMSW9wfH'),
        'MangoPay'         => [
                                        'URL'                       => env('MangoPayDemo_URL','https://api.sandbox.mangopay.com'),
                                        'ClientId'                  => env('MangoPayDemo_ClientId','archibox'),
                                        'ClientPassword'            => env('MangoPayDemo_ClientPassword','8FcFejD24TytcZQ3F1PuTfZ9dZEnb0ARwCYRvCfxCHCdu02nnm'),
                                        'TemporaryFolder'           => public_path(env('MangoPayDemo_TemporaryFolder','uploads')),
                                        'owner_FirstName'           => env('MangoPay_owner_FirstName','Sachin'),
                                        'owner_LastName'            => env('MangoPay_owner_LastName','Jagtap'),
                                        'owner_Email'               => env('MangoPay_owner_Email','sachin@webwingtechnologies.com'),
                                        'owner_Nationality'         => env('MangoPay_owner_Nationality','IN'),
                                        'owner_CountryOfResidence'  => env('MangoPay_owner_CountryOfResidence','IN'),
                                        'owner_Address'             => env('MangoPay_owner_Address','6 Boulevard de Lorraine, 77360 Vaires-sur-Marne'),
                                        'owner_City'                => env('MangoPay_owner_City','Nashik'),
                                        'owner_Region'              => env('MangoPay_owner_Region','Nashik'),
                                        'owner_PostalCode'          => env('MangoPay_owner_PostalCode','422001'),
                                        'owner_Country'             => env('MangoPay_owner_Country','IN'),
                                        'owner_IBAN'                => env('MangoPay_owner_IBAN','FR7230002004210000133250B86'),
                                        'owner_BIC'                 => env('MangoPay_owner_BIC','CRLYFRPP')
                                ],          
        ],

     'project_fixed_rates'=>
    [
      'en'=> [
                    '5001-50000'=> 'Over 5001 (Jumbo)',
                    '2001-5000' => '2001-5000 (Big)',
                    '751-2000'  => '751-2000 (Large)',
                    '251-750'   => '251-750 (Medium)',
                    '51-250'    => '51-250 (Small)',
                    '20-50'     => '20-50 (Tiny)',
                ], 

        'de'=>[ 
                    '5001-50000'=> 'Über 5001 (Jumbo)',
                    '2001-5000' => '2001-5000 (Groß)',
                    '751-2000'  => '751-2000 (Groß)',
                    '251-750'   => '251-750 (Mittel)',
                    '51-250'    => '51-250 (Klein)',
                    '20-50'     => '20-50 (Winzig)',
              ]
    ],

    'public_fixed_rate'=>
    [
               '20-50'     => '20-50',
               '51-250'    => '51-250',
               '251-750'   => '251-750',
               '751-2000'  => '751-2000',
               '2001-5000' => '2001-5000',
               '5001-50000'=> 'Over 5001',
    ],

    'public_hourly_rates' => 
    [
              '5-10'  => '5-10 /hour',
              '11-25' => '11-25 /hour',
              '26-50' => '26-50 /hour',
              '51-100'    => 'Over 51 /hour',  
    ],

    'project_hourly_rates'=>
    [
        'en'=>[
                '51-100'  => 'Over 51 /hour',
                '26-50' => '26-50 /hour',
                '11-25'    => '11-25 /hour',
                '5-10'    => '5-10 /hour'
              ],

        'de'=>[
                '51-100'  => 'Über 51 /Stunde',
                '26-50' => '26-50 /Stunde',
                '11-25'    => '11-25 /Stunde',
                '5-10'    => '5-10 /Stunde'
               ]
    ],

    'project_duration'=>
    [
        '1-2' => '1-2 weeks',
        '3-4' => '3-4 weeks',
        '1-3' => '1-3 months',
        '3+' => '3+ months'
    ],

    'hours_per_week'=>
    [
        '1-10' => '1-10 hours/week',
        '11-30' => '11-30 hours/week',
        '30+' => '30+ hours/week',
    ],

    'project_other_rates'=>
    [
        'NDA_RATE'     => '20',
        'PRIVATE_RATE' => '20',
        'URGENT_RATE'  => '20',
    ],

    'project_currency'=>
    [
        '$'   => 'USD',
        '€'   => 'EUR',
        '£'   => 'GBP',
        'zł'  => 'PLN',
        'CHF' => 'CHF',
        'kr'  => 'NOK',
        'kr'  => 'SEK',
        'Kr.' => 'DKK',
        'CAD' => 'CAD',
        'R'   => 'ZAR',
        'AUD' => 'AUD',
        'HK$' => 'HKD',
        '¥'   => 'JPY',
        'Kč'  => 'CZK'
    ],

    'project_converted_currency'=>
    [
        "USD" => "$",
        "EUR" => "€",
        "GBP" => "£",
        "PLN" => "zł",
        "CHF" => "CHF",
        "NOK" => "kr",
        "SEK" => "kr",
        "DKK" => "Kr.",
        "CAD" => "CAD",
        "ZAR" => "R",
        "AUD" => "AUD",
        "HKD" => "HK$",
        "JPY" => "¥",
        "CZK" => "Kč"
    ],
	 'project_currency_symbol'=>
    [
        '$' => '$',
    ],
    'currency_symbol' => '$',
	    'project_post_document_formats' => 
    [   '264','3d2','3d4','3da','3dc','3dl','3dm','3ds','3dv','3dw','3dx','3dxml','3g1','3gp','3mf','a2c','aac','aaf','abs','act','ai','aif','amf','amr','an8','anim','anm','art','asc','asd','ase','asf','asm','asp','atl','atm','avi','avr','awg','b3d','bimx','bin','bip','bld','blend','bmf','bmp','br7','brd','bsp','bsw','bswx','c3d','c4','c4d','cad','cam','catdrawing','catpart','catproduct','ccb','cdl','cdr','cdw','cdx','cel','cf2','cfg','cg','cgfx','cgm','cgr','chr','cht','cib','clp','cmp','cpd','cpixml','cr2','crv','crw','crz','csl','csn','cso','csv','cyp','dae','dat','dcr','des','dft','dgn','dif','dif','divx','dlv','dlv3','dlv4','dlx','dmd','doc','docx','dpd','dps','dpt','drv','drw','dsf','dst','dtd','dts','duf','dvf','dwf','dwfx','dwg','dwk','dws','dwt','dwz','dxb','dxe','dxf','dxr','dxt','egg','emf','epf','eps','exc','exp','f3d','f4a','f4b','f4p','f4v','fbx','fg','fh','fh1','fh10','fh11','fh2','fh3','fh4','fh5','fh6','fh7','fh8','fh9','fla','flac','flc','fli','flm','flt','flv','fmv','fodg','fp3','fpf','frm','fsh','fxb','fxg','gdraw','gem','gif','gla','glm','gltf','glx','gml','gtc','gtcx','gts','h264','hcg','hgl','hip','hlsl','hpg','hpl','iam','idw','ifc','ifcXML','ifczip','iges','igs','ipn','ipt','irr','irrmesh','iso','iv','jff','jpeg','jpg','jt','jtf','key','keynote','kiss','kmz','lin','lmts','lnd','lpr','lwo','lxf','lxo','m1v','m2a','m2p','m2t','m2ts','m2v','m3d','m4a','m4b','m4p','m4v','ma','mat','max','maxc','mcd','md2','md5','md5anim','md5camera','md5mesh','mdc','mdd','mdl','meb','mesh','mgf','mgx','mid','mix','mj2','mjp2','model','mot','mov','mp','mp2','mp3','mp4','mpa','mpe','mpeg','mpg','mpv','ms3d','msh','mts','mtx','mu','mxf','mxm','mxs','nap','nc','nkb','nmf','nms','nurbs','nx','obj','objf','obz','oct','odc','odf','odg','odm','odp','ods','odt','off','ofx','ole','omf','p3d','p3l','p3l','p4d','p5d','pal','par','pcs','pct','pcx','pdf','pgl','phy','pic','pkg','pl','pl0','pl1','pl2','pln','plt','ply','pmd','pml','pmx','png','pov','ppf','ppi','pps','ppsm','ppsx','ppt','pptx','ppz','prc','prm','prn','pro','psa','psd','psid','psk','psm','pss','ptl','pub','qpr','raw','re1','re2','reb','rig','rm','rmvb','s3d','sat','scr','sda','sdl','sdnf','sds','sdw','session','sev','sh3d','shp','sht','sin','sketch','sketchpad','skp','sld','sldprt','smd','spr','srf','srg','staad','stc','std','step','stl','stp','svd','svg','svgz','svm','swf','sxc','sxd','sxg','sxi','sxw','sy3','t3d','tcc','tec','tga','thing','tif','tiff','tin','tmv','tri','ts','tvm','tvt','txt','u3d','uni','v3d','v3o','vcd','vda','vdafs','vec','vert','vob','vrl','vsd','vsh','wav','wire','wks','wma','wmf','wmv','wow','wrl','x','x_b','x_t','x3d','x3g','xaf','xgl','xl','xlc','xls','xlsx','xmf','xml','xps','xrf','xsf','yuv','z3d','zpr','zt','zip','rar','sql','php','json','cda',
    ],
    'CurrencyConversionAPIKey' => '607f18e4529df8766751'
];
