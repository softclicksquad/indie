<?php return array (
  'app' => 
  array (
    'env' => 'local',
    'debug' => true,
    'url' => 'http://archexperts.com/demo/public',
    'timezone' => 'Europe/Berlin',
    'locale' => 'en',
    'fallback_locale' => 'en',
    'key' => 'base64:NDKDc8RVOkBeLuAQhB1v2VK+adDddxkNkXFOwbOWLxM=',
    'cipher' => 'AES-256-CBC',
    'log' => 'single',
    'providers' => 
    array (
      0 => 'Illuminate\\Auth\\AuthServiceProvider',
      1 => 'Illuminate\\Broadcasting\\BroadcastServiceProvider',
      2 => 'Illuminate\\Bus\\BusServiceProvider',
      3 => 'Illuminate\\Cache\\CacheServiceProvider',
      4 => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
      5 => 'Illuminate\\Cookie\\CookieServiceProvider',
      6 => 'Illuminate\\Database\\DatabaseServiceProvider',
      7 => 'Illuminate\\Encryption\\EncryptionServiceProvider',
      8 => 'Illuminate\\Filesystem\\FilesystemServiceProvider',
      9 => 'Illuminate\\Foundation\\Providers\\FoundationServiceProvider',
      10 => 'Illuminate\\Hashing\\HashServiceProvider',
      11 => 'Illuminate\\Mail\\MailServiceProvider',
      12 => 'Illuminate\\Pagination\\PaginationServiceProvider',
      13 => 'Illuminate\\Pipeline\\PipelineServiceProvider',
      14 => 'Illuminate\\Queue\\QueueServiceProvider',
      15 => 'Illuminate\\Redis\\RedisServiceProvider',
      16 => 'Illuminate\\Auth\\Passwords\\PasswordResetServiceProvider',
      17 => 'Illuminate\\Session\\SessionServiceProvider',
      18 => 'Illuminate\\Translation\\TranslationServiceProvider',
      19 => 'Illuminate\\Validation\\ValidationServiceProvider',
      20 => 'Illuminate\\View\\ViewServiceProvider',
      21 => 'Maatwebsite\\Excel\\ExcelServiceProvider',
      22 => 'App\\Providers\\AppServiceProvider',
      23 => 'App\\Providers\\AuthServiceProvider',
      24 => 'App\\Providers\\EventServiceProvider',
      25 => 'App\\Providers\\RouteServiceProvider',
      26 => 'Laracasts\\Flash\\FlashServiceProvider',
      27 => 'App\\Providers\\HelperServiceProvider',
      28 => 'Dimsav\\Translatable\\TranslatableServiceProvider',
      29 => 'Cartalyst\\Sentinel\\Laravel\\SentinelServiceProvider',
      30 => 'App\\Providers\\HelperServiceProvider',
      31 => 'Intervention\\Image\\ImageServiceProvider',
      32 => 'Cmgmyr\\Messenger\\MessengerServiceProvider',
      33 => 'Elibyy\\TCPDF\\ServiceProvider',
      34 => 'Chumper\\Zipper\\ZipperServiceProvider',
    ),
    'aliases' => 
    array (
      'App' => 'Illuminate\\Support\\Facades\\App',
      'Artisan' => 'Illuminate\\Support\\Facades\\Artisan',
      'Auth' => 'Illuminate\\Support\\Facades\\Auth',
      'Blade' => 'Illuminate\\Support\\Facades\\Blade',
      'Cache' => 'Illuminate\\Support\\Facades\\Cache',
      'Config' => 'Illuminate\\Support\\Facades\\Config',
      'Cookie' => 'Illuminate\\Support\\Facades\\Cookie',
      'Crypt' => 'Illuminate\\Support\\Facades\\Crypt',
      'DB' => 'Illuminate\\Support\\Facades\\DB',
      'Eloquent' => 'Illuminate\\Database\\Eloquent\\Model',
      'Event' => 'Illuminate\\Support\\Facades\\Event',
      'File' => 'Illuminate\\Support\\Facades\\File',
      'Gate' => 'Illuminate\\Support\\Facades\\Gate',
      'Hash' => 'Illuminate\\Support\\Facades\\Hash',
      'Lang' => 'Illuminate\\Support\\Facades\\Lang',
      'Log' => 'Illuminate\\Support\\Facades\\Log',
      'Mail' => 'Illuminate\\Support\\Facades\\Mail',
      'Password' => 'Illuminate\\Support\\Facades\\Password',
      'Queue' => 'Illuminate\\Support\\Facades\\Queue',
      'Redirect' => 'Illuminate\\Support\\Facades\\Redirect',
      'Redis' => 'Illuminate\\Support\\Facades\\Redis',
      'Request' => 'Illuminate\\Support\\Facades\\Request',
      'Response' => 'Illuminate\\Support\\Facades\\Response',
      'Route' => 'Illuminate\\Support\\Facades\\Route',
      'Schema' => 'Illuminate\\Support\\Facades\\Schema',
      'Session' => 'Illuminate\\Support\\Facades\\Session',
      'Storage' => 'Illuminate\\Support\\Facades\\Storage',
      'URL' => 'Illuminate\\Support\\Facades\\URL',
      'Validator' => 'Illuminate\\Support\\Facades\\Validator',
      'View' => 'Illuminate\\Support\\Facades\\View',
      'Excel' => 'Maatwebsite\\Excel\\Facades\\Excel',
      'Flash' => 'Laracasts\\Flash\\Flash',
      'Activation' => 'Cartalyst\\Sentinel\\Laravel\\Facades\\Activation',
      'Reminder' => 'Cartalyst\\Sentinel\\Laravel\\Facades\\Reminder',
      'Sentinel' => 'Cartalyst\\Sentinel\\Laravel\\Facades\\Sentinel',
      'CreditCard' => 'Inacho\\CreditCard',
      'Image' => 'Intervention\\Image\\Facades\\Image',
      'PDF' => 'Elibyy\\TCPDF\\Facades\\TCPDF',
      'Zipper' => 'Chumper\\Zipper\\Zipper',
    ),
    'project' => 
    array (
      'name' => 'ArchExperts',
      'img_path' => 
      array (
        'category' => '/uploads/categories/',
        'profile_image' => '/uploads/front/profile/',
        'cover_image' => '/uploads/front/cover_image/',
        'bid_attachment' => '/uploads/front/bids/',
        'project_attachment' => '/uploads/front/postprojects/',
        'portfolio_image' => '/uploads/front/portfolio/',
        'blog_image' => '/uploads/blog_image/',
        'invoices_image' => '/uploads/front/invoices/',
        'expert_project_attachment' => '/uploads/front/project_attachments/',
        'category_image' => '/uploads/admin/categories_images/',
        'profession_image' => '/uploads/admin/professions_images/',
        'contest_files' => '/uploads/front/postcontest/',
        'support_ticket_files' => '/public/uploads/support_ticket/',
        'contest_send_entry_files' => '/uploads/front/postcontest/send_entry/',
      ),
      'admin_panel_slug' => 'admin',
      'pagi_cnt' => 10,
      'project_url' => 'https://www.archexperts.com/demo/public/',
      'RECAPTCHA_SITE_KEY' => '6LeuzrMZAAAAAOwwwMQop4cgEQXrPhw4niSLh3dw',
      'RECAPTCHA_SECRET_KEY' => '6LeuzrMZAAAAAE7uDpshLkNU76ZCspoygMSW9wfH',
      'MangoPay' => 
      array (
        'URL' => 'https://api.sandbox.mangopay.com',
        'ClientId' => 'archibox',
        'ClientPassword' => '8FcFejD24TytcZQ3F1PuTfZ9dZEnb0ARwCYRvCfxCHCdu02nnm',
        'TemporaryFolder' => 'E:\\wamp64\\www\\indie\\public\\uploads',
        'owner_FirstName' => 'Sachin',
        'owner_LastName' => 'Jagtap',
        'owner_Email' => 'sachin@webwingtechnologies.com',
        'owner_Nationality' => 'IN',
        'owner_CountryOfResidence' => 'IN',
        'owner_Address' => '6 Boulevard de Lorraine, 77360 Vaires-sur-Marne',
        'owner_City' => 'Nashik',
        'owner_Region' => 'Nashik',
        'owner_PostalCode' => '422001',
        'owner_Country' => 'IN',
        'owner_IBAN' => 'FR7230002004210000133250B86',
        'owner_BIC' => 'CRLYFRPP',
      ),
    ),
    'project_fixed_rates' => 
    array (
      'en' => 
      array (
        '5001-50000' => 'Over 5001 (Jumbo)',
        '2001-5000' => '2001-5000 (Big)',
        '751-2000' => '751-2000 (Large)',
        '251-750' => '251-750 (Medium)',
        '51-250' => '51-250 (Small)',
        '20-50' => '20-50 (Tiny)',
      ),
      'de' => 
      array (
        '5001-50000' => 'Über 5001 (Jumbo)',
        '2001-5000' => '2001-5000 (Groß)',
        '751-2000' => '751-2000 (Groß)',
        '251-750' => '251-750 (Mittel)',
        '51-250' => '51-250 (Klein)',
        '20-50' => '20-50 (Winzig)',
      ),
    ),
    'public_fixed_rate' => 
    array (
      '20-50' => '20-50',
      '51-250' => '51-250',
      '251-750' => '251-750',
      '751-2000' => '751-2000',
      '2001-5000' => '2001-5000',
      '5001-50000' => 'Over 5001',
    ),
    'public_hourly_rates' => 
    array (
      '5-10' => '5-10 /hour',
      '11-25' => '11-25 /hour',
      '26-50' => '26-50 /hour',
      '51-100' => 'Over 51 /hour',
    ),
    'project_hourly_rates' => 
    array (
      'en' => 
      array (
        '51-100' => 'Over 51 /hour',
        '26-50' => '26-50 /hour',
        '11-25' => '11-25 /hour',
        '5-10' => '5-10 /hour',
      ),
      'de' => 
      array (
        '51-100' => 'Über 51 /Stunde',
        '26-50' => '26-50 /Stunde',
        '11-25' => '11-25 /Stunde',
        '5-10' => '5-10 /Stunde',
      ),
    ),
    'project_duration' => 
    array (
      '1-2' => '1-2 weeks',
      '3-4' => '3-4 weeks',
      '1-3' => '1-3 months',
      '3+' => '3+ months',
    ),
    'hours_per_week' => 
    array (
      '1-10' => '1-10 hours/week',
      '11-30' => '11-30 hours/week',
      '30+' => '30+ hours/week',
    ),
    'project_other_rates' => 
    array (
      'NDA_RATE' => '20',
      'PRIVATE_RATE' => '20',
      'URGENT_RATE' => '20',
    ),
    'project_currency' => 
    array (
      '$' => 'USD',
      '€' => 'EUR',
      '£' => 'GBP',
      'zł' => 'PLN',
      'CHF' => 'CHF',
      'kr' => 'SEK',
      'Kr.' => 'DKK',
      'CAD' => 'CAD',
      'R' => 'ZAR',
      'AUD' => 'AUD',
      'HK$' => 'HKD',
      '¥' => 'JPY',
      'Kč' => 'CZK',
    ),
    'project_converted_currency' => 
    array (
      'USD' => '$',
      'EUR' => '€',
      'GBP' => '£',
      'PLN' => 'zł',
      'CHF' => 'CHF',
      'NOK' => 'kr',
      'SEK' => 'kr',
      'DKK' => 'Kr.',
      'CAD' => 'CAD',
      'ZAR' => 'R',
      'AUD' => 'AUD',
      'HKD' => 'HK$',
      'JPY' => '¥',
      'CZK' => 'Kč',
    ),
    'project_currency_symbol' => 
    array (
      '$' => '$',
    ),
    'currency_symbol' => '$',
    'project_post_document_formats' => 
    array (
      0 => '264',
      1 => '3d2',
      2 => '3d4',
      3 => '3da',
      4 => '3dc',
      5 => '3dl',
      6 => '3dm',
      7 => '3ds',
      8 => '3dv',
      9 => '3dw',
      10 => '3dx',
      11 => '3dxml',
      12 => '3g1',
      13 => '3gp',
      14 => '3mf',
      15 => 'a2c',
      16 => 'aac',
      17 => 'aaf',
      18 => 'abs',
      19 => 'act',
      20 => 'ai',
      21 => 'aif',
      22 => 'amf',
      23 => 'amr',
      24 => 'an8',
      25 => 'anim',
      26 => 'anm',
      27 => 'art',
      28 => 'asc',
      29 => 'asd',
      30 => 'ase',
      31 => 'asf',
      32 => 'asm',
      33 => 'asp',
      34 => 'atl',
      35 => 'atm',
      36 => 'avi',
      37 => 'avr',
      38 => 'awg',
      39 => 'b3d',
      40 => 'bimx',
      41 => 'bin',
      42 => 'bip',
      43 => 'bld',
      44 => 'blend',
      45 => 'bmf',
      46 => 'bmp',
      47 => 'br7',
      48 => 'brd',
      49 => 'bsp',
      50 => 'bsw',
      51 => 'bswx',
      52 => 'c3d',
      53 => 'c4',
      54 => 'c4d',
      55 => 'cad',
      56 => 'cam',
      57 => 'catdrawing',
      58 => 'catpart',
      59 => 'catproduct',
      60 => 'ccb',
      61 => 'cdl',
      62 => 'cdr',
      63 => 'cdw',
      64 => 'cdx',
      65 => 'cel',
      66 => 'cf2',
      67 => 'cfg',
      68 => 'cg',
      69 => 'cgfx',
      70 => 'cgm',
      71 => 'cgr',
      72 => 'chr',
      73 => 'cht',
      74 => 'cib',
      75 => 'clp',
      76 => 'cmp',
      77 => 'cpd',
      78 => 'cpixml',
      79 => 'cr2',
      80 => 'crv',
      81 => 'crw',
      82 => 'crz',
      83 => 'csl',
      84 => 'csn',
      85 => 'cso',
      86 => 'csv',
      87 => 'cyp',
      88 => 'dae',
      89 => 'dat',
      90 => 'dcr',
      91 => 'des',
      92 => 'dft',
      93 => 'dgn',
      94 => 'dif',
      95 => 'dif',
      96 => 'divx',
      97 => 'dlv',
      98 => 'dlv3',
      99 => 'dlv4',
      100 => 'dlx',
      101 => 'dmd',
      102 => 'doc',
      103 => 'docx',
      104 => 'dpd',
      105 => 'dps',
      106 => 'dpt',
      107 => 'drv',
      108 => 'drw',
      109 => 'dsf',
      110 => 'dst',
      111 => 'dtd',
      112 => 'dts',
      113 => 'duf',
      114 => 'dvf',
      115 => 'dwf',
      116 => 'dwfx',
      117 => 'dwg',
      118 => 'dwk',
      119 => 'dws',
      120 => 'dwt',
      121 => 'dwz',
      122 => 'dxb',
      123 => 'dxe',
      124 => 'dxf',
      125 => 'dxr',
      126 => 'dxt',
      127 => 'egg',
      128 => 'emf',
      129 => 'epf',
      130 => 'eps',
      131 => 'exc',
      132 => 'exp',
      133 => 'f3d',
      134 => 'f4a',
      135 => 'f4b',
      136 => 'f4p',
      137 => 'f4v',
      138 => 'fbx',
      139 => 'fg',
      140 => 'fh',
      141 => 'fh1',
      142 => 'fh10',
      143 => 'fh11',
      144 => 'fh2',
      145 => 'fh3',
      146 => 'fh4',
      147 => 'fh5',
      148 => 'fh6',
      149 => 'fh7',
      150 => 'fh8',
      151 => 'fh9',
      152 => 'fla',
      153 => 'flac',
      154 => 'flc',
      155 => 'fli',
      156 => 'flm',
      157 => 'flt',
      158 => 'flv',
      159 => 'fmv',
      160 => 'fodg',
      161 => 'fp3',
      162 => 'fpf',
      163 => 'frm',
      164 => 'fsh',
      165 => 'fxb',
      166 => 'fxg',
      167 => 'gdraw',
      168 => 'gem',
      169 => 'gif',
      170 => 'gla',
      171 => 'glm',
      172 => 'gltf',
      173 => 'glx',
      174 => 'gml',
      175 => 'gtc',
      176 => 'gtcx',
      177 => 'gts',
      178 => 'h264',
      179 => 'hcg',
      180 => 'hgl',
      181 => 'hip',
      182 => 'hlsl',
      183 => 'hpg',
      184 => 'hpl',
      185 => 'iam',
      186 => 'idw',
      187 => 'ifc',
      188 => 'ifcXML',
      189 => 'ifczip',
      190 => 'iges',
      191 => 'igs',
      192 => 'ipn',
      193 => 'ipt',
      194 => 'irr',
      195 => 'irrmesh',
      196 => 'iso',
      197 => 'iv',
      198 => 'jff',
      199 => 'jpeg',
      200 => 'jpg',
      201 => 'jt',
      202 => 'jtf',
      203 => 'key',
      204 => 'keynote',
      205 => 'kiss',
      206 => 'kmz',
      207 => 'lin',
      208 => 'lmts',
      209 => 'lnd',
      210 => 'lpr',
      211 => 'lwo',
      212 => 'lxf',
      213 => 'lxo',
      214 => 'm1v',
      215 => 'm2a',
      216 => 'm2p',
      217 => 'm2t',
      218 => 'm2ts',
      219 => 'm2v',
      220 => 'm3d',
      221 => 'm4a',
      222 => 'm4b',
      223 => 'm4p',
      224 => 'm4v',
      225 => 'ma',
      226 => 'mat',
      227 => 'max',
      228 => 'maxc',
      229 => 'mcd',
      230 => 'md2',
      231 => 'md5',
      232 => 'md5anim',
      233 => 'md5camera',
      234 => 'md5mesh',
      235 => 'mdc',
      236 => 'mdd',
      237 => 'mdl',
      238 => 'meb',
      239 => 'mesh',
      240 => 'mgf',
      241 => 'mgx',
      242 => 'mid',
      243 => 'mix',
      244 => 'mj2',
      245 => 'mjp2',
      246 => 'model',
      247 => 'mot',
      248 => 'mov',
      249 => 'mp',
      250 => 'mp2',
      251 => 'mp3',
      252 => 'mp4',
      253 => 'mpa',
      254 => 'mpe',
      255 => 'mpeg',
      256 => 'mpg',
      257 => 'mpv',
      258 => 'ms3d',
      259 => 'msh',
      260 => 'mts',
      261 => 'mtx',
      262 => 'mu',
      263 => 'mxf',
      264 => 'mxm',
      265 => 'mxs',
      266 => 'nap',
      267 => 'nc',
      268 => 'nkb',
      269 => 'nmf',
      270 => 'nms',
      271 => 'nurbs',
      272 => 'nx',
      273 => 'obj',
      274 => 'objf',
      275 => 'obz',
      276 => 'oct',
      277 => 'odc',
      278 => 'odf',
      279 => 'odg',
      280 => 'odm',
      281 => 'odp',
      282 => 'ods',
      283 => 'odt',
      284 => 'off',
      285 => 'ofx',
      286 => 'ole',
      287 => 'omf',
      288 => 'p3d',
      289 => 'p3l',
      290 => 'p3l',
      291 => 'p4d',
      292 => 'p5d',
      293 => 'pal',
      294 => 'par',
      295 => 'pcs',
      296 => 'pct',
      297 => 'pcx',
      298 => 'pdf',
      299 => 'pgl',
      300 => 'phy',
      301 => 'pic',
      302 => 'pkg',
      303 => 'pl',
      304 => 'pl0',
      305 => 'pl1',
      306 => 'pl2',
      307 => 'pln',
      308 => 'plt',
      309 => 'ply',
      310 => 'pmd',
      311 => 'pml',
      312 => 'pmx',
      313 => 'png',
      314 => 'pov',
      315 => 'ppf',
      316 => 'ppi',
      317 => 'pps',
      318 => 'ppsm',
      319 => 'ppsx',
      320 => 'ppt',
      321 => 'pptx',
      322 => 'ppz',
      323 => 'prc',
      324 => 'prm',
      325 => 'prn',
      326 => 'pro',
      327 => 'psa',
      328 => 'psd',
      329 => 'psid',
      330 => 'psk',
      331 => 'psm',
      332 => 'pss',
      333 => 'ptl',
      334 => 'pub',
      335 => 'qpr',
      336 => 'raw',
      337 => 're1',
      338 => 're2',
      339 => 'reb',
      340 => 'rig',
      341 => 'rm',
      342 => 'rmvb',
      343 => 's3d',
      344 => 'sat',
      345 => 'scr',
      346 => 'sda',
      347 => 'sdl',
      348 => 'sdnf',
      349 => 'sds',
      350 => 'sdw',
      351 => 'session',
      352 => 'sev',
      353 => 'sh3d',
      354 => 'shp',
      355 => 'sht',
      356 => 'sin',
      357 => 'sketch',
      358 => 'sketchpad',
      359 => 'skp',
      360 => 'sld',
      361 => 'sldprt',
      362 => 'smd',
      363 => 'spr',
      364 => 'srf',
      365 => 'srg',
      366 => 'staad',
      367 => 'stc',
      368 => 'std',
      369 => 'step',
      370 => 'stl',
      371 => 'stp',
      372 => 'svd',
      373 => 'svg',
      374 => 'svgz',
      375 => 'svm',
      376 => 'swf',
      377 => 'sxc',
      378 => 'sxd',
      379 => 'sxg',
      380 => 'sxi',
      381 => 'sxw',
      382 => 'sy3',
      383 => 't3d',
      384 => 'tcc',
      385 => 'tec',
      386 => 'tga',
      387 => 'thing',
      388 => 'tif',
      389 => 'tiff',
      390 => 'tin',
      391 => 'tmv',
      392 => 'tri',
      393 => 'ts',
      394 => 'tvm',
      395 => 'tvt',
      396 => 'txt',
      397 => 'u3d',
      398 => 'uni',
      399 => 'v3d',
      400 => 'v3o',
      401 => 'vcd',
      402 => 'vda',
      403 => 'vdafs',
      404 => 'vec',
      405 => 'vert',
      406 => 'vob',
      407 => 'vrl',
      408 => 'vsd',
      409 => 'vsh',
      410 => 'wav',
      411 => 'wire',
      412 => 'wks',
      413 => 'wma',
      414 => 'wmf',
      415 => 'wmv',
      416 => 'wow',
      417 => 'wrl',
      418 => 'x',
      419 => 'x_b',
      420 => 'x_t',
      421 => 'x3d',
      422 => 'x3g',
      423 => 'xaf',
      424 => 'xgl',
      425 => 'xl',
      426 => 'xlc',
      427 => 'xls',
      428 => 'xlsx',
      429 => 'xmf',
      430 => 'xml',
      431 => 'xps',
      432 => 'xrf',
      433 => 'xsf',
      434 => 'yuv',
      435 => 'z3d',
      436 => 'zpr',
      437 => 'zt',
      438 => 'zip',
      439 => 'rar',
      440 => 'sql',
      441 => 'php',
      442 => 'json',
      443 => 'cda',
    ),
    'CurrencyConversionAPIKey' => '607f18e4529df8766751',
  ),
  'auth' => 
  array (
    'defaults' => 
    array (
      'guard' => 'web',
      'passwords' => 'users',
    ),
    'guards' => 
    array (
      'web' => 
      array (
        'driver' => 'session',
        'provider' => 'users',
      ),
      'api' => 
      array (
        'driver' => 'token',
        'provider' => 'users',
      ),
    ),
    'providers' => 
    array (
      'users' => 
      array (
        'driver' => 'eloquent',
        'model' => 'App\\User',
      ),
    ),
    'passwords' => 
    array (
      'users' => 
      array (
        'provider' => 'users',
        'email' => 'auth.emails.password',
        'table' => 'password_resets',
        'expire' => 60,
      ),
    ),
  ),
  'broadcasting' => 
  array (
    'default' => 'pusher',
    'connections' => 
    array (
      'pusher' => 
      array (
        'driver' => 'pusher',
        'key' => NULL,
        'secret' => NULL,
        'app_id' => NULL,
        'options' => 
        array (
        ),
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
      ),
      'log' => 
      array (
        'driver' => 'log',
      ),
    ),
  ),
  'cache' => 
  array (
    'default' => 'file',
    'stores' => 
    array (
      'apc' => 
      array (
        'driver' => 'apc',
      ),
      'array' => 
      array (
        'driver' => 'array',
      ),
      'database' => 
      array (
        'driver' => 'database',
        'table' => 'cache',
        'connection' => NULL,
      ),
      'file' => 
      array (
        'driver' => 'file',
        'path' => 'E:\\wamp64\\www\\indie\\storage\\framework/cache',
      ),
      'memcached' => 
      array (
        'driver' => 'memcached',
        'servers' => 
        array (
          0 => 
          array (
            'host' => '127.0.0.1',
            'port' => 11211,
            'weight' => 100,
          ),
        ),
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
      ),
    ),
    'prefix' => 'laravel',
  ),
  'cartalyst' => 
  array (
    'sentinel' => 
    array (
      'session' => 'cartalyst_sentinel',
      'cookie' => 'cartalyst_sentinel',
      'users' => 
      array (
        'model' => 'App\\Models\\UserModel',
      ),
      'roles' => 
      array (
        'model' => 'Cartalyst\\Sentinel\\Roles\\EloquentRole',
      ),
      'permissions' => 
      array (
        'class' => 'Cartalyst\\Sentinel\\Permissions\\StandardPermissions',
      ),
      'persistences' => 
      array (
        'model' => 'Cartalyst\\Sentinel\\Persistences\\EloquentPersistence',
        'single' => false,
      ),
      'checkpoints' => 
      array (
        0 => 'throttle',
        1 => 'activation',
      ),
      'activations' => 
      array (
        'model' => 'Cartalyst\\Sentinel\\Activations\\EloquentActivation',
        'expires' => 259200,
        'lottery' => 
        array (
          0 => 2,
          1 => 100,
        ),
      ),
      'reminders' => 
      array (
        'model' => 'Cartalyst\\Sentinel\\Reminders\\EloquentReminder',
        'expires' => 14400,
        'lottery' => 
        array (
          0 => 2,
          1 => 100,
        ),
      ),
      'throttling' => 
      array (
        'model' => 'Cartalyst\\Sentinel\\Throttling\\EloquentThrottle',
        'global' => 
        array (
          'interval' => 900,
          'thresholds' => 
          array (
            10 => 1,
            20 => 2,
            30 => 4,
            40 => 8,
            50 => 16,
            60 => 12,
          ),
        ),
        'ip' => 
        array (
          'interval' => 900,
          'thresholds' => 5,
        ),
        'user' => 
        array (
          'interval' => 900,
          'thresholds' => 5,
        ),
      ),
    ),
  ),
  'compile' => 
  array (
    'files' => 
    array (
    ),
    'providers' => 
    array (
    ),
  ),
  'database' => 
  array (
    'fetch' => 8,
    'default' => 'mysql',
    'connections' => 
    array (
      'sqlite' => 
      array (
        'driver' => 'sqlite',
        'database' => 'indie',
        'prefix' => '',
      ),
      'mysql' => 
      array (
        'driver' => 'mysql',
        'host' => 'localhost',
        'port' => '3306',
        'database' => 'indie',
        'username' => 'root',
        'password' => '',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix' => 'vhc_',
        'strict' => false,
        'engine' => NULL,
      ),
      'pgsql' => 
      array (
        'driver' => 'pgsql',
        'host' => 'localhost',
        'port' => '5432',
        'database' => 'indie',
        'username' => 'root',
        'password' => '',
        'charset' => 'utf8',
        'prefix' => '',
        'schema' => 'public',
      ),
    ),
    'migrations' => 'migrations',
    'redis' => 
    array (
      'cluster' => false,
      'default' => 
      array (
        'host' => 'localhost',
        'password' => NULL,
        'port' => '6379',
        'database' => 0,
      ),
    ),
  ),
  'excel' => 
  array (
    'cache' => 
    array (
      'enable' => true,
      'driver' => 'memory',
      'settings' => 
      array (
        'memoryCacheSize' => '32MB',
        'cacheTime' => 600,
      ),
      'memcache' => 
      array (
        'host' => 'localhost',
        'port' => 11211,
      ),
      'dir' => 'E:\\wamp64\\www\\indie\\storage\\cache',
    ),
    'properties' => 
    array (
      'creator' => 'Maatwebsite',
      'lastModifiedBy' => 'Maatwebsite',
      'title' => 'Spreadsheet',
      'description' => 'Default spreadsheet export',
      'subject' => 'Spreadsheet export',
      'keywords' => 'maatwebsite, excel, export',
      'category' => 'Excel',
      'manager' => 'Maatwebsite',
      'company' => 'Maatwebsite',
    ),
    'sheets' => 
    array (
      'pageSetup' => 
      array (
        'orientation' => 'portrait',
        'paperSize' => '9',
        'scale' => '100',
        'fitToPage' => false,
        'fitToHeight' => true,
        'fitToWidth' => true,
        'columnsToRepeatAtLeft' => 
        array (
          0 => '',
          1 => '',
        ),
        'rowsToRepeatAtTop' => 
        array (
          0 => 0,
          1 => 0,
        ),
        'horizontalCentered' => false,
        'verticalCentered' => false,
        'printArea' => NULL,
        'firstPageNumber' => NULL,
      ),
    ),
    'creator' => 'Maatwebsite',
    'csv' => 
    array (
      'delimiter' => ',',
      'enclosure' => '"',
      'line_ending' => '
',
    ),
    'export' => 
    array (
      'autosize' => true,
      'autosize-method' => 'approx',
      'generate_heading_by_indices' => true,
      'merged_cell_alignment' => 'left',
      'calculate' => false,
      'includeCharts' => false,
      'sheets' => 
      array (
        'page_margin' => false,
        'nullValue' => NULL,
        'startCell' => 'A1',
        'strictNullComparison' => false,
      ),
      'store' => 
      array (
        'path' => 'E:\\wamp64\\www\\indie\\storage\\exports',
        'returnInfo' => false,
      ),
      'pdf' => 
      array (
        'driver' => 'DomPDF',
        'drivers' => 
        array (
          'DomPDF' => 
          array (
            'path' => 'E:\\wamp64\\www\\indie\\vendor/dompdf/dompdf/',
          ),
          'tcPDF' => 
          array (
            'path' => 'E:\\wamp64\\www\\indie\\vendor/tecnick.com/tcpdf/',
          ),
          'mPDF' => 
          array (
            'path' => 'E:\\wamp64\\www\\indie\\vendor/mpdf/mpdf/',
          ),
        ),
      ),
    ),
    'filters' => 
    array (
      'registered' => 
      array (
        'chunk' => 'Maatwebsite\\Excel\\Filters\\ChunkReadFilter',
      ),
      'enabled' => 
      array (
      ),
    ),
    'import' => 
    array (
      'heading' => 'slugged',
      'startRow' => 1,
      'separator' => '_',
      'includeCharts' => false,
      'to_ascii' => true,
      'encoding' => 
      array (
        'input' => 'UTF-8',
        'output' => 'UTF-8',
      ),
      'calculate' => true,
      'ignoreEmpty' => false,
      'force_sheets_collection' => false,
      'dates' => 
      array (
        'enabled' => true,
        'format' => false,
        'columns' => 
        array (
        ),
      ),
      'sheets' => 
      array (
        'test' => 
        array (
          'firstname' => 'A2',
        ),
      ),
    ),
    'views' => 
    array (
      'styles' => 
      array (
        'th' => 
        array (
          'font' => 
          array (
            'bold' => true,
            'size' => 12,
          ),
        ),
        'strong' => 
        array (
          'font' => 
          array (
            'bold' => true,
            'size' => 12,
          ),
        ),
        'b' => 
        array (
          'font' => 
          array (
            'bold' => true,
            'size' => 12,
          ),
        ),
        'i' => 
        array (
          'font' => 
          array (
            'italic' => true,
            'size' => 12,
          ),
        ),
        'h1' => 
        array (
          'font' => 
          array (
            'bold' => true,
            'size' => 24,
          ),
        ),
        'h2' => 
        array (
          'font' => 
          array (
            'bold' => true,
            'size' => 18,
          ),
        ),
        'h3' => 
        array (
          'font' => 
          array (
            'bold' => true,
            'size' => 13.5,
          ),
        ),
        'h4' => 
        array (
          'font' => 
          array (
            'bold' => true,
            'size' => 12,
          ),
        ),
        'h5' => 
        array (
          'font' => 
          array (
            'bold' => true,
            'size' => 10,
          ),
        ),
        'h6' => 
        array (
          'font' => 
          array (
            'bold' => true,
            'size' => 7.5,
          ),
        ),
        'a' => 
        array (
          'font' => 
          array (
            'underline' => true,
            'color' => 
            array (
              'argb' => 'FF0000FF',
            ),
          ),
        ),
        'hr' => 
        array (
          'borders' => 
          array (
            'bottom' => 
            array (
              'style' => 'thin',
              'color' => 
              array (
                0 => 'FF000000',
              ),
            ),
          ),
        ),
      ),
    ),
  ),
  'filesystems' => 
  array (
    'default' => 'local',
    'cloud' => 's3',
    'disks' => 
    array (
      'local' => 
      array (
        'driver' => 'local',
        'root' => 'E:\\wamp64\\www\\indie\\storage\\app',
      ),
      'public' => 
      array (
        'driver' => 'local',
        'root' => 'E:\\wamp64\\www\\indie\\storage\\app/public',
        'visibility' => 'public',
      ),
      's3' => 
      array (
        'driver' => 's3',
        'key' => 'your-key',
        'secret' => 'your-secret',
        'region' => 'your-region',
        'bucket' => 'your-bucket',
      ),
    ),
  ),
  'image' => 
  array (
    'driver' => 'gd',
  ),
  'imagecache' => 
  array (
    'route' => NULL,
    'paths' => 
    array (
      0 => 'E:\\wamp64\\www\\indie\\public\\upload',
      1 => 'E:\\wamp64\\www\\indie\\public\\images',
    ),
    'templates' => 
    array (
      'small' => 'Intervention\\Image\\Templates\\Small',
      'medium' => 'Intervention\\Image\\Templates\\Medium',
      'large' => 'Intervention\\Image\\Templates\\Large',
    ),
    'lifetime' => 43200,
  ),
  'mail' => 
  array (
    'driver' => 'mail',
    'host' => 'smtp.ionos.de',
    'port' => '587',
    'from' => 
    array (
      'address' => NULL,
      'name' => NULL,
    ),
    'encryption' => 'tls',
    'username' => 'no-reply@archexperts.com',
    'password' => '1N!#8KULjK4}2eL@4',
    'sendmail' => '/usr/sbin/sendmail -bs',
  ),
  'messenger' => 
  array (
    'user_model' => 'App\\Models\\UserModel',
    'message_model' => 'Cmgmyr\\Messenger\\Models\\Message',
    'participant_model' => 'Cmgmyr\\Messenger\\Models\\Participant',
    'thread_model' => 'Cmgmyr\\Messenger\\Models\\Thread',
    'messages_table' => 'messenger_messages',
    'participants_table' => 'messenger_participants',
    'threads_table' => 'messenger_threads',
  ),
  'paypal' => 
  array (
    'client_id' => 'AV2kbYOwsga_37DiEybRI7kQprodd5erP4SvAieGMnRE6bBTph5Dl8STv9WUB2Yki0_yh__eSjMqMY7w',
    'secret' => 'EImTBWVa6ZyH2k7wm3jJf7aXpoInq7YnbKnKjavM0PK0oPnRjwRY8n_Z4gZcM5QGPDYS6scJoCeVRrPR',
    'settings' => 
    array (
      'mode' => 'sandbox',
      'http.ConnectionTimeOut' => 100,
      'log.LogEnabled' => true,
      'log.FileName' => 'E:\\wamp64\\www\\indie\\storage/logs/paypal.log',
      'log.LogLevel' => 'FINE',
    ),
  ),
  'queue' => 
  array (
    'default' => 'sync',
    'connections' => 
    array (
      'sync' => 
      array (
        'driver' => 'sync',
      ),
      'database' => 
      array (
        'driver' => 'database',
        'table' => 'jobs',
        'queue' => 'default',
        'expire' => 60,
      ),
      'beanstalkd' => 
      array (
        'driver' => 'beanstalkd',
        'host' => 'localhost',
        'queue' => 'default',
        'ttr' => 60,
      ),
      'sqs' => 
      array (
        'driver' => 'sqs',
        'key' => 'your-public-key',
        'secret' => 'your-secret-key',
        'prefix' => 'https://sqs.us-east-1.amazonaws.com/your-account-id',
        'queue' => 'your-queue-name',
        'region' => 'us-east-1',
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
        'queue' => 'default',
        'expire' => 60,
      ),
    ),
    'failed' => 
    array (
      'database' => 'mysql',
      'table' => 'failed_jobs',
    ),
  ),
  'services' => 
  array (
    'mailgun' => 
    array (
      'domain' => NULL,
      'secret' => NULL,
    ),
    'ses' => 
    array (
      'key' => NULL,
      'secret' => NULL,
      'region' => 'us-east-1',
    ),
    'sparkpost' => 
    array (
      'secret' => NULL,
    ),
    'stripe' => 
    array (
      'model' => 'App\\User',
      'key' => NULL,
      'secret' => NULL,
    ),
  ),
  'session' => 
  array (
    'driver' => 'file',
    'lifetime' => 30,
    'expire_on_close' => true,
    'encrypt' => false,
    'files' => 'E:\\wamp64\\www\\indie\\storage\\framework/sessions',
    'connection' => NULL,
    'table' => 'sessions',
    'lottery' => 
    array (
      0 => 2,
      1 => 100,
    ),
    'cookie' => 'laravel_session',
    'path' => '/',
    'domain' => NULL,
    'secure' => false,
    'http_only' => true,
  ),
  'tcpdf' => 
  array (
    'page_format' => 'A4',
    'page_orientation' => 'P',
    'page_units' => 'mm',
    'unicode' => true,
    'encoding' => 'UTF-8',
    'font_directory' => '',
    'image_directory' => '',
    'tcpdf_throw_exception' => false,
  ),
  'translatable' => 
  array (
    'locales' => 
    array (
      0 => 'en',
      1 => 'de',
    ),
    'locale_separator' => '-',
    'locale' => NULL,
    'use_fallback' => false,
    'fallback_locale' => 'en',
    'translation_suffix' => 'Translation',
    'locale_key' => 'locale',
    'always_fillable' => false,
  ),
  'view' => 
  array (
    'paths' => 
    array (
      0 => 'E:\\wamp64\\www\\indie\\resources\\views',
    ),
    'compiled' => 'E:\\wamp64\\www\\indie\\storage\\framework\\views',
  ),
);
