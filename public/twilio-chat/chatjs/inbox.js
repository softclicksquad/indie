var username = "Bylancer";
var Ses_img = "Bylancer.jpg";
var audioogg = new Audio(twilio_chat_base_url + '/audio/chat.ogg');
var audiomp3 = new Audio(twilio_chat_base_url + '/audio/chat.mp3');

function scrollDown(){
    var wtf    = $('.wchat-chat-body');
    var height = wtf[0].scrollHeight;
    wtf.scrollTop(height);
    $(".scroll-down").css({'visibility':'hidden'});
}

function checkChatBoxInputKey(event,chatboxtextarea) {

    $(".input-placeholder").css({'visibility':'hidden'});

    if((event.keyCode == 13 && event.shiftKey == 0))  {
        message = $(chatboxtextarea).val();
        message = message.replace(/^\s+|\s+$/g,"");

        $(chatboxtextarea).val('');
        $(chatboxtextarea).focus();
        $(".input-placeholder").css({'visibility':'visible'});
        $(".chatboxtextarea").css('height','20px');
        if (message != '') {
            message = message.replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/\"/g,"&quot;");
            message = message.replace(/\n/g, "<br />");
            var $con = message;
            var $words = $con.split(' ');
            for (i in $words) {
                if ($words[i].indexOf('http://') == 0 || $words[i].indexOf('https://') == 0) {
                    $words[i] = '<a href="' + $words[i] + '">' + $words[i] + '</a>';
                }
                else if ($words[i].indexOf('www') == 0 ) {
                    $words[i] = '<a href="' + $words[i] + '">' + $words[i] + '</a>';
                }
            }
            message = $words.join(' ');
            message = emojione.shortnameToImage(message); // Set imotions
            
            if(projectChannel!=undefined){
                projectChannel.sendMessage(message);
            }

            $(".target-emoji").css({'display':'none'});
            $('.wchat-filler').css({'height':0+'px'});

            scrollDown();
        }

        return false;
    }

    var adjustedHeight = chatboxtextarea.clientHeight;
    var maxHeight = 60;

    if (maxHeight > adjustedHeight) {
        adjustedHeight = Math.max(chatboxtextarea.scrollHeight, adjustedHeight);

        if (maxHeight)
            adjustedHeight = Math.min(maxHeight, adjustedHeight);
        if (adjustedHeight > chatboxtextarea.clientHeight)
            $(chatboxtextarea).css('height',adjustedHeight+8 +'px');
    } else {
        $(chatboxtextarea).css('overflow','auto');
    }

}

function clickTosendMessage() {
    message = $(".chatboxtextarea").val();

    message = message.replace(/^\s+|\s+$/g,"");

    $(".chatboxtextarea").val('');
    $(".chatboxtextarea").focus();
    $(".input-placeholder").css({'visibility':'visible'});
    $(".chatboxtextarea").css('height','20px');
    if (message != '') {

        message = message.replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/\"/g,"&quot;");
        message = message.replace(/\n/g, "<br />");
        var $con = message;
        var $words = $con.split(' ');
        for (i in $words) {
            if ($words[i].indexOf('http://') == 0 || $words[i].indexOf('https://') == 0) {
                $words[i] = '<a href="' + $words[i] + '">' + $words[i] + '</a>';
            }
            else if ($words[i].indexOf('www') == 0 ) {
                $words[i] = '<a href="' + $words[i] + '">' + $words[i] + '</a>';
            }
        }
        message = $words.join(' ');
        message = emojione.shortnameToImage(message);  // Set imotions

        if(projectChannel!=undefined){
            projectChannel.sendMessage(message);
        }

        $(".target-emoji").css({'display':'none'});
        $('.wchat-filler').css({'height':0+'px'});
        scrollDown();
    }



    var adjustedHeight = $(".chatboxtextarea").clientHeight;
    var maxHeight = 40;

    if (maxHeight > adjustedHeight) {
        adjustedHeight = Math.max($(".chatboxtextarea").scrollHeight, adjustedHeight);
        if (maxHeight)
            adjustedHeight = Math.min(maxHeight, adjustedHeight);
        if (adjustedHeight > $(".chatboxtextarea").clientHeight)
            $($(".chatboxtextarea")).css('height',adjustedHeight+8 +'px');
    } else {
        $($(".chatboxtextarea")).css('overflow','auto');
    }
    return false;
}