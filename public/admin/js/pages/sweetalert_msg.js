function confirm_action(e, t, o) {
    o = o || !1;
    t.preventDefault(), swal({
        title: "Are you sure ?",
        text: o,
        type: "warning",
        showCancelButton: !0,
        confirmButtonColor: "#82b548",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: !1,
        closeOnCancel: !0
    }, function(t) {
        1 == t && (showProcessingOverlay(), window.location = $(e).attr("href"))
    })
}

function check_multi_action(e, t) {
    var o = $('input[name="checked_record[]"]:checked').length,
        n = $("#" + e);
    if (o <= 0) return swal("Oops..", "Please select the record to perform this Action."), !1;
    if ("delete" == t) var c = "Do you really want to delete selected record(s) ?";
    else if ("deactivate" == t) c = "Do you really want to in-activate selected record(s) ?";
    else if ("activate" == t) c = "Do you really want to activate selected record(s) ?";
    swal({
        title: "Are you sure ?",
        text: c,
        type: "warning",
        showCancelButton: !0,
        confirmButtonColor: "#82b548",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: !0,
        closeOnCancel: !0
    }, function(e) {
        if (!e) return !1;
        $('input[name="multi_action"]').val(t), $(n)[0].submit(), showProcessingOverlay()
    })
}

function showAlert(e, t, o) {
    return o = o || "Ok", swal({
        title: "",
        text: e,
        type: t,
        confirmButtonText: o,
        showCancelButton: !0,
        closeOnCancel: !0
    }), !1
}