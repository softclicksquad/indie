/* ------------------------------------------------------------------------------
*
*  # Styled checkboxes, radios and file input
*
*  Specific JS code additions for form_checkboxes_radios.html page
*
*  Version: 1.0
*  Latest update: Aug 1, 2015
*
* ---------------------------------------------------------------------------- */

$(function() {
    if (Array.prototype.forEach)(e = Array.prototype.slice.call(document.querySelectorAll(".switchery"))).forEach(function(e) {
        new Switchery(e)
    });
    else
        for (var e = document.querySelectorAll(".switchery"), r = 0; r < e.length; r++) new Switchery(e[r])
});
$(".control-success").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-success-600 text-success-800'
    });